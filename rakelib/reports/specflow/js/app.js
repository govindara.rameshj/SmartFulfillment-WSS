var mainApp = angular.module('specflowReport', ['ui.bootstrap']);

mainApp.controller('reportController', function($scope) {
  
  $scope.dataLoaded = false;
  $scope.features = reportData.features;
  $scope.dataLoaded = true;
  
  $scope.isFeatCollapsed = true;
  $scope.isScenarioCollapsed = true;
  $scope.filter = '';

  $scope.features = _.map($scope.features, function(feature) {
    if (feature.tags.length > 0) {
      return feature;
    } else {
      feature.tags.push('Undefined');
      return feature;
    }
  });

  $scope.combineFeatureName = function(feature){
    return feature.tags.join(" ") + ' - ' + feature.title;
  };
  
  $scope.uniqueTags = function(){   
    return _.uniq(_.flatten(_.pluck(this.features, 'tags')));
  };

  $scope.chooseColor = function (result) {
    if (result == "OK") {
      return "success";
    } else if (result == "Error") {
      return "fail";
    } else {
      return "";
    }
  };
  
  $scope.recombineSteps = function (a, typeA, b, typeB, getter) {
    var typedA = _.map(a, function(value) { return { type: typeA, value: value} });
    var typedB = _.map(b, function(value) { return { type: typeB, value: value} });

    var union = _.sortBy(_.union(typedA, typedB), function(item) {
        return getter(item.value)});
        
    var result = [];    
    _.reduce(union, function(group, pair) {
      if (group == null || pair.type != group.type) {
          group = { type: pair.type, items: [pair.value]}; 
          result.push(group);        
      }
      else {
          group.items.push(pair.value);
      }        
      return group;
    }, null);
    
    return result;
  };
  
  _.forEach($scope.features, function(feature){
    _.forEach(feature.scenarios, function(scenario){
      scenario.recombined = $scope.recombineSteps(scenario.when.steps, 'When', scenario.then.steps, 'Then', function(item){return item.end_time;});
    })
  });
  
});