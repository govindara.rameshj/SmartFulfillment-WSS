## This script cleans registry after building
#  You should specify Till UK folder path as a parameter

require 'win32/registry'
require 'rexml/document'

def read_registry(type, key)
  Win32::Registry::HKEY_CLASSES_ROOT.open("#{type}#{key}")
rescue
  #puts "#{type}\\#{key} was not found"
end

def delete_reg(type, key)
  access = Win32::Registry::KEY_ALL_ACCESS
  Win32::Registry::HKEY_CLASSES_ROOT.open("#{type}#{key}", access) do |reg|
    unless reg.nil?
      reg.delete_key('', true)
      puts "#{type}\\#{key} has been successfully deleted"
    end
  end
  rescue
end

def collect_project_names(file)
  xml = File.read(file)
  doc = REXML::Document.new(xml)
  project_list = []
  doc.elements.each('Project/ItemGroup/*') do |e|
    project_list << e.attributes['Include'].gsub('$(ProjectsDir)\\', '')
  end
  project_list
end

def grep(file, pattern)
  File.readlines(file).select { |line| line =~ pattern }
end

def get_name(projname, pattern)
  grep(projname, pattern).join('')
    .sub!(pattern, '')
    .sub!(/"\n/, '')
end

Dir.chdir ARGV[0]

project_list = collect_project_names('MSBuild/Build.proj')
arr = []
keys_for_interfaces = []
type_libs = []
project_list.each do |projname|
  path_to_proj = File.dirname(projname)
  name_of_proj = get_name(projname, /(^Name=")/)
  unless grep(projname, /(Type=OleDll)|(Type=Control)/).nil?
    Dir.glob("#{path_to_proj}/*.cls").each do |classname|
      name_of_class = get_name(classname, /^Attribute VB_Name = "/)
      unless (grep(projname, /#{name_of_class}/).nil?) || (grep(projname, /DeleteClass\d=#{name_of_class}/).nil?)
        arr.push(name_of_proj + '.' + name_of_class)
      end
    end
  end
end

arr.each do |entry|
  reg = read_registry(entry, '\\Clsid')
  clsid = reg[''] unless reg.nil?
  reg2 = read_registry("CLSID\\#{clsid}\\", 'TypeLib')
  unless reg2.nil?
    typelib = reg2[""]
    type_libs.push(typelib)
  end
  to_update =
  [
    ['', entry],
    ['CLSID\\', clsid],
    ['AppID\\', clsid],
    ['TypeLib\\', typelib]
  ]
  to_update.each do |pair|
    delete_reg(pair[0], pair[1]) unless pair[1].nil?
  end
end

## Process Interfaces section
reg3 = read_registry('Interface', '')
reg3.each_key do |key|
  reg4 = read_registry("Interface\\#{key}", '\\TypeLib')
  keys_for_interfaces.push(key) if !reg4.nil? && type_libs.include?(reg4[''])
end

keys_for_interfaces.each do |key|
  delete_reg('Interface\\', key)
end
