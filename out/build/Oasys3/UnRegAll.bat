@echo UnRegAll.bat - Version 4.0
cd /d %~dp0
@echo 3rd party components
regsvr32 /u /s "ccrpprg6.ocx"
regsvr32 /u /s "SS60PP.dll"
regsvr32 /u /s "Flp32a30.ocx"
regsvr32 /u /s "Flp32d30.dll"
regsvr32 /u /s "fpFlp30.ocx"
regsvr32 /u /s "fpSpr60.ocx"
regsvr32 /u /s "FPSPR70.ocx"
regsvr32 /u /s "OPOSCashDrawer.ocx"
regsvr32 /u /s "OPOSPOSPrinter.ocx"
regsvr32 /u /s "spr32d60.dll"
regsvr32 /u /s "SPR32X60.ocx"
regsvr32 /u /s "COMDLG32.OCX"
regsvr32 /u /s "mcdapi32.ocx"
regsvr32 /u /s "MSADODC.OCX"
regsvr32 /u /s "mscomct2.ocx"
regsvr32 /u /s "Mscomctl.ocx"
regsvr32 /u /s "mscomm32.ocx"
regsvr32 /u /s "MSSOAP30.dll"
regsvr32 /u /s "MSSTDFMT.DLL"
regsvr32 /u /s "msstkprp.dll"
regsvr32 /u /s "mswinsck.ocx"
regsvr32 /u /s "TABCTL32.OCX"
regsvr32 /u /s "scanner.ocx"

@echo Highest dependancy level (depends on some or all below)
regsvr32 /u /s "CaptureCustUC.ocx"
regsvr32 /u /s "cCashBalanceBO.dll"
regsvr32 /u /s "cOPEvents_Wickes.dll"
regsvr32 /u /s "cPrintQuote_Wickes.dll"
regsvr32 /u /s "cReturns.dll"
regsvr32 /u /s "cSummaries.dll"
regsvr32 /u /s "cTillPrompts.dll"
regsvr32 /u /s "CTSEOrderProcess.dll"
regsvr32 /u /s "CTSHOHSFileProcess.dll"
regsvr32 /u /s "CTSOasysTranslator.dll"
regsvr32 /u /s "EstoreImport4dotNet.dll"
regsvr32 /u /s "HotkeyList.ocx"
regsvr32 /u /s "ItemFilter.ocx"
regsvr32 /u /s "LogicTender.dll"

@echo COMLink is 3rd party but might be needed until logictender is unregistered
regsvr32 /u /s "COMLink.dll"
regsvr32 /u /s "NumTextControl.ocx"
regsvr32 /u /s "OA2Logon.dll"
regsvr32 /u /s "OasysClientGeneric.dll"
regsvr32 /u /s "OASYSMenu.dll"
regsvr32 /u /s "OrderFilter.ocx"
regsvr32 /u /s "TillOrderItem.ocx"
regsvr32 /u /s "TillTranRetrieve.ocx"
regsvr32 /u /s "ucPostCodeEntry.ocx"
regsvr32 /u /s "Wsscommidea.dll"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "cEOrderSalesBO.dll"
regsvr32 /u /s "cLookUps.dll"
regsvr32 /u /s "cReceiptPrint.dll"
regsvr32 /u /s "cTillEvents_Wickes.dll"
regsvr32 /u /s "CTSProgBar.ocx"
regsvr32 /u /s "KeyPadUC.ocx"
regsvr32 /u /s "OasysSqlDatabaseCom.dll"
regsvr32 /u /s "WickesLibrary.dll"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "cCustomerOrders.dll"
regsvr32 /u /s "cCustResBO.dll"
regsvr32 /u /s "cInputBoxEx.dll"
regsvr32 /u /s "cPurchase.dll"
regsvr32 /u /s "cSale.dll"
regsvr32 /u /s "cSalesTotals.dll"
regsvr32 /u /s "EditDateCtl.ocx"
regsvr32 /u /s "EditNumberTill.ocx"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "cInventory.dll"
regsvr32 /u /s "cMsgBoxEx.dll"
regsvr32 /u /s "cOPEvents.dll"
regsvr32 /u /s "cSalesOrder.dll"
regsvr32 /u /s "HHTLabelCommon.dll"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "cEnterprise.dll"
regsvr32 /u /s "EditNumberCtl.ocx"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "OasysStartCom.dll"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "OasysRootCom.dll"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "OasysDbInterfaceCom.dll"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "OasysInterfaces.dll"

@echo Next dependancy level (depends on some or all below)
regsvr32 /u /s "OasysCBaseCom.dll"

@echo Lowest dependancy level (depends on nothing here)
regsvr32 /u /s "VbUtils.dll"

@echo Unregistering .NET interop

@if exist COMOrderManager.dll (
	regasm /u /s COMOrderManager.dll /tlb /codebase
	ComInteropRegistryCleaner COMOrderManager.dll
)
@if exist COMTPWickes.InterOp.Interface.dll (
	regasm /u /s COMTPWickes.InterOp.Interface.dll /tlb /codebase
	ComInteropRegistryCleaner COMTPWickes.InterOp.Interface.dll
)
@if exist COMTPWickes.InterOp.Wrapper.dll (
	regasm /u /s COMTPWickes.InterOp.Wrapper.dll /tlb /codebase
	ComInteropRegistryCleaner COMTPWickes.InterOp.Wrapper.dll
)
@if exist COMTPWickes.InterOp.Implementation.dll (
	regasm /u /s COMTPWickes.InterOp.Implementation.dll /tlb /codebase
	ComInteropRegistryCleaner COMTPWickes.InterOp.Implementation.dll
)
@echo End of UnRegAll.bat