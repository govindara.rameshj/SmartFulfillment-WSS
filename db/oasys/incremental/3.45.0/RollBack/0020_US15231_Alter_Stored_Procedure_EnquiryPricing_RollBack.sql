PRINT ('Altering procedure EnquiryPricing')
GO

-- =============================================
-- Author        : Alan Lewis
-- Update Date   : 30/07/2012
-- User Story	 : 6125
-- Project		 : P022-003 - Stock Enquiry Details - change the background colours of
--				 : specific areas based on data conditions.
-- Task Id		 : 6338
-- Description   : Add new fields called ResizeThis and EmboldenThis & Set to true for
--				 : the Price field.  Used by reporting engine to apply styles to
--				 : individual cells.
-- _____________________________________________
-- User Story	 : 6131
-- Project		 : P022-009 - Stock Enquiry Details - amend the design of what is displayed.
-- Task Id		 : 6344
-- Description   : Move fields and links to and from other reports.
--				 :
--				 : Add the following fields and hyperlinks move from other reports
--				 : reports.
--				 :	To Add					Move From
--				 : Events (hl)				Stock
--				 : Labels (hl)				N/A - was a dashboard report that is becoming a
--				 :								   standalone report
-- =============================================
-- Author      : Partha Dutta
-- Create Date : 29/09/2010
-- Referral No : 424
-- Notes       : Desk Enquiries -> Stock Item Enquiry -> SKU Details
--               Add "Next P/C" statistics to "Pricing" report
--               get list of price changes for this SKU and pick the earlist Date
-- =============================================

ALTER Procedure [dbo].[EnquiryPricing]
	@SkuNumber	Char(6)
As
Begin
	Set NOCOUNT On;

	Declare 
		@Table Table 
			(
				RowId Int, 
				SkuNumber Char(6), 
				ResizeThis Bit Default 0, 
				EmboldenThis Bit Default 0, 
				HighlightThis Bit Default 0, 
				Description VarChar(50), 
				Display VarChar(50)
			);
	Declare
		@price			Dec(9,2),
		@previous		Dec(9,2),
		@DateChange		Date,
		@vatCode		Char(1),
		@vatRate		Dec(9,2),
		@vatbreak		Dec(9,2),
		@HaveActiveEvent Bit;

	Select
		@price			= pric,
		@previous		= ppri,
		@DateChange		= dprc,
		@vatCode		= VATC
	From
		STKMAS
	Where
		SKUN = @SkuNumber

	Set @vatRate	= (Select CAse
							When @vatcode=1 Then (Select VATR1 From RETopT Where FKEY='01')
							When @vatcode=2 Then (Select VATR2 From RETopT Where FKEY='01')
							When @vatcode=3 Then (Select VATR3 From RETopT Where FKEY='01')
							When @vatcode=4 Then (Select VATR4 From RETopT Where FKEY='01')
							When @vatcode=5 Then (Select VATR5 From RETopT Where FKEY='01')
							When @vatcode=6 Then (Select VATR6 From RETopT Where FKEY='01')
							When @vatcode=7 Then (Select VATR7 From RETopT Where FKEY='01')
							When @vatcode=8 Then (Select VATR8 From RETopT Where FKEY='01')
							When @vatcode=9 Then (Select VATR9 From RETopT Where FKEY='01')
						End);
						
	Set @vatbreak = @vatRate * @price / 100;

	Select
		@HaveActiveEvent = 
			(
				Select Case
					When
						(
							Select
								COUNT(*)
							From
								EVTENQ E
									Inner Join
										EVTHDR H
									On
										E.NUMB = H.NUMB
							Where
								SKUN = @SkuNumber
							And
								(
									[dbo].udf_EventIsActive(H.SDAT, H.EDAT, H.STIM, H.ETIM, H.DACT1, H.DACT2, H.DACT3, H.DACT4, H.DACT5, H.DACT6, H.DACT7, H.IDEL) = 1
								)
						) > 0
					Then
						1
					Else
						0
				End
			);

	Insert Into 
		@Table
			(
				RowId,
				ResizeThis,
				EmboldenThis,
				Description, 
				Display) 
		Values 
			(
				2,
				1,
				1,
				'Item Price',
				@price
			);
	Insert Into @Table(Description, Display) Values ('Prior Price',	@previous);
	Insert Into @Table(Description, Display) Values ('Last Price Change', Convert(VarChar(10), @DateChange, 103));

    --next price change - start
    Declare @NextPriceChangeDate Date
    Set @NextPriceChangeDate = (Select Top 1 PDAT From PRCCHG Where SKUN = @SkuNumber and PSTA = 'U' Order By PDAT Asc)
	Insert Into @Table(Description, Display) Values ('Next Price Change', Convert(VarChar(10), @NextPriceChangeDate, 103));
    --next price change - End

	Insert Into @Table(Description, Display) Values ('VAT Rate %', @vatRate);
	--Insert Into @Table(Description, Display) Values ('VAT Breakdown', @vatbreak);
	Insert Into @Table(Description, Display) Values ('Ex VAT Price', @price - @vatbreak);
	Insert Into @Table(Description, Display) Values ('', '');
	Insert Into @Table(RowId, SkuNumber, Description) Values (1, @SkuNumber, 'Price Change History');
	Insert Into @Table(RowId, SkuNumber, Description) Values (4, @SkuNumber, 'Labels');
	Insert Into 
		@Table
			(
				RowId,
				SkuNumber,
				Description,
				HighlightThis,
				EmboldenThis
			) 
		Values 
			(
				3,
				@SkuNumber,
				'Events',
				@HaveActiveEvent,
				@HaveActiveEvent
			);

	Select * From @Table;
End
