﻿IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Locks]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	DROP TABLE [dbo].[Locks];
END
GO

If @@Error = 0
   Print 'Success: Drop Table Locks for US28399 has been successfully dropped'
Else
   Print 'Failure: Drop Table Locks for US28399 has not been successfully dropped'
GO
