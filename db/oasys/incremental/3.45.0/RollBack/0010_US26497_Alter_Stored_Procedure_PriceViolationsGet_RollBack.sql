PRINT ('Altering procedure PriceViolationsGet')
GO

ALTER PROCEDURE [dbo].[PriceViolationsGet]
	@Date	date
AS
BEGIN
	SET NOCOUNT ON;

select 
	dt.CASH				as 'CashierId',
	su.TillReceiptName	as 'CashierName',
	dl.SKUN				as 'SkuNumber',
	sm.DESCR			as 'Description',
	dl.TILL				as 'TillId',
	dl.[TRAN]			as 'TranNumber',
	dl.NUMB				as 'LineNumber',
	dl.SPRI				as 'PriceLU',
	dl.PRIC				as 'PriceKey',
	dl.QUAN				as 'Qty',
	dl.POPD				as 'PriceDifference',
	dl.PORC				as 'ReasonCode',	
	case dl.PORC
		when 1 then 'Damaged Goods'
		when 2 then 'Wrong Price on Shelf'
		when 3 then 'Refund Price Change'
		when 4 then 'Wrong P.O.S Sign'
		when 5 then 'Wrong Booklet Price'
		when 6 then 'Deleted Item'
		when 7 then 'Price Match'
		when 8 then 'Event Refund'
		when 9 then 'Colleague Refund'
		When 11 then 'Incorrect Day Price'
		else 'Other'
	end					as 'ReasonDescription',
	dl.SUPV				as 'SupervisorId',
	(select TillReceiptName from SystemUsers where EmployeeCode=dl.SUPV) as 'SupervisorName'
from
	DLLINE dl
inner join
	STKMAS sm on sm.SKUN = dl.SKUN
inner join
	DLTOTS dt on dt.DATE1=dl.DATE1 and dt.TILL=dl.TILL and dt.[TRAN]=dl.[TRAN]
inner join
	SystemUsers su on su.EmployeeCode=dt.CASH
where
	dl.DATE1 = @Date
	and dl.POPD <> 0
	and dt.VOID = 0
	and dl.LREV = 0
	and dt.TMOD = 0
order by
	dt.CASH,dl.TILL,dl.[TRAN],dl.NUMB
	
END

GO

if @@error <> 0
   print 'Failure: The Price Violation Get Store Procedure for US26497 has NOT been successfully rolled back'
else
   print 'Sucess: The Price Violation Get Store Procedure for US26497 has been successfully rolled back'


