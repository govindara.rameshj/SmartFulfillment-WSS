﻿IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Locks]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	PRINT 'Creating table Locks';

	CREATE TABLE [dbo].[Locks]
	(
		[EntityId] [nvarchar](200) NOT NULL,
		[OwnerId] [nvarchar](200) NOT NULL,
		[LockTime] [datetime] NOT NULL,
		CONSTRAINT [PK_Locks] PRIMARY KEY CLUSTERED 
		(
			[EntityId] ASC
		)
	);
END
ELSE
BEGIN
	PRINT 'Locks table is already exists';
END
GO

If @@Error = 0
   Print 'Success: Create Table Locks for US28399 has been deployed successfully'
Else
   Print 'Failure: Create Table Locks for US28399 has not been deployed successfully'
GO
