IF NOT EXISTS (SELECT 1 from dbo.[Parameters] WHERE ParameterID = 129)
    BEGIN
        INSERT INTO [Parameters]
        (ParameterID, [Description], LongValue, ValueType)
        VALUES 
        (129, 'Branding Image Backcolor', 16777215, 4)
    END

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US27812 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US27812 has not been deployed'
GO