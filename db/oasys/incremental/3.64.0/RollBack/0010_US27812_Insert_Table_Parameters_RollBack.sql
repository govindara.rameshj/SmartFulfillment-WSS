IF EXISTS (SELECT 1 from dbo.[Parameters] WHERE ParameterID = 129)
    BEGIN
        DELETE FROM [Parameters] WHERE ParameterID = 129
    END
    
If @@Error = 0
   Print 'Success: The Delete from Parameters table for US27812 has been deployed successfully'
Else
   Print 'Failure: The Delete from Parameters table for US27812 has not been deployed'
GO