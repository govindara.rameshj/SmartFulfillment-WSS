IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CONSUM') and name = 'IsLastProcessed')
    BEGIN
        PRINT 'Drop default constraint for IsLastProcessed column from dbo.CONSUM'
        DECLARE @ConstraintName nvarchar(200)
        SELECT @ConstraintName = NAME FROM sys.default_constraints
        WHERE PARENT_OBJECT_ID = OBJECT_ID('CONSUM')
        AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
                        WHERE NAME = N'IsLastProcessed'
                        AND OBJECT_ID = OBJECT_ID(N'CONSUM'))
                        
        IF @ConstraintName IS NOT NULL
        EXEC('ALTER TABLE CONSUM DROP CONSTRAINT ' + @ConstraintName)
                
        PRINT 'Drop IsLastProcessed column from dbo.CONSUM'
        ALTER TABLE dbo.CONSUM
        DROP COLUMN [IsLastProcessed]
    END
GO

If @@Error = 0
   Print 'Success: The Alter dbo.CONSUM table for US18885 has been deployed successfully'
Else
   Print 'Failure: The Alter dbo.CONSUM table for US18885 has not been deployed'
GO