IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42610)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 131 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42610
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42611)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 155 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42611
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42612)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 154 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42612
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42613)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 100 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42613
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42614)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42600 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42614
    END
GO

If @@Error = 0
   Print 'Success: The Update of ReportColumns table for USXXXXX has been deployed successfully'
Else
   Print 'Failure: The Update of ReportColumns table for USXXXXX has not been deployed'
GO