IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42619)
    BEGIN
        DELETE FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42619
    END
GO

If @@Error = 0
   Print 'Success: The Delete from ReportColumn table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Delete from ReportColumn table for DE2193 has not been deployed'
GO