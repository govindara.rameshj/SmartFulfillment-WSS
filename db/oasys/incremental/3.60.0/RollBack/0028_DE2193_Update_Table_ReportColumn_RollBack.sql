IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 238)
    BEGIN
        UPDATE dbo.ReportColumn SET MaxWidth = NULL WHERE Id = 238
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 306)
    BEGIN
        UPDATE dbo.ReportColumn SET MaxWidth = NULL WHERE Id = 306
    END
GO

If @@Error = 0
   Print 'Success: The Update into ReportColumn table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Update into ReportColumn table for DE2193 has not been deployed'
GO