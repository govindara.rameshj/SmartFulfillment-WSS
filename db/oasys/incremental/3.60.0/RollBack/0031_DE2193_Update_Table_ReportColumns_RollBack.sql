IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42615)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 230 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42615
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42616)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 151 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42616
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42617)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 237 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42617
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42618)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 305 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42618
    END
GO