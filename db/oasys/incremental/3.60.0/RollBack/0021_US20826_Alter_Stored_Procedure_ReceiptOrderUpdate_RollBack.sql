﻿IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'ReceiptOrderUpdate') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure ReceiptOrderUpdate'
    EXEC ('CREATE PROCEDURE dbo.ReceiptOrderUpdate AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure ReceiptOrderUpdate')
GO

ALTER PROCEDURE [dbo].[ReceiptOrderUpdate]
(
    @Number         char(6),
    @DeliveryNote1  char(10),
    @DeliveryNote2  char(10),
    @DeliveryNote3  char(10),
    @DeliveryNote4  char(10),
    @DeliveryNote5  char(10),
    @DeliveryNote6  char(10),
    @DeliveryNote7  char(10),
    @DeliveryNote8  char(10),
    @DeliveryNote9  char(10),
    @Value          dec(9,2),
    @Comments       char(20)
)   
AS
BEGIN
    SET NOCOUNT ON;

    update      drlsum
    
    set         [0dl1] = @DeliveryNote1,
                [0dl2] = @DeliveryNote2,
                [0dl3] = @DeliveryNote3,
                [0dl4] = @DeliveryNote4,
                [0dl5] = @DeliveryNote5,
                [0dl6] = @DeliveryNote6,
                [0dl7] = @DeliveryNote7,
                [0dl8] = @DeliveryNote8,
                [0dl9] = @DeliveryNote9,
                valu = @Value,
                info = @Comments,
                rti = 'N'
    
    where       numb = @Number  

END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ReceiptOrderUpdate for US20826 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure ReceiptOrderUpdate for US20826 has not been rolled back'
GO