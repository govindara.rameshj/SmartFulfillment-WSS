IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42615)
    BEGIN
        DELETE FROM ReportColumn WHERE Id = 42615
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42616)
    BEGIN
        DELETE FROM ReportColumn WHERE Id = 42616
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42617)
    BEGIN
        DELETE FROM ReportColumn WHERE Id = 42617
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42618)
    BEGIN
        DELETE FROM ReportColumn WHERE Id = 42618
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42619)
    BEGIN
        DELETE FROM ReportColumn WHERE Id = 42619
    END
GO

If @@Error = 0
   Print 'Success: The Delete from ReportColumn table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Delete from ReportColumn table for DE2193 has not been deployed'
GO