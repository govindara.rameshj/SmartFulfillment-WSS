IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42610)
    BEGIN
        DELETE FROM dbo.ReportColumn WHERE Id = 42610
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42611)
    BEGIN
        DELETE FROM dbo.ReportColumn WHERE Id = 42611
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42612)
    BEGIN
        DELETE FROM dbo.ReportColumn WHERE Id = 42612
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42613)
    BEGIN
        DELETE FROM dbo.ReportColumn WHERE Id = 42613
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42614)
    BEGIN
        DELETE FROM dbo.ReportColumn WHERE Id = 42614
    END
GO

If @@Error = 0
   Print 'Success: The Delete from ReportColumn table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportColumn table for DE2193 has not been deployed'
GO