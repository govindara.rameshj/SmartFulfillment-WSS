﻿IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DRLSUM') and name = 'INFO')
BEGIN
    PRINT 'Alter INFO column from DRLSUM table'

    ALTER TABLE [dbo].[DRLSUM]
    ALTER COLUMN [INFO] char(20) NULL
END
GO

If @@Error = 0
   Print 'Success: The Alter DRLSUM table for US20826 has been rolled back successfully'
Else
   Print 'Failure: The Alter DRLSUM table for US20826 has not been rolled back'
GO 