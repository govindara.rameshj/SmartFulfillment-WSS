DECLARE @MenuID int = 4190

DELETE FROM dbo.ProfilemenuAccess
WHERE [MenuConfigID] = @MenuID

DELETE FROM dbo.MenuConfig
WHERE ID = @MenuID

If @@Error = 0
   Print 'Success: The Delete from MenuConfig table for US18885 has been deployed successfully'
Else
   Print 'Failure: The Delete from MenuConfig table for US18885 has not been deployed'
GO
