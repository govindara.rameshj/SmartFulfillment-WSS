IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42615)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42615, 'TranNumber', 'Tran Number', 0, NULL, 0, NULL, 80, NULL)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42616)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42616, 'TillId', 'Till Id', 0, NULL, 0, NULL, 50, NULL)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42617)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42617, 'LineNumber', 'Line', 0, NULL, 0, NULL, 45, NULL)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42618)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42618, 'CashierName', 'Cashier Name', 0, NULL, 0, 60, NULL, NULL)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42619)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42619, 'Competitor', 'Competitor', 0, NULL, 0, 60, 150, NULL)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportColumn table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportColumn table for DE2193 has not been deployed'
GO