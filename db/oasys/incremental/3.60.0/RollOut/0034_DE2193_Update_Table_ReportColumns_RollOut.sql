IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 230)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42615 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 230
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 151)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42616 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 151
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 237)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42617 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 237
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 305)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42618 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 305
    END
GO