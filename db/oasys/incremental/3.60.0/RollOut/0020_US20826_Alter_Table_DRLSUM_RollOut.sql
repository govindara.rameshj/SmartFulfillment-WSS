﻿IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DRLSUM') and name = 'INFO')
BEGIN
    PRINT 'Alter INFO column from DRLSUM table'

    ALTER TABLE [dbo].[DRLSUM]
    ALTER COLUMN [INFO] char(75) NULL
END
GO

If @@Error = 0
   Print 'Success: The Alter DRLSUM table for US20826 has been deployed successfully'
Else
   Print 'Failure: The Alter DRLSUM table for US20826 has not been deployed'
GO 