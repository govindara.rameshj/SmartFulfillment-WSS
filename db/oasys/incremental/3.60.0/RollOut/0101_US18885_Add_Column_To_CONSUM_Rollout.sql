IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CONSUM') and name = 'IsLastProcessed')
    BEGIN
        PRINT 'Add IsLastProcessed column to dbo.CONSUM'
        ALTER TABLE [dbo].[CONSUM]
        ADD [IsLastProcessed] BIT NOT NULL DEFAULT(0)
        
        EXEC sp_addextendedproperty 
        @name = N'MS_Description', @value = 'Was container added or modified during last HPSTI file processing',
        @level0type = N'Schema', @level0name = 'dbo',
        @level1type = N'Table',  @level1name = 'CONSUM',
        @level2type = N'Column', @level2name = 'IsLastProcessed'
    END
GO

If @@Error = 0
   Print 'Success: Alter Table dbo.CONSUM for US18885 has been deployed successfully'
Else
   Print 'Failure: Alter Table dbo.CONSUM for US18885 has not been deployed successfully'
Go