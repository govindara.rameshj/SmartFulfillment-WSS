IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42610)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42610, 'ReasonCode', 'Reason Code', 0, NULL, 0, NULL, 70, NULL)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42611)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42611, 'SupervisorId', 'Sup Id', 0, NULL, 0, NULL, 50, NULL)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42612)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42612, 'CashierId', 'Cashier ID', 0, NULL, 0, NULL, 65, NULL)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42613)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42613, 'Description', 'Description', 0, NULL, 0, NULL, 200, NULL)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42614)
    BEGIN
        INSERT INTO dbo.ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES (42614, 'OVCTranNumber', 'OVC Tran Number', 0, NULL, 0, NULL, 110, NULL)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportColumn table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportColumn table for DE2193 has not been deployed'
GO