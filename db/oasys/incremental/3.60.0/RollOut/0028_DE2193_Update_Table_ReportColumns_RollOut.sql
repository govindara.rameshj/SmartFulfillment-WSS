IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 131)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42610 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 131
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 155)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42611 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 155
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 154)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42612 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 154
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 100)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42613 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 100
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42600)
    BEGIN
        UPDATE ReportColumns SET ColumnId = 42614 WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42600
    END
GO

If @@Error = 0
   Print 'Success: The Update of ReportColumns table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Update of ReportColumns table for DE2193 has not been deployed'
GO