IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 238)
    BEGIN
        UPDATE dbo.ReportColumn SET MinWidth = 80, MaxWidth = NULL WHERE Id = 238
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 306)
    BEGIN
        UPDATE dbo.ReportColumn SET MinWidth = 60, MaxWidth = NULL WHERE Id = 306
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42610)
    BEGIN
        UPDATE dbo.ReportColumn SET MaxWidth = 65 WHERE Id = 42610
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42611)
    BEGIN
        UPDATE dbo.ReportColumn SET MaxWidth = 45 WHERE Id = 42611
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42612)
    BEGIN
        UPDATE dbo.ReportColumn SET MaxWidth = 62 WHERE Id = 42612
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42613)
    BEGIN
        UPDATE dbo.ReportColumn SET MinWidth = 90 WHERE Id = 42613
    END
GO

IF EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42614)
    BEGIN
        UPDATE dbo.ReportColumn SET MinWidth = 80, MaxWidth = NULL WHERE Id = 42614
    END
GO

If @@Error = 0
   Print 'Success: The Update of ReportColumn table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Update of eportColumn table for DE2193 has not been deployed'
GO