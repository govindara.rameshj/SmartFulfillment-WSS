BEGIN TRANSACTION ContainerSummaryTransaction
BEGIN TRY
    DECLARE @MenuID int = 4190
    DECLARE @MasterID int = 4100
    
    DECLARE @DisplaySequence int
    SELECT @DisplaySequence = MAX(DisplaySequence) + 1 FROM dbo.MenuConfig WHERE MasterID = @MasterID

    IF NOT EXISTS (SELECT 1 FROM dbo.MenuConfig WHERE ID = @MenuID)
    BEGIN
        INSERT INTO dbo.MenuConfig (
                ID,
                MasterID,
                AppName,
                AssemblyName,
                ClassName,
                MenuType,
                [Parameters],
                ImagePath,
                LoadMaximised,
                IsModal,
                DisplaySequence,
                AllowMultiple,
                WaitForExit,
                TabName,
                [Description],
                ImageKey,
                [Timeout],
                DoProcessingType)
        VALUES (@MenuID, @MasterID, 'Container Summary Report', 'Purchasing.ContainerSummaryReport.dll', 'Purchasing.ContainerSummaryReport.HPSTIReport', 4, NULL, 'icons\icon_g.ico', 0, 1, @DisplaySequence, 0, 0, NULL, NULL, 0, 0, 1)
    END

    IF NOT EXISTS (SELECT 1 FROM dbo.ProfilemenuAccess WHERE MenuConfigID = @MenuID)
    BEGIN   
        INSERT INTO dbo.ProfilemenuAccess (
            [ID],
            [MenuConfigID],
            [AccessAllowed],
            [OverrideAllowed],
            [SecurityLevel],
            [IsManager],
            [IsSupervisor],
            [LastEdited],
            [Parameters]
            )
        SELECT 
                [ID],
                @MenuID AS [MenuConfigID],
                [AccessAllowed],
                [OverrideAllowed],
                [SecurityLevel],
                [IsManager],
                [IsSupervisor],
                [LastEdited],
                [Parameters]
        FROM dbo.ProfilemenuAccess AS PA
        WHERE PA.MenuConfigID = 4050 -- Entry of Process IW
            AND EXISTS(SELECT * FROM dbo.SecurityProfile AS SP WHERE SP.ID = PA.ID)
    END

    COMMIT TRANSACTION ContainerSummaryTransaction
    Print 'Success: Scripts for US18885 have been deployed successfully'
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION ContainerSummaryTransaction
    Print 'Failure: Scripts for US18885 have not been deployed successfully'
END CATCH