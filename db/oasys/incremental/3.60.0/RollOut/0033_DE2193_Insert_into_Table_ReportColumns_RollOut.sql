IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42619)
    BEGIN
        INSERT INTO dbo.ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize)
        VALUES (304, 1, 42619, 0, 1, 0, NULL, NULL, NULL)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportColumns table for DE2193 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportColumns table for DE2193 has not been deployed'
GO