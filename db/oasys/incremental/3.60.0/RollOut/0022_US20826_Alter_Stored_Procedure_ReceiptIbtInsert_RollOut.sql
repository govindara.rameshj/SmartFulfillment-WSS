﻿IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'ReceiptIbtInsert') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure ReceiptIbtInsert'
    EXEC ('CREATE PROCEDURE dbo.ReceiptIbtInsert AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure ReceiptIbtInsert')
GO

ALTER PROCEDURE [dbo].[ReceiptIbtInsert]
@Type CHAR (1), 
@DateReceipt DATE, @UserId INT, 
@Initials CHAR (5) = null, 
@Comments CHAR (75) = null, 
@Value DECIMAL (9, 2) = 0, 
@IbtStoreId CHAR (3) = null, 
@IbtConsignmentNumber CHAR (6) = null, 
@IbtNeedsPrinting BIT = 1, 
@RtiState CHAR (1) = null, 
@Number CHAR (6) OUTPUT
AS
BEGIN
    SET NOCOUNT ON;
    
    --get next receipt number from system numbers
    declare @NextNumber int;
    exec    @NextNumber = SystemNumbersGetNext @Id = 4
    
    --convert next number to number
    set @Number = Convert(Char(6), @NextNumber);
    
    --insert into drlsum
    insert into DRLSUM (                
        NUMB,
        [TYPE],
        DATE1,
        [INIT],
        INFO,
        VALU,
        [1STR],
        [1IBT],
        [1PRT],
        RTI,
        EmployeeId
    ) values (
        @Number,
        @Type,
        @DateReceipt,
        @Initials,
        @Comments,
        @Value,
        @IbtStoreId,
        @IbtConsignmentNumber,
        @IbtNeedsPrinting,
        @RtiState,
        @UserId
    )
    
    return @@rowcount
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ReceiptIbtInsert for US20826 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure ReceiptIbtInsert for US20826 has not been deployed'
GO
