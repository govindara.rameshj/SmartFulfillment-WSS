update dbo.SYSCOD
set IsActive = 1
where TYPE = 'OV' and CODE in ('04', '05', '08', '09')
go
update dbo.SYSCOD
set DESCR = 'Wrong Price on Shelf'
where TYPE = 'OV' and CODE = '02'
go
update dbo.SYSCOD
set DESCR = 'Deleted Item'
where TYPE = 'OV' and CODE = '06'
go
update dbo.SYSCOD
set DESCR = 'Refund Price Change'
where TYPE = 'OV' and CODE = '03'
go
update dbo.SYSCOD
set DESCR = 'Other'
where TYPE = 'OV' and CODE = '10'

go

If @@Error = 0
   Print 'Success: Update SYSCOD for US22879 has been deployed successfully'
Else
   Print 'Failure: Update SYSCOD for US22879 has not been deployed'
GO