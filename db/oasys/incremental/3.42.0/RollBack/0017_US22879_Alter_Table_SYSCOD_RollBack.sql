IF EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'SYSCOD' and col.name = 'IsActive'
)
BEGIN
PRINT 'Drop IsActive column from SYSCOD'
		ALTER TABLE dbo.SYSCOD DROP DF__SYSCOD__IsActive
		ALTER TABLE dbo.SYSCOD DROP COLUMN IsActive
END

GO

If @@Error = 0
   Print 'Success: Drop IsActive column from SYSCOD for US22879 has been deployed successfully'
ELSE
   Print 'Failure: Drop IsActive column from SYSCOD for US22879 has not been deployed successfully'
GO	