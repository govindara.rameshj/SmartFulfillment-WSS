update RETOPT
set PORC2 = 'WRONG PRICE ON SHELF',
PORC3 = 'REFUND PRICE CHANGE',
PORC6 = 'DELETED ITEM',
PORC10 = 'OTHER'

go

If @@Error = 0
   Print 'Success: Update RETOPT for US22879 has been deployed successfully'
Else
   Print 'Failure: Update RETOPT for US22879 has not been deployed'
GO