SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER procedure [dbo].[DashSales2]
   @DateEnd date
as
begin
    set nocount on

    declare @table table(RowId                int,
                         [Description]        varchar(50),
                         Qty                  varchar(50),
                         QtyWtd               varchar(50),
                         Value                dec(9,2),
                         ValueWtd             dec(9,2))

    declare @InputDate                                         date,
            @EquivalentDateLastYear                            date,

            @KitchenAndBathroomDepositsQuantityForDay          int,
            @KitchenAndBathroomDepositsQuantityForWeek         int,
            @KitchenAndBathroomDepositsValueForDay             dec(9,2),
            @KitchenAndBathroomDepositsValueForWeek            dec(9,2),

            @KitchenAndBathroomSalesQuantityForDay             int,
            @KitchenAndBathroomSalesQuantityFoWeek             int,
            @KitchenAndBathroomSalesValueForDay                dec(9,2),
            @KitchenAndBathroomSalesValueForWeek               dec(9,2),

            @KitchenAndBathroomDepositsQuantityForDayLastYear  int,
            @KitchenAndBathroomDepositsQuantityForWeekLastYear int,
            @KitchenAndBathroomDepositsValueForDayLastYear     dec(9,2),
            @KitchenAndBathroomDepositsValueForWeekLastYear    dec(9,2),

            @KitchenAndBathroomSalesQuantityForDayLastYear     int,
            @KitchenAndBathroomSalesQuantityFoWeekLastYear     int,
            @KitchenAndBathroomSalesValueForDayLastYear        dec(9,2),
            @KitchenAndBathroomSalesValueForWeekLastYear       dec(9,2),

            @CoreSalesQuantityForDay                           int,
            @CoreSalesQuantityForWeek                          int,
            @CoreSalesValueForDay                              dec(9,2),
            @CoreSalesValueForWeek                             dec(9,2),
 
            @CoreRefundsQuantityForDay                         int,
            @CoreRefundsQuantityForWeek                        int,
            @CoreRefundsValueForDay                            dec(9,2),
            @CoreRefundsValueForWeek                           dec(9,2),

            @CoreSalesQuantityForDayLastYear                   int,
            @CoreSalesQuantityForWeekLastYear                  int,
            @CoreSalesValueForDayLastYear                      dec(9,2),
            @CoreSalesValueForWeekLastYear                     dec(9,2),
 
            @CoreRefundsQuantityForDayLastYear                 int,
            @CoreRefundsQuantityForWeekLastYear                int,
            @CoreRefundsValueForDayLastYear                    dec(9,2),
            @CoreRefundsValueForWeekLastYear                   dec(9,2),

            @CoreLinesSoldQuantityForDay                       int,
            @CoreLinesSoldQuantityForWeek                      int,

            @CoreLinesScannedQuantityForDay                    int,
            @CoreLinesScannedQuantityForWeek                   int,

            @EANLinesScannedForDayPercentage                   dec(9,2),
            @EANLinesScannedForWeekPercentage                  dec(9,2),

            @RefundsAgainstSalesForDayPercentage               dec(9,2),
            @RefundsAgainstSalesForWeekPercentage              dec(9,2),

-- C&C variables 
            @ClickAndColectSalesQuantityForDay				   int,
            @ClickAndColectSalesQuantityForWeek                int,
            @ClickAndColectSalesValueForDay                    dec(9,2),
            @ClickAndColectSalesValueForWeek                   dec(9,2),

            @ClickAndColectRefundsQuantityForDay               int,
            @ClickAndColectRefundsQuantityForWeek              int,
            @ClickAndColectRefundsValueForDay                  dec(9,2),
            @ClickAndColectRefundsValueForWeek                 dec(9,2),
            
            @ClickAndColectRefundsAgainstSalesForDayPercentage  dec(9,2),
            @ClickAndColectRefundsAgainstSalesForWeekPercentage dec(9,2)

    set @InputDate              = @DateEnd
    set @EquivalentDateLastYear = dbo.udf_GetThisDayLastYear(@InputDate)

    select @KitchenAndBathroomDepositsQuantityForDay          = dbo.svf_ReportKBDepositsQtyDAY(@InputDate),
           @KitchenAndBathroomDepositsQuantityForWeek         = dbo.svf_ReportKBDepositsQtyWTD(@InputDate),
           @KitchenAndBathroomDepositsValueForDay             = dbo.svf_ReportKBDepositsValueDAY(@InputDate),
           @KitchenAndBathroomDepositsValueForWeek            = dbo.svf_ReportKBDepositsValueWTD(@InputDate),

           @KitchenAndBathroomSalesQuantityForDay             = dbo.svf_ReportKBSalesQtyDAY(@InputDate),
           @KitchenAndBathroomSalesQuantityFoWeek             = dbo.svf_ReportKBSalesQtyWTD(@InputDate),
           @KitchenAndBathroomSalesValueForDay                = dbo.svf_ReportKBSalesValueDAY(@InputDate),
           @KitchenAndBathroomSalesValueForWeek               = dbo.svf_ReportKBSalesValueWTD(@InputDate),

           @KitchenAndBathroomDepositsQuantityForDayLastYear  = dbo.svf_ReportKBDepositsQtyDAY(@EquivalentDateLastYear),
           @KitchenAndBathroomDepositsQuantityForWeekLastYear = dbo.svf_ReportKBDepositsQtyWTD(@EquivalentDateLastYear),
           @KitchenAndBathroomDepositsValueForDayLastYear     = dbo.svf_ReportKBDepositsValueDAY(@EquivalentDateLastYear),
           @KitchenAndBathroomDepositsValueForWeekLastYear    = dbo.svf_ReportKBDepositsValueWTD(@EquivalentDateLastYear),

           @KitchenAndBathroomSalesQuantityForDayLastYear     = dbo.svf_ReportKBSalesQtyDAY(@EquivalentDateLastYear),
           @KitchenAndBathroomSalesQuantityFoWeekLastYear     = dbo.svf_ReportKBSalesQtyWTD(@EquivalentDateLastYear),
           @KitchenAndBathroomSalesValueForDayLastYear        = dbo.svf_ReportKBSalesValueDAY(@EquivalentDateLastYear),
           @KitchenAndBathroomSalesValueForWeekLastYear       = dbo.svf_ReportKBSalesValueWTD(@EquivalentDateLastYear),

           @CoreSalesQuantityForDay                           = dbo.svf_ReportCoreSalesQtyDAY(@InputDate),
           @CoreSalesQuantityForWeek                          = dbo.svf_ReportCoreSalesQtyWTD(@InputDate),
           @CoreSalesValueForDay                              = dbo.svf_ReportCoreSalesValueDAY(@InputDate),
           @CoreSalesValueForWeek                             = dbo.svf_ReportCoreSalesValueWTD(@InputDate),

           @CoreRefundsQuantityForDay                         = dbo.svf_ReportCoreRefundsQtyDAY(@InputDate),
           @CoreRefundsQuantityForWeek                        = dbo.svf_ReportCoreRefundsQtyWTD(@InputDate),
           @CoreRefundsValueForDay                            = dbo.svf_ReportCoreRefundsValueDAY(@InputDate),
           @CoreRefundsValueForWeek                           = dbo.svf_ReportCoreRefundsValueWTD(@InputDate),

           @CoreSalesQuantityForDayLastYear                   = dbo.svf_ReportCoreSalesQtyDAY(@EquivalentDateLastYear),
           @CoreSalesQuantityForWeekLastYear                  = dbo.svf_ReportCoreSalesQtyWTD(@EquivalentDateLastYear),
           @CoreSalesValueForDayLastYear                      = dbo.svf_ReportCoreSalesValueDAY(@EquivalentDateLastYear),
           @CoreSalesValueForWeekLastYear                     = dbo.svf_ReportCoreSalesValueWTD(@EquivalentDateLastYear),

           @CoreRefundsQuantityForDayLastYear                 = dbo.svf_ReportCoreRefundsQtyDAY(@EquivalentDateLastYear),
           @CoreRefundsQuantityForWeekLastYear                = dbo.svf_ReportCoreRefundsQtyWTD(@EquivalentDateLastYear),
           @CoreRefundsValueForDayLastYear                    = dbo.svf_ReportCoreRefundsValueDAY(@EquivalentDateLastYear),
           @CoreRefundsValueForWeekLastYear                   = dbo.svf_ReportCoreRefundsValueWTD(@EquivalentDateLastYear),

           @CoreLinesScannedQuantityForDay                    = dbo.svf_ReportCoreScanningQtyDAY(@InputDate),
           @CoreLinesScannedQuantityForWeek                   = dbo.svf_ReportCoreScanningQtyWTD(@InputDate),
           @CoreLinesSoldQuantityForDay                       = dbo.svf_ReportCoreLinesSoldQtyDAY(@InputDate),
           @CoreLinesSoldQuantityForWeek                      = dbo.svf_ReportCoreLinesSoldQtyWTD(@InputDate),

-- C&C data load
		   @ClickAndColectSalesQuantityForDay                 = dbo.svf_ReportClickAndCollectSalesQtyDAY(@InputDate),
           @ClickAndColectSalesQuantityForWeek                = dbo.svf_ReportClickAndCollectSalesQtyWTD(@InputDate),
           @ClickAndColectSalesValueForDay                    = dbo.svf_ReportClickAndCollectSalesValueDAY(@InputDate),
           @ClickAndColectSalesValueForWeek                   = dbo.svf_ReportClickAndCollectSalesValueWTD(@InputDate),
           
           @ClickAndColectRefundsQuantityForDay				  = dbo.svf_ReportClickAndCollectRefundsQtyDAY(@InputDate),               
		   @ClickAndColectRefundsQuantityForWeek			  = dbo.svf_ReportClickAndCollectRefundsQtyWTD(@InputDate),             
		   @ClickAndColectRefundsValueForDay				  = dbo.svf_ReportClickAndCollectRefundsValueDAY(@InputDate),     
		   @ClickAndColectRefundsValueForWeek				  = dbo.svf_ReportClickAndCollectRefundsValueWTD(@InputDate)

    if @CoreLinesSoldQuantityForDay  <> 0 set @EANLinesScannedForDayPercentage  = (cast(@CoreLinesScannedQuantityForDay  as dec(9, 2)) / cast(@CoreLinesSoldQuantityForDay  as dec(9, 2))) * 100
    if @CoreLinesSoldQuantityForWeek <> 0 set @EANLinesScannedForWeekPercentage = (cast(@CoreLinesScannedQuantityForWeek as dec(9, 2)) / cast(@CoreLinesSoldQuantityForWeek as dec(9, 2))) * 100

    if (@CoreRefundsValueForDay <> 0 and @CoreSalesValueForDay <> 0)
		set @RefundsAgainstSalesForDayPercentage = abs((@CoreRefundsValueForDay / @CoreSalesValueForDay) * 100)
		
    if (@CoreRefundsValueForWeek <> 0 and @CoreSalesValueForWeek <> 0)
		set @RefundsAgainstSalesForWeekPercentage = abs((@CoreRefundsValueForWeek / @CoreSalesValueForWeek) * 100)

    if (@ClickAndColectRefundsValueForDay <> 0 and @ClickAndColectSalesValueForDay <> 0)
		set @ClickAndColectRefundsAgainstSalesForDayPercentage = abs((@ClickAndColectRefundsValueForDay / @ClickAndColectSalesValueForDay) * 100)
		
    if (@ClickAndColectRefundsValueForWeek <> 0 and @ClickAndColectSalesValueForWeek <> 0)
		set @ClickAndColectRefundsAgainstSalesForWeekPercentage = abs((@ClickAndColectRefundsValueForWeek / @ClickAndColectSalesValueForWeek) * 100)



    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (1, 'Core Sales',						@CoreSalesQuantityForDay,   @CoreSalesQuantityForWeek,   @CoreSalesValueForDay,   @CoreSalesValueForWeek)
    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (2, 'Core Refunds',					@CoreRefundsQuantityForDay, @CoreRefundsQuantityForWeek, @CoreRefundsValueForDay, @CoreRefundsValueForWeek)

    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (6, 'K&B Sales',						@KitchenAndBathroomSalesQuantityForDay         + @KitchenAndBathroomDepositsQuantityForDay,
																														@KitchenAndBathroomSalesQuantityFoWeek         + @KitchenAndBathroomDepositsQuantityForWeek,
																														@KitchenAndBathroomSalesValueForDay            + @KitchenAndBathroomDepositsValueForDay,
																														@KitchenAndBathroomSalesValueForWeek           + @KitchenAndBathroomDepositsValueForWeek)

	insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (4, 'C&C Sales',						@ClickAndColectSalesQuantityForDay, @ClickAndColectSalesQuantityForWeek, @ClickAndColectSalesValueForDay, @ClickAndColectSalesValueForWeek)
	insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (5, 'C&C Cancels/Returns',			@ClickAndColectRefundsQuantityForDay, @ClickAndColectRefundsQuantityForWeek, @ClickAndColectRefundsValueForDay, @ClickAndColectRefundsValueForWeek)


    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (8, 'Core Sales Last Year',			@CoreSalesQuantityForDayLastYear               + @CoreRefundsQuantityForDayLastYear,
																														@CoreSalesQuantityForWeekLastYear              + @CoreRefundsQuantityForWeekLastYear,
																														@CoreSalesValueForDayLastYear                  + @CoreRefundsValueForDayLastYear,
																														@CoreSalesValueForWeekLastYear                 + @CoreRefundsValueForWeekLastYear)
    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (9, 'K&B Sales Last Year',			@KitchenAndBathroomSalesQuantityForDayLastYear + @KitchenAndBathroomDepositsQuantityForDayLastYear,
																														@KitchenAndBathroomSalesQuantityFoWeekLastYear + @KitchenAndBathroomDepositsQuantityForWeekLastYear,
																														@KitchenAndBathroomSalesValueForDayLastYear    + @KitchenAndBathroomDepositsValueForDayLastYear,
																														@KitchenAndBathroomSalesValueForWeekLastYear   + @KitchenAndBathroomDepositsValueForWeekLastYear)

    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (10, 'Percentage Line Scanned (Exc C&C)',			@EANLinesScannedForDayPercentage,     @EANLinesScannedForWeekPercentage,     null, null)
    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (11, 'Percentage Refunds to Sales',				@RefundsAgainstSalesForDayPercentage, @RefundsAgainstSalesForWeekPercentage, null, null)
    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) values (11, 'Percentage C&C Cancels/Returns to Sales',	@ClickAndColectRefundsAgainstSalesForDayPercentage, @ClickAndColectRefundsAgainstSalesForWeekPercentage, null, null)


    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) select 3, 'Net Core Sales',         sum(cast(Qty as int)), sum(cast(QtyWtd as int)), sum(Value), sum(ValueWtd) from @table where RowID in (1, 2)

    insert into @table(RowId, [Description], Qty, QtyWtd, Value, ValueWtd) select 7, 'Core Sales + K&B Sales + C&C Sales', sum(cast(Qty as int)), sum(cast(QtyWtd as int)), sum(Value), sum(ValueWtd) from @table where RowID in (3, 4, 5, 6)

    select * from @table order by RowId

end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure DashSales2 for US20146 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure DashSales2 for US20146 has not been deployed successfully'
GO

