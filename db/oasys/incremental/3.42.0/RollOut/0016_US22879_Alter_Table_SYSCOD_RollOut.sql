IF NOT EXISTS (
	select 1 from sys.columns
	where object_id = object_id('SYSCOD')
		and name = 'IsActive'
)
BEGIN
PRINT 'Add IsActive column to SYSCOD'
	ALTER TABLE [dbo].[SYSCOD] ADD IsActive bit not null 
	CONSTRAINT DF__SYSCOD__IsActive DEFAULT 1
END

GO

If @@Error = 0
   Print 'Success: Add IsActive column to SYSCOD for US22879 has been deployed successfully'
ELSE
   Print 'Failure: Add IsActive column to SYSCOD for US22879 has not been deployed successfully'
GO		