update dbo.SYSCOD
set IsActive = 0
where TYPE = 'OV' and CODE in ('04', '05', '08', '09')
go
update dbo.SYSCOD
set DESCR = 'Incorrect Price Displayed'
where TYPE = 'OV' and CODE = '02'
go
update dbo.SYSCOD
set DESCR = 'Deleted Product'
where TYPE = 'OV' and CODE = '06'
go
update dbo.SYSCOD
set DESCR = 'Refund Price Difference'
where TYPE = 'OV' and CODE = '03'
go
update dbo.SYSCOD
set DESCR = 'Managers Discretion'
where TYPE = 'OV' and CODE = '10'

go

If @@Error = 0
   Print 'Success: Update SYSCOD for US22879 has been deployed successfully'
Else
   Print 'Failure: Update SYSCOD for US22879 has not been deployed'
GO