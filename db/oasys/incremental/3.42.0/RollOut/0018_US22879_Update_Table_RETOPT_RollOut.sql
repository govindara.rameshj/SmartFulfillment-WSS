update RETOPT
set PORC2 = 'INCORRECT PRICE DISP',
PORC3 = 'REFUND PRICE DIFFERE',
PORC6 = 'DELETED PRODUCT',
PORC10 = 'MANAGERS DISCRETION'

go

If @@Error = 0
   Print 'Success: Update RETOPT for US22879 has been deployed successfully'
Else
   Print 'Failure: Update RETOPT for US22879 has not been deployed'
GO