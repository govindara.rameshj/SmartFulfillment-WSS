insert NITMAS(NSET, TASK, DESCR, PROG,
              NITE, RETY, [WEEK], PEND, LIVE, OPTN, ABOR, JRUN, DAYS1, DAYS2, DAYS3, DAYS4, DAYS5, DAYS6, DAYS7,
              BEGD, ENDD, SDAT, STIM, EDAT, ETIM, ADAT, ATIM, TTYPE)
      values (1, 999, 'House Keeping', 'NightlyTaskSQLScriptExecutor.exe| -F "F:\WIX\Scripts\HouseKeeping.sql"',
              1, 0, 1, 0, 0, null, 0, null, 0, 0, 0, 0, 0, 1, 0,
              null, null, null, null, null, null, null, null, 0)
go


if @@error = 0
   print 'Success: The metadata change to NITMAS for HouseKeeping has been successfully deployed'
else
   print 'Failure: The metadata change to NITMAS for HouseKeeping has NOT been successfully deployed'
go