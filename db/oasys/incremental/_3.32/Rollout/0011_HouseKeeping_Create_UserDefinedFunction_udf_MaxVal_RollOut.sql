create function udf_MaxVal(@val1 int, @val2 int)
	returns int
	as
	begin
	  if @val1 > @val2
	    return @val1
	  return isnull(@val2,@val1)		

	end
go

if @@error = 0
   print 'Success: User defined function udf_MaxVal for HouseKeeping has been successfully deployed'
else
   print 'Failure: User defined function udf_MaxVal for HouseKeeping has NOT been successfully deployed'
go
