create procedure RegenerateIndex
as
--USE master
 
DECLARE
	@databasename varchar(255),
	@lngDBCount int,
	@lngtableCount int,
	@lngLoop1 int,
	@SQL1 nvarchar(50),
	@SQL2 nvarchar(max),
	@SQL3 nvarchar(max),
	@frag decimal,
	@lngCount int
 
	-- Set constants
	SET @lngLoop1 = 1 
	SET @lngDBCount = @@ROWCOUNT
 
    SET @databasename = (SELECT 'Oasys') 
	set @SQL1 = 'USE '+@databasename + Char(30) 
	SET @SQL2 = 
 
		'SET NOCOUNT ON;
		DECLARE @objectid int;
		DECLARE @indexid int;
		DECLARE @partitioncount bigint;
		DECLARE @schemaname nvarchar(130);
		DECLARE @objectname nvarchar(130);
		DECLARE @indexname nvarchar(130);
		DECLARE @partitionnum bigint;
		DECLARE @partitions bigint;
		DECLARE @frag float;
		DECLARE @command nvarchar(4000); 
 
		SELECT
			object_id AS objectid,
			index_id AS indexid,
			partition_number AS partitionnum,
			avg_fragmentation_in_percent AS frag
		INTO #work_to_do
		FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, ''LIMITED'')
		WHERE avg_fragmentation_in_percent > 10.0 AND index_id > 0;
 
		/*Declare the cursor for the list of partitions to be processed.*/
		DECLARE partitions CURSOR FOR SELECT * FROM #work_to_do;
 
		/*Open the cursor.*/
		OPEN partitions;
 
		/* Loop through the partitions.*/
		WHILE (1=1)
			BEGIN;
				FETCH NEXT
				   FROM partitions
				   INTO @objectid, @indexid, @partitionnum, @frag;
				IF @@FETCH_STATUS < 0 BREAK;
				SELECT @objectname = QUOTENAME(o.name), @schemaname = QUOTENAME(s.name)
				FROM sys.objects AS o
				JOIN sys.schemas as s ON s.schema_id = o.schema_id
				WHERE o.object_id = @objectid;
				SELECT @indexname = QUOTENAME(name)
				FROM sys.indexes
				WHERE  object_id = @objectid AND index_id = @indexid;
				SELECT @partitioncount = count (*)
				FROM sys.partitions
				WHERE object_id = @objectid AND index_id = @indexid;
 
		/* 30 is an arbitrary decision point at which to switch between reorganizing and rebuilding.*/
				IF @frag < 30.0
					SET @command = N''ALTER INDEX '' + @indexname + N'' ON '' + @schemaname + N''.'' + @objectname + N'' REORGANIZE'';
				IF @frag >= 30.0
					SET @command = N''ALTER INDEX '' + @indexname + N'' ON '' + @schemaname + N''.'' + @objectname + N'' REBUILD'';
				IF @partitioncount > 1
					SET @command = @command + N'' PARTITION='' + CAST(@partitionnum AS nvarchar(10));
				EXEC (@command);
				/*insert into master.dbo.IndexesBuilt([Database],Command,[Date]) select DB_NAME(),N''Executed: '' + @command,getdate();*/
				PRINT N''Executed: '' + @command;
			END;
 
		/* Close and deallocate the cursor.*/
		CLOSE partitions;
		DEALLOCATE partitions;
 
		/* Drop the temporary table.*/
		DROP TABLE #work_to_do;'
  
	SET @SQL3 = (@SQL1 + @SQL2) 
	EXEC dbo.sp_executesql @SQL3 
	SET @lngLoop1 = @lngLoop1 + 1
 
go

if @@error = 0
   print 'Success: Stored procedure RegenerateIndex for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure RegenerateIndex for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On RegenerateIndex To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure RegenerateIndex for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure RegenerateIndex for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on RegenerateIndex to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure RegenerateIndex for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure RegenerateIndex for HouseKeeping has NOT been successfully deployed'
go
