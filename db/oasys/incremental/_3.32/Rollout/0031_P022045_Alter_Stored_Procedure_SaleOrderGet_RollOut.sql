ALTER PROCEDURE [dbo].[SaleOrderGet]
	@OrderNumber CHAR (6)=null, 
	@OmOrderNumber INT=null, 
	@VendaNumber char(6)=null,
	@DeliveryStatusMin INT=null, 
	@DeliveryStatusMax INT=null,
	@RefundStatusMin INT=null, 
	@RefundStatusMax INT=null, 
	@DateStart DATE=null, 
	@DateEnd DATE=null,
	@PostCode Char(10) = null,
	@TelephoneNumber VarChar(20) = null,
	@CustomerName VarChar(50) = null
AS
BEGIN
	SET NOCOUNT ON
	Declare @PostCodeWithoutSpaces VarChar(10)
	if @PostCode Is Not Null  Set @PostCodeWithoutSpaces = REPLACE(@PostCode,' ','')
	
	Declare @TelephoneNumberWithoutSpaces VarChar(20)
	if @TelephoneNumber Is Not Null  Set @TelephoneNumberWithoutSpaces = REPLACE(@TelephoneNumber,' ','')
	
	select
		ch.NUMB				as Number,
		ch.CUST				as CustomerNumber ,
		ch.NAME				as CustomerName ,
		ch.DATE1			as DateOrder ,
		ch.DELD				as DateDelivery ,
		ch.CANC				as 'Status',
		ch.DELI				as IsForDelivery ,
		ch.DELC				as DeliveryConfirmed ,
		ch.AMDT				as TimesAmended ,
		ch.ADDR1			as DeliveryAddress1 ,
		ch.ADDR2			as DeliveryAddress2 ,
		ch.ADDR3			as DeliveryAddress3 ,
		ch.ADDR4			as DeliveryAddress4 ,
		ch.POST				as DeliveryPostCode ,
		ch.PHON				as PhoneNumber ,
		ch.MOBP				as PhoneNumberMobile ,
		ch.PRNT				as IsPrinted ,
		ch.RPRN				as NumberReprints ,
		ch.REVI				as RevisionNumber ,
		ch.MVST				as MerchandiseValue ,
		ch.DCST				as DeliveryCharge ,
		ch.QTYO				as QtyOrdered ,
		ch.QTYT				as QtyTaken ,
		ch.QTYR				as QtyRefunded ,
		ch.WGHT				as OrderWeight ,
		ch.VOLU				as OrderVolume ,
		ch.SDAT				as TranDate ,
		ch.STIL				as TranTill ,
		ch.STRN				as TranNumber ,
		ch.RDAT				as RefundTranDate ,
		ch.RTIL				as RefundTill ,
		ch.RTRN				as RefundTranNumber,
		ch4.DDAT			as DateDespatch,
		ch4.OEID			as OrderTakerId,
		ch4.FDID			as ManagerId,
		ch4.DeliveryStatus,
		ch4.RefundStatus,
		ch4.SellingStoreId,
		ch4.SellingStoreOrderId,
		ch4.OMOrderNumber,
		ch4.CustomerAddress1,
		ch4.CustomerAddress2,
		ch4.CustomerAddress3,
		ch4.CustomerAddress4,
		ch4.CustomerPostcode,
		ch4.CustomerEmail,
		ch4.PhoneNumberWork,
		ch4.IsSuspended,
		ch5.Source,
        	ch5.SourceOrderNumber,
	        ch5.ExtendedLeadTime,
	        ch5.DeliveryContactName,
        	ch5.DeliveryContactPhone,
        	ch5.CustomerAccountNo

	from
		CORHDR ch
	inner join
		CORHDR4	ch4 on ch.NUMB=ch4.NUMB
		LEFT OUTER JOIN CORHDR5 ch5 on ch5.NUMB = ch4.NUMB

	where
		((@OrderNumber is null) or (@OrderNumber is not null and ch.NUMB=@OrderNumber))
		and ((@VendaNumber is null) or (@VendaNumber is not null and ch.CUST=@VendaNumber))
		and ((@OmOrderNumber is null) or (@OmOrderNumber is not null and ch4.OMOrderNumber=@OmOrderNumber))
		and ((@DeliveryStatusMin is null) or (@DeliveryStatusMin is not null and ch4.DeliveryStatus>=@DeliveryStatusMin))
		and ((@DeliveryStatusMax is null) or (@DeliveryStatusMax is not null and ch4.DeliveryStatus<=@DeliveryStatusMax))
		and ((@RefundStatusMin is null) or (@RefundStatusMin is not null and ch4.RefundStatus>=@RefundStatusMin))
		and ((@RefundStatusMax is null) or (@RefundStatusMax is not null and ch4.RefundStatus<=@RefundStatusMax))
		and ((@DateStart is null) or (@DateStart is not null and ch.DATE1>=@DateStart))
		and ((@DateEnd is null) or (@DateEnd is not null and ch.DATE1<=@DateEnd))
		and ((@TelephoneNumber is null) 
			or (@TelephoneNumber is not null and REPLACE(ch.PHON, ' ', '') like '%' + @TelephoneNumberWithoutSpaces + '%')
			or (@TelephoneNumber is not null and REPLACE(ch.MOBP, ' ', '') like '%' + @TelephoneNumberWithoutSpaces + '%')
			or (@TelephoneNumber is not null and REPLACE(ch4.PhoneNumberWork, ' ', '') like '%' + @TelephoneNumberWithoutSpaces + '%')
			or (@TelephoneNumber is not null and REPLACE(ch5.DeliveryContactPhone, ' ', '') like '%' + @TelephoneNumberWithoutSpaces + '%'))
		and ((@PostCode is null) 
			or (@PostCode is not null and REPLACE(ch.POST, ' ', '') like '%' + @PostCodeWithoutSpaces + '%')
			or (@PostCode is not null and REPLACE(ch4.CustomerPostcode, ' ', '')  like '%' + @PostCodeWithoutSpaces + '%'))
		and ((@CustomerName is null) 
			or (@CustomerName is not null and ch.NAME like '%' + @CustomerName + '%')
			or (@CustomerName is not null and ch4.NAME like '%' + @CustomerName + '%')
			or (@CustomerName is not null and ch5.DeliveryContactName like '%' + @CustomerName + '%'))
		and ch4.DeliveryStatus < 9999
			
END
Go
If @@Error = 0
   Print 'Success: The Alter Stored Procedure SaleOrderGet for P022-045 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SaleOrderGet for P022-045 has not been deployed successfully'
Go
