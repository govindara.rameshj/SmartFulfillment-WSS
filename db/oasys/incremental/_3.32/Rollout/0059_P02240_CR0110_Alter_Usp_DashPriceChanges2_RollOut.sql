ALTER procedure [dbo].[DashPriceChanges2]
	@DateEnd Date
as
begin
   set nocount on

   declare @Output table(RowId         int, 
                         [Description] varchar(50), 
                         Qty           int,
                         Peg           int,
                         Small         int,
                         Medium        int)

   declare @PCToday	                         int,
           @OverdueQty                       int,
           @OutstandingQty                   int,
           @Peg                              int,
           @Small                            int,
           @Medium                           int,
           @PriceChangesConfirmedDayQuantity int

	DECLARE @PriceChangeDates TABLE (
	SKU char(6),
	EffectiveDate DateTime)
	
	INSERT INTO @PriceChangeDates (SKU,EffectiveDate)
	SELECT SKUN,dbo.udf_GetLatestPriceChangeDateForSku(SKUN)
	FROM PRCCHG
	WHERE PSTA = 'U'
	
	SELECT @PCToday = COUNT(*)
	FROM @PriceChangeDates pcd
	JOIN sysdat sd ON sd.TMDT = pcd.EffectiveDate

	SELECT @OverdueQty = COUNT(*)
	FROM @PriceChangeDates pcd
	JOIN sysdat sd ON sd.TMDT > pcd.EffectiveDate
	
   set @OutstandingQty                   = dbo.svf_ReportPriceChangesOutstandingQty()
   set @Peg                              = dbo.svf_ReportPriceChangesLabelsRequiredPeg()
   set @Small                            = dbo.svf_ReportPriceChangesLabelsRequiredSmall()
   set @Medium                           = dbo.svf_ReportPriceChangesLabelsRequiredMedium()
   set @PriceChangesConfirmedDayQuantity = dbo.svf_ReportPriceChangesConfirmedDAY(@DateEnd)

   insert @Output values (1, 'Waiting (Inc. AutoApply)',  @OutstandingQty,                   null, null,   null)
   insert @Output values (2, 'Overdue (Inc. AutoApply)',  @OverdueQty,                       null, null,   null)
   insert @Output values (3, 'Labels Required',                        (@Peg + @Small + @Medium),          @Peg, @Small, @Medium)
   insert @Output values (4, 'Price Changes Today',                     @PCToday,                          null, null,   null)
   insert @Output values (5, 'Price Change Confirmations',              @PriceChangesConfirmedDayQuantity, null, null,   null)
   insert @Output values (6, 'Labels Request',                          null,                              null, null,   null)

   select * from @Output order by RowId
end
go

if @@error = 0
   print 'Success: Stored procedure DashPriceChanges2 for P022-040 has been successfully altered'
else
   print 'Failure: Stored procedure DashPriceChanges2 for P022-040 has NOT been successfully altered'
go