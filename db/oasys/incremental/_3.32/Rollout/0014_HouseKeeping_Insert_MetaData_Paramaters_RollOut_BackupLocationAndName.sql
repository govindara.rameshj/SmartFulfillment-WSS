insert Parameters (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
           values (189, 'House Keeping Backup', 'F:\Userdata\Wix\Sql\HouseKeepingBackup.bak', null, 0, null, 0)
go

if @@error = 0
   print 'Success: The metadata change to Parameters for HouseKeeping has been successfully deployed'
else
   print 'Failure: The metadata change to Parameters for HouseKeeping has NOT been successfully deployed'
go
