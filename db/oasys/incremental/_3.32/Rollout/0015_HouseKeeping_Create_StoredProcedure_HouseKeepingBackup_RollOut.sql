create procedure HouseKeepingBackup
as
begin

   declare @BackupLocationAndName varchar(max)

   set @BackupLocationAndName = (select StringValue from Parameters where ParameterID = 189)

   backup database Oasys to disk = @BackupLocationAndName with format

end
go

if @@error = 0
   print 'Success: Stored procedure HouseKeepingBackup for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure HouseKeepingBackup for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On HouseKeepingBackup To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure HouseKeepingBackup for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure HouseKeepingBackup for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on HouseKeepingBackup to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure HouseKeepingBackup for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure HouseKeepingBackup for HouseKeeping has NOT been successfully deployed'
go
