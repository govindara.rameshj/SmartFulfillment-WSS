
INSERT INTO PARAMETERS
VALUES( -220045 , 
        'P022-045 SOM Customer Search' , 
        NULL , 
        NULL , 
        1 , 
        NULL , 
        3
      );
GO      
IF @@Error = 0
    BEGIN
        PRINT 'Success: The active requirement switch for P022-045 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: The active requirement switch for P022-045 might NOT have been deployed';
    END;
GO
