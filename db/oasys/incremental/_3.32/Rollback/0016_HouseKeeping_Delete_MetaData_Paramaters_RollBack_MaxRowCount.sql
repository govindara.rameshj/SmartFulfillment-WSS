﻿delete Parameters where ParameterID = 188
go

if @@error = 0
   print 'Success: The metadata change to Parameters for House Keeping max row count has been successfully removed'
else
   print 'Failure: The metadata change to Parameters for House Keeping max row count has NOT been successfully removed'
go