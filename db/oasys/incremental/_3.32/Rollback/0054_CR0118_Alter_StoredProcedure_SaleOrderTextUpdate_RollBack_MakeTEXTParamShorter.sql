ALTER PROCEDURE [dbo].[SaleOrderTextUpdate]
@OrderNumber CHAR (6), @Number CHAR (2), @Type CHAR (2), @Text VARCHAR (100)
AS
BEGIN
	SET NOCOUNT ON;

	update 
		CORTXT
	set
		[TEXT]	= @Text
	where
		NUMB		= @OrderNumber
		and LINE	= @Number
		and [TYPE]	= @Type
		
	return @@rowcount
END
Go
If @@Error = 0
   Print 'Success: The Alter Stored Procedure SaleOrderTextUpdate for CR0118 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SaleOrderTextUpdate for CR0118 has not been deployed successfully'
Go
