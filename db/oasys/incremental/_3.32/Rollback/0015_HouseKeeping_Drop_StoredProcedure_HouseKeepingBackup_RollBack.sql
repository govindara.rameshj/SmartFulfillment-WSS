drop procedure HouseKeepingBackup
go

if @@error = 0
   print 'Success: Stored procedure HouseKeepingBackup for HouseKeeping has been successfully dropped'
else
   print 'Failure: Stored procedure HouseKeepingBackup for HouseKeeping has NOT been successfully dropped'
go