Update
	[Parameters]
Set
	StringValue = '3.30.0'
Where
	ParameterID = 0
Go

If @@Error = 0
   Print 'Success: The "Database Version No" has been sucessfully rolled back'
Else
   Print 'Failure: The "Database Version No" might NOT have been rolled back'
Go
