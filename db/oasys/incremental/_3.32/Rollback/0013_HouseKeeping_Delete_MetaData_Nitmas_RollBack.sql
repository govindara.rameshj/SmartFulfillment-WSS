delete NITMAS where TASK = 999
go

if @@error = 0
   print 'Success: The metadata change to NITMAS for HouseKeeping has been successfully removed'
else
   print 'Failure: The metadata change to NITMAS for HouseKeeping has NOT been successfully removed'
go