Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	-22020,      'P022-020 Pick Instructions Report', Null,        Null,      1,            Null,         3)
Go
Go
If @@Error = 0
   Print 'Success: The Roll Back of requirement switch for P022-020 has been successful'
Else
   Print 'Failure: The Roll Back of requirement switch for P022-020 was NOT successful'
Go
