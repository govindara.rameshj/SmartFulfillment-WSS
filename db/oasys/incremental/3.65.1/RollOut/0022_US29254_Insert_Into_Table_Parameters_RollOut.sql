IF NOT EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterID = 7008)
    BEGIN
        INSERT INTO dbo.[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES (7008, 'Enable/Disable Users Synchronization', '', 0, 0, 0.00000, 3)         
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US29254 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US29254 has not been deployed'
GO