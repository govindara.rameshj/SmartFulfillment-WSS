IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('SystemUsers') and name = 'SynchronizedWhen')
BEGIN
    PRINT 'Add SynchronizedWhen column to SystemUsers'
    ALTER TABLE SystemUsers
    ADD SynchronizedWhen datetime NULL
END
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('SystemUsers') and name = 'SynchronizationFailedWhen')
BEGIN
    PRINT 'Add SynchronizationFailedWhen column to SystemUsers'
    ALTER TABLE SystemUsers
    ADD SynchronizationFailedWhen datetime NULL
END
GO

If @@Error = 0
   Print 'Success: Add new columns to SystemUsers for US26995 has been deployed successfully'
ELSE
   Print 'Failure: Add new columns to SystemUsers for US26995 has not been deployed successfully'
GO