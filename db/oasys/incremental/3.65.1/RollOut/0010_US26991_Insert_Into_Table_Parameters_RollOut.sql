IF NOT EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterID = 7007)
    BEGIN
        INSERT INTO dbo.[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES (7007, 'User Management Service URL', 'https://demo3769610.mockable.io/', 0, 0, 0.00000, 0)         
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US26991 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US26991 has not been deployed'
GO