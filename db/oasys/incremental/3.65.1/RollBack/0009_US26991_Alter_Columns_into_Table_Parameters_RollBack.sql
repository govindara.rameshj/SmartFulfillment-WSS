IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('Parameters') and name = 'StringValue' and max_length = 250)
BEGIN
    PRINT 'Alter StringValue column into Parameters'
    ALTER TABLE dbo.Parameters
    ALTER COLUMN StringValue varchar(80) NULL
END
GO

If @@Error = 0
   Print 'Success: Alter column into Parameters for US26991 has been deployed successfully'
ELSE
   Print 'Failure: Alter column into Parameters for US26991 has not been deployed successfully'
GO