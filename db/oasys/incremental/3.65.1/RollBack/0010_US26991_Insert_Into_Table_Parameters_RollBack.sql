IF EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterID = 7007)
    BEGIN
        DELETE FROM dbo.[Parameters] WHERE ParameterID = 7007
    END
GO

If @@Error = 0
   Print 'Success: The Delete into Parameters table for US26991 has been deployed successfully'
Else
   Print 'Failure: The Delete into Parameters table for US26991 has not been deployed'
GO