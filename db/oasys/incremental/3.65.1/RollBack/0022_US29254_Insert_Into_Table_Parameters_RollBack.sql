IF EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterID = 7008)
    BEGIN
        DELETE FROM dbo.[Parameters] WHERE ParameterID = 7008
    END
GO

If @@Error = 0
   Print 'Success: The Delete into Parameters table for US29254 has been deployed successfully'
Else
   Print 'Failure: The Delete into Parameters table for US29254 has not been deployed'
GO