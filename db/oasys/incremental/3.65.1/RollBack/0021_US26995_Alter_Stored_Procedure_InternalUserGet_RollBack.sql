IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[InternalUserGet]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure [InternalUserGet]'
    EXEC ('CREATE PROCEDURE dbo.[InternalUserGet] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

ALTER PROCEDURE [dbo].[InternalUserGet]
   @Id INT = null,
   @OnlyBankingIds bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
		@Idnotshow varchar(max),
		@sqltext nvarchar(max),
		@paramDef nvarchar(100) = '@paramId INT, @paramOnlyBankingIds bit'
		
	SET @Idnotshow = (SELECT [StringValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 985)
	
	SET @sqltext = N'
		SELECT 
			ID						as Id,
			EmployeeCode			as Code,
			SecurityProfileID		as ProfileId,
			Name,
			Initials,
			Position,
			PayrollID				as PayrollId,
			Password,
			PasswordExpires,
			SupervisorPassword		as SuperPassword,
			SupervisorPwdExpires	as SuperPasswordExpires,
			Outlet,
			IsManager,
			IsSupervisor,
			IsDeleted,
			DeletedDate,
			DeletedBy,
			DeletedWhere,
			TillReceiptName,
			LanguageCode
		FROM
			SystemUsers
		WHERE ((@paramId is null) or (@paramId is not null and ID = @paramId)) and '  

	IF len(RTRIM(@Idnotshow)) > 0
		SET @sqltext = @sqltext + '((ID < 500 and ID not in (' + @Idnotshow + ')) or (ID = 555 and @paramOnlyBankingIds = 1))'
	ELSE
		SET @sqltext = @sqltext + '(ID < 500 or (ID = 555 and @paramOnlyBankingIds = 1))'
	
	EXEC sp_executesql @sqltext, @paramDef, @paramId = @Id, @paramOnlyBankingIds = @OnlyBankingIds
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure InternalUserGet for US26995 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure InternalUserGet for US26995 has not been rolled back'
GO