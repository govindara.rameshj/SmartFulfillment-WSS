IF EXISTS (select 1 from sys.columns col inner join sys.objects obj on obj.object_id = col.object_id where obj.Name = 'SystemUsers' and col.name = 'SynchronizedWhen')
BEGIN
    PRINT 'Dropping SynchronizedWhen column from SystemUsers'
    ALTER TABLE dbo.SystemUsers DROP COLUMN SynchronizedWhen
END
GO

IF EXISTS (select 1 from sys.columns col inner join sys.objects obj on obj.object_id = col.object_id where obj.Name = 'SystemUsers' and col.name = 'SynchronizationFailedWhen')
BEGIN
    PRINT 'Dropping SynchronizationFailedWhen column from SystemUsers'
    ALTER TABLE dbo.SystemUsers DROP COLUMN SynchronizationFailedWhen
END
GO

If @@Error = 0
   Print 'Success: Altering table SystemUsers for US26995 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table SystemUsers for US26995 has not been rolled back successfully'
GO