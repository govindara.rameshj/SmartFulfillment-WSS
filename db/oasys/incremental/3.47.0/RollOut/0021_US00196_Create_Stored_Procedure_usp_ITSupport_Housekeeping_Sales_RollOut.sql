---------------------------------------------------------------------------------------------------------------------
-- Task 01 - Install Stored Procedure
---------------------------------------------------------------------------------------------------------------------
Print ('Task 01 - Install usp_ITSupport_Housekeeping_Sales')
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ITSupport_Housekeeping_Sales]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ITSupport_Housekeeping_Sales]
GO

CREATE PROCEDURE dbo.usp_ITSupport_Housekeeping_Sales
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 6th November 2013
-- Description	: System Housekeeping Procedure (Sales)
-----------------------------------------------------------------------------------
@sp_HKDT Date,
@sp_Max Int
As
Begin

Declare @HKDT Date = @sp_HKDT
Declare @Max_Records Int = @sp_Max

-----------------------------------------------------------------------------------
-- Transaction Data Records --
-----------------------------------------------------------------------------------
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#KM_Housekeeping_DL%') and [TYPE] = 'U') Drop Table #KM_Housekeeping_DL
Create Table #KM_Housekeeping_DL([ID] [int] IDENTITY(1,1),[Date] Date Not Null,[Till] Char(2) Not Null, [TranID] Char(4) Not Null)
Insert into #KM_Housekeeping_DL
Select Top (@Max_Records) Date1, Till, [Tran] From DLTOTS Where DATE1 <= @HKDT


-----------------------------------------------------------------------------------
-- DLANAS --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_ANAS_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_ANAS_Count as CHAR))) + ' (DLANAS) Transactional Data record(s). -----')
Declare @KMHK_DL_ANAS_ID int
Declare @KMHK_DL_ANAS_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_ANAS_ID = 1 
While	(@KMHK_DL_ANAS_ID <= @KMHK_DL_ANAS_MAXID)
	Begin
		Declare @KMHK_DL_ANAS_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_ANAS_ID)
		Declare @KMHK_DL_ANAS_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_ANAS_ID)
		Declare @KMHK_DL_ANAS_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_ANAS_ID)
		Delete From DLANAS Where DATE1 = @KMHK_DL_ANAS_Date and [Till] = @KMHK_DL_ANAS_Till and [TRAN] = @KMHK_DL_ANAS_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_ANAS_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_ANAS_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_ANAS_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_ANAS_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_ANAS_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_ANAS_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_ANAS_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_ANAS_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_ANAS_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_ANAS_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_ANAS_ID = @KMHK_DL_ANAS_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLCOMM --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_COMM_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_COMM_Count as CHAR))) + ' (DLCOMM) Transactional Data record(s). -----')
Declare @KMHK_DL_COMM_ID int
Declare @KMHK_DL_COMM_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_COMM_ID = 1 
While	(@KMHK_DL_COMM_ID <= @KMHK_DL_COMM_MAXID)
	Begin
		Declare @KMHK_DL_COMM_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_COMM_ID)
		Declare @KMHK_DL_COMM_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_COMM_ID)
		Declare @KMHK_DL_COMM_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_COMM_ID)
		Delete From DLCOMM Where DATE1 = @KMHK_DL_COMM_Date and [Till] = @KMHK_DL_COMM_Till and [TRAN] = @KMHK_DL_COMM_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_COMM_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_COMM_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_COMM_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_COMM_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_COMM_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_COMM_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_COMM_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_COMM_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_COMM_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_COMM_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_COMM_ID = @KMHK_DL_COMM_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLCOUPON --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_COUPON_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_COUPON_Count as CHAR))) + ' (DLCOUPON) Transactional Data record(s). -----')
Declare @KMHK_DL_COUPON_ID int
Declare @KMHK_DL_COUPON_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_COUPON_ID = 1 
While	(@KMHK_DL_COUPON_ID <= @KMHK_DL_COUPON_MAXID)
	Begin
		Declare @KMHK_DL_COUPON_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_COUPON_ID)
		Declare @KMHK_DL_COUPON_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_COUPON_ID)
		Declare @KMHK_DL_COUPON_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_COUPON_ID)
		Delete From DLCOUPON Where TranDate = @KMHK_DL_COUPON_Date and [TranTillID] = @KMHK_DL_COUPON_Till and [TRANNO] = @KMHK_DL_COUPON_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_COUPON_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_COUPON_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_COUPON_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_COUPON_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_COUPON_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_COUPON_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_COUPON_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_COUPON_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_COUPON_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_COUPON_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_COUPON_ID = @KMHK_DL_COUPON_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLEANCHK --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_EANCHK_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_EANCHK_Count as CHAR))) + ' (DLEANCHK) Transactional Data record(s). -----')
Declare @KMHK_DL_EANCHK_ID int
Declare @KMHK_DL_EANCHK_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_EANCHK_ID = 1 
While	(@KMHK_DL_EANCHK_ID <= @KMHK_DL_EANCHK_MAXID)
	Begin
		Declare @KMHK_DL_EANCHK_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_EANCHK_ID)
		Declare @KMHK_DL_EANCHK_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_EANCHK_ID)
		Declare @KMHK_DL_EANCHK_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_EANCHK_ID)
		Delete From DLEANCHK Where DATE1 = @KMHK_DL_EANCHK_Date and [Till] = @KMHK_DL_EANCHK_Till and [TRAN] = @KMHK_DL_EANCHK_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_EANCHK_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_EANCHK_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_EANCHK_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_EANCHK_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_EANCHK_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_EANCHK_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_EANCHK_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_EANCHK_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_EANCHK_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_EANCHK_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_EANCHK_ID = @KMHK_DL_EANCHK_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLEVNT --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_EVNT_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_EVNT_Count as CHAR))) + ' (DLEVNT) Transactional Data record(s). -----')
Declare @KMHK_DL_EVNT_ID int
Declare @KMHK_DL_EVNT_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_EVNT_ID = 1 
While	(@KMHK_DL_EVNT_ID <= @KMHK_DL_EVNT_MAXID)
	Begin
		Declare @KMHK_DL_EVNT_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_EVNT_ID)
		Declare @KMHK_DL_EVNT_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_EVNT_ID)
		Declare @KMHK_DL_EVNT_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_EVNT_ID)
		Delete From DLEVNT Where DATE1 = @KMHK_DL_EVNT_Date and [Till] = @KMHK_DL_EVNT_Till and [TRAN] = @KMHK_DL_EVNT_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_EVNT_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_EVNT_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_EVNT_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_EVNT_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_EVNT_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_EVNT_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_EVNT_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_EVNT_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_EVNT_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_EVNT_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_EVNT_ID = @KMHK_DL_EVNT_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLGIFT --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_GIFT_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_GIFT_Count as CHAR))) + ' (DLGIFT) Transactional Data record(s). -----')
Declare @KMHK_DL_GIFT_ID int
Declare @KMHK_DL_GIFT_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_GIFT_ID = 1 
While	(@KMHK_DL_GIFT_ID <= @KMHK_DL_GIFT_MAXID)
	Begin
		Declare @KMHK_DL_GIFT_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_GIFT_ID)
		Declare @KMHK_DL_GIFT_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_GIFT_ID)
		Declare @KMHK_DL_GIFT_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_GIFT_ID)
		Delete From DLGIFT Where DATE1 = @KMHK_DL_GIFT_Date and [Till] = @KMHK_DL_GIFT_Till and [TRAN] = @KMHK_DL_GIFT_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFT_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFT_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_GIFT_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_GIFT_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFT_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFT_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFT_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_GIFT_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_GIFT_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFT_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_GIFT_ID = @KMHK_DL_GIFT_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLGIFTCARD --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_GIFTCARD_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_GIFTCARD_Count as CHAR))) + ' (DLGIFTCARD) Transactional Data record(s). -----')
Declare @KMHK_DL_GIFTCARD_ID int
Declare @KMHK_DL_GIFTCARD_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_GIFTCARD_ID = 1 
While	(@KMHK_DL_GIFTCARD_ID <= @KMHK_DL_GIFTCARD_MAXID)
	Begin
		Declare @KMHK_DL_GIFTCARD_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_GIFTCARD_ID)
		Declare @KMHK_DL_GIFTCARD_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_GIFTCARD_ID)
		Declare @KMHK_DL_GIFTCARD_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_GIFTCARD_ID)
		Delete From DLGIFTCARD Where DATE1 = @KMHK_DL_GIFTCARD_Date and [Till] = @KMHK_DL_GIFTCARD_Till and [TRAN] = @KMHK_DL_GIFTCARD_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFTCARD_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFTCARD_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_GIFTCARD_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_GIFTCARD_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFTCARD_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFTCARD_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFTCARD_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_GIFTCARD_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_GIFTCARD_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_GIFTCARD_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_GIFTCARD_ID = @KMHK_DL_GIFTCARD_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLLINE --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_LINE_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_LINE_Count as CHAR))) + ' (DLLINE) Transactional Data record(s). -----')
Declare @KMHK_DL_LINE_ID int
Declare @KMHK_DL_LINE_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_LINE_ID = 1 
While	(@KMHK_DL_LINE_ID <= @KMHK_DL_LINE_MAXID)
	Begin
		Declare @KMHK_DL_LINE_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_LINE_ID)
		Declare @KMHK_DL_LINE_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_LINE_ID)
		Declare @KMHK_DL_LINE_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_LINE_ID)
		Delete From DLLINE Where DATE1 = @KMHK_DL_LINE_Date and [Till] = @KMHK_DL_LINE_Till and [TRAN] = @KMHK_DL_LINE_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_LINE_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_LINE_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_LINE_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_LINE_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_LINE_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_LINE_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_LINE_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_LINE_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_LINE_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_LINE_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_LINE_ID = @KMHK_DL_LINE_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLOCUS --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_OCUS_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_OCUS_Count as CHAR))) + ' (DLOCUS) Transactional Data record(s). -----')
Declare @KMHK_DL_OCUS_ID int
Declare @KMHK_DL_OCUS_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_OCUS_ID = 1 
While	(@KMHK_DL_OCUS_ID <= @KMHK_DL_OCUS_MAXID)
	Begin
		Declare @KMHK_DL_OCUS_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_OCUS_ID)
		Declare @KMHK_DL_OCUS_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_OCUS_ID)
		Declare @KMHK_DL_OCUS_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_OCUS_ID)
		Delete From DLOCUS Where DATE1 = @KMHK_DL_OCUS_Date and [Till] = @KMHK_DL_OCUS_Till and [TRAN] = @KMHK_DL_OCUS_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_OCUS_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_OCUS_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_OCUS_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_OCUS_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_OCUS_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_OCUS_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_OCUS_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_OCUS_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_OCUS_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_OCUS_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_OCUS_ID = @KMHK_DL_OCUS_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLOLIN --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_OLIN_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_OLIN_Count as CHAR))) + ' (DLOLIN) Transactional Data record(s). -----')
Declare @KMHK_DL_OLIN_ID int
Declare @KMHK_DL_OLIN_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_OLIN_ID = 1 
While	(@KMHK_DL_OLIN_ID <= @KMHK_DL_OLIN_MAXID)
	Begin
		Declare @KMHK_DL_OLIN_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_OLIN_ID)
		Declare @KMHK_DL_OLIN_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_OLIN_ID)
		Declare @KMHK_DL_OLIN_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_OLIN_ID)
		Delete From DLOLIN Where DATE1 = @KMHK_DL_OLIN_Date and [Till] = @KMHK_DL_OLIN_Till and [TRAN] = @KMHK_DL_OLIN_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_OLIN_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_OLIN_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_OLIN_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_OLIN_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_OLIN_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_OLIN_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_OLIN_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_OLIN_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_OLIN_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_OLIN_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_OLIN_ID = @KMHK_DL_OLIN_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLPAID --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_PAID_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_PAID_Count as CHAR))) + ' (DLPAID) Transactional Data record(s). -----')
Declare @KMHK_DL_PAID_ID int
Declare @KMHK_DL_PAID_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_PAID_ID = 1 
While	(@KMHK_DL_PAID_ID <= @KMHK_DL_PAID_MAXID)
	Begin
		Declare @KMHK_DL_PAID_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_PAID_ID)
		Declare @KMHK_DL_PAID_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_PAID_ID)
		Declare @KMHK_DL_PAID_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_PAID_ID)
		Delete From DLPAID Where DATE1 = @KMHK_DL_PAID_Date and [Till] = @KMHK_DL_PAID_Till and [TRAN] = @KMHK_DL_PAID_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_PAID_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_PAID_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_PAID_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_PAID_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_PAID_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_PAID_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_PAID_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_PAID_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_PAID_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_PAID_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_PAID_ID = @KMHK_DL_PAID_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLRCUS --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_RCUS_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_RCUS_Count as CHAR))) + ' (DLRCUS) Transactional Data record(s). -----')
Declare @KMHK_DL_RCUS_ID int
Declare @KMHK_DL_RCUS_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_RCUS_ID = 1 
While	(@KMHK_DL_RCUS_ID <= @KMHK_DL_RCUS_MAXID)
	Begin
		Declare @KMHK_DL_RCUS_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_RCUS_ID)
		Declare @KMHK_DL_RCUS_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_RCUS_ID)
		Declare @KMHK_DL_RCUS_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_RCUS_ID)
		Delete From DLRCUS Where DATE1 = @KMHK_DL_RCUS_Date and [Till] = @KMHK_DL_RCUS_Till and [TRAN] = @KMHK_DL_RCUS_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_RCUS_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_RCUS_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_RCUS_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_RCUS_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_RCUS_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_RCUS_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_RCUS_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_RCUS_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_RCUS_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_RCUS_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_RCUS_ID = @KMHK_DL_RCUS_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLREJECT --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_REJECT_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_REJECT_Count as CHAR))) + ' (DLREJECT) Transactional Data record(s). -----')
Declare @KMHK_DL_REJECT_ID int
Declare @KMHK_DL_REJECT_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_REJECT_ID = 1 
While	(@KMHK_DL_REJECT_ID <= @KMHK_DL_REJECT_MAXID)
	Begin
		Declare @KMHK_DL_REJECT_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_REJECT_ID)
		Declare @KMHK_DL_REJECT_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_REJECT_ID)
		Declare @KMHK_DL_REJECT_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_REJECT_ID)
		Delete From DLREJECT Where DATE1 = @KMHK_DL_REJECT_Date and [Till] = @KMHK_DL_REJECT_Till and [TRAN] = @KMHK_DL_REJECT_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_REJECT_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_REJECT_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_REJECT_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_REJECT_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_REJECT_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_REJECT_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_REJECT_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_REJECT_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_REJECT_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_REJECT_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_REJECT_ID = @KMHK_DL_REJECT_ID+1 
	End
-----------------------------------------------------------------------------------
-- DLTOTS --
-----------------------------------------------------------------------------------
Declare @KMHK_DL_TOTS_Count int = (Select LTRIM(RTRIM(CAST(Count(ID) as CHAR))) From #KM_Housekeeping_DL)
Print ('----- Preparing to Delete ' + LTRIM(RTRIM(CAST(@KMHK_DL_TOTS_Count as CHAR))) + ' (DLTOTS) Transactional Data record(s). -----')
Declare @KMHK_DL_TOTS_ID int
Declare @KMHK_DL_TOTS_MAXID int = (Select MAX(ID)FROM #KM_Housekeeping_DL)
Set		@KMHK_DL_TOTS_ID = 1 
While	(@KMHK_DL_TOTS_ID <= @KMHK_DL_TOTS_MAXID)
	Begin
		Declare @KMHK_DL_TOTS_Date Date = (Select [Date] From #KM_Housekeeping_DL Where ID = @KMHK_DL_TOTS_ID)
		Declare @KMHK_DL_TOTS_Till Char(2) = (Select [Till] From #KM_Housekeeping_DL Where ID = @KMHK_DL_TOTS_ID)
		Declare @KMHK_DL_TOTS_Tran Char(4) = (Select [TranID] From #KM_Housekeeping_DL Where ID = @KMHK_DL_TOTS_ID)
		Delete From DLTOTS Where DATE1 = @KMHK_DL_TOTS_Date and [Till] = @KMHK_DL_TOTS_Till and [TRAN] = @KMHK_DL_TOTS_Tran
		If @@ERROR = 0
			Begin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_TOTS_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_TOTS_MaxID as Char))) + ' Success T:<' + LTRIM(RTRIM(@KMHK_DL_TOTS_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_TOTS_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_TOTS_Date as Char))) + '>')		
			End
		Else
			BEgin
				Print('R:' + LTRIM(RTRIM(Cast(@KMHK_DL_TOTS_ID as Char))) + '/' + LTRIM(RTRIM(Cast(@KMHK_DL_TOTS_MaxID as Char))) + ' ERROR T:<' + LTRIM(RTRIM(@KMHK_DL_TOTS_Till)) + '> X:<' + LTRIM(RTRIM(@KMHK_DL_TOTS_Tran)) + '> D:<' + LTRIM(RTRIM(Cast(@KMHK_DL_TOTS_Date as Char))) + '>')		
			End	
		Set @KMHK_DL_TOTS_ID = @KMHK_DL_TOTS_ID+1 
	End
Drop Table #KM_Housekeeping_DL

-- End Sales Process
End
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure usp_ITSupport_Housekeeping_Sales for US196 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure usp_ITSupport_Housekeeping_Sales for US196 has not been deployed successfully'
GO