IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'SecurityProfileGetByType') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure SecurityProfileGetByType'
	EXEC ('CREATE PROCEDURE dbo.SecurityProfileGetByType AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SecurityProfileGetByType')
GO

ALTER PROCEDURE [dbo].[SecurityProfileGetByType]
	@ProfileType	char(1)='A'
AS
begin
	SET NOCOUNT ON;

	select 
		ID					as Id,
		ProfileType,
		Description,
		Position,
		IsAreaManager,
		PasswordValidFor	as DaysPasswordValid,
		IsDeleted,
		DeletedBy,
		DeletedDate			as DateDeleted,
		DateLastEdited,
		IsSystemAdmin,
		IsHelpDeskUser
	from
		SecurityProfile
	where
		ProfileType=@ProfileType
		
end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SecurityProfileGetByType for US85 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SecurityProfileGetByType for US85 has not been deployed'
GO
