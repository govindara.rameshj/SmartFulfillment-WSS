SET XACT_ABORT ON
GO

IF NOT EXISTS (
	SELECT 1 FROM sys.columns col 
	INNER JOIN sys.objects obj ON obj.object_id = col.object_id
	WHERE obj.Name = 'SecurityProfile' AND col.name = 'IsSupervisor'
) AND NOT EXISTS (
	SELECT 1 FROM sys.columns col 
	INNER JOIN sys.objects obj ON obj.object_id = col.object_id
	WHERE obj.Name = 'SecurityProfile' AND col.name = 'IsManager'
)
BEGIN
	PRINT 'Failure: Data migration has already done earlier for US85'
END
ELSE
BEGIN
	BEGIN TRANSACTION;

	BEGIN TRY

		PRINT 'Starting data migration from SecurityProfile table into SystemUsers table for US85'
		
		-- Save original values IsSupervisor and IsManager that belong SystemUsers table into SYSPAS table
		
		UPDATE [dbo].[SYSPAS] 
		SET [IAFG1] = su.[IsSupervisor], [IAFG2] = su.[IsManager]
		FROM [dbo].[SystemUsers] AS su 
		WHERE su.[EmployeeCode] = [EEID]
		
		-- Migrate values IsSupervisor and IsManager from SecurityProfile table into SystemUsers table	
		
		DECLARE @systemUsersID INT
		DECLARE @securityProfileID INT
		
		DECLARE SystemUsersCursor CURSOR FOR 
		SELECT [ID], [SecurityProfileID]
		FROM [dbo].[SystemUsers]

		OPEN SystemUsersCursor
		FETCH NEXT FROM SystemUsersCursor INTO @systemUsersID, @securityProfileID

		WHILE @@FETCH_STATUS = 0
		BEGIN
				
			EXEC('UPDATE [dbo].[SystemUsers] 
			SET [dbo].[SystemUsers].[IsSupervisor] = sp.[IsSupervisor], 
				[dbo].[SystemUsers].[IsManager] = sp.[IsManager]
			FROM [dbo].[SecurityProfile] AS sp 
			WHERE sp.[ID] = [dbo].[SystemUsers].[SecurityProfileID] 
			AND [dbo].[SystemUsers].[SecurityProfileID] = ' + @securityProfileID
			+ ' AND [dbo].[SystemUsers].[ID] = ' + @systemUsersID)
	                
			FETCH NEXT FROM SystemUsersCursor INTO @systemUsersID, @securityProfileID
		END

		CLOSE SystemUsersCursor
		DEALLOCATE SystemUsersCursor
		
		--- Columns dropping 
		
		DECLARE @constraintName nvarchar(200)
		IF EXISTS (
			SELECT 1 FROM sys.columns col 
			INNER JOIN sys.objects obj ON obj.object_id = col.object_id
			WHERE obj.Name = 'SecurityProfile' AND col.name = 'IsSupervisor'
		)
		BEGIN	
			-- Drop constraint
			SELECT @constraintName = Name 
			FROM SYS.DEFAULT_CONSTRAINTS
			WHERE PARENT_OBJECT_ID = OBJECT_ID('SecurityProfile')
				AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
									WHERE NAME = N'IsSupervisor'
									AND object_id = OBJECT_ID(N'SecurityProfile'))
			                        
			IF @ConstraintName IS NOT NULL
			BEGIN
				EXEC('ALTER TABLE SecurityProfile DROP CONSTRAINT ' + @constraintName)		
			END
			-- Drop column
			ALTER TABLE dbo.SecurityProfile DROP COLUMN IsSupervisor
		END

		IF EXISTS (
			SELECT 1 FROM sys.columns col 
			INNER JOIN sys.objects obj ON obj.object_id = col.object_id
			WHERE obj.Name = 'SecurityProfile' AND col.name = 'IsManager'
		)
		BEGIN
			-- Drop constraint
			SELECT @constraintName = Name 
			FROM SYS.DEFAULT_CONSTRAINTS
			WHERE PARENT_OBJECT_ID = OBJECT_ID('SecurityProfile')
				AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
									WHERE NAME = N'IsManager'
									AND object_id = OBJECT_ID(N'SecurityProfile'))
			                        
			IF @ConstraintName IS NOT NULL
			BEGIN
				EXEC('ALTER TABLE SecurityProfile DROP CONSTRAINT ' + @constraintName)		
			END		
			-- Drop column
			ALTER TABLE dbo.SecurityProfile DROP COLUMN IsManager
		END		
		
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
			PRINT 'Failure: Data migration from SecurityProfile table into SystemUsers table for US85 has not been deployed'
			PRINT 'ErrorNumber = ' + CONVERT(varchar, ERROR_NUMBER()) 
				+ ', ErrorSeverity = ' +  CONVERT(varchar, ERROR_SEVERITY())
				+ ', ErrorState = ' + CONVERT(varchar, ERROR_STATE())
				+ ', ErrorLine = ' + CONVERT(varchar, ERROR_LINE())
				+ ', ErrorMessage = ' + ERROR_MESSAGE()
		END
	END CATCH;

	IF @@TRANCOUNT > 0
	BEGIN
		COMMIT TRANSACTION;
		PRINT 'Success: Data migration from SecurityProfile table into SystemUsers table for US85 has been deployed successfully'
	END
END
GO