If not exists (select 1 from [dbo].[NITMAS] where [TASK] = 139)
INSERT INTO [dbo].[NITMAS]
           ([NSET]
           ,[TASK]
           ,[DESCR]
           ,[PROG]
           ,[NITE]
           ,[RETY]
           ,[WEEK]
           ,[PEND]
           ,[LIVE]
           ,[OPTN]
           ,[ABOR]
           ,[JRUN]
           ,[DAYS1]
           ,[DAYS2]
           ,[DAYS3]
           ,[DAYS4]
           ,[DAYS5]
           ,[DAYS6]
           ,[DAYS7]
           ,[BEGD]
           ,[ENDD]
           ,[SDAT]
           ,[STIM]
           ,[EDAT]
           ,[ETIM]
           ,[ADAT]
           ,[ATIM]
           ,[TTYPE])
     VALUES
           (1
           ,139
           ,'Process Trans - Pending Supplier Updates'
           ,'ProcessTransmissions.exe| TIBPFV'
           ,1
           ,1
           ,0
           ,0
           ,1
           ,null
           ,0
           ,null
           ,1
           ,1
           ,1
           ,1
           ,1
           ,1
           ,1
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null
           ,0)
GO

If @@Error = 0
   Print 'Success: Insert Table [NITMAS] for US193 has been deployed successfully'
Else
   Print 'Failure: Insert Table [NITMAS] for US193 has not been deployed successfully'
Go

