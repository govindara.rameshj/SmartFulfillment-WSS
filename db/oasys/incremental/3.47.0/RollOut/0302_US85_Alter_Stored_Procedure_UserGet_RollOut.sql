IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'UserGet') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure UserGet'
	EXEC ('CREATE PROCEDURE dbo.UserGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure UserGet')
GO

ALTER PROCEDURE [dbo].[UserGet]
@Id INT=null
AS
begin
	SET NOCOUNT ON;

	select 
		ID				as Id,
		EmployeeCode			as Code,
		SecurityProfileID		as ProfileId,
		Name,
		Initials,
		Position,
		PayrollID			as PayrollId,
		Password,
		PasswordExpires,
		SupervisorPassword		as SuperPassword,
		SupervisorPwdExpires		as SuperPasswordExpires,
		Outlet,
		IsManager,
		IsSupervisor,
		IsDeleted,
		DeletedDate,
		DeletedBy,
		DeletedWhere,
		TillReceiptName,
		LanguageCode
	from
		SystemUsers
	where
		(@Id is null) or (@Id is not null and ID = @Id)
	
end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure UserGet for US85 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure UserGet for US85 has not been deployed'
GO