INSERT INTO [dbo].[Parameters]
	([ParameterID]
	,[Description]
	,[StringValue]
	,[LongValue]
	,[BooleanValue]
	,[DecimalValue]
	,[ValueType])
SELECT 
	 [ParameterID]
	,[Description]
	,[StringValue]
	,0   as [LongValue]
	,0   as [BooleanValue]
	,0.0 as [DecimalValue]
	,0   as [ValueType]
FROM (
	SELECT 7000 as [ParameterID], cast('ApiAddress' as varchar(50)) as [Description], cast('' as varchar(80)) as [StringValue]
	UNION ALL SELECT 7005, 'Max Retries Number for ExternalRequestMonitor', '5'
	UNION ALL SELECT 7006, 'Retry Period Minutes for ExternalRequestMonitor', '5'
) t
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] prm WHERE prm.ParameterID = t.ParameterID)      
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US33083 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US33083 has not been deployed successfully'
Go