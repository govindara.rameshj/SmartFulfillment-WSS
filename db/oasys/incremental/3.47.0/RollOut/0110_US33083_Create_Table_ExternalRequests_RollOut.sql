IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ExternalRequests]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	PRINT 'Creating table ExternalRequests'
	CREATE TABLE [dbo].[ExternalRequests]
	(
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreateTime] [datetime] NOT NULL DEFAULT GETDATE(),
	[RequestDetailsXml] [xml] NOT NULL,
	[Status] [int] NOT NULL,
	[Type] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
	[Source] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
	[Error] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
	[RetriesCount] [int] NOT NULL,
	[LastRetryDateTime] [datetime] NULL
	
	CONSTRAINT [PK_ExternalRequests] PRIMARY KEY CLUSTERED ([Id] ASC)
	) ON [PRIMARY]
	
	EXEC sp_addextendedproperty N'MS_Description', N'REST requests to cloud services', 'SCHEMA', N'dbo', 'TABLE', N'ExternalRequests', NULL, NULL
	EXEC sp_addextendedproperty N'MS_Description', N'XML that contains request details, different for each type of request', 'SCHEMA', N'dbo', 'TABLE', N'ExternalRequests', 'COLUMN', N'RequestDetailsXml'
	EXEC sp_addextendedproperty N'MS_Description', N'Current status: 0 - New, 1 - Processed, 2 - Failed', 'SCHEMA', N'dbo', 'TABLE', N'ExternalRequests', 'COLUMN', N'Status'
	EXEC sp_addextendedproperty N'MS_Description', N'Corresponds to operations supported by service', 'SCHEMA', N'dbo', 'TABLE', N'ExternalRequests', 'COLUMN', N'Type'
	EXEC sp_addextendedproperty N'MS_Description', N'Describes business action that led to request creation', 'SCHEMA', N'dbo', 'TABLE', N'ExternalRequests', 'COLUMN', N'Source'
	EXEC sp_addextendedproperty N'MS_Description', N'Description of error occured during request processing', 'SCHEMA', N'dbo', 'TABLE', N'ExternalRequests', 'COLUMN', N'Error'
	EXEC sp_addextendedproperty N'MS_Description', N'How much retries was made by ExternalRequestMonitor', 'SCHEMA', N'dbo', 'TABLE', N'ExternalRequests', 'COLUMN', N'RetriesCount'
	EXEC sp_addextendedproperty N'MS_Description', N'Date and time of last retry', 'SCHEMA', N'dbo', 'TABLE', N'ExternalRequests', 'COLUMN', N'LastRetryDateTime'
END
ELSE
BEGIN
 PRINT 'ExternalRequests table is already exists.'
END
GO

If @@Error = 0
   Print 'Success: Create Table ExternalRequests for US33083 has been deployed successfully'
Else
   Print 'Failure: Create Table ExternalRequests for US33083 has not been deployed successfully'
Go