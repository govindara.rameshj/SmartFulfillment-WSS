IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'StartingFloat') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure StartingFloat'
	EXEC ('CREATE PROCEDURE dbo.StartingFloat AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure StartingFloat')
GO

--"manual check" & un-assigned floats need to maintain its link to the pickup that created it
ALTER PROCEDURE [dbo].[StartingFloat]
   @FloatID      INT,
   @StartFloatID INT OUTPUT
AS
BEGIN
SET NOCOUNT ON
	DECLARE @RelatedBagID INT
	DECLARE @Iteration INT
	
	SET @Iteration = 0
	
	WHILE 1 = 1 --previous cancelled float exists
		BEGIN
			SET @Iteration = @Iteration + 1
			
			IF @Iteration > 1000
				BEGIN
					RAISERROR('Could not find StartingFloat within 1000 iterations.', 16, 3)
					RETURN -1					
				END
				
			SET @RelatedBagID = (SELECT RelatedBagID FROM SafeBags WHERE ID = @FloatID)
			
			IF @RelatedBagID IS NULL
				BEGIN
					RAISERROR('Bag is not defined.', 16, 3)
					RETURN -1
				END

			IF @RelatedBagID = 0
				BEGIN
					--start of chain
					SET @StartFloatID = @FloatID
					BREAK
				END
			ELSE
				BEGIN
					SET @FloatID = @RelatedBagID
				END			
		END	
END
GO

IF @@Error = 0
   PRINT 'Success: The Alter Stored Procedure StartingFloat for US818 has been deployed successfully'
ELSE
   PRINT 'Failure: The Alter Stored Procedure StartingFloat for US818 has not been deployed'
GO