DELETE FROM [dbo].[Parameters]
WHERE [ParameterID] = 4601
GO

INSERT INTO [dbo].[Parameters]
	([ParameterID]
	,[Description]
	,[StringValue]
	,[LongValue]
	,[BooleanValue]
	,[DecimalValue]
	,[ValueType])
SELECT 
		 [ParameterID]
		,[Description]
		,''  as [StringValue]
		,[LongValue]
		,0   as [BooleanValue]
		,0.0 as [DecimalValue]
		,1   as [ValueType]
FROM (
	SELECT  4600 as [ParameterID], cast('Retry count for SQL execution' as varchar(50)) as [Description], cast(2 as bigint) as [LongValue]
) t
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] prm WHERE prm.ParameterID = t.ParameterID)      
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US191 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US191 has not been deployed successfully'
GO
