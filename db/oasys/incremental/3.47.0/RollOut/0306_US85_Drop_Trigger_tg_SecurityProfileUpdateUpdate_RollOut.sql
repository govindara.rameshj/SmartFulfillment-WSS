IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'tg_SecurityProfileUpdateUpdate'))
BEGIN
	PRINT 'Dropping trigger tg_SecurityProfileUpdateUpdate'
	DROP TRIGGER dbo.tg_SecurityProfileUpdateUpdate
END
GO

If @@Error = 0
   Print 'Success: The Drop Trigger Stg_SecurityProfileUpdateUpdate for US85 has been deployed successfully'
Else
   Print 'Failure: Drop Trigger Stg_SecurityProfileUpdateUpdate for US85 has not been deployed'
GO