IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'ReturnsSelectDetailsForId') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure ReturnsSelectDetailsForId'
	EXEC ('CREATE PROCEDURE dbo.ReturnsSelectDetailsForId AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure ReturnsSelectDetailsForId')
GO

ALTER PROCEDURE [dbo].[ReturnsSelectDetailsForId]
@Id INT
AS
begin
	SELECT		
		rl.TKEY, 
		rl.HKEY, 
		rl.SKUN, 
		sk.DESCR AS SkuDescription, 
		rl.QUAN, 
		sk.PRIC, 
		rl.COST, 
		rl.QUAN * sk.PRIC AS Value,
		rl.REAS, 
		sc.DESCR AS ReasonDescription, 
		rl.RTI
	FROM        
		RETLIN rl 
	INNER JOIN 
		STKMAS sk ON rl.SKUN = sk.SKUN 
	INNER JOIN 
		SYSCOD sc ON right('00' + rtrim(rl.REAS), 2) = sc.CODE and sc.TYPE = 'SR'
	WHERE
		rl.HKEY = @Id
	ORDER BY	
		SkuDescription

end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ReturnsSelectDetailsForId for US34221 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure ReturnsSelectDetailsForId for US34221 has not been deployed'
GO


