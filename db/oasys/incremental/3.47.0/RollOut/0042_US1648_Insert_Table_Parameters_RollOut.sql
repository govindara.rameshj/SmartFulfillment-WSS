IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = 958)
BEGIN
	INSERT INTO dbo.Parameters (
		[ParameterID],
		[Description],
		[StringValue],
		[LongValue],
		[BooleanValue],
		[DecimalValue],
		[ValueType])
	VALUES (958, 'POS Printer wait timeout (milliseconds)', NULL, 20000, 0, NULL, 1)	
END

If @@Error = 0
   Print 'Success: Insert into table Parameters for US1648 has been deployed successfully'
Else
   Print 'Failure: Insert into table Parameters for US1648 has not been deployed successfully'
GO