DELETE [dbo].[Parameters]
WHERE [ParameterID] IN (7000, 7005, 7006)
GO

If @@Error = 0
   Print 'Success: Delete from Table Parameters for US33083 has been deployed successfully'
Else
   Print 'Failure: Delete from Table Parameters for US33083 has not been deployed successfully'
Go