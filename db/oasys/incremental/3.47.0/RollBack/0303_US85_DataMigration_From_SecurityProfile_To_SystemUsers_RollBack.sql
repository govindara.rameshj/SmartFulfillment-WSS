SET XACT_ABORT ON
GO

IF EXISTS (
	SELECT 1 FROM sys.columns col 
	INNER JOIN sys.objects obj ON obj.object_id = col.object_id
	WHERE obj.Name = 'SecurityProfile' AND col.name = 'IsSupervisor'
) AND EXISTS (
	SELECT 1 FROM sys.columns col 
	INNER JOIN sys.objects obj ON obj.object_id = col.object_id
	WHERE obj.Name = 'SecurityProfile' AND col.name = 'IsManager'
)
BEGIN
	PRINT 'Failure: Data migration was not executed earlier for US85'
END
ELSE
BEGIN
	BEGIN TRANSACTION;

	BEGIN TRY

		PRINT 'Starting data migration rollback for US85'
		
		-- Add columns IsManager and IsSupervisor
		
		IF NOT EXISTS (
			SELECT 1 FROM sys.columns col 
			INNER JOIN sys.objects obj ON obj.object_id = col.object_id
			WHERE obj.Name = 'SecurityProfile' AND col.name = 'IsSupervisor'
		)
		BEGIN		
			ALTER TABLE dbo.SecurityProfile ADD IsSupervisor BIT NOT NULL DEFAULT (0)		
			EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If supervisor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'IsSupervisor'
		END

		IF NOT EXISTS (
			SELECT 1 FROM sys.columns col 
			INNER JOIN sys.objects obj ON obj.object_id = col.object_id
			WHERE obj.Name = 'SecurityProfile' AND col.name = 'IsManager'
		)
		BEGIN
			ALTER TABLE dbo.SecurityProfile ADD IsManager BIT NOT NULL DEFAULT (0)		
			EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Manager' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'IsManager'
		END				
		
		
		-- Restore values IsSupervisor and IsManager in SecurityProfile table from SystemUsers table	
		
		DECLARE @isManager BIT
		DECLARE @isSupervisor BIT
		DECLARE @securityProfileID INT
		
		DECLARE SecurityProfileCursor CURSOR FOR 
		SELECT [ID]
		FROM [dbo].[SecurityProfile]
		WHERE ProfileType = 'U'

		OPEN SecurityProfileCursor
		FETCH NEXT FROM SecurityProfileCursor INTO @securityProfileID

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @isManager = 0
			SET @isSupervisor = 0
			
			SELECT TOP 1
				@isManager = ROUND(AVG(CAST([IsManager] AS FLOAT)), 0)
				,@isSupervisor = ROUND(AVG(CAST([IsSupervisor] AS FLOAT)), 0)
			FROM [dbo].[SystemUsers]
			WHERE [SecurityProfileID] = @securityProfileID
			GROUP BY [SecurityProfileID]

			EXEC('UPDATE [dbo].[SecurityProfile] SET [IsManager] = ' + @isManager + ',[IsSupervisor] = ' + @isSupervisor + ' WHERE [ID] = ' + @securityProfileID)
	                
			FETCH NEXT FROM SecurityProfileCursor INTO @securityProfileID
		END

		CLOSE SecurityProfileCursor
		DEALLOCATE SecurityProfileCursor
			
		
		-- Restore original values IsSupervisor and IsManager that belong SystemUsers table from SYSPAS table
		
		DECLARE @systemUsersID INT
		
		DECLARE SystemUsersCursor CURSOR FOR 
		SELECT [ID]
		FROM [dbo].[SystemUsers]

		OPEN SystemUsersCursor
		FETCH NEXT FROM SystemUsersCursor INTO @systemUsersID

		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			UPDATE [dbo].[SystemUsers] 
			SET [IsSupervisor] = sp.[IAFG1], [IsManager] = sp.[IAFG2] 
			FROM [dbo].[SYSPAS] AS sp 
			WHERE sp.[EEID] = [EmployeeCode] AND [ID] = @systemUsersID
	                
			FETCH NEXT FROM SystemUsersCursor INTO @systemUsersID
		END

		CLOSE SystemUsersCursor
		DEALLOCATE SystemUsersCursor
		
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
			PRINT 'Failure: Data migration rollback for US85 has not been deployed'
			PRINT 'ErrorNumber = ' + CONVERT(varchar, ERROR_NUMBER()) 
				+ ', ErrorSeverity = ' +  CONVERT(varchar, ERROR_SEVERITY())
				+ ', ErrorState = ' + CONVERT(varchar, ERROR_STATE())
				+ ', ErrorLine = ' + CONVERT(varchar, ERROR_LINE())
				+ ', ErrorMessage = ' + ERROR_MESSAGE()
		END
	END CATCH;

	IF @@TRANCOUNT > 0
	BEGIN
		COMMIT TRANSACTION;
		PRINT 'Success: Data migration rollback for US85 has been deployed successfully'
	END
END
GO