IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'tg_SecurityProfileUpdateUpdate'))
BEGIN
	PRINT 'Creating trigger tg_SecurityProfileUpdateUpdate'
	EXEC ('CREATE TRIGGER dbo.tg_SecurityProfileUpdateUpdate ON dbo.SecurityProfile AFTER UPDATE AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering trigger tg_SecurityProfileUpdateUpdate')
GO

-- =============================================
-- Author:		Alan Lewis (deployed by Partha Dutta)
-- Create date: 10/05/2011
-- Description:	Use the IsSupervisor and IsManager values to update
--	            any SystemUser entries that have their SecurityProfileId
--	            equal to the Id of the updated SecurityPrfile record
-- =============================================
ALTER TRIGGER 
	[dbo].[tg_SecurityProfileUpdateUpdate]
ON
	[dbo].[SecurityProfile]
AFTER 
	UPDATE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE
		@IsSupervisor	bit,
		@IsManager	bit,
		@Id		int;

		Set	@IsSupervisor	= (Select i.IsSupervisor FROM inserted as i)
		Set	@IsManager	= (Select i.IsManager FROM inserted as i)
		Set	@Id		= (Select i.ID FROM inserted as i)

		-----------------------------------------------------------------
		-- Call SystemUsers Update (usp_SecurityProfileUpdateSystemUsers)
		-----------------------------------------------------------------
		Begin
			EXEC	[dbo].[usp_SecurityProfileUpdateSystemUsers]	
						@Id,
						@IsSupervisor,
						@IsManager
		End
    END

GO

If @@Error = 0
   Print 'Success: The Alter Trigger Stg_SecurityProfileUpdateUpdate for US85 has been rolled back successfully'
Else
   Print 'Failure: The Alter Trigger Stg_SecurityProfileUpdateUpdate for US85 has not been rolled back'
GO