IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'UserProfileGet') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure UserProfileGet'
	EXEC ('CREATE PROCEDURE dbo.UserProfileGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure UserProfileGet')
GO

ALTER PROCEDURE [dbo].[UserProfileGet]
@Id INT=null
AS
begin
	SET NOCOUNT ON;

	select 
		Id,
		Description,
		PasswordValidFor	as DaysPasswordValid,
		IsSupervisor,
		IsManager,
		IsDeleted
	from
		SecurityProfile
	where
		ProfileType='U'
		and ((@Id is null) or (@Id is not null and ID = @Id))
	
end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure UserProfileGet for US85 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure UserProfileGet for US85 has not been rolled back'
GO

