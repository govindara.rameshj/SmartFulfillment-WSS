IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ExternalRequests]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	PRINT 'Drop table ExternalRequests'
	DROP TABLE [dbo].[ExternalRequests]
END
GO

If @@Error = 0
   Print 'Success: Table ExternalRequests for US33083 has been dropped successfully'
Else
   Print 'Failure: Table ExternalRequests for US33083 has not been dropped successfully'
Go
