DELETE FROM  [dbo].[Parameters] WHERE ParameterID in (4600, 4601)
GO

If @@Error = 0
   Print 'Success: Delete from Table Parameters for US191 has been deployed successfully'
Else
   Print 'Failure: Delete from Table Parameters for US191 has not been deployed successfully'
Go
