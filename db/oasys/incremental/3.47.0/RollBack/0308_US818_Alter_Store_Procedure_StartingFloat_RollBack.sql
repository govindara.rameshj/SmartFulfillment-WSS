IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'StartingFloat') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure StartingFloat'
	EXEC ('CREATE PROCEDURE dbo.StartingFloat AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure StartingFloat')
GO

--"manual check" & un-assigned floats need to maintain its link to the pickup that created it
ALTER procedure [dbo].[StartingFloat]
   @FloatID      int,
   @StartFloatID int output
as
begin
set nocount on
   declare @RelatedBagID int

   set @RelatedBagID = (select RelatedBagID from SafeBags where ID = @FloatID)

   if @RelatedBagID = 0
   begin
      --start of chain
      set @StartFloatID = @FloatID
   end
   else
   begin
      --previous cancelled float exists
      exec StartingFloat @RelatedBagID, @StartFloatID output
   end
end
GO

IF @@Error = 0
   PRINT 'Success: The Alter Stored Procedure StartingFloat for US818 has been deployed successfully'
ELSE
   PRINT 'Failure: The Alter Stored Procedure StartingFloat for US818 has not been deployed'
GO