---------------------------------------------------------------------------------------------------------------------
-- Task 01 - Install Stored Procedure
---------------------------------------------------------------------------------------------------------------------
Print ('Task 01 - Install usp_ITSupport_Housekeeping_General')
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ITSupport_Housekeeping_General]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ITSupport_Housekeeping_General]
GO

CREATE PROCEDURE dbo.usp_ITSupport_Housekeeping_General
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 6th November 2013
-- Description	: System Housekeeping Procedure (General Tables)
-----------------------------------------------------------------------------------
@sp_HKPD Int,
@sp_HKDT Date,
@sp_HKLD Char(11),
@sp_HKOB Date,
@sp_HKOC Date,
@sp_HKOD Date
As
Begin

Declare @Event_Delete Char(1) = '1'
Declare @HKPD Int = @sp_HKPD
Declare @HKDT Date = @sp_HKDT
Declare @HKLD Char(11) = @sp_HKLD
Declare @HKOB Date = @sp_HKOB
Declare @HKOC Date = @sp_HKOC
Declare @HKOD Date = @sp_HKOD
Print ('Housekeeping GENERAL - ' + LTRIM(RTRIM(CAST(@HKPD as Char))) + ':' + LTRIM(RTRIM(CAST(@HKDT as Char))) + ':' + LTRIM(RTRIM(CAST(@HKLD as Char))) + ':' + LTRIM(RTRIM(CAST(@HKOB as Char))) + ':' + LTRIM(RTRIM(CAST(@HKOC as Char))) + ':' + LTRIM(RTRIM(CAST(@HKOD as Char))))
-----------------------------------------------------------------------------------------------------------------
-- Perform Deletes
-----------------------------------------------------------------------------------------------------------------

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'DLFTOT')) 
	Begin
		Delete From DLFTOT Where DATE1 <= @HKLD
	End
Else
	Begin
		Print ('Table DLFTOT D.N.E. - Skipping')
	End

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ActivityLog')) 
	Begin
		Delete From ActivityLog Where LogDate <= @HKDT
	End
Else
	Begin
		Print ('Table ActivityLog D.N.E. - Skipping')
	End
			
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ACTLOG')) 
	Begin
		Delete From ACTLOG Where DATE1 <= @HKDT
	End
Else
	Begin
		Print ('Table ACTLOG D.N.E. - Skipping')
	End		
		
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ACCARD')) 
	Begin
		Delete From ACCARD Where DDEL <= @HKOB and DELC = '1'
	End
Else
	Begin
		Print ('Table ACCARD D.N.E. - Skipping')
	End		
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ACCMAS')) 
	Begin
		Delete From ACCMAS Where DDEL <= @HKOB and DELC = '1'
	End
Else
	Begin
		Print ('Table ACCMAS D.N.E. - Skipping')
	End	
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'AFGCTL')) 
	Begin
		Delete From AFGCTL Where NUMB <> '77'
	End
Else
	Begin
		Print ('Table AFGCTL D.N.E. - Skipping')
	End	

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'AFDCTL')) 
	Begin
		Delete From AFDCTL Where AFGN <> '77'
	End
Else
	Begin
		Print ('Table AFDCTL D.N.E. - Skipping')
	End	
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'AFPCTL')) 
	Begin
		Delete From AFPCTL Where AFGN <> '77'
	End
Else
	Begin
		Print ('Table AFPCTL D.N.E. - Skipping')
	End	

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ALPDES')) 
	Begin
		Delete From ALPDES
	End
Else
	Begin
		Print ('Table ALPDES D.N.E. - Skipping')
	End	
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ALPLOG')) 
	Begin
		Delete From ALPLOG
	End
Else
	Begin
		Print ('Table ALPLOG D.N.E. - Skipping')
	End	

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CASPER')) 
	Begin
		Delete From CASPER Where DATE1 <= @HKOB
	End
Else
	Begin
		Print ('Table CASPER D.N.E. - Skipping')
	End	
			
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CBSCAS')) 
	Begin
		Delete From CBSCAS --Where DATE1 in (Select DATE1 From CBSCTL Where DATE1 <= @HKDT and COMM = '1' and DONE = '1')
	End
Else
	Begin
		Print ('Table CBSCAS D.N.E. - Skipping')
	End	
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CBSCSA')) 
	Begin
		Delete From CBSCSA Where DATE1 <= @HKOB --and DONE = '1'
	End
Else
	Begin
		Print ('Table CBSCSA D.N.E. - Skipping')
	End	
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CBSCTL')) 
	Begin
		Delete From CBSCTL Where DATE1 <= @HKOB --and COMM = '1' and DONE = '1'
	End
Else
	Begin
		Print ('Table CBSCTL D.N.E. - Skipping')
	End	
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'COLCWI')) 
	Begin
		Delete From COLCWI
	End
Else
	Begin
		Print ('Table COLCWI D.N.E. - Skipping')
	End	

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'COLHOT')) 
	Begin
		Delete From COLHOT
	End
Else
	Begin
		Print ('Table COLHOT D.N.E. - Skipping')
	End	

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CONSUM')) 
	Begin
		Delete From CONSUM Where DDAT <= @HKDT-- and IREC = '1'
	End
Else
	Begin
		Print ('Table CONSUM D.N.E. - Skipping')
	End	

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CONDET')) 
	Begin
		Delete From CONDET Where CNUM in (Select CNUM From CONSUM)
	End
Else
	Begin
		Print ('Table CONDET D.N.E. - Skipping')
	End	

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CONMAS')) 
	Begin
		Delete From CONMAS Where EDAT <= @HKDT and DONE = '1'
	End
Else
	Begin
		Print ('Table CONMAS D.N.E. - Skipping')
	End	
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'COUPON')) 
	Begin
		Delete From COUPON Where DATE1 <= @HKDT and DONE = '1' --and COMM = '1' 
	End
Else
	Begin
		Print ('Table COUPON D.N.E. - Skipping')
	End	
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'COUPONMASTER')) 
	Begin
		Delete From COUPONMASTER Where Deleted = '1'
	End
Else
	Begin
		Print ('Table COUPONMASTER D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CUSMAS')) 
	Begin
		Delete From CUSMAS
	End
Else
	Begin
		Print ('Table CUSMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CYHMAS')) 
	Begin
		Delete From CYHMAS Where SKUN not in (Select SKUN From STKMAS)
	End
Else
	Begin
		Print ('Table CYHMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'DHLMAS')) 
	Begin
		Delete From DHLMAS Where DATE1 <= @HKDT
	End
Else
	Begin
		Print ('Table DHLMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'GapWalk')) 
	Begin
		Delete From GapWalk Where DateCreated <= @HKOD
	End
Else
	Begin
		Print ('Table GapWalk D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'HHTDET')) 
	Begin
		Delete From HHTDET Where DATE1 <= @HKDT
	End
Else
	Begin
		Print ('Table HHTDET D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'HHTHDR')) 
	Begin
		Delete From HHTHDR Where DATE1 <= @HKDT
	End
Else
	Begin
		Print ('Table HHTHDR D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ISULINEHistory')) 
	Begin
		Delete From ISULINEHistory Where DATEADDED <= @HKOB
	End
Else
	Begin
		Print ('Table ISULINEHistory D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ISUHDRHistory')) 
	Begin
		Delete From ISUHDRHistory Where DATEADDED <= @HKOB
	End
Else
	Begin
		Print ('Table ISUHDRHistory D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'LogSOM_InvalidXml')) 
	Begin
		Delete From LogSOM_InvalidXml Where DateLogged <= @HKOD
	End
Else
	Begin
		Print ('Table LogSOM_InvalidXml D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'LogSOM_InvalidXmlDetail')) 
	Begin
		Delete From LogSOM_InvalidXmlDetail Where LogID not in (Select ID From LogSOM_InvalidXml)
	End
Else
	Begin
		Print ('Table LogSOM_InvalidXmlDetail D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'MODDET')) 
	Begin
		Delete From MODDET
	End
Else
	Begin
		Print ('Table MODDET D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'MODMAS')) 
	Begin
		Delete From MODMAS
	End
Else
	Begin
		Print ('Table MODMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PRCCHG')) 
	Begin
		Delete From PRCCHG Where PDAT <= @HKOB and PSTA <> 'U'
	End
Else
	Begin
		Print ('Table PRCCHG D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PSSCOP')) 
	Begin
		Delete From PSSCOP
	End
Else
	Begin
		Print ('Table PSSCOP D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PSSDCF')) 
	Begin
		Delete From PSSDCF
	End
Else
	Begin
		Print ('Table PSSDCF D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PSSINT')) 
	Begin
		Delete From PSSINT
	End
Else
	Begin
		Print ('Table PSSINT D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PSSMSG')) 
	Begin
		Delete From PSSMSG
	End
Else
	Begin
		Print ('Table PSSMSG D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PSSOPT')) 
	Begin
		Delete From PSSOPT
	End
Else
	Begin
		Print ('Table PSSOPT D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PSSRNG')) 
	Begin
		Delete From PSSRNG
	End
Else
	Begin
		Print ('Table PSSRNG D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PSSSTY')) 
	Begin
		Delete From PSSSTY
	End
Else
	Begin
		Print ('Table PSSSTY D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PVLINE')) 
	Begin
		Delete From PVLINE
	End
Else
	Begin
		Print ('Table PVLINE D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PVPAID')) 
	Begin
		Delete From PVPAID
	End
Else
	Begin
		Print ('Table PVPAID D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PVTOTS')) 
	Begin
		Delete From PVTOTS
	End
Else
	Begin
		Print ('Table PVTOTS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'RMPMAS')) 
	Begin
		Delete From RMPMAS
	End
Else
	Begin
		Print ('Table RMPMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'TMPFIL')) 
	Begin
		Delete From TMPFIL Where FKEY not like ('WXITSP_%')
	End
Else
	Begin
		Print ('Table TMPFIL D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PICAUD')) 
	Begin
		Delete From PICAUD Where DATE1 <= @HKOC
	End
Else
	Begin
		Print ('Table PICAUD D.N.E. - Skipping')
	End

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'DRLSUM')) 
	Begin
		Delete From DRLSUM Where DATE1 <= @HKDT and COMM = '1' and RTI = 'C'
	End
Else
	Begin
		Print ('Table DRLSUM D.N.E. - Skipping')
	End

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'DRLDET')) 
	Begin
		Delete From DRLDET Where NUMB not in (Select NUMB From DRLSUM)
	End
Else
	Begin
		Print ('Table DRLDET D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CARD_LIST')) 
	Begin
		Delete From CARD_LIST Where DELETED = '1'
	End
Else
	Begin
		Print ('Table CARD_LIST D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CARD_SCHEME')) 
	Begin
		Delete From CARD_SCHEME Where DELETED = '1'
	End
Else
	Begin
		Print ('Table CARD_SCHEME D.N.E. - Skipping')
	End

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'HhtLocation')) 
	Begin
		Delete From HhtLocation where DateCreated <= @HKDT
	End
Else
	Begin
		Print ('Table HhtLocation D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'HTRSUM')) 
	Begin
		Delete From HTRSUM
	End
Else
	Begin
		Print ('Table HTRSUM D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ITEMPROMPTS')) 
	Begin
		Delete From ITEMPROMPTS Where DateDeleted <= @HKOC
	End
Else
	Begin
		Print ('Table ITEMPROMPTS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'MDHMAS')) 
	Begin
		Delete From MDHMAS
	End
Else
	Begin
		Print ('Table MDHMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'MSGCTL')) 
	Begin
		Delete From MSGCTL
	End
Else
	Begin
		Print ('Table MSGCTL D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'MSGTXT')) 
	Begin
		Delete From MSGTXT
	End
Else
	Begin
		Print ('Table MSGTXT D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'NITLOG')) 
	Begin
		Delete From NITLOG Where DATE1 <= @HKOC
	End
Else
	Begin
		Print ('Table NITLOG D.N.E. - Skipping')
	End
	
--Delete From PCTTOS

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PendingIbtIn')) 
	Begin
		Delete From PendingIbtIn
	End
Else
	Begin
		Print ('Table PendingIbtIn D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PendingIbtOut')) 
	Begin
		Delete From PendingIbtOut
	End
Else
	Begin
		Print ('Table PendingIbtOut D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PicExternalAudit')) 
	Begin
		Delete From PicExternalAudit Where CountDate <= @HKOC
	End
Else
	Begin
		Print ('Table PicExternalAudit D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PIMMAS')) 
	Begin
		Delete From PIMMAS
	End
Else
	Begin
		Print ('Table PIMMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PLANGRAM')) 
	Begin
		Delete From PLANGRAM Where DELE = '1'
	End
Else
	Begin
		Print ('Table PLANGRAM D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PlanNumbers')) 
	Begin
		Delete From PlanNumbers Where IsActive = '0'
	End
Else
	Begin
		Print ('Table PlanNumbers D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PlanSegments')) 
	Begin
		Delete From PlanSegments Where IsActive = '0'
	End
Else
	Begin
		Print ('Table PlanSegments D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PlanStocks')) 
	Begin
		Delete From PlanStocks Where IsActive = '0'
	End
Else
	Begin
		Print ('Table PlanStocks D.N.E. - Skipping')
	End
	
--Delete From PROMPTS

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PURHDR')) 
	Begin
		Delete From PURHDR Where (RCOM = '1' and DDAT <= @HKDT) or (DELM = '1' and ODAT <= @HKDT)
	End
Else
	Begin
		Print ('Table PURHDR D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'PURLIN')) 
	Begin
		Delete From PURLIN Where HKEY not in (Select TKEY From PURHDR)
	End
Else
	Begin
		Print ('Table PURLIN D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'QUOHDR')) 
	Begin
		Delete From QUOHDR Where DATE1 <= @HKDT
	End
Else
	Begin
		Print ('Table QUOHDR D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'QUOLIN')) 
	Begin
		Delete From QUOLIN Where NUMB not in (Select NUMB From QUOLIN)
	End
Else
	Begin
		Print ('Table QUOLIN D.N.E. - Skipping')
	End

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'RELITM')) 
	Begin
		Delete From RELITM Where SPOS not in (Select SKUN From STKMAS)
	End
Else
	Begin
		Print ('Table RELITM D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'RETHDR')) 
	Begin
		Delete From RETHDR Where RDAT <= @HKDT
	End
Else
	Begin
		Print ('Table RETHDR D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'RETLIN')) 
	Begin
		Delete From RETLIN Where HKEY not in (Select TKEY From RETHDR)
	End
Else
	Begin
		Print ('Table RETLIN D.N.E. - Skipping')
	End

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'RSFLAS')) 
	Begin
		Delete From RSFLAS
	End
Else
	Begin
		Print ('Table RSFLAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'RSTILL')) 
	Begin
		Delete From RSTILL
	End
Else
	Begin
		Print ('Table RSTILL D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'SOQSKU')) 
	Begin
		Delete From SOQSKU Where SKUN not in (Select SKUN From STKMAS)
	End
Else
	Begin
		Print ('Table SOQSKU D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'STKHIR')) 
	Begin
		Delete From STKHIR Where IDEL = '1'
	End
Else
	Begin
		Print ('Table STKHIR D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'StockMovement')) 
	Begin
		Delete From StockMovement Where DataDate <= @HKOB
	End
Else
	Begin
		Print ('Table StockMovement D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'StoreSaleWeight')) 
	Begin
		Delete From StoreSaleWeight Where DateActive <= @HKOB
	End
Else
	Begin
		Print ('Table StoreSaleWeight D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'SUPMAS')) 
	Begin
		Delete From SUPMAS Where DELC = '1'
	End
Else
	Begin
		Print ('Table SUPMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'SUPDET')) 
	Begin
		Delete From SUPDET Where SUPN not in (Select SUPN From SUPMAS)
	End
Else
	Begin
		Print ('Table SUPDET D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'SUPNOT')) 
	Begin
		Delete From SUPNOT Where SUPN not in (Select SUPN From SUPMAS)
	End
Else
	Begin
		Print ('Table SUPNOT D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'WSCONFIG')) 
	Begin
		Delete From WSCONFIG
	End
Else
	Begin
		Print ('Table WSCONFIG D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'EFTCDF')) 
	Begin
		Delete From EFTCDF
	End
Else
	Begin
		Print ('Table EFTCDF D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'EFTLOG')) 
	Begin
		Delete From EFTLOG
	End
Else
	Begin
		Print ('Table EFTLOG D.N.E. - Skipping')
	End

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'SystemUsers')) 
	Begin
		Delete From SystemUsers Where DeletedDate < @HKDT
	End
Else
	Begin
		Print ('Table SystemUsers D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CASMAS')) 
	Begin
		Delete From CASMAS Where CASH not in (Select EmployeeCode From SystemUsers)
	End
Else
	Begin
		Print ('Table CASMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CASPER')) 
	Begin
		Delete From CASPER Where CASH not in (Select EmployeeCode From SystemUsers)
	End
Else
	Begin
		Print ('Table CASPER D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'SYSPAS')) 
	Begin
		Delete From SYSPAS Where EEID not in (Select EmployeeCode From SystemUsers)
	End
Else
	Begin
		Print ('Table SYSPAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'RSCASH')) 
	Begin
		Delete From RSCASH Where CASH not in (Select EmployeeCode From SystemUsers)
	End
Else
	Begin
		Print ('Table RSCASH D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'RSTILL')) 
	Begin
		Delete From RSTILL Where CASH not in (Select EmployeeCode From SystemUsers)
	End
Else
	Begin
		Print ('Table RSTILL D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'EXRATE')) 
	Begin
		Delete From EXRATE
	End
Else
	Begin
		Print ('Table EXRATE D.N.E. - Skipping')
	End
	
--- STKMAS Deletion Routine ---

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'AUDMAS')) 
	Begin
		Delete From AUDMAS Where SKUN not in (Select SKUN From STKMAS)
	End
Else
	Begin
		Print ('Table AUDMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'AuditMaster')) 
	Begin
		Delete From AuditMaster Where AuditDate < @HKOC
	End
Else
	Begin
		Print ('Table AuditMaster D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CYHMAS')) 
	Begin
		Delete From CYHMAS Where SKUN not in (Select SKUN From STKMAS)
	End
Else
	Begin
		Print ('Table CYHMAS D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'STKADJ')) 
	Begin
		Delete From STKADJ Where DATE1 < @HKDT --or SKUN not in (Select SKUN From STKMAS)
	End
Else
	Begin
		Print ('Table STKADJ D.N.E. - Skipping')
	End
	
/*
Delete From STKLOG Where DATE1 < @HKDT --or SKUN not in (Select SKUN From STKMAS)
Delete From STKWMV Where WEDT < @HKDT --or SKUN not in (Select SKUN From STKMAS)
*/

If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'STKTXT')) 
	Begin
		Delete From STKTXT Where SKUN not in (Select SKUN From STKMAS)
	End
Else
	Begin
		Print ('Table STKTXT D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'EANMAS')) 
	Begin
		Delete From EANMAS Where SKUN not in (Select SKUN From STKMAS)
	End
Else
	Begin
		Print ('Table EANMAS D.N.E. - Skipping')
	End
	
--Select TOP 1 (SUBSTRING(TEXT,4, 8)) From GIFHOT


If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'VisionLine')) 
	Begin
		Delete From VisionLine Where TranDate <= @HKDT
	End
Else
	Begin
		Print ('Table VisionLine D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'VisionPayment')) 
	Begin
		Delete From VisionPayment Where TranDate <= @HKDT
	End
Else
	Begin
		Print ('Table VisionPayment D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'VisionTotal')) 
	Begin
		Delete From VisionTotal Where TranDate <= @HKDT
	End
Else
	Begin
		Print ('Table VisionTotal D.N.E. - Skipping')
	End
	
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'Alert')) 
	Begin
		Delete From Alert Where ReceivedDate <= @HKDT
	End
Else
	Begin
		Print ('Table Alert D.N.E. - Skipping')
	End
	
	
If (@Event_Delete = '1') 
	Begin
		Delete From EVTCHG Where IDEL = '1'
		Delete From EVTDLG Where IDEL = '1'
		Delete From EVTHDR Where IDEL = '1'
		Delete From EVTHEX Where IDEL = '1'
		Delete From EVTMAS Where IDEL = '1'
		Delete From EVTMMG Where IDEL = '1'
	End
Else
	Begin
		Print ('Event Information D.E.P. - Skipping')
	End
----------------------------------------------------------------------------------------------------------
-- Remove Odd Development / Support Fixes 
----------------------------------------------------------------------------------------------------------
--Declare @Database sysname = 'Oasys'
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'COLCWI')) Begin Drop Table Oasys.dbo.COLCWI End Else Begin Print('No COLCWI') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'COLHOT')) Begin Drop Table Oasys.dbo.COLHOT End Else Begin Print('No COLHOT') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CashBalCashier_20120921_RecoveryData')) Begin Drop Table Oasys.dbo.CashBalCashier_20120921_RecoveryData End Else Begin Print('No CBC21') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CashBalCashier_20120927_RecoveryData')) Begin Drop Table Oasys.dbo.CashBalCashier_20120927_RecoveryData End Else Begin Print('No CBC27') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CashBalCashier_20120928_RecoveryData')) Begin Drop Table Oasys.dbo.CashBalCashier_20120928_RecoveryData End Else Begin Print('No CBC28') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CashBalCashier_20120929_RecoveryData')) Begin Drop Table Oasys.dbo.CashBalCashier_20120929_RecoveryData End Else Begin Print('No CBC29') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CashBalCashierTen_20120921_RecoveryData')) Begin Drop Table Oasys.dbo.CashBalCashierTen_20120921_RecoveryData End Else Begin Print('No CBCT21') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CashBalCashierTen_20120927_RecoveryData')) Begin Drop Table Oasys.dbo.CashBalCashierTen_20120927_RecoveryData End Else Begin Print('No CBCT27') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CashBalCashierTen_20120928_RecoveryData')) Begin Drop Table Oasys.dbo.CashBalCashierTen_20120928_RecoveryData End Else Begin Print('No CBCT28') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'CashBalCashierTen_20120929_RecoveryData')) Begin Drop Table Oasys.dbo.CashBalCashierTen_20120929_RecoveryData End Else Begin Print('No CBCT29') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'MenuConfigBackupBeforeNewSecurityModelImplemented')) Begin Drop Table Oasys.dbo.MenuConfigBackupBeforeNewSecurityModelImplemented End Else Begin Print('No MC') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'MenuConfigBackupBeforeP022037')) Begin Drop Table Oasys.dbo.MenuConfigBackupBeforeP022037 End Else Begin Print('No MC') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ProfilemenuAccessBackupBeforeNewSecurityModelImplemented')) Begin Drop Table Oasys.dbo.ProfilemenuAccessBackupBeforeNewSecurityModelImplemented End Else Begin Print('No PMAC') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'ProfilemenuAccessBackupBeforeP022037')) Begin Drop Table Oasys.dbo.ProfilemenuAccessBackupBeforeP022037 End Else Begin Print('No PMAC') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'SecurityProfileBackupBeforeNewSecurityModelImplemented')) Begin Drop Table Oasys.dbo.SecurityProfileBackupBeforeNewSecurityModelImplemented End Else Begin Print('No SM') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'StkmasBackup20100915')) Begin Drop Table Oasys.dbo.StkmasBackup20100915 End Else Begin Print('No STKBU') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'SystemUsersBackupBeforeNewSecurityModelImplemented')) Begin Drop Table Oasys.dbo.SystemUsersBackupBeforeNewSecurityModelImplemented End Else Begin Print('No SU') End
If (Exists (Select * From Information_Schema.Tables Where Table_Catalog = 'Oasys' and Table_Name = 'Coverage')) Begin Drop Table Oasys.dbo.Coverage End Else Begin Print('No Coverage') End

End
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure usp_ITSupport_Housekeeping_General for US196 has been rolled back successfully'
Else
   Print 'Failure: The Create Stored Procedure usp_ITSupport_Housekeeping_General for US196 has not been rolled back'
GO