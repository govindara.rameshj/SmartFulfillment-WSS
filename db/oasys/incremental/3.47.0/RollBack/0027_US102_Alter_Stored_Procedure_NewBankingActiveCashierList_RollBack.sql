IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'NewBankingActiveCashierList') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure NewBankingActiveCashierList'
	EXEC ('CREATE PROCEDURE dbo.NewBankingActiveCashierList AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingActiveCashierList')
GO

ALTER procedure [dbo].[NewBankingActiveCashierList]
   @PeriodID  int,
   @CashierID int
as
begin
set nocount on

select CashierID           = a.CashierID,
       CashierUserName     = b.Name,
       CashierEmployeeCode = EmployeeCode,
       SystemSales         = a.GrossSalesAmount,
       TotalPickups        = (select sum(Value)                      --total pickup (cash drops & e.o.d pickup)
                              from SafeBags
                              where [Type]           = 'P'
                              and   [State]         <> 'C'
                              and   PickupPeriodID   = @PeriodID
                              and   AccountabilityID = a.CashierID),
       StartFloatValue     = (select sum(Value)
                              from SafeBags
                              where [Type]         = 'F'
                              and   [State]        = 'R'
                              and OutPeriodID      = @PeriodID
                              and AccountabilityID = a.CashierID),
       NewFloatValue       = (select sum(Value)
                              from SafeBags
                              where ID in (select RelatedBagId
                                           from SafeBags
                                           where [Type]           = 'P'
                                           and   [State]         <> 'C'
                                           and   PickupPeriodID   = @PeriodID
                                           and   AccountabilityID = a.CashierID
                                           and   RelatedBagId    <> 0))
from CashBalCashier a
inner join SystemUsers b
      on b.ID = a.CashierID
where a.PeriodID          = @PeriodID 
and   a.CurrencyID        = (select ID from SystemCurrency where IsDefault = 1)
and   a.NumTransactions   > 0
--no valid sales e.g invalid "vision deposit"
and  (a.GrossSalesAmount <> 0 or (select count(*)
                                  from CashBalCashierTen
                                  where PeriodID   = @PeriodID 
                                  and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                  and   CashierID  = a.CashierID) > 0)
and  (@CashierID is null or a.CashierID = @CashierID)
end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingActiveCashierList for US102 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingActiveCashierList for US102 has not been rolled back'
GO