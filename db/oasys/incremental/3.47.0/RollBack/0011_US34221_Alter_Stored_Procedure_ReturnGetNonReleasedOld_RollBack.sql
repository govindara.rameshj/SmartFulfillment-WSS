IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'ReturnGetNonReleasedOld') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure ReturnGetNonReleasedOld'
	EXEC ('CREATE PROCEDURE dbo.ReturnGetNonReleasedOld AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure ReturnGetNonReleasedOld')
GO

ALTER PROCEDURE [dbo].[ReturnGetNonReleasedOld]

AS
begin
	select		
		rh.TKEY,
		rh.NUMB,
		rh.SUPP,
		sm.NAME as SupplierName,
		rh.EDAT,
		rh.RDAT,
		rh.PONO,
		rh.DRLN,
		rh.VALU, 
		rh.EPRT, 
		rh.RPRT,
		rh.IsDeleted,
		rh.RTI
	from		
		RETHDR rh
	inner join	
		SUPMAS sm on rh.SUPP = sm.SUPN
	where		
		rh.DRLN = '000000'
		and	rh.isdeleted = 0
	order by	
		sm.NAME, 
		rh.NUMB

end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ReturnGetNonReleasedOld for US34221 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure ReturnGetNonReleasedOld for US34221 has not been rolled back'
GO
