IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'NewBankingBankingValidation') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure NewBankingBankingValidation'
	EXEC ('CREATE PROCEDURE dbo.NewBankingBankingValidation AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingBankingValidation')
GO

-- =============================================
-- Author      : Partha Dutta
-- Create Date : 05/01/2011
-- Referral No : 659
-- Notes       : Banking
--               Certain TILL or Vision Import transaction(s) are invalid and need to be ignored
--               
--               Entry Of Banking Screen       - NewBankingBankingValidation
--               Assign Float Screen           - NewBankingFloatList
--               Floated Pickup Screen         - NewBankingFloatedPickupList
--               UnFloated Pickup Screen       - NewBankingUnFloatedPickupList
--               Banking ReCheck Pickup Screen - NewBankingActiveCashierList





--               When a zero pickup where all tender values are zero is returned to the safe



-- =============================================

-----------------------------------------------------
--                 MAIN SCREEN                     --
-----------------------------------------------------

ALTER procedure [dbo].[NewBankingBankingValidation]
   @PeriodID int
as
begin
set nocount on

declare @FloatedCashierNoSales            int
declare @FloatedCashierSalesNoPickupBag   int
declare @UnFloatedCashierSalesNoPickupBag int

--floated cashiers with no sales
set @FloatedCashierNoSales = (select count(*)
                              from SafeBags a
                              inner join CashBalCashier b
                                    on  b.PeriodID   = @PeriodID
                                    and b.CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                    and b.CashierID  = a.AccountabilityID 
                              left outer join (select * from SafeBags where Type = 'P' and [State] <> 'C') c
                                         on  c.PickupPeriodID   = a.OutPeriodID
                                         and c.AccountabilityID = a.AccountabilityID
                              where    a.[Type]           = 'F'
                              and      a.[State]          = 'R'
                              and      a.OutPeriodID      = @PeriodID
                            --and   b.NumTransactions  = 0

                              --no valid sales e.g invalid "vision deposit"
                              and not (b.NumTransactions  <> 0 and (b.GrossSalesAmount <> 0 or (select count(*)
                                                                                                from CashBalCashierTen
                                                                                                where PeriodID   = @PeriodID 
                                                                                                and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                                                                                and   CashierID  = a.AccountabilityID) > 0))
                              and   c.ID is null)
--floated cashiers with sales and no pickup (exclude cash drops)
set @FloatedCashierSalesNoPickupBag = (select count(*)
                                       from SafeBags a
                                       inner join CashBalCashier b
                                             on  b.PeriodID   = @PeriodID 
                                             and b.CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                             and b.CashierID  = a.AccountabilityID
                                       --end of day pickup for this cashier and period
                                       left outer join (select *
                                                        from SafeBags
                                                        where [Type]            = 'P'
                                                        and   [State]          <> 'C'
                                                        and isnull(CashDrop, 0) = 0) c
                                                  on  c.PickupPeriodID   = a.OutPeriodID
                                                  and c.AccountabilityID = a.AccountabilityID
                                       where a.[Type]            = 'F'
                                       and   a.[State]           = 'R'
                                       and   a.OutPeriodID       = @PeriodID
                                     --and   b.NumTransactions  <> 0

                                     --no valid sales e.g invalid "vision deposit"
                                       and   b.NumTransactions  <> 0
                                       and  (b.GrossSalesAmount <> 0 or (select count(*)
                                                                         from CashBalCashierTen
                                                                         where PeriodID   = @PeriodID 
                                                                         and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                                                         and   CashierID  = a.AccountabilityID) > 0)
                                       and   c.ID is null)
--unfloated cashiers with sales and no pickup (exclude cash drops)
set @UnFloatedCashierSalesNoPickupBag = (select count(*)
                                         from CashBalCashier a
                                         --float for this cashier and period
                                         left outer join (select * from SafeBags where Type = 'F' and [State] = 'R' and OutPeriodID = @PeriodID) b
                                                    on  b.AccountabilityID = a.CashierID
                                         --end of day pickup for this cashier and period
                                         left outer join (select *
                                                          from SafeBags
                                                          where [Type]            = 'P'
                                                          and   [State]          <> 'C'
                                                          and PickupPeriodID      = @PeriodID
                                                          and isnull(CashDrop, 0) = 0) c
                                                    on  c.AccountabilityID = a.CashierID
                                         where a.PeriodID          = @PeriodID
                                         and   a.CurrencyID        = (select ID from SystemCurrency where IsDefault = 1)
                                       --and   a.NumTransactions  <> 0

                                         --no valid sales e.g invalid "vision deposit"
                                         and   a.NumTransactions  <> 0
                                         and  (a.GrossSalesAmount <> 0 or (select count(*)
                                                                           from CashBalCashierTen
                                                                           where PeriodID   = @PeriodID
                                                                           and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                                                           and   CashierID  = a.CashierID) > 0)
                                         and   b.ID is null
                                         and   c.ID is null)

select FloatedCashierNoSales            = cast((case @FloatedCashierNoSales
                                                   when 0 then 0
                                                   else 1
                                                 end) as bit),
       FloatedCashierSalesNoPickupBag   = cast((case @FloatedCashierSalesNoPickupBag
                                                    when 0 then 0
                                                    else 1
                                                 end) as bit),
       UnFloatedCashierSalesNoPickupBag = cast((case @UnFloatedCashierSalesNoPickupBag
                                                    when 0 then 0
                                                    else 1
                                                 end) as bit)
end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingBankingValidation for US102 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingBankingValidation for US102 has not been rolled back'
GO