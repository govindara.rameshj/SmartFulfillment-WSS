IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'SecurityProfileGetAll') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure SecurityProfileGetAll'
	EXEC ('CREATE PROCEDURE dbo.SecurityProfileGetAll AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SecurityProfileGetAll')
GO

ALTER PROCEDURE [dbo].[SecurityProfileGetAll]
@ProfileType CHAR (1)=null
AS
begin
	SET NOCOUNT ON;

	select 
		ID					as Id,
		ProfileType,
		Description,
		Position,
		IsSupervisor,
		IsManager,
		IsAreaManager,
		PasswordValidFor	as DaysPasswordValid,
		IsDeleted,
		DeletedBy,
		DeletedDate			as DateDeleted,
		DateLastEdited,
		IsSystemAdmin,
		IsHelpDeskUser
	from
		SecurityProfile
	where
	 	((@ProfileType is null) or  (@ProfileType is not null and ProfileType=@ProfileType))
		
end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SecurityProfileGetAll for US85 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure SecurityProfileGetAll for US85 has not been rolled back'
GO
