DELETE FROM dbo.Parameters
WHERE ParameterID = 958

If @@Error = 0
   Print 'Success: Delete from table Parameters for US1648 has been deployed successfully'
Else
   Print 'Failure: Delete from table Parameters for US1648 has not been deployed successfully'
GO