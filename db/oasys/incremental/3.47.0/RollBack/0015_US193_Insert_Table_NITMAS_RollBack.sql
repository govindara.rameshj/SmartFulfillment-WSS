If exists (select 1 from [dbo].[NITMAS] where [TASK] = 139)
Delete from [dbo].[NITMAS] where [TASK] = 139
GO

If @@Error = 0
   Print 'Success: Delete from Table [NITMAS] for US193 has been deployed successfully'
Else
   Print 'Failure: Delete from Table [NITMAS] for US193 has not been deployed successfully'
Go

