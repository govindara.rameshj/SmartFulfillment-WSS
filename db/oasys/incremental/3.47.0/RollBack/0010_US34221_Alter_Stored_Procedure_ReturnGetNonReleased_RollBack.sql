IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'ReturnGetNonReleased') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure ReturnGetNonReleased'
	EXEC ('CREATE PROCEDURE dbo.ReturnGetNonReleased AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure ReturnGetNonReleased')
GO

ALTER PROCEDURE [dbo].[ReturnGetNonReleased]

AS
begin
	select		
		rh.TKEY		as Id,
		rh.NUMB		as Number,
		rh.SUPP		as SupplierNumber,
		sm.NAME		as SupplierName,
		rh.EDAT		as DateCreated,
		rh.RDAT		as DateCollected,
		rh.DRLN		as ReceiptNumber,
		rh.VALU		as Value, 
		rh.EPRT		as NotPrintedEntry, 
		rh.RPRT		as NotPrintedRelease,
		rh.IsDeleted
	from		
		RETHDR rh
	inner join	
		SUPMAS sm on rh.SUPP = sm.SUPN
	where		
		(rh.DRLN = null or rh.DRLN='000000')
		and	rh.isdeleted = 0
	order by	
		sm.NAME, 
		rh.NUMB

end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ReturnGetNonReleased for US34221 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure ReturnGetNonReleased for US34221 has not been rolled back'
GO