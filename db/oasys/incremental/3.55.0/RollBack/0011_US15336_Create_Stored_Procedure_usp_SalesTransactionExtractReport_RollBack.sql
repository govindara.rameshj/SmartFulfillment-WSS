IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SalesTransactionExtractReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SalesTransactionExtractReport]
GO

If @@Error = 0
   Print 'Success: The Drop of Stored Procedure usp_SalesTransactionExtractReport for US15336 has been deployed successfully'
Else
   Print 'Failure: The Drop of Stored Procedure usp_SalesTransactionExtractReport for US15336 has not been deployed'
GO