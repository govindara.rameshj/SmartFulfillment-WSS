IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('STKMAS') and name = 'DateLastSold')
    BEGIN
        PRINT 'Drop DateLastSold column from dbo.STKMAS'
        ALTER TABLE dbo.STKMAS
        DROP COLUMN [DateLastSold]
    END
GO

If @@Error = 0
   Print 'Success: The Alter dbo.STKMAS table for US17409 has been deployed successfully'
Else
   Print 'Failure: The Alter dbo.STKMAS table for US17409 has not been deployed'
GO