IF EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 169)
    BEGIN
        DELETE FROM dbo.[Parameters] WHERE ParameterId = 169
    END
GO

If @@Error = 0
   Print 'Success: The Delete from Parameters table for Tech has been deployed successfully'
Else
   Print 'Failure: The Delete from Parameters table for Tech has not been deployed'
GO