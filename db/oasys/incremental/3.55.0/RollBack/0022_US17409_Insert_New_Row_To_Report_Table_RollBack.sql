IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportColumns]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DELETE FROM [dbo].[ReportColumns] WHERE [ReportId] = 203 AND [TableId] = 1 AND [ColumnId] = 2510
GO

If @@Error = 0
   Print 'Success: Insert row to [dbo].[ReportColumns] for US17409 has been deployed successfully'
Else
   Print 'Failure: Insert row to [dbo].[ReportColumns] for US17409 has not been deployed successfully'
Go