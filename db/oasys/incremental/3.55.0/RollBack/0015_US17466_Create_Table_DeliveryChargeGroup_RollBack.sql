IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeliveryChargeGroup]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    DROP TABLE [dbo].[DeliveryChargeGroup]
END
GO

If @@Error = 0
   Print 'Success: Drop Table DeliveryChargeGroup for US17466 has been successfully dropped'
Else
   Print 'Failure: Drop Table DeliveryChargeGroup for US17466 has not been successfully dropped'
GO