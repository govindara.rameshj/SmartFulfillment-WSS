IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OrderDataExtractReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_OrderDataExtractReport]
GO

If @@Error = 0
   Print 'Success: The Drop of Stored Procedure usp_OrderDataExtractReport for US15356 has been deployed successfully'
Else
   Print 'Failure: The Drop of Stored Procedure usp_OrderDataExtractReport for US15356 has not been deployed'
GO