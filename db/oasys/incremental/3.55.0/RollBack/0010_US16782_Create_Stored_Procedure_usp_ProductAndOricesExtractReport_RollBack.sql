IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ProductsAndPricesExtractReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].usp_ProductsAndPricesExtractReport
GO

If @@Error = 0
   Print 'Success: The Drop of Stored Procedure usp_ProductsAndPricesExtractReport for US16782 has been deployed successfully'
Else
   Print 'Failure: The Drop of Stored Procedure usp_ProductsAndPricesExtractReport for US16782 has not been deployed'
GO