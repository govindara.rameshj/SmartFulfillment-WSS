IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[GapWalkGetItems]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure [GapWalkGetItems]'
    EXEC ('CREATE PROCEDURE dbo.[GapWalkGetItems] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

ALTER PROCEDURE [dbo].[GapWalkGetItems]
    @Date date
AS
BEGIN
    SET NOCOUNT ON;

    select
        gw.SkuNumber,
        sk.DESCR                        as 'Description',
        sk.ONHA                         as 'QtyOnHand',
        sk.MDNQ                         as 'QtyMarkdown',
        gw.QuantityRequired             as 'QtyRequired',
        ''                              as 'QtyPick',
        sk.DREC                         as 'DateLastReceived',
        ''                              as 'Comment',
        (select case
            when sk.inon=1 then 'Non Stock Products'
            when sk.NOOR=1 then 'Non Orderable Stock'
            when sk.IOBS=1 then 'Obsolete/ Deleted Items'
            when sk.IDEL=1 then 'Obsolete/ Deleted Items'
            else 'Stock - No Restrictions'
        end)                            as 'Status'
    from
        GapWalk gw
    inner join
        STKMAS sk on sk.SKUN=gw.SkuNumber
    where
        gw.DateCreated = @Date

END

go
If @@Error = 0
   Print 'Success: The Alter Stored Procedure GapWalkGetItems for US17409 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure GapWalkGetItems for US17409 has not been deployed'
GO