IF NOT EXISTS (SELECT 1 FROM dbo.NITMAS WHERE TASK = 435)
    BEGIN
        INSERT [dbo].[NITMAS] ([NSET], [TASK], [DESCR], [PROG], [NITE], [RETY], [WEEK], [PEND], [LIVE], [OPTN], [ABOR], [JRUN], [DAYS1], [DAYS2], [DAYS3], [DAYS4], [DAYS5], [DAYS6], [DAYS7], [BEGD], [ENDD], [SDAT], [STIM], [EDAT], [ETIM], [ADAT], [ATIM], [TTYPE]) VALUES (N'1', N'435', N'Parked Transactions Listing', N'ParkedTransactionsListing.dll,ParkedTransactionsListing.ParkedTransactionsListing', 1, 1, 0, 0, 1, NULL, 1, NULL, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.NITMAS WHERE TASK = 485)
    BEGIN
        INSERT [dbo].[NITMAS] ([NSET], [TASK], [DESCR], [PROG], [NITE], [RETY], [WEEK], [PEND], [LIVE], [OPTN], [ABOR], [JRUN], [DAYS1], [DAYS2], [DAYS3], [DAYS4], [DAYS5], [DAYS6], [DAYS7], [BEGD], [ENDD], [SDAT], [STIM], [EDAT], [ETIM], [ADAT], [ATIM], [TTYPE]) VALUES (N'1', N'485', N'Daily Vouchers Report', N'Reporting.dll,Reporting.ReportViewer', 1, 1, 0, 0, 1, NULL, 1, N'300', 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Table dbo.NITMAS for US17616 has been deployed successfully'
Else
   Print 'Failure: The Insert into Table dbo.NITMAS for US17616 has not been deployed'
GO