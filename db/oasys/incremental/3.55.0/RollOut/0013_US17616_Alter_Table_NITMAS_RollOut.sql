IF EXISTS (SELECT 1 FROM dbo.NITMAS WHERE TASK = 435)
    BEGIN
        DELETE FROM dbo.NITMAS WHERE TASK = 435
    END
GO

IF EXISTS (SELECT 1 FROM dbo.NITMAS WHERE TASK = 485)
    BEGIN
        DELETE FROM dbo.NITMAS WHERE TASK = 485
    END
GO

If @@Error = 0
   Print 'Success: The Delete from Table dbo.NITMAS for US17616 has been deployed successfully'
Else
   Print 'Failure: The Delete from Table dbo.NITMAS for US17616 has not been deployed'
GO