IF NOT EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 169)
    BEGIN
        INSERT INTO dbo.Parameters (ParameterID, [Description],	StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES
        (169,'BOReceiver Delivery Sku Number', '805000', 0, 0, 0.00000, 0)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Parameters table for Tech has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for Tech has not been deployed'
GO