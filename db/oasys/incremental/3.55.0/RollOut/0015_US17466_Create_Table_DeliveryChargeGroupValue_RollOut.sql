IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeliveryChargeGroupValue]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    PRINT 'Creating table DeliveryChargeGroupValue'

    CREATE TABLE [dbo].DeliveryChargeGroupValue
    (
        Id int NOT NULL,
        [Group] char(1) NOT NULL,
        Value decimal(9,2) NOT NULL UNIQUE,
        CONSTRAINT FK_DeliveryChargeGroupValue_DeliveryChargeGroup FOREIGN KEY (Id, [Group]) REFERENCES DeliveryChargeGroup(Id, [Group])
    ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[DeliveryChargeGroupValue] WHERE Id = 1 AND [Group] = 'A' AND Value = 7.95) 
BEGIN
	INSERT INTO [dbo].[DeliveryChargeGroupValue] (Id, [Group], Value)
	VALUES
		(1, 'A', 7.95)		
END
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[DeliveryChargeGroupValue] WHERE Id = 1 AND [Group] = 'A' AND Value = 12.95) 
BEGIN
	INSERT INTO [dbo].[DeliveryChargeGroupValue] (Id, [Group], Value)
	VALUES
		(1, 'A', 12.95)		
END
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[DeliveryChargeGroupValue] WHERE [Group] = 'B') 
BEGIN
	INSERT INTO [dbo].[DeliveryChargeGroupValue] (Id, [Group], Value)
	VALUES
		(2, 'B', 9.95)		
END
GO

if @@error = 0
   print 'Success: Create Table DeliveryChargeGroup for US17466 has been deployed successfully'
else
   print 'Failure: Create Table DeliveryChargeGroup for US17466 has been deployed successfully'
go