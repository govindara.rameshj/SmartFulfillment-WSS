IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('STKMAS') and name = 'DateLastSold')
    BEGIN
        PRINT 'Add DateLastSold column to dbo.STKMAS'
        ALTER TABLE dbo.STKMAS
        ADD [DateLastSold] DATE NULL
    END
GO

If @@Error = 0
   Print 'Success: Alter Table dbo.STKMAS for US17409 has been deployed successfully'
Else
   Print 'Failure: Alter Table dbo.STKMAS for US17409 has not been deployed successfully'
Go