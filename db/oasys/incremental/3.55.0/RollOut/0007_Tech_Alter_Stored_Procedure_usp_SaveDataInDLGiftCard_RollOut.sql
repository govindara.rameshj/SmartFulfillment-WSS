﻿IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[usp_SaveDataInDLGiftCard]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure [usp_SaveDataInDLGiftCard]'
    EXEC ('CREATE PROCEDURE dbo.[usp_SaveDataInDLGiftCard] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

ALTER PROCEDURE dbo.usp_SaveDataInDLGiftCard
    @DATE1      date,
    @TILL       char(2),
    @TRAN       char(4),
    @CARDNUM    char(19),
    @EEID       char(3),
    @TYPE       char(2),
    @AMNT       decimal(9, 2),
    @AUTH       char(150),
    @MSGNUM     char(150),
    @TRANID     decimal(38,0),
    @RTIFlag    char(1)='S',
    @SEQN       int=1
AS
BEGIN
    SET NOCOUNT ON;
    
    INSERT INTO dbo.DLGIFTCARD (DATE1, TILL, [TRAN], CARDNUM, EEID, [TYPE], AMNT, AUTH, MSGNUM, TRANID, RTI, SEQN)
    VALUES (@DATE1, @TILL, @TRAN, @CARDNUM, @EEID, @TYPE, @AMNT, @AUTH, @MSGNUM, @TRANID, @RTIFlag, @SEQN)
    
    SET NOCOUNT OFF;
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_SaveDataInDLGiftCard for Tech has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_SaveDataInDLGiftCard for Tech has not been deployed'
GO