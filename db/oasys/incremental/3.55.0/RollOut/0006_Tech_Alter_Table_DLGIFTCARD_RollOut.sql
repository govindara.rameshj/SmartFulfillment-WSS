﻿IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLGIFTCARD') and name = 'SEQN')
    BEGIN
        ALTER TABLE dbo.DLGIFTCARD
        ADD SEQN INT NULL
        END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLGIFTCARD') and name = 'SEQN') 
    BEGIN 
        UPDATE dbo.DLGIFTCARD
        SET SEQN = 1
        WHERE SEQN IS NULL
    END
GO
  
IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLGIFTCARD') and name = 'SEQN' and is_nullable = 1)
    BEGIN
        ALTER TABLE dbo.DLGIFTCARD
        ALTER COLUMN SEQN INT NOT NULL
    END
GO
  
IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLGIFTCARD') and name = 'TYPE')
    BEGIN
        UPDATE dbo.DLGIFTCARD
        SET [TYPE] = 'N'
        WHERE [TYPE] IS NULL OR [TYPE] = ''
    END
GO
  
IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLGIFTCARD') and name = 'TYPE' and is_nullable = 1)
    BEGIN
        ALTER TABLE dbo.DLGIFTCARD
        ALTER COLUMN [TYPE] CHAR(2) NOT NULL
    END
GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'DLGIFTCARD') AND name = N'PK_DLGIFTCARD')
    BEGIN
        ALTER TABLE [dbo].[DLGIFTCARD]
        DROP CONSTRAINT PK_DLGIFTCARD
      
        ALTER TABLE [dbo].[DLGIFTCARD] 
        ADD CONSTRAINT PK_DLGIFTCARD PRIMARY KEY CLUSTERED (DATE1, TILL, [TRAN], AUTH, SEQN, [TYPE])
    END
GO

If @@Error = 0
   Print 'Success: Alter Table dbo.DLGIFTCARD for [Tech] has been deployed successfully'
Else
   Print 'Failure: Alter Table dbo.DLGIFTCARD for [Tech] has not been deployed successfully'
Go