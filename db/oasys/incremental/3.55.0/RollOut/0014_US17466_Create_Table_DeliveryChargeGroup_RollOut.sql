IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeliveryChargeGroup]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    PRINT 'Creating table DeliveryChargeGroup'

    CREATE TABLE [dbo].[DeliveryChargeGroup]
    (
        Id int NOT NULL IDENTITY(1,1),
        [Group] char(1) NOT NULL UNIQUE,
        SkuNumber char(6) NOT NULL,
        IsDefault bit DEFAULT 0,
        CONSTRAINT PK_DeliveryChargeGroup PRIMARY KEY (Id, [GROUP])) ON [PRIMARY]        
END
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[DeliveryChargeGroup] WHERE [Group] = 'A') 
BEGIN
	INSERT INTO [dbo].[DeliveryChargeGroup] ([Group], SkuNumber)
	VALUES
		('A', '805002')		
END
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[DeliveryChargeGroup] WHERE [Group] = 'B') 
BEGIN
	INSERT INTO [dbo].[DeliveryChargeGroup] ([Group], SkuNumber)
	VALUES
		('B', '805006')		
END
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[DeliveryChargeGroup] WHERE [Group] = 'C') 
BEGIN
	INSERT INTO [dbo].[DeliveryChargeGroup] ([Group], SkuNumber, IsDefault)
	VALUES
		('C', '805000', 1)		
END
GO

if @@error = 0
   print 'Success: Create Table DeliveryChargeGroup for US17466 has been deployed successfully'
else
   print 'Failure: Create Table DeliveryChargeGroup for US17466 has been deployed successfully'
go