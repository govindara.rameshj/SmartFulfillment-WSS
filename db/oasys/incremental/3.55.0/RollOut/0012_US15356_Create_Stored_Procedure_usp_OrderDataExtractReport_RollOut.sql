IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[usp_OrderDataExtractReport]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure [usp_OrderDataExtractReport]'
    EXEC ('CREATE PROCEDURE dbo.[usp_OrderDataExtractReport] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

ALTER PROCEDURE [dbo].[usp_OrderDataExtractReport] 
	 @StartDate AS date,
	 @EndDate AS date = NULL
AS
BEGIN

	SELECT
		SellingStoreId AS 'Selling Store Id', 
		OrderLine.SKUN AS 'SKU No',
		SourceOrderNumber AS 'OVC Reference', 
		OrderLine.NUMB AS 'Back Office Order Number', 
		DELD AS 'Delivery Date', 
		OrderLine.QTYO * OrderLine.Price AS 'Line Value', 
		'Fulfiller Store Id' = 
			CASE 
				WHEN OrderLine.DeliverySource IS NULL THEN ''
				WHEN OrderLine.DeliverySource = '0' THEN ''
				ELSE OrderLine.DeliverySource
			END
	FROM vwCORHDRFull as OrderHeaderFull
		JOIN (SELECT SKUN, NUMB, QTYO, Price, DeliverySource FROM CORLIN) AS OrderLine ON OrderLine.NUMB = OrderHeaderFull.NUMB
	WHERE ((OrderHeaderFull.DATE1 = @StartDate AND @EndDate IS NULL) OR (OrderHeaderFull.DATE1 >= @StartDate AND OrderHeaderFull.DATE1 <= @EndDate)) AND SourceOrderNumber IS NOT NULL AND OrderHeaderFull.Source = 'OVC'
END
GO
If @@Error = 0
   Print 'Success: The Create Stored Procedure usp_OrderDataExtractReport for US15356 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure usp_OrderDataExtractReport for US15356 has not been deployed'
GO