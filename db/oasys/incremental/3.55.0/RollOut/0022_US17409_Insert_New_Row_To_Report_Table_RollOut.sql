INSERT INTO [dbo].[ReportColumns]
           ([ReportId], [TableId], [ColumnId], [Sequence], [IsVisible], [IsBold], [IsHighlight], [HighlightColour], [Fontsize])
SELECT t.[ReportId], t.[TableId], t.[ColumnId], t.[Sequence], t.[IsVisible], t.[IsBold], t.[IsHighlight], t.[HighlightColour], t.[Fontsize]
FROM (
        SELECT 203 AS [ReportId], 1 AS [TableId], 2510 AS [ColumnId], 0 AS [Sequence], 1 AS [IsVisible], 0 AS [IsBold], NULL AS [IsHighlight], NULL AS [HighlightColour], NULL AS [Fontsize]
    ) t 
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[ReportColumns] t1 WHERE t1.[ReportId] = t.[ReportId] and t1.[TableId] = t.[TableId] and t1.[ColumnId] = t.[ColumnId])
GO

If @@Error = 0
   Print 'Success: Insert row to [dbo].[ReportColumns] for US17409 has been deployed successfully'
Else
   Print 'Failure: Insert row to [dbo].[ReportColumns] for US17409 has not been deployed successfully'
Go