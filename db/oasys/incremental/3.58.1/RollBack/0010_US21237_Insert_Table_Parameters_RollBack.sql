IF EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 505)
    BEGIN
        DELETE FROM dbo.[Parameters] WHERE ParameterId = 505
    END
GO

IF EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 506)
    BEGIN
        DELETE FROM dbo.[Parameters] WHERE ParameterId = 506
    END
GO

If @@Error = 0
   Print 'Success: The Delete from Parameters table for US21237 has been deployed successfully'
Else
   Print 'Failure: The Delete from Parameters table for US21237 has not been deployed'
GO