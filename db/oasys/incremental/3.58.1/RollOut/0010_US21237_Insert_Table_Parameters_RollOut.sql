IF NOT EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 505)
    BEGIN
        INSERT INTO dbo.Parameters (ParameterID, [Description],	StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES
        (505,'STA Audit Upload folder path', '\\WX0IVE01\Polling\Transmission\{store}OP{date}.xml', 0, 0, 0.00000, 0)
    END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 506)
    BEGIN
        INSERT INTO dbo.Parameters (ParameterID, [Description],	StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES
        (506,'STA Audit Upload date format', 'yyyyMMdd', 0, 0, 0.00000, 0)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US21237 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US21237 has not been deployed'
GO