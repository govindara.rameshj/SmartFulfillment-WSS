SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('STKADJ') and name = 'IsStaAuditUpload')
BEGIN
    PRINT 'Add IsStaAuditUpload column to STKADJ'
    ALTER TABLE dbo.STKADJ ADD IsStaAuditUpload bit NULL CONSTRAINT DF_STKADJ_IsStaAuditUpload DEFAULT 0 WITH VALUES
END
GO

If @@Error = 0
   Print 'Success: Altering table STKADJ for US21242 has been rolled out successfully'
ELSE
   Print 'Failure: Altering table STKADJ for US21242 has not been rolled out successfully'
GO