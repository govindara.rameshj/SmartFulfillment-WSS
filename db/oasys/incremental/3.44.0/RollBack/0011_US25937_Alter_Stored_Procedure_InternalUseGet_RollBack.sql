SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[InternalUserGet]
   @Id INT = null,
   @OnlyBankingIds bit = 0
AS
Begin
	SET NOCOUNT ON;

	Select 
		ID						as Id,
		EmployeeCode			as Code,
		SecurityProfileID		as ProfileId,
		Name,
		Initials,
		Position,
		PayrollID				as PayrollId,
		Password,
		PasswordExpires,
		SupervisorPassword		as SuperPassword,
		SupervisorPwdExpires	as SuperPasswordExpires,
		Outlet,
		IsDeleted,
		DeletedDate,
		DeletedBy,
		DeletedWhere,
		TillReceiptName,
		LanguageCode
	From
		SystemUsers
	Where
		((@Id is null) or (@Id is not null and ID = @Id))
			and   
		(ID < 500 or (ID = 555 and @OnlyBankingIds = 1))
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure InternalUserGet for US25937 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure InternalUserGet for US25937 has not been deployed successfully'
GO

