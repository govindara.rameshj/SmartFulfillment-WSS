DELETE FROM  [dbo].[Parameters] WHERE ParameterID in (985, 5500, 5501, 5502, 5503, 5504, 5505, 5506)
GO

If @@Error = 0
   Print 'Success: Delete from Table Parameters for US25937 has been deployed successfully'
Else
   Print 'Failure: Delete from Table Parameters for US25937 has not been deployed successfully'
Go
