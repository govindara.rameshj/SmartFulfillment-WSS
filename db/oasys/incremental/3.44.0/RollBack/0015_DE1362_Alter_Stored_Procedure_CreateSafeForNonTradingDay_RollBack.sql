IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'CreateSafeForNonTradingDay') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure CreateSafeForNonTradingDay'
	EXEC ('CREATE PROCEDURE dbo.CreateSafeForNonTradingDay AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure CreateSafeForNonTradingDay')
GO


ALTER PROCEDURE dbo.CreateSafeForNonTradingDay
	@periodDate date
AS
BEGIN

IF NOT EXISTS (SELECT 1 FROM [dbo].[Safe] s where s.[PeriodDate] = DATEADD(dd, -1, @periodDate))
BEGIN
	INSERT INTO [dbo].[Safe] 
		(
			[PeriodID],
			[PeriodDate],
			[UserID1],
			[UserID2],
			[LastAmended],
			[IsClosed],
			[EndOfDayCheckDone]
		)
	SELECT 
		sp.[ID]			AS [PeriodID], 
		sp.[StartDate]	AS [PeriodDate], 
		0				AS [UserID1], 
		0				AS [UserID2], 
		GETDATE()		AS [LastAmended], 
		0				AS [IsClosed], 
		0				AS [EndOfDayCheckDone]
	FROM [dbo].[SystemPeriods] sp
	CROSS JOIN (SELECT TOP(1) [PeriodDate] FROM [dbo].[Safe] s WHERE [PeriodDate] < @periodDate ORDER BY [PeriodDate] DESC) s
	INNER JOIN [dbo].[NonTradingDay] n on n.[SystemPeriodID] = sp.[ID]
	WHERE sp.[StartDate] < @periodDate
	 AND sp.[StartDate] > s.[PeriodDate]
	ORDER BY sp.[StartDate]
	
	RETURN @@ROWCOUNT
END
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure CreateSafeForNonTradingDay for DE1362 has been rolled back successfully'
Else
   Print 'Failure: The Create Stored Procedure CreateSafeForNonTradingDay for DE1362 has not rolled back'
Go
