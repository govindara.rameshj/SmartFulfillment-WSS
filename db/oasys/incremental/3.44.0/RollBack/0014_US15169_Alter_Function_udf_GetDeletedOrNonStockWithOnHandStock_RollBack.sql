SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER Function
	[dbo].[udf_GetDeletedOrNonStockWithOnHandStock](@Status Char(1)  = 'A') 
Returns 
	@OutputTable Table
		(
			HieCategory      Char(6)  Not Null,
			HieCategoryName  VarChar(50)  Not Null,
			SkuNumber        Char(6)  Not Null,
			Description      Char(40)  Null,
			IsNonStock       Bit Not Null,
			IsDeleted        Bit Not Null,
			IsObsolete       Bit Not Null,
			DateDeleted      Date Null,
			DateLastSold     Date Null,
			DateLastReceived Date Null,
			PricePrior       Decimal(9, 2) Null,
			DatePrior        Date Null,
			Price            Decimal(9, 2) Null,
			QtyOnHand        Int Null,
			ValueDeleted     Decimal(9, 2) Null,
			ValueNonStock    Decimal(9, 2) Null,
			Comment          VarChar(20)
		) 
As
Begin

    Insert Into
		@OutputTable
    Select
		st.CTGY,
		hm.DESCR,
		st.skun,
		st.descr,
		st.INON,
		st.IDEL,
		st.IOBS,
		Case
           When st.IOBS = 0 And st.IDEL = 1 Then st.DDEL
           When st.IOBS = 1 And st.IDEL = 0 Then st.DOBS
           When st.IOBS = 1 And st.IDEL = 1 Then st.DDEL
        End,
        st.DSOL,
        st.drec,
        st.PPRI,
        st.DPRC,
        st.pric,
        st.onha,
        Case
			When st.IDEL = 1 Then st.ONHA * st.PRIC
			When st.IOBS = 1 Then st.ONHA * st.PRIC
			Else 0
		End,
		Case
			When st.IDEL = 0 And st.IOBS = 0 And st.INON = 1 Then st.ONHA * st.PRIC
			Else 0
		End,
		''
      From
		stkmas st
			Inner Join
				HIEMAS hm
			On
				st.CTGY =  hm.NUMB
			And
				hm.LEVL = 5
	Where
		st.ONHA <> 0
	And
		(
			@Status = 'A'
		And
			(
				st.INON = 1
			Or
				st.IDEL = 1
			Or
				st.IOBS = 1
			)
		Or
			@Status = 'D'
		And
			(
				st.IDEL = 1
			Or
				st.IOBS = 1
			)
		Or
			(
				@Status = 'N'
			And
				st.INON = 1
			)
		);
	
	Return;
End;
GO

If @@Error = 0
   Print 'Success: The Alter Function udf_GetDeletedOrNonStockWithOnHandStock for US15169 has been deployed successfully'
Else
   Print 'Failure: The Alter Function udf_GetDeletedOrNonStockWithOnHandStock for US15169 has not been deployed successfully'
GO

