IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'PicVarianceReport') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure PicVarianceReport'
	EXEC ('CREATE PROCEDURE dbo.PicVarianceReport AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure PicVarianceReport')
GO

ALTER PROCEDURE [dbo].[PicVarianceReport]
	@Date	date,
	@VariancesOnly bit=0
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @checkcount INT 

SET @checkcount = 1 

DECLARE @StartOnHand Dec(9,2)
DECLARE @QtyVariance Dec(9,2)
DECLARE @Value Dec(9,2)
DECLARE @ValueOnhand Dec(9,2)
DECLARE @MarkDownOnhand Dec(9,2)
DECLARE @Origin As VARCHAR(MAX)
DECLARE @PercentUnitsRefunded Dec(9,2)=0
DECLARE @PercentValueRefunded Dec(9,2)=0
DECLARE @PercentUnitsAudit Dec(9,2)=0
DECLARE @PercentValueAudit Dec(9,2)=0
DECLARE @PercentUnitsNegative Dec(9,2)=0
DECLARE @PercentValueNegative Dec(9,2)=0
DECLARE @PercentUnitsStoreAdded Dec(9,2)=0
DECLARE @PercentValueStoreAdded Dec(9,2)=0
DECLARE @PercentUnitsStockLoss Dec(9,2)=0
DECLARE @PercentValueStockLoss Dec(9,2)=0
DECLARE @PercentUnitsParked Dec(9,2)=0
DECLARE @PercentValueParked Dec(9,2)=0
DECLARE @PercentUnitsSTACount Dec(9,2)=0
DECLARE @PercentValueSTACount Dec(9,2)=0
DECLARE @PercentValueSTACounter Dec(9,2)=0
DECLARE @PercentValueParkedCounter Dec(9,2)=0
DECLARE @PercentValueStockLossCounter Dec(9,2)=0
DECLARE @PercentValueStoreAddedCounter Dec(9,2)=0
DECLARE @PercentValueNegativeCounter Dec(9,2)=0
DECLARE @PercentValueAuditCounter Dec(9,2)=0
DECLARE @PercentValueRefundedCounter Dec(9,2)=0
DECLARE @PercentUnitSTACounter Dec(9,2)=0
DECLARE @PercentUnitParkedCounter Dec(9,2)=0
DECLARE @PercentUnitStockLossCounter Dec(9,2)=0
DECLARE @PercentUnitStoreAddedCounter Dec(9,2)=0
DECLARE @PercentUnitNegativeCounter Dec(9,2)=0
DECLARE @PercentUnitAuditCounter Dec(9,2)=0
DECLARE @PercentUnitRefundedCounter Dec(9,2)=0

	declare @table table(
		Description			varchar(100),
		SkuNumber			char(6),
		Price				dec(9,2),
		QtyOnHand			dec(9,2),
		QtyShopFloor		int,
		QtyWarehouse		int,
		QtyPreSold			int,
		QtyMarkdownOnHand	int,
		QtyMarkdown			int,
		QtyTotal			int,
		QtyVariance			dec(9,2),
		Value				dec(9,2),
		ValueOnHand			dec(9,2),
		IsApplied			bit,
		IsLabelled			bit,
		IsNonStock			bit,
		Origin				varchar(100));

	--get pic count values
	insert into
		@table
			(Description,
			SkuNumber,
			Price,
			QtyOnHand,
			QtyShopFloor,
			QtyWarehouse,
			QtyPresold,
			QtyMarkdownOnHand,
			QtyMarkdown,
			IsApplied,
			IsLabelled,
			IsNonStock,
			Origin)
	select	
		sk.DESCR,
		hd.SKUN, 
		sk.PRIC,
		hd.onha,
		gr.QtyShopFloor,
		gr.QtyWarehouse, 
		hd.TPRE,
		hd.MDNQ,
		hd.TMDC,
		hd.IADJ,
		hd.LBOK,				
		hd.INON, 
		gr.orig
	from
		HHTDET hd
	inner join
		STKMAS sk		on sk.SKUN = hd.SKUN
	inner join
		(select
			skun,
			date1, 		
			(sum(case when hl.IslandType='S' then hl.StockCount else 0 end)) as QtyShopFloor,
			(sum(case when hl.IslandType='W' then hl.StockCount else 0 end)) as QtyWarehouse,
			(case orig 
				when 'R' then 'Refunded Items (System Generated)'
				when 'A' then 'Audit Count Items'
				when 'N' then 'Negative Stock Items (System Generated)'
				when 'S' then 'Store Added Items (Manual Count Adjustments)'
				when 'H' then 'Stock Loss Items (System Generated)'
				when 'P' then 'Parked Items (System Generated)'
				else 'STA Count Variances (Scheduled Count)'
			end) as orig
		from 
			HHTDET
		left outer join 
			HhtLocation hl	on hl.DateCreated = DATE1 and hl.SkuNumber=SKUN 
		group by
			SKUN,DATE1, ORIG) gr on gr.SKUN = hd.SKUN and gr.DATE1=hd.DATE1
	where	
		hd.DATE1 = convert(date, @Date)
	order by 
		hd.SKUN;
				
		
	--update table with summary and variance and return
	update @table set QtyTotal = QtyShopFloor + QtyWarehouse + QtyMarkdown - QtyPresold;
	update @table set QtyVariance = QtyTotal - QtyOnHand - QtyMarkdownOnHand;
	update @table set Value	= QtyVariance * Price;
	update @table set ValueOnHand = (QtyOnHand + QtyMarkdownOnHand) * Price;
	select * from @table where (@VariancesOnly=0 or (@VariancesOnly=1 and QtyVariance<>0));
	
SELECT IDENTITY(INT, 1, 1) AS [key], 
       * 
INTO   #temptable 
FROM   @table 
WHERE  ( @VariancesOnly = 0 
          OR ( @VariancesOnly = 1 
               AND qtyvariance <> 0 ) ); 

DECLARE @counter AS INT 

SELECT @counter = MAX([key]) 
FROM   #temptable  

WHILE @counter >= @checkcount 
  BEGIN 
      SELECT @StartOnHand = qtyonhand, 
             @QtyVariance = qtyvariance, 
             @Value = VALUE, 
             @ValueOnhand = valueonhand, 
             @Origin = origin,
             @MarkDownOnhand = QtyMarkdownOnHand  
      FROM   #temptable 
      WHERE  [key] = @checkcount 

      IF ( @Origin = 'Refunded Items (System Generated)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsRefunded = 
                  @PercentUnitsRefunded + Abs(@QtyVariance)                 
                  SET @PercentValueRefunded = 
                  @PercentValueRefunded +Abs(@Value) 
                  SET @PercentUnitRefundedCounter = 
                  @PercentUnitRefundedCounter + Abs( 
                  @StartOnHand)+@MarkDownOnhand    
                  SET @PercentValueRefundedCounter = 
                  @PercentValueRefundedCounter + Abs( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Audit Count Items' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsAudit = 
                  @PercentUnitsAudit + Abs(@QtyVariance)
                  SET @PercentValueAudit = 
                  @PercentValueAudit + Abs(@Value)
                  SET @PercentUnitAuditCounter = @PercentUnitAuditCounter + Abs 
                                                 ( 
                                                 @StartOnHand)+@MarkDownOnhand  
                  SET @PercentValueAuditCounter = @PercentValueAuditCounter + 
                                                  Abs(@ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Negative Stock Items (System Generated)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsNegative = 
                  @PercentUnitsNegative + Abs(@QtyVariance )
                  SET @PercentValueNegative = 
                  @PercentValueNegative + Abs(@Value)
                  SET @PercentUnitNegativeCounter = 
                  @PercentUnitNegativeCounter + Abs( 
                  @StartOnHand)+@MarkDownOnhand     
                  SET @PercentValueNegativeCounter = 
                  @PercentValueNegativeCounter + Abs( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Store Added Items (Manual Count Adjustments)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsStoreAdded = @PercentUnitsStoreAdded + 
                  Abs(@QtyVariance ) 
                  SET @PercentValueStoreAdded = 
                  @PercentValueStoreAdded + Abs(@Value )
                  SET @PercentUnitStoreAddedCounter = 
                  @PercentUnitStoreAddedCounter + Abs( 
                  @StartOnHand)+@MarkDownOnhand   
                  SET @PercentValueStoreAddedCounter = 
                  @PercentValueStoreAddedCounter + Abs( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Stock Loss Items (System Generated)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsStockLoss = 
                  @PercentUnitsStockLoss + Abs(@QtyVariance)
                  SET @PercentValueStockLoss = 
                  @PercentValueStockLoss + Abs(@Value)
                  SET @PercentUnitStockLossCounter = 
                  @PercentUnitStockLossCounter + Abs( 
                  @StartOnHand)+@MarkDownOnhand     
                  SET @PercentValueStockLossCounter = 
                  @PercentValueStockLossCounter + Abs( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Parked Items (System Generated)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsParked = 
                  @PercentUnitsParked + Abs(@QtyVariance)
                  SET @PercentValueParked = 
                  @PercentValueParked + Abs(@Value)
                  SET @PercentUnitParkedCounter = @PercentUnitParkedCounter + 
                                                  Abs(@StartOnHand)+@MarkDownOnhand  
                  SET @PercentValueParkedCounter = 
                  @PercentValueParkedCounter + Abs( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'STA Count Variances (Scheduled Count)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsSTACount = 
                  @PercentUnitsSTACount + Abs(@QtyVariance)
                  SET @PercentValueSTACount = 
                  @PercentValueSTACount + Abs(@Value)                   
                  SET @PercentUnitSTACounter = @PercentUnitSTACounter + 
                                               Abs(@StartOnHand)+@MarkDownOnhand  
                  SET @PercentValueSTACounter = @PercentValueSTACounter + Abs( 
                                                @ValueOnhand) 
              END 
        END 

      SET @checkcount = @checkcount + 1 
  END 

SET @PercentUnitsAudit = [dbo].[udf_PicPercentageUnitsCalculation](@PercentUnitsAudit,@PercentUnitAuditCounter)                                               
SET @PercentUnitsNegative = [dbo].[udf_PicPercentageUnitsCalculation](@PercentUnitsNegative,@PercentUnitNegativeCounter) 
SET @PercentUnitsParked = [dbo].[udf_PicPercentageUnitsCalculation] (@PercentUnitsParked,@PercentUnitParkedCounter) 
SET @PercentUnitsRefunded =[dbo]. [udf_PicPercentageUnitsCalculation](@PercentUnitsRefunded,@PercentUnitRefundedCounter) 
SET @PercentUnitsSTACount =[dbo]. [udf_PicPercentageUnitsCalculation](@PercentUnitsSTACount,@PercentUnitSTACounter) 
SET @PercentUnitsStockLoss =[dbo]. [udf_PicPercentageUnitsCalculation](@PercentUnitsStockLoss,@PercentUnitStockLossCounter) 
SET @PercentUnitsStoreAdded = [dbo].[udf_PicPercentageUnitsCalculation](@PercentUnitsStoreAdded,@PercentUnitStoreAddedCounter) 
SET @PercentValueAudit = [dbo].[udf_PicPercentageUnitsCalculation](@PercentValueAudit,@PercentValueAuditCounter) 
SET @PercentValueNegative = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueNegative,@PercentValueNegativeCounter) 
SET @PercentValueParked = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueParked,@PercentValueParkedCounter) 
SET @PercentValueRefunded = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueRefunded,@PercentValueRefundedCounter) 
SET @PercentValueSTACount = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueSTACount,@PercentValueSTACounter) 
SET @PercentValueStockLoss = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueStockLoss,@PercentValueStockLossCounter) 
SET @PercentValueStoreAdded = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueStoreAdded,@PercentValueStoreAddedCounter)  

declare @summary table(
		Origin			varchar(100),
		[Count]			int,
		QtyOnHand		dec(9,2),
		QtyShopFloor	int,
		QtyWarehouse	int,
		QtyPreSold		int,
		QtyMarkdownOnHand int,
		QtyMarkdown		int,
		QtyTotal		int,
		QtyVariance		dec(9,2),
		Value			dec(9,2),
		ValueOnHand		dec(9,2),	
		PercUnits		dec(9,2),
		PercValue		dec(9,2))
	
	--get summary info from @table
	insert into 
		@summary 
			(Origin,
			[Count],
			QtyOnHand,
			QtyShopFloor,
			QtyWarehouse,
			QtyPresold,
			QtyMarkdownOnHand,
			QtyMarkdown,
			QtyTotal,
			QtyVariance,
			Value,
			ValueOnHand)
	select
		Origin,
		COUNT(origin),
		sum(QtyOnHand),
		sum(QtyShopFloor),
		sum(QtyWarehouse),
		sum(QtyPreSold),
		SUM(QtyMarkdownOnHand),
		sum(QtyMarkdown),
		sum(QtyTotal),
		sum(QtyVariance),
		sum(Value),
		SUM(ValueOnhand)
	from
		@table 
		where (@VariancesOnly=0 or (@VariancesOnly=1 and QtyVariance<>0))
	group by
		Origin
		
IF EXISTS(SELECT 1 
          FROM   @summary 
          WHERE  origin = 'Refunded Items (System Generated)') 
  BEGIN 
      UPDATE @summary 
      SET    percunits = @PercentUnitsRefunded 
      WHERE  origin = 'Refunded Items (System Generated)' 

      UPDATE @summary 
      SET    percvalue = @PercentValueRefunded 
      WHERE  origin = 'Refunded Items (System Generated)' 
  END 

IF EXISTS(SELECT 1 
          FROM   @summary 
          WHERE  origin = 'Audit Count Items') 
  BEGIN 
      UPDATE @summary 
      SET    percunits = @PercentUnitsAudit 
      WHERE  origin = 'Audit Count Items' 

      UPDATE @summary 
      SET    percvalue = @PercentValueAudit 
      WHERE  origin = 'Audit Count Items' 
  END 

IF EXISTS(SELECT 1 
          FROM   @summary 
          WHERE  origin = 'Negative Stock Items (System Generated)') 
  BEGIN 
      UPDATE @summary 
      SET    percunits = @PercentUnitsNegative 
      WHERE  origin = 'Negative Stock Items (System Generated)' 

      UPDATE @summary 
      SET    percvalue = @PercentValueNegative 
      WHERE  origin = 'Negative Stock Items (System Generated)' 
  END 

IF EXISTS(SELECT 1 
          FROM   @summary 
          WHERE  origin = 'Store Added Items (Manual Count Adjustments)') 
  BEGIN 
      UPDATE @summary 
      SET    percunits = @PercentUnitsStoreAdded 
      WHERE  origin = 'Store Added Items (Manual Count Adjustments)' 

      UPDATE @summary 
      SET    percvalue = @PercentValueStoreAdded 
      WHERE  origin = 'Store Added Items (Manual Count Adjustments)' 
  END 

IF EXISTS(SELECT 1 
          FROM   @summary 
          WHERE  origin = 'Stock Loss Items (System Generated)') 
  BEGIN 
      UPDATE @summary 
      SET    percunits = @PercentUnitsStockLoss 
      WHERE  origin = 'Stock Loss Items (System Generated)' 

      UPDATE @summary 
      SET    percvalue = @PercentValueStockLoss 
      WHERE  origin = 'Stock Loss Items (System Generated)' 
  END 

IF EXISTS(SELECT 1 
          FROM   @summary 
          WHERE  origin = 'Parked Items (System Generated)') 
  BEGIN 
      UPDATE @summary 
      SET    percunits = @PercentUnitsParked 
      WHERE  origin = 'Parked Items (System Generated)' 

      UPDATE @summary 
      SET    percvalue = @PercentValueParked 
      WHERE  origin = 'Parked Items (System Generated)' 
  END 

IF EXISTS(SELECT 1 
          FROM   @summary 
          WHERE  origin = 'STA Count Variances (Scheduled Count)') 
  BEGIN 
      UPDATE @summary 
      SET    percunits = @PercentUnitsSTACount 
      WHERE  origin = 'STA Count Variances (Scheduled Count)' 

      UPDATE @summary 
      SET    percvalue = @PercentValueSTACount 
      WHERE  origin = 'STA Count Variances (Scheduled Count)' 
  END  
select * from @summary;

	declare @signature table(Signature1	varchar(200), Signature2 varchar(200))
	insert into @signature values('STA Signature','Managers Signature') ;
	insert into @signature values('Date','Date') ;
	select * from @signature;

	declare @sigfooter table(Signature3 varchar(400))
	insert into @sigfooter values ('Authorised By: <NOT AUTHORISED>');
	select * from @sigfooter ;

END
GO

IF @@Error = 0
   PRINT 'Success: The Alter Stored Procedure PicVarianceReport for US26465 has been deployed successfully'
ELSE
   PRINT 'Failure: The Alter Stored Procedure PicVarianceReport for US264652 has not been deployed'
GO
