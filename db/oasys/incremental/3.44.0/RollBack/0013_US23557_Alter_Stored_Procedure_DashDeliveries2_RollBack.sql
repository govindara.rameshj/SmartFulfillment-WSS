ALTER PROCEDURE [dbo].[DashDeliveries2]
	@DateEnd	date
as

Begin

	Set NOCOUNT On
	
	--------------------------------------------------------------------------------------------------------------
	-- Create Temporary Table
	--------------------------------------------------------------------------------------------------------------
	Declare		@Table 
	Table		(
				Description	varchar(50),
				Qty			varchar(50),
				QtyWtd		varchar(50),
				Value		dec(9,2),
				ValueWtd	dec(9,2)
				);
				
	
	--------------------------------------------------------------------------------------------------------------
	-- Declare Variables Used for Report
	--------------------------------------------------------------------------------------------------------------
	Declare
				@StartDate				date,
				@DeliveryTranC_TD		int,
				@DeliveryTranV_TD		dec(9,2),
				@DeliveryChargeC_TD		int,
				@DeliveryChargeV_TD		dec(9,2),
				@DeliveryTranC_WD		int,
				@DeliveryTranV_WD		dec(9,2),
				@DeliveryChargeC_WD		int,
				@DeliveryChargeV_WD		dec(9,2),
				--@DeliveryChargeC_PTS
				@CoreDayValue			dec(9,2),
				@CoreWeekValue			dec(9,2);	
	
	
	---------------------------------------------------------------------------------------------
	-- Setting @InputDate as passed Date
	--------------------------------------------------------------------------------------------- 
	Declare		@InputDate	date
	Set			@InputDate	=	@DateEnd;
	
	
	---------------------------------------------------------------------------------------------
	-- Delivery Charge's WTD (WEEK TO DATE)
	--------------------------------------------------------------------------------------------- 
	Select			
	-- Retrieve Delivery Charges
	@DeliveryChargeC_WD		= (Select Oasys.dbo.svf_ReportDeliveryChargesQtyWTD(@InputDate)),
	@DeliveryChargeV_WD		= (Select Oasys.dbo.svf_ReportDeliveryChargesValueWTD(@InputDate)),
	@DeliveryChargeC_TD		= (Select Oasys.dbo.svf_ReportDeliveryChargesQtyDAY(@InputDate)),
	@DeliveryChargeV_TD		= (Select Oasys.dbo.svf_ReportDeliveryChargesValueDAY(@InputDate)),
	-- Retrieve Delivery Transaction Values (Merchandise Only)
	@DeliveryTranC_TD		= (Select Oasys.dbo.svf_ReportDeliveryTransQtyDAY(@InputDate)),
	@DeliveryTranV_TD		= (Select Oasys.dbo.svf_ReportDeliveryTransValueDAY(@InputDate)),
	@DeliveryTranC_WD		= (Select Oasys.dbo.svf_ReportDeliveryTransQtyWTD(@InputDate)),
	@DeliveryTranV_WD		= (Select Oasys.dbo.svf_ReportDeliveryTransValueWTD(@InputDate)),
	-- Sales Data for Calculation
	@CoreDayValue			= (Select Oasys.dbo.svf_ReportNETSalesValueDAY(@InputDate)),
	@CoreWeekValue			= (Select Oasys.dbo.svf_ReportNETSalesValueWTD(@InputDate))
	
	
	---------------------------------------------------------------------------------------------
	-- Create % to Core Sales Data
	--------------------------------------------------------------------------------------------- 
	-- Do Delivery Charge Count & Value for (TD) and (WTD)	
	if @CoreDayValue <> 0 set @CoreDayValue = (@DeliveryTranV_TD / @CoreDayValue) * 100;	
	if @CoreWeekValue <> 0 set @CoreWeekValue = (@DeliveryTranV_WD / @CoreWeekValue) * 100;
	
	
	----------------------------------------------------------------------------------------------
	-- Insert Data into @Table
	----------------------------------------------------------------------------------------------
	Insert into @table values ('Delivery Goods', @DeliveryTranC_TD, @DeliveryTranC_WD, @DeliveryTranV_TD, @DeliveryTranV_WD) ;
	Insert into @table values ('Delivery charges', @DeliveryChargeC_TD, @DeliveryChargeC_WD, @DeliveryChargeV_TD, @DeliveryChargeV_WD) ;
	Insert into @table(Description, Qty, QtyWtd) values ('Percentage Delivery to Sales', @CoreDayValue, @CoreWeekValue) ;


	--------------------------------------------------------------------------------------------------------------
	-- Return Temporary Table for Report
	--------------------------------------------------------------------------------------------------------------
	Select * From @Table

End
