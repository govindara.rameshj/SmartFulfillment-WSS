update [SystemCodes]
set Code = 9878, Name = 'Gift Card'
where id = 12 and [type] = 'M+'
GO

If @@Error = 0
   Print 'Success: Update Table SystemCodes for DE1813 has been deployed successfully'
Else
   Print 'Failure: Update Table SystemCodes for DE1813 has not been deployed successfully'
Go