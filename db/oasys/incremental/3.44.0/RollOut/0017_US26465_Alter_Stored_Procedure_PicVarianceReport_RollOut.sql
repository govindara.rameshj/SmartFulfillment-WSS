IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'PicVarianceReport') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure PicVarianceReport'
	EXEC ('CREATE PROCEDURE dbo.PicVarianceReport AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure PicVarianceReport')
GO

ALTER PROCEDURE [dbo].[PicVarianceReport]
	@Date	date,
	@VariancesOnly bit=0
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @checkcount INT 

SET @checkcount = 1 

DECLARE @StartOnHand Dec(9,2)
DECLARE @QtyVariance Dec(9,2)
DECLARE @Value Dec(9,2)
DECLARE @ValueOnhand Dec(9,2)
DECLARE @MarkDownOnhand Dec(9,2)
DECLARE @Origin As VARCHAR(MAX)
DECLARE @PercentUnitsRefunded Dec(9,2)=0
DECLARE @PercentValueRefunded Dec(9,2)=0
DECLARE @PercentUnitsAudit Dec(9,2)=0
DECLARE @PercentValueAudit Dec(9,2)=0
DECLARE @PercentUnitsNegative Dec(9,2)=0
DECLARE @PercentValueNegative Dec(9,2)=0
DECLARE @PercentUnitsStoreAdded Dec(9,2)=0
DECLARE @PercentValueStoreAdded Dec(9,2)=0
DECLARE @PercentUnitsStockLoss Dec(9,2)=0
DECLARE @PercentValueStockLoss Dec(9,2)=0
DECLARE @PercentUnitsParked Dec(9,2)=0
DECLARE @PercentValueParked Dec(9,2)=0
DECLARE @PercentUnitsSTACount Dec(9,2)=0
DECLARE @PercentValueSTACount Dec(9,2)=0
DECLARE @PercentValueSTACounter Dec(9,2)=0
DECLARE @PercentValueParkedCounter Dec(9,2)=0
DECLARE @PercentValueStockLossCounter Dec(9,2)=0
DECLARE @PercentValueStoreAddedCounter Dec(9,2)=0
DECLARE @PercentValueNegativeCounter Dec(9,2)=0
DECLARE @PercentValueAuditCounter Dec(9,2)=0
DECLARE @PercentValueRefundedCounter Dec(9,2)=0
DECLARE @PercentUnitSTACounter Dec(9,2)=0
DECLARE @PercentUnitParkedCounter Dec(9,2)=0
DECLARE @PercentUnitStockLossCounter Dec(9,2)=0
DECLARE @PercentUnitStoreAddedCounter Dec(9,2)=0
DECLARE @PercentUnitNegativeCounter Dec(9,2)=0
DECLARE @PercentUnitAuditCounter Dec(9,2)=0
DECLARE @PercentUnitRefundedCounter Dec(9,2)=0
DECLARE @Authorised char(50) = ''


	DECLARE @table TABLE(
		Description			varchar(100),
		SkuNumber			char(6),
		Price				dec(9,2),
		QtyOnHand			dec(9,2),
		QtyShopFloor		int,
		QtyWarehouse		int,
		QtyPreSold			int,
		QtyMarkdownOnHand	int,
		QtyMarkdown			int,
		QtyTotal			int,
		QtyVariance			dec(9,2),
		Value				dec(9,2),
		ValueOnHand			dec(9,2),
		IsApplied			bit,
		IsLabelled			bit,
		IsNonStock			bit,
		Origin				varchar(100));

	--get pic count values
	INSERT INTO
		@table
			(Description,
			SkuNumber,
			Price,
			QtyOnHand,
			QtyShopFloor,
			QtyWarehouse,
			QtyPresold,
			QtyMarkdownOnHand,
			QtyMarkdown,
			IsApplied,
			IsLabelled,
			IsNonStock,
			Origin)
	SELECT	
		sk.DESCR,
		hd.SKUN, 
		sk.PRIC,
		hd.onha,
		gr.QtyShopFloor,
		gr.QtyWarehouse, 
		hd.TPRE,
		hd.MDNQ,
		hd.TMDC,
		hd.IADJ,
		hd.LBOK,				
		hd.INON, 
		gr.orig
	FROM
		HHTDET hd
	INNER JOIN
		STKMAS sk		on sk.SKUN = hd.SKUN
	INNER JOIN
		(SELECT
			skun,
			date1, 		
			(SUM(CASE WHEN hl.IslandType='S' THEN hl.StockCount ELSE 0 END)) AS QtyShopFloor,
			(SUM(CASE WHEN hl.IslandType='W' THEN hl.StockCount ELSE 0 END)) AS QtyWarehouse,
			(CASE orig 
				WHEN 'R' THEN 'Refunded Items (System Generated)'
				WHEN 'A' THEN 'Audit Count Items'
				WHEN 'N' THEN 'Negative Stock Items (System Generated)'
				WHEN 'S' THEN 'Store Added Items (Manual Count Adjustments)'
				WHEN 'H' THEN 'Stock Loss Items (System Generated)'
				WHEN 'P' THEN 'Parked Items (System Generated)'
				ELSE 'STA Count Variances (Scheduled Count)'
			END) AS orig
		FROM 
			HHTDET
		LEFT OUTER JOIN 
			HhtLocation hl	ON hl.DateCreated = DATE1 AND hl.SkuNumber=SKUN 
		GROUP BY
			SKUN,DATE1, ORIG) gr ON gr.SKUN = hd.SKUN AND gr.DATE1=hd.DATE1
	WHERE	
		hd.DATE1 = CONVERT(date, @Date)
	ORDER BY 
		hd.SKUN;
				
		
	--update table with summary and variance and return
	UPDATE @table SET QtyTotal = QtyShopFloor + QtyWarehouse + QtyMarkdown - QtyPresold;
	UPDATE @table SET QtyVariance = QtyTotal - QtyOnHand - QtyMarkdownOnHand;
	UPDATE @table SET Value	= QtyVariance * Price;
	UPDATE @table SET ValueOnHand = (QtyOnHand + QtyMarkdownOnHand) * Price;
	SELECT * FROM @table WHERE (@VariancesOnly=0 OR (@VariancesOnly=1 AND QtyVariance<>0));
	
SELECT IDENTITY(INT, 1, 1) AS [key], 
       * 
INTO   #temptable 
FROM   @table 
WHERE  ( @VariancesOnly = 0 
          OR ( @VariancesOnly = 1 
               AND qtyvariance <> 0 ) ); 

DECLARE @counter AS INT 

SELECT @counter = MAX([key]) 
FROM   #temptable  

WHILE @counter >= @checkcount 
  BEGIN 
      SELECT @StartOnHand = qtyonhand, 
             @QtyVariance = qtyvariance, 
             @Value = VALUE, 
             @ValueOnhand = valueonhand, 
             @Origin = origin,
             @MarkDownOnhand = QtyMarkdownOnHand  
      FROM   #temptable 
      WHERE  [key] = @checkcount 

      IF ( @Origin = 'Refunded Items (System Generated)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsRefunded = 
                  @PercentUnitsRefunded + ABS(@QtyVariance)                 
                  SET @PercentValueRefunded = 
                  @PercentValueRefunded + ABS(@Value) 
                  SET @PercentUnitRefundedCounter = 
                  @PercentUnitRefundedCounter + ABS( 
                  @StartOnHand)+@MarkDownOnhand    
                  SET @PercentValueRefundedCounter = 
                  @PercentValueRefundedCounter + ABS( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Audit Count Items' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsAudit = 
                  @PercentUnitsAudit + ABS(@QtyVariance)
                  SET @PercentValueAudit = 
                  @PercentValueAudit + ABS(@Value)
                  SET @PercentUnitAuditCounter = @PercentUnitAuditCounter + ABS 
                                                 ( 
                                                 @StartOnHand)+@MarkDownOnhand  
                  SET @PercentValueAuditCounter = @PercentValueAuditCounter + 
                                                  ABS(@ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Negative Stock Items (System Generated)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsNegative = 
                  @PercentUnitsNegative + ABS(@QtyVariance )
                  SET @PercentValueNegative = 
                  @PercentValueNegative + ABS(@Value)
                  SET @PercentUnitNegativeCounter = 
                  @PercentUnitNegativeCounter + ABS( 
                  @StartOnHand)+@MarkDownOnhand     
                  SET @PercentValueNegativeCounter = 
                  @PercentValueNegativeCounter + ABS( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Store Added Items (Manual Count Adjustments)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsStoreAdded = @PercentUnitsStoreAdded + 
                  ABS(@QtyVariance ) 
                  SET @PercentValueStoreAdded = 
                  @PercentValueStoreAdded + ABS(@Value )
                  SET @PercentUnitStoreAddedCounter = 
                  @PercentUnitStoreAddedCounter + ABS( 
                  @StartOnHand)+@MarkDownOnhand   
                  SET @PercentValueStoreAddedCounter = 
                  @PercentValueStoreAddedCounter + ABS( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Stock Loss Items (System Generated)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsStockLoss = 
                  @PercentUnitsStockLoss + ABS(@QtyVariance)
                  SET @PercentValueStockLoss = 
                  @PercentValueStockLoss + ABS(@Value)
                  SET @PercentUnitStockLossCounter = 
                  @PercentUnitStockLossCounter + ABS( 
                  @StartOnHand)+@MarkDownOnhand     
                  SET @PercentValueStockLossCounter = 
                  @PercentValueStockLossCounter + ABS( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'Parked Items (System Generated)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsParked = 
                  @PercentUnitsParked + ABS(@QtyVariance)
                  SET @PercentValueParked = 
                  @PercentValueParked + ABS(@Value)
                  SET @PercentUnitParkedCounter = @PercentUnitParkedCounter + 
                                                  ABS(@StartOnHand)+@MarkDownOnhand  
                  SET @PercentValueParkedCounter = 
                  @PercentValueParkedCounter + ABS( 
                  @ValueOnhand) 
              END 
        END 

      IF ( @Origin = 'STA Count Variances (Scheduled Count)' ) 
        BEGIN 
            IF @StartOnHand <> 0 
              BEGIN 
                  SET @PercentUnitsSTACount = 
                  @PercentUnitsSTACount + ABS(@QtyVariance)
                  SET @PercentValueSTACount = 
                  @PercentValueSTACount + ABS(@Value)                   
                  SET @PercentUnitSTACounter = @PercentUnitSTACounter + 
                                               ABS(@StartOnHand)+@MarkDownOnhand  
                  SET @PercentValueSTACounter = @PercentValueSTACounter + ABS( 
                                                @ValueOnhand) 
              END 
        END 

      SET @checkcount = @checkcount + 1 
  END 

SET @PercentUnitsAudit = [dbo].[udf_PicPercentageUnitsCalculation](@PercentUnitsAudit,@PercentUnitAuditCounter)                                               
SET @PercentUnitsNegative = [dbo].[udf_PicPercentageUnitsCalculation](@PercentUnitsNegative,@PercentUnitNegativeCounter) 
SET @PercentUnitsParked = [dbo].[udf_PicPercentageUnitsCalculation] (@PercentUnitsParked,@PercentUnitParkedCounter) 
SET @PercentUnitsRefunded =[dbo]. [udf_PicPercentageUnitsCalculation](@PercentUnitsRefunded,@PercentUnitRefundedCounter) 
SET @PercentUnitsSTACount =[dbo]. [udf_PicPercentageUnitsCalculation](@PercentUnitsSTACount,@PercentUnitSTACounter) 
SET @PercentUnitsStockLoss =[dbo]. [udf_PicPercentageUnitsCalculation](@PercentUnitsStockLoss,@PercentUnitStockLossCounter) 
SET @PercentUnitsStoreAdded = [dbo].[udf_PicPercentageUnitsCalculation](@PercentUnitsStoreAdded,@PercentUnitStoreAddedCounter) 
SET @PercentValueAudit = [dbo].[udf_PicPercentageUnitsCalculation](@PercentValueAudit,@PercentValueAuditCounter) 
SET @PercentValueNegative = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueNegative,@PercentValueNegativeCounter) 
SET @PercentValueParked = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueParked,@PercentValueParkedCounter) 
SET @PercentValueRefunded = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueRefunded,@PercentValueRefundedCounter) 
SET @PercentValueSTACount = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueSTACount,@PercentValueSTACounter) 
SET @PercentValueStockLoss = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueStockLoss,@PercentValueStockLossCounter) 
SET @PercentValueStoreAdded = [dbo].[Udf_picpercentagevaluecalculation](@PercentValueStoreAdded,@PercentValueStoreAddedCounter)  

DECLARE @summary TABLE(
		Origin			varchar(100),
		[Count]			int,
		QtyOnHand		dec(9,2),
		QtyShopFloor	int,
		QtyWarehouse	int,
		QtyPreSold		int,
		QtyMarkdownOnHand int,
		QtyMarkdown		int,
		QtyTotal		int,
		QtyVariance		dec(9,2),
		Value			dec(9,2),
		ValueOnHand		dec(9,2),	
		PercUnits		dec(9,2),
		PercValue		dec(9,2))
	
	--get summary info from @table
	INSERT INTO 
		@summary 
			(Origin,
			[Count],
			QtyOnHand,
			QtyShopFloor,
			QtyWarehouse,
			QtyPresold,
			QtyMarkdownOnHand,
			QtyMarkdown,
			QtyTotal,
			QtyVariance,
			Value,
			ValueOnHand)
	SELECT
		Origin,
		COUNT(origin),
		SUM(QtyOnHand),
		SUM(QtyShopFloor),
		SUM(QtyWarehouse),
		SUM(QtyPreSold),
		SUM(QtyMarkdownOnHand),
		SUM(QtyMarkdown),
		SUM(QtyTotal),
		SUM(QtyVariance),
		SUM(Value),
		SUM(ValueOnhand)
	FROM
		@table 
		WHERE (@VariancesOnly = 0 OR (@VariancesOnly = 1 AND QtyVariance <> 0))
	GROUP BY
		Origin
		
	IF EXISTS(SELECT 1 
			  FROM   @summary 
			  WHERE  origin = 'Refunded Items (System Generated)') 
	  BEGIN 
		  UPDATE @summary 
		  SET    percunits = @PercentUnitsRefunded 
		  WHERE  origin = 'Refunded Items (System Generated)' 

		  UPDATE @summary 
		  SET    percvalue = @PercentValueRefunded 
		  WHERE  origin = 'Refunded Items (System Generated)' 
	  END 

	IF EXISTS(SELECT 1 
			  FROM   @summary 
			  WHERE  origin = 'Audit Count Items') 
	  BEGIN 
		  UPDATE @summary 
		  SET    percunits = @PercentUnitsAudit 
		  WHERE  origin = 'Audit Count Items' 

		  UPDATE @summary 
		  SET    percvalue = @PercentValueAudit 
		  WHERE  origin = 'Audit Count Items' 
	  END 

	IF EXISTS(SELECT 1 
			  FROM   @summary 
			  WHERE  origin = 'Negative Stock Items (System Generated)') 
	  BEGIN 
		  UPDATE @summary 
		  SET    percunits = @PercentUnitsNegative 
		  WHERE  origin = 'Negative Stock Items (System Generated)' 

		  UPDATE @summary 
		  SET    percvalue = @PercentValueNegative 
		  WHERE  origin = 'Negative Stock Items (System Generated)' 
	  END 

	IF EXISTS(SELECT 1 
			  FROM   @summary 
			  WHERE  origin = 'Store Added Items (Manual Count Adjustments)') 
	  BEGIN 
		  UPDATE @summary 
		  SET    percunits = @PercentUnitsStoreAdded 
		  WHERE  origin = 'Store Added Items (Manual Count Adjustments)' 

		  UPDATE @summary 
		  SET    percvalue = @PercentValueStoreAdded 
		  WHERE  origin = 'Store Added Items (Manual Count Adjustments)' 
	  END 

	IF EXISTS(SELECT 1 
			  FROM   @summary 
			  WHERE  origin = 'Stock Loss Items (System Generated)') 
	  BEGIN 
		  UPDATE @summary 
		  SET    percunits = @PercentUnitsStockLoss 
		  WHERE  origin = 'Stock Loss Items (System Generated)' 

		  UPDATE @summary 
		  SET    percvalue = @PercentValueStockLoss 
		  WHERE  origin = 'Stock Loss Items (System Generated)' 
	  END 

	IF EXISTS(SELECT 1 
			  FROM   @summary 
			  WHERE  origin = 'Parked Items (System Generated)') 
	  BEGIN 
		  UPDATE @summary 
		  SET    percunits = @PercentUnitsParked 
		  WHERE  origin = 'Parked Items (System Generated)' 

		  UPDATE @summary 
		  SET    percvalue = @PercentValueParked 
		  WHERE  origin = 'Parked Items (System Generated)' 
	  END 

	IF EXISTS(SELECT 1 
			  FROM   @summary 
			  WHERE  origin = 'STA Count Variances (Scheduled Count)') 
	  BEGIN 
		  UPDATE @summary 
		  SET    percunits = @PercentUnitsSTACount 
		  WHERE  origin = 'STA Count Variances (Scheduled Count)' 

		  UPDATE @summary 
		  SET    percvalue = @PercentValueSTACount 
		  WHERE  origin = 'STA Count Variances (Scheduled Count)' 
	  END  
	  
	SELECT * FROM @summary;

	DECLARE @signature TABLE(Signature1	varchar(200), Signature2 varchar(200))
	INSERT INTO @signature VALUES('STA Signature','Managers Signature') ;
	INSERT INTO @signature VALUES('Date','Date') ;
	
	SELECT * FROM @signature;

	SELECT TOP(1) @Authorised = RTRIM(s.Name) 
	FROM dbo.HHTHDR h
	LEFT JOIN dbo.SystemUsers s ON h.AUTH = s.EmployeeCode
	WHERE h.DATE1 = CONVERT(date, @Date);
		
	IF (LEN(@Authorised) < 1 OR @Authorised IS NULL)
		BEGIN
			SET @Authorised = 'NOT AUTHORISED'
		END

	DECLARE @sigfooter TABLE(Signature3 varchar(400))
	INSERT INTO @sigfooter VALUES ('Authorised By: <' + rtrim(@Authorised) + '>');
	
	SELECT * FROM @sigfooter ;

END
GO

IF @@Error = 0
   PRINT 'Success: The Alter Stored Procedure PicVarianceReport for US26465 has been deployed successfully'
ELSE
   PRINT 'Failure: The Alter Stored Procedure PicVarianceReport for US264652 has not been deployed'
GO