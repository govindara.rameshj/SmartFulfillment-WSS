IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'StockGetPicStockCounts') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure StockGetPicStockCounts'
	EXEC ('CREATE PROCEDURE dbo.StockGetPicStockCounts AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure StockGetPicStockCounts')
GO

ALTER PROCEDURE [dbo].[StockGetPicStockCounts]
@SkuNumber CHAR (6)
AS
BEGIN
	SET NOCOUNT ON;

	
	select
		hd.DATE1							as 'Date',
		hd.SKUN								as 'SkuNumber',
		hd.INON								as 'IsNonStock',
		hd.ONHA								as 'QtyOnHand',
		hd.TPRE								as 'QtyPreSold',
		hd.TSCN								as 'QtyShopFloor',
		hd.TWCN								as 'QtyWarehouse',
		hd.MDNQ								as 'StartM/Down',
		hd.TMDC								as 'M/DownCount',
		coalesce(SUM(hl.StockCount),0)		as 'StockCount',
		hd.ICNT								as 'IsCounted',
		hd.IADJ								as 'IsAdjusted',
		hd.TSCN+hd.TWCN - hd.TPRE - hd.ONHA	as 'Code2',	
		(hd.TMDC-hd.MDNQ)					as 'Code53',		
		(select case hd.ORIG
			when 'H' then 'Historic'
			when 'R' then 'Refund'
			when 'A' then 'Audit'
			when 'S' then 'Store'
			else 'PIC'
		end) 							as 'Origin'
	from
		HHTDET hd
	left outer join
		HhtLocation hl on hl.DateCreated = hd.DATE1 and hl.SkuNumber = hd.SKUN
	where
		hd.SKUN = @SkuNumber
	group by
		hd.DATE1,
		hd.SKUN,
		hd.INON,
		hd.ONHA,
		hd.TPRE,
		hd.TSCN,
		hd.TWCN,
		hd.MDNQ,
		hd.TMDC,
		hd.ICNT,
		hd.IADJ,
		hd.ORIG
	order by
		hd.DATE1 desc
END

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure StockGetPicStockCounts for US13912 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure StockGetPicStockCounts for US13912 has not been deployed'
GO

