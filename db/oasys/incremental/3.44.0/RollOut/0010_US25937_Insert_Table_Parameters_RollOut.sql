INSERT INTO [dbo].[Parameters]
	([ParameterID]
	,[Description]
	,[StringValue]
	,[LongValue]
	,[BooleanValue]
	,[DecimalValue]
	,[ValueType])
SELECT 
		 [ParameterID]
		,[Description]
		,[StringValue]
		,0   as [LongValue]
		,0   as [BooleanValue]
		,0.0 as [DecimalValue]
		,0   as [ValueType]
FROM (
	SELECT  985 as [ParameterID], cast('Not show ID Cashier' as varchar(50)) as [Description], cast('499' as varchar(80)) as [StringValue]
	UNION ALL SELECT 5500, 'Log files place, default is All users App data', ''
	UNION ALL SELECT 5501, 'Log file size, MB', '2'
	UNION ALL SELECT 5502, 'Log archive size, MB', '500'
	UNION ALL SELECT 5503, 'Log from, yyyy-MM-dd hh:mm:ss', '2000-01-01 00:00:00'
	UNION ALL SELECT 5504, 'Log to, yyyy-MM-dd hh:mm:ss', '2001-01-01 00:00:00'
	UNION ALL SELECT 5505, 'Log Tills, comma separated, default is all', ''
	UNION ALL SELECT 5506, 'Log zipping period HH:MM:SS, default is 1 min', '00:01:00'
) t
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] prm WHERE prm.ParameterID = t.ParameterID)      
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US25937 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US25937 has not been deployed successfully'
Go