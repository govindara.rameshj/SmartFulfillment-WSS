delete Parameters where ParameterID = -22011
go

if @@error = 0
   print 'Success: The metadata change for PO22-011 has been successfully rolled back'
else
   print 'Failure: The metadata change for PO22-011 has NOT been successfully rolled back'
go