-- =============================================
-- Author        : Alan Lewis
-- Create date   : 06/11/2012
-- User Story	 : 9019: Iteration 18 Rework for pilot
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 9020
-- Description   : Drop udf to read parameter setting for
--				 : comments delimiter.
-- =============================================
Drop Function [dbo].[udf_GetCommentsDelimiter]
Go
If @@Error = 0
   Print 'Success: The user defined function "udf_GetCommentsDelimiter" has been sucessfully dropped for P022-017'
Else
   Print 'Failure: The user defined function "udf_GetCommentsDelimiter" might NOT Been dropped for P022-017'
Go
