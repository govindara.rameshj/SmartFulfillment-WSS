DELETE FROM	Parameters Where ParameterID = -22035
Go
If @@Error = 0
   Print 'Success: The active requirement switch for P022-035 has been successfully Rolled Back'
Else
   Print 'Failure: active requirement switch for P022-035 has NOT been Rolled Back'
Go
