Delete [Oasys].[dbo].[ReportColumns] Where reportID=500 and Columnid = 1200
GO
If @@Error = 0
   Print 'Success: The Report Value Column has been successfully rolled back'
Else
   Print 'Failure: The Report Value has NOT been rolled back'
Go
Delete [Oasys].[dbo].[ReportColumns] Where reportID=500 and Columnid = 303
GO
If @@Error = 0
   Print 'Success: The Report Supplier Name Column has been successfully ROLLED BACK'
Else
   Print 'Failure: The Report Supplier Name has NOT been ROLLED BACK'
Go
Delete [Oasys].[dbo].[ReportSummary] Where reportID=500 and Columnid = 1200
GO
If @@Error = 0
   Print 'Success: The Report Summary has been successfully rolled back'
Else
   Print 'Failure: The Report Summary has NOT been rolled back'
Go
UPDATE [Oasys].[dbo].[MenuConfig]
   SET [ID] = 4230
      ,[MasterID] = 4200
      ,[AppName] = 'Open Returns'
      ,[AssemblyName] = 'Reporting.dll'
      ,[ClassName] = 'Reporting.ReportViewer'
      ,[MenuType] = 4
      ,[Parameters] = '500'
      ,[ImagePath] = 'icons\icon_g.ico'
      ,[LoadMaximised] = 1
      ,[IsModal] = 1
      ,[DisplaySequence] = 2
      ,[AllowMultiple] = 0
      ,[WaitForExit] = 0
      ,[TabName] = NULL
      ,[Description] = NULL
      ,[ImageKey] = 0
      ,[Timeout] = 0
      ,[DoProcessingType] = 1
 WHERE ID=4230
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Menu 1 has been successfully rolled back'
Else
   Print 'Failure: The update script for Open Returns Menu 1 has NOT been rolled back'
GO
UPDATE [Oasys].[dbo].[MenuConfig]
   SET [ID] = 6120
      ,[MasterID] = 6100
      ,[AppName] = 'Open Returns'
      ,[AssemblyName] = 'Reporting.dll'
      ,[ClassName] = 'Reporting.ReportViewer'
      ,[MenuType] = 4
      ,[Parameters] = '500'
      ,[ImagePath] = 'icons\icon_g.ico'
      ,[LoadMaximised] = 1
      ,[IsModal] = 1
      ,[DisplaySequence] = 2
      ,[AllowMultiple] = 0
      ,[WaitForExit] = 0
      ,[TabName] = NULL
      ,[Description] = NULL
      ,[ImageKey] = 0
      ,[Timeout] = 0
      ,[DoProcessingType] = 1
 WHERE ID=6120
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Menu 2 has been successfully rolled back'
Else
   Print 'Failure: The update script for Open Returns Menu 2 has NOT been rolled back'
GO
UPDATE [Oasys].[dbo].[MenuConfig]
   SET [ID] = 11440
      ,[MasterID] = 11400
      ,[AppName] = 'Open Returns'
      ,[AssemblyName] = 'Reporting.dll'
      ,[ClassName] = 'Reporting.ReportViewer'
      ,[MenuType] = 4
      ,[Parameters] = '500'
      ,[ImagePath] = 'icons\icon_g.ico'
      ,[LoadMaximised] = 1
      ,[IsModal] = 1
      ,[DisplaySequence] = 2
      ,[AllowMultiple] = 0
      ,[WaitForExit] = 0
      ,[TabName] = NULL
      ,[Description] = NULL
      ,[ImageKey] = 0
      ,[Timeout] = 0
      ,[DoProcessingType] = 1
 WHERE ID=11440
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Menu 3 has been successfully rolled back'
Else
   Print 'Failure: The update script for Open Returns Menu 3 has NOT been rolled back'
GO
UPDATE [Oasys].[dbo].[MenuConfig]
   SET [ID] = 11630
      ,[MasterID] = 11600
      ,[AppName] = 'Open Returns'
      ,[AssemblyName] = 'Reporting.dll'
      ,[ClassName] = 'Reporting.ReportViewer'
      ,[MenuType] = 4
      ,[Parameters] = '500'
      ,[ImagePath] = 'icons\icon_g.ico'
      ,[LoadMaximised] = 1
      ,[IsModal] = 1
      ,[DisplaySequence] = 2
      ,[AllowMultiple] = 0
      ,[WaitForExit] = 0
      ,[TabName] = NULL
      ,[Description] = NULL
      ,[ImageKey] = 0
      ,[Timeout] = 0
      ,[DoProcessingType] = 1
 WHERE ID=11630
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Menu 4 has been successfully rolled back'
Else
   Print 'Failure: The update script for Open Returns Menu 4 has NOT been rolled back'
GO
UPDATE [Oasys].[dbo].[ReportColumn]
   SET [Id] = 303
      ,[Name] = 'SupplierName'
      ,[Caption] = 'Supplier Name'
      ,[FormatType] = 0
      ,[Format] = NULL
      ,[IsImagePath] = 0
      ,[MinWidth] = 100
      ,[MaxWidth] = NULL
      ,[Alignment] = NULL
 WHERE Id=303
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Supplier Column has been successfully rolled back'
Else
   Print 'Failure: The update script for Open Returns Supplier Column has NOT been rolled back'
GO
UPDATE [Oasys].[dbo].[ReportColumn]
   SET [Id] = 1200
      ,[Name] = 'Value'
      ,[Caption] = 'Value'
      ,[FormatType] = 1
      ,[Format] = 'c2'
      ,[IsImagePath] = 0
      ,[MinWidth] = 55
      ,[MaxWidth] = NULL
      ,[Alignment] = 2
 WHERE Id=1200
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Value Column has been successfully rolled back'
Else
   Print 'Failure: The update script for Open Returns Value Column has NOT been rolled back'
GO
 
  