drop procedure NewBankingGetLatestSafe

GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [NewBankingGetLatestSafe] for Easter Banking has been successfully dropped';
    END
ELSE
    BEGIN
        PRINT 'Failure: [NewBankingGetLatestSafe] for Easter Banking has NOT been successfully dropped';
    END;
GO
