-- =============================================
-- Author        : Alan Lewis
-- Create date   : 17/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7970
-- Description   : Drop new NewBankingSafeBagGet stored procedure.
-- =============================================
Drop Procedure
	[dbo].[NewBankingSafeBagGet]
Go

If @@Error = 0
   Print 'Success: The new stored procedure "NewBankingSafeBagGet" for P022-017 has been sucessfully dropped'
Else
   Print 'Failure: The new stored procedure "NewBankingSafeBagGet" for P022-017 has NOT been dropped'
Go
