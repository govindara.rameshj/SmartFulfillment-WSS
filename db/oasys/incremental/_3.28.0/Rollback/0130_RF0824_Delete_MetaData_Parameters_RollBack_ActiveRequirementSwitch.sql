DELETE FROM	Parameters Where ParameterID = -824
Go
If @@Error = 0
   Print 'Success: The active requirement switch for RF0824 has been successfully Rolled Back'
Else
   Print 'Failure: active requirement switch for RF0824 has NOT been Rolled Back'
Go
