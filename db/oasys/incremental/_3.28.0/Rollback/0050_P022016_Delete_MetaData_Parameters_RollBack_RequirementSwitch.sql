delete Parameters where ParameterID = -22016
go

if @@error = 0
   print 'Success: The metadata change for PO22-016 has been successfully rolled back'
else
   print 'Failure: The metadata change for PO22-016 has NOT been successfully rolled back'
go