-- =============================================
-- Author        : Alan Lewis
-- Create date   : 07/11/2012
-- User Story	 : 9019: Iteration 18 Rework for pilot
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 9020
-- Description   : Drop new parameter specifying a Comments Delimiter.
-- =============================================
Delete
	[Parameters]
Where
	ParameterID = 2210
Go

If @@Error = 0
   Print 'Success: The Banking Comments Delimiter parameter for P022-017 has been successfully deleted'
Else
   Print 'Failure: Banking Comments Delimiter parameter for P022-017 might NOT been deleted'
Go
