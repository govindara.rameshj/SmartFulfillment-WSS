alter procedure NewBankingEndOfDayCheckReport

   @BankingDate datetime

as
begin

   set nocount on

   declare @PeriodID              int
   declare @ProcessID             int
   
   declare @ScannedPickupBagCount int

   declare @BankingCashBag        table(ID int)
   declare @BankingChequeBag      table(ID int)

   declare @PickupBag             table(SealNumber char(20), Value decimal(9, 2), Comment nvarchar(max))
   declare @PickupBagNull         table(SealNumber char(20), Comment nvarchar(max))

   declare @BankingBagCash        table(SealNumber char(20), Value decimal(9, 2), Comment nvarchar(max))
   declare @BankingBagCheque      table(SealNumber char(20), Value decimal(9, 2), Comment nvarchar(max))
   declare @BankingBagNull        table(SealNumber char(20), Comment nvarchar(max))

   declare @PickupComment         table(ID int, SafeBagsType nchar(1), Comment nvarchar(max))
   declare @BankingComment        table(ID int, SafeBagsType nchar(1), Comment nvarchar(max))

   declare @AuthoriserName        table(ManagerName varchar(50), WitnessName varchar(50), EndOfDayCheckLockedFrom datetime, EndOfDayCheckLockedTo datetime)

   declare @Output                table(RowId int,
                                        CategoryDescription  nvarchar(max),
                                        SealNumber           nvarchar(max),
                                        Value                nvarchar(max),
                                        Comment              nvarchar(max),
                                        EmboldenThis         bit default 0)

   set @PeriodID  = (select ID from SystemPeriods where datediff(day, @BankingDate, StartDate) = 0)
   set @ProcessID = (select ID from BusinessProcess where [Description] = 'End Of Day Check Process')

   if not exists(select * from Safe where PeriodID = @PeriodID)
   begin
   
      insert @Output values(null, 'Safe Not Found', null, null, null, 1)
   
   end
   else
   begin

      insert @BankingCashBag   select BagID from SafeBagsDenoms where TenderID = 1 group by BagID
      insert @BankingChequeBag select BagID from SafeBagsDenoms where TenderID = 2 group by BagID

      insert @PickupBag        select b.SealNumber, b.Value, a.Comment
                               from SafeBagScanned a
                               inner join SafeBags b
                                     on b.ID = a.SafeBagsID
                               where a.SafePeriodID      = @PeriodID
                               and   a.BusinessProcessId = @ProcessID
                               and   b.[Type]            = 'P'

      insert @PickupBagNull    select SealNumber, Comment
                               from SafeBagScanned 
                               where SafePeriodID      = @PeriodID
                               and   BusinessProcessId = @ProcessID
                               and   SafeBagsId       is null
                               and	 BagType           = 'P'

      insert @BankingBagCash   select b.SealNumber, b.Value, a.Comment
                               from SafeBagScanned a
                               inner join SafeBags b
                                     on b.ID = a.SafeBagsID
                               where a.SafePeriodID      = @PeriodID
                               and   a.BusinessProcessId = @ProcessID
                               and   a.SafeBagsID       in (select ID from @BankingCashBag)
                               and   b.[Type]            = 'B'

      insert @BankingBagCheque select b.SealNumber, b.Value, a.Comment
                               from SafeBagScanned a
                               inner join SafeBags b
                                     on b.ID = a.SafeBagsID
                               where a.SafePeriodID      = @PeriodID
                               and   a.BusinessProcessId = @ProcessID
                               and   a.SafeBagsID       in (select ID from @BankingChequeBag)
                               and   b.[Type]            = 'B'

      insert @BankingBagNull   select SealNumber, Comment
                               from SafeBagScanned 
                               where SafePeriodID      = @PeriodID
                               and   BusinessProcessId = @ProcessID
                               and   SafeBagsId       is null
                               and	 BagType           = 'B'

      insert @PickupComment    select ID, BagType, Comment from SafeComment where SafePeriodID = @PeriodID and BagType = 'P' and BusinessProcessId = @ProcessID
      insert @BankingComment   select ID, BagType, Comment from SafeComment where SafePeriodID = @PeriodID and BagType = 'B' and BusinessProcessId = @ProcessID

      insert @AuthoriserName   select b.Name, c.Name, a.EndOfDayCheckLockedFrom, a.EndOfDayCheckLockedTo 
                               from Safe a
                               inner join SystemUsers b
                                     on b.ID = a.EndOfDayCheckManagerID 
                               inner join SystemUsers c
                                     on c.ID = a.EndOfDayCheckWitnessID 
                               where a.PeriodID = @PeriodId

      set @ScannedPickupBagCount = (select count(*) from @PickupBag) + (select count(*) from @PickupBagNull)

      insert @Output values(1, 'End Of Day Check Info', 'Summary', null, null, 1)
      insert @Output values(2, null, 'Manager Check ID', (select ManagerName                                    from @AuthoriserName), null, 0)
      insert @Output values(3, null, 'Witness Check ID', (select WitnessName                                    from @AuthoriserName), null, 0)
      insert @Output values(4, null, 'Safe Locked From', (select cast(EndOfDayCheckLockedFrom as nvarchar(max)) from @AuthoriserName), null, 0)
      insert @Output values(5, null, 'Safe Locked To',   (select cast(EndOfDayCheckLockedTo as nvarchar(max))   from @AuthoriserName), null, 0)

      insert @Output values(10, 'Bag Counts', 'Summary', null, null,1)
      insert @Output values(11, null, 'Pickup Bag Physical Count',         cast(@ScannedPickupBagCount as nvarchar(max)),                                                                                                                                      null, 0)
      insert @Output values(12, null, 'Pickup Bag System Count',           (select cast(count(*) as varchar(max)) from dbo.udf_PickupBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID))),                                                 null, 0)
      insert @Output values(13, null, 'Banking Bag Cash Physical Count',   (select cast(count(*) as varchar(max)) from @BankingBagCash),                                                                                                                       null, 0)
      insert @Output values(14, null, 'Banking Bag Cash System Count',     (select cast(count(*) as varchar(max)) from dbo.udf_BankingBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID)) where ID in (select ID from @BankingCashBag)),   null, 0)
      insert @Output values(15, null, 'Banking Bag Cheque Physical Count', (select cast(count(*) as varchar(max)) from @BankingBagCheque),                                                                                                                     null, 0)
      insert @Output values(16, null, 'Banking Bag Cheque System Count',   (select cast(count(*) as varchar(max)) from dbo.udf_BankingBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID)) where ID in (select ID from @BankingChequeBag)), null, 0)

      insert @Output values(20, 'Pickup Section', 'Seal Number', 'Value', null,1)
      insert @Output select 21, null, rtrim(SealNumber), Value, Comment, 0 from @PickupBag
      insert @Output select 22, null, rtrim(SealNumber), null,  Comment, 0 from @PickupBagNull
      insert @Output select 23, null, null,              null,  Comment, 0 from @PickupComment

      insert @Output values(30, 'Banking Section','Seal Number', 'Value', null,1)
      insert @Output select 31, null, rtrim(SealNumber), Value, Comment, 0 from @BankingBagCash
      insert @Output select 32, null, rtrim(SealNumber), Value, Comment, 0 from @BankingBagCheque
      insert @Output select 33, null, rtrim(SealNumber), null,  Comment, 0 from @BankingBagNull
      insert @Output select 34, null, null,              null,  Comment, 0 from @BankingComment

   end

   select * from @Output order by RowId

end
go

if @@error = 0
   print 'Success: Stored procedure NewBankingEndOfDayCheckReport for P022-012 has been successfully rolled back'
else
   print 'Failure: Stored procedure NewBankingEndOfDayCheckReport for P022-012 has NOT been successfully rolled back'
go