-- =============================================
-- Author        : Alan Lewis
-- Create date   : 18/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : When rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7971
-- Description   : Drop SafeBag Comments column from returned data.
-- =============================================
ALTER procedure [dbo].[NewBankingFloatList]
   @PeriodID int
as
begin
set nocount on
declare @StartFloatID        int

declare @CursorFloatID       int
declare @CursorStartFloatID  int

declare @Temp table(FloatID            int,
                    FloatSealNumber    char(20),
                    FloatValue         decimal(9,2),
                    AssignedToUserName varchar(50),
                    AssignedToUserID   int,
                    SaleTaken          bit,
                    FloatChecked       bit,
                    StartFloatID       int)

insert @Temp --cashier accountability model only
             --unassigned floats - ignore the PeriodID when the float was created
             select FloatID            = ID,
                    FloatSealNumber    = SealNumber,
                    FloatValue         = Value,
                    AssignedToUserName = '',
                    AssignedToUserID   = cast(null as int),
                    SaleTaken          = cast(0 as bit),
                    FloatChecked       = isnull(FloatChecked, 0),
                    StartFloatID       = null
             from SafeBags
             where [Type]  = 'F'
             and   [State] = 'S'
             union all
             --assigned floats - ignore the PeriodID when the float was created
             --                  the OutPeriodID will indicate if the float was assigned on this PeriodID
             select FloatID            = a.ID,
                    FloatSealNumber    = a.SealNumber,
                    FloatValue         = a.Value,
                    AssignedToUserName = b.Name,
                    AssignedToUserID   = b.ID,
                    /*
                    SaleTaken          = cast((case isnull((select NumTransactions
                                                            from CashBalCashier
                                                            where PeriodID    = @PeriodID 
                                                            and   CurrencyID  = (select ID from SystemCurrency where IsDefault = 1)
                                                            and   CashierID   = a.AccountabilityID), 0)
                                                  when 0 then 0
                                                  else 1
                                               end) as bit),
                    */
                    --no valid sales e.g invalid "vision deposit"
                    SaleTaken          = cast((case isnull((select NumTransactions
                                                            from CashBalCashier
                                                            where PeriodID    = @PeriodID 
                                                            and   CurrencyID  = (select ID from SystemCurrency where IsDefault = 1)
                                                            and   CashierID   = a.AccountabilityID
                                                            and  (GrossSalesAmount <> 0 or (select count(*)
                                                                                            from CashBalCashierTen
                                                                                            where PeriodID   = @PeriodID 
                                                                                            and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                                                                            and   CashierID  = a.AccountabilityID) > 0)), 0)
                                                  when 0 then 0
                                                  else 1
                                               end) as bit),
                    FloatChecked       = isnull(a.FloatChecked, 0),
                    StartFloatID       = null
             from SafeBags a
             inner join SystemUsers b
                   on b.ID = a.AccountabilityID
             where a.[Type]      = 'F'
             and   a.[State]     = 'R'
             and   a.OutPeriodID = @PeriodID 

--"manual check" & un-assigned floats need to maintain its link to the pickup that created it

declare TempCursor cursor for
select FloatID, StartFloatID from @Temp
open TempCursor
fetch next from TempCursor into @CursorFloatID, @CursorStartFloatID
while @@fetch_status <> -1
begin
   exec StartingFloat @CursorFloatID, @StartFloatID output

   update @Temp set StartFloatID = @StartFloatID where FloatID = @CursorFloatID

   --next record
   fetch next from TempCursor into @CursorFloatID, @CursorStartFloatID
end
close TempCursor
deallocate TempCursor

select a.FloatID,
       a.FloatSealNumber,
       a.FloatValue,
       FloatCreatedFromUserName    = isnull(c.Name, ''),
       FloatCreatedFromPickupBagID = b.ID,
       a.AssignedToUserName,
       a.AssignedToUserID,
       a.SaleTaken,
       a.FloatChecked
from @Temp a
--float created from a pickup bag
left outer join (select * from SafeBags where [Type] = 'P' and [State] <> 'C') b
           on b.RelatedBagId = a.StartFloatID
left outer join SystemUsers c
           on c.ID = b.AccountabilityID
end
Go

If @@Error = 0
   Print 'Success: The stored procedure "NewBankingFloatList" has been successfully rolled back to no longer return safebag comment for P022-017'
Else
   Print 'Failure: The stored procedure "NewBankingFloatList" has NOT been rolled back to no longer return safebag comment for P022-017'
Go
