-- =============================================
-- Author        : Alan Lewis
-- Create date   : 14/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7969
-- Description   : Drop the SafeBag.Comments column from the
--				 : returned data.
-- =============================================
ALTER procedure [dbo].[NewBankingFloatedCashierDisplay]
   @PeriodID int
as
begin
   set nocount on
   --cashier accountability model only
   --floated cashiers no end of day pickup
   select StartFloatChecked              = isnull(a.FloatChecked, 0),
          StartFloatValue                = a.Value,
          StartFloatSealNumber           = a.SealNumber,
          StartFloatAssignedToUserID     = b.ID,
          StartFloatAssignedToUserName   = b.Name,
          StartFloatAssignedByUserID     = c.ID,
          StartFloatAssignedByUserName   = c.Name,
          TillVariance                   = cast(null as decimal(9,2)),
          NewFloatSealNumber             = '',
          PickupSealNumber               = '',
          PickupAssignedByUserID         = cast(null as int),
          PickupAssignedByUserName       = '',
          PickupAssignedBySecondUserID   = cast(null as int),
          PickupAssignedBySecondUserName = ''
   from SafeBags a
   --cashier
   inner join SystemUsers b
         on b.ID = a.AccountabilityID
   --float assigned by
   inner join SystemUsers c
         on c.ID = a.OutUserID1
   --filter out if end of day pickup bag exist for this cashier
   left outer join (select *
                    from SafeBags
                    where Type              = 'P'
                    and   [State]          <> 'C'
                    and isnull(CashDrop, 0) = 0) e
              on  e.PickupPeriodID   = a.OutPeriodID
              and e.AccountabilityID = a.AccountabilityID
   where a.[Type]      = 'F'
   and   a.[State]     = 'R'
   and   a.OutPeriodID = @PeriodID
   and   e.ID is null
   union all
   --floated cashiers with pickup bags
   select StartFloatChecked              = isnull(e.FloatChecked, 0),
          StartFloatValue                = e.Value,
          StartFloatSealNumber           = isnull(e.SealNumber, ''),
          StartFloatAssignedToUserID     = f.ID,
          StartFloatAssignedToUserName   = isnull(f.Name, ''),
          StartFloatAssignedByUserID     = g.ID,
          StartFloatAssignedByUserName   = isnull(g.Name, ''),
          TillVariance                   = (select sum(Value)                                       --total pickup (cash drops & e.o.d pickup)
                                            from SafeBags
                                            where [Type]           = 'P'
                                            and   [State]         <> 'C'
                                            and   PickupPeriodID   = @PeriodID
                                            and   AccountabilityID = a.AccountabilityID) +
                                           (select Value                                            --new float
                                            from SafeBags
                                            where ID = (select top 1 RelatedBagId
                                                        from SafeBags
                                                        where [Type]           = 'P'
                                                        and   [State]         <> 'C'
                                                        and   PickupPeriodID   = @PeriodID
                                                        and   AccountabilityID = a.AccountabilityID
                                                        and   RelatedBagId    <> 0)) -
                                            e.Value -                                               --start float
                                           (select GrossSalesAmount                                 --system sales
                                            from CashBalCashier
                                            where PeriodID   = @PeriodID
                                            and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                            and   CashierID  = a.AccountabilityID),
          NewFloatSealNumber             = isnull(d.SealNumber, ''),
          PickupSealNumber               = a.SealNumber,
          PickupAssignedByUserID         = b.ID,
          PickupAssignedByUserName       = b.Name,
          PickupAssignedBySecondUserID   = c.ID,
          PickupAssignedBySecondUserName = c.Name
   from SafeBags a
   --assigned by
   inner join SystemUsers b
         on b.ID = a.InUserID1
   --assigned by second check
   inner join SystemUsers c
         on c.ID = a.InUserID2 
   --new float
   left outer join SafeBags d
              on d.ID = a.RelatedBagId
   --start float
   inner join (select * from SafeBags where Type = 'F' and State = 'R') e
         on  e.OutPeriodID      = a.PickupPeriodID
         and e.AccountabilityID = a.AccountabilityID
   inner join SystemUsers f
         on f.ID = e.AccountabilityID
   inner join SystemUsers g
         on g.ID = e.OutUserID1
   where a.[Type]              = 'P'
   and   a.[State]            <> 'C'
   and   a.PickupPeriodID      = @PeriodID
   and   isnull(a.CashDrop, 0) = 0         --end of day pickup bag, should only have one entry only
end
Go

If @@Error = 0
   Print 'Success: The stored procedure "NewBankingFloatedCashierDisplay" has been sucessfully rolled back'
Else
   Print 'Failure: The stored procedure "NewBankingFloatedCashierDisplay" has NOT been rolled back'
Go
