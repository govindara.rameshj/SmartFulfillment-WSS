INSERT INTO [Oasys].[dbo].[ReportParameters]
           ([ReportId]
           ,[ParameterId]
           ,[Sequence]
           ,[AllowMultiple]
           ,[DefaultValue])
     VALUES
           (500
           ,100
           ,2
           ,0
           ,NULL)
GO
If @@Error = 0
   Print 'Success: The date Created Parameter has been successfully rolled back'
Else
   Print 'Failure: The date Created Parameter has NOT been rolled back'
Go
