-- =============================================
-- Author        : Alan Lewis
-- Create date   : 06/11/2012
-- User Story	 : 9019: Iteration 18 Rework for pilot
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 9020
-- Description   : Delete the new procedure that uses same udf used
--				 : in stored procedure NewBankingFloatedCashierDisplay
--				 : to get the Comments Delimiter.
-- =============================================
Drop Procedure [dbo].[NewBankingGetCommentsDelimiter]
Go

If @@Error = 0
   Print 'Success: The Stored Procedure "NewBankingGetCommentsDelimiter" has been sucessfully dropped for P022-017'
Else
   Print 'Failure: The Stored Procedure "NewBankingGetCommentsDelimiter" might NOT have Been dropped for P022-017'
Go
