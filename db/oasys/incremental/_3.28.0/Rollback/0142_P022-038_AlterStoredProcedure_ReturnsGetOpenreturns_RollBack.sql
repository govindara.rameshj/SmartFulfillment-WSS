ALTER PROCEDURE [dbo].[ReturnsGetOpenReturns]
	@SupplierNumber		VARCHAR(5)	=Null,
	@Date				DATETIME	=Null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		NUMB as 'Number'
		,SUPP as 'SupplierNumber'
		,EDAT as 'DateCreated'
		,RDAT as 'DateCollect'
		,DRLN as 'DrlNumber'
	FROM 
		RETHDR 
	WHERE 
			DRLN = ('000000')
		AND EDAT >= @Date	
		And (@SupplierNumber is null or SUPP = @SupplierNumber)
		
END
Go
If @@Error = 0
   Print 'Success: The change to stored procedure ReturnGetOpenReturns has been successfully rolled back'
Else
   Print 'Failure: The change to stored procedure ReturnGetOpenReturns has NOT been rolled back'
Go