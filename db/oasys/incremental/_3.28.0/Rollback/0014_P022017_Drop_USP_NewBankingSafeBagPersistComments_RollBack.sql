-- =============================================
-- Author        : Alan Lewis
-- Create date   : 02/10/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7975
-- Description   : Delete the new NewBankingSafeBagPersistComments
--				 : stored procedure.
-- =============================================
Drop Procedure 
	[dbo].[NewBankingSafeBagPersistComments]
Go

If @@Error = 0
   Print 'Success: The new stored procedure "NewBankingSafeBagPersistComments" for P022-017 has been sucessfully deleted'
Else
   Print 'Failure: The new stored procedure "NewBankingSafeBagPersistComments" for P022-017 might NOT been deleted'
Go
