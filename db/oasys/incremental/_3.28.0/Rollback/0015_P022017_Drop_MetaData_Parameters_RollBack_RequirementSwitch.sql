-- =============================================
-- Author        : Alan Lewis
-- Create date   : 14/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7969
-- Description   : Drop active requirement switch
-- =============================================
Delete
	Parameters
Where
	ParameterID = -22017
Go

If @@Error = 0
   Print 'Success: The active requirement switch for P022-017 has been successfully dropped'
Else
   Print 'Failure: active requirement switch for P022-017 has NOT been dropped'
Go
