alter procedure NewBankingSafeMaintenanceBagsHeld
   @PeriodID int
as
begin
set nocount on
--float / pickup / banking bags
select BagID      = ID,
       BagType    = [Type],
       SealNumber = SealNumber,
       BagValue   = Value 
into #Temp
from SafeBags
where [Type]  in ('F', 'P', 'B')
and   [State] <> 'C' 
and ((InPeriodID = @PeriodID)                               or   --all non-cancelled bags for this period
      (InPeriodID < @PeriodID and OutPeriodID = 0)          or   --all non-cancelled bags before this period that has no been used
      (InPeriodID < @PeriodID and OutPeriodID >= @PeriodID)      --all non-cancelled bags that straddled this period
    )
order by BagID

select * from #Temp where BagType = 'F'
union all
select * from #Temp where BagType = 'P'
union all
select * from #Temp where BagType = 'B'

end
go

if @@error = 0
   print 'Success: The stored procedure (NewBankingSafeMaintenanceBagsHeld) for PO22-011 has been successfully rolled back'
else
   print 'Failure: The stored procedure (NewBankingSafeMaintenanceBagsHeld) for PO22-011 has NOT been successfully rolled back'
go


