Delete from ReportParameters where ReportId = 500 and ParameterId=100
Go
If @@Error = 0
   Print 'Success: The Removal of Date Created Parameter has been successfully deployed'
Else
   Print 'Failure: The Removal of Date Created Parameter has NOT been deployed'
Go
