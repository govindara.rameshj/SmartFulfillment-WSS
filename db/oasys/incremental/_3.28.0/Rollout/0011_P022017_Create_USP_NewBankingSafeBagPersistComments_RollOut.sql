-- =============================================
-- Author        : Alan Lewis
-- Create date   : 02/10/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7975
-- Description   : Create new stored procedure NewBankingSafeBagPersistComments
--				 : to update SafeBag entry's (identified by BagId parameter)
--				 : Comments field with Comments parameter.
-- =============================================
Create Procedure [dbo].[NewBankingSafeBagPersistComments]
   @BagID int,
   @Comments Char(255)
As
Begin
	Set NoCount On

	Update
		SafeBags
	Set
		Comments = @Comments
	Where
		ID = @BagID
End
Go

If @@Error = 0
   Print 'Success: The new stored procedure "NewBankingSafeBagPersistComments" for P022-017 has been sucessfully created'
Else
   Print 'Failure: The new stored procedure "NewBankingSafeBagPersistComments" for P022-017 has NOT been created'
Go
------------------------------------------------------------------------------------------------------------
Grant Execute On NewBankingSafeBagPersistComments To [role_execproc];
Go

If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure NewBankingSafeBagPersistComments for P022-017 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure NewBankingSafeBagPersistComments for P022-017 might NOT have been successfully deployed';
    End;
Go
