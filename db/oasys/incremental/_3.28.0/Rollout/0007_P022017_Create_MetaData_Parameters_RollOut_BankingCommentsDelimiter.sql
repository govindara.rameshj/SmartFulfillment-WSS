-- =============================================
-- Author        : Alan Lewis
-- Create date   : 07/11/2012
-- User Story	 : 9019: Iteration 18 Rework for pilot
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 9020
-- Description   : Create new parameter specifying a Comments Delimiter.
-- =============================================
Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	2210,      'Banking Comments Delimiter', ';~)',      Null,      1,            Null,         3)
Go

If @@Error = 0
   Print 'Success: The Banking Comments Delimiter parameter for P022-017 has been successfully deployed'
Else
   Print 'Failure: Banking Comments Delimiter parameter for P022-017 might NOT been deployed'
Go
