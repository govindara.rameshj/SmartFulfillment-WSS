-- =============================================
-- Author        : Alan Lewis
-- Create date   : 17/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7970
-- Description   : Return SafeBag entry that corresponds to
--				 : BagId parameter.
-- =============================================
Create Procedure [dbo].[NewBankingSafeBagGet]
   @BagID int
As
Begin
	Set NoCount On

	Select
		ID,
		SealNumber,
		[Type],
		[State],
		Value,
		Comments
	From
		SafeBags
	Where
		ID = @BagID
End
Go

If @@Error = 0
   Print 'Success: The new stored procedure "NewBankingSafeBagGet" for P022-017 has been sucessfully created'
Else
   Print 'Failure: The new stored procedure "NewBankingSafeBagGet" for P022-017 has NOT been created'
Go
------------------------------------------------------------------------------------------------------------
Grant Execute On NewBankingSafeBagGet To [role_execproc];
Go

If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure NewBankingSafeBagGet for P022-017 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure NewBankingSafeBagGet for P022-017 might NOT have been successfully deployed';
    End;
Go
