-- =============================================
-- Author        : Alan Lewis
-- Create date   : 26/10/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 8866
-- Description   : Alter stored procedure NewBankingPickupCashDrop
--				 : to include Comments field in returned data.
-- =============================================
Alter Procedure [dbo].[NewBankingPickupCashDrop]
   @PeriodID Int,
   @CashierID Int
As
Begin
	Set NoCount On
	Select
		PickupID         = a.ID,
		PickupPeriodID   = a.PickupPeriodID,
		PickupDate       = b.StartDate,
		PickupSealNumber = a.SealNumber,
		PickupValue      = a.Value,
		PickupComment	 = a.Comments
	From 
		SafeBags a
			Inner Join 
				SystemPeriods b
			On 
				b.ID = a.PickupPeriodID
	Where 
		a.[Type]              = 'P'
	And   
		a.[State]            <> 'C'
	And   
		a.PickupPeriodID      = @PeriodID
	And   
		a.AccountabilityID    = @CashierID 
	And   
		IsNull(a.CashDrop, 0) = 1
	Order By 
		a.ID Desc
End
Go

If @@Error = 0
   Print 'Success: The stored procedure "NewBankingPickupCashDrop" has successfully had Comments field added to return dataset for P022-017'
Else
   Print 'Failure: The new stored procedure "NewBankingPickupCashDrop" might NOT have had Comments field added to return dataset for P022-017'
Go
