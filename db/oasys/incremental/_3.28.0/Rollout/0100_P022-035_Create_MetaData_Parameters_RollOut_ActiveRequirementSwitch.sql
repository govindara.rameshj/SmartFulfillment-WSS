Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	-22035,      'User Story P022-035 - IBT Store names', Null,        Null,      1,            Null,         3)
Go
If @@Error = 0
   Print 'Success: The active requirement switch for P022-035 has been successfully deployed'
Else
   Print 'Failure: active requirement switch for P022-035 has NOT been deployed'
Go
