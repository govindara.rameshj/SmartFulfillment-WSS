-- =============================================
-- Author        : Alan Lewis
-- Create date   : 18/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : When rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7971
-- Description   : Add SafeBag Comments column to returned data.
-- =============================================
Alter Procedure [dbo].[NewBankingFloatList]
   @PeriodID Int
As
Begin
	Set NoCount On
	Declare @StartFloatID        Int

	Declare @CursorFloatID       Int
	Declare @CursorStartFloatID  Int

	Declare @Temp Table
		(
			FloatID            Int,
			FloatSealNumber    Char(20),
			FloatValue         Decimal(9,2),
			AssignedToUserName varChar(50),
			AssignedToUserID   Int,
			SaleTaken          Bit,
			FloatChecked       Bit,
			StartFloatID       Int,
			Comments		   VarChar(255)
		)

	Insert
		@Temp --cashier accountability model only
	 --unassigned floats - ignore the PeriodID When the float was created
	Select
		FloatID            = ID,
		FloatSealNumber    = SealNumber,
		FloatValue         = Value,
		AssignedToUserName = '',
		AssignedToUserID   = Cast(Null As Int),
		SaleTaken          = Cast(0 As Bit),
		FloatChecked       = IsNull(FloatChecked, 0),
		StartFloatID       = Null,
		Comments		   = Comments
	From
		SafeBags
	Where
		[Type]  = 'F'
	And
		[State] = 'S'
	Union all
	 --assigned floats - ignore the PeriodID When the float was created
	 --                  the OutPeriodID will indicate if the float was assigned On this PeriodID
	Select
		FloatID            = a.ID,
		FloatSealNumber    = a.SealNumber,
		FloatValue         = a.Value,
		AssignedToUserName = b.Name,
		AssignedToUserID   = b.ID,
		--no valid sales e.g invalid "vision deposit"
		SaleTaken          = 
			Cast(
				(
					Case IsNull(
						(
							Select
								NumTransactions
							From
								CashBalCashier
							Where
								PeriodID = @PeriodID
							And
								CurrencyID  = 
									(
										Select
											ID
										From
											SystemCurrency
										Where
											IsDefault = 1
									)
							And
								CashierID = a.AccountabilityID
							And
								(
									GrossSalesAmount <> 0 
								Or
									(
										Select
											Count(*)
										From
											CashBalCashierTen
										Where
											PeriodID = @PeriodID
										And
											CurrencyID = 
												(
													Select
														ID
													From
														SystemCurrency
													Where
														IsDefault = 1
												)
										And
											CashierID = a.AccountabilityID
									) > 0
								)), 0)
					When 0 Then 0
					Else 1
					End
				) As Bit),
		FloatChecked = IsNull(a.FloatChecked, 0),
		StartFloatID = Null,
		Comments = a.Comments
From
	SafeBags a
		Inner Join
			SystemUsers b
		On
			b.ID = a.AccountabilityID
Where
	a.[Type] = 'F'
And
	a.[State] = 'R'
And
	a.OutPeriodID = @PeriodID 

	--"manual check" & un-assigned floats need to maintain its link to the pickup that created it
	Declare
		TempCursor
	Cursor For
		Select
			FloatID,
			StartFloatID
		From
			@Temp
	Open
		TempCursor
		Fetch Next From
			TempCursor
		Into
			@CursorFloatID,
			@CursorStartFloatID
		While @@FETCH_STATUS <> -1
			Begin
				Exec StartingFloat @CursorFloatID, @StartFloatID Output
				Update
					@Temp
				Set
					StartFloatID = @StartFloatID
				Where
					FloatID = @CursorFloatID
			--Next record
			Fetch Next From
				TempCursor
			Into
				@CursorFloatID,
				@CursorStartFloatID
		End
	Close
		TempCursor
	Deallocate
		TempCursor

	Select
		a.FloatID,
		a.FloatSealNumber,
		a.FloatValue,
		FloatCreatedFromUserName    = IsNull(c.Name, ''),
		FloatCreatedFromPickupBagID = b.ID,
		a.AssignedToUserName,
		a.AssignedToUserID,
		a.SaleTaken,
		a.FloatChecked,
		a.Comments
	From
		@Temp a
			--float created From a pickup bag
			Left Outer Join 
				(
					Select 
						*
					From
						SafeBags
					Where
						[Type] = 'P'
					And
						[State] <> 'C'
				) b
			On
				b.RelatedBagId = a.StartFloatID
				Left Outer Join
					SystemUsers c
				On
					c.ID = b.AccountabilityID
End
Go

If @@Error = 0
   Print 'Success: The stored procedure "NewBankingFloatList" has been successfully altered to additionally return safebag comment for P022-017'
Else
   Print 'Failure: The stored procedure "NewBankingFloatList" has NOT been altered to additionally return safebag comment for P022-017'
Go
