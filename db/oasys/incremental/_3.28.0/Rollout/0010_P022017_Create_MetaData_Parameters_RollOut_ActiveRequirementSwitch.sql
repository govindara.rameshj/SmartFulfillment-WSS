-- =============================================
-- Author        : Alan Lewis
-- Create date   : 14/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7969
-- Description   : Create active requirement switch
-- =============================================
Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	-22017,      'Enable requirement P022-017', Null,        Null,      1,            Null,         3)
Go

If @@Error = 0
   Print 'Success: The active requirement switch for P022-017 has been successfully deployed'
Else
   Print 'Failure: active requirement switch for P022-017 has NOT been deployed'
Go
