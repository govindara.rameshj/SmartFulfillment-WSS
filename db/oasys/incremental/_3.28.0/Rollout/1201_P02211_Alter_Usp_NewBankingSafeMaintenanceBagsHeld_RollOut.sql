Alter Procedure NewBankingSafeMaintenanceBagsHeld
   @PeriodID Int
As
Begin
   Set NoCount On
	
	Select 
		BagID      = ID,
		BagType    = [Type],
		SealNumber = SealNumber,
		BagValue   = Value 
	Into 
		#Temp
	From
		SafeBags
	Where
		[Type] in ('F')
	And
		[State] <> 'C'
	And 
		(
			--all non-cancelled bags for this period
			(
				InPeriodID = @PeriodID
			)                              
		Or  --all non-cancelled bags before this period that has no been used 
			(
				InPeriodID < @PeriodID 
			And 
				OutPeriodID = 0
			)          
		Or   
			--all non-cancelled bags that straddled this period
			(
				InPeriodID < @PeriodID 
			And 
				OutPeriodID >= @PeriodID
			)      
		)


    union all
    select BagID      = ID,
           BagType    = [Type],
           SealNumber = SealNumber,
           BagValue   = Value 
    from dbo.udf_PickupBagsInSafeOnDate(@PeriodID)

    union All
    select BagID      = ID,
           BagType    = [Type],
           SealNumber = SealNumber,
           BagValue   = Value 
    from dbo.udf_BankingBagsInSafeOnDate(@PeriodID)

	Select * From #Temp Where BagType = 'F'
	Union All
	Select * From #Temp Where BagType = 'P'
	Union All
	Select * From #Temp Where BagType = 'B'
End

Go
If @@error = 0
   Print 'Success: The stored procedure (NewBankingSafeMaintenanceBagsHeld) for PO22-011 has been successfully deployed'
Else
   Print 'Failure: The stored procedure (NewBankingSafeMaintenanceBagsHeld) for PO22-011 has NOT been successfully deployed'
go