create procedure NewBankingGetLatestSafe

   @PeriodID int output

as
begin

   set @PeriodID = (select max(PeriodID) from [Safe])

   return @PeriodID

end
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [NewBankingGetLatestSafe] for Easter Banking has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [NewBankingGetLatestSafe] for Easter Banking has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT EXECUTE ON NewBankingGetLatestSafe TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_legacy] on [NewBankingGetLatestSafe] for Easter Banking has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_legacy] on [NewBankingGetLatestSafe] for Easter Banking has NOT been successfully deployed';
    END;
GO

GRANT EXECUTE ON NewBankingGetLatestSafe TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_execproc] on [NewBankingGetLatestSafe] for Easter Banking has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_execproc] on [NewBankingGetLatestSafe] for Easter Banking has NOT been successfully deployed';
    END;
GO
