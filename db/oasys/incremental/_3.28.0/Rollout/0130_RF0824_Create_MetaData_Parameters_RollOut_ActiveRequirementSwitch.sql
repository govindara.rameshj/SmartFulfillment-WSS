Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	-824,      'Referral 824 - Refresh Returns Header', Null,        Null,      1,            Null,         3)
Go
If @@Error = 0
   Print 'Success: The active requirement switch for RF0824 has been successfully deployed'
Else
   Print 'Failure: active requirement switch for RF0824 has NOT been deployed'
Go
