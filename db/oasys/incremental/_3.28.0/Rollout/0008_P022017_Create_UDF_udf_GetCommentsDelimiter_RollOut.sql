-- =============================================
-- Author        : Alan Lewis
-- Create date   : 06/11/2012
-- User Story	 : 9019: Iteration 18 Rework for pilot
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 9020
-- Description   : Create udf to read parameter setting for
--				 : comments delimiter.
-- =============================================
Create Function [dbo].[udf_GetCommentsDelimiter]
(
)
Returns VarChar(80)
Begin
	Declare @CommentsDelimiter As VarChar(80) = Char(13) + Char(10);
	
	If Exists(Select ParameterID From [Parameters] Where ParameterID = 2210)
		Begin
			Select 
				@CommentsDelimiter = StringValue
			From
				[Parameters]
			Where
				ParameterID = 2210;
		End

	Return @CommentsDelimiter
End
Go
If @@Error = 0
   Print 'Success: The user defined function "udf_GetCommentsDelimiter" has been sucessfully created for P022-017'
Else
   Print 'Failure: The user defined function "udf_GetCommentsDelimiter" might NOT Been created for P022-017'
Go
