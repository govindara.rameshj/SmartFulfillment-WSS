Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	-121,      'CR0121 - Negative Stock Issues', Null,        Null,      1,            Null,         3)
Go
If @@Error = 0
   Print 'Success: The active requirement switch for CR0121 has been successfully deployed'
Else
   Print 'Failure: active requirement switch for CR0121 has NOT been deployed'
Go