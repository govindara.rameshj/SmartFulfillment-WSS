insert into Parameters (ParameterID, Description, StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
                values (-22011, 'Enable requirement PO22-011', null, null, 1, null, 3)
go

if @@error = 0
   print 'Success: The metadata change for PO22-011 has been successfully deployed'
else
   print 'Failure: The metadata change for PO22-011 has NOT been successfully deployed'
go