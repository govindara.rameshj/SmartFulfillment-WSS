-- =============================================
-- Author        : Alan Lewis
-- Create date   : 06/11/2012
-- User Story	 : 9019: Iteration 18 Rework for pilot
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 9020
-- Description   : Create stored procedure to use same udf used
--				 : in stored procedure NewBankingFloatedCashierDisplay
--				 : to get the Comments Delimiter.
-- =============================================
Create Procedure [dbo].[NewBankingGetCommentsDelimiter]
As
Begin
	Set NoCount On
	
	Declare @Return As Table
		(
			CommentsDelimiter VarChar(80)
		)

	Insert Into
		@Return
	Values
		(
			[dbo].[udf_GetCommentsDelimiter]()
		)

	Select
		CommentsDelimiter
	From
		@Return
End
Go

If @@Error = 0
   Print 'Success: The Stored Procedure "NewBankingGetCommentsDelimiter" has been sucessfully created for P022-017'
Else
   Print 'Failure: The Stored Procedure "NewBankingGetCommentsDelimiter" might NOT have Been created for P022-017'
Go
------------------------------------------------------------------------------------------------------------
Grant Execute On NewBankingGetCommentsDelimiter To [role_execproc];
Go

If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure NewBankingGetCommentsDelimiter for P022-017 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure NewBankingGetCommentsDelimiter for P022-017 might NOT have been successfully deployed';
    End;
Go
