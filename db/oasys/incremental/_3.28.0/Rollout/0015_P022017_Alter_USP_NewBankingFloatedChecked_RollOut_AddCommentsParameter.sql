-- =============================================
-- Author        : Alan Lewis
-- Create date   : 19/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : When rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7971
-- Description   : Add SafeBag Comments column Update criteria.
-- =============================================
Alter Procedure [dbo].[NewBankingFloatChecked]
   @FloatBagID Int,
   @UserID1    Int,
   @UserID2    Int,
   @Comments   Char(255) = Null
As
Begin
	Set NoCount On

	If @Comments Is Null
		Begin
			Update
				SafeBags
			Set
				FloatChecked        = 1,
				FloatCheckedUserID1 = @UserID1,
				FloatCheckedUserID2 = @UserID2
			Where
				ID = @FloatBagID
		End
	Else
		Begin
			Update
				SafeBags
			Set
				FloatChecked        = 1,
				FloatCheckedUserID1 = @UserID1,
				FloatCheckedUserID2 = @UserID2,
				Comments	        = @Comments
			Where
				ID = @FloatBagID
		End
End
Go

If @@Error = 0
   Print 'Success: The stored procedure "NewBankingFloatChecked" has been successfully altered to accept additional comment parameter to update safebag with for P022-017'
Else
   Print 'Failure: The stored procedure "NewBankingFloatChecked" has NOT been altered to accept additional comment parameter to update safebag with for P022-017'
Go
