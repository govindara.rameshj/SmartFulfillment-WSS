ALTER PROCEDURE [dbo].[ReturnsGetOpenReturns]
	@SupplierNumber		VARCHAR(5)	=Null,
	@Date				DATETIME	=Null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		NUMB as 'Number'
		,SUPP as 'SupplierNumber'
		,sm.NAME as 'SupplierName'
		,EDAT as 'DateCreated'
		,RDAT as 'DateCollect'
		,DRLN as 'DrlNumber'
		,VALU as 'Value'
	FROM 
		RETHDR rh
	inner join	
		SUPMAS sm on rh.SUPP = sm.SUPN
	where		
		rh.DRLN = '000000'
		and	rh.isdeleted = 0
		And (@SupplierNumber is null or SUPP = @SupplierNumber)
	order by	
		sm.NAME, 
		rh.NUMB
		
END
Go
If @@Error = 0
   Print 'Success: The change to stored procedure ReturnGetOpenReturns has been successfully deployed'
Else
   Print 'Failure: The change to stored procedure ReturnGetOpenReturns has NOT been deployed'
Go