INSERT INTO [Oasys].[dbo].[ReportColumns]
           ([ReportId]
           ,[TableId]
           ,[ColumnId]
           ,[Sequence]
           ,[IsVisible]
           ,[IsBold]
           ,[IsHighlight]
           ,[HighlightColour]
           ,[Fontsize])
     VALUES
           (500
           ,1
           ,1200
           ,0
           ,1
           ,0
           ,NULL
           ,NULL
           ,NULL)
GO
If @@Error = 0
   Print 'Success: The Inserting New Report Value Column has been successfully deployed'
Else
   Print 'Failure: The Inserting New Report Value has NOT been deployed'
Go
INSERT INTO [Oasys].[dbo].[ReportColumns]
           ([ReportId]
           ,[TableId]
           ,[ColumnId]
           ,[Sequence]
           ,[IsVisible]
           ,[IsBold]
           ,[IsHighlight]
           ,[HighlightColour]
           ,[Fontsize])
     VALUES
           (500
           ,1
           ,303
           ,0
           ,1
           ,0
           ,NULL
           ,NULL
           ,NULL)
GO
If @@Error = 0
   Print 'Success: The Inserting New Report Supplier Name Column has been successfully deployed'
Else
   Print 'Failure: The Inserting New Report Supplier Name has NOT been deployed'
Go
INSERT INTO [Oasys].[dbo].[ReportSummary]
           ([ReportId]
           ,[TableId]
           ,[ColumnId]
           ,[SummaryType]
           ,[ApplyToGroups]
           ,[Format]
           ,[DenominatorId]
           ,[NumeratorId])
     VALUES
           (500
           ,1
           ,1200
           ,0
           ,0
           ,'Total {0:c2}'
           ,NULL
           ,NULL)
GO
If @@Error = 0
   Print 'Success: The Inserting New Report Summary has been successfully deployed'
Else
   Print 'Failure: The Inserting New Report Summary has NOT been deployed'
Go
UPDATE [Oasys].[dbo].[MenuConfig]
   SET [ID] = 4230
      ,[MasterID] = 4200
      ,[AppName] = 'Open Returns'
      ,[AssemblyName] = 'Returns.Maintain.dll'
      ,[ClassName] = 'Returns.Maintain.Maintain'
      ,[MenuType] = 4
      ,[Parameters] = 'open'
      ,[ImagePath] = 'icons\icon_g.ico'
      ,[LoadMaximised] = 1
      ,[IsModal] = 1
      ,[DisplaySequence] = 2
      ,[AllowMultiple] = 0
      ,[WaitForExit] = 0
      ,[TabName] = NULL
      ,[Description] = NULL
      ,[ImageKey] = 0
      ,[Timeout] = 0
      ,[DoProcessingType] = 1
 WHERE ID=4230
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Menu 1 has been successfully deployed'
Else
   Print 'Failure: The update script for Open Returns Menu 1 has NOT been deployed'
GO
UPDATE [Oasys].[dbo].[MenuConfig]
   SET [ID] = 6120
      ,[MasterID] = 6100
      ,[AppName] = 'Open Returns'
      ,[AssemblyName] = 'Returns.Maintain.dll'
      ,[ClassName] = 'Returns.Maintain.Maintain'
      ,[MenuType] = 4
      ,[Parameters] = 'open'
      ,[ImagePath] = 'icons\icon_g.ico'
      ,[LoadMaximised] = 1
      ,[IsModal] = 1
      ,[DisplaySequence] = 2
      ,[AllowMultiple] = 0
      ,[WaitForExit] = 0
      ,[TabName] = NULL
      ,[Description] = NULL
      ,[ImageKey] = 0
      ,[Timeout] = 0
      ,[DoProcessingType] = 1
 WHERE ID=6120
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Menu 2 has been successfully deployed'
Else
   Print 'Failure: The update script for Open Returns Menu 2 has NOT been deployed'
GO
UPDATE [Oasys].[dbo].[MenuConfig]
   SET [ID] = 11440
      ,[MasterID] = 11400
      ,[AppName] = 'Open Returns'
      ,[AssemblyName] = 'Returns.Maintain.dll'
      ,[ClassName] = 'Returns.Maintain.Maintain'
      ,[MenuType] = 4
      ,[Parameters] = 'open'
      ,[ImagePath] = 'icons\icon_g.ico'
      ,[LoadMaximised] = 1
      ,[IsModal] = 1
      ,[DisplaySequence] = 2
      ,[AllowMultiple] = 0
      ,[WaitForExit] = 0
      ,[TabName] = NULL
      ,[Description] = NULL
      ,[ImageKey] = 0
      ,[Timeout] = 0
      ,[DoProcessingType] = 1
 WHERE ID=11440
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Menu 3 has been successfully deployed'
Else
   Print 'Failure: The update script for Open Returns Menu 3 has NOT been deployed'
GO
UPDATE [Oasys].[dbo].[MenuConfig]
   SET [ID] = 11630
      ,[MasterID] = 11600
      ,[AppName] = 'Open Returns'
      ,[AssemblyName] = 'Returns.Maintain.dll'
      ,[ClassName] = 'Returns.Maintain.Maintain'
      ,[MenuType] = 4
      ,[Parameters] = 'open'
      ,[ImagePath] = 'icons\icon_g.ico'
      ,[LoadMaximised] = 1
      ,[IsModal] = 1
      ,[DisplaySequence] = 2
      ,[AllowMultiple] = 0
      ,[WaitForExit] = 0
      ,[TabName] = NULL
      ,[Description] = NULL
      ,[ImageKey] = 0
      ,[Timeout] = 0
      ,[DoProcessingType] = 1
 WHERE ID=11630
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Menu 4 has been successfully deployed'
Else
   Print 'Failure: The update script for Open Returns Menu 4 has NOT been deployed'
GO
UPDATE [Oasys].[dbo].[ReportColumn]
   SET [Id] = 303
      ,[Name] = 'SupplierName'
      ,[Caption] = 'Supplier Name'
      ,[FormatType] = 0
      ,[Format] = NULL
      ,[IsImagePath] = 0
      ,[MinWidth] = 200
      ,[MaxWidth] = 300
      ,[Alignment] = NULL
 WHERE Id=303
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Supplier Column has been successfully deployed'
Else
   Print 'Failure: The update script for Open Returns Supplier Column has NOT been deployed'
GO
UPDATE [Oasys].[dbo].[ReportColumn]
   SET [Id] = 1200
      ,[Name] = 'Value'
      ,[Caption] = 'Value'
      ,[FormatType] = 1
      ,[Format] = 'c2'
      ,[IsImagePath] = 0
      ,[MinWidth] = 55
      ,[MaxWidth] = NULL
      ,[Alignment] = 3
 WHERE Id=1200
GO
If @@Error = 0
   Print 'Success: The update script for Open Returns Value Column has been successfully deployed'
Else
   Print 'Failure: The update script for Open Returns Value Column has NOT been deployed'
GO




