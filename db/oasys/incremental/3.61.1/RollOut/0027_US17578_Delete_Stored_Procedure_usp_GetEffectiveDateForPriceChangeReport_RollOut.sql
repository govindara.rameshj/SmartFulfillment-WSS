﻿IF EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[usp_GetEffectiveDateForPriceChangeReport]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Dropping procedure [usp_GetEffectiveDateForPriceChangeReport]'
    DROP PROCEDURE usp_GetEffectiveDateForPriceChangeReport
END
GO

If @@Error = 0
   Print 'Success: The Dropping Stored Procedure usp_GetEffectiveDateForPriceChangeReport for US17578 has been deployed successfully'
Else
   Print 'Failure: The Dropping Stored Procedure usp_GetEffectiveDateForPriceChangeReport for US17578 has not been deployed'
GO
