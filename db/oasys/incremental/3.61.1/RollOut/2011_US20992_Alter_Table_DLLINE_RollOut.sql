SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLLINE') and name = 'SourceItemId')
BEGIN
    PRINT 'Add SourceItemId column to DLLINE'
    ALTER TABLE dbo.DLLINE ADD SourceItemId varchar(50) NULL
END
GO

If @@Error = 0
   Print 'Success: Altering table DLLINE for US20992 has been rolled out successfully'
ELSE
   Print 'Failure: Altering table DLLINE for US20992 has not been rolled out successfully'
GO