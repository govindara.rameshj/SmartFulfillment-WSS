﻿IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[usp_GetCountPricesToPrintLabels]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure [usp_GetCountPricesToPrintLabels]'
    EXEC ('CREATE PROCEDURE dbo.[usp_GetCountPricesToPrintLabels] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

ALTER PROCEDURE [dbo].[usp_GetCountPricesToPrintLabels]
   @Date date,
   @CountPRCCHNG3Days int output,
   @CountPRCCHNGAlreadyHappened int output
AS
BEGIN
SET NOCOUNT ON

    DECLARE @3Date date 
    SET @3Date = DATEADD(day, 3, @Date)
    SET @CountPRCCHNG3Days = 0
    SET @CountPRCCHNGAlreadyHappened = 0

    SELECT @CountPRCCHNG3Days = COUNT(1) FROM [dbo].[PRCCHG] p
    INNER JOIN [dbo].[STKMAS] s ON s.[SKUN] = p.[SKUN] AND (s.[AAPC] = 0 OR (s.[AAPC] = 1 AND (s.[ONHA] + s.[MDNQ]) <> 0))
                              AND (s.[INON] = 0 OR (s.[INON] = 1 AND (s.[ONHA] + s.[MDNQ]) <> 0))
                              AND (s.[IOBS] = 0 OR (s.[IOBS] = 1 AND (s.[ONHA] + s.[MDNQ]) <> 0))
    WHERE p.[SHEL] = 0 AND (p.[LABS] = 1 OR p.[LABM] = 1 OR p.[LABL] = 1)
    AND (p.[PDAT] > @Date AND p.[PDAT] <= @3Date)

    SELECT @CountPRCCHNGAlreadyHappened = COUNT(DISTINCT p.[SKUN]) FROM [dbo].[PRCCHG] p
    INNER JOIN [dbo].[STKMAS] s ON s.[SKUN] = p.[SKUN] AND (s.[AAPC] = 0 OR (s.[AAPC] = 1 AND (s.[ONHA] + s.[MDNQ]) <> 0))
                              AND (s.[INON] = 0 OR (s.[INON] = 1 AND (s.[ONHA] + s.[MDNQ]) <> 0))
                              AND (s.[IOBS] = 0 OR (s.[IOBS] = 1 AND (s.[ONHA] + s.[MDNQ]) <> 0))
    WHERE p.[SHEL] = 0 AND (p.[LABS] = 1 OR p.[LABM] = 1 OR p.[LABL] = 1)
    AND p.[PDAT] <= @Date
END  
GO 

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_GetCountPricesToPrintLabels for US16002 and US22354 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_GetCountPricesToPrintLabels for US16002 and US22354 has not been deployed'
GO