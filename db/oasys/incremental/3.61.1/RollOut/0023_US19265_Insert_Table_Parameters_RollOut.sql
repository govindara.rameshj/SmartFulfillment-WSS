IF NOT EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 504)
    BEGIN
        INSERT INTO dbo.Parameters (ParameterID, [Description],	StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES
        (504,'Load x PC System Days', NULL, 30, 0, 0.00000, 1)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US19265 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US19265 has not been deployed'
GO