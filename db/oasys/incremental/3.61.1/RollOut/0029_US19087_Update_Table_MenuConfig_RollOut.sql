UPDATE dbo.MenuConfig
SET AppName = 'Price Change Audit Report'
WHERE ID = 9040
GO

If @@Error = 0
   Print 'Success: Update Table MenuConfig for US19083 has been deployed successfully'
Else
   Print 'Failure: Update Table MenuConfig for US19083 has not been deployed successfully'
Go