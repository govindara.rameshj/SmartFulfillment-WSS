BEGIN TRANSACTION StaAuditUploadTransaction
BEGIN TRY
    DECLARE @MenuID int = 10286
    DECLARE @MasterID int = 10200
    
    DECLARE @DisplaySequence int
    SELECT @DisplaySequence = MAX(DisplaySequence) + 1 FROM dbo.MenuConfig WHERE MasterID = @MasterID

    IF NOT EXISTS (SELECT 1 FROM dbo.MenuConfig WHERE ID = @MenuID)
    BEGIN
        INSERT INTO dbo.MenuConfig (
                ID,
                MasterID,
                AppName,
                AssemblyName,
                ClassName,
                MenuType,
                [Parameters],
                ImagePath,
                LoadMaximised,
                IsModal,
                DisplaySequence,
                AllowMultiple,
                WaitForExit,
                TabName,
                [Description],
                ImageKey,
                [Timeout],
                DoProcessingType)
        VALUES (@MenuID, @MasterID, 'STA Audit Upload', 'WSS.BO.Pic.StaAuditUpload.Host.exe', NULL, 1, '', 'icons\icon_g.ico', 0, 1, @DisplaySequence, 0, 0, NULL, NULL, '0', 0, 0)
    END
    
    IF NOT EXISTS (SELECT 1 FROM dbo.ProfilemenuAccess WHERE MenuConfigID = @MenuID)
    BEGIN    
        INSERT INTO    dbo.ProfilemenuAccess (
            [ID],
            [MenuConfigID],
            [AccessAllowed],
            [OverrideAllowed],
            [SecurityLevel],
            [IsManager],
            [IsSupervisor],
            [LastEdited],
            [Parameters]
            )
        SELECT 
                [ID],
                @MenuID AS [MenuConfigID],
                [AccessAllowed],
                [OverrideAllowed],
                [SecurityLevel],
                [IsManager],
                [IsSupervisor],
                [LastEdited],
                [Parameters]
        FROM dbo.ProfilemenuAccess
        WHERE MenuConfigID = 10285
    END

    COMMIT TRANSACTION StaAuditUploadTransaction
    Print 'Success: Scripts for US21237 has been deployed successfully'
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION StaAuditUploadTransaction
    Print 'Failure: Scripts for US21237 has not been deployed successfully'
END CATCH