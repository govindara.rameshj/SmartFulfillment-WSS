INSERT INTO [dbo].[Parameters]
           ([ParameterID], [Description], [StringValue], [LongValue], [BooleanValue], [DecimalValue], [ValueType])
SELECT t.[ParameterID], t.[Description], t.[StringValue], t.[LongValue], t.[BooleanValue], t.[DecimalValue], t.[ValueType]
FROM (
        SELECT 990099 AS [ParameterID], 'Exclude price changes from HOSTU processing' AS [Description], NULL AS [StringValue], NULL AS [LongValue], 0 AS [BooleanValue], NULL AS [DecimalValue], 3 AS [ValueType]
    ) t 
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] t1 WHERE t1.[ParameterID] = t.[ParameterID])
GO

If @@Error = 0
   Print 'Success: Insert row to [dbo].[Parameters] for US19078 has been deployed successfully'
Else
   Print 'Failure: Insert row to [dbo].[Parameters] for US19078 has not been deployed successfully'
Go