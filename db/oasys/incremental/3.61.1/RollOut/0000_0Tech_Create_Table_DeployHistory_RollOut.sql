﻿IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeployHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    PRINT 'Creating table DeployHistory'

    CREATE TABLE [dbo].DeployHistory
    (
        [Id] int NOT NULL IDENTITY (1, 1),
        [Version] [nvarchar](10) NOT NULL,
        [Action] [nvarchar](10) NOT NULL,
        [Date] datetime NOT NULL,
        [Status] bit NOT NULL,
        [Hash] [nvarchar](100) NOT NULL
        CONSTRAINT [PK_DeployHistory] PRIMARY KEY CLUSTERED ([Id])
    ) ON [PRIMARY]
END
GO

IF @@error = 0
   PRINT 'Success: Create Table DeployHistory has been deployed successfully'
ELSE
   PRINT 'Failure: Create Table DeployHistory has been deployed successfully'
GO
