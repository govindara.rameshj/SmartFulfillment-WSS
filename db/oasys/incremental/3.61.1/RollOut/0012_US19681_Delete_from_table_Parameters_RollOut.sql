IF EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = '-220045')
    DELETE FROM dbo.Parameters WHERE ParameterID = '-220045'
GO

If @@Error = 0
   Print 'Success: The Delete from table Parameters for US19681 has been deployed successfully'
Else
   Print 'Failure: The Delete from table Parameters for US19681 has not been deployed'
GO