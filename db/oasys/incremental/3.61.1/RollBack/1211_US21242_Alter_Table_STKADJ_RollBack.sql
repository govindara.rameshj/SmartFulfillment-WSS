IF EXISTS (select 1 from sys.columns where object_id = object_id('STKADJ') and name = 'IsStaAuditUpload')
BEGIN
    PRINT 'Dropping IsStaAuditUpload column from STKADJ'
    ALTER TABLE dbo.STKADJ DROP CONSTRAINT DF_STKADJ_IsStaAuditUpload
    ALTER TABLE dbo.STKADJ DROP COLUMN IsStaAuditUpload
END

If @@Error = 0
   Print 'Success: Altering table STKADJ for US21242 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table STKADJ for US21242 has not been rolled back successfully'
GO