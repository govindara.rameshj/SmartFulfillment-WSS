UPDATE dbo.MenuConfig
SET AppName = 'Price Change Report'
WHERE ID = 9050
GO

If @@Error = 0
   Print 'Success: Update Table MenuConfig for US19083 has been deployed successfully'
Else
   Print 'Failure: Update Table MenuConfig for US19083 has not been deployed successfully'
Go