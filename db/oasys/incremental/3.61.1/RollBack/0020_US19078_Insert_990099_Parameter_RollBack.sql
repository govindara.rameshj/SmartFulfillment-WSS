IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Parameters]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DELETE FROM [dbo].[Parameters] WHERE [ParameterID] = 990099
GO

If @@Error = 0
   Print 'Success: Insert row to [dbo].[Parameters] for US19078 has been deployed successfully'
Else
   Print 'Failure: Insert row to [dbo].[Parameters] for US19078 has not been deployed successfully'
Go