IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = '-220045')
    INSERT INTO [dbo].[Parameters]
               ([ParameterID]
               ,[Description]
               ,[StringValue]
               ,[LongValue]
               ,[BooleanValue]
               ,[DecimalValue]
               ,[ValueType])
    SELECT '-220045' AS [ParameterID],
           'P022-045 SOM Customer Search' AS [Description],
           NULL AS [StringValue],
           NULL AS [LongValue],
           CAST(1 AS bit) AS [BooleanValue],
           NULL AS [DecimalValue],
           3 AS [ValueType]           
GO

If @@Error = 0
   Print 'Success: The Delete from table Parameters for US19681 has been rolled back successfully'
Else
   Print 'Failure: The Delete from table Parameters for US19681 has not been rolled back'
GO