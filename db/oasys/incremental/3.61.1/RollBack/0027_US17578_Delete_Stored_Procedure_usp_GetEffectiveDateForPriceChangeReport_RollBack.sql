﻿IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[usp_GetEffectiveDateForPriceChangeReport]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure [usp_GetEffectiveDateForPriceChangeReport]'
    EXEC ('CREATE PROCEDURE dbo.[usp_GetEffectiveDateForPriceChangeReport] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

ALTER PROCEDURE [dbo].[usp_GetEffectiveDateForPriceChangeReport]
    @SkuNumber varchar(6),
    @EventNumber varchar(6),
    @Priority varchar(2)
AS
BEGIN

    SELECT 'StartDate' = dbo.udf_GetLatestPriceChangeDateForSKU(@SkuNumber)

END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_GetEffectiveDateForPriceChangeReport for US17578 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_GetEffectiveDateForPriceChangeReport for US17578 has not been deployed'
GO
