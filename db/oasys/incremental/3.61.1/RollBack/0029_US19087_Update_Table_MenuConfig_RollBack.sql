UPDATE dbo.MenuConfig
SET AppName = 'Price Change Confirmation'
WHERE ID = 9040
GO

If @@Error = 0
   Print 'Success: Update Table MenuConfig for US19087 has been deployed successfully'
Else
   Print 'Failure: Update Table MenuConfig for US19087 has not been deployed successfully'
Go