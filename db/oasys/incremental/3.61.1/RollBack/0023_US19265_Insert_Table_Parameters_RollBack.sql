IF EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 504)
    BEGIN
        DELETE FROM dbo.[Parameters] WHERE ParameterId = 504
    END
GO

If @@Error = 0
   Print 'Success: The Delete from Parameters table for US19265 has been deployed successfully'
Else
   Print 'Failure: The Delete from Parameters table for US19265 has not been deployed'
GO