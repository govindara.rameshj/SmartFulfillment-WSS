IF EXISTS (select 1 from sys.columns where object_id = object_id('DLLINE') and name = 'SourceItemId')
BEGIN
    PRINT 'Dropping SourceItemId column from DLLINE'
    ALTER TABLE dbo.DLLINE DROP COLUMN SourceItemId
END

If @@Error = 0
   Print 'Success: Altering table DLLINE for US20992 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table DLLINE for US20992 has not been rolled back successfully'
GO