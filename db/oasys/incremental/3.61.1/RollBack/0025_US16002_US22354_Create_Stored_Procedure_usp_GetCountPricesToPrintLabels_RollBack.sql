﻿IF EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[usp_GetCountPricesToPrintLabels]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Dropping procedure [usp_GetCountPricesToPrintLabels]'
    DROP PROCEDURE dbo.usp_GetCountPricesToPrintLabels
END
GO

If @@Error = 0
   Print 'Success: The Drop Stored Procedure usp_GetCountPricesToPrintLabels for US16002 and US22354 has been rolled back successfully'
Else
   Print 'Failure: The Drop Stored Procedure usp_GetCountPricesToPrintLabels for US16002 and US22354 has not been rolled back'
GO 