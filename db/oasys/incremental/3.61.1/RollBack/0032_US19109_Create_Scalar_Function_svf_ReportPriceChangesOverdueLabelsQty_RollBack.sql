IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesOverdueLabelsQty') AND type in (N'FN'))
DROP FUNCTION svf_ReportPriceChangesOverdueLabelsQty
GO

IF @@Error = 0
   Print 'Success: Scalar-values Function svf_ReportPriceChangesOverdueLabelsQty for US19109 has been successfully dropped'
ELSE
   Print 'Failure: Scalar-values Function svf_ReportPriceChangesOverdueLabelsQty for US19109 has NOT been successfully dropped'
GO