IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesOutstandingQty') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesOutstandingQty'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesOutstandingQty() RETURNS int AS BEGIN RETURN 0 END')
END
GO

ALTER FUNCTION [dbo].[svf_ReportPriceChangesOutstandingQty]
(
)
RETURNS int
AS
BEGIN

    DECLARE @OutstandingQty INT
    
    SELECT @OutstandingQty = count(sm.SKUN)
    FROM PRCCHG AS Pc
    INNER JOIN STKMAS AS sm ON sm.SKUN = pc.SKUN
    WHERE PSTA = 'U'
    
    SET @OutstandingQty = ISNULL(@OutstandingQty, 0);
        
    RETURN          @OutstandingQty

END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesOutstandingQty for US19109 has been deployed successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesOutstandingQty for US19109 has not been deployed successfully'
GO