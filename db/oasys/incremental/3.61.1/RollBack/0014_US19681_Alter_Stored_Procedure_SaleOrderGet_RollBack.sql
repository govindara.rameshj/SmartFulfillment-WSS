IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'SaleOrderGet') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure SaleOrderGet'
    EXEC ('CREATE PROCEDURE dbo.SaleOrderGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SaleOrderGet')
GO

ALTER PROCEDURE [dbo].[SaleOrderGet]
    @OrderNumber CHAR (6) = null, 
    @OmOrderNumber INT = null, 
    @VendaNumber char(6) = null,
    @DeliveryStatusMin INT = null, 
    @DeliveryStatusMax INT = null,
    @RefundStatusMin INT = null, 
    @RefundStatusMax INT = null, 
    @DateStart DATE = null, 
    @DateEnd DATE = null,
    @PostCode Char(10) = null,
    @TelephoneNumber VarChar(20) = null,
    @CustomerName VarChar(50) = null,
    @IncludeSuspended bit = 1,
    @ForOMProcessing bit = 0,
    
    @SourceOrderNumber char(20) = null,
    @Source char(2) = null,
    @IsClickAndCollect bit = null
AS
BEGIN
    SET NOCOUNT ON
    Declare @PostCodeWithoutSpaces VarChar(10)
    if @PostCode Is Not Null  Set @PostCodeWithoutSpaces = REPLACE(@PostCode,' ','')
    
    Declare @TelephoneNumberWithoutSpaces VarChar(20)
    if @TelephoneNumber Is Not Null  Set @TelephoneNumberWithoutSpaces = REPLACE(@TelephoneNumber,' ','')
    
    select
        vc.NUMB             as Number,
        vc.CUST             as CustomerNumber ,
        vc.NAME             as CustomerName ,
        vc.DATE1            as DateOrder ,
        vc.DELD             as DateDelivery ,
        vc.CANC             as 'Status',
        vc.DELI             as IsForDelivery ,
        vc.DELC             as DeliveryConfirmed ,
        vc.AMDT             as TimesAmended ,
        vc.ADDR1            as DeliveryAddress1 ,
        vc.ADDR2            as DeliveryAddress2 ,
        vc.ADDR3            as DeliveryAddress3 ,
        vc.ADDR4            as DeliveryAddress4 ,
        vc.POST             as DeliveryPostCode ,
        vc.PHON             as PhoneNumber ,
        vc.HOMP             as PhoneNumberHome ,
        vc.MOBP             as PhoneNumberMobile ,
        vc.PRNT             as IsPrinted ,
        vc.RPRN             as NumberReprints ,
        vc.REVI             as RevisionNumber ,
        vc.MVST             as MerchandiseValue ,
        vc.DCST             as DeliveryCharge ,
        vc.QTYO             as QtyOrdered ,
        vc.QTYT             as QtyTaken ,
        vc.QTYR             as QtyRefunded ,
        vc.WGHT             as OrderWeight ,
        vc.VOLU             as OrderVolume ,
        vc.SDAT             as TranDate ,
        vc.STIL             as TranTill ,
        vc.STRN             as TranNumber ,
        vc.RDAT             as RefundTranDate ,
        vc.RTIL             as RefundTill ,
        vc.RTRN             as RefundTranNumber,
        vc.DDAT             as DateDespatch,
        vc.OEID             as OrderTakerId,
        vc.FDID             as ManagerId,
        vc.DeliveryStatus,
        vc.RefundStatus,
        vc.SellingStoreId,
        vc.SellingStoreOrderId,
        vc.OMOrderNumber,
        vc.CustomerAddress1,
        vc.CustomerAddress2,
        vc.CustomerAddress3,
        vc.CustomerAddress4,
        vc.CustomerPostcode,
        vc.CustomerEmail,
        vc.PhoneNumberWork,
        vc.IsSuspended,
        vc.Source,
        vc.SourceOrderNumber,
        vc.ExtendedLeadTime,
        vc.DeliveryContactName,
        vc.DeliveryContactPhone,
        vc.CustomerAccountNo,
        vc.RequiredDeliverySlotID,
        vc.RequiredDeliverySlotDescription,
        vc.RequiredDeliverySlotStartTime,
        vc.RequiredDeliverySlotEndTime
    from
        --Tables CORHDR, CORHDR4 and CORHDR5 have been replaced with the view vwCORHDRFull
        vwCORHDRFull vc
    where
        (@OrderNumber is null or vc.NUMB = @OrderNumber)
        and (@VendaNumber is null or vc.CUST = @VendaNumber)

        and (@SourceOrderNumber is null or vc.SourceOrderNumber = @SourceOrderNumber)
        and (@Source is null or vc.Source = @Source)
        and (@IsClickAndCollect is null or vc.IS_CLICK_AND_COLLECT = @IsClickAndCollect)

        and (@OmOrderNumber is null or vc.OMOrderNumber = @OmOrderNumber)
        and (@DeliveryStatusMin is null or vc.DeliveryStatus >= @DeliveryStatusMin)
        and (@DeliveryStatusMax is null or vc.DeliveryStatus <= @DeliveryStatusMax)
        and (@RefundStatusMin is null or vc.RefundStatus >= @RefundStatusMin)
        and (@RefundStatusMax is null or vc.RefundStatus <= @RefundStatusMax)
        and (@DateStart is null or vc.DATE1 >= @DateStart)
        and (@DateEnd is null or vc.DATE1 <= @DateEnd)
        and ((@TelephoneNumber is null) 
            or (REPLACE(vc.PHON, ' ', '') like '%' + @TelephoneNumberWithoutSpaces + '%')
            or (REPLACE(vc.MOBP, ' ', '') like '%' + @TelephoneNumberWithoutSpaces + '%')
            or (REPLACE(vc.PhoneNumberWork, ' ', '') like '%' + @TelephoneNumberWithoutSpaces + '%')
            or (REPLACE(vc.DeliveryContactPhone, ' ', '') like '%' + @TelephoneNumberWithoutSpaces + '%'))
        and ((@PostCode is null) 
            or (REPLACE(vc.POST, ' ', '') like '%' + @PostCodeWithoutSpaces + '%')
            or (REPLACE(vc.CustomerPostcode, ' ', '')  like '%' + @PostCodeWithoutSpaces + '%'))
        and ((@CustomerName is null) 
            or (vc.NAME like '%' + @CustomerName + '%')
            or (vc.NAME like '%' + @CustomerName + '%')
            or (vc.DeliveryContactName like '%' + @CustomerName + '%'))
        and vc.DeliveryStatus < 9999
        
        -- Filter out suspended only if @IncludeSuspended is set to 0 (not null or 1)
        and (coalesce(@IncludeSuspended, 1) = 1 or vc.IsSuspended = 0)
        -- Filter orders depending on usage (specified by ForOmProcessing flag) and order source settings from QODSource table
        and ((@OrderNumber is not null) or (@OmOrderNumber is not null) or (@VendaNumber is not  null) or (@SourceOrderNumber is not null) -- skip order filtering if exact ID is specified
             or (@ForOmProcessing = 0 and  vc.[SHOW_IN_UI] = 1) or (@ForOmProcessing = 1 and vc.[SEND_UPDATES_TO_OM] = 1)
                    )           
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SaleOrderGet for US19681 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure SaleOrderGet for US19681 has not been rolled back'
GO