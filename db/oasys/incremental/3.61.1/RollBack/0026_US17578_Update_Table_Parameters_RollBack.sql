 IF NOT EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterId = 980110)
    BEGIN
        INSERT INTO dbo.Parameters (ParameterID, [Description],	StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES
        (980110,'Enable Change Request CR00110', NULL, NULL, 1, NULL, 3)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US17578 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US17578 has not been deployed'
GO