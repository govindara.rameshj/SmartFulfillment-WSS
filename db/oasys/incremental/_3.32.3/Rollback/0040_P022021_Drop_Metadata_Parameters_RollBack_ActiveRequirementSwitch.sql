
DELETE FROM Parameters
  WHERE ParameterID
        = 
        -22021;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The metadata change for P022-021 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: The metadata change for P022-021 has NOT been successfully deployed';
    END;
GO



