Update Parameters Set BooleanValue = 0 Where ParameterID = 980110
If @@Error = 0
   Print 'Success: The requirement switch parameter for P02240/CR0110 has been sucessfully deactivated'
Else
   Print 'Failure: The requirement switch parameter for P02240/CR0110 might not have been  deactivated'
Go