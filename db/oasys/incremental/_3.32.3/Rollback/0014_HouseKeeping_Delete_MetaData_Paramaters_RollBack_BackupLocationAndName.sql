delete Parameters where ParameterID = 189
go

if @@error = 0
   print 'Success: The metadata change to Parameters for HouseKeeping has been successfully removed'
else
   print 'Failure: The metadata change to Parameters for HouseKeeping has NOT been successfully removed'
go