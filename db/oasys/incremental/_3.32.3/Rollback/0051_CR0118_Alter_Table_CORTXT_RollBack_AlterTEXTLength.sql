DELETE FROM CORTXT WHERE LEN(TEXT) > 99
GO
ALTER TABLE [dbo].CORTXT
ALTER COLUMN [TEXT] [nvarchar](100) NULL
GO
If @@Error = 0
   Print 'Success: The Alter table Column Text for CR0118 has been successfully Rolled Back'
Else
   Print 'Failure: The Alter table Column Text for CR0118 might NOT have been Rolled Back'
Go
