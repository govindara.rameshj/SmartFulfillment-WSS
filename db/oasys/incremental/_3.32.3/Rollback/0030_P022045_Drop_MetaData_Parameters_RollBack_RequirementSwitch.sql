Delete
	[Parameters]
Where
	ParameterID = -220045
Go

If @@Error = 0
   Print 'Success: The requirement switch for P022-045 has been successfully deleted'
Else
   Print 'Failure: requirement switch for P022-045 might NOT have been deleted'
Go
