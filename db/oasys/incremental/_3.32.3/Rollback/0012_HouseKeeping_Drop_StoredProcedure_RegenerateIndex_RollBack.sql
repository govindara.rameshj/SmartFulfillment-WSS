drop procedure RegenerateIndex
go

if @@error = 0
   print 'Success: Stored procedure RegenerateIndex for HouseKeeping has been successfully dropped'
else
   print 'Failure: Stored procedure RegenerateIndex for HouseKeeping has NOT been successfully dropped'
go