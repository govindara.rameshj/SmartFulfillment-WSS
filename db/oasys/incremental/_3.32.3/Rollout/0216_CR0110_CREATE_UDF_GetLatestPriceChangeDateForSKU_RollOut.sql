ALTER FUNCTION [dbo].[udf_GetLatestPriceChangeDateForSku] 
(
	@SKU char(6)
)

RETURNS date
AS
BEGIN
	DECLARE @ReturnDate date
	DECLARE @Status char(1)
	DECLARE	@CurrentDate date
	SET @CurrentDate = DATEADD(dd, DATEDIFF(dd,0,GETDATE()), 0)
	
/*
** Get the minimum unapplied start date for a record with an end date on or after today 
** and a start date on or before today.
*/

	SELECT @ReturnDate = MIN(e.SDAT)
	FROM PRCCHG p
	JOIN EVTCHG e ON p.SKUN = e.SKUN
				AND p.EVNT = e.NUMB
				AND p.PRIO = e.PRIO
	WHERE e.SKUN = @sku
	AND e.EDAT >= @CurrentDate
	AND p.PSTA = 'U'
	
/*
** Get the maximum record with a start date before today and an end date grouped by status.
** The effective date will be the end date + 1 day.
*/

	IF @ReturnDate IS NULL
	BEGIN
		SELECT TOP 1 @ReturnDate = DATEADD(day,1,MAX(e.EDAT)),@Status = CASE p.PSTA WHEN 'U' THEN 'U' ELSE 'S' END
		FROM PRCCHG p
		JOIN EVTCHG e ON p.SKUN = e.SKUN
					AND p.EVNT = e.NUMB
					AND p.PRIO = e.PRIO
		WHERE e.SKUN = @sku
		AND e.EDAT IS NOT NULL
		AND e.SDAT < @CurrentDate
		GROUP BY p.PSTA
		ORDER BY 2 DESC
		
		IF @ReturnDate IS NOT NULL
		BEGIN
			SELECT TOP 1 @ReturnDate = MIN(e.SDAT),@Status = CASE p.PSTA WHEN 'U' THEN 'U' ELSE 'S' END
			FROM PRCCHG p
			JOIN EVTCHG e ON p.SKUN = e.SKUN
						AND p.EVNT = e.NUMB
						AND p.PRIO = e.PRIO
			WHERE e.SKUN = @sku
			AND e.EDAT IS NULL
			AND e.SDAT > @ReturnDate
			GROUP BY p.PSTA
			ORDER BY 2 DESC
		END
	END
	
/*
** Get the minimum start date, ordered by status
*/

	IF @ReturnDate IS NULL
	BEGIN
		SELECT TOP 1 @ReturnDate = MAX(e.SDAT),@Status = CASE p.PSTA WHEN 'U' THEN 'U' ELSE 'S' END
		FROM PRCCHG p
		JOIN EVTCHG e ON p.SKUN = e.SKUN
					AND p.EVNT = e.NUMB
					AND p.PRIO = e.PRIO
		WHERE e.SKUN = @sku
		GROUP BY p.PSTA
		ORDER BY 2 DESC
	END

	RETURN @ReturnDate

END

Go

grant execute on dbo.udf_GetLatestPriceChangeDateForSku to [NT AUTHORITY\IUSR]
grant execute on dbo.udf_GetLatestPriceChangeDateForSku to [role_execproc]

go

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_GetLatestPriceChangeDateForSku" has been sucessfully amended';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_GetLatestPriceChangeDateForSku" might NOT have been amended';
    END
GO

