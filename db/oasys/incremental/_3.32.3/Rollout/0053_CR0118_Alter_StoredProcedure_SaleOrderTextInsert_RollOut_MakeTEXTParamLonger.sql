ALTER PROCEDURE [dbo].[SaleOrderTextInsert]
@OrderNumber CHAR (6), @Number CHAR (2), @Type CHAR (2), @SellingStoreId INT=0, @SellingStoreOrderId INT=0, @Text VARCHAR (200)
AS
BEGIN
	SET NOCOUNT ON;
	
	--insert order line
	insert into	CORTXT 
		(
		NUMB,
		LINE,
		[TYPE],
		SellingStoreId,
		SellingStoreOrderId,
		[TEXT]
		)
	values		(
		@orderNumber,
		@number,
		@Type,
		@sellingStoreId,
		@sellingStoreOrderId,
		@Text
	)
	
	return @@rowcount
END
Go
If @@Error = 0
   Print 'Success: The Alter Stored Procedure SaleOrderTextInsert for CR0118 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SaleOrderTextInsert for CR0118 has not been deployed successfully'
Go
