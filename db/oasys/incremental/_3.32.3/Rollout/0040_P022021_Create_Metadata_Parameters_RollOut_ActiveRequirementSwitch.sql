INSERT INTO Parameters( ParameterID , 
                        Description , 
                        StringValue , 
                        LongValue , 
                        BooleanValue , 
                        DecimalValue , 
                        ValueType
                      )
VALUES( -22021 , 
        'Enable P022-021 Picking Confirm Status Added' , 
        NULL , 
        NULL , 
        1 , 
        NULL , 
        3
      );
GO

IF @@error = 0
    BEGIN
        PRINT 'Success: The metadata change for P022-021 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: The metadata change for P022-021 has NOT been successfully deployed';
    END;
GO




