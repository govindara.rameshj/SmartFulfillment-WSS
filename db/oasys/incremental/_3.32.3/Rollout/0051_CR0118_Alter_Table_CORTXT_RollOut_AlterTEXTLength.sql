ALTER TABLE [dbo].CORTXT
ALTER COLUMN [TEXT] [nvarchar](200) NULL
GO
If @@Error = 0
   Print 'Success: The Alter table Column Text for CR0118 has been successfully deployed'
Else
   Print 'Failure: The Alter table Column Text for CR0118 might NOT have been deployed'
Go
