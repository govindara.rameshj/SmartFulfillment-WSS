If Exists(Select ParameterID From Parameters Where ParameterID = 980110)
	Begin
		Update Parameters Set BooleanValue = 1 Where ParameterID = 980110
	End
Else
	Begin
		Insert Into Parameters (
			[ParameterID],
			[Description],
			[StringValue],
			[LongValue],
			[BooleanValue],
			[DecimalValue],
			[ValueType]
		)
		Values (
			980110,
			'Enable Change Request CR00110',
			Null,
			Null,
			1,
            Null,
			3)
	End
If @@Error = 0
   Print 'Success: The active requirement switch parameter for P02240/CR0110 has been sucessfully created/updated'
Else
   Print 'Failure: The active requirement switch parameter for P02240/CR0110 might not have been created/updated'
Go