Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	-118,      'CR0118 Pick Instructions Switch', Null,        Null,      1,            Null,         3)
Go
If @@Error = 0
   Print 'Success: The active requirement switch for CR0118 has been successfully deployed'
Else
   Print 'Failure: active requirement switch for CR0118 might NOT have been deployed'
Go
Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	-109,      'CR0109 Delivery Instructions Switch', Null,        Null,      1,            Null,         3)
Go
If @@Error = 0
   Print 'Success: The active requirement switch for CR0109 has been successfully deployed'
Else
   Print 'Failure: active requirement switch for CR0109 might NOT have been deployed'
Go
