SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLTOTS') and name = 'ReceiptBarcode')
BEGIN
	PRINT 'Add ReceiptBarcode column to DLTOTS'
	ALTER TABLE dbo.DLTOTS ADD ReceiptBarcode varchar(30) NULL
END
GO

If @@Error = 0
   Print 'Success: Altering table DLTOTS for US19160 has been rolled out successfully'
ELSE
   Print 'Failure: Altering table DLTOTS for US19160 has not been rolled out successfully'
GO