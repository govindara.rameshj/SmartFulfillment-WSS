IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'AlertInsert') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure AlertInsert'
    EXEC ('CREATE PROCEDURE dbo.AlertInsert AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure AlertInsert')
GO
ALTER PROCEDURE [dbo].[AlertInsert] 
    @OrderNumber char(6),
    @ReceivedDate datetime,
    @AlertType tinyint
AS
BEGIN

INSERT INTO [dbo].[Alert]
           ([OrderNumb],
            [ReceivedDate],
            [AlertType]
           )
     VALUES
           (@OrderNumber,
            @ReceivedDate,
            @AlertType
            )
Return @@RowCount
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure AlertInsert for US16460 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure AlertInsert for US16460 has not been deployed'
GO