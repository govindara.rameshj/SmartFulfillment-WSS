﻿IF NOT EXISTS (select * from sys.columns where object_id = OBJECT_ID('Alert') and name = 'AlertType')
BEGIN
    PRINT 'Add AlertType column to Alert table'
    ALTER TABLE [dbo].[Alert]
    ADD [AlertType] tinyint NOT NULL
    CONSTRAINT [DF_Alert_AlertType] DEFAULT (1)
END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('Alert') and name = 'Refund')
BEGIN
    PRINT 'Drop Refund column from Alert table'

    UPDATE [dbo].[Alert]
    SET [AlertType] = 2
    WHERE [Refund] = 1

    ALTER TABLE [dbo].[Alert]
    DROP COLUMN [Refund]
END
GO

If @@Error = 0
   Print 'Success: The Alter Alert table for US16460 has been deployed successfully'
Else
   Print 'Failure: The Alter Alert table for US16460 has not been deployed'
GO