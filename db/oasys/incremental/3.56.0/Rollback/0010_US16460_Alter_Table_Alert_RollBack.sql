IF NOT EXISTS (select * from sys.columns where object_id = OBJECT_ID('Alert') and name = 'Refund')
BEGIN
    PRINT 'Add Refund column to Alert table'
    ALTER TABLE [dbo].[Alert]
    ADD [Refund] bit NOT NULL
    CONSTRAINT [DF_Alert_Refund] DEFAULT (0)
END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('Alert') and name = 'AlertType')
BEGIN
    PRINT 'Drop AlertType column from Alert table'
    
    UPDATE [dbo].[Alert]
    SET [Refund] = 1
    WHERE [AlertType] = 2

    ALTER TABLE [dbo].[Alert]
    DROP CONSTRAINT [DF_Alert_AlertType]
    
    ALTER TABLE [dbo].[Alert]
    DROP COLUMN [AlertType]
END
GO

IF EXISTS (select 1 from sys.default_constraints where parent_object_id = OBJECT_ID('Alert') and name = 'DF_Alert_Refund')
BEGIN
    PRINT 'Drop constraint of Refund column from Alert table'
    ALTER TABLE [dbo].[Alert]
    DROP CONSTRAINT [DF_Alert_Refund]
END
GO

If @@Error = 0
   Print 'Success: The Alter Alert table for US16460 has been rolled back successfully'
Else
   Print 'Failure: The Alter Alert table for US16460 has not been rolled back'
GO