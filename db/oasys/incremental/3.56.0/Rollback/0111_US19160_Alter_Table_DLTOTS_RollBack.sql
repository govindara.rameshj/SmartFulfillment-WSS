IF EXISTS (select 1 from sys.columns where object_id = object_id('DLTOTS') and name = 'ReceiptBarcode')
BEGIN
	PRINT 'Dropping ReceiptBarcode column from DLTOTS'
	ALTER TABLE dbo.DLTOTS DROP COLUMN ReceiptBarcode
END

If @@Error = 0
   Print 'Success: Altering table DLTOTS for US19160 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table DLTOTS for US19160 has not been rolled back successfully'
GO