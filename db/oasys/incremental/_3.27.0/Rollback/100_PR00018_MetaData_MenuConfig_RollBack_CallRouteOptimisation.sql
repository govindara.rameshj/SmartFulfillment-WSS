delete ProfileMenuAccess where MenuConfigID = 2060 and ID in (13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 30, 31, 32, 33, 40, 103, 104, 105, 106, 107, 108, 109, 110, 199)
go
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(MenuConfigID: 2060) for PR00018 has been successfully rolled back'
else
   print 'Failure: The metadata change to ProfileMenuAccess(MenuConfigID: 2060) for PR00018 has NOT been successfully rolled back'
go

------------------------------------------------------------------------
delete MenuConfig where ID = 2060
go
if @@error = 0
   print 'Success: The metadata change to MenuConfig for PR00018 has been successfully rolled back'
else
   print 'Failure: The metadata change to MenuConfig for PR00018 has NOT been successfully rolled back'
go

------------------------------------------------------------------------
alter table MenuConfig alter column Parameters char(50) null
go
if @@error = 0
   print 'Success: MenuConfig:Parameters field definition for PR00018 has been successfully rolled back'
else
   print 'Failure: MenuConfig:Parameters field definition for PR00018 has NOT been successfully rolled back'
go
