USE [OASYS]
GO

/****** Object:  Table [dbo].[Vehicle]    Script Date: 03/08/2012 12:04:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE [dbo].[Vehicle]

if @@error = 0
   print 'Success: Table [Vehicle] has been successfully dropped'
else
   print 'Failure: Table [Vehicle] has NOT been successfully dropped'
go
