

IF EXISTS( SELECT *
             FROM sys.objects
             WHERE object_id
                   = 
                   OBJECT_ID( N'[dbo].[usp_AddOrUpdateCollectionLines]'
                            )
               AND type IN( N'P' , N'PC'
                          )
         )
 BEGIN
DROP PROCEDURE [dbo].[usp_AddOrUpdateCollectionLines]
if @@error = 0
   print 'Success: Procedure usp_AddOrUpdateCollectionLines for P0018 has been successfully removed'
else
   print 'Failure: Procedure usp_AddOrUpdateCollectionLines for P0018 has NOT been successfully removed'
END
ELSE
BEGIN
     PRINT 'Success: Procedure usp_AddOrUpdateCollectionLines for P0018 is not available on the database';

END



