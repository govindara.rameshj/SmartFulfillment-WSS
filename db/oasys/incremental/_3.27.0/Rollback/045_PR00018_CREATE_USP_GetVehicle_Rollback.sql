USE [OASYS]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetVehicle]    Script Date: 03/08/2012 15:31:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE [dbo].[usp_GetVehicle]
GO

if @@error = 0
   print 'Success: Procedure usp_GetVehicle for P0018 has been successfully removed'
else
   print 'Failure: Procedure usp_GetVehicle for P0018 has NOT been successfully removed'
go
