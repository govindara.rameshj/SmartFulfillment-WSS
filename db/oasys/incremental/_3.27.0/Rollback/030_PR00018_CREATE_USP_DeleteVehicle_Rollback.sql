USE [OASYS]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteVehicle]    Script Date: 03/08/2012 15:29:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE [dbo].[usp_DeleteVehicle]
GO

if @@error = 0
   print 'Success: Procedure usp_DeleteVehicle for P0018 has been successfully removed'
else
   print 'Failure: Procedure usp_DeleteVehicle for P0018 has NOT been successfully removed'
go