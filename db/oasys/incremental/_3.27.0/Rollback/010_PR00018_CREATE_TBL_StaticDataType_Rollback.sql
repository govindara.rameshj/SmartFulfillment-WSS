USE [OASYS]
GO

/****** Object:  Table [dbo].[StaticDataType]    Script Date: 03/08/2012 12:01:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE [dbo].[StaticDataType]
GO

if @@error = 0
   print 'Success: Table StaticDataType for P0018 has been successfully removed'
else
   print 'Failure: Table StaticDataType for P0018 has NOT been successfully removed'
go
