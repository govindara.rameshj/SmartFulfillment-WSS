

IF EXISTS( SELECT *
                 FROM sys.objects
                 WHERE object_id
                       = 
                       OBJECT_ID( N'[dbo].[VehicleAllocation]'
                                )
                   AND type IN( N'U'
                              )
             )
BEGIN

DROP TABLE [dbo].[VehicleAllocation]

if @@error = 0
   print 'Success: Table VehicleAllocation for P0018 has been successfully removed'
else
   print 'Failure: Table VehicleAllocation for P0018 has NOT been successfully removed'
END
ELSE
BEGIN
    print 'Success: Table VehicleAllocation for P0018 is not available on the database'
END


