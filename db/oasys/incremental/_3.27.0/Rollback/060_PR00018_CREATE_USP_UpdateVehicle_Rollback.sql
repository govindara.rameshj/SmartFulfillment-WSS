USE [OASYS]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateVehicle]    Script Date: 03/08/2012 15:34:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE [dbo].[usp_UpdateVehicle]
GO

if @@error = 0
   print 'Success: Procedure usp_UpdateVehicle for P0018 has been successfully removed'
else
   print 'Failure: Procedure usp_UpdateVehicle for P0018 has NOT been successfully removed'
go