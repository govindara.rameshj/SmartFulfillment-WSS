
IF EXISTS (SELECT 1 FROM PARAMETERS WHERE ParameterID = 194)
BEGIN
DELETE FROM [Oasys].[dbo].[Parameters]
WHERE [ParameterID] = 194

IF @@ERROR = 0 AND @@ROWCOUNT = 1
   print 'Success: Parameter insert for Saved Routes Directory for P0018 has been successfully rolled back'
else
   print 'Failure: Parameter insert for Saved Routes Directory for P0018 has NOT been successfully rolled back'
END
ELSE
BEGIN
    print 'Success: Parameter for Saved Routes Directory for P0018 is not available on the database'
END
