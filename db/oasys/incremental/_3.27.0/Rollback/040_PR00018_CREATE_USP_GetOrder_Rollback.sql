USE [OASYS]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOrder]    Script Date: 03/08/2012 09:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE [dbo].[usp_GetOrder]
GO

if @@error = 0
   print 'Success: Procedure usp_GetOrder for P0018 has been successfully removed'
else
   print 'Failure: Procedure usp_GetOrder for P0018 has NOT been successfully removed'
go
