DELETE FROM [Oasys].[dbo].[Parameters]
WHERE [ParameterID] = 199

IF @@ERROR = 0 AND @@ROWCOUNT = 1
   print 'Success: Parameter insert for Paragon Flat File Directory for P0018 has been successfully rolled back'
else
   print 'Failure: Parameter insert for Paragon Flat File Directory for P0018 has NOT been successfully rolled back'
go


