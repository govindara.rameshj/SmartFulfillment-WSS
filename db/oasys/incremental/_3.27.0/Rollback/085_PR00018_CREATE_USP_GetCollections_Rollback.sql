
IF EXISTS( SELECT *
             FROM sys.objects
             WHERE object_id
                   = 
                   OBJECT_ID( N'[dbo].[usp_GetCollections]'
                            )
               AND type IN( N'P' , N'PC'
                          )
         )
    BEGIN
        DROP PROCEDURE dbo.usp_GetCollections;

        IF @@error = 0
            BEGIN
                PRINT 'Success: Procedure usp_GetCollections for P0018 has been successfully removed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Procedure usp_GetCollections for P0018 has NOT been successfully removed';
            END;
    END;
ELSE
    BEGIN
        PRINT 'Success: Procedure usp_GetCollections for P0018 is not available on the database';

    END;



