

IF EXISTS( SELECT *
                 FROM sys.objects
                 WHERE object_id
                       = 
                       OBJECT_ID( N'[dbo].[VehicleCollectionLine]'
                                )
                   AND type IN( N'U'
                              )
             )
BEGIN

DROP TABLE [dbo].[VehicleCollectionLine]

if @@error = 0
   print 'Success: Table VehicleCollectionLine for P0018 has been successfully removed'
else
   print 'Failure: Table VehicleCollectionLine for P0018 has NOT been successfully removed'
END
ELSE
BEGIN
    print 'Success: Table VehicleCollectionLine for P0018 is not available on the database'
END

