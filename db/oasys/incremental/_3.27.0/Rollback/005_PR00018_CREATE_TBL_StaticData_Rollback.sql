USE [OASYS]
GO

/****** Object:  Table [dbo].[StaticData]    Script Date: 03/08/2012 12:03:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE [dbo].[StaticData]
GO

if @@error = 0
   print 'Success: Table [StaticData] has been successfully dropped'
else
   print 'Failure: Table [StaticData] has NOT been successfully dropped'
go


