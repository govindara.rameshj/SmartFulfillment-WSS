USE [OASYS];
GO

/****** Object:  Table [dbo].[StaticData]    Script Date: 03/08/2012 12:03:21 ******/

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

SET ANSI_PADDING ON;
GO
IF NOT EXISTS( SELECT *
                 FROM sys.objects
                 WHERE object_id
                       = 
                       OBJECT_ID( N'[dbo].[StaticData]'
                                )
                   AND type IN( N'U'
                              )
             )
    BEGIN
        CREATE TABLE dbo.StaticData( ID int NULL , 
                                     StaticDataTypeID int NULL , 
                                     Description varchar( 250
                                                        )NULL , 
                                     [Value] varchar( 250
                                                    )NULL , 
                                     Notes varchar( 250
                                                  )NULL , 
                                     Enabled bit NULL
                                   )
        ON [PRIMARY];


        SET ANSI_PADDING ON;


        ALTER TABLE dbo.StaticData
        ADD CONSTRAINT DF_StaticData_Enabled DEFAULT 1 FOR Enabled;


        IF @@error = 0
            BEGIN
                PRINT 'Success: [StaticData] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: [StaticData] for P0018 has NOT been successfully deployed';
            END;


        GRANT SELECT ON StaticData TO [role_legacy];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Select security for [role_legacy] on table [StaticData] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Select security for [role_legacy] on table [StaticData] for P0018 has NOT been successfully deployed';
            END;


        GRANT SELECT ON StaticData TO [role_execproc];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Select security for [role_execproc] on table [StaticData] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Select security for [role_execproc] on table [StaticData] for P0018 has NOT been successfully deployed';
            END;

    END;
ELSE
    BEGIN
        PRINT 'Sucess: The table StaticDataType already exists on the database';
    END;