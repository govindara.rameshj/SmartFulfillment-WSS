USE [OASYS]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateVehicle]    Script Date: 03/08/2012 15:34:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateVehicle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateVehicle]
GO

CREATE PROCEDURE [dbo].[usp_UpdateVehicle] (
	@VehicleID INT,
	@Registration VARCHAR(50)='NO REGISTRATION',
	@VehicleTypeID INT=-1,
	@StartTime TIME='00:00:00',
	@StopTime TIME='00:00:00',
	@MaximumWeight VARCHAR(50)='0',
	@LastUpdateTime Datetime
	)
AS
BEGIN
	UPDATE Vehicle SET 
		[Registration] = @Registration,
		[VehicleTypeID] = @VehicleTypeID,
		[StartTime] = @StartTime,
		[StopTime] = @StopTime,
		[MaximumWeight] = @MaximumWeight,
		[LastUpdateTime]= @LastUpdateTime
	WHERE Vehicle.ID = @VehicleID

	RETURN @@ROWCOUNT
END

go
if @@error = 0
   print 'Success: [usp_UpdateVehicle] for P0018 has been successfully deployed'
else
   print 'Failure: [usp_UpdateVehicle] for P0018 has NOT been successfully deployed'
go

--Apply Permissions
grant execute on [usp_UpdateVehicle] to [role_legacy]
go
if @@error = 0
   print 'Success: execute security for [role_legacy] on [usp_UpdateVehicle] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_legacy] on [usp_UpdateVehicle] for P0018 has NOT been successfully deployed'
go

GRANT execute on [usp_UpdateVehicle] TO [role_execproc] 

GO 

if @@error = 0
   print 'Success: execute security for [role_execproc] on [usp_UpdateVehicle] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_execproc] on [usp_UpdateVehicle] for P0018 has NOT been successfully deployed'
go
