USE [OASYS]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDepot]    Script Date: 03/08/2012 15:30:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetDepot]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetDepot]
GO

CREATE PROCEDURE [dbo].[usp_GetDepot] (
	@DepotID INT = NULL
	)

--TODO: Remove @DepotID input Parameter

AS

BEGIN
	SET NOCOUNT ON;

	--DECLARE @DepotID INT


	/*All stores are numbered (as per "Store" table), but the format of this number (aka Depot ID) seems to differ throughout OASYS. 
	Route Optimisation simply reqires a 4 character store code, prefixed with an 8*/
	SET @DepotID = (SELECT TOP 1 '8'+ RIGHT('000'+ STOR, 3) FROM RETOPT WITH (NOLOCK))

	SELECT  @DepotID AS Id, 
	Name, 
	Address1, 
	Address3, 
	Address2, 
	Address4, 
	Address5, 
	PostCode, PhoneNumber, FaxNumber, Manager
	FROM Store WITH (NOLOCK)
	WHERE ID=(select top 1 Stor from retopt)
	ORDER BY 1 desc

	SET NOCOUNT OFF;
END

go
if @@error = 0
   print 'Success: [usp_GetDepot] for P0018 has been successfully deployed'
else
   print 'Failure: [usp_GetDepot] for P0018 has NOT been successfully deployed'
go

--Apply Permissions
grant execute on [usp_GetDepot] to [role_legacy]
go
if @@error = 0
   print 'Success: execute security for [role_legacy] on [usp_GetDepot] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_legacy] on [usp_GetDepot] for P0018 has NOT been successfully deployed'
go

GRANT execute on [usp_GetDepot] TO [role_execproc] 

GO 

if @@error = 0
   print 'Success: execute security for [role_execproc] on [usp_GetDepot] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_execproc] on [usp_GetDepot] for P0018 has NOT been successfully deployed'
go