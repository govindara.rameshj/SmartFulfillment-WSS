IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetOrder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetOrder]
GO

CREATE PROCEDURE [dbo].[usp_GetOrder] (
	@DeliveryDate DATETIME = NULL
	)

AS

BEGIN
	SET NOCOUNT ON;

	SELECT 
		NUMB = cl.NUMB, 
		LINE = cl.LINE, 
		QtyToBeDelivered = cl.QtyToBeDelivered, 
	--	DeliveryStatus = ch4.DeliveryStatus, 
		DeliveryStatusDescription = (
			select Description 
			from StaticData 
			where CONVERT(VARCHAR(10),StaticData.Value) = CONVERT(VARCHAR(10), ch4.DeliveryStatus)),
		NAME = ch4.NAME,
		OMOrderNumber = ch4.OMOrderNumber, 
		CustomerAddress1 = ISNULL(ch.ADDR1,''),
		CustomerAddress2 = ISNULL(ch.ADDR2,''),
		CustomerAddress3 = ISNULL(ch.ADDR3,''),
		CustomerAddress4 = ISNULL(ch.ADDR4,''),
		CustomerPostcode = ISNULL(ch.POST,''),
		SKUN = cl.SKUN, 
		DELD = left(ch.DELD,11), 
		VOLU = stk.VOLU, 
		WGHT = stk.WGHT,
		TotalOrderItemWeight = stk.WGHT * cl.QtyToBeDelivered,
		PLUD,
		DESCR,
		Notes = ISNULL(REPLACE(REPLACE((
			SELECT COALESCE(txt.[TEXT],'') AS Notes 
			FROM  cortxt txt
			WHERE numb= ch4.numb FOR XML PATH('') ),'<Notes>',' '),'</Notes>',''),''),
		ISNULL(ch.PHON,'') AS PHON ,
		va.VehicleTypeIndex as VechileSelected, 
		va.LastVehicleUpdateDateTime as LastUpdateDateTime
	FROM CORHDR ch
		INNER JOIN CORHDR4 ch4
			ON ch.NUMB = ch4.NUMB 
		INNER JOIN CORLIN cl WITH (NOLOCK)
			ON ch4.NUMB = cl.NUMB
		INNER JOIN STKMAS stk
			ON cl.SKUN = stk.SKUN 
	     LEFT OUTER JOIN VehicleAllocation va
	          on ch4.OMOrderNumber = va.OrderId	
	        and va.DeliveryDate =  ch4.DELD        
	WHERE ((cl.DeliveryStatus > 499 AND cl.DeliveryStatus < 700) OR (cl.DeliveryStatus = 899))
		AND cl.DeliverySource IN ('8' + (SELECT TOP 1 STOR FROM RETOPT WITH (NOLOCK) ))
		AND ch4.DELD = @DeliveryDate
		AND cl.IsDeliveryChargeItem = 0
		AND cl.QtyToBeDelivered > 0
		AND ch.DELI = 1		
	ORDER BY cl.NUMB, cl.LINE

	SET NOCOUNT OFF;
END
GO
if @@error = 0
   print 'Success: [usp_GetOrder] for P0018 has been successfully deployed'
else
   print 'Failure: [usp_GetOrder] for P0018 has NOT been successfully deployed'
go

--Apply Permissions
grant execute on [usp_GetOrder] to [role_legacy]
go
if @@error = 0
   print 'Success: execute security for [role_legacy] on [usp_GetOrder] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_legacy] on [usp_GetOrder] for P0018 has NOT been successfully deployed'
go

GRANT execute on [usp_GetOrder] TO [role_execproc] 

if @@error = 0
   print 'Success: execute security for [role_execproc] on [usp_GetOrder] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_execproc] on [usp_GetOrder] for P0018 has NOT been successfully deployed'
go




