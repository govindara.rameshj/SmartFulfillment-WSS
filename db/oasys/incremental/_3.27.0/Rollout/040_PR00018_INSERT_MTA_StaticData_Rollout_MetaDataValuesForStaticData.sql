use OASYS 

DELETE FROM STATICDATA
insert into StaticData (ID, StaticDataTypeID, Description, Value, Notes, [Enabled])
select 1, 1, 'Van', '930', 'Paragon Vehicle Type - Van', 1 union all
select 2, 1, '7.5t', '2800', 'Paragon Vehicle Type - 7.5t', 1 union all
select 3, 1, '13t', '5000', 'Paragon Vehicle Type - 13t', 0 union all
select 4, 1, '18t', '8500', 'Paragon Vehicle Type - 18t', 1 union all
select 5, 1, '26t', '13300', 'Paragon Vehicle Type - 26t', 1 union all
select 6, 1, '32t', '17000', 'Paragon Vehicle Type - 32t', 0 union all
select 7, 1, 'Artic', '20000', 'Paragon Vehicle Type - Artic', 0 union all
select 8, 2, 'Order Update - No Delivery Required', '0', NULL, NULL union all
select 9, 2, 'Connection Error - Order Update to OM failed', '10', NULL, NULL union all
select 10, 2, 'OM - Delivery request returned a failed state', '120', NULL, NULL union all
select 11, 2, 'OM - Delivery request contained invalid data', '130', NULL, NULL union all
select 12, 2, 'OM - Delivery Request Accepted', '200', NULL, NULL union all
select 13, 2, 'Connection Error - Fulfilment request failed confirmation ( timeout)', '210', NULL, NULL union all
select 14, 2, 'OM - Fulfilment request returned a failed state', '220', NULL, NULL union all
select 15, 2, 'OM - Fulfilment request contained invalid data', '230', NULL, NULL union all
select 16, 2, 'IBT Out created at Distribution Site', '300', NULL, NULL union all
select 17, 2, 'Connection Error - IBT Out notification failed', '310', NULL, NULL union all
select 18, 2, 'OM - IBT Out notification returned a failed state', '320', NULL, NULL union all
select 19, 2, 'OM - IBT Out notification contained invalid data', '330', NULL, NULL union all
select 20, 2, 'IBT In created at Originating Site', '399', NULL, NULL union all
select 21, 2, 'Distribution Site confirms receipt of order', '400', NULL, NULL union all
select 22, 2, 'Connection Error - Receipt notification failed', '410', NULL, NULL union all
select 23, 2, 'Receipt notification returned a failed state', '420', NULL, NULL union all
select 24, 2, 'Receipt notification contained invalid data', '430', NULL, NULL union all
select 25, 2, 'OM - Receipt notification recorded', '450', NULL, NULL union all
select 26, 2, 'Connection Error - Receipt status update failed', '460', NULL, NULL union all
select 27, 2, 'Receipt status update returned a failed state', '470', NULL, NULL union all
select 28, 2, 'Receipt status update contained invalid data', '480', NULL, NULL union all
select 29, 2, 'Receipt status update successful', '499', NULL, NULL union all
select 30, 2, 'Picking Sheet Printed', '500', NULL, NULL union all
select 31, 2, 'Picking notification failed on connection', '510', NULL, NULL union all
select 32, 2, 'Picking notification returned a failed state', '520', NULL, NULL union all
select 33, 2, 'Picking notification contained invalid data', '530', NULL, NULL union all
select 34, 2, 'OM - Picking notification recorded', '550', NULL, NULL union all
select 35, 2, 'Connection Error -  Picking status update failed', '560', NULL, NULL union all
select 36, 2, 'Picking status update returned a failed state', '570', NULL, NULL union all
select 37, 2, 'Picking status update contained invalid data', '580', NULL, NULL union all
select 38, 2, 'Picking status update successful', '599', NULL, NULL union all
select 39, 2, 'Despatch created', '700', NULL, NULL union all
select 40, 2, 'Connection Error - Despatch notification failed', '710', NULL, NULL union all
select 41, 2, 'Despatch notification returned a failed state', '720', NULL, NULL union all
select 42, 2, 'Despatch notification contained invalid data', '730', NULL, NULL union all
select 43, 2, 'OM - Despatch notification recorded', '750', NULL, NULL union all
select 44, 2, 'Connection Error - Despatch status update failed', '760', NULL, NULL union all
select 45, 2, 'Despatch status update returned a failed state', '770', NULL, NULL union all
select 46, 2, 'Despatch status update contained invalid data', '780', NULL, NULL union all
select 47, 2, 'Despatch status update successful', '799', NULL, NULL union all
select 48, 2, 'Failed \ Re-Delivery created', '800', NULL, NULL union all
select 49, 2, 'Connection Error - Undelivered notification failed', '810', NULL, NULL union all
select 50, 2, 'Undelivered notification returned a failed state', '820', NULL, NULL union all
select 51, 2, 'Undelivered notification contained invalid data', '830', NULL, NULL union all
select 52, 2, 'OM - Undelivered notification recorded', '850', NULL, NULL union all
select 53, 2, 'Connection Error - Undelivered status update failed', '860', NULL, NULL union all
select 54, 2, 'Undelivered status update returned a failed state', '870', NULL, NULL union all
select 55, 2, 'Undelivered status update contained invalid data', '880', NULL, NULL union all
select 56, 2, 'Undelivered status update successful', '899', NULL, NULL union all
select 57, 2, 'Despatch Complete', '900', NULL, NULL union all
select 58, 2, 'Connection Error - Delivered notification failed', '910', NULL, NULL union all
select 59, 2, 'Delivered notification returned a failed state', '920', NULL, NULL union all
select 60, 2, 'Delivered notification contained invalid data', '930', NULL, NULL union all
select 61, 2, 'OM - Delivered notification recorded', '950', NULL, NULL union all
select 62, 2, 'Connection Error - Delivered status update failed', '960', NULL, NULL union all
select 63, 2, 'Delivered status update returned a failed state', '970', NULL, NULL union all
select 64, 2, 'Delivered status update contained invalid data', '980', NULL, NULL union all
select 65, 2, 'Despatch Confirmed – Order Complete', '999', NULL, NULL union all
select 66, 1, '15t Hiab', '6300', 'Paragon Vehicle Type - Hiab', 1 union all
select 67, 1, 'Other', '50000', 'Paragon Vehicle Type - Other', 1 

if @@ROWCOUNT = 67 and @@error = 0
   print 'Success: Data rows were for StaticData for P0018 were successfully deployed'
else
   print 'Failure: Data rows were for StaticData for P0018 were NOT successfully deployed'
