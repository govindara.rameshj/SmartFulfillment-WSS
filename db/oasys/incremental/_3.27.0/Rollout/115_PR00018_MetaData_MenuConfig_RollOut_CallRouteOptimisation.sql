alter table MenuConfig alter column Parameters varchar(250) null
go
if @@error = 0
   print 'Success: MenuConfig:Parameters field definition for PR00018 has been successfully amended'
else
   print 'Failure: MenuConfig:Parameters field definition for PR00018 has NOT been successfully amended'
go

------------------------------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM MENUCONFIG WHERE ID= 2060)
BEGIN
    declare @Parameters varchar(250)

    set @Parameters = '"Data Source=%SERVERNAMETOKEN%;Initial Catalog=OASYS;Integrated Security=True"'
    set @Parameters = replace(@Parameters, '%SERVERNAMETOKEN%', convert(varchar(max), serverproperty('ServerName')))

    insert MenuConfig values (2060, 2000, 'Route Optimisation', 'Route\WSS.RouteOptimisation.exe', NULL, 1, @Parameters, 'icons\icon_g.ico', 1, 0, 5, 0, 0, null, null, 0, 0, 0)
    if @@error = 0
       print 'Success: The metadata change to MenuConfig for PR00018 has been successfully deployed'
    else
    print 'Failure: The metadata change to MenuConfig for PR00018 has NOT been successfully deployed'

END
ELSE
BEGIN
    print 'Success: The metadata change to MenuConfig for PR00018 is already available on the table'
END
------------------------------------------------------------------------


IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=13 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (13,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 13, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 13, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 13, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go


IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=15 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (15,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 15, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 15, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 15, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go


IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=16 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (16,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 16, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 16, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 16, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=17 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (17,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 17, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 17, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 17, MenuConfigID: 2060) for PR00018 is already available on the table'
END   
 
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=18 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (18,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 18, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 18, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 18, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=19 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (19,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 19, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 19, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 19, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=20 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (20,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 20, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 20, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 20, MenuConfigID: 2060) for PR00018 is already available on the table'
END

go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=21 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (21,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 21, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 21, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 21, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=22 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (22,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 22, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 22, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 22, MenuConfigID: 2060) for PR00018 is already available on the table'
END

go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=23 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (23,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 23, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 23, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 23, MenuConfigID: 2060) for PR00018 is already available on the table'
END

go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=24 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (24,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 24, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 24, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 24, MenuConfigID: 2060) for PR00018 is already available on the table'
END

go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=25 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (25,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 25, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 25, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 25, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=27 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (27,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 27, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 27, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 27, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=30 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (30,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 30, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 30, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 30, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=31 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (31,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 31, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 31, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 31, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=32 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (32,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 32, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 32, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 32, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go


IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=33 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (33,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 33, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 33, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 33, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=40 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (40,  2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 40, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 40, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 40, MenuConfigID: 2060) for PR00018 is already available on the table'
END
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=103 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (103, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 103, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 103, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 103, MenuConfigID: 2060) for PR00018 is already available on the table'
END
   
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=104 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (104, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 104, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 104, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 104, MenuConfigID: 2060) for PR00018 is already available on the table'
END   
 
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=105 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (105, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 105, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 105, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 105, MenuConfigID: 2060) for PR00018 is already available on the table'
END 
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=106 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (106, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 106, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 106, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 106, MenuConfigID: 2060) for PR00018 is already available on the table'
END 
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=107 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (107, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 107, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 107, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 107, MenuConfigID: 2060) for PR00018 is already available on the table'
END 
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=108 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (108, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 108, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 108, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 108, MenuConfigID: 2060) for PR00018 is already available on the table'
END 
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=109 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (109, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 109, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 109, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 109, MenuConfigID: 2060) for PR00018 is already available on the table'
END 
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=110 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (110, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 110, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 110, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
 END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 110, MenuConfigID: 2060) for PR00018 is already available on the table'
END 
go

IF NOT EXISTS (SELECT 1 FROM ProfilemenuAccess where Id=199 and MenuConfigID=2060)
BEGIN
insert ProfileMenuAccess values (199, 2060, 1, 0, 1, 0, 0, NULL, NULL)
if @@error = 0
   print 'Success: The metadata change to ProfileMenuAccess(ID: 199, MenuConfigID: 2060) for PR00018 has been successfully deployed'
else
   print 'Failure: The metadata change to ProfileMenuAccess(ID: 199, MenuConfigID: 2060) for PR00018 has NOT been successfully deployed'
END
ELSE
BEGIN
    print 'Success: The metadata change to ProfileMenuAccess(ID: 199, MenuConfigID: 2060) for PR00018 is already available on the table'
END 
go