USE [OASYS]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetVehicleType]    Script Date: 03/08/2012 15:32:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetVehicleType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetVehicleType]
GO
CREATE PROCEDURE [dbo].[usp_GetVehicleType]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT ID, Description 
	FROM  STATICDATA WITH (NOLOCK)
	WHERE STATICDATATYPEID =	(
							SELECT ID 
							FROM STATICDATATYPE ITH (NOLOCK)
							WHERE DESCRIPTION  ='VehicleType'
								)
		AND [Enabled] = 1
	ORDER BY CAST(Value as int) 
	/*
	SELECT ID, Description 
	FROM dbo.VehicleType WITH (NOLOCK)
	WHERE ENABLED=1

	*/

	SET NOCOUNT OFF;
END

go
if @@error = 0
   print 'Success: [usp_GetVehicleType] for P0018 has been successfully deployed'
else
   print 'Failure: [usp_GetVehicleType] for P0018 has NOT been successfully deployed'
go

--Apply Permissions
grant execute on [usp_GetVehicleType] to [role_legacy]
go
if @@error = 0
   print 'Success: execute security for [role_legacy] on [usp_GetVehicleType] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_legacy] on [usp_GetVehicleType] for P0018 has NOT been successfully deployed'
go

GRANT execute on [usp_GetVehicleType] TO [role_execproc] 

GO 

if @@error = 0
   print 'Success: execute security for [role_execproc] on [usp_GetVehicleType] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_execproc] on [usp_GetVehicleType] for P0018 has NOT been successfully deployed'
go