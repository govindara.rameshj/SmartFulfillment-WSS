
CREATE TABLE dbo.VehicleCollectionHeader( CollectionID int IDENTITY(1 , 1
                                                                   )
                                                           NOT NULL , 
                                          Description varchar( 100
                                                             ) , 
                                          TotalWeight int NOT NULL , 
                                          Name varchar( 50
                                                      ) , 
                                          CustomerAddress1 varchar( 50
                                                                  ) , 
                                          CustomerAddress2 varchar( 50
                                                                  ) , 
                                          CustomerAddress3 varchar( 50
                                                                  ) , 
                                          PostCode varchar( 8
                                                          ) , 
                                          VehicleTypeIndex varchar( 10
                                                                  )NOT NULL , 
                                          DeliveryDate [date] NOT NULL , 
                                          CONSTRAINT PK_VehicleCollectionHeader PRIMARY KEY CLUSTERED( CollectionID ASC
                                                                                                     )
                                              WITH( PAD_INDEX = OFF , STATISTICS_NORECOMPUTE = OFF , IGNORE_DUP_KEY = OFF , ALLOW_ROW_LOCKS = ON , ALLOW_PAGE_LOCKS = ON
                                                  )ON [PRIMARY]
                                        )
ON [PRIMARY];

GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT SELECT ON VehicleCollectionHeader TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Select security for [role_legacy] on table [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Select security for [role_legacy] on table [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT INSERT ON VehicleCollectionHeader TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Insert security for [role_legacy] on table [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Insert security for [role_legacy] on table [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO


GRANT UPDATE ON VehicleCollectionHeader TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Update security for [role_legacy] on table [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Update security for [role_legacy] on table [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO


GRANT DELETE ON VehicleCollectionHeader TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Delete security for [role_legacy] on table [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Delete security for [role_legacy] on table [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT SELECT ON VehicleCollectionHeader TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: Select security for [role_execproc] on table [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Select security for [role_execproc] on table [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO


GRANT INSERT ON VehicleCollectionHeader TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Insert security for [role_execproc] on table [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Insert security for [role_execproc] on table [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO


GRANT UPDATE ON VehicleCollectionHeader TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Update security for [role_execproc] on table [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Update security for [role_execproc] on table [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO


GRANT DELETE ON VehicleCollectionHeader TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Delete security for [role_execproc] on table [VehicleCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Delete security for [role_execproc] on table [VehicleCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO



