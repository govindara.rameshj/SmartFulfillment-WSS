USE [OASYS];
GO

/****** Object:  Table [dbo].[Vehicle]    Script Date: 03/08/2012 12:04:20 ******/

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

SET ANSI_PADDING ON;
GO

IF NOT EXISTS( SELECT *
                 FROM sys.objects
                 WHERE object_id
                       = 
                       OBJECT_ID( N'[dbo].[Vehicle]'
                                )
                   AND type IN( N'U'
                              )
             )
    BEGIN

        CREATE TABLE dbo.Vehicle( ID int IDENTITY(1 , 1
                                                 )
                                         NOT NULL , 
                                  DepotID varchar( 50
                                                 )NULL , 
                                  Registration varchar( 50
                                                      )NULL , 
                                  VehicleTypeID int NULL , 
                                  StartTime [time]( 7
                                                  )NULL , 
                                  StopTime [time]( 7
                                                 )NULL , 
                                  MaximumWeight decimal( 19 , 2
                                                       )NULL , 
                                  MaximumVolume decimal( 19 , 2
                                                       )NULL , 
                                  CONSTRAINT PK_Vehicle PRIMARY KEY CLUSTERED( ID ASC
                                                                             )
                                      WITH( PAD_INDEX = OFF , STATISTICS_NORECOMPUTE = OFF , IGNORE_DUP_KEY = OFF , ALLOW_ROW_LOCKS = ON , ALLOW_PAGE_LOCKS = ON
                                          )ON [PRIMARY]
                                )
        ON [PRIMARY];


        SET ANSI_PADDING ON;


        ALTER TABLE dbo.Vehicle
        ADD CONSTRAINT DF_Vehicle_StartTime DEFAULT '00:00:00' FOR StartTime;


        ALTER TABLE dbo.Vehicle
        ADD CONSTRAINT DF_Vehicle_StopTime DEFAULT '00:00:00' FOR StopTime;


        ALTER TABLE dbo.Vehicle
        ADD CONSTRAINT DF_Vehicle_MaximumWeight DEFAULT 0 FOR MaximumWeight;


        ALTER TABLE dbo.Vehicle
        ADD CONSTRAINT DF_Vehicle_MaximumVolume DEFAULT 0 FOR MaximumVolume;

        IF @@error = 0
            BEGIN
                PRINT 'Success: [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: [Vehicle] for P0018 has NOT been successfully deployed';
            END;

        GRANT INSERT ON Vehicle TO [role_legacy];
        IF @@error = 0
            BEGIN
                PRINT 'Success: Insert security for [role_legacy] on table [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Insert security for [role_legacy] on table [Vehicle] for P0018 has NOT been successfully deployed';
            END;

        GRANT SELECT ON Vehicle TO [role_legacy];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Select security for [role_legacy] on table [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Select security for [role_legacy] on table [Vehicle] for P0018 has NOT been successfully deployed';
            END;

        GRANT UPDATE ON Vehicle TO [role_legacy];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Update security for [role_legacy] on table [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Update security for [role_legacy] on table [Vehicle] for P0018 has NOT been successfully deployed';
            END;

        GRANT DELETE ON Vehicle TO [role_legacy];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Delete security for [role_legacy] on table [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Delete security for [role_legacy] on table [Vehicle] for P0018 has NOT been successfully deployed';
            END;

        GRANT INSERT ON Vehicle TO [role_execproc];
        IF @@error = 0
            BEGIN
                PRINT 'Success: Insert security for [role_execproc] on table [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Insert security for [role_execproc] on table [Vehicle] for P0018 has NOT been successfully deployed';
            END;

        GRANT SELECT ON Vehicle TO [role_execproc];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Select security for [role_execproc] on table [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Select security for [role_execproc] on table [Vehicle] for P0018 has NOT been successfully deployed';
            END;

        GRANT UPDATE ON Vehicle TO [role_execproc];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Update security for [role_execproc] on table [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Update security for [role_execproc] on table [Vehicle] for P0018 has NOT been successfully deployed';
            END;

        GRANT DELETE ON Vehicle TO [role_execproc];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Delete security for [role_execproc] on table [Vehicle] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Delete security for [role_execproc] on table [Vehicle] for P0018 has NOT been successfully deployed';
            END;

    END;
ELSE
    BEGIN
        PRINT 'Sucess: The table Vehicle already exists on the database';
    END;