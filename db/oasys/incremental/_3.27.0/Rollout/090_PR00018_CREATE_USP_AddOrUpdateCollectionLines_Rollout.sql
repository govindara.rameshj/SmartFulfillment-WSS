

CREATE PROCEDURE usp_AddOrUpdateCollectionLines  @Id int = null,
                                              @CollectionId int , 
                                              @Description varchar( 100
                                                                  ) , 
                                              @Weight int , 
                                              @Quantity int , 
                                              @TotalWeight int , 
                                              @DeliveryDate date
AS
BEGIN
    IF EXISTS( SELECT 1
                 FROM VehicleCollectionLine
                 WHERE ID
                       = 
                       @Id
                 AND
                      CollectionID
                       = 
                       @collectionid
                  AND DeliveryDate
                       = 
                       @DeliveryDate
             )
        BEGIN
            UPDATE VehicleCollectionLine
            SET Description = @Description , 
                Weight = @Weight , 
                Quantity = @Quantity , 
                TotalWeight = @TotalWeight
              WHERE CollectionID
                    = 
                    @CollectionID
             AND ID = @Id;
        END;
    ELSE
        BEGIN
            INSERT INTO VehicleCollectionLine( CollectionID,
             [Description] , 
                                                Weight , 
                                                Quantity , 
                                                TotalWeight , 
                                                DeliveryDate
                                              )
            VALUES( @CollectionId,
                    @Description , 
                    @Weight , 
                    @Quantity , 
                    @TotalWeight , 
                    @DeliveryDate
                  );
        END;
 SELECT SCOPE_IDENTITY()
END;

GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [usp_AddOrUpdateCollectionLines] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [usp_AddOrUpdateCollectionLines] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT EXECUTE ON usp_AddOrUpdateCollectionLines TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_legacy] on [usp_AddOrUpdateCollectionLines] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_legacy] on [usp_AddOrUpdateCollectionLines] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT EXECUTE ON usp_AddOrUpdateCollectionLines TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_execproc] on [usp_AddOrUpdateCollectionLines] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_execproc] on [usp_AddOrUpdateCollectionLines] for P0018 has NOT been successfully deployed';
    END;
GO






