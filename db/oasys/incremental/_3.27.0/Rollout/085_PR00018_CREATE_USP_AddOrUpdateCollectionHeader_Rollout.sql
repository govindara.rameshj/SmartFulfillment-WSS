
CREATE PROCEDURE [dbo].[usp_AddOrUpdateCollectionHeader] @CollectionId int = null , 
                                               @Description varchar( 100
                                                                   ) , 
                                               @TotalWeight int , 
                                               @Name varchar( 50
                                                            ) , 
                                               @Address1 varchar( 50
                                                                ) , 
                                               @Address2 varchar( 50
                                                                ) , 
                                               @Address3 varchar( 50
                                                                ) , 
                                               @PostCode varchar( 8
                                                                ) , 
                                               @VehicleTypeIndex varchar( 10
                                                                        ) , 
                                               @DeliveryDate date,
                                               
                                               @LastUpdateDateTime datetime
                                               
AS
BEGIN
    IF EXISTS( SELECT 1
                 FROM vehicleCollectionHeader
                 WHERE CollectionID
                       = 
                       @collectionid
                   AND DeliveryDate
                       = 
                       @DeliveryDate
             )
        BEGIN
            UPDATE vehicleCollectionHeader
            SET vehicleTypeIndex = @VehicleTypeIndex , 
                Description = @Description , 
                TotalWeight = @TotalWeight , 
                Name = @Name , 
                CustomerAddress1 = @Address1 , 
                CustomerAddress2 = @Address2 , 
                CustomerAddress3 = @Address3 , 
                PostCode = @PostCode,
                LastUpdateDateTime = @LastUpdateDateTime
              WHERE CollectionID
                    = 
                    @CollectionID;
        END;
    ELSE
        BEGIN
            INSERT INTO vehicleCollectionHeader( Description , 
                                                 TotalWeight , 
                                                 Name , 
                                                 CustomerAddress1 , 
                                                 CustomerAddress2 , 
                                                 CustomerAddress3 , 
                                                 PostCode , 
                                                 VehicleTypeIndex , 
                                                 DeliveryDate,
                                                 LastUpdateDateTime
                                               )
            VALUES( @Description , 
                    @TotalWeight , 
                    @Name , 
                    @Address1 , 
                    @Address2 , 
                    @Address3 , 
                    @PostCode , 
                    @VehicleTypeIndex , 
                    @DeliveryDate,
                    @LastUpdateDateTime
                  );
        END;
        
 SELECT SCOPE_IDENTITY()
 
END;

GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [usp_AddOrUpdateCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [usp_AddOrUpdateCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT EXECUTE ON usp_AddOrUpdateCollectionHeader TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_legacy] on [usp_AddOrUpdateCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_legacy] on [usp_AddOrUpdateCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT EXECUTE ON usp_AddOrUpdateCollectionHeader TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_execproc] on [usp_AddOrUpdateCollectionHeader] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_execproc] on [usp_AddOrUpdateCollectionHeader] for P0018 has NOT been successfully deployed';
    END;
GO


