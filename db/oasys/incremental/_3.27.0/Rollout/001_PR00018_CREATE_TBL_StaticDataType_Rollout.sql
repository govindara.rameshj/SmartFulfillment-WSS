USE [OASYS];
GO

/****** Object:  Table [dbo].[StaticDataType]    Script Date: 03/08/2012 12:01:58 ******/

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

SET ANSI_PADDING ON;
GO

IF NOT EXISTS( SELECT *
                 FROM sys.objects
                 WHERE object_id
                       = 
                       OBJECT_ID( N'[dbo].[StaticDataType]'
                                )
                   AND type IN( N'U'
                              )
             )
    BEGIN

        CREATE TABLE dbo.StaticDataType( ID int NULL , 
                                         Description varchar( 50
                                                            )NULL , 
                                         Notes varchar( 50
                                                      )NULL
                                       )
        ON [PRIMARY];


        IF @@error = 0
            BEGIN
                PRINT 'Success: [StaticDataType] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: [StaticDataType] for P0018 has NOT been successfully deployed';
            END;


        --Apply Permissions
        GRANT SELECT ON StaticDataType TO [role_legacy];

        IF @@error = 0
            BEGIN
                PRINT 'Success: Select security for [role_legacy] on table [StaticDataType] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Select security for [role_legacy] on table [StaticDataType] for P0018 has NOT been successfully deployed';
            END;


        GRANT SELECT ON StaticDataType TO [role_execproc];


        IF @@error = 0
            BEGIN
                PRINT 'Success: Select security for [role_execproc] on table [StaticDataType] for P0018 has been successfully deployed';
            END
        ELSE
            BEGIN
                PRINT 'Failure: Select security for [role_execproc] on table [StaticDataType] for P0018 has NOT been successfully deployed';
            END;
    END;
ELSE
    BEGIN
        PRINT 'Sucess: The table StaticDataType already exists on the database';
    END;


