USE [OASYS];
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVehicle]    Script Date: 03/08/2012 15:31:08 ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF EXISTS( SELECT *
             FROM sys.objects
             WHERE object_id
                   = 
                   OBJECT_ID( N'[dbo].[usp_GetVehicle]'
                            )
               AND type IN( N'P' , N'PC'
                          )
         )
    BEGIN
        DROP PROCEDURE dbo.usp_GetVehicle
    END;
GO
CREATE PROCEDURE dbo.usp_GetVehicle( @VehicleID int = NULL , 
                                     @DepotID int = NULL
                                   )
AS
BEGIN
    SELECT Vehicle.ID , 
           Vehicle.DepotID , 
           Vehicle.Registration , 
           Vehicle.VehicleTypeID , 
           Vehicle.StartTime , 
           Vehicle.StopTime , 
           Vehicle.MaximumWeight AS MaximumWeight , 
           Vehicle.MaximumVolume , 
           VehicleTypeDescription = ISNULL( VehicleType.Description , ''
                                          ) , 
           Vehicle.lastUpdateTime
      FROM
           Vehicle WITH ( NOLOCK
                        ) INNER JOIN StaticData AS VehicleType
           ON Vehicle.VehicleTypeID
              = 
              VehicleType.ID
      WHERE Vehicle.ID
            = 
            COALESCE( @VehicleID , Vehicle.ID
                    )
        AND Vehicle.DepotID
            = 
            COALESCE( @DepotID , Vehicle.DepotID
                    );
END;
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [usp_GetVehicle] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [usp_GetVehicle] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT EXECUTE ON usp_GetVehicle TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_legacy] on [usp_GetVehicle] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_legacy] on [usp_GetVehicle] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT EXECUTE ON usp_GetVehicle TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_execproc] on [usp_GetVehicle] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_execproc] on [usp_GetVehicle] for P0018 has NOT been successfully deployed';
    END;
GO