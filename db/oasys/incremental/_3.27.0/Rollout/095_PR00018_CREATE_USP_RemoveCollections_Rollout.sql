
CREATE PROCEDURE usp_RemoveCollections @CollectionId int
AS
BEGIN
    DELETE FROM VehicleCollectionHeader
      WHERE CollectionId
            = 
            @CollectionId;
END;

GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [usp_RemoveCollections] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [usp_RemoveCollections] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT EXECUTE ON usp_RemoveCollections TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_legacy] on [usp_RemoveCollections] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_legacy] on [usp_RemoveCollections] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT EXECUTE ON usp_RemoveCollections TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_execproc] on [usp_RemoveCollections] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_execproc] on [usp_RemoveCollections] for P0018 has NOT been successfully deployed';
    END;
GO



