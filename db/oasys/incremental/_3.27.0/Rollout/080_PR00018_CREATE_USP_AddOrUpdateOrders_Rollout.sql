

CREATE PROCEDURE dbo.usp_AddOrUpdateOrders @OrderId int , 
                                           @VehicleTypeIndex varchar( 10
                                                                    ) , 
                                           @DeliveryDate date , 
                                           @LastUpdateDateTime datetime
AS
BEGIN
    IF EXISTS( SELECT 1
                 FROM vehicleAllocation
                 WHERE OrderID
                       = 
                       @OrderId
                   AND DeliveryDate
                       = 
                       @DeliveryDate
             )
        BEGIN
            UPDATE vehicleAllocation
            SET vehicleTypeIndex = @VehicleTypeIndex , 
                lastvehicleupdateDateTime = @LastUpdateDateTime
              WHERE orderid
                    = 
                    @OrderId;
        END;
    ELSE
        BEGIN
            INSERT INTO VehicleAllocation( OrderID , 
                                           VehicleTypeIndex , 
                                           DeliveryDate , 
                                           LastvehicleUpdateDateTime
                                         )
            VALUES( @OrderId , 
                    @VehicleTypeIndex , 
                    @DeliveryDate , 
                    @LastUpdateDateTime
                  );
        END;
END;
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [usp_AddOrUpdateOrders] for P0018 has been successfully deployed';
    END;
ELSE
    BEGIN
        PRINT 'Failure: [usp_AddOrUpdateOrders] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT EXECUTE ON usp_AddOrUpdateOrders TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_legacy] on [usp_AddOrUpdateOrders] for P0018 has been successfully deployed';
    END;
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_legacy] on [usp_AddOrUpdateOrders] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT EXECUTE ON usp_AddOrUpdateOrders TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_execproc] on [usp_AddOrUpdateOrders] for P0018 has been successfully deployed';
    END;
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_execproc] on [usp_AddOrUpdateOrders] for P0018 has NOT been successfully deployed';
    END;
GO



