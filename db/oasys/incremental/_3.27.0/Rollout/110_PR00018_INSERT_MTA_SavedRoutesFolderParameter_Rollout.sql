INSERT INTO Oasys.dbo.Parameters( ParameterID , 
                                  Description , 
                                  StringValue , 
                                  LongValue , 
                                  BooleanValue , 
                                  DecimalValue , 
                                  ValueType
                                )
VALUES( 194 , 
        'Saved Routes Directory' , 
        'C:\Program Files\CTS Retail\Oasys3\Route\SavedRoutes' , 
        NULL , 
        0 , 
        NULL , 
        0
      );

IF @@ERROR = 0
AND @@ROWCOUNT = 1
    BEGIN
        PRINT 'Success: Parameter insert for Saved Routes Directory for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Parameter insert for Saved Routes Directory for P0018 has NOT been successfully deployed';
    END;
GO

