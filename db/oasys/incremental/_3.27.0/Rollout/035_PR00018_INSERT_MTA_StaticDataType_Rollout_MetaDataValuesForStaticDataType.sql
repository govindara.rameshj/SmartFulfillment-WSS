USE OASYS;

DELETE FROM STATICDATATYPE;
INSERT INTO StaticDataType( ID , 
                            Description , 
                            Notes
                          )
SELECT 1 , 
       'VehicleType' , 
       'Vehicle types used by Paragon'
UNION ALL
SELECT 2 , 
       'DeliveryStatusType' , 
       'System wide delivery status types';

IF @@ROWCOUNT = 2
AND @@error = 0
    BEGIN
        PRINT 'Success: Data rows were for StaticDataType for P0018 were successfully deployed';
    END;
ELSE
    BEGIN
        PRINT 'Failure: Data rows were for StaticDataType for P0018 were NOT successfully deployed';
    END;