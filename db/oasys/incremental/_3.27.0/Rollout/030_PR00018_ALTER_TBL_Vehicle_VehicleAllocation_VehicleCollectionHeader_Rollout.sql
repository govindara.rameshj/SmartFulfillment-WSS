ALTER TABLE Vehicle
ADD LastUpdateTime datetime NULL;
GO

IF @@error = 0
    BEGIN
		update Vehicle set LastUpdateTime = '2012 jan 1'
        PRINT 'Success: New field [LastUpdateTime] added to table Vehicle for P018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: New field [LastUpdateTime] added to table Vehicle for P018 has NOT been successfully deployed';
    END;
GO

--------------------------------------------------------------------------


ALTER TABLE VehicleCollectionHeader
ADD LastUpdateDateTime datetime NULL;
GO

IF @@error = 0
    BEGIN
        PRINT 'Success: New field [LastUpdateTime] added to table VehicleCollectionHeader for P018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: New field [LastUpdateTime] added to table VehicleCollectionHeader for P018 has NOT been successfully deployed';
    END;
GO

--------------------------------------------------------------------------


ALTER TABLE VehicleAllocation
ADD LastVehicleUpdateDateTime datetime NULL;
GO

IF @@error = 0
    BEGIN
        PRINT 'Success: New field [LastVehicleUpdateDateTime] added to table VehicleAllocation for P018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: New field [LastVehicleUpdateDateTime] added to table VehicleAllocation for P018 has NOT been successfully deployed';
    END;
GO

--------------------------------------------------------------------------


