USE [OASYS];
GO

/****** Object:  StoredProcedure [dbo].[usp_DeleteVehicle]    Script Date: 03/08/2012 15:29:21 ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF EXISTS( SELECT *
             FROM sys.objects
             WHERE object_id
                   = 
                   OBJECT_ID( N'[dbo].[usp_DeleteVehicle]'
                            )
               AND type IN( N'P' , N'PC'
                          )
         )
    BEGIN

        DROP PROCEDURE usp_DeleteVehicle
    END;
GO
CREATE PROCEDURE dbo.usp_DeleteVehicle( @VehicleID int
                                      )
AS
BEGIN
    DELETE dbo.Vehicle
      WHERE Vehicle.ID
            = 
            @VehicleID;

    RETURN @@ROWCOUNT;
END;

GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [usp_DeleteVehicle] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [usp_DeleteVehicle] for P0018 has NOT been successfully deployed';
    END;
GO
--Apply Permissions
GRANT EXECUTE ON usp_DeleteVehicle TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_legacy] on [usp_DeleteVehicle] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_legacy] on [usp_DeleteVehicle] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT EXECUTE ON usp_DeleteVehicle TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_execproc] on [usp_DeleteVehicle] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_execproc] on [usp_DeleteVehicle] for P0018 has NOT been successfully deployed';


    END;