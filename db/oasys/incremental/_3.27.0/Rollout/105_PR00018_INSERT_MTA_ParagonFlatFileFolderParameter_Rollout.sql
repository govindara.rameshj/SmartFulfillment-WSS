
IF NOT EXISTS(SELECT 1 FROM PARAMETERS WHERE ParameterID=199)
BEGIN
INSERT INTO [Oasys].[dbo].[Parameters]
           ([ParameterID]
           ,[Description]
           ,[StringValue]
           ,[LongValue]
           ,[BooleanValue]
           ,[DecimalValue]
           ,[ValueType])
     VALUES
           (199
           ,'Paragon Flat File Directory'
           ,'\\pos-b.tpplc.local\data_tmp\paragon\wickes\'
           ,NULL
           ,0
           ,NULL
           ,0)

IF @@ERROR = 0 AND @@ROWCOUNT = 1
   print 'Success: Parameter insert for Paragon Flat File Directory for P0018 has been successfully deployed'
else
   print 'Failure: Parameter insert for Paragon Flat File Directory for P0018 has NOT been successfully deployed'
END
ELSE
BEGIN
print 'Success: Parameter for Paragon Flat File Directory for P0018 is already available on the table'
END


