

CREATE TABLE dbo.VehicleCollectionLine( ID int IDENTITY(1 , 1
                                                       ) , 
                                        CollectionID int NOT NULL , 
                                        Description varchar( 100
                                                           ) , 
                                        Weight int NOT NULL , 
                                        Quantity int NOT NULL , 
                                        TotalWeight int NOT NULL , 
                                        DeliveryDate [date] NOT NULL , 
                                        CONSTRAINT PK_VehicleCollectionLine PRIMARY KEY CLUSTERED( ID ASC , CollectionID ASC
                                                                                                 )
                                            WITH( PAD_INDEX = OFF , STATISTICS_NORECOMPUTE = OFF , IGNORE_DUP_KEY = OFF , ALLOW_ROW_LOCKS = ON , ALLOW_PAGE_LOCKS = ON
                                                )ON [PRIMARY]
                                      )
ON [PRIMARY];

GO
ALTER TABLE dbo.VehicleCollectionLine
        WITH CHECK
ADD CONSTRAINT FK_VehicleCollectionLine_VehicleCollectionHeader FOREIGN KEY( CollectionID
                                                                           )REFERENCES dbo.VehicleCollectionHeader( CollectionID
                                                                                                                  )ON DELETE CASCADE;
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT SELECT ON VehicleCollectionLine TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Select security for [role_legacy] on table [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Select security for [role_legacy] on table [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO
GRANT INSERT ON VehicleCollectionLine TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Insert security for [role_legacy] on table [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Insert security for [role_legacy] on table [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO
GRANT UPDATE ON VehicleCollectionLine TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Update security for [role_legacy] on table [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Update security for [role_legacy] on table [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO
GRANT DELETE ON VehicleCollectionLine TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Delete security for [role_legacy] on table [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Delete security for [role_legacy] on table [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT SELECT ON VehicleCollectionLine TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: Select security for [role_execproc] on table [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Select security for [role_execproc] on table [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO
GRANT INSERT ON VehicleCollectionLine TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Insert security for [role_execproc] on table [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Insert security for [role_execproc] on table [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO
GRANT UPDATE ON VehicleCollectionLine TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Update security for [role_execproc] on table [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Update security for [role_execproc] on table [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO
GRANT DELETE ON VehicleCollectionLine TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Delete security for [role_execproc] on table [VehicleCollectionLine] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Delete security for [role_execproc] on table [VehicleCollectionLine] for P0018 has NOT been successfully deployed';
    END;
GO


