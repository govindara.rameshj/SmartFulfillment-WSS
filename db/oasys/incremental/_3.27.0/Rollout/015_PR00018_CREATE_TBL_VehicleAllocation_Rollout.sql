
CREATE TABLE dbo.VehicleAllocation( OrderID int NOT NULL , 
                                    VehicleTypeIndex varchar( 10
                                                            )NOT NULL , 
                                    DeliveryDate [date] NOT NULL , 
                                    CONSTRAINT PK_VehicleAllocation PRIMARY KEY CLUSTERED( OrderID ASC , DeliveryDate ASC
                                                                                         )
                                        WITH( PAD_INDEX = OFF , STATISTICS_NORECOMPUTE = OFF , IGNORE_DUP_KEY = OFF , ALLOW_ROW_LOCKS = ON , ALLOW_PAGE_LOCKS = ON
                                            )ON [PRIMARY]
                                  )
ON [PRIMARY];

GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT SELECT ON VehicleAllocation TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Select security for [role_legacy] on table [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Select security for [role_legacy] on table [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT INSERT ON VehicleAllocation TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Insert security for [role_legacy] on table [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Insert security for [role_legacy] on table [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT UPDATE ON VehicleAllocation TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Update security for [role_legacy] on table [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Update security for [role_legacy] on table [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT DELETE ON VehicleAllocation TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Delete security for [role_legacy] on table [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Delete security for [role_legacy] on table [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT SELECT ON VehicleAllocation TO [role_execproc]; 
GO

IF @@error = 0
    BEGIN
        PRINT 'Success: Select security for [role_execproc] on table [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Select security for [role_execproc] on table [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;

GO

GRANT INSERT ON VehicleAllocation TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Insert security for [role_execproc] on table [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Insert security for [role_execproc] on table [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT UPDATE ON VehicleAllocation TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Update security for [role_execproc] on table [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Update security for [role_execproc] on table [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT DELETE ON VehicleAllocation TO [role_execproc];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: Delete security for [role_execproc] on table [VehicleAllocation] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: Delete security for [role_execproc] on table [VehicleAllocation] for P0018 has NOT been successfully deployed';
    END;
GO
