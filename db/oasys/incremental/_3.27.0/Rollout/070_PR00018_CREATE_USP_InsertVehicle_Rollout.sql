USE [OASYS]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertVehicle]    Script Date: 03/08/2012 15:33:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InsertVehicle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InsertVehicle]
GO

CREATE PROCEDURE [dbo].[usp_InsertVehicle]

(
	@Registration VARCHAR(50)='NO REGISTRATION', 
	@Make VARCHAR(50)='NO MAKE',
	@Model VARCHAR(50)='NO MODEL',
	@VehicleTypeID INT=-1,
	@DriverName VARCHAR(50)='NO DRIVER',
	@StartTime TIME='00:00:00',
	@StopTime TIME='00:00:00',
	@MaximumWeight DECIMAL(19,2)=0,
	@MaximumVolume DECIMAL(19,2)=0,
	@LastUpdateTime DATETIME)

AS

BEGIN
	DECLARE @DepotID INT


	/*All stores are numbered (as per "Store" table), but the format of this number (aka Depot ID) seems to differ throughout OASYS. 
	Route Optimisation simply reqires a 4 character store code, prefixed with an 8*/
	SET @DepotID = (SELECT TOP 1 '8'+ RIGHT('000'+ STOR, 3) FROM RETOPT WITH (NOLOCK))

	INSERT INTO dbo.Vehicle
			   ([DepotID]
			   ,[Registration]
			   ,[VehicleTypeID]
			   ,[StartTime]
			   ,[StopTime]
			   ,[MaximumWeight]
			   ,[MaximumVolume]
			   ,[LastUpdateTime])
	VALUES(@DepotID,
				ISNULL(@Registration,'NO REGISTRATION'), 
				ISNULL(@VehicleTypeID,-1), 
				CONVERT(VARCHAR(10), @StartTime, 108),
				CONVERT(VARCHAR(10), @StopTime, 108),
				ISNULL(@MaximumWeight,0) ,
				ISNULL(@MaximumVolume,0),
				@LastUpdateTime)
				
	SELECT SCOPE_IDENTITY()
END

go
if @@error = 0
   print 'Success: [usp_InsertVehicle] for P0018 has been successfully deployed'
else
   print 'Failure: [usp_InsertVehicle] for P0018 has NOT been successfully deployed'
go

--Apply Permissions
grant execute on [usp_InsertVehicle] to [role_legacy]
go
if @@error = 0
   print 'Success: execute security for [role_legacy] on [usp_InsertVehicle] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_legacy] on [usp_InsertVehicle] for P0018 has NOT been successfully deployed'
go

GRANT execute on [usp_InsertVehicle] TO [role_execproc] 

GO 

if @@error = 0
   print 'Success: execute security for [role_execproc] on [usp_InsertVehicle] for P0018 has been successfully deployed'
else
   print 'Failure: execute security for [role_execproc] on [usp_InsertVehicle] for P0018 has NOT been successfully deployed'
go