CREATE PROCEDURE usp_GetCollections @DeliveryDate date
AS
BEGIN

    SELECT vh.collectionId , 
           vh.Description , 
           vh.TotalWeight , 
           vh.Name , 
           vh.CustomerAddress1 , 
           vh.CustomerAddress2 , 
           vh.CustomerAddress3 , 
           vh.PostCode AS CustomerPostcode , 
           vh.VehicleTypeIndex as VehicleSelected, 
           vl.ID AS CollectionLineId , 
           vl.Description AS CollectionDescription , 
           vl.Weight AS CollectionLineWeight , 
           vl.Quantity AS CollectionLineQuantity , 
           vl.TotalWeight AS CollectionLineTotalWeight,
           vh.LastUpdateDateTime As CollectionLastUpdateDateTime
      FROM
           VehicleCollectionHeader vh INNER JOIN VehicleCollectionLine vl
           ON vh.CollectionId
              = 
              vl.CollectionId
      WHERE vh.DeliveryDate
            = 
            @DeliveryDate;
END
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: [usp_GetCollections] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: [usp_GetCollections] for P0018 has NOT been successfully deployed';
    END;
GO

--Apply Permissions
GRANT EXECUTE ON usp_GetCollections TO [role_legacy];
GO
IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_legacy] on [usp_GetCollections] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_legacy] on [usp_GetCollections] for P0018 has NOT been successfully deployed';
    END;
GO

GRANT EXECUTE ON usp_GetCollections TO [role_execproc]; 

GO

IF @@error = 0
    BEGIN
        PRINT 'Success: execute security for [role_execproc] on [usp_GetCollections] for P0018 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: execute security for [role_execproc] on [usp_GetCollections] for P0018 has NOT been successfully deployed';
    END;
GO



