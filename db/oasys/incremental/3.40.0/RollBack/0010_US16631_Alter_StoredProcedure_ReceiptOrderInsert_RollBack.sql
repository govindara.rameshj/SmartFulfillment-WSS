ALTER PROCEDURE [dbo].[ReceiptOrderInsert]
	@EmployeeId			int,
	@Comments			char(20),
	@Value				decimal(9,2),
	@Quantity			int,	
	@PoNumber			int=null,
	@PoId				int,
	@PoConsignNumber	int=null,
	@PoSupplierNumber	char(5),
	@PoSupplierBbc		bit,	
	@PoOrderDate		date,
	@PoSoqNumber		int=null,
	@PoReleaseNumber	int,
	@PoDeliveryNote1	char(10),
	@PoDeliveryNote2	char(10),
	@PoDeliveryNote3	char(10),
	@PoDeliveryNote4	char(10),
	@PoDeliveryNote5	char(10),
	@PoDeliveryNote6	char(10),
	@PoDeliveryNote7	char(10),
	@PoDeliveryNote8	char(10),
	@PoDeliveryNote9	char(10),
	@Number				char(6) output
AS
BEGIN
	SET NOCOUNT ON;

	--get next receipt number from system numbers
	declare @NextNumber int;
	exec	@NextNumber = SystemNumbersGetNext @Id=4
	
	--convert next number to number
	set @Number = Convert(Char(6), @NextNumber);
	
	--insert into drlsum
	insert into		
		drlsum (				
		numb,
		[type],
		date1,
		employeeId,
		info,
		valu,
		[0pon],
		tkey,
		[0con],
		[0sup],
		[0bbc],
		[0dat],
		[0soq],
		[0rel],
		[0dl1],
		[0dl2],
		[0dl3],
		[0dl4],
		[0dl5],
		[0dl6],
		[0dl7],
		[0dl8],
		[0dl9]
	) values (
		@Number,
		0,
		getdate(),
		@EmployeeId	,
		@Comments,
		@Value,
		@PoNumber,
		@PoId,
		@PoConsignNumber,
		@PoSupplierNumber,
		@PoSupplierBbc,
		@PoOrderDate,
		@PoSoqNumber,
		@PoReleaseNumber,
		@PoDeliveryNote1,
		@PoDeliveryNote2,
		@PoDeliveryNote3,
		@PoDeliveryNote4,
		@PoDeliveryNote5,
		@PoDeliveryNote6,
		@PoDeliveryNote7,
		@PoDeliveryNote8,
		@PoDeliveryNote9
	)
	
	--update purchase order header as delivered
	update	
		purhdr 
	set		
		reln = Convert(Char(2), @PoReleaseNumber+1),
		rnum = @Number,
		rcom = 1,
		rpar = 1
	where	
		tkey = @PoId
	
	--update supplier as received
	update	
		supmas 
	set		
		opon = opon -1,
		opov = opov - @Value,
		qtyo1 = qtyo1 - @Quantity,
		dlre = getdate(),
		qflg = 0
	where	
		supn = @PoSupplierNumber
	
	--update consignment as received (and delivery notes) if appropriate
	update	
		CONMAS 
	set		
		DONE = 1,
		DNOT1 = @PoDeliveryNote1,
		DNOT2 = @PoDeliveryNote2,
		DNOT3 = @PoDeliveryNote3,
		DNOT4 = @PoDeliveryNote4,
		DNOT5 = @PoDeliveryNote5,
		DNOT6 = @PoDeliveryNote6,
		DNOT7 = @PoDeliveryNote7,
		DNOT8 = @PoDeliveryNote8,
		DNOT9 = @PoDeliveryNote9
	where	
		NUMB = @PoConsignNumber
	
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ReceiptOrderInsert for US16631 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure ReceiptOrderInsert for US16631 has not been deployed successfully'
GO