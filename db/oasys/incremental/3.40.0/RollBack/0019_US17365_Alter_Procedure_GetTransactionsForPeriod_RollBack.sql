SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'GetTransactionsForPeriod') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure GetTransactionsForPeriod'
	EXEC ('CREATE PROCEDURE GetTransactionsForPeriod AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure GetTransactionsForPeriod')
GO

ALTER PROCEDURE GetTransactionsForPeriod
	@periodID int
as
begin
	set nocount on; 

	declare @transDate datetime
	declare @tempDltots table (DATE1 date,TILL char(2),[TRAN] char(4))

	set @transDate = cast((select StartDate from SystemPeriods
						where ID = @periodID) as Date)

	insert into @tempDltots
	select DATE1, TILL, [TRAN] from DLTOTS
	where (CASH = 499 and cast(ReceivedDate as DATE) = @transDate) or (CASH <> 499 and DATE1 = @transDate)

	--DLTOTS
	select dt.* from DLTOTS dt
	inner join @tempDltots t on dt.[TRAN] = t.[TRAN]
		and dt.TILL = t.TILL
		and dt.DATE1 = t.DATE1

	--DLLINE
	select l.* from DLLINE l
	inner join @tempDltots t on l.[TRAN] = t.[TRAN]
		and l.TILL = t.TILL
		and l.DATE1 = t.DATE1
		
	--DLPAID	
	select p.* from DLPAID p
	inner join @tempDltots t on p.[TRAN] = t.[TRAN]
		and p.TILL = t.TILL
		and p.DATE1 = t.DATE1

end
go

If @@Error = 0
   Print 'Success: The Create Stored Procedure GetTransactionsForPeriod for US17365 has been rolled back successfully'
Else
   Print 'Failure: The Create Stored Procedure GetTransactionsForPeriod for US17365 has not been rolled back'
Go