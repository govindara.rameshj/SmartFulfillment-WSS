SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_EventsGetEventsBySku]
	@SkuNumber	CHAR(6) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @EVENTS TABLE
		(
			[Priority]			CHAR(2),
			[Number]			CHAR(6),
			[Description]		CHAR(40),
			[Active]			CHAR(6),
			[When Active]		VARCHAR(90),
			[Type]				CHAR(40),
			[Deal Group Number]	CHAR(8),
			[MMHS]				CHAR(6),
			[Sku Number]		CHAR(6)
		)
	DECLARE @DEALGROUPS TABLE
		(
			[Deal Group Number]	CHAR(8),
			[Description]		CHAR(10),
			[Deal Price]		DECIMAL(9, 2),
			[Priority]			CHAR(2),
			[Event Number]		CHAR(6)
		)
	DECLARE @DGMIXANDMATCHGROUPS TABLE
		(
			[Mix and Match Number]	CHAR(6),
			[Description]			CHAR(17),
			[Buy Quantity]			DECIMAL(7,0),
			[Deal Price]			DECIMAL(9,2),
			[Deal Group Number]		CHAR(8),
			[Event Number]			CHAR(6)
		)
	DECLARE @HIERARCHYGROUPS TABLE
		(
			[Hierarchy Group Number]	CHAR(6),
			[Category]					CHAR(50),
			[Spend Level]				DECIMAL(9,2),
			[Discount]					CHAR(9),
			[Buy Coupon]				CHAR(7),
			[Get Coupon]				CHAR(7),
			[Priority]					CHAR(2),
			[Event Number]				CHAR(6)
		)
	DECLARE @HSSKUDETAIL TABLE
		(
			[Sku Number]	CHAR(6),
			[Description]	CHAR(40),
			[Price]			DECIMAL(9,2),
			[HS Lookup]		CHAR(6)
		)
	DECLARE @DGMANDMSKUDETAIL TABLE	
		(
			[Sku Number]	CHAR(6), 
			[Description]	CHAR(40),
			[Price]			DECIMAL(9,2),
			[Mix and Match Number]	CHAR(6),
			[Deal Group Number]		CHAR(8),
			[Event Number]			CHAR(6)
		)
	DECLARE @DGSKUDETAIL TABLE
		(
			[Sku Number]		CHAR(6),
			[Description]		CHAR(40),
			[Buy Quantity]		DECIMAL(7,0),
			[Price]				DECIMAL(9,2),
			[Deal Group Number]	CHAR(8),
			[Event Number]		CHAR(6)
		)
	DECLARE @HIERARCHYEXCLUSIONS TABLE
		(
			[Event Number]				CHAR(6),
			[Hierarchy Group Number]	CHAR(6),
			[Sku Number]				CHAR(6),
			[Description]				CHAR(40)
		)
	DECLARE @TEMPMIXANDMATCHGROUPS TABLE	
		(
			[Mix and Match Number]	CHAR(6),
			[Description]			CHAR(17),
			[Deal Price]			DECIMAL(9,2),
			[Event Number]			CHAR(6),
			[Priority]				CHAR(2)
		)
	DECLARE @TEMPMANDMSKUDETAIL TABLE	
		(
			[Sku Number]			CHAR(6), 
			[Description]			CHAR(40),
			[Event Price]			DECIMAL(9,2),
			[Price]					DECIMAL(9,2),
			[Mix and Match Number]	CHAR(6)
		)
	DECLARE @TEMPSKUDETAIL TABLE	
		(
			[Sku Number]	CHAR(6), 
			[Description]	CHAR(40),
			[Event Price]	DECIMAL(9,2),
			[Price]			DECIMAL(9,2),
			[Event Number]	CHAR(6),
			[Priority]		CHAR(2)
		)
	DECLARE @MULTIBUYMIXANDMATCHGROUPS TABLE	
		(
			[Mix and Match Number]	CHAR(6),
			[Description]			CHAR(17),
			[Discount]				CHAR(9),
			[Buy Quantity]			DECIMAL(7,0),
			[Get Quantity]			DECIMAL(7,0),
			[Event Number]			CHAR(6),
			[Priority]				CHAR(2)
		)
	DECLARE @MULTIBUYMANDMSKUDETAIL TABLE	
		(
			[Sku Number]			CHAR(6), 
			[Description]			CHAR(40),
			[Price]					DECIMAL(9,2),
			[Mix and Match Number]	CHAR(6)
		)
	DECLARE @MULTIBUYSKUDETAIL TABLE	
		(
			[Sku Number]	CHAR(6), 
			[Description]	CHAR(40),
			[Price]			DECIMAL(9,2),
			[Buy Quantity]	DECIMAL(7,0),
			[Get Quantity]	DECIMAL(7,0),
			[Event Number]	CHAR(6),
			[Priority]		CHAR(2)
		)
	DECLARE @QUANTITYBREAKMIXANDMATCHGROUPS TABLE	
		(
			[Mix and Match Number]		CHAR(6),
			[Description]				CHAR(17),
			[Buy Quantity]				DECIMAL(7,0),
			[Total Buy Quantity Value]	DECIMAL(9,2),
			[Event Number]				CHAR(6),
			[Priority]					CHAR(2)
		)
	DECLARE @QUANTITYBREAKMANDMSKUDETAIL TABLE	
		(
			[Sku Number]			CHAR(6), 
			[Description]			CHAR(40),
			[Price]					DECIMAL(9,2),
			[Mix and Match Number]	CHAR(6)
		)
	DECLARE @QUANTITYBREAKSKUDETAIL TABLE	
		(
			[Sku Number]				CHAR(6), 
			[Description]				CHAR(40),
			[Buy Quantity]				DECIMAL(7,0),
			[Total Buy Quantity Value]	DECIMAL(9,2),
			[Price]						DECIMAL(9,2),
			[Event Number]				CHAR(6),
			[Priority]					CHAR(2)
		)

	-- If delete a parameter on Menu GUI code sends a blank string rather than NULL
	-- so convert it to NULL here
	SET @SkuNumber = 
		CASE
			WHEN
				@SkuNumber = ''
			THEN
				NULL
			ELSE
				@SkuNumber
		END

	INSERT INTO
		@EVENTS
			(
				[Priority],
				[Number],
				[Description],
				[Active],
				[When Active],
				[Type],
				[Deal Group Number],
				[MMHS],
				[Sku Number]
			)
	SELECT
		EH.PRIO AS [Priority],
		EH.NUMB AS [Number],
		EH.DESCR AS [Description],
		CASE 
			WHEN
				[dbo].[udf_EventIsActive](EH.SDAT, EH.EDAT, EH.STIM, EH.ETIM, EH.DACT1, EH.DACT2, EH.DACT3, EH.DACT4, EH.DACT5, EH.DACT6, EH.DACT7, EH.IDEL) = 1
			THEN
				'Active'
			ELSE
				''
		END As [Active],
		[dbo].[udf_EventWhenActive](EH.SDAT, EH.EDAT, EH.STIM, EH.ETIM, EH.DACT1, EH.DACT2, EH.DACT3, EH.DACT4, EH.DACT5, EH.DACT6, EH.DACT7, EH.IDEL) As [When Active],
		[dbo].[udf_EventGetTypeDescription](EE.ETYP) AS [Type],
		SUBSTRING( '00000000' + EE.DLGN, 1 + LEN(LTRIM(RTRIM(EE.DLGN))), 8) AS [Deal Group Number],
		EE.MMHS AS [MMHS],
		EE.SKUN AS [Sku Number]
	FROM
		EVTHDR AS EH
			CROSS APPLY
				(
					SELECT TOP(1)
						EQ.DLGN,
						EQ.MMHS,
						EQ.SKUN,	
						EQ.ETYP			
					FROM
						EVTENQ As EQ
					WHERE
						EH.NUMB = EQ.NUMB
					AND
						EH.PRIO = EQ.PRIO
					AND
						EQ.IDEL = 0
					AND
						(
							(
								NOT @SkuNumber IS NULL
							AND
								(
									NOT EQ.SKUN IS NULL
								AND
									EQ.SKUN = @SkuNumber
								)
							)
							OR
								@SkuNumber IS NULL
						)
				) AS EE
	WHERE
		EH.IDEL = 0
	AND
		(
			DATEDIFF(d, GETDATE(), EH.EDAT) >= 0
		OR
			EH.EDAT IS NULL
		)
	INSERT INTO
		@EVENTS
			(
				[Priority],
				[Number],
				[Description],
				[Active],
				[When Active],
				[Type],
				[Deal Group Number],
				[MMHS],
				[Sku Number]
			)
	SELECT
		EH.PRIO,
		EH.NUMB,
		EH.DESCR,
		CASE 
			WHEN
				[dbo].[udf_EventIsActive](EH.SDAT, EH.EDAT, EH.STIM, EH.ETIM, EH.DACT1, EH.DACT2, EH.DACT3, EH.DACT4, EH.DACT5, EH.DACT6, EH.DACT7, EH.IDEL) = 1
			THEN
				'Active'
			ELSE
				''
		END As [Active],
		[dbo].[udf_EventWhenActive](EH.SDAT, EH.EDAT, EH.STIM, EH.ETIM, EH.DACT1, EH.DACT2, EH.DACT3, EH.DACT4, EH.DACT5, EH.DACT6, EH.DACT7, EH.IDEL) As [When Active],
		[dbo].[udf_EventGetTypeDescription](EM.TYPE),
		'00000000',
		EM.KEY1,
		SM.SKUN
	FROM
		EVTHDR AS EH
			INNER JOIN
				EVTMAS AS EM
			ON
				EH.NUMB = EM.NUMB
			AND
				EH.PRIO = EM.PRIO
			INNER JOIN
				STKMAS AS SM
			ON
				(
					EM.KEY1 = SM.STYL
				OR
					EM.KEY1 = SM.SGRP
				OR
					EM.KEY1 = SM.GROU
				OR
					EM.KEY1 = SM.CTGY
				)
			LEFT OUTER JOIN
				@EVENTS AS E
			ON
				EM.NUMB = E.Number
			AND
				EM.PRIO = E.Priority
			AND
				EM.KEY1 = E.MMHS
	WHERE
		EM.TYPE = 'HS'
	AND
		SM.SKUN = @SkuNumber
	AND
		EH.IDEL = 0
	AND
		(
			DATEDIFF(d, GETDATE(), EH.EDAT) >= 0
		OR
			EH.EDAT IS NULL
		)
	AND
		E.Number IS NULL

	SELECT
		[Priority],
		[Number],
		[Description],
		[Active],
		[When Active],
		[Type],
		[Deal Group Number],
		[MMHS],
		[Sku Number]
	FROM
		@EVENTS
	ORDER BY
		[Number]		ASC,
		[Priority]		DESC,
		[Active]	DESC
		
	INSERT INTO
		@DEALGROUPS
			(
				[Deal Group Number],
				[Description],
				[Deal Price],
				[Priority],
				[Event Number]
			)
	SELECT
		EM.KEY2 As [Deal Group Number],
		'Deal Group' As [Description],
		EM.PRIC AS [Deal Price],
		EM.PRIO AS [Priority],
		EM.NUMB as [Event Number]
	FROM
		[EVTMAS] AS EM
			LEFT OUTER JOIN
				@EVENTS AS E
			ON
				EM.NUMB = E.Number
			AND
				EM.PRIO = E.Priority
			AND
				EM.KEY2 = SUBSTRING('00000000' + E.[Deal Group Number], 1 + LEN(LTRIM(RTRIM(E.[Deal Group Number]))), 8)
	WHERE
		EM.[TYPE] = 'DG'
	AND
		EM.NUMB IN
			(
				SELECT E.Number FROM @EVENTS AS E
			)

	SELECT
		[Deal Group Number],
		[Description],
		[Deal Price],
		[Priority],
		[Event Number]
	FROM
		@DEALGROUPS
	
	INSERT INTO
		@HIERARCHYGROUPS
			(
				[Hierarchy Group Number],
				[Category],
				[Spend Level],
				[Discount],
				[Buy Coupon],
				[Get Coupon],
				[Priority],
				[Event Number]
			)
	SELECT
		EM.KEY1 As [Hierarchy Group Number],
		[dbo].[udf_EventGetCategory](EM.KEY1) As [Category],
		EM.PRIC AS [Spend Level],
		[dbo].[udf_EventGetDiscount](EM.VDIS, EM.PDIS) AS [Discount],
		EM.BUYCPN AS [Buy Coupon],
		EM.GETCPN AS [Get Coupon],
		EM.PRIO AS [Priority],
		EM.NUMB AS [Event Number]
	FROM
		[EVTMAS] AS EM
			INNER JOIN
				@EVENTS AS E
			ON
				EM.NUMB = E.Number
			AND
				EM.PRIO = E.Priority
			AND
				EM.KEY1 = E.[MMHS]
	WHERE
		EM.[TYPE] = 'HS'			

	SELECT
		[Hierarchy Group Number],
		[Category],
		[Spend Level],
		[Discount],
		[Buy Coupon],
		[Get Coupon],
		[Priority],
		[Event Number]
	FROM
		@HIERARCHYGROUPS

	INSERT INTO
		@HSSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Price],
			[HS Lookup]
		)
	SELECT
		SM.SKUN AS [Sku Number],
		SM.DESCR AS [Description],
		SM.[PRIC] AS [Price],
--		HG.[Spend Level] AS [Price],
		HG.[Hierarchy Group Number] AS [HS Lookup]
	FROM
		@HIERARCHYGROUPS AS HG
			INNER JOIN
				STKMAS AS SM
			ON
				(
					HG.[Hierarchy Group Number] = SM.STYL
				OR
					HG.[Hierarchy Group Number] = SM.SGRP
				OR
					HG.[Hierarchy Group Number] = SM.GROU
				OR
					HG.[Hierarchy Group Number] = SM.CTGY
				)
			LEFT OUTER JOIN
				EVTHEX AS EX
			ON
				HG.[Event Number] = EX.NUMB
			AND
				HG.[Hierarchy Group Number] = EX.HIER
			AND
				SM.SKUN = EX.ITEM
			AND 
				EX.IDEL = 0
	WHERE
		EX.ITEM IS NULL
	ORDER BY
		SM.SKUN

	SELECT
		[Sku Number],
		[Description],
		[Price],
		[HS Lookup]
	FROM
		@HSSKUDETAIL
	WHERE
		(
			[Sku Number] = @SkuNumber
		OR
			@SkuNumber IS NULL
		)
	
	INSERT INTO
		@DGMIXANDMATCHGROUPS
		(
			[Deal Group Number],
			[Description],
			[Buy Quantity],
			[Deal Price],
			[Mix and Match Number],
			[Event Number]
		)
	SELECT
		DG.[Deal Group Number],
		'Mix & Match Group',
		ED.QUAN AS [Buy Quantity],
		DG.[Deal Price],
		ED.KEY1,
		DG.[Event Number]
	FROM
		EVTDLG AS ED
			INNER JOIN
				@DEALGROUPS AS DG
			ON
				DG.[Deal Group Number]  = SUBSTRING('00000000' + ED.DLGN, 1 + LEN(LTRIM(RTRIM(ED.DLGN))), 8)
			AND
				DG.[Event Number] = ED.NUMB
	WHERE
		ED.IDEL = 0
	AND
		ED.[TYPE] = 'M'

	SELECT
		[Description],
		[Buy Quantity],
		[Deal Price],
		[Mix and Match Number],
		[Deal Group Number],
		[Event Number]
	FROM
		@DGMIXANDMATCHGROUPS
	
	INSERT INTO
		@DGMANDMSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Price],
			[Mix and Match Number],
			[Deal Group Number],
			[Event Number]
		)
	SELECT
		EM.SKUN AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END	AS [Description],
		SM.PRIC AS [Price],
		DGMAMG.[Mix and Match Number],
		DGMAMG.[Deal Group Number],
		DGMAMG.[Event Number]
	FROM
		@DGMIXANDMATCHGROUPS AS DGMAMG
			INNER JOIN
				EVTMMG AS EM
			ON
				DGMAMG.[Mix and Match Number] = EM.MMGN
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				EM.SKUN = SM.SKUN
	WHERE
		EM.IDEL = 0
	
	SELECT
		[Sku Number],
		[Description],
		[Price],
		[Mix and Match Number],
		[Deal Group Number],
		[Event Number] 
	FROM
		@DGMANDMSKUDETAIL
-- Deal Groups need to show all the SKUs involved as all must be bought to
-- activate the deal
--	WHERE
--		(
--			[Sku Number] = @SkuNumber
--		OR
--			@SkuNumber IS NULL
--		)
	
	INSERT INTO
		@DGSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Buy Quantity],
			[Price],
			[Deal Group Number],
			[Event Number]
		)
	SELECT
		ED.KEY1 AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END	AS [Description],
		ED.QUAN,
		SM.[PRIC] AS [Price],
		DG.[Deal Group Number]  AS [Deal Group Number],
		DG.[Event Number]
	FROM
		@DEALGROUPS AS DG
			INNER JOIN
				EVTDLG AS ED
			ON
				DG.[Deal Group Number] = SUBSTRING('00000000' + ED.DLGN, 1 + LEN(LTRIM(RTRIM(ED.DLGN))), 8)
			AND
				DG.[Event Number] = ED.NUMB
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				ED.KEY1 = SM.SKUN
	WHERE
		ED.IDEL = 0
	AND
		ED.TYPE = 'S'
	ORDER BY
		SM.SKUN

	SELECT
		[Sku Number],
		[Description],
		[Buy Quantity],
		[Price],
		[Deal Group Number],
		[Event Number]
	FROM
		@DGSKUDETAIL
-- Deal Groups need to show all the SKUs involved as all must be bought to
-- activate the deal
--	WHERE
--		(
--			[Sku Number] = @SkuNumber
--		OR
--			@SkuNumber IS NULL
--		)

	INSERT INTO
		@HIERARCHYEXCLUSIONS
		(
			[Event Number],
			[Hierarchy Group Number],
			[Sku Number],
			[Description]
		)
	SELECT
		HG.[Event Number] AS [Event Number],
		HG.[Hierarchy Group Number] AS [Hierarchy Group Number],
		EX.ITEM AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END AS [Description]
	FROM
		@HIERARCHYGROUPS AS HG
			INNER JOIN
				EVTHEX AS EX
			ON
				HG.[Event Number] = EX.NUMB
			AND
				HG.[Hierarchy Group Number] = EX.HIER
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				EX.ITEM = SM.SKUN
	WHERE
		EX.IDEL = 0
		
	SELECT
		[Event Number],
		[Hierarchy Group Number],
		[Sku Number],
		[Description]
	FROM
		@HIERARCHYEXCLUSIONS
	WHERE
		(
			[Sku Number] = @SkuNumber
		OR
			@SkuNumber IS NULL
		)
	
	INSERT INTO
		@MULTIBUYMIXANDMATCHGROUPS
		(
			[Mix and Match Number],
			[Description],
			[Discount],
			[Buy Quantity],
			[Get Quantity],
			[Event Number],
			[Priority]
		)
	SELECT
		EM.KEY1 AS [Mix and Match Number],
		'Mix & Match Group' AS [Description],
		[dbo].udf_EventGetDiscount(EM.VDIS, EM.PDIS) AS [Discount],
		EM.BQTY AS [Buy Quantity],
		EM.GQTY AS [Get Quantity],
		E.Number AS [Event Number],
		E.Priority AS [Priority]
	FROM
		EVTMAS AS EM
			INNER JOIN
				@EVENTS AS E
			ON
				EM.NUMB  = E.Number
			AND
				EM.PRIO = E.Priority
			AND
				EM.KEY1 = E.MMHS	
	WHERE
		EM.IDEL = 0
	AND
		EM.TYPE = 'MM'

	SELECT
		[Mix and Match Number],
		[Description],
		[Discount],
		[Buy Quantity],
		[Get Quantity],
		[Event Number],
		[Priority]
	FROM
		@MULTIBUYMIXANDMATCHGROUPS
	
	INSERT INTO
		@MULTIBUYMANDMSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Price],
			[Mix and Match Number]
		)
	SELECT
		EM.SKUN AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END	AS [Description],
		SM.PRIC AS [Price],
		MMMAMG.[Mix and Match Number]
	FROM
		@MULTIBUYMIXANDMATCHGROUPS AS MMMAMG
			INNER JOIN
				EVTMMG AS EM
			ON
				MMMAMG.[Mix and Match Number] = EM.MMGN
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				EM.SKUN = SM.SKUN
	WHERE
		EM.IDEL = 0
	
	SELECT
		[Sku Number],
		[Description],
		[Price],
		[Mix and Match Number]
	FROM
		@MULTIBUYMANDMSKUDETAIL
	WHERE
		(
			[Sku Number] = @SkuNumber
		OR
			@SkuNumber IS NULL
		)
	
	INSERT INTO
		@MULTIBUYSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Price],
			[Buy Quantity],
			[Get Quantity],
			[Event Number],
			[Priority]
		)
	SELECT
		EM.KEY1 AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END	AS [Description],
		EM.PRIC AS [Price],
		EM.BQTY AS [Buy Quantity],
		EM.GQTY AS [Get Quantity],
		EM.NUMB AS [Event Number],
		EM.PRIO AS [Priority]
	FROM
		@EVENTS AS E
			INNER JOIN
				EVTMAS AS EM
			ON
				E.Number = EM.NUMB
			AND
				E.Priority = EM.PRIO
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				EM.KEY1 = SM.SKUN
	WHERE
		EM.IDEL = 0
	AND
		EM.TYPE = 'MS'
	ORDER BY
		SM.SKUN

	SELECT
		[Sku Number],
		[Description],
		[Price],
		[Buy Quantity],
		[Get Quantity],
		[Event Number],
		[Priority]
	FROM
		@MULTIBUYSKUDETAIL
	WHERE
		(
			[Sku Number] = @SkuNumber
		OR
			@SkuNumber IS NULL
		)
	
	INSERT INTO
		@QUANTITYBREAKMIXANDMATCHGROUPS
		(
			[Mix and Match Number],
			[Description],
			[Buy Quantity],
			[Total Buy Quantity Value],
			[Event Number],
			[Priority]
		)
	SELECT
		EM.KEY1 AS [Mix and Match Number],
		'Mix & Match Group' AS [Description],
		CONVERT(DECIMAL(7,0), EM.KEY2) AS [Buy Quantity],
		EM.PRIC AS [Total Buy Quantity Value],
		E.Number AS [Event Number],
		E.Priority AS [Priority]
	FROM
		EVTMAS AS EM
			INNER JOIN
				@EVENTS AS E
			ON
				EM.NUMB  = E.Number
			AND
				EM.PRIO = E.Priority
			AND
				EM.KEY1 = E.MMHS	
	WHERE
		EM.IDEL = 0
	AND
		EM.TYPE = 'QM'

	SELECT
		[Mix and Match Number],
		[Description],
		[Buy Quantity],
		[Total Buy Quantity Value],
		[Event Number],
		[Priority]
	FROM
		@QUANTITYBREAKMIXANDMATCHGROUPS
	
	INSERT INTO
		@QUANTITYBREAKMANDMSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Price],
			[Mix and Match Number]
		)
	SELECT
		EM.SKUN AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END	AS [Description],
		SM.PRIC AS [Price],
		QBMAMG.[Mix and Match Number]
	FROM
		@QUANTITYBREAKMIXANDMATCHGROUPS AS QBMAMG
			INNER JOIN
				EVTMMG AS EM
			ON
				QBMAMG.[Mix and Match Number] = EM.MMGN
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				EM.SKUN = SM.SKUN
	WHERE
		EM.IDEL = 0
	
	SELECT
		[Sku Number],
		[Description],
		[Price],
		[Mix and Match Number]
	FROM
		@QUANTITYBREAKMANDMSKUDETAIL
	WHERE
		(
			[Sku Number] = @SkuNumber
		OR
			@SkuNumber IS NULL
		)
	
	INSERT INTO
		@QUANTITYBREAKSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Buy Quantity],
			[Total Buy Quantity Value],
			[Price],
			[Event Number],
			[Priority]
		)
	SELECT
		EM.KEY1 AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END	AS [Description],
		CONVERT(DECIMAL(7,0), EM.KEY2) AS [Buy Quantity],
		EM.PRIC AS [Total Buy Quantity Value],
		SM.PRIC AS [Price],
		EM.NUMB AS [Event Number],
		EM.PRIO AS [Priority]
	FROM
		@EVENTS AS E
			INNER JOIN
				EVTMAS AS EM
			ON
				E.Number = EM.NUMB
			AND
				E.Priority = EM.PRIO
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				EM.KEY1 = SM.SKUN
	WHERE
		EM.IDEL = 0
	AND
		EM.TYPE = 'QS'
	ORDER BY
		SM.SKUN

	SELECT
		[Sku Number],
		[Description],
		[Buy Quantity],
		[Total Buy Quantity Value],
		[Price],
		[Event Number],
		[Priority]
	FROM
		@QUANTITYBREAKSKUDETAIL
	WHERE
		(
			[Sku Number] = @SkuNumber
		OR
			@SkuNumber IS NULL
		)
	
	INSERT INTO
		@TEMPMIXANDMATCHGROUPS
		(
			[Mix and Match Number],
			[Description],
			[Deal Price],
			[Event Number],
			[Priority]
		)
	SELECT
		EM.KEY1 AS [Mix and Match Number],
		'Mix & Match Group' AS [Description],
		EM.PRIC AS [Deal Price],
		E.Number AS [Event Number],
		E.Priority AS [Priority]
	FROM
		EVTMAS AS EM
			INNER JOIN
				@EVENTS AS E
			ON
				EM.NUMB  = E.Number
			AND
				EM.PRIO = E.Priority
			AND
				EM.KEY1 = E.MMHS	
	WHERE
		EM.IDEL = 0
	AND
		EM.TYPE = 'TM'

	SELECT
		[Mix and Match Number],
		[Description],
		[Deal Price],
		[Event Number],
		[Priority]
	FROM
		@TEMPMIXANDMATCHGROUPS
	
	INSERT INTO
		@TEMPMANDMSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Event Price],
			[Price],
			[Mix and Match Number]
		)
	SELECT
		EM.SKUN AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END	AS [Description],
		TMAMG.[Deal Price],
		SM.PRIC AS [Price],
		TMAMG.[Mix and Match Number]
	FROM
		@TEMPMIXANDMATCHGROUPS AS TMAMG
			INNER JOIN
				EVTMMG AS EM
			ON
				TMAMG.[Mix and Match Number] = EM.MMGN
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				EM.SKUN = SM.SKUN
	WHERE
		EM.IDEL = 0
	
	SELECT
		[Sku Number],
		[Description],
		[Event Price],
		[Price],
		[Mix and Match Number]
	FROM
		@TEMPMANDMSKUDETAIL
	WHERE
		(
			[Sku Number] = @SkuNumber
		OR
			@SkuNumber IS NULL
		)
	
	INSERT INTO
		@TEMPSKUDETAIL
		(
			[Sku Number],
			[Description],
			[Event Price],
			[Price],
			[Event Number],
			[Priority]
		)
	SELECT
		EM.KEY1 AS [Sku Number],
		CASE
			WHEN
				SM.SKUN IS NULL
			THEN
				'<-- Sku Number Not Found on Stock Master'
			ELSE
				SM.DESCR
		END	AS [Description],
		EM.PRIC AS [Price],
		SM.PRIC,
		EM.NUMB AS [Event Number],
		EM.PRIO AS [Priority]
	FROM
		@EVENTS AS E
			INNER JOIN
				EVTMAS AS EM
			ON
				E.Number = EM.NUMB
			AND
				E.Priority = EM.PRIO
			LEFT OUTER JOIN
				STKMAS AS SM
			ON
				EM.KEY1 = SM.SKUN
	WHERE
		EM.IDEL = 0
	AND
		EM.TYPE = 'TS'
	ORDER BY
		SM.SKUN

	SELECT
		[Sku Number],
		[Description],
		[Event Price],
		[Price],
		[Event Number],
		[Priority]
	FROM
		@TEMPSKUDETAIL
	WHERE
		(
			[Sku Number] = @SkuNumber
		OR
			@SkuNumber IS NULL
		)
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_EventsGetEventsBySku for US17339 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_EventsGetEventsBySku for US17339 has not been deployed successfully'
GO