IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateSafeForNonTradingDay]') AND type in (N'P', N'PC'))
drop procedure dbo.CreateSafeForNonTradingDay;
go

if @@error = 0
   print 'Success: Stored procedure CreateSafeForNonTradingDay for US19817 has been successfully dropped'
else
   print 'Failure: Stored procedure CreateSafeForNonTradingDay for US19817 has NOT been successfully dropped'
go