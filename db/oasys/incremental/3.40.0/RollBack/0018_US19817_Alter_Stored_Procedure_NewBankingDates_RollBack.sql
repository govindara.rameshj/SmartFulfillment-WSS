USE [Oasys]
GO

/****** Object:  StoredProcedure [dbo].[NewBankingDates]    Script Date: 02/25/2014 15:49:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER procedure [dbo].[NewBankingDates]
   @Today date
as
begin
set nocount on
--create records in systemperiods table if required
exec SystemPeriodAutoPopulate

--get today's period
declare @PeriodID int

set @PeriodID = (select ID from SystemPeriods where StartDate = @Today)

select PeriodId                     = a.ID,
       PeriodDate                   = a.StartDate,
       BankingComplete              = b.IsClosed,
       NextDateToBeBanked           = cast((case (select top 1 PeriodID
                                                  from [Safe]
                                                  where IsClosed = 0
                                                  order by PeriodID asc)
                                               when a.ID then 1
                                               else 0
                                            end) as bit),
       LastDateBanked               = cast((case (select top 1 PeriodID
                                                  from [Safe]
                                                  where IsClosed = 1
                                                  order by PeriodID desc)
                                               when a.ID then 1
                                               else 0
                                            end) as bit),
       BankingBagCollectionRequired = cast((case (select count(*)
                                                  from SafeBags a
                                                  where a.[Type]      = 'B'
                                                  and   a.[State]     = 'M'
                                                  and   a.SealNumber <> '00000000')  --banking bag containing non-cash(except cheque) tenders do not need to be physically collected
                                             when 0 then 0                           --this bag is created for process transmission only
                                             else 1
                                          end) as bit),
       [Description]                = ltrim(str(a.ID)) + ' - ' +
                                      replicate('0', 2 - len(ltrim(str(day(a.StartDate)))))   + ltrim(str(day(a.StartDate)))   + '/' +
                                      replicate('0', 2 - len(ltrim(str(month(a.StartDate))))) + ltrim(str(month(a.StartDate))) + '/' +
                                      ltrim(str(year(a.StartDate))) + ' - ' + case
                                                                                 when b.IsClosed is null then 'Safe record not found'
                                                                                 when b.IsClosed = 0     then 'Banking not completed'
                                                                                 when b.IsClosed = 1     then 'Banking complete'
                                                                              end,
      SafeMaintenanceDone           = isnull(b.SafeChecked, 0),

      b.EndOfDayCheckDone 
      
from (
      select *
      from SystemPeriods where ID in (
                                      select ID from SystemPeriods
                                      except
                                      select SystemPeriodID from NonTradingDay
                                     )
     ) a
left outer join [Safe] b
           on b.PeriodID  = a.ID
where a.ID <= @PeriodID
and   a.ID >= (@PeriodID - (select LongValue from [Parameters] where ParameterID = 2200)) --cutdown on the number of days being displayed
order by a.ID desc
end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingDates for US19817 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingDates for US19817 has not been rolled back'
GO