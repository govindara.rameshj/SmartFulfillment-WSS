ALTER PROCEDURE [dbo].[ReceiptLineInsert]
	@ReceiptNumber	char(6),
	@Sequence		char(4),
	@SkuNumber		char(6),
	@OrderQty		int,
	@OrderPrice		dec(9,2),
	@ReceivedQty	int,
	@ReceivedPrice	dec(9,2),
	@OrderLineId	int,
	@UserId			int
AS
BEGIN
	SET NOCOUNT ON;
	declare @onHand			int;
	declare @returns		int;
	declare	@markdowns		int;
	declare @writeOffs		int;
	declare @price			dec(9,2);
	declare @logKey			char(50);
	declare @logType		char(2);
	declare @poId			int;

	--insert reciept line
	insert into		drldet 
				(
					numb,
					seqn,
					skun,
					ordq,
					ordp,
					recq,
					pric,
					poln
				)
	values		(
					@ReceiptNumber,
					@Sequence,
					@SkuNumber,
					@OrderQty,
					@OrderPrice,
					@ReceivedQty,
					@ReceivedPrice,
					@OrderLineId
				)
	
	--get stock item values
	select	@onHand		= onha,
			@returns	= retq, 
			@markdowns	= mdnq,
			@writeOffs	= wtfq,
			@price		= pric	
	from	stkmas
	where	skun		= @SkuNumber
	
	--update stock item as received
	update	stkmas 
	set		onha = onha + @ReceivedQty,
			onor = onor - @OrderQty,
			treq = treq + @ReceivedQty,
			trev = trev + (@ReceivedQty * @ReceivedPrice),
			drec = getdate(),
			tact = 1
	where	skun = @SkuNumber
	
	--is this from an actual order line
	if @OrderLineId = 0
		begin
			set @logKey = @ReceiptNumber + ' ' + @Sequence;
			set @logType = '72'
		end
	else
		begin
			set @poId = (select hkey from purlin where tkey = @OrderLineId)
			set @logKey = @poId + ' ' + @OrderLineId + ' ' + @ReceiptNumber + ' ' + @Sequence;
			set @logType = '71'
			
			--update purlin record as received
			update	purlin 
			set		rqty = @ReceivedQty,
					conf = 'C',
					rdat = getdate(),
					dele = 1
			where	tkey = @OrderLineId
			
		end
		
	--insert stock log entry
	insert into		stklog 
				(
					skun,
					dayn,
					[type],
					date1,
					[time],
					keys,
					eeid,
					sstk, estk,
					sret, eret,
					smdn, emdn,
					swtf, ewtf,
					spri, epri
				)
	values
				(
					@SkuNumber,
					datediff(d, '1900-01-01', getdate()) + 1,
					@logType,
					getdate(),
					replace(convert (varchar(8), getdate(), 108),':',''),
					@logKey,
					@UserId,
					@onHand, @onHand + @ReceivedQty,
					@returns, @returns,
					@markdowns, @markdowns,
					@writeOffs, @writeOffs,
					@price, @price
				)
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ReceiptLineInsert for US16631 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure ReceiptLineInsert for US16631 has not been deployed successfully'
GO