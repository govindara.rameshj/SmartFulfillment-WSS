update ReportRelation
set ParentColumns = 'Hierarchy Group Number', 
	ChildColumns = 'HS Lookup'
where Name = 'Spend Level Saving Sku Details'
GO

If @@Error = 0
   Print 'Success: Update Table ReportRelation for US17339 has been deployed successfully'
Else
   Print 'Failure: Update Table ReportRelation for US17339 has not been deployed successfully'
Go
