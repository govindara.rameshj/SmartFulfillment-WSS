ALTER PROCEDURE [dbo].[ReceiptLineUpdate]
(
	@ReceiptNumber	char(6),
	@Sequence		char(4),
	@SkuNumber		char(6),
	@ReceivedChange	int,
	@UserId			int
)
AS
BEGIN
	SET NOCOUNT ON;
	declare @onHand			int;
	declare @returns		int;
	declare	@markdowns		int;
	declare @writeOffs		int;
	declare @price			dec(9,2);

	--update receipt line qty received
	update		drldet
	set			recq = recq + @ReceivedChange
	where		numb = @ReceiptNumber
	and			seqn = @Sequence
	
	--get stock item values
	select	@onHand		= onha,
			@returns	= retq, 
			@markdowns	= mdnq,
			@writeOffs	= wtfq,
			@price		= pric	
	from	stkmas
	where	skun		= @SkuNumber
	
	--update stock item
	update	stkmas
	set		onha = onha + @ReceivedChange,
          --referral 363 - prevent update of ONOR
          --onor = onor - @ReceivedChange,
			treq = treq + @ReceivedChange,
			trev = trev + (@ReceivedChange * @price),
			drec = getdate(),
			tact = 1
	where	skun = @SkuNumber
	
	
	--insert stock log entry
	insert into		stklog
				(
					skun,
					dayn,
					[type],
					date1,
					[time],
					keys,
					eeid,
					sstk, estk,
					sret, eret,
					smdn, emdn,
					swtf, ewtf,
					spri, epri
				)
	values
				(
					@SkuNumber,
					datediff(d, '1900-01-01', getdate()) + 1,
					'72',
					getdate(),
					replace(convert (varchar(8), getdate(), 108),':',''),
					@ReceiptNumber + ' ' + @Sequence,					
					@UserId,
					@onHand, @onHand + @ReceivedChange,
					@returns, @returns,
					@markdowns, @markdowns,
					@writeOffs, @writeOffs,
					@price, @price
				)
	
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ReceiptLineUpdate for US16631 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure ReceiptLineUpdate for US16631 has not been deployed successfully'
GO