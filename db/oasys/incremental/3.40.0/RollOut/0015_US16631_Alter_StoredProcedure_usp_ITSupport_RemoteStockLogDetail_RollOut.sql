ALTER PROCEDURE [dbo].[usp_ITSupport_RemoteStockLogDetail]
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 12th February 2013
-- 
-- Task     : Extract System SKU Stock Log Data.
------------------------------------------------------------------------------
	
---------------------------------------------------------------------------
-- Declare Parameters
---------------------------------------------------------------------------
@SKU	Char(6),
@sp_Comma Char(1) = 'N', 
@Groups VarChar(20) = '1,2,3,4,5'
As
Begin
	Set NoCount On;
	Declare @TKEY Char(6)   
	---------------------------------------------------------------------------
	-- Create Working Table
	---------------------------------------------------------------------------
	If Exists (Select Name From TempDB.Sys.Objects Where Name like ('#Temp_STKLOGData_%') and [Type] = 'U') Drop Table #Temp_STKLOGData
	Create Table #Temp_STKLOGData
		(
		Id								int				Identity(1,1),
		[Reference]						int				NOT NULL,
		[Date]							Date			NOT NULL,
		[Time]							[char](12)		NOT NULL,
		[User]							[char](200)		NULL,
		[Stock Adjustment Code]			[char](4)		NULL,
		[Store Movement Description]	[char](200)		NULL,
		[System Type ID]				[char](4)		NULL,
		[System Log Description]		[char](200)		NULL,
		[Start Price]					[decimal](7,2)	NULL,
		[End Price]						[decimal](7,2)	NULL,				
		[Start Stock]					[decimal](7,0)	NULL,
		[End Stock]						[decimal](7,0)	NULL,
		[Variance]						[decimal](7,0)	NULL,
		[M/D Start Stock]				[decimal](7,0)	NULL,
		[M/D End Stock]					[decimal](7,0)	NULL,
		[M/D Variance]					[decimal](7,0)	NULL,
		[W/O Start Stock]				[decimal](7,0)	NULL,		
		[W/O End Stock]					[decimal](7,0)	NULL,
		[W/O Variance]					[decimal](7,0)	NULL,
		[Keys]							[char](200)		NULL,
		[Detail: Date]					[char](12)		NULL,
		[Detail: Till]					[char](10)		NULL,
		[Detail: Transaction]			[char](10)		NULL,
		[Detail: Line/Sequence]			[char](10)		NULL,
		[Detail: DRL]					[char](12)		NULL,
		[Detail: Returns]				[char](12)		NULL,
		[Detail: Issue]					[char](12)		NULL,
		[Detail: Consignment]			[char](20)		NULL,
		[Detail: Other]					[char](200)		NULL
	)
	
	---------------------------------------------------------------------------
	-- Insert Data into Working Table
	---------------------------------------------------------------------------
	Insert Into #Temp_STKLOGData
	Select	sl.TKEY					as 'Reference',
			sl.DATE1				as 'Date',
			sl.TIME					as 'Time',
			LTRIM(RTRIM(sl.EEID)) + ' ' + (Select LTRIM(RTRIM(su.Name))) as 'User',
  			Case
				when [Type] = 31 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 31 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 32 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 32 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 33 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 33 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 34 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 34 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 35 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 35 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 36 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 36 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 37 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 37 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 66 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 66 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 67 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 67 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 68 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 68 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				when [Type] = 69 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
				when [Type] = 69 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
				ELSE ' '
		   End as 'Stock Adjustment Code',
   		   CASE
				when [Type] = 31 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 31 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 32 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 32 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 33 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 33 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 34 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 34 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 35 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 35 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 36 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 36 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 37 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 37 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 66 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 66 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 67 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 67 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 68 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 68 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				when [Type] = 69 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
				when [Type] = 69 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
				Else slt.ShortDescription
			   End as 'Store Movement Description',
			   sl.[Type] as 'System Type ID',
			   CASE
				when [Type] <> 0 and [Type] IS NOT NULL Then (Select [Description] From StockLogType Where Id = CAST([TYPE] as int))
				Else ''
			   End as 'System Log Description',
			sl.SPRI					as 'Start Price',
			sl.EPRI					as 'End Price',				
			sl.SSTK					as 'Start Stock',
			sl.ESTK					as 'End Stock',
			(sl.ESTK-sl.SSTK)		as 'Variance',
			sl.SMDN					as 'M/D Start Stock',
			sl.EMDN					as 'M/D End Stock',
			(sl.EMDN-sl.SMDN)		as 'M/D Variance',
			sl.SWTF					as 'W/O Start Stock',		
			sl.EWTF					as 'W/O End Stock',
			(sl.EWTF-sl.SWTF)		as 'W/O Variance',
			sl.KEYS					as 'KEYS',
			/*
			Case
				When (sl.TYPE = 1 OR sl.TYPE = 2 OR sl.TYPE = 3 OR sl.TYPE = 4 OR sl.TYPE = 5) then 'Date:' + SUBSTRING(sl.keys, 1,8) + ' Till:' + SUBSTRING(sl.keys, 9,2) + ' Tran:' + SUBSTRING(sl.keys,11,4) + ' Line Number:' +SUBSTRING(sl.KEYS,15,6)
				When ((sl.TYPE = 21 OR sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)=' ') then 'Header Tkey:' + SUBSTRING(sl.keys, 10, 6) + ' Initials:' + SUBSTRING(sl.keys, 25,9) 
				When ((sl.TYPE = 21 OR sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)='0') then 'Header Number:' + SUBSTRING(sl.keys, 1,6) + ' Header Tkey:' + SUBSTRING(sl.keys, 12,6) + ' Line Tkey:' + SUBSTRING(sl.keys,20,8)
				When ((sl.TYPE = 21) and SUBSTRING(sl.KEYS,2,1)<>' ') then 'Header Number:' + SUBSTRING(sl.keys, 1,6) + ' Header Tkey:' + SUBSTRING(sl.keys,11,6) + ' Line Tkey:' + SUBSTRING(sl.keys,20,8)
				When ((sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)<>' ') then 'Header Number:' + SUBSTRING(sl.keys, 1,6) + ' Header Tkey:' + SUBSTRING(sl.keys,13,4) + ' Line Tkey:' + SUBSTRING(sl.keys,21,6)
				When ((sl.TYPE = 31 OR sl.TYPE = 32 OR sl.TYPE = 35  OR sl.TYPE = 36  OR sl.TYPE = 37  OR sl.TYPE = 66  OR sl.TYPE = 67  OR sl.TYPE = 68  OR sl.TYPE = 69) and SUBSTRING(sl.KEYS,9,1)=' ' )then 'Date:' + SUBSTRING(sl.keys, 1,8) + ' Code:' + SUBSTRING(sl.keys, 11,2) +  ' Seqn:' + SUBSTRING(sl.keys,22,9)
				When ((sl.TYPE = 31 OR sl.TYPE = 32 OR sl.TYPE = 35  OR sl.TYPE = 36  OR sl.TYPE = 37  OR sl.TYPE = 66  OR sl.TYPE = 67  OR sl.TYPE = 68  OR sl.TYPE = 69) and SUBSTRING(sl.KEYS,9,1)<>' ' )then 'Date:' + SUBSTRING(sl.keys, 1,10) + ' Code:' + SUBSTRING(sl.keys, 13,2) +  ' Seqn:' + SUBSTRING(sl.keys,24,9)
				When ((sl.TYPE = 41 OR sl.TYPE = 42 OR sl.TYPE = 73) and SUBSTRING(sl.keys,8,1) = ' ') then 'DRLN:' + SUBSTRING(sl.keys, 1,6) + ' Seqn:' + SUBSTRING(sl.keys, 9, 8) 
				When ((sl.TYPE = 41 OR sl.TYPE = 42 OR sl.TYPE = 73) and SUBSTRING(sl.keys,8,1) <> ' ') then 'DRLN:' + SUBSTRING(sl.keys, 1,6) + ' Seqn:' + SUBSTRING(sl.keys, 8, 8) 
				When (sl.TYPE = 51) then 'Single Item SKU:' + SUBSTRING(sl.keys, 1,6) + ' Bulk/Pack Sku:' + SUBSTRING(sl.keys, 11, 6)  
				When ((sl.TYPE = 61) and SUBSTRING(sl.KEYS,11,1)<>' ') then 'Date:' + SUBSTRING(sl.keys, 11, 8) + ' Status when applied:' + SUBSTRING(sl.keys, 21, 1) 
				When ((sl.TYPE = 61) and SUBSTRING(sl.KEYS,11,1)=' ') then 'Date:' + SUBSTRING(sl.keys, 12, 8) + ' Status when applied:' + SUBSTRING(sl.keys, 22, 1) 
				When ((sl.TYPE = 71) and SUBSTRING(sl.KEYS,8,1)=' ') then 'Issue No:' + SUBSTRING(sl.keys, 1,6) + ' Line No:' + SUBSTRING(sl.keys, 11,4) + ' DRL No:' + SUBSTRING(sl.keys,17,6) + ' Seqn:' + SUBSTRING(sl.keys,25,4)
				When ((sl.TYPE = 71) and SUBSTRING(sl.KEYS,8,1)<>' ') then 'Issue No:' + SUBSTRING(sl.keys, 1,6) + ' Line No:' + SUBSTRING(sl.keys, 8,4) + ' DRL No:' + SUBSTRING(sl.keys,13,6) + ' Seqn:' + SUBSTRING(sl.keys,20,6)
				When (sl.TYPE = 72) then 'DRL No:' + SUBSTRING(sl.keys, 1,6) + ' Seqn:' + SUBSTRING(sl.keys, 11,4) + ' Consignment No:' + SUBSTRING(sl.keys,17,6)
				Else sl.KEYS
			End as 'Details',
			*/			
			-------------------------------------------------------------------------------------------------------------------
			-- Store Details for Subsequent Lookups
			-------------------------------------------------------------------------------------------------------------------
			Case 
				When (sl.TYPE = 1 OR sl.TYPE = 2 OR sl.TYPE = 3 OR sl.TYPE = 4 OR sl.TYPE = 5) then SUBSTRING(sl.keys, 1,8)
				When ((sl.TYPE = 31 OR sl.TYPE = 32 OR sl.TYPE = 35  OR sl.TYPE = 36  OR sl.TYPE = 37  OR sl.TYPE = 66  OR sl.TYPE = 67  OR sl.TYPE = 68  OR sl.TYPE = 69) and SUBSTRING(sl.KEYS,9,1)=' ' )then SUBSTRING(sl.keys, 1,8)
				When ((sl.TYPE = 31 OR sl.TYPE = 32 OR sl.TYPE = 35  OR sl.TYPE = 36  OR sl.TYPE = 37  OR sl.TYPE = 66  OR sl.TYPE = 67  OR sl.TYPE = 68  OR sl.TYPE = 69) and SUBSTRING(sl.KEYS,9,1)<>' ' )then SUBSTRING(sl.keys, 1,10)
				When ((sl.TYPE = 61) and SUBSTRING(sl.KEYS,11,1)<>' ') then SUBSTRING(sl.keys, 11, 8)
				When ((sl.TYPE = 61) and SUBSTRING(sl.KEYS,11,1)=' ') then SUBSTRING(sl.keys, 12, 8)
				Else ''
			End as 'Detail: Date',
			
			Case
				When (sl.TYPE = 1 OR sl.TYPE = 2 OR sl.TYPE = 3 OR sl.TYPE = 4 OR sl.TYPE = 5) then SUBSTRING(sl.keys, 9,2)
				Else '' 
			End as 'Detail: Till',
			
			Case
				When (sl.TYPE = 1 OR sl.TYPE = 2 OR sl.TYPE = 3 OR sl.TYPE = 4 OR sl.TYPE = 5) then SUBSTRING(sl.keys,11,4)
				Else ''
			End as 'Detail: Transaction',
			
			Case
				When (sl.TYPE = 1 OR sl.TYPE = 2 OR sl.TYPE = 3 OR sl.TYPE = 4 OR sl.TYPE = 5) then SUBSTRING(sl.KEYS,15,6)
				When ((sl.TYPE = 21 OR sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)='0') then SUBSTRING(sl.keys,20,8)
				When ((sl.TYPE = 21) and SUBSTRING(sl.KEYS,2,1)<>' ') then SUBSTRING(sl.keys,20,8)
				When ((sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)<>' ') then LTRIM(RTRIM(SUBSTRING(sl.keys,21,6)))
				When ((sl.TYPE = 31 OR sl.TYPE = 32 OR sl.TYPE = 35  OR sl.TYPE = 36  OR sl.TYPE = 37  OR sl.TYPE = 66  OR sl.TYPE = 67  OR sl.TYPE = 68  OR sl.TYPE = 69) and SUBSTRING(sl.KEYS,9,1)=' ' )then SUBSTRING(sl.keys,22,9)
				When ((sl.TYPE = 31 OR sl.TYPE = 32 OR sl.TYPE = 35  OR sl.TYPE = 36  OR sl.TYPE = 37  OR sl.TYPE = 66  OR sl.TYPE = 67  OR sl.TYPE = 68  OR sl.TYPE = 69) and SUBSTRING(sl.KEYS,9,1)<>' ' )then SUBSTRING(sl.keys,24,9)
				When ((sl.TYPE = 41 OR sl.TYPE = 42 OR sl.TYPE = 73) and SUBSTRING(sl.keys,10,1) = ' ') then SUBSTRING(sl.keys, 11, 8) 
				When ((sl.TYPE = 41 OR sl.TYPE = 42 OR sl.TYPE = 73) and SUBSTRING(sl.keys,10,1) <> ' ') then SUBSTRING(sl.keys, 10, 8)-- Changed 8 to 11
				When ((sl.TYPE = 71) and SUBSTRING(sl.KEYS,8,1)=' ') then SUBSTRING(sl.keys, 11,4)
				When ((sl.TYPE = 71) and SUBSTRING(sl.KEYS,8,1)<>' ') then SUBSTRING(sl.keys, 8,4)
				When (sl.TYPE = 72) then SUBSTRING(sl.keys, 11,4)
				Else ''
			End as 'Detail: Line/Sequence',
			
			Case
				When ((sl.TYPE = 41 OR sl.TYPE = 42 OR sl.TYPE = 73) and SUBSTRING(sl.keys,8,1) = ' ') then SUBSTRING(sl.keys, 1,6)
				When ((sl.TYPE = 41 OR sl.TYPE = 42 OR sl.TYPE = 73) and SUBSTRING(sl.keys,8,1) <> ' ') then SUBSTRING(sl.keys, 1,6)
				When ((sl.TYPE = 71) and SUBSTRING(sl.KEYS,8,1)=' ') then SUBSTRING(sl.keys,17,6)
				When ((sl.TYPE = 71) and SUBSTRING(sl.KEYS,8,1)<>' ') then SUBSTRING(sl.keys,13,6)
				When (sl.TYPE = 72) then SUBSTRING(sl.keys, 1,6)
				Else '000000'
			End as 'Detail: DRL',
			
			Case
				When ((sl.TYPE = 21 OR sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)='0') then SUBSTRING(sl.keys, 1,6)
				When ((sl.TYPE = 21) and SUBSTRING(sl.KEYS,2,1)<>' ') then SUBSTRING(sl.keys, 1,6)
				When ((sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)<>' ') then SUBSTRING(sl.keys, 1,6)
				Else ''
			End as 'Detail: Returns',
			
			Case
				When ((sl.TYPE = 71) and SUBSTRING(sl.KEYS,8,1)=' ') then SUBSTRING(sl.keys, 1,6)
				When ((sl.TYPE = 71) and SUBSTRING(sl.KEYS,8,1)<>' ') then SUBSTRING(sl.keys, 1,6)
				Else ''
			End as 'Detail: Issue',
			
			Case
				When (sl.TYPE = 72) then SUBSTRING(sl.keys,17,6)
				Else ''
			End as 'Detail: Consignment',
			
			Case
				When (sl.TYPE = 51) then 'Single Item SKU:' + SUBSTRING(sl.keys, 1,6) + ' Bulk/Pack Sku:' + SUBSTRING(sl.keys, 11, 6)  
				When ((sl.TYPE = 61) and SUBSTRING(sl.KEYS,11,1)<>' ') then 'Date:' + SUBSTRING(sl.keys, 11, 8) + ' Status when applied:' + SUBSTRING(sl.keys, 21, 1) 
				When ((sl.TYPE = 61) and SUBSTRING(sl.KEYS,11,1)=' ') then 'Date:' + SUBSTRING(sl.keys, 12, 8) + ' Status when applied:' + SUBSTRING(sl.keys, 22, 1) 
				Else ''
			End as 'Detail: Other'
			 
		From
			STKLOG as sl
		Right join
			StockLogType	slt		on sl.TYPE = slt.Id
		Inner join
			StockLogTypeGroup sltg	on slt.GroupId = sltg.Id
		Inner join
			fnCsvToTable(@Groups) as fn	on fn.String = sltg.Id
		Inner Join
			SystemUsers as su on cast(su.EmployeeCode as int) = cast(sl.EEID as int)
		where
			sl.SKUN = @SKU
		Order By
			sl.DATE1 DESC, sl.TKEY DESC


		-----------------------------------------------------------------------------------
		-- Process Data to Correct Enteries / Insert Missing Data
		-----------------------------------------------------------------------------------
		Declare @WorkingDAT_ID int
		Declare @WorkingDAT_MAXID int
		Select  @WorkingDAT_MAXID = MAX(ID)FROM #Temp_STKLOGData
		Set		@WorkingDAT_ID = 1
		  
		While	(@WorkingDAT_ID <= @WorkingDAT_MAXID)
			Begin		         
				-- Setup Initial Parameters
				Declare @WorkingDAT_Type Char(2) = (Select LTRIM(RTRIM([System Type ID])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
				
				-- Improve Type 22 Results
				if @WorkingDAT_Type = '22'
					Begin
						Declare @WD_Return char(6) = (Select LTRIM(RTRIM([Detail: Returns])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
						Update #Temp_STKLOGData
						Set [Detail: DRL] = (Select LTRIM(RTRIM(DRLN)) From RETHDR Where NUMB = @WD_Return)
						Where ID = @WorkingDAT_ID
					End
				
				-- Improve Type 31 Results	
				if @WorkingDAT_Type = '31'
					Begin
						Declare @WD_Code Char(2) = (Select LTRIM(RTRIM([Stock Adjustment Code])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
						-- Code 4 DRL Number
						if @WD_Code = '04'
							Begin
								Update #Temp_STKLOGData
								Set [Detail: DRL] = (Select TOP(1) LTRIM(RTRIM(DRLN)) From STKADJ Where SKUN = @SKU and Date1 = (Select [Date] From #Temp_STKLOGData Where ID = @WorkingDAT_ID))
								Where ID = @WorkingDAT_ID
							End
						-- Code 53 Details
						if @WD_Code = '53'
							Begin
								Update #Temp_STKLOGData
								Set [Detail: Issue] = (Select TOP(1) LTRIM(RTRIM(WAUT)) From STKADJ Where SKUN = @SKU and DAUT = (Select [Date] From #Temp_STKLOGData Where ID = @WorkingDAT_ID)),
									[Detail: Date] = (Select TOP(1) DAUT From STKADJ Where SKUN = @SKU and DAUT = (Select [Date] From #Temp_STKLOGData Where ID = @WorkingDAT_ID))
								Where ID = @WorkingDAT_ID
							End
					End
				
				-- Improve Type 41 Results (Out)
				if @WorkingDAT_Type = '41'
					Begin
						Declare @WD_DRL char(6) = (Select LTRIM(RTRIM([Detail: DRL])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
						Update #Temp_STKLOGData
						Set [Detail: Returns] = (Select LTRIM(RTRIM([1STR])) From DRLSUM Where NUMB = @WD_DRL)
						Where ID = @WorkingDAT_ID
					End
					
				-- Improve Type 42 Results (In)
				if @WorkingDAT_Type = '42'
					Begin
						Declare @WD_DRL2 char(6) = (Select LTRIM(RTRIM([Detail: DRL])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
						Update #Temp_STKLOGData
						Set [Detail: Returns] = (Select LTRIM(RTRIM([1STR])) From DRLSUM Where NUMB = @WD_DRL2),
							[Detail: Issue] = (Select LTRIM(RTRIM([1IBT])) From DRLSUM Where NUMB = @WD_DRL2),
							[Detail: Consignment] = (Select LTRIM(RTRIM([1CON])) From DRLSUM Where NUMB = @WD_DRL2)
						Where ID = @WorkingDAT_ID
					End
				
				-- Improve Type 71 Results (Directs)
				if @WorkingDAT_Type = '71'
					Begin
						Declare @WD_IWDRL char(6) = (Select LTRIM(RTRIM([Detail: DRL])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
						Declare @WD_IWISU char(6) = (Select LTRIM(RTRIM([Detail: Issue])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
						Declare @WD_IWLNE char(6) = (Select LTRIM(RTRIM([Detail: Line/Sequence])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
						Declare @WD_IWPON Char(6) = (Select LTRIM(RTRIM([0PON])) From DRLSUM Where NUMB = @WD_IWDRL and [0DL1] = @WD_IWISU)
						Update #Temp_STKLOGData
						Set [Detail: Other] = (Select LTRIM(RTRIM([ADEP]))+ '/' + LTRIM(RTRIM([CNUM])) From CONDET Where PNUM = @WD_IWPON and LINE = @WD_IWLNE and SKUN = @SKU)
						Where ID = @WorkingDAT_ID
					End
					
				-- Improve Type 72 Results (Directs)
				if @WorkingDAT_Type = '72'
					Begin
						Declare @WD_DRL3 char(6) = (Select LTRIM(RTRIM([Detail: DRL])) From #Temp_STKLOGData Where ID = @WorkingDAT_ID)
						Update #Temp_STKLOGData
						Set [Detail: Returns] = (Select LTRIM(RTRIM([0PON])) From DRLSUM Where NUMB = @WD_DRL3)
						Where ID = @WorkingDAT_ID
					End
							
				Set @WorkingDAT_ID = @WorkingDAT_ID+1 
			End
			
		--------------------------------------------------------------------------------------
		-- Select Records for Output
		--------------------------------------------------------------------------------------                                                                                                                        
		Select	[Reference],
				[Date],
				[Time],
				[User],
				[Stock Adjustment Code],
				[Store Movement Description],
				[System Type ID],
				[System Log Description],
				[Start Price],
				[End Price],				
				[Start Stock],
				[End Stock],
				[Variance],
				[M/D Start Stock],
				[M/D End Stock],
				[M/D Variance],
				[W/O Start Stock],		
				[W/O End Stock],
				[W/O Variance],
		Case
			-- Sales Formatting
			When LTRIM(RTRIM([System Type ID])) = '01' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Till: ' + LTRIM(RTRIM([Detail: Till])) + ' Transaction: ' + LTRIM(RTRIM([Detail: Transaction])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '02' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Till: ' + LTRIM(RTRIM([Detail: Till])) + ' Transaction: ' + LTRIM(RTRIM([Detail: Transaction])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '03' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Till: ' + LTRIM(RTRIM([Detail: Till])) + ' Transaction: ' + LTRIM(RTRIM([Detail: Transaction])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '04' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Till: ' + LTRIM(RTRIM([Detail: Till])) + ' Transaction: ' + LTRIM(RTRIM([Detail: Transaction])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '05' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Till: ' + LTRIM(RTRIM([Detail: Till])) + ' Transaction: ' + LTRIM(RTRIM([Detail: Transaction])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '11' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Till: ' + LTRIM(RTRIM([Detail: Till])) + ' Transaction: ' + LTRIM(RTRIM([Detail: Transaction])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '12' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Till: ' + LTRIM(RTRIM([Detail: Till])) + ' Transaction: ' + LTRIM(RTRIM([Detail: Transaction])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			-- Returns Formatting
			When LTRIM(RTRIM([System Type ID])) = '21' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Return: ' + LTRIM(RTRIM([Detail: Returns]))
			When LTRIM(RTRIM([System Type ID])) = '22' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Return: ' + LTRIM(RTRIM([Detail: Returns])) + ' DRL: ' + LTRIM(RTRIM([Detail: DRL]))
			-- Stock Adjustment Formatting
			When ((LTRIM(RTRIM([System Type ID])) = '31') and LTRIM(RTRIM([Stock Adjustment Code])) <> '04') Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When ((LTRIM(RTRIM([System Type ID])) = '31') and LTRIM(RTRIM([Stock Adjustment Code])) = '04') Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' DRL: ' + LTRIM(RTRIM([Detail: DRL])) 
			When ((LTRIM(RTRIM([System Type ID])) = '32') and LTRIM(RTRIM([Stock Adjustment Code])) <> '04') Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When ((LTRIM(RTRIM([System Type ID])) = '32') and LTRIM(RTRIM([Stock Adjustment Code])) = '04') Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' DRL: ' + LTRIM(RTRIM([Detail: DRL])) 
			When LTRIM(RTRIM([System Type ID])) = '33' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '34' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '35' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '36' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			When LTRIM(RTRIM([System Type ID])) = '37' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			-- IBT Formatting
			When LTRIM(RTRIM([System Type ID])) = '41' Then 'DRL: ' + LTRIM(RTRIM([Detail: DRL])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' TO Store: ' + LTRIM(RTRIM([Detail: Returns]))
			When LTRIM(RTRIM([System Type ID])) = '42' Then 'DRL: ' + LTRIM(RTRIM([Detail: DRL])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' FROM Store: ' + LTRIM(RTRIM([Detail: Returns])) + ' DRL(IBT): ' + LTRIM(RTRIM([Detail: Issue])) + ' Consignment: ' + LTRIM(RTRIM([Detail: Consignment]))
			When LTRIM(RTRIM([System Type ID])) = '43' Then 'DRL: ' + LTRIM(RTRIM([Detail: DRL])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' TO Store: ' + LTRIM(RTRIM([Detail: Returns]))
			When LTRIM(RTRIM([System Type ID])) = '44' Then 'DRL: ' + LTRIM(RTRIM([Detail: DRL])) + ' Line: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' FROM Store: ' + LTRIM(RTRIM([Detail: Returns])) + ' DRL(IBT): ' + LTRIM(RTRIM([Detail: Issue])) + ' Consignment: ' + LTRIM(RTRIM([Detail: Consignment]))
			-- Bulk To Single Transfers
			When LTRIM(RTRIM([System Type ID])) = '51' Then 'Single Item SKU: ' + SUBSTRING(Keys, 1,6) + ' Bulk/Pack Sku: ' + SUBSTRING(Keys, 11, 6)  
			-- Price Changes Formatting
			When ((LTRIM(RTRIM([System Type ID])) = '61') and SUBSTRING(KEYS,11,1)<>' ') then 'Date: ' + SUBSTRING(Keys, 11, 8) + ' Status: ' + SUBSTRING(KEYS,21,1)
			When ((LTRIM(RTRIM([System Type ID])) = '61') and SUBSTRING(KEYS,11,1)=' ') then 'Date: ' + SUBSTRING(Keys, 12, 8) + ' Status: ' + SUBSTRING(KEYS,22,1)
			-- Markdown Formatting
			When LTRIM(RTRIM([System Type ID])) = '66' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' DRL: ' + LTRIM(RTRIM([Detail: DRL])) 
			When LTRIM(RTRIM([System Type ID])) = '67' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence]))
			-- Purchase Orders Formatting
			When LTRIM(RTRIM([System Type ID])) = '71' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' DRL: ' + LTRIM(RTRIM([Detail: DRL])) + ' Issue: ' + LTRIM(RTRIM([Detail: Issue])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' Consignment: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' Container: ' + LTRIM(RTRIM([Detail: Other]))
			When LTRIM(RTRIM([System Type ID])) = '72' Then 'Date: ' + LTRIM(RTRIM([Detail: Date])) + ' DRL: ' + LTRIM(RTRIM([Detail: DRL])) + ' Sequence: ' + LTRIM(RTRIM([Detail: Line/Sequence])) + ' Consignment: ' + LTRIM(RTRIM([Detail: Consignment])) + ' P/O: ' + LTRIM(RTRIM([Detail: Returns]))
			-- System Formatting
			When LTRIM(RTRIM([System Type ID])) = '91' Then 'System Adjustment 91 - Refer to IT Support' 
			When LTRIM(RTRIM([System Type ID])) = '99' Then 'Date: ' + SUBSTRING(Keys, 11, 8)
			Else LTRIM(RTRIM(Keys))
		End as 'Detail Information'
		From #Temp_STKLOGData
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_ITSupport_RemoteStockLogDetail for US16631 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_ITSupport_RemoteStockLogDetail for US16631 has not been deployed successfully'
GO