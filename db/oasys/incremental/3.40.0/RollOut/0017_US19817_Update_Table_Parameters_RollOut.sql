update dbo.[Parameters]
set BooleanValue = 1
where ParameterID = 2203

GO

If @@Error = 0
   Print 'Success: The Update parameter 2203 for zero banking US19817 has been deployed successfully'
Else
   Print 'Failure: The Update parameter 2203 for zero banking US19817 has not been deployed'
GO