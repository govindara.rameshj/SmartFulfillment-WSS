IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'usp_UpdatePriceChangeRecordWithOriginalDate') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure usp_UpdatePriceChangeRecordWithOriginalDate'
	EXEC ('CREATE PROCEDURE dbo.usp_UpdatePriceChangeRecordWithOriginalDate AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure usp_UpdatePriceChangeRecordWithOriginalDate')
GO

ALTER PROCEDURE [dbo].[usp_UpdatePriceChangeRecordWithOriginalDate] 
	@Skun Char(6),
	@OriginalPDAT Date,
	@OriginalEventNumber Char(6),
	@OriginalPrice decimal(10,2),
	@OriginalStatus Char(1),
	@StartDate DATE,
	@Price decimal(10,2),
	@Status Char(1),
	@EventNumber Char(6),
	@Priority Char(2),
	@AutoApplyDate DATE
AS
BEGIN
	SET NOCOUNT ON;

Update [Oasys].[dbo].[PRCCHG]
        set 
			[PDAT] = @StartDate
		   ,[EVNT] = @EventNumber
           ,[PRIC] = @Price
           ,[PSTA] = @Status
           ,[PRIO] = @Priority
           ,[AUDT] = @AutoApplyDate
		   ,[AUAP] = NULL
        Where
            [Skun] = @Skun And 
			[PDAT] = @OriginalPDAT And
			[EVNT] = @OriginalEventNumber And 
			[PRIC] = @OriginalPrice And
			[PSTA] = @OriginalStatus
Return @@RowCount
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_UpdatePriceChangeRecordWithOriginalDate for US9523 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_UpdatePriceChangeRecordWithOriginalDate for US9523 has not been deployed'
GO