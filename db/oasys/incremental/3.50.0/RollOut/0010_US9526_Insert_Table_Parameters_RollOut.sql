INSERT INTO [dbo].[Parameters]
    ([ParameterID]
    ,[Description]
    ,[StringValue]
    ,[LongValue]
    ,[BooleanValue]
    ,[DecimalValue]
    ,[ValueType])
SELECT
     4700
    ,'System Enquiry Report Tills'
    ,'0:12;65:69;90:99'
    ,0
    ,0
    ,0
    ,0
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] WHERE ParameterID = 4700)
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US5926 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US5926 has not been deployed successfully'
GO
