IF EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = 51)
DELETE FROM dbo.Parameters WHERE ParameterID = 51

If @@Error = 0
   Print 'Success: Delete from table Parameters for US10650 has been deployed successfully'
Else
   Print 'Failure: Delete from table Parameters for US10650 has not been deployed successfully'
GO