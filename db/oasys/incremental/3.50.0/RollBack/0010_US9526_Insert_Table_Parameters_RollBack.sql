DELETE FROM  [dbo].[Parameters] WHERE ParameterID = 4700
GO

If @@Error = 0
   Print 'Success: Delete from Table Parameters for US5926 has been deployed successfully'
Else
   Print 'Failure: Delete from Table Parameters for US5926 has not been deployed successfully'
Go
