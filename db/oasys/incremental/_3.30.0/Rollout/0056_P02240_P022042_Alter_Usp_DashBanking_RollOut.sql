alter procedure DashBanking
@DateEnd	date =null
as
begin
   set nocount on

   declare @Output table(
                         RowId int,
                         [Description]   varchar(50),
                         [Date]           date,
                         TodayValue       dec(9,2),
                         WeekToTodayValue dec(9,2),
                         SelectedDate Date
                         )

   declare @LastDate date

   set @LastDate = (select max(PeriodDate) from Safe where Safe.IsClosed = 1)

   insert into @Output values (1,'Last Daily Banking sent to HQ', @LastDate, null, null,@DateEnd)
   insert into @Output values (2,'Safe Check',                    null,      null, null,@DateEnd)
/* insert into @Output values (3,'Cash Shorts (Cash Perf)',       null,      null, null,@DateEnd)*/

   select
          RowId,    
          [Description],
          [Date],
          [Day] = TodayValue,
          WTD   = WeekToTodayValue,
          SelectedDate
   from @Output

end
go

if @@error = 0
   print 'Success: Stored procedure DashBanking for P022-040 and P022-042 has been successfully altered'
else
   print 'Failure: Stored procedure DashBanking for P022-040 and P022-042 has NOT been successfully altered'
go