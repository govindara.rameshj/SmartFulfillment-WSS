CREATE function [dbo].[udf_EffectiveEVTCHGRecordForSkuOnDate] 
	(
	@sku char(6),
	@date date
	)
RETURNS @Results TABLE
(
	[SKUN] [char](6) NOT NULL,
	[SDAT] [date] NOT NULL,
	[PRIO] [char](2) NOT NULL,
	[NUMB] [char](6) NOT NULL,
	[EDAT] [date] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[IDEL] [bit] NOT NULL,
	[SPARE] [char](40) NULL
)
AS

BEGIN
	insert into @Results 
	select EVTCHGFullRow.* from Oasys.dbo.EVTCHG EVTCHGFullRow
		inner join (
			(select evt.skun, max(evt.numb) 'numb' 					
			from Oasys.dbo.evtchg evt					
			inner join (					
						select evt.skun, max(evt.prio) 'prio' 		
						from Oasys.dbo.evtchg evt		
							inner join Oasys.dbo.stkmas stk	
								on stk.SKUN = evt.SKUN 
						where stk.SKUN = @sku
							and sdat <= @date		
							and isnull(edat, @date) >= @date
							and evt.idel = 0
							and stk.IDEL = 0
						group by evt.skun) MaximumPriorityEVTCHGForSkuOnDate		
				on evt.skun = MaximumPriorityEVTCHGForSkuOnDate.skun				
					and evt.prio = MaximumPriorityEVTCHGForSkuOnDate.prio			
			inner join Oasys.dbo.stkmas stk					
				on stk.SKUN = evt.SKUN 				
			where stk.skun = @sku
				and sdat <= @date					
				and isnull(edat, @date) >= @date
				and evt.idel = 0			
				and stk.IDEL = 0
			group by evt.skun)
			) EffectiveEVTCHGRecordKeyValues
			on EffectiveEVTCHGRecordKeyValues.Skun = EVTCHGFullRow.SKUN
				and EffectiveEVTCHGRecordKeyValues.Numb = EVTCHGFullRow.NUMB 
				
	return
END

GO

grant select on dbo.udf_EffectiveEVTCHGRecordForSkuOnDate to [NT AUTHORITY\IUSR]
grant select on dbo.udf_EffectiveEVTCHGRecordForSkuOnDate to [role_execproc]

go

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_EffectiveEVTCHGRecordForSkuOnDate" has been sucessfully created';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_EffectiveEVTCHGRecordForSkuOnDate" might NOT have been created';
    END
GO