Grant Execute On Usp_GetNumberOfDeliveriesToday To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure Usp_GetNumberOfDeliveriesToday has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure Usp_GetNumberOfDeliveriesToday might NOT have been successfully deployed';
    End;
Go

Grant Execute On Usp_GetNumberOfDeliveriesToday To [role_execproc];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure Usp_GetNumberOfDeliveriesToday has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure Usp_GetNumberOfDeliveriesToday might NOT have been successfully deployed';
    End;
Go
