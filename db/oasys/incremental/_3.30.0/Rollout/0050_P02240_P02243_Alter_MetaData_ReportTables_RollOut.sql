set nocount on

declare @Count  int
declare @Output nvarchar(max)
declare @ColumnOneWidth int = 450
declare @ColumnTwoWidth int = 420
declare @ColumnThreeWidth int = 350

------------------------------------------------------------------------------------------------------------------------
print 'Reorganise Dashboard'

begin try

   begin transaction

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Sales Panel'
   print @Output

   set @Count = 0

   update Report set Title    = 'Store Sales'					 where ID = 1; set @Count += @@rowcount
   update Report set MinWidth = @ColumnOneWidth, MinHeight = 315 where ID = 1; set @Count += @@rowcount

   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(1, 1, 0, 0, 0, 0, null, null, null); set @Count += @@rowcount

   insert ReportHyperlink(ReportId, TableId, RowId, ColumnName, Value) values(1, 1, 2, 'Description', 'MenuId=3330');            set @Count += @@rowcount
   insert ReportHyperlink(ReportId, TableId, RowId, ColumnName, Value) values(1, 1, 8, 'Description', 'IncorrectEANReport.exe'); set @Count += @@rowcount

   Delete ReportColumn Where Id = 6022; set @Count += @@rowcount
   Delete ReportColumn Where Id = 6023; set @Count += @@rowcount
   update ReportColumn set MinWidth = 160, MaxWidth = 160                                       where Id = 6024; set @Count += @@rowcount
   update ReportColumn set MinWidth =  77, MaxWidth =  77, Caption = 'Trans Day'                where Id = 6025; set @Count += @@rowcount
   update ReportColumn set MinWidth =  68, MaxWidth =  68, Caption = 'WTD'                      where Id = 6026; set @Count += @@rowcount
   update ReportColumn set MinWidth =  77, MaxWidth =  77, Caption = 'Day Value', Alignment = 3 where Id = 6027; set @Count += @@rowcount
   update ReportColumn set MinWidth =  64, MaxWidth =  64, Caption = 'WTD',       Alignment = 3 where Id = 6028; set @Count += @@rowcount

   if @Count = 12
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 1)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Shrinkage Panel'
   print @Output

   set @Count = 0

   update Report set MinWidth  = @ColumnTwoWidth where ID = 4; set @Count += @@rowcount
   update Report set MinHeight = 195             where ID = 4; set @Count += @@rowcount

   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId =  103; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 1100; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 1101; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 1200; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 1201; set @Count += @@rowcount

   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4010, 'No. Adj. Day', 'No. Adj. Day', 1, 'n0', 0, 90,   90, 2);    set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4011, 'No. Adj. WTD', 'WTD',          1, 'n0', 0, 70,   70, 2);    set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4012, 'Total',        'Total',        1, 'c2', 0, 62,   62, 3);    set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4013, 'WTD',          'WTD',          1, 'c2', 0, 64,   64, 3);    set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4014, 'Description',  'Description',  0, null, 0, 120, 120, null); set @Count += @@rowcount

   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (4, 1,    0, 0, 0, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (4, 1, 4014, 1, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (4, 1, 4010, 2, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (4, 1, 4011, 3, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (4, 1, 4012, 4, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (4, 1, 4013, 5, 1, 0, null, null, null); set @Count += @@rowcount

   if @Count = 18
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Banking Panel'
   print @Output

   set @Count = 0

   update Report set MinWidth  = @ColumnThreeWidth, MinHeight = 200 where ID = 6; set @Count += @@rowcount

   delete ReportColumns where ReportId = 6 and TableId = 1 and ColumnId =  103; set @Count += @@rowcount

   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4020, 'Day',          'Day',         1, 'c2', 0, 60,  60,  3); set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4021, 'WTD',          'WTD',         1, 'c2', 0, 60,  60,  3); set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4022, 'Description',  'Description', 0, null, 0, 151, 151, null); set @Count += @@rowcount

   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (6, 1,    0, 0, 0, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (6, 1, 4022, 1, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (6, 1, 4020, 3, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (6, 1, 4021, 4, 1, 0, null, null, null); set @Count += @@rowcount

   if @Count = 9
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 5)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Deliveries Panel'
   print @Output

   set @Count = 0

   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (6029, 'Qty',      'Qty',   1, 'n0', 0, 77, 77, 2); set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (6030, 'QtyWtd',   'WTD',   1, 'n0', 0, 81, 81, 2); set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (6031, 'Value',    'Value', 1, 'c2', 0, 64, 64, 3); set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (6032, 'ValueWtd', 'WTD',   1, 'c2', 0, 64, 64, 3); set @Count += @@rowcount

   update Report set Title     = 'Deliveries Sold' where ID = 5; set @Count += @@rowcount
   update Report set MinWidth = @ColumnOneWidth    where ID = 5; set @Count += @@rowcount
   update Report set MinHeight = 160               where ID = 5; set @Count += @@rowcount

   update ReportColumn set Alignment = 3 where Id = 1201; set @Count += @@rowcount
   
   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 103; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 6024, 2, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 1100; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 6029, 3, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 1101; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 6030, 4, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 1200; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 6031, 5, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 1201; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 6032, 6, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   if @Count = 18
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 2)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Customer Order Status Panel'
   print @Output

   set @Count = 0

   update Report set Title = 'Pending Orders'					 where ID = 10; set @Count += @@rowcount
   update Report set MinWidth = @ColumnOneWidth, MinHeight = 250 where Id = 10; set @Count += @@rowcount

   delete ReportColumns where ReportId = 10 and TableId = 1 and ColumnId = 1100; set @Count += @@rowcount
   delete ReportColumns where ReportId = 10 and TableId = 1 and ColumnId = 1200; set @Count += @@rowcount
   Delete ReportColumns Where ReportId = 10 And TableId = 1 And ColumnId = 103; Set @Count += @@rowcount

   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4000, 'Today Qty',   'Today Qty',   1, 'n0', 0, 78,  78,  2); Set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4001, 'Today Value', 'Today Value', 1, 'c2', 0, 91,  91,  3); Set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4002, 'Total Qty',   'Total',       1, 'n0', 0, 66,  66, 2);  Set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4003, 'Total Value', 'Total',       1, 'c2', 0, 83,  83, 3);  Set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4004, 'Description', 'Description', 0, Null, 0, 128, 128, 1); Set @Count += @@rowcount

   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (10, 1, 4004, 1, 1, 0, 1, -65536, null); Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (10, 1, 4000, 2, 1, 0, 1, -65536, null); Set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (10, 1, 4002, 3, 1, 0, 1, -65536, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (10, 1, 4001, 4, 1, 0, 1, -65536, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (10, 1, 4003, 5, 1, 0, 1, -65536, null); set @Count += @@rowcount

   insert ReportParameters(ReportId, ParameterId, Sequence, AllowMultiple, DefaultValue) values(10, 103, 1, 0, null); set @Count += @@rowcount

   if @Count = 16
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 3)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Pending Shrinkage Panel (NEW)'
   print @Output

   set @Count = 0

   insert Report(Id, Header, Title, ProcedureName, HideWhenNoData, PrintLandscape, MinWidth, MinHeight) values(13, 'Manager Dashboard', 'Pending Shrinkage', 'DashPendingShrinkage', 0, 0, @ColumnTwoWidth, 120); set @Count += @@rowcount

   insert ReportTable (ReportId, Id, Name, AllowGrouping, AllowSelection, ShowHeaders, ShowIndicator, ShowLinesVertical,
                       ShowLinesHorizontal, ShowBorder, ShowInPrintOnly, AutoFitColumns, HeaderHeight, RowHeight, AnchorTop, AnchorBottom)
                values(13, 1, 'Master', 0, 0, 1, 0, 1, 1, 1, 0, 1, null, null, 1, 1); set @Count += @@rowcount

   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4050, 'Today Qty',   'Today Qty',   1, 'n0', 0, 90,   90, 2);    set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4051, 'WTD Qty',     'WTD',         1, 'n0', 0, 70,   70, 2);    set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4052, 'Today Value', 'Today Value', 1, 'c2', 0, 62,   62, 3);    set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4053, 'WTD Value',   'WTD',         1, 'c2', 0, 64,   64, 3);    set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4054, 'Description', 'Description', 0, null, 0, 120, 120, null); set @Count += @@rowcount

   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (13, 1, 0,    1, 0, 0, null, null,   null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (13, 1, 4054, 2, 1, 0, 1,    -65536, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (13, 1, 4050, 3, 1, 0, 1,    -65536, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (13, 1, 4051, 4, 1, 0, 1,    -65536, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (13, 1, 4052, 5, 1, 0, 1,    -65536, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (13, 1, 4053, 6, 1, 0, 1,    -65536, null); set @Count += @@rowcount

   if @Count = 13
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'DRL Analysis Panel'
   print @Output

   set @Count = 0

   update Report set MinWidth  = @ColumnTwoWidth where ID = 2; set @Count += @@rowcount
   update Report set MinHeight = 160             where ID = 2; set @Count += @@rowcount

   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4040, 'Today', 'Today', 1, 'c2', 0, 55,  55,  3); set @Count += @@rowcount

   delete ReportColumns where ReportId = 2 and TableId = 1 and ColumnId = 1200; set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (2, 1,    0, 0, 0, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (2, 1, 4040, 4, 1, 0, null, null, null); set @Count += @@rowcount


   if @Count = 6
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Stock Panel'
   print @Output

   set @Count = 0

   update Report set Title = 'Stock Reports'     where ID = 7; set @Count += @@rowcount
   update Report set MinWidth  = @ColumnTwoWidth where ID = 7; set @Count += @@rowcount

   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4030, 'Qty',         'Qty',         1, 'n0', 0,  86,  86, 2); set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4031, 'Value',       'Value',       1, 'c2', 0,  96,  96, 3); set @Count += @@rowcount
   insert ReportColumn (Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4032, 'Description', 'Description', 0, NULL, 0, 224, 224, 1); set @Count += @@rowcount
   
   delete ReportColumns where ReportId = 7 and TableId = 1 and ColumnId =  103; set @Count += @@rowcount
   delete ReportColumns where ReportId = 7 and TableId = 1 and ColumnId = 1100; set @Count += @@rowcount
   delete ReportColumns where ReportId = 7 and TableId = 1 and ColumnId = 1200; set @Count += @@rowcount

   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (7, 1,    0, 0, 0, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (7, 1, 4032, 1, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (7, 1, 4030, 2, 1, 0, null, null, 9);    set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (7, 1, 4031, 3, 1, 0, null, null, 9);    set @Count += @@rowcount

   if @Count = 12
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 6)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Activity Report'
   print @Output

   set @Count = 0

   update Report set Title = 'Banking Reports'     where ID = 9; set @Count += @@rowcount
   update Report set MinWidth  = @ColumnThreeWidth where ID = 9; set @Count += @@rowcount

   insert ReportColumn(Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4033, 'Description', 'Description', 0, NULL, 0, 123, 123, 1); set @Count += @@rowcount
   insert ReportColumn(Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4034, 'Value',       'Value',       1, 'c2', 0,  61,  61, 3); set @Count += @@rowcount
   insert ReportColumn(Id, Name, Caption, FormatType, Format, IsImagePath, MinWidth, MaxWidth, Alignment) values (4035, 'ValueWtd',    'WTD',         1, 'c2', 0,  62,  62, 3); set @Count += @@rowcount
   
   delete ReportColumns where ReportId = 9 and TableId = 1 and ColumnId =   103; set @Count += @@rowcount
   delete ReportColumns where ReportId = 9 and TableId = 1 and ColumnId =  1200; set @Count += @@rowcount
   delete ReportColumns where ReportId = 9 and TableId = 1 and ColumnId =  1201; set @Count += @@rowcount
   
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (9, 1,    0, 0, 0, 0, NULL, NULL, NULL); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (9, 1, 4033, 2, 1, 0, NULL, NULL, NULL); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (9, 1, 4034, 5, 1, 0, NULL, NULL, NULL); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (9, 1, 4035, 6, 1, 0, NULL, NULL, NULL); set @Count += @@rowcount

   if @Count = 12
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Price Change Report'
   print @Output

   set @Count = 0

   update Report set MinWidth = @ColumnThreeWidth, MinHeight = 250 where ID = 8; set @Count += @@rowcount

   insert ReportParameters(ReportId, ParameterId, Sequence, AllowMultiple, DefaultValue) values(8, 103, 1, 0, null); set @Count += @@rowcount

   if @Count = 2
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 9)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Configure Display & Sort Order'
   print @Output

   set @Count = 0

   update MenuConfig set Parameters = '1,4,6,13,9,5,2,10,7,8' where ID = 10110; set @Count += @@rowcount
   update MenuConfig set Parameters = '1,4,6,13,9,5,2,10,7,8' where ID = 11010; set @Count += @@rowcount

   if @Count = 2
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 7)
   end

   commit transaction
   print 'Reorganise Dashboard: Successful'

end try

begin catch

   rollback transaction
   print 'Reorganise Dashboard: Unsuccessful'
   print 'Rollout unsuccessful'
   return

end catch

print 'Rollout successful'