DROP FUNCTION dbo.svf_ReportPriceChangesOverdueQty
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The function "svf_ReportPriceChangesOverdueQty" for CR0110 has been sucessfully dropped';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The function "svf_ReportPriceChangesOverdueQty" for CR0110 has NOT been dropped';
    END;