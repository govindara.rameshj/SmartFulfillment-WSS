Create Procedure [dbo].[usp_GetRelatedItemsRequiringAdjustments]
As
	Begin
		Select 
			SingleSkun = single.SKUN,
			SinglePrice = single.PRIC,
			SingleOnHand = single.ONHA,
			PackSkun = pack.SKUN, 
			PackPrice = pack.PRIC,
			PackOnHand = pack.ONHA,
			ItemPackSize = item.NSPP
		From
			RELITM item
				Inner Join
					STKMAS single
				On
					item.SPOS = single.SKUN
				inner join
					STKMAS pack
				On
					item.PPOS = pack.SKUN
		Where
			single.ONHA < 0
		And
			item.DELC = 0
End
Go
If @@Error = 0
   Print 'Success: The new stored procedure "usp_GetRelatedItemsRequiringAdjustments" for RF0987 has been sucessfully created'
Else
   Print 'Failure: The new stored procedure "usp_GetRelatedItemsRequiringAdjustments" for RF0987 has NOT been created'
Go
--Apply Permissions

Grant Execute On usp_GetRelatedItemsRequiringAdjustments To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure usp_GetRelatedItemsRequiringAdjustments for RF0987 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure usp_GetRelatedItemsRequiringAdjustments for RF0987 might NOT have been successfully deployed';
    End;
Go

Grant Execute On usp_GetRelatedItemsRequiringAdjustments To [role_execproc];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure usp_GetRelatedItemsRequiringAdjustments for RF0987 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure usp_GetRelatedItemsRequiringAdjustments for RF0987 might NOT have been successfully deployed';
    End;
Go
