-- =============================================
-- Author        : Alan Lewis
-- Create date   : 02/11/2012
-- User Story	 : 8783
-- Project		 : RF1094:  Till performing multiple refunds
-- Task Id		 : 8957
-- Description   : Create active requirement switch
-- =============================================
Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	981094,      'Enable requirement RF1094', Null,        Null,      1,            Null,         3)
Go

If @@Error = 0
   Print 'Success: The active requirement switch for RF1094 has been successfully deployed'
Else
   Print 'Failure: active requirement switch for RF1094 might NOT have been deployed'
Go
