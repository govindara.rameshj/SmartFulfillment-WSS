Create Procedure [dbo].[usp_AdjustRelatedItemsMarkup]
	@Skun As Int,
	@Value As Decimal(9, 2)
As
	Begin
	
		Set NoCount On
		
		Update
			RELITM
		Set
			WTDS = WTDS + NSPP,
			WTDM = WTDM + @Value,
			PTDM = PTDM + @Value,
			YTDM = YTDM + @Value
		Where
			SPOS = @Skun
End
Go
If @@Error = 0
   Print 'Success: The new stored procedure "usp_AdjustRelatedItemsMarkup" for RF0987 has been sucessfully created'
Else
   Print 'Failure: The new stored procedure "usp_AdjustRelatedItemsMarkup" for RF0987 has NOT been created'
Go
--Apply Permissions

Grant Execute On usp_AdjustRelatedItemsMarkup To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure usp_AdjustRelatedItemsMarkup for RF0987 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure usp_AdjustRelatedItemsMarkup for RF0987 might NOT have been successfully deployed';
    End;
Go

Grant Execute On usp_AdjustRelatedItemsMarkup To [role_execproc];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure usp_AdjustRelatedItemsMarkup for RF0987 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure usp_AdjustRelatedItemsMarkup for RF0987 might NOT have been successfully deployed';
    End;
Go
