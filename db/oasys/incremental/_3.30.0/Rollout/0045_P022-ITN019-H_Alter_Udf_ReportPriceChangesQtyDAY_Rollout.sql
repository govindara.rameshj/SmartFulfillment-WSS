-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 8th June 2011
-- Description	: Returns the figure to use for Price Changes Qty for TODAY - Scalar Valued Function
-- Notes		: This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION dbo.svf_ReportPriceChangesQtyDAY(
                                               )
RETURNS int
AS
BEGIN

    DECLARE
       @PCToday int;
    SELECT @PCToday = COUNT( *
                           )
      FROM
           PRCCHG prc INNER JOIN STKMAS stk
           ON prc.SKUN
              = 
              stk.SKUN
                      INNER JOIN evtchg ec
           ON ec.SKUN
              = 
              prc.SKUN
          AND ec.NUMB
              = 
              prc.EVNT
          AND ec.PRIO
              = 
              prc.PRIO
      WHERE prc.PSTA = 'U'
        AND ec.SDAT
            = 
            ( 
              SELECT tmdt
                FROM sysdat
            );

    RETURN @PCToday;

END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportPriceChangesQtyDAY" has been sucessfully altered for P022-ITNO19-H';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportPriceChangesQtyDAY" might NOT have been altered for P022-ITNO19-H';
    END;
GO


