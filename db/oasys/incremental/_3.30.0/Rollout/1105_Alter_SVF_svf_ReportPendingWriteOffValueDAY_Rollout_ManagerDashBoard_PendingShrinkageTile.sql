Alter Function [dbo].[svf_ReportPendingWriteOffValueDAY]()
	Returns Numeric(9,2)
As
	Begin
		Declare @WOValue Numeric(9,2)
	Select
		@WOValue = Abs(Sum(SA.QUAN * SA.PRIC))
	From
		STKADJ As SA
			Inner Join
				STKMAS As SM
			On
				SA.SKUN = SM.SKUN
	Where
		SA.MOWT = 'W'
	And
		SA.WAUT = 0;

	Set @WOValue = IsNull(@WOValue, 0);
		
	Return @WOValue
End