-- =============================================
-- Author        : Alan Lewis
-- Create date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Create udf to determine whether a given
--				 : cashier has taken any sales for a given
--				 : banking period.
-- =============================================
Create Function [dbo].[udf_CashierHasTakenSaleOnDay]
(
	@CashierID As Int,
	@PeriodID  As Int = Null
)
Returns Bit
Begin
	Declare @CashierHasTakenSaleToday As Bit
	
	Set @CashierHasTakenSaleToday =
		CAST
			(
				(
					Case IsNull
						(
							(
								Select 
									NumTransactions
								From 
									CashBalCashier
								Where 
									(
										PeriodID    = @PeriodID
									Or
										(
											PeriodID    <> @PeriodID
										And
											@PeriodID Is Null
										)
									)
								 And   
									CurrencyID  = 
										(
											Select 
												ID 
											From 
												SystemCurrency 
											Where 
												IsDefault = 1
										)
							 And   
								CashierID   = @CashierID
							 And  
								(
									GrossSalesAmount <> 0 
								Or 
									(
										Select 
											Count(*)
										From 
											CashBalCashierTen
										Where 
											(
												PeriodID    = @PeriodID
											Or
												(
													PeriodID    <> @PeriodID
												And
													@PeriodID Is Null
												)
											)
										And   
											CurrencyID = 
												(
													Select 
														ID 
													From 
														SystemCurrency 
													Where 
														IsDefault = 1
												)
										And
											CashierID  = @CashierID
									) > 0
								)
							), 0)
						When 0 
							Then 0
						Else 
							1
						End
				) As Bit
			)
		
	Return @CashierHasTakenSaleToday
End
Go

If @@Error = 0
   Print 'Success: The user defined function "udf_CashierHasTakenSaleOnDay" has been sucessfully created for P022-ITNO19-I'
Else
   Print 'Failure: The user defined function "udf_CashierHasTakenSaleOnDay" might NOT have been created for P022-ITNO19-I'
Go
