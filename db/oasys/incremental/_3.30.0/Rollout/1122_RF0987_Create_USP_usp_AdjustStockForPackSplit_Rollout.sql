Create Procedure [dbo].[usp_AdjustStockForPackSplit]
	@Skun As Int,
	@Quantity As Int,
	@Value As Decimal(9,2)
As
	Begin
	
		Set NoCount On
		
		Update
			STKMAS
		Set
			ONHA = ONHA + @Quantity,
			MBSQ1 = MBSQ1 + @Quantity,
			MBSV1 = MBSV1 + @Value
		Where
			SKUN = @Skun
End
Go
If @@Error = 0
   Print 'Success: The new stored procedure "usp_AdjustStockForPackSplit" for RF0987 has been sucessfully created'
Else
   Print 'Failure: The new stored procedure "usp_AdjustStockForPackSplit" for RF0987 has NOT been created'
Go
--Apply Permissions

Grant Execute On usp_AdjustStockForPackSplit To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure usp_AdjustStockForPackSplit for RF0987 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure usp_AdjustStockForPackSplit for RF0987 might NOT have been successfully deployed';
    End;
Go

Grant Execute On usp_AdjustStockForPackSplit To [role_execproc];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure usp_AdjustStockForPackSplit for RF0987 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure usp_AdjustStockForPackSplit for RF0987 might NOT have been successfully deployed';
    End;
Go
