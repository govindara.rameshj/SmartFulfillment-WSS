CREATE PROCEDURE usp_GetRefundType @ReportDate AS date , 
                                  @TillId AS varchar( 2
                                                    ) , 
                                  @TranNo AS varchar( 4
                                                    )
AS
BEGIN
    SELECT COUNT( *
                )
      FROM
           DLLINE DL LEFT OUTER JOIN STKMAS IM
           ON IM.SKUN
              = 
              DL.SKUN
      WHERE DATE1
            = 
            @ReportDate
        AND TILL = @TillId
        AND [TRAN] = @TranNo
        AND QUAN > 0
        AND LREV = 0;

END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The Create Stored Procedure usp_GetRefundType for P022-030 has been deployed successfully';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The Create Stored Procedure usp_GetRefundType for P022-030 has not been deployed successfully';
    END;
--Apply Permissions

Grant Execute On usp_GetRefundType To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure usp_GetRefundType for P022-030 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure usp_GetRefundType for P022-030 might NOT have been successfully deployed';
    End;
Go

Grant Execute On usp_GetRefundType To [role_execproc];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure usp_GetRefundType for P022-030 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure usp_GetRefundType for P022-030 might NOT have been successfully deployed';
    End;
Go
