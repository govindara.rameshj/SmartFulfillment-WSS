DROP FUNCTION [dbo].[svf_ReportNonStockQty]
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportNonStockQty" has been sucessfully dropped for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportNonStockQty" has NOT been sucessfully dropped for P022-041';
    END;
GO
