
INSERT INTO PARAMETERS
VALUES( '-22030' , 
        'Enable Requirement P022-030' , 
        NULL , 
        NULL , 
        1 , 
        NULL , 
        3
      );
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The active requirement switch for P022-030 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: The active requirement switch for P022-030 might NOT have been deployed';
    END;
GO