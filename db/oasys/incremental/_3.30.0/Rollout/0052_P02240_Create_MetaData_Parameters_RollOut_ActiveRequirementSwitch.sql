insert into Parameters (ParameterID, Description, StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
                values (-22040, 'Enable requirement PO22-040', null, null, 1, null, 3)
go

if @@error = 0
   print 'Success: The metadata change for PO22-040 has been successfully deployed'
else
   print 'Failure: The metadata change for PO22-040 has NOT been successfully deployed'
go