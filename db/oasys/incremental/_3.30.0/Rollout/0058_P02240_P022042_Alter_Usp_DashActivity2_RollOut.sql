alter procedure DashActivity2
   @DateEnd date
as
begin
  set nocount on	
	
	Declare		@Table 
	Table		(
	               RowId Int,
				Description		varchar(50),
				Qty				int,
				QtyWtd			int,
				Value			dec(9,2),
				ValueWtd		dec(9,2),
				SelectedDate Date
				)

   declare @InputDate date,
           @StartDate date,
				@ColleagueDayQty		int,
				@ColleagueDayValue		dec(9,2),
				@ColleagueWeekQty		int,
				@ColleagueWeekValue		dec(9,2),
				@DuressDayQty			int,
				@DuressWeekQty			int,
				@GiftAllocateDayQty		int,
				@GiftAllocateDayValue	dec(9,2),
				@GiftAllocateWeekQty	int,
				@GiftAllocateWeekValue	dec(9,2),
				@GiftRedemDayQty		int,
				@GiftRedemDayValue		dec(9,2),
				@GiftRedemWeekQty		int,
				@GiftRedemWeekValue		dec(9,2),
				@MiscInDayQty			int,
				@MiscInDayValue			dec(9,2),
				@MiscInWeekQty			int,
				@MiscInWeekValue		dec(9,2),
				@MiscOutDayQty			int,
				@MiscOutDayValue		dec(9,2),
				@MiscOutWeekQty			int,
				@MiscOutWeekValue		dec(9,2)

   set @InputDate = @DateEnd

	Select
	@ColleagueDayQty		= Oasys.dbo.svf_ReportColleagueDiscountQtyDAY(@InputDate),		-- Retrieve Colleague Discount Quantity for DAY
	@ColleagueDayValue		= Oasys.dbo.svf_ReportColleagueDiscountValueDAY(@InputDate),	-- Retrieve Colleague Discount Value for DAY
	@ColleagueWeekQty		= Oasys.dbo.svf_ReportColleagueDiscountQtyWTD(@InputDate),		-- Retrieve Colleague Discount Quantity for WTD
	@ColleagueWeekValue		= Oasys.dbo.svf_ReportColleagueDiscountValueWTD(@InputDate),	-- Retrieve Colleague Discount Value for WTD

	@DuressDayQty			= Oasys.dbo.svf_ReportDuressCodeUsageDAY(@InputDate),			-- Retrieve Duress Code Usage for DAY
	@DuressWeekQty			= Oasys.dbo.svf_ReportDuressCodeUsageWTD(@InputDate),			-- Retrieve Duress Code Usage for WTD

	@MiscInDayQty			= Oasys.dbo.svf_ReportMiscIncomeQtyDAY(@InputDate),				-- Retrieve Misc Income Quantity for DAY
	@MiscInDayValue			= Oasys.dbo.svf_ReportMiscIncomeValueDAY(@InputDate),			-- Retrieve Misc Income Value for DAY
	@MiscInWeekQty			= Oasys.dbo.svf_ReportMiscIncomeQtyWTD(@InputDate),				-- Retrieve Misc Income Quantity for WTD
	@MiscInWeekValue		= Oasys.dbo.svf_ReportMiscIncomeValueWTD(@InputDate),			-- Retrieve Misc Income Value for WTD

	@MiscOutDayQty			= Oasys.dbo.svf_ReportMiscOutgoingQtyDAY(@InputDate),			-- Retrieve Misc Outgoing Quantity for DAY
	@MiscOutDayValue		= Oasys.dbo.svf_ReportMiscOutgoingValueDAY(@InputDate),			-- Retrieve Misc Outgoing Value for DAY
	@MiscOutWeekQty			= Oasys.dbo.svf_ReportMiscOutgoingQtyWTD(@InputDate),			-- Retrieve Misc Outgoing Quantity for WTD
	@MiscOutWeekValue		= Oasys.dbo.svf_ReportMiscOutgoingValueWTD(@InputDate),			-- Retrieve Misc Outgoing Value for WTD

	@GiftAllocateDayQty		= Oasys.dbo.svf_ReportGiftVoucherAllocationQtyDAY(@InputDate),	-- Retrieve Gift Voucher Allocation Qty for DAY	
	@GiftAllocateDayValue	= Oasys.dbo.svf_ReportGiftVoucherAllocationValueDAY(@InputDate),-- Retrieve Gift Voucher Allocation Value for DAY

	@GiftRedemDayQty		= Oasys.dbo.svf_ReportGiftVoucherRedemptionQtyDAY(@InputDate),	-- Retrieve Gift Voucher Redemption Qty for DAY
	@GiftRedemDayValue		= Oasys.dbo.svf_ReportGiftVoucherRedemptionValueDAY(@InputDate) * -1,-- Retrieve Gift Voucher Redemption Value for DAY
	@GiftAllocateWeekQty	= Oasys.dbo.svf_ReportGiftVoucherAllocationQtyWTD(@InputDate),	-- Retrieve Gift Voucher Allocation Qty for WTD	
	@GiftAllocateWeekValue	= Oasys.dbo.svf_ReportGiftVoucherAllocationValueWTD(@InputDate),-- Retrieve Gift Voucher Allocation Value for WTD
	@GiftRedemWeekQty		= Oasys.dbo.svf_ReportGiftVoucherRedemptionQtyWTD(@InputDate),	-- Retrieve Gift Voucher Redemption Qty for WTD
	@GiftRedemWeekValue		= Oasys.dbo.svf_ReportGiftVoucherRedemptionValueWTD(@InputDate) * -1-- Retrieve Gift Voucher Redemption Value for WTD

   insert into @Table                           values (1,'Colleague Discount',         @ColleagueDayQty,    @ColleagueWeekQty,    @ColleagueDayValue,    @ColleagueWeekValue,@DateEnd)
   insert into @Table(Rowid,Description, Qty, QtyWtd,SelectedDate) values (2,'Duress Code Used',           @DuressDayQty,       @DuressWeekQty,@DateEnd)
   insert into @Table                           values (3,'Gift Token Allocations',     @GiftAllocateDayQty, @GiftAllocateWeekQty, @GiftAllocateDayValue, @GiftAllocateWeekValue,@DateEnd)
   insert into @Table                           values (4,'Gift Token Redemptions',     @GiftRedemDayQty,    @GiftRedemWeekQty,    @GiftRedemDayValue,    @GiftRedemWeekValue,@DateEnd)
   insert into @Table                           values (5,'Miscellaneous Incomes',      @MiscInDayQty,       @MiscInWeekQty,       @MiscInDayValue,       @MiscInWeekValue,@DateEnd)
   insert into @Table                           values (6,'Miscellaneous Outgoings',    @MiscOutDayQty,      @MiscOutWeekQty,      @MiscOutDayValue,      @MiscOutWeekValue,@DateEnd)

   select * From @Table

end
go

if @@error = 0
   print 'Success: Stored procedure DashActivity2 for P022-040 and P022-042 has been successfully altered'
else
   print 'Failure: Stored procedure DashActivity2 for P022-040 and P022-042 has NOT been successfully altered'
go