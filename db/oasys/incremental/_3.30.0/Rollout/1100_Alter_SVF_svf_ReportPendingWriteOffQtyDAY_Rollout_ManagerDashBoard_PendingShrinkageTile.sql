Alter Function [dbo].[svf_ReportPendingWriteOffQtyDAY]()
	Returns Int
As
	Begin
		Declare @WOQty Int
		
		Select
			@WOQty =Abs(Sum(SA.QUAN))
		From
			STKADJ As SA
				Inner Join
					STKMAS As SM
				On
					SA.SKUN = SM.SKUN
		Where
			SA.MOWT = 'W'
		And
			SA.WAUT = 0;
					
	Set @WOQty = IsNull(@WOQty, 0);
		
	Return @WOQty
End