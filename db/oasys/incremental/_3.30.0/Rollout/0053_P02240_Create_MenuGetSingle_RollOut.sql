create procedure MenuGetSingle

   @MenuID int

as
begin

   set nocount on

   select Id            = ID,
          MasterId      = MasterID,
          [Description] = AppName,
          AssemblyName,
          ClassName,
          [LoadType]    = MenuType,
          [Parameters],
          ImagePath,
          IsMaximised   = LoadMaximised,
          IsModal,
          DisplayOrder  = DisplaySequence
   from MenuConfig
   where ID = @MenuID

end
go

if @@error = 0
   print 'Success: Stored procedure MenuGetSingle for P022-040 has been successfully deployed'
else
   print 'Failure: Stored procedure MenuGetSingle for P022-040 has NOT been successfully deployed'
go

------------------------------------------------------------------------------------------------------------
--Apply Permissions

grant execute on MenuGetSingle to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure MenuGetSingle for P022-040 has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure MenuGetSingle for P022-040 has NOT been successfully deployed'
go

Grant Execute On MenuGetSingle To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure MenuGetSingle for P022-040 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure MenuGetSingle for P022-040 might NOT have been successfully deployed';
    End;
Go
