CREATE FUNCTION [dbo].[udf_GetLatestPriceChangeDateForSku] 
(
	@SKU char(6)
)

RETURNS date
AS
BEGIN
	declare @StartDate date, @EndDate date, @ReturnDate date, @ActiveEventNumber char(6), 
		@CurrentEventNumber char(6), @DateInPast date = dateadd(day, 5, getdate())

	declare @PreviousValueOfDateInPast date

	select 
		@StartDate = SDAT, 
		@EndDate = EDAT, 
		@ActiveEventNumber = NUMB, 
		@CurrentEventNumber = NUMB
	from dbo.udf_EffectiveEVTCHGRecordForSkuOnDate(@sku, @DateInPast)

	-- find the most recently ended event for SKU, if one exists
	IF EXISTS(SELECT 1 FROM EVTCHG WHERE SKUN = @SKU AND EDAT <= GETDATE() AND EDAT IS NOT NULL)
		SELECT @ReturnDate = DATEADD(day, 1, MAX(EDAT))
		FROM EVTCHG 
		WHERE SKUN = @SKU 
			AND EDAT <= GETDATE() 
			AND EDAT IS NOT NULL
	
	-- If there is only one EVTCHG record, use its start date
	IF @ReturnDate IS NULL AND (SELECT COUNT(*) FROM EVTCHG WHERE SKUN = @SKU) = 1
		SELECT @ReturnDate = SDAT
		FROM EVTCHG 
		WHERE SKUN = @SKU 

	IF @ReturnDate IS NULL AND @EndDate IS NOT NULL
		-- If there is an end date then the current event is a temporary price so we can use the evtchg values directly
		set @ReturnDate = @StartDate
	
	IF @ReturnDate IS NULL
		begin
			while @ActiveEventNumber = @CurrentEventNumber
			begin
				set @PreviousValueOfDateInPast = @DateInPast
				set @DateInPast = dateadd(day, -1, @DateInPast)
				
				select 
					@StartDate = SDAT, 
					@CurrentEventNumber = NUMB
				from dbo.udf_EffectiveEVTCHGRecordForSkuOnDate(@sku, @DateInPast)
			end			
			
			set @ReturnDate = dateadd(day, 1, @DateInPast)
		end

	RETURN @ReturnDate

END



Go

grant execute on dbo.udf_GetLatestPriceChangeDateForSku to [NT AUTHORITY\IUSR]
grant execute on dbo.udf_GetLatestPriceChangeDateForSku to [role_execproc]

go

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_GetLatestPriceChangeDateForSku" has been sucessfully created';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_GetLatestPriceChangeDateForSku" might NOT have been created';
    END
GO

