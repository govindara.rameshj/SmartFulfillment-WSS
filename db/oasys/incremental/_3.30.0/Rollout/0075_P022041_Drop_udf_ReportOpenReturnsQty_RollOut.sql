DROP FUNCTION [dbo].[svf_ReportOpenReturnsQty]
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportOpenReturnsQty" has been sucessfully dropped for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportOpenReturnsQty" has NOT been sucessfully dropped for P022-041';
    END;
GO
