Create PROCEDURE [dbo].[usp_GetEffectiveDateForPriceChangeReport]
	@SkuNumber varchar(6),
	@EventNumber varchar(6),
	@Priority varchar(2)
AS
BEGIN

    SELECT 'StartDate' = dbo.udf_GetLatestPriceChangeDateForSKU(@SkuNumber)

END

go

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "usp_GetEffectiveDateForPriceChangeReport" has been sucessfully created';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "usp_GetEffectiveDateForPriceChangeReport" might NOT have been created';
    END
GO

Grant Execute On usp_GetEffectiveDateForPriceChangeReport To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure usp_GetEffectiveDateForPriceChangeReport for CR0110 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure usp_GetEffectiveDateForPriceChangeReport for CR0110 might NOT have been successfully deployed';
    End;
Go

Grant Execute On usp_GetEffectiveDateForPriceChangeReport To [role_execproc];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure usp_GetEffectiveDateForPriceChangeReport for CR0110 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure usp_GetEffectiveDateForPriceChangeReport for CR0110 might NOT have been successfully deployed';
    End;
Go
