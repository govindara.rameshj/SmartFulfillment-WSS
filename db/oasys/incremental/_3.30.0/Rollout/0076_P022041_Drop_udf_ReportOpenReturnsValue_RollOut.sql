DROP FUNCTION [dbo].[svf_ReportOpenReturnsValue]
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportOpenReturnsValue" has been sucessfully dropped for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportOpenReturnsValue" has NOT been sucessfully dropped for P022-041';
    END;
GO
