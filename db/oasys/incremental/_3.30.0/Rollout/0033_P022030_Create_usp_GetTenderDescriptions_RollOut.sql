CREATE PROCEDURE usp_GetTenderDescriptions
AS
BEGIN
    SELECT TTDE1 , 
           TTDE2 , 
           TTDE3 , 
           TTDE4 , 
           TTDE5 , 
           TTDE6 , 
           TTDE7 , 
           TTDE8 , 
           TTDE9 , 
           TTDE10
      FROM RETOPT
      WHERE FKEY = '01';
END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The Create Stored Procedure usp_GetTenderDescriptions for P022-030 has been deployed successfully';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The Create Stored Procedure usp_GetTenderDescriptions for P022-030 has not been deployed successfully';
    END;
--Apply Permissions

Grant Execute On usp_GetTenderDescriptions To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure usp_GetTenderDescriptions for P022-030 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure usp_GetTenderDescriptions for P022-030 might NOT have been successfully deployed';
    End;
Go

Grant Execute On usp_GetTenderDescriptions To [role_execproc];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_execproc] on stored procedure usp_GetTenderDescriptions for P022-030 has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_execproc] on stored procedure usp_GetTenderDescriptions for P022-030 might NOT have been successfully deployed';
    End;
Go
