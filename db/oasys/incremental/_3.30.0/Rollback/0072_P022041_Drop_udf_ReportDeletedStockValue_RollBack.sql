-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Deleted Stock Value - Scalar Valued Function
-- Notes		: This uses STKMAS:IDEL + IOBS value to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 8th July 2011
-- Description	: Changed return data type to decimal(9, 2) from int
--					Changed where clause from ONHA + MDNQ <> 0 to ONHA + MDNQ > 0
-- =================================================================================================
CREATE FUNCTION [dbo].[svf_ReportDeletedStockValue]()
RETURNS decimal(9,2) 
AS
BEGIN

	Declare		@DeletedValue		decimal(9,2)
		
	Set			@DeletedValue		=	
									(
									Select	SUM((ONHA + MDNQ) * PRIC)
									From	STKMAS
									Where	(IDEL	=	1 or IOBS = 1)
											and (ONHA + MDNQ) > 0
									)
	
	RETURN		@DeletedValue

END
GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportDeletedStockValue" has been sucessfully created for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportDeletedStockValue" might NOT have been created for P022-041';
    END;
GO