-- Verion Control Information
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Create Date	: 23/08/2010
-- Version		: 3.0.0.0
-- Description	: This procedure is used to get the activity data for the Managers Dashboard.
-- ==========================================================================================================
-- Version Revision(s)
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Alter Date	: 08/06/2011
-- Version		: 3.0.2.0
-- Notes		: Stored Procedure Re-design - uses scalar functions to retrieve data used in this report.
-- ==========================================================================================================
-- Author		: Sean Moir
-- Alter Date	: 7/7/2011
-- Version		: 3.0.3.0
-- Notes		: Change polarity of @GiftRedemWeekQty reported
-- ==========================================================================================================
-- Author		: Sean Moir
-- Alter Date	: 8/7/2011
-- Version		: 3.0.4.0
-- Notes		: @MiscOutWeekValue correctly reported - previously was giving @MiscInWeekValue
-- ==========================================================================================================
-- Author		: Sean Moir
-- Alter Date	: 8/7/2011
-- Version		: 3.0.5.0
-- Notes		: Change polarity of @GiftRedemWeekQty, @GiftRedemDayValue and @GiftRedemWeekValue reported
-- ==========================================================================================================
ALTER PROCEDURE [dbo].[DashActivity2]
	@DateEnd	date
as

Begin
	
	Set NOCOUNT On
	
	--------------------------------------------------------------------------------------------------------------
	-- Set Input Date as Date for Passing to SCALAR FUNCTIONS
	--------------------------------------------------------------------------------------------------------------
	Declare		@InputDate		date
	Set			@InputDate		=		@DateEnd
	
	--------------------------------------------------------------------------------------------------------------
	-- Create Temporary Table
	--------------------------------------------------------------------------------------------------------------
	Declare		@Table 
	Table		(
				Description		varchar(50),
				Qty				int,
				QtyWtd			int,
				Value			dec(9,2),
				ValueWtd		dec(9,2)
				);
				
	--------------------------------------------------------------------------------------------------------------
	-- Declare Variables Used for Report
	--------------------------------------------------------------------------------------------------------------
	Declare
				@StartDate				date,
				@ColleagueDayQty		int,
				@ColleagueDayValue		dec(9,2),
				@ColleagueWeekQty		int,
				@ColleagueWeekValue		dec(9,2),
				@CreditDayQty			int,
				@CreditDayValue			dec(9,2),
				@CreditWeekQty			int,
				@CreditWeekValue		dec(9,2),
				@DuressDayQty			int,
				@DuressWeekQty			int,
				@GiftAllocateDayQty		int,
				@GiftAllocateDayValue	dec(9,2),
				@GiftAllocateWeekQty	int,
				@GiftAllocateWeekValue	dec(9,2),
				@GiftRedemDayQty		int,
				@GiftRedemDayValue		dec(9,2),
				@GiftRedemWeekQty		int,
				@GiftRedemWeekValue		dec(9,2),
				@MiscInDayQty			int,
				@MiscInDayValue			dec(9,2),
				@MiscInWeekQty			int,
				@MiscInWeekValue		dec(9,2),
				@MiscOutDayQty			int,
				@MiscOutDayValue		dec(9,2),
				@MiscOutWeekQty			int,
				@MiscOutWeekValue		dec(9,2),
				@PendingDayQty			int,
				@PendingDayValue		dec(9,2),
				@ConfirmDayQty			int,
				@ConsignedDayQty		int,
				@ConsignedWeekQty		int,
				@RejectedDayQty			int,
				@RejectedDayValue		dec(9,2),
				@RejectedWeekQty		int,
				@RejectedWeekValue		dec(9,2);
	
	
	----------------------------------------------------------------------------------------------
	-- Set Variables from Scalar Functions
	----------------------------------------------------------------------------------------------
	Select
	-- Colleague Discount Data
	@ColleagueDayQty		= Oasys.dbo.svf_ReportColleagueDiscountQtyDAY(@InputDate),		-- Retrieve Colleague Discount Quantity for DAY
	@ColleagueDayValue		= Oasys.dbo.svf_ReportColleagueDiscountValueDAY(@InputDate),	-- Retrieve Colleague Discount Value for DAY
	@ColleagueWeekQty		= Oasys.dbo.svf_ReportColleagueDiscountQtyWTD(@InputDate),		-- Retrieve Colleague Discount Quantity for WTD
	@ColleagueWeekValue		= Oasys.dbo.svf_ReportColleagueDiscountValueWTD(@InputDate),	-- Retrieve Colleague Discount Value for WTD
	-- Duress Code Data
	@DuressDayQty			= Oasys.dbo.svf_ReportDuressCodeUsageDAY(@InputDate),			-- Retrieve Duress Code Usage for DAY
	@DuressWeekQty			= Oasys.dbo.svf_ReportDuressCodeUsageWTD(@InputDate),			-- Retrieve Duress Code Usage for WTD
	-- Miscellaneous Income Data
	@MiscInDayQty			= Oasys.dbo.svf_ReportMiscIncomeQtyDAY(@InputDate),				-- Retrieve Misc Income Quantity for DAY
	@MiscInDayValue			= Oasys.dbo.svf_ReportMiscIncomeValueDAY(@InputDate),			-- Retrieve Misc Income Value for DAY
	@MiscInWeekQty			= Oasys.dbo.svf_ReportMiscIncomeQtyWTD(@InputDate),				-- Retrieve Misc Income Quantity for WTD
	@MiscInWeekValue		= Oasys.dbo.svf_ReportMiscIncomeValueWTD(@InputDate),			-- Retrieve Misc Income Value for WTD
	-- Miscellaneous Outgoing Data
	@MiscOutDayQty			= Oasys.dbo.svf_ReportMiscOutgoingQtyDAY(@InputDate),			-- Retrieve Misc Outgoing Quantity for DAY
	@MiscOutDayValue		= Oasys.dbo.svf_ReportMiscOutgoingValueDAY(@InputDate),			-- Retrieve Misc Outgoing Value for DAY
	@MiscOutWeekQty			= Oasys.dbo.svf_ReportMiscOutgoingQtyWTD(@InputDate),			-- Retrieve Misc Outgoing Quantity for WTD
	@MiscOutWeekValue		= Oasys.dbo.svf_ReportMiscOutgoingValueWTD(@InputDate),			-- Retrieve Misc Outgoing Value for WTD
	-- Credit/ Debit Card Summary Data
	@CreditDayQty			= Oasys.dbo.svf_ReportCardSummaryQtyDAY(@InputDate),			-- Retrieve Card Summary Quantity for DAY	
	@CreditDayValue			= Oasys.dbo.svf_ReportCardSummaryValueDAY(@InputDate),			-- Retrieve Card Summary Value for DAY
	@CreditWeekQty			= Oasys.dbo.svf_ReportCardSummaryQtyWTD(@InputDate),			-- Retrieve Card Summary Quantity for WTD	
	@CreditWeekValue		= Oasys.dbo.svf_ReportCardSummaryValueWTD(@InputDate),			-- Retrieve Card Summary Value for WTD
	-- Gift Token Allocation / Redemption
	@GiftAllocateDayQty		= Oasys.dbo.svf_ReportGiftVoucherAllocationQtyDAY(@InputDate),	-- Retrieve Gift Voucher Allocation Qty for DAY	
	@GiftAllocateDayValue	= Oasys.dbo.svf_ReportGiftVoucherAllocationValueDAY(@InputDate),-- Retrieve Gift Voucher Allocation Value for DAY
	@GiftRedemDayQty		= Oasys.dbo.svf_ReportGiftVoucherRedemptionQtyDAY(@InputDate),	-- Retrieve Gift Voucher Redemption Qty for DAY
	@GiftRedemDayValue		= Oasys.dbo.svf_ReportGiftVoucherRedemptionValueDAY(@InputDate) * -1,-- Retrieve Gift Voucher Redemption Value for DAY
	@GiftAllocateWeekQty	= Oasys.dbo.svf_ReportGiftVoucherAllocationQtyWTD(@InputDate),	-- Retrieve Gift Voucher Allocation Qty for WTD	
	@GiftAllocateWeekValue	= Oasys.dbo.svf_ReportGiftVoucherAllocationValueWTD(@InputDate),-- Retrieve Gift Voucher Allocation Value for WTD
	@GiftRedemWeekQty		= Oasys.dbo.svf_ReportGiftVoucherRedemptionQtyWTD(@InputDate),	-- Retrieve Gift Voucher Redemption Qty for WTD
	@GiftRedemWeekValue		= Oasys.dbo.svf_ReportGiftVoucherRedemptionValueWTD(@InputDate) * -1,-- Retrieve Gift Voucher Redemption Value for WTD
	-- Pending Write-Off Data
	@PendingDayQty			= Oasys.dbo.svf_ReportPendingWriteOffQtyDAY(),					-- Retrieve Pending Write-Off Quantity for DAY
	@PendingDayValue		= Oasys.dbo.svf_ReportPendingWriteOffValueDAY(),				-- Retrieve Pending Write-Off Value for DAY
	-- Consignments Data
	@ConsignedDayQty		= Oasys.dbo.svf_ReportConsignmentsNotReceived(),				-- Retrieve Consignments Not Received
	@ConsignedWeekQty		= Oasys.dbo.svf_ReportConsignmentsNotReceived(),				-- Retrieve Consignments Not Received
	-- Rejected Orders
	@RejectedDayQty			= Oasys.dbo.svf_ReportRejectedOrdersQtyDAY(@InputDate),			-- Retrieve Rejected Orders Qty for DAY
	@RejectedDayValue		= Oasys.dbo.svf_ReportRejectedOrdersValueDAY(@InputDate),		-- Retrieve Rejected Orders Value for DAY
	@RejectedWeekQty		= Oasys.dbo.svf_ReportRejectedOrdersQtyWTD(@InputDate),			-- Retrieve Rejected Orders Qty for WTD
	@RejectedWeekValue		= Oasys.dbo.svf_ReportRejectedOrdersValueWTD(@InputDate),		-- Retrieve Rejected Orders Value for WTD
	-- Price Chnages Confirmed
	@ConfirmDayQty			= Oasys.dbo.svf_ReportPriceChangesConfirmedDAY(@InputDate)		-- Retrieve Price Changes Confirmed for DAY
	
	
	----------------------------------------------------------------------------------------------
	-- Insert Data into @Table
	----------------------------------------------------------------------------------------------
	Insert into @table values ('Colleague Discount', @ColleagueDayQty, @ColleagueWeekQty, @ColleagueDayValue, @ColleagueWeekValue) ;
	Insert into @table values ('Credit Card Summary', @CreditDayQty, @CreditWeekQty, @CreditDayValue, @CreditWeekValue) ;
	Insert into @table(Description, Qty, QtyWtd) values ('Duress Code Used', @DuressDayQty, @DuressWeekQty) ;
	Insert into @table values ('Gift Token Allocations', @GiftAllocateDayQty, @GiftAllocateWeekQty, @GiftAllocateDayValue, @GiftAllocateWeekValue) ;
	Insert into @table values ('Gift Token Redemptions', @GiftRedemDayQty, @GiftRedemWeekQty, @GiftRedemDayValue, @GiftRedemWeekValue) ;
	Insert into @table values ('Miscellaneous Incomes', @MiscInDayQty, @MiscInWeekQty, @MiscInDayValue, @MiscInWeekValue) ;
	Insert into @table values ('Miscellaneous Outgoings', @MiscOutDayQty, @MiscOutWeekQty, @MiscOutDayValue, @MiscOutWeekValue) ;
	Insert into @table(Description, Qty, Value) values ('Pending Write Offs', @PendingDayQty, @PendingDayValue) ;
	Insert into @table(Description, Qty) values ('Price Change Confirmations', @ConfirmDayQty) ;
	Insert into @table(Description, Qty, QtyWtd) values ('Orders Consigned but not Received', @ConsignedDayQty, @ConsignedWeekQty) ;
	Insert into @table values ('Rejected Orders', @RejectedDayQty, @RejectedWeekQty, @RejectedDayValue, @RejectedWeekValue) ;


	--------------------------------------------------------------------------------------------------------------
	-- Return Temporary Table for Report
	--------------------------------------------------------------------------------------------------------------
	Select * From @Table

End
go

if @@error = 0
   print 'Success: Stored procedure DashActivity2 for P022-040 has been successfully rolled back'
else
   print 'Failure: Stored procedure DashActivity2 for P022-040 has NOT been successfully rolled back'
go