-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Non Stock Value - Scalar Valued Function
-- Notes		: This uses STKMAS:INON value to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 8th July 2011
-- Description	: Changed where clause from ONHA + MDNQ <> 0 to ONHA + MDNQ > 0
-- =================================================================================================
CREATE FUNCTION [dbo].[svf_ReportNonStockValue]()
RETURNS numeric(9,2) 
AS
BEGIN

	Declare		@NSValue		numeric(9,2)
		
	Set			@NSValue		=	
									(
									Select	SUM((ONHA + MDNQ) * PRIC)
									From	STKMAS
									Where	(INON	=	1)
											and ONHA + MDNQ > 0
									)
	
	RETURN		@NSValue

END
GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportNonStockValue" has been sucessfully created for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportNonStockValue" might NOT have been created for P022-041';
    END;
GO