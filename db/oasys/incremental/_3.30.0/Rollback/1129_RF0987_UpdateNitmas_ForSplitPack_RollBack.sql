BEGIN
   UPDATE [Oasys].[dbo].[NITMAS]
   SET [NSET] = 1
      ,[TASK] = '150'
      ,[DESCR] = 'Related Items Update'
      ,[PROG] = 'RelatedItemsUpdate.exe'
      ,[NITE] = 1
      ,[RETY] = 1
      ,[WEEK] = 0
      ,[PEND] = 0
      ,[LIVE] = 1
      ,[OPTN] = 0
      ,[ABOR] = 1
      ,[JRUN] = ''
      ,[DAYS1] = 1
      ,[DAYS2] = 1
      ,[DAYS3] = 1
      ,[DAYS4] = 1
      ,[DAYS5] = 1
      ,[DAYS6] = 1
      ,[DAYS7] = 1
      ,[BEGD] = NULL
      ,[ENDD] = NULL
      ,[SDAT] = NULL
      ,[STIM] = NULL
      ,[EDAT] = NULL
      ,[ETIM] = NULL
      ,[ADAT] = NULL
      ,[ATIM] = NULL
      ,[TTYPE] = 0
 WHERE NSET='1' AND TASK ='150'
 if @@error = 0
    print 'Success: The metadata change to NITMAS for RF0987 has been successfully Rolled Back'
 else
    print 'Failure: The metadata change to NITMAS for RF0987 has NOT been successfully Rolled Back'

END
