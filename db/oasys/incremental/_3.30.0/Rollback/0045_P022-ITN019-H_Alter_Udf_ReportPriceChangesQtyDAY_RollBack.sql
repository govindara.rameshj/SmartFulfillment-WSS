
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 8th June 2011
-- Description	: Returns the figure to use for Price Changes Qty for TODAY - Scalar Valued Function
-- Notes		: This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION dbo.svf_ReportPriceChangesQtyDAY(
                                               )
RETURNS int
AS
BEGIN

    DECLARE
       @PCToday int;
                         
    ----------------------------------------------------------------------------------
    -- Retrieve Count for Todays Price Changes + Outstanding
    ----------------------------------------------------------------------------------
    SELECT @PCToday = COUNT( SKUN
                           )
      FROM PRCCHG
      WHERE PSTA = 'U'
        AND PDAT
            <= 
            GETDATE(
                   );

    SET @PCToday = ISNULL( @PCToday , 0
                         );

    RETURN @PCToday;

END;
GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportPriceChangesQtyDAY" has been sucessfully rolled back for P022-ITNO19-H';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportPriceChangesQtyDAY" might NOT have been rolled back for P022-ITNO19-H';
    END;
GO
