-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Pending Write-Off Value for DAY - Scalar Valued Function
-- Notes		: This uses STKADJ to draw data to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 2.0
-- Create date	: 7th July 2011
-- Description	: Changed value to use STKADJ.PRIC rather than STKMAS.PRIC
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportPendingWriteOffValueDAY]
(
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@WOValue			numeric(9,2)

					
	----------------------------------------------------------------------------------
	-- Get Pending Write-Off Value Value - DAY
	----------------------------------------------------------------------------------
	Select			@WOValue				=		abs(sum(SA.QUAN * SA.PRIC))
	From			STKADJ as SA			
	Inner Join		STKMAS as SM			on		SA.SKUN = SM.SKUN
	Where			SA.MOWT					=		'W'
					and SA.DAUT						is Null;

	Set				@WOValue				=		isnull(@WOValue, 0);
		
	RETURN			@WOValue

END
