---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Version Control Information
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Create date	: 23/08/2010
-- Version		: 3.0.0.0
-- Description	: This procedure is used to get the sales data for the Managers Dashboard.
-- ==========================================================================================================
-- Version Revision(s)
-- ==========================================================================================================
-- Author		: Partha Dutta
-- Alter Date	: 29/09/2010
-- Version		: 3.0.0.1
-- Notes		: Percentage statistics showing pound signs
-- ==========================================================================================================
-- Author		: Partha Dutta
-- Alter Date	: 15/10/2010
-- Version		: 3.0.1.0
-- Notes		: Some calculations are wrong because of NULL values not being dealth with correctly
-- ==========================================================================================================
-- Author		: Alan Lewis
-- Alter Date	: 03/11/2010
-- Version		: 3.0.1.1
-- Notes		: Set field 'Null' values to 0 so they get displayed on report as 0 rather than an empty cell.
--				: This moves isnull checking to earlier is proc, so removed from later.
--				: Corrected KB calc. sums so use 0 when field is null rather than the field value, i.e. NULL
--				: value (ie when and else calc.s were wrong way round.
--				: Correct 'Core Sales + K&B Sales' entries for week values - was calculating CoreWeek... then
--				: adding/subtracting ..Day... values instead of ...Week... values.
--				: Use DLLINE.IBAR = 1 for scanned weekly calc, as per the scanned daily calc.
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Alter Date	: 16/11/2010
-- Version		: 3.0.1.2
-- Notes		: Changed K&B Sales to look at new Vision Tables.
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Alter Date	: 24/11/2010
-- Version		: 3.0.1.3
-- Notes		: Added new K&B Deposits to look at DLTOTS Table.
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Alter Date	: 06/06/2011
-- Version		: 3.0.2.0
-- Notes		: Stored Procedure Re-design - uses scalar functions to retrieve data used in this report.
-- ==========================================================================================================
-- Author		: Sean Moir
-- Alter Date	: 07/07/2011
-- Version		: 3.0.2.1
-- Notes		: Corrected @CoreDayValue and @CoreWeekValue values reported.  
--					These were erroneously being updated instead of @CoreDayQty and @CoreWeekQty
-- ==========================================================================================================

ALTER PROCEDURE [dbo].[DashSales2]
	@DateEnd	date
as

Begin
    
    Set NOCOUNT On
    
    --------------------------------------------------------------------------------------------------------------
	-- Set Input Date as Date for Passing to SCALAR FUNCTIONS
	--------------------------------------------------------------------------------------------------------------
	Declare		@InputDate		date
	Set			@InputDate		=		@DateEnd

    --------------------------------------------------------------------------------------------------------------
	-- Create Temporary Table
	--------------------------------------------------------------------------------------------------------------
	Declare	@table table
					(
					[Description]        varchar(50),
                    Qty                  int,
                    QtyWtd	              int,
                    Value                dec(9,2),
                    ValueWtd             dec(9,2),
                    PercentageSalesValue dec(9,2),
		            PercentageWtdValue   dec(9,2)
		            );
	
	--------------------------------------------------------------------------------------------------------------
	-- Retrieve Values Used for Report
	--------------------------------------------------------------------------------------------------------------
	Declare			@CoreDayQty	      dec(9,2),		
					@CoreDayValue     dec(9,2),		
					@CoreWeekQty      dec(9,2),		
					@CoreWeekValue    dec(9,2),
					@RefundDayQty     dec(9,2),
					@RefundDayValue	  dec(9,2),
					@RefundDayValuePerc	  dec(9,2),
					@RefundWeekQty    dec(9,2),
					@RefundWeekValue  dec(9,2),
					@RefundWeekValuePerc  dec(9,2),
					@VoucherDayQty    int,
					@VoucherDayValue  dec(9,2),
					@VoucherWeekQty	  int,
					@VoucherWeekValue dec(9,2),
					@KbDayQty         int,
					@KbDayValue	      dec(9,2),
					@KbWeekQty        int,
					@KbWeekValue      dec(9,2),
					@LinesDay         dec(9,2),
					@LinesWeek        dec(9,2),
					@ScannedDay	      dec(9,2),
					@ScannedWeek      dec(9,2),
					@KbDepDayQty      int,
					@KbDepDayValue	  dec(9,2),
					@KbDepWeekQty     int,
					@KbDepWeekValue   dec(9,2);
	
	
	--------------------------------------------------------------------------------------------------------------
	-- Retrieve Values Used for Report
	--------------------------------------------------------------------------------------------------------------
	Select		
	-- Kitchen & Bathroom (K&B) DEPOSITS
	@KbDepDayQty		=	Oasys.dbo.svf_ReportKBDepositsQtyDAY(@InputDate),		-- Retrieve K&B Deposits Quantity for DAY
	@KbDepDayValue		=	Oasys.dbo.svf_ReportKBDepositsValueDAY(@InputDate),		-- Retrieve K&B Deposits Value for DAY
	@KbDepWeekValue		=	Oasys.dbo.svf_ReportKBDepositsValueWTD(@InputDate),		-- Retrieve K&B Deposits Value for WTD
	@KbDepWeekQty		=	Oasys.dbo.svf_ReportKBDepositsQtyWTD(@InputDate),		-- Retrieve K&B Deposits Quantity for WTD
	-- Kitchen and Bathroom (K&B) SALES
	@KbDayQty			=	Oasys.dbo.svf_ReportKBSalesQtyDAY(@InputDate),			-- Retrieve K&B Sales Quantity for DAY
	@KbDayValue			=	Oasys.dbo.svf_ReportKBSalesValueDAY(@InputDate),		-- Retrieve K&B Sales Value for DAY
	@KbWeekQty			=	Oasys.dbo.svf_ReportKBSalesQtyWTD(@InputDate),			-- Retrieve K&B Sales Quantity for WTD
	@KbWeekValue		=	Oasys.dbo.svf_ReportKBSalesValueWTD(@InputDate),		-- Retrieve K&B Sales Value for WTD
	-- Core SALES Information
	@CoreDayQty			=	Oasys.dbo.svf_ReportCoreSalesQtyDAY(@InputDate),		-- Retrieve Core Sales Qty for DAY
	@CoreDayValue		=	Oasys.dbo.svf_ReportCoreSalesValueDAY(@InputDate),		-- Retrieve Core Sales Value for DAY
	@CoreWeekQty		=	Oasys.dbo.svf_ReportCoreSalesQtyWTD(@InputDate),		-- Retrieve Core Sales Qty for WTD
	@CoreWeekValue		=	Oasys.dbo.svf_ReportCoreSalesValueWTD(@InputDate),		-- Retrieve Core Sales Value for WTD
	-- Core REFUND Information
	@RefundDayQty		=	Oasys.dbo.svf_ReportCoreRefundsQtyDAY(@InputDate),		-- Retrieve Core Refunds Quantity for DAY
	@RefundDayValue		=	Oasys.dbo.svf_ReportCoreRefundsValueDAY(@InputDate),	-- Retrieve Core Refunds Value for DAY
	@RefundWeekQty		=	Oasys.dbo.svf_ReportCoreRefundsQtyWTD(@InputDate),		-- Retrieve Core Refunds Quantity for WTD
	@RefundWeekValue	=	Oasys.dbo.svf_ReportCoreRefundsValueWTD(@InputDate),	-- Retrieve Core Refunds Value for WTD
	-- Core Sales VOUCHER Information
	@VoucherDayQty		=	Oasys.dbo.svf_ReportCoreVouchersQtyDAY(@InputDate),		-- Retrieve Core Vouchers Quantity for DAY	
	@VoucherDayValue	=	Oasys.dbo.svf_ReportCoreVouchersValueDAY(@InputDate),	-- Retrieve Core Vouchers Value for DAY
	@VoucherWeekQty		=	Oasys.dbo.svf_ReportCoreVouchersQtyWTD(@InputDate),		-- Retrieve Core Vouchers Quantity for WTD
	@VoucherWeekValue	=	Oasys.dbo.svf_ReportCoreVouchersValueWTD(@InputDate),	-- Retrieve Core Vouchers Value for WTD
	-- Core Sales SCANNING Information
	@ScannedDay			=	Oasys.dbo.svf_ReportCoreScanningQtyDAY(@InputDate),		-- Retrieve Core Scanned Lines Quantity DAY(used for Scanning Percentage)
	@ScannedWeek		=	Oasys.dbo.svf_ReportCoreScanningQtyWTD(@InputDate),		-- Retrieve Core Scanned Lines Quantity WTD(used for Scanning Percentage)
	@LinesDay			=	Oasys.dbo.svf_ReportCoreLinesSoldQtyDAY(@InputDate),	-- Retrieve Core Lines Sold Quantity DAY(used for Scanning Percentage)
	@LinesWeek			=	Oasys.dbo.svf_ReportCoreLinesSoldQtyWTD(@InputDate)		-- Retrieve Core Lines Sold Quantity WTD(used for Scanning Percentage)
		
	
	--------------------------------------------------------------------------------------------------------------
	-- Create Scanning Percentage & Refund Percentage to Sales Information
	--------------------------------------------------------------------------------------------------------------
	if @LinesDay	<> 0 set @LinesDay		= (@ScannedDay / @LinesDay) * 100			-- Calculate Scanned %'s allowing for divide by zero
	if @LinesWeek	<> 0 set @LinesWeek		= (@ScannedWeek /  @LinesWeek) * 100		-- Calculate Scanned %'s allowing for divide by zero
	if @RefundDayValue	<> 0 set @RefundDayValuePerc = ABS((@RefundDayValue / @CoreDayValue) * 100);		-- Calculate Refund %'s avoiding divide by zero	
	if @RefundWeekValue <> 0 set @RefundWeekValuePerc = ABS((@RefundWeekValue / @CoreWeekValue) * 100);	-- Calculate Refund %'s avoiding divide by zero
		
	--------------------------------------------------------------------------------------------------------------
	-- Insert Data into Temporary Table for Report
	--------------------------------------------------------------------------------------------------------------
	Insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Core Sales',						@CoreDayQty, @CoreWeekQty, @CoreDayValue, @CoreWeekValue) ;
    Insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Core Refunds',						@RefundDayQty, @RefundWeekQty, @RefundDayValue, @RefundWeekValue) ;
    Insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Voucher Value',					@VoucherDayQty, @VoucherWeekQty, @VoucherDayValue, @VoucherWeekValue) ;
    Insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Core Sales + Refund - Vouchers',	@CoreDayQty    + @RefundDayQty    - @VoucherDayQty,
																												@CoreWeekQty   + @RefundWeekQty   - @VoucherWeekQty,
																												@CoreDayValue  + @RefundDayValue  - @VoucherDayValue,
																												@CoreWeekValue + @RefundWeekValue - @VoucherWeekQty);
	Insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('K&B Sales',						@KbDayQty, @KbWeekQty, @KbDayValue, @KbWeekValue) ;
	Insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('K&B Deposits',						@KbDepDayQty, @KbDepWeekQty, @KbDepDayValue, @KbDepWeekValue) ;
	Insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Core Sales + K&B Sales',			@CoreDayQty    + @RefundDayQty   -  @VoucherDayQty    + @KbDayQty,
																												@CoreWeekQty   + @RefundWeekQty  -  @VoucherWeekQty   + @KbWeekQty,
																												@CoreDayValue  + @RefundDayValue -  @VoucherDayValue  + @KbDayValue,
																												@CoreWeekValue + @RefundWeekValue - @VoucherWeekValue + @KbWeekValue) ;
    Insert into @table([Description], PercentageSalesValue, PercentageWtdValue) values ('Percentage Lines Scanned', @LinesDay, @LinesWeek) ;
	Insert into @table([Description], PercentageSalesValue, PercentageWtdValue) values ('Percentage Refunds to Sales', @RefundDayValuePerc, @RefundWeekValuePerc) ;


	--------------------------------------------------------------------------------------------------------------
	-- Return Temporary Table for Report
	--------------------------------------------------------------------------------------------------------------
	Select * From @table
		
End
go

if @@error = 0
   print 'Success: Stored procedure DashSales2 for P022-040 has been successfully rolled back'
else
   print 'Failure: Stored procedure DashSales2 for P022-040 has NOT been successfully rolled back'
go