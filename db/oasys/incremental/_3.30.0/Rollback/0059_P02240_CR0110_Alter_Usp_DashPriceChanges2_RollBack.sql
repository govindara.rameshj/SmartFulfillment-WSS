-- Version Control Information
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Create date	: 08/06/2011
-- Version		: 3.0.2.0
-- Description	: This procedure is used to get the price change data for the Managers Dashboard.
-- ==========================================================================================================
-- Version Revision(s)
-- ==========================================================================================================
-- Author		: 
-- Alter Date	: 
-- Version		: 
-- Notes		: 
-- ==========================================================================================================

ALTER PROCEDURE [dbo].[DashPriceChanges2]
as

Begin
    
    Set NOCOUNT On
    
    ----------------------------------------------------------------------------------------------
	-- Create Temporary Table
	----------------------------------------------------------------------------------------------
	Declare		@Table 
	Table		(
				RowId			int, 
				Description		varchar(50), 
				Qty				int, 
				Peg				int, 
				Small			int, 
				Medium			int				
				);

	----------------------------------------------------------------------------------------------
	-- Declare Variables
	----------------------------------------------------------------------------------------------
	Declare		@PCToday			int,				
				@OutstandingQty		int,
				@OverDueQty          int,
				@Peg				int,
				@Small				int,
				@Medium				int
				
				
				
	
	----------------------------------------------------------------------------------------------
	-- Retrieve Values Used for Report
	----------------------------------------------------------------------------------------------
	Select
	@PCToday			= dbo.svf_ReportPriceChangesQtyDAY(),	
	@OutstandingQty	= dbo.svf_ReportPriceChangesOutstandingQty(),
	@OverDueQty         = dbo.svf_ReportPriceChangesOverdueQty(),
	@Peg				= dbo.svf_ReportPriceChangesLabelsRequiredPeg(),
	@Small			= dbo.svf_ReportPriceChangesLabelsRequiredSmall(),
	@Medium			= dbo.svf_ReportPriceChangesLabelsRequiredMedium()
	
	----------------------------------------------------------------------------------------------
	-- Insert Data into @Table
	----------------------------------------------------------------------------------------------	
     Insert into @Table values (1, 'Price Changes Waiting (Inc. AutoApply)', @OutstandingQty, null, null,null) ;
	Insert into @Table values (2, 'Price Changes Overdue (Inc. AutoApply)', @OverDueQty,null,null,null) ;
	Insert into @Table values (3, 'Labels Required', (@Peg + @Small + @Medium), @Peg, @Small, @Medium) ;
	Insert into @Table values (4, 'Price Changes Today',   @PCToday, null, null,null)
    
	----------------------------------------------------------------------------------------------
	-- Return Temporary Table for Report
	----------------------------------------------------------------------------------------------
	Select * From @Table
End

GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The stored Procedure "DashPriceChanges2" for CR0110 has been sucessfully deployed';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The stored Procedure "DashPriceChanges2" for CR0110 has NOT been deployed';
    END;
