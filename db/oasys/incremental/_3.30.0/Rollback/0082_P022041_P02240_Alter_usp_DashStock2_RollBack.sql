
-- Verion Control Information
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Create Date	: 23/08/2010
-- Version		: 3.0.0.0
-- Description	: This procedure is used to get the stock data for the Managers Dashboard.
-- ==========================================================================================================
-- Version Revision(s)
-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Alter Date	: 07/06/2011
-- Version		: 3.0.2.0
-- Notes		: Stored Procedure Re-design - uses scalar functions to retrieve data used in this report.
-- ==========================================================================================================
-- Author		: Sean Moir
-- Alter Date	: 08/07/2011
-- Version		: 3.0.3.0
-- Notes		: Changed sales figure to be based on svf_ReportNETSalesValuePrevious7Days rather than svf_ReportNETSalesValueWTD
-- ==========================================================================================================


ALTER PROCEDURE [dbo].[DashStock2]
	@DateEnd	date
as
Begin

	Set NOCOUNT On

	--------------------------------------------------------------------------------------------------------------
	-- Create Temporary Table
	--------------------------------------------------------------------------------------------------------------
	Declare			@table				table(Description varchar(50), Qty varchar(20), Value dec(9,2));
	
	--------------------------------------------------------------------------------------------------------------
	-- Declare Values Used for Report
	--------------------------------------------------------------------------------------------------------------
	Declare			@StockHolding		dec(9,2),
					@MarkdownQty		int,		
					@MarkdownValue		dec(9,2),
					@DeletedQty			int,
					@DeletedValue		dec(9,2),
					@NonStockQty		int,
					@NonStockValue		dec(9,2),
					@ReturnsQty			int,
					@ReturnsValue		dec(9,2),
					@Sales				dec(9,2),
					@ReturnsPercentage	dec(9,2),
					@OutStockQty		int,
					@BelowImpactQty		int;
	
	----------------------------------------------------------------------------------------------
	-- Set Variables from Scalar Functions
	----------------------------------------------------------------------------------------------
	Set	@StockHolding		= Oasys.dbo.svf_ReportStockHoldingValue()
	Set	@MarkdownQty		= Oasys.dbo.svf_ReportMarkdownStockQty()
	Set	@MarkdownValue		= Oasys.dbo.svf_ReportMarkdownStockValue()
	Set	@DeletedQty			= Oasys.dbo.svf_ReportDeletedStockQty()
	Set	@DeletedValue		= Oasys.dbo.svf_ReportDeletedStockValue()
	Set	@NonStockQty		= Oasys.dbo.svf_ReportNonStockQty()
	Set	@NonStockValue		= Oasys.dbo.svf_ReportNonStockValue()
	Set	@ReturnsQty			= ISNULL(Oasys.dbo.svf_ReportOpenReturnsQty(), 0)
	Set	@ReturnsValue		= Oasys.dbo.svf_ReportOpenReturnsValue()
	Set	@OutStockQty		= Oasys.dbo.svf_ReportOutOfStocks()
	Set	@BelowImpactQty		= Oasys.dbo.svf_ReportBelowImpacts()
		
	----------------------------------------------------------------------------------------------
	-- Get NET Sales for Returns % to Sales
	----------------------------------------------------------------------------------------------
	Set @Sales				= Oasys.dbo.svf_ReportNETSalesValuePrevious7Days(@DateEnd)
	Set @ReturnsPercentage	= (@Sales)
	If @ReturnsPercentage <> 0 Set @ReturnsPercentage = (@ReturnsValue / @Sales) * 100
	
	----------------------------------------------------------------------------------------------
	-- Insert Data into @Table
	----------------------------------------------------------------------------------------------
	insert into @table(Description, Value) values ('Stockholding', (@StockHolding));
	insert into @table(Description, Qty, Value) values ('Markdown Stockholding', (@MarkdownQty), (@MarkdownValue));
	insert into @table(Description, Qty, Value) values ('Deleted Stockholding', (@DeletedQty), (@DeletedValue));
	insert into @table(Description, Qty, Value) values ('Non-Stock Stockholding', (@NonStockQty) ,(@NonStockValue));
	insert into @table(Description, Qty, Value) values ('Open Returns Stockholding', (@ReturnsQty), (@ReturnsValue));
	insert into @table(Description, Qty) values ('Percentage Returns to Weekly Sales', (@ReturnsPercentage));
	insert into @table(Description, Qty) values ('Number Items Out of Stock', (@OutStockQty));
	insert into @table(Description, Qty) values ('Number Items Below Impact', (@BelowImpactQty));
	insert into @table(Description) values('N.B. (Live Figures) Valid at time of report');

	
	--------------------------------------------------------------------------------------------------------------
	-- Return Temporary Table for Report
	--------------------------------------------------------------------------------------------------------------
	Select * From @table

End
Go

If @@Error = 0
   Print 'Success: The stored procedure "DashStock2" has been sucessfully rolled back for P022-041'
Else
   Print 'Failure: The stored procedure "DashStock2" might NOT have been rolled back for P022-041'
Go
