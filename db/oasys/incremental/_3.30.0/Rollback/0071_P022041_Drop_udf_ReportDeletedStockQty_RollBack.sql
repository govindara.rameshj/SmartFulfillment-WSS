-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Deleted Stock Qty - Scalar Valued Function
-- Notes		: This uses STKMAS:IDEL + IOBS quantity to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 8th July 2011
-- Description	: Changed return data type to decimal(9, 2) from int
--					Changed where clause from ONHA + MDNQ <> 0 to ONHA + MDNQ > 0
-- =================================================================================================
CREATE FUNCTION [dbo].[svf_ReportDeletedStockQty]()
RETURNS int 
AS
BEGIN

	Declare		@DeletedQty		int
		
	Set			@DeletedQty		=	
									(
									Select	SUM(ONHA + MDNQ)
									From	STKMAS
									Where	(IDEL	=	1 or IOBS = 1)
											and (ONHA + MDNQ) > 0
									)
	
	RETURN		@DeletedQty

END
GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportDeletedStockQty" has been sucessfully created for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportDeletedStockQty" might NOT have been created for P022-041';
    END;
GO

