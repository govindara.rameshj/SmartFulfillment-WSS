Drop Procedure [dbo].[usp_AdjustRelatedItemsMarkup]
Go
If @@Error = 0
   Print 'Success: The stored procedure "usp_AdjustRelatedItemsMarkup" for RF0987 has been sucessfully dropped'
Else
   Print 'Failure: The stored procedure "usp_AdjustRelatedItemsMarkup" for RF0987 might NOT have been dropped'
Go