set nocount on

declare @Count int
declare @Output nvarchar(max)

------------------------------------------------------------------------------------------------------------------------
print 'Reorganise Dashboard'

begin try

   begin transaction

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Sales Panel'
   print @Output

   set @Count = 0

   update Report set Title    = 'Sales'			     where ID = 1; set @Count += @@rowcount
   update Report set MinWidth = 520, MinHeight = 245 where ID = 1; set @Count += @@rowcount

   delete ReportColumns   where ReportId = 1 and TableId  = 1 and ColumnId = 0; set @Count += @@rowcount

   delete ReportHyperlink where ReportId = 1 and TableId  = 1 and RowId = 2; set @Count += @@rowcount
   delete ReportHyperlink where ReportId = 1 and TableId  = 1 and RowId = 8; set @Count += @@rowcount

	Insert Into ReportColumn Values (6022, 'PercentageSalesValue', 'Sale %', 0, NULL, 0, 60, 60, 2); set @Count += @@rowcount
	Insert Into ReportColumn Values (6023, 'PercentageWtdValue', 'WTD %', 0, NULL, 0, 70, 70, 2); set @Count += @@rowcount
	update ReportColumn set MinWidth = 160, MaxWidth = Null                   where Id = 6024; set @Count += @@rowcount
	update ReportColumn set MinWidth =  40, MaxWidth =  40, Caption = 'Qty'   where Id = 6025; set @Count += @@rowcount
	update ReportColumn set MinWidth =  50, MaxWidth =  50, Caption = 'WTD'   where Id = 6026; set @Count += @@rowcount
	update ReportColumn set MinWidth =  60, MaxWidth =  60, Caption = 'Value', Alignment = 2 where Id = 6027; set @Count += @@rowcount
	update ReportColumn set MinWidth =  60, MaxWidth =  60, Caption = 'WTD',   Alignment = 2 where Id = 6028; set @Count += @@rowcount

   if @Count = 12
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 1)
   end
   
   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Shrinkage Panel'
   print @Output

   set @Count = 0

   update Report set MinWidth  = 420 where ID = 4; set @Count += @@rowcount
   update Report set MinHeight = 120 where ID = 4; set @Count += @@rowcount

   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(4, 1,  103, 2, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(4, 1, 1100, 3, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(4, 1, 1101, 4, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(4, 1, 1200, 5, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(4, 1, 1201, 6, 1, 0, null, null, null); set @Count += @@rowcount

   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId =    0; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 4010; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 4011; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 4012; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 4013; set @Count += @@rowcount
   delete ReportColumns where ReportId = 4 and TableId = 1 and ColumnId = 4014; set @Count += @@rowcount

   delete ReportColumn where Id = 4010; set @Count += @@rowcount
   delete ReportColumn where Id = 4011; set @Count += @@rowcount
   delete ReportColumn where Id = 4012; set @Count += @@rowcount
   delete ReportColumn where Id = 4013; set @Count += @@rowcount
   delete ReportColumn where Id = 4014; set @Count += @@rowcount

   if @Count = 18
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Banking Panel'
   print @Output

   set @Count = 0

   update Report set MinHeight = 100 where ID = 6; set @Count += @@rowcount
   update Report set MinWidth  = 300 where ID = 6; set @Count += @@rowcount

   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(6, 1,  103, 2, 1, 0, null, null, null); set @Count += @@rowcount

   delete ReportColumns where ReportId = 6 and TableId = 1 and ColumnId =    0; set @Count += @@rowcount
   delete ReportColumns where ReportId = 6 and TableId = 1 and ColumnId = 4020; set @Count += @@rowcount
   delete ReportColumns where ReportId = 6 and TableId = 1 and ColumnId = 4021; set @Count += @@rowcount
   delete ReportColumns where ReportId = 6 and TableId = 1 and ColumnId = 4022; set @Count += @@rowcount

   delete ReportColumn where Id = 4020; set @Count += @@rowcount
   delete ReportColumn where Id = 4021; set @Count += @@rowcount
   delete ReportColumn where Id = 4022; set @Count += @@rowcount

   if @Count = 10
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 5)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Deliveries Panel'
   print @Output

   set @Count = 0

   update Report set Title     = 'Deliveries' where ID = 5; set @Count += @@rowcount
   update Report set MinHeight = 120          where ID = 5; set @Count += @@rowcount
   update Report set MinWidth = 420           where ID = 5; set @Count += @@rowcount

   update ReportColumn set Alignment = 2 where Id = 1201; set @Count += @@rowcount
   
   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 6024; Set @Count += @@rowcount
   Insert Into ReportColumns Values (5,	1, 103, 2, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount
   
   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 6029; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 1100, 3, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 6030; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 1101, 4, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 6031; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 1200, 5, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   Delete ReportColumns Where ReportId = 5 And TableId = 1 And ColumnId = 6032; Set @Count += @@rowcount
   Insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) Values (5,	1, 1201, 6, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   Delete ReportColumn Where Id = 6029; set @Count += @@rowcount
   Delete ReportColumn Where Id = 6030; set @Count += @@rowcount
   Delete ReportColumn Where Id = 6031; set @Count += @@rowcount
   Delete ReportColumn Where Id = 6032; set @Count += @@rowcount

   if @Count = 18
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 2)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Customer Order Status Panel'
   print @Output

   set @Count = 0

   update Report set Title = 'Customer Order Status' where ID = 10; set @Count += @@rowcount

   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(10, 1, 1100, 0, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(10, 1, 1200, 0, 1, 0, null, null, null); set @Count += @@rowcount

   delete ReportColumns where ReportId = 10 and TableId = 1 and ColumnId = 4000; set @Count += @@rowcount
   delete ReportColumns where ReportId = 10 and TableId = 1 and ColumnId = 4001; set @Count += @@rowcount
   delete ReportColumns where ReportId = 10 and TableId = 1 and ColumnId = 4002; set @Count += @@rowcount
   delete ReportColumns where ReportId = 10 and TableId = 1 and ColumnId = 4003; set @Count += @@rowcount
   Delete ReportColumns where ReportId = 10 and TableId = 1 and ColumnId = 4004; set @Count += @@rowcount
   Insert Into ReportColumns Values (10, 1, 103, 0, 1, 0, NULL, NULL, NULL); Set @Count += @@rowcount

   delete ReportColumn where Id = 4000; set @Count += @@rowcount
   delete ReportColumn where Id = 4001; set @Count += @@rowcount
   delete ReportColumn where Id = 4002; set @Count += @@rowcount
   delete ReportColumn where Id = 4003; set @Count += @@rowcount
   delete ReportColumn where Id = 4004; set @Count += @@rowcount

   delete ReportParameters where ReportId = 10 and ParameterId = 103 and Sequence = 1; set @Count += @@rowcount

   update Report set MinWidth = 520, MinHeight = 220 where Id = 10; set @Count += @@rowcount

   if @Count = 16
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 3)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Pending Shrinkage Panel (NEW)'
   print @Output

   set @Count = 0

   delete ReportColumns where ReportId = 13 and TableId = 1 and ColumnId = 0;    set @Count += @@rowcount
   delete ReportColumns where ReportId = 13 and TableId = 1 and ColumnId = 4050; set @Count += @@rowcount
   delete ReportColumns where ReportId = 13 and TableId = 1 and ColumnId = 4051; set @Count += @@rowcount
   delete ReportColumns where ReportId = 13 and TableId = 1 and ColumnId = 4052; set @Count += @@rowcount
   delete ReportColumns where ReportId = 13 and TableId = 1 and ColumnId = 4053; set @Count += @@rowcount
   delete ReportColumns where ReportId = 13 and TableId = 1 and ColumnId = 4054; set @Count += @@rowcount

   delete ReportColumn where Id = 4050; set @Count += @@rowcount
   delete ReportColumn where Id = 4051; set @Count += @@rowcount
   delete ReportColumn where Id = 4052; set @Count += @@rowcount
   delete ReportColumn where Id = 4053; set @Count += @@rowcount
   delete ReportColumn where Id = 4054; set @Count += @@rowcount

   delete ReportTable where ReportId=13 and Id =1; set @Count += @@rowcount

   delete Report where Id=13; set @Count += @@rowcount

   if @Count = 13
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'DRL Analysis Panel'
   print @Output

   set @Count = 0

   update Report set MinWidth  = 420 where ID = 2; set @Count += @@rowcount
   update Report set MinHeight = 95  where ID = 2; set @Count += @@rowcount

   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(2, 1, 1200, 5, 1, 0, null, null, null); set @Count += @@rowcount

   delete ReportColumns where ReportId = 2 and TableId = 1 and ColumnId =    0; set @Count += @@rowcount
   delete ReportColumns where ReportId = 2 and TableId = 1 and ColumnId = 4040; set @Count += @@rowcount

   delete ReportColumn where Id = 4040; set @Count += @@rowcount

   if @Count = 6
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Stock Panel'
   print @Output

   set @Count = 0

   update Report set Title = 'Stock' where ID = 7; set @Count += @@rowcount
   update Report set MinWidth  = 300 where ID = 7; set @Count += @@rowcount

   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(7, 1, 1100, 3, 1, 0, null, null, null); set @Count += @@rowcount
   insert ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values(7, 1, 1200, 5, 1, 0, null, null, null); set @Count += @@rowcount

   delete ReportColumns where ReportId = 7 and TableId = 1 and ColumnId = 0;    set @Count += @@rowcount
   delete ReportColumns where ReportId = 7 and TableId = 1 and ColumnId = 4030; set @Count += @@rowcount
   delete ReportColumns where ReportId = 7 and TableId = 1 and ColumnId = 4031; set @Count += @@rowcount
   delete ReportColumns where ReportId = 7 and TableId = 1 and ColumnId = 4032; set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (7, 1, 103, 2, 1, 0, null, null, null); set @Count += @@rowcount

   delete ReportColumn where Id = 4030; set @Count += @@rowcount
   delete ReportColumn where Id = 4031; set @Count += @@rowcount
   delete ReportColumn where Id = 4032; set @Count += @@rowcount

   if @Count = 12
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 6)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Activity Report'
   print @Output

   set @Count = 0

   update Report set Title = 'Activity Reports' where ID = 9; set @Count += @@rowcount
   update Report set MinWidth  = 520            where ID = 9; set @Count += @@rowcount

   delete ReportColumns where ReportId = 9 and TableId = 1 and ColumnId =     0; set @Count += @@rowcount
   delete ReportColumns where ReportId = 9 and TableId = 1 and ColumnId =  4033; set @Count += @@rowcount
   delete ReportColumns where ReportId = 9 and TableId = 1 and ColumnId =  4034; set @Count += @@rowcount
   delete ReportColumns where ReportId = 9 and TableId = 1 and ColumnId =  4035; set @Count += @@rowcount
   
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (9, 1,  103, 2, 1, 0, NULL, NULL, NULL); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (9, 1, 1200, 5, 1, 0, NULL, NULL, NULL); set @Count += @@rowcount
   insert ReportColumns(ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold, IsHighlight, HighlightColour, Fontsize) values (9, 1, 1201, 6, 1, 0, NULL, NULL, NULL); set @Count += @@rowcount

   Delete ReportColumn Where Id = 4033; set @Count += @@rowcount
   Delete ReportColumn Where Id = 4034; set @Count += @@rowcount
   Delete ReportColumn Where Id = 4035; set @Count += @@rowcount

   if @Count = 12
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Price Change Report'
   print @Output

   set @Count = 0

   delete ReportParameters where ReportId = 8 and ParameterId = 103 and Sequence = 1; set @Count += @@rowcount

   update Report set MinHeight = 150, MinWidth = 420 where ID = 8; set @Count += @@rowcount

   if @Count = 2
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 9)
   end

   --------------------------------------------------------------------------------------------------------------------------
   set @Output = 'Configure Display & Sort Order'
   print @Output

   set @Count = 0

   update MenuConfig set Parameters = '1,4,6,2,7,5,9,3,12,11,10,8' where ID = 10110; set @Count += @@rowcount
   update MenuConfig set Parameters = '1,2,6,5,8,9,3,7,4,12'       where ID = 11010; set @Count += @@rowcount
   
   if @Count = 2
      print @Output + ': Success'
   else
   begin
      set @Output = @Output + ': Failure'
      print @Output
      raiserror(@Output, 15, 7)
   end

   commit transaction
   print 'Reorganise Dashboard: Successful'

end try

begin catch

   rollback transaction
   print 'Reorganise Dashboard: Unsuccessful'
   print 'Rollback unsuccessful'
   return

end catch

print 'Rollback successful'
