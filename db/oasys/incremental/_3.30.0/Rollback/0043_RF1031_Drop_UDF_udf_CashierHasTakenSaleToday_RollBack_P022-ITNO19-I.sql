-- =============================================
-- Author        : Alan Lewis
-- Create date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Drop new udf udf_CashierHasTakenSaleOnDay.
-- =============================================
Drop Function [dbo].[udf_CashierHasTakenSaleOnDay]
Go

If @@Error = 0
   Print 'Success: The user defined function "udf_CashierHasTakenSaleOnDay" has been sucessfully deleted for P022-ITNO19-I'
Else
   Print 'Failure: The user defined function "udf_CashierHasTakenSaleOnDay" might NOT have been deleted for P022-ITNO19-I'
Go
