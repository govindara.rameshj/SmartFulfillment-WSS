-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Pending Write-Off Qty for DAY - Scalar Valued Function
-- Notes		: This uses STKADJ to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportPendingWriteOffQtyDAY]
(
)
RETURNS int
AS
BEGIN

	Declare			@WOQty			int

					
	----------------------------------------------------------------------------------
	-- Get Pending Write-Off Qty - DAY
	----------------------------------------------------------------------------------
	Select			@WOQty					=		abs(sum(SA.QUAN))
	From			STKADJ as SA			
	Inner Join		STKMAS as SM			on		SA.SKUN = SM.SKUN
	Where			SA.MOWT					=		'W'
					and SA.DAUT						is Null;
					
	Set				@WOQty					=		isnull(@WOQty, 0);
		
	RETURN			@WOQty
END
