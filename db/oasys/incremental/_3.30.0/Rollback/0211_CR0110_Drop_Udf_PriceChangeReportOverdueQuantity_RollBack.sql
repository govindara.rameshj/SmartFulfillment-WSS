
DROP FUNCTION dbo.udf_PriceChangeReportOverdueQuantity;
GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The new function "udf_PriceChangeReportOverdueQuantity" for CR0110 has been sucessfully dropped';
    END
ELSE
    BEGIN
        PRINT 'Failure: The new function "udf_PriceChangeReportOverdueQuantity" for CR0110 has NOT been dropped';
    END;
GO

