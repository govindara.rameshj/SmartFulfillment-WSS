
DELETE FROM Parameters
  WHERE ParameterID
        = 
        980110;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The metadata change for CR00110 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: The metadata change for CR00110 has NOT been successfully deployed';
    END;
GO



