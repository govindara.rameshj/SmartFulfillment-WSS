-- =============================================
-- Author        : Alan Lewis
-- Create date   : 02/11/2012
-- User Story	 : 8783
-- Project		 : RF1094:  Till performing multiple refunds
-- Task Id		 : 8957
-- Description   : Drop requirement switch
-- =============================================
Delete
	Parameters
Where
	ParameterID = 981094
Go

If @@Error = 0
   Print 'Success: The active requirement switch for RF1094 has been successfully deleted'
Else
   Print 'Failure: active requirement switch for RF1094 might NOT have been deleted'
Go
