Drop Procedure [dbo].[usp_AdjustSinglesSoldNotUpdatedYet]
Go
If @@Error = 0
   Print 'Success: The stored procedure "usp_AdjustSinglesSoldNotUpdatedYet" for RF0987 has been sucessfully dropped'
Else
   Print 'Failure: The stored procedure "usp_AdjustSinglesSoldNotUpdatedYet" for RF0987 might NOT have been dropped'
Go