Drop Procedure [dbo].[usp_AdjustUnitsSoldInCurrentWeek]
Go
If @@Error = 0
   Print 'Success: The stored procedure "usp_AdjustUnitsSoldInCurrentWeek" for RF0987 has been sucessfully dropped'
Else
   Print 'Failure: The stored procedure "usp_AdjustUnitsSoldInCurrentWeek" for RF0987 might NOT have been dropped'
Go