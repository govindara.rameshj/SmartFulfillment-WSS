---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Alter Date	: 06/06/2011
-- Version		: 1.0.0.0
-- Notes		: Original version
-- ==========================================================================================================
-- Author		: Sean Moir
-- Alter Date	: 07/07/2011
-- Version		: 1.0.1.0
-- Notes		: Changed where clause to use DAUT instead of DATE1 for write offs
--				  This is to only show authorised write offs rather than all.  Uptrac has this behaviour
-- ==========================================================================================================
ALTER PROCEDURE [dbo].[DashShrinkage]
	@DateEnd	date
as
begin
	declare @table table(
		Description	varchar(50),
		Qty			int,
		QtyWtd		int,
		Value		dec(9,2),
		ValueWtd	dec(9,2));
	declare
		@StartDate			date,
		@StockLossDayQty	int,
		@StockLossDayValue	dec(9,2),		
		@StockLossWeekQty	int,		
		@StockLossWeekValue	dec(9,2),
		@KnownDayQty		int,
		@KnownDayValue		dec(9,2),		
		@KnownWeekQty		int,		
		@KnownWeekValue		dec(9,2),
		@WriteOffDayQty		int,
		@WriteOffDayValue	dec(9,2),		
		@WriteOffWeekQty	int,		
		@WriteOffWeekValue	dec(9,2);
	
	--set startdate to sunday before enddate
	set	@StartDate = @DateEnd;
	While datepart(weekday, @StartDate) <> 1
	begin
		set @StartDate = dateadd(day, -1, @StartDate)
	end	
	
	--get stock loss/known theft adjustment today
	select
		@StockLossDayQty	= sum(case sc.IsStockLoss when 1 then sa.quan else 0 end),	
	 	@StockLossDayValue	= sum(case sc.IsStockLoss when 1 then sa.quan * sa.pric else 0 end),
	 	@KnownDayQty		= sum(case sc.IsKnownTheft when 1 then sa.quan else 0 end),
	 	@KnownDayValue		= sum(case sc.IsKnownTheft when 1 then sa.quan * sa.pric else 0 end),
		@WriteOffDayQty		= sum(case sa.mowt when 'W' then sa.quan else 0 end)
	from	
		stkadj sa
	inner join
		sacode sc	on sc.numb = sa.code
	where	
		sa.DATE1	= @DateEnd
		
	-- get write offs adjustment today
	select
		@WriteOffDayQty		= isnull(sum(case sa.mowt when 'W' then sa.quan else 0 end), 0),	
	 	@WriteOffDayValue	= isnull(sum(case sa.mowt when 'W' then sa.quan * sa.pric else 0 end), 0)
	from	
		stkadj sa
	inner join
		sacode sc	on sc.numb = sa.code
	where	
		sa.DAUT = @DateEnd
	
	--get stock loss/known adjustment week
	select
		@StockLossWeekQty	= sum(case sc.IsStockLoss when 1 then sa.quan else 0 end),	
	 	@StockLossWeekValue	= sum(case sc.IsStockLoss when 1 then sa.quan * sa.pric else 0  end),
	 	@KnownWeekQty		= sum(case sc.IsKnownTheft when 1 then sa.quan else 0 end),
	 	@KnownWeekValue		= sum(case sc.IsKnownTheft when 1 then sa.quan * sa.pric else 0 end)
	from	
		stkadj sa
	inner join
		sacode sc	on sc.numb = sa.code /*and sc.IsStockLoss = 1*/
	where	
		sa.DATE1		<= @DateEnd
		and sa.DATE1	>= @StartDate		
	
	--get write offs adjustment week
	select
		@WriteOffWeekQty	= isnull(sum(case sa.MOWT when 'W' then sa.quan else 0 end), 0),	
	 	@WriteOffWeekValue	= isnull(sum(case sa.MOWT when 'W' then sa.quan * sa.pric else 0 end), 0)
	from	
		stkadj sa
	inner join
		sacode sc	on sc.numb = sa.code /*and sc.IsStockLoss = 1*/
	where	
		sa.DAUT <= @DateEnd
		and sa.DAUT >= @StartDate	

	--return values
	insert into @table values ('Stock Loss', @StockLossDayQty, @StockLossWeekQty, @StockLossDayValue, @StockLossWeekValue) ;
	insert into @table values ('Known Theft', @KnownDayQty, @KnownWeekQty, @KnownDayValue, @KnownWeekValue) ;
	insert into @table values ('Write Off Adjustments', @WriteOffDayQty, @WriteOffWeekQty, @WriteOffDayValue, @WriteOffWeekValue) ;

	select * from @table

end
go

if @@error = 0
   print 'Success: Stored procedure [DashShrinkage] for P022-040 has been successfully rolled back'
else
   print 'Failure: Stored procedure [DashShrinkage] for P022-040 has NOT been successfully rolled back'
go