DROP FUNCTION [dbo].[udf_EffectiveEVTCHGRecordsForSkuInDateRange]

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_EffectiveEVTCHGRecordsForSkuInDateRange" has been sucessfully dropped';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_EffectiveEVTCHGRecordsForSkuInDateRange" might NOT have been dropped';
    END
GO