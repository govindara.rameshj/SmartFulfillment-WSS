
DROP PROCEDURE usp_GetTransactionLines;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The Stored Procedure usp_GetTransactionLines for P022-030 has been successfully rolled back';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The Stored Procedure usp_GetTransactionLines for P022-030 has not been successfully rolled back';
    END;


