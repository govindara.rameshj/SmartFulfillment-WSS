BEGIN TRANSACTION SpotCheckTransaction
BEGIN TRY
	DECLARE @MenuID int = 3150
	DECLARE @MasterID int = 3100
	
	DECLARE @DisplaySequence int
	SELECT @DisplaySequence = MAX(DisplaySequence) + 1 FROM dbo.MenuConfig WHERE MasterID = @MasterID

	IF NOT EXISTS (SELECT 1 FROM dbo.MenuConfig WHERE ID = @MenuID)
	BEGIN
		INSERT INTO dbo.MenuConfig (
				ID,
				MasterID,
				AppName,
				AssemblyName,
				ClassName,
				MenuType,
				[Parameters],
				ImagePath,
				LoadMaximised,
				IsModal,
				DisplaySequence,
				AllowMultiple,
				WaitForExit,
				TabName,
				[Description],
				ImageKey,
				[Timeout],
				DoProcessingType)
		VALUES (@MenuID, @MasterID, 'Spot Check', 'NewBanking.Form.dll', 'NewBanking.Form.SpotCheck', 4, NULL, 'icons\icon_g.ico', 0, 1, @DisplaySequence, 0, 0, NULL, NULL, 0, 0, 1)
	END

	IF NOT EXISTS (SELECT 1 FROM dbo.ProfilemenuAccess WHERE MenuConfigID = @MenuID)
	BEGIN	
		INSERT INTO	dbo.ProfilemenuAccess (
			[ID],
			[MenuConfigID],
			[AccessAllowed],
			[OverrideAllowed],
			[SecurityLevel],
			[IsManager],
			[IsSupervisor],
			[LastEdited],
			[Parameters]
			)
		SELECT 
				[ID],
				@MenuID AS [MenuConfigID],
				[AccessAllowed],
				[OverrideAllowed],
				[SecurityLevel],
				[IsManager],
				[IsSupervisor],
				[LastEdited],
				[Parameters]
		FROM dbo.ProfilemenuAccess AS PA
		WHERE PA.MenuConfigID = 3010 -- Entry of Banking
			AND EXISTS(SELECT * FROM dbo.SecurityProfile AS SP WHERE SP.ID = PA.ID)
	END

	COMMIT TRANSACTION SpotCheckTransaction
	Print 'Success: Scripts for US10647 has been deployed successfully'
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION SpotCheckTransaction
	Print 'Failure: Scripts for US10647 has not been deployed successfully'
END CATCH