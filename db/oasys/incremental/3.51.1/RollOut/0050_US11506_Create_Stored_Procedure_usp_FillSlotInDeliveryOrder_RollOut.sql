IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'usp_FillSlotInDeliveryOrder') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure usp_FillSlotInDeliveryOrder'
	EXEC ('CREATE PROCEDURE dbo.usp_FillSlotInDeliveryOrder AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure usp_FillSlotInDeliveryOrder')
GO

ALTER PROCEDURE [dbo].[usp_FillSlotInDeliveryOrder]
   @OrderNumber char(6),
   @DeliveryDate Date = NULL
AS
BEGIN
SET NOCOUNT ON
--    1 - Sunday
--    2 - Monday
--    3 - Tuesday
--    4 - Wednesday
--    5 - Thursday
--    6 - Friday
--    7 - Saturday
DECLARE
	@SlotId int,
	@SlotDescription varchar(50),	
	@SlotStartTime time,
	@SlotEndTime time
	
	IF @DeliveryDate IS NOT NULL 
		BEGIN
			IF (DATEPART(weekday, @DeliveryDate) + @@datefirst) % 7 = 0 
				BEGIN
					SELECT @SlotId = [LongValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 57
					SELECT @SlotDescription = [StringValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 58	
					SELECT @SlotStartTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 59
					SELECT @SlotEndTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 60	
				END
			ELSE
				BEGIN
					SELECT @SlotId = [LongValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 53
					SELECT @SlotDescription = [StringValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 54	
					SELECT @SlotStartTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 55
					SELECT @SlotEndTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 56	
				END	
			
			UPDATE dbo.CORHDR
			SET [RequiredDeliverySlotID] = @SlotId,
				[RequiredDeliverySlotDescription] = @SlotDescription,
				[RequiredDeliverySlotStartTime] = @SlotStartTime,
				[RequiredDeliverySlotEndTime] = @SlotEndTime
			WHERE NUMB = @OrderNumber  AND DELI = 1
		END
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure usp_FillSlotInDeliveryOrder for US11506 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure usp_FillSlotInDeliveryOrder for US11506 has not been deployed'
GO

--Apply Permissions

Grant Execute On dbo.usp_FillSlotInDeliveryOrder To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure dbo.usp_FillSlotInDeliveryOrder has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure dbo.usp_FillSlotInDeliveryOrder might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on dbo.usp_FillSlotInDeliveryOrder to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure dbo.usp_FillSlotInDeliveryOrder has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure dbo.usp_FillSlotInDeliveryOrder has NOT been successfully deployed'
go
