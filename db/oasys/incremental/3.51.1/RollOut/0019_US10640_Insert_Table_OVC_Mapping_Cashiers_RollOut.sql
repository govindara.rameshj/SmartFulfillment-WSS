INSERT INTO [dbo].[OVC_Mapping_Cashiers]
           ([OVC_TillID]
           ,[Mapping_CashierID])
SELECT t.[OVC_TillID], t.[Mapping_CashierID]
FROM (            
		SELECT 1 AS [OVC_TillID], 401 AS [Mapping_CashierID]
		UNION ALL SELECT 2, 402
		UNION ALL SELECT 3, 403
		UNION ALL SELECT 4, 404
		UNION ALL SELECT 5, 405
		UNION ALL SELECT 6, 406
		UNION ALL SELECT 7, 407
		UNION ALL SELECT 8, 408
		UNION ALL SELECT 9, 409
		UNION ALL SELECT 10, 410
		UNION ALL SELECT 11, 411
		UNION ALL SELECT 12, 412
		UNION ALL SELECT 65, 465
		UNION ALL SELECT 66, 466
		UNION ALL SELECT 67, 467
		UNION ALL SELECT 68, 468
		UNION ALL SELECT 69, 469
	) t	
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[OVC_Mapping_Cashiers] omc WHERE omc.[OVC_TillID] = t.[OVC_TillID] and omc.[Mapping_CashierID] = t.[Mapping_CashierID])
GO

If @@Error = 0
   Print 'Success: Insert Table OVC_Mapping_Cashiers for US10640 has been deployed successfully'
Else
   Print 'Failure: Insert Table OVC_Mapping_Cashiers for US10640 has not been deployed successfully'
Go