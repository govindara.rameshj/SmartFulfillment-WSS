IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'GetDeliverySlots') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure GetDeliverySlots'
	EXEC ('CREATE PROCEDURE dbo.GetDeliverySlots AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure GetDeliverySlots')
GO

ALTER PROCEDURE [dbo].[GetDeliverySlots]
AS
BEGIN
	SET NOCOUNT ON
	select
		cast(p1.LongValue as int) [Id],
		p2.StringValue [Description],
		cast(p3.StringValue as time) [StartTime],
		cast(p4.StringValue as time) [EndTime]
	from dbo.Parameters p1
	inner join dbo.Parameters p2 on p2.ParameterID = 54
	inner join dbo.Parameters p3 on p3.ParameterID = 55
	inner join dbo.Parameters p4 on p4.ParameterID = 56
	where p1.ParameterID = 53

	union all

	select
		cast(p1.LongValue as int),
		p2.StringValue,
		cast(p3.StringValue as time),
		cast(p4.StringValue as time)
	from dbo.Parameters p1
	inner join dbo.Parameters p2 on p2.ParameterID = 58
	inner join dbo.Parameters p3 on p3.ParameterID = 59
	inner join dbo.Parameters p4 on p4.ParameterID = 60
	where p1.ParameterID = 57
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure GetDeliverySlots for US11509 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure GetDeliverySlots for US11509 has not been deployed'
GO

--Apply Permissions

Grant Execute On dbo.GetDeliverySlots To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure dbo.GetDeliverySlots has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure dbo.GetDeliverySlots might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on dbo.GetDeliverySlots to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure dbo.GetDeliverySlots has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure dbo.GetDeliverySlots has NOT been successfully deployed'
go
