IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'[SalesTransactionGet]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure [SalesTransactionGet]'
	EXEC ('CREATE PROCEDURE dbo.[SalesTransactionGet] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [SalesTransactionGet]')
GO

ALTER procedure [dbo].[SalesTransactionGet]
	@Date			date,
	@TillNumber		int = null,
	@TranNumber		int = null,
	@OVCTranNumber	varchar(20) = null,
	@SkuNumber		char(6) = null,
	@TenderType		char(2) = null,
	@Amount			char(9) = null
as
BEGIN

IF @TillNumber = 0
	SET @TillNumber = NULL

IF @TranNumber = 0 
	SET @TranNumber = NULL

IF LTRIM(@SkuNumber) = '' 
	SET @SkuNumber = NULL
	
IF LTRIM(@Amount) = '' 
	SET @Amount = NULL

CREATE TABLE #Table (
	DATE1 date, 
	[TRAN] int, 
	TILL int,
	OVCTranNumber varchar(20)  
	PRIMARY KEY	([DATE1], [TILL], [TRAN]))

	INSERT INTO #Table (DATE1, [TRAN], TILL, OVCTranNumber) 
		SELECT DATE1, DT.[TRAN], DT.TILL, DT.OVCTranNumber 
		FROM dbo.vwDLTOTS AS DT
		WHERE 
			DT.DATE1 = @Date
			AND (@TillNumber is null or @TillNumber = 0 or DT.TILL = @TillNumber)
			AND (@TranNumber is null or @TranNumber = 0 or DT.[TRAN] = @TranNumber)
			AND (@OVCTranNumber is null or RTRIM(@OVCTranNumber) = '' or DT.OVCTranNumber = @OVCTranNumber)
			AND (@Amount is null or DT.TOTL = CONVERT(DECIMAL(9,2), @Amount))
			AND (@SkuNumber is null or EXISTS (SELECT 1 FROM DLLINE WHERE SKUN = @SkuNumber And DATE1 = DT.DATE1 And [TRAN] = DT.[TRAN] And TILL = DT.TILL))
			AND (@TenderType IS NULL or  EXISTS (SELECT 1 FROM dbo.DLPAID WHERE [TRAN] = DT.[TRAN] AND TILL = DT.TILL AND ([TYPE] = CONVERT(DECIMAL(3,0), @TenderType)OR (@TenderType = '3' AND [TYPE] IN (3, 8, 9))) AND DATE1 = @Date))

IF (EXISTS (SELECT 1 FROM #Table))
BEGIN
	select
		dt.DATE1		 as 'Date',
		dt.TILL			 as 'TillId',
		dt.[TRAN]		 as 'TranNumber',
		dt.OVCTranNumber as 'OVCTranNumber',
		dt.[TIME]		 as 'Time',
		dt.CASH			 as 'CashierId',
		coalesce(su.Name, 'Unknown')	as 'CashierName',
		dt.TOTL			 as 'Value',
		dt.TAXA			 as 'ValueTax',
		'Type' = 
			case 
				when 
					(dt.TCOD = 'RF' AND EXISTS (SELECT 1 FROM DLLINE WHERE DATE1 = @Date AND TILL = dt.TILL AND [TRAN] = dt.[TRAN] AND QUAN > 0 AND LREV = 0)) 
					OR (dt.TCOD = 'SA' AND EXISTS (SELECT 1 FROM DLLINE WHERE DATE1 = @Date AND TILL = dt.TILL AND [TRAN] = dt.[TRAN] AND QUAN < 0 AND LREV = 0) AND EXISTS (SELECT 1 FROM DLLINE WHERE DATE1 = @Date AND TILL = dt.TILL AND [TRAN] = dt.[TRAN] AND QUAN > 0 AND LREV = 0)) then 'Exchange'
				when dt.TCOD = 'RF' OR (dt.TCOD = 'SA' AND NOT EXISTS (SELECT 1 FROM DLLINE WHERE DATE1 = @Date AND TILL = dt.TILL AND [TRAN] = dt.[TRAN] AND QUAN > 0 AND LREV = 0)) then 'Refund'
				when dt.TCOD = 'SA' then 'Sale'
				when dt.TCOD = 'SC' then 'Sale Correction'
				when dt.TCOD = 'RC' then 'Refund Correction'
				when dt.TCOD = 'CO' then 'Sign On'
				when dt.TCOD = 'CC' then 'Sign Off'
				when dt.TCOD = 'M+' then 'Misc Income'
				when dt.TCOD = 'M-' then 'Misc Outgoings'
				when dt.TCOD = 'C+' then 'Misc Income Correction'
				when dt.TCOD = 'C-' then 'Misc Outgoings Correction'
				when dt.TCOD = 'OD' then 'Open Drawer'
				when dt.TCOD = 'RL' then 'Reprint Logo'
				when dt.TCOD = 'XR' then 'X-Read'
				when dt.TCOD = 'ZR' then 'Z-Read'
			end,
		dt.VSUP			 as 'SupervisorIdVoided',
		dt.VOID			 as 'Void',
		dt.TMOD			 as 'IsTraining',
		dt.SUSE			 as 'IsSupervisorUsed',
		dt.[PROC]		 as 'UpdatedProc',
		DocumentNumber = case dt.DOCN
							when '00000000' then ''
							else dt.DOCN
						 end,
		dt.MERC			 as 'ValueMerchandise',
		dt.DISC			 as 'ValueDiscount',
		dt.ACCN			 as 'CustomerNumber',
		dt.ICOM			 as 'IsComplete',
		dt.VATR1		 as 'VatRate1',
		dt.XVAT1		 as 'ValueExVat1',
		dt.PARK			 as 'IsParked',
		dt.REMO			 as 'IsOffline',
		dt.CARD_NO		 as 'ColleagueCard'
	from dbo.vwDLTOTS dt
	inner join #Table t on t.DATE1 = DT.DATE1 AND t.TILL = DT.TILL AND t.[TRAN] = DT.[TRAN] 	
	left join dbo.SystemUsers su on su.ID = dt.CASH
	order by dt.TILL, dt.[TRAN] desc	

	select
		dl.DATE1		 as 'Date',
		dl.TILL			 as 'TillId',
		dl.[TRAN]		 as 'TranNumber',
		dl.NUMB			 as 'LineNumber',
		dl.SKUN			 as 'SkuNumber',
		sm.DESCR		 as 'Description',
		dl.QUAN			 as 'Qty',
		dl.PRIC			 as 'Price',
		dl.EXTP			 as 'ValueExtended',
		dl.IBAR			 as 'IsScanned',
		dl.SUPV			 as 'SupervisorId',
		dl.PORC			 as 'ReasonCode',
		dl.LREV			 as 'IsReversed',
		dl.VATV			 as 'ValueVat',
		case dl.SALT
			when 'B' then 'Basic Item'
			when 'P' then 'Performance Item'
			when 'A' then 'Aesthetic Item'
			when 'S' then 'Showroom Item'
			when 'G' then 'Good Pallet Item'
			when 'R' then 'Reduced Pallet Item'
			when 'D' then 'Delivery Item'
			when 'I' then 'Installation Item'
			when 'W' then 'WEEE Item'
			else 'Other'
		end				 as 'SaleType'
	from dbo.DLLINE dl
	inner join #Table t on t.DATE1 = DL.DATE1 AND t.TILL = DL.TILL AND t.[TRAN] = DL.[TRAN] 	
	left join dbo.STKMAS sm on sm.SKUN = dl.SKUN
	order by dl.DATE1,dl.TILL,dl.[TRAN] desc, dl.NUMB
	
	select
		dp.DATE1	as 'Date',
		dp.TILL		as 'TillId',
		dp.[TRAN]	as 'TranNumber',
		dp.NUMB		as 'Sequence',
		dp.[TYPE]	as 'Type',
		tc.TTDE		as 'Description',
		dp.AMNT		as 'Value',
		dp.[CARD]	as 'CardNumber',
		dp.SUPV		as 'SupervisorId',
		dp.CTYP		as 'CardType',
		dp.COPN		as 'CouponNumber'
	from dbo.DLPAID dp
	inner join #Table t on t.DATE1 = DP.DATE1 AND t.TILL = DP.TILL AND t.[TRAN] = DP.[TRAN]
	left join (select TTID, TTDE from dbo.TENCTL WHERE IUSE = 1 AND TTID NOT IN (8, 9) UNION select TenderID, DisplayText FROM SystemCurrencyDen WHERE TenderID IN (8, 9, 10, 12)) tc on tc.TTID = dp.[TYPE]
	order by dp.DATE1, dp.TILL, dp.[TRAN] desc,dp.NUMB
		
	SELECT
		dc.DATE1		 as 'Date',
		dc.TILL			 as 'TillId',
		dc.[TRAN]		 as 'TranNumber',
		dt.OVCTranNumber as 'OVCTranNumber',
		dc.ODAT			 as 'OriginalDate',
		dc.OTIL			 as 'OriginalTillId',
		dc.OTRN			 as 'OriginalTranNumber',
		dt1.OVCTranNumber as 'OriginalOVCTranNumber'
	from dbo.DLRCUS dc
	inner join #Table t on t.DATE1 = DC.DATE1 AND t.TILL = DC.TILL AND t.[TRAN] = DC.[TRAN]
	inner join vwDLTOTS dt on dt.DATE1 = DC.DATE1 AND dt.TILL = DC.TILL AND dt.[TRAN] = DC.[TRAN]
	inner join vwDLTOTS dt1 on dt1.DATE1 = DC.ODAT AND dt1.TILL = DC.OTIL AND dt1.[TRAN] = DC.OTRN
	WHERE dc.ODAT IS NOT NULL AND dc.OTIL <> '00'
	order by dc.DATE1, dc.TILL, dc.[TRAN] desc,dc.NUMB
END	
ELSE
BEGIN

	DECLARE @table AS TABLE (
		[Date] DATE NOT NULL,
		TillId CHAR(2) NOT NULL,
		TranNumber CHAR(4) NOT NULL,
		CashierName CHAR(4) NOT NULL,
		Value CHAR(4) NOT NULL,
		ValueTax CHAR(4) NOT NULL,
		[Type] CHAR(4) NOT NULL,
		SupervisorIdVoided CHAR(4) NOT NULL,
		Void CHAR(4) NOT NULL,
		IsTraining CHAR(4) NOT NULL,
		IsSupervisorUsed CHAR(4) NOT NULL,
		UpdatedProc CHAR(4) NOT NULL,
		DocumentNumber CHAR(4) NOT NULL,
		ValueMerchandise CHAR(4) NOT NULL,
		ValueDiscount CHAR(4) NOT NULL,
		CustomerNumber  CHAR(4) NOT NULL,
		IsComplete CHAR(4) NOT NULL,
		VatRate1  CHAR(4) NOT NULL,
		ValueExVat1  CHAR(4) NOT NULL,
		IsParked  CHAR(4) NOT NULL,
		IsOffline  CHAR(4) NOT NULL,
		ColleagueCard  CHAR(4) NOT NULL)
	
	SELECT * FROM @table
	
	SELECT * FROM @table
	
	SELECT * FROM @table
		
	SELECT * FROM @table

END	

IF OBJECT_ID('tempdb..#Table') IS NOT NULL DROP TABLE #Table	
	
END

go
If @@Error = 0
   Print 'Success: The Alter Stored Procedure SalesTransactionGet for US10307 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SalesTransactionGet for US10307 has not been deployed'
GO