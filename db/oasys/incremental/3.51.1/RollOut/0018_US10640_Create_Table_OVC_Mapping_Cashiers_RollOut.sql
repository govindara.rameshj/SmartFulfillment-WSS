IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[OVC_Mapping_Cashiers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	PRINT 'Creating table OVC_Mapping_Cashiers'

	CREATE TABLE [dbo].[OVC_Mapping_Cashiers](
		[OVC_TillID] [int] NOT NULL,
		[Mapping_CashierID] [int] NOT NULL,
	 CONSTRAINT [PK_OVC_Mapping_Cashiers] PRIMARY KEY CLUSTERED 
	(
		[OVC_TillID] ASC,
		[Mapping_CashierID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE
BEGIN
 PRINT 'OVC_Mapping_Cashiers table is already exists.'
END
GO

If @@Error = 0
   Print 'Success: Create Table OVC_Mapping_Cashiers for US10640 has been deployed successfully'
Else
   Print 'Failure: Create Table OVC_Mapping_Cashiers for US10640 has not been deployed successfully'
Go


--Apply Permissions

Grant select On dbo.OVC_Mapping_Cashiers To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on table dbo.OVC_Mapping_Cashiers has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on table dbo.OVC_Mapping_Cashiers might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant select on dbo.OVC_Mapping_Cashiers to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on table dbo.OVC_Mapping_Cashiers has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on table dbo.OVC_Mapping_Cashiers has NOT been successfully deployed'
go
