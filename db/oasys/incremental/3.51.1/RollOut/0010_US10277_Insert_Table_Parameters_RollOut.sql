IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterId = 6500)
BEGIN
INSERT INTO dbo.Parameters (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
VALUES
    (6500,'Min NewTill transaction number', '', 5001, 0, 0.00000, 0)
END

IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterId = 6501)
BEGIN
INSERT INTO dbo.Parameters (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
VALUES
    (6501,'Max NewTill transaction number', '', 9999, 0, 0.00000, 0)
END

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US10277 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US10277 has not been deployed'
GO