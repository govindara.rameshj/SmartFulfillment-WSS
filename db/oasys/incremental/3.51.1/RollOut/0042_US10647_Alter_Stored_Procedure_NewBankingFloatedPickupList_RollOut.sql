SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'NewBankingFloatedPickupList') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure NewBankingFloatedPickupList'
	EXEC ('CREATE PROCEDURE dbo.NewBankingFloatedPickupList AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingFloatedPickupList')
GO

-- =============================================
-- Author        : Alan Lewis
-- Update date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Modify to use new udf_CashierHasTakenSaleToday
--				 : to determine whether a cashier has taken a sale
--				 : for the banking period.
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 25/06/2012
-- User Story	 : 5623
-- Change Request: CR0087: Prevent Pickup if Cashier still signed On till
-- Task Id		 : 5705
-- Description   : Update stored procedure to include a nullable logged On till Id
-- =============================================
ALTER Procedure [dbo].[NewBankingFloatedPickupList]
   @PeriodID Int
As
Begin
	Set NoCount On
	Select
		PickupID             = p.ID,
		PickupSealNumber     = IsNull(p.SealNumber, ''),
		PickupComment        = IsNull(p.Comments, ''),
		StartFloatID         = f.ID,
		StartFloatSealNumber = f.SealNumber,
		StartFloatValue      = f.Value,
		SaleTaken            = case when p.ID is null
									--floated cashiers no pickups
									then [dbo].[udf_CashierHasTakenSaleOnDay](f.AccountabilityID, f.OutPeriodID)
									--floated cashiers with pickup bags
									else Cast(1 As Bit)
							   end,
		CashierID            = u.ID,
		CashierUserName      = u.Name,
		CashierEmployeeCode  = u.EmployeeCode,
		LoggedOnTillId       = c.TILL
	From SystemUsers u 
	--floats
	Inner Join SafeBags f On f.AccountabilityID = u.ID
						 And f.OutPeriodID = @PeriodID 
						 And f.[Type] = 'F'
						 And f.[State] = 'R'
	--pickup
	Left Outer Join SafeBags p On p.AccountabilityID = u.ID
							  And p.PickupPeriodID = @PeriodID
							  And p.[Type] = 'P'
							  And p.[State] <> 'C'
							  And IsNull(p.CashDrop, 0) = 0
	Left Outer Join RSCASH c On c.CASH = u.ID
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingFloatedPickupList for US10647 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingFloatedPickupList for US10647 has not been deployed'
GO
