IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'SaleOrderLineGet') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure SaleOrderLineGet'
	EXEC ('CREATE PROCEDURE dbo.SaleOrderLineGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SaleOrderLineGet')
GO

ALTER PROCEDURE [dbo].[SaleOrderLineGet]
@OrderNumber CHAR (6)
AS
BEGIN
	SET NOCOUNT ON;

	select
		cl.NUMB				as OrderNumber,
		cl.LINE				as Number,
		cl.SKUN				as SkuNumber,
		st.DESCR			as SkuDescription,
		st.BUYU				as SkuUnitMeasure,
		cl.QTYO				as QtyOrdered,
		cl.QTYT				as QtyTaken,
		cl.QTYR				as QtyRefunded,
		cl.QtyToBeDelivered as QtyToDeliver,
		st.ONHA				as QtyOnHand,
		st.ONOR				as QtyOnOrder,
		cl.Price,
		cl.WGHT				as Weight,
		cl.VOLU				as Volume,
		cl.IsDeliveryChargeItem,	
		cl.PORC				as PriceOverrideCode,
		cl.DeliveryStatus,
		cl.SellingStoreId,
		cl.SellingStoreOrderId,
		cl.DeliverySource,
		cl.DeliverySourceIbtOut,
		cl.SellingStoreIbtIn,
		cl.QuantityScanned,
		cl2.SourceOrderLineNo,
		(select ph.DDAT from PURHDR ph where ph.TKEY=
			(select top 1 pl.HKEY from PURLIN pl where pl.SKUN=cl.skun order by pl.TKEY desc)) as NextPoDateDelivery,
		cl.RequiredFulfiller
	from
		CORLIN cl
	--added for hubs 2.0
    left outer join
         CORLIN2 cl2 on cl2.NUMB = cl.NUMB
         AND cl2.LINE = cl.LINE
    inner join
		STKMAS st on st.SKUN=cl.SKUN
	where
		cl.NUMB = @OrderNumber
	order by
		cl.LINE
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SaleOrderLineGet for US11506 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SaleOrderLineGet for US11506 has not been deployed'
GO