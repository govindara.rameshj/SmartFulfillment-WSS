PRINT ('Altering table CORHDR')
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotID')
BEGIN
	PRINT 'Add RequiredDeliverySlotID column to CORHDR'
	ALTER TABLE [dbo].[CORHDR] ADD [RequiredDeliverySlotID] varchar(5) null
END
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotDescription')
BEGIN
	PRINT 'Add RequiredDeliverySlotDescription column to CORHDR'
	ALTER TABLE [dbo].[CORHDR] ADD [RequiredDeliverySlotDescription] varchar(50) null
END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotStartTime' and [precision] = 16)
BEGIN
	PRINT 'Dropping RequiredDeliverySlotStartTime column to CORHDR'
	ALTER TABLE [dbo].[CORHDR] DROP COLUMN [RequiredDeliverySlotStartTime]
END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotEndTime' and [precision] = 16)
BEGIN
	PRINT 'Dropping RequiredDeliverySlotEndTime column to CORHDR'
	ALTER TABLE [dbo].[CORHDR] DROP COLUMN [RequiredDeliverySlotEndTime]
END
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotStartTime')
BEGIN
	PRINT 'Add RequiredDeliverySlotStartTime column to CORHDR'
	ALTER TABLE [dbo].[CORHDR] ADD [RequiredDeliverySlotStartTime] time(0) null
END
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotEndTime')
BEGIN
	PRINT 'Add RequiredDeliverySlotEndTime column to CORHDR'
	ALTER TABLE [dbo].[CORHDR] ADD [RequiredDeliverySlotEndTime] time(0) null
END
GO

If @@Error = 0
   Print 'Success: Alter Table CORHDR for US11506 has been deployed successfully'
Else
   Print 'Failure: Alter Table CORHDR for US11506 has not been deployed successfully'
Go