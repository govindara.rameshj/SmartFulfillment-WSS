PRINT ('Altering table CORLIN')
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORLIN') and name = 'RequiredFulfiller')
BEGIN
	PRINT 'Add RequiredFulfiller column to CORLIN'
	ALTER TABLE [dbo].[CORLIN] ADD [RequiredFulfiller] int null
END
GO

If @@Error = 0
   Print 'Success: Alter Table CORLIN for US11506 has been deployed successfully'
Else
   Print 'Failure: Alter Table CORLIN for US11506 has not been deployed successfully'
Go