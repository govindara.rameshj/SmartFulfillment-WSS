IF NOT EXISTS(SELECT 1 FROM dbo.ReportParameters WHERE ParameterId = 353 AND ReportId = 310)
	INSERT INTO dbo.ReportParameters (
					ReportId,
					ParameterId,
					Sequence,
					AllowMultiple,
					DefaultValue)
	VALUES (310, 353, 3, 0, NULL)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportParameters for US10307 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportParameters for US10307 has not been deployed successfully'
Go

