IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'ColleagueDiscountGet') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure ColleagueDiscountGet'
	EXEC ('CREATE PROCEDURE dbo.ColleagueDiscountGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure ColleagueDiscountGet')
GO

ALTER procedure [dbo].[ColleagueDiscountGet]
   @Date date
as
begin
set nocount on

select dl.DATE1     as 'Date',
       dl.[TIME]    as 'Time',
       dl.TILL      as 'TillId',
       dl.CASH      as 'CashierId',
       coalesce(su.Name, 'Unknown')	as 'CashierName',
       dl.CARD_NO   as 'ColleagueCard',
       ci.CUST_NAME as 'ColleagueName',
       dl.TOTL      as 'ValueTransaction',
       dl.DISC      as 'ValueDiscount'
from DLTOTS dl
left join SystemUsers su
     on su.EmployeeCode = dl.CASH
inner join CARD_LIST ci
      on ci.CARD_NO = dl.CARD_NO
where dl.DATE1   = @Date
and   dl.VOID	 = '0'
and	  dl.TMOD	 = '0'
and   dl.PARK	 = '0'
and   dl.IEMP    = 1
and   dl.CARD_NO > '9*'
order by dl.TILL, dl.[TRAN]
end
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure ColleagueDiscountGet for US12062 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure ColleagueDiscountGet for US12062 has not been deployed'
GO