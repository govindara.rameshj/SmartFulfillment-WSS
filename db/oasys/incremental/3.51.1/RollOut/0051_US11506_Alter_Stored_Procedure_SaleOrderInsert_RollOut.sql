IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'SaleOrderInsert') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure SaleOrderInsert'
	EXEC ('CREATE PROCEDURE dbo.SaleOrderInsert AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SaleOrderInsert')
GO

ALTER PROCEDURE [dbo].[SaleOrderInsert]
@CustomerNumber CHAR (10) = null,
@CustomerName VARCHAR (30) = null,
@DateOrder DATE,
@DateDelivery DATE = null,
@DateDespatch DATE = null, 
@Status CHAR (1) = null, 
@IsForDelivery BIT = 0, 
@DeliveryConfirmed BIT = 0, 
@TimesAmended CHAR (3) = null, 
@DeliveryAddress1 VARCHAR (60), 
@DeliveryAddress2 VARCHAR (30) = null, 
@DeliveryAddress3 VARCHAR (30), 
@DeliveryAddress4 VARCHAR (30) = null, 
@DeliveryPostCode VARCHAR (8), 
@PhoneNumber VARCHAR (20) = null, 
@PhoneNumberMobile VARCHAR (15) = null, 
@IsPrinted BIT=0, 
@NumberReprints DECIMAL (3) = 0, 
@RevisionNumber DECIMAL (3) = 0, 
@MerchandiseValue DECIMAL (9, 2) = 0, 
@DeliveryCharge DECIMAL (9, 2) = 0, 
@QtyOrdered DECIMAL (7) = 0, 
@QtyTaken DECIMAL (7) = 0, 
@QtyRefunded DECIMAL (7) = 0, 
@OrderWeight DECIMAL (7, 2) = 0, 
@OrderVolume DECIMAL (7, 2) = 0, 
@TranDate DATE = null, 
@TranTill CHAR (2) = null, 
@TranNumber CHAR (4) = null, 
@RefundTranDate DATE = null, 
@RefundTill CHAR (2) = null, 
@RefundTranNumber CHAR (4) = null, 
@OrderTakerId CHAR (3) = null, 
@ManagerId CHAR (3) = null, 
@DeliveryStatus INT = 0, 
@RefundStatus INT = 0, 
@SellingStoreId INT = 0, 
@SellingStoreOrderId INT = 0, 
@OmOrderNumber INT = 0, 
@CustomerAddress1 VARCHAR (30), 
@CustomerAddress2 VARCHAR (30) = null, 
@CustomerAddress3 VARCHAR (30), 
@CustomerAddress4 VARCHAR (30) = null, 
@CustomerPostcode VARCHAR (8) = null, 
@CustomerEmail VARCHAR (100) = null, 
@PhoneNumberWork VARCHAR (20) = null, 
@IsSuspended BIT=0, 
@Source VARCHAR (2) = null,
@SourceOrderNumber VARCHAR (20) = null,
@ExtendedLeadTime BIT=0,
@DeliveryContactName VARCHAR (50) = null,
@DeliveryContactPhone VARCHAR (50) = null,
@CustomerAccountNo VARCHAR (20) = null,
@IsClickAndCollect BIT = 0,
@RequiredDeliverySlotID VARCHAR (5) = null,
@RequiredDeliverySlotDescription VARCHAR (50) = null,
@RequiredDeliverySlotStartTime TIME(0) = null,
@RequiredDeliverySlotEndTime TIME(0) = null,
@Number CHAR (6) OUTPUT
AS
BEGIN
	--get next receipt number from system numbers
	declare @StoreId int;
	declare @NextNumber int;
	exec	@NextNumber = SystemNumbersGetNext @Id=10
	
	--convert next number to number (padded to 6 digits)
	set @Number = replace(str(@NextNumber,6),' ','0')
	
	--check if is selling store
	set @StoreId = (Select STOR from SYSOPT where FKEY='01');
	if @StoreId<1000 set @StoreId=@StoreId + 8000;
	if @StoreId=@SellingStoreId set @SellingStoreOrderId=@Number;
		
	--insert into corhdr
	insert into	CORHDR (	
		NUMB,
		CUST,
		NAME,
		DATE1,
		DELD,
		CANC,
		DELI,
		DELC,
		AMDT,
		ADDR1,
		ADDR2,
		ADDR3,
		ADDR4,
		POST,
		PHON,
		MOBP,
		PRNT,
		RPRN,
		REVI,
		MVST,
		DCST,
		QTYO,
		QTYT,
		QTYR,
		WGHT,
		VOLU,
		SDAT,
		STIL,
		STRN,
		RDAT,
		RTIL,
		RTRN,
		RequiredDeliverySlotID,
		RequiredDeliverySlotDescription,
		RequiredDeliverySlotStartTime,
		RequiredDeliverySlotEndTime
	) values (
		@Number,
		@CustomerNumber,
		@CustomerName,
		@DateOrder,
		@DateDelivery,
		@Status,
		@IsForDelivery,
		@DeliveryConfirmed,
		@TimesAmended,
		@DeliveryAddress1,
		@DeliveryAddress2,
		@DeliveryAddress3,
		@DeliveryAddress4,
		@DeliveryPostCode,
		@PhoneNumber,
		@PhoneNumberMobile,
		@IsPrinted,
		@NumberReprints,
		@RevisionNumber,
		@MerchandiseValue,
		@DeliveryCharge,
		@QtyOrdered,
		@QtyTaken,
		@QtyRefunded,
		@OrderWeight,
		@OrderVolume,
		@TranDate,
		@TranTill,
		@TranNumber,
		@RefundTranDate,
		@RefundTill,
		@RefundTranNumber,
		@RequiredDeliverySlotID,
		@RequiredDeliverySlotDescription,
		@RequiredDeliverySlotStartTime,
		@RequiredDeliverySlotEndTime
	)
	
	--inser into corhdr4
	insert into CORHDR4 (
		NUMB,
		DATE1,
		DELD,
		NAME,
		CUST,
		DDAT,
		OEID,
		FDID,
		DeliveryStatus,
		RefundStatus,
		SellingStoreId,
		SellingStoreOrderId,
		OMOrderNumber,
		CustomerAddress1,
		CustomerAddress2,
		CustomerAddress3,
		CustomerAddress4,
		CustomerPostcode,
		CustomerEmail,
		PhoneNumberWork,
		IsSuspended
	) values (
		@Number,
		@DateOrder,
		@DateDelivery,
		@CustomerName,
		@CustomerNumber,
		@DateDespatch,
		@OrderTakerId,
		@ManagerId,	
		@DeliveryStatus,
		@RefundStatus,
		@SellingStoreId,
		@SellingStoreOrderId,
		@OmOrderNumber,
		@CustomerAddress1,
		@CustomerAddress2,
		@CustomerAddress3,
		@CustomerAddress4,
        IsNull(@CustomerPostcode,dbo.udf_StorePostCode(@StoreId)),
		@CustomerEmail,
		@PhoneNumberWork,
		@IsSuspended
	)
	
	--insert into corhdr5
	
	insert into CORHDR5
	(
	NUMB,
	Source,
	SourceOrderNumber,
	ExtendedLeadTime,
	DeliveryContactName,
	DeliveryContactPhone,
	CustomerAccountNo,
	IsClickAndCollect
	)
	values
	(
	@Number,
	@Source,
	@SourceOrderNumber,
    @ExtendedLeadTime,
    @DeliveryContactName,
    @DeliveryContactPhone,
    IsNull(@CustomerAccountNo,' '),
    @IsClickAndCollect
	)
	
	return @@rowcount
	
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SaleOrderInsert for US11506 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SaleOrderInsert for US11506 has not been deployed'
GO