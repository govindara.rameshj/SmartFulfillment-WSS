SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'NewBankingUnFloatedPickupList') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure NewBankingUnFloatedPickupList'
	EXEC ('CREATE PROCEDURE dbo.NewBankingUnFloatedPickupList AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingUnFloatedPickupList')
GO

-- =============================================
-- Author        : Alan Lewis
-- Create date   : 25/06/2012
-- User Story	 : 5623
-- Change Request: CR0087: Prevent Pickup if Cashier still signed On till
-- Task Id		 : 5705
-- Description   : Update stored procedure to include a nullable logged On till Id
-- =============================================
ALTER Procedure [dbo].[NewBankingUnFloatedPickupList]
   @PeriodID Int
As
Begin
	Set NoCount On
	Select 
		PickupID            = p.ID,
		PickupSealNumber    = ISNULL(p.SealNumber, ''),
		PickupComment       = ISNULL(p.Comments, ''),
		CashierID           = u.ID,
		CashierUserName     = u.Name,
		CashierEmployeeCode = u.EmployeeCode,
		LoggedOnTillId      = c.Till       
	From SystemUsers u
	Inner Join (
		Select *
		From CashBalCashier z
		Inner Join SystemCurrency s On s.IsDefault = 1
		Where z.PeriodID = @PeriodID 
			And z.CurrencyID = s.ID
			And (z.NumTransactions > 0 or z.NumCorrections > 0)
			And (z.GrossSalesAmount <> 0 Or Exists (
					Select *
					From CashBalCashierTen
					Where PeriodID = @PeriodID 
						And CurrencyID = s.ID
						And CashierID = z.CashierID)
				)
		) z On z.CashierID = u.ID
	--end of day pickup bag
	Left Outer Join SafeBags p On p.AccountabilityID = u.ID
							  And p.PickupPeriodID = @PeriodID
							  And p.[Type] = 'P'
							  And p.[State] <> 'C'
							  And IsNull(p.CashDrop, 0) = 0
	Left Outer Join RSCASH c On u.ID = c.CASH
	Where
		--remove deleted cashiers
		u.IsDeleted = 0
		--and cashiers already assigned floats
		And Not Exists (
			Select * From SafeBags f
			Where f.[Type] = 'F'
				And f.[State] = 'R'
				And f.OutPeriodID = @PeriodID
				And f.AccountabilityID = u.ID)
	
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingUnFloatedPickupList for US10647 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingUnFloatedPickupList for US10647 has not been deployed'
GO
