IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = 52)
BEGIN
	INSERT INTO dbo.Parameters (
		[ParameterID],
		[Description],
		[StringValue],
		[LongValue],
		[BooleanValue],
		[DecimalValue],
		[ValueType])
	VALUES (52, 'Choice of cashier', '0-Real cashiers, 1-Virtual Cashiers, 2-Both together', 0, 0, NULL, 1)	
END

If @@Error = 0
   Print 'Success: Insert into table Parameters for US10640 has been deployed successfully'
Else
   Print 'Failure: Insert into table Parameters for US10640 has not been deployed successfully'
GO