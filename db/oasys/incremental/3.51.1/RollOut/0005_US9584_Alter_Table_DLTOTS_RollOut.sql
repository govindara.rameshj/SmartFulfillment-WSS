SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('DLTOTS') AND name = 'IX_DLTOTS_SourceTranId')
BEGIN 
	PRINT 'Dropping index IX_DLTOTS_SourceTranId from table DLTOTS'
	DROP INDEX [IX_DLTOTS_SourceTranId] ON dbo.DLTOTS
END

IF EXISTS (select * from sys.columns c inner join sys.types t on t.system_type_id = c.system_type_id where object_id = OBJECT_ID('DLTOTS') and c.name = 'SourceTranId' and t.name = 'uniqueidentifier')
BEGIN
	PRINT 'Dropping SourceTranId column from DLTOTS'
	ALTER TABLE dbo.DLTOTS DROP COLUMN [SourceTranId]
END

IF NOT EXISTS (select * from sys.columns where object_id = OBJECT_ID('DLTOTS') and name = 'SourceTranId')
BEGIN
	PRINT 'Add SourceTranId column to DLTOTS'
	ALTER TABLE dbo.DLTOTS ADD [SourceTranId] varchar(50) NULL
END
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLTOTS') and name = 'SourceTranNumber')
BEGIN
	PRINT 'Add SourceTranNumber column to DLTOTS'
	ALTER TABLE dbo.DLTOTS ADD [SourceTranNumber] varchar(20) NULL
END
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLTOTS') and name = 'Source')
BEGIN
	PRINT 'Add Source column to DLTOTS'
	ALTER TABLE dbo.DLTOTS ADD [Source] varchar(10) NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('DLTOTS') AND name = 'IX_DLTOTS_Source_SourceTranId')
	BEGIN 
		PRINT 'Creating index IX_DLTOTS_Source_SourceTranId on table DLTOTS'
		CREATE NONCLUSTERED INDEX [IX_DLTOTS_Source_SourceTranId] ON [dbo].[DLTOTS] 
		(
			[Source] ASC,
			[SourceTranId] ASC
		)
	END
ELSE
	BEGIN
		IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('DLTOTS') AND name = 'IX_DLTOTS_Source_SourceTranId' AND is_unique = 0)
			BEGIN
				CREATE UNIQUE INDEX IX_DLTOTS_Source_SourceTranId ON dbo.DLTOTS 
				(
					[Source] ASC,
					[SourceTranId] ASC
				)
				WHERE SourceTranId IS NOT NULL
				WITH (DROP_EXISTING = ON)
			END
	END
GO

If @@Error = 0
   Print 'Success: Altering table DLTOTS for US9584 has been deployed successfully'
ELSE
   Print 'Failure: Altering table DLTOTS for US9584 has not been deployed successfully'
GO	
