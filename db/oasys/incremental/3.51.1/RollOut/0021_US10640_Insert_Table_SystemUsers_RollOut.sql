IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'tgSystemUsersInsert') AND type in (N'TR'))
BEGIN
	PRINT 'Disable trigger tgSystemUsersInsert';
	DISABLE TRIGGER [dbo].[tgSystemUsersInsert] ON [dbo].[SystemUsers];
END
GO

DECLARE @PasswordExpires as date
SET @PasswordExpires = GETDATE()

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 401) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 401 as [ID], '401' as [EmployeeCode], 'Checkout 1' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END
	
IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 402) 
BEGIN	           
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 402 as [ID], '402' as [EmployeeCode], 'Checkout 2' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 403) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 403 as [ID], '403' as [EmployeeCode], 'Checkout 3' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 404) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 404 as [ID], '404' as [EmployeeCode], 'Checkout 4' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 405) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 405 as [ID], '405' as [EmployeeCode], 'Checkout 5' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 406) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 406 as [ID], '406' as [EmployeeCode], 'Checkout 6' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 407) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 407 as [ID], '407' as [EmployeeCode], 'Checkout 7' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 408) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 408 as [ID], '408' as [EmployeeCode], 'Checkout 8' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 409) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 409 as [ID], '409' as [EmployeeCode], 'Checkout 9' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 410) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 410 as [ID], '410' as [EmployeeCode], 'Checkout 10' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 411) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 411 as [ID], '411' as [EmployeeCode], 'Refund Checkout 1' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 412) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 412 as [ID], '412' as [EmployeeCode], 'Bulk Checkout 1' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 465) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 465 as [ID], '465' as [EmployeeCode], 'Counter Checkout 65' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 466) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 466 as [ID], '466' as [EmployeeCode], 'Counter Checkout 66' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 467) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 467 as [ID], '467' as [EmployeeCode], 'Counter Checkout 67' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 468) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 468 as [ID], '468' as [EmployeeCode], 'Counter Checkout 68' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] su WHERE su.[ID] = 469) 
BEGIN	
	INSERT INTO [dbo].[SystemUsers]
			   ([ID]
			   ,[EmployeeCode]
			   ,[Name]
			   ,[Initials]
			   ,[Position]
			   ,[PayrollID]
			   ,[Password]
			   ,[PasswordExpires]
			   ,[IsManager]
			   ,[IsSupervisor]
			   ,[SupervisorPassword]
			   ,[SupervisorPwdExpires]
			   ,[Outlet]
			   ,[IsDeleted]
			   ,[DeletedDate]
			   ,[DeletedTime]
			   ,[DeletedBy]
			   ,[DeletedWhere]
			   ,[TillReceiptName]
			   ,[DefaultAmount]
			   ,[LanguageCode]
			   ,[SecurityProfileID])
	SELECT 469 as [ID], '469' as [EmployeeCode], 'Counter Checkout 69' as [Name], 'SYS' as [Initials], 'System' as [Position], '000000' as [PayrollID],
			'00000' as [Password], @PasswordExpires as [PasswordExpires], 0 as [IsManager], 0 as [IsSupervisor], NULL as [SupervisorPassword], 
			NULL as [SupervisorPwdExpires], '60' as [Outlet], 1 as [IsDeleted], NULL as [DeletedDate], NULL as [DeletedTime], 
			NULL as [DeletedBy], NULL as [DeletedWhere], 'Virtual Cashier' as [TillReceiptName], 0.00 as [DefaultAmount], 
			'000' as [LanguageCode], 110 as [SecurityProfileID]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'tgSystemUsersInsert') AND type in (N'TR'))
BEGIN
	PRINT 'Enable trigger tgSystemUsersInsert';
	ENABLE TRIGGER [dbo].[tgSystemUsersInsert] ON [dbo].[SystemUsers];
END
GO

If @@Error = 0
   Print 'Success: Insert Table SystemUsers for US10640 has been deployed successfully'
Else
   Print 'Failure: Insert Table SystemUsers for US10640 has not been deployed successfully'
Go
