SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'NewBankingFloatedPickupList') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure NewBankingFloatedPickupList'
	EXEC ('CREATE PROCEDURE dbo.NewBankingFloatedPickupList AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingFloatedPickupList')
GO

-- =============================================
-- Author        : Alan Lewis
-- Update date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Modify to use new udf_CashierHasTakenSaleToday
--				 : to determine whether a cashier has taken a sale
--				 : for the banking period.
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 25/06/2012
-- User Story	 : 5623
-- Change Request: CR0087: Prevent Pickup if Cashier still signed On till
-- Task Id		 : 5705
-- Description   : Update stored procedure to include a nullable logged On till Id
-- =============================================
ALTER Procedure [dbo].[NewBankingFloatedPickupList]
   @PeriodID Int
As
Begin
	Set NoCount On
	--cashier accountability model only
	--floated cashiers no pickups
	Select
		PickupID = CAST(null As Int),
		PickupSealNumber = '',
		PickupComment = '',
		StartFloatID = a.ID,
		StartFloatSealNumber = a.SealNumber,
		StartFloatValue = a.Value,
		SaleTaken = [dbo].[udf_CashierHasTakenSaleOnDay](a.AccountabilityID, @PeriodID),
		CashierID = a.AccountabilityID,
		CashierUserName = b.Name,
		CashierEmployeeCode = b.EmployeeCode,
		LoggedOnTillId = d.TILL
	From 
		SafeBags a
		--cashier
			Inner Join 
				SystemUsers b
			On 
				b.ID = a.AccountabilityID
			--filter out if pickup bag (not cash drop) exist for this cashier
			Left Outer Join 
				(
					Select 
						*
					From 
						SafeBags
					Where 
						[Type]            = 'P'
					And   
						[State]          <> 'C'
					And 
						IsNull(CashDrop, 0) = 0 
				) c
			On  
				c.PickupPeriodID   = a.OutPeriodID  
			And 
				c.AccountabilityID = a.AccountabilityID
			Left Outer Join
				RSCASH d
			On
				a.AccountabilityID = d.CASH				
	Where
		a.[Type]      = 'F'
	And   
		a.[State]     = 'R'
	And   
		a.OutPeriodID = @PeriodID 
	And   
		c.ID is null
	Union all
	--floated cashiers with pickup bags
	Select
		PickupID             = a.ID,
		PickupSealNumber     = a.SealNumber,
		PickupComment        = a.Comments,
		StartFloatID         = b.ID,
		StartFloatSealNumber = b.SealNumber,
		StartFloatValue      = b.Value,
		SaleTaken            = Cast(1 As Bit),       
		CashierID            = a.AccountabilityID,
		CashierUserName      = c.Name,
		CashierEmployeeCode  = c.EmployeeCode,
		LoggedOnTillId = d.TILL
	From 
		SafeBags a
			--starting float
			Inner Join 
				(
					Select 
						* 
					From 
						SafeBags 
					Where 
						[Type] = 'F' 
					And 
						[State] = 'R'
				) b
			On  
				b.OutPeriodID      = a.PickupPeriodID 
			And 
				b.AccountabilityID = a.AccountabilityID
				Inner Join 
					SystemUsers c
				On 
					c.ID = b.AccountabilityID
			Left Outer Join
				RSCASH d
			On
				a.AccountabilityID = d.CASH				
		Where 
			a.[Type]              = 'P'
		And   
			a.[State]            <> 'C'
		And   
			a.PickupPeriodID      = @PeriodID
		And   
			IsNull(a.CashDrop, 0) = 0
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingFloatedPickupList for US10647 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingFloatedPickupList for US10647 has not been deployed'
GO
