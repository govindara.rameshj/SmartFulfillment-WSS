IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'usp_FillSlotInDeliveryOrder') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.usp_FillSlotInDeliveryOrder
END
GO

If @@Error = 0
   Print 'Success: The Deleting of Stored Procedure usp_FillSlotInDeliveryOrder for US11506 has been deployed successfully'
Else
   Print 'Failure: The Deleting of Stored Procedure usp_FillSlotInDeliveryOrder for US11506 has not been deployed'
GO