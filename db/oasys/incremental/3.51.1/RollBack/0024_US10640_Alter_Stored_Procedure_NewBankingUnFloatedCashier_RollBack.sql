IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'NewBankingUnFloatedCashier') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure NewBankingUnFloatedCashier'
	EXEC ('CREATE PROCEDURE dbo.NewBankingUnFloatedCashier AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingUnFloatedCashier')
GO

ALTER procedure [dbo].[NewBankingUnFloatedCashier]
   @PeriodID int
as
begin
set nocount on

select UserID = ID,
       Employee = EmployeeCode + ' - ' + Name
from SystemUsers
where IsDeleted = 0                                             --remove delete cashiers
and   ID       <= (select cast(HCAS as integer) from RETOPT)    --filter out non operational users
and   ID not in (select AccountabilityID                        --remove cashiers already assigned floats
                 from SafeBags
                 where [Type]      = 'F'
                 and   [State]     = 'R'
                 and   OutPeriodID = @PeriodID)
end
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingUnFloatedCashier for US10640 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingUnFloatedCashier for US10640 has not been rolled back'
GO