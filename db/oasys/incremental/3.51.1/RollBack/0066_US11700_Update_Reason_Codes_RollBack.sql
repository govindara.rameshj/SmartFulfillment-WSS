--Paid Out 
--Reason Code - "Stamps"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 6 AND [TYPE] = 'M-' AND [NAME] = 'Postage')
BEGIN
	UPDATE [dbo].[SystemCodes]
	SET [NAME] = 'Stamps'
	WHERE [ID] = 6 AND [TYPE] = 'M-' AND [NAME] = 'Postage'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '06' AND [TYPE] = 'M-' AND [DESCR] = 'Postage')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [DESCR] = 'Stamps'
	WHERE [CODE] = '06' AND [TYPE] = 'M-' AND [DESCR] = 'Postage'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [MMRC6] = 'POSTAGE')
BEGIN
	UPDATE [dbo].[RETOPT]
	SET [MMRC6] = 'STAMPS'
	WHERE [FKEY] = '01' AND [MMRC6] = 'POSTAGE'
END
GO

--Reason Code - "Change Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '10' AND [TYPE] = 'M-' AND [DESCR] = 'Change Fund')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [IsActive] = 1
	WHERE [CODE] = '10' AND [TYPE] = 'M-' AND [DESCR] = 'Change Fund'
END
GO

--Reason Code - "Leukaemia Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 14 AND [TYPE] = 'M-' AND [NAME] = 'Charity')
BEGIN
	UPDATE [dbo].[SystemCodes]
	SET [NAME] = 'Leukaemia Fund'
	WHERE [ID] = 14 AND [TYPE] = 'M-' AND [NAME] = 'Charity'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '14' AND [TYPE] = 'M-' AND [DESCR] = 'Charity')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [DESCR] = 'Leukaemia Fund'
	WHERE [CODE] = '14' AND [TYPE] = 'M-' AND [DESCR] = 'Charity'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [MMRC14] = 'CHARITY')
BEGIN
	UPDATE [dbo].[RETOPT]
	SET [MMRC14] = 'LEUKAEMIA FU'
	WHERE [FKEY] = '01' AND [MMRC14] = 'CHARITY'
END
GO

--Misc In 
--Reason Code - "Burger Van"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 4 AND [TYPE] = 'M+' AND [NAME] = 'Car Park Trading')
BEGIN
	UPDATE [dbo].[SystemCodes]
	SET [NAME] = 'Burger Van'
	WHERE [ID] = 4 AND [TYPE] = 'M+' AND [NAME] = 'Car Park Trading'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '04' AND [TYPE] = 'M+' AND [DESCR] = 'Car Park Trading')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [DESCR] = 'Burger Van'
	WHERE [CODE] = '04' AND [TYPE] = 'M+' AND [DESCR] = 'Car Park Trading'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [MPRC4] = 'CAR PARK TRADING')
BEGIN
	UPDATE [dbo].[RETOPT]
	SET [MPRC4] = 'BURGER VAN'
	WHERE [FKEY] = '01' AND [MPRC4] = 'CAR PARK TRADING'
END
GO

--Reason Code - "Phone Box"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '06' AND [TYPE] = 'M+' AND [DESCR] = 'Phone Box')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [IsActive] = 1
	WHERE [CODE] = '06' AND [TYPE] = 'M+' AND [DESCR] = 'Phone Box'
END
GO

--Reason Code - "Rent"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '08' AND [TYPE] = 'M+' AND [DESCR] = 'Rent')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [IsActive] = 1
	WHERE [CODE] = '08' AND [TYPE] = 'M+' AND [DESCR] = 'Rent'
END
GO

--Reason Code - "Leukaemia Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 14 AND [TYPE] = 'M+' AND [NAME] = 'Charity')
BEGIN
	UPDATE [dbo].[SystemCodes]
	SET [NAME] = 'Leukaemia Fund'
	WHERE [ID] = 14 AND [TYPE] = 'M+' AND [NAME] = 'Charity'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '14' AND [TYPE] = 'M+' AND [DESCR] = 'Charity')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [DESCR] = 'Leukaemia Fund'
	WHERE [CODE] = '14' AND [TYPE] = 'M+' AND [DESCR] = 'Charity'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [MPRC14] = 'CHARITY')
BEGIN
	UPDATE [dbo].[RETOPT]
	SET [MPRC14] = 'LEUKAEMIA FU'
	WHERE [FKEY] = '01' AND [MPRC14] = 'CHARITY'
END
GO

--Paid Out correction 
--Reason Code - "Stamps"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 6 AND [TYPE] = 'C-' AND [NAME] = 'Postage')
BEGIN
	UPDATE [dbo].[SystemCodes]
	SET [NAME] = 'Stamps'
	WHERE [ID] = 6 AND [TYPE] = 'C-' AND [NAME] = 'Postage'
END
GO

--Reason Code - "Leukaemia Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 14 AND [TYPE] = 'C-' AND [NAME] = 'Charity')
BEGIN
	UPDATE [dbo].[SystemCodes]
	SET [NAME] = 'Leukaemia Fund'
	WHERE [ID] = 14 AND [TYPE] = 'C-' AND [NAME] = 'Charity'
END
GO

--Misc In correction
--Reason Code - "Burger Van"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 4 AND [TYPE] = 'C+' AND [NAME] = 'Car Park Trading')
BEGIN
	UPDATE [dbo].[SystemCodes]
	SET [NAME] = 'Burger Van'
	WHERE [ID] = 4 AND [TYPE] = 'C+' AND [NAME] = 'Car Park Trading'
END
GO

--Reason Code - "Leukaemia Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 14 AND [TYPE] = 'C+' AND [NAME] = 'Charity')
BEGIN
	UPDATE [dbo].[SystemCodes]
	SET [NAME] = 'Leukaemia Fund'
	WHERE [ID] = 14 AND [TYPE] = 'C+' AND [NAME] = 'Charity'
END
GO

--Refund
--Reason Code - "Not Needed"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '01' AND [TYPE] = 'RF' AND [DESCR] = 'Customer Changed Mind')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [DESCR] = 'Not Needed'
	WHERE [CODE] = '01' AND [TYPE] = 'RF' AND [DESCR] = 'Customer Changed Mind'
END
GO

--Reason Code - "Wrong Item"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '03' AND [TYPE] = 'RF' AND [DESCR] = 'Wrong Item')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [IsActive] = 1
	WHERE [CODE] = '03' AND [TYPE] = 'RF' AND [DESCR] = 'Wrong Item'
END
GO

--Reason Code - "Other"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '06' AND [TYPE] = 'RF' AND [DESCR] = 'Other')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [IsActive] = 1
	WHERE [CODE] = '06' AND [TYPE] = 'RF' AND [DESCR] = 'Other'
END
GO

--Price Override
--Reason Code - "Deleted Product"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '06' AND [TYPE] = 'OV' AND [DESCR] = 'Web Return')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [DESCR] = 'Deleted Product'
	WHERE [CODE] = '06' AND [TYPE] = 'OV' AND [DESCR] = 'Web Return'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [PORC6] = 'WEB RETURN')
BEGIN
	UPDATE [dbo].[RETOPT]
	SET [PORC6] = 'DELETED PRODUCT'
	WHERE [FKEY] = '01' AND [PORC6] = 'WEB RETURN'
END
GO

--Reason Code - "Incorrect Day Price"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '11' AND [TYPE] = 'OV' AND [DESCR] = 'Incorrect Day Price')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [IsActive] = 1
	WHERE [CODE] = '11' AND [TYPE] = 'OV' AND [DESCR] = 'Incorrect Day Price'
END
GO

--Reason Code - "Temporary Deal Group"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '12' AND [TYPE] = 'OV' AND [DESCR] = 'Temporary Deal Group')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [IsActive] = 1
	WHERE [CODE] = '12' AND [TYPE] = 'OV' AND [DESCR] = 'Temporary Deal Group'
END
GO

--Reason Code - "Even Refunds"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '08' AND [TYPE] = 'OV' AND [DESCR] = 'HDC Return')
BEGIN
	UPDATE [dbo].[SYSCOD]
	SET [IsActive] = 0,
		[DESCR] = 'Event Refund'
	WHERE [CODE] = '08' AND [TYPE] = 'OV' AND [DESCR] = 'HDC Return'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [PORC8] = 'HDC RETURN')
BEGIN
	UPDATE [dbo].[RETOPT]
	SET [PORC8] = 'EVENT REFUND'
	WHERE [FKEY] = '01' AND [PORC8] = 'HDC RETURN'
END
GO

If @@Error = 0
   Print 'Success: Update Reason Codes for US11700 has been deployed successfully'
Else
   Print 'Failure: Update Reason Codes for US11700 has not been deployed successfully'
Go