IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'EnquirySales') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure EnquirySales'
	EXEC ('CREATE PROCEDURE dbo.EnquirySales AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure EnquirySales')
GO

ALTER Procedure [dbo].[EnquirySales]
	@SkuNumber	Char(6)
As
Begin
	Set NOCOUNT On;

	Declare 
		@table table 
			(
				Date			Date,
				TillId			Char(2),
				TranNumber		Char(4),
				RowId			Int, 
				SkuNumber		Char(6), 
				Description		VarChar(50), 
				Display			VarChar(50)
			);
	Declare 
		@date			Date,
		@qtySold		Int,
		@qtyRefund		Int,
		@qtyMarkdown	Int,
		@lastSaleDate	Date,
		@lastSaleTime	Char(6),
		@lastSaleTill	Char(2),
		@lastSaleTran	Char(4);

	Set @date = CONVERT(Date, getdate());

	Select
		@qtySold	= sum(dl.quan)
	From	
		dltots dt
	Inner Join
		dlline dl	
	On 
		dl.date1=dt.date1 
	And 
		dl.TILL=dt.TILL 
	And 
		dl.[tran]=dt.[tran] 
	And 
		dl.skun = @skuNumber
	Where	
		dt.DATE1	= @date
	And	
		dt.CASH		<> '000'
	And	
		dt.VOID		= 0
	And	
		dt.PARK		= 0
	And	
		dt.TMOD		= 0
	And 
		dt.TCOD		= 'SA';
	
	Select
		@qtyRefund	= sum(dl.quan)
	From	
		dltots dt
	Inner Join
		dlline dl	
	On 
		dl.date1=dt.date1 
	And 
		dl.TILL=dt.TILL 
	And 
		dl.[tran]=dt.[tran] 
	And 
		dl.skun = @skuNumber
	Where	
		dt.DATE1	= @date
	And	
		dt.CASH		<> '000'
	And	
		dt.VOID		= 0
	And	
		dt.PARK		= 0
	And	
		dt.TMOD		= 0
	And 
		dt.TCOD		= 'RF';

	Select
		@qtyMarkdown	= sum(dl.quan)
	From	
		dltots dt
	Inner Join
		dlline dl	
	On 
		dl.date1=dt.date1 
	And 
		dl.TILL=dt.TILL 
	And 
		dl.[tran]=dt.[tran] 
	And 
		dl.skun = @skuNumber
	And 
		dl.imdn=1
	Where	
		dt.DATE1	= @date
	And	
		dt.CASH		<> '000'
	And	
		dt.VOID		= 0
	And	
		dt.PARK		= 0
	And	
		dt.TMOD		= 0
	And 
		dt.TCOD		= 'SA';

	Select Top 1
		@lastSaleDate	= dt.date1,
		@lastSaleTime	= dt.[time],
		@lastSaleTill	= dt.till,
		@lastSaleTran	= dt.[tran]
	From	
		dltots dt
	Inner Join
		dlline dl	
	On	
		dt.DATE1=dl.DATE1 
	And 
		dt.TILL=dl.TILL
	And 
		dt.[TRAN]=dl.[TRAN] 
	And 
		dl.skun = @skuNumber
	Where	
		dt.CASH		<> '000'
	And	
		dt.VOID		= 0
	And	
		dt.PARK		= 0
	And	
		dt.TMOD		= 0
	And 
		dt.TCOD		= 'SA'
	Order By
		dt.date1 Desc, 
		dt.[time] Desc

	Insert Into @table (Description, Display) Values ('Sold Today',		coalesce(@qtySold, 0) );
	Insert Into @table (Description, Display) Values ('Refunded Today',	coalesce(@qtyRefund, 0) );
	Insert Into @table (Description, Display) Values ('MD Sold Today',	coalesce(@qtyMarkdown,0) );
	Insert Into @table (Description, Display) Values ('Last Sale',		convert(Varchar(10), @lastSaleDate, 103) + ' ' + substring(@lastSaleTime,1,2) + ':' + substring(@lastSaleTime, 3, 2) );
	Insert Into @table (Description, Display) Values ('', '');
	Insert Into 
		@table
			(rowid, Date, TillId, TranNumber, Description, Display) 
		Values
			(2, @lastSaleDate, @lastSaleTill, @lastSaleTran, 'Last Sale Tran', @lastSaleTill + '-' + @lastSaleTran );
	
	Insert Into 
		@table	
			(RowId, SkuNumber, [Description]) 
		Values
			(1, @SkuNumber, '13 Week Sales');
		
	Select * From @table;
End

go

If @@Error = 0
   Print 'Success: The Deleting of Stored Procedure EnquirySales for US10307 has been deployed successfully'
Else
   Print 'Failure: The Deleting of Stored Procedure EnquirySales for US10307 has not been deployed'
GO