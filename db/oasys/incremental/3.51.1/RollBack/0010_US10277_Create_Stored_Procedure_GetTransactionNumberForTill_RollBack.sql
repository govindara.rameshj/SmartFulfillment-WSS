IF EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'GetTransactionNumberForTill') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE dbo.GetTransactionNumberForTill
END
GO

If @@Error = 0
   Print 'Success: The Deleting of Stored Procedure GetTransactionNumberForTill for US10277 has been deployed successfully'
Else
   Print 'Failure: The Deleting of Stored Procedure GetTransactionNumberForTill for US10277 has not been deployed'
GO