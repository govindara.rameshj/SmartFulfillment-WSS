IF EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID IN (53, 54, 55, 56, 57, 58, 59, 60))
DELETE FROM dbo.Parameters WHERE ParameterID IN (53, 54, 55, 56, 57, 58, 59, 60)

If @@Error = 0
   Print 'Success: Delete from table Parameters for US11506 has been deployed successfully'
Else
   Print 'Failure: Delete from table Parameters for US11506 has not been deployed successfully'
GO