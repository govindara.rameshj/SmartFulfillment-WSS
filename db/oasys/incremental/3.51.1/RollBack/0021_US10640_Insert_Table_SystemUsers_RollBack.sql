DELETE FROM [dbo].[SystemUsers] WHERE [ID] IN (401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 465, 466, 467, 468, 469)

If @@Error = 0
   Print 'Success: Delete from table SystemUsers for US10640 has been deployed successfully'
Else
   Print 'Failure: Delete from table SystemUsers for US10640 has not been deployed successfully'
GO