IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'NewBankingCashier') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure NewBankingCashier'
	EXEC ('CREATE PROCEDURE dbo.NewBankingCashier AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingCashier')
GO

-- =============================================
-- Author        : Alan Lewis
-- Update date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Modify to use new udf_CashierHasTakenSaleToday
--				 : to determine whether a cashier has taken a sale
--				 : for the banking period and filter out those that
--				 : have not.
-- =============================================
ALTER Procedure [dbo].[NewBankingCashier]
	@PeriodID As Integer = Null
As
Begin
	Set NoCount On

	Select
		UserID = ID,
		Employee = EmployeeCode + ' - ' + Name
	From
		SystemUsers
	Where
		IsDeleted = 0	--remove delete cashiers
	And
		ID <= --filter out non operational users
			(
				Select 
					Cast(HCAS As Integer) 
				From 
					RETOPT
			)
	And   --filter out cashiers that have not taken a sale
		(
			[dbo].[udf_CashierHasTakenSaleOnDay](ID, @PeriodID) = 1
		Or
			@PeriodID Is Null
		)
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingCashier for US10640 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingCashier for US10640 has not been rolled back'
GO