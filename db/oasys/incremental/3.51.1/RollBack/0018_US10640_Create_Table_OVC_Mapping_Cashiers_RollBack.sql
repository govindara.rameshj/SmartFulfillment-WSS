IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[OVC_Mapping_Cashiers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	DROP TABLE [dbo].[OVC_Mapping_Cashiers]
END
GO

If @@Error = 0
   Print 'Success: Drop Table OVC_Mapping_Cashiers for US10640 has been successfully dropped'
Else
   Print 'Failure: Drop Table OVC_Mapping_Cashiers for US10640 has not been successfully dropped'
GO