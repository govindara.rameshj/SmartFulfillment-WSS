SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'NewBankingUnFloatedPickupList') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure NewBankingUnFloatedPickupList'
	EXEC ('CREATE PROCEDURE dbo.NewBankingUnFloatedPickupList AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingUnFloatedPickupList')
GO

-- =============================================
-- Author        : Alan Lewis
-- Create date   : 25/06/2012
-- User Story	 : 5623
-- Change Request: CR0087: Prevent Pickup if Cashier still signed On till
-- Task Id		 : 5705
-- Description   : Update stored procedure to include a nullable logged On till Id
-- =============================================
ALTER Procedure [dbo].[NewBankingUnFloatedPickupList]
   @PeriodID Int
As
Begin
	Set NoCount On
	--cashier accountability model only
	--cashiers (floated & unfloated) and design consultants will always have an entry in CashBalCashier

	--no e.o.d pickup bag
	Select 
		PickupID            = Cast(Null As Int),
		PickupSealNumber    = '',
		PickupComment       = '',
		CashierID           = a.ID,
		CashierUserName     = a.Name,
		CashierEmployeeCode = a.EmployeeCode,
		LoggedOnTillId = e.Till       
	From 
		SystemUsers a
	--cashiers with sales
	/*
	inner join (select *
				from CashBalCashier
				where PeriodID        = @PeriodID 
				and   CurrencyID      = (select ID from SystemCurrency where IsDefault = 1)
				and   NumTransactions > 0) b
		  on b.CashierID   = a.ID
	*/
	--no valid sales e.g invalid "vision deposit"
	Inner join 
		(
			Select 
				*
			From 
				CashBalCashier z
			Where 
				z.PeriodID = @PeriodID 
			And   
				z.CurrencyID = 
					(
						Select 
							ID 
						From 
							SystemCurrency 
						Where 
							IsDefault = 1
					)
			And   
				(z.NumTransactions > 0 or z.NumCorrections > 0)
			And  
				(
					z.GrossSalesAmount <> 0 
				Or 
					(
						Select 
							Count(*)
						From 
							CashBalCashierTen
						Where 
							PeriodID   = @PeriodID
						And   
							CurrencyID = 
								(
									Select 
										ID 
									From 
										SystemCurrency 
									Where 
										IsDefault = 1
								)
						And   
							CashierID  = z.CashierID
					) > 0
				)
		) b
	On 
		b.CashierID   = a.ID
	--cashiers already assigned floats
	Left Outer Join 
		(
			Select 
				AccountabilityID
			From 
				SafeBags
			Where 
				[Type]      = 'F'
			And   
				[State]     = 'R'
			And   
				OutPeriodID = @PeriodID
		) c
	On 
		c.AccountabilityID = a.ID 
	--cashiers already assigned a end of day pickup bag
	Left Outer Join 
		(
			Select 
				AccountabilityID
			From
				SafeBags
			Where
				[Type]              = 'P'
			And
				[State]            <> 'C'
			And
				PickupPeriodID      = @PeriodID
			And
				IsNull(CashDrop, 0) = 0
		) d
	On 
		d.AccountabilityID = a.ID 
	Left Outer Join 
		RSCASH e
	On 
		a.ID = e.CASH
	Where 
		a.IsDeleted = 0               --remove delete cashiers
	And
		c.AccountabilityID is null    --remove cashiers assigned floats
	And
		d.AccountabilityID is null    --remove cashiers assigned a e.o.d pickup bag
	Union all
	--e.o.d pickup bag
	Select 
		PickupID            = d.ID,
		PickupSealNumber    = d.SealNumber ,
		PickupComment       = d.Comments,
		CashierID           = a.ID,
		CashierUserName     = a.Name,
		CashierEmployeeCode = a.EmployeeCode,
		LoggedOnTillId = e.Till       
	From
		SystemUsers a
	--cashiers with sales
	/*
	inner join (select *
				from CashBalCashier
				where PeriodID        = @PeriodID 
				and   CurrencyID      = (select ID from SystemCurrency where IsDefault = 1)
				and   NumTransactions > 0) b
		  on b.CashierID   = a.ID
	*/
	--no valid sales e.g invalid "vision deposit"
	Inner Join 
		(
			Select 
				*
			From 
				CashBalCashier z
			Where 
				z.PeriodID          = @PeriodID 
			And   
				z.CurrencyID        = (Select ID From SystemCurrency Where IsDefault = 1)
			And   
				(z.NumTransactions > 0 or z.NumCorrections > 0)
			And  
				(
					z.GrossSalesAmount <> 0 
				Or 
					(
						Select 
							Count(*)
						From 
							CashBalCashierTen
						Where 
							PeriodID   = @PeriodID 
						And   
							CurrencyID = 
								(
									Select 
										ID 
									From 
										SystemCurrency 
										Where 
									IsDefault = 1
								)
						And   
							CashierID  = z.CashierID
					) > 0
				)
		) b
	On 
		b.CashierID   = a.ID
	--cashiers already assigned floats
	Left Outer Join 
		(
			Select 
				AccountabilityID
			From 
				SafeBags
			Where 
				[Type]      = 'F'
			And   
				[State]     = 'R'
			And   
				OutPeriodID = @PeriodID
		) c
	On c.AccountabilityID = a.ID 
	--cashiers already assigned a end of day pickup bag
	Left Outer Join 
		(
			Select 
				*
			From 
				SafeBags
			Where 
				[Type]              = 'P'
			And   
				[State]            <> 'C'
			And   
				PickupPeriodID      = @PeriodID
			And   
				IsNull(CashDrop, 0) = 0
		) d
	On 
		d.AccountabilityID = a.ID 
	Left Outer Join 
		RSCASH e
	On 
		a.ID = e.CASH
Where 
	a.IsDeleted = 0                  --remove delete cashiers
And   
	c.AccountabilityID Is Null       --remove cashiers assigned floats
And   
	d.AccountabilityID Is Not Null   --cashiers assigned a e.o.d pickup bag
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingUnFloatedPickupList for US10647 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingUnFloatedPickupList for US10647 has not been deployed'
GO
