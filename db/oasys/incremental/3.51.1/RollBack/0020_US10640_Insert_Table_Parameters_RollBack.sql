IF EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = 52)
DELETE FROM dbo.Parameters WHERE ParameterID = 52

If @@Error = 0
   Print 'Success: Delete from table Parameters for US10640 has been deployed successfully'
Else
   Print 'Failure: Delete from table Parameters for US10640 has not been deployed successfully'
GO