DECLARE @MenuID int = 3150

DELETE FROM dbo.ProfilemenuAccess
WHERE [MenuConfigID] = @MenuID

DELETE FROM dbo.MenuConfig
WHERE ID = @MenuID

If @@Error = 0
   Print 'Success: The Delete from MenuConfig table for US10647 has been deployed successfully'
Else
   Print 'Failure: The Delete from MenuConfig table for US10647 has not been deployed'
GO
