DELETE FROM dbo.ReportColumn
WHERE Id IN (42600, 42553)
GO

If @@Error = 0
   Print 'Success: Delete from Table ReportColumn for US10307 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportColumn for US10307 has not been deployed successfully'
Go