IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'QODDeliveryIncome') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure QODDeliveryIncome'
	EXEC ('CREATE PROCEDURE dbo.QODDeliveryIncome AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure QODDeliveryIncome')
GO
-- =============================================
-- Author:		Mike O'Cain
-- Create date: 07/10/2009
-- Description:	Delivery Income Detail
-- =============================================
ALTER PROCEDURE [dbo].[QODDeliveryIncome] 
	@DateStart datetime = NULL, 
	@DateEnd datetime = NULL
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT 
		   convert(varchar,vc.DELD,103)		as 'Date'
		 , DATENAME(dw,vc.DELD)				as 'Day'	 
		 , vc.Numb							as 'OrderNumber'
	     , vc.REVI							as 'RevisionNumber'
	     , vc.NAME							as 'CustomerName'
	     , vc.POST							as 'PostCode'
		 , vc.DCST							as 'DeliveryCharge'
		 , vc.WGHT							as 'Weight'
		 , vc.VOLU							as 'Volume'
		 , su.Name							as 'OrderTaker'
		 --Tables CORHDR and CORHDR4 have been replaced with the view vwCORHDRFull
	     FROM vwCORHDRFull as vc 
	     INNER JOIN SystemUsers as su on vc.OEID = su.EmployeeCode
	     WHERE vc.SHOW_IN_UI = 1 and vc.DELI = 1 and (vc.DELD >= @DateStart and vc.DELD <= @DateEnd)
		 ORDER BY vc.DELD
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure QODDeliveryIncome for US12062 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure QODDeliveryIncome for US12062 has not been deployed'
GO