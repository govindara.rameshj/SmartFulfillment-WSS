IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'usp_GetRefundListDetails') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure usp_GetRefundListDetails'
	EXEC ('CREATE PROCEDURE dbo.usp_GetRefundListDetails AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure usp_GetRefundListDetails')
GO

ALTER PROCEDURE [dbo].[usp_GetRefundListDetails] @ReportDate AS date
AS
BEGIN

    SELECT CM.EmployeeCode , 
           CM.Name , 
           DT.TCOD , 
           DT.TOTL , 
           DT.RMAN , 
           DT.TILL , 
           DT.[TIME] , 
           DT.[TRAN] , 
           DT.SUPV , 
           DT.RSUP , 
           DT.RCAS RefundCashier , 
           RC.Name RefundCashierName , 
           SU.Name SupervisorName , 
           RM.Name ManagerName
      FROM
           SystemUsers CM 
			INNER JOIN DLTOTS DT 
				ON DT.CASH = CM.EmployeeCode
					AND DT.DATE1 = @ReportDate
					AND DT.VOID = 0
					AND DT.TMOD = 0
					AND (DT.TCOD = 'SA'
						OR DT.TCOD = 'RF')
            LEFT OUTER JOIN SystemUsers RC
				ON RC.EmployeeCode = DT.RCAS
            LEFT OUTER JOIN SystemUsers SU
				ON SU.EmployeeCode = DT.RSUP
            LEFT OUTER JOIN SystemUsers RM
				ON RM.EmployeeCode = DT.RMAN
			LEFT JOIN vwCORHDRFull CH
				ON DT.ORDN = CH.NUMB 
	  WHERE CH.IS_CLICK_AND_COLLECT IS NULL OR CH.IS_CLICK_AND_COLLECT = 0
      ORDER BY CM.EmployeeCode , DT.[TIME] , DT.TILL , DT.[TRAN];

END;

GO

If @@Error = 0
   Print 'Success: The Deleting of Stored Procedure usp_GetRefundListDetails for US10277 has been deployed successfully'
Else
   Print 'Failure: The Deleting of Stored Procedure usp_GetRefundListDetails for US10277 has not been deployed'
GO