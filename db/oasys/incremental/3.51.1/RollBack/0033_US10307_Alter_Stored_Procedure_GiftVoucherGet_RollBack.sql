IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'GiftVoucherGet') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure GiftVoucherGet'
	EXEC ('CREATE PROCEDURE dbo.GiftVoucherGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure GiftVoucherGet')
GO

ALTER procedure [dbo].[GiftVoucherGet]
	@Date	date = null
as
begin

select
	dg.DATE1	as 'Date',
	dg.TILL		as 'TillId',
	dg.[TRAN]	as 'TranNumber',
	dg.SERI		as 'SerialNumber',
	case dg.[TYPE]
		when 'SA' then 'Allocation'
		when 'TR' then 'Allocation'
		when 'CR' then 'Allocation'
		when 'CC' then 'Allocation'
		when 'VR' then 'Allocation'
		when 'TS' then 'Redemption'
		when 'RR' then 'Redemption'
		when 'CS' then 'Redemption'
		when 'VS' then 'Redemption'
	end			as 'VoucherType'
from
	DLGIFT dg
where
	@Date is null or (@Date is not null and dg.DATE1 = @Date) 
union
select
	DATE1	as 'Date',
	TILL	as 'TillId',
	[TRAN]	as 'TranNumber',
	CARDNUM	as 'SerialNumber',
	case [TYPE]
		when 'SA' then 'Allocation'
		when 'TR' then 'Allocation'
		when 'CR' then 'Allocation'
		when 'CC' then 'Allocation'
		when 'VR' then 'Allocation'
		when 'TS' then 'Redemption'
		when 'RR' then 'Redemption'
		when 'CS' then 'Redemption'
		when 'VS' then 'Redemption'
	end		as 'VoucherType'
from 
	dlgiftcard 
where
	@Date is null or (@Date is not null and DATE1 = @Date) 
order by
	DATE1,TILL,[TRAN]
end

GO

If @@Error = 0
   Print 'Success: The Deleting of Stored Procedure GiftVoucherGet for US10307 has been deployed successfully'
Else
   Print 'Failure: The Deleting of Stored Procedure GiftVoucherGet for US10307 has not been deployed'
GO