IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('DLTOTS') AND name = 'IX_DLTOTS_SourceTranId')
BEGIN 
	PRINT 'Dropping index IX_DLTOTS_SourceTranId from table DLTOTS'
	DROP INDEX [IX_DLTOTS_SourceTranId] ON dbo.DLTOTS
END

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('DLTOTS') AND name = 'IX_DLTOTS_Source_SourceTranId')
BEGIN 
	PRINT 'Dropping index IX_DLTOTS_Source_SourceTranId from table DLTOTS'
	DROP INDEX [IX_DLTOTS_Source_SourceTranId] ON dbo.DLTOTS
END

IF EXISTS (select 1 from sys.columns where object_id = object_id('DLTOTS') and name = 'SourceTranId')
BEGIN
	PRINT 'Dropping SourceTranId column from DLTOTS'
	ALTER TABLE dbo.DLTOTS DROP COLUMN [SourceTranId]
END

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLTOTS') and name = 'SourceTranNumber')
BEGIN
	PRINT 'Dropping SourceTranNumber column from DLTOTS'
	ALTER TABLE dbo.DLTOTS DROP COLUMN [SourceTranNumber]
END

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLTOTS') and name = 'Source')
BEGIN
	PRINT 'Dropping Source column from DLTOTS'
	ALTER TABLE dbo.DLTOTS DROP COLUMN [Source]
END

If @@Error = 0
   Print 'Success: Altering table DLTOTS for US9584 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table DLTOTS for US9584 has not been rolled back successfully'
GO	
