IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'SaleOrderLineInsert') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure SaleOrderLineInsert'
	EXEC ('CREATE PROCEDURE dbo.SaleOrderLineInsert AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SaleOrderLineInsert')
GO

ALTER PROCEDURE [dbo].[SaleOrderLineInsert]
@OrderNumber CHAR (6), @Number CHAR (4), @SkuNumber CHAR (6), @QtyOrdered DECIMAL (5)=0, @QtyTaken DECIMAL (5)=0, @QtyRefunded DECIMAL (5)=0, @Price DECIMAL (7, 2)=0, @IsDeliveryChargeItem BIT=0, @Weight DECIMAL (7, 2)=0, @Volume DECIMAL (7, 2)=0, @PriceOverrideCode INT=0, @DeliveryStatus INT=0, @SellingStoreId INT=0, @SellingStoreOrderId INT=0, @QtyToDeliver INT=0, @DeliverySource CHAR (4)=null, @DeliverySourceIbtOut CHAR (6)=null, @SellingStoreIbtIn CHAR (6)=null,@SourceOrderLineNo INT,@QtyScanned INT
AS
BEGIN
	SET NOCOUNT ON;
	
	--insert order line
	insert into	CORLIN 
		(
		NUMB,
		LINE,
		SKUN,
		QTYO,
		QTYT,
		QTYR,
		Price,
		IsDeliveryChargeItem,
		WGHT,
		VOLU,
		PORC,
		DeliveryStatus,
		SellingStoreId,
		SellingStoreOrderId,
		QtyToBeDelivered,
		DeliverySource,
		DeliverySourceIbtOut,
		SellingStoreIbtIn,
		-- added for hubs 2.0
		QuantityScanned
		)
	values		(
		@orderNumber,
		@number,
		@skuNumber,
		@qtyOrdered,
		@qtyTaken,
		@qtyRefunded,
		@price,
		@isDeliveryChargeItem,
		@weight,
		@volume,
		@priceOverrideCode,
		@deliveryStatus,
		@sellingStoreId,
		@sellingStoreOrderId,
		@QtyToDeliver,
		@deliverySource,
		@deliverySourceIbtOut,
		@sellingStoreIbtIn,
		@QtyScanned
	)
	-- added for hubs 2.0
	INSERT INTO CORLIN2
	(
	NUMB,
	LINE,
	SourceOrderLineNo
	)
	VALUES
	(
	 @orderNumber,
	 @Number,
	 @SourceOrderLineNo
	)
	
	return @@rowcount
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SaleOrderLineInsert for US11506 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SaleOrderLineInsert for US11506 has not been deployed'
GO