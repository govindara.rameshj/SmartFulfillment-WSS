IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'SalesGetVoucherSales') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure SalesGetVoucherSales'
	EXEC ('CREATE PROCEDURE dbo.SalesGetVoucherSales AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SalesGetVoucherSales')
GO

ALTER PROCEDURE [dbo].[SalesGetVoucherSales] 
	@Date		DateTime
AS
BEGIN
	SET NOCOUNT ON;

	select 
		DT.CASH		as 'UserId',
		SU.Name		as 'UserName',
		DT.TILL		as 'TillId',
		DT.[TRAN]	as 'TranNumber',
		DT.TOTL		as 'SaleValue',
		DP.COPN		as 'CouponNumber',
		DP.AMNT		as 'TenderValue',
		convert	(Decimal(9,2),	case DT.TOTL
									when 0 then 0
									else(( DP.AMNT / DT.TOTL ) * 100 )
								end)		as 'VoucherPercent'
	from 
		DLTOTS as DT
	inner join 
		SystemUsers as SU on DT.CASH = SU.EmployeeCode
	inner join 
		DLPAID as DP	on	dt.date1 = dp.date1
						and	dt.till	 = dp.till	
						and	DT.[TRAN] = DP.[TRAN] 
						and dp.copn <> '000000'
	where
		dt.date1 = convert(date, @Date)
  
END

GO

If @@Error = 0
   Print 'Success: The Deleting of Stored Procedure SalesGetVoucherSales for US10307 has been deployed successfully'
Else
   Print 'Failure: The Deleting of Stored Procedure SalesGetVoucherSales for US10307 has not been deployed'
GO