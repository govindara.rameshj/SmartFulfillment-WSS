IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'SystemNumbersGetNext') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure SystemNumbersGetNext'
    EXEC ('CREATE PROCEDURE dbo.SystemNumbersGetNext AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SystemNumbersGetNext')
GO

ALTER PROCEDURE [dbo].[SystemNumbersGetNext]
@Id INT
AS
BEGIN
    SET NOCOUNT ON;

    declare @Number int;
    declare @NextNumber int;
    
    --check if for qod (ie 10)
    --updated for hubs 2.0
    if @Id in (10, 16)
        begin
            if @Id = 10
            begin
                set @Number = (select next15 from SYSNUM where FKEY='01');
                set @NextNumber = @Number +1;
                if @NextNumber + 1 > 999999 set @NextNumber = 1;
                
                update SYSNUM set NEXT15 = replace(str(@NextNumber,6),' ','0') where FKEY='01';
            end
            else --
            begin
               declare @TillNo INT, @TranNo INT
               set @Number = (select NEXT16 from SYSNUM where FKEY='01');
               SET @TillNo =  CONVERT(INT,SUBSTRING(CONVERT(CHAR,@Number),0,2)) 
               SET @TranNo =  CONVERT(INT,SUBSTRING(CONVERT(CHAR,@Number),2,4))  
               If @TranNo < 9999
                 BEGIN
                  SET @TranNo = @TranNo+1
                 END
                ELSE
                 BEGIN
                  SET @TranNo = 1
                   IF @TillNo <10 
                     BEGIN
                      SET @TillNo = @TillNo +1
                     END
                     ELSE
                     BEGIN
                     SET @TillNo = 1
                     END
                  END   
              update SYSNUM 
              set NEXT16 = CONVERT(CHAR,STUFF(@TillNo, 1, 0, REPLICATE('0', 2 - LEN(@TillNo))) + STUFF(@TranNo, 1, 0, REPLICATE('0', 4 - LEN(@TranNo))))
              where FKEY='01';          
            end
            return @Number;
        end
    else
        begin
            set @Number = (select NextNumber from SystemNumbers where ID = @Id);
            set @NextNumber = @Number +1;
            if @NextNumber + 1 > 999999 set @NextNumber = 1;
            
            update SystemNumbers set NextNumber = @NextNumber where ID = @Id;
            
            return @Number;     
        end
    
END

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SystemNumbersGetNext for US10277 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SystemNumbersGetNext for US10277 has not been deployed'
GO