IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'SaleOrderUpdate') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure SaleOrderUpdate'
	EXEC ('CREATE PROCEDURE dbo.SaleOrderUpdate AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure SaleOrderUpdate')
GO

ALTER PROCEDURE [dbo].[SaleOrderUpdate]

@CustomerName VARCHAR(50)=null,
@OrderNumber CHAR (6), 
@DateDelivery DATE=null, 
@DateDespatch DATE=null, 
@DeliveryAddress1 VARCHAR (60), 
@DeliveryAddress2 VARCHAR (30)=null, 
@DeliveryAddress3 VARCHAR (30), 
@DeliveryAddress4 VARCHAR (30)=null, 
@DeliveryPostCode VARCHAR (8)=null, 
@PhoneNumber VARCHAR(20)=null,
@PhoneNumberHome VARCHAR (20)=null, 
@PhoneNumberMobile VARCHAR (15)=null, 
@DeliveryStatus INT, 
@RefundStatus INT, 
@CustomerEmail VARCHAR (100)=null, 
@PhoneNumberWork VARCHAR (20)=null, 
@OmOrderNumber INT, 
@IsSuspended BIT, 
@RevisionNumber INT=0,
@DeliveryConfirmed BIT=0, 
@IsPrinted BIT, 
@NumberReprints DECIMAL (3),
@QtyOrdered INT,
@QtyReturned INT,
@QtyTaken INT,
@CancelledStatus CHAR(1) = '0',
@Source Char(2) = null, 
@SourceOrderNumber Char(20)=null,
@RefundDate Date = null,
@RefundTill Char(2) = null,
@RefundTran Char(4) = null
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @rowcount int;
	set @rowcount=0;	

	--update corhdr with delivery date
	update
		CORHDR
	set
		NAME	= @CustomerName,
		DELD	= @DateDelivery,
		ADDR1	= @DeliveryAddress1,
		ADDR2	= @DeliveryAddress2,
		ADDR3	= @DeliveryAddress3,
		ADDR4	= @DeliveryAddress4,
		POST	= @DeliveryPostCode,
		PHON	= @PhoneNumber,
		MOBP	= @PhoneNumberMobile,
		REVI	= @RevisionNumber,
		DELC	= @DeliveryConfirmed,
		PRNT	= @IsPrinted,
		RPRN	= @NumberReprints,
		QTYO    = @QtyOrdered,
		QTYR    = @QtyReturned,
		QTYT    = @QtyTaken,
		CANC    = @CancelledStatus,
		RDAT	= @RefundDate,
		RTIL	= @RefundTill,
		RTRN	= @RefundTran,
		HOMP	= @PhoneNumberHome

	where
		NUMB	= @OrderNumber;

	--get number of rows affected
	set @rowcount = @@ROWCOUNT;

	--update corhdr4 with info
	update
		CORHDR4
	set
		NAME			= @CustomerName,
		DELD			= @DateDelivery,
		DDAT			= @DateDespatch,
		DeliveryStatus	= @DeliveryStatus,
		RefundStatus	= @RefundStatus,
		CustomerEmail	= @CustomerEmail,
		PhoneNumberWork	= @PhoneNumberWork,
		IsSuspended		= @IsSuspended,
		OMOrderNumber	= @OmOrderNumber

	where
		NUMB	= @OrderNumber;

	--update corhdr5 with info
	Update 
		CORHDR5 
	set
        	Source=@Source, 
	        SourceOrderNumber= @SourceOrderNumber
	where
		NUMB	= @OrderNumber;

	--get number of rows affected and return
	set @rowcount = @rowcount + @@ROWCOUNT;	
	return @@rowcount
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SaleOrderUpdate for US11506 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure SaleOrderUpdate for US11506 has not been rolled back'
GO