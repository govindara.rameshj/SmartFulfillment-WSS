IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('SystemNumbers') and name = 'Min')
    BEGIN
        PRINT 'Drop Min column to SystemNumbers'
        ALTER TABLE dbo.SystemNumbers
        DROP COLUMN [Min]
    END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('SystemNumbers') and name = 'Max')
    BEGIN
        PRINT 'Drop Max column to SystemNumbers'
        ALTER TABLE dbo.SystemNumbers
        DROP COLUMN [Max]
    END
GO
  
IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('SystemNumbers') and name = 'Suffix')
    BEGIN
        PRINT 'Add Suffix column to SystemNumbers'
        ALTER TABLE dbo.SystemNumbers
        ADD Suffix char(10) NULL
    END
GO
  
IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('SystemNumbers') and name = 'Prefix')
    BEGIN
        PRINT 'Add Prefix column to SystemNumbers'
        ALTER TABLE dbo.SystemNumbers
        ADD Prefix char(10) NULL
    END
GO

If @@Error = 0
   Print 'Success: The Alter SystemNumbers table for US10277 has been deployed successfully'
Else
   Print 'Failure: The Alter SystemNumbers table for US10277 has not been deployed'
GO