If EXISTS (SELECT 1 FROM dbo.Report WHERE Id = 330)
    BEGIN  
        DELETE FROM dbo.Report WHERE Id = 330
    END
GO

If @@Error = 0
   Print 'Success: The Delete into Report table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Delete into Report table for US22216 has not been deployed'
GO