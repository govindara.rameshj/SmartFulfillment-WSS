SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'CheckBankingStarted') AND type in (N'P'))
BEGIN
    PRINT 'Dropping procedure CheckBankingStarted'
    DROP PROCEDURE dbo.[CheckBankingStarted];
END
GO

IF @@Error = 0
   Print 'Success: The Drop procedure CheckBankingStarted for US22361 has been deployed successfully'
ELSE
   Print 'Failure: The Drop procedure CheckBankingStarted for US22361 has not been deployed successfully'
GO