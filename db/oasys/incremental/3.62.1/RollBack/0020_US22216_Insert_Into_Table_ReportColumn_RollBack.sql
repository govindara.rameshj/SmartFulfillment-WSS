IF EXISTS (SELECT 1 FROM ReportColumn WHERE Id = 99)
    BEGIN
        DELETE FROM ReportColumn WHERE Id = 99
    END
GO

IF EXISTS (SELECT 1 FROM ReportColumn WHERE Id = 229)
    BEGIN
        DELETE FROM ReportColumn WHERE Id = 229
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportColumn table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportColumn table for US22216 has not been deployed'
GO