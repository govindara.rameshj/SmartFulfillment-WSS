IF EXISTS (SELECT 1 FROM dbo.ProfilemenuAccess WHERE MenuConfigID = 10242)
    BEGIN    
        DELETE FROM dbo.ProfilemenuAccess WHERE MenuConfigID = 10242
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ProfilemenuAccess table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ProfilemenuAccess table for US22216 has not been deployed'
GO