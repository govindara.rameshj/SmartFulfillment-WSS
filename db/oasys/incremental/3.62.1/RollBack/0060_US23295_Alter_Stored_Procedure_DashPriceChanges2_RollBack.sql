IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'DashPriceChanges2') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure DashPriceChanges2'
    EXEC ('CREATE PROCEDURE dbo.DashPriceChanges2 AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure DashPriceChanges2')
GO

ALTER procedure [dbo].[DashPriceChanges2]
    @DateEnd Date
as
begin
   set nocount on

   declare @Output table(RowId         int, 
                         [Description] varchar(50), 
                         Qty           int,
                         Peg           int,
                         Small         int,
                         Medium        int)

   declare @PCToday                          int,
           @OverdueQty                       int,
           @OutstandingQty                   int,
           @Peg                              int,
           @Small                            int,
           @Medium                           int,
           @PriceChangesConfirmedDayQuantity int

    DECLARE @PriceChangeDates TABLE (
    SKU char(6),
    EffectiveDate DateTime)
    
    INSERT INTO @PriceChangeDates (SKU,EffectiveDate)
    SELECT SKUN,dbo.udf_GetLatestPriceChangeDateForSku(SKUN)
    FROM PRCCHG
    WHERE PSTA = 'U'
    
    SELECT @PCToday = COUNT(*)
    FROM @PriceChangeDates pcd
    JOIN sysdat sd ON sd.TMDT = pcd.EffectiveDate
    
   set @OutstandingQty                   = dbo.svf_ReportPriceChangesOutstandingQty()
   set @OverdueQty                       = dbo.svf_ReportPriceChangesOverdueLabelsQty()
   set @Peg                              = dbo.svf_ReportPriceChangesLabelsRequiredPeg()
   set @Small                            = dbo.svf_ReportPriceChangesLabelsRequiredSmall()
   set @Medium                           = dbo.svf_ReportPriceChangesLabelsRequiredMedium()
   set @PriceChangesConfirmedDayQuantity = dbo.svf_ReportPriceChangesConfirmedDAY(@DateEnd)

   insert @Output values (1, 'Price Labels Action Required',  @OutstandingQty,                   null, null,   null)
   insert @Output values (2, 'Overdue Labels',  @OverdueQty,                       null, null,   null)
   insert @Output values (3, 'Labels Required',                        (@Peg + @Small + @Medium),          @Peg, @Small, @Medium)
   insert @Output values (4, 'Outstanding Price Label Report',                     @PCToday,                          null, null,   null)
   insert @Output values (5, 'Price Change Audit Report',              @PriceChangesConfirmedDayQuantity, null, null,   null)
   insert @Output values (6, 'Labels Request',                          null,                              null, null,   null)

   select * from @Output order by RowId
end
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure DashPriceChanges2 for US has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure DashPriceChanges2 for US has not been rolled back'
GO