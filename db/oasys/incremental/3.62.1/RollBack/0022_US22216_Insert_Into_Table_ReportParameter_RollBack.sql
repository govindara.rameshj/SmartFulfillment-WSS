IF EXISTS (SELECT 1 FROM ReportParameter WHERE Id = 122)
    BEGIN
        DELETE FROM ReportParameter WHERE Id = 122
    END
GO

IF EXISTS (SELECT 1 FROM ReportParameter WHERE Id = 119)
    BEGIN
        DELETE FROM ReportParameter WHERE Id = 119
    END
GO

If @@Error = 0
   Print 'Success: The Delete into ReportParameter table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Delete into ReportParameter table for US22216 has not been deployed'
GO