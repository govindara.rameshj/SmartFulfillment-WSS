IF EXISTS (SELECT 1 FROM dbo.ReportRelation WHERE ReportId = 330 AND Name = 'Explosives')
    BEGIN
        DELETE FROM dbo.ReportRelation WHERE ReportId = 330 AND Name = 'Explosives'
    END
GO

If @@Error = 0
   Print 'Success: The Delete from ReportRelation table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Delete from into ReportRelation table for US22216 has not been deployed'
GO