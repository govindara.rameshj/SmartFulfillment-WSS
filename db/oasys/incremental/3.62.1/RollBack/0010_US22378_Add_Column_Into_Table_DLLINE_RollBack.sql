IF EXISTS (select 1 from sys.columns col inner join sys.objects obj on obj.object_id = col.object_id where obj.Name = 'DLLINE' and col.name = 'IsExplosive')
BEGIN
    PRINT 'Dropping IsExplosive column from DLLINE'
    ALTER TABLE dbo.DLLINE DROP CONSTRAINT DF_DLLINE_IsExplosive
    ALTER TABLE dbo.DLLINE DROP COLUMN IsExplosive
END

If @@Error = 0
   Print 'Success: Altering table DLLINE for US22378 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table DLLINE for US22378 has not been rolled back successfully'
GO