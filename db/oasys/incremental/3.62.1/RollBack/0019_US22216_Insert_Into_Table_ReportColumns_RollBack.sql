IF EXISTS (SELECT 1 FROM ReportColumns WHERE ReportId = 330)
    BEGIN
        DELETE FROM ReportColumns WHERE ReportId = 330
    END
GO

If @@Error = 0
   Print 'Success: The Delete from ReportColumns table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Delete from ReportColumns table for US22216 has not been deployed'
GO