IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesOverdueLabelsQty') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesOverdueLabelsQty'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesOverdueLabelsQty() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportPriceChangesOverdueLabelsQty')
GO

ALTER FUNCTION [dbo].[svf_ReportPriceChangesOverdueLabelsQty]
(
)
RETURNS int
AS
BEGIN

    DECLARE @OutstandingQty INT
    
    SELECT @OutstandingQty = count(sm.SKUN)
    FROM PRCCHG AS Pc
    INNER JOIN STKMAS AS sm ON sm.SKUN = pc.SKUN
    INNER JOIN SYSDAT AS sd ON sd.FKEY = '01'
    WHERE pc.PSTA = 'U' AND NOT ((sm.INON = '1' or sm.IOBS = '1' or sm.AAPC = '1') and sm.ONHA + sm.MDNQ = 0) AND SHEL = '1' AND pc.PDAT <= sd.TMDT
    
    SET @OutstandingQty = ISNULL(@OutstandingQty, 0);
        
    RETURN          @OutstandingQty

END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesOverdueLabelsQty for US has been rolled back successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesOverdueLabelsQty for US has not been rolled back'
GO