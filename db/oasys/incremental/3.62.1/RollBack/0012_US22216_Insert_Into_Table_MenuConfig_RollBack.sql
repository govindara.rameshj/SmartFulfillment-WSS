IF EXISTS (SELECT 1 FROM dbo.MenuConfig WHERE ID = 10242)
    BEGIN
        DELETE FROM dbo.MenuConfig WHERE ID = 10242
    END
GO

If @@Error = 0
   Print 'Success: The Delete from MenuConfig table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Delete from MenuConfig table for US22216 has not been deployed'
GO
