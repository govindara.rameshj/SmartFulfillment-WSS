IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesLabelsRequiredMedium') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesLabelsRequiredMedium'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesLabelsRequiredMedium() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportPriceChangesLabelsRequiredMedium')
GO
-- =================================================================================================
-- Author       : Kevan Madelin
-- Version      : 1.0
-- Create date  : 8th June 2011
-- Description  : Returns the figure to use for Price Changes - Labels Required MEDIUM - Scalar Valued Function
-- Notes        : This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
-- Author       : Sean Moir
-- Version      : 1.1
-- Create date  : 7th July 2011
-- Description  : Changed to return sum of STKMAS.LABL
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportPriceChangesLabelsRequiredMedium]
(
)
RETURNS int
AS
BEGIN

    Declare         @Medium         int,
                    @PCDate         date
                    
    Set             @PCDate         =       Oasys.dbo.svf_SystemPriceChangeVisibilityDate()
                    
    ----------------------------------------------------------------------------------
    -- Retrieve Count for Price Change Labels Required - MEDIUM
    ----------------------------------------------------------------------------------
    Select          @Medium         =       sum(stk.LABL)
    From            PRCCHG prc
        inner join STKMAS stk
            on prc.SKUN = stk.SKUN
    Where           PSTA            =       'U'
                    and prc.LABL    =       1
                    and PDAT        <=      @PCDate

    Set             @Medium         =       isnull(@Medium, 0);
        
    RETURN          @Medium

END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesLabelsRequiredMedium for US has been rolled back successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesLabelsRequiredMedium for US has not been rolled back'
GO
