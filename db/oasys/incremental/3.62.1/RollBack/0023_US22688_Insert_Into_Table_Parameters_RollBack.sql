IF EXISTS (SELECT 1 FROM [dbo].[Parameters] where ParameterID = 6667)
    BEGIN
        DELETE FROM [dbo].[Parameters] where ParameterID = 6667
    END
GO

If @@Error = 0
   Print 'Success: The Delete into Parameters table for US22688 has been deployed successfully'
Else
   Print 'Failure: The Delete into Parameters table for US22688 has not been deployed'
GO