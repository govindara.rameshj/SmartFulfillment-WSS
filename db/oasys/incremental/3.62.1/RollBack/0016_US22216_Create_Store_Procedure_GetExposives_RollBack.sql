IF EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'GetExplosives') AND type in (N'P', N'PC'))
BEGIN
    PRINT ('Drop of procedure GetExplosives')
    DROP PROCEDURE GetExplosives
END
GO

If @@Error = 0
   Print 'Success: The Drop of Stored Procedure GetExplosives for US22216 has been deployed successfully'
Else
   Print 'Failure: The Drop of Stored Procedure GetExplosives for US22216 has not been deployed'
GO