IF EXISTS (SELECT 1 FROM ReportTable WHERE ReportId = 330)
    BEGIN
        DELETE FROM ReportTable WHERE ReportId = 330
    END
GO

If @@Error = 0
   Print 'Success: The Delete into ReportTable table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Delete into ReportTable table for US22216 has not been deployed'
GO