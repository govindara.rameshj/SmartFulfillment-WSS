IF EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterID = 6666)
    BEGIN
        DELETE FROM dbo.[Parameters] WHERE ParameterID = 6666
    END
GO

If @@Error = 0
   Print 'Success: The Delete into Parameters table for US22378 has been deployed successfully'
Else
   Print 'Failure: The Delete into Parameters table for US22378 has not been deployed'
GO