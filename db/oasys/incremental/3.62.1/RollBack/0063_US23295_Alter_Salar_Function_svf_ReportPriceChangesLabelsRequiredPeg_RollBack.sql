IF NOT EXISTS (SELECT * FROM sys.objects
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesLabelsRequiredPeg') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesLabelsRequiredPeg'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesLabelsRequiredPeg() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportPriceChangesLabelsRequiredPeg')
GO
-- =================================================================================================
-- Author       : Kevan Madelin
-- Version      : 1.0
-- Create date  : 8th June 2011
-- Description  : Returns the figure to use for Price Changes - Labels Required PEG - Scalar Valued Function
-- Notes        : This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
-- Author       : Sean Moir
-- Version      : 1.1
-- Create date  : 7th July 2011
-- Description  : Changed to return sum of STKMAS.LABN
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportPriceChangesLabelsRequiredPeg]
(
)
RETURNS int
AS
BEGIN

    Declare         @Peg            int,
                    @PCDate         date

    Set             @PCDate         =       Oasys.dbo.svf_SystemPriceChangeVisibilityDate()

    ----------------------------------------------------------------------------------
    -- Retrieve Count for Price Change Labels Required - PEG
    ----------------------------------------------------------------------------------
    Select          @Peg            =       sum(stk.LABN)
    From            PRCCHG prc
        inner join STKMAS stk
            on prc.SKUN = stk.SKUN
    Where           PSTA            =       'U'
                    and prc.LABS    =       1
                    and PDAT        <=      @PCDate

    Set             @Peg            =       isnull(@Peg, 0);

    RETURN          @Peg

END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesLabelsRequiredPeg for US has been rolled back successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesLabelsRequiredPeg for US has not been rolled back'
GO
