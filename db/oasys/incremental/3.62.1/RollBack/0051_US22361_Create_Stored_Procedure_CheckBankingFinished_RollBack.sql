SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'CheckBankingFinished') AND type in (N'P'))
BEGIN
    PRINT 'Dropping procedure CheckBankingFinished'
    DROP PROCEDURE dbo.[CheckBankingFinished];
END
GO

IF @@Error = 0
   Print 'Success: The Drop procedure CheckBankingFinished for US22361 has been deployed successfully'
ELSE
   Print 'Failure: The Drop procedure CheckBankingFinished for US22361 has not been deployed successfully'
GO