IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'GetExplosives') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure GetExplosives'
    EXEC ('CREATE PROCEDURE dbo.GetExplosives AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure GetExplosives')
GO

ALTER PROCEDURE [dbo].GetExplosives
    @DateStart      date = null, 
    @DateEnd        date = null,
    @TillNumber     int = null,
    @TranNumber     int = null,
    @OVCTranNumber  varchar(20) = null,
    @SkuNumber      char(6) = null
AS
BEGIN
    IF @TillNumber = 0
    SET @TillNumber = NULL

    IF @TranNumber = 0 
    SET @TranNumber = NULL

    IF LTRIM(@SkuNumber) = '' 
    SET @SkuNumber = NULL
    
    IF LTRIM(@OVCTranNumber) = '' 
    SET @OVCTranNumber = NULL
    
    SET NOCOUNT ON;
    
    SELECT 
        dt.DATE1            'Date',
        dt.TILL             'TillId',
        dt.[TRAN]           'TranNumber',
        dt.OVCTranNumber    'OVCTranNumber',
        dt.[TIME]           'Time',
        dt.CASH             'CashierId'
    FROM dbo.vwDLTOTS dt
    WHERE
        (EXISTS (SELECT 1 FROM DLLINE WHERE (@SkuNumber IS NULL OR SKUN = @SkuNumber) AND DATE1 = dt.DATE1 AND TILL = dt.TILL AND [TRAN] = dt.[TRAN] AND IsExplosive = 1))
        AND dt.DATE1 BETWEEN @DateStart AND @DateEnd
        AND (@TillNumber IS NULL OR dt.TILL = @TillNumber)
        AND (@TranNumber IS NULL OR dt.[TRAN] = @TranNumber)
        AND (@OVCTranNumber IS NULL OR dt.OVCTranNumber = @OVCTranNumber)
        
    SELECT 
        dl.DATE1         as 'Date',
        dl.TILL          as 'TillId',
        dl.[TRAN]        as 'TranNumber',
        dl.SKUN          as 'SkuNumber',
        sm.DESCR         as 'Description',
        dl.QUAN          as 'Qty'
    FROM dbo.DLLINE dl
    LEFT JOIN dbo.STKMAS sm ON sm.SKUN = dl.SKUN
    WHERE
        (@SkuNumber IS NULL OR dl.SKUN = @SkuNumber)
        AND dl.DATE1 BETWEEN @DateStart AND @DateEnd
        AND (@TillNumber IS NULL OR dl.TILL = @TillNumber)
        AND (@TranNumber IS NULL OR dl.[TRAN] = @TranNumber)
        AND IsExplosive = 1
        AND (EXISTS (SELECT 1 FROM dbo.vwDLTOTS WHERE (@OVCTranNumber IS NULL OR OVCTranNumber = @OVCTranNumber) AND DATE1 = dl.DATE1 AND TILL = dl.TILL AND [TRAN] = dl.[TRAN]))
    order by dl.DATE1,dl.TILL,dl.[TRAN] desc, dl.NUMB
END

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure GetExplosives for US22216 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure GetExplosives for US22216 has not been deployed'
GO
