IF NOT EXISTS (SELECT 1 FROM dbo.MenuConfig WHERE ID = 10242)
    BEGIN
        INSERT INTO dbo.MenuConfig (
                ID,
                MasterID,
                AppName,
                AssemblyName,
                ClassName,
                MenuType,
                [Parameters],
                ImagePath,
                LoadMaximised,
                IsModal,
                DisplaySequence,
                AllowMultiple,
                WaitForExit,
                ImageKey,
                [Timeout],
                DoProcessingType)
        VALUES (10242, 10200, 'Explosives Report', 'Reporting.dll', 'Reporting.ReportViewer', 4, '330', 'icons\icon_g.ico', 1,  1,  2,  0, 0, 0, 0, 0)               
    END
GO

If @@Error = 0
   Print 'Success: The Insert into MenuConfig table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into MenuConfig table for US22216 has not been deployed'
GO