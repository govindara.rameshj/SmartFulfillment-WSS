If NOT EXISTS (SELECT 1 FROM dbo.Report WHERE Id = 330)
    BEGIN  
        INSERT INTO dbo.Report (Id, Header, Title, ProcedureName, HideWhenNoData, PrintLandscape, ShowResetButton)
        VALUES (330, 'Sales', 'Explosives Report', 'GetExplosives', 0, 1, 1)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Report table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into Report table for US22216 has not been deployed'
GO