IF NOT EXISTS (SELECT 1 FROM ReportParameter WHERE Id = 122)
    BEGIN
        INSERT INTO ReportParameter (Id, [Description], Name, DataType, Size)
        VALUES (122, 'Transaction Number' , 'TranNumber', 3, 4)
    END
GO

IF NOT EXISTS (SELECT 1 FROM ReportParameter WHERE Id = 119)
    BEGIN
        INSERT INTO ReportParameter (Id, [Description], Name, DataType, Size)
        VALUES (119, 'Till Number' , 'TillNumber', 3, 2)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportParameter table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportParameter table for US22216 has not been deployed'
GO