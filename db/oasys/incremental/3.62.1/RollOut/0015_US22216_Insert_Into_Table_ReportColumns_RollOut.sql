IF NOT EXISTS (SELECT 1 FROM ReportColumns WHERE ReportId = 330)
    BEGIN
      INSERT INTO ReportColumns (ReportId, TableId, ColumnId, Sequence, IsVisible, IsBold)
      VALUES
        (330, 1, 2500, 1, 1, 0),
        (330, 1, 151, 2, 1, 0),
        (330, 1, 229, 3, 1, 0),
        (330, 1, 42600, 4, 1, 0),
        (330, 1, 154, 5, 1, 0),
        (330, 2, 151,  1, 0, 0),
        (330, 2, 229, 2, 0, 0),
        (330, 2, 2500, 3, 0, 0),
        (330, 2, 42600, 4, 0, 0),
        (330, 2, 201, 5, 1, 0),
        (330, 2, 99, 6, 1, 0),
        (330, 2, 1100, 7, 1, 0)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportColumns table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportColumns table for US22216 has not been deployed'
GO