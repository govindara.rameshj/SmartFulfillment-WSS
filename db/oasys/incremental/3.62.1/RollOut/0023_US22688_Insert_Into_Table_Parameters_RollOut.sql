IF NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] where ParameterID = 6667)
    BEGIN
        INSERT [dbo].[Parameters] 
            (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES
            (6667, 'Hierarchy Spend Event Discount Code', '000001', 0, 0, 0.00000, 0)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US22688 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US22688 has not been deployed'
GO