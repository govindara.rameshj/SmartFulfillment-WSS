IF NOT EXISTS (SELECT 1 FROM dbo.[Parameters] WHERE ParameterID = 6666)
    BEGIN
        INSERT INTO dbo.[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
        VALUES (6666, 'Quantity Break Event Discount Code', '000001', 0, 0, 0.00000, 0)         
    END
GO

If @@Error = 0
   Print 'Success: The Insert into Parameters table for US22378 has been deployed successfully'
Else
   Print 'Failure: The Insert into Parameters table for US22378 has not been deployed'
GO