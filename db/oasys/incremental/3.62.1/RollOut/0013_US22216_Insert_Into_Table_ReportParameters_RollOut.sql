IF NOT EXISTS (SELECT 1 FROM ReportParameters WHERE ReportId = 330)
    BEGIN
        INSERT INTO ReportParameters (ReportId, ParameterId, Sequence, AllowMultiple, DefaultValue)
        VALUES 
            (330, 102, 1, 0, NULL),
            (330, 103, 2, 0, NULL),
            (330, 119, 3, 0, NULL),
            (330, 122, 4, 0, NULL),
            (330, 353, 5, 0, NULL),
            (330, 200, 6, 0, NULL)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportParameters table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportParameters table for US22216 has not been deployed'
GO