IF NOT EXISTS (SELECT 1 FROM ReportColumn WHERE Id = 99)
    BEGIN
        INSERT INTO ReportColumn (Id, Name, Caption, FormatType, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES 
            (99, 'Description', 'Description', 0, 0, 500, 600, 1)
    END
GO

IF NOT EXISTS (SELECT 1 FROM ReportColumn WHERE Id = 229)
    BEGIN
        INSERT INTO ReportColumn (Id, Name, Caption, FormatType, IsImagePath, MinWidth, MaxWidth, Alignment)
        VALUES 
            (229, 'TranNumber', 'Transaction Number', 1, 0, 100, NULL, 2)
    END
GO


If @@Error = 0
   Print 'Success: The Insert into ReportColumn table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportColumn table for US22216 has not been deployed'
GO