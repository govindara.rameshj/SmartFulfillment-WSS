SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'CheckBankingFinished') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure CheckBankingFinished'
    EXEC ('CREATE PROCEDURE CheckBankingFinished AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure CheckBankingFinished')
GO

ALTER PROCEDURE CheckBankingFinished (@bankingDate DATETIME, @bankingFinished bit = 0 Output)
AS
BEGIN
    DECLARE @periodID INT
    SET @periodID = (SELECT ID FROM SystemPeriods WHERE StartDate = CONVERT(varchar(10), @bankingDate, 121))

    IF EXISTS(SELECT 1 
              FROM Safe
              WHERE [PeriodID] = @periodID AND [IsClosed] = 1)
        SET @bankingFinished = 1 
    ELSE
        SET @bankingFinished = 0
END
GO

IF @@Error = 0
   PRINT 'Success: The Create Stored Procedure CheckBankingFinished for US22361 has been deployed successfully'
ELSE
   PRINT 'Failure: The Create Stored Procedure CheckBankingFinished for US22361 has not been deployed successfully'
GO
