IF NOT EXISTS (SELECT 1 FROM dbo.ReportRelation WHERE ReportId = 330 AND Name = 'Lines with explosive SKUs')
    BEGIN
        INSERT INTO dbo.ReportRelation (ReportId, Name, ParentTable, ParentColumns, ChildTable, ChildColumns)
        VALUES (330, 'Lines with explosive SKUs', 'Header', 'Date,TillId,TranNumber', 'Lines with explosive SKUs', 'Date,TillId,TranNumber')
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportRelation table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportRelation table for US22216 has not been deployed'
GO