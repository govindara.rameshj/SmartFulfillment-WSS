IF NOT EXISTS (SELECT 1 FROM dbo.ProfilemenuAccess WHERE MenuConfigID = 10242)
    BEGIN    
        INSERT INTO    dbo.ProfilemenuAccess (
            [ID],
            [MenuConfigID],
            [AccessAllowed],
            [OverrideAllowed],
            [SecurityLevel],
            [IsManager],
            [IsSupervisor],
            [LastEdited],
            [Parameters]
            )
        SELECT 
                [ID],
                10242,
                [AccessAllowed],
                [OverrideAllowed],
                [SecurityLevel],
                [IsManager],
                [IsSupervisor],
                [LastEdited],
                [Parameters]
        FROM dbo.ProfilemenuAccess
        WHERE MenuConfigID = 10241
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ProfilemenuAccess table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ProfilemenuAccess table for US22216 has not been deployed'
GO