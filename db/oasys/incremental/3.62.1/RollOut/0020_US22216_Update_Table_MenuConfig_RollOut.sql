UPDATE [dbo].[MenuConfig]
SET DisplaySequence = 3
WHERE ID = 10240 AND MasterID = 10200
GO

UPDATE [dbo].[MenuConfig]
SET DisplaySequence = 4
WHERE ID = 10285 AND MasterID = 10200
GO

UPDATE [dbo].[MenuConfig]
SET DisplaySequence = 5
WHERE ID = 10270 AND MasterID = 10200
GO

UPDATE [dbo].[MenuConfig]
SET DisplaySequence = 6
WHERE ID = 10250 AND MasterID = 10200
GO

UPDATE [dbo].[MenuConfig]
SET DisplaySequence = 7
WHERE ID = 10280 AND MasterID = 10200
GO

UPDATE [dbo].[MenuConfig]
SET DisplaySequence = 8
WHERE ID = 10286 AND MasterID = 10200
GO