SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'CheckBankingStarted') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure CheckBankingStarted'
    EXEC ('CREATE PROCEDURE CheckBankingStarted AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure CheckBankingStarted')
GO

ALTER PROCEDURE CheckBankingStarted (@bankingDate DATETIME, @bankingStarted bit = 0 Output)
AS
BEGIN
    DECLARE @dateToCheck DATE
    SET @dateToCheck = CONVERT(varchar(10), @bankingDate, 121)
    
    IF EXISTS(SELECT 1 FROM [dbo].[Locks] 
              WHERE [EntityId] = 'NewBanking.Form.BankingPickupCheck' 
                AND CONVERT(varchar(10), [LockTime], 121) = @dateToCheck)
        SET @bankingStarted = 1 
    ELSE
        SET @bankingStarted = 0
END
GO

IF @@Error = 0
   PRINT 'Success: The Create Stored Procedure CheckBankingStarted for US22361 has been deployed successfully'
ELSE
   PRINT 'Failure: The Create Stored Procedure CheckBankingStarted for US22361 has not been deployed successfully'
GO
