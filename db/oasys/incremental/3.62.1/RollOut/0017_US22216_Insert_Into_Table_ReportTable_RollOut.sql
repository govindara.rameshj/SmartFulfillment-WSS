IF NOT EXISTS (SELECT 1 FROM ReportTable WHERE ReportId = 330)
    BEGIN
        INSERT INTO dbo.ReportTable (ReportId, Id, Name, AllowGrouping, AllowSelection, ShowHeaders, ShowIndicator, ShowLinesVertical, ShowLinesHorizontal, ShowBorder, ShowInPrintOnly, AutoFitColumns, HeaderHeight, AnchorTop, AnchorBottom)
        VALUES
        (330, 1, 'Header', 1, 0, 1, 1, 1, 1, 1, 0, 0, 35, 1, 1),
        (330, 2, 'Lines with explosive SKUs', 0, 0, 1, 1, 1, 1, 1, 0, 0, NULL, 1, 1)
    END
GO

If @@Error = 0
   Print 'Success: The Insert into ReportTable table for US22216 has been deployed successfully'
Else
   Print 'Failure: The Insert into ReportTable table for US22216 has not been deployed'
GO