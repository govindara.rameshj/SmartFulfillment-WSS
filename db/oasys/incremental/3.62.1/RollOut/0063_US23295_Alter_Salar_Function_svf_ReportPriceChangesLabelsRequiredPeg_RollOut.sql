IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesLabelsRequiredPeg') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesLabelsRequiredPeg'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesLabelsRequiredPeg() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportPriceChangesLabelsRequiredPeg')
GO
-- =================================================================================================
-- Author       : Kevan Madelin
-- Version      : 1.0
-- Create date  : 8th June 2011
-- Description  : Returns the figure to use for Price Changes - Labels Required PEG - Scalar Valued Function
-- Notes        : This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
-- Author       : Sean Moir
-- Version      : 1.1
-- Create date  : 7th July 2011
-- Description  : Changed to return sum of STKMAS.LABN
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportPriceChangesLabelsRequiredPeg]
(
)
RETURNS int
AS
BEGIN

    DECLARE @Peg int, 
            @PegLessTODT int,
            @PegLessTMDT int,
            @TODT date,
            @TMDT date
                    
    ----------------------------------------------------------------------------------
    -- Retrieve Count for Price Change Labels Required - PEG
    ----------------------------------------------------------------------------------

    SELECT @TODT = TODT, @TMDT = DATEADD(d, 7, TMDT) FROM dbo.SYSDAT WHERE FKEY = '01' 
    
    SELECT @PegLessTODT = COUNT(DISTINCT pc.SKUN)
    FROM PRCCHG AS pc
    INNER JOIN STKMAS AS sm ON sm.SKUN = pc.SKUN AND sm.LABN = pc.LABS
    WHERE NOT ((sm.INON = '1' OR sm.IOBS = '1' OR sm.AAPC = '1') AND sm.ONHA + sm.MDNQ = 0) 
    AND pc.LABS = 1 AND pc.PDAT <= @TODT

    SELECT @PegLessTMDT = COUNT(pc.SKUN)
    FROM PRCCHG AS pc
    INNER JOIN STKMAS AS sm ON sm.SKUN = pc.SKUN AND sm.LABN = pc.LABS
    WHERE NOT ((sm.INON = '1' OR sm.IOBS = '1' OR sm.AAPC = '1') AND sm.ONHA + sm.MDNQ = 0) 
    AND pc.LABS = 1 AND pc.PDAT > @TODT AND pc.PDAT <= @TMDT

    SET @Peg = ISNULL(@PegLessTODT, 0) + ISNULL(@PegLessTMDT, 0);
        
    RETURN @Peg
    
END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesLabelsRequiredPeg for US has been deployed successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesLabelsRequiredPeg for US has not been deployed'
GO
