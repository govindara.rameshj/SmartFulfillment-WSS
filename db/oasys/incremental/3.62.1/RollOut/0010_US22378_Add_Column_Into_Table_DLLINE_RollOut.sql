SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLLINE') and name = 'IsExplosive')
BEGIN
    PRINT 'Add IsExplosive column to DLLINE'
    ALTER TABLE DLLINE
    ADD IsExplosive bit NOT NULL CONSTRAINT DF_DLLINE_IsExplosive DEFAULT (0)
END

GO

If @@Error = 0
   Print 'Success: Add IsExplosive column to DLLINE for US22378 has been deployed successfully'
ELSE
   Print 'Failure: Add IsExplosive column to DLLINE for US22378 has not been deployed successfully'
GO  