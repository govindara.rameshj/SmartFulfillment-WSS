create procedure TrimDatabase
   @ageInMonths int,
   @maxRowAffected int
as
begin
	declare @OlderThan date;
	declare @rowcount int;
	declare @errorMsg varchar;
	declare @errorSeverity int;

	set @rowcount = @maxRowAffected; 
	set @OlderThan = cast(DATEADD(month, -1 * @ageInMonths, GETDATE()) as date);		
	set @errorMsg = 'A SQL error occurred in the housekeeping stored procedure TrimDatabase';
	set @errorSeverity = 18;
		
	WHILE @rowcount >= @maxRowAffected
	begin
		set @rowcount = 0;

		---------------------------------------------------------------------------------------------------------------------------------
		-- Sales
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

			select DATE1, Till, [TRAN]
			into #Sales
			from DLTOTS
			where DATE1 < @OlderThan and ORDN not in (select NUMB from CORHDR4 where DeliveryStatus <> 999)

		set rowcount 0;

		begin try
			begin transaction

                delete DLTOTS
                from DLTOTS a
                inner join #Sales b
                      on  b.DATE1  = a.DATE1
                      and b.TILL   = a.TILL
                      and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
				
                delete DLLINE
                from DLLINE a
                inner join #Sales b
                      on  b.DATE1  = a.DATE1
                      and b.TILL   = a.TILL
                      and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

                delete DLPAID
                from DLPAID a
                inner join #Sales b
                      on  b.DATE1  = a.DATE1
                      and b.TILL   = a.TILL
                      and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
				
                delete DLCOMM
                from DLCOMM a
                inner join #Sales b
                      on  b.DATE1  = a.DATE1
                      and b.TILL   = a.TILL
                      and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

                delete DLCOUPON
                from DLCOUPON a
                inner join #Sales b
                      on  b.DATE1  = a.TranDate
                      and b.TILL   = a.TranTillID
                      and b.[TRAN] = a.TranNo
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

                delete DLEANCHK
                from DLEANCHK a
                inner join #Sales b
                      on  b.DATE1  = a.DATE1
                      and b.TILL   = a.TILL
                      and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

                delete DLEVNT
                from DLEVNT a
                inner join #Sales b
                      on  b.DATE1  = a.DATE1
                      and b.TILL   = a.TILL
                      and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
                delete DLGIFT
                from DLGIFT a
                inner join #Sales b
                      on  b.DATE1  = a.DATE1
                      and b.TILL   = a.TILL
                      and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
                delete DLOCUS
                from DLOCUS a
                inner join #Sales b
                      on  b.DATE1  = a.DATE1
                      and b.TILL   = a.TILL
                      and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
				delete DLOLIN
				from DLOLIN a
				inner join #Sales b
					  on  b.DATE1  = a.DATE1
					  and b.TILL   = a.TILL
					  and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
				delete DLRCUS
				from DLRCUS a
				inner join #Sales b
					  on  b.DATE1  = a.DATE1
					  and b.TILL   = a.TILL
					  and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
				delete DLREJECT
				from DLREJECT a
				inner join #Sales b
					  on  b.DATE1  = a.DATE1
					  and b.TILL   = a.TILL
					  and b.[TRAN] = a.[TRAN]
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, @errorSeverity, 1)
		end catch

        drop table #Sales		

		---------------------------------------------------------------------------------------------------------------------------------
		-- Stock Movements
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

		begin try
			begin transaction
				--tkey
				delete from STKLOG where DATE1 < @OlderThan;
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 2)
		end catch

		set rowcount 0;

		---------------------------------------------------------------------------------------------------------------------------------
		-- Receipts & IBTs
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

			select NUMB into #Receipts from DRLSUM where DATE1 < @OlderThan;

		set rowcount 0;

		begin try
			begin transaction
			 
				delete from DRLDET where NUMB in (select NUMB from #Receipts)
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
						
				delete from DRLSUM where NUMB in (select NUMB from #Receipts)
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, @errorSeverity, 3)
		end catch

		drop table #Receipts;		

		---------------------------------------------------------------------------------------------------------------------------------
		-- Quote Order Deliveries
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

			select NUMB
			into #QuoteOrderDelivery
			from CORHDR4
			where DATE1 < @OlderThan
			and DeliveryStatus = 999

		set rowcount 0;

		begin try
			begin transaction

				delete CORHDR
				from CORHDR a
				inner join #QuoteOrderDelivery b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete CORHDR4
				from CORHDR4 a
				inner join #QuoteOrderDelivery b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete CORHDR5
				from CORHDR5 a
				inner join #QuoteOrderDelivery b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete CORLIN
				from CORLIN a
				inner join #QuoteOrderDelivery b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete CORLIN2
				from CORLIN2 a
				inner join #QuoteOrderDelivery b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete CORREFUND
				from CORREFUND a
				inner join #QuoteOrderDelivery b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete CORTXT
				from CORTXT a
				inner join #QuoteOrderDelivery b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 4)
		end catch

        drop table #QuoteOrderDelivery

		---------------------------------------------------------------------------------------------------------------------------------
		-- Purchase Orders
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

			select TKEY into #Purchases from PURHDR where DDAT < @OlderThan;

		set rowcount 0;

		begin try
			begin transaction	
			 
				delete from PURLIN where TKEY in (select TKEY from #Purchases)
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
				delete from PURHDR where TKEY in (select TKEY from #Purchases)		
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);					
				
			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 5)
		end catch

		drop table #Purchases;

		---------------------------------------------------------------------------------------------------------------------------------
		-- Issues
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

			select DATEADDED, NUMB into #Issues from ISUHDRHISTORY where DATEADDED < @OlderThan

		set rowcount 0;

		begin try
			begin transaction

				delete ISUHDRHISTORY
				from ISUHDRHISTORY a
				inner join #Issues b
					  on  b.DATEADDED = a.DATEADDED
					  and b.NUMB      = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete ISULINEHISTORY
				from ISULINEHISTORY a
				inner join #Issues b
					  on  b.DATEADDED = a.DATEADDED
					  and b.NUMB      = a.NUMB

				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 6)
		end catch

		drop table #Issues		

		---------------------------------------------------------------------------------------------------------------------------------
		-- Consignment
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

		begin try
			begin transaction

				delete from CONMAS where EDAT < @OlderThan
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 7)
		end catch

		set rowcount 0;

		---------------------------------------------------------------------------------------------------------------------------------
		-- Containers
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

			select ADEP, CNUM into #Containers from CONSUM where DELD < @OlderThan

		set rowcount 0;

		begin try
			begin transaction

				delete CONSUM
				from CONSUM a
				inner join #Containers b
					  on  b.ADEP = a.ADEP
					  and b.CNUM = a.CNUM
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete CONDET
				from CONDET a
				inner join #Containers b
					  on  b.ADEP = a.ADEP
					  and b.CNUM = a.CNUM
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 8)
		end catch

		drop table #Containers		

		---------------------------------------------------------------------------------------------------------------------------------
		-- Events
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

		select NUMB, SDAT, EDAT
		into #Events
		from EVTHDR
		where EDAT < @OlderThan
		and NUMB not in (select NUMB from EVTMAS where EDAT >= @OlderThan)

		set rowcount 0;

		begin try
			begin transaction

                ----------------------
                --mix and match:EVTDLG
                ----------------------
				delete EVTMMG
				from EVTMMG a
				inner join EVTDLG b
					  on  b.KEY1 = a.MMGN
			    inner join #Events c
					  on  c.NUMB = b.NUMB
                where b.[TYPE] = 'M'
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount)
				--orphaned records:EVTDLG 
				delete EVTMMG
				from EVTMMG a
				inner join EVTDLG b
					  on  b.KEY1 = a.MMGN
			    left outer join EVTHDR c
					  on  c.NUMB = b.NUMB
                where b.[TYPE] = 'M'
                and   c.NUMB is null
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount)


                --mix and match:EVTMAS ?
				--orphaned records: EVTMAS ?

                ----------------------
                --deal groups
                ----------------------
				delete EVTDLG
				from EVTDLG a
				inner join #Events b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
				--orphaned records
				delete EVTDLG
				from EVTDLG a
				left outer join EVTHDR b
					  on  b.NUMB = a.NUMB
			    where b.NUMB is null
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

                ----------------------
                --price changes
                ----------------------
				delete EVTCHG
				from EVTCHG a
				inner join #Events b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
                --orphaned records
				delete EVTCHG
				from EVTCHG a
				left outer join EVTHDR b
					  on  b.NUMB = a.NUMB
			    where b.NUMB is null
			    and   a.EDAT < @OlderThan
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

                ----------------------
                --hierarchy
                ----------------------
				delete EVTHEX
				from EVTHEX a
				inner join #Events b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

                ----------------------
                --master
                ----------------------
				delete EVTMAS
				from EVTMAS a
				inner join #Events b
					  on  b.NUMB = a.NUMB
			    where a.EDAT < @OlderThan
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

                --headrer
				delete EVTHDR
				from EVTHDR a
				inner join #Events b
					  on  b.NUMB = a.NUMB
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 9)
		end catch

		drop table #Events		

		---------------------------------------------------------------------------------------------------------------------------------
		-- Quotes
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

			select NUMB into #quohdr from QUOHDR where EXPD < @OlderThan;

		set rowcount 0;

		begin try
			begin transaction

				delete from QUOLIN where NUMB in (select NUMB from #quohdr)
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
				delete from QUOHDR where NUMB in (select NUMB from #quohdr)
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);				
				
			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 10)
		end catch

		drop table #quohdr;
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Returns
		---------------------------------------------------------------------------------------------------------------------------------
		set rowcount @maxRowAffected;

			select TKEY into #Returns from RETHDR where RDAT < @OlderThan

		set rowcount 0;

		begin try
			begin transaction

				delete RETHDR
				from RETHDR a
				inner join #Returns b
					  on  b.TKEY = a.TKEY
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

				delete RETLIN
				from RETLIN a
				inner join #Returns b
					  on  b.TKEY = a.HKEY
				set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);


			commit transaction
		end try
		begin catch
			if @@trancount > 0
				rollback transaction
				
			raiserror(@errorMsg, 18, 11)
		end catch

		drop table #Returns
	
	end
end
go

if @@error = 0
   print 'Success: Stored procedure TrimDatabase for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure TrimDatabase for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On TrimDatabase To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure TrimDatabase for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure TrimDatabase for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on TrimDatabase to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure TrimDatabase for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure TrimDatabase for HouseKeeping has NOT been successfully deployed'
go
