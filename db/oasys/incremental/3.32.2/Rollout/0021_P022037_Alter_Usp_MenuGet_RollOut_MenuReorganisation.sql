alter procedure MenuGet

   @MenuID int = null  

as
begin
   set nocount on

   --Menu model
   --    Consists of multiple root(top) level menu item(s)
   --    Under each root; menu item(s) are displayed next
   --    Under each root; sub menu item(s) finally last
   --
   --    NOTE: Following code will deliver above; will not cater for generic hierarchical tree to n levels

   --   Root level menu items     : MenuConfig:ID = 0 and MenuConfig:AssemblyName = null
   --   First level menu items    : MenuConfig:ID = 0 and MenuConfig:AssemblyName = null
   --   First level sub menu items: MenuConfig:ID = 0 and MenuConfig:AssemblyName = null

   declare @CursorTempSortOrderID int 
   declare @CursorID              int
   declare @CursorDisplaySequence int
   
   declare @Output   table (RevisedID int identity (1, 1), MenuConfigID int)

   declare TempCursor cursor for

   --need "sub menu" items to respect "display sequence"

   --select ID
   --from  MenuConfig
   --where (@MenuID is null     and AssemblyName is null)
   --or    (@MenuID is not null and MasterId = @MenuID)
   --order by ID

   select TempSortOrderID = ID,       ID, DisplaySequence from  MenuConfig where @MenuID is null and MasterID = 0                                --root level menu items
   union
   select TempSortOrderID = MasterID, ID, DisplaySequence from  MenuConfig where @MenuID is null and MasterID <> 0 and AssemblyName is null      --first level sub menu items
   union
   select TempSortOrderID = MasterID, ID, DisplaySequence from  MenuConfig where @MenuID is not null and MasterID = @MenuID                      --first level menu items
   order by TempSortOrderID, DisplaySequence   

   open TempCursor
   fetch next from TempCursor into @CursorTempSortOrderID, @CursorID, @CursorDisplaySequence
   while @@fetch_status = 0
   begin
      --parent menu
      insert @Output select ID
                     from MenuConfig
                     where ID = @CursorID

      --sub menu; process this only when the entire menu tree is required
      insert @Output select ID
                     from MenuConfig
                     where @MenuID is null
                     and   MasterID = @CursorID
                     and   AssemblyName is not null
                     order by DisplaySequence, ID

      --next record
      fetch next from TempCursor into @CursorTempSortOrderID, @CursorID, @CursorDisplaySequence
   end
   close TempCursor
   deallocate TempCursor

   select b.ID              as Id,
          b.MasterID        as MasterId,
          b.AppName	        as 'Description',
          b.AssemblyName,
          b.ClassName,
          b.MenuType        as 'LoadType',
          b.[Parameters],
          b.ImagePath,
          b.LoadMaximised	as IsMaximised,
          b.IsModal,
          b.DisplaySequence	as DisplayOrder
   from @Output a
   inner join MenuConfig b
         on b.ID = a.MenuConfigID 
   order by RevisedID

end
go

if @@error = 0
   print 'Success: The stored procedure (MenuGet) for PO22-037 has been successfully deployed'
else
   print 'Failure: The stored procedure (MenuGet) for PO22-037 has NOT been successfully deployed'
go