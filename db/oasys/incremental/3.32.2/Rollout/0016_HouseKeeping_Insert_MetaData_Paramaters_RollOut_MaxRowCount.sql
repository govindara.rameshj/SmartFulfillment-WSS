﻿insert Parameters (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
           values (188, 'House Keeping max row count', '1000', null, 0, null, 0)
go

if @@error = 0
   print 'Success: The metadata change to Parameters for House Keeping max row count has been successfully deployed'
else
   print 'Failure: The metadata change to Parameters for House Keeping max row count has NOT been successfully deployed'
go
