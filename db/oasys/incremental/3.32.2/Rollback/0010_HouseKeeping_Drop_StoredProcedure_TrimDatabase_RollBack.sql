drop procedure TrimDatabase
go

if @@error = 0
   print 'Success: Stored procedure TrimDatabase for HouseKeeping has been successfully dropped'
else
   print 'Failure: Stored procedure TrimDatabase for HouseKeeping has NOT been successfully dropped'
go