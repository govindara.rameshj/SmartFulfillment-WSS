drop function udf_MaxVal
go

if @@error = 0
   print 'Success: User defined function udf_MaxVal for HouseKeeping has been successfully dropped'
else
   print 'Failure: User defined function udf_MaxVal for HouseKeeping has NOT been successfully dropped'
go