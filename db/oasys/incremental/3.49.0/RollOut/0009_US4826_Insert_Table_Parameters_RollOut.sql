IF EXISTS (SELECT * FROM [dbo].[Parameters] WHERE [ParameterID] = 50 AND [Description] = 'MCFC')
	DELETE FROM [dbo].[Parameters] WHERE [ParameterID] = 50
GO

INSERT INTO [dbo].[Parameters]
	([ParameterID]
	,[Description]
	,[StringValue]
	,[LongValue]
	,[BooleanValue]
	,[DecimalValue]
	,[ValueType])
SELECT 
		 [ParameterID]
		,[Description]
		,''  as [StringValue]
		,0   as [LongValue]
		,CASE WHEN CAST('8' + [STOR] as int) IN (SELECT [LongValue] FROM [Parameters] WHERE [ParameterID] in ('3020', '3030'))
		 THEN 0 ELSE 1 END as [BooleanValue]
		,0.0 as [DecimalValue]
		,3   as [ValueType]
FROM (
	SELECT 50 as [ParameterID], cast('Notify Delivery service' as varchar(50)) as [Description]
) t
INNER JOIN [RETOPT] ON FKEY = '01'
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] prm WHERE prm.ParameterID = t.ParameterID)      
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US4826 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US4826 has not been deployed successfully'
GO
