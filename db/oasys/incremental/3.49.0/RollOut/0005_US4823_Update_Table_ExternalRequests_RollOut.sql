update er
set [Status] = 3 /*Retry*/
from [dbo].[ExternalRequests] er
inner join [dbo].[Parameters] on [ParameterID] = 7005 /*Max Retries Number for ExternalRequestMonitor*/
where [Status] = 2 /*Failed*/ and [RetriesCount] < CONVERT(int, [StringValue])
	and [type] in ('Allocate', 'Deallocate', 'AllocateDeallocate')


If @@Error = 0
   Print 'Success: Update Table ExternalRequests for US4823 has been deployed successfully'
Else
   Print 'Failure: Update Table ExternalRequests for US4823 has not been deployed successfully'
Go