update [dbo].[ExternalRequests]
set [Status] = 2 /*Failed*/
where [Status] = 3 /*Retry*/


If @@Error = 0
   Print 'Success: Update Table ExternalRequests for US4823 has been deployed successfully'
Else
   Print 'Failure: Update Table ExternalRequests for US4823 has not been deployed successfully'
Go