DELETE FROM  [dbo].[Parameters] WHERE ParameterID = 50
GO

If @@Error = 0
   Print 'Success: Delete from Table Parameters for US4826 has been deployed successfully'
Else
   Print 'Failure: Delete from Table Parameters for US4826 has not been deployed successfully'
Go
