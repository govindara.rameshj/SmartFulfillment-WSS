delete from [dbo].[Parameters] where ParameterID in (4500, 4501, 4502, 4503, 4504)

If @@Error = 0
   Print 'Success: The Insert parameter 4500, 4501, 4502, 4503, 4504 for US33338 has been successfully rolled back'
Else
   Print 'Failure: The Insert parameter 4500, 4501, 4502, 4503, 4504 for US33338 has not been rolled back'
GO
