PRINT ('Dropping procedure AlertInsert')
GO

IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'AlertInsert') AND type in (N'P', N'PC'))
BEGIN
drop procedure AlertInsert
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure AlertInsert for US33338 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure AlertInsert for US33338 has not been deployed'
GO