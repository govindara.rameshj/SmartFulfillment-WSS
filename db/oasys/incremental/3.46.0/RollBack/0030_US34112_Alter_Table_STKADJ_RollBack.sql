SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'STKADJ' and col.name = 'AdjustmentCount'
)
BEGIN
PRINT 'Drop AdjustmentCount column from STKADJ'
 ALTER TABLE [dbo].STKADJ DROP CONSTRAINT DF__STKADJ__AdjustmentCount
 ALTER TABLE [dbo].STKADJ DROP COLUMN AdjustmentCount 
END

GO
