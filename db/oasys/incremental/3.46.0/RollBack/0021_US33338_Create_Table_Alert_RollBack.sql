DROP TABLE dbo.Alert

If @@Error = 0
   Print 'Success: The Create Table Alert for US33338 has been rolled back successfully'
Else
   Print 'Failure: The Create Table Alert for US33338 has not been rolled back successfully'
GO