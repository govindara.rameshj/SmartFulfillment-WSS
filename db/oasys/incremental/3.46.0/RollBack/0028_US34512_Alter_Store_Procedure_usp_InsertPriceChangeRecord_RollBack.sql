IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'usp_InsertPriceChangeRecord') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure usp_InsertPriceChangeRecord'
	EXEC ('CREATE PROCEDURE dbo.usp_InsertPriceChangeRecord AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure usp_InsertPriceChangeRecord')
GO

ALTER PROCEDURE [dbo].[usp_InsertPriceChangeRecord] 
	@Skun Char(6),
	@StartDate DATE,
	@Price decimal(10,2),
	@Status Char(1),
	@IsShelfLabel bit,
	@AutoApplyDate DATE,
	@SmallLabel bit,
	@MediumLabel bit,
	@LargeLabel bit,
	@EventNumber Char(6),
	@Priority Char(2)
	
AS
BEGIN

INSERT INTO [Oasys].[dbo].[PRCCHG]
           ([SKUN]
           ,[PDAT]
           ,[PRIC]
           ,[PSTA]
           ,[SHEL]
           ,[AUDT]
           ,[AUAP]
           ,[MARK]
           ,[MCOM]
           ,[LABS]
           ,[LABM]
           ,[LABL]
           ,[EVNT]
           ,[PRIO]
           ,[EEID]
           ,[MEID])
     VALUES
           (@Skun 
           ,@StartDate
           ,@Price
           ,@Status
           ,@IsShelfLabel
           ,@AutoApplyDate
           ,Null
           ,Null
           ,0
           ,@SmallLabel
           ,@MediumLabel
           ,@LargeLabel
           ,@EventNumber
           ,@Priority
           ,Null
           ,Null)
RETURN @@RowCount
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_InsertPriceChangeRecord for US34512 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_InsertPriceChangeRecord for US34512 has not been deployed'
GO