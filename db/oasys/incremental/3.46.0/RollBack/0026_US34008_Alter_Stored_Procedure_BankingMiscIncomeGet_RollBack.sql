IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'BankingMiscIncomeGet') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure BankingMiscIncomeGet'
	EXEC ('CREATE PROCEDURE dbo.BankingMiscIncomeGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure BankingMiscIncomeGet')
GO

ALTER procedure [dbo].[BankingMiscIncomeGet]
	@DateStart date=null, @DateEnd date=null
as
begin

select
	dt.CASH		CashierId,
	su.Name		CashierName,	
	case dt.TCOD
		when 'M+' then 'In'
		when 'M-' then 'Out'
	end			'Type',
	dt.DATE1	'Date', 
	dt.TILL		TillId,
	dt.[TRAN]	TranNumber,	
	sc.Code		AccountCode,
	sc.Name		AccountName,
	dt.TOTL		Value,
	dt.DOCN		DocumentNumber 
from 
	dltots dt
inner join
	SystemUsers su on su.ID=CASH
inner join
	SystemCodes sc on sc.Id=dt.MISC and sc.[Type]=dt.TCOD
where 
	dt.TCOD in ('M+','M-') and
	dt.CASH<>'000' and
	dt.TMOD=0 and
	dt.VOID=0 and
	dt.PARK=0 and
	(@DateStart is null or (@DateStart is not null and dt.DATE1>=@DateStart)) and
	(@DateEnd is null or (@DateEnd is not null and dt.DATE1<=@DateEnd))
order by
	dt.CASH, dt.DATE1

end

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure BankingMiscIncomeGet for US34008 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure BankingMiscIncomeGet for US34008 has not been deployed'
GO