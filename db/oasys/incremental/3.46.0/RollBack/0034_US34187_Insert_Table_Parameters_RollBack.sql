DELETE FROM  [dbo].[Parameters] WHERE ParameterID in (947, 948, 949)
GO

If @@Error = 0
   Print 'Success: Delete from Table Parameters for US34187 has been deployed successfully'
Else
   Print 'Failure: Delete from Parameters for US34187 has not been deployed successfully'
Go
