IF EXISTS (select 1 from [SystemCodes] 
			  where [Id] in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20) and [Type] in ('C-','C+'))
DELETE [dbo].[SystemCodes]
WHERE [Id] in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20) and [Type] in ('C-','C+')

GO

If @@Error = 0
   Print 'Success: Delete Table [SystemCodes] for US34008 has been deployed successfully'
Else
   Print 'Failure: Delete Table [SystemCodes] for US34008 has not been deployed successfully'
Go