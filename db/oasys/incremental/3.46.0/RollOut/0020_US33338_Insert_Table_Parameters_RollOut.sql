If not exists (select 1 from [dbo].[Parameters] where ParameterID = 4500)
	insert [dbo].[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
	values (4500, 'Click&Collect notification period in seconds', null, 60, 0, null, 1)
GO

If @@Error = 0
   Print 'Success: The Insert parameter 4500 for US33338 has been deployed successfully'
Else
   Print 'Failure: The Insert parameter 4500 for US33338 has not been deployed'
GO

If not exists (select 1 from [dbo].[Parameters] where ParameterID = 4501)
	insert [dbo].[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
	values (4501, 'Link to Enable and parameters', '--app=https://enable.wickes.co.uk/ordermanagementportal/login', 0, 0, null, 0)
GO

If @@Error = 0
   Print 'Success: The Insert parameter 4501 for US33338 has been deployed successfully'
Else
   Print 'Failure: The Insert parameter 4501 for US33338 has not been deployed'
GO

If not exists (select 1 from [dbo].[Parameters] where ParameterID = 4502)
	insert [dbo].[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
	values (4502, 'Disable Click&Collect alerting on workstations', '', 0, 0, null, 0)
GO

If @@Error = 0
   Print 'Success: The Insert parameter 4502 for US33338 has been deployed successfully'
Else
   Print 'Failure: The Insert parameter 4502 for US33338 has not been deployed'
GO

If not exists (select 1 from [dbo].[Parameters] where ParameterID = 4503)
	insert [dbo].[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
	values (4503, 'Browser path 1', 'C:\Program Files\Google\Chrome\Application\Chrome.exe', 0, 0, null, 0)
GO

If @@Error = 0
   Print 'Success: The Insert parameter 4503 for US33338 has been deployed successfully'
Else
   Print 'Failure: The Insert parameter 4503 for US33338 has not been deployed'
GO

If not exists (select 1 from [dbo].[Parameters] where ParameterID = 4504)
	insert [dbo].[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
	values (4504, 'Browser path 2', 'C:\Program Files (x86)\Google\Chrome\Application\Chrome.exe', 0, 0, null, 0)
GO

If @@Error = 0
   Print 'Success: The Insert parameter 4504 for US33338 has been deployed successfully'
Else
   Print 'Failure: The Insert parameter 4504 for US33338 has not been deployed'
GO