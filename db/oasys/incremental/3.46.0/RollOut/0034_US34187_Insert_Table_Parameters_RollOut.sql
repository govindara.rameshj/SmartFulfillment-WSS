If not exists (select 1 from [dbo].[Parameters] where ParameterID = 947)
 insert [dbo].[Parameters] 
	(ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
 values 
	(947, 'Receipt houskeeping interval', null, 14, 0, null, 1)
GO

If not exists (select 1 from [dbo].[Parameters] where ParameterID = 948)
	insert [dbo].[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
	values (948, 'Receipt logging on tills', '', null, 0, null, 0)
GO

If not exists (select 1 from [dbo].[Parameters] where ParameterID = 949)
	insert [dbo].[Parameters] (ParameterID, [Description], StringValue, LongValue, BooleanValue, DecimalValue, ValueType)
	values (949, 'Receipt logging path', 'C:\Wix\Receipt\', null, 0, null, 0)
GO

If @@Error = 0
   Print 'Success: Adding parameters into table Parameters for US34187 has been deployed successfully'
Else
   Print 'Failure: Adding parameters into table Parameters for US34187 has not been deployed successfully'
Go