SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'STKADJ' and col.name = 'AdjustmentCount'
)
BEGIN
PRINT 'Add AdjustmentCount column to STKADJ'
 ALTER TABLE [dbo].STKADJ ADD AdjustmentCount int not null
 CONSTRAINT DF__STKADJ__AdjustmentCount DEFAULT 1
END

GO
