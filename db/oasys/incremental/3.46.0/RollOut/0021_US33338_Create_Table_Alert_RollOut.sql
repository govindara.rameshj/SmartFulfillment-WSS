PRINT ('Creating table Alert')
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Alert]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE dbo.Alert
	(
	Id int NOT NULL IDENTITY (1, 1),
	OrderNumb char(6) NOT NULL,
	Refund bit NOT NULL,
	ReceivedDate datetime NOT NULL,
	Authorized bit NOT NULL DEFAULT 0,
	AuthUser int NULL,
	AuthDate datetime NULL
CONSTRAINT [PK_Alert] PRIMARY KEY CLUSTERED ([Id])
) ON [PRIMARY]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notifications on the new or cancelled C&C orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Alert'

CREATE NONCLUSTERED INDEX [IDX_Alert] ON [dbo].[Alert] 
(
	[Authorized] ASC
)

END

GO

If @@Error = 0
   Print 'Success: The Create Table Alert for US33338 has been deployed successfully'
Else
   Print 'Failure: The Create Table Alert for US33338 has not been deployed successfully'
GO