IF NOT EXISTS (select 1 from [SystemCodes] 
			  where [Id] in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20) and [Type] in ('C-','C+'))
INSERT INTO [dbo].[SystemCodes]
           ([Id]
           ,[Type]
           ,[Code]
           ,[Name])
VALUES 
		(1,'C-',4003,'Customer Concern'),
		(1,'C+',1115,'Bad Cheque'),
		(2,'C-',5604,'Stationery'),
		(2,'C+',6601,'Social Fund'),
		(3,'C-',6705,'Breakfast Voucher'),
		(3,'C+',6520,'Suspense'),
		(4,'C-',6602,'Social Fund'),
		(4,'C+',3005,'Burger Van'),
		(5,'C-',6502,'Travel'),
		(5,'C+',8501,'Safe Overs'),
		(6,'C-',6508,'Stamps'),
		(6,'C+',6504,'Phone Box'),
		(7,'C-',5603,'Meeting Food'),
		(7,'C+',6524,'Vending Machine'),
		(8,'C-',5602,'Colleague Welfare'),
		(8,'C+',6704,'Rent'),
		(9,'C-',6709,'Council Account'),
		(9,'C+',1595,'Display Sale'),
		(10,'C-',1001,'Change Fund'),
		(10,'C+',1001,'Safe Change'),
		(11,'C-',6521,'Suspense'),
		(11,'C+',6708,'Council Account'),
		(12,'C-',8501,'Safe Unders'),
		(12,'C+',9878,'Gift Card'),
		(13,'C-',6312,'Cleaning'),
		(13,'C+',0,'Not Used'),
		(14,'C-',6115,'Leukaemia Fund'),
		(14,'C+',6115,'Leukaemia Fund'),
		(15,'C-',1004,'Refund Till'),
		(15,'C+',1003,'Refund Till'),
		(16,'C-',2134,'H/O Cheque'),
		(16,'C+',2134,'H/O Cheque'),
		(17,'C-',6529,'Forklift Fuel'),
		(17,'C+',0,'Not Used'),
		(18,'C-',1113,'Project Loan'),
		(18,'C+',1113,'Repay Project Loan'),
		(19,'C-',9876,'Gift Voucher'),
		(19,'C+',9876,'Gift Voucher'),
		(20,'C-',9998,'Deposits'),
		(20,'C+',9999,'Deposits')
GO

If @@Error = 0
   Print 'Success: Insert Table [SystemCodes] for US34008 has been deployed successfully'
Else
   Print 'Failure: Insert Table [SystemCodes] for US34008 has not been deployed successfully'
Go