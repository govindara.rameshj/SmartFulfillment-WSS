IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'usp_UpdateStockAdjustment') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure usp_UpdateStockAdjustment'
	EXEC ('CREATE PROCEDURE dbo.usp_UpdateStockAdjustment AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure usp_UpdateStockAdjustment')
GO

ALTER PROCEDURE [dbo].[usp_UpdateStockAdjustment] 
	-- Add the parameters for the stored procedure here
	@Adjustmentdate DATE, 
	@AdjustmentCode CHAR(2),
	@Skun Char(6),
	@Seqn char(2),
	@AmendId int,
	@Initials Char(5),
	@Department Char(2),	
	@StartingStock decimal,
	@AdjustmentQty decimal,
	@AdjustmentPrice decimal(10,2),
	@AdjustmentCost decimal(10,2),
	@IsSentToHO bit = 0,
	@AdjustmentType Char(1),
	@Comment char(20),
	@DRLNumber char(6),
	@ReversalCode char(1),
	@MarkDownWriteOff char(1),
	@WriteOffAuthorised char(3),
	@DateAuthorised date = NULL,
	@RTI char(1) ='N',
	@PeriodId int,
	@TransferSku Char (6)= '000000',
	@TransferValue decimal(10,2) = 0.0,
	@TransferStart decimal(10,2) = 0.0,
	@TransferPrice decimal(10,2) = 0.0,
	@IsReversed	bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE STKADJ 
    SET
		AmendId			= @AmendId,
		[INIT]			= @Initials,
		DEPT			= @Department,	
		SSTK			= @StartingStock,
		QUAN			= @AdjustmentQty,
		PRIC			= @AdjustmentPrice,
		COST			= @AdjustmentCost,
		COMM			= @IsSentToHO,
		[TYPE]			= @AdjustmentType,
		INFO			= @Comment,
		DRLN			= @DRLNumber,
		RCOD			= @ReversalCode,
		MOWT			= @MarkDownWriteOff,
		WAUT			= ISNULL(NULLIF(@WriteOffAuthorised, ''), '0'),
		DAUT			= @DateAuthorised,
		RTI				= @RTI,
		PeriodId		= @PeriodId,
		TransferStart	= @TransferStart,
		TransferPrice	= @TransferPrice,
		IsReversed		= @IsReversed,
		TSKU			= @TransferSku,
		TVAL			= @TransferValue
	WHERE
		DATE1			= @AdjustmentDate and
		CODE			= @AdjustmentCode and
		SKUN			= @SKUN and 
		SEQN			= @Seqn and
		WAUT			In ('000', '0') 
	return @@RowCount

END

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_UpdateStockAdjustment for US34112 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_UpdateStockAdjustment for US34112 has not been deployed'
GO