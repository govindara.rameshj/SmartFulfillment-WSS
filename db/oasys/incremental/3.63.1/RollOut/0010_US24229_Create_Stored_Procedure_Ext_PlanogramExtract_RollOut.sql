IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'Ext_PlanogramExtract') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure Ext_PlanogramExtract'
    EXEC ('CREATE PROCEDURE Ext_PlanogramExtract AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure Ext_PlanogramExtract')
GO

ALTER PROCEDURE dbo.Ext_PlanogramExtract
AS
BEGIN
    SELECT  STORE [StoreNumber],
            PLANNO [PlanogramNumber],
            PLANNAME [PlanogramName],
            SKUN [Sku]
    FROM dbo.PLANGRAM
END
GO

IF @@Error = 0
   PRINT 'Success: The Create Stored Procedure Ext_PlanogramExtract for US24229 has been deployed successfully'
ELSE
   PRINT 'Failure: The Create Stored Procedure Ext_PlanogramExtract for US24229 has not been deployed successfully'
GO
