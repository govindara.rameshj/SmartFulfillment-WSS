IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'DashPriceChanges2') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure DashPriceChanges2'
    EXEC ('CREATE PROCEDURE dbo.DashPriceChanges2 AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure DashPriceChanges2')
GO

ALTER PROCEDURE [dbo].[DashPriceChanges2]
    @DateEnd Date
AS
BEGIN
   SET NOCOUNT ON

   DECLARE @Output TABLE(RowId         int, 
                         [Description] varchar(50), 
                         Qty           int,
                         Peg           int,
                         Small         int,
                         Medium        int)

   DECLARE @OverdueQty                       int,
           @OutstandingQty                   int,
           @Peg                              int,
           @Small                            int,
           @Medium                           int,
           @PriceChangesConfirmedDayQuantity int
  
   SET @OutstandingQty                   = dbo.svf_ReportPriceChangesOutstandingQty()
   SET @OverdueQty                       = dbo.svf_ReportPriceChangesOverdueLabelsQty()
   SET @Peg                              = dbo.svf_ReportPriceChangesLabelsRequiredPeg()
   SET @Small                            = dbo.svf_ReportPriceChangesLabelsRequiredSmall()
   SET @Medium                           = dbo.svf_ReportPriceChangesLabelsRequiredMedium()
   SET @PriceChangesConfirmedDayQuantity = dbo.svf_ReportPriceChangesConfirmedDAY()

   INSERT @Output VALUES (1, 'Price Labels Action Required',   @OutstandingQty,                   null, null,   null)
   INSERT @Output VALUES (2, 'Overdue Labels',                 @OverdueQty,                       null, null,   null)
   INSERT @Output VALUES (3, 'Labels Required',                (@Peg + @Small + @Medium),         @Peg, @Small, @Medium)
   INSERT @Output VALUES (4, 'Outstanding Price Label Report', (@OutstandingQty + @OverdueQty),   null, null,   null)
   INSERT @Output VALUES (5, 'Price Change Audit Report',      @PriceChangesConfirmedDayQuantity, null, null,   null)
   INSERT @Output VALUES (6, 'Labels Request',                 null,                              null, null,   null)

   SELECT * FROM @Output ORDER BY RowId
end
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure DashPriceChanges2 for DE3259 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure DashPriceChanges2 for DE3259 has not been deployed'
GO
