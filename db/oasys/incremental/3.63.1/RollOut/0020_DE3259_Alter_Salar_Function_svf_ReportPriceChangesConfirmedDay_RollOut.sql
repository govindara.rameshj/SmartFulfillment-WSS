IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesConfirmedDAY') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesConfirmedDAY'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesConfirmedDAY() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportPriceChangesConfirmedDAY')
GO

ALTER FUNCTION [dbo].[svf_ReportPriceChangesConfirmedDAY]
(
)
RETURNS int
AS
BEGIN

    Declare 
        @TomorrowDate date,
        @TodayDate date,
        @Confirms int
    
    Set @TomorrowDate = (SELECT TOP 1 TMDT FROM SYSDAT);        
    Set @TodayDate = (SELECT TOP 1 TODT FROM SYSDAT);

                    
    ----------------------------------------------------------------------------------
    -- Get Price Changes Confirmed
    ----------------------------------------------------------------------------------
    Select @Confirms = count(PC.SKUN)
    From PRCCHG as PC
    Inner Join STKMAS as SM on SM.SKUN = PC.SKUN AND SM.DPRC = PC.AUAP AND SM.PRIC = PC.PRIC
    Where 
    ((PC.AUAP = @TomorrowDate AND PC.PSTA <> 'U' AND PC.PSTA <> 'S')
    OR (PC.AUAP = @TodayDate AND PC.PSTA = 'S'))
    AND NOT ((sm.INON = '1' OR sm.IOBS = '1' OR sm.AAPC = '1') AND sm.ONHA + sm.MDNQ = 0);

    Set             @Confirms           =       isnull(@Confirms, 0);
        
    RETURN          @Confirms

END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesConfirmedDAY for DE3259 has been deployed successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesConfirmedDAY for DE3259 has not been deployed'
GO