IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesOverdueLabelsQty') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesOverdueLabelsQty'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesOverdueLabelsQty() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportPriceChangesOverdueLabelsQty')
GO

ALTER FUNCTION [dbo].[svf_ReportPriceChangesOverdueLabelsQty]
(
)
RETURNS int
AS
BEGIN

    DECLARE @OverdueQty INT
    
    SELECT @OverdueQty = count(DISTINCT pc.SKUN)
    FROM PRCCHG AS pc
    INNER JOIN STKMAS AS sm ON sm.SKUN = pc.SKUN
    INNER JOIN SYSDAT AS sd ON sd.FKEY = '01'
    WHERE NOT ((sm.INON = '1' OR sm.IOBS = '1' OR sm.AAPC = '1') AND sm.ONHA + sm.MDNQ = 0) 
    AND (pc.LABS = 1 OR pc.LABM = 1 OR pc.LABL = 1)
    AND pc.PDAT <= sd.TMDT
    
    SET @OverdueQty = ISNULL(@OverdueQty, 0);
        
    RETURN @OverdueQty

END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesOverdueLabelsQty for DF3259 has been deployed successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesOverdueLabelsQty for DF3259 has not been deployed'
GO