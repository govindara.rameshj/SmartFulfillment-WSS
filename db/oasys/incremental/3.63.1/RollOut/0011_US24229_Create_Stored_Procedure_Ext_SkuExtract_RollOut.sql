IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'Ext_SkuExtract') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure Ext_SkuExtract'
    EXEC ('CREATE PROCEDURE Ext_SkuExtract AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure Ext_SkuExtract')
GO

ALTER PROCEDURE dbo.Ext_SkuExtract
AS
BEGIN
    SELECT  SKUN [Sku],
            DESCR [Description],
            INON [IsStocked]
    FROM dbo.STKMAS
END
GO

IF @@Error = 0
   PRINT 'Success: The Create Stored Procedure Ext_SkuExtract for US24229 has been deployed successfully'
ELSE
   PRINT 'Failure: The Create Stored Procedure Ext_SkuExtract for US24229 has not been deployed successfully'
GO