IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'PriceViolationsGet') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure PriceViolationsGet'
    EXEC ('CREATE PROCEDURE dbo.PriceViolationsGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure PriceViolationsGet')
GO

ALTER PROCEDURE [dbo].[PriceViolationsGet]
    @Date   date
AS
BEGIN
    SET NOCOUNT ON;

select
    dt.CASH             as 'CashierId',
    coalesce(su.TillReceiptName, 'Unknown') as 'CashierName',
    dl.SKUN             as 'SkuNumber',
    sm.DESCR            as 'Description',
    dl.TILL             as 'TillId',
    dl.[TRAN]           as 'TranNumber',
    dt.OVCTranNumber    as 'OVCTranNumber',
    dl.NUMB             as 'LineNumber',
    dl.SPRI             as 'PriceLU',
    dl.PRIC             as 'PriceKey',
    dl.QUAN             as 'Qty',
    dl.POPD             as 'PriceDifference',
    dl.PORC             as 'ReasonCode',    
    case dl.PORC
        when 1 then 'Damaged Goods'
        when 2 then 'Incorrect Price Displayed'
        when 3 then 'Refund Price Difference'
        when 4 then 'Wrong P.O.S. Sign'
        when 5 then 'Wrong Booklet Price'
        when 6 then 'Web Return'
        when 7 then 'Price Match/Promise'
        when 8 then 'HDC Return'
        when 9 then 'Colleague Refund'
        when 10 then 'Managers Discretion'
        when 11 then 'Incorrect Day Price'
        when 12 then 'Temporary Deal Group'
        else 'Other'
    end                 as 'ReasonDescription',
    do.CNAM             as 'Competitor',
    dl.SUPV             as 'SupervisorId',
    coalesce((select TillReceiptName from SystemUsers where EmployeeCode = dl.SUPV), 'Unknown')  as 'SupervisorName'
from
    DLLINE dl
inner join
    STKMAS sm on sm.SKUN = dl.SKUN
inner join
    vwDLTOTS dt on dt.DATE1 = dl.DATE1 and dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN]
left join
    SystemUsers su on su.EmployeeCode = dt.CASH
left join 
    DLOLIN do on do.DATE1 = dl.DATE1 and do.TILL = dl.TILL and do.[TRAN] = dl.[TRAN] and do.NUMB = dl.NUMB
where
    dl.DATE1 = @Date
    and dl.POPD <> 0
    and dt.VOID = 0
    and dl.LREV = 0
    and dt.TMOD = 0
    and dt.CASH <> '499'
    and (dt.OVCTranNumber IS NULL or dl.PORC <> 12) 
order by
    dt.CASH, dl.TILL, dl.[TRAN], dl.NUMB
    
END

If @@Error = 0
   Print 'Success: The Alter Stored Procedure PriceViolationsGet for US24473 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure PriceViolationsGet for US24473 has not been deployed'
GO
