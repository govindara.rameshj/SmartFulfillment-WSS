IF NOT EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('DLRCUS') and name = 'ONUM')
BEGIN
    PRINT 'Add ONUM column to DLRCUS'
    ALTER TABLE DLRCUS
    ADD ONUM smallint NULL
END

GO

If @@Error = 0
   Print 'Success: Add ONUM column to DLRCUS for US25770 has been deployed successfully'
ELSE
   Print 'Failure: Add ONUM column to DLRCUS for US25770 has not been deployed successfully'
GO