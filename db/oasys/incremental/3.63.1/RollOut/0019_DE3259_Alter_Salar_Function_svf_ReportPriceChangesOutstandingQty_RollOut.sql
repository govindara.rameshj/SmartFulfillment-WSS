IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesOutstandingQty') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesOutstandingQty'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesOutstandingQty() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportPriceChangesOutstandingQty')
GO

ALTER FUNCTION [dbo].[svf_ReportPriceChangesOutstandingQty]
(
)
RETURNS int
AS
BEGIN

    DECLARE @OutstandingQty INT
    DECLARE @OutstandingOverQty INT
    DECLARE @ResOutstandingQty INT

    SELECT @OutstandingOverQty = ISNULL(COUNT(DISTINCT pc.SKUN), 0)
    FROM PRCCHG AS pc
    INNER JOIN STKMAS AS sm ON sm.SKUN = pc.SKUN
    INNER JOIN SYSDAT AS sd ON sd.FKEY = '01'
    WHERE pc.PSTA = 'U' AND NOT ((sm.INON = '1' OR sm.IOBS = '1' OR sm.AAPC = '1') AND sm.ONHA + sm.MDNQ = 0)  
    AND ((pc.SHEL = '1' AND pc.LABS = 0 AND pc.LABM = 0 AND pc.LABL = 0) OR (pc.LABS = 1 OR pc.LABM = 1 OR pc.LABL = 1))
    AND pc.PDAT <= sd.TMDT
    
    SELECT @OutstandingQty = ISNULL(COUNT(pc.SKUN), 0)
    FROM PRCCHG AS pc
    INNER JOIN STKMAS AS sm ON sm.SKUN = pc.SKUN
    INNER JOIN SYSDAT AS sd ON sd.FKEY = '01'
    WHERE pc.PSTA = 'U' AND NOT ((sm.INON = '1' OR sm.IOBS = '1' OR sm.AAPC = '1') AND sm.ONHA + sm.MDNQ = 0)  
    AND ((pc.SHEL = '1' AND pc.LABS = 0 AND pc.LABM = 0 AND pc.LABL = 0) OR (pc.LABS = 1 OR pc.LABM = 1 OR pc.LABL = 1))
    AND  pc.PDAT > sd.TMDT AND pc.PDAT <= DATEADD(d, 7, sd.TMDT)
    
    SET @ResOutstandingQty = @OutstandingQty + @OutstandingOverQty;
        
    RETURN @ResOutstandingQty

END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesOutstandingQty for DF3259 has been deployed successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesOutstandingQty for DF3259 has not been deployed'
GO
