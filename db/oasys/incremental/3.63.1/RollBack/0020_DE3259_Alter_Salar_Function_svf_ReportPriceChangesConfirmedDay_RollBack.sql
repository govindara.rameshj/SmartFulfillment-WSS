IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_ReportPriceChangesConfirmedDAY') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_ReportPriceChangesConfirmedDAY'
    EXEC ('CREATE FUNCTION svf_ReportPriceChangesConfirmedDAY() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportPriceChangesConfirmedDAY')
GO

ALTER FUNCTION [dbo].[svf_ReportPriceChangesConfirmedDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

    Declare         @StartDate              date,
                    @Confirms               int
    
    Set             @StartDate          =       @InputDate;     

                    
    ----------------------------------------------------------------------------------
    -- Get Price Changes Confirmed
    ----------------------------------------------------------------------------------
    Select          @Confirms           =       count(PC.SKUN)
    From            PRCCHG as PC
    Inner Join      STKMAS as SM        on      SM.SKUN = PC.SKUN
    Where           SM.DPRC             =       @StartDate
                    and sm.INON = 0;

    Set             @Confirms           =       isnull(@Confirms, 0);
        
    RETURN          @Confirms

END
GO

IF @@Error = 0
   Print 'Success: The Alter Scalar-values Function svf_ReportPriceChangesConfirmedDAY for DE3259 has been deployed successfully'
ELSE
   Print 'Failure: The Alter Scalar-values Function svf_ReportPriceChangesConfirmedDAY for DE3259 has not been deployed'
GO
