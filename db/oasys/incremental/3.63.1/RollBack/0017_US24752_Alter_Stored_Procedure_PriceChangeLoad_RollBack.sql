﻿IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[PriceChangeLoad]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure [PriceChangeLoad]'
    EXEC ('CREATE PROCEDURE dbo.[PriceChangeLoad] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

ALTER procedure [dbo].[PriceChangeLoad]
   @Date           date
as
set nocount on

select a.*
from PRCCHG a
inner join STKMAS b
      on b.SKUN = a.SKUN
where a.PSTA = 'U'
and  a.PDAT <= @Date
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure PriceChangeLoad for US24752 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure PriceChangeLoad for US24752 has not been rolled back'
GO