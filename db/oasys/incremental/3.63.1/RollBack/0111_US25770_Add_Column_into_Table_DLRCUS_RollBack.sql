IF EXISTS (select 1 from sys.columns col inner join sys.objects obj on obj.object_id = col.object_id where obj.Name = 'DLRCUS' and col.name = 'ONUM')
BEGIN
    PRINT 'Dropping ONUM column from DLRCUS'
    ALTER TABLE dbo.DLRCUS DROP COLUMN ONUM
END

If @@Error = 0
   Print 'Success: Altering table DLRCUS for US25770 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table DLRCUS for US25770 has not been rolled back successfully'
GO