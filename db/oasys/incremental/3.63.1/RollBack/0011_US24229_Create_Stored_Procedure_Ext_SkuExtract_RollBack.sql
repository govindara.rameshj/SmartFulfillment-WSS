IF EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'Ext_SkuExtract') AND type in (N'P'))
BEGIN
    PRINT 'Dropping procedure Ext_SkuExtract'
    DROP PROCEDURE dbo.[Ext_SkuExtract];
END
GO

IF @@Error = 0
   Print 'Success: The Create Stored Procedure Ext_SkuExtract for US24229 has been rolled back successfully'
ELSE
   Print 'Failure: The Create Stored Procedure Ext_SkuExtract for US24229 has not been rolled back'
GO