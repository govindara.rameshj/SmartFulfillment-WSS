SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'svf_ReportClickAndCollectRefundsQtyDAY') AND type in (N'FN'))
BEGIN
	PRINT 'Creating function svf_ReportClickAndCollectRefundsQtyDAY'
	EXEC ('CREATE FUNCTION svf_ReportClickAndCollectRefundsQtyDAY() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportClickAndCollectRefundsQtyDAY')
GO

ALTER FUNCTION [dbo].[svf_ReportClickAndCollectRefundsQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare	@StartDate		date,
			@CCDayQty		numeric(9,2)
	
	Set		@StartDate		=	@InputDate;	
	
	Select @CCDayQty = COUNT(DT.TOTL)
	From DLTOTS DT 
	Inner Join vwCORHDRFull COR	on	DT.ORDN		= COR.NUMB
	Where	TCOD			= 'RF'
			and COR.IS_CLICK_AND_COLLECT = 1
			and DT.DATE1	=	@StartDate;
	
	Set		@CCDayQty		=	isnull(@CCDayQty, 0)
		
	RETURN	@CCDayQty

END
GO

IF @@Error = 0
   Print 'Success: The Create Scalar-values Function svf_ReportClickAndCollectRefundsQtyDAY for US14255 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function svf_ReportClickAndCollectRefundsQtyDAY for US14255 has not been deployed successfully'
GO