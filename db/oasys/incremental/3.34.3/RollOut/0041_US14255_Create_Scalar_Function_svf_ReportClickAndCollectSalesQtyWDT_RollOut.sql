SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'svf_ReportClickAndCollectSalesQtyWTD') AND type in (N'FN'))
BEGIN
	PRINT 'Creating function svf_ReportClickAndCollectSalesQtyWTD'
	EXEC ('CREATE FUNCTION svf_ReportClickAndCollectSalesQtyWTD() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportClickAndCollectSalesQtyWTD')
GO

ALTER FUNCTION [dbo].[svf_ReportClickAndCollectSalesQtyWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN
	Declare		@StartDate		date,
				@DateEnd		date,
				@CCQty			numeric(9,2)
	
	Set			@StartDate	=	@InputDate;			
	Set			@DateEnd	=	@InputDate;
	
	While		Datepart(Weekday, @StartDate) <> 1
					
				Begin
					Set	@StartDate = DateAdd(Day, -1, @StartDate)
				End	

	Select @CCQty = COUNT(DT.TOTL)
	From DLTOTS DT 
	Inner Join vwCORHDRFull COR	on	DT.ORDN		= COR.NUMB
	Where	TCOD 			= 'SA'
			and COR.IS_CLICK_AND_COLLECT = 1
			and DT.DATE1	>=	@StartDate
			and DT.DATE1	<=	@DateEnd;

	Set			@CCQty		=	isnull(@CCQty, 0);
		
	RETURN		@CCQty

END
GO

IF @@Error = 0
   Print 'Success: The Create Scalar-values Function svf_ReportClickAndCollectSalesQtyWTD for US14255 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function svf_ReportClickAndCollectSalesQtyWTD for US14255 has not been deployed successfully'
GO