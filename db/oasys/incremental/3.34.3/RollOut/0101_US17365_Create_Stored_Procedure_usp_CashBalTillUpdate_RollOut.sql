SET ANSI_NULLS ON
GO
SET ANSI_WARNINGS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CashBalTillUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[usp_CashBalTillUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

ALTER PROCEDURE [dbo].[usp_CashBalTillUpdate]
@TransactionDate date,
@TillNumber CHAR (2),
@TransactionNumber CHAR (4)
AS
BEGIN

SET NOCOUNT OFF
        
   
create table #BankingTemp
(
   
    RID INT IDENTITY(1,1),
    [PeriodID] [int] NOT NULL,
	[TillID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[Value] [decimal](9,2) NULL,           
	[Discount] [decimal](9,2) NULL,       
	[Type] [char](9) NULL	
)

create table #CashBalTillTemp
(
    [PeriodID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[TillID] [int] NOT NULL,
	[GrossSalesAmount] [decimal](8, 2) NOT NULL,
	[DiscountAmount] [decimal](8, 2) NOT NULL,
	[SalesCount] [decimal](4, 0) NOT NULL,
	[SalesAmount] [decimal](8, 2) NOT NULL,
	[RefundCount] [decimal](4, 0) NOT NULL,
	[RefundAmount] [decimal](8, 2) NOT NULL,
	[NumTransactions] [int] NOT NULL
	
)

INSERT INTO #BankingTemp ([PeriodID],[CurrencyID],[TillID] ,[Value],[Discount] ,[Type])
SELECT 
    (select id from SystemPeriods
     where
		(DT.CASH = '800' and StartDate = dt.DATE1 or 
		 DT.CASH = '499' and StartDate = DATEADD(DAY, DATEDIFF(DAY, 0, dt.ReceivedDate), 0))
    ) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.TILL as int) as TillID, 
	ISNULL(dt.TOTL,0) as Value, 
	ISNULL(dt.DISC,0) as Discount, 
	dt.TCOD as Type
	
FROM DLTOTS as DT where
DT.CASH in ('800', '499') and dt.DATE1 = @TransactionDate and DT.TILL = @TillNumber and dt.[TRAN] = @TransactionNumber 


INSERT INTO #CashBalTillTemp
SELECT [PeriodID],[CurrencyID],[TillID],[GrossSalesAmount],[DiscountAmount],[SalesCount],[SalesAmount],[RefundCount],[RefundAmount],[NumTransactions]
 FROM CashBalTill 

    DECLARE @RID_1 INT
	DECLARE @MAXID_1 INT
	SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID_1 = 1
	DECLARE @PERIODID INT
	DECLARE @CURRENCY CHAR(5)
	DECLARE @TILL_ID INT
	DECLARE @GrossAmount INT
	DECLARE @LINEUPDATED AS INT
	
	
 
-- looping the #temp_old from first record to last record.
	WHILE(@RID_1<=@MAXID_1)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @PERIODID = PeriodID,@CURRENCY= CurrencyID,@TILL_ID= TillID  FROM #BankingTemp WHERE RID = @RID_1
			-- if the Primary key exists then update, by taking the values from the #temp_old  
            SET @LINEUPDATED = 0
			IF EXISTS(SELECT 1 FROM #CashBalTillTemp WHERE PeriodID  = @PERIODID AND CurrencyID = @CURRENCY AND TillID=@TILL_ID)
				BEGIN
                    
                    UPDATE CashBalTill  
					SET	
					    [DiscountAmount]  = T.Discount,
	                    [SalesCount]  = (Case  when T.[type]= 'SA'
	                                           then [SalesCount] + 1 else [SalesCount]
	                                     end),
	                    [SalesAmount]  = (Case  when T.[type]= 'SA'
	                                           then  [SalesAmount] + T.Value else [SalesAmount]
	                                      end),
	                    [RefundCount] = (Case  when T.[type]= 'RF'
	                                           then [RefundCount] + 1 else [RefundCount]
	                                     end),
	                    [RefundAmount]  = (Case  when T.[type]= 'RF'
	                                           then [RefundAmount] + T.Value else [RefundAmount]
	                                       end),
	                     
	                    [GrossSalesAmount] =  (Case  when  T.[type] in ('SA', 'RF')
													then [GrossSalesAmount] + T.Value else [GrossSalesAmount]
											   end),
	                    
	                    [NumTransactions]= [NumTransactions]+1
	                     
					    
					    					    					    				    					    										
					FROM #BankingTemp AS T WHERE (CashBalTill.PeriodID = @PERIODID AND T.PeriodID = @PERIODID) AND (CashBalTill.CurrencyID = @CURRENCY and  T.CurrencyID =@CURRENCY ) AND (CashBalTill.TillID = @TILL_ID and  T.TillID = @TILL_ID )
					SET @LINEUPDATED = @LINEUPDATED + @@ROWCOUNT 
				END
			ELSE -- ELSE INSERT NEW RECORD
				BEGIN
					INSERT INTO CashBalTill (PeriodID,CurrencyID,TillID,[GrossSalesAmount],
					[DiscountAmount],[SalesCount],[SalesAmount],[RefundCount]
					,[RefundAmount],[NumTransactions])
					
					SELECT T.PeriodID,T.CurrencyID,T.TillID,T.Value,T.Discount,(Case  when T.[type]= 'SA'then 1 else 0 end),
					(Case  when T.[type]= 'SA'then T.Value else 0  end),(Case  when T.[type]= 'RF'then 1 else 0 end),
					(Case  when T.[type]= 'RF'then T.Value else 0  end),
					1	                					
					FROM #BankingTemp   AS T 
					WHERE T.RID = @RID_1
					SET @LINEUPDATED = @LINEUPDATED + @@ROWCOUNT 
				END

            delete #BankingTemp where RID = @RID_1 
			SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp

            delete #CashBalTillTemp  
            INSERT INTO #CashBalTillTemp
            SELECT [PeriodID],[CurrencyID],[TillID],[GrossSalesAmount],[DiscountAmount],[SalesCount],[SalesAmount],[RefundCount],[RefundAmount],[NumTransactions]
            FROM CashBalTill              
               
			SET @RID_1 = @RID_1+1 -- increment loop count
		END
RETURN @LINEUPDATED

END;
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure usp_CashBalTillUpdate for US17365 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure usp_CashBalTillUpdate for US17365 has not been deployed successfully'
GO
