ALTER procedure [dbo].[QodGetNonZeroStockOrOutstandingIbt]
as
begin

select * from 
	vwQodNonZeroStockOrOutstandingIbt
where
	not (QtyOnHand=0 and QtyOutstanding=0)
order 
	by SkuNumber;

select 
	cl.SKUN					as SkuNumber,
	cl.NUMB					as OrderNumber,
	cl.LINE					as Number,
	vc.OMOrderNumber,
	vc.CUST					as VendaNumber,
	cl.QTYO					as QtyOrdered,
	cl.QTYR					as QtyReturned,
	cl.QTYT					as QtyTaken,
	cl.QTYO-cl.QTYR-cl.QTYT as QtyOutstanding,
	cl.DeliveryStatus
from 
	corlin cl
--Tables CORHDR and CORHDR4 have been replaced with the view vwCORHDRFull	
left join vwCORHDRFull as vc on cl.NUMB = vc.NUMB 
where
	cl.DeliveryStatus <= 399
	and cl.QTYO - cl.QTYR - cl.QTYT <> 0
	and (vc.NUMB is null or vc.SHOW_IN_UI = 1)
order by cl.SKUN
end
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure QodGetNonZeroStockOrOutstandingIbt for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure QodGetNonZeroStockOrOutstandingIbt for US14227 has not been deployed successfully'
GO
