SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:Michael O'Cain
-- Create date: 02/08/2011
-- Description:	Get the next till and transaction number from the store
--
-- Author: Sean Moir
-- Create date: 12/08/2011
-- Description:	Refactored to ensure that returned value is always 6 characters long
-- =============================================
ALTER PROCEDURE [dbo].[usp_getNextTillTranNumber]
      @TillTranNumber char(6)='000000' out
as

begin
	set nocount on
	
	declare @ID int

	insert into TillTranNumber([DateInserted]) values (GETDATE())

	set @ID = @@IDENTITY
	
	set @TillTranNumber = '0' + RIGHT('00000' + CAST(@ID as varchar(10)), 5)

	-- check that Till number part is not zero
	if LEFT(@TillTranNumber, 2) = '00'
		begin
			declare @ReSeed int = @ID + 10000
		
			-- add 10,000 to the identity value
			dbcc checkident (TillTranNumber, reseed, @ReSeed)
			
			set @ID = @ReSeed
		end

	-- check that Transaction part is not zero
	if RIGHT(@TillTranNumber, 4) = '0000'
		begin
			insert into TillTranNumber([DateInserted]) values (GETDATE())

			set @ID = @@IDENTITY 
		end

	set @TillTranNumber = '0' + RIGHT('00000' + CAST(@ID as varchar(10)), 5)		
	
end
GO
