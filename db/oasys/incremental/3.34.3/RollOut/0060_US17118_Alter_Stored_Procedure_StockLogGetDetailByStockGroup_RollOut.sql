SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[StockLogGetDetailByStockGroup]
	@SkuNumber		char(6),
	@GroupIds		varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @TkeyField Char(6)   
	
	select
		RowId = 1,
		sl.TKEY					as 'TKEY',
		sl.DATE1				as 'Date',
		sl.TIME					as 'Time',
		sl.EEID					as 'UserId',
  	    CASE
			when Type = 31 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 31 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 32 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 32 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 33 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 33 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 34 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 34 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 35 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 35 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 36 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 36 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 37 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 37 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 66 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 66 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 67 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 67 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 68 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 68 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			when Type = 69 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then SUBSTRING(sl.KEYS,11,2)
			when Type = 69 AND SUBSTRING(sl.KEYS,11,2) = ' ' then SUBSTRING(sl.KEYS,13,2)
			ELSE ' '
	   End						as 'Code',
   	   CASE
			when Type = 31 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 31 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 32 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 32 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 33 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 33 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 34 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 34 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 35 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 35 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 36 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 36 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 37 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 37 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 66 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 66 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 67 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 67 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 68 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 68 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
			when Type = 69 AND SUBSTRING(sl.KEYS,11,2) <> ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,11,2))
			when Type = 69 AND SUBSTRING(sl.KEYS,11,2) = ' ' then (select DESCR from SACODE where NUMB = SUBSTRING(sl.KEYS,13,2))
            when Type in (6,7) then 'C&C ' + slt.ShortDescription			Else slt.ShortDescription
		   End					as 'Description',
		sl.SPRI					as 'PriceStart',
		sl.EPRI					as 'PriceEnd',				
		sl.SSTK					as 'StockStart',
		sl.ESTK					as 'StockEnd',
		(sl.ESTK-sl.SSTK)		as 'Variance',
		sl.SMDN					as 'MarkdownStart',
		sl.EMDN					as 'MarkdownEnd',
		(sl.EMDN-sl.SMDN)		as 'VarianceMove',
		sl.SWTF					as 'WriteOffStart',		
		sl.EWTF					as 'WriteOffEnd',
		(sl.EWTF-sl.SWTF)		as 'VarianceStock',
		sl.KEYS					as 'Comment'
	from
		STKLOG sl
	inner join
		StockLogType	slt		on sl.TYPE = slt.Id
	inner join
		StockLogTypeGroup sltg	on slt.GroupId = sltg.Id
	inner join
		fnCsvToTable(@GroupIds) fn	on fn.String = sltg.Id
	where
		sl.SKUN = @SkuNumber
	order by
		sl.DATE1 DESC, sl.TKEY DESC

	SELECT sl.KEYS, sl.TYPE  
	From STKLOG sl 
	where TKEY = @TkeyField
END
go

If @@Error = 0
   Print 'Success: The Alter Stored Procedure StockLogGetDetailByStockGroup for US17118 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure StockLogGetDetailByStockGroup for US17118 has not been deployed successfully'