ALTER VIEW [dbo].[vwQODTables]
AS
SELECT 
	vc.NUMB, 
	vc.CUST, 
	vc.DATE1, 
	vc.DELD, 
	vc.CANC, 
	vc.DELI, 
	vc.DELC, 
    vc.AMDT, 
    vc.ADDR1, 
    vc.ADDR2, 
    vc.ADDR3, 
    vc.ADDR4, 
    vc.PHON, 
    vc.PRNT, 
    vc.RPRN, 
    vc.REVI, 
    vc.MVST, 
    vc.DCST, 
    vc.QTYO, 
    vc.QTYT, 
    vc.QTYR, 
    vc.WGHT, 
    vc.VOLU, 
    vc.SDAT, 
    vc.STIL, 
    vc.STRN, 
    vc.RDAT, 
    vc.RTIL, 
    vc.RTRN, 
    vc.MOBP, 
    vc.POST, 
    vc.NAME, 
    vc.OEID, 
    vc.FDID, 
    su.Name AS EmployeeName
FROM vwCORHDRFull as vc 
INNER JOIN dbo.SystemUsers as su ON vc.OEID = su.EmployeeCode
WHERE vc.SHOW_IN_UI = 1;

GO

If @@Error = 0
   Print 'Success: The Alter View vwQODTables for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter View vwQODTables for US14227 has not been deployed successfully'
GO


