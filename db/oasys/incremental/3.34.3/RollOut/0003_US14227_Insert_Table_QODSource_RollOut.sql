IF NOT EXISTS (SELECT 1 FROM [dbo].[QODSource] WHERE SOURCE_CODE = 'WO')
INSERT INTO [dbo].[QODSource]
           ([SOURCE_CODE]
           ,[SHOW_IN_UI]
           ,[SEND_UPDATES_TO_OM]
           ,[ALLOW_REMOTE_CREATE]
		   ,[IS_CLICK_AND_COLLECT])
SELECT CAST('WO' AS [char](2)) [SOURCE_CODE], CAST(1 AS [BIT]) [SHOW_IN_UI], CAST(1 AS [BIT]) [SEND_UPDATES_TO_OM], CAST(0 AS [BIT]) [ALLOW_REMOTE_CREATE], CAST(0 AS [BIT]) [IS_CLICK_AND_COLLECT]
UNION ALL SELECT 'NU', 1, 1, 0, 0
UNION ALL SELECT '', 1, 1, 0, 0
UNION ALL SELECT 'HY', 0, 0, 1, 1          
GO

If @@Error = 0
   Print 'Success: The Insert Table QODSource for US14227 has been deployed successfully'
Else
   Print 'Failure: The Insert Table QODSource for US14227 has not been deployed successfully'
GO
