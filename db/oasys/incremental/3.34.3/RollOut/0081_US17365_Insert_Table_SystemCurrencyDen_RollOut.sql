SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemCurrencyDen] where [TenderID] = 12)
BEGIN

INSERT INTO [dbo].[SystemCurrencyDen]
           ([ID]
           ,[CurrencyID]
           ,[TenderID]
           ,[DisplayText]
           ,[BullionMultiple]
           ,[BankingBagLimit]
           ,[SafeMinimum]
           ,[Amendable])
     VALUES
           (0.01
           ,'GBP'
           ,12
           ,'Web Tender'
           ,0.01
           ,100000.00
           ,0.00
           ,1)

Print 'Success: Web Tender has been added successfully to SystemCurrencyDen'
END

ELSE

Print 'Failure: Web Tender has not been added successfully to SystemCurrencyDen'

GO