update RETOPT
set TTDE11 = ''
,TTDE12 = 'Web Tender'
,TTDE13 = ''
,TTDE14 = ''
,TTDE15 = ''
,TTDE16 = ''
,TTDE17 = ''
,TTDE18 = ''
,TTDE19 = ''
,TTDE20 = ''
go

If @@Error = 0
   Print 'Success: Update Table RETOPT for US15339 has been deployed successfully'
Else
   Print 'Failure: Update Table RETOPT for US15339 has not been deployed successfully'
Go