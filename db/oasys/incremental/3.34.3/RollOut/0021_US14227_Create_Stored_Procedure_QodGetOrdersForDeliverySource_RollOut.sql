SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'QodGetOrdersForDeliverySource') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure QodGetOrdersForDeliverySource'
	EXEC ('CREATE PROCEDURE QodGetOrdersForDeliverySource AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure QodGetOrdersForDeliverySource')
GO


ALTER PROCEDURE QodGetOrdersForDeliverySource
	@deliveryStoreID char(4),
	@selectDelivery bit -- if 1 - select deliveries, if 0 - collections
as
begin
	set nocount on;
	
	SELECT DISTINCT
			vc.NUMB,
			vc.DATE1,
			vc.DELC,
			vc.DELD,
			vc.CUST,
			vc.NAME, 
			vc.ADDR1, 
			vc.ADDR2, 
			vc.ADDR3, 
			vc.ADDR4, 
			vc.POST,
			vc.PHON,
			vc.DELI,
			vc.STIL,
			vc.SDAT,
			vc.STRN,
			vc.DeliveryStatus, 
			lin.DeliverySource
		FROM vwCORHDRFull as vc
			INNER JOIN CORLIN as lin ON vc.NUMB = lin.NUMB
		WHERE 
			vc.DELI = @selectDelivery
			AND (vc.QTYO + vc.QTYR - vc.QTYT > 0)
			AND (vc.STIL IS NULL OR vc.STIL < 13)
			AND vc.DELC = 0 
			AND vc.SHOW_IN_UI = 1
			AND lin.DeliverySource = @deliveryStoreID;
			
end
go

If @@Error = 0
   Print 'Success: The Create Stored Procedure QodGetOrdersForDeliverySource for US14227 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure QodGetOrdersForDeliverySource for US14227 has not been deployed successfully'
Go
