SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'svf_ReportCoreRefundsQtyDAY') AND type in (N'FN'))
BEGIN
	PRINT 'Creating function svf_ReportCoreRefundsQtyDAY'
	EXEC ('CREATE FUNCTION svf_ReportCoreRefundsQtyDAY() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportCoreRefundsQtyDAY')
GO

ALTER FUNCTION [dbo].[svf_ReportCoreRefundsQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CoreRefundQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

	----------------------------------------------------------------------------------
	-- Get Core REFUND Qty - DAY
	----------------------------------------------------------------------------------
	Select			@CoreRefundQty	=		count(TOTL)
	From			DLTOTS DT left join vwCORHDRFull COR on DT.ORDN = COR.NUMB 
	Where			TCOD			=		'RF'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and DT.Date1	=		@StartDate
					and (cor.Source IS NULL or cor.IS_CLICK_AND_COLLECT = 0)

	Set				@CoreRefundQty	=		isnull(@CoreRefundQty, 0);
		
	RETURN			@CoreRefundQty

END

GO
IF @@Error = 0
   Print 'Success: The Create Scalar-values Function svf_ReportCoreRefundsQtyDAY for US15772 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function svf_ReportCoreRefundsQtyDAY for US15772 has not been deployed successfully'
GO