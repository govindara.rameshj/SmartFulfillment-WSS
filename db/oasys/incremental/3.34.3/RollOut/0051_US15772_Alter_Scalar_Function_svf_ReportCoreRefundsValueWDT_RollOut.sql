SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'svf_ReportCoreRefundsValueWTD') AND type in (N'FN'))
BEGIN
	PRINT 'Creating function svf_ReportCoreRefundsValueWTD'
	EXEC ('CREATE FUNCTION svf_ReportCoreRefundsValueWTD() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportCoreRefundsValueWTD')
GO

ALTER FUNCTION [dbo].[svf_ReportCoreRefundsValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@CoreRefundValue	numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Core REFUND Value - WTD
	----------------------------------------------------------------------------------
	Select			@CoreRefundValue	=		sum(TOTL)
	From			DLTOTS DT left join vwCORHDRFull COR on DT.ORDN = COR.NUMB 
	Where			TCOD				=		'RF'
					and CASH			<>		'000'
					and VOID			=		0
					and PARK			=		0
					and TMOD			=		0  
					and DT.Date1		>=		@StartDate
					and DT.Date1		<=		@DateEnd
					and (cor.Source IS NULL or cor.IS_CLICK_AND_COLLECT = 0)

	Set				@CoreRefundValue	=		isnull(@CoreRefundValue, 0);
		
	RETURN			@CoreRefundValue

END

GO
IF @@Error = 0
   Print 'Success: The Create Scalar-values Function svf_ReportCoreRefundsValueWTD for US14255 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function svf_ReportCoreRefundsValueWTD for US14255 has not been deployed successfully'
GO