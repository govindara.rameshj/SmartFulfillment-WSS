if OBJECT_ID('usp_GetDeliveryStatusLogInfo') is null
	exec('create procedure dbo.usp_GetDeliveryStatusLogInfo as select 1')
go

ALTER PROCEDURE [dbo].[usp_GetDeliveryStatusLogInfo]
@DateInterestedIn DateTime
AS
BEGIN
	SELECT 
		chl.NewStatus, 
		COUNT(*) as Total 
	FROM CORHDR4LOG chl
	LEFT JOIN vwCORHDRFull vch ON vch.NUMB = isnull(chl.NUMB, '')
	WHERE chl.DateChanged = @DateInterestedIn and (chl.NUMB is null or vch.SHOW_IN_UI = 1) 
	GROUP BY chl.NewStatus 
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_GetDeliveryStatusLogInfo for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_GetDeliveryStatusLogInfo for US14227 has not been deployed successfully'
GO
