SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[usp_GetTenderDescriptions]
AS
BEGIN
    SELECT TTDE1 , 
           TTDE2 , 
           TTDE3 , 
           TTDE4 , 
           TTDE5 , 
           TTDE6 , 
           TTDE7 , 
           TTDE8 , 
           TTDE9 , 
           TTDE10,
           TTDE11,
           TTDE12,
           TTDE13,
           TTDE14,
           TTDE15,
           TTDE16,
           TTDE17,
           TTDE18,
           TTDE19,
           TTDE20
      FROM RETOPT
      WHERE FKEY = '01';
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_GetTenderDescriptions for DE00807 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_GetTenderDescriptions for DE00807 has not been deployed successfully'
GO
