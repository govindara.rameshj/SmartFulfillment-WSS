SET ANSI_NULLS ON
GO
SET ANSI_WARNINGS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnableTillTranNumber]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[EnableTillTranNumber](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateInserted] [datetime2](7) NOT NULL,
CONSTRAINT [PK_EnableTillTranNumber] PRIMARY KEY CLUSTERED ([Id])
) ON [PRIMARY]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till and transaction numbers generator for Enable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EnableTillTranNumber'

END
GO

If @@Error = 0
   Print 'Success: The Create Table EnableTillTranNumber for US17362 has been deployed successfully'
Else
   Print 'Failure: The Create Table EnableTillTranNumber for US17362 has not been deployed successfully'
GO
