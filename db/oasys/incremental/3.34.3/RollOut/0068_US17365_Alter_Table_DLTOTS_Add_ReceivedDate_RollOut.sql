SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'DLTOTS' and col.name = 'ReceivedDate'
)
BEGIN
PRINT 'Add ReceivedDate column to DLTOTS'
 ALTER TABLE [dbo].[DLTOTS] ADD ReceivedDate datetime 
 CONSTRAINT DF_DLTOTS_ReceivedDate DEFAULT getdate()
END

GO

IF NOT EXISTS (
select 1 from sys.indexes idx
inner join sys.objects obj on obj.object_id = idx.object_id
where obj.Name = 'DLTOTS' and idx.name = 'IX_DLTOTS_ReceivedDate'
)
BEGIN
	PRINT 'Add index for table DLTOTS on ReceivedDate column'
	create index IX_DLTOTS_ReceivedDate on [dbo].[DLTOTS] (ReceivedDate)
END

GO

IF @@Error = 0
   Print 'Success: Add ReceivedDate column to DLTOTS for US17365 has been deployed successfully'
ELSE
   Print 'Failure: Add ReceivedDate column to DLTOTS for US17365 has not been deployed successfully'
GO