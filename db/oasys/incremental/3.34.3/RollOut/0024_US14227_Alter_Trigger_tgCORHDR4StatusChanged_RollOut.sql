if not exists (select * from sys.triggers where type = 'TR' and name = 'tgCORHDR4StatusChanged')
begin
  EXEC ('CREATE TRIGGER dbo.tgCORHDR4StatusChanged
             ON dbo.CORHDR4
             AFTER INSERT
           AS
             BEGIN
               SELECT 1
             END') 
end
go

ALTER trigger [dbo].[tgCORHDR4StatusChanged] 
ON [dbo].[CORHDR4] AFTER UPDATE
AS
	IF UPDATE(DeliveryStatus)
		BEGIN
				insert into [dbo].CORHDR4Log (DateChanged, OldStatus, NewStatus, NUMB)
					SELECT GETDATE(), d.DeliveryStatus, i.DeliveryStatus, d.NUMB FROM inserted i INNER JOIN deleted d on i.NUMB = d.NUMB 
					Where i.DeliveryStatus <> d.DeliveryStatus AND (i.DeliveryStatus = 700 or i.DeliveryStatus = 800 or i.DeliveryStatus = 900)
		END
GO

If @@Error = 0
   Print 'Success: Alter Trigger tgCORHDR4StatusChanged for US14227 has been deployed successfully'
Else
   Print 'Failure: Alter Trigger tgCORHDR4StatusChanged for US14227 has not been deployed successfully'
Go