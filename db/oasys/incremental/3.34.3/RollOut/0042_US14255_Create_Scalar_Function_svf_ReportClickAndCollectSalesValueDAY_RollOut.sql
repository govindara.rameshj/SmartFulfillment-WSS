SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'svf_ReportClickAndCollectSalesValueDAY') AND type in (N'FN'))
BEGIN
	PRINT 'Creating function svf_ReportClickAndCollectSalesValueDAY'
	EXEC ('CREATE FUNCTION svf_ReportClickAndCollectSalesValueDAY() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportClickAndCollectSalesValueDAY')
GO

ALTER FUNCTION [dbo].[svf_ReportClickAndCollectSalesValueDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CCWeekValue		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

	
	Select @CCWeekValue = SUM(DT.TOTL)
	From DLTOTS DT 
	Inner Join vwCORHDRFull COR	on	DT.ORDN		= COR.NUMB
	Where	TCOD 			= 'SA'
			and COR.IS_CLICK_AND_COLLECT = 1
			and DT.DATE1	=	@StartDate;
	
	Set		@CCWeekValue		=	isnull(@CCWeekValue, 0)
		
	RETURN	@CCWeekValue

END
GO

IF @@Error = 0
   Print 'Success: The Create Scalar-values Function svf_ReportClickAndCollectSalesValueDAY for US14255 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function svf_ReportClickAndCollectSalesValueDAY for US14255 has not been deployed successfully'
GO