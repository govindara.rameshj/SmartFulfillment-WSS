if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[CORHDR4Log]') and type in (N'U'))
begin
    print 'Table CORHDR4log was not found, creating...'
    
    CREATE TABLE [dbo].[CORHDR4Log](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[DateChanged] [date] NOT NULL,
		[OldStatus] [int] NOT NULL,
		[NewStatus] [int] NOT NULL,
	CONSTRAINT [PK_CORHDR4Log] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end
go

if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'NUMB' and t.[name] = 'CORHDR4Log' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.CORHDR4Log add [NUMB] char(6) null
end
GO

If @@Error = 0
   Print 'Success: Alter Table CORHDR4Log for US14227 has been deployed successfully'
Else
   Print 'Failure: Alter Table CORHDR4Log for US14227 has not been deployed successfully'
Go