SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'svf_ReportCoreRefundsValueDAY') AND type in (N'FN'))
BEGIN
	PRINT 'Creating function svf_ReportCoreRefundsValueDAY'
	EXEC ('CREATE FUNCTION svf_ReportCoreRefundsValueDAY() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportCoreRefundsValueDAY')
GO

ALTER FUNCTION [dbo].[svf_ReportCoreRefundsValueDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CoreRefundValue	numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Core REFUND Value - DAY
	----------------------------------------------------------------------------------
	Select			@CoreRefundValue	=		sum(TOTL)
	From			DLTOTS DT left join vwCORHDRFull COR on DT.ORDN = COR.NUMB 
	Where			TCOD				=		'RF'
					and CASH			<>		'000'
					and VOID			=		0
					and PARK			=		0
					and TMOD			=		0  
					and DT.Date1		=		@StartDate
					and (cor.Source IS NULL or cor.IS_CLICK_AND_COLLECT = 0)

	Set				@CoreRefundValue	=		isnull(@CoreRefundValue, 0);
		
	RETURN			@CoreRefundValue

END

GO
IF @@Error = 0
   Print 'Success: The Create Scalar-values Function svf_ReportCoreRefundsValueDAY for US14255 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function svf_ReportCoreRefundsValueDAY for US14255 has not been deployed successfully'
GO