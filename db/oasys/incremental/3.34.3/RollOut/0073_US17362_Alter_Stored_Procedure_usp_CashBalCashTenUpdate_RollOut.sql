SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[usp_CashBalCashTenUpdate]
@TransactionDate date,
@TillNumber CHAR (2),
@TransactionNumber CHAR (4)
AS
BEGIN

SET NOCOUNT OFF
        
create table #BankingTemp1
(
   
    RID INT IDENTITY(1,1),
    [PeriodID] [int] NOT NULL,
	[CashierID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[ID] [decimal](9,2) NOT NULL,	
	[Amount] [decimal](9,2) NULL,
	[PickUp] [decimal](9, 2) NOT NULL	        
)

create table #CashBalTenTemp
(
    [PeriodID] [int] NOT NULL,
	[CashierID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[ID] [int] NOT NULL,
	[Quantity] [decimal](5, 0) NOT NULL,
	[Amount] [decimal](9, 2) NOT NULL,
	[PickUp] [decimal](9, 2) NOT NULL
)

INSERT INTO #BankingTemp1 ([PeriodID],[CurrencyID],[CashierID] ,[ID],[Amount],[PickUp])
SELECT 

    (select id from SystemPeriods
     where
		(DT.CASH = '800' and StartDate = dt.DATE1 or 
		 DT.CASH = '499' and StartDate = DATEADD(DAY, DATEDIFF(DAY, 0, dt.ReceivedDate), 0))
    ) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	dp.[Type] as ID,
	ISNULL(dp.AMNT,0) as Amount,
	'0'	
	
FROM DLTOTS as DT 
     inner join DLPAID DP on  dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL	
where 
DT.CASH in ('800', '499') and DT.DATE1 = @TransactionDate and DT.TILL = @TillNumber and dt.[TRAN] = @TransactionNumber 


INSERT INTO #CashBalTenTemp 
SELECT * FROM CashBalCashierTen 

    DECLARE @RID_1 INT
	DECLARE @MAXID_1 INT
	SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp1 -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID_1 = 1
	DECLARE @PERIODID INT
	DECLARE @CURRENCY CHAR(5)
	DECLARE @CASHIER_ID INT
	DECLARE @ID INT
	DECLARE @LINEUPDATED AS INT
	 
-- looping the #temp_old from first record to last record.
	WHILE(@RID_1<=@MAXID_1)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @PERIODID = PeriodID,@CURRENCY= CurrencyID,@CASHIER_ID= CashierID, @ID = ID FROM #BankingTemp1 WHERE RID = @RID_1
			-- if the Primary key exists then update, by taking the values from the #temp_old  
	    SET @LINEUPDATED = 0
		IF EXISTS(SELECT 1 FROM #CashBalTenTemp WHERE PeriodID  = @PERIODID AND CurrencyID = @CURRENCY AND CashierID=@CASHIER_ID AND ID = @ID)
		
		  BEGIN
		   UPDATE CashBalCashierTen 
		   SET 
			[Quantity] = CashBalCashierTen.[Quantity]+1 ,
	        [Amount] = CashBalCashierTen.[Amount]-T.Amount
	        FROM #BankingTemp1 AS T WHERE (CashBalCashierTen.PeriodID = @PERIODID AND T.PeriodID = @PERIODID) AND (CashBalCashierTen.CurrencyID = @CURRENCY and  T.CurrencyID =@CURRENCY ) AND (CashBalCashierTen.CashierID = @CASHIER_ID and  T.CashierID = @CASHIER_ID) AND (CashBalCashierTen.ID  = @ID  AND T.ID = @ID)
	        SET @LINEUPDATED = @LINEUPDATED +@@ROWCOUNT 
	     END
     ELSE
       BEGIN
        INSERT INTO CashBalCashierTen(PeriodID,CurrencyID,CashierID,ID,Quantity,Amount,PickUp)
        SELECT T.PeriodID, T.CurrencyID,T.CashierID,T.ID, 1,-(T.Amount),T.PickUp
      	FROM #BankingTemp1   AS T 
		WHERE T.RID = @RID_1
		SET @LINEUPDATED = @LINEUPDATED +@@ROWCOUNT 
     END
     
     delete #BankingTemp1 where RID = @RID_1
	 SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp1
				
     delete #CashBalTenTemp  
     INSERT INTO #CashBalTenTemp
     SELECT * FROM CashBalCashierTen  
     	  
SET @RID_1 = @RID_1+1 
 END
 RETURN @LINEUPDATED
END

GO


