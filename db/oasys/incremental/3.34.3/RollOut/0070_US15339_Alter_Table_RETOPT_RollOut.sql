if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE11' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE11] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE12' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE12] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE13' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE13] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE14' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE14] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE15' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE15] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE16' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE16] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE17' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE17] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE18' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE18] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE19' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE19] char(12) null
end
GO
if not exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'TTDE20' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT add [TTDE20] char(12) null
end
GO

If @@Error = 0
   Print 'Success: Alter Table RETOPT for US15339 has been deployed successfully'
Else
   Print 'Failure: Alter Table RETOPT for US15339 has not been deployed successfully'
Go