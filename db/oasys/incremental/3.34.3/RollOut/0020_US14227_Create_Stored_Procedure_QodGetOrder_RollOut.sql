SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'QodGetOrder') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure QodGetOrder'
	EXEC ('CREATE PROCEDURE QodGetOrder AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure QodGetOrder')
GO


ALTER PROCEDURE QodGetOrder
	@orderNumber char(6)
as
begin
	set nocount on;
	SELECT DISTINCT
		vc.NUMB,
		vc.DATE1,
		vc.DELC,
		vc.DELD,
		vc.CUST,
		vc.NAME,
		vc.ADDR1,
		vc.ADDR2, 
		vc.ADDR3, 
		vc.ADDR4, 
		vc.POST, 
		vc.PHON,
		vc.DELI,
		vc.STIL,
		vc.SDAT,
		vc.STRN,
		vc.DeliveryStatus,
		lin.DeliverySource
		FROM vwCORHDRFull as vc
			INNER JOIN QUOHDR as q ON vc.NUMB = q.NUMB
			INNER JOIN CORLIN as lin ON vc.NUMB = lin.NUMB
		WHERE vc.NUMB = @orderNumber and vc.SHOW_IN_UI = 1;
		
end
go

If @@Error = 0
   Print 'Success: The Create Stored Procedure QodGetOrder for US14227 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure QodGetOrder for US14227 has not been deployed successfully'
Go
