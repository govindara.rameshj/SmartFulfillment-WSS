SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'svf_ReportCoreSalesQtyDAY') AND type in (N'FN'))
BEGIN
	PRINT 'Creating function svf_ReportCoreSalesQtyDAY'
	EXEC ('CREATE FUNCTION svf_ReportCoreSalesQtyDAY() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_ReportCoreSalesQtyDAY')
GO

ALTER FUNCTION [dbo].[svf_ReportCoreSalesQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CoreDayQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Core SALES Qty - DAY
	----------------------------------------------------------------------------------
	Select			@CoreDayQty		=		count(TOTL)
	From			DLTOTS DT left join vwCORHDRFull COR on DT.ORDN = COR.NUMB 
	Where			TCOD			=		'SA'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and DT.Date1	=		@StartDate
					and (cor.Source IS NULL or cor.IS_CLICK_AND_COLLECT = 0)

	Set				@CoreDayQty	=		isnull(@CoreDayQty, 0);
		
	RETURN			@CoreDayQty

END

GO
IF @@Error = 0
   Print 'Success: The Create Scalar-values Function svf_ReportCoreSalesQtyDAY for US14255 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function svf_ReportCoreSalesQtyDAY for US14255 has not been deployed successfully'
GO