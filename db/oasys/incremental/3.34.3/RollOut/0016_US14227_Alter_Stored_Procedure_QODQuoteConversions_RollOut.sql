-- =============================================
-- Author:		Mike O'Cain
-- Create date: 07/10/2009
-- Description:	Quote Conversions
-- =============================================
ALTER PROCEDURE [dbo].[QODQuoteConversions] 
	@DateStart datetime = Null, 
	@DateEnd datetime = Null
AS
BEGIN
	Drop Table TMP_QOD_TABLE
	
	SELECT
		 convert(varchar,vc.DELD,103)				as 'DelDate'
		 , DATENAME(dw,vc.DELD)						as 'DelDay'	 
		 , count(qh.NUMB)							as 'QuotesRaised'
		 , Count(vc.Numb)							as 'OrdersRaised'
		 ,(Count(vc.Numb) / Count(qh.Numb) * 100)	as 'PercSold'
		 , SUM(convert(int,vc.DELI))				as 'QuotesDelivered'
	INTO TMP_QOD_TABLE
	--Tables CORHDR has been replaced with the view vwCORHDRFull
	FROM vwCORHDRFull as vc, QUOHDR as qh
	Where vc.SHOW_IN_UI = 1 and vc.DELD >= @DateStart AND vc.DELD <= @DateEnd 
	GROUP BY vc.DELD

   	SELECT  DelDate									as 'Date'
		  ,	DelDay									as 'Day'
		  , QuotesRaised							as 'QuotesRaised'
 		  , OrdersRaised							as 'OrdersRaised'
		  , PercSold								as 'PercSold'
		  , QuotesDelivered							as 'QuotesDelivered'
		  , (QuotesDelivered / QuotesRaised) * 100	as 'PercDel'
	From TMP_QOD_TABLE
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure QODQuoteConversions for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure QODQuoteConversions for US14227 has not been deployed successfully'
GO