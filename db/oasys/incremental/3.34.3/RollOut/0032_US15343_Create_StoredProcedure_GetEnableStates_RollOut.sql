SET ANSI_NULLS ON
GO
SET ANSI_WARNINGS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEnableStates]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[GetEnableStates] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

ALTER PROCEDURE [dbo].[GetEnableStates]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		[State],
		[Description],
		[DeliveryStatus]
	FROM [dbo].[EnableState]

	SET NOCOUNT OFF;
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure GetEnableStates for US15343 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure GetEnableStates for US15343 has not been deployed successfully'
GO
