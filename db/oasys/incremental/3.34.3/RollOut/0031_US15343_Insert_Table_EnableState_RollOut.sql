IF NOT EXISTS (SELECT 1 FROM [dbo].[EnableState])
INSERT INTO [dbo].[EnableState](
	[State],
	[Description],
	[DeliveryStatus])
SELECT CAST('001' AS [char](3)) [State], CAST('New' AS [varchar](50)) [Description], CAST(0 AS [int]) [DeliveryStatus]
UNION ALL SELECT '002', 'Pick Note Printed', 400
UNION ALL SELECT '003', 'Order Picked', 500
UNION ALL SELECT '004', 'Order Picked Customer Notified', 600
UNION ALL SELECT '005', 'Collected & Confirmed', 900
UNION ALL SELECT '006', 'Order Cancelled & Refunded', 800
GO

If @@Error = 0
   Print 'Success: The Insert Table EnableState for US15343 has been deployed successfully'
Else
   Print 'Failure: The Insert Table EnableState for US15343 has not been deployed successfully'
GO
