SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemUsers] where [ID] = 499)
BEGIN

INSERT INTO [dbo].[SystemUsers]
           ([ID]
           ,[EmployeeCode]
           ,[Name]
           ,[Initials]
           ,[Position]
           ,[PayrollID]
           ,[Password]
           ,[PasswordExpires]
           ,[IsManager]
           ,[IsSupervisor]
           ,[SupervisorPassword]
           ,[SupervisorPwdExpires]
           ,[Outlet]
           ,[IsDeleted]
           ,[DeletedDate]
           ,[DeletedTime]
           ,[DeletedBy]
           ,[DeletedWhere]
           ,[TillReceiptName]
           ,[DefaultAmount]
           ,[LanguageCode]
           ,[SecurityProfileID])
     VALUES
           (499
           ,499
           ,'Click And Collect'
           ,'SYS'
           ,'System'
           ,'000000'
           ,'11111'
           ,'2020-12-31'
           ,0
           ,0
           ,22222
           ,NULL
           ,60
           ,0
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,'Click And Collect'
           ,100
           ,'000'
           ,110)

Print 'Success: C&C user has been added successfully to SystemUsers'
END

ELSE

Print 'Failure: C&C user has not been added to SystemUsers'

GO