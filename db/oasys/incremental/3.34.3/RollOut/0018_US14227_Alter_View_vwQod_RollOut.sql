ALTER VIEW [dbo].[vwQod]
AS
SELECT     
	vc.NUMB AS Number, 
	vc.DeliveryStatus, 
	vc.OMOrderNumber, 
	vc.IsSuspended, 
    SUM(c.QTYO * c.Price) AS Value, 
    vc.DELD AS DateDelivery
--Tables CORHDR and CORHDR4 have been replaced with the view vwCORHDRFull
FROM vwCORHDRFull as vc 
INNER JOIN dbo.CORLIN as c ON vc.NUMB = c.NUMB
WHERE vc.SHOW_IN_UI = 1
GROUP BY 
	vc.NUMB, 
	vc.DeliveryStatus, 
	vc.OMOrderNumber, 
	vc.IsSuspended, 
	vc.CANC, 
    vc.DELD
HAVING (vc.CANC <> '1')
GO

If @@Error = 0
   Print 'Success: The Alter View vwQod for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter View vwQod for US14227 has not been deployed successfully'
GO


