IF NOT EXISTS (SELECT 1 FROM [dbo].[EnableTillTranNumber])
BEGIN
SET IDENTITY_INSERT [dbo].[EnableTillTranNumber] ON

INSERT INTO [dbo].[EnableTillTranNumber]
(
	[Id],
	[DateInserted]
)
VALUES (900000, GETDATE())

SET IDENTITY_INSERT [dbo].[EnableTillTranNumber] OFF

dbcc checkident (EnableTillTranNumber, reseed, 900000)

END
GO

If @@Error = 0
   Print 'Success: The Insert Table EnableTillTranNumber for US17362 has been deployed successfully'
Else
   Print 'Failure: The Insert Table EnableTillTranNumber for US17362 has not been deployed successfully'
GO
