﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'CORHDR5' and col.name = 'IsClickAndCollect'
)
BEGIN
PRINT 'Add IsClickAndCollect column to CORHDR5'
	ALTER TABLE [dbo].[CORHDR5] ADD IsClickAndCollect bit not null 
	CONSTRAINT DF_CORHDR5_IsClickAndCollect DEFAULT 0
END

GO

If @@Error = 0
   Print 'Success: Add IsClickAndCollect column to CORHDR5 for US14227 has been deployed successfully'
ELSE
   Print 'Failure: Add IsClickAndCollect column to CORHDR5 for US14227 has not been deployed successfully'
GO		