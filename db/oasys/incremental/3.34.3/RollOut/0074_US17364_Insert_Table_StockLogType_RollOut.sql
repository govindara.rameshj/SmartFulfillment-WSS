DECLARE @TransactionGroupId int
SET @TransactionGroupId = 1 -- id of transaction group from StockLogTypeGroup
INSERT INTO [dbo].[StockLogType]
		([Id]
		,[GroupId]
		,[Description]
		,[ShortDescription])
SELECT 6, @TransactionGroupId, 'C&C Sale', 'Sale'
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[StockLogType] WHERE [Id] = 6)
UNION
SELECT 7, @TransactionGroupId, 'C&C Refund', 'Refund'
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[StockLogType] WHERE [Id] = 7)

If @@Error = 0
   Print 'Success: The Insert Table StockLogType for US14227 has been deployed successfully'
Else
   Print 'Failure: The Insert Table StockLogType for US14227 has not been deployed successfully'
GO