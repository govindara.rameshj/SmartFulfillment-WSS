SET ANSI_NULLS ON
GO
SET ANSI_WARNINGS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetStkLogSoldPerDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[GetStkLogSoldPerDay] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

ALTER PROCEDURE dbo.GetStkLogSoldPerDay
	@skun char(6),
	@dayn decimal(9,0)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @date date
	DECLARE @endOfDay datetime
	DECLARE @startOfDay datetime 
	DECLARE @startDayN decimal(9,0)
	DECLARE @startTime datetime
	DECLARE @endDayN decimal(9,0)
	DECLARE @endTime datetime

	SET @date = DATEADD(dd, @dayn, '1900-01-01')
	SET @endOfDay = dbo.udf_GetLastNightlyRoutineTime(@date)
	SET @startOfDay = dbo.udf_GetLastNightlyRoutineTime(DATEADD(dd, -1, @date))
	
	IF @endOfDay = @startOfDay
		SET @endOfDay = GETDATE()
	
	SET @startDayN = DATEDIFF(dd, '1900-01-01', @startOfDay) + 1
	SET @startTime = DATEADD(dd, -DATEDIFF(dd, '1900-01-01', @startOfDay), @startOfDay)
	
	SET @endDayN = DATEDIFF(dd, '1900-01-01', @endOfDay) + 1
	SET @endTime = DATEADD(dd, -DATEDIFF(dd, '1900-01-01', @endOfDay), @endOfDay)
	
	-- select normal movement
	SELECT ISNULL(SUM([SOLD]), 0)
	FROM (
		SELECT  CASE TYPE
					WHEN 4 THEN (SMDN - EMDN)
					ELSE (SSTK - ESTK)
				END [SOLD]
		FROM STKLOG
		WHERE TYPE IN (1, 2, 3, 4, 5, 11, 12)
			AND SKUN = @skun AND DAYN = @dayn
		UNION ALL
		-- select C&C movement
		SELECT SSTK - ESTK
		FROM STKLOG
		WHERE TYPE IN (6, 7)
			AND SKUN = @skun
			AND (@startOfDay IS NULL AND @endOfDay IS NULL AND DAYN = @dayn 
			
			OR  (@startOfDay IS NULL AND DAYN >= @dayn OR DAYN > @startDayN OR DAYN = @startDayN
			AND CONVERT(datetime, '1900-01-01 ' + SUBSTRING([TIME], 1, 2) + ':' + SUBSTRING([TIME], 3, 2) + ':' + SUBSTRING([TIME], 5, 2)) >= @startTime)

			AND (DAYN < @endDayN OR DAYN = @endDayN
			AND CONVERT(datetime, '1900-01-01 ' + SUBSTRING([TIME], 1, 2) + ':' + SUBSTRING([TIME], 3, 2) + ':' + SUBSTRING([TIME], 5, 2)) < @endTime))
	) t

	SET NOCOUNT OFF
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure GetStkLogSoldPerDay for US17341 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure GetStkLogSoldPerDay for US17341 has not been deployed successfully'
GO
