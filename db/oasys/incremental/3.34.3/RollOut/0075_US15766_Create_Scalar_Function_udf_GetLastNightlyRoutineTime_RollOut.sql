SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'udf_GetLastNightlyRoutineTime') AND type in (N'FN'))
BEGIN
	PRINT 'Creating function udf_GetLastNightlyRoutineTime'
	EXEC ('CREATE FUNCTION udf_GetLastNightlyRoutineTime() RETURNS datetime AS BEGIN RETURN GETDATE() END')
END
GO

PRINT ('Altering function udf_GetLastNightlyRoutineTime')
GO

ALTER FUNCTION [dbo].[udf_GetLastNightlyRoutineTime](
	@date date
)
RETURNS datetime
AS
BEGIN
	if @date is null
		set @date = CAST(GETDATE() as date)
	
	RETURN (SELECT TOP(1) 
				   CONVERT(datetime, CAST(nl.EDAT as CHAR(10)) + ' ' + SUBSTRING(nl.ETIM, 1, 2) + ':' + SUBSTRING(nl.ETIM, 3, 2) + ':' + SUBSTRING(nl.ETIM, 5, 2), 120) as [Date]
		    FROM [dbo].[NITLOG] nl
		    WHERE CAST(nl.TASK as int) < CAST((SELECT p.StringValue FROM [dbo].[Parameters] p WHERE ParameterID = 6001) as int)
				  AND nl.DATE1 < @date
				  AND nl.EDAT IS NOT NULL AND nl.ETIM IS NOT NULL
		    ORDER BY nl.EDAT DESC, nl.ETIM DESC, nl.TASK DESC)
END
GO

IF @@Error = 0
   Print 'Success: The Create Scalar-values Function udf_GetLastNightlyRoutineTime for US15766 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function udf_GetLastNightlyRoutineTime for US15766 has not been deployed successfully'
GO