ALTER PROCEDURE [dbo].[usp_GetNumberofDeliveriesToday] 
	-- Add the parameters for the stored procedure here
	@DateInterestedIn Date 
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT COUNT(*) as TotalDeliveries from CORHDR4 where DELD = @DateInterestedIn 
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_GetNumberofDeliveriesToday for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_GetNumberofDeliveriesToday for US14227 has not been deployed successfully'
GO