IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'svf_ReportClickAndCollectSalesValueDAY') AND type in (N'FN'))
DROP FUNCTION svf_ReportClickAndCollectSalesValueDAY
GO

IF @@Error = 0
   Print 'Success: Scalar-values Function svf_ReportClickAndCollectSalesValueDAY for US14255 has been successfully dropped'
ELSE
   Print 'Failure: Scalar-values Function svf_ReportClickAndCollectSalesValueDAY for US14255 has NOT been successfully dropped'
GO