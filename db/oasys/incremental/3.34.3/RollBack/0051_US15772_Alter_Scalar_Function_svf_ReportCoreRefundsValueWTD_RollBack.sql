SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[svf_ReportCoreRefundsValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@CoreRefundValue	numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Core REFUND Value - WTD
	----------------------------------------------------------------------------------
	Select			@CoreRefundValue		=		sum(TOTL)
	From			DLTOTS 
	Where			TCOD				=		'RF'
					and CASH			<>		'000'
					and VOID			=		0
					and PARK			=		0
					and TMOD			=		0  
					and Date1			>=		@StartDate
					and Date1			<=		@DateEnd;

	Set				@CoreRefundValue	=		isnull(@CoreRefundValue, 0);
		
	RETURN			@CoreRefundValue

END


GO


