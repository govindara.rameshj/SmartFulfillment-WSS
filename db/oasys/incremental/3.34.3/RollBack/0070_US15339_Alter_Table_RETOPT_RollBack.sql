if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE11' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE11]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE12' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE12]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE13' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE13]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE14' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE14]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE15' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE15]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE16' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE16]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE17' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE17]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE18' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE18]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE19' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE19]
end
GO
if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					  inner join sys.schemas s on s.schema_id = t.schema_id
			 where c.[name] = 'TTDE20' and t.[name] = 'RETOPT' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.RETOPT drop column [TTDE20]
end
GO

If @@Error = 0
  Print 'Success: Alter Table RETOPT for US15339 has been deployed successfully'
Else
  Print 'Failure: Alter Table RETOPT for US15339 has been deployed successfully'
Go