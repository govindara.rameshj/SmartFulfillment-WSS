IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'udf_GetLastNightlyRoutineTime') AND type in (N'FN'))
DROP FUNCTION udf_GetLastNightlyRoutineTime
GO

IF @@Error = 0
   Print 'Success: Scalar-values Function udf_GetLastNightlyRoutineTime for US15766 has been successfully dropped'
ELSE
   Print 'Failure: Scalar-values Function udf_GetLastNightlyRoutineTime for US15766 has NOT been successfully dropped'
GO