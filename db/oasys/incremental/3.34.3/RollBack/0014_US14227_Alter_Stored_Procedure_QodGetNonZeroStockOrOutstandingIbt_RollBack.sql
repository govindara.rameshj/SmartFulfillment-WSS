ALTER procedure [dbo].[QodGetNonZeroStockOrOutstandingIbt]
as
begin

select * from 
	vwQodNonZeroStockOrOutstandingIbt
where
	not (QtyOnHand=0 and QtyOutstanding=0)
order 
	by SkuNumber;

select 
	cl.SKUN					as SkuNumber,
	cl.NUMB					as OrderNumber,
	cl.LINE					as Number,
	ch4.OMOrderNumber,
	ch.CUST					as VendaNumber,
	cl.QTYO					as QtyOrdered,
	cl.QTYR					as QtyReturned,
	cl.QTYT					as QtyTaken,
	cl.QTYO-cl.QTYR-cl.QTYT as QtyOutstanding,
	cl.DeliveryStatus
from 
	corlin cl
left join
	CORHDR4 ch4 on cl.NUMB=ch4.NUMB
left join
	CORHDR ch on ch.NUMB=cl.NUMB
where
	cl.DeliveryStatus<=399
	and cl.QTYO-cl.QTYR-cl.QTYT <>0
order by
	cl.SKUN;
end
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure QodGetNonZeroStockOrOutstandingIbt for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure QodGetNonZeroStockOrOutstandingIbt for US14227 has not been deployed successfully'
GO