SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'DLTOTS' and col.name = 'ReceivedDate'
)
BEGIN
	begin tran
	PRINT 'drop index for DLTOTS ReceivedDate '
	drop index IX_DLTOTS_ReceivedDate on dbo.DLTOTS;

	PRINT 'drop ReceivedDate column from DLTOTS'
	ALTER TABLE dbo.DLTOTS DROP DF_DLTOTS_ReceivedDate
	ALTER TABLE dbo.DLTOTS DROP COLUMN ReceivedDate
	commit
END

GO

IF @@Error = 0
   Print 'Success: Drop ReceivedDate column from DLTOTS for US17365 has been deployed successfully'
ELSE
   Print 'Failure: Drop ReceivedDate column from DLTOTS for US17365 has not been deployed successfully'
GO