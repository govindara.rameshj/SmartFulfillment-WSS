ALTER VIEW [dbo].[vwQod]
AS
SELECT     dbo.CORHDR.NUMB AS Number, dbo.CORHDR4.DeliveryStatus, dbo.CORHDR4.OMOrderNumber, dbo.CORHDR4.IsSuspended, 
                      SUM(dbo.CORLIN.QTYO * dbo.CORLIN.Price) AS Value, dbo.CORHDR.DELD AS DateDelivery
FROM         dbo.CORHDR INNER JOIN
                      dbo.CORHDR4 ON dbo.CORHDR.NUMB = dbo.CORHDR4.NUMB INNER JOIN
                      dbo.CORLIN ON dbo.CORHDR.NUMB = dbo.CORLIN.NUMB
GROUP BY dbo.CORHDR.NUMB, dbo.CORHDR4.DeliveryStatus, dbo.CORHDR4.OMOrderNumber, dbo.CORHDR4.IsSuspended, dbo.CORHDR.CANC, 
                      dbo.CORHDR.DELD
HAVING      (dbo.CORHDR.CANC <> '1')
GO

If @@Error = 0
   Print 'Success: The Alter View vwQod for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter View vwQod for US14227 has not been deployed successfully'
GO