SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[svf_ReportCoreSalesQtyWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@CoreWeekQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Core SALES Qty - WDT
	----------------------------------------------------------------------------------
	Select			@CoreWeekQty	=		count(TOTL)
	From			DLTOTS 
	Where			TCOD			=		'SA'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1	>=		@StartDate
					and Date1	<=		@DateEnd

	Set				@CoreWeekQty	=		isnull(@CoreWeekQty, 0);
		
	RETURN			@CoreWeekQty

END


GO


