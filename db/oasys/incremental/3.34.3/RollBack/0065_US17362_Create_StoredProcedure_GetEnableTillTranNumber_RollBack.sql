IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEnableTillTranNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [dbo].[GetEnableTillTranNumber]
END
GO

If @@Error = 0
   Print 'Success: The Stored Procedure GetEnableTillTranNumber for US17362 has been successfully dropped'
Else
   Print 'Failure: The Stored Procedure GetEnableTillTranNumber for US17362 has not been successfully dropped'
GO
