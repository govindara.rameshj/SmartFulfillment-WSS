IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'svf_ReportClickAndCollectRefundsValueDAY') AND type in (N'FN'))
DROP FUNCTION svf_ReportClickAndCollectRefundsValueDAY
GO

IF @@Error = 0
   Print 'Success: Scalar-values Function svf_ReportClickAndCollectRefundsValueDAY for US15772 has been successfully dropped'
ELSE
   Print 'Failure: Scalar-values Function svf_ReportClickAndCollectRefundsValueDAY for US15772 has NOT been successfully dropped'
GO