SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'CORHDR5' and col.name = 'IsClickAndCollect'
)
BEGIN
PRINT 'drop ReceivedDate column from CORHDR5'
		ALTER TABLE dbo.CORHDR5 DROP DF_CORHDR5_IsClickAndCollect
		ALTER TABLE dbo.CORHDR5 DROP COLUMN IsClickAndCollect
END

GO

IF @@Error = 0
   Print 'Success: Drop IsClickAndCollect column from CORHDR5 for US14227 has been deployed successfully'
ELSE
   Print 'Failure: Drop IsClickAndCollect column from CORHDR5 for US14227 has not been deployed successfully'
GO