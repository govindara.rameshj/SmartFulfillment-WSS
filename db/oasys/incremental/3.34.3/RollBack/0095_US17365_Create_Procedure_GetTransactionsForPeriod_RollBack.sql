SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'GetTransactionsForPeriod') AND type in (N'P'))
BEGIN
	PRINT 'Dropping procedure GetTransactionsForPeriod'
	DROP PROCEDURE dbo.GetTransactionsForPeriod;
END
GO

IF @@Error = 0
   Print 'Success: The Drop procedure GetTransactionsForPeriod for US17365 has been deployed successfully'
ELSE
   Print 'Failure: The Drop procedure GetTransactionsForPeriod for US17365 has not been deployed successfully'
GO