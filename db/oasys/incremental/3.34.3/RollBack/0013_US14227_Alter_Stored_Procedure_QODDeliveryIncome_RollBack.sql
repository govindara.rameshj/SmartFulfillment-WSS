-- =============================================
-- Author:		Mike O'Cain
-- Create date: 07/10/2009
-- Description:	Delivery Income Detail
-- =============================================
ALTER PROCEDURE [dbo].[QODDeliveryIncome] 
	@DateStart datetime = NULL, 
	@DateEnd datetime = NULL
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT convert(varchar,ch.DELD,103) as 'Date'
		 , DATENAME(dw,ch.DELD)				as 'Day'	 
		 , ch.Numb							as 'OrderNumber'
	     , ch.REVI							as 'RevisionNumber'
	     , ch.NAME							as 'CustomerName'
	     , ch.POST							as 'PostCode'
		 , ch.DCST							as 'DeliveryCharge'
		 , ch.WGHT							as 'Weight'
		 , ch.VOLU							as 'Volume'
		 , su.Name							as 'OrderTaker'
	     From CORHDR as ch, CORHDR4 as c4, SystemUsers as su
	     Where (ch.NUMB = c4.NUMB) and (c4.OEID = su.EmployeeCode) And ch.DELI = 1 and (ch.DELD >= @DateStart and ch.DELD <= @DateEnd)
		 Order by ch.DELD
END

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure QODDeliveryIncome for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure QODDeliveryIncome for US14227 has not been deployed successfully'
GO