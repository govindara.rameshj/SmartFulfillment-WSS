-- =============================================
-- Author:		Mike O'Cain
-- Create date: 07/10/2009
-- Description:	Quote Conversions
-- =============================================
ALTER PROCEDURE [dbo].[QODQuoteConversions] 
	@DateStart datetime = Null, 
	@DateEnd datetime = Null
AS
BEGIN
	Drop Table TMP_QOD_TABLE
	
	SELECT
		 convert(varchar,oh.DELD,103)				as 'DelDate'
		 , DATENAME(dw,oh.DELD)						as 'DelDay'	 
		 , count(qh.NUMB)							as 'QuotesRaised'
		 , Count(oh.Numb)							as 'OrdersRaised'
		 ,(Count(oh.Numb) / Count(qh.Numb) * 100)	as 'PercSold'
		 , SUM(convert(int,oh.DELI))				as 'QuotesDelivered'
	INTO TMP_QOD_TABLE
	FROM CORHDR as oh, QUOHDR as qh
	Where oh.DELD >= @DateStart AND oh.DELD <= @DateEnd 
	GROUP BY oh.DELD

   	SELECT  DelDate									as 'Date'
		  ,	DelDay									as 'Day'
		  , QuotesRaised							as 'QuotesRaised'
 		  , OrdersRaised							as 'OrdersRaised'
		  , PercSold								as 'PercSold'
		  , QuotesDelivered							as 'QuotesDelivered'
		  , (QuotesDelivered / QuotesRaised) * 100	as 'PercDel'
	From TMP_QOD_TABLE
END

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure QODQuoteConversions for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure QODQuoteConversions for US14227 has not been deployed successfully'
GO