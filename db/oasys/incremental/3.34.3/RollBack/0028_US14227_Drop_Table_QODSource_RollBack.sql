IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODSource]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	DROP TABLE [dbo].[QODSource]
END
GO

If @@Error = 0
   Print 'Success: Table QODSource for US14227 has been successfully dropped'
Else
   Print 'Failure: Table QODSource for US14227 has not been successfully dropped'
GO
