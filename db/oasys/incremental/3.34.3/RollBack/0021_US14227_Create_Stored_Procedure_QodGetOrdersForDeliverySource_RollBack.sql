IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QodGetOrdersForDeliverySource]') AND type in (N'P', N'PC'))
drop procedure QodGetOrdersForDeliverySource;
go

if @@error = 0
   print 'Success: Stored procedure QodGetOrdersForDeliverySource for US14227 has been successfully dropped'
else
   print 'Failure: Stored procedure QodGetOrdersForDeliverySource for US14227 has NOT been successfully dropped'
go