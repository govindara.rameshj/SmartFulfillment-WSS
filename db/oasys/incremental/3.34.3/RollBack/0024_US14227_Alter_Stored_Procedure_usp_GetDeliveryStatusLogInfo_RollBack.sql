ALTER PROCEDURE [dbo].[usp_GetDeliveryStatusLogInfo]
@DateInterestedIn DateTime
AS
BEGIN
	SELECT NewStatus, COUNT(*) as Total from CORHDR4LOG where DateChanged = @DateInterestedIn Group by NewStatus 
END
GO

If @@Error = 0
   Print 'Success: Alter Stored Procedure usp_GetDeliveryStatusLogInfo for US14227 has been deployed successfully'
Else
   Print 'Failure: Alter Stored Procedure usp_GetDeliveryStatusLogInfo for US14227 has not been deployed successfully'
GO