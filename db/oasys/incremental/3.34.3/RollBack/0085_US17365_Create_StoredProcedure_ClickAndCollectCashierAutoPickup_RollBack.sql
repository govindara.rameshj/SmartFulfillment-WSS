IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ClickAndCollectCashierAutoPickup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [dbo].[ClickAndCollectCashierAutoPickup]
END
GO

If @@Error = 0
   Print 'Success: The Stored Procedure ClickAndCollectCashierAutoPickup for US17365 has been successfully dropped'
Else
   Print 'Failure: The Stored Procedure ClickAndCollectCashierAutoPickup for US17365 has not been successfully dropped'
GO
