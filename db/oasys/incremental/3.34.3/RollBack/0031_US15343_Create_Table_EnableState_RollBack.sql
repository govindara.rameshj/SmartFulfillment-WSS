IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnableState]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	DROP TABLE [dbo].[EnableState]
END
GO

If @@Error = 0
   Print 'Success: The Table EnableState for US15343 has been successfully dropped'
Else
   Print 'Failure: The Table EnableState for US15343 has not been successfully dropped'
GO
