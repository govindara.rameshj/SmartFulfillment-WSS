IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'svf_ReportClickAndCollectRefundsValueWTD') AND type in (N'FN'))
DROP FUNCTION svf_ReportClickAndCollectRefundsValueWTD
GO

IF @@Error = 0
   Print 'Success: Scalar-values Function svf_ReportClickAndCollectRefundsValueWTD for US15772 has been successfully dropped'
ELSE
   Print 'Failure: Scalar-values Function svf_ReportClickAndCollectRefundsValueWTD for US15772 has NOT been successfully dropped'
GO