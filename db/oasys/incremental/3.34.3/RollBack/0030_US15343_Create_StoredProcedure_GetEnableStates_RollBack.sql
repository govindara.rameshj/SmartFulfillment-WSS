IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEnableStates]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [dbo].[GetEnableStates]
END
GO

If @@Error = 0
   Print 'Success: The Stored Procedure GetEnableStates for US15343 has been successfully dropped'
Else
   Print 'Failure: The Stored Procedure GetEnableStates for US15343 has not been successfully dropped'
GO
