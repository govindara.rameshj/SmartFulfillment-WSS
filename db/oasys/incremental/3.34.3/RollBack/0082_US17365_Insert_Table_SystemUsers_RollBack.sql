SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM [dbo].[SystemUsers] where [ID] = 499)
BEGIN

delete [SystemUsers]
where ID=499

Print 'Success: C&C user has been deleted successfully from SystemUsers'
END

ELSE

Print 'Failure: C&C user has not been deleted from SystemUsers'

GO