SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[svf_ReportCoreRefundsQtyWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@CoreRefundQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Core REFUND Qty - WTD
	----------------------------------------------------------------------------------
	Select			@CoreRefundQty	=		count(TOTL)
	From			DLTOTS 
	Where			TCOD			=		'RF'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		>=		@StartDate
					and Date1		<=		@DateEnd;

	Set				@CoreRefundQty	=		isnull(@CoreRefundQty, 0);
		
	RETURN			@CoreRefundQty

END


GO


