IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCORHDRFull]'))
DROP VIEW [dbo].[vwCORHDRFull]
GO

IF @@Error = 0
   PRINT 'Success: View vwCORHDRFull for US14227 has been deployed successfully dropped'
ELSE
   PRINT 'Failure: View vwCORHDRFull for US14227 has not been deployed successfully dropped'
Go
