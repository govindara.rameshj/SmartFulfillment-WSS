SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[svf_ReportCoreRefundsQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CoreRefundQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Core REFUND Qty - DAY
	----------------------------------------------------------------------------------
	Select			@CoreRefundQty	=		count(TOTL)
	From			DLTOTS 
	Where			TCOD			=		'RF'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		=		@StartDate;

	Set				@CoreRefundQty	=		isnull(@CoreRefundQty, 0);
		
	RETURN			@CoreRefundQty

END


GO


