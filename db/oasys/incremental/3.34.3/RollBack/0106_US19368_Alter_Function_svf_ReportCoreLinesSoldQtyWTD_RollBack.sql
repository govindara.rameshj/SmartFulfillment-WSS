SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Core LinesSold Qty for WTD - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 7th July 2011
-- Description	: Removed DL.IBAR condition from where clause so that all included
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportCoreLinesSoldQtyWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate				date,
					@DateEnd				date,
					@CoreLinesQty			numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Core Lines Sold Qty - WTD
	----------------------------------------------------------------------------------
	Select			@CoreLinesQty		=		count(DL.SKUN)
	From			DLTOTS as DT
	Inner Join		DLLINE as DL        on		DL.DATE1	= DT.DATE1
										and		DL.TILL		= DT.TILL
										and		DL.[TRAN]	= DT.[TRAN]
	Where			DL.LREV			=		0
					and DT.CASH			<>		'000'
					and DT.VOID			=		0
					and DT.PARK			=		0
					and DT.TMOD			=		0  
					and DT.DATE1		>=		@StartDate
					and DT.DATE1		<=		@DateEnd;

	Set				@CoreLinesQty		=		isnull(@CoreLinesQty, 0);
		
	RETURN			@CoreLinesQty

END


GO

If @@Error = 0
   Print 'Success: The Alter Function svf_ReportCoreLinesSoldQtyWTD for US19368 has been rolled back successfully'
Else
   Print 'Failure: The Alter Function svf_ReportCoreLinesSoldQtyWTD for US19368 has not been rolled back successfully'
GO

