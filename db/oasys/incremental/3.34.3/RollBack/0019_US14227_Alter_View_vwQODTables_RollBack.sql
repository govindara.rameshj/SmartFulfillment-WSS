ALTER VIEW [dbo].[vwQODTables]
AS
SELECT     dbo.CORHDR.NUMB, dbo.CORHDR.CUST, dbo.CORHDR.DATE1, dbo.CORHDR.DELD, dbo.CORHDR.CANC, dbo.CORHDR.DELI, dbo.CORHDR.DELC, 
                      dbo.CORHDR.AMDT, dbo.CORHDR.ADDR1, dbo.CORHDR.ADDR2, dbo.CORHDR.ADDR3, dbo.CORHDR.ADDR4, dbo.CORHDR.PHON, 
                      dbo.CORHDR.PRNT, dbo.CORHDR.RPRN, dbo.CORHDR.REVI, dbo.CORHDR.MVST, dbo.CORHDR.DCST, dbo.CORHDR.QTYO, dbo.CORHDR.QTYT, 
                      dbo.CORHDR.QTYR, dbo.CORHDR.WGHT, dbo.CORHDR.VOLU, dbo.CORHDR.SDAT, dbo.CORHDR.STIL, dbo.CORHDR.STRN, dbo.CORHDR.RDAT, 
                      dbo.CORHDR.RTIL, dbo.CORHDR.RTRN, dbo.CORHDR.MOBP, dbo.CORHDR.POST, dbo.CORHDR.NAME, dbo.CORHDR4.OEID, dbo.CORHDR4.FDID, 
                      dbo.SystemUsers.Name AS EmployeeName
FROM         dbo.CORHDR INNER JOIN
                      dbo.CORHDR4 ON dbo.CORHDR.NUMB = dbo.CORHDR4.NUMB INNER JOIN
                      dbo.SystemUsers ON dbo.CORHDR4.OEID = dbo.SystemUsers.EmployeeCode;
GO

If @@Error = 0
   Print 'Success: The Alter View vwQODTables for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter View vwQODTables for US14227 has not been deployed successfully'
GO