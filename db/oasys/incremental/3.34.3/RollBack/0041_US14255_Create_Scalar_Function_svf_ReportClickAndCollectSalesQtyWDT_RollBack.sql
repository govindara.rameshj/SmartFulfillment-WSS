IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'svf_ReportClickAndCollectSalesQtyWTD') AND type in (N'FN'))
DROP FUNCTION svf_ReportClickAndCollectSalesQtyWTD
GO

IF @@Error = 0
   Print 'Success: Scalar-values Function svf_ReportClickAndCollectSalesQtyWTD for US14255 has been successfully dropped'
ELSE
   Print 'Failure: Scalar-values Function svf_ReportClickAndCollectSalesQtyWTD for US14255 has NOT been successfully dropped'
GO