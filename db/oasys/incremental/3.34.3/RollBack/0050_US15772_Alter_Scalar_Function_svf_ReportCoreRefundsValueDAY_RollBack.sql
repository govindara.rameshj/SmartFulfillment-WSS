SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[svf_ReportCoreRefundsValueDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CoreRefundValue	numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Core REFUND Value - DAY
	----------------------------------------------------------------------------------
	Select			@CoreRefundValue	=		sum(TOTL)
	From			DLTOTS 
	Where			TCOD				=		'RF'
					and CASH			<>		'000'
					and VOID			=		0
					and PARK			=		0
					and TMOD			=		0  
					and Date1			=		@StartDate;

	Set				@CoreRefundValue	=		isnull(@CoreRefundValue, 0);
		
	RETURN			@CoreRefundValue

END


GO


