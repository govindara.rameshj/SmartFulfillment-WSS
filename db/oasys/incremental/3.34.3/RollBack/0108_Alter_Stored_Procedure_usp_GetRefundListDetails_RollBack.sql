SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[usp_GetRefundListDetails] @ReportDate AS date
AS
BEGIN

    SELECT CM.EmployeeCode , 
           CM.Name , 
           DT.TCOD , 
           DT.TOTL , 
           DT.RMAN , 
           DT.TILL , 
           DT.[TIME] , 
           DT.[TRAN] , 
           DT.SUPV , 
           DT.RSUP , 
           DT.RCAS RefundCashier , 
           RC.Name RefundCashierName , 
           SU.Name SupervisorName , 
           RM.Name ManagerName
      FROM
           SystemUsers CM INNER JOIN DLTOTS DT
           ON DT.CASH
              = 
              CM.EmployeeCode
          AND DT.DATE1
              = 
              @ReportDate
          AND DT.VOID = 0
          AND DT.TMOD = 0
          AND (DT.TCOD = 'SA'
            OR DT.TCOD = 'RF')
                          LEFT OUTER JOIN SystemUsers RC
           ON RC.EmployeeCode
              = 
              DT.RCAS
                          LEFT OUTER JOIN SystemUsers SU
           ON SU.EmployeeCode
              = 
              DT.RSUP
                          LEFT OUTER JOIN SystemUsers RM
           ON RM.EmployeeCode
              = 
              DT.RMAN
      ORDER BY CM.EmployeeCode , DT.[TIME] , DT.TILL , DT.[TRAN];

END;

GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_GetRefundListDetails has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_GetRefundListDetails has not been deployed successfully'
GO