SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM [dbo].[SystemCurrencyDen] where [TenderID] = 12)
BEGIN

delete [SystemCurrencyDen]
where TenderID=12

Print 'Success: Web Tender has been deleted successfully from SystemCurrencyDen'
END

ELSE

Print 'Failure: Web Tender has not been deleted from SystemCurrencyDen'

GO