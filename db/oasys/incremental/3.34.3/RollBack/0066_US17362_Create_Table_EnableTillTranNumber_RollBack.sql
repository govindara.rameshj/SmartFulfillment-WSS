IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnableTillTranNumber]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EnableTillTranNumber]
GO

If @@Error = 0
   Print 'Success: The Table EnableTillTranNumber for US17362 has been successfully dropped'
Else
   Print 'Failure: The Table EnableTillTranNumber for US17362 has not been successfully dropped'
GO
