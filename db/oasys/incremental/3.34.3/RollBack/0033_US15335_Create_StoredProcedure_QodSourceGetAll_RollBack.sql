IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QodSourceGetAll]') AND type in (N'P', N'PC'))
drop procedure QodSourceGetAll;
go

if @@error = 0
   print 'Success: Stored procedure QodSourceGetAll for US15335 has been successfully dropped'
else
   print 'Failure: Stored procedure QodSourceGetAll for US15335 has NOT been successfully dropped'
go