if exists(select * from sys.columns c inner join sys.tables t on t.object_id = c.object_id 
					   inner join sys.schemas s on s.schema_id = t.schema_id
			  where c.[name] = 'NUMB' and t.[name] = 'CORHDR4Log' and t.[type] = 'U' and s.name = 'dbo')
begin
 alter table dbo.CORHDR4Log drop column [NUMB] 
end
GO

If @@Error = 0
   Print 'Success: Alter Table CORHDR4Log for US14227 has been deployed successfully'
Else
   Print 'Failure: Alter Table CORHDR4Log for US14227 has not been deployed successfully'
Go