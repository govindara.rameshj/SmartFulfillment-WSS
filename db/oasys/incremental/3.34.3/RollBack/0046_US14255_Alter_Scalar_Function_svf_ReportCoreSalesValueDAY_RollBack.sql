SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[svf_ReportCoreSalesValueDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CoreDayValue		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Core SALES Qty - DAY
	----------------------------------------------------------------------------------
	Select			@CoreDayValue	=		sum(TOTL)
	From			DLTOTS 
	Where			TCOD			=		'SA'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1	=		@StartDate

	Set				@CoreDayValue	=		isnull(@CoreDayValue, 0);
		
	RETURN			@CoreDayValue

END


GO


