IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CashBalTillTenVarUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [dbo].[usp_CashBalTillTenVarUpdate]
END
GO

If @@Error = 0
   Print 'Success: The Stored Procedure usp_CashBalTillTenVarUpdate for US17365 has been dropped successfully'
Else
   Print 'Failure: The Stored Procedure usp_CashBalTillTenVarUpdate for US17365 has not been dropped successfully'
GO
