-- =============================================
-- Author:		Mike O'Cain
-- Create date: 7/10/2009
-- Description:	Despatched Order Lististing
-- =============================================
ALTER PROCEDURE [dbo].[QODDespatchedOrders] 
	-- Add the parameters for the stored procedure here
	@DateStart datetime = NULL, 
	@DateEnd datetime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ch.Numb						as 'OrderNumber'
	     , ch.REVI						as 'RevisionNumber'
	     , ch.NAME						as 'CustomerName'
	     , ch.POST						as 'PostCode'
	     , convert(varchar,ch.DELD,103) as 'DateRequired'
	     , convert(varchar,c4.DDAT,103) as 'DateDelivered'
	     , ch.QTYO						as 'QtyOrdered'
	     , ch.QTYT						as 'QtyTaken'
	     , ch.QTYR						as 'QtyDel'
	     , case ch.DELI 
				when 1 then 'Delivery'
				else 'Collection'
		   end							as 'DelCol'
		 , ch.DCST						as 'DelCharge'
		 , ch.WGHT						as 'Weight'
		 , ch.VOLU						as 'Volume'
		 , su.Name						as 'OrderTaker'
	     From CORHDR as ch, CORHDR4 as c4, SystemUsers as su
	     Where (ch.NUMB = c4.NUMB) and (c4.OEID = su.EmployeeCode) And ch.DELC = 1 and (ch.DELD >= @DateStart and ch.DELD <= @DateEnd)
		 Order by ch.DELD
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure QODDespatchedOrders for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure QODDespatchedOrders for US14227 has not been deployed successfully'
GO
