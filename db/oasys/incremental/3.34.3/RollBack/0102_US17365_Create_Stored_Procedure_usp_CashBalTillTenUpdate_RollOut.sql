IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CashBalTillTenUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [dbo].[usp_CashBalTillTenUpdate]
END
GO

If @@Error = 0
   Print 'Success: The Stored Procedure usp_CashBalTillTenUpdate for US17365 has been dropped successfully'
Else
   Print 'Failure: The Stored Procedure usp_CashBalTillTenUpdate for US17365 has not been dropped successfully'
GO
