-- =============================================
-- Author:		Mike O'Cain
-- Create date: 01-10-2009
-- Description:	List Deliveries for the period passed in
-- =============================================
ALTER PROCEDURE [dbo].[QODOrderDeliveryListing] 
	@DateStart datetime = Null, 
	@DateEnd datetime = Null
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT convert(varchar,DELD,103)	as 'Date'
		, DATENAME(dw,DELD)					as 'Day'
		,NUMB								as 'OrderNumber'
		,NAME								as 'CustomerName'
		,POST								as 'PostCode'
		,PHON								as 'PhoneNumber'
		,QTYO								as 'QtyDeliveryItems'
		,WGHT								as 'Weight'
		,VOLU								as 'Volume'
	From CORHDR	
	WHERE DELD >= @DateStart AND DELD <= @DateEnd and DELI = 1 
	Order by DELD
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure QODOrderDeliveryListing for US14227 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure QODOrderDeliveryListing for US14227 has not been deployed successfully'
GO