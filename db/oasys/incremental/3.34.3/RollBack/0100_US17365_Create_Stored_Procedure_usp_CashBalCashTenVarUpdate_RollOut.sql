IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CashBalCashTenVarUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [dbo].[usp_CashBalCashTenVarUpdate]
END
GO

If @@Error = 0
   Print 'Success: The Stored Procedure usp_CashBalCashTenVarUpdate for US17365 has been dropped successfully'
Else
   Print 'Failure: The Stored Procedure usp_CashBalCashTenVarUpdate for US17365 has not been dropped successfully'
GO
