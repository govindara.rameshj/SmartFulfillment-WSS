DELETE
FROM [dbo].[StockLogType]
WHERE [Id] in (6, 7)

If @@Error = 0
   Print 'Success: The Insert Table StockLogType for US14227 has been rolled back successfully'
Else
   Print 'Failure: The Insert Table StockLogType for US14227 has not been rolled back successfully'
GO