﻿IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'[SalesGetHeaderLines]') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure [SalesGetHeaderLines]'
    EXEC ('CREATE PROCEDURE dbo.[SalesGetHeaderLines] AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [SalesGetHeaderLines]')
GO
ALTER PROCEDURE [dbo].[SalesGetHeaderLines] 
    @Date           date,
    @TillNumber     char(2),
    @TranNumber     char(4)
AS
BEGIN
    SET NOCOUNT ON;

    select
        dt.DATE1         as 'Date',
        dt.TILL          as 'TillId',
        dt.[TRAN]        as 'TranNumber',
        dt.OVCTranNumber as 'OVCTranNumber',    
        dt.[TIME]        as 'Time',     
        dt.CASH          as 'UserId',
        coalesce(su.Name, 'Unknown') as 'UserName',
        dt.TCOD          as 'SaleType',
        dt.VOID          as 'IsVoid',
        dt.TMOD          as 'IsTraining',
        dt.DISC          as 'Discount',     
        dt.TOTL          as 'TotalAmount'   
    from
        vwDLTOTS dt
    left join
        SystemUsers su on su.ID = dt.CASH
    where
            dt.DATE1    = convert(date, @Date)
        and dt.TILL     = @TillNumber
        and dt.[TRAN]   = @TranNumber
    
    
    select
        dl.DATE1    as 'Date',
        dl.TILL     as 'TillId',
        dl.[TRAN]   as 'TranNumber',
        dl.NUMB     as 'Number',
        dl.SKUN     as 'SkuNumber',
        sk.DESCR    as 'Description',
        dl.QUAN     as 'Quantity',
        dl.SPRI     as 'SalePrice',
        dl.PRIC     as 'Price',
        dl.EXTP     as 'ExtendedPrice'
    from
        DLLINE dl
    inner join
        STKMAS sk on sk.SKUN = dl.SKUN
    where
            dl.DATE1    = convert(date, @Date)
        and dl.TILL     = @TillNumber
        and dl.[TRAN]   = @TranNumber       
    
END
go
If @@Error = 0
   Print 'Success: The Alter Stored Procedure SalesGetHeaderLines for US10307 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SalesGetHeaderLines for US10307 has not been deployed'
GO