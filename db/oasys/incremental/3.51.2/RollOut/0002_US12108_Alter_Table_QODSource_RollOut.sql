SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODSource]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
    AND NOT EXISTS (SELECT 1 FROM sys.key_constraints WHERE parent_object_id = object_id('QODSource') AND [type] = 'PK')
BEGIN
    PRINT 'Adding primary key to table QODSource'
    ALTER TABLE [dbo].[QODSource]
    ADD [ID] int IDENTITY(1,1) CONSTRAINT [PK_QODSource] PRIMARY KEY ([ID])
        
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QODSource', @level2type=N'COLUMN',@level2name=N'ID'
END
GO

If @@Error = 0
   Print 'Success: The Alter Table QODSource for US12108 has been deployed successfully'
Else
   Print 'Failure: The Alter Table QODSource for US12108 has not been deployed'
GO
