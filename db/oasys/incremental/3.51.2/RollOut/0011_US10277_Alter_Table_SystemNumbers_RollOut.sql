﻿IF EXISTS (select * from sys.columns where object_id = OBJECT_ID('SystemNumbers') and name = 'Prefix')
    BEGIN
        PRINT 'Drop Prefix column to SystemNumbers'
        ALTER TABLE dbo.SystemNumbers
        DROP COLUMN Prefix
    END
GO

IF EXISTS (select * from sys.columns where object_id = OBJECT_ID('SystemNumbers') and name = 'Suffix')
    BEGIN
        PRINT 'Drop Suffix column to SystemNumbers'
        ALTER TABLE dbo.SystemNumbers
        DROP COLUMN Suffix
    END
GO
  
IF NOT EXISTS (select * from sys.columns where object_id = OBJECT_ID('SystemNumbers') and name = 'Min')
    BEGIN
        PRINT 'Add Min column to SystemNumbers'
        ALTER TABLE dbo.SystemNumbers
        ADD [Min] int NULL
    END
GO
  
IF NOT EXISTS (select * from sys.columns where object_id = OBJECT_ID('SystemNumbers') and name = 'Max')
    BEGIN
        PRINT 'Add Max column to SystemNumbers'
        ALTER TABLE dbo.SystemNumbers
        ADD [Max] int NULL
    END
GO

If @@Error = 0
   Print 'Success: The Alter SystemNumbers table for US10277 has been deployed successfully'
Else
   Print 'Failure: The Alter SystemNumbers table for US10277 has not been deployed'
GO