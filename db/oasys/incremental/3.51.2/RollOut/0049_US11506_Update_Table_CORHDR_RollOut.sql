--    1 - Sunday
--    2 - Monday
--    3 - Tuesday
--    4 - Wednesday
--    5 - Thursday
--    6 - Friday
--    7 - Saturday
DECLARE
    @WeekdaySlotId int,
    @WeekdaySlotDescription varchar(50),    
    @WeekdaySlotStartTime time,
    @WeekdaySlotEndTime time,
    @SaturdaySlotId int,
    @SaturdaySlotDescription varchar(50),
    @SaturdaySlotStartTime time,
    @SaturdaySlotEndTime time
    
SELECT @WeekdaySlotId = [LongValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 53
SELECT @WeekdaySlotDescription = [StringValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 54 
SELECT @WeekdaySlotStartTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 55
SELECT @WeekdaySlotEndTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 56  
SELECT @SaturdaySlotId = [LongValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 57
SELECT @SaturdaySlotDescription = [StringValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 58    
SELECT @SaturdaySlotStartTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 59
SELECT @SaturdaySlotEndTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 60 

UPDATE dbo.CORHDR
SET [RequiredDeliverySlotID] = CASE WHEN (DATEPART(weekday, ch.DELD) + @@datefirst) % 7 = 0 THEN @SaturdaySlotId ELSE @WeekdaySlotId END,
    [RequiredDeliverySlotDescription] = CASE WHEN (DATEPART(weekday, ch.DELD) + @@datefirst) % 7 = 0 THEN @SaturdaySlotDescription ELSE @WeekdaySlotDescription END,
    [RequiredDeliverySlotStartTime] = CASE WHEN (DATEPART(weekday, ch.DELD) + @@datefirst) % 7 = 0 THEN @SaturdaySlotStartTime ELSE @WeekdaySlotStartTime END,
    [RequiredDeliverySlotEndTime] = CASE WHEN (DATEPART(weekday, ch.DELD) + @@datefirst) % 7 = 0 THEN @SaturdaySlotEndTime ELSE @WeekdaySlotEndTime END
FROM dbo.CORHDR ch
LEFT JOIN dbo.CORHDR5 ch5 ON ch5.NUMB = ch.NUMB
WHERE LEN(ISNULL(ch5.[Source], '')) = 0 AND ch.DELI = 1 AND (LEN(ISNULL(ch.RequiredDeliverySlotID, '')) = 0 OR [RequiredDeliverySlotStartTime] IS NULL)
GO

If @@Error = 0
   Print 'Success: Update into table CORHDR for US11506 has been deployed successfully'
Else
   Print 'Failure: Update into table CORHDR for US11506 has not been deployed successfully'
GO