DECLARE @sql NVARCHAR(MAX), 
        @view_name NVARCHAR(100)

SET @view_name='vwDLTOTS'

SELECT @sql='VIEW dbo.['+@view_name+'] AS
SELECT
       [DATE1]
      ,[TILL]
      ,[TRAN]
      ,[CASH]
      ,[TIME]
      ,[SUPV]
      ,[TCOD]
      ,[OPEN]
      ,[MISC]
      ,[DESCR]
      ,[ORDN]
      ,[ACCT]
      ,[VOID]
      ,[VSUP]
      ,[TMOD]
      ,[PROC]
      ,[DOCN]
      ,[SUSE]
      ,[STOR]
      ,[MERC]
      ,[NMER]
      ,[TAXA]
      ,[DISC]
      ,[DSUP]
      ,[TOTL]
      ,[ACCN]
      ,[CARD]
      ,[AUPD]
      ,[BACK]
      ,[ICOM]
      ,[IEMP]
      ,[RCAS]
      ,[RSUP]
      ,[VATR1]
      ,[VATR2]
      ,[VATR3]
      ,[VATR4]
      ,[VATR5]
      ,[VATR6]
      ,[VATR7]
      ,[VATR8]
      ,[VATR9]
      ,[VSYM1]
      ,[VSYM2]
      ,[VSYM3]
      ,[VSYM4]
      ,[VSYM5]
      ,[VSYM6]
      ,[VSYM7]
      ,[VSYM8]
      ,[VSYM9]
      ,[XVAT1]
      ,[XVAT2]
      ,[XVAT3]
      ,[XVAT4]
      ,[XVAT5]
      ,[XVAT6]
      ,[XVAT7]
      ,[XVAT8]
      ,[XVAT9]
      ,[VATV1]
      ,[VATV2]
      ,[VATV3]
      ,[VATV4]
      ,[VATV5]
      ,[VATV6]
      ,[VATV7]
      ,[VATV8]
      ,[VATV9]
      ,[PARK]
      ,[RMAN]
      ,[TOCD]
      ,[PKRC]
      ,[REMO]
      ,[GTPN]
      ,[CCRD]
      ,[SSTA]
      ,[SSEQ]
      ,[CBBU]
      ,[CARD_NO]
      ,[RTI]
      ,[ReceivedDate]
      ,case when [Source]=''OVC'' then [SourceTranNumber] end As ''OVCTranNumber''
FROM dbo.DLTOTS'

IF OBJECT_ID (@view_name,'view') IS NOT NULL 
    SET @sql='ALTER '+@sql
ELSE 
    SET @sql='CREATE '+@sql;

EXEC (@sql);

IF OBJECT_ID(@view_name,'view') IS NOT NULL 
    BEGIN
        SELECT @sql = 'sp_refreshview ''dbo.'+@view_name+'''; '
        EXEC(@sql)
    END
GO

IF @@Error = 0
   PRINT 'Success: The Create View vwDLTOTS for US10307 has been deployed successfully'
ELSE
   PRINT 'Failure: The Create View vwDLTOTS for US10307 has not been deployed successfully'
Go

--Apply Permissions

Grant select On dbo.vwDLTOTS To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on view dbo.vwDLTOTS has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on view dbo.vwDLTOTS might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant select on dbo.vwDLTOTS to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on view dbo.vwDLTOTS has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on view dbo.vwDLTOTS has NOT been successfully deployed'
go
