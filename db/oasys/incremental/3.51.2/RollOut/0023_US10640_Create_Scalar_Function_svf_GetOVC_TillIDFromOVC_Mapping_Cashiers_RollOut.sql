IF NOT EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'svf_GetOVC_TillIDFromOVC_Mapping_Cashiers') AND type in (N'FN'))
BEGIN
    PRINT 'Creating function svf_GetOVC_TillIDFromOVC_Mapping_Cashiers'
    EXEC ('CREATE FUNCTION svf_GetOVC_TillIDFromOVC_Mapping_Cashiers() RETURNS int AS BEGIN RETURN 0 END')
END
GO

PRINT ('Altering function svf_GetOVC_TillIDFromOVC_Mapping_Cashiers')
GO

ALTER FUNCTION [dbo].[svf_GetOVC_TillIDFromOVC_Mapping_Cashiers]
(
@InputCashierID int
)
RETURNS int
AS
BEGIN
    DECLARE @TillID int
    
    SELECT @TillID = [OVC_TillID]
    FROM [dbo].[OVC_Mapping_Cashiers]  
    WHERE   [Mapping_CashierID] =  @InputCashierID
        
    RETURN @TillID
END
GO

IF @@Error = 0
   Print 'Success: The Create Scalar-values Function svf_GetOVC_TillIDFromOVC_Mapping_Cashiers for US10640 has been deployed successfully'
ELSE
   Print 'Failure: The Create Scalar-values Function svf_GetOVC_TillIDFromOVC_Mapping_Cashiers for US10640 has not been deployed successfully'
GO

--Apply Permissions

Grant Execute On dbo.svf_GetOVC_TillIDFromOVC_Mapping_Cashiers To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure dbo.svf_GetOVC_TillIDFromOVC_Mapping_Cashiers has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure dbo.svf_GetOVC_TillIDFromOVC_Mapping_Cashiers might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on dbo.svf_GetOVC_TillIDFromOVC_Mapping_Cashiers to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure dbo.svf_GetOVC_TillIDFromOVC_Mapping_Cashiers has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure dbo.svf_GetOVC_TillIDFromOVC_Mapping_Cashiers has NOT been successfully deployed'
go
