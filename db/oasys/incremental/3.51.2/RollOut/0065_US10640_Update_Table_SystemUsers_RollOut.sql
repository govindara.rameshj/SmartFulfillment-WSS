﻿-- KM - v2.0 - 16/11/2015 - OVC Activate Till Accounts (Wickes Estate Data Script)
-- Extract Run : Nov 16 2015  1:17PM

IF EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'tgSystemUsersUpdate') AND type in (N'TR'))
BEGIN
    PRINT 'Disable trigger tgSystemUsersUpdate';
    DISABLE TRIGGER [dbo].[tgSystemUsersUpdate] ON [dbo].[SystemUsers];
END
GO

Declare @Store Char(4) = (Select '8' + LTRIM(RTRIM(STOR)) From RETOPT Where FKEY = '01')

------- Store : 8052 Details ------
If (@Store = '8052')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End
  
------- Store : 8053 Details ------
If (@Store = '8053')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8055 Details ------
If (@Store = '8055')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8059 Details ------
If (@Store = '8059')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8061 Details ------
If (@Store = '8061')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8062 Details ------
If (@Store = '8062')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8064 Details ------
If (@Store = '8064')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8065 Details ------
If (@Store = '8065')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8066 Details ------
If (@Store = '8066')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8067 Details ------
If (@Store = '8067')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8071 Details ------
If (@Store = '8071')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
  End

------- Store : 8072 Details ------
If (@Store = '8072')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8073 Details ------
If (@Store = '8073')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
  End

------- Store : 8074 Details ------
If (@Store = '8074')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8075 Details ------
If (@Store = '8075')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8076 Details ------
If (@Store = '8076')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8077 Details ------
If (@Store = '8077')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8078 Details ------
If (@Store = '8078')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8079 Details ------
If (@Store = '8079')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8080 Details ------
If (@Store = '8080')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8081 Details ------
If (@Store = '8081')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8082 Details ------
If (@Store = '8082')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8083 Details ------
If (@Store = '8083')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8088 Details ------
If (@Store = '8088')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8090 Details ------
If (@Store = '8090')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8091 Details ------
If (@Store = '8091')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8093 Details ------
If (@Store = '8093')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8094 Details ------
If (@Store = '8094')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8096 Details ------
If (@Store = '8096')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8097 Details ------
If (@Store = '8097')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8098 Details ------
If (@Store = '8098')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8120 Details ------
If (@Store = '8120')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8208 Details ------
If (@Store = '8208')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8209 Details ------
If (@Store = '8209')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8214 Details ------
If (@Store = '8214')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8215 Details ------
If (@Store = '8215')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8216 Details ------
If (@Store = '8216')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8218 Details ------
If (@Store = '8218')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8220 Details ------
If (@Store = '8220')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
  End

------- Store : 8221 Details ------
If (@Store = '8221')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8222 Details ------
If (@Store = '8222')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8225 Details ------
If (@Store = '8225')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
  End

------- Store : 8226 Details ------
If (@Store = '8226')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8227 Details ------
If (@Store = '8227')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8228 Details ------
If (@Store = '8228')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8229 Details ------
If (@Store = '8229')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8230 Details ------
If (@Store = '8230')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8231 Details ------
If (@Store = '8231')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8232 Details ------
If (@Store = '8232')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8233 Details ------
If (@Store = '8233')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 465) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 465
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 466) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 466
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 467) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 467
        End 
  End

------- Store : 8234 Details ------
If (@Store = '8234')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8235 Details ------
If (@Store = '8235')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8236 Details ------
If (@Store = '8236')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8237 Details ------
If (@Store = '8237')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8238 Details ------
If (@Store = '8238')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 465) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 465
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 466) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 466
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 467) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 467
        End 
  End

------- Store : 8239 Details ------
If (@Store = '8239')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 465) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 465
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 466) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 466
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 467) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 467
        End 
  End

------- Store : 8240 Details ------
If (@Store = '8240')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8243 Details ------
If (@Store = '8243')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8245 Details ------
If (@Store = '8245')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8246 Details ------
If (@Store = '8246')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8247 Details ------
If (@Store = '8247')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8248 Details ------
If (@Store = '8248')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
  End

------- Store : 8250 Details ------
If (@Store = '8250')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8251 Details ------
If (@Store = '8251')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8252 Details ------
If (@Store = '8252')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8253 Details ------
If (@Store = '8253')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8254 Details ------
If (@Store = '8254')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8255 Details ------
If (@Store = '8255')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8256 Details ------
If (@Store = '8256')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8257 Details ------
If (@Store = '8257')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8258 Details ------
If (@Store = '8258')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
  End

------- Store : 8259 Details ------
If (@Store = '8259')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8260 Details ------
If (@Store = '8260')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8261 Details ------
If (@Store = '8261')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8262 Details ------
If (@Store = '8262')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8263 Details ------
If (@Store = '8263')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8264 Details ------
If (@Store = '8264')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8265 Details ------
If (@Store = '8265')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8266 Details ------
If (@Store = '8266')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8267 Details ------
If (@Store = '8267')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8268 Details ------
If (@Store = '8268')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8270 Details ------
If (@Store = '8270')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 465) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 465
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 466) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 466
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 467) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 467
        End 
  End

------- Store : 8271 Details ------
If (@Store = '8271')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8273 Details ------
If (@Store = '8273')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8274 Details ------
If (@Store = '8274')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8275 Details ------
If (@Store = '8275')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8276 Details ------
If (@Store = '8276')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8277 Details ------
If (@Store = '8277')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8278 Details ------
If (@Store = '8278')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8279 Details ------
If (@Store = '8279')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8280 Details ------
If (@Store = '8280')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8281 Details ------
If (@Store = '8281')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End


------- Store : 8282 Details ------
If (@Store = '8282')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8283 Details ------
If (@Store = '8283')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8284 Details ------
If (@Store = '8284')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8287 Details ------
If (@Store = '8287')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8289 Details ------
If (@Store = '8289')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8290 Details ------
If (@Store = '8290')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8291 Details ------
If (@Store = '8291')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
  End

------- Store : 8292 Details ------
If (@Store = '8292')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8293 Details ------
If (@Store = '8293')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8295 Details ------
If (@Store = '8295')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8296 Details ------
If (@Store = '8296')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8297 Details ------
If (@Store = '8297')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8298 Details ------
If (@Store = '8298')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8299 Details ------
If (@Store = '8299')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8301 Details ------
If (@Store = '8301')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
  End

------- Store : 8303 Details ------
If (@Store = '8303')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8308 Details ------
If (@Store = '8308')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8309 Details ------
If (@Store = '8309')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8310 Details ------
If (@Store = '8310')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End     
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 412) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 412
        End
  End

------- Store : 8311 Details ------
If (@Store = '8311')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 407) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 407
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 412) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 412
        End 
  End

------- Store : 8312 Details ------
If (@Store = '8312')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8313 Details ------
If (@Store = '8313')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8314 Details ------
If (@Store = '8314')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 407) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 407
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 408) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 408
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 409) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 409
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8316 Details ------
If (@Store = '8316')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 407) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 407
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8317 Details ------
If (@Store = '8317')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8318 Details ------
If (@Store = '8318')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8319 Details ------
If (@Store = '8319')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8320 Details ------
If (@Store = '8320')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8321 Details ------
If (@Store = '8321')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8322 Details ------
If (@Store = '8322')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 408) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 408
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 409) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 409
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8323 Details ------
If (@Store = '8323')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 407) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 407
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8326 Details ------
If (@Store = '8326')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8327 Details ------
If (@Store = '8327')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8328 Details ------
If (@Store = '8328')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8329 Details ------
If (@Store = '8329')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8330 Details ------
If (@Store = '8330')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8331 Details ------
If (@Store = '8331')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8332 Details ------
If (@Store = '8332')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8334 Details ------
If (@Store = '8334')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 407) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 407
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8336 Details ------
If (@Store = '8336')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8337 Details ------
If (@Store = '8337')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8338 Details ------
If (@Store = '8338')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 412) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 412
        End 
  End

------- Store : 8340 Details ------
If (@Store = '8340')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8342 Details ------
If (@Store = '8342')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 407) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 407
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 412) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 412
        End 
  End

------- Store : 8343 Details ------
If (@Store = '8343')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8344 Details ------
If (@Store = '8344')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8347 Details ------
If (@Store = '8347')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8348 Details ------
If (@Store = '8348')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8349 Details ------
If (@Store = '8349')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8350 Details ------
If (@Store = '8350')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8351 Details ------
If (@Store = '8351')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8352 Details ------
If (@Store = '8352')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8353 Details ------
If (@Store = '8353')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8354 Details ------
If (@Store = '8354')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8355 Details ------
If (@Store = '8355')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8357 Details ------
If (@Store = '8357')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8359 Details ------
If (@Store = '8359')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8360 Details ------
If (@Store = '8360')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8364 Details ------
If (@Store = '8364')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8365 Details ------
If (@Store = '8365')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8366 Details ------
If (@Store = '8366')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8367 Details ------
If (@Store = '8367')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8368 Details ------
If (@Store = '8368')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8370 Details ------
If (@Store = '8370')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8371 Details ------
If (@Store = '8371')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 406) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 406
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 407) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 407
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 412) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 412
        End 
  End

------- Store : 8372 Details ------
If (@Store = '8372')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8374 Details ------
If (@Store = '8374')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8377 Details ------
If (@Store = '8377')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8381 Details ------
If (@Store = '8381')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8382 Details ------
If (@Store = '8382')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8385 Details ------
If (@Store = '8385')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8404 Details ------
If (@Store = '8404')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8405 Details ------
If (@Store = '8405')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8406 Details ------
If (@Store = '8406')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8407 Details ------
If (@Store = '8407')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8409 Details ------
If (@Store = '8409')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8410 Details ------
If (@Store = '8410')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8411 Details ------
If (@Store = '8411')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8412 Details ------
If (@Store = '8412')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8414 Details ------
If (@Store = '8414')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8415 Details ------
If (@Store = '8415')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
  End

------- Store : 8416 Details ------
If (@Store = '8416')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8417 Details ------
If (@Store = '8417')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8418 Details ------
If (@Store = '8418')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8422 Details ------
If (@Store = '8422')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8423 Details ------
If (@Store = '8423')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8428 Details ------
If (@Store = '8428')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8429 Details ------
If (@Store = '8429')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8430 Details ------
If (@Store = '8430')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8431 Details ------
If (@Store = '8431')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8434 Details ------
If (@Store = '8434')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8436 Details ------
If (@Store = '8436')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8437 Details ------
If (@Store = '8437')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8438 Details ------
If (@Store = '8438')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8441 Details ------
If (@Store = '8441')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8443 Details ------
If (@Store = '8443')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8444 Details ------
If (@Store = '8444')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8448 Details ------
If (@Store = '8448')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8449 Details ------
If (@Store = '8449')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 412) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 412
        End 
  End

------- Store : 8451 Details ------
If (@Store = '8451')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8452 Details ------
If (@Store = '8452')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8453 Details ------
If (@Store = '8453')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8454 Details ------
If (@Store = '8454')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8455 Details ------
If (@Store = '8455')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8456 Details ------
If (@Store = '8456')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8457 Details ------
If (@Store = '8457')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8458 Details ------
If (@Store = '8458')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8459 Details ------
If (@Store = '8459')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8460 Details ------
If (@Store = '8460')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8461 Details ------
If (@Store = '8461')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8464 Details ------
If (@Store = '8464')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8465 Details ------
If (@Store = '8465')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8466 Details ------
If (@Store = '8466')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8467 Details ------
If (@Store = '8467')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8468 Details ------
If (@Store = '8468')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 412) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 412
        End 
  End

------- Store : 8469 Details ------
If (@Store = '8469')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 465) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 465
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 466) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 466
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 467) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 467
        End 
  End

------- Store : 8471 Details ------
If (@Store = '8471')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8472 Details ------
If (@Store = '8472')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8473 Details ------
If (@Store = '8473')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8474 Details ------
If (@Store = '8474')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8475 Details ------
If (@Store = '8475')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8479 Details ------
If (@Store = '8479')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8480 Details ------
If (@Store = '8480')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8481 Details ------
If (@Store = '8481')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8482 Details ------
If (@Store = '8482')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8483 Details ------
If (@Store = '8483')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8485 Details ------
If (@Store = '8485')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8486 Details ------
If (@Store = '8486')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 465) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 465
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 466) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 466
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 467) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 467
        End 
  End

------- Store : 8488 Details ------
If (@Store = '8488')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 465) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 465
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 466) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 466
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 467) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 467
        End 
  End

------- Store : 8494 Details ------
If (@Store = '8494')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8495 Details ------
If (@Store = '8495')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 403) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 403
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 404) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 404
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 405) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 405
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8497 Details ------
If (@Store = '8497')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 465) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 465
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 466) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 466
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 467) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 467
        End 
  End

------- Store : 8716 Details ------
If (@Store = '8716')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8717 Details ------
If (@Store = '8717')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8718 Details ------
If (@Store = '8718')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8719 Details ------
If (@Store = '8719')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8720 Details ------
If (@Store = '8720')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8721 Details ------
If (@Store = '8721')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8722 Details ------
If (@Store = '8722')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8723 Details ------
If (@Store = '8723')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
  End

------- Store : 8726 Details ------
If (@Store = '8726')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8727 Details ------
If (@Store = '8727')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8728 Details ------
If (@Store = '8728')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8729 Details ------
If (@Store = '8729')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8730 Details ------
If (@Store = '8730')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8731 Details ------
If (@Store = '8731')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8801 Details ------
If (@Store = '8801')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8804 Details ------
If (@Store = '8804')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
  End

------- Store : 8805 Details ------
If (@Store = '8805')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8807 Details ------
If (@Store = '8807')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8808 Details ------
If (@Store = '8808')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8809 Details ------
If (@Store = '8809')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8810 Details ------
If (@Store = '8810')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
  End

------- Store : 8811 Details ------
If (@Store = '8811')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8812 Details ------
If (@Store = '8812')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8813 Details ------
If (@Store = '8813')
  Begin
   -- Base Configuration Entered
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 411) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 411
        End 
  End

------- Store : 8952 Details ------
If (@Store = '8952')
  Begin
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 401) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 401
        End 
    If Exists (Select 1 From [dbo].[SystemUsers] su Where su.[ID] = 402) 
        Begin
            Update dbo.SystemUsers Set IsDeleted = '0' Where Id = 402
        End 
  End
GO

IF EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'tgSystemUsersUpdate') AND type in (N'TR'))
BEGIN
    PRINT 'Enable trigger tgSystemUsersUpdate';
    ENABLE TRIGGER [dbo].[tgSystemUsersUpdate] ON [dbo].[SystemUsers];
END
GO

If @@Error = 0
   Print 'Success: Update Table SystemUsers for US10640 has been deployed successfully'
Else
   Print 'Failure: Update Table SystemUsers for US10640 has not been deployed successfully'
Go