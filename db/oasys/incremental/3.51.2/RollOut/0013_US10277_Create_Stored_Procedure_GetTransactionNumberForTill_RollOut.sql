IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'GetTransactionNumberForTill') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure GetTransactionNumberForTill'
    EXEC ('CREATE PROCEDURE dbo.GetTransactionNumberForTill AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure GetTransactionNumberForTill')
GO

ALTER PROCEDURE dbo.GetTransactionNumberForTill 
    @Id int 
AS
    BEGIN
        DECLARE @TranNumber int
        SET @Id = @Id + 9000
        IF EXISTS (SELECT 1 FROM dbo.SystemNumbers WHERE Id = @Id)
            BEGIN
                EXEC @TranNumber = dbo.SystemNumbersGetNext @Id
            END 
        ELSE
            BEGIN
                declare @min as int
                select @min = LongValue FROM dbo.Parameters WHERE ParameterId = 6500
                declare @max as int
                select @max = LongValue FROM dbo.Parameters WHERE ParameterId = 6501
                
                INSERT INTO dbo.SystemNumbers([ID],[Description],[NextNumber],[Size],[Min],[Max])
                    VALUES (@Id, CAST(@Id AS char(20)), @min, len(cast(@max as varchar)), @min, @max)
                    
                EXEC @TranNumber = dbo.SystemNumbersGetNext @Id
            END
        RETURN @TranNumber
    END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure GetTransactionNumber for US10277 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure GetTransactionNumber for US10277 has not been deployed'
GO

--Apply Permissions

Grant Execute On dbo.GetTransactionNumberForTill To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure dbo.GetTransactionNumberForTill has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure dbo.GetTransactionNumberForTill might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on dbo.GetTransactionNumberForTill to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure dbo.GetTransactionNumberForTill has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure dbo.GetTransactionNumberForTill has NOT been successfully deployed'
go
