IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 610 AND TableId = 1 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (610, 1, 42600, 0, 1, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 52 AND TableId = 1 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (52, 1, 42600, 0, 0, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (304, 1, 42600, 0, 1, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 300 AND TableId = 1 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (300, 1, 42600, 0, 1, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 1 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (310, 1, 42600, 0, 1, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 2 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (310, 2, 42600, 0, 0, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 3 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (310, 3, 42600, 0, 0, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (310, 4, 42600, 0, 0, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 42553)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (310, 4, 42553, 7, 1, 0, null, null, null)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 306 AND TableId = 1 AND ColumnId = 42600)
    INSERT INTO ReportColumns (
            [ReportId],
            [TableId],
            [ColumnId],
            [Sequence],
            [IsVisible],
            [IsBold],
            [IsHighlight],
            [HighlightColour],
            [Fontsize])
    VALUES
        (306, 1, 42600, 0, 1, 0, null, null, null)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportColumns for US10307 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportColumns for US10307 has not been deployed successfully'
Go
