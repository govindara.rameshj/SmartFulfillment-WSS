﻿IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'NewBankingUnFloatedCashier') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure NewBankingUnFloatedCashier'
    EXEC ('CREATE PROCEDURE dbo.NewBankingUnFloatedCashier AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure NewBankingUnFloatedCashier')
GO

ALTER procedure [dbo].[NewBankingUnFloatedCashier]
   @PeriodID int,
   @ChoiceOfCashier int = 2
as
begin
set nocount on

select UserID = su.ID,
       Employee = su.EmployeeCode + ' - ' + su.Name
from dbo.SystemUsers su 
where su.IsDeleted = 0                                                              --remove delete cashiers
and ((@ChoiceOfCashier = 0 and su.ID <= (select cast(HCAS as integer) from RETOPT)) --filter out non operational users
    or (@ChoiceOfCashier = 1 and su.ID in (select Mapping_CashierID from dbo.OVC_Mapping_Cashiers)) --filter add virtual cashier    
    or (@ChoiceOfCashier = 2 and (su.ID <= (select cast(HCAS as integer) from RETOPT) or su.ID in (select Mapping_CashierID from dbo.OVC_Mapping_Cashiers))))
and   su.ID not in (select AccountabilityID                                         --remove cashiers already assigned floats
                 from SafeBags
                 where [Type]      = 'F'
                 and   [State]     = 'R'
                 and   OutPeriodID = @PeriodID)
end
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure NewBankingUnFloatedCashier for US10640 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure NewBankingUnFloatedCashier for US10640 has not been deployed'
GO