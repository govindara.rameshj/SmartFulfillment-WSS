IF NOT EXISTS(SELECT 1 FROM dbo.ReportParameter WHERE Id = 353)
    INSERT INTO dbo.ReportParameter (
                    Id,
                    Description,
                    Name,
                    DataType,
                    Size,
                    LookupValuesProcedure,
                    Mask)
    VALUES (353, 'OVC Transaction Number', 'OVCTranNumber', 3, 20, NULL, NULL)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportParameter for US10307 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportParameter for US10307 has not been deployed successfully'
Go
