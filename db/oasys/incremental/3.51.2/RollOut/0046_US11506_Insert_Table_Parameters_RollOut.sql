IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID IN (53, 54, 55, 56, 57, 58, 59, 60))
BEGIN
    INSERT INTO dbo.Parameters (
        [ParameterID],
        [Description],
        [StringValue],
        [LongValue],
        [BooleanValue],
        [DecimalValue],
        [ValueType])
    SELECT 53 as [ParameterID], 'WeekdaySlotId' as [Description], NULL as [StringValue], 1 as [LongValue], 0 as [BooleanValue], NULL as [DecimalValue], 1 as [ValueType]
    UNION ALL 
    SELECT 54, 'WeekdaySlotDescription', 'Full day slot', 0, 0, NULL, 0
    UNION ALL
    SELECT 55, 'WeekdaySlotStartTime', '08:00:00', 0, 0, NULL, 0    
    UNION ALL
    SELECT 56, 'WeekdaySlotEndTime', '21:00:00', 0, 0, NULL, 0  
    UNION ALL
    SELECT 57, 'SaturdaySlotId', NULL, 3, 0, NULL, 1    
    UNION ALL
    SELECT 58, 'SaturdaySlotDescription', 'Saturday slot', 0, 0, NULL, 0    
    UNION ALL
    SELECT 59, 'SaturdaySlotStartTime', '08:00:00', 0, 0, NULL, 0   
    UNION ALL
    SELECT 60, 'SaturdaySlotEndTime', '21:00:00', 0, 0, NULL, 0 

END
GO

If @@Error = 0
   Print 'Success: Insert into table Parameters for US11506 has been deployed successfully'
Else
   Print 'Failure: Insert into table Parameters for US11506 has not been deployed successfully'
GO