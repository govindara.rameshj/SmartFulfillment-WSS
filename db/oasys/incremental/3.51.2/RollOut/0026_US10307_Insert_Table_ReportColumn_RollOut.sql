IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumn WHERE Id = 42600)
    INSERT INTO dbo.ReportColumn (
                [Id],
                [Name],
                [Caption],
                [FormatType],
                [Format],
                [IsImagePath],
                [MinWidth],
                [MaxWidth],
                [Alignment])
    VALUES 
        (42600, 'OVCTranNumber', 'OVC Tran Number', 1, null, 0, 130, 160, 2)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumn WHERE Id = 42553)
    INSERT INTO dbo.ReportColumn (
                [Id],
                [Name],
                [Caption],
                [FormatType],
                [Format],
                [IsImagePath],
                [MinWidth],
                [MaxWidth],
                [Alignment])
    VALUES 
        (42553, 'OriginalOVCTranNumber', 'Original OVC Tran Number', 1, null, 0, 130, 160, 2)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportColumn for US10307 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportColumn for US10307 has not been deployed successfully'
Go