--Paid Out 
--Reason Code - "Stamps"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 6 AND [TYPE] = 'M-' AND [NAME] = 'Stamps')
BEGIN
    UPDATE [dbo].[SystemCodes]
    SET [NAME] = 'Postage'
    WHERE [ID] = 6 AND [TYPE] = 'M-' AND [NAME] = 'Stamps'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '06' AND [TYPE] = 'M-' AND [DESCR] = 'Stamps')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [DESCR] = 'Postage'
    WHERE [CODE] = '06' AND [TYPE] = 'M-' AND [DESCR] = 'Stamps'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [MMRC6] = 'STAMPS')
BEGIN
    UPDATE [dbo].[RETOPT]
    SET [MMRC6] = 'POSTAGE'
    WHERE [FKEY] = '01' AND [MMRC6] = 'STAMPS'
END
GO

--Reason Code - "Change Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '10' AND [TYPE] = 'M-' AND [DESCR] = 'Change Fund')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [IsActive] = 0
    WHERE [CODE] = '10' AND [TYPE] = 'M-' AND [DESCR] = 'Change Fund'
END
GO

--Reason Code - "Leukaemia Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 14 AND [TYPE] = 'M-' AND [NAME] = 'Leukaemia Fund')
BEGIN
    UPDATE [dbo].[SystemCodes]
    SET [NAME] = 'Charity'
    WHERE [ID] = 14 AND [TYPE] = 'M-' AND [NAME] = 'Leukaemia Fund'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '14' AND [TYPE] = 'M-' AND [DESCR] = 'Leukaemia Fund')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [DESCR] = 'Charity'
    WHERE [CODE] = '14' AND [TYPE] = 'M-' AND [DESCR] = 'Leukaemia Fund'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [MMRC14] = 'LEUKAEMIA FU')
BEGIN
    UPDATE [dbo].[RETOPT]
    SET [MMRC14] = 'CHARITY'
    WHERE [FKEY] = '01' AND [MMRC14] = 'LEUKAEMIA FU'
END
GO

--Misc In 
--Reason Code - "Burger Van"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 4 AND [TYPE] = 'M+' AND [NAME] = 'Burger Van')
BEGIN
    UPDATE [dbo].[SystemCodes]
    SET [NAME] = 'Car Park Trading'
    WHERE [ID] = 4 AND [TYPE] = 'M+' AND [NAME] = 'Burger Van'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '04' AND [TYPE] = 'M+' AND [DESCR] = 'Burger Van')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [DESCR] = 'Car Park Trading'
    WHERE [CODE] = '04' AND [TYPE] = 'M+' AND [DESCR] = 'Burger Van'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [MPRC4] = 'Burger Van')
BEGIN
    UPDATE [dbo].[RETOPT]
    SET [MPRC4] = 'CAR PARK TRADING'
    WHERE [FKEY] = '01' AND [MPRC4] = 'Burger Van'
END
GO

--Reason Code - "Phone Box"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '06' AND [TYPE] = 'M+' AND [DESCR] = 'Phone Box')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [IsActive] = 0
    WHERE [CODE] = '06' AND [TYPE] = 'M+' AND [DESCR] = 'Phone Box'
END
GO

--Reason Code - "Rent"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '08' AND [TYPE] = 'M+' AND [DESCR] = 'Rent')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [IsActive] = 0
    WHERE [CODE] = '08' AND [TYPE] = 'M+' AND [DESCR] = 'Rent'
END
GO

--Reason Code - "Leukaemia Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 14 AND [TYPE] = 'M+' AND [NAME] = 'Leukaemia Fund')
BEGIN
    UPDATE [dbo].[SystemCodes]
    SET [NAME] = 'Charity'
    WHERE [ID] = 14 AND [TYPE] = 'M+' AND [NAME] = 'Leukaemia Fund'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '14' AND [TYPE] = 'M+' AND [DESCR] = 'Leukaemia Fund')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [DESCR] = 'Charity'
    WHERE [CODE] = '14' AND [TYPE] = 'M+' AND [DESCR] = 'Leukaemia Fund'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [MPRC14] = 'LEUKAEMIA FU')
BEGIN
    UPDATE [dbo].[RETOPT]
    SET [MPRC14] = 'CHARITY'
    WHERE [FKEY] = '01' AND [MPRC14] = 'LEUKAEMIA FU'
END
GO

--Paid Out correction 
--Reason Code - "Stamps"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 6 AND [TYPE] = 'C-' AND [NAME] = 'Stamps')
BEGIN
    UPDATE [dbo].[SystemCodes]
    SET [NAME] = 'Postage'
    WHERE [ID] = 6 AND [TYPE] = 'C-' AND [NAME] = 'Stamps'
END
GO

--Reason Code - "Leukaemia Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 14 AND [TYPE] = 'C-' AND [NAME] = 'Leukaemia Fund')
BEGIN
    UPDATE [dbo].[SystemCodes]
    SET [NAME] = 'Charity'
    WHERE [ID] = 14 AND [TYPE] = 'C-' AND [NAME] = 'Leukaemia Fund'
END
GO

--Misc In correction
--Reason Code - "Burger Van"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 4 AND [TYPE] = 'C+' AND [NAME] = 'Burger Van')
BEGIN
    UPDATE [dbo].[SystemCodes]
    SET [NAME] = 'Car Park Trading'
    WHERE [ID] = 4 AND [TYPE] = 'C+' AND [NAME] = 'Burger Van'
END
GO

--Reason Code - "Leukaemia Fund"
IF EXISTS (SELECT 1 FROM [dbo].[SystemCodes] WHERE [ID] = 14 AND [TYPE] = 'C+' AND [NAME] = 'Leukaemia Fund')
BEGIN
    UPDATE [dbo].[SystemCodes]
    SET [NAME] = 'Charity'
    WHERE [ID] = 14 AND [TYPE] = 'C+' AND [NAME] = 'Leukaemia Fund'
END
GO

--Refund
--Reason Code - "Not Needed"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '01' AND [TYPE] = 'RF' AND [DESCR] = 'Not Needed')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [DESCR] = 'Customer Changed Mind'
    WHERE [CODE] = '01' AND [TYPE] = 'RF' AND [DESCR] = 'Not Needed'
END
GO

--Reason Code - "Wrong Item"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '03' AND [TYPE] = 'RF' AND [DESCR] = 'Wrong Item')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [IsActive] = 0
    WHERE [CODE] = '03' AND [TYPE] = 'RF' AND [DESCR] = 'Wrong Item'
END
GO

--Reason Code - "Other"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '06' AND [TYPE] = 'RF' AND [DESCR] = 'Other')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [IsActive] = 0
    WHERE [CODE] = '06' AND [TYPE] = 'RF' AND [DESCR] = 'Other'
END
GO

--Price Override
--Reason Code - "Deleted Product"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '06' AND [TYPE] = 'OV' AND [DESCR] = 'Deleted Product')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [DESCR] = 'Web Return'
    WHERE [CODE] = '06' AND [TYPE] = 'OV' AND [DESCR] = 'Deleted Product'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [PORC6] = 'DELETED PRODUCT')
BEGIN
    UPDATE [dbo].[RETOPT]
    SET [PORC6] = 'WEB RETURN'
    WHERE [FKEY] = '01' AND [PORC6] = 'DELETED PRODUCT'
END
GO

--Reason Code - "Incorrect Day Price"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '11' AND [TYPE] = 'OV' AND [DESCR] = 'Incorrect Day Price')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [IsActive] = 0
    WHERE [CODE] = '11' AND [TYPE] = 'OV' AND [DESCR] = 'Incorrect Day Price'
END
GO

--Reason Code - "Temporary Deal Group"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '12' AND [TYPE] = 'OV' AND [DESCR] = 'Temporary Deal Group')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [IsActive] = 0
    WHERE [CODE] = '12' AND [TYPE] = 'OV' AND [DESCR] = 'Temporary Deal Group'
END
GO

--Reason Code - "Even Refunds"
IF EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [CODE] = '08' AND [TYPE] = 'OV' AND [DESCR] = 'Event Refund')
BEGIN
    UPDATE [dbo].[SYSCOD]
    SET [IsActive] = 1,
        [DESCR] = 'HDC Return'
    WHERE [CODE] = '08' AND [TYPE] = 'OV' AND [DESCR] = 'Event Refund'
END
GO

IF EXISTS (SELECT 1 FROM [dbo].[RETOPT] WHERE [FKEY] = '01' AND [PORC8] = 'EVENT REFUND')
BEGIN
    UPDATE [dbo].[RETOPT]
    SET [PORC8] = 'HDC RETURN'
    WHERE [FKEY] = '01' AND [PORC8] = 'EVENT REFUND'
END
GO

If @@Error = 0
   Print 'Success: Update Reason Codes for US11700 has been deployed successfully'
Else
   Print 'Failure: Update Reason Codes for US11700 has not been deployed successfully'
Go