IF EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'GetDeliverySlots') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Dropping procedure GetDeliverySlots'
    DROP PROCEDURE dbo.GetDeliverySlots
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure GetDeliverySlots for US11509 has been rolled back successfully'
Else
   Print 'Failure: The Create Stored Procedure GetDeliverySlots for US11509 has not been rolled back'
GO