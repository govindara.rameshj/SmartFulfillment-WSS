DELETE FROM dbo.Parameters WHERE ParameterId IN (6500, 6501)
GO

If @@Error = 0
   Print 'Success: The Delete into Parameters table for US10277 has been deployed successfully'
Else
   Print 'Failure: The Delete into Parameters table for US10277 has not been deployed'
GO