IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'svf_GetCashierIDFromOVC_Mapping_Cashiers') AND type in (N'FN'))
DROP FUNCTION svf_GetCashierIDFromOVC_Mapping_Cashiers
GO

IF @@Error = 0
   Print 'Success: Scalar-values Function svf_GetCashierIDFromOVC_Mapping_Cashiers for US10640 has been successfully dropped'
ELSE
   Print 'Failure: Scalar-values Function svf_GetCashierIDFromOVC_Mapping_Cashiers for US10640 has NOT been successfully dropped'
GO