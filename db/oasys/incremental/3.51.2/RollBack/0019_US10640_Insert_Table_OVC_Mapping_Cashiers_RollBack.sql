IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[OVC_Mapping_Cashiers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DELETE FROM  [dbo].[OVC_Mapping_Cashiers] WHERE [OVC_TillID] in (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 65, 66, 67, 68, 69)
GO

If @@Error = 0
   Print 'Success: Insert Table OVC_Mapping_Cashier for US10640 has been deployed successfully'
Else
   Print 'Failure: Insert Table OVC_Mapping_Cashier for US10640 has not been deployed successfully'
Go