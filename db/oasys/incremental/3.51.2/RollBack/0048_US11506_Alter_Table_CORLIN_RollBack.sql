PRINT ('Altering table CORLIN')
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORLIN') and name = 'RequiredFulfiller')
BEGIN
    PRINT 'Dropping RequiredFulfiller column to CORLIN'
    ALTER TABLE [dbo].[CORLIN] DROP COLUMN [RequiredFulfiller]
END
GO

If @@Error = 0
   Print 'Success: Altering table CORLIN for US11506 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table CORLIN for US11506 has not been rolled back successfully'
GO  