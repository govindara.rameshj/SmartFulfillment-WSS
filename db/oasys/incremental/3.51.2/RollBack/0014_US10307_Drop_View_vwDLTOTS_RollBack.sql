IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwDLTOTS]'))
DROP VIEW [dbo].[vwDLTOTS]
GO

IF @@Error = 0
   PRINT 'Success: View vwDLTOTS for US10307 has been deployed successfully dropped'
ELSE
   PRINT 'Failure: View vwDLTOTS for US10307 has not been deployed successfully dropped'
Go
