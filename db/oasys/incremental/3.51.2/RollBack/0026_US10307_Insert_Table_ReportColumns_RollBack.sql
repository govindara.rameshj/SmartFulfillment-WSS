DELETE FROM dbo.ReportColumns
WHERE ReportId = 610 AND TableId = 1 AND ColumnId = 42600
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 52 AND TableId = 1 AND ColumnId = 42600
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 304 AND TableId = 1 AND ColumnId = 42600
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 300 AND TableId = 1 AND ColumnId = 42600
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 310 AND TableId = 1 AND ColumnId = 42600
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 310 AND TableId = 2 AND ColumnId = 42600
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 310 AND TableId = 3 AND ColumnId = 42600
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 42600
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 42553
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 306 AND TableId = 1 AND ColumnId = 42600
GO

If @@Error = 0
   Print 'Success: Delete from Table ReportColumns for US10307 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportColumns for US10307 has not been deployed successfully'
Go