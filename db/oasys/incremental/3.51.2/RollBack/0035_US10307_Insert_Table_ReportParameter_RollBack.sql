﻿DELETE FROM dbo.ReportParameter
WHERE Id IN (351, 352)
GO

If @@Error = 0
   Print 'Success: Delete from Table ReportParameter for US113 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportParameter for US113 has not been deployed successfully'
Go
