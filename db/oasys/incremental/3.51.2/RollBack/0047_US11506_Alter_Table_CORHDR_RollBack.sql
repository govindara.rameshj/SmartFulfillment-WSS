PRINT ('Altering table CORHDR')
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotID')
BEGIN
    PRINT 'Dropping RequiredDeliverySlotID column to CORHDR'
    ALTER TABLE [dbo].[CORHDR] DROP COLUMN [RequiredDeliverySlotID]
END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotDescription')
BEGIN
    PRINT 'Dropping RequiredDeliverySlotDescription column to CORHDR'
    ALTER TABLE [dbo].[CORHDR] DROP COLUMN [RequiredDeliverySlotDescription]
END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotStartTime')
BEGIN
    PRINT 'Dropping RequiredDeliverySlotStartTime column to CORHDR'
    ALTER TABLE [dbo].[CORHDR] DROP COLUMN [RequiredDeliverySlotStartTime]
END
GO

IF EXISTS (select 1 from sys.columns where object_id = OBJECT_ID('CORHDR') and name = 'RequiredDeliverySlotEndTime')
BEGIN
    PRINT 'Dropping RequiredDeliverySlotEndTime column to CORHDR'
    ALTER TABLE [dbo].[CORHDR] DROP COLUMN [RequiredDeliverySlotEndTime]
END
GO

If @@Error = 0
   Print 'Success: Altering table CORHDR for US11506 has been rolled back successfully'
ELSE
   Print 'Failure: Altering table CORHDR for US11506 has not been rolled back successfully'
GO  