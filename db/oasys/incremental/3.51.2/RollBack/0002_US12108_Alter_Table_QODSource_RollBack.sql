SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODSource]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
    AND EXISTS (SELECT 1 FROM sys.key_constraints WHERE parent_object_id = object_id('QODSource') AND [type] = 'PK')
BEGIN
    PRINT 'Dropping primary key from table QODSource'

    ALTER TABLE [dbo].[QODSource]
    DROP CONSTRAINT [PK_QODSource]
    
    ALTER TABLE [dbo].[QODSource]
    DROP COLUMN [ID]
END
GO

If @@Error = 0
   Print 'Success: The Alter Table QODSource for US12108 has been rolled back successfully'
Else
   Print 'Failure: The Alter Table QODSource for US12108 has not been rolled back'
GO
