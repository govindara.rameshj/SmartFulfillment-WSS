DECLARE @sql NVARCHAR(MAX), 
        @view_name NVARCHAR(100)

SET @view_name='vwCORHDRFull'

SELECT @sql='VIEW dbo.['+@view_name+'] AS
SELECT     
CH.NUMB, 
CH.CUST, 
CH.DATE1, 
CH.DELD, 
CH.CANC, 
CH.DELI, 
CH.DELC, 
CH.AMDT, 
CH.ADDR1, 
CH.ADDR2, 
CH.ADDR3, 
CH.ADDR4, 
CH.PHON, 
CH.PRNT, 
CH.RPRN, 
CH.REVI, 
CH.MVST, 
CH.DCST, 
CH.QTYO, 
CH.QTYT, 
CH.QTYR, 
CH.WGHT, 
CH.VOLU, 
CH.SDAT, 
CH.STIL, 
CH.STRN, 
CH.RDAT, 
CH.RTIL, 
CH.RTRN, 
CH.MOBP, 
CH.POST, 
CH.NAME,
CH.HOMP, 
CH4.DDAT, 
CH4.OEID, 
CH4.FDID, 
CH4.DeliveryStatus, 
CH4.RefundStatus, 
CH4.SellingStoreId, 
CH4.SellingStoreOrderId, 
CH4.OMOrderNumber, 
CH4.CustomerAddress1, 
CH4.CustomerAddress2, 
CH4.CustomerAddress3, 
CH4.CustomerAddress4, 
CH4.CustomerPostcode, 
CH4.CustomerEmail, 
CH4.PhoneNumberWork, 
CH4.IsSuspended, CH5.Source, 
CH5.SourceOrderNumber, 
CH5.ExtendedLeadTime, 
CH5.DeliveryContactName, 
CH5.DeliveryContactPhone, 
CH5.CustomerAccountNo, 
ISNULL(CH5.Source, ''NU'') AS SOURCE_CODE, 
CAST(1 - ISNULL(CH5.IsClickAndCollect, 0) AS BIT) AS SHOW_IN_UI, 
CAST(1 - ISNULL(CH5.IsClickAndCollect, 0) AS BIT) AS SEND_UPDATES_TO_OM, 
ISNULL(CH5.IsClickAndCollect, 0) AS ALLOW_REMOTE_CREATE, ISNULL(CH5.IsClickAndCollect, 0) AS IS_CLICK_AND_COLLECT
FROM dbo.CORHDR AS CH 
    INNER JOIN dbo.CORHDR4 AS CH4 ON CH.NUMB = CH4.NUMB 
    LEFT OUTER JOIN dbo.CORHDR5 AS CH5 ON CH5.NUMB = CH.NUMB'

IF OBJECT_ID (@view_name,'view') IS NOT NULL 
    SET @sql='ALTER '+@sql
ELSE 
    SET @sql='CREATE '+@sql;

EXEC (@sql);

IF OBJECT_ID(@view_name,'view') IS NOT NULL 
    BEGIN
        SELECT @sql = 'sp_refreshview ''dbo.'+@view_name+'''; '
        EXEC(@sql)
    END
GO

IF @@Error = 0
   PRINT 'Success: The Alter View vwCORHDRFull for US11506 has been deployed successfully'
ELSE
   PRINT 'Failure: The Alter View vwCORHDRFull for US11506 has not been deployed successfully'
Go
