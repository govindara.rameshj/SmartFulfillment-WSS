IF EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'NewBankingSpotCheckList') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Dropping procedure NewBankingSpotCheckList'
    DROP PROCEDURE [dbo].[NewBankingSpotCheckList]
END
GO

If @@Error = 0
   Print 'Success: The Drop Stored Procedure NewBankingSpotCheckList for US10647 has been deployed successfully'
Else
   Print 'Failure: The Drop Stored Procedure NewBankingSpotCheckList for US10647 has not been deployed'
GO
