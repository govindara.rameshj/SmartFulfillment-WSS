IF NOT EXISTS (SELECT 1 FROM sys.objects 
    WHERE object_id = OBJECT_ID(N'DuressCodeReportGet') AND type in (N'P', N'PC'))
BEGIN
    PRINT 'Creating procedure DuressCodeReportGet'
    EXEC ('CREATE PROCEDURE dbo.DuressCodeReportGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure DuressCodeReportGet')
GO

ALTER PROCEDURE [dbo].[DuressCodeReportGet]
    @DateStart datetime = NULL, 
    @DateEnd datetime = NULL
AS
BEGIN
    SET NOCOUNT ON;
    
    SELECT
        DATE1 as 'Date',
        CASH as 'Cashier',
        TILL as 'Till',
        [TRAN] as 'Transaction'
    FROM DLTOTS 
    WHERE   [OPEN]          =       '9'
            AND CASH        <>      '000'
            AND VOID        =       0
            AND PARK        =       0
            AND TMOD        =       0  
            AND (Date1 >= @DateStart and Date1 <= @DateEnd)
END

GO

If @@Error = 0
   Print 'Success: The Deleting of Stored Procedure DuressCodeReportGet for US10307 has been deployed successfully'
Else
   Print 'Failure: The Deleting of Stored Procedure DuressCodeReportGet for US10307 has not been deployed'
GO