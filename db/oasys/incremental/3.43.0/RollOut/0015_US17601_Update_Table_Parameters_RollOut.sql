UPDATE [dbo].[Parameters]
   SET [Description] = 'Gift Card Hot Key'
 WHERE [ParameterID] = 873
GO

If @@Error = 0
   Print 'Success: Update Table Parameters for US17601 has been deployed successfully'
Else
   Print 'Failure: Update Table Parameters for US17601 has not been deployed successfully'
Go