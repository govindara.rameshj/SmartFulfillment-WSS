UPDATE dbo.SystemAccountCodes
     SET MiscInAccountCode12 = '9878'
GO

If @@Error = 0
   Print 'Success: Update Table SystemAccountCodes for US17601 has been deployed successfully'
Else
   Print 'Failure: Update Table SystemAccountCodes for US17601 has not been deployed successfully'
Go