SET ANSI_NULLS ON
GO
SET ANSI_WARNINGS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SaveDataInDLGiftCard]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[usp_SaveDataInDLGiftCard] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

ALTER PROCEDURE [dbo].[usp_SaveDataInDLGiftCard]
	@DATE1		date,
	@TILL		char(2),
	@TRAN		char(4),
	@CARDNUM	char(19),
	@EEID		char(3),
	@TYPE		char(2),
	@AMNT		decimal(9, 2),
	@AUTH		char(150),
	@MSGNUM		char(150),
	@TRANID		decimal(38,0),
	@RTIFlag	char(1)='S'
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO [dbo].[DLGIFTCARD] ([DATE1], [TILL], [TRAN], [CARDNUM], [EEID], [TYPE], [AMNT], [AUTH], [MSGNUM], [TRANID], [RTI])
	VALUES (@DATE1, @TILL, @TRAN, @CARDNUM, @EEID, @TYPE, @AMNT, @AUTH, @MSGNUM, @TRANID, @RTIFlag)
	
	SET NOCOUNT OFF;
END
GO

If @@Error = 0
   Print 'Success: Create Stored Procedure usp_SaveDataInDLGiftCard for US17601 has been deployed successfully'
Else
   Print 'Failure: Create Stored Procedure usp_SaveDataInDLGiftCard for US17601 has not been deployed successfully'
GO
