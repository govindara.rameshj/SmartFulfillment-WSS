INSERT INTO [dbo].[Parameters]
           ([ParameterID]
           ,[Description]
           ,[StringValue]
           ,[LongValue]
           ,[BooleanValue]
           ,[DecimalValue]
           ,[ValueType])
     SELECT 
           979				as [ParameterID]
           ,'GiftCard SKU'  as [Description]
           ,'769999'		as [StringValue]
           ,0				as [LongValue]
           ,0				as [BooleanValue]
           ,0.0				as [DecimalValue]
           ,0				as [ValueType]
     WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] WHERE ParameterID = 979)      
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US17601 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US17601 has not been deployed successfully'
Go
