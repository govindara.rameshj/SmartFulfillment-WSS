INSERT INTO [dbo].[Parameters]
           ([ParameterID]
           ,[Description]
           ,[StringValue]
           ,[LongValue]
           ,[BooleanValue]
           ,[DecimalValue]
           ,[ValueType])
SELECT * FROM (
     SELECT 
           970021					as [ParameterID]
           ,'Store AccountId'		as [Description]
           ,''						as [StringValue]
           ,0						as [LongValue]
           ,0						as [BooleanValue]
           ,0.0						as [DecimalValue]
           ,0						as [ValueType]
     UNION ALL
     SELECT
           970022
           ,'Partial Gift Card payment timeout'
           ,NULL
           ,20
           ,0
           ,NULL
           ,1 
) t
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] p WHERE p.ParameterID = t.[ParameterID])
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US17714 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US17714 has not been deployed successfully'
Go