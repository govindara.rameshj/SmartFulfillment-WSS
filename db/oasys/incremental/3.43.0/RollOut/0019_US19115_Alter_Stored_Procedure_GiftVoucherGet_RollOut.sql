SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[GiftVoucherGet]
	@Date	date = null
as
begin

select
	dg.DATE1	as 'Date',
	dg.TILL		as 'TillId',
	dg.[TRAN]	as 'TranNumber',
	dg.SERI		as 'SerialNumber',
	case dg.[TYPE]
		when 'SA' then 'Allocation'
		when 'TR' then 'Allocation'
		when 'CR' then 'Allocation'
		when 'CC' then 'Allocation'
		when 'VR' then 'Allocation'
		when 'TS' then 'Redemption'
		when 'RR' then 'Redemption'
		when 'CS' then 'Redemption'
		when 'VS' then 'Redemption'
	end			as 'VoucherType'
from
	DLGIFT dg
where
	@Date is null or (@Date is not null and dg.DATE1 = @Date) 
union
select
	DATE1	as 'Date',
	TILL	as 'TillId',
	[TRAN]	as 'TranNumber',
	CARDNUM	as 'SerialNumber',
	case [TYPE]
		when 'SA' then 'Allocation'
		when 'TR' then 'Allocation'
		when 'CR' then 'Allocation'
		when 'CC' then 'Allocation'
		when 'VR' then 'Allocation'
		when 'TS' then 'Redemption'
		when 'RR' then 'Redemption'
		when 'CS' then 'Redemption'
		when 'VS' then 'Redemption'
	end		as 'VoucherType'
from 
	dlgiftcard 
where
	@Date is null or (@Date is not null and DATE1 = @Date) 
order by
	DATE1,TILL,[TRAN]
end
go

If @@Error = 0
   Print 'Success: The Alter Stored Procedure GiftVoucherGet for US19115 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure GiftVoucherGet for US19115 has not been deployed successfully'