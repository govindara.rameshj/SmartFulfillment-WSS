update ViewerConfig 
set Title = 'Gift Voucher / Card Report' 
where ID = 90
GO

If @@Error = 0
   Print 'Success: Update Table ViewerConfig for US19115 has been deployed successfully'
Else
   Print 'Failure: Update Table ViewerConfig for US19115 has not been deployed successfully'
Go