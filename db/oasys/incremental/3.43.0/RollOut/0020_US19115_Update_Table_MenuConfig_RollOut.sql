update [MenuConfig] 
set appname = 'Gift Voucher / Card Enquiry' 
where ID in (3210, 11360)
GO

If @@Error = 0
   Print 'Success: Update Table MenuConfig for US19115 has been deployed successfully'
Else
   Print 'Failure: Update Table MenuConfig for US19115 has not been deployed successfully'
Go