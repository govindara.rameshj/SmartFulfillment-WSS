update RETOPT
set TTDE13 = 'Gift Card'
go

If @@Error = 0
   Print 'Success: Update Table RETOPT for US15339 has been deployed successfully'
Else
   Print 'Failure: Update Table RETOPT for US15339 has not been deployed successfully'
Go