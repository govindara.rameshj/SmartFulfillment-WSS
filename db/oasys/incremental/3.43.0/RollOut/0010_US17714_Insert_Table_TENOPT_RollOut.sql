INSERT INTO [dbo].[TENOPT]
(
	[TEND],
	[TTID],
	[TTDE],
	[TTDS],
	[TADA],
	[TTOT],
	[InUse]
)
SELECT
	13,
	13,
	'Gift Card',
	6,
	1,
	0,
	1
WHERE NOT EXISTS (SELECT 1 FROM [dbo].[TENOPT] WHERE [TTID] = 13)

UPDATE [dbo].[TENOPT]
SET [TTOT] = 0
WHERE [TEND] = 13
