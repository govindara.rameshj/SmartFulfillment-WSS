ALTER TABLE RSCASH 
ALTER COLUMN NAME [varchar] (35) NULL

If @@Error = 0
   Print 'Success: Alter Table RSCASH for US20578 has been deployed successfully'
Else
   Print 'Failure: Alter Table RSCASH for US20578 has not been deployed successfully'
Go