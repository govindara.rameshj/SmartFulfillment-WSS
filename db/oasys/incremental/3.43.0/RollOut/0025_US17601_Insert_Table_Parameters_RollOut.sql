INSERT INTO [dbo].[Parameters]
           ([ParameterID]
           ,[Description]
           ,[StringValue]
           ,[LongValue]
           ,[BooleanValue]
           ,[DecimalValue]
           ,[ValueType])
     SELECT 
           980						as [ParameterID]
           ,'GiftCard Max Value'	as [Description]
           ,'500'					as [StringValue]
           ,0						as [LongValue]
           ,0						as [BooleanValue]
           ,0.0						as [DecimalValue]
           ,0						as [ValueType]
     WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] WHERE ParameterID = 980)      
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US17601 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US17601 has not been deployed successfully'
Go