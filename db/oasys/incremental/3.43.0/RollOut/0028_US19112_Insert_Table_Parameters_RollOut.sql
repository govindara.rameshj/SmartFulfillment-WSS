INSERT INTO [dbo].[Parameters]
           ([ParameterID]
           ,[Description]
           ,[StringValue]
           ,[LongValue]
           ,[BooleanValue]
           ,[DecimalValue]
           ,[ValueType])
     SELECT 
           981						as [ParameterID]
           ,'GiftCard Min Value'	as [Description]
           ,'1'						as [StringValue]
           ,0						as [LongValue]
           ,0						as [BooleanValue]
           ,0.0						as [DecimalValue]
           ,0						as [ValueType]
     WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] WHERE ParameterID = 981)      
GO

INSERT INTO [dbo].[Parameters]
           ([ParameterID]
           ,[Description]
           ,[StringValue]
           ,[LongValue]
           ,[BooleanValue]
           ,[DecimalValue]
           ,[ValueType])
     SELECT 
           982						as [ParameterID]
           ,'GiftCard Refund Max Value'	as [Description]
           ,'2500'					as [StringValue]
           ,0						as [LongValue]
           ,0						as [BooleanValue]
           ,0.0						as [DecimalValue]
           ,0						as [ValueType]
     WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] WHERE ParameterID = 982)      
GO

INSERT INTO [dbo].[Parameters]
           ([ParameterID]
           ,[Description]
           ,[StringValue]
           ,[LongValue]
           ,[BooleanValue]
           ,[DecimalValue]
           ,[ValueType])
     SELECT 
           983						as [ParameterID]
           ,'GiftCard Refund Min Value'	as [Description]
           ,'1'						as [StringValue]
           ,0						as [LongValue]
           ,0						as [BooleanValue]
           ,0.0						as [DecimalValue]
           ,0						as [ValueType]
     WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] WHERE ParameterID = 983)      
GO

INSERT INTO [dbo].[Parameters]
           ([ParameterID]
           ,[Description]
           ,[StringValue]
           ,[LongValue]
           ,[BooleanValue]
           ,[DecimalValue]
           ,[ValueType])
     SELECT 
           984						as [ParameterID]
           ,'GiftCard Redeem Max Value'	as [Description]
           ,'30000'					as [StringValue]
           ,0						as [LongValue]
           ,0						as [BooleanValue]
           ,0.0						as [DecimalValue]
           ,0						as [ValueType]
     WHERE NOT EXISTS (SELECT 1 FROM [dbo].[Parameters] WHERE ParameterID = 984)      
GO

If @@Error = 0
   Print 'Success: Insert Table Parameters for US19112 has been deployed successfully'
Else
   Print 'Failure: Insert Table Parameters for US19112 has not been deployed successfully'
Go