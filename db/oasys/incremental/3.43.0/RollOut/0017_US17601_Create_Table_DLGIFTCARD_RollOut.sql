IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLGIFTCARD]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	PRINT 'Creating table DLGIFTCARD'
	CREATE TABLE [dbo].[DLGIFTCARD]
	(
	[DATE1] [date] NOT NULL,
	[TILL] [char] (2) COLLATE Latin1_General_CI_AS NOT NULL,
	[TRAN] [char] (4) COLLATE Latin1_General_CI_AS NOT NULL,
	[CARDNUM] [char] (19) COLLATE Latin1_General_CI_AS NOT NULL,
	[EEID] [char] (3) COLLATE Latin1_General_CI_AS NULL,
	[TYPE] [char] (2) COLLATE Latin1_General_CI_AS NULL,
	[AMNT] [decimal] (9, 2) NULL,
	[AUTH] [char] (150) COLLATE Latin1_General_CI_AS NOT NULL,
	[MSGNUM] [char] (150) COLLATE Latin1_General_CI_AS NULL,
	[TRANID] [decimal] (38, 0) NULL,
	[RTI] [char] (1) COLLATE Latin1_General_CI_AS NULL
	) ON [PRIMARY]

	ALTER TABLE [dbo].[DLGIFTCARD] ADD CONSTRAINT [PK_DLGIFTCARD] PRIMARY KEY CLUSTERED ([DATE1], [TILL], [TRAN], [AUTH]) ON [PRIMARY]
	EXEC sp_addextendedproperty N'MS_Description', N'D L G I F T C A R D  =   Daily Gift Card Transaction Lines.', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', NULL, NULL
	EXEC sp_addextendedproperty N'MS_Description', N'Tender Amount - Opposite sign of Transaction', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'AMNT'
	EXEC sp_addextendedproperty N'MS_Description', N'Transaction Date - ie, AP Number', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'DATE1'
	EXEC sp_addextendedproperty N'MS_Description', N'Employee ID performing sale', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'EEID'
	EXEC sp_addextendedproperty N'MS_Description', N'Serial Number of Gift Card', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'CARDNUM'
	EXEC sp_addextendedproperty N'MS_Description', N'PC Till Id - Network ID Number', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'TILL'
	EXEC sp_addextendedproperty N'MS_Description', N'Transaction Number from PC Till', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'TRAN'
	EXEC sp_addextendedproperty N'MS_Description', N'Transaction Type
	"SA" - Sale Of Voucher                - Allocation
	"TS" - Tender In Sale                 - Redemption
	"TR" - Tender In Refund               - Allocation
	"RR" - Refund Of Voucher              - Redemption
	"CS" - Cancel/line rever', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'TYPE'
	EXEC sp_addextendedproperty N'MS_Description', N'Authorisation Code assigned by Card Commerce', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'AUTH'
	EXEC sp_addextendedproperty N'MS_Description', N'Message number allocated to the transaction by Commidea', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'MSGNUM'
	EXEC sp_addextendedproperty N'MS_Description', N'Transaction ID assigned by Commidea�s processing system', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'TRANID'
	EXEC sp_addextendedproperty N'MS_Description', N'RTI Flag', 'SCHEMA', N'dbo', 'TABLE', N'DLGIFTCARD', 'COLUMN', N'RTI'
END
ELSE
BEGIN
 PRINT 'DLGIFTCARD table is already exists.'
END
GO

If @@Error = 0
   Print 'Success: Create Table DLGIFTCARD for US17601 has been deployed successfully'
Else
   Print 'Failure: Create Table DLGIFTCARD for US17601 has not been deployed successfully'
Go
