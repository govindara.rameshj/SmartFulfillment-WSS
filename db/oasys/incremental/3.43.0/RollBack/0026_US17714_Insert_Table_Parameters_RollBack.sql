DELETE FROM  [dbo].[Parameters] WHERE ParameterID IN (970021, 970022)
GO

If @@Error = 0
   Print 'Success: Delete from Table Parameters for US17714 has been deployed successfully'
Else
   Print 'Failure: Delete from Parameters for US17714 has not been deployed successfully'
Go
