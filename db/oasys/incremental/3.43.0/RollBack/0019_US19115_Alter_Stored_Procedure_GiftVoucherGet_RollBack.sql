SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[GiftVoucherGet]
	@Date	date = null
as
begin

select
	dg.DATE1	as 'Date',
	dg.TILL		as 'TillId',
	dg.[TRAN]	as 'TranNumber',
	dg.SERI		as 'SerialNumber',
	case dg.[TYPE]
		when 'SA' then 'Allocation'
		when 'TR' then 'Allocation'
		when 'CR' then 'Allocation'
		when 'CC' then 'Allocation'
		when 'VR' then 'Allocation'
		when 'TS' then 'Redemption'
		when 'RR' then 'Redemption'
		when 'CS' then 'Redemption'
		when 'VS' then 'Redemption'
	end			as 'VoucherType'
from
	DLGIFT dg
where
	@Date is null or (@Date is not null and dg.DATE1 = @Date) 
order by
	dg.DATE1,dg.TILL,dg.[TRAN]

end
go

If @@Error = 0
   Print 'Success: The Alter Stored Procedure GiftVoucherGet for US19115 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure GiftVoucherGet for US19115 has not been deployed successfully'