update
SystemCurrencyDen
set TenderID = 4,
	DisplayText = 'Old Token'
where TenderID = 13

If @@Error = 0
Print 'Success: Prepaid Card has been removed from SystemCurrencyDen'
Else
Print 'Failure: Prepaid Card has not been removed from SystemCurrencyDen'

GO