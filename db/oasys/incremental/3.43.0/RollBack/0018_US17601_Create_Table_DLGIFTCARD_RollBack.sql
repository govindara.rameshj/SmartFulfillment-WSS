IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLGIFTCARD]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	DROP TABLE [dbo].[DLGIFTCARD]
END
GO

If @@Error = 0
   Print 'Success: Drop Table DLGIFTCARD for US17601 has been successfully dropped'
Else
   Print 'Failure: Drop Table DLGIFTCARD for US17601 has not been successfully dropped'
GO
