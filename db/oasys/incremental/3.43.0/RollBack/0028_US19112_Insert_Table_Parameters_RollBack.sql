DELETE FROM  [dbo].[Parameters] WHERE ParameterID IN (981, 982, 983, 984)
GO

If @@Error = 0
   Print 'Success: Delete from Parameters for US19112 has been deployed successfully'
Else
   Print 'Failure: Delete from Parameters for US19112 has not been deployed successfully'
Go
