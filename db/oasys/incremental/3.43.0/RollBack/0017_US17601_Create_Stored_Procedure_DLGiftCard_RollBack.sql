IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SaveDataInDLGiftCard]') AND type in (N'P', N'PC'))
drop procedure usp_SaveDataInDLGiftCard;
go

if @@error = 0
   print 'Success: Stored procedure usp_SaveDataInDLGiftCard for US17601 has been successfully dropped'
else
   print 'Failure: Stored procedure usp_SaveDataInDLGiftCard for US17601 has not been successfully dropped'
go