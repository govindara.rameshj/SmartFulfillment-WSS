ALTER TABLE RSCASH 
ALTER COLUMN NAME [char](22) NULL

If @@Error = 0
   Print 'Success: Alter Table RSCASH for US20578 has been rolled back successfully'
Else
   Print 'Failure: Alter Table RSCASH for US20578 has not been rolled back'
Go