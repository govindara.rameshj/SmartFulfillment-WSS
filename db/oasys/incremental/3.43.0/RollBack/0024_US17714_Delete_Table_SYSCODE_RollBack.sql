INSERT INTO [dbo].[SYSCOD]
           ([TYPE]
           ,[CODE]
           ,[DESCR]
           ,[ATTR]
           ,[LABL]
           ,[IRPO]
           ,[SUPV]
           ,[MANA]
           ,[IPER]
           ,[ACTIVE]
           ,[PMATCH]
           ,[VALUE1]
           ,[VALUE2]
           ,[MFLEX])
	SELECT
           'M-' AS [TYPE],
           '19' AS[CODE],
           'Gift Voucher' AS [DESCR],
           ' ' AS [ATTR],
           CAST(0 AS BIT) AS [LABL],
           CAST(0 AS BIT) AS [IRPO],
           CAST(0 AS BIT) AS [SUPV],
           CAST(0 AS BIT) AS [MANA],
           CAST(0 AS CHAR(1)) AS [IPER],
           CAST(0 AS CHAR(1)) AS [ACTIVE],
           CAST(0 AS BIT) AS [PMATCH],
           CAST(1 AS DECIMAL(9,2)) AS [VALUE1],
           CAST(0 AS DECIMAL(9,2)) AS [VALUE2],
           CAST(0 AS BIT) AS [MFLEX]
    WHERE NOT EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [TYPE] = 'M-' and [CODE] = 19) 
GO
          
INSERT INTO [dbo].[SYSCOD]
           ([TYPE]
           ,[CODE]
           ,[DESCR]
           ,[ATTR]
           ,[LABL]
           ,[IRPO]
           ,[SUPV]
           ,[MANA]
           ,[IPER]
           ,[ACTIVE]
           ,[PMATCH]
           ,[VALUE1]
           ,[VALUE2]
           ,[MFLEX])
	SELECT
           'M+' AS [TYPE],
           '19' AS[CODE],
           'Gift Voucher' AS [DESCR],
           ' ' AS [ATTR],
           CAST(0 AS BIT) AS [LABL],
           CAST(0 AS BIT) AS [IRPO],
           CAST(0 AS BIT) AS [SUPV],
           CAST(0 AS BIT) AS [MANA],
           CAST(0 AS CHAR(1)) AS [IPER],
           CAST(0 AS CHAR(1)) AS [ACTIVE],
           CAST(0 AS BIT) AS [PMATCH],
           CAST(1 AS DECIMAL(9,2)) AS [VALUE1],
           CAST(0 AS DECIMAL(9,2)) AS [VALUE2],
           CAST(0 AS BIT) AS [MFLEX]
     WHERE NOT EXISTS (SELECT 1 FROM [dbo].[SYSCOD] WHERE [TYPE] = 'M+' and [CODE] = 19)       
       
GO