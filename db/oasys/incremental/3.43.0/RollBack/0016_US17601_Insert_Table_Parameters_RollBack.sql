DELETE FROM  [dbo].[Parameters] WHERE ParameterID = 979
GO

If @@Error = 0
   Print 'Success: Delete from Table Parameters for US17601 has been deployed successfully'
Else
   Print 'Failure: Delete from Parameters for US17601 has not been deployed successfully'
Go
