USE [Oasys]
GO
/****** Object:  StoredProcedure [dbo].[NewBankingFloatedCashierDisplay]    Script Date: 10/21/2013 16:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 14/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to Add comments
--				 : when rechecking floats, Performing Pickups,
--				 : Performing Cash drops, rechecking Pickups.
-- TAsk Id		 : 7969
-- DescriptiOn   : Add the SafeBag.Comments column From the
--				 : returned data.
-- TAsk Id		 : 7973
-- DescriptiOn   : Ensure Float Manual Check comments
--				 : And Pickup Bag Comments Are Both Seen
--				 : in the rare event of there Being Both
--				 : for A given floated cAshier.
-- =============================================
ALTER Procedure [dbo].[NewBankingFloatedCashierDisplay]
   @PeriodID Int
As
Begin
	Set NoCount On
	
	Declare @CommentsDelimiter As VarChar(80)
	
	Set @CommentsDelimiter = [dbo].[udf_GetCommentsDelimiter]()
	
	--cashier Accountability model Only
	--floated cAshiers no End of day Pickup
	Select 
		StartFloatChecked              = IsNull(a.FloatChecked, 0),
		StartFloatValue                = a.Value,
		StartFloatSealNumber           = a.SealNumber,
		StartFloatAssignedToUserID     = b.ID,
		StartFloatAssignedToUserName   = b.Name,
		StartFloatAssignedByUserID     = c.ID,
		StartFloatAssignedByUserName   = c.Name,
		TillVariance                   = Cast(Null As Decimal(9,2)),
		NewFloatSealNumber             = '',
		PickupSealNumber               = '',
		PickupAssignedByUserID         = Cast(Null As Int),
		PickupAssignedByUserName       = '',
		PickupAssignedBySecOndUserID   = Cast(Null As Int),
		PickupAssignedBySecOndUserName = '',
		Comments						 = a.Comments
	From
		SafeBags a
			--cashier
			Inner Join
				SystemUsers b
			On
				b.ID = a.AccountabilityID
			--float assigned by
			Inner Join 
				SystemUsers c
			On 
				c.ID = a.OutUserID1 
			--filter out if End of day Pickup Bag exist for this cAshier
			Left Outer Join 
				(
					Select 
						*
					From
						SafeBags
					Where
						Type = 'P'
					And
						[State] <> 'C'
					And 
						IsNull(CashDrop, 0) = 0
				) e
			On
				e.PickupPeriodID   = a.OutPeriodID
			And
				e.AccountabilityID = a.AccountabilityID
	Where
		a.[Type]      = 'F'
	And
		a.[State]     = 'R'
	And
		a.OutPeriodID = @PeriodID
	And
		e.ID is Null
Union All
	--floated cAshiers with Pickup Bags
	Select 
		StartFloatChecked              = IsNull(e.FloatChecked, 0),
		StartFloatValue                = e.Value,
		StartFloatSealNumber           = IsNull(e.SealNumber, ''),
		StartFloatAssignedToUserID     = f.ID,
		StartFloatAssignedToUserName   = IsNull(f.Name, ''),
		StartFloatAssignedByUserID     = g.ID,
		StartFloatAssignedByUserName   = IsNull(g.Name, ''),
		TillVariance                   = 
			(
				Select
					Sum(Value)                                       --total Pickup (Cash drops & e.o.d Pickup)
				From
					SafeBags
				Where
					[Type]           = 'P'
				And
					[State]         <> 'C'
				And
					PickupPeriodID   = @PeriodID
				And
					AccountabilityID = a.AccountabilityID
			) +
			(
				Select
					Value                                            --new float
				From
					SafeBags
				Where
					ID = 
						(
							Select 
								Top 1 RelatedBagId
							From 
								SafeBags
							Where
								[Type]           = 'P'
							And
								[State]         <> 'C'
							And
								PickupPeriodID   = @PeriodID
							And
								AccountabilityID = a.AccountabilityID
							And
								RelatedBagId    <> 0
						)
				) -
				e.Value -                                               --start float
				(
					Select
						GrossSalesAmount                                 --system Sales
					From 
						CashBalCashier
					Where
						PeriodID   = @PeriodID
					And
						CurrencyID =
							(
								Select
									ID
								From
									SystemCurrency
								Where
									IsDefault = 1
							)
					And
						CashierID = a.AccountabilityID
				),
		NewFloatSealNumber             = IsNull(d.SealNumber, ''),
		PickupSealNumber               = a.SealNumber,
		PickupAssignedByUserID         = b.ID,
		PickupAssignedByUserName       = b.Name,
		PickupAssignedBySecOndUserID   = c.ID,
		PickupAssignedBySecOndUserName = c.Name,
		Comments						 = 
		(
			Select
				Case 
					When 
						Len(e.Comments) > 0 
					And 
						Len(a.Comments) > 0 
					Then 
						LTrim(RTrim(e.Comments)) + @CommentsDelimiter + LTrim(RTrim(a.Comments))
				Else 
					LTrim(RTrim(e.Comments)) + LTrim(RTrim(a.Comments))
			End
		)
	From
		SafeBags a
			--Assigned By
			Inner Join 
				SystemUsers b
			On 
				b.ID = a.InUserID1
			--Assigned By SecOnd check
			Inner Join 
				SystemUsers c
			On 
				c.ID = a.InUserID2 
			--new float
			Left Outer Join 
				SafeBags d
			On 
				d.ID = a.RelatedBagId
			--start float
			Inner Join 
				(
					Select
						*
					From
						SafeBags
					Where
						Type = 'F'
					And
						State = 'R'
				) e
			On  
				e.OutPeriodID      = a.PickupPeriodID
			And 
				e.AccountabilityID = a.AccountabilityID
				Inner Join 
					SystemUsers f
				On 
					f.ID = e.AccountabilityID
				Inner Join 
					SystemUsers g
				On 
					g.ID = e.OutUserID1 
			Where 
				a.[Type] = 'P'
			And   
				a.[State] <> 'C'
			And   
				a.PickupPeriodID = @PeriodID
			And   
				IsNull(a.CashDrop, 0) = 0         --End of day Pickup Bag, Should Only have One entry Only
End
