-- =============================================
-- Author        : Alan Lewis
-- Create date   : 08/11/2012
-- User Story	 : 8924: P022-ITN019-I
-- Referral		 : RF1031:  Cash Drop Cashiers Dropdown List
-- Task Id		 : 8985
-- Description   : Drop requirement switch
-- =============================================
Delete
	[Parameters]
Where
	ParameterID = -1031
Go

If @@Error = 0
   Print 'Success: The requirement switch for RF1031 has been successfully deleted'
Else
   Print 'Failure: requirement switch for RF1031 might NOT have been deleted'
Go
