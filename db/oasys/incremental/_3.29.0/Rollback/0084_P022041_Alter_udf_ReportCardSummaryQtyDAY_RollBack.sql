-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Credit Card Summary Qty for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION dbo.svf_ReportCardSummaryQtyDAY( @InputDate date
                                              )
RETURNS int
AS
BEGIN


    DECLARE
       @StartDate date , 
       @CardQty int;

    SET @StartDate = @InputDate;    

                         
    ----------------------------------------------------------------------------------
    -- Get Card Summary Qty - DAY
    ----------------------------------------------------------------------------------
    SELECT @CardQty = COUNT( DP.AMNT
                           )
      FROM
           DLTOTS AS DT INNER JOIN DLPAID AS DP
           ON DP.DATE1
              = 
              DT.DATE1
          AND DP.TILL
              = 
              DT.TILL
          AND DP.[TRAN]
              = 
              DT.[TRAN]
      WHERE DP.TYPE IN( 3 , 8 , 9
                      )
        AND DP.CARD
            <> 
            '0000000000000000000'
        AND DT.CASH <> '000'
        AND DT.VOID = 0
        AND DT.PARK = 0
        AND DT.TMOD = 0
        AND DT.Date1
            = 
            @StartDate;

    SET @CardQty = ISNULL( @CardQty , 0
                         );

    RETURN @CardQty;

END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportCardSummaryQtyDAY" has been sucessfully rolled back for P022-041';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportCardSummaryQtyDAY" might NOT have been rolled back for P022-041';
    END;