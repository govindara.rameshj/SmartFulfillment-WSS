drop procedure MenuGetSingle
go

if @@error = 0
   print 'Success: Stored procedure MenuGetSingle for P022-040 has been successfully dropped'
else
   print 'Failure: Stored procedure MenuGetSingle for P022-040 has NOT been successfully dropped'
go