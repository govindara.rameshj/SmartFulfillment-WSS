
ALTER PROCEDURE dbo.StockGetNonStockDeletedWithStock @Status char( 1
                                                                 ) = 'A'
AS
BEGIN
    SET NOCOUNT ON;
    select
        st.CTGY                 as HieCategory,
        hm.DESCR                as HieCategoryName,
		st.skun					as SkuNumber,
		st.descr				as 'Description',
		st.INON					as IsNonStock,
		st.IDEL					as IsDeleted,
		st.IOBS					as IsObsolete,
		/*st.DDEL					as DateDeleted,*/
		case
		when (st.IOBS = 0 and st.IDEL = 1) then st.[DDEL]
		when (st.IOBS = 1 and st.IDEL = 0) then st.[DOBS]
		when (st.IOBS = 1 and st.IDEL = 1) then st.[DDEL]
		end						as DateDeleted,
		st.DSOL					as DateLastSold,
		st.drec					as DateLastReceived,
		st.PPRI					as PricePrior,
		st.DPRC					as DatePrior,
		st.pric					as Price,
		st.onha					as QtyOnHand,
		case 
			when st.IDEL = 1 then (st.ONHA*st.PRIC) 
			when st.IOBS = 1 then (st.ONHA*st.PRIC)
			/*when st.INON = 1 then 0*/
			else 0
			/*else st.ONHA*st.PRIC*/
		end						as ValueDeleted,
		case 
			when st.DDEL is null then st.ONHA*st.PRIC
			when st.DOBS IS NULL then st.ONHA*st.PRIC
			else 0
		end						as ValueNonStock,
		''						as Comment
	from
		stkmas st
	inner join
		HIEMAS hm on st.CTGY=hm.NUMB and hm.LEVL=5
    where
        st.ONHA <> 0
    AND (   (@Status='A' and (st.INON=1 or (st.IDEL=1 and st.IOBS=1)))
         OR (@Status='D' and (st.IDEL=1 or st.IOBS=1))
         OR (@Status='N' and st.INON=1)
        )
END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The stored procedure "StockGetNonStockDeletedWithStock" has been sucessfully rolled back for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The stored procedure "StockGetNonStockDeletedWithStock" has NOT been sucessfully rolled back for P022-041';
    END;
GO



