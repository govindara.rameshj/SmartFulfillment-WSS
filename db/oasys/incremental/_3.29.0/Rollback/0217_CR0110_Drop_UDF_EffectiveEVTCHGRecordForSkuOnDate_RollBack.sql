DROP function [dbo].[udf_EffectiveEVTCHGRecordForSkuOnDate] 

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_EffectiveEVTCHGRecordForSkuOnDate" has been sucessfully dropped';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_EffectiveEVTCHGRecordForSkuOnDate" might NOT have been dropped';
    END
GO