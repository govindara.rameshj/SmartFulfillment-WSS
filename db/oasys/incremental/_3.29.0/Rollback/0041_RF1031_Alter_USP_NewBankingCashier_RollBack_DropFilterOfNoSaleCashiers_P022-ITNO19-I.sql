-- =============================================
-- Author        : Alan Lewis
-- Update date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Rollback to not filter out cashiers
--				 : that.
-- =============================================
Alter Procedure [dbo].[NewBankingCashier]
As
Begin
	Set NoCount On

Select
	UserID = ID,
	Employee = EmployeeCode + ' - ' + Name
From
	SystemUsers
Where
	IsDeleted = 0	--remove delete cashiers
And
	ID <=			--filter out non operational users
		(
			Select
				Cast(HCAS As Integer) 
			From 
				RETOPT
		)
End
Go

If @@Error = 0
   Print 'Success: The stored procedure "NewBankingCashier" has been sucessfully rolled back for P022-ITNO19-I'
Else
   Print 'Failure: The stored procedure "NewBankingCashier" might NOT have been rolled back for P022-ITNO19-I'
Go
