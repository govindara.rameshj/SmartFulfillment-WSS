set nocount on

print 'P022-037: Clear MenuConfig & ProfileMenuAccess before restore'
delete from ProfileMenuAccess
delete from MenuConfig

------------------------------------------------------------------------------------------------------------------------
print 'P022-037: Restore MenuConfig'

insert MenuConfig select * from MenuConfigBackupBeforeP022037

if (select count(*) from MenuConfig) = (select count(*) from MenuConfigBackupBeforeP022037)
begin
   print 'P022-037: Succesful restore'
end
else
begin
   print 'P022-037: Unsuccessful restore. Aborting rollback. Manual intervention required'
   return
end

------------------------------------------------------------------------------------------------------------------------
print 'P022-037: Restore ProfileMenuAccess'

insert ProfileMenuAccess select * from ProfileMenuAccessBackupBeforeP022037

if (select count(*) from ProfileMenuAccess) = (select count(*) from ProfileMenuAccessBackupBeforeP022037)
begin
   print 'P022-037: Succesful restore'
end
else
begin
   print 'P022-037: Unsuccessful restore. Aborting rollback. Manual intervention required'
   return
end