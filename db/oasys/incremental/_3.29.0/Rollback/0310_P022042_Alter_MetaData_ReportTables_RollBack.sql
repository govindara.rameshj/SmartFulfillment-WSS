
SET NOCOUNT ON;
------------------------------------------------------------------------------------------------------------------------
PRINT 'Rollback Hyperlinks for Shrinkage';

BEGIN TRY

    BEGIN TRANSACTION;

    --------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Price Voilations - Start';

    DELETE FROM ReportColumns
      WHERE reportid = 4
        AND TableId = 1
        AND ColumnId = 42506;

    DELETE FROM reportcolumn
      WHERE id = 42506;

    DELETE FROM ReportHyperlink
      WHERE ReportId = 4
        AND Tableid = 1
        AND RowId = 5
        AND ColumnName
            = 
            'Description';


    PRINT 'Rollback Hyperlinks for Price Voilations - End';
   
    --------------------------------------------------------------------------------------------------------------------------
   
    --------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Incorrect Day Price - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 4
        AND Tableid = 1
        AND RowId = 6
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for Incorrect Day Price - End';
   
    --------------------------------------------------------------------------------------------------------------------------
   
    --------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Temporary Deal Group - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 4
        AND Tableid = 1
        AND RowId = 7
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for Temporary Deal Group - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Rollback Hyperlinks for Stock Loss - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 4
        AND Tableid = 1
        AND RowId = 1
        AND ColumnName
            = 
            'Description';


    PRINT 'Rollback Hyperlinks for Stock Loss - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Rollback Hyperlinks for Known Theft - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 4
        AND Tableid = 1
        AND RowId = 2
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for Known Theft - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Rollback Hyperlinks for Write Off Adjustments - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 4
        AND Tableid = 1
        AND RowId = 3
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for Write Off Adjustments - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Rollback Hyperlinks for Markdowns - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 4
        AND Tableid = 1
        AND RowId = 4
        AND ColumnName
            = 
            'Description';


    PRINT 'Rollback Hyperlinks for Markdowns - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Rollback Hyperlinks for Shrinkage - End'; 
   
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for DRL Analysis - Start';

    PRINT 'Rollback Hyperlinks for Receipt Adjustment - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 2
        AND Tableid = 1
        AND RowId = 2
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for Receipt Adjustment - End';
   
    ------------------------------------------------------------------------------------------------------------------------

    PRINT 'Rollback Hyperlinks for Outstanding Direct Deliveries - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 2
        AND Tableid = 1
        AND RowId = 3
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for Outstanding Direct Deliveries - End';

    PRINT 'Rollback Hyperlinks for DRL Analysis -End ';     


    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Stock Reports - Start';

    DELETE FROM ReportColumns
      WHERE reportid = 7
        AND TableId = 1
        AND ColumnId = 42506;

    ------------------------------------------------------------------------------------------------------------------------

    PRINT 'Rollback Hyperlinks for GapWalk Report - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 7
        AND Tableid = 1
        AND RowId = 1
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for GapWalk Report - End';
   
    ------------------------------------------------------------------------------------------------------------------------

    PRINT 'Rollback Hyperlinks for Number Items Out of Stock - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 7
        AND Tableid = 1
        AND RowId = 2
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for Number Items Out of Stock - End';
   
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Markdown Stockholding - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 7
        AND Tableid = 1
        AND RowId = 3
        AND ColumnName
            = 
            'Description';


    PRINT 'Rollback Hyperlinks for Markdown Stockholding - End';
    
/*    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Open Returns Stockholding - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 7
        AND Tableid = 1
        AND RowId = 6
        AND ColumnName
            = 
            'Description';


    PRINT 'Rollback Hyperlinks for Open Returns Stockholding - End';
*/    
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Deleted Stockholding - Start';
    DELETE FROM ReportHyperlink
      WHERE ReportId = 7
        AND Tableid = 1
        AND RowId = 4
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for Deleted Stockholding - End';
   
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Non-Stock Stockholding - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 7
        AND Tableid = 1
        AND RowId = 5
        AND ColumnName
            = 
            'Description';

    PRINT 'Rollback Hyperlinks for  Non-Stock  Stockholding - End';

    PRINT 'Rollback Hyperlinks for Stock Reports -End ';     


/*
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Banking - Start';

    DELETE FROM ReportColumns
      WHERE reportid = 6
        AND TableId = 1
        AND ColumnId = 42506;

    DELETE FROM ReportParameters
      WHERE ReportId = 6
        AND ParameterId
            = 
            103
        AND Sequence = 1;


    DELETE FROM ReportHyperlink
      WHERE ReportId = 6
        AND Tableid = 1
        AND RowId = 3
        AND ColumnName
            = 
            'Description';


    PRINT 'Rollback Hyperlinks for Banking - End';   
*/


    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Rollback Hyperlinks for Banking Reports - Start';

    DELETE FROM ReportColumns
      WHERE reportid = 9
        AND TableId = 1
        AND ColumnId = 42506;


    DELETE FROM ReportHyperlink
      WHERE ReportId = 9
        AND Tableid = 1
        AND RowId IN( 1 , 3 , 4 , 5 , 6
                    )
        AND ColumnName
            = 
            'Description';


    PRINT 'Rollback Hyperlinks for Banking Reports - End';

    PRINT 'Rollback Hyperlinks for Price Changes - Start';

    DELETE FROM ReportHyperlink
      WHERE ReportId = 8
        AND Tableid = 1
        AND RowId = 6
        AND ColumnName
            = 
            'Description';


    PRINT 'Rollback Hyperlinks for Price Changes - End';


    COMMIT TRANSACTION;
    PRINT 'HyperLink Reports in Dashboard: Successful';
END TRY
BEGIN CATCH

    ROLLBACK TRANSACTION;
    PRINT 'HyperLink Reports in Dashboard: Unsuccessful';
    PRINT 'Rollback unsuccessful';
    RETURN;
END CATCH;

PRINT 'Rollback successful';