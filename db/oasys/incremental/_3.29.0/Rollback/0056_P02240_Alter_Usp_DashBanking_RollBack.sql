ALTER PROCEDURE [dbo].[DashBanking]
as
begin
	declare		
		@LastDate		date;
	
	--get last date
	set @LastDate = (select max(PeriodDate) from Safe where Safe.IsClosed = 1);
	
	--return values
	select
		'Last Daily Banking sent to HQ'		as 'Description',
		@LastDate							as 'Date';
		
end
go

if @@error = 0
   print 'Success: Stored procedure DashBankingGetLastBanked for P022-040 has been successfully rolled back'
else
   print 'Failure: Stored procedure DashBankingGetLastBanked for P022-040 has NOT been successfully rolled back'
go