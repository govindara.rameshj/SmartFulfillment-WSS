DROP FUNCTION [dbo].[udf_GetLatestPriceChangeDateForSKU]

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_GetLatestPriceChangeDateForSKU" has been sucessfully dropped';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_GetLatestPriceChangeDateForSKU" might NOT have been dropped';
    END
GO