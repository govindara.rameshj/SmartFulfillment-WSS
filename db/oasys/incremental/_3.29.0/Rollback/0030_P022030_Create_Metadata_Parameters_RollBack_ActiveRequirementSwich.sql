
DELETE Parameters
  WHERE ParameterID
        = 
        -22030;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The active requirement switch for P022-030 has been successfully deleted';
    END
ELSE
    BEGIN
        PRINT 'Failure: active requirement switch for P022-030 was NOT have been deleted';
    END;
GO
