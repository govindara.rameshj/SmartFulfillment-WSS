Drop PROCEDURE [dbo].[usp_GetEffectiveDateForPriceChangeReport]
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The stored procedure "usp_GetEffectiveDateForPriceChangeReport" has been sucessfully dropped';
    END
ELSE
    BEGIN
        PRINT 'Failure: The stored procedure "usp_GetEffectiveDateForPriceChangeReport" might NOT have been dropped';
    END
GO