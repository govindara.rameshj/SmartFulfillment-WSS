-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Open Returns Quantity - Scalar Valued Function
-- Notes		: This uses STKMAS:RETQ quantity to calculate the items.
-- =================================================================================================
CREATE FUNCTION [dbo].[svf_ReportOpenReturnsQty]()
RETURNS int 
AS
BEGIN

	Declare		@OpenReturnsQty		int
		
	Set			@OpenReturnsQty		=	
										(
										Select	SUM(RETQ)
										From	STKMAS
										Where	(RETQ)	<>	0
										)
	
	RETURN		@OpenReturnsQty

END
GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportOpenReturnsQty" has been sucessfully created for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportOpenReturnsQty" might NOT have been created for P022-041';
    END;
GO