-- =============================================
-- Author        : Alan Lewis
-- Update date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Rollback to using own code to determine
--				 : whether cashier has taken a sale on given
--				 : day.
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 25/06/2012
-- User Story	 : 5623
-- Change Request: CR0087: Prevent Pickup if Cashier still signed On till
-- Task Id		 : 5705
-- Description   : Update stored procedure to include a nullable logged On till Id
-- =============================================
ALTER Procedure [dbo].[NewBankingFloatedPickupList]
   @PeriodID Int
As
Begin
	Set nocount on
	--cashier accountability model only
	--floated cashiers no pickups
	Select
		PickupID = CAST(null As Int),
		PickupSealNumber = '',
		PickupComment = '',
		StartFloatID = a.ID,
		StartFloatSealNumber = a.SealNumber,
		StartFloatValue = a.Value,
		/*
		SaleTaken = CAST((case IsNull((Select NumTransactions
											 From CashBalCashier
											 Where PeriodID    = @PeriodID 
											 And   CurrencyID  = (Select ID From SystemCurrency Where IsDefault = 1)
											 And   CashierID   = a.AccountabilityID), 0)
								   When 0 Then 0
								   Else 1
								end) As Bit),
		*/
		--no valid sales e.g invalid "visiOn deposit"
		SaleTaken = CAST((
						Case IsNull(
							(
								Select 
									NumTransactions
								From 
									CashBalCashier
								Where 
									PeriodID    = @PeriodID 
								 And   
									CurrencyID  = 
										(
											Select 
												ID 
											From 
												SystemCurrency 
											Where 
												IsDefault = 1
										)
							 And   
								CashierID   = a.AccountabilityID
							 And  
								(
									GrossSalesAmount <> 0 
								Or 
									(
										Select 
											count(*)
										From 
											CashBalCashierTen
										Where 
											PeriodID   = @PeriodID
										And   
											CurrencyID = 
												(
													Select 
														ID 
													From 
														SystemCurrency 
													Where 
														IsDefault = 1
												)
										And
											CashierID  = a.AccountabilityID
									) > 0
								)
							), 0)
						When 0 
							Then 0
						Else 
							1
						End) As Bit),
		CashierID = a.AccountabilityID,
		CashierUserName = b.Name,
		CashierEmployeeCode = b.EmployeeCode,
		LoggedOnTillId = d.TILL
	From 
		SafeBags a
		--cashier
			Inner Join 
				SystemUsers b
			On 
				b.ID = a.AccountabilityID
			--filter out if pickup bag (not cash drop) exist for this cashier
			Left Outer Join 
				(
					Select 
						*
					From 
						SafeBags
					Where 
						[Type]            = 'P'
					And   
						[State]          <> 'C'
					And 
						IsNull(CashDrop, 0) = 0 
				) c
			On  
				c.PickupPeriodID   = a.OutPeriodID  
			And 
				c.AccountabilityID = a.AccountabilityID
			Left Outer Join
				RSCASH d
			On
				a.AccountabilityID = d.CASH				
	Where
		a.[Type]      = 'F'
	And   
		a.[State]     = 'R'
	And   
		a.OutPeriodID = @PeriodID 
	And   
		c.ID is null
	Union all
	--floated cashiers with pickup bags
	Select
		PickupID             = a.ID,
		PickupSealNumber     = a.SealNumber,
		PickupComment        = a.Comments,
		StartFloatID         = b.ID,
		StartFloatSealNumber = b.SealNumber,
		StartFloatValue      = b.Value,
		SaleTaken            = CAST(1 As Bit),       
		CashierID            = a.AccountabilityID,
		CashierUserName      = c.Name,
		CashierEmployeeCode  = c.EmployeeCode,
		LoggedOnTillId = d.TILL
	From 
		SafeBags a
			--starting float
			Inner Join 
				(
					Select 
						* 
					From 
						SafeBags 
					Where 
						[Type] = 'F' 
					And 
						[State] = 'R'
				) b
			On  
				b.OutPeriodID      = a.PickupPeriodID 
			And 
				b.AccountabilityID = a.AccountabilityID
				Inner Join 
					SystemUsers c
				On 
					c.ID = b.AccountabilityID
			Left Outer Join
				RSCASH d
			On
				a.AccountabilityID = d.CASH				
		Where 
			a.[Type]              = 'P'
		And   
			a.[State]            <> 'C'
		And   
			a.PickupPeriodID      = @PeriodID
		And   
			IsNull(a.CashDrop, 0) = 0
End
Go

If @@Error = 0
   Print 'Success: The stored procedure "NewBankingFloatedPickupList" has been sucessfully rolled back for P022-ITNO19-I'
Else
   Print 'Failure: The stored procedure "NewBankingFloatedPickupList" might NOT have been rolled back for P022-ITNO19-I'
Go
