-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Credit Card Summary Value for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION dbo.svf_ReportCardSummaryValueDAY( @InputDate date
                                                )
RETURNS numeric( 9 , 2
               )
AS
BEGIN

    DECLARE
       @StartDate date , 
       @CardValue numeric( 9 , 2
                         );

    SET @StartDate = @InputDate;    

                         
    ----------------------------------------------------------------------------------
    -- Get Card Summary Value - DAY
    ----------------------------------------------------------------------------------
    SELECT @CardValue = SUM( DP.AMNT * -1
                           )
      FROM
           DLTOTS AS DT INNER JOIN DLPAID AS DP
           ON DP.DATE1
              = 
              DT.DATE1
          AND DP.TILL
              = 
              DT.TILL
          AND DP.[TRAN]
              = 
              DT.[TRAN]
      WHERE DP.TYPE IN( 3 , 8 , 9
                      )
        AND DP.CARD
            <> 
            '0000000000000000000'
        AND DT.CASH <> '000'
        AND DT.VOID = 0
        AND DT.PARK = 0
        AND DT.TMOD = 0
        AND DT.Date1
            = 
            @StartDate;

    SET @CardValue = ISNULL( @CardValue , 0
                           );

    RETURN @CardValue;

END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportCardSummaryValueDAY" has been sucessfully rolled back for P022-041';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportCardSummaryValueDAY" might NOT have been rolled back for P022-041';
    END;