DROP FUNCTION dbo.udf_GetOpenReturns
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_GetOpenReturns" has been sucessfully rolled back for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_GetOpenReturns" has NOT been sucessfully rolled back for P022-041';
    END;


