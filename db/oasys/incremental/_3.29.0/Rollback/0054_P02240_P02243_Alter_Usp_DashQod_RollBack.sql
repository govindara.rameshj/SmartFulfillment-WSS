ALTER PROCEDURE [dbo].[DashQod]
AS
begin
    declare @table table(RowId         int,
                         [Description] varchar(50),
                         Qty           int,
                         Value         dec(9,2));
    declare @unconfirmedQty	  int,
            @unconfirmedValue dec(9,2),
            @confirmedQty     int,
            @confirmedValue	  dec(9,2),
            @pickingQty	      int,
            @pickingValue     dec(9,2),
            @despatchQty      int,
            @despatchValue    dec(9,2),
            @undeliveredQty	  int,
            @undeliveredValue dec(9,2),
            @deliveredQty     int,
            @deliveredValue	  dec(9,2),
            @completedQty     int,
            @completedValue	  dec(9,2),
            @suspendedQty     int,
            @suspendedValue	  dec(9,2)

    -- Unconfirmed Orders
    select @unconfirmedQty   = count(*),	
           @unconfirmedValue = coalesce(sum(value),0)
    from
        vwQod
    where
        DeliveryStatus < 300
		
    -- Confirmed Orders
    select
        @confirmedQty   = count(*),	
        @confirmedValue	= coalesce(SUM(value),0)
    from	
        vwQod
    where
        DeliveryStatus > 299
    and DeliveryStatus < 500
		
	-- Orders in Picking	
	select
		@pickingQty			= count(*),	
	 	@pickingValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus>=500
		and DeliveryStatus<600
		
	-- Orders in Despatch	
	select
		@despatchQty		= count(*),	
	 	@despatchValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus>=700
		and DeliveryStatus<800		

	-- Failed Deliveries
	select
		@undeliveredQty		= count(*),	
	 	@undeliveredValue	= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus>=800
		and DeliveryStatus<900
	
	-- Confirmed Deliveries
	select
		@deliveredQty		= count(*),	
	 	@deliveredValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus>=900
		and DeliveryStatus<999	
	
	-- Completed Deliveries
	select
		@completedQty		= count(*),	
	 	@completedValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus>=999
	
	-- Suspended Orders
	select
		@suspendedQty		= count(*),	
	 	@suspendedValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		IsSuspended=1
			
	-- Output the Dashboard Records
    insert into @table values (1, 'Unconfirmed Orders', @unconfirmedQty, @unconfirmedValue) ;
    insert into @table values (2, 'Confirmed Orders',   @confirmedQty,   @confirmedValue) ;
    insert into @table values (3, 'Orders in Picking',  @pickingQty,     @pickingValue) ;
    insert into @table values (4, 'Orders in Despatch', @despatchQty,    @despatchValue) ;
    insert into @table values (5, 'Undelivered',        @undeliveredQty, @undeliveredValue) ;
    insert into @table values (6, 'Delivered',          @deliveredQty,   @deliveredValue) ;
    insert into @table values (0, 'Completed',          @completedQty,   @completedValue) ;
    insert into @table values (0, 'Suspended',          @suspendedQty,   @suspendedValue) ;
	
    select * from @table

end
go

if @@error = 0
   print 'Success: Stored procedure DashQod for P022-040 has been successfully rolled back'
else
   print 'Failure: Stored procedure DashQod for P022-040 has NOT been successfully rolled back'
go




