Drop Function [dbo].[udf_GetThisDayLastYear]
Go

If @@Error = 0
   Print 'Success: The user defined function "udf_GetThisDayLastYear" has been sucessfully deleted for P022-044'
Else
   Print 'Failure: The user defined function "udf_GetThisDayLastYear" might NOT have been deleted for P022-044'
Go
