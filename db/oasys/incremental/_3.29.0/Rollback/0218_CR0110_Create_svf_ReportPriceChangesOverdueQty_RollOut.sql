-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 8th June 2011
-- Description	: Returns the figure to use for Price Changes OVERDUE Qty - Scalar Valued Function
-- Notes		: This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
CREATE FUNCTION [dbo].[svf_ReportPriceChangesOverdueQty]
(
)
RETURNS int
AS
BEGIN

	Declare			@PCToday	int
					
	----------------------------------------------------------------------------------
	-- Retrieve Count for Price Changes Outstanding
	----------------------------------------------------------------------------------
	Select			@PCToday		=		count(SKUN)
	From			PRCCHG 
	Where			PSTA			=		'U' 
					and PDAT		<		GetDate()

	Set				@PCToday		=		isnull(@PCToday, 0);
		
	RETURN			@PCToday

END
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The function "svf_ReportPriceChangesOverdueQty" for CR0110 has been sucessfully rolled back';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The function "svf_ReportPriceChangesOverdueQty" for CR0110 has NOT been rolled back';
    END;
GO
