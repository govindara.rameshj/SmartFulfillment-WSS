-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Open Returns Value - Scalar Valued Function
-- Notes		: This uses STKMAS:RETV value to calculate the items.
-- =================================================================================================
CREATE FUNCTION [dbo].[svf_ReportOpenReturnsValue]()
RETURNS numeric(9,2) 
AS
BEGIN

	Declare		@OpenReturnsValue		numeric(9,2)
		
	Set			@OpenReturnsValue		=	
										(
										Select	SUM(RETV)
										From	STKMAS
										Where	(RETV)	<>	0
										)
	
	RETURN		@OpenReturnsValue

END

GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportOpenReturnsValue" has been sucessfully created for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportOpenReturnsValue" might NOT have been created for P022-041';
    END;
GO
