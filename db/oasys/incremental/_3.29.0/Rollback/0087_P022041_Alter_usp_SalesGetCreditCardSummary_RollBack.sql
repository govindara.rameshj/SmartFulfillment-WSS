-- =============================================
-- Author:  Kevan Madelin
-- Create date: 09/06/2010
-- Description: This procedure is used to get the summary data needed for the credit card report
--
-- Update Author: Alanl
-- Update date: 03/08/2010
-- Description: Added tender type 9 - American Express to Summary information
--
-- Update Author: Kevan Madelin
-- Update date: 17/08/2010
-- Description: Added Union statement at Line 368 - American Express Section Missing on Vision Summary information

-- Update Author:Dhanesh Ramachandran
-- Update date: 01/10/2010
-- Description: Modifed to filter out Vision Sales Summary information
-- =============================================
ALTER PROCEDURE [dbo].[SalesGetCreditCardSummary]
 @Date Datetime
AS
BEGIN
 SET NOCOUNT ON

 select 'Credit/Debit Card Sales (Core)' as Description,
 COUNT(sdp.AMNT ) as Qty,
 ISNULL(SUM(sdp.AMNT)*-1,0) as Value
   from DLPAID as SDP
 join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and SDT.DATE1 = SDP.DATE1
where
 SDT.TCOD = 'SA' and SDT.TOTL >= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'SC' and SDT.TOTL > 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'RC' and SDT.TOTL >= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C-' and SDT.TOTL > 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
AND NOT (SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 )

union
	
select 'Credit/Debit Card Refunds (Core)' as Description,
 COUNT(sdp.AMNT ) as Qty,
 ISNULL(SUM(sdp.AMNT)*-1,0) as Value 
 from DLPAID as SDP
  join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN] and SDT.till = SDP.till and SDT.DATE1 = SDP.DATE1  where
 SDT.TCOD = 'RF' and SDT.TOTL <= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M+' and SDT.TOTL < 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C+' and SDT.TOTL >= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C-' and SDT.TOTL <= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
AND NOT (
 SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0)

union
	
select  'Credit/Debit Card Total (Core)' as Description,
 COUNT(sdp.AMNT ) as Qty,
 ISNULL(SUM(sdp.AMNT)*-1,0) as Value 
 from DLPAID as SDP
  join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and SDT.DATE1 = SDP.DATE1  where
 SDT.TCOD = 'SA' and SDT.TOTL >= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'SC' and SDT.TOTL > 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'RC'and SDT.TOTL < 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'RC' and SDT.TOTL >= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C-' and SDT.TOTL > 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'RF' and SDT.TOTL <= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M+' and SDT.TOTL < 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C+' and SDT.TOTL >= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C-' and SDT.TOTL <= 0.00 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
AND NOT (SDT.TCOD = 'M+' and SDT.TOTL >= 0.00  and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
  Or SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0)

union

select  'American Express Sales (Core)' as Description,
 COUNT(sdp.AMNT ) as Qty,
 ISNULL(SUM(sdp.AMNT)*-1,0) as Value 
 from DLPAID as SDP
  join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and SDT.DATE1 = SDP.DATE1  where
SDT.TCOD = 'SA' and SDT.TOTL >= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'SC' and SDT.TOTL > 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'RC' and SDT.TOTL >= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C-' and SDT.TOTL > 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
AND NOT (SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 )

union

select 'American Express Refunds (Core)' as Description,
 COUNT(sdp.AMNT ) as Qty,
 ISNULL(SUM(sdp.AMNT)*-1,0) as Value 
  from DLPAID as SDP
  join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN] and SDT.till = SDP.till and SDT.DATE1 = SDP.DATE1  where
SDT.TCOD = 'RF' and SDT.TOTL <= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M+' and SDT.TOTL < 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C+' and SDT.TOTL >= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C-' and SDT.TOTL <= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
AND NOT (
 SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0)
union

select  'American Express Total (Core)' as Description,
 COUNT(sdp.AMNT ) as Qty,
 ISNULL(SUM(sdp.AMNT)*-1,0) as Value 
  from DLPAID as SDP
  join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and SDT.DATE1 = SDP.DATE1  where
 SDT.TCOD = 'SA' and SDT.TOTL >= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'SC' and SDT.TOTL > 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'RC'and SDT.TOTL < 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'RC' and SDT.TOTL >= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C-' and SDT.TOTL > 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'RF' and SDT.TOTL <= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M+' and SDT.TOTL < 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C+' and SDT.TOTL >= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'C-' and SDT.TOTL <= 0.00 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
AND NOT (SDT.TCOD = 'M+' and SDT.TOTL >= 0.00  and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
  Or SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE]=9 and SDP.DATE1 = @Date  and SDP.ATYP <> '' and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0)


select 'Credit/Debit Card Refunds (Vision)' as Description,
        COUNT(VRefunds.Qty) as Qty,
         ISNULL(SUM(VRefunds.Qty)*-1,0) as Value   from 
(select sdp.AMNT  as Qty
  from DLPAID as SDP
join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and sdt.DATE1 = sdp.DATE1 where
(
 SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  
 and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),
(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 )
union all
(
select VP.ValueTender as Qty
 from VisionPayment as VP
where VP.TranDate = @Date and VP.TenderTypeId in('03','08') and VP.ValueTender >= 0.00))as VRefunds

union

select 'Credit/Debit Card Sale (Vision)' as Description,
        COUNT(VSales.Qty) as Qty,
         ISNULL(SUM(VSales.Qty)*-1,0) as Value   from 
(select sdp.AMNT  as Qty
  from DLPAID as SDP
join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and sdt.DATE1 = sdp.DATE1 where
(SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date 
and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))
And SDT.VOID = 0 and SDT.TMOD = 0 )
union all
(
select VP.ValueTender as Qty
 from VisionPayment as VP
where VP.TranDate = @Date and VP.TenderTypeId in('03','08') and VP.ValueTender < 0.00))as VSales

union

select 'Credit/Debit Card Total (Vision)' as Description,
        COUNT(VSalesTotal.Qty) as Qty,
         ISNULL(SUM(VSalesTotal.Qty)*-1,0) as Value   from 
(select sdp.AMNT  as Qty
  from DLPAID as SDP
join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and sdt.DATE1 = sdp.DATE1 where
(SDT.TCOD = 'M+' and SDT.TOTL >= 0.00  and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  
and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or
SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] in (3,8) and SDP.DATE1 = @Date  
and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 )
union all
(
select VP.ValueTender as Qty
 from VisionPayment as VP
where  VP.TranDate = @Date and VP.TenderTypeId in ('03','08')))as VSalesTotal

union

select 'American Express Refunds (Vision)' as Description,
        COUNT(VAmexRefunds.Qty) as Qty,
         ISNULL(SUM(VAmexRefunds.Qty)*-1,0) as Value   from 
(select sdp.AMNT  as Qty
  from DLPAID as SDP
join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and sdt.DATE1 = sdp.DATE1 where
(
 SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] = 9 and SDP.DATE1 = @Date  
  and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0)
union all
(
select VP.ValueTender as Qty
 from VisionPayment as VP
where  VP.TranDate = @Date and VP.TenderTypeId = '09' and VP.ValueTender >= 0.00))as VAmexRefunds


union

select 'American Express Sales (Vision)' as Description,
        COUNT(VAmexSales.Qty) as Qty,
         ISNULL(SUM(VAmexSales.Qty)*-1,0) as Value   from 
(select sdp.AMNT  as Qty
  from DLPAID as SDP
join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and sdt.DATE1 = sdp.DATE1 where
(SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] = 9 and SDP.DATE1 = @Date 
 and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0)
union all
(
select VP.ValueTender as Qty
 from VisionPayment as VP
where  VP.TranDate = @Date and VP.TenderTypeId = '09' and VP.ValueTender < 0.00))as VAmexSales


union

select 'American Express Total (Vision)' as Description,
        COUNT(VAmexTotal.Qty) as Qty,
         ISNULL(SUM(VAmexTotal.Qty)*-1,0) as Value   from 
(select sdp.AMNT  as Qty
  from DLPAID as SDP
join DLTOTS as SDT on SDP.[TRAN] = SDT.[TRAN]  and SDP.TILL = sdt.TILL  and sdt.DATE1 = sdp.DATE1 where
(SDT.TCOD = 'M+' and SDT.TOTL >= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] = 9 and SDP.DATE1 = @Date  
and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0 
Or SDT.TCOD = 'M-' and SDT.TOTL <= 0.00 and sdt.DOCN is not null and sdt.MISC = 20 and SDP.[TYPE] = 9 and SDP.DATE1 = @Date  
and SDP.[TYPE] not in ((select ACON from RETOPT),(select WION from RETOPT),(select CHON from RETOPT),(select SVON from RETOPT))And SDT.VOID = 0 and SDT.TMOD = 0)
union all
(
select VP.ValueTender as Qty
 from VisionPayment as VP
where  VP.TranDate = @Date and VP.TenderTypeId = '09'
))as VAmexTotal

END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The stored procedure "SalesGetCreditCardSummary" has been sucessfully rolled back for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The stored procedure "SalesGetCreditCardSummary" might NOT have been rolled back for P022-041';
    END;
GO