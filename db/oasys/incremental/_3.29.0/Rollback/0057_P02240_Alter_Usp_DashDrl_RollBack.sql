---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- ==========================================================================================================
-- Author		: Kevan Madelin
-- Alter Date	: 06/06/2011
-- Version		: 1.0.0.0
-- Notes		: Original version
-- ==========================================================================================================
-- Author		: Sean Moir
-- Alter Date	: 07/07/2011
-- Version		: 1.0.1.0
-- Notes		: DRL Value uses types 0 and 1 as positive counts and 2 and 3 as negative counts
-- ==========================================================================================================
ALTER PROCEDURE [dbo].[DashDrl]
	@DateEnd	date
as
begin
	declare @table table(
		Description	varchar(50),
		Qty			int,
		QtyWtd		int,
		Value		dec(9,2),
		ValueWtd	dec(9,2));
	declare
		@StartDate			date,
		@DrlDayQty			int,
		@DrlDayValue		dec(9,2),
		@DrlWeekQty			int,		
		@DrlWeekValue		dec(9,2),
		@ReceiptDayQty		int,
		@ReceiptDayValue	dec(9,2),		
		@ReceiptWeekQty		int,		
		@ReceiptWeekValue	dec(9,2);;
	
	--set startdate to sunday before enddate
	set	@StartDate = @DateEnd;
	While datepart(weekday, @StartDate) <> 1
	begin
		set @StartDate = dateadd(day, -1, @StartDate)
	end	
	
	--get drl value today
	select
		@DrlDayQty		= count(dl.valu),	
	 	@DrlDayValue	= (select sum(valu) from DRLSUM 
	 							where TYPE in ('0', '1') and DATE1	= @DateEnd)
	 						- (select sum(valu) from DRLSUM 
	 							where TYPE in ('2', '3') and DATE1	= @DateEnd)
	from	
		drlsum dl
	where	
		dl.DATE1	= @DateEnd
	
	--get drl value week
	select
		@DrlWeekQty		= count(dl.valu),	
	 	@DrlWeekValue	= (select sum(valu) from DRLSUM 
	 							where TYPE in ('0', '1') and DATE1 <= @DateEnd	and date1 >= @StartDate)
	 						- (select sum(valu) from DRLSUM 
	 							where TYPE in ('2', '3') and DATE1 <= @DateEnd and date1 >= @StartDate)
	from	
		drlsum dl
	where	
		dl.DATE1	<= @DateEnd
	and dl.date1	>= @StartDate
	
	--get receipt adjustment today
	select
		@ReceiptDayQty		= sum(sa.quan),	
	 	@ReceiptDayValue	= sum(sa.quan * sa.pric)
	from	
		stkadj sa
	where	
		sa.DATE1	= convert(date, @DateEnd)
		and sa.code	= '04'
	
	--get receipt adjustment week
	select
		@ReceiptWeekQty		= sum(sa.quan),	
	 	@ReceiptWeekValue	= sum(sa.quan * sa.pric)
	from	
		stkadj sa
	where	
		sa.DATE1		<= @DateEnd
		and sa.date1	>= @StartDate		
		and sa.code		= '04'

	--get rid of any nulls
	select
		@DrlDayQty			= coalesce(@DrlDayQty,0),
		@DrlWeekQty			= coalesce(@DrlWeekQty,0),
		@DrlDayValue		= coalesce(@DrlDayValue,0),
		@DrlWeekValue		= coalesce(@DrlWeekValue,0),
		@ReceiptDayQty		= coalesce(@ReceiptDayQty,0),
		@ReceiptWeekQty		= coalesce(@ReceiptWeekQty,0),
		@ReceiptDayValue	= coalesce(@ReceiptDayValue,0),
		@ReceiptWeekValue	= coalesce(@ReceiptWeekValue,0)


	--return values
	insert into @table values ('DRL Value',	@DrlDayQty, @DrlWeekQty, @DrlDayValue, @DrlWeekValue) ;
	insert into @table values ('Receipt Adjustments', @ReceiptDayQty, @ReceiptWeekQty, @ReceiptDayValue, @ReceiptWeekValue) ;

	select * from @table

end
go

if @@error = 0
   print 'Success: Stored procedure DashDrl for P022-040 has been successfully rolled back'
else
   print 'Failure: Stored procedure DashDrl for P022-040 has NOT been successfully rolled back'
go