Insert Into
	TOSTEN
Values
	('01', '09'),
	('01', '80'),
	('02', '80'),
	('03', '09'),
	('03', '80'),
	('04', '80'),
	('05', '80'),
	('08', '09'),
	('30', '80')
Go

If @@Error = 0
    Begin
        Print 'Success: The ''Old Tokens'' tender and ''Coupon'' tender buttons have been successfully re-enabled for CR0122';
    End
Else
    Begin
        Print 'Failure: The ''Old Tokens'' tender and ''Coupon'' tender buttons might NOT have been re-enable for CR0122';
    End;
Go
