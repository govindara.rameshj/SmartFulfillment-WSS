ALTER PROCEDURE [dbo].[ReturnsGetOpenReturns]
	@SupplierNumber		VARCHAR(5)	=Null,
	@Date				DATETIME	=Null
AS
BEGIN
	SET NOCOUNT ON;	

	SELECT 
		NUMB as 'Number'
		,SUPP as 'SupplierNumber'
		,EDAT as 'DateCreated'
		,RDAT as 'DateCollect'
		,DRLN as 'DrlNumber'
		,VALU as 'Value'
	FROM  RETHDR rh INNER JOIN SUPMAS sm
       ON rh.SUPP
          = 
          sm.SUPN
  WHERE rh.DRLN
        = 
        '000000'
    AND rh.isdeleted = 0
    AND (@SupplierNumber IS NULL
      OR SUPP
         = 
         @SupplierNumber)
  ORDER BY sm.NAME , rh.NUMB;
		
END
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The stored procedure "ReturnsGetOpenReturns" has been sucessfully rolled back for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The stored procedure "ReturnsGetOpenReturns" has NOT been sucessfully rolled back for P022-041';
    END;
GO



