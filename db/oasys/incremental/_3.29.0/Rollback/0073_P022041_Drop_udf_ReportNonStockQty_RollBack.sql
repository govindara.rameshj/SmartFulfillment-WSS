-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Non Stock Quantity - Scalar Valued Function
-- Notes		: This uses STKMAS:INON quantity to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 8th July 2011
-- Description	: Changed where clause from ONHA + MDNQ <> 0 to ONHA + MDNQ > 0
-- =================================================================================================
CREATE FUNCTION [dbo].[svf_ReportNonStockQty]()
RETURNS int
AS
BEGIN

	Declare		@NSQty		int
		
	Set			@NSQty		=	
									(
									Select	SUM(ONHA + MDNQ)
									From	STKMAS
									Where	(INON	=	1)
											and (ONHA + MDNQ) > 0
									)
	
	RETURN		@NSQty

END

GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportNonStockQty" has been sucessfully created for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportNonStockQty" might NOT have been created for P022-041';
    END;
GO
