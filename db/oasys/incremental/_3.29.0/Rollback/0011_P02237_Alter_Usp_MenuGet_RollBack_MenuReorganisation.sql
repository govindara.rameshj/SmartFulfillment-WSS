ALTER PROCEDURE [dbo].[MenuGet]
@MenuId INT=null
AS
BEGIN
	SET NOCOUNT ON;

	select
		ID				as Id,
		MasterID		as MasterId,
		AppName			as 'Description',
		AssemblyName,
		ClassName,
		MenuType		as 'LoadType',
		[Parameters],
		ImagePath,
		LoadMaximised	as IsMaximised,
		IsModal,
		DisplaySequence	as DisplayOrder
	from 
		MenuConfig
	where
		(@MenuID is null) or (@MenuId is not null and MasterId=@MenuId)

END
go

if @@error = 0
   print 'Success: The stored procedure (MenuGet) for PO22-037 has been successfully rolled back'
else
   print 'Failure: The stored procedure (MenuGet) for PO22-037 has NOT been successfully rolled back'
go