Update
	[Parameters]
Set
	StringValue = '3.29.0'
Where
	ParameterID = 0
Go

If @@Error = 0
   Print 'Success: The "Database Version No" has been sucessfully updated'
Else
   Print 'Failure: The "Database Version No" has NOT been updated'
Go
