alter procedure DashStock2
   @DateEnd date
as
begin
   set nocount on

   declare @Output table(RowId         int,
                         EmboldenThis  bit,
                         [Description] varchar(50),
                         Qty           varchar(20),
                         Value         dec(9, 2),
                         SelectedDate Date
                         )

	Declare			@StockHolding		dec(9,2),
					@MarkdownQty		int,		
					@MarkdownValue		dec(9,2),
					@DeletedQty			int,
					@DeletedValue		dec(9,2),
					@NonStockQty		int,
					@NonStockValue		dec(9,2),
					@ReturnsQty			int,
					@ReturnsValue		dec(9,2),
					@Sales				dec(9,2),
					@ReturnsPercentage	dec(9,2),
					@OutStockQty		int

	Set	@StockHolding		= dbo.svf_ReportStockHoldingValue()
	Set	@MarkdownQty		= dbo.svf_ReportMarkdownStockQty()
	Set	@MarkdownValue		= dbo.svf_ReportMarkdownStockValue()
	Set	@DeletedQty		= ((select count(*)from dbo.udf_GetDeletedOrNonStockWithOnHandStock('D')) - (dbo.udf_GetDeletedNonStockRecordCount()))
	Set	@DeletedValue		= (select sum(ValueDeleted) from dbo.udf_GetDeletedOrNonStockWithOnHandStock('D'))
	Set	@NonStockQty		= (select count(*)from dbo.udf_GetDeletedOrNonStockWithOnHandStock('N'))
	Set	@NonStockValue		= (select sum(ValueNonStock)from dbo.udf_GetDeletedOrNonStockWithOnHandStock('N'))	
	Set	@ReturnsQty		= (select count(*)from dbo.udf_GetOpenReturns(null,null))
	Set	@ReturnsValue		= (select sum(Value)from dbo.udf_GetOpenReturns(null,null))
	Set	@OutStockQty		= dbo.svf_ReportOutOfStocks()
	Set @Sales				= dbo.svf_ReportNETSalesValuePrevious7Days(@DateEnd)
	Set @ReturnsPercentage	= (@Sales)
	If @ReturnsPercentage <> 0 Set @ReturnsPercentage = (@ReturnsValue / @Sales) * 100


   insert into @Output(RowId, EmboldenThis, [Description], Qty, Value,SelectedDate) values (1, 1, 'GAP WALK',                           null,               null,@DateEnd)
   insert into @Output(RowId, EmboldenThis, [Description], Qty, Value,SelectedDate) values (2, 1, 'Number Items Out of Stock',          @OutStockQty,       null,@DateEnd)
   insert into @Output(RowId, EmboldenThis, [Description], Qty, Value,SelectedDate) values (3, 0, 'Markdown Stockholding',              @MarkdownQty,       @MarkdownValue,@DateEnd)
   insert into @Output(RowId, EmboldenThis, [Description], Qty, Value,SelectedDate) values (4, 0, 'Deleted Stockholding',               @DeletedQty,        @DeletedValue,@DateEnd)
   insert into @Output(RowId, EmboldenThis, [Description], Qty, Value,SelectedDate) values (5, 0, 'Non-Stock Stockholding',             @NonStockQty,       @NonStockValue,@DateEnd)
   insert into @Output(RowId, EmboldenThis, [Description], Qty, Value,SelectedDate) values (6, 0, 'Open Returns Stockholding',          @ReturnsQty,        @ReturnsValue,@DateEnd)
   insert into @Output(RowId, EmboldenThis, [Description], Qty, Value,SelectedDate) values (7, 0, 'Percentage Returns to Weekly Sales', @ReturnsPercentage, null,@DateEnd)
   insert into @Output(RowId, EmboldenThis, [Description], Qty, Value,SelectedDate) values (8, 0, 'Stockholding',                       null,               @StockHolding,@DateEnd)

   select * from @Output

end
go

if @@error = 0
   print 'Success: Stored procedure DashStock2 for P022-041, P022-040 & P022-042 has been successfully altered'
else
   print 'Failure: Stored procedure DashStock2 for P022-041, P022-040 & P022-042 has NOT been successfully altered'
go