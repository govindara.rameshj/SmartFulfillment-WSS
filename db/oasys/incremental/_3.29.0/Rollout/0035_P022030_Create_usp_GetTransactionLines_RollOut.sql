CREATE PROCEDURE usp_GetTransactionLines
@ReportDate AS Date,
@TillId As varchar(2),
@TranNo As varchar(4)
as
begin

SELECT DL.SKUN , 
       DL.PRIC , 
       DL.QUAN , 
       DL.EXTP , 
       IM.DESCR , 
       IM.INON , 
       IM.SALT , 
       DL.QBPD , 
       DL.DGPD , 
       DL.MBPD , 
       DL.HSPD , 
       DL.ESEV
  FROM
       DLLINE DL LEFT OUTER JOIN STKMAS IM
       ON IM.SKUN
          = 
          DL.SKUN
  WHERE DATE1
        = 
        @ReportDate
    AND TILL
        = 
        @TillId
    AND [TRAN]
        = 
        @TranNo
    AND QUAN < 0
    AND LREV = 0
  ORDER BY NUMB;
  
end
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The Create Stored Procedure usp_GetTransactionLines for P022-030 has been deployed successfully';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The Create Stored Procedure usp_GetTransactionLines for P022-030 has not been deployed successfully';
    END;