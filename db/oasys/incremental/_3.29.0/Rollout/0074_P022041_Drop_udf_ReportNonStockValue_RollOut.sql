DROP FUNCTION [dbo].[svf_ReportNonStockValue]
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "svf_ReportNonStockValue" has been sucessfully dropped for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "svf_ReportNonStockValue" has NOT been sucessfully dropped for P022-041';
    END;
GO
