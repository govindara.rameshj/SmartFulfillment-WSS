CREATE PROCEDURE usp_GetRefundListDetails @ReportDate AS date
AS
BEGIN

    SELECT CM.EmployeeCode , 
           CM.Name , 
           DT.TCOD , 
           DT.TOTL , 
           DT.RMAN , 
           DT.TILL , 
           DT.[TIME] , 
           DT.[TRAN] , 
           DT.SUPV , 
           DT.RSUP , 
           DT.RCAS RefundCashier , 
           RC.Name RefundCashierName , 
           SU.Name SupervisorName , 
           RM.Name ManagerName
      FROM
           SystemUsers CM INNER JOIN DLTOTS DT
           ON DT.CASH
              = 
              CM.EmployeeCode
          AND DT.DATE1
              = 
              @ReportDate
          AND DT.VOID = 0
          AND DT.TMOD = 0
          AND (DT.TCOD = 'SA'
            OR DT.TCOD = 'RF')
                          LEFT OUTER JOIN SystemUsers RC
           ON RC.EmployeeCode
              = 
              DT.RCAS
                          LEFT OUTER JOIN SystemUsers SU
           ON SU.EmployeeCode
              = 
              DT.RSUP
                          LEFT OUTER JOIN SystemUsers RM
           ON RM.EmployeeCode
              = 
              DT.RMAN
      ORDER BY CM.EmployeeCode , DT.[TIME] , DT.TILL , DT.[TRAN];

END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The Create Stored Procedure usp_GetRefundListDetails for P022-030 has been deployed successfully';
    END
ELSE
    BEGIN
        PRINT 'Failure: The Create Stored Procedure usp_GetRefundListDetails for P022-030 has not been deployed successfully';
    END;
GO
  
  