-- =============================================
-- Author        : Alan Lewis
-- Create date   : 08/11/2012
-- User Story	 : 8924: P022-ITN019-I
-- Referral		 : RF1031:  Cash Drop Cashiers Dropdown List
-- Task Id		 : 8985
-- Description   : Create active requirement switch
-- =============================================
Insert Into
	Parameters
	--	ParameterID, Description,                   StringValue, LongValue, BooleanValue, DecimalValue, ValueType
Values
	(	-1031,      'Enable requirement RF1031', Null,        Null,      1,            Null,         3)
Go

If @@Error = 0
   Print 'Success: The active requirement switch for RF1031 has been successfully deployed'
Else
   Print 'Failure: active requirement switch for RF1031 might NOT have been deployed'
Go
