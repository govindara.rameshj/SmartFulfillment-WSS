CREATE FUNCTION [dbo].[udf_GetDeletedNonStockRecordCount]
(
)
RETURNS int 
AS
BEGIN

	Declare		@NonStockQty		int
		
	Set			@NonStockQty		=	
									(
									Select COUNT(*)
									From	STKMAS st
									inner join HIEMAS hm 
									on st.CTGY=hm.NUMB and hm.LEVL=5
									Where										
									 st.ONHA <> 0									 
									 AND (st.IDEL=1 or st.IOBS=1 and st.INON = 0)                                  
                                                  
									 
									)
	
	RETURN		@NonStockQty

END
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_GetDeletedNonStockRecordCount" has been sucessfully created for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_GetDeletedNonStockRecordCount" might NOT have been created for P022-041';
    END;
