INSERT INTO Parameters( ParameterID , 
                        Description , 
                        StringValue , 
                        LongValue , 
                        BooleanValue , 
                        DecimalValue , 
                        ValueType
                      )
VALUES( 980110 , 
        'Enable Change Request CR00110' , 
        NULL , 
        NULL , 
        1 , 
        NULL , 
        3
      );
GO

IF @@error = 0
    BEGIN
        PRINT 'Success: The metadata change for CR00110 has been successfully deployed';
    END
ELSE
    BEGIN
        PRINT 'Failure: The metadata change for CR00110 has NOT been successfully deployed';
    END;
GO




