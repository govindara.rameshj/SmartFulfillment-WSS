-- =============================================
-- Author        : Alan Lewis
-- Update date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Modify to use new udf_CashierHasTakenSaleToday
--				 : to determine whether a cashier has taken a sale
--				 : for the banking period and filter out those that
--				 : have not.
-- =============================================
Alter Procedure [dbo].[NewBankingCashier]
	@PeriodID As Integer = Null
As
Begin
	Set NoCount On

	Select
		UserID = ID,
		Employee = EmployeeCode + ' - ' + Name
	From
		SystemUsers
	Where
		IsDeleted = 0	--remove delete cashiers
	And
		ID <= --filter out non operational users
			(
				Select 
					Cast(HCAS As Integer) 
				From 
					RETOPT
			)
	And   --filter out cashiers that have not taken a sale
		(
			[dbo].[udf_CashierHasTakenSaleOnDay](ID, @PeriodID) = 1
		Or
			@PeriodID Is Null
		)
End
Go

If @@Error = 0
   Print 'Success: The stored procedure "NewBankingCashier" has been sucessfully altered to use new function udf_CashierHasTakenSaleOnDay for P022-ITNO19-I'
Else
   Print 'Failure: The stored procedure "NewBankingCashier" might NOT have been altered to use new function udf_CashierHasTakenSaleOnDay for P022-ITNO19-I'
Go
