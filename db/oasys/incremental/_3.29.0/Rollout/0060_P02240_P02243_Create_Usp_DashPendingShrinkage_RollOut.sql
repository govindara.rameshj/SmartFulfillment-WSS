create procedure DashPendingShrinkage
as
begin
   set nocount on

   declare @Output table(RowId               int,
                         [Description]       varchar(50),
                         EmboldenThis        bit,
                         HighlightThis       bit,
                         TodayQuantity       int,
                         WeekToTodayQuantity int,
                         TodayValue          dec(9, 2),
                         WeekToTodayValue    dec(9, 2))

   declare @PendingDayQuantity int
   declare @PendingDayValue    dec(9, 2)

   set @PendingDayQuantity = dbo.svf_ReportPendingWriteOffQtyDAY()
   set @PendingDayValue    = dbo.svf_ReportPendingWriteOffValueDAY()

   insert into @Output values (1, 'Primary Count',      null, null, null,                null, null,             null)
   insert into @Output values (2, 'Pending Write Offs', null, null, @PendingDayQuantity, null, @PendingDayValue, null)

   update @Output set EmboldenThis  = case
                                         when RowId = 1 then 0
                                         when RowId = 2 and TodayQuantity > 0 then 1
                                         else 0
                                     end,
                     HighlightThis  = case
                                         when RowId = 1 then 0
                                         when RowId = 2 and TodayQuantity > 0 then 1
                                         else 0
                                     end

   select RowId,
          [Description],
          EmboldenThis,
          HighlightThis,
          [Today Qty]   = TodayQuantity,
          [WTD Qty]     = WeekToTodayQuantity,
          [Today Value] = TodayValue,
          [WTD Value]   = WeekToTodayValue
   from @Output

end
go

if @@error = 0
   print 'Success: Stored procedure DashPendingShrinkage for P022-040 has been successfully created'
else
   print 'Failure: Stored procedure DashPendingShrinkage for P022-040 has NOT been successfully created'
go

------------------------------------------------------------------------------------------------------------
grant execute on NewBankingSafeUpdate to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure DashPendingShrinkage for P022-040 has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure DashPendingShrinkage for P022-040 has NOT been successfully deployed'
go