ALTER PROCEDURE [dbo].[ReturnsGetOpenReturns]
	@SupplierNumber		VARCHAR(5)	=Null,
	@Date				DATETIME	=Null
AS
BEGIN
	SET NOCOUNT ON;

	select * from dbo.udf_GetOpenReturns(@SupplierNumber,@Date)
		
END
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The stored procedure "ReturnsGetOpenReturns" has been sucessfully altered to use new function udf_GetOpenReturns for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The stored procedure "ReturnsGetOpenReturns" might NOT have been altered to use new function udf_GetOpenReturns for P022-041';
    END;
GO



