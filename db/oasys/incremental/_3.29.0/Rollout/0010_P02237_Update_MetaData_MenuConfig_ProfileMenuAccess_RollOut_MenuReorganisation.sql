set nocount on

declare @Count int

------------------------------------------------------------------------------------------------------------------------
print 'Backup MenuConfig'

if exists (select * from sys.objects where object_id = object_id('MenuConfigBackupBeforeP022037') and type = 'U')
begin
   --backup table exists
   if (select count(*) from MenuConfig) = (select count(*) from MenuConfigBackupBeforeP022037)
   begin
      print 'Succesful backup already exists'
   end
   else
   begin
      print 'Unsuccessful backup already exists. Aborting rollout'
      raiserror('Script Failure', 15, 15)
      return
   end

end
else
begin
   --backup table does not exists
   select * into MenuConfigBackupBeforeP022037 from MenuConfig
   
   if (select count(*) from MenuConfig) = (select count(*) from MenuConfigBackupBeforeP022037)
   begin
      print 'Successful backup'
   end
   else
   begin
      print 'Unsuccessful backup. Aborting rollout'
      raiserror('Script Failure', 15, 15)
      return
   end
end

------------------------------------------------------------------------------------------------------------------------
print 'Backup ProfileMenuAccess'

if exists (select * from sys.objects where object_id = object_id('ProfileMenuAccessBackupBeforeP022037') and type = 'U')
begin
   --backup table exists
   if (select count(*) from ProfileMenuAccess) = (select count(*) from ProfileMenuAccessBackupBeforeP022037)
   begin
      print 'Succesful backup already exists'
   end
   else
   begin
      print 'Unsuccessful backup already exists. Aborting rollout'
      raiserror('Script Failure', 15, 15)
      return
   end

end
else
begin
   --backup table does not exists
   select * into ProfileMenuAccessBackupBeforeP022037 from ProfileMenuAccess
   
   if (select count(*) from ProfileMenuAccess) = (select count(*) from ProfileMenuAccessBackupBeforeP022037)
   begin
      print 'Successful backup'
   end
   else
   begin
      print 'Unsuccessful backup. Aborting rollout'
      raiserror('Script Failure', 15, 15)
      return
   end
end

------------------------------------------------------------------------------------------------------------------------
print 'Reorganising Menu'

begin try

   begin transaction

   --------------------------------------------------------------------------------------------------------------------------
   print 'Add Missing Menu Items If Required: Start'

   if not exists(select * from MenuConfig where ID = 12160)
      insert into MenuConfig values (12160, 12100, 'Product Coverage', 'wixsqcalcsql.exe',  null, 1, null, 'icons\icon_g.ico', 0, 1, 6, 0, 1, null, null, 0, 0, 0)

   if not exists(select * from MenuConfig where ID = 12170)
      insert into MenuConfig values (12170, 12100, 'Product Volume',   'wixvolcalcsql.exe', null, 1, null, 'icons\icon_g.ico', 0, 1, 7, 0, 1, null, null, 0, 0, 0)

   print 'Add Missing Menu Items If Required: End'

   --------------------------------------------------------------------------------------------------------------------------
   print 'Remove Menu Items'

   set @Count = 0

   delete from MenuConfig where ID = 2130; set @Count += @@rowcount
   delete from MenuConfig where ID = 2150; set @Count += @@rowcount
   delete from MenuConfig where ID = 3140; set @Count += @@rowcount
   delete from MenuConfig where ID = 3150; set @Count += @@rowcount
   delete from MenuConfig where ID = 3200; set @Count += @@rowcount
   delete from MenuConfig where ID = 3220; set @Count += @@rowcount
   delete from MenuConfig where ID = 4120; set @Count += @@rowcount
   delete from MenuConfig where ID = 4140; set @Count += @@rowcount
   delete from MenuConfig where ID = 4240; set @Count += @@rowcount
   delete from MenuConfig where ID = 4260; set @Count += @@rowcount
   delete from MenuConfig where ID = 7110; set @Count += @@rowcount
   delete from MenuConfig where ID = 7120; set @Count += @@rowcount
   delete from MenuConfig where ID = 7140; set @Count += @@rowcount
   delete from MenuConfig where ID = 7200; set @Count += @@rowcount
   delete from MenuConfig where ID = 7220; set @Count += @@rowcount
   delete from MenuConfig where ID = 7230; set @Count += @@rowcount
   delete from MenuConfig where ID = 7300; set @Count += @@rowcount
   delete from MenuConfig where ID = 7320; set @Count += @@rowcount
   delete from MenuConfig where ID = 7400; set @Count += @@rowcount
   delete from MenuConfig where ID = 10220; set @Count += @@rowcount
   delete from MenuConfig where ID = 11140; set @Count += @@rowcount
   delete from MenuConfig where ID = 11150; set @Count += @@rowcount
   delete from MenuConfig where ID = 11200; set @Count += @@rowcount
   delete from MenuConfig where ID = 11240; set @Count += @@rowcount
   delete from MenuConfig where ID = 11310; set @Count += @@rowcount
   delete from MenuConfig where ID = 11320; set @Count += @@rowcount
   delete from MenuConfig where ID = 11340; set @Count += @@rowcount
   delete from MenuConfig where ID = 11380; set @Count += @@rowcount
   delete from MenuConfig where ID = 11420; set @Count += @@rowcount
   delete from MenuConfig where ID = 11430; set @Count += @@rowcount
   delete from MenuConfig where ID = 11440; set @Count += @@rowcount
   delete from MenuConfig where ID = 11450; set @Count += @@rowcount
   delete from MenuConfig where ID = 11500; set @Count += @@rowcount
   delete from MenuConfig where ID = 11510; set @Count += @@rowcount
   delete from MenuConfig where ID = 11530; set @Count += @@rowcount
   delete from MenuConfig where ID = 11550; set @Count += @@rowcount
   delete from MenuConfig where ID = 11630; set @Count += @@rowcount
   delete from MenuConfig where ID = 11640; set @Count += @@rowcount
   delete from MenuConfig where ID = 11670; set @Count += @@rowcount
   delete from MenuConfig where ID = 11760; set @Count += @@rowcount
   delete from MenuConfig where ID = 11770; set @Count += @@rowcount
   delete from MenuConfig where ID = 11830; set @Count += @@rowcount
   delete from MenuConfig where ID = 11850; set @Count += @@rowcount

   if @Count = 43
   begin
      print 'Remove Menu Items: Success'  
   end
   else
   begin
      print 'Remove Menu Items: Failure'
      raiserror('Remove Menu Items: Failure', 15, 1)
   end

   --------------------------------------------------------------------------------------------------------------------------
   print 'Move Menu Items'

   set @Count = 0

   update MenuConfig set MasterID = 3100 where ID = 3210; set @Count += @@rowcount
   update MenuConfig set MasterID = 3100 where ID = 3330; set @Count += @@rowcount
   update MenuConfig set MasterID = 11300 where ID = 3350; set @Count += @@rowcount
   update MenuConfig set MasterID = 3100 where ID = 3360; set @Count += @@rowcount
   update MenuConfig set MasterID = 3100 where ID = 3390; set @Count += @@rowcount
   update MenuConfig set MasterID = 3100 where ID = 3395; set @Count += @@rowcount
   update MenuConfig set MasterID = 3100 where ID = 3396; set @Count += @@rowcount
   update MenuConfig set MasterID = 11900 where ID = 4130; set @Count += @@rowcount
   update MenuConfig set MasterID = 11900 where ID = 4230; set @Count += @@rowcount
   update MenuConfig set MasterID = 7100 where ID = 7210; set @Count += @@rowcount
   update MenuConfig set MasterID = 7100 where ID = 7310; set @Count += @@rowcount
   update MenuConfig set MasterID = 7100 where ID = 7330; set @Count += @@rowcount
   update MenuConfig set MasterID = 7100 where ID = 7410; set @Count += @@rowcount
   update MenuConfig set MasterID = 3300 where ID = 9055; set @Count += @@rowcount
   update MenuConfig set MasterID = 9000 where ID = 10230; set @Count += @@rowcount
   update MenuConfig set MasterID = 4200 where ID = 10290; set @Count += @@rowcount
   update MenuConfig set MasterID = 2100 where ID = 11210; set @Count += @@rowcount
   update MenuConfig set MasterID = 1000 where ID = 11220; set @Count += @@rowcount
   update MenuConfig set MasterID = 11300 where ID = 11230; set @Count += @@rowcount
   update MenuConfig set MasterID = 11100 where ID = 11360; set @Count += @@rowcount
   update MenuConfig set MasterID = 3300 where ID = 11370; set @Count += @@rowcount
   update MenuConfig set MasterID = 11300 where ID = 11520; set @Count += @@rowcount
   update MenuConfig set MasterID = 11300 where ID = 11540; set @Count += @@rowcount
   update MenuConfig set MasterID = 4200 where ID = 11660; set @Count += @@rowcount
   update MenuConfig set MasterID = 11100 where ID = 11715; set @Count += @@rowcount
   update MenuConfig set MasterID = 7100 where ID = 11725; set @Count += @@rowcount
   update MenuConfig set MasterID = 11100 where ID = 11730; set @Count += @@rowcount
   update MenuConfig set MasterID = 11100 where ID = 11745; set @Count += @@rowcount
   update MenuConfig set MasterID = 11100 where ID = 11750; set @Count += @@rowcount
   update MenuConfig set MasterID = 11100 where ID = 11755; set @Count += @@rowcount

   if @Count = 30
   begin
      print 'Move Menu Items: Success'  
   end
   else
   begin
      print 'Move Menu Items: Failure'
      raiserror('Move Menu Items: Failure', 15, 2)
   end

   --------------------------------------------------------------------------------------------------------------------------
   print 'Alter Menu Items Display Order'

   set @Count = 0

   update MenuConfig set DisplaySequence = 5 where ID = 1050; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 2035; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 2040; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 2050; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 2060; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 2100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 2110; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 2120; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 2140; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 3100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 3110; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 3120; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 9 where ID = 3130; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 3160; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 7 where ID = 3170; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 8 where ID = 3210; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 3300; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 7 where ID = 3310; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 3320; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 8 where ID = 3325; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 11 where ID = 3330; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 3340; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 3350; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 10 where ID = 3360; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 3370; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 3380; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 3390; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 3395; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 3396; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 3400; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 4100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 4110; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 4130; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 4150; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 4160; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 4170; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 4175; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 4180; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 4200; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 4210; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 4220; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 4230; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 4250; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 6100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 6110; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 6120; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 7080; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 7081; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 7082; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 7083; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 7084; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 7100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 7130; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 7150; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 7210; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 7 where ID = 7310; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 7330; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 7410; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 8030; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 8040; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 8050; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 9020; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 9030; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 9040; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 9050; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 9055; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 9060; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 9070; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 7 where ID = 9080; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 8 where ID = 9085; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 9 where ID = 9090; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 10100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 10200; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 10 where ID = 10230; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 10240; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 10241; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 10250; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 10270; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 10280; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 10285; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 10290; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 10300; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 11100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 11120; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 9 where ID = 11130; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 11160; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 7 where ID = 11170; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 11210; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 11220; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 11230; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 11300; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 11330; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 11350; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 8 where ID = 11360; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 11370; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 11400; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 11410; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 11460; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 11470; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 11480; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 7 where ID = 11520; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 11540; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 11600; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 11610; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 11620; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 11650; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 11660; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 11700; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 7 where ID = 11705; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 11710; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 11 where ID = 11715; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 11720; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 8 where ID = 11722; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 11725; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 10 where ID = 11730; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 11735; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 11740; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 11745; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 11750; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 11755; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 11800; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 11810; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 11820; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 11840; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 12010; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 12100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 12110; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 7 where ID = 12120; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 12130; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 12140; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 12150; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 12160; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 12170; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 13100; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 13110; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 13120; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 13130; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 13140; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 13150; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 13200; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 13210; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 13220; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 13240; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 13250; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 13260; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 13270; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 13300; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 13310; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 13320; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 13330; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 13400; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 13410; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 3 where ID = 13420; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 4 where ID = 13430; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 1 where ID = 13440; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 2 where ID = 13450; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 5 where ID = 13500; set @Count += @@rowcount
   update MenuConfig set DisplaySequence = 6 where ID = 13600; set @Count += @@rowcount

   if @Count = 158
   begin
      print 'Alter Menu Items Display Order: Success'  
   end
   else
   begin
      print 'Alter Menu Items Display Order: Failure'
      raiserror('Move Menu Items: Failure', 15, 3)
   end

   --------------------------------------------------------------------------------------------------------------------------
   print 'Rename Menu Items'

   set @Count = 0

   update MenuConfig set AppName = 'Enquiries and Stock Admin Reports' where ID = 7100; set @Count += @@rowcount
   update MenuConfig set AppName = 'Banking Reports' where ID = 11100; set @Count += @@rowcount
   update MenuConfig set AppName = 'Enquiries and Stock Admin Reports' where ID = 11300; set @Count += @@rowcount
   update MenuConfig set AppName = 'Purchase Order Reports' where ID = 11400; set @Count += @@rowcount
   update MenuConfig set AppName = 'Stock Management Reports' where ID = 11600; set @Count += @@rowcount
   update MenuConfig set AppName = 'Retail Reports' where ID = 11700; set @Count += @@rowcount
   update MenuConfig set AppName = 'QOD Reports' where ID = 11800; set @Count += @@rowcount

   if @Count = 7
   begin
      print 'Rename Menu Items: Success'  
   end
   else
   begin
      print 'Rename Menu Items: Failure'
      raiserror('Move Menu Items: Failure', 15, 4)
   end

   --------------------------------------------------------------------------------------------------------------------------
   --                            Custom entries below; not driven by "P022-037 Menu Reposition" spreadsheet
   --------------------------------------------------------------------------------------------------------------------------

   --------------------------------------------Reports->QOD Reports--------------------------------------------
   print 'Create Customer Orders Menu Item'

   set @Count = 0

   insert MenuConfig select 11860, 11800, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal, DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType
                     from MenuConfig where ID = 11210; set @Count += @@rowcount

   update MenuConfig set DisplaySequence = 1 where ID = 11860; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11860, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11800; set @Count += @@rowcount

   if @Count = 23
   begin
      print 'Create Customer Orders Menu Item: Success'  
   end
   else
   begin
      print 'Create Customer Orders Menu Item: Failure'
      raiserror('Create Customer Orders Menu Item: Failure', 15, 5)
   end

   --------------------------------------------Reports->Retail Reports--------------------------------------------
   print 'Create Colleague Discount Report Menu Item'

   set @Count = 0

   insert MenuConfig select 11780, 11700, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal, DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType
                     from MenuConfig where ID = 11370; set @Count += @@rowcount

   update MenuConfig set DisplaySequence = 1 where ID = 11780; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11780, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11700; set @Count += @@rowcount

   if @Count = 23
   begin
      print 'Create Colleague Discount Report Menu Item: Success'  
   end
   else
   begin
      print 'Create Colleague Discount Report Menu Item: Failure'
      raiserror('Create Colleague Discount Report Menu Item: Failure', 15, 6)
   end

   ----------------------------------------------------
   print 'Create Price Violations Report Menu Item'

   set @Count = 0

   insert MenuConfig select 11790, 11700, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal, DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType
                     from MenuConfig where ID = 9055; set @Count += @@rowcount

   update MenuConfig set DisplaySequence = 5 where ID = 11790; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11790, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11700; set @Count += @@rowcount

   if @Count = 23
   begin
      print 'Create Price Violations Report Menu Item: Success'  
   end
   else
   begin
      print 'Create Price Violations Report Menu Item: Failure'
      raiserror('Create Price Violations Report Menu Item: Failure', 15, 7)
   end

   --------------------------------------------Reports->Purchase Order Reports--------------------------------------------
   print 'Create DRL Print Menu Item'

   set @Count = 0

   insert MenuConfig select 11490, 11400, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal, DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType
                     from MenuConfig where ID = 4170; set @Count += @@rowcount

   update MenuConfig set DisplaySequence = 2 where ID = 11490; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11490, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11400; set @Count += @@rowcount

   if @Count = 23
   begin
      print 'Create DRL Print Menu Item: Success'  
   end
   else
   begin
      print 'Create DRL Print Menu Item: Failure'
      raiserror('Create DRL Print Menu Item: Failure', 15, 8)
   end

   ----------------------------------------------------
   print 'Create Order Confirmation Menu Item'

   set @Count = 0

   insert MenuConfig select 11495, 11400, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal, DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType
                     from MenuConfig where ID = 4180; set @Count += @@rowcount

   update MenuConfig set DisplaySequence = 4 where ID = 11495; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11495, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11400; set @Count += @@rowcount

   if @Count = 23
   begin
      print 'Create Order Confirmation Menu Item: Success'  
   end
   else
   begin
      print 'Create Order Confirmation Menu Item: Failure'
      raiserror('Create Order Confirmation Menu Item: Failure', 15, 9)
   end

   --------------------------------------------Reports->Stock Management Reports--------------------------------------------
   print 'Create GapWalk Clear Menu Item'

   set @Count = 0

   insert MenuConfig select 11680, 11600, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal, DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType
                     from MenuConfig where ID = 10290; set @Count += @@rowcount

   update MenuConfig set DisplaySequence = 2 where ID = 11680; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11680, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11600; set @Count += @@rowcount

   if @Count = 23
   begin
      print 'Create GapWalk Clear Menu Item: Success'  
   end
   else
   begin
      print 'Create GapWalk Clear Menu Item: Failure'
      raiserror('Create GapWalk Clear Menu Item: Failure', 15, 10)
   end

   ----------------------------------------------------
   print 'Create Markdowns Report Menu Item'

   set @Count = 0

   insert MenuConfig select 11690, 11600, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal, DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType
                     from MenuConfig where ID = 11660; set @Count += @@rowcount

   update MenuConfig set DisplaySequence = 3 where ID = 11690; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11690, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11600; set @Count += @@rowcount

   if @Count = 23
   begin
      print 'Create Markdowns Report Menu Item: Success'  
   end
   else
   begin
      print 'Create Markdowns Report Menu Item: Failure'
      raiserror('Create Markdowns Report Menu Item: Failure', 15, 11)
   end

   --------------------------------------------Enquiries and Stock Admin Reports--------------------------------------------
   print 'Create Audit Roll Enquiry Menu Item'

   set @Count = 0

   insert MenuConfig select 11390, 11300, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal, DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType
                     from MenuConfig where ID = 7410; set @Count += @@rowcount

   update MenuConfig set DisplaySequence = 1 where ID = 11390; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11390, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11300; set @Count += @@rowcount

   if @Count = 23
   begin
      print 'Create Audit Roll Enquiry Menu Item: Success'  
   end
   else
   begin
      print 'Create Audit Roll Enquiry Menu Item: Failure'
      raiserror('Create Audit Roll Enquiry Menu Item: Failure', 15, 12)
   end

   --------------------------------------------------------------------------------------------------------------------------
   print 'Create Return Reports Sub Menu Item'

   set @Count = 0

   insert MenuConfig (ID, MasterID, AppName, AssemblyName, ClassName, MenuType, Parameters, ImagePath, LoadMaximised, IsModal,
                      DisplaySequence, AllowMultiple, WaitForExit, TabName, [Description], ImageKey, [Timeout], DoProcessingType)
              values (11900, 11000, 'Return Reports', null, null, 3, null, 'icons\icon_g.ico', 0, 1, 7, 0, 0, null, null, 0, 0, 0); set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11900, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11000; set @Count += @@rowcount

   if @Count = 22
   begin
      print 'Create Return Reports Sub Menu Item: Success'  
   end
   else
   begin
      print 'Create Return Reports Sub Menu Item: Failure'
      raiserror('Create Return Reports Sub Menu Item: Failure', 15, 13)
   end

   --------------------------------------------------------------------------------------------------------------------------
   print 'Create Vision Sales Report Security'

   set @Count = 0

   insert ProfilemenuAccess select ID, 3325, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 3300; set @Count += @@rowcount

   insert ProfilemenuAccess select ID, 11722, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters
                            from ProfilemenuAccess where MenuConfigID = 11700; set @Count += @@rowcount

   if @Count = 43
   begin
      print 'Create Vision Sales Report Security: Success'  
   end
   else
   begin
      print 'Create Vision Sales Report Security: Failure'
      raiserror('Create Vision Sales Report Security: Failure', 15, 14)
   end

   --------------------------------------------------------------------------------------------------------------------------
   print 'Configure Security - Customer Orders'

   delete from ProfilemenuAccess where MenuConfigID = 11210

   insert ProfilemenuAccess (ID, MenuConfigID, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters)
                      select ID, 11210, 1, 0,
                             case 
                                when ID > 100 then ID - 100
                                when ID < 100 then  1
                             end,
                             0, 0, null, null
                      from SecurityProfile

   print 'Configure Security - Customer Orders: Success'

   --------------------------------------------------------------------------------------------------------------------------
   print 'Configure Security - Label Maintenance'

   delete from ProfilemenuAccess where MenuConfigID = 10230

   insert ProfilemenuAccess (ID, MenuConfigID, AccessAllowed, OverrideAllowed, SecurityLevel, IsManager, IsSupervisor, LastEdited, Parameters)
                      select ID, 10230, 1, 0,
                             case 
                                when ID > 100 then ID - 100
                                when ID < 100 then  1
                             end,
                             0, 0, null, null
                      from SecurityProfile

   print 'Configure Security - Label Maintenance: Success'

   commit transaction
   print 'Reorganising Menu - Successful'

end try

begin catch

   rollback transaction
   print 'Reorganising Menu - Unsuccessful'
   print 'Aborting rollout'
   raiserror('Script Failure', 15, 15)
   return

end catch

print 'Rollout completed successfully'