CREATE FUNCTION dbo.udf_PriceChangeReportOverdueQuantity(
                                                        )
RETURNS int
AS
BEGIN

    DECLARE
       @Quantity int;

    SELECT @Quantity = count (*)
      FROM
           PRCCHG prc INNER JOIN STKMAS stk
           ON prc.SKUN
              = 
              stk.SKUN
                      INNER JOIN evtchg ec
           ON ec.SKUN
              = 
              prc.SKUN
          AND ec.NUMB
              = 
              prc.EVNT
          AND ec.PRIO
              = 
              prc.PRIO
      WHERE prc.PSTA = 'U'
        AND ec.SDAT
            < 
            ( 
              SELECT tmdt
                FROM sysdat
            );

    SET @Quantity = ISNULL( @Quantity , 0
                          );

    RETURN @Quantity;

END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The function "udf_PriceChangeReportOverdueQuantity" for CR0110 has been sucessfully deployed';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The function "udf_PriceChangeReportOverdueQuantity" for CR0110 has NOT been deployed';
    END;

