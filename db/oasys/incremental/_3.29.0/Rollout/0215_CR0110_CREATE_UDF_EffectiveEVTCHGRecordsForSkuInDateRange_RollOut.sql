CREATE FUNCTION [dbo].[udf_EffectiveEVTCHGRecordsForSkuInDateRange]
	(
	@sku char(6),
	@startDate date,
	@windowLengthInDays int = 7
	)
RETURNS @Results TABLE
(
	[ActiveDate] date,
	[SKUN] [char](6) NOT NULL,
	[SDAT] [date] NOT NULL,
	[PRIO] [char](2) NOT NULL,
	[NUMB] [char](6) NOT NULL,
	[EDAT] [date] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[IDEL] [bit] NOT NULL
)
AS


BEGIN
	declare @interimResults TABLE
	(
		[ActiveDate] date,
		[SKUN] [char](6) NOT NULL,
		[SDAT] [date] NOT NULL,
		[PRIO] [char](2) NOT NULL,
		[NUMB] [char](6) NOT NULL,
		[EDAT] [date] NULL,
		[PRIC] [decimal](9, 2) NULL,
		[IDEL] [bit] NOT NULL

		unique clustered ([SKUN], [ActiveDate])
		
	)

	set @startDate = dateadd(DAY, 1, @startDate)
	
	declare @endDate date = dateadd(DAY, @windowLengthInDays, @startDate)
	declare @datePointer date = @startDate
	declare @skun [char](6),
			@sdat [date],
			@PRIO [char](2),
		    @NUMB [char](6),
		    @EDAT [date],
			@PRIC [decimal](9, 2),
		    @IDEL [bit]
	
		    
	while @datePointer < @endDate	
	
	begin
			insert into @interimResults --( [SKUN], [SDAT], [PRIO], [NUMB], [EDAT], [PRIC], [IDEL], [SPARE])

			select @datePointer, new.[SKUN], new.[SDAT], new.[PRIO], new.[NUMB], new.[EDAT], new.[PRIC], new.[IDEL]
			from dbo.[udf_EffectiveEVTCHGRecordForSkuOnDate] (@sku, @datePointer) new
			
			select @skun = new.[SKUN], @sdat = new.[SDAT], @PRIO = new.[PRIO], @NUMB = new.[NUMB], 
     			   @EDAT = NULLIF(new.[EDAT], ''), @PRIC = new.[PRIC], @IDEL = new.[IDEL]
			from dbo.[udf_EffectiveEVTCHGRecordForSkuOnDate] (@sku, @datePointer) new
		
		set @datePointer = DATEADD(day, 1, @datePointer)	
		
	end	
	
	
	
	   insert into @Results
   select z.[ActiveDate], z.[SKUN], z.[ActiveDate], z.[PRIO], z.[NUMB], z.[EDAT], z.[PRIC], z.[IDEL]
   from (
         select [ActiveDate], [SKUN]
         from @interimResults
         group by [ActiveDate], [SKUN]
        ) y
   left outer join (
                    select [ActiveDate], [SKUN], [Activedate] as [SDAT] , [PRIO], [NUMB], [EDAT], [PRIC], [IDEL],
                           PreviousPrice = (select Pric 
                                            from @interimResults
                                            where SKUN       =   a.SKUN
                                            and   ActiveDate = dateadd(day, -1, a.ActiveDate))
                    from @interimResults a
                   ) z
             on  z.ActiveDate = y.ActiveDate 
             and z.SKUN       = y.SKUN 
             and z.PRIC       <> isnull(z.PreviousPrice, 0)
   where z.ActiveDate is not null
   order by y.SKUN, y.ActiveDate		

   return
END

GO

grant select on dbo.[udf_EffectiveEVTCHGRecordsForSkuInDateRange] to [NT AUTHORITY\IUSR]
grant select on dbo.[udf_EffectiveEVTCHGRecordsForSkuInDateRange] to [role_execproc]

go

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_EffectiveEVTCHGRecordsForSkuInDateRange" has been sucessfully created';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_EffectiveEVTCHGRecordsForSkuInDateRange" might NOT have been created';
    END
GO