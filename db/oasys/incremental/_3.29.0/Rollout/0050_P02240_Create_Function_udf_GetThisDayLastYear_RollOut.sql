CREATE FUNCTION udf_GetThisDayLastYear 
(
	@DateIn datetime
)
RETURNS datetime
AS
BEGIN
	RETURN DATEADD(week, -52, @DateIn)
END
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_GetThisDayLastYear" has been sucessfully altered for P022-044';
    END;
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_GetThisDayLastYear" might NOT have been altered for P022-044';
    END;
GO