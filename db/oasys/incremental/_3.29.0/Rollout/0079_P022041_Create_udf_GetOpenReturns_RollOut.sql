CREATE FUNCTION dbo.udf_GetOpenReturns
(
@SupplierNumber		VARCHAR(5)	=Null,
@Date				DATETIME	=Null
)
	
RETURNS @OutputTable TABLE(
                            Number char(6) NULL ,                             
                            SupplierNumber char(5)NULL , 
                            SupplierName char(30)NULL , 
                            DateCreated date NULL , 
                            DateCollect date NULL , 
                            DrlNumber char(6) NULL,
                            Value decimal(9,2)NULL                             
                          )
AS
BEGIN

    INSERT INTO @OutputTable
SELECT NUMB , 
       SUPP , 
       sm.NAME , 
       EDAT , 
       RDAT , 
       DRLN , 
       VALU
  FROM
       RETHDR rh INNER JOIN SUPMAS sm
       ON rh.SUPP
          = 
          sm.SUPN
  WHERE rh.DRLN
        = 
        '000000'
    AND rh.isdeleted = 0
    AND (@SupplierNumber IS NULL
      OR SUPP
         = 
         @SupplierNumber)
  ORDER BY sm.NAME , rh.NUMB;
RETURN;

END;

GO

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "udf_GetOpenReturns" has been sucessfully created for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "udf_GetOpenReturns" might NOT have been created for P022-041';
    END;
GO



