
SET NOCOUNT ON;
------------------------------------------------------------------------------------------------------------------------
PRINT 'Set Hyperlinks for Shrinkage';

BEGIN TRY

    BEGIN TRANSACTION;

    --------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for Price Voilations - Start';

    INSERT INTO ReportColumn( Id , 
                              Name , 
                              Caption , 
                              FormatType , 
                              Format , 
                              IsImagePath , 
                              MinWidth , 
                              MaxWidth , 
                              Alignment
                            )
    VALUES( 42506 , 
            'SelectedDate' , 
            'SelectedDate' , 
            0 , 
            NULL , 
            0 , 
            NULL , 
            NULL , 
            NULL
          );
    INSERT INTO ReportColumns( ReportId , 
                               TableId , 
                               ColumnId , 
                               Sequence , 
                               IsVisible , 
                               IsBold , 
                               IsHighlight , 
                               HighlightColour , 
                               Fontsize
                             )
    VALUES( 4 , 
            1 , 
            42506 , 
            6 , 
            0 , 
            0 , 
            NULL , 
            NULL , 
            NULL
          );

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 4 , 
            1 , 
            5 , 
            'Description' , 
            'ReportId=304|[SelectedDate]'
          );


    PRINT 'Set Hyperlinks for Price Voilations - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Set Hyperlinks for Incorrect Day Price - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 4 , 
            1 , 
            6 , 
            'Description' , 
            'ReportId=308|[SelectedDate]|[SelectedDate]'
          );

    PRINT 'Set Hyperlinks for Incorrect Day Price - End';
   
    --------------------------------------------------------------------------------------------------------------------------
   
    --------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for Temporary Deal Group - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 4 , 
            1 , 
            7 , 
            'Description' , 
            'ReportId=309|[SelectedDate]|[SelectedDate]'
          );

    PRINT 'Set Hyperlinks for Temporary Deal Group - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Set Hyperlinks for Stock Loss - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 4 , 
            1 , 
            1 , 
            'Description' , 
            'MenuId=7030'
          );

    PRINT 'Set Hyperlinks for Stock Loss - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Set Hyperlinks for Known Theft - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 4 , 
            1 , 
            2 , 
            'Description' , 
            'MenuId=7030'
          );

    PRINT 'Set Hyperlinks for Known Theft - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Set Hyperlinks for Write Off Adjustments - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 4 , 
            1 , 
            3 , 
            'Description' , 
            'MenuId=7030'
          );

    PRINT 'Set Hyperlinks for Write Off Adjustments - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Set Hyperlinks for Markdowns - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 4 , 
            1 , 
            4 , 
            'Description' , 
            'MenuId=7030'
          );

    PRINT 'Set Hyperlinks for Markdowns - End';
   
    --------------------------------------------------------------------------------------------------------------------------

    PRINT 'Set Hyperlinks for Shrinkage - End'; 
   
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for DRL Analysis - Start';

    PRINT 'Set Hyperlinks for Receipt Adjustment - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 2 , 
            1 , 
            2 , 
            'Description' , 
            'MenuId=7030'
          );

    PRINT 'Set Hyperlinks for Receipt Adjustment - End';
   
    ------------------------------------------------------------------------------------------------------------------------

    PRINT 'Set Hyperlinks for Outstanding Direct Deliveries - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 2 , 
            1 , 
            3 , 
            'Description' , 
            'MenuId=4030'
          );

    PRINT 'Set Hyperlinks for Outstanding Direct Deliveries - End';

    PRINT 'Set Hyperlinks for DRL Analysis -End ';     


    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for Stock Reports - Start';

    INSERT INTO ReportColumns( ReportId , 
                               TableId , 
                               ColumnId , 
                               Sequence , 
                               IsVisible , 
                               IsBold , 
                               IsHighlight , 
                               HighlightColour , 
                               Fontsize
                             )
    VALUES( 7 , 
            1 , 
            42506 , 
            6 , 
            0 , 
            0 , 
            NULL , 
            NULL , 
            NULL
          );


    PRINT 'Set Hyperlinks for GapWalk Report - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 7 , 
            1 , 
            1 , 
            'Description' , 
            'ReportId=203|[SelectedDate]'
          );

    PRINT 'Set Hyperlinks for GapWalk Report - End';
   
    ------------------------------------------------------------------------------------------------------------------------

    PRINT 'Set Hyperlinks for Number Items Out of Stock - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 7 , 
            1 , 
            2 , 
            'Description' , 
            'ReportId=100'
          );

    PRINT 'Set Hyperlinks for Number Items Out of Stock - End';
   
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for Markdown Stockholding - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 7 , 
            1 , 
            3 , 
            'Description' , 
            'ReportId=206'
          );

    PRINT 'Set Hyperlinks for Markdown Stockholding - End';
    
    ------------------------------------------------------------------------------------------------------------------------
/*    PRINT 'Set Hyperlinks for Open Returns Stockholding - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 7 , 
            1 , 
            6 , 
            'Description' , 
            'MenuId=6120|All'
          );

    PRINT 'Set Hyperlinks for Open Returns Stockholding - End';
*/    
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for Deleted Stockholding - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 7 , 
            1 , 
            4 , 
            'Description' , 
            'ReportId=205'
          );

    PRINT 'Set Hyperlinks for Deleted Stockholding - End';
   
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for Non-Stock Stockholding - Start';

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 7 , 
            1 , 
            5 , 
            'Description' , 
            'ReportId=205'
          );

    PRINT 'Set Hyperlinks for  Non-Stock  Stockholding - End';

    PRINT 'Set Hyperlinks for Stock Reports -End ';     


/*
    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for Banking - Start';

    INSERT INTO ReportColumns( ReportId , 
                               TableId , 
                               ColumnId , 
                               Sequence , 
                               IsVisible , 
                               IsBold , 
                               IsHighlight , 
                               HighlightColour , 
                               Fontsize
                             )
    VALUES( 6 , 
            1 , 
            42506 , 
            6 , 
            0 , 
            0 , 
            NULL , 
            NULL , 
            NULL
          );
    INSERT INTO Reportparameters( ReportId , 
                                  ParameterId , 
                                  Sequence , 
                                  AllowMultiple , 
                                  DefaultValue
                                )
    VALUES( 6 , 
            103 , 
            1 , 
            0 , 
            NULL
          );
    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 6 , 
            1 , 
            3 , 
            'Description' , 
            'ReportId=611|[SelectedDate]|[SelectedDate]'
          );
    PRINT 'Set Hyperlinks for Banking - End';   

*/

    ------------------------------------------------------------------------------------------------------------------------
    PRINT 'Set Hyperlinks for Banking Reports - Start';

    INSERT INTO ReportColumns( ReportId , 
                               TableId , 
                               ColumnId , 
                               Sequence , 
                               IsVisible , 
                               IsBold , 
                               IsHighlight , 
                               HighlightColour , 
                               Fontsize
                             )
    VALUES( 9 , 
            1 , 
            42506 , 
            6 , 
            0 , 
            0 , 
            NULL , 
            NULL , 
            NULL
          );
    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 9 , 
            1 , 
            1 , 
            'Description' , 
            'ReportId=305|[SelectedDate]'
          );

    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 9 , 
            1 , 
            3 , 
            'Description' , 
            'ReportId=306'
          );
    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 9 , 
            1 , 
            4 , 
            'Description' , 
            'ReportId=306'
          );
    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 9 , 
            1 , 
            5 , 
            'Description' , 
            'ReportId=610|[SelectedDate]|[SelectedDate]'
          );
    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 9 , 
            1 , 
            6 , 
            'Description' , 
            'ReportId=610|[SelectedDate]|[SelectedDate]'
          );
    PRINT 'Set Hyperlinks for Banking Reports - End';

    PRINT 'Set Hyperlinks for Price Changes - Start';
    INSERT INTO ReportHyperlink( ReportId , 
                                 TableId , 
                                 RowId , 
                                 ColumnName , 
                                 Value
                               )
    VALUES( 8 , 
            1 , 
            6 , 
            'Description' , 
            'VB Label Request Audit'
          );
    PRINT 'Set Hyperlinks for Price Changes - End';


    PRINT 'Set Hyperlinks for Pending Orders - Start';

    UPDATE reporthyperlink
    SET value = 'saleordermaintain.exe'
      WHERE ReportId = 10;

    PRINT 'Set Hyperlinks for Pending Orders - End';


    COMMIT TRANSACTION;
    PRINT 'HyperLink Reports in Dashboard: Successful';
END TRY
BEGIN CATCH

    ROLLBACK TRANSACTION;
    PRINT 'HyperLink Reports in Dashboard: Unsuccessful';
    PRINT 'Rollout unsuccessful';
    RETURN;
END CATCH;

PRINT 'Rollout successful';