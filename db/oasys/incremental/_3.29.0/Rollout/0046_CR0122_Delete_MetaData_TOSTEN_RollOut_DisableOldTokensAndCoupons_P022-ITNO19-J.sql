Delete
	TOSTEN
Where
	TEND In ('09', '80')
Go      

If @@Error = 0
    Begin
        Print 'Success: The ''Old Tokens'' tender and ''Coupon'' tender buttons have been successfully disabled for CR0122';
    End
Else
    Begin
        Print 'Failure: The ''Old Tokens'' tender and ''Coupon'' tender buttons might NOT have been disable for CR0122';
    End;
Go
