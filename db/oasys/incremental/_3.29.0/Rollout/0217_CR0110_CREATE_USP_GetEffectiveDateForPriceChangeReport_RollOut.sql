Create PROCEDURE [dbo].[usp_GetEffectiveDateForPriceChangeReport]
	@SkuNumber varchar(6),
	@EventNumber varchar(6),
	@Priority varchar(2)
AS
BEGIN

    SELECT 'StartDate' = dbo.udf_GetLatestPriceChangeDateForSKU(@SkuNumber)

END

go

IF @@Error = 0
    BEGIN
        PRINT 'Success: The user defined function "usp_GetEffectiveDateForPriceChangeReport" has been sucessfully created';
    END
ELSE
    BEGIN
        PRINT 'Failure: The user defined function "usp_GetEffectiveDateForPriceChangeReport" might NOT have been created';
    END
GO
