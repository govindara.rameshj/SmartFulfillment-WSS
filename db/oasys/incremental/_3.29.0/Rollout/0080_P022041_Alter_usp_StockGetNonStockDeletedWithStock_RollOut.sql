
ALTER PROCEDURE dbo.StockGetNonStockDeletedWithStock @Status char( 1
                                                                 ) = 'A'
AS
BEGIN
    SET NOCOUNT ON;
    SELECT *
      FROM dbo.udf_GetDeletedOrNonStockWithOnHandStock( @Status
                                                      );
END;
GO
IF @@Error = 0
    BEGIN
        PRINT 'Success: The stored procedure "StockGetNonStockDeletedWithStock" has been sucessfully altered to use new function udf_GetDeletedOrNonStockWithOnHandStock for P022-041';
    END
ELSE
    BEGIN
        PRINT 'Failure: The stored procedure "StockGetNonStockDeletedWithStock" might NOT have been altered to use new function udf_GetDeletedOrNonStockWithOnHandStock for P022-041';
    END;
GO



