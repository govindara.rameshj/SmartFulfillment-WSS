IF EXISTS (select * from dbo.[Parameters] where ParameterID = 920 AND [Description] = 'Use AddressLookup Service')
    BEGIN
        PRINT 'Change Description of Parameter with Id = 920'
        UPDATE dbo.[Parameters]
        SET 
        [Description] = 'Capscan in Use'
        WHERE ParameterID = 920
    END
GO

If @@Error = 0
   Print 'Success: Update into table dbo.[Parameters] for US16788 has been deployed successfully'
Else
   Print 'Failure: Update into table dbo.[Parameters] for US16788 has not been deployed successfully'
GO