IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'StockLogInsertCheck91') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure StockLogInsertCheck91'
	EXEC ('CREATE PROCEDURE dbo.StockLogInsertCheck91 AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure StockLogInsertCheck91')
GO

ALTER PROCEDURE [dbo].[StockLogInsertCheck91]
@SkuNumber CHAR (6), @StockStart INT, @UserId INT
AS
BEGIN
	SET NOCOUNT ON;

	declare 
		@stockEnd int,
		@markdownEnd int,
		@writeoffEnd int,
		@returnsEnd int,
		@priceEnd dec(9,2);
		
	--check that last log qty = last qty for sku
	select top 1
		@stockEnd = ESTK,
		@markdownEnd = EMDN,
		@writeoffEnd = EWTF,
		@returnsEnd = ERET,
		@priceEnd = EPRI
	from 
		STKLOG where SKUN=@SkuNumber 
	order by 
		TKEY desc;
	
	--insert adjustment if needed
    if @stockEnd is not null
    begin
		if @stockEnd <> @StockStart
		begin
			insert into 
			STKLOG (
			SKUN,
			DAYN,
			[TYPE],
			DATE1,
			[TIME],
			KEYS,
			EEID,
			ICOM,
			SSTK,
			ESTK,
			SRET,
			ERET,
			SMDN,
			EMDN,
			SWTF,
			EWTF,
			SPRI,
			EPRI,
			RTI)
		values (
			@SkuNumber,
			DATEDIFF(day, '1900-01-01', getdate()),
			'91',
			convert(date, GETDATE()),
			REPLACE(CONVERT(varchar, getdate(), 108), ':', ''),
			'System Adjustment 91',
			@UserId,
			0,
            @stockEnd,
			@stockStart,
			@returnsEnd,
			@returnsEnd,
			@markdownEnd,
			@markdownEnd,
			@writeoffEnd,
			@writeoffEnd,
			@priceEnd,
			@priceEnd,
			'S'
		);
	end
end
	
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure StockLogInsertCheck91 for US97 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure StockLogInsertCheck91 for US97 has not been rolled back'
GO

