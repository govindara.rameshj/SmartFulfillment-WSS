UPDATE dbo.ReportColumns
SET ColumnId = 101
WHERE ReportId = 201 and ColumnId = 4032

If @@Error = 0
   Print 'Success: Update Table ReportColumns for DE562 has been deployed successfully'
Else
   Print 'Failure: Update Table ReportColumns for DE562 has not been deployed successfully'
Go