DELETE FROM dbo.ReportColumn WHERE Id IN (42570, 42571)

If @@Error = 0
   Print 'Success: Delete from Table ReportColumn for US3914 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportColumn for US3914 has not been deployed successfully'
Go