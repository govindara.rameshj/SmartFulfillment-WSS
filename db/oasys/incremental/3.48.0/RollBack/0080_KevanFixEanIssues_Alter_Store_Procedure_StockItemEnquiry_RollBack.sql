IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[StockItemEnquiry]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure StockItemEnquiry'
	EXEC ('CREATE PROCEDURE dbo.StockItemEnquiry AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [StockItemEnquiry]')
GO

-- =============================================
-- Author        : Alan Lewis
-- Create date   : 22/08/2012
-- User Story	 : 6219
-- Project		 : P022-007 - Fuzzy Logic Search.
-- Task Id		 : 7262
-- Description   : Alter to use new name for fuzzy logic function - 
--				 : Udf_StockItemEnquiryFuzzy instead of UdfStockItemEnquiryFuzzy!
-- =============================================
ALTER procedure [dbo].[StockItemEnquiry]
   @ProductCode          nvarchar(6)  = null,
   @EanNumber            nvarchar(16) = null,
   @ProductDescription   nvarchar(40) = null,
   @SaleType             nchar(1)     = null,
   @Supplier             nvarchar(5)  = null,
   @HierarchyCategory    nvarchar(6)  = null,
   @HierarchyGroup       nvarchar(6)  = null,
   @HierarchySubGroup    nvarchar(6)  = null,
   @HierarchyStyleNumber nvarchar(6)  = null,
   @ExcludeNonStock      bit          = null,
   @ExcludeDeletedStock  bit          = null,
   @ExcludeObsoleteStock bit          = null,
   @ExactMatch           bit,                    --true:  exact match;  @FuzzyMatch parameter ignored; @FuzzyMatch = 1 value hard-coded
                                                 --false: fuzzy match ; @FuzzyMatch parameter respected
                                                 --                     @FuzzyMatch = 1; used "like" on entire search string
                                                 --                     @FuzzyMatch = 2; used "like" on each word in search string
                                                 --                     @FuzzyMatch = 3; used "soundex"
                                                 --                     @FuzzyMatch = 1 will
                                                 
                                                 --false; fuzzy match using character & vowel stripping algorithm
                                                 --       this will supercede the above fuzzy match process

   @FuzzyMethod          int,                    --method one: perform a wildcard search on the entire description field on the search citeria 
                                                 --            knocking one off until the min size has been reached
                                                 --method two: refinement of method one
                                                 --method three: refinement of method two but uses "soundex" instead of "like" 
   @MaxResults           int,
   @AllowMisSpelling	 Bit		  = 0,
   @ElevateForHierarchy	 Bit		  = 0
as
begin

set nocount on


declare @TempTable table (LocalID             int identity,
                          SKUN                nchar(6),
                          [Description]       nvarchar(40),
                          PRIC                decimal(9, 2),
                          ONHA                int,
                          ONOR                int,
                          [SupplierName]      nvarchar(30),
                          [HierarchyCategory] nvarchar(50),
                          IDEL                bit,
                          IOBS                bit,
                          INON                bit,
                          IRIS                bit,
                          IRIB                smallint,
                          StockImageLocation  nvarchar(255))

insert @TempTable (SKUN, [Description], PRIC, ONHA, ONOR, [SupplierName], [HierarchyCategory], IDEL, IOBS, INON, IRIS, IRIB, StockImageLocation)
           select a.SKUN,
                  [Description]       = rtrim(a.DESCR),
                  a.PRIC,
                  a.ONHA,
                  a.ONOR,
                  [SupplierName]      = rtrim(b.NAME),
                  [HierarchyCategory] = rtrim(c.DESCR),
                  a.IDEL,
                  a.IOBS,
                  a.INON,
                  a.IRIS,
                  a.IRIB,
                  StockImageLocation = dbo.StockImageLocation(a.SKUN)
           from STKMAS a                 --stock
           inner join SUPMAS b           --supplier
                 on b.SUPN = a.SUPP
           inner join HIECAT c
                 on c.NUMB = a.CTGY
           left outer join (select * from Udf_StockItemEnquiryFuzzy(@ProductDescription, @MaxResults, @AllowMisSpelling, @ElevateForHierarchy)) d
                      on  @ExactMatch = 0
                      and a.SKUN      = d.SKU

           where (@ProductCode          is null or a.SKUN = @ProductCode)
           and   (@EanNumber            is null or a.SKUN = (select SKUN from EANMAS where NUMB = @EanNumber))
           and   (@SaleType             is null or a.SALT = @SaleType)
           and   (@Supplier             is null or a.SUPP = @Supplier)
           and   (@HierarchyCategory    is null or a.CTGY = @HierarchyCategory)
           and   (@HierarchyGroup       is null or a.GRUP = @HierarchyGroup)
           and   (@HierarchySubGroup    is null or a.SGRP = @HierarchySubGroup)
           and   (@HierarchyStyleNumber is null or a.STYL = @HierarchyStyleNumber)

           and   (@ExcludeNonStock      is null or not (a.INON = @ExcludeNonStock))
           and   (@ExcludeDeletedStock  is null or not (a.IDEL = @ExcludeDeletedStock))
           and   (@ExcludeObsoleteStock is null or not (a.IOBS = @ExcludeObsoleteStock))

           and (@ProductDescription is null
                or
                --exact match: search for entire phrase
                (@ExactMatch = 1 and a.SKUN in (select SKUN
                                                from Udf_StockItemEnquiryMatch(@ProductDescription, len(@ProductDescription), @MaxResults, 1)))
                or
                ----fuzzy match:
                --(@ExactMatch = 0 and a.SKUN in (select SKUN
                --                                from Udf_StockItemEnquiryMatch(@ProductDescription, len(@ProductDescription), @MaxResults, @FuzzyMethod)))

                --fuzzy match: new version
                (@ExactMatch = 0 and d.SKU is not null)
               )
           order by d.FinalWeightedScore desc, a.SKUN asc

select SKUN, [Description], PRIC, ONHA, ONOR, [SupplierName], [HierarchyCategory], IDEL, IOBS, INON, IRIS, IRIB, StockImageLocation
from @TempTable
where LocalID <= @MaxResults

end 
go

If @@Error = 0
   Print 'Success: The Alter Stored Procedure StockItemEnquiry for EAN Issues fix by Kevan has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure StockItemEnquiry for EAN Issues fix by Kevan has not been rolled back successfully'
GO
