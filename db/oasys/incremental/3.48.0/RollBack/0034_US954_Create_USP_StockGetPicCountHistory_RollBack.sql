IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[StockGetPicCountHistory]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Deleting procedure StockGetPicCountHistory'
	EXEC ('DROP PROCEDURE dbo.StockGetPicCountHistory')
END
GO

If @@Error = 0
   Print 'Success: The delete Stored Procedure StockGetPicCountHistory for US954 has been deployed successfully'
Else
   Print 'Failure: The delete Stored Procedure StockGetPicCountHistory for US954 has not been deployed successfully'
GO