DELETE FROM dbo.Report
WHERE Id = 620
GO

If @@Error = 0
   Print 'Success: Delete from Table Report for US954 has been deployed successfully'
Else
   Print 'Failure: Delete from Table Report for US954 has not been deployed successfully'
Go