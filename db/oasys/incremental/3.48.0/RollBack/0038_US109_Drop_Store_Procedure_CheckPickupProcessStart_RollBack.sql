IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[CheckPickupProcessStart]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Deleting procedure CheckPickupProcessStart'
	EXEC ('DROP PROCEDURE dbo.CheckPickupProcessStart')
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure CheckPickupProcessStart for US109 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure CheckPickupProcessStart for US109 has not been deployed successfully'
GO