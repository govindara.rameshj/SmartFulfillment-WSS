IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[CheckPickupProcessEnd]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Deleting procedure CheckPickupProcessEnd'
	EXEC ('DROP PROCEDURE dbo.CheckPickupProcessEnd')
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure CheckPickupProcessEnd for US109 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure CheckPickupProcessEnd for US109 has not been deployed successfully'
GO