UPDATE dbo.ReportColumns SET IsVisible = 1
WHERE ReportId = 310 AND TableId = 1 AND ColumnId IN (225, 1214, 2069)

If @@Error = 0
   Print 'Success: Updating Table ReportColumns for US3914 has been deployed successfully'
Else
   Print 'Failure: Updating Table ReportColumns for US3914 has not been deployed successfully'
Go