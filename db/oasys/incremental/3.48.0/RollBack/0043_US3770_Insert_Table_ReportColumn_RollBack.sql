DELETE FROM dbo.ReportColumn
WHERE Id IN (42560, 42561, 42562)

If @@Error = 0
   Print 'Success: Delete from Table ReportColumn for US3770 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportColumn for US3770 has not been deployed successfully'
Go