IF NOT EXISTs (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 620 AND ColumnId = 2013)
	BEGIN 
		INSERT INTO dbo.ReportColumns 
				([ReportId]
				,[TableId]
				,[ColumnId]
				,[Sequence]
				,[IsVisible]
				,[IsBold]
				,[IsHighlight]
				,[HighlightColour]
				,[Fontsize])
		VALUES
			(620, 1, 2013, 2, 1, 0, NULL, NULL, NULL)
	END
GO
	
UPDATE dbo.ReportColumns SET Sequence = 3
WHERE ReportId = 620 AND ColumnId = 2014
GO

DELETE FROM dbo.ReportColumns
WHERE ReportId = 620 AND ColumnId IN (42560, 42561,42562)

If @@Error = 0
   Print 'Success: Delete from Table ReportColumns for US3770 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportColumns for US3770 has not been deployed successfully'
Go