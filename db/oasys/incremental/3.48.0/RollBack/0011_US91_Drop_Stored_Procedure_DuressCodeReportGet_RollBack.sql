IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'DuressCodeReportGet') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Dropping procedure DuressCodeReportGet'
	Drop PROCEDURE [dbo].[DuressCodeReportGet]
END
GO


If @@Error = 0
   Print 'Success: The stored procedure "DuressCodeReportGet" has been sucessfully dropped'
Else
   Print 'Failure: The stored procedure "DuressCodeReportGet" might NOT have been dropped'
GO

