DELETE FROM dbo.ReportColumn
WHERE Id IN (42550, 42551, 42552)
GO

If @@Error = 0
   Print 'Success: Delete from Table ReportColumn for US113 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportColumn for US113 has not been deployed successfully'
Go