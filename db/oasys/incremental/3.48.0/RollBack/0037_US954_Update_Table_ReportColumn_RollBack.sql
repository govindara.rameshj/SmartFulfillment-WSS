UPDATE dbo.ReportColumn
SET MaxWidth = NULL
WHERE Id in (2013, 2014)

If @@Error = 0
   Print 'Success: Update into Table ReportColumn for US954 has been deployed successfully'
Else
   Print 'Failure: Update into Table ReportColumn for US954 has not been deployed successfully'
Go