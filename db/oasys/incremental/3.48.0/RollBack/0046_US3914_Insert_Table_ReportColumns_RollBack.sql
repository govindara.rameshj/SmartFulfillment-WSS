DELETE FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 1 AND ColumnId IN (42570, 42571)

If @@Error = 0
   Print 'Success: Delete from Table ReportColumns for US3914 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportColumns for US3914 has not been deployed successfully'
Go