DELETE FROM dbo.ReportRelation
WHERE ReportId = 310 AND Name = 'Original Receipt Details'
GO

If @@Error = 0
   Print 'Success: Delete from Table ReportRelation for US113 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportRelation for US113 has not been deployed successfully'
Go