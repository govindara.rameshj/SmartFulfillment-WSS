IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[SalesTransactionGet]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure SalesTransactionGet'
	EXEC ('CREATE PROCEDURE dbo.SalesTransactionGet AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [SalesTransactionGet]')
GO

ALTER procedure [dbo].[SalesTransactionGet]
	@Date			date,
	@TillNumber		int=null,
	@TranNumber		int=null
as
begin

select
    dt.DATE1    as 'Date',
    dt.TILL	    as 'TillId',
    dt.[TRAN]   as 'TranNumber',
    dt.[TIME]   as 'Time',
    dt.CASH	    as 'CashierId',
    su.Name	    as 'CashierName',
    dt.TOTL     as 'Value',
    dt.TAXA	    as 'ValueTax',
	case dt.TCOD
		when 'SA' then 'Sale'
		when 'SC' then 'Sale Correction'
		when 'RF' then 'Refund'
		when 'RC' then 'Refund Correction'
		when 'CO' then 'Sign On'
		when 'CC' then 'Sign Off'
		when 'M+' then 'Misc Income'
		when 'M-' then 'Misc Outgoings'
		when 'C+' then 'Misc Income Correction'
		when 'C-' then 'Misc Outgoings Correction'
		when 'OD' then 'Open Drawer'
		when 'RL' then 'Reprint Logo'
		when 'XR' then 'X-Read'
		when 'ZR' then 'Z-Read'
	end         as 'Type',
    dt.VSUP     as 'SupervisorIdVoided',
    dt.TMOD     as 'IsTraining',
    dt.SUSE     as 'IsSupervisorUsed',
    dt.[PROC]   as 'UpdatedProc',
    DocumentNumber = case dt.DOCN
                        when '00000000' then ''
                        else dt.DOCN
                     end,
    dt.MERC	    as 'ValueMerchandise',
    dt.DISC	    as 'ValueDiscount',
    dt.ACCN	    as 'CustomerNumber',
    dt.ICOM	    as 'IsComplete',
    dt.VATR1    as 'VatRate1',
    dt.XVAT1    as 'ValueExVat1',
    dt.PARK     as 'IsParked',
    dt.REMO	    as 'IsOffline',
    dt.CARD_NO  as 'ColleagueCard'
from
	DLTOTS dt
left join
	SystemUsers su on su.ID=dt.CASH
where
	dt.DATE1 = @Date
	and (@TillNumber is null or (@TillNumber is not null and dt.TILL=@TillNumber))
	and (@TranNumber is null or (@TranNumber is not null and dt.[TRAN]=@TranNumber))
order by
	dt.TILL, dt.[TRAN] desc;
	
select
	dl.DATE1	as 'Date',
	dl.TILL		as 'TillId',
	dl.[TRAN]	as 'TranNumber',
	dl.NUMB		as 'LineNumber',
	dl.SKUN		as 'SkuNumber',
	sm.DESCR	as 'Description',
	dl.QUAN		as 'Qty',
	dl.PRIC		as 'Price',
	dl.EXTP		as 'ValueExtended',
	dl.IBAR		as 'IsScanned',
	dl.SUPV		as 'SupervisorId',
	dl.PORC		as 'ReasonCode',
	dl.LREV		as 'IsReversed',
	dl.VATV		as 'ValueVat',
	case dl.SALT
		when 'B' then 'Basic Item'
		when 'P' then 'Performance Item'
		when 'A' then 'Aesthetic Item'
		when 'S' then 'Showroom Item'
		when 'G' then 'Good Pallet Item'
		when 'R' then 'Reduced Pallet Item'
		when 'D' then 'Delivery Item'
		when 'I' then 'Installation Item'
		when 'W' then 'WEEE Item'
		else 'Other'
	end			as 'SaleType'
from 
	DLLINE dl
left join
	STKMAS sm on sm.SKUN=dl.SKUN
where
	dl.DATE1 = @Date
	and (@TillNumber is null or (@TillNumber is not null and dl.TILL=@TillNumber))
	and (@TranNumber is null or (@TranNumber is not null and dl.[TRAN]=@TranNumber))
order by
	dl.DATE1,dl.TILL,dl.[TRAN] desc, dl.NUMB
	
select
	dp.DATE1	as 'Date',
	dp.TILL		as 'TillId',
	dp.[TRAN]	as 'TranNumber',
	dp.NUMB		as 'Sequence',
	dp.[TYPE]	as 'Type',
	tc.TTDE		as 'Description',
	dp.AMNT		as 'Value',
	dp.[CARD]	as 'CardNumber',
	dp.SUPV		as 'SupervisorId',
	dp.CTYP		as 'CardType',
	dp.COPN		as 'CouponNumber'
from 
	DLPAID dp
left join
	TENCTL tc on tc.TTID=dp.[TYPE]
where
	dp.DATE1 = @Date
	and (@TillNumber is null or (@TillNumber is not null and dp.TILL=@TillNumber))
	and (@TranNumber is null or (@TranNumber is not null and dp.[TRAN]=@TranNumber))
order by 
	dp.DATE1, dp.TILL, dp.[TRAN] desc,dp.NUMB;
	
end

If @@Error = 0
   Print 'Success: The Alter Stored Procedure SalesTransactionGet for US113 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure SalesTransactionGet for US113 has not been deployed successfully'
GO