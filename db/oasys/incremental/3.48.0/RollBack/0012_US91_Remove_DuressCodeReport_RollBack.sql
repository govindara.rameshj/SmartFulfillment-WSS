
PRINT 'Start to remove Duress Code Report'

BEGIN TRANSACTION;
BEGIN TRY
	
	PRINT 'Dropping hyperlink to Duress Code Report from Dashboard'
		
	DELETE [dbo].[ReportHyperlink]
	WHERE ReportId = 9 AND TableId = 1 AND RowId = 2

	PRINT 'Dropping Duress Code Report records from ProfilemenuAccess'

	DELETE [dbo].[ProfilemenuAccess]
	WHERE MenuConfigID = 3381
	
	
	PRINT 'Dropping Duress Code Report records from MenuConfig'

	DELETE [dbo].[MenuConfig]
	WHERE [ID] = 3381
	
	
	PRINT 'Dropping Duress Code Report record from ReportTable'
	
	DELETE [dbo].[ReportTable]
	WHERE ReportId = 321
		
		
	PRINT 'Dropping Duress Code Report parameters'

	DELETE [dbo].[ReportParameters] 
	WHERE ReportId = 321
	
	
	PRINT 'Dropping Duress Code Report'

	DELETE [dbo].[Report]
	WHERE Id = 321

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION;
		PRINT 'Failure: Duress Code Report for US91 has not been removed'
		PRINT 'ErrorNumber = ' + CONVERT(varchar, ERROR_NUMBER()) 
			+ ', ErrorSeverity = ' +  CONVERT(varchar, ERROR_SEVERITY())
			+ ', ErrorState = ' + CONVERT(varchar, ERROR_STATE())
			+ ', ErrorLine = ' + CONVERT(varchar, ERROR_LINE())
			+ ', ErrorMessage = ' + ERROR_MESSAGE()
	END
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
	COMMIT TRANSACTION;
	PRINT 'Success: Duress Code Report for US91 has been removed successfully'
END
GO