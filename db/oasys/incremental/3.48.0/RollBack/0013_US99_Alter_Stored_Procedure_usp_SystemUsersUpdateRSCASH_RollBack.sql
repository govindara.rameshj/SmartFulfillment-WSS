IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'usp_SystemUsersUpdateRSCASH') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure usp_SystemUsersUpdateRSCASH'
	EXEC ('CREATE PROCEDURE dbo.usp_SystemUsersUpdateRSCASH AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure usp_SystemUsersUpdateRSCASH')
GO

ALTER PROCEDURE [dbo].[usp_SystemUsersUpdateRSCASH]
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 8th March 2011
-- 
-- Task     : Updates / Inserts Users into RSCASH table for use by Till Systems.
-- Notes	: SP updates or inserts data as old data is sometimes present. Updating or Inserting the
--            data will prevent the stored procedure from failing. Data in Pilot stores has shown that
--            existing data can or may not already exist.
-----------------------------------------------------------------------------------
	
		@sp_EmployeeCode			char(3),
		@sp_Name					char(35),
		@sp_Password				char(5)

AS
BEGIN
	SET NOCOUNT ON;

--------------------------------------------------------------------------------------------------------------------------
-- Insert New / Update Existing User into CASMAS Table
--------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select CASH FROM [Oasys].[dbo].[RSCASH] WHERE CASH = @sp_EmployeeCode)
	Begin	
		insert into RSCASH 
				(
				cash,name,secc,ftky,till,numb,amnt
				) 
		values	( 
				@sp_EmployeeCode,@sp_Name,@sp_Password,'0','99','0', '0'
				)	
	End
ELSE
	Begin
		Update	RSCASH
		Set		NAME = @sp_Name, SECC = @sp_Password, FTKY = '0', TILL = '99', NUMB = '0', AMNT = '0'
		Where	CASH = @sp_EmployeeCode
	End	

			
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_SystemUsersUpdateRSCASH for US99 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_SystemUsersUpdateRSCASH for US99 has not been rolled back'
GO