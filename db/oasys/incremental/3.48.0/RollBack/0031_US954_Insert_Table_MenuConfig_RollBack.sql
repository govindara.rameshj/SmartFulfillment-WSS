DELETE FROM dbo.MenuConfig
WHERE ID IN (11550, 7340)

If @@Error = 0
   Print 'Success: The Delete from table MenuConfig for US954 has been deployed successfully'
Else
   Print 'Failure: The Delete from table MenuConfig for US954 has not been deployed successfully'
GO