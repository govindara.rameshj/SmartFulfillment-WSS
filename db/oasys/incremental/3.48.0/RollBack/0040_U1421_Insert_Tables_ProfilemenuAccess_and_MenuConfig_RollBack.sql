BEGIN TRANSACTION ReprintLabelsMenuTransaction
BEGIN TRY
	DECLARE @MenuID int = 9065

	DELETE FROM dbo.ProfilemenuAccess
	WHERE MenuConfigID = @MenuID

	DELETE FROM dbo.MenuConfig
	WHERE ID = @MenuID

	UPDATE dbo.MenuConfig
	SET DisplaySequence -= 1
	WHERE ID IN (9070, 9080, 9085, 9090, 10230)
	COMMIT TRANSACTION ReprintLabelsMenuTransaction
	Print 'Success: Scripts for US1421 has been deployed successfully'
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION ReprintLabelsMenuTransaction
	Print 'Failure: Scripts for US1421 has not been deployed successfully'
END CATCH