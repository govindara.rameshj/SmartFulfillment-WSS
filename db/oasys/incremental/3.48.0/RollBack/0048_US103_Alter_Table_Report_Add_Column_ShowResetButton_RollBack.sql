IF EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'Report' and col.name = 'ShowResetButton'
)
BEGIN
PRINT 'Drop ShowResetButton column from Report'
	ALTER TABLE dbo.Report DROP COLUMN ShowResetButton
END
GO

If @@Error = 0
   Print 'Success: Add ShowResetBtn column to Report for US3770 has been deployed successfully'
ELSE
   Print 'Failure: Add ShowResetBtn column to Report for US3770 has not been deployed successfully'
GO	