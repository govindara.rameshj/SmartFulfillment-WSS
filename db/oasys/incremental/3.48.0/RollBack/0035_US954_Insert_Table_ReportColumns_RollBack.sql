DELETE FROM dbo.ReportColumns
WHERE ReportId = 620 AND TableId = 1 AND ColumnId IN (2500, 2013, 2014)

If @@Error = 0
   Print 'Success: Insert into Table ReportTable for US954 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportTable for US954 has not been deployed successfully'
Go