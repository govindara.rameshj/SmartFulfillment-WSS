UPDATE dbo.MenuConfig
SET AppName = 'PIC Count History'
WHERE ID IN (11540, 7330)
GO

UPDATE dbo.MenuConfig
SET DisplaySequence = 6
WHERE AppName = 'Purchase Order Enquiry' AND MasterId IN (11300, 7100)
GO

UPDATE dbo.MenuConfig
SET DisplaySequence = 7
WHERE AppName = 'Search Stock Adjustments' AND MasterId IN (11300, 7100)
GO

If @@Error = 0
   Print 'Success: Update Table MenuConfig for US118 has been deployed successfully'
Else
   Print 'Failure: Update Table MenuConfig for US118 has not been deployed successfully'
GO