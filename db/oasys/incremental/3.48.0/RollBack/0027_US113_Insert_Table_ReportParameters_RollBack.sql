DELETE FROM dbo.ReportParameters
WHERE ParameterId IN (200, 351, 352) AND ReportId = 310
GO

If @@Error = 0
   Print 'Success: Delete from Table ReportParameters for US113 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportParameters for US113 has not been deployed successfully'
Go