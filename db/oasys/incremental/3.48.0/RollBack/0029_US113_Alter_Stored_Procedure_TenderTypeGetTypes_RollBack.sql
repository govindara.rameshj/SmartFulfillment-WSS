IF EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[TenderTypeGetTypes]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Droping procedure TenderTypeGetTypes'
	DROP PROCEDURE dbo.TenderTypeGetTypes
END
GO

If @@Error = 0
   Print 'Success: The Drop Stored Procedure TenderTypeGetTypes for US113 has been deployed successfully'
Else
   Print 'Failure: The Drop Stored Procedure TenderTypeGetTypes for US113 has not been deployed successfully'
GO