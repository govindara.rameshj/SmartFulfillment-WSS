DELETE FROM dbo.ProfilemenuAccess 
WHERE MenuConfigID IN (11550, 7340)
GO

If @@Error = 0
   Print 'Success: Delete from Table ProfilemenuAccess for US954 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ProfilemenuAccess for US954 has not been deployed successfully'
Go