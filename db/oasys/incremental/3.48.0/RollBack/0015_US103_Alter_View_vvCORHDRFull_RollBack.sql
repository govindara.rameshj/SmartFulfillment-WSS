ALTER VIEW dbo.vwCORHDRFull
AS
SELECT     
CH.NUMB, 
CH.CUST, 
CH.DATE1, 
CH.DELD, 
CH.CANC, 
CH.DELI, 
CH.DELC, 
CH.AMDT, 
CH.ADDR1, 
CH.ADDR2, 
CH.ADDR3, 
CH.ADDR4, 
CH.PHON, 
CH.PRNT, 
CH.RPRN, 
CH.REVI, 
CH.MVST, 
CH.DCST, 
CH.QTYO, 
CH.QTYT, 
CH.QTYR, 
CH.WGHT, 
CH.VOLU, 
CH.SDAT, 
CH.STIL, 
CH.STRN, 
CH.RDAT, 
CH.RTIL, 
CH.RTRN, 
CH.MOBP, 
CH.POST, 
CH.NAME, 
CH4.DDAT, 
CH4.OEID, 
CH4.FDID, 
CH4.DeliveryStatus, 
CH4.RefundStatus, 
CH4.SellingStoreId, 
CH4.SellingStoreOrderId, 
CH4.OMOrderNumber, 
CH4.CustomerAddress1, 
CH4.CustomerAddress2, 
CH4.CustomerAddress3, 
CH4.CustomerAddress4, 
CH4.CustomerPostcode, 
CH4.CustomerEmail, 
CH4.PhoneNumberWork, 
CH4.IsSuspended, CH5.Source, 
CH5.SourceOrderNumber, 
CH5.ExtendedLeadTime, 
CH5.DeliveryContactName, 
CH5.DeliveryContactPhone, 
CH5.CustomerAccountNo, 
ISNULL(CH5.Source, 'NU') AS SOURCE_CODE, 
CAST(1 - ISNULL(CH5.IsClickAndCollect, 0) AS BIT) AS SHOW_IN_UI, 
CAST(1 - ISNULL(CH5.IsClickAndCollect, 0) AS BIT) AS SEND_UPDATES_TO_OM, 
ISNULL(CH5.IsClickAndCollect, 0) AS ALLOW_REMOTE_CREATE, ISNULL(CH5.IsClickAndCollect, 0) AS IS_CLICK_AND_COLLECT
FROM dbo.CORHDR AS CH 
	INNER JOIN dbo.CORHDR4 AS CH4 ON CH.NUMB = CH4.NUMB 
	LEFT OUTER JOIN dbo.CORHDR5 AS CH5 ON CH5.NUMB = CH.NUMB
GO
	
If @@Error = 0
   Print 'Success: The Alter view dbo.vvCORHDRFull for US103 has been deployed successfully'
Else
   Print 'Failure: The Alter view dbo.vvCORHDRFull for US103 has not been deployed'
GO