SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'CORHDR' and col.name = 'HOMP'
)
BEGIN
PRINT 'Add HOMP column to CORHDR'
	ALTER TABLE dbo.CORHDR DROP COLUMN HOMP
END

GO

If @@Error = 0
   Print 'Success: Drop HOMP column to CORHDR for US103 has been deployed successfully'
ELSE
   Print 'Failure: Drop HOMP column to CORHDR for US103 has not been deployed successfully'
GO	