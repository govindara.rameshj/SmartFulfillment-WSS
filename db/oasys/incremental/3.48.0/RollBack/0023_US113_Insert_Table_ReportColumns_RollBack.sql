DELETE FROM dbo.ReportColumns
WHERE ReportId = 310 AND TableId = 4
GO

If @@Error = 0
   Print 'Success: Delete from Table ReportColumns for US113 has been deployed successfully'
Else
   Print 'Failure: Delete from Table ReportColumns for US113 has not been deployed successfully'
Go