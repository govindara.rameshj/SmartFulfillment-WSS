IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[CheckPickupProcessStart]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure CheckPickupProcessStart'
	EXEC ('CREATE PROCEDURE dbo.CheckPickupProcessStart AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [CheckPickupProcessStart]')
GO

ALTER procedure [dbo].[CheckPickupProcessStart](@ownerID varchar(3), @pickupProcessStart bit = 0 Output)
AS
BEGIN
	DECLARE @currentDate date
	SET @currentDate = CONVERT(varchar(10), GETDATE(), 121)
	
	IF EXISTS(SELECT 1 FROM [dbo].[Locks] 
			  WHERE [EntityId] = 'NewBanking.Form.PickupEntry' 
				AND CAST([OwnerId] as int) = CAST(@ownerID as int) 
				AND CONVERT(varchar(10), [LockTime], 121) = @currentDate)
		SET @pickupProcessStart = 1	
	ELSE
		SET @pickupProcessStart = 0
END
GO 

If @@Error = 0
   Print 'Success: The Create Stored Procedure CheckPickupProcessStart for US109 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure CheckPickupProcessStart for US109 has not been deployed successfully'
GO