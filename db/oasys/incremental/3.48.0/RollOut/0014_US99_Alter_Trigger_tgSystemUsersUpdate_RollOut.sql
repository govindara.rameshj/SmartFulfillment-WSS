IF NOT EXISTS (SELECT * FROM sys.triggers WHERE type = 'TR' AND name = 'tgSystemUsersUpdate')
BEGIN
  PRINT 'Creating trigger tgSystemUsersUpdate'
  EXEC ('CREATE TRIGGER [dbo].[tgSystemUsersUpdate] ON [dbo].[SystemUsers] AFTER UPDATE AS BEGIN SELECT 1 END') 
END
GO

ALTER TRIGGER [dbo].[tgSystemUsersUpdate] on [dbo].[SystemUsers] for update
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.2
-- Author   : Kevan Madelin
-- Date	    : 8th March 2011
-- 
-- Task     : Updates / Inserts Users using stored procedures.
-- Notes	: This Trigger uses three stored procedures to update or insert data into
--            SYSPAS, RSCASH and CASMAS tables. This allows the Till and HHT systems 
--            to continue to work and updates the user until the applications are re-written
--            to not use the older table structure.
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.3
-- Author   : Partha Dutta
-- Date	    : 15/6/2011
--
-- Task     : Chnage trigger name from tgSystemUsersUpdate to tgSystemUsersInsert
--
--            RSCASH no longer required to be created
-----------------------------------------------------------------------------------

AS
BEGIN
	SET NOCOUNT ON;

DECLARE	
		@EmployeeCode			char(3),
		@Name					char(35),
		@Initials				char(5),
		@Position				char(20),
		@PayrollId				char(20),
		@Password				char(5),
		@PasswordExpires		date,
		@IsSupervisor			bit,
		@SuperPassword			char(5),
		@SuperPasswordExpires	date,
		@Outlet					char(2),
		@IsDeleted				bit,
		@DeletedDate			date,
		@DeletedTime			char(6),
		@DeletedBy				char(3),
		@DeletedWhere			char(2),
		@TillReceiptName		char(35),
		@DefaultAmount			dec(9,2),
		@LanguageCode			char(3),
		@IsManager				bit,
		@SecurityProfileId		int;

Set		@EmployeeCode			= (Select i.EmployeeCode From inserted as i)
Set		@Name					= (Select i.Name FROM inserted as i)
Set		@Initials				= (Select i.Initials FROM inserted as i)
Set		@Position				= (Select i.Position FROM inserted as i)
Set		@PayrollId				= (Select i.PayrollId FROM inserted as i)
Set		@Password				= (Select i.Password FROM inserted as i)
Set		@PasswordExpires		= (Select i.PasswordExpires FROM inserted as i)
Set		@IsSupervisor			= (Select i.IsSupervisor FROM inserted as i)
Set		@SuperPassword			= (Select i.SupervisorPassword FROM inserted as i)
Set		@SuperPasswordExpires	= (Select i.SupervisorPwdExpires FROM inserted as i)
Set		@Outlet					= (Select i.Outlet FROM inserted as i)
Set		@IsDeleted				= (Select i.IsDeleted FROM inserted as i)
Set		@DeletedDate			= (Select i.DeletedDate FROM inserted as i)
Set		@DeletedTime			= (Select i.DeletedTime FROM inserted as i)
Set		@DeletedBy				= (Select i.DeletedBy FROM inserted as i)
Set		@DeletedWhere			= (Select i.DeletedWhere FROM inserted as i)
Set		@TillReceiptName		= (Select i.TillReceiptName FROM inserted as i)
Set		@DefaultAmount			= (Select i.DefaultAmount FROM inserted as i)
Set		@LanguageCode			= (Select i.LanguageCode FROM inserted as i)
Set		@IsManager				= (Select i.IsManager FROM inserted as i)
Set		@SecurityProfileId		= (Select i.SecurityProfileID FROM inserted as i)

--------------------------------------------------------------------------------------------------------------------------
-- Call SYSPAS Update / Insert (usp_SystemUsersUpdateSYSPAS)
--------------------------------------------------------------------------------------------------------------------------
Begin
EXEC	[dbo].[usp_SystemUsersUpdateSYSPAS]
				
				@EmployeeCode,
				@Name,
				@Initials,
				@Position,
				@PayrollId,
				@Password,
				@PasswordExpires,
				@IsSupervisor,
				@SuperPassword,
				@SuperPasswordExpires,
				@Outlet,
				@IsDeleted,
				@DeletedDate,
				@DeletedTime,
				@DeletedBy,
				@DeletedWhere,
				@TillReceiptName,
				@DefaultAmount,
				@LanguageCode,
				@IsManager,
				@SecurityProfileId		
End

		
--------------------------------------------------------------------------------------------------------------------------
-- Call CASMAS Update / Insert (usp_SystemUsersUpdateCASMAS)
--------------------------------------------------------------------------------------------------------------------------
Begin
EXEC	[dbo].[usp_SystemUsersUpdateCASMAS]
				
				@EmployeeCode,
				@Position,
				@Password,
				@IsSupervisor,
				@IsDeleted,
				@TillReceiptName,
				@DefaultAmount
End
								
--------------------------------------------------------------------------------------------------------------------------
-- Call RSCASH Update (usp_SystemUsersUpdateRSCASH)
--------------------------------------------------------------------------------------------------------------------------
Begin
EXEC	[dbo].[usp_SystemUsersUpdateRSCASH]
				
				@EmployeeCode,
				@Name,		
				@Password
End	
			
END
GO

If @@Error = 0
   Print 'Success: Alter Trigger tgSystemUsersUpdate for US99 has been deployed successfully'
Else
   Print 'Failure: Alter Trigger tgSystemUsersUpdate for US99 has not been deployed successfully'
Go
