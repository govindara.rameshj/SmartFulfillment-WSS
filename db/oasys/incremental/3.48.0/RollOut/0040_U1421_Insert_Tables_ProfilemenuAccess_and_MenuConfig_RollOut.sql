BEGIN TRANSACTION ReprintLabelsMenuTransaction
BEGIN TRY
	UPDATE dbo.MenuConfig
	SET DisplaySequence += 1
	WHERE ID IN (9070, 9080, 9085, 9090, 10230)

	DECLARE @MenuID int = 9065

	IF NOT EXISTS (SELECT 1 FROM dbo.MenuConfig WHERE ID = @MenuID)
	BEGIN
		INSERT INTO dbo.MenuConfig (
				ID,
				MasterID,
				AppName,
				AssemblyName,
				ClassName,
				MenuType,
				[Parameters],
				ImagePath,
				LoadMaximised,
				IsModal,
				DisplaySequence,
				AllowMultiple,
				WaitForExit,
				TabName,
				[Description],
				ImageKey,
				[Timeout],
				DoProcessingType)
		VALUES (@MenuID, 9000, 'Reprint Labels', 'ReprintLabels.dll', 'ReprintLabels.ReprintLabelsForm', 4, NULL, 'icons\icon_g.ico', 0, 1, 6, 0, 0, NULL, NULL, 0, 0, 1)
	END

	IF NOT EXISTS (SELECT 1 FROM dbo.ProfilemenuAccess WHERE MenuConfigID = @MenuID)
	BEGIN	
		INSERT INTO	dbo.ProfilemenuAccess (
			[ID],
			[MenuConfigID],
			[AccessAllowed],
			[OverrideAllowed],
			[SecurityLevel],
			[IsManager],
			[IsSupervisor],
			[LastEdited],
			[Parameters]
			)
		SELECT 
				[ID],
				9065 AS [MenuConfigID],
				[AccessAllowed],
				[OverrideAllowed],
				[SecurityLevel],
				[IsManager],
				[IsSupervisor],
				[LastEdited],
				[Parameters]
		FROM dbo.ProfilemenuAccess AS PA
		WHERE PA.MenuConfigID = 9060 AND (EXISTS(SELECT * FROM dbo.SecurityProfile AS SP WHERE SP.ID = PA.ID AND SP.ProfileType = 'U') OR PA.ID IN (20, 30))
		-- We need to get only ID-s of users or WSID = 20 and 30 from SecurityProfile. In SecurityProfile users are marked by letter 'U' in ProfileType.
	END

	COMMIT TRANSACTION ReprintLabelsMenuTransaction
	Print 'Success: Scripts for US1421 has been deployed successfully'
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION ReprintLabelsMenuTransaction
	Print 'Failure: Scripts for US1421 has not been deployed successfully'
END CATCH