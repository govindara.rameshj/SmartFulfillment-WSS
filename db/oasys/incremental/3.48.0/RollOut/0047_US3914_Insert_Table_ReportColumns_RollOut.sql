IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 1 AND ColumnId = 42570)
	BEGIN 
		INSERT INTO dbo.ReportColumns
		VALUES (310, 1, 42570, 0, 0, 0, NULL,	NULL, NULL)
	END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 1 AND ColumnId = 42571)
	BEGIN  
		INSERT INTO dbo.ReportColumns
		VALUES (310, 1, 42571, 0, 1, 0, NULL,	NULL, NULL)
	END
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportColumns for US3914 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportColumns for US3914 has not been deployed successfully'
Go