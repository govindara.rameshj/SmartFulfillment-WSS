IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 2500)
	INSERT INTO ReportColumns (
			[ReportId],
			[TableId],
			[ColumnId],
			[Sequence],
			[IsVisible],
			[IsBold],
			[IsHighlight],
			[HighlightColour],
			[Fontsize])
	VALUES
		(310, 4, 2500, 1, 0, 0, NULL, NULL, NULL)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 151)
	INSERT INTO ReportColumns (
			[ReportId],
			[TableId],
			[ColumnId],
			[Sequence],
			[IsVisible],
			[IsBold],
			[IsHighlight],
			[HighlightColour],
			[Fontsize])
	VALUES
		(310, 4, 151, 2, 0, 0, NULL, NULL, NULL)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 230)
	INSERT INTO ReportColumns (
			[ReportId],
			[TableId],
			[ColumnId],
			[Sequence],
			[IsVisible],
			[IsBold],
			[IsHighlight],
			[HighlightColour],
			[Fontsize])
	VALUES
		(310, 4, 230, 3, 0, 0, NULL, NULL, NULL)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 42552)
	INSERT INTO ReportColumns (
			[ReportId],
			[TableId],
			[ColumnId],
			[Sequence],
			[IsVisible],
			[IsBold],
			[IsHighlight],
			[HighlightColour],
			[Fontsize])
	VALUES
		(310, 4, 42552, 4, 1, 0, NULL, NULL, NULL)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 42550)
	INSERT INTO ReportColumns (
			[ReportId],
			[TableId],
			[ColumnId],
			[Sequence],
			[IsVisible],
			[IsBold],
			[IsHighlight],
			[HighlightColour],
			[Fontsize])
	VALUES
		(310, 4, 42550, 5, 1, 0, NULL, NULL, NULL)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 310 AND TableId = 4 AND ColumnId = 42551)
	INSERT INTO ReportColumns (
			[ReportId],
			[TableId],
			[ColumnId],
			[Sequence],
			[IsVisible],
			[IsBold],
			[IsHighlight],
			[HighlightColour],
			[Fontsize])
	VALUES
		(310, 4, 42551, 6, 1, 0, NULL, NULL, NULL)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportColumns for US113 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportColumns for US113 has not been deployed successfully'
Go