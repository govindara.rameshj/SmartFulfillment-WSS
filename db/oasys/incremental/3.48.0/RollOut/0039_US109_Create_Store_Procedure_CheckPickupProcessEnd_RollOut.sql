IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[CheckPickupProcessEnd]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure CheckPickupProcessEnd'
	EXEC ('CREATE PROCEDURE dbo.CheckPickupProcessEnd AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [CheckPickupProcessEnd]')
GO

ALTER procedure [dbo].[CheckPickupProcessEnd](@ownerID varchar(3), @pickupProcessEnd bit = 0 Output)
AS
BEGIN

	declare @currentPeriodID int
	
	set @currentPeriodID = (select ID from SystemPeriods where StartDate = CONVERT(varchar(10), GETDATE(), 121))

	if exists (
		select 1 
		from SafeBags 
		where PickupPeriodID = @currentPeriodID
			and [Type] = 'P'
			and CashDrop is null
			and AccountabilityID = CAST(@ownerID as int)
		)
		set @pickupProcessEnd = 1
	else 
		set @pickupProcessEnd = 0
	
END
GO 

If @@Error = 0
   Print 'Success: The Create Stored Procedure CheckPickupProcessEnd for US109 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure CheckPickupProcessEnd for US109 has not been deployed successfully'
GO