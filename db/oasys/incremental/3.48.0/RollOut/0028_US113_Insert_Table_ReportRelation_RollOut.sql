IF NOT EXISTS(SELECT 1 FROM dbo.ReportRelation WHERE ReportId = 310 AND Name = 'Original Receipt Details')
	INSERT INTO dbo.ReportRelation (
		[ReportId],
		[Name],
		[ParentTable],
		[ParentColumns],
		[ChildTable],
		[ChildColumns])
	VALUES (310, 'Original Receipt Details', 'Header', 'Date,TillId,TranNumber', 'Original Receipt Details', 'Date,TillId,TranNumber')
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportRelation for US113 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportRelation for US113 has not been deployed successfully'
Go