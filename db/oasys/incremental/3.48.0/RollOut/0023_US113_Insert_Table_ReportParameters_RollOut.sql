IF NOT EXISTS(SELECT 1 FROM dbo.ReportParameters WHERE ParameterId = 200 AND ReportId = 310)
	INSERT INTO dbo.ReportParameters (
					ReportId,
					ParameterId,
					Sequence,
					AllowMultiple,
					DefaultValue)
	VALUES (310, 200, 4, 0, NULL)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportParameters WHERE ParameterId = 351 AND ReportId = 310)
	INSERT INTO dbo.ReportParameters (
					ReportId,
					ParameterId,
					Sequence,
					AllowMultiple,
					DefaultValue)
	VALUES (310, 351, 6, 0, NULL)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportParameters WHERE ParameterId = 352 AND ReportId = 310)
	INSERT INTO dbo.ReportParameters (
					ReportId,
					ParameterId,
					Sequence,
					AllowMultiple,
					DefaultValue)
	VALUES (310, 352, 5, 0, NULL)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportParameters for US113 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportParameters for US113 has not been deployed successfully'
Go