IF NOT EXISTS(SELECT 1 FROM dbo.ReportParameter WHERE Id = 351)
	INSERT INTO dbo.ReportParameter (
					Id,
					Description,
					Name,
					DataType,
					Size,
					LookupValuesProcedure,
					Mask)
	VALUES (351, 'Amount', 'Amount', 3, 9, NULL, '(\+|-)?[0-9]+(\.[0-9][0-9]?)?')
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportParameter WHERE Id = 352)
	INSERT INTO dbo.ReportParameter (
					Id,
					Description,
					Name,
					DataType,
					Size,
					LookupValuesProcedure,
					Mask)
	VALUES (352, 'Tender Type', 'TenderType', 3, 2, 'TenderTypeGetTypes', NULL)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportParameter for US113 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportParameter for US113 has not been deployed successfully'
Go
