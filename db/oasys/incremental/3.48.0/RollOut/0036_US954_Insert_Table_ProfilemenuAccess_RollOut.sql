IF NOT EXISTS (SELECT 1 FROM dbo.ProfilemenuAccess WHERE MenuConfigID = 7340)
BEGIN	
	INSERT INTO	dbo.ProfilemenuAccess (
		[ID],
		[MenuConfigID],
		[AccessAllowed],
		[OverrideAllowed],
		[SecurityLevel],
		[IsManager],
		[IsSupervisor],
		[LastEdited],
		[Parameters]
		)
	SELECT 
			[ID],
			7340 AS [MenuConfigID],
			[AccessAllowed],
			[OverrideAllowed],
			[SecurityLevel],
			[IsManager],
			[IsSupervisor],
			[LastEdited],
			[Parameters]
	FROM dbo.ProfilemenuAccess
	WHERE MenuConfigID = 7330
END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ProfilemenuAccess WHERE MenuConfigID = 11550)
BEGIN	
	INSERT INTO	dbo.ProfilemenuAccess (
		[ID],
		[MenuConfigID],
		[AccessAllowed],
		[OverrideAllowed],
		[SecurityLevel],
		[IsManager],
		[IsSupervisor],
		[LastEdited],
		[Parameters]
		)
	SELECT 
			[ID],
			11550 AS [MenuConfigID],
			[AccessAllowed],
			[OverrideAllowed],
			[SecurityLevel],
			[IsManager],
			[IsSupervisor],
			[LastEdited],
			[Parameters]
	FROM dbo.ProfilemenuAccess
	WHERE MenuConfigID = 11540
END
GO

If @@Error = 0
   Print 'Success: Insert into Table ProfilemenuAccess for US954 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ProfilemenuAccess for US954 has not been deployed successfully'
Go

