UPDATE dbo.MenuConfig
SET 
	AppName = 'PIC Stock Count'
WHERE ID IN (11540, 7330)
GO

UPDATE dbo.MenuConfig
SET DisplaySequence = 7
WHERE AppName = 'Purchase Order Enquiry' AND MasterId IN (11300, 7100)
GO

UPDATE dbo.MenuConfig
SET DisplaySequence = 8
WHERE AppName = 'Search Stock Adjustments' AND MasterId IN (11300, 7100)
GO

If @@Error = 0
   Print 'Success: Update Table MenuConfig for US954 has been deployed successfully'
Else
   Print 'Failure: Update Table MenuConfig for US954 has not been deployed successfully'
Go