IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumn WHERE Id = 42550)
	INSERT INTO dbo.ReportColumn (
				[Id],
				[Name],
				[Caption],
				[FormatType],
				[Format],
				[IsImagePath],
				[MinWidth],
				[MaxWidth],
				[Alignment])
	VALUES 
		(42550, 'OriginalTillId', 'Original Till Id', 1, 'n0', 0, 80, NULL, 2)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumn WHERE Id = 42551)
	INSERT INTO dbo.ReportColumn (
				[Id],
				[Name],
				[Caption],
				[FormatType],
				[Format],
				[IsImagePath],
				[MinWidth],
				[MaxWidth],
				[Alignment])
	VALUES
		(42551, 'OriginalTranNumber', 'Original Tran Number', 1, '', 0, 80, NULL, 2)
GO

IF NOT EXISTS(SELECT 1 FROM dbo.ReportColumn WHERE Id = 42552)
	INSERT INTO dbo.ReportColumn (
				[Id],
				[Name],
				[Caption],
				[FormatType],
				[Format],
				[IsImagePath],
				[MinWidth],
				[MaxWidth],
				[Alignment])
	VALUES
		(42552, 'OriginalDate', 'Original Date', 2, 'dd/MM/yyyy', 0, 130, 160, 2)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportColumn for US113 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportColumn for US113 has not been deployed successfully'
Go