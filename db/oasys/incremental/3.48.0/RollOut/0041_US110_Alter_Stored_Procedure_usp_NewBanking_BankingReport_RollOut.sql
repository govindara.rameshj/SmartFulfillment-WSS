IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[usp_NewBanking_BankingReport]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure usp_NewBanking_BankingReport'
	EXEC ('CREATE PROCEDURE dbo.usp_NewBanking_BankingReport AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [usp_NewBanking_BankingReport]')
GO

-- =============================================
-- Author        : Alan Lewis
-- Create date   : 31/08/2012
-- User Story	 : 6237
-- Project		 : P022-015: RF0608 - New Banking Report.
-- Task Id		 : 7403
-- Description   : Create stored procedure to feed the
--				 : new 'Banking Report' that shows what
--				 : was banked on any given logical banking
--				 : day.
-- =============================================
ALTER Procedure [dbo].[usp_NewBanking_BankingReport]
   @BankingDate DateTime
As
Begin
	Set NoCount On

	Declare @PeriodID				Int
	Declare @BankingCashBag Table
		(
			RID			Int Identity,
			ID			Int,
			SlipNumber	NVarChar(Max),
			SealNumber	NVarChar(Max)
		)
	Declare @BankingCashBagCount		Int = 0
	Declare @NextBankingCashBag			Int = 0
	Declare @Value						Decimal(9,2)
	Declare @TotalValue					Decimal(9,2) = 0
	Declare @BankingChequeBag Table
		(
			RID			Int Identity,
			ID			Int,
			SlipNumber	NVarChar(Max),
			SealNumber	NVarChar(Max)
		)
	Declare @BankingBagCashDenom Table
		(
			ID Decimal(9, 2),
			Value Decimal(9, 2)
		)
	Declare @BankingChequeBagCount		Int = 0
	Declare @NextBankingChequeBag		Int = 0
	Declare @BankingBagChequeDenom Table
		(
			ID Decimal(9, 2),
			Value Decimal(9, 2)
		)
	Declare @NoneCashTenders Table
	(
		TenderID Int,
		TenderName NVarChar(25)
	)
	Declare @TenderID					Int
	Declare @TenderName					NVarChar(25)
			
	Declare @Output           
		Table
			(
				Denomination	nVarChar(Max),
				Value			nVarChar(Max),
				SlipNumber		NVarChar(Max),
				SealNumber		NVarChar(Max)
			)

	Set @PeriodID = (Select ID From SystemPeriods Where DateDiff(Day, @BankingDate, StartDate) = 0)

	If Not Exists(Select * From Safe Where PeriodID = @PeriodID)
		Begin
		  Insert @Output Values(Null, 'No Report', 'Safe Not Found', Null)
		End
	Else
		Begin		
			If Not Exists (Select sb.ID
						From SafeBags sb Inner Join SafeBagsDenoms sbd On sbd.BagID = sb.ID
						Where sb.PickupPeriodID = @PeriodID
							And sb.[Type] = 'B'
							And sb.[State] In ('M', 'Q')
						Group By sb.ID, sbd.SlipNumber, sb.SealNumber)
				Begin
					Insert @Output Values(Null, 'No Report', 'No Bags today', Null)
				End
			Else
				Begin
					
					Insert
						@BankingCashBag
					Select sb.ID, sbd.SlipNumber, sb.SealNumber
					From
						SafeBags sb
							Inner Join
								SafeBagsDenoms sbd
							On
								sbd.BagID = sb.ID
					Where
						sb.PickupPeriodID = @PeriodID
					And
						sb.[Type] = 'B'
					And
						sb.[State] In ('M', 'Q')
					And				
						sbd.TenderID = 1
					Group By
						sb.ID, sbd.SlipNumber, sb.SealNumber
					
					Select @BankingCashBagCount = Count(ID) From @BankingCashBag
					If @BankingCashBagCount = 0
						Begin
							Insert @Output Select 'Banking Bag Number', '', 'Slip Number', ''
							Insert @Output Select 'Denomination', 'Banked', '', ''							
							Insert @Output Select '100.00',	'', '', ''
							Insert @Output Select '50.00', '', '', ''
							Insert @Output Select '20.00', '', '', ''
							Insert @Output Select '10.00', '', '', ''
							Insert @Output Select '5.00', '', '', ''
							Insert @Output Select 'Total', '', '', ''
							Insert @Output Select '', '', '', ''						
						End
					Else
						Begin
							While @NextBankingCashBag < @BankingCashBagCount
								Begin
									Set @NextBankingCashBag = @NextBankingCashBag + 1

									Insert
										@Output
									Select
										'Banking Bag Number', bcb.SealNumber, 'Slip Number', bcb.SlipNumber
									From
										@BankingCashBag bcb
									Where
										bcb.RID = @NextBankingCashBag

									Insert @Output Select 'Denomination', 'Banked', '', ''
									
									Delete @BankingBagCashDenom
									Insert 
										@BankingBagCashDenom
									Select sbd.ID, sbd.Value					
									From
										SafeBagsDenoms sbd
									Where 
										sbd.BagID = (Select ID From @BankingCashBag Where RID = @NextBankingCashBag)
									Select @Value =	(Select Value From @BankingBagCashDenom Where ID = 100.00)
									Insert @Output Select '100.00',	(Select '£' + Convert(NVarChar(Max), Cast(@Value As SmallMoney), 1)), '', ''
									If Not @Value Is Null
										Begin
											Set @TotalValue = @TotalValue + @Value
										End
									Select @Value =	 (Select Value From @BankingBagCashDenom Where ID = 50.00)
									Insert @Output Select '50.00',	(Select '£' + Convert(NVarChar(Max), Cast(@Value As SmallMoney), 1)), '', ''
									If Not @Value Is Null
										Begin
											Set @TotalValue = @TotalValue + @Value
										End
									Select @Value =	(Select Value From @BankingBagCashDenom Where ID = 20.00)
									Insert @Output Select '20.00',	(Select '£' + Convert(NVarChar(Max), Cast(@Value As SmallMoney), 1)), '', ''
									If Not @Value Is Null
										Begin
											Set @TotalValue = @TotalValue + @Value
										End
									Select @Value =	(Select Value From @BankingBagCashDenom Where ID = 10.00)
									Insert @Output Select '10.00',	(Select '£' + Convert(NVarChar(Max), Cast(@Value As SmallMoney), 1)), '', ''
									If Not @Value Is Null
										Begin
											Set @TotalValue = @TotalValue + @Value
										End
									Select @Value =	(Select Value From @BankingBagCashDenom Where ID = 5.00)
									Insert @Output Select '5.00',	(Select '£' + Convert(NVarChar(Max), Cast(@Value As SmallMoney), 1)), '', ''
									If Not @Value Is Null
										Begin
											Set @TotalValue = @TotalValue + @Value
										End
									Insert @Output Select 'Total', (Select '£' + Convert(NVarChar(Max), Cast(@TotalValue As SmallMoney), 1)), '', ''
									Insert @Output Select '', '', '', ''
									Set @TotalValue = 0
								End
						End

					Insert
						@BankingChequeBag
					Select
						sb.ID, sbd.SlipNumber, sb.SealNumber
					From
						SafeBags sb
							Inner Join
								SafeBagsDenoms sbd
							On
								sbd.BagID = sb.ID
					Where
						sb.PickupPeriodID = @PeriodID
					And
						sb.[Type] = 'B'
					And
						sb.[State] In ('M', 'Q')
					And				
						sbd.TenderID = 2
					Group By
						sb.ID,
						sbd.SlipNumber,
						sb.SealNumber
						
					Select @BankingChequeBagCount = Count(ID) From @BankingChequeBag
					If @BankingChequeBagCount = 0
						Begin
							Insert @Output Select 'Banking Bag Number', '', 'Slip Number', ''
							Insert @Output Select 'Denomination', 'Banked', '', ''	
							Insert @Output Select 'Cheque', '', '', ''
							Insert @Output Select '', '', '', ''
						End
					Else
						Begin
							While @NextBankingChequeBag < @BankingChequeBagCount
								Begin
									Set @NextBankingChequeBag = @NextBankingChequeBag + 1

									Insert
										@Output
									Select
										'Banking Bag Number', bcb.SealNumber, 'Slip Number', bcb.SlipNumber
									From
										@BankingChequeBag bcb
									Where
										bcb.RID = @NextBankingChequeBag

									Insert @Output Select 'Denomination', 'Banked', '', ''
									
									Delete @BankingBagChequeDenom
									Insert 
										@BankingBagChequeDenom
									Select
										sbd.ID,
										sbd.Value					
									From
										SafeBagsDenoms sbd
									Where 
										sbd.BagID = (Select ID From @BankingChequeBag Where RID = @NextBankingChequeBag)
									Insert @Output Select 'Cheque', ('£' + Convert(NVarChar(Max), Cast(Value As SmallMoney), 1)), '', '' From @BankingBagChequeDenom
									Insert @Output Select '', '', '', ''
								End
						End

					-- Fill NoneCash Tenders
							
					Insert @NoneCashTenders
					Select TenderID, DisplayText
					From SystemCurrencyDen			
					Where TenderID Not In (1, 2)
						And CurrencyID = 'GBP'
					Order By TenderID
			
					Select Top 1 @TenderID = TenderID, @TenderName = TenderName From @NoneCashTenders
					While (@@rowcount > 0)
					Begin
						Set @TotalValue = 0

						Select @TotalValue = IsNull(Sum(sbdOut.Value),0)						
						From
							SafeBagsDenoms sbdOut
						Where 
							sbdOut.BagID In 
							(
								Select
									sb.ID
								From
									SafeBags sb
										Inner Join
											SafeBagsDenoms sbd
										On
											sbd.BagID = sb.ID
								Where
									sb.PickupPeriodID = @PeriodID
								And
									sb.[Type] = 'B'
								And
									sb.[State] In ('M', 'Q')
								And				
									sbd.TenderID = @TenderID
								Group By
									sb.ID,
									sbd.SlipNumber,
									sb.SealNumber
							)
							and sbdOut.TenderID = @TenderID					

						Insert @Output 
						Select @TenderName, 
							Case @TotalValue
								When 0 Then ''
								Else ('£' + Convert(NVarChar(Max), Cast(@TotalValue As SmallMoney), 1))
							End, '', '' 	
					  
						Delete from @NoneCashTenders Where @TenderID = TenderID
						Select Top 1 @TenderID = TenderID, @TenderName = TenderName From @NoneCashTenders
					End
					Insert @Output Select '', '', '', ''
					
				End
		End
	Select * From @Output
End
Go

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_NewBanking_BankingReport for US110 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_NewBanking_BankingReport for US110 has not been deployed successfully'
GO
