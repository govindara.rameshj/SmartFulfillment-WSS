IF NOT EXISTS (
select 1 from sys.columns col 
inner join sys.objects obj on obj.object_id = col.object_id
where obj.Name = 'Report' and col.name = 'ShowResetButton'
)
BEGIN
	PRINT 'Add ShowResetButton column to Report'
	ALTER TABLE dbo.Report ADD ShowResetButton bit NULL
END
GO

UPDATE dbo.Report SET ShowResetButton = 
	CASE 
		WHEN Id = 310 THEN 1
		ELSE 0
	END
GO

If @@Error = 0
   Print 'Success: Add ShowResetButton column to Report for US3770 has been deployed successfully'
ELSE
   Print 'Failure: Add ShowResetButton column to Report for US3770 has not been deployed successfully'
GO	