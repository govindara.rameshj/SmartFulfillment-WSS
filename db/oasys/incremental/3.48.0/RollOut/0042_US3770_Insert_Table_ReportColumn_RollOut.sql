IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42560)
	BEGIN
		INSERT INTO dbo.ReportColumn 
				([Id]
				,[Name]
				,[Caption]
				,[FormatType]
				,[Format]
				,[IsImagePath]
				,[MinWidth]
				,[MaxWidth]
				,[Alignment])
		VALUES
			(42560,	'TotalCountItems', 'Total Count Items', 0, NULL, 0,	130, 160, NULL)
	END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42561)
	BEGIN
		INSERT INTO dbo.ReportColumn 
				([Id]
				,[Name]
				,[Caption]
				,[FormatType]
				,[Format]
				,[IsImagePath]
				,[MinWidth]
				,[MaxWidth]
				,[Alignment])
		VALUES
			(42561,	'PhysicallyCountedItems', 'Physically Counted Items', 0, NULL, 0,	130, 160, NULL)
	END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42562)
	BEGIN
		INSERT INTO dbo.ReportColumn 
				([Id]
				,[Name]
				,[Caption]
				,[FormatType]
				,[Format]
				,[IsImagePath]
				,[MinWidth]
				,[MaxWidth]
				,[Alignment])
		VALUES
			(42562,	'AuthorisedBy', 'Authorised By', 0, NULL, 0,	130, 160, NULL)
	END
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportColumn for US3770 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportColumn for US3770 has not been deployed successfully'
Go