IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE Id = 620)
BEGIN
	INSERT INTO dbo.Report (
		[Id],
		[Header],
		[Title],
		[ProcedureName],
		[HideWhenNoData],
		[PrintLandscape],
		[MinWidth],
		[MinHeight])
	VALUES (620, 'Stock Administration Report', 'PIC Count History', 'StockGetPicCountHistory', 0, 0, NULL, NULL)
END
GO

If @@Error = 0
   Print 'Success: Insert into Table Report for US954 has been deployed successfully'
Else
   Print 'Failure: Insert into Table Report for US954 has not been deployed successfully'
Go