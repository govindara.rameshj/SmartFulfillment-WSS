UPDATE dbo.MenuConfig
SET 
Parameters = '71',
ClassName = 'Reporting.ReportViewer'
WHERE ID IN (11540, 7330)
GO

If @@Error = 0
   Print 'Success: Update Table MenuConfig for US118 has been deployed successfully'
Else
   Print 'Failure: Update Table MenuConfig for US118 has not been deployed successfully'
Go
