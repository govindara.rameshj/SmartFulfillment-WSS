IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[TenderTypeGetTypes]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure TenderTypeGetTypes'
	EXEC ('CREATE PROCEDURE dbo.TenderTypeGetTypes AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [TenderTypeGetTypes]')
GO

ALTER procedure [dbo].[TenderTypeGetTypes]
AS
BEGIN
	declare @Table TABLE ( Id char(6), TenderDescription char(30) )

	insert into @Table values (null, '---All---');
	insert into @Table select TTID, TTDE from dbo.TENCTL WHERE IUSE = 1 AND TTID NOT IN (8, 9) UNION select TenderID, DisplayText FROM SystemCurrencyDen WHERE TenderID IN (10, 12)
	
	UPDATE @Table SET TenderDescription = 'Credit/Debit Card' WHERE TenderDescription LIKE 'Cr/Dr Card'
	
	select * from @Table
END
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure TenderTypeGetTypes for US113 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure TenderTypeGetTypes for US113 has not been deployed successfully'
GO
