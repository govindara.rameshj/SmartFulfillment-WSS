/****** Script for SelectTopNRows command from SSMS  ******/
DELETE FROM dbo.ReportColumns
WHERE ReportId = 620 AND ColumnId = 2013
GO

UPDATE dbo.ReportColumns SET Sequence = 5
WHERE ReportId = 620 AND ColumnId = 2014
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 620 AND ColumnId = 42560)
INSERT INTO dbo.ReportColumns 
		([ReportId]
		,[TableId]
		,[ColumnId]
		,[Sequence]
		,[IsVisible]
		,[IsBold]
		,[IsHighlight]
		,[HighlightColour]
		,[Fontsize])
VALUES 
	(620, 1, 42560,	2, 1, 0, NULL, NULL, NULL)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 620 AND ColumnId = 42561)
INSERT INTO dbo.ReportColumns 
		([ReportId]
		,[TableId]
		,[ColumnId]
		,[Sequence]
		,[IsVisible]
		,[IsBold]
		,[IsHighlight]
		,[HighlightColour]
		,[Fontsize])
VALUES 
	(620, 1, 42561,	3, 1, 0, NULL, NULL, NULL)
GO


IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 620 AND ColumnId = 42562)
INSERT INTO dbo.ReportColumns 
		([ReportId]
		,[TableId]
		,[ColumnId]
		,[Sequence]
		,[IsVisible]
		,[IsBold]
		,[IsHighlight]
		,[HighlightColour]
		,[Fontsize])
VALUES 
	(620, 1, 42562,	4, 1, 0, NULL, NULL, NULL)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportColumns for US3770 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportColumns for US3770 has not been deployed successfully'
Go