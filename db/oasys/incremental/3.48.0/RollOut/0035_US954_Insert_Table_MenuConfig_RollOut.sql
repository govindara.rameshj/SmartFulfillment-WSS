IF NOT EXISTS (SELECT 1 FROM dbo.MenuConfig WHERE ID = 11550)
BEGIN
	INSERT INTO dbo.MenuConfig (
		[ID],
		[MasterID],
		[AppName],
		[AssemblyName],
		[ClassName],
		[MenuType],
		[Parameters],
		[ImagePath],
		[LoadMaximised],
		[IsModal],
		[DisplaySequence],
		[AllowMultiple],
		[WaitForExit],
		[TabName],
		[Description],
		[ImageKey],
		[Timeout],
		[DoProcessingType])
	VALUES
		(11550, 11300, 'PIC Count History', 'Reporting.dll', 'Reporting.ReportViewer', 4, 620, 'icons\icon_g.ico', 1, 1, 6, 0, 0, NULL, NULL, 0, 0, 1)
END

IF NOT EXISTS (SELECT 1 FROM dbo.MenuConfig WHERE ID = 7340)
BEGIN
	INSERT INTO dbo.MenuConfig (
		[ID],
		[MasterID],
		[AppName],
		[AssemblyName],
		[ClassName],
		[MenuType],
		[Parameters],
		[ImagePath],
		[LoadMaximised],
		[IsModal],
		[DisplaySequence],
		[AllowMultiple],
		[WaitForExit],
		[TabName],
		[Description],
		[ImageKey],
		[Timeout],
		[DoProcessingType])
	VALUES
		(7340, 7100, 'PIC Count History', 'Reporting.dll', 'Reporting.ReportViewer', 4, 620, 'icons\icon_g.ico', 1, 1, 6, 0, 0, NULL, NULL, 0, 0, 1)
END

If @@Error = 0
   Print 'Success: The Insert into table MenuConfig for US954 has been deployed successfully'
Else
   Print 'Failure: The Insert into table MenuConfig for US954 has not been deployed successfully'
GO