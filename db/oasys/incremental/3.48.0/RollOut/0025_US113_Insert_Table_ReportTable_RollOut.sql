IF NOT EXISTS(SELECT 1 FROM dbo.ReportTable WHERE ReportId = 310 AND Id = 4)
	INSERT INTO dbo.ReportTable 
			([ReportId],
			[Id],
			[Name],
			[AllowGrouping],
			[AllowSelection],
			[ShowHeaders],
			[ShowIndicator],
			[ShowLinesVertical],
			[ShowLinesHorizontal],
			[ShowBorder],
			[ShowInPrintOnly],
			[AutoFitColumns],
			[HeaderHeight],
			[RowHeight],
			[AnchorTop],
			[AnchorBottom])
	VALUES (310, 4, 'Original Receipt Details', 0, 0, 1, 1, 1, 1, 1, 0, 1, NULL, NULL, 1, 1)
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportTable for US113 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportTable for US113 has not been deployed successfully'
Go