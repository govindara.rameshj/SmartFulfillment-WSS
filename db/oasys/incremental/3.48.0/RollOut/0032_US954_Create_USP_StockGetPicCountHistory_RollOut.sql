IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'dbo.[StockGetPicCountHistory]') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure StockGetPicCountHistory'
	EXEC ('CREATE PROCEDURE dbo.StockGetPicCountHistory AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure [StockGetPicCountHistory]')
GO

ALTER procedure [dbo].[StockGetPicCountHistory]
AS
BEGIN
	SELECT
		hh.DATE1 AS 'Date',
		hh.NUMB AS 'Total Count Items',
		hh.DONE AS'Physicaly Counted Items',
		hh.AUTH AS 'Authorised by',
		hh.IADJ AS 'IsAdjusted'
	FROM dbo.HHTHDR hh
	WHERE (hh.DATE1 >= DATEADD(MONTH, -3, CONVERT (DATETIME, GETDATE())))
	ORDER BY DATE1 DESC
END

GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure StockGetPicCountHistory for US954 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure StockGetPicCountHistory for US954 has not been deployed successfully'
GO