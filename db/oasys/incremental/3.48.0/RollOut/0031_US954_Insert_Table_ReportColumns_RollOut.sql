IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 620 AND TableId = 1 AND ColumnId = 2500)
BEGIN
	INSERT INTO dbo.ReportColumns (
		[ReportId],
		[TableId],
		[ColumnId],
		[Sequence],
		[IsVisible],
		[IsBold],
		[IsHighlight],
		[HighlightColour],
		[Fontsize])
	VALUES (620, 1, 2500, 1, 1, 0, NULL, NULL, NULL)
END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 620 AND TableId = 1 AND ColumnId = 2013)
BEGIN
	INSERT INTO dbo.ReportColumns (
		[ReportId],
		[TableId],
		[ColumnId],
		[Sequence],
		[IsVisible],
		[IsBold],
		[IsHighlight],
		[HighlightColour],
		[Fontsize])
	VALUES (620, 1, 2013, 2, 1, 0, NULL, NULL, NULL)
END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumns WHERE ReportId = 620 AND TableId = 1 AND ColumnId = 2014)
BEGIN
	INSERT INTO dbo.ReportColumns (
		[ReportId],
		[TableId],
		[ColumnId],
		[Sequence],
		[IsVisible],
		[IsBold],
		[IsHighlight],
		[HighlightColour],
		[Fontsize])
	VALUES (620, 1, 2014, 3, 1, 0, NULL, NULL, NULL)
END
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportTable for US954 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportTable for US954 has not been deployed successfully'
Go
