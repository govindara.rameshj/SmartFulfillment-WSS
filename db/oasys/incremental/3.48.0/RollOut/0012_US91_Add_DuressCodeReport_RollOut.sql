
PRINT 'Start to create Duress Code Report'

BEGIN TRANSACTION;
BEGIN TRY

	PRINT 'Creating Duress Code Report'

	IF OBJECT_ID ('tempdb..#Temp_Report') IS NOT NULL DROP TABLE #Temp_Report;
	 
	CREATE TABLE #Temp_Report
	(  	
		[Id] [int] NOT NULL,
		[Header] [varchar](50) NULL,
		[Title] [varchar](50) NULL,
		[ProcedureName] [varchar](50) NULL,
		[HideWhenNoData] [bit] NOT NULL,
		[PrintLandscape] [bit] NOT NULL,
		[MinWidth] [int] NULL,
		[MinHeight] [int] NULL
	 );
	 
	 
	INSERT INTO #Temp_Report 
	(
		Id, 
		Header,		
		Title,					
		ProcedureName,			
		HideWhenNoData, 
		PrintLandscape, 
		MinWidth, 
		MinHeight
	)
	VALUES 
	(
		321,	
		'Sales',	
		'Duress Code Report',	
		'DuressCodeReportGet', 
		0,
		1,
		NULL,
		NULL
	)


	MERGE  [dbo].[Report] AS Target
	 USING #Temp_Report AS Source
		ON Target.Id = Source.Id
	WHEN MATCHED
	THEN
	   UPDATE SET
		  Target.Header = Source.Header
		 ,Target.Title = Source.Title
		 ,Target.ProcedureName = Source.ProcedureName
		 ,Target.HideWhenNoData = Source.HideWhenNoData
		 ,Target.PrintLandscape = Source.PrintLandscape
		 ,Target.MinWidth = Source.MinWidth
		 ,Target.MinHeight = Source.MinHeight     
	WHEN NOT MATCHED BY TARGET
	THEN
	   INSERT 
	   (
			Id, 
			Header,		
			Title,					
			ProcedureName,			
			HideWhenNoData, 
			PrintLandscape, 
			MinWidth, 
			MinHeight
		)
	   VALUES 
	   (
			Source.Id, 
			Source.Header,		
			Source.Title,					
			Source.ProcedureName,			
			Source.HideWhenNoData, 
			Source.PrintLandscape, 
			Source.MinWidth, 
			Source.MinHeight
		);

	IF OBJECT_ID ('tempdb..#Temp_Report') IS NOT NULL DROP TABLE #Temp_Report;		
		
	PRINT 'Creating Duress Code Report parameters'

	IF OBJECT_ID ('tempdb..#Temp_ReportParameters') IS NOT NULL DROP TABLE #Temp_ReportParameters;
	 
	CREATE TABLE #Temp_ReportParameters
	(  	
		[ReportId] [int] NOT NULL,
		[ParameterId] [int] NOT NULL,
		[Sequence] [int] NOT NULL,
		[AllowMultiple] [bit] NOT NULL,
		[DefaultValue] [varchar](100) NULL
	 );
	 
	 
	INSERT INTO #Temp_ReportParameters (
			ReportId, 
			ParameterId, 
			Sequence, 
			AllowMultiple, 
			DefaultValue
		)
	VALUES 
	(
		321,	
		102,	
		1,	
		0,
		NULL
	),
	(
		321,	
		103,	
		2,	
		0,
		NULL
	)


	MERGE  [dbo].[ReportParameters] AS Target
	 USING #Temp_ReportParameters AS Source
		ON Target.ReportId = Source.ReportId AND Target.ParameterId = Source.ParameterId
	WHEN MATCHED
	THEN
	   UPDATE SET
		  Target.Sequence = Source.Sequence
		 ,Target.AllowMultiple = Source.AllowMultiple
		 ,Target.DefaultValue = Source.DefaultValue
	WHEN NOT MATCHED BY TARGET
	THEN
	   INSERT 
	   (	
			ReportId, 
			ParameterId, 
			Sequence, 
			AllowMultiple, 
			DefaultValue
		)
	   VALUES 
	   (
   			Source.ReportId, 
			Source.ParameterId, 
			Source.Sequence, 
			Source.AllowMultiple, 
			Source.DefaultValue
		);
		
	IF OBJECT_ID ('tempdb..#Temp_ReportParameters') IS NOT NULL DROP TABLE #Temp_ReportParameters;
	
	PRINT 'Creating Duress Code Report record in ReportTable'

	IF OBJECT_ID ('tempdb..#Temp_ReportTable') IS NOT NULL DROP TABLE #Temp_ReportTable;
	 
	CREATE TABLE #Temp_ReportTable
	(  	
		[ReportId] [int] NOT NULL,
		[Id] [int] NOT NULL,
		[Name] [varchar](50) NOT NULL,
		[AllowGrouping] [bit] NOT NULL,
		[AllowSelection] [bit] NOT NULL,
		[ShowHeaders] [bit] NOT NULL,
		[ShowIndicator] [bit] NOT NULL,
		[ShowLinesVertical] [bit] NOT NULL,
		[ShowLinesHorizontal] [bit] NOT NULL,
		[ShowBorder] [bit] NOT NULL,
		[ShowInPrintOnly] [bit] NOT NULL,
		[AutoFitColumns] [bit] NOT NULL,
		[HeaderHeight] [int] NULL,
		[RowHeight] [int] NULL,
		[AnchorTop] [bit] NOT NULL,
		[AnchorBottom] [bit] NOT NULL
	 );
	 
	INSERT INTO #Temp_ReportTable 
	(
		ReportId, 
		Id, 
		Name, 
		AllowGrouping, 
		AllowSelection, 
		ShowHeaders, 
		ShowIndicator, 
		ShowLinesVertical, 
		ShowLinesHorizontal, 
		ShowBorder, 
		ShowInPrintOnly, 
		AutoFitColumns, 
		HeaderHeight, 
		RowHeight, 
		AnchorTop, 
		AnchorBottom
	)
	VALUES 
	(
		321,
		1,	
		'Master',
		1,	
		0,	
		1,	
		1,	
		1,	
		1,	
		1,	
		0,	
		1,
		35,
		NULL,
		1,
		1
	)
	
	MERGE  [dbo].[ReportTable] AS Target
	 USING #Temp_ReportTable AS Source
		ON Target.ReportId = Source.ReportId
	WHEN MATCHED
	THEN
	   UPDATE SET
		  Target.Id = Source.Id
		 ,Target.Name = Source.Name
		 ,Target.AllowGrouping = Source.AllowGrouping
		 ,Target.AllowSelection = Source.AllowSelection
		 ,Target.ShowHeaders = Source.ShowHeaders
		 ,Target.ShowIndicator = Source.ShowIndicator
		 ,Target.ShowLinesVertical = Source.ShowLinesVertical
		 ,Target.ShowLinesHorizontal = Source.ShowLinesHorizontal
		 ,Target.ShowBorder = Source.ShowBorder
		 ,Target.ShowInPrintOnly = Source.ShowInPrintOnly
		 ,Target.AutoFitColumns = Source.AutoFitColumns
		 ,Target.HeaderHeight = Source.HeaderHeight
		 ,Target.RowHeight = Source.RowHeight
		 ,Target.AnchorTop = Source.AnchorTop
		 ,Target.AnchorBottom = Source.AnchorBottom
	WHEN NOT MATCHED BY TARGET
	THEN
	   INSERT 
	   (
			ReportId, 
			Id, 
			Name, 
			AllowGrouping, 
			AllowSelection, 
			ShowHeaders, 
			ShowIndicator, 
			ShowLinesVertical, 
			ShowLinesHorizontal, 
			ShowBorder, 
			ShowInPrintOnly, 
			AutoFitColumns, 
			HeaderHeight, 
			RowHeight, 
			AnchorTop, 
			AnchorBottom	
	   )
	   VALUES 
	   (
   			Source.ReportId, 
   			Source.Id,
			Source.Name ,
			Source.AllowGrouping,
			Source.AllowSelection,
			Source.ShowHeaders,
			Source.ShowIndicator,
			Source.ShowLinesVertical,
			Source.ShowLinesHorizontal,
			Source.ShowBorder,
			Source.ShowInPrintOnly,
			Source.AutoFitColumns,
			Source.HeaderHeight,
			Source.RowHeight,
			Source.AnchorTop,
			Source.AnchorBottom
		);
	
	IF OBJECT_ID ('tempdb..#Temp_ReportTable') IS NOT NULL DROP TABLE #Temp_ReportTable;
	
	PRINT 'Creating records in MenuConfig'

	IF OBJECT_ID ('tempdb..#Temp_MenuConfig') IS NOT NULL DROP TABLE #Temp_MenuConfig;
	 
	CREATE TABLE #Temp_MenuConfig
	(  	
		[ID] [int] NOT NULL,
		[MasterID] [int] NOT NULL,
		[AppName] [varchar](100) NOT NULL,
		[AssemblyName] [varchar](100) NULL,
		[ClassName] [varchar](100) NULL,
		[MenuType] [int] NOT NULL,
		[Parameters] [varchar](250) NULL,
		[ImagePath] [char](50) NULL,
		[LoadMaximised] [bit] NOT NULL,
		[IsModal] [bit] NOT NULL,
		[DisplaySequence] [int] NOT NULL,
		[AllowMultiple] [bit] NOT NULL,
		[WaitForExit] [bit] NOT NULL,
		[TabName] [varchar](100) NULL,
		[Description] [varchar](100) NULL,
		[ImageKey] [char](20) NULL,
		[Timeout] [int] NOT NULL,
		[DoProcessingType] [int] NOT NULL
	 );
	 
	 	 
	INSERT INTO #Temp_MenuConfig 
	(
		ID, 
		MasterID, 
		AppName, 
		AssemblyName, 
		ClassName, 
		MenuType, 
		[Parameters], 
		ImagePath, 
		LoadMaximised, 
		IsModal, 
		DisplaySequence, 
		AllowMultiple, 
		WaitForExit, 
		TabName, 
		[Description], 
		ImageKey, 
		[Timeout], 
		DoProcessingType
	)
	VALUES 
	(
		3381,	
		11700,	
		'Duress Code Report',	
		'Reporting.dll',
		'Reporting.ReportViewer',
		4,
		321,
		'icons\icon_g.ico',
		1,
		1,
		9,
		0,
		0,
		NULL,
		NULL,
		0,
		0,
		0
	)
	
	MERGE  [dbo].[MenuConfig] AS Target
	 USING #Temp_MenuConfig AS Source
		ON Target.ID = Source.ID 
	WHEN MATCHED
	THEN
	   UPDATE SET
		Target.MasterID =			Source.MasterID, 
		Target.AppName =			Source.AppName, 
		Target.AssemblyName =		Source.AssemblyName, 
		Target.ClassName =			Source.ClassName, 
		Target.MenuType =			Source.MenuType, 
		Target.[Parameters] =		Source.[Parameters], 
		Target.ImagePath =			Source.ImagePath, 
		Target.LoadMaximised =		Source.LoadMaximised, 
		Target.IsModal =			Source.IsModal, 
		Target.DisplaySequence =	Source.DisplaySequence, 
		Target.AllowMultiple =		Source.AllowMultiple, 
		Target.WaitForExit =		Source.WaitForExit, 
		Target.TabName =			Source.TabName, 
		Target.[Description] =		Source.[Description], 
		Target.ImageKey =			Source.ImageKey, 
		Target.[Timeout] =			Source.[Timeout], 
		Target.DoProcessingType =	Source.DoProcessingType
	WHEN NOT MATCHED BY TARGET
	THEN
	   INSERT 
	   (	
			ID, 
			MasterID, 
			AppName, 
			AssemblyName, 
			ClassName, 
			MenuType, 
			[Parameters], 
			ImagePath, 
			LoadMaximised, 
			IsModal, 
			DisplaySequence, 
			AllowMultiple, 
			WaitForExit, 
			TabName, 
			[Description], 
			ImageKey, 
			[Timeout], 
			DoProcessingType
		)
	   VALUES 
	   (
			Source.ID,
			Source.MasterID, 
			Source.AppName, 
			Source.AssemblyName, 
			Source.ClassName, 
			Source.MenuType, 
			Source.[Parameters], 
			Source.ImagePath, 
			Source.LoadMaximised, 
			Source.IsModal, 
			Source.DisplaySequence,
			Source.AllowMultiple, 
			Source.WaitForExit, 
			Source.TabName, 
			Source.[Description], 
			Source.ImageKey, 
			Source.[Timeout], 
			Source.DoProcessingType
		);
		
	IF OBJECT_ID ('tempdb..#Temp_MenuConfig') IS NOT NULL DROP TABLE #Temp_MenuConfig;

	PRINT 'Creating records in ProfilemenuAccess'

	IF OBJECT_ID ('tempdb..#Temp_ProfilemenuAccess') IS NOT NULL DROP TABLE #Temp_ProfilemenuAccess;
	 
	CREATE TABLE #Temp_ProfilemenuAccess
	(  	
		[ID] [int] NOT NULL,
		[MenuConfigID] [int] NOT NULL,
		[AccessAllowed] [bit] NOT NULL,
		[OverrideAllowed] [bit] NOT NULL,
		[SecurityLevel] [tinyint] NOT NULL,
		[IsManager] [bit] NOT NULL,
		[IsSupervisor] [bit] NOT NULL,
		[LastEdited] [datetime] NULL,
		[Parameters] [varchar](100) NULL
	 );
	 
	 
	INSERT INTO #Temp_ProfilemenuAccess 
	(
		ID, 
		MenuConfigID, 
		AccessAllowed, 
		OverrideAllowed, 
		SecurityLevel, 
		IsManager, 
		IsSupervisor, 
		LastEdited, 
		[Parameters]
	)
	VALUES 
		(13, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(15, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(16, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(17, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(18, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(19, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(20, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(21, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(22, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(23, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(24, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(25, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(27, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(30, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(31, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(32, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(33, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(40, 3381, 1, 0, 1, 0, 0, Null, Null), 
		(109, 3381, 1, 0, 9, 0, 0, Null, Null), 
		(110, 3381, 1, 0, 10, 0, 0, Null, Null), 
		(199, 3381, 1, 0, 99, 0, 0, Null, Null) 
	MERGE  [dbo].[ProfilemenuAccess] AS Target
	 USING #Temp_ProfilemenuAccess AS Source
		ON Target.ID = Source.ID AND Target.MenuConfigID = Source.MenuConfigID
	WHEN MATCHED
	THEN
	   UPDATE SET
		  Target.AccessAllowed = Source.AccessAllowed
		 ,Target.OverrideAllowed = Source.OverrideAllowed
		 ,Target.SecurityLevel = Source.SecurityLevel
		 ,Target.IsManager = Source.IsManager
		 ,Target.IsSupervisor = Source.IsSupervisor
		 ,Target.LastEdited = Source.LastEdited
		 ,Target.[Parameters] = Source.[Parameters]
	WHEN NOT MATCHED BY TARGET
	THEN
	   INSERT 
	   (	
		ID, 
		MenuConfigID, 
		AccessAllowed, 
		OverrideAllowed, 
		SecurityLevel, 
		IsManager, 
		IsSupervisor, 
		LastEdited, 
		[Parameters]
		)
	   VALUES 
	   (
	   	Source.ID, 
		Source.MenuConfigID, 
		Source.AccessAllowed, 
		Source.OverrideAllowed, 
		Source.SecurityLevel, 
		Source.IsManager, 
		Source.IsSupervisor, 
		Source.LastEdited, 
		Source.[Parameters]
		);
		

	PRINT 'Creating records in ReportHyperlink for dashboard'

	IF OBJECT_ID ('tempdb..#Temp_ReportHyperlink') IS NOT NULL DROP TABLE #Temp_ReportHyperlink;
	 
	CREATE TABLE #Temp_ReportHyperlink
	(  	
		[ReportId] [int] NOT NULL,
		[TableId] [int] NOT NULL,
		[RowId] [int] NOT NULL,
		[ColumnName] [varchar](50) NOT NULL,
		[Value] [varchar](200) NOT NULL
	 );
	 
	 
	INSERT INTO #Temp_ReportHyperlink 
	(
		ReportId, 
		TableId, 
		RowId, 
		ColumnName, 
		Value
	)
	VALUES 
	(
		9,
		1,
		2,
		'Description',
		'ReportId=321|[SelectedDate]|[SelectedDate]'
	)
	
	MERGE  [dbo].[ReportHyperlink] AS Target
	 USING #Temp_ReportHyperlink AS Source
		ON Target.ReportId = Source.ReportId 
			AND Target.TableId = Source.TableId
			AND Target.RowId = Source.RowId
	WHEN MATCHED
	THEN
	   UPDATE SET
		  Target.ColumnName = Source.ColumnName,
		  Target.Value = Source.Value
	WHEN NOT MATCHED BY TARGET
	THEN
	   INSERT 
	   (	
			ReportId, 
			TableId, 
			RowId, 
			ColumnName, 
			Value
	   )
	   VALUES 
	   (
   			Source.ReportId, 
			Source.TableId, 
			Source.RowId, 
			Source.ColumnName, 
			Source.Value
		);	
		
		
	IF OBJECT_ID ('tempdb..#Temp_ReportHyperlink') IS NOT NULL DROP TABLE #Temp_ReportHyperlink;		

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION;
		PRINT 'Failure: Duress Code Report for US91 has not been deployed'
		PRINT 'ErrorNumber = ' + CONVERT(varchar, ERROR_NUMBER()) 
			+ ', ErrorSeverity = ' +  CONVERT(varchar, ERROR_SEVERITY())
			+ ', ErrorState = ' + CONVERT(varchar, ERROR_STATE())
			+ ', ErrorLine = ' + CONVERT(varchar, ERROR_LINE())
			+ ', ErrorMessage = ' + ERROR_MESSAGE()
	END
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
	COMMIT TRANSACTION;
	PRINT 'Success: Duress Code Report for US91 has been deployed successfully'
END
GO