IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42570)
	BEGIN
		INSERT INTO dbo.ReportColumn 
		VALUES (42570, 'IsOffline', 'Is Offline', 0, NULL, 0, NULL, 65, NULL)
	END
GO

IF NOT EXISTS (SELECT 1 FROM dbo.ReportColumn WHERE Id = 42571)
	BEGIN  
		INSERT INTO dbo.ReportColumn 
		VALUES (42571, 'Void', 'Void', 0, NULL, 0, NULL, 65, NULL)
	END
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportColumn for US3914 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportColumn for US3914 has not been deployed successfully'
Go