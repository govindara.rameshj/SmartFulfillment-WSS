UPDATE dbo.ReportColumns
SET ColumnId = 4032
WHERE ReportId = 201 and ColumnId = 101

If @@Error = 0
   Print 'Success: Update Table ReportColumns for DE562 has been deployed successfully'
Else
   Print 'Failure: Update Table ReportColumns for DE562 has not been deployed successfully'
Go