UPDATE dbo.ReportColumns
SET IsVisible = 0
WHERE ColumnId IN (6000, 1240, 1215, 156, 2001, 1216) AND ReportId = 310

If @@Error = 0
   Print 'Success: Update Table ReportColumns for US113 has been deployed successfully'
Else
   Print 'Failure: Update Table ReportColumns for US113 has not been deployed successfully'
Go