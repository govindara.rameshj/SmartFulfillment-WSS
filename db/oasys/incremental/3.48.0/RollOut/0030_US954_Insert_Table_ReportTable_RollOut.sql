IF NOT EXISTS (SELECT 1 FROM dbo.ReportTable WHERE ReportId = 620 AND Id = 1)
BEGIN
	INSERT INTO dbo.ReportTable (
		[ReportId],
		[Id],
		[Name],
		[AllowGrouping],
		[AllowSelection],
		[ShowHeaders],
		[ShowIndicator],
		[ShowLinesVertical],
		[ShowLinesHorizontal],
		[ShowBorder],
		[ShowInPrintOnly],
		[AutoFitColumns],
		[HeaderHeight],
		[RowHeight],
		[AnchorTop],
		[AnchorBottom])
	VALUES (620, 1, 'PIC Count History', 0, 0, 1, 1, 1, 1, 1, 0, 0, NULL, NULL, 1, 1)
END
GO

If @@Error = 0
   Print 'Success: Insert into Table ReportTable for US954 has been deployed successfully'
Else
   Print 'Failure: Insert into Table ReportTable for US954 has not been deployed successfully'
Go