create procedure udp_TrimDatabaseQuotes
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 10: Quotes tidy...'

	if exists
	(
	select *
	from tempdb.dbo.sysobjects
	where ID = OBJECT_ID(N'tempdb..#quohdr')
	)
	begin
		drop table #quohdr
	end
	
	print '    Selecting QUOHDR...'
	
	set rowcount @maxRowAffected;
	
	select NUMB into #quohdr from QUOHDR where EXPD < @OlderThan;

	set rowcount 0;

	begin try

		set @statusMsg = '10a: transaction';
		print '    Starting '+@statusMsg+'...'
		
		begin transaction

			set @statusMsg = '10b: delete from QUOLIN';
			print '    Starting '+@statusMsg+'...'

			delete from QUOLIN where NUMB in (select NUMB from #quohdr)
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
			set @statusMsg = '10c: delete from QUOHDR';
			print '    Starting '+@statusMsg+'...'
			
			delete from QUOHDR where NUMB in (select NUMB from #quohdr)
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);			

			set @statusMsg = '10d: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
			
		set @errorMsg = 'Error performing '+@errorMsg
		raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	drop table #quohdr
	
	set @statusMsg = '';
	print 'Completed 10: Quotes tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end	

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseQuotes for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseQuotes for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseQuotes To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseQuotes for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseQuotes for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseQuotes to [role_execproc]

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseQuotes for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseQuotes for HouseKeeping has NOT been successfully deployed'
go
