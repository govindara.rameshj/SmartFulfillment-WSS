create procedure udp_TrimDatabaseQuoteOrderDeliveries
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 4: Quote Order Deliveries tidy...'

	begin try
	
		set @statusMsg = '4a: drop table';
		print '    Starting '+@statusMsg+'...'
		
		if exists
		(
		select *
		from tempdb.dbo.sysobjects
		where ID = OBJECT_ID(N'tempdb..#QuoteOrderDelivery')
		)
		begin
			drop table #QuoteOrderDelivery
		end
		
		set rowcount @maxRowAffected;
		
		-- KM Login to be > 999, IsSuspended and RefundStatus added and join on CORHDR
		
		select ch4.NUMB
		into #QuoteOrderDelivery
		from CORHDR4 ch4
		join CORHDR ch ON ch.NUMB = ch4.NUMB
		where ch4.DATE1 < @OlderThan
		and ch4.DeliveryStatus > 999
		and ch4.IsSuspended = 0
		and ch4.RefundStatus >= 199
		and ch.delc = 0
		
		set rowcount 0;

		set @statusMsg = '4a: transaction';
		print '    Starting '+@statusMsg+'...'
			
		begin transaction
		
			set @statusMsg = '4b: delete from CORHDR';
			print '    Starting '+@statusMsg+'...'
			
			delete CORHDR
			from CORHDR a
			inner join #QuoteOrderDelivery b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '4c: delete from CORHDR4';
			print '    Starting '+@statusMsg+'...'
			
			delete CORHDR4
			from CORHDR4 a
			inner join #QuoteOrderDelivery b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '4d: delete from CORHDR5';
			print '    Starting '+@statusMsg+'...'
			
			delete CORHDR5
			from CORHDR5 a
			inner join #QuoteOrderDelivery b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '4e: delete from CORLIN';
			print '    Starting '+@statusMsg+'...'
			
			delete CORLIN
			from CORLIN a
			inner join #QuoteOrderDelivery b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '4f: delete from CORLIN2';
			print '    Starting '+@statusMsg+'...'
			
			delete CORLIN2
			from CORLIN2 a
			inner join #QuoteOrderDelivery b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '4g: delete from CORREFUND';
			print '    Starting '+@statusMsg+'...'
			
			delete CORREFUND
			from CORREFUND a
			inner join #QuoteOrderDelivery b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '4h: delete from CORTXT';
			print '    Starting '+@statusMsg+'...'
			
			delete CORTXT
			from CORTXT a
			inner join #QuoteOrderDelivery b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
			set @statusMsg = '4i: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
				
		set @errorMsg = 'Error performing '+@errorMsg
			raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	drop table #QuoteOrderDelivery;
	
	print 'Completed 4: Quote Order Deliveries tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseQuoteOrderDeliveries for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseQuoteOrderDeliveries for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseQuoteOrderDeliveries To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseQuoteOrderDeliveries for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseQuoteOrderDeliveries for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseQuoteOrderDeliveries to [role_execproc]

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseQuoteOrderDeliveries for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseQuoteOrderDeliveries for HouseKeeping has NOT been successfully deployed'
go
