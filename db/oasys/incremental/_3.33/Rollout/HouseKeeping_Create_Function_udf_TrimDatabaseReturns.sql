create procedure udp_TrimDatabaseReturns
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 11: Returns tidy...'

	if exists
	(
	select *
	from tempdb.dbo.sysobjects
	where ID = OBJECT_ID(N'tempdb..#Returns')
	)
	begin
		drop table #Returns
	end
	
	print 'Selecting RETHDR...'
	
	set rowcount @maxRowAffected;
	
	-- KM Added IsDeleted contraint
	
	select TKEY into #Returns from RETHDR where RDAT < @OlderThan and IsDeleted = 1

	set rowcount 0;

	begin try

		set @statusMsg = '11a: transaction';
		print '    Starting '+@statusMsg+'...'
		
		begin transaction

			set @statusMsg = '11b: delete from RETHDR';
			print '    Starting '+@statusMsg+'...'

			delete RETHDR
			from RETHDR a
			inner join #Returns b
				  on  b.TKEY = a.TKEY
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
			set @statusMsg = '11c: delete from RETLIN';
			print '    Starting '+@statusMsg+'...'
			
			delete RETLIN
			from RETLIN a
			inner join #Returns b
				  on  b.TKEY = a.HKEY
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '11d: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
			
		set @errorMsg = 'Error performing '+@errorMsg
		raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	drop table #Returns
	
	set @statusMsg = '';
	print 'Completed 11: Returns tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end	

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseReturns for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseReturns for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseReturns To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseReturns for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseReturns for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseReturns to [role_execproc]

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseReturns for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseReturns for HouseKeeping has NOT been successfully deployed'
go
