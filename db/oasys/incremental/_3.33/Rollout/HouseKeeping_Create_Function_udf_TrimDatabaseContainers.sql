create procedure udp_TrimDatabaseContainers
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 8: Containers tidy...'

	if exists
	(
	select *
	from tempdb.dbo.sysobjects
	where ID = OBJECT_ID(N'tempdb..#Containers')
	)
	begin
		drop table #Containers
	end
	
	print '    Selecting CONSUM...'
	
	set rowcount @maxRowAffected;
	
	-- KM Added constraint of irec = 1
	
	select ADEP, CNUM into #Containers from CONSUM where DELD < @OlderThan and irec = 1

	set rowcount 0;

	begin try

		set @statusMsg = '8a: transaction';
		print '    Starting '+@statusMsg+'...'
		
		begin transaction

			set @statusMsg = '8b: delete from CONSUM';
			print '    Starting '+@statusMsg+'...'
			
			delete CONSUM
			from CONSUM a
			inner join #Containers b
				  on  b.ADEP = a.ADEP
				  and b.CNUM = a.CNUM
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '8c: delete from CONDET';
			print '    Starting '+@statusMsg+'...'
			
			delete CONDET
			from CONDET a
			inner join #Containers b
				  on  b.ADEP = a.ADEP
				  and b.CNUM = a.CNUM
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '8d: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
			
		set @errorMsg = 'Error performing '+@errorMsg
		raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	drop table #Containers
	
	set @statusMsg = '';
	print 'Completed 8: Containers tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end	

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseContainers for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseContainers for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseContainers To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseContainers for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseContainers for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseContainers to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseContainers for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseContainers for HouseKeeping has NOT been successfully deployed'
go
