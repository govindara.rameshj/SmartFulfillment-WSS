--drop procedure udp_TrimDatabaseQuotes
--drop procedure udp_TrimDatabaseReceiptsIBTs
--drop procedure udp_TrimDatabaseReturns
--drop procedure udp_TrimDatabaseSales
--drop procedure udp_TrimDatabaseStockMovements
--drop procedure udp_TrimDatabaseQuoteOrderDeliveries
--drop procedure udp_TrimDatabasePurchaseOrders
--drop procedure udp_TrimDatabaseIssues
--drop procedure udp_TrimDatabaseEvents
--drop procedure udp_TrimDatabaseContainers
--drop procedure udp_TrimDatabaseConsignment

alter procedure TrimDatabase
   @ageInMonths int,
   @maxRowAffected int
as
begin
	declare @OlderThan date;
	declare @rowcount int;
	declare @currentRowcount int;
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;

	set nocount on
	
	set @rowcount = @maxRowAffected; 
	set @OlderThan = cast(DATEADD(month, -1 * @ageInMonths, GETDATE()) as date);		
	set @errorSeverity = 18;
		
	print 'Starting housekeeping process...'
		
	while @rowcount >= @maxRowAffected
	begin

		---------------------------------------------------------------------------------------------------------------------------------
		-- Sales
		---------------------------------------------------------------------------------------------------------------------------------
		exec @rowcount = dbo.udp_TrimDatabaseSales @OlderThan,@maxRowAffected
	
		---------------------------------------------------------------------------------------------------------------------------------
		-- Stock Movements
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseStockMovements @OlderThan,@maxRowAffected
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)

		---------------------------------------------------------------------------------------------------------------------------------
		-- Receipts & IBTs
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseReceiptsIBTs @OlderThan,@maxRowAffected
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Quote Order Deliveries
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseQuoteOrderDeliveries @OlderThan,@maxRowAffected
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Purchase Orders
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabasePurchaseOrders @OlderThan,@maxRowAffected
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Issues
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseIssues @OlderThan,@maxRowAffected
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Consignment
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseConsignment @OlderThan,@maxRowAffected
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Containers
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseContainers @OlderThan,@maxRowAffected
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Events
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseEvents @OlderThan,@maxRowAffected	
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Quotes
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseQuotes @OlderThan,@maxRowAffected	
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Returns
		---------------------------------------------------------------------------------------------------------------------------------
		exec @currentRowcount = dbo.udp_TrimDatabaseReturns @OlderThan,@maxRowAffected	
		select @rowcount = dbo.udf_MaxVal(@rowcount,@currentRowcount)
		
		set @statusMsg = cast(@rowcount as varchar(32)) + ' rows affected this iteration'
		print @statusMsg
	end
	
	-- Final Process to Include File Generation for Store System Analysis 
	-- Create a File in Parameter Defined Location (I.e. F:\Userdata\Wix\Install\Housekeep_Process.log) to be used to alert central systems (of TMPFIL additions).

------------------------------------------------------------------------------------------------------
-- MISSING ELEMENTS - 6 Month Retention Data
--Declare @HKDT date
--Declare @HKPD int
--Declare @HKLD char(8)
--Set @HKDT = Oasys.dbo.svf_SystemHouseKeepingDate()
--Set @HKPD = Oasys.dbo.svf_SystemHouseKeepingPeriod()
--Set @HKLD = Oasys.dbo.svf_SystemHouseKeepingLikeDate()
--Print('Housekeeping Date is      : ' + CAST(@HKDT as Char))
--Print('Housekeeping Period is    : ' + CAST(@HKPD as Char))
--Print('Housekeeping Like Date is : ' + CAST(@HKLD as Char))
-- Clear Everything Else!
--Delete From ActivityLog Where LogDate <= @HKDT
--Delete From ACTLOG Where DATE1 <= @HKDT
--Delete From AFDCTL Where AFGN <> '77'
--Delete From AFPCTL Where AFGN <> '77'
--Delete From ALPDES (Deprecated)
--Delete From ALPLOG (Deprecated)
--Delete From CASPER Where DATE1 <= @HKDT
--Delete cc From CBSCAS as cc Inner Join CBSCTL as cb on cb.DATE1 = cc.DATE1 where cc.DATE1 <= @HKDT and cb.COMM = '1' and cb.DONE = '1' (Deprecated)
--Delete From CBSCSA Where DATE1 <= @HKDT and DONE = '1' (Deprecated)
--Delete From CBSCTL Where DATE1 <= @HKDT and COMM = '1' and DONE = '1' (Deprecated)
-- Old Colleague Tables
--Delete From COLCWI where COL_DELE = '1' (Deprecated)
--Delete From COLHOT (Deprecated)
--Delete From COUPON Where DATE1 <= @HKDT and COMM = '1' and DONE = '1'
--Delete From CUSMAS (Deprecated)
--Delete From DHLMAS Where DATE1 <= @HKDT
--Delete From GapWalk Where DateCreated <= @HKDT
--Delete From HHTDET Where DATE1 <= @HKDT
--Delete From HHTHDR Where DATE1 <= @HKDT
--Delete From MODDET (Deprecated)
--Delete From MODMAS (Deprecated)
--Delete From PRCCHG Where PDAT <= @HKDT and PSTA <> 'U'
--Delete From PSSCOP (Deprecated)
--Delete From PSSDCF (Deprecated)
--Delete From PSSINT (Deprecated)
--Delete From PSSMSG (Deprecated)
--Delete From PSSOPT (Deprecated)
--Delete From PSSRNG (Deprecated)
--Delete From PSSSTY (Deprecated)
--Delete From PVLINE (Deprecated)
--Delete From PVPAID (Deprecated)
--Delete From PVTOTS (Deprecated)
--Delete From RMPMAS (Deprecated)
--Delete From CARD_LIST Where DELETED = '1'
--Delete From CARD_SCHEME Where DELETED = '1'
--Delete cbc From CashBalCashier as cbc Inner Join Safe as s on s.PeriodID = cbc.PeriodID Where s.PeriodID <= @HKPD and s.IsClosed = '1'
--Delete cbct From CashBalCashierTen as cbct Inner Join Safe as s on s.PeriodID = cbct.PeriodID Where s.PeriodID <= @HKPD and s.IsClosed = '1'
--Delete cbctv From CashBalCashierTenVar as cbctv Inner Join Safe as s on s.PeriodID = cbctv.PeriodID Where s.PeriodID <= @HKPD and s.IsClosed = '1'
--Delete From GIFHOT Where TEXT like ('%' + @HKLD + '%')
--Delete From HhtLocation where DateCreated <= @HKDT
--Delete From HTRSUM (Deprecated)
--Delete From ITEMPROMPTS Where DateDeleted <= @HKDT
--Delete From MDHMAS (Deprecated)
--Delete From MSGCTL (Deprecated)
--Delete From MSGTXT (Deprecated)
--Delete From NITLOG Where DATE1 <= @HKDT
--Delete From PendingIbtIn (Deprecated)
--Delete From PendingIbtOut (Deprecated)
--Delete From PicExternalAudit Where CountDate <= @HKDT
--Delete From PIMMAS (Deprecated)
--Delete From PLANGRAM Where DELE = '1'
--Delete From PlanNumbers Where IsActive = '0'
--Delete From PlanSegments Where IsActive = '0'
--Delete From PlanStocks Where IsActive = '0'
--Delete From RELITM Where SPOS not in (Select SKUN From STKMAS)
--Delete From Safe Where PeriodID <= @HKPD and IsClosed = '1'
--Delete From SafeBags Where PickupPeriodID <= @HKPD and PickupPeriodID not in (Select PeriodId From Safe)
--Delete From SafeBagsDenoms Where BagID not in (Select ID From SafeBags) 
--Delete From SafeDenoms Where PeriodID not in (Select PeriodID From Safe)
--Delete From SOQSKU Where SKUN not in (Select SKUN From STKMAS)
--Delete From STKHIR Where IDEL = '1'
--Delete From StoreSaleWeight Where DateActive <= @HKDT
--Delete From STRMAS Where DELC = '1'
--Delete From Store Where ID not in (Select NUMB From STRMAS)
--Delete From SUPMAS Where DELC = '1'
--Delete From SUPDET Where SUPN not in (Select SUPN From SUPMAS)
--Delete From SUPNOT Where SUPN not in (Select SUPN From SUPMAS)
--Delete From WSCONFIG (Deprecated)
--Delete From EFTCDF (Deprecated)
--Delete From EFTLOG (Deprecated)
--Delete From EXRATE (Deprecated)
--Delete From AUDMAS Where SKUN not in (Select SKUn From STKMAS)
--Delete From AuditMaster Where AuditDate < @HKDT
--Delete From CYHMAS Where SKUN not in (Select SKUN From STKMAS)
--Delete From STKADJ Where DATE1 < @HKDT --or SKUN not in (Select SKUN From STKMAS)
--Delete From STKWMV Where WEDT < @HKDT --or SKUN not in (Select SKUN From STKMAS)
--Delete From STKTXT Where SKUN not in (Select SKUN From STKMAS)
--Delete From EANMAS Where SKUN not in (Select SKUN From STKMAS)
--Delete From VisionLine Where TranDate <= @HKDT
--Delete From VisionPayment Where TranDate <= @HKDT
--Delete From Vision Total Where TranDate <= @HKDT
------------------------------------------------------------------------------------------------------
	
end
go

if @@error = 0
   print 'Success: Stored procedure TrimDatabase for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure TrimDatabase for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On TrimDatabase To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure TrimDatabase for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure TrimDatabase for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on TrimDatabase to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure TrimDatabase for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure TrimDatabase for HouseKeeping has NOT been successfully deployed'
go
