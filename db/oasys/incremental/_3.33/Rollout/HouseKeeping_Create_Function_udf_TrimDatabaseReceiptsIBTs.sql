create procedure udp_TrimDatabaseReceiptsIBTs
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 3: Receipts and IBTs tidy...'

	begin try
	
		set @statusMsg = '3a: drop table';
		print '    Starting '+@statusMsg+'...'
		
		if exists
		(
		select *
		from tempdb.dbo.sysobjects
		where ID = OBJECT_ID(N'tempdb..#Receipts')
		)
		begin
			drop table #Receipts
		end
	
		set rowcount @maxRowAffected;
		
		-- KM Added login for comm and rti
		
		select NUMB into #Receipts from DRLSUM where DATE1 < @OlderThan and comm = 1 and rti = 'C';
		
		set rowcount 0;

		set @statusMsg = '3b: transaction';
		print '    Starting '+@statusMsg+'...'
			
		begin transaction
		
			set @statusMsg = '3c: delete from DRLDET';
			print '    Starting '+@statusMsg+'...'
			
			delete from DRLDET where NUMB in (select NUMB from #Receipts)
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '3d: delete from DRLSUM';
			print '    Starting '+@statusMsg+'...'
			
			delete from DRLSUM where NUMB in (select NUMB from #Receipts)
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
			set @statusMsg = '3e: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
				
		set @errorMsg = 'Error performing '+@errorMsg
			raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	drop table #Receipts;
	
	set @statusMsg = '';
	print 'Completed 3: Receipts and IBTs tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseReceiptsIBTs for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseReceiptsIBTs for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseReceiptsIBTs To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseReceiptsIBTs for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseReceiptsIBTs for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseReceiptsIBTs to [role_execproc]

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseReceiptsIBTs for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseReceiptsIBTs for HouseKeeping has NOT been successfully deployed'
go
