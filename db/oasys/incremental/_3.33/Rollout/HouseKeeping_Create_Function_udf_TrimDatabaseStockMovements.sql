create procedure udp_TrimDatabaseStockMovements
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 2: Stock Movement tidy...'

	set rowcount @maxRowAffected;

	begin try
	
		set @statusMsg = '2a: transaction';
		print '    Starting '+@statusMsg+'...'
			
		begin transaction
		
			set @statusMsg = '2b: delete from STKLOG';
			print '    Starting '+@statusMsg+'...'
			
			delete from STKLOG where DATE1 < @OlderThan;
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
			-- KM Added StockMovement table
			
			set @statusMsg = '2c: delete from StockMovement';
			print '    Starting '+@statusMsg+'...'
			
			delete from StockMovement where DataDate < @OlderThan;
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
			set @statusMsg = '2d: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
				
		set @errorMsg = 'Error performing '+@errorMsg
			raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	set rowcount 0;
	
	set @statusMsg = '';
	print 'Completed 2: Stock Movement tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseStockMovements for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseStockMovements for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseStockMovements To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseStockMovements for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseStockMovements for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseStockMovements to [role_execproc]

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseStockMovements for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseStockMovements for HouseKeeping has NOT been successfully deployed'
go
