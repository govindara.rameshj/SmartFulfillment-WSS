create procedure udp_TrimDatabasePurchaseOrders
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 5: Purchase Orders tidy...'

	begin try
	
		set @statusMsg = '5a: drop table';
		print '    Starting '+@statusMsg+'...'
		
		if exists
		(
		select *
		from tempdb.dbo.sysobjects
		where ID = OBJECT_ID(N'tempdb..#Purchases')
		)
		begin
			drop table #Purchases
		end
	
		set rowcount @maxRowAffected;
		
		-- KM Add rcom = 1 and NOT IN SELECTs
		
		select TKEY 
		into #Purchases 
		from PURHDR 
		where DDAT < @OlderThan and rcom = 1
		and NUMB not in (select NUMB from DRLSUM) -- RFW - is this correct??????
		and NUMB not in (select NUMB from ISUHDR)
		
		set rowcount 0;

		set @statusMsg = '5b: transaction';
		print '    Starting '+@statusMsg+'...'
			
		begin transaction
		
			set @statusMsg = '5c: delete from PURLIN';
			print '    Starting '+@statusMsg+'...'
			
			delete from PURLIN where HKEY in (select TKEY from #Purchases)
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '5d: delete from PURHDR';
			print '    Starting '+@statusMsg+'...'
			
			delete from PURHDR where TKEY in (select TKEY from #Purchases)		
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
			set @statusMsg = '5e: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
				
		set @errorMsg = 'Error performing '+@errorMsg
			raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	drop table #Purchases;
	
	set @statusMsg = '';
	print 'Completed 5: Purchase Orders tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabasePurchaseOrders for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabasePurchaseOrders for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabasePurchaseOrders To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabasePurchaseOrders for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabasePurchaseOrders for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabasePurchaseOrders to [role_execproc]

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabasePurchaseOrders for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabasePurchaseOrders for HouseKeeping has NOT been successfully deployed'
go
