create procedure udp_TrimDatabaseConsignment
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 7: Consignment tidy...'

	begin try
	
		set rowcount @maxRowAffected;
		
		set @statusMsg = '7a: transaction';
		print '    Starting '+@statusMsg+'...'
			
		begin transaction
		
			set @statusMsg = '7b: delete from CONMAS';
			print '    Starting '+@statusMsg+'...'
			
			-- KM Added constraint of done = 1
			
			delete from CONMAS where EDAT < @OlderThan AND done = 1
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
			set @statusMsg = '7c: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
				
		set @errorMsg = 'Error performing '+@errorMsg
			raiserror(@errorMsg, @errorSeverity, 1)
	end catch
	
	set rowcount 0;

	set @statusMsg = '';
	print 'Completed 7: Consignment tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseConsignment for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseConsignment for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseConsignment To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseConsignment for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseConsignment for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseConsignment to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseConsignment for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseConsignment for HouseKeeping has NOT been successfully deployed'
go
