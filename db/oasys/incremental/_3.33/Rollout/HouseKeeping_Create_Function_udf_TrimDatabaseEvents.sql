create procedure udp_TrimDatabaseEvents
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 9: Events tidy...'

	if exists
	(
	select *
	from tempdb.dbo.sysobjects
	where ID = OBJECT_ID(N'tempdb..#Events')
	)
	begin
		drop table #Events
	end
	
	print '    Selecting EVTHDR...'
	
	set rowcount @maxRowAffected;
	
	-- KM Added constraint of idel = 1
	
	select NUMB, SDAT, EDAT
		into #Events
		from EVTHDR
		where EDAT < @OlderThan
		and NUMB not in (select NUMB from EVTMAS where EDAT >= @OlderThan)
		and IDEL = 1

	set rowcount 0;

	begin try

		set @statusMsg = '9a: transaction';
		print '    Starting '+@statusMsg+'...'
		
		begin transaction

			set @statusMsg = '9b: delete from EVTMMG (mix and match)';
			print '    Starting '+@statusMsg+'...'

			delete EVTMMG
			from EVTMMG a
			inner join EVTDLG b
				  on  b.KEY1 = a.MMGN
			inner join #Events c
				  on  c.NUMB = b.NUMB
            where b.[TYPE] = 'M'
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount)
			
			set @statusMsg = '9c: delete from EVTDLG (orphaned)';
			print '    Starting '+@statusMsg+'...'
			
			delete EVTMMG
			from EVTMMG a
			inner join EVTDLG b
				  on  b.KEY1 = a.MMGN
			left outer join EVTHDR c
					  on  c.NUMB = b.NUMB
            where b.[TYPE] = 'M'
            and   c.NUMB is null
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount)

			set @statusMsg = '9d: delete from EVTDLG (deal groups)';
			print '    Starting '+@statusMsg+'...'

			delete EVTDLG
			from EVTDLG a
			inner join #Events b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
				
			set @statusMsg = '9e: delete from EVTDLG (orphaned)';
			print '    Starting '+@statusMsg+'...'
			
			delete EVTDLG
			from EVTDLG a
			left outer join EVTHDR b
				  on  b.NUMB = a.NUMB
			where b.NUMB is null
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

            set @statusMsg = '9f: delete from EVTDLG (price changes)';
			print '    Starting '+@statusMsg+'...'
			
			delete EVTCHG
			from EVTCHG a
			inner join #Events b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
			set @statusMsg = '9f: delete from EVTDLG (orphaned)';
			print '    Starting '+@statusMsg+'...'
			
			-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			-- KM WE MUST leave 1 Type 10 Base Price along with Promotional Types 20/30 in EVTCHG (Even if it is older than the Housekeeping Date. 
			-- Old Type 20/30 can be removed but... 'Last' valid Type 10 must remain for Price to Increase/Decrease (Regression Process).
			-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

			delete EVTCHG
			from EVTCHG a
			left outer join EVTHDR b
				  on  b.NUMB = a.NUMB
			where b.NUMB is null
			and   a.EDAT < @OlderThan
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '9g: delete from EVTDLG (hierarchy)';
			print '    Starting '+@statusMsg+'...'

			delete EVTHEX
			from EVTHEX a
			inner join #Events b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '9h: delete from EVTMAS (master)';
			print '    Starting '+@statusMsg+'...'
			
			delete EVTMAS
			from EVTMAS a
			inner join #Events b
				  on  b.NUMB = a.NUMB
			where a.EDAT < @OlderThan
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '9i: delete from EVTMAS (header)';
			print '    Starting '+@statusMsg+'...'
			
			delete EVTHDR
			from EVTHDR a
			inner join #Events b
				  on  b.NUMB = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '9j: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
			
		set @errorMsg = 'Error performing '+@errorMsg
		raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	drop table #Events
	
	set @statusMsg = '';
	print 'Completed 9: Events tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end	

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseEvents for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseEvents for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseEvents To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseEvents for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseEvents for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseEvents to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseEvents for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseEvents for HouseKeeping has NOT been successfully deployed'
go
