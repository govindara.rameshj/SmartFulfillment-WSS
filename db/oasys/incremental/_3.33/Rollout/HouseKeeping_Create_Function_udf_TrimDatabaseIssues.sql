create procedure udp_TrimDatabaseIssues
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 6: Issues tidy...'

	begin try
	
		set @statusMsg = '6a: drop table';
		print '    Starting '+@statusMsg+'...'
		
		if exists
		(
		select *
		from tempdb.dbo.sysobjects
		where ID = OBJECT_ID(N'tempdb..#Issues')
		)
		begin
			drop table #Issues
		end
	
		set rowcount @maxRowAffected;
		
		select DATEADDED, NUMB into #Issues from ISUHDRHISTORY where DATEADDED < @OlderThan;
		
		set rowcount 0;

		set @statusMsg = '6b: transaction';
		print '    Starting '+@statusMsg+'...'
			
		begin transaction
		
			set @statusMsg = '6c: delete from PURLIN';
			print '    Starting '+@statusMsg+'...'
			
			delete ISUHDRHISTORY
			from ISUHDRHISTORY a
			inner join #Issues b
				  on  b.DATEADDED = a.DATEADDED
				  and b.NUMB      = a.NUMB
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '6d: delete from PURHDR';
			print '    Starting '+@statusMsg+'...'
			
			delete ISULINEHISTORY
			from ISULINEHISTORY a
			inner join #Issues b
				  on  b.DATEADDED = a.DATEADDED
				  and b.NUMB      = a.NUMB	
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
			set @statusMsg = '6e: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
				
		set @errorMsg = 'Error performing '+@errorMsg
			raiserror(@errorMsg, @errorSeverity, 1)
	end catch

	drop table #Issues;
	
	set @statusMsg = '';
	print 'Completed 6: Issues tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseIssues for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseIssues for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseIssues To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseIssues for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseIssues for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseIssues to [role_execproc]
go

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseIssues for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseIssues for HouseKeeping has NOT been successfully deployed'
go
