create procedure udp_TrimDatabaseSales
   @OlderThan datetime,
   @maxRowAffected int
as
begin
		
	declare @errorMsg varchar(256);
	declare @statusMsg varchar(256);
	declare @errorSeverity int;
	declare @rowcount int;
	
	set @rowcount = 0;
	set @errorSeverity = 18;
		
	set @statusMsg = '';
	print 'Starting 1: Sales tidy...'

	if exists
	(
	select *
	from tempdb.dbo.sysobjects
	where ID = OBJECT_ID(N'tempdb..#Sales')
	)
	begin
		drop table #Sales
	end
	
	print 'Selecting DLTOTS...'
	
	set rowcount @maxRowAffected;
	
	-- KM Changed DeliveryStatus to > 999
	
	select DATE1, Till, [TRAN]
	into #Sales
	from DLTOTS
	where DATE1 < @OlderThan and ORDN not in (select NUMB from CORHDR4 where DeliveryStatus > 999)

	-- KM Sales which DO NOT fulfil this check need to be inserted into TMPFIL:FKEY = 'HKR_DL****_SALE DATE1 = <Routine Date> DATA/DAT1/DAT2/DAT3/DAT4 to be filled as appropriate'
	
	set rowcount 0;

	begin try
		
		set @statusMsg = '1a: transaction';
		print '    Starting '+@statusMsg+'...'
		
		begin transaction

			set @statusMsg = '1b: delete from DLTOTS';
			print '    Starting '+@statusMsg+'...'

            delete DLTOTS
            from DLTOTS a
            inner join #Sales b
                  on  b.DATE1  = a.DATE1
                  and b.TILL   = a.TILL
                  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '1c: delete from DLLINE';
			print '    Starting '+@statusMsg+'...'
			
            delete DLLINE
            from DLLINE a
            inner join #Sales b
                  on  b.DATE1  = a.DATE1
                  and b.TILL   = a.TILL
                 and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '1d: delete from DLPAID';
			print '    Starting '+@statusMsg+'...'
			
            delete DLPAID
            from DLPAID a
            inner join #Sales b
                  on  b.DATE1  = a.DATE1
                  and b.TILL   = a.TILL
                  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
				
			set @statusMsg = '1e: delete from DLCOMM';
			print '    Starting '+@statusMsg+'...'
			
            delete DLCOMM
            from DLCOMM a
            inner join #Sales b
                  on  b.DATE1  = a.DATE1
                  and b.TILL   = a.TILL
                  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '1f: delete from DLCOUPON';
			print '    Starting '+@statusMsg+'...'
			
            delete DLCOUPON
            from DLCOUPON a
            inner join #Sales b
                  on  b.DATE1  = a.TranDate
                  and b.TILL   = a.TranTillID
                  and b.[TRAN] = a.TranNo
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '1g: delete from DLEANCHK';
			print '    Starting '+@statusMsg+'...'
			
            delete DLEANCHK
            from DLEANCHK a
            inner join #Sales b
                  on  b.DATE1  = a.DATE1
                  and b.TILL   = a.TILL
                  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '1h: delete from DLEVNT';
			print '    Starting '+@statusMsg+'...'
			
            delete DLEVNT
            from DLEVNT a
            inner join #Sales b
                  on  b.DATE1  = a.DATE1
                  and b.TILL   = a.TILL
                  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
			set @statusMsg = '1i: delete from DLGIFT';
			print '    Starting '+@statusMsg+'...'
			
            delete DLGIFT
            from DLGIFT a
            inner join #Sales b
                  on  b.DATE1  = a.DATE1
                  and b.TILL   = a.TILL
                  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
					
			set @statusMsg = '1j: delete from DLOCUS';
			print '    Starting '+@statusMsg+'...'
			
            delete DLOCUS
            from DLOCUS a
            inner join #Sales b
                  on  b.DATE1  = a.DATE1
                  and b.TILL   = a.TILL
                  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
			set @statusMsg = '1k: delete from DLOLIN';
			print '    Starting '+@statusMsg+'...'
			
			delete DLOLIN
			from DLOLIN a
			inner join #Sales b
				  on  b.DATE1  = a.DATE1
				  and b.TILL   = a.TILL
				  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
				
			set @statusMsg = '1l: delete from DLRCUS';
			print '    Starting '+@statusMsg+'...'
				
			delete DLRCUS
			from DLRCUS a
			inner join #Sales b
				  on  b.DATE1  = a.DATE1
				  and b.TILL   = a.TILL
				  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);
			
			set @statusMsg = '1m: delete from DLREJECT';
			print '    Starting '+@statusMsg+'...'
			
			delete DLREJECT
			from DLREJECT a
			inner join #Sales b
				  on  b.DATE1  = a.DATE1
				  and b.TILL   = a.TILL
				  and b.[TRAN] = a.[TRAN]
			set @rowcount = dbo.udf_MaxVal(@rowcount, @@rowcount);

			set @statusMsg = '1n: commit';
			print '    Starting '+@statusMsg+'...'
			
		commit transaction
	end try
	begin catch
		if @@trancount > 0
			rollback transaction
			
		set @errorMsg = 'Error performing '+@errorMsg
		raiserror(@errorMsg, @errorSeverity, 1)
	end catch

    drop table #Sales
	
	print 'Completed 1: Sales tidy - ' + cast(@rowcount as varchar(32)) + ' rows'
	
	return @rowcount
end

if @@error = 0
   print 'Success: Stored procedure udp_TrimDatabaseSales for HouseKeeping has been successfully deployed'
else
   print 'Failure: Stored procedure udp_TrimDatabaseSales for HouseKeeping has NOT been successfully deployed'
go

--Apply Permissions

Grant Execute On udp_TrimDatabaseSales To [role_legacy];
Go
If @@Error = 0
    Begin
        Print 'Success: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseSales for HouseKeeping has been successfully deployed';
    End
Else
    Begin
        Print 'Failure: Grant security for [role_legacy] on stored procedure udp_TrimDatabaseSales for HouseKeeping might NOT have been successfully deployed';
    End;
Go

------------------------------------------------------------------------------------------------------------
grant execute on udp_TrimDatabaseSales to [role_execproc]

if @@error = 0
   print 'Success: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseSales for HouseKeeping has been successfully deployed'
else
   print 'Failure: Grant security for [role_execproc] on stored procedure udp_TrimDatabaseSales for HouseKeeping has NOT been successfully deployed'
go
