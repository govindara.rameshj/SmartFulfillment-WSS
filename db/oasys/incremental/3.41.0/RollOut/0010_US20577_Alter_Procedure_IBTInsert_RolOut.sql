SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

PRINT ('Altering procedure IBTInsert')
GO

ALTER PROCEDURE [dbo].[IBTInsert] 
	@Number Char(6) out, 
	@Type Char(1) = NULL,
	@Date date = NUll,
	@Info Char(20) = Null,
	@Value decimal(9,2),
	@1Str Char(3) = Null,
	@1IBT int = Null
AS
BEGIN
	SET NOCOUNT ON;

	If LEN(ISNULL(@Number, '')) = 0
	begin
		--get next receipt number from system numbers
		declare @NextNumber int;
		exec	@NextNumber = SystemNumbersGetNext @Id=4
		
		--convert next number to number
		set @Number = Convert(Char(6), @NextNumber);
	end

	Insert into DRLSUM (NUMB,TYPE,DATE1,INIT,INFO,VALU,[1STR],[1PRT],[1IBT],RTI)
			Values(@Number,@Type,@Date,'Auto',@Info,@Value,@1Str,0,@1IBt,'S')

	SET NOCOUNT OFF;
END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure IBTInsert for US20577 has been deployed successfully'
Else
   Print 'Failure: The Create Stored Procedure IBTInsert for US20577 has not been deployed'
Go