SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

PRINT ('Altering procedure IBTInsert')
GO

ALTER PROCEDURE [dbo].[IBTInsert] 
	@Number Char(6), 
	@Type Char(1) = NULL,
	@Date date = NUll,
	@Info Char(20) = Null,
	@Value decimal(9,2),
	@1Str Char(3) = Null,
	@1IBT int = Null
AS
BEGIN
	SET NOCOUNT ON;

	Insert into DRLSUM (NUMB,TYPE,DATE1,INIT,INFO,VALU,[1STR],[1PRT],[1IBT],RTI)
			Values(@Number,@Type,@Date,'Auto',@Info,@Value,@1Str,0,@1IBt,'S')

END
GO

If @@Error = 0
   Print 'Success: The Create Stored Procedure IBTInsert for US20577 has been rolled back successfully'
Else
   Print 'Failure: The Create Stored Procedure IBTInsert for US20577 has not been rolled back'
Go