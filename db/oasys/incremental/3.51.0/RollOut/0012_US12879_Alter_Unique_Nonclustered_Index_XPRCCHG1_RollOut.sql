CREATE UNIQUE NONCLUSTERED INDEX [XPRCCHG1] ON [dbo].[PRCCHG] 
(
 [SKUN] ASC,
 [PDAT] ASC,
 [EVNT] ASC,
 [PRIC] ASC,
 [PSTA] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = ON, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

If @@Error = 0
   Print 'Success: The Alter Unique Nonclustered Index XPRCCHG1 for US12879 has been rolled back successfully'
Else
   Print 'Failure: The Alter Unique Nonclustered Index XPRCCHG1 for US12879 has not been rolled back'
GO