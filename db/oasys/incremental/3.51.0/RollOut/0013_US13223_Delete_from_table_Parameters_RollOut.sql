IF EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = '-980')
	DELETE FROM dbo.Parameters WHERE ParameterID = '-980'
GO

IF EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = '-1069')
	DELETE FROM dbo.Parameters WHERE ParameterID = '-1069'
GO

IF EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = '-1080')
	DELETE FROM dbo.Parameters WHERE ParameterID = '-1080'
GO

If @@Error = 0
   Print 'Success: The Delete from table Parameters for US13223 has been deployed successfully'
Else
   Print 'Failure: The Delete from table Parameters for US13223 has not been deployed'
GO