IF NOT EXISTS (SELECT 1 FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'usp_UpdatePriceChangeRecordResetLabelWithOriginalDate') AND type in (N'P', N'PC'))
BEGIN
	PRINT 'Creating procedure usp_UpdatePriceChangeRecordResetLabelWithOriginalDate'
	EXEC ('CREATE PROCEDURE dbo.usp_UpdatePriceChangeRecordResetLabelWithOriginalDate AS RAISERROR(''Not implemented yet.'', 16, 3);')
END
GO

PRINT ('Altering procedure usp_UpdatePriceChangeRecordResetLabelWithOriginalDate')
GO

ALTER PROCEDURE [dbo].[usp_UpdatePriceChangeRecordResetLabelWithOriginalDate] 
	@Skun Char(6),
	@OriginalPDAT Date,
	@OriginalEventNumber Char(6),
	@OriginalPrice decimal(10,2),
	@OriginalStatus Char(1),
	@StartDate DATE,
	@Price decimal(10,2),
	@Status Char(1),
	@EventNumber Char(6),
	@Priority Char(2),
	@AutoApplyDate Date, 
	@IsShelfLabel bit,
	@SmallLabel bit,
	@MediumLabel bit,
	@LargeLabel bit
	
AS
BEGIN
	SET NOCOUNT ON;

IF NOT EXISTS (SELECT 1 FROM [dbo].[PRCCHG] WHERE [Skun] = @Skun AND [PDAT] = @StartDate AND [PRIC] = @Price AND [PSTA] = @Status AND [EVNT] = @EventNumber)
BEGIN
	Update [dbo].[PRCCHG]
			set 
				[PDAT] = @StartDate	
			   ,[EVNT] = @EventNumber
			   ,[PRIO] = @Priority
			   ,[PRIC] = @Price
			   ,[PSTA] = @Status
			   ,[AUDT] = @AutoApplyDate
			   ,[SHEL] = @IsShelfLabel
			   ,[LABS] = @SmallLabel
			   ,[LABM] = @MediumLabel
			   ,[LABL] = @LargeLabel
			   ,[AUAP] = NULL
			Where
				[Skun] = @Skun And 
				[PDAT] = @OriginalPDAT And
				[EVNT] = @OriginalEventNumber And
				[PSTA] = @OriginalStatus And
				[PRIC] = @OriginalPrice 
END
Return @@Rowcount            
End
GO

If @@Error = 0
   Print 'Success: The Alter Stored Procedure usp_UpdatePriceChangeRecordResetLabelWithOriginalDate for US12879 has been rolled back successfully'
Else
   Print 'Failure: The Alter Stored Procedure usp_UpdatePriceChangeRecordResetLabelWithOriginalDate for US12879 has not been rolled back'
GO