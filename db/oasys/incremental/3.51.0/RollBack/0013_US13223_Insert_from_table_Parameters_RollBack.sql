IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = '-980')
	INSERT INTO [dbo].[Parameters]
			   ([ParameterID]
			   ,[Description]
			   ,[StringValue]
			   ,[LongValue]
			   ,[BooleanValue]
			   ,[DecimalValue]
			   ,[ValueType])
	SELECT '-980' AS [ParameterID],
		   'Enable requirement RF0980' AS [Description],
		   NULL AS [StringValue],
		   NULL AS [StringValue],
		   1 AS [LongValue],
		   NULL AS [DecimalValue],
		   3 AS [ValueType]           
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = '-1069')
	INSERT INTO [dbo].[Parameters]
			   ([ParameterID]
			   ,[Description]
			   ,[StringValue]
			   ,[LongValue]
			   ,[BooleanValue]
			   ,[DecimalValue]
			   ,[ValueType])
	SELECT '-1069' AS [ParameterID],
		   'RF1069 Price Change PK Violation Issue' AS [Description],
		   NULL AS [StringValue],
		   NULL AS [StringValue],
		   1 AS [LongValue],
		   NULL AS [DecimalValue],
		   3 AS [ValueType]           
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Parameters WHERE ParameterID = '-1080')
	INSERT INTO [dbo].[Parameters]
			   ([ParameterID]
			   ,[Description]
			   ,[StringValue]
			   ,[LongValue]
			   ,[BooleanValue]
			   ,[DecimalValue]
			   ,[ValueType])
	SELECT '-1080' AS [ParameterID],
		   'RF1080 Multiple Labels Price Change Issue' AS [Description],
		   NULL AS [StringValue],
		   NULL AS [StringValue],
		   1 AS [LongValue],
		   NULL AS [DecimalValue],
		   3 AS [ValueType]           
GO

If @@Error = 0
   Print 'Success: The Insert in table Parameters for US13223 has been deployed successfully'
Else
   Print 'Failure: The Insert in table Parameters for US13223 has not been deployed'
GO