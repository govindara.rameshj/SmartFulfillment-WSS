﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_MENREM]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_MENREM'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_MENREM] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_MENREM'
GO
ALTER PROCEDURE dbo.KevanConversion_MENREM
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 13 - Remove Un-Used Menu Options
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

-----------------------------------------------------------------------------------
-- Task     : 13/01 - Remove Change Order Details
-----------------------------------------------------------------------------------
DELETE MenuConfig WHERE [ID] = 2030
DELETE ProfileMenuAccess WHERE MenuConfigID = 2030

-----------------------------------------------------------------------------------
-- Task     : 13/02 - Remove Product Area Calculator Application
-----------------------------------------------------------------------------------
DELETE MenuConfig WHERE [ID] = 12170
DELETE ProfileMenuAccess WHERE MenuConfigID = 12170

-----------------------------------------------------------------------------------
-- Task     : 13/03 - Remove Product Volume Calculator Application
-----------------------------------------------------------------------------------
DELETE MenuConfig WHERE [ID] = 12160
DELETE ProfileMenuAccess WHERE MenuConfigID = 12160

-----------------------------------------------------------------------------------
-- Task     : 13/04 - Remove Reset User Password
-----------------------------------------------------------------------------------
DELETE MenuConfig WHERE [ID] = 10260
DELETE ProfileMenuAccess WHERE MenuConfigID = 10260

END;
GO

