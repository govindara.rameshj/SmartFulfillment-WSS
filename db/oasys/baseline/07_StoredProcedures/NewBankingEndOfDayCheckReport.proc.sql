﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingEndOfDayCheckReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingEndOfDayCheckReport'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingEndOfDayCheckReport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingEndOfDayCheckReport'
GO
ALTER PROCedure NewBankingEndOfDayCheckReport

   @BankingDate datetime

as
begin

   set nocount on

   declare @PeriodID              int
   declare @PeriodEndDate         datetime
   declare @ProcessID             int
   
   declare @ScannedPickupBagCount int

   declare @BankingCashBag        table(ID int)
   declare @BankingChequeBag      table(ID int)

   declare @PickupBag             table(ID int, SealNumber char(20), Value decimal(9, 2), Comment nvarchar(max))
   declare @PickupBagNotScanned   table(SealNumber char(20), Value decimal(9, 2))
   declare @PickupBagNull         table(SealNumber char(20), Comment nvarchar(max))

   declare @BankingBagCash        table(ID int, SealNumber char(20), Value decimal(9, 2), Comment nvarchar(max))
   declare @BankingBagCheque      table(ID int, SealNumber char(20), Value decimal(9, 2), Comment nvarchar(max))
   declare @BankingBagNotScanned  table(SealNumber char(20), Value decimal(9, 2))
   declare @BankingBagNull        table(SealNumber char(20), Comment nvarchar(max))

   declare @PickupComment         table(ID int, SafeBagsType nchar(1), Comment nvarchar(max))
   declare @BankingComment        table(ID int, SafeBagsType nchar(1), Comment nvarchar(max))

   declare @AuthoriserName        table(ManagerName varchar(50), WitnessName varchar(50), EndOfDayCheckLockedFrom datetime, EndOfDayCheckLockedTo datetime)

   declare @Output                table(RowId int,
                                        CategoryDescription  nvarchar(max),
                                        SealNumber           nvarchar(max),
                                        Value                nvarchar(max),
                                        Comment              nvarchar(max),
                                        EmboldenThis         bit,
                                        ResizeThis           bit)

   set @ProcessID     = (select ID from BusinessProcess where [Description] = 'End Of Day Check Process')
   set @PeriodID      = (select ID                      from SystemPeriods where datediff(day, @BankingDate, StartDate) = 0)
   set @PeriodEndDate = (select cast(StartDate as date) from SystemPeriods where datediff(day, @BankingDate, StartDate) = 0)
   set @PeriodEndDate = dateadd(hour,   23, @PeriodEndDate)
   set @PeriodEndDate = dateadd(minute, 59, @PeriodEndDate)
   set @PeriodEndDate = dateadd(second, 59, @PeriodEndDate)

   if not exists(select * from Safe where PeriodID = @PeriodID)
   begin
   
      insert @Output values(null, 'Safe Not Found', null, null, null, 1, null)
   
   end
   else
   begin

      insert @BankingCashBag   select BagID from SafeBagsDenoms where TenderID = 1 group by BagID
      insert @BankingChequeBag select BagID from SafeBagsDenoms where TenderID = 2 group by BagID

      insert @PickupBag             select b.ID, b.SealNumber, b.Value, a.Comment
                                    from SafeBagScanned a
                                    inner join SafeBags b
                                          on b.ID = a.SafeBagsID
                                    where a.SafePeriodID      = @PeriodID
                                    and   a.BusinessProcessId = @ProcessID
                                    and   b.[Type]            = 'P'

      insert @PickupBagNotScanned   select SealNumber, Value
                                    from dbo.udf_BagsInSafeAtDateAndTime(@PeriodEndDate, 'P')
                                    where ID not in (select ID from @PickupBag)

      insert @PickupBagNull         select SealNumber, Comment
                                    from SafeBagScanned 
                                    where SafePeriodID      = @PeriodID
                                    and   BusinessProcessId = @ProcessID
                                    and   SafeBagsId       is null
                                    and	 BagType           = 'P'

      insert @BankingBagCash        select b.ID, b.SealNumber, b.Value, a.Comment
                                    from SafeBagScanned a
                                    inner join SafeBags b
                                          on b.ID = a.SafeBagsID
                                    where a.SafePeriodID      = @PeriodID
                                    and   a.BusinessProcessId = @ProcessID
                                    and   a.SafeBagsID       in (select ID from @BankingCashBag)
                                    and   b.[Type]            = 'B'

      insert @BankingBagCheque      select b.ID, b.SealNumber, b.Value, a.Comment
                                    from SafeBagScanned a
                                    inner join SafeBags b
                                          on b.ID = a.SafeBagsID
                                    where a.SafePeriodID      = @PeriodID
                                    and   a.BusinessProcessId = @ProcessID
                                    and   a.SafeBagsID       in (select ID from @BankingChequeBag)
                                    and   b.[Type]            = 'B'

      insert @BankingBagNotScanned  select SealNumber, Value
                                    from dbo.udf_BagsInSafeAtDateAndTime(@PeriodEndDate, 'B')
                                    where ID not in (select ID from @BankingBagCash
                                                     union all
                                                     select ID from @BankingBagCheque)

      insert @BankingBagNull        select SealNumber, Comment
                                    from SafeBagScanned 
                                    where SafePeriodID      = @PeriodID
                                    and   BusinessProcessId = @ProcessID
                                    and   SafeBagsId       is null
                                    and	 BagType           = 'B'

      insert @PickupComment         select ID, BagType, Comment from SafeComment where SafePeriodID = @PeriodID and BagType = 'P' and BusinessProcessId = @ProcessID
      insert @BankingComment        select ID, BagType, Comment from SafeComment where SafePeriodID = @PeriodID and BagType = 'B' and BusinessProcessId = @ProcessID

      insert @AuthoriserName        select b.Name, c.Name, a.EndOfDayCheckLockedFrom, a.EndOfDayCheckLockedTo 
                                    from Safe a
                                    inner join SystemUsers b
                                          on b.ID = a.EndOfDayCheckManagerID 
                                    inner join SystemUsers c
                                          on c.ID = a.EndOfDayCheckWitnessID 
                                    where a.PeriodID = @PeriodId

      set @ScannedPickupBagCount = (select count(*) from @PickupBag) + (select count(*) from @PickupBagNull)

      insert @Output values(1, 'End Of Day Check Info', 'Summary', null, null, 1, 0)
      insert @Output values(2, null, 'Manager Check ID', (select ManagerName                                    from @AuthoriserName), null, 0, 0)
      insert @Output values(3, null, 'Witness Check ID', (select WitnessName                                    from @AuthoriserName), null, 0, 0)
      insert @Output values(4, null, 'Safe Locked From', (select cast(EndOfDayCheckLockedFrom as nvarchar(max)) from @AuthoriserName), null, 0, 0)
      insert @Output values(5, null, 'Safe Locked To',   (select cast(EndOfDayCheckLockedTo as nvarchar(max))   from @AuthoriserName), null, 0, 0)

      insert @Output values(10, 'Bag Counts', 'Summary', null, null, 1, 0)
      insert @Output values(11, null, 'Pickup Bag Physical Count',         cast(@ScannedPickupBagCount as nvarchar(max)),                                                                                                                                      null, 0, 0)
      insert @Output values(12, null, 'Pickup Bag System Count',           (select cast(count(*) as varchar(max)) from dbo.udf_PickupBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID))),                                                 null, 0, 0)
      insert @Output values(13, null, 'Banking Bag Cash Physical Count',   (select cast(count(*) as varchar(max)) from @BankingBagCash),                                                                                                                       null, 0, 0)
      insert @Output values(14, null, 'Banking Bag Cash System Count',     (select cast(count(*) as varchar(max)) from dbo.udf_BankingBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID)) where ID in (select ID from @BankingCashBag)),   null, 0, 0)
      insert @Output values(15, null, 'Banking Bag Cheque Physical Count', (select cast(count(*) as varchar(max)) from @BankingBagCheque),                                                                                                                     null, 0, 0)
      insert @Output values(16, null, 'Banking Bag Cheque System Count',   (select cast(count(*) as varchar(max)) from dbo.udf_BankingBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID)) where ID in (select ID from @BankingChequeBag)), null, 0, 0)

      insert @Output values(20, 'Pickup Section', 'Seal Number', 'Value', null, 1, 0)
      insert @Output select 21, null, rtrim(SealNumber), Value, Comment, 0, 0 from @PickupBag
      insert @Output select 22, null, rtrim(SealNumber), null,  Comment, 0, 0 from @PickupBagNull
      insert @Output select 23, null, null,              null,  Comment, 0, 0 from @PickupComment

      insert @Output values(30, 'Pickup Bags Expected', 'Seal Number', 'Value', null, 1, 0)
      insert @Output select 31, null, rtrim(SealNumber), -1 * Value, null, 0, 0 from @PickupBagNotScanned
      insert @Output select 32, 'Missing Bags Value', null, sum(-1 * Value), null, 1, 0 from @PickupBagNotScanned
      insert @Output values(33, null, null, null, null, 1, 0)

      insert @Output values(40, 'Banking Section','Seal Number', 'Value', null, 1, 0)
      insert @Output select 41, null, rtrim(SealNumber), Value, Comment, 0, 0 from @BankingBagCash
      insert @Output select 42, null, rtrim(SealNumber), Value, Comment, 0, 0 from @BankingBagCheque
      insert @Output select 43, null, rtrim(SealNumber), null,  Comment, 0, 0 from @BankingBagNull
      insert @Output select 44, null, null,              null,  Comment, 0, 0 from @BankingComment

      insert @Output values(50, 'Banking Bags Expected', 'Seal Number', 'Value', null, 1, 0)
      insert @Output select 51, null, rtrim(SealNumber), -1 * Value, null, 0, 0 from @BankingBagNotScanned
      insert @Output select 52, 'Missing Bags Value', null, sum(-1 * Value), null, 1, 0 from @BankingBagNotScanned

   end

   select * from @Output order by RowId

end
GO

