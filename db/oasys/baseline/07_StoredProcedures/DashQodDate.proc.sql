﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashQodDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashQodDate'
	EXEC ('CREATE PROCEDURE [dbo].[DashQodDate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashQodDate'
GO
ALTER PROCEDURE [dbo].[DashQodDate]
@DateEnd DATE
AS
begin
	declare @table table(
		RowId		int,
		Description	varchar(50),
		Qty			int,
		Value		dec(9,2));
	declare
		@unconfirmedQty		int,
		@unconfirmedValue	dec(9,2),
		@confirmedQty		int,
		@confirmedValue		dec(9,2),
		@pickingQty			int,
		@pickingValue		dec(9,2),
		@despatchQty		int,
		@despatchValue		dec(9,2),
		@undeliveredQty		int,
		@undeliveredValue	dec(9,2),
		@deliveredQty		int,
		@deliveredValue		dec(9,2),
		@completedQty		int,
		@completedValue		dec(9,2)		

	select
		@unconfirmedQty		= count(*),	
	 	@unconfirmedValue	= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus < 300
		and DateDelivery <= DATEADD(d, 1, @DateEnd)
		
	select
		@confirmedQty		= count(*),	
	 	@confirmedValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus > 299
		and DeliveryStatus < 500
		and DateDelivery <= DATEADD(d, 1, @DateEnd)
		
	select
		@pickingQty			= count(*),	
	 	@pickingValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus >= 500
		and DeliveryStatus < 600
		and DateDelivery <= DATEADD(d, 1, @DateEnd)
		
	select
		@despatchQty		= count(*),	
	 	@despatchValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus >= 700
		and DeliveryStatus < 800
		and DateDelivery = @DateEnd		

	select
		@undeliveredQty		= count(*),	
	 	@undeliveredValue	= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus >= 800
		and DeliveryStatus < 900
		and DateDelivery = @DateEnd
	
	select
		@deliveredQty		= count(*),	
	 	@deliveredValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus >= 900
		and DeliveryStatus < 999
		and DateDelivery = @DateEnd
	
	select
		@completedQty		= count(*),	
	 	@completedValue		= coalesce(SUM(value),0)
	from	
		vwQod
	where
		DeliveryStatus >= 999	
		and DateDelivery = @DateEnd
				
	--return values
	insert into @table values (1,'Unconfirmed Orders', @unconfirmedQty, @unconfirmedValue) ;
	insert into @table values (2,'Confirmed Orders', @confirmedQty, @confirmedValue) ;
	insert into @table values (3,'Orders in Picking', @pickingQty, @pickingValue) ;
	insert into @table values (4,'Orders in Despatch', @despatchQty, @despatchValue) ;
	insert into @table values (5,'Undelivered', @undeliveredQty, @undeliveredValue) ;
	insert into @table values (6,'Delivered', @deliveredQty, @deliveredValue) ;
	insert into @table values (0,'Completed', @completedQty, @completedValue) ;
	
	select * from @table

end
GO

