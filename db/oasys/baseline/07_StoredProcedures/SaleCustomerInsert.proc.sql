﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleCustomerInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleCustomerInsert'
	EXEC ('CREATE PROCEDURE [dbo].[SaleCustomerInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleCustomerInsert'
GO
ALTER PROCEDURE [dbo].[SaleCustomerInsert]
 @TransactionDate DATE,
 @TillNumber CHAR (2), 
 @TransactionNumber CHAR(4),
 @CustomerName CHAR(30),
 @HouseNumber CHAR(4) = null,
 @PostCode CHAR(8) = '',
 @StoreNumber CHAR(3)='000',
 @SaleDate DATE=null,
 @SaleTill CHAR(2)= '00',
 @SaleTransaction CHAR(4) = '0000',
 @HouseNameNo CHAR(15),
 @Address1 CHAR(30)= NULL,
 @Address2 CHAR(30)= NULL,
 @Address3 CHAR(30)=NULL,
 @PhoneNumber CHAR(15),
 @LineNumber INT = 0,
 @TransactionValidated BIT=0,
 @OriginalTenderType DECIMAL(3,0)= 0,
 @RefundReasonCode CHAR(2) = '00',
 @Labels BIT=0,
 @PhoneNumberMobile CHAR(15)=NULL,
 @RTIFlag CHAR(1)='S',
 @EmailAddress CHAR(1),
 @PhoneNumberWork CHAR(1)=NULL
 
AS
BEGIN
    SET NOCOUNT ON;

    INSERT INTO DLRCUS 
       (
            DATE1,
            TILL,
            [TRAN],
            NAME,
            HNUM,
            POST,
            OSTR,
            ODAT,
            OTIL,
            OTRN,
            HNAM,
            HAD1,
            HAD2,
            HAD3,
            PHON,
            NUMB,
            OVAL,
            OTEN,
            RCOD,
            LABL,
            MOBP,
            RTI,
            EmailAddress,
            PhoneNumberWork
      )
VALUES
(
 @TransactionDate ,
 @TillNumber, 
 @TransactionNumber ,
 @CustomerName,
 @HouseNumber,
 @PostCode,
 @StoreNumber ,
 @SaleDate,
 @SaleTill ,
 @SaleTransaction ,
 @HouseNameNo,
 @Address1 ,
 @Address2,
 @Address3 ,
 @PhoneNumber,
 @LineNumber,
 @TransactionValidated ,
 @OriginalTenderType,
 @RefundReasonCode,
 @Labels,
 @PhoneNumberMobile,
 @RTIFlag,
 @EmailAddress,
 @PhoneNumberWork 
)
return @@rowcount
END
GO

