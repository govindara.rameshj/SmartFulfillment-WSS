﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetReducedStockSkuns]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetReducedStockSkuns'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetReducedStockSkuns] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetReducedStockSkuns'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 22/06/2011
-- Description:	Gets the Reduced Stock Sku's from database
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetReducedStockSkuns] 
	-- Add the parameters for the stored procedure here
	@SaltCode Char(1) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		SKUN as 'Number',
		DESCR as 'Name'
		
	FROM
		STKMAS 
	WHERE
		SALT = @SaltCode 
END
GO

