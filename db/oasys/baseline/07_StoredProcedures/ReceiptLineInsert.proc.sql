﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptLineInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptLineInsert'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptLineInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptLineInsert'
GO
ALTER PROCEDURE [dbo].[ReceiptLineInsert]
	@ReceiptNumber	char(6),
	@Sequence		char(4),
	@SkuNumber		char(6),
	@OrderQty		int,
	@OrderPrice		dec(9,2),
	@ReceivedQty	int,
	@ReceivedPrice	dec(9,2),
	@OrderLineId	int,
	@StockLogType	char(2),
	@StockLogKey	char(30),
	@UserId			int
AS
BEGIN
	SET NOCOUNT ON;
	declare @onHand			int;
	declare @returns		int;
	declare	@markdowns		int;
	declare @writeOffs		int;
	declare @price			dec(9,2);

	--insert reciept line
	insert into		drldet 
				(
					numb,
					seqn,
					skun,
					ordq,
					ordp,
					recq,
					pric,
					poln
				)
	values		(
					@ReceiptNumber,
					@Sequence,
					@SkuNumber,
					@OrderQty,
					@OrderPrice,
					@ReceivedQty,
					@ReceivedPrice,
					@OrderLineId
				)
	
	--get stock item values
	select	@onHand		= onha,
			@returns	= retq, 
			@markdowns	= mdnq,
			@writeOffs	= wtfq,
			@price		= pric	
	from	stkmas
	where	skun		= @SkuNumber
	
	--update stock item as received
	update	stkmas 
	set		onha = onha + @ReceivedQty,
			onor = onor - @OrderQty,
			treq = treq + @ReceivedQty,
			trev = trev + (@ReceivedQty * @ReceivedPrice),
			drec = getdate(),
			tact = 1
	where	skun = @SkuNumber
	
	--insert stock log entry
	insert into		stklog 
				(
					skun,
					dayn,
					[type],
					date1,
					[time],
					keys,
					eeid,
					sstk, estk,
					sret, eret,
					smdn, emdn,
					swtf, ewtf,
					spri, epri
				)
	values
				(
					@SkuNumber,
					datediff(d, '1900-01-01', getdate()) + 1,
					@StockLogType,
					getdate(),
					replace(convert (varchar(8), getdate(), 108),':',''),
					@StockLogKey,
					RIGHT('000' + Convert(varchar, @UserId), 3),
					@onHand, @onHand + @ReceivedQty,
					@returns, @returns,
					@markdowns, @markdowns,
					@writeOffs, @writeOffs,
					@price, @price
				)
END
GO

