﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EventMasterGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EventMasterGet'
	EXEC ('CREATE PROCEDURE [dbo].[EventMasterGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EventMasterGet'
GO
ALTER PROCEDURE [dbo].[EventMasterGet] 
	@EventNumber char(6)=null
AS
BEGIN
	SET NOCOUNT ON;

Declare @ETYP  char(2);
set @ETYP = (select Top 1 en.ETYP FROM EVTHDR as eh INNER JOIN EVTENQ as en ON eh.NUMB=en.NUMB  where en.Numb=@EventNumber);

if @ETYP = 'DM' Or @ETYP = 'DS'
	BEGIN
	-- Deal Group Query
		select Distinct 
			  RowId = 1,
			  @EventNumber  as Item,	
			  ev.KEY2		as 'Event',
			  ed.Key1		as SkuNumber,
			  st.Descr		as 'Description',	
			  ev.BQTY		as QtyBuy,	
			  ev.PRIC		as PriceDeal
		FROM  EVTMAS as ev INNER JOIN
			  EVTDLG as ed ON ev.NUMB = ev.NUMB INNER JOIN
			  STKMAS as st ON ed.KEY1 = st.SKUN
		WHERE EV.NUMB = @EventNumber
	END
else
	BEGIN
	-- Original Query
		select 
			   RowId = 1,	
			   @EventNumber as Item,
			   ev.KEY2		as 'Event',
			   ev.Key1		as SkuNumber,
			   st.Descr		as 'Description',	
			   ev.BQTY		as QtyBuy,	
			   ev.PRIC		as PriceDeal
		From EVTMAS as ev inner join STKMAS as st on KEY1 = st.SKUN  
		where Numb = @EventNumber
	END
END
GO

