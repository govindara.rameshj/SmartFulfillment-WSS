﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetDaysToAdjustForHeadOfficeToStoreImport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetDaysToAdjustForHeadOfficeToStoreImport'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetDaysToAdjustForHeadOfficeToStoreImport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetDaysToAdjustForHeadOfficeToStoreImport'
GO
ALTER PROCEDURE usp_GetDaysToAdjustForHeadOfficeToStoreImport 
@ParameterId int
AS
BEGIN
    SELECT LongValue
      FROM parameters
      WHERE ParameterID
            = 
            @ParameterId;
END;
GO

