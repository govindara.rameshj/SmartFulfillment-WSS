﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CheckPickupProcessStart]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure CheckPickupProcessStart'
	EXEC ('CREATE PROCEDURE [dbo].[CheckPickupProcessStart] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure CheckPickupProcessStart'
GO
ALTER PROCedure [dbo].[CheckPickupProcessStart](@ownerID varchar(3), @pickupProcessStart bit = 0 Output)
AS
BEGIN
	DECLARE @currentDate date
	SET @currentDate = CONVERT(varchar(10), GETDATE(), 121)
	
	IF EXISTS(SELECT 1 FROM [dbo].[Locks] 
			  WHERE [EntityId] = 'NewBanking.Form.PickupEntry' 
				AND CAST([OwnerId] as int) = CAST(@ownerID as int) 
				AND CONVERT(varchar(10), [LockTime], 121) = @currentDate)
		SET @pickupProcessStart = 1	
	ELSE
		SET @pickupProcessStart = 0
END
GO

