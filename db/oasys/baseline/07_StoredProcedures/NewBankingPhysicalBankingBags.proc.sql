﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingPhysicalBankingBags]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingPhysicalBankingBags'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingPhysicalBankingBags] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingPhysicalBankingBags'
GO
ALTER PROCedure NewBankingPhysicalBankingBags

   @PeriodID int 

as
begin
   set nocount on

   select * from dbo.udf_BankingBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID))

end
GO

