﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ContainerGetLines]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ContainerGetLines'
	EXEC ('CREATE PROCEDURE [dbo].[ContainerGetLines] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ContainerGetLines'
GO
ALTER PROCEDURE [dbo].[ContainerGetLines]
	@AssemblyDepotNumber	char(3),
	@ContainerNumber		char(9)
AS
BEGIN
	SET NOCOUNT ON;

	select		
		cd.ADEP		as AssemblyDepotNumber,
		cd.CNUM		as ContainerNumber,
		cd.PNUM		as StorePoNumber,
		cd.LINE		as StorePoLineNumber,
		cd.SKUN		as SkuNumber,
		sk.DESCR	as SkuDescription,
		sk.PROD		as SkuProductCode,
		sk.PACK		as SkuPackSize,
		cd.QUAN		as Qty,
		cd.PRIC		as Price,
		cd.REJI		as RejectedReason
	from
		CONDET cd
	inner join	
		stkmas sk on sk.skun = cd.skun
	where
		cd.ADEP = @AssemblyDepotNumber
		and cd.CNUM = @ContainerNumber
	order by
		cd.PNUM, cd.LINE

END
GO

