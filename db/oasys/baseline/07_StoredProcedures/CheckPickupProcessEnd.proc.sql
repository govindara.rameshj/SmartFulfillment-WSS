﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CheckPickupProcessEnd]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure CheckPickupProcessEnd'
	EXEC ('CREATE PROCEDURE [dbo].[CheckPickupProcessEnd] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure CheckPickupProcessEnd'
GO
ALTER PROCedure [dbo].[CheckPickupProcessEnd](@ownerID varchar(3), @pickupProcessEnd bit = 0 Output)
AS
BEGIN

	declare @currentPeriodID int
	
	set @currentPeriodID = (select ID from SystemPeriods where StartDate = CONVERT(varchar(10), GETDATE(), 121))

	if exists (
		select 1 
		from SafeBags 
		where PickupPeriodID = @currentPeriodID
			and [Type] = 'P'
			and CashDrop is null
			and AccountabilityID = CAST(@ownerID as int)
		)
		set @pickupProcessEnd = 1
	else 
		set @pickupProcessEnd = 0
	
END
GO

