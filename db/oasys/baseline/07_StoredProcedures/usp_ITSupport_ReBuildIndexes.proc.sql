﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_ReBuildIndexes]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_ReBuildIndexes'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_ReBuildIndexes] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_ReBuildIndexes'
GO
ALTER PROCEDURE dbo.usp_ITSupport_ReBuildIndexes
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 21st May 2013
-- Description	: Rebuilds Server Indexes
-----------------------------------------------------------------------------------
As
Begin
Set NoCount On

----------------------------------------------------------------------------------------------   
Declare @FillFactor Int = 80
-- The fill-factor option is provided for fine-tuning index data storage and performance. 
-- When an index is created or rebuilt, the fill-factor value determines the percentage of
-- space on each leaf-level page to be filled with data, reserving the remainder on each
-- page as free space for future growth. For example, specifying a fill-factor value of 80
-- means that 20 percent of each leaf-level page will be left empty, providing space for index
-- expansion as data is added to the underlying table. The empty space is reserved between
-- the index rows rather than at the end of the index.
----------------------------------------------------------------------------------------------
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#Rebuild_Database%') and [TYPE] = 'U') Drop Table #Rebuild_Data
Create Table #Rebuild_Database([ID] [int] IDENTITY(1,1),[Database] VarChar(255) NOT NULL)
Insert into #Rebuild_Database 
Select Name From Master.dbo.SysDatabases Where Name Not In ('master','msdb','tempdb','model','distribution', 'ReportServer', 'ReportServerTempDB', 'TillOFFLINE') Order By 1  
------------------------------------------
-- Process Database
------------------------------------------
Print ('Starting Database ReBuild Index Process')
Declare @RBD_ID int
Declare @RBD_MAXID int
Select  @RBD_MAXID = MAX(ID)FROM #Rebuild_Database
Set		@RBD_ID = 1
Declare @Database VarChar(255)   
Declare @Table VarChar(255)  
Declare @Cmd NVarChar(600) 
While	(@RBD_ID <= @RBD_MAXID)
	Begin
		Set @Database = (Select LTRIM(RTRIM([Database])) From #Rebuild_Database Where ID = @RBD_ID)
		Print('Processing Database : ' + LTRIM(RTRIM(Cast(@Database as Char)))) 
		Select @Cmd = 'Select ''['' + Table_Catalog + ''].['' + Table_Schema + ''].['' + Table_Name + '']'' as TableName From ' + @Database + '.INFORMATION_SCHEMA.TABLES Where Table_Type = ''BASE TABLE'' Order By TableName Asc'
  		If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#Rebuild_Tables%') and [TYPE] = 'U') Drop Table #Rebuild_Tables
		Create Table #Rebuild_Tables([ID] [int] IDENTITY(1,1),[Tables] VarChar(255) NOT NULL)
		Insert into #Rebuild_Tables 
		Exec (@Cmd)
		Declare @RBT_ID int
		Declare @RBT_MAXID int
		Select  @RBT_MAXID = MAX(ID)FROM #Rebuild_Tables
		Set		@RBT_ID = 1 
		Declare @TLen int = (Select LEN(@RBT_MAXID))
		Declare @Mask Char(1) = '0'
		Declare @RBT_RefId Char(4)
		While (@RBT_ID <= @RBT_MAXID)   
			Begin
				Set @Table = (Select LTRIM(RTRIM([Tables])) From #Rebuild_Tables Where ID = @RBT_ID)
				Set @RBT_RefId = Right(REPLICATE(@Mask, @TLen) + Cast(@RBT_ID as Varchar),@TLen)
				   
				If(@@MICROSOFTVERSION/POWER(2, 24) >= 9)
					Begin
						-- SQL 2005+
						Print ('RI (' + LTRIM(RTRIM(Cast(@RBT_RefId as Char))) + '/' + LTRIM(RTRIM(Cast(@RBT_MAXID as Char))) + '): ' + LTRIM(@Table)) 
						Set @Cmd = 'Alter Index All On ' + @Table + ' Rebuild With (FillFactor = ' + Convert(VarChar(3),@FillFactor) + ')' 
						Exec(@Cmd) 
					End
				Else
					Begin
						-- Pre SQL 2005
						Print ('RI (' + LTRIM(RTRIM(Cast(@RBT_RefId as Char))) + '/' + LTRIM(RTRIM(Cast(@RBT_MAXID as Char))) + '): ' + LTRIM(@Table)) 
						Dbcc DBReindex(@Table,' ',@FillFactor)  
					End
				Set @RBT_ID = @RBT_ID+1   
			End   
		Set @RBD_ID = @RBD_ID+1
	End  
If @@Error = 0 Print ('Processing Complete')

End
GO

