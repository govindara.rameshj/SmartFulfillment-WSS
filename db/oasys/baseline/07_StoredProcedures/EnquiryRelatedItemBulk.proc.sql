﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryRelatedItemBulk]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryRelatedItemBulk'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryRelatedItemBulk] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryRelatedItemBulk'
GO
ALTER PROCEDURE [dbo].[EnquiryRelatedItemBulk]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	select
		1			as 'RowId',
		rl.PPOS		as 'SkuNumber',
		sk.DESCR	as 'Description',
		sk.PRIC		as 'Price',
		sk.ONHA		as 'QtyOnHand'
	from
		RELITM rl
	inner join
		STKMAS sk	on sk.SKUN = rl.PPOS
	where
		rl.SPOS = @SkuNumber

END
GO

