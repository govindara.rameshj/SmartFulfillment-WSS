﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PriceChangeLoad]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PriceChangeLoad'
	EXEC ('CREATE PROCEDURE [dbo].[PriceChangeLoad] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PriceChangeLoad'
GO
ALTER PROCedure [dbo].[PriceChangeLoad]
   @Date           date,
   @ShelfEdgeLabel bit = null
as
set nocount on

select a.*
from PRCCHG a
inner join STKMAS b
      on b.SKUN = a.SKUN
where a.PSTA = 'U'
and  (@ShelfEdgeLabel is null or a.SHEL = @ShelfEdgeLabel)
and  (a.PDAT <= @Date or a.AUDT <= @Date)
GO

