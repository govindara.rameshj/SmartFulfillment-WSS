﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingCurrencySymbol]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingCurrencySymbol'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingCurrencySymbol] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingCurrencySymbol'
GO
ALTER PROCedure NewBankingCurrencySymbol
   @CurrencySymbol char(3) output
as
begin
set nocount on
set @CurrencySymbol = (select PrintSymbol from SystemCurrency where IsDefault = 1)
end
GO

