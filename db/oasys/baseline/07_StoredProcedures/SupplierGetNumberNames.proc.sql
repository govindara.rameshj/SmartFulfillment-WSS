﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierGetNumberNames]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierGetNumberNames'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierGetNumberNames] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierGetNumberNames'
GO
ALTER PROCedure [dbo].[SupplierGetNumberNames]
AS
BEGIN
	declare @OutputTable TABLE ( Id char(5), Display varchar(150) )

	insert into @OutputTable values (null, '---All---');
	insert into @OutputTable select SUPN, SUPN + ' ' + NAME from SUPMAS;
	
	select * from @OutputTable
	
END
GO

