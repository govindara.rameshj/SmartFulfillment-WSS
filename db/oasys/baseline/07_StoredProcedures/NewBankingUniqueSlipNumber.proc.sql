﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingUniqueSlipNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingUniqueSlipNumber'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingUniqueSlipNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingUniqueSlipNumber'
GO
ALTER PROCedure NewBankingUniqueSlipNumber
   @SlipNumber char(20),
   @UniqueSlip bit output
as
begin
set nocount on
if exists (select * from SafeBagsDenoms where SlipNumber  = @SlipNumber)
   --not unique
   set @UniqueSlip = 0
else
   --unique
   set @UniqueSlip = 1
end
GO

