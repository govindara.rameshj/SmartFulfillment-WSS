﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetStockLogKey]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetStockLogKey'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetStockLogKey] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetStockLogKey'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 02/08/2011
-- Description:	Get last Stock Log Record for Sku
-- =============================================
ALTER PROCEDURE [dbo].[StockGetStockLogKey]
	-- Add the parameters for the stored procedure here
	@SkuNumber Char(6) 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select 
		Max(TKEY) as MaxKey 
    FROM STKLOG 
    Where TYPE <> '99' and DAYN > 0 and SKUN = @SkuNumber
END
GO

