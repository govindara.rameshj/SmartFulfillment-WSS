﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PicWorksheetHierarchy]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PicWorksheetHierarchy'
	EXEC ('CREATE PROCEDURE [dbo].[PicWorksheetHierarchy] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PicWorksheetHierarchy'
GO
ALTER PROCEDURE [dbo].[PicWorksheetHierarchy]
@CategoryNumber CHAR (6)=null
AS
BEGIN
	SET NOCOUNT ON;

	select
		sk.SKUN				as 'SkuNumber',
		sk.DESCR			as 'Description',
		sk.PACK				as 'PackSize',		
		(case sk.IRIS when 1 then (select rl.PPOS from RELITM rl where rl.SPOS=sk.SKUN) end) as 'SkuRelated',
		''					as 'QtyShopFloor',
		''					as 'QtyWarehouse',
		''					as 'QtyTotal',
		''					as 'QtyPreSold',
		sk.ONHA				as 'QtyOnHand',
		''					as 'QtyVariance',
		sk.PRIC				as 'Price',
		'YES / NO'			as 'SELYesNo',
		hc.DESCR			as 'HieCategoryName'
	from
		STKMAS sk
	inner join
		HIECAT hc on hc.NUMB=sk.CTGY
	where
		(@CategoryNumber is null) or (@CategoryNumber is not null and sk.CTGY = @CategoryNumber)
	order by
		sk.SKUN

END
GO

