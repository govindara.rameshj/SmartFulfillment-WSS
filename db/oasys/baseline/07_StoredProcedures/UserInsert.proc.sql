﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UserInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure UserInsert'
	EXEC ('CREATE PROCEDURE [dbo].[UserInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure UserInsert'
GO
-- =============================================
-- Author       : Partha Dutta
-- Create date  : 15/06/2011
-- Referral No  : 376
-- Description  : New versions of UserInsert & UserUpdate
--                Add stored procedures usp_SystemUsersUpdateRSCASH, usp_SystemUsersUpdateCASMAS, usp_SystemUsersUpdateSYSPAS
--                Add stored procedures usp_SecurityProfileUpdateSystemUsers
-- =============================================


-- =============================================
-- Author:		Not Known
-- Create date: 1st March 2011
-- Description:	Baseline version
--
-- Modified by  : Partha Dutta
-- Modified date: 15/06/2011
-- Description  : Referral 376
--                IsSupervisor & IsManager data now supplied, default to false if stored procedure does not receive any data
--
-- =============================================
ALTER PROCEDURE [dbo].[UserInsert]
 @Id INT, @EmployeeCode CHAR (3), @Name CHAR (35), @Initials CHAR (5), @Position CHAR (20),
 @PayrollId CHAR (20), @Outlet CHAR (2), @TillReceiptName CHAR (35), @LanguageCode CHAR (3), @ProfileId INT, @Password CHAR (5),
 @PasswordExpires DATE, @SuperPassword CHAR (5)=null, @SuperPasswordExpires DATE=null,
 @IsManager    bit = 0,
 @IsSupervisor bit = 0
AS
begin
	set nocount on;
	
	declare @rowCount int;
	set @rowCount=0;
	
	INSERT INTO 
		SystemUsers (
		[ID],
		[EmployeeCode],
		[Name],
		[Initials],
		[Position],
		[PayrollID],
		[Outlet],
		[TillReceiptName],
		[LanguageCode],
		[SecurityProfileID],
		[Password],
		[PasswordExpires],
		IsManager,
		IsSupervisor
	) VALUES (
		@Id,
		@EmployeeCode,
		@Name,
		@Initials,
		@Position,
		@PayrollId,
		@Outlet,
		@TillReceiptName,
		@LanguageCode,
		@ProfileId,
		@Password,
		@PasswordExpires,
		@IsManager,
        @IsSupervisor
		)
		
	set @rowCount = @@ROWCOUNT;
		
	if @SuperPassword is not null
	begin
		update 
			SystemUsers
		set 
			[SupervisorPassword]	= @SuperPassword,
			[SupervisorPwdExpires]	= @SuperPasswordExpires
		 where 
			Id = @Id;
	end
		
	return @rowcount;
	
end
GO

