﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetAllStores]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GetAllStores'
	EXEC ('CREATE PROCEDURE [dbo].[GetAllStores] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GetAllStores'
GO
ALTER PROCEDURE [dbo].[GetAllStores]

AS
begin
	SET NOCOUNT ON;
	
	SELECT     
		Id,
		Name,
		Address1,
		Address2,
		Address3,
		Address4,
		Address5,
		PostCode,
		PhoneNumber,
		FaxNumber,
		Manager,
		RegionCode,
		CountryCode,
		IsClosed,
		IsHeadOffice
	FROM 
		Store
END
GO

