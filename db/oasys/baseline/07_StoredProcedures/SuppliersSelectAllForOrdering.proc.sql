﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SuppliersSelectAllForOrdering]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SuppliersSelectAllForOrdering'
	EXEC ('CREATE PROCEDURE [dbo].[SuppliersSelectAllForOrdering] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SuppliersSelectAllForOrdering'
GO
ALTER PROCEDURE [dbo].[SuppliersSelectAllForOrdering]
@Type CHAR (1)='A'
AS
DECLARE @DayWeek	INT;
SET		@DayWeek = DATEPART(weekday, getdate());

select distinct 
					sm.SUPN, 
					sm.NAME, 
					sm.ODNO, 
					sm.DLPO, 
					sm.QCTL, 
					sm.QFLG, 
					sm.SOQDate,
					sm.PalletCheck, 
					sd.TNET, 
					sd.BBCC, 
					sd.NOTE,
					sd.PHO1,
					sd.ReviewDay0, 
					sd.ReviewDay1, sd.ReviewDay2, sd.ReviewDay3, 
					sd.ReviewDay4, sd.ReviewDay5, sd.ReviewDay6, 
					sd.VLED1, 
					sd.VLED2, 
					sd.VLED3, 
					sd.VLED4,
					sd.VLED5, 
					sd.VLED6,
					sd.VLED7, 
					sd.LEAD, 
					sd.MCPT, 
					sd.MCPV, 
					sd.MCPC, 
					sd.MCPW

from				SUPDET sd
inner join			SUPMAS sm ON sd.SUPN = sm.SUPN AND sd.DEPO = sm.ODNO 
right outer join	STKMAS sk ON sm.SUPN = sk.SUP1

WHERE				sd.TYPE = 'O' 
AND					sk.IDEL = 0 
AND					sk.IOBS = 0
AND					sk.IRIS = 0
AND					sk.INON = 0
AND					sk.NOOR = 0
AND					CASE @DayWeek
						WHEN 1 THEN sd.ReviewDay0
						WHEN 2 THEN sd.ReviewDay1
						WHEN 3 THEN sd.ReviewDay2
						WHEN 4 THEN sd.ReviewDay3
						WHEN 5 THEN sd.ReviewDay4
						WHEN 6 THEN sd.ReviewDay5
						WHEN 7 THEN sd.ReviewDay6
					END = 1	

AND					(@Type='A' 
					OR (@Type='D' AND sd.BBCC = '') 
					OR (@Type='W' AND sd.BBCC <>''))
			
AND					(sd.OCSD IS NULL OR sd.OCSD > GETDATE()
					OR (sd.OCED IS NOT NULL AND sd.OCED < GETDATE()))			

ORDER BY			sm.NAME
GO

