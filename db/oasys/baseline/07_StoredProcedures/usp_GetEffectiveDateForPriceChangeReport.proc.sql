﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEffectiveDateForPriceChangeReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEffectiveDateForPriceChangeReport'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEffectiveDateForPriceChangeReport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEffectiveDateForPriceChangeReport'
GO
ALTER PROCEDURE [dbo].[usp_GetEffectiveDateForPriceChangeReport]
    @SkuNumber varchar(6),
    @EventNumber varchar(6),
    @Priority varchar(2)
AS
BEGIN

    SELECT 'StartDate' = dbo.udf_GetLatestPriceChangeDateForSKU(@SkuNumber)

END
GO

