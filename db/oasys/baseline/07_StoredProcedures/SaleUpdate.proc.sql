﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SaleUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleUpdate'
GO
-- ===============================================================================
-- Author         : Partha Dutta
-- Date	          : 23/09/2011
-- Change Request : CR0054-01
-- TFS User Story : 2574
-- TFS Task ID    : 2595
-- Description    : BO-120 iteration's database layer (rollout/rollback scripts) refactored to deal with deployment to 
--                     A. Standard Store
--                     B. eStore 120
--                     C. eWarehouse 076
-- ===============================================================================

ALTER PROCEDURE [dbo].[SaleUpdate]
@TransactionDate DATE, 
@TillNumber CHAR (2),
@TransactionNumber CHAR (4), 
@TransactionTime CHAR (6),
@CashierID CHAR (3), 
@SupervisorCashierNo CHAR (3), 
@TransactionCode CHAR (2), 
@ODCode INT, 
@ReasonCode INT, 
@Description CHAR (20), 
@OrderNumber CHAR (6), 
@AccountSale BIT=0, 
@Void BIT=0, 
@VoidSupervisor CHAR (3), 
@TrainingMode BIT=0, 
@Processed BIT=0, 
@DocumentNumber CHAR (8), 
@SupervisorUsed BIT=0,
@StoreNumber CHAR (3), 
@MerchandiseAmount DECIMAL (9, 2), 
@NonMerchandiseAmount DECIMAL (9, 2), 
@TaxAmount DECIMAL (9, 2), 
@Discount DECIMAL (9, 2), 
@DiscountSupervisor CHAR (3),
@TotalSaleAmount DECIMAL (9, 2), 
@AccountNumber CHAR (6), 
@AccountCardNumber CHAR (6), 
@PCCollection BIT=0, 
@FromDCOrders BIT=0, 
@TransactionComplete BIT=1, 
@EmployeeDiscountOnly BIT=0, 
@RefundCashierNo CHAR (3), 
@RefundSupervisor CHAR (3), 
@Parked BIT=0, 
@RefundManager CHAR (3), 
@TenderOverrideCode CHAR (2), 
@UnParked BIT=0, 
@TransactionOnLine BIT=1, 
@TokensPrinted DECIMAL (3, 0), 
@ColleagueNumber CHAR (9), 
@SaveStatus CHAR (2), 
@SaveSequence CHAR (4), 
@CBBUpdate CHAR (8), 
@DiscountCardNo CHAR (19), 
@RTI CHAR (1)

AS
BEGIN
SET NOCOUNT ON;
		
UPDATE DLTOTS
SET
	
  [CASH] = @CashierID
 ,[TIME] = @TransactionTime
 ,[SUPV] = @SupervisorCashierNo
 ,[TCOD] = @TransactionCode 
 ,[OPEN] = @ODCode
 ,[MISC] = @ReasonCode
 ,[DESCR]= @Description
 ,[ORDN] = @OrderNumber
 ,[ACCT] = @AccountSale
 ,[VOID] = @Void
 ,[VSUP] = @VoidSupervisor
 ,[TMOD] = @TrainingMode
 ,[PROC] = @Processed
 ,[DOCN] = @DocumentNumber
 ,[SUSE] = @SupervisorUsed
 ,[STOR] = @StoreNumber
 ,[MERC] = @MerchandiseAmount
 ,[NMER] = @NonMerchandiseAmount
 ,[TAXA] = @TaxAmount
 ,[DISC] = @Discount
 ,[DSUP] = @DiscountSupervisor
 ,[TOTL] = @TotalSaleAmount
 ,[ACCN] = @AccountNumber
 ,[CARD] = @AccountCardNumber
 ,[AUPD] = @PCCollection
 ,[BACK] = @FromDCOrders
 ,[ICOM] = @TransactionComplete
 ,[IEMP] = @EmployeeDiscountOnly
 ,[RCAS] = @RefundCashierNo
 ,[RSUP] = @RefundSupervisor
 ,[PARK] = @Parked
 ,[RMAN] = @RefundManager
 ,[TOCD] = @TenderOverrideCode
 ,[PKRC] = @UnParked
 ,[REMO] = @TransactionOnLine 
 ,[GTPN] = @TokensPrinted
 ,[CCRD] = @ColleagueNumber
 ,[SSTA] = @SaveStatus
 ,[SSEQ] = @SaveSequence
 ,[CBBU] = @CBBUpdate
 ,[CARD_NO] = @DiscountCardNo
 ,[RTI] = @RTI
    
WHERE
     DATE1 = @TransactionDate
     AND TILL = @TillNumber
     AND [TRAN] = @TransactionNumber 	
	
return @@rowcount
END
GO

