﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetSTKMASDataForStockLog]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetSTKMASDataForStockLog'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetSTKMASDataForStockLog] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetSTKMASDataForStockLog'
GO
-- =============================================
-- Author: Michael O'Cain
-- Create date: 02?08/2011
-- Description:	Get STKMAS record for use in the STKLOG record for the skun entered
-- =============================================
ALTER PROCEDURE StockGetSTKMASDataForStockLog 
	-- Add the parameters for the stored procedure here
	@SkuNumber Char(6) 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
           st.SKUN	as PartCode,
           st.ONHA	as QuantityOnHand,
           st.RETQ	as UnitsInOpenReturns,
           st.MDNQ	as MarkDownQuantity,
           st.WTFQ	as WriteOffQuantity,
           st.PRIC	as NormalSellPrice,
           st.SALU1	as UnitsSoldYesterday,
           st.SALU2	as UnitsSoldThisWeek,
           st.SALU4	as UnitsSoldThisPeriod,
           st.SALU6	as UnitsSoldThisYear,
           st.SALV1	as ValueSoldYesterday,
           st.SALV2	as ValueSoldThisWeek,
           st.SALV4	as ValueSoldThisPeriod,
           st.SALV6	as ValueSoldThisYear,
           st.SALT	as SaleTypeAttribute 
    FROM STKMAS st
    WHERE  st.SKUN =  @SkuNumber
END
GO

