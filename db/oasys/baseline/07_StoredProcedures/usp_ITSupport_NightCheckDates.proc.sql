﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_NightCheckDates]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_NightCheckDates'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_NightCheckDates] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_NightCheckDates'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_NightCheckDates]
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 4th June 2013
-- 
-- Task     : Get System Dates.
-----------------------------------------------------------------------------------

AS
BEGIN
SET NOCOUNT ON;

Declare @Store Char(4)
Declare @YDTH_TDNR Date
Declare @TMNR_TDTH Date
Declare @LDNR Date
Declare @LDRF_Temp Char(8)
Declare @LDRF Date

Select 
	@Store = '8' + LTRIM(RTRIM(ro.STOR)),
	@YDTH_TDNR = sd.TODT,
	@TMNR_TDTH = sd.TMDT,
	@LDNR = sd.LDAT,
	@LDRF_Temp = ro.DOLR	
From RETOPT as ro
Inner Join SYSDAT as sd on sd.FKEY = ro.FKEY
Where ro.FKEY = '01'

Declare @DOLR_Stripped Char(6) = Replace(@LDRF_Temp,'/','')
Set @LDRF = (Select Convert(Date, Stuff(Stuff(@DOLR_Stripped,5,0,'.'),3,0,'.'), 4))

If @Store is Null Set @Store = '8000'
If @YDTH_TDNR is Null Set @YDTH_TDNR = '2010-01-01'
If @TMNR_TDTH is Null Set @TMNR_TDTH = '2010-01-01'
If @LDNR is Null Set @LDNR = '2010-01-01'
If @LDRF is Null Set @LDRF = '2010-01-01'

Select	CAST(@Store as Int) as 'Store', 
		@YDTH_TDNR as 'YDTH_TDNR',
		@TMNR_TDTH as 'TMNR_TDTH',
		@LDNR as 'LDNR',
		@LDRF as 'LDRF'

END;
GO

