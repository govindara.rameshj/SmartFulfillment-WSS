﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QodGetNonZeroStockOrOutstandingIbt]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure QodGetNonZeroStockOrOutstandingIbt'
	EXEC ('CREATE PROCEDURE [dbo].[QodGetNonZeroStockOrOutstandingIbt] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure QodGetNonZeroStockOrOutstandingIbt'
GO
ALTER PROCedure [dbo].[QodGetNonZeroStockOrOutstandingIbt]
as
begin

select * from 
	vwQodNonZeroStockOrOutstandingIbt
where
	not (QtyOnHand=0 and QtyOutstanding=0)
order 
	by SkuNumber;

select 
	cl.SKUN					as SkuNumber,
	cl.NUMB					as OrderNumber,
	cl.LINE					as Number,
	vc.OMOrderNumber,
	vc.CUST					as VendaNumber,
	cl.QTYO					as QtyOrdered,
	cl.QTYR					as QtyReturned,
	cl.QTYT					as QtyTaken,
	cl.QTYO-cl.QTYR-cl.QTYT as QtyOutstanding,
	cl.DeliveryStatus
from 
	corlin cl
--Tables CORHDR and CORHDR4 have been replaced with the view vwCORHDRFull	
left join vwCORHDRFull as vc on cl.NUMB = vc.NUMB 
where
	cl.DeliveryStatus <= 399
	and cl.QTYO - cl.QTYR - cl.QTYT <> 0
	and (vc.NUMB is null or vc.SHOW_IN_UI = 1)
order by cl.SKUN
end
GO

