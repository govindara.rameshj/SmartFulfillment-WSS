﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdatePriceChangeRecordResetLabelWithOriginalDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdatePriceChangeRecordResetLabelWithOriginalDate'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdatePriceChangeRecordResetLabelWithOriginalDate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdatePriceChangeRecordResetLabelWithOriginalDate'
GO
ALTER PROCEDURE [dbo].[usp_UpdatePriceChangeRecordResetLabelWithOriginalDate] 
	@Skun Char(6),
	@OriginalPDAT Date,
	@OriginalEventNumber Char(6),
	@OriginalPrice decimal(10,2),
	@OriginalStatus Char(1),
	@StartDate DATE,
	@Price decimal(10,2),
	@Status Char(1),
	@EventNumber Char(6),
	@Priority Char(2),
	@AutoApplyDate Date, 
	@IsShelfLabel bit,
	@SmallLabel bit,
	@MediumLabel bit,
	@LargeLabel bit
	
AS
BEGIN
	SET NOCOUNT ON;

Update [Oasys].[dbo].[PRCCHG]
        set 
			[PDAT] = @StartDate	
		   ,[EVNT] = @EventNumber
           ,[PRIO] = @Priority
           ,[PRIC] = @Price
           ,[PSTA] = @Status
           ,[AUDT] = @AutoApplyDate
           ,[SHEL] = @IsShelfLabel
           ,[LABS] = @SmallLabel
           ,[LABM] = @MediumLabel
           ,[LABL] = @LargeLabel
		   ,[AUAP] = NULL
        Where
            [Skun] = @Skun And 
			[PDAT] = @OriginalPDAT And
			[EVNT] = @OriginalEventNumber And
			[PSTA] = @OriginalStatus And
			[PRIC] = @OriginalPrice 
Return @@Rowcount            
End
GO

