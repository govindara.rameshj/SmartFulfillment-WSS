﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptGetOrderVariances]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptGetOrderVariances'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptGetOrderVariances] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptGetOrderVariances'
GO
ALTER PROCEDURE [dbo].[ReceiptGetOrderVariances]
	@DateStart			date, 
	@DateEnd			date, 
	@OverVarianceType	NVARCHAR (1)
AS
BEGIN
	SET NOCOUNT ON;

	select
		DS."0PON"						as 'PoNumber',					
		DS."0SUP"						as 'SupplierNumber',		
		SM.NAME							as 'SupplierName',		
		DD.SKUN							as 'SkuNumber',				
		SK.DESCR						as 'Description',			
		DD.ORDQ							as 'QtyOrdered',				
		DD.RECQ							as 'QtyReceived',			
		(DD.RECQ - DD.ORDQ)				as 'QtyShortOver',	
		DD.PRIC							as 'Price',				
		((DD.RECQ - DD.ORDQ)* DD.PRIC)	as 'ValueShortOver'	
	from
		DRLDET DD
	inner join 
		DRLSUM DS on DS.NUMB = DD.NUMB
	inner join 
		SUPMAS SM on DS.[0SUP] = SM.SUPN
	inner join 
		STKMAS SK on DD.SKUN = SK.SKUN
	where 
			DS.[TYPE]	= 0 
		and DS.IPSO		= 0  
		and DS.[0DL3]	<> 'IMPORT'  
		and	DS.DATE1 >= @DateStart
		and DS.DATE1 <= @DateEnd
		and (select DELE from PURLIN where DD.POLN=TKEY) = 1
		and ((@OverVarianceType = 'S' and	DD.RECQ < DD.ORDQ) 
			Or 	(@OverVarianceType = 'O' and DD.RECQ > DD.ORDQ))
	
END
GO

