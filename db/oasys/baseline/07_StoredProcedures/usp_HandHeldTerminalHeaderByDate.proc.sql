﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_HandHeldTerminalHeaderByDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_HandHeldTerminalHeaderByDate'
	EXEC ('CREATE PROCEDURE [dbo].[usp_HandHeldTerminalHeaderByDate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_HandHeldTerminalHeaderByDate'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 30-06-2011
-- Description:	Get HHTDET Record by date supplied
-- =============================================
ALTER PROCEDURE dbo.usp_HandHeldTerminalHeaderByDate 
	-- Add the parameters for the stored procedure here
	@SelectedDate Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select top 1 * from HHTHDR 
	where Date1 = @SelectedDate  
	order by DATE1 desc

END
GO

