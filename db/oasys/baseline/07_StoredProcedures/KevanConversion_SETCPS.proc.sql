﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SETCPS]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SETCPS'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SETCPS] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SETCPS'
GO
ALTER PROCEDURE dbo.KevanConversion_SETCPS
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 11 - Change CAPSCAN Path to New 'Back Office' System Path
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

UPDATE Parameters
SET StringValue = 'F:\Wix\Capscan'
WHERE ParameterID = '921'

END;
GO

