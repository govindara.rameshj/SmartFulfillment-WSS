﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TransactionLinesGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure TransactionLinesGet'
	EXEC ('CREATE PROCEDURE [dbo].[TransactionLinesGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure TransactionLinesGet'
GO
ALTER PROCEDURE [dbo].[TransactionLinesGet]
	@TranDate	date,
	@TillId		int,
	@TranNumber	char(4)
AS
begin
	SET NOCOUNT ON;

	select
        DATE1	as TranDate,
        TILL	as TillId,
        [TRAN]	as TranNumber,
        NUMB	as Number,
        SKUN	as SkuNumber,
        QUAN	as Quantity,
        PRIC	as Price
	from
		DLLINE
	where
		DATE1 = @TranDate
		and TILL=@TillId
		and [TRAN]=@TranNumber
	order by
		NUMB
end
GO

