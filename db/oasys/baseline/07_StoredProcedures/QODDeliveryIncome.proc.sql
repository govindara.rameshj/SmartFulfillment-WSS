﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODDeliveryIncome]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure QODDeliveryIncome'
	EXEC ('CREATE PROCEDURE [dbo].[QODDeliveryIncome] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure QODDeliveryIncome'
GO
-- =============================================
-- Author:      Mike O'Cain
-- Create date: 07/10/2009
-- Description: Delivery Income Detail
-- =============================================
ALTER PROCEDURE [dbo].[QODDeliveryIncome] 
    @DateStart datetime = NULL, 
    @DateEnd datetime = NULL
AS
BEGIN
    SET NOCOUNT ON;

    -- Insert statements for procedure here
        SELECT 
           convert(varchar,vc.DELD,103)     as 'Date'
         , DATENAME(dw,vc.DELD)             as 'Day'     
         , vc.Numb                          as 'OrderNumber'
         , vc.REVI                          as 'RevisionNumber'
         , vc.NAME                          as 'CustomerName'
         , vc.POST                          as 'PostCode'
         , vc.DCST                          as 'DeliveryCharge'
         , vc.WGHT                          as 'Weight'
         , vc.VOLU                          as 'Volume'
         , coalesce(su.Name, 'Unknown')     as 'OrderTaker'
         --Tables CORHDR and CORHDR4 have been replaced with the view vwCORHDRFull
         FROM vwCORHDRFull as vc 
         LEFT JOIN SystemUsers as su on vc.OEID = su.EmployeeCode
         WHERE vc.SHOW_IN_UI = 1 and vc.DELI = 1 and (vc.DELD >= @DateStart and vc.DELD <= @DateEnd) and vc.OEID is not null
         ORDER BY vc.DELD
END
GO

