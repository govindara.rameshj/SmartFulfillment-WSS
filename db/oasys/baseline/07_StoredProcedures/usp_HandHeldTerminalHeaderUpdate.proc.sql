﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_HandHeldTerminalHeaderUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_HandHeldTerminalHeaderUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[usp_HandHeldTerminalHeaderUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_HandHeldTerminalHeaderUpdate'
GO
-- =============================================
-- Author       : Partha Dutta
-- Create date  : 25/06/2011
-- Referral No  : 773
-- Description  : Baseline version
--                Update HHT Header record
-- =============================================

ALTER PROCedure dbo.usp_HandHeldTerminalHeaderUpdate
   @SelectedDate      date,
   @ItemsToCount      int     = null,
   @ItemsCounted      int     = null,
   @AdjustmentApplied bit     = null,
   @AuthorisationID   char(3) = null,
   @IsLocked          bit     = null
as
set nocount on
update HHTHDR set NUMB     = case 
                               when @ItemsToCount is null then NUMB
                               else @ItemsToCount
                             end,
                  DONE     = case 
                               when @ItemsCounted is null then DONE
                               else @ItemsCounted
                             end,
                  IADJ     = case 
                               when @AdjustmentApplied is null then IADJ
                               else @AdjustmentApplied
                             end,
                  AUTH     = case 
                               when @AuthorisationID is null then AUTH
                               else @AuthorisationID
                             end,
                  IsLocked = case 
                               when @IsLocked is null then IsLocked
                               else @IsLocked
                             end
where DATE1 = @SelectedDate
GO

