﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_HandHeldTerminalDetailUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_HandHeldTerminalDetailUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[usp_HandHeldTerminalDetailUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_HandHeldTerminalDetailUpdate'
GO
-- =============================================
-- Author       : Partha Dutta
-- Create date  : 25/06/2011
-- Referral No  : 773
-- Description  : Baseline version
--                Update HHT Detail record
-- =============================================

ALTER PROCedure dbo.usp_HandHeldTerminalDetailUpdate
   @SelectedDate      date,
   @ProductCode       char(6),
   @AdjustmentApplied bit
as
set nocount on
update HHTDET set IADJ = @AdjustmentApplied where DATE1 = @SelectedDate and SKUN = @ProductCode
GO

