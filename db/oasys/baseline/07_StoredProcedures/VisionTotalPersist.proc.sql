﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionTotalPersist]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VisionTotalPersist'
	EXEC ('CREATE PROCEDURE [dbo].[VisionTotalPersist] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VisionTotalPersist'
GO
ALTER PROCEDURE [dbo].[VisionTotalPersist]
@TranDate DATE, @TillId INT, @TranNumber INT, @CashierId INT, @TranDateTime CHAR (8), @Type CHAR (2)=null, @ReasonCode CHAR (2)=null, @ReasonDescription VARCHAR (50)=null, @OrderNumber CHAR (6)=null, @Value DECIMAL (9, 2), @ValueMerchandising DECIMAL (9, 2), @ValueNonMerchandising DECIMAL (9, 2), @ValueTax DECIMAL (9, 2), @ValueDiscount DECIMAL (9, 2), @ValueColleagueDiscount DECIMAL (9, 2), @IsColleagueDiscount BIT, @IsPivotal BIT
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @rowcount int;
	set @rowcount=0;	

	--update record if exists
	update
		VisionTotal
	set
		CashierId		= @CashierId,
		TranDateTime	= @TranDateTime,
		Type			= @Type,
		ReasonCode		= @ReasonCode,
		ReasonDescription = @ReasonDescription,
		OrderNumber		= @OrderNumber,
		Value			= @Value,
		ValueMerchandising = @ValueMerchandising,
		ValueNonMerchandising = @ValueNonMerchandising,
		ValueTax		= @ValueTax,
		ValueDiscount	= @ValueDiscount,
		ValueColleagueDiscount = @ValueColleagueDiscount,
		IsColleagueDiscount	= @IsColleagueDiscount,
		IsPivotal		= @IsPivotal
	where
		TranDate	= @TranDate and
		TillId		= @TillId and
		TranNumber	= @TranNumber;

	--get number of rows affected
	set @rowcount = @@ROWCOUNT;
	
	--if nothing updated then insert
	if @rowcount=0
	begin
		insert into 
			VisionTotal (
			TranDate,
			TillId,
			TranNumber,
			CashierId,
			TranDateTime,
			Type,
			ReasonCode,
			ReasonDescription,
			OrderNumber,
			Value,
			ValueMerchandising,
			ValueNonMerchandising,
			ValueTax,
			ValueDiscount,
			ValueColleagueDiscount,
			IsColleagueDiscount,
			IsPivotal
		) values (
			@TranDate,
			@TillId,
			@TranNumber,
			@CashierId,
			@TranDateTime,
			@Type,
			@ReasonCode,
			@ReasonDescription,
			@OrderNumber,
			@Value,
			@ValueMerchandising,
			@ValueNonMerchandising,
			@ValueTax,
			@ValueDiscount,
			@ValueColleagueDiscount,
			@IsColleagueDiscount,
			@IsPivotal
		);
			
		set @rowcount = @@ROWCOUNT;
	end

	return @rowcount;
END
GO

