﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptIbtInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptIbtInsert'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptIbtInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptIbtInsert'
GO
ALTER PROCEDURE [dbo].[ReceiptIbtInsert]
@Type CHAR (1), 
@DateReceipt DATE, @UserId INT, 
@Initials CHAR (5) = null, 
@Comments CHAR (75) = null, 
@Value DECIMAL (9, 2) = 0, 
@IbtStoreId CHAR (3) = null, 
@IbtConsignmentNumber CHAR (6) = null, 
@IbtNeedsPrinting BIT = 1, 
@RtiState CHAR (1) = null, 
@Number CHAR (6) OUTPUT
AS
BEGIN
    SET NOCOUNT ON;
    
    --get next receipt number from system numbers
    declare @NextNumber int;
    exec    @NextNumber = SystemNumbersGetNext @Id = 4
    
    --convert next number to number
    set @Number = Convert(Char(6), @NextNumber);
    
    --insert into drlsum
    insert into DRLSUM (                
        NUMB,
        [TYPE],
        DATE1,
        [INIT],
        INFO,
        VALU,
        [1STR],
        [1IBT],
        [1PRT],
        RTI,
        EmployeeId
    ) values (
        @Number,
        @Type,
        @DateReceipt,
        @Initials,
        @Comments,
        @Value,
        @IbtStoreId,
        @IbtConsignmentNumber,
        @IbtNeedsPrinting,
        @RtiState,
        @UserId
    )
    
    return @@rowcount
END
GO

