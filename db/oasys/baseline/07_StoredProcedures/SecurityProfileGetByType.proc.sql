﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SecurityProfileGetByType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SecurityProfileGetByType'
	EXEC ('CREATE PROCEDURE [dbo].[SecurityProfileGetByType] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SecurityProfileGetByType'
GO
ALTER PROCEDURE [dbo].[SecurityProfileGetByType]
	@ProfileType	char(1)='A'
AS
begin
	SET NOCOUNT ON;

	select 
		ID					as Id,
		ProfileType,
		Description,
		Position,
		IsAreaManager,
		PasswordValidFor	as DaysPasswordValid,
		IsDeleted,
		DeletedBy,
		DeletedDate			as DateDeleted,
		DateLastEdited,
		IsSystemAdmin,
		IsHelpDeskUser
	from
		SecurityProfile
	where
		ProfileType=@ProfileType
		
end
GO

