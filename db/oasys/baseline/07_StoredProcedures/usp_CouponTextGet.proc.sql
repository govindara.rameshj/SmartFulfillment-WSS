﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CouponTextGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_CouponTextGet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_CouponTextGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_CouponTextGet'
GO
ALTER PROCedure [dbo].[usp_CouponTextGet]
	@CouponId       CHAR (7),
	@SequenceNumber CHAR(2) = NULL,
	@IncludeDeleted BIT = 0
	
As
Begin
	Set NOCOUNT ON;

Select 
	ct.[COUPONID],
	ct.[SEQUENCENO],
	ct.[PRINTSIZE],
	ct.[TEXTALIGN],
	ct.[PRINTTEXT],
	ct.[DELETED]
From
	[COUPONTEXT] ct
Where
	ct.[COUPONID] = @CouponId
And
	(
		@SequenceNumber Is Null
	Or
		(
			ct.[SEQUENCENO] = @SequenceNumber
		And
			Not @SequenceNumber Is Null
		)
	)
And
	(
		@IncludeDeleted = 1
	Or
		ct.[DELETED] = 0
	)
End
GO

