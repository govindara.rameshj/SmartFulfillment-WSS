﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_PERNTU]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_PERNTU'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_PERNTU] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_PERNTU'
GO
ALTER PROCEDURE dbo.KevanConversion_PERNTU
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 27 - Apply Security Access to NT Authority User on Server
------------------------------------------------------------------------------------
-- Updated to Add new StoreProcedures for Store 120
-- Version  : 1.1 
-- Revision : 1.1
-- Author   : Michael O'Cain
-- Date	    : 08th August 2011
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Start Applying Permissions
-----------------------------------------------------------------------------------

-- SP ActivityLogEnded
If OBJECT_ID('ActivityLogEnded') is not null
	Begin
	Print 'SP : ActivityLogEnded - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ActivityLogEnded] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ActivityLogEnded - Skipping Permissions (D.N.E)'
	End  

-- SP ActivityLogInsert
If OBJECT_ID('ActivityLogInsert') is not null
	Begin
	Print 'SP : ActivityLogInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ActivityLogInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ActivityLogInsert - Skipping Permissions (D.N.E)'
	End 

-- SP AdjustGetStockShrinkage
If OBJECT_ID('AdjustGetStockShrinkage') is not null
	Begin
	Print 'SP : AdjustGetStockShrinkage - Applying Permissions'
	GRANT EXECUTE ON [dbo].[AdjustGetStockShrinkage] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : AdjustGetStockShrinkage - Skipping Permissions (D.N.E)'
	End 

-- SP AdjustsSelectCodeDetailDateRange
If OBJECT_ID('AdjustsSelectCodeDetailDateRange') is not null
	Begin
	Print 'SP : AdjustsSelectCodeDetailDateRange - Applying Permissions'
	GRANT EXECUTE ON [dbo].[AdjustsSelectCodeDetailDateRange] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : AdjustsSelectCodeDetailDateRange - Skipping Permissions (D.N.E)'
	End 


-- SP AdjustsSelectCodeSummaryDateRange
If OBJECT_ID('AdjustsSelectCodeSummaryDateRange') is not null
	Begin
	Print 'SP : AdjustsSelectCodeSummaryDateRange - Applying Permissions'
	GRANT EXECUTE ON [dbo].[AdjustsSelectCodeSummaryDateRange] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : AdjustsSelectCodeSummaryDateRange - Skipping Permissions (D.N.E)'
	End 

-- SP AdjustsSelectDateRange
If OBJECT_ID('AdjustsSelectDateRange') is not null
	Begin
	Print 'SP : AdjustsSelectDateRange - Applying Permissions'
	GRANT EXECUTE ON [dbo].[AdjustsSelectDateRange] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : AdjustsSelectDateRange - Skipping Permissions (D.N.E)'
	End 

-- SP AdjustsSelectForSkuAndCode
If OBJECT_ID('AdjustsSelectForSkuAndCode') is not null
	Begin
	Print 'SP : AdjustsSelectForSkuAndCode - Applying Permissions'
	GRANT EXECUTE ON [dbo].[AdjustsSelectForSkuAndCode] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : AdjustsSelectForSkuAndCode - Skipping Permissions (D.N.E)'
	End 

-- SP AdjustsSelectWriteOffs
If OBJECT_ID('AdjustsSelectWriteOffs') is not null
	Begin
	Print 'SP : AdjustsSelectWriteOffs - Applying Permissions'
	GRANT EXECUTE ON [dbo].[AdjustsSelectWriteOffs] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : AdjustsSelectWriteOffs - Skipping Permissions (D.N.E)'
	End 

-- SP BankingCashierPerformanceGet
If OBJECT_ID('BankingCashierPerformanceGet') is not null
	Begin
	Print 'SP : BankingCashierPerformanceGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[BankingCashierPerformanceGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : BankingCashierPerformanceGet - Skipping Permissions (D.N.E)'
	End 

-- SP BankingCorrectBankingDate
If OBJECT_ID('BankingCorrectBankingDate') is not null
	Begin
	Print 'SP : BankingCorrectBankingDate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[BankingCorrectBankingDate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : BankingCorrectBankingDate - Skipping Permissions (D.N.E)'
	End 

-- SP BankingDoesOpenPickupBagsExist
If OBJECT_ID('BankingDoesOpenPickupBagsExist') is not null
	Begin
	Print 'SP : BankingDoesOpenPickupBagsExist - Applying Permissions'
	GRANT EXECUTE ON [dbo].[BankingDoesOpenPickupBagsExist] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : BankingDoesOpenPickupBagsExist - Skipping Permissions (D.N.E)'
	End 

-- SP BankingMiscIncomeGet
If OBJECT_ID('BankingMiscIncomeGet') is not null
	Begin
	Print 'SP : BankingMiscIncomeGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[BankingMiscIncomeGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : BankingMiscIncomeGet - Skipping Permissions (D.N.E)'
	End 

-- SP CardReportGet
If OBJECT_ID('CardReportGet') is not null
	Begin
	Print 'SP : CardReportGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[CardReportGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : CardReportGet - Skipping Permissions (D.N.E)'
	End 

-- SP ColleagueDiscountGet
If OBJECT_ID('ColleagueDiscountGet') is not null
	Begin
	Print 'SP : ColleagueDiscountGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ColleagueDiscountGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ColleagueDiscountGet - Skipping Permissions (D.N.E)'
	End 

-- SP ConsignmentGetByNumber
If OBJECT_ID('ConsignmentGetByNumber') is not null
	Begin
	Print 'SP : ConsignmentGetByNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ConsignmentGetByNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ConsignmentGetByNumber - Skipping Permissions (D.N.E)'
	End 

-- SP ConsignmentGetNotReceivedCount
If OBJECT_ID('ConsignmentGetNotReceivedCount') is not null
	Begin
	Print 'SP : ConsignmentGetNotReceivedCount - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ConsignmentGetNotReceivedCount] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ConsignmentGetNotReceivedCount - Skipping Permissions (D.N.E)'
	End 

-- SP ContainerGetByDateRange
If OBJECT_ID('ContainerGetByDateRange') is not null
	Begin
	Print 'SP : ContainerGetByDateRange - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ContainerGetByDateRange] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ContainerGetByDateRange - Skipping Permissions (D.N.E)'
	End 

-- SP ContainerGetLines
If OBJECT_ID('ContainerGetLines') is not null
	Begin
	Print 'SP : ContainerGetLines - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ContainerGetLines] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ContainerGetLines - Skipping Permissions (D.N.E)'
	End 

-- SP CurrencyGet
If OBJECT_ID('CurrencyGet') is not null
	Begin
	Print 'SP : CurrencyGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[CurrencyGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : CurrencyGet - Skipping Permissions (D.N.E)'
	End 

-- SP CurrencyLinesGet
If OBJECT_ID('CurrencyLinesGet') is not null
	Begin
	Print 'SP : CurrencyLinesGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[CurrencyLinesGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : CurrencyLinesGet - Skipping Permissions (D.N.E)'
	End 

-- SP DashActivity
If OBJECT_ID('DashActivity') is not null
	Begin
	Print 'SP : DashActivity - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashActivity] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashActivity - Skipping Permissions (D.N.E)'
	End 

-- SP DashActivity2
If OBJECT_ID('DashActivity2') is not null
	Begin
	Print 'SP : DashActivity2 - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashActivity2] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashActivity2 - Skipping Permissions (D.N.E)'
	End 

-- SP DashBanking
If OBJECT_ID('DashBanking') is not null
	Begin
	Print 'SP : DashBanking - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashBanking] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashBanking - Skipping Permissions (D.N.E)'
	End 

-- SP DashDeliveries
If OBJECT_ID('DashDeliveries') is not null
	Begin
	Print 'SP : DashDeliveries - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashDeliveries] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashDeliveries - Skipping Permissions (D.N.E)'
	End 

-- SP DashDeliveries2
If OBJECT_ID('DashDeliveries2') is not null
	Begin
	Print 'SP : DashDeliveries2 - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashDeliveries2] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashDeliveries2 - Skipping Permissions (D.N.E)'
	End 

-- SP DashDrl
If OBJECT_ID('DashDrl') is not null
	Begin
	Print 'SP : DashDrl - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashDrl] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashDrl - Skipping Permissions (D.N.E)'
	End 

-- SP DashPriceChanges
If OBJECT_ID('DashPriceChanges') is not null
	Begin
	Print 'SP : DashPriceChanges - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashPriceChanges] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashPriceChanges - Skipping Permissions (D.N.E)'
	End 

-- SP DashPriceChanges2
If OBJECT_ID('DashPriceChanges2') is not null
	Begin
	Print 'SP : DashPriceChanges2 - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashPriceChanges2] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashPriceChanges2 - Skipping Permissions (D.N.E)'
	End 

-- SP DashPrices
If OBJECT_ID('DashPrices') is not null
	Begin
	Print 'SP : DashPrices - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashPrices] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashPrices - Skipping Permissions (D.N.E)'
	End 

-- SP DashQod
If OBJECT_ID('DashQod') is not null
	Begin
	Print 'SP : DashQod - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashQod] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashQod - Skipping Permissions (D.N.E)'
	End 

-- SP DashQodDate
If OBJECT_ID('DashQodDate') is not null
	Begin
	Print 'SP : DashQodDate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashQodDate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashQodDate - Skipping Permissions (D.N.E)'
	End 

-- SP DashSales
If OBJECT_ID('DashSales') is not null
	Begin
	Print 'SP : DashSales - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashSales] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashSales - Skipping Permissions (D.N.E)'
	End 

-- SP DashSales2
If OBJECT_ID('DashSales2') is not null
	Begin
	Print 'SP : DashSales2 - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashSales2] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashSales2 - Skipping Permissions (D.N.E)'
	End 

-- SP DashShrinkage
If OBJECT_ID('DashShrinkage') is not null
	Begin
	Print 'SP : DashShrinkage - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashShrinkage] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashShrinkage - Skipping Permissions (D.N.E)'
	End 

-- SP DashStock
If OBJECT_ID('DashStock') is not null
	Begin
	Print 'SP : DashStock - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashStock] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashStock - Skipping Permissions (D.N.E)'
	End 

-- SP DashStock2
If OBJECT_ID('DashStock2') is not null
	Begin
	Print 'SP : DashStock2 - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DashStock2] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DashStock2 - Skipping Permissions (D.N.E)'
	End 

-- SP DeleteUnappliedBySkun
If OBJECT_ID('DeleteUnappliedBySkun') is not null
	Begin
	Print 'SP : DeleteUnappliedBySkun - Applying Permissions'
	GRANT EXECUTE ON [dbo].[DeleteUnappliedBySkun] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : DeleteUnappliedBySkun - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryGeneral
If OBJECT_ID('EnquiryGeneral') is not null
	Begin
	Print 'SP : EnquiryGeneral - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryGeneral] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryGeneral - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryHierarchy
If OBJECT_ID('EnquiryHierarchy') is not null
	Begin
	Print 'SP : EnquiryHierarchy - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryHierarchy] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryHierarchy - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryLabel
If OBJECT_ID('EnquiryLabel') is not null
	Begin
	Print 'SP : EnquiryLabel - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryLabel] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryLabel - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryOrder
If OBJECT_ID('EnquiryOrder') is not null
	Begin
	Print 'SP : EnquiryOrder - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryOrder] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryOrder - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryOutstandingOrder
If OBJECT_ID('EnquiryOutstandingOrder') is not null
	Begin
	Print 'SP : EnquiryOutstandingOrder - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryOutstandingOrder] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryOutstandingOrder - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryPricing
If OBJECT_ID('EnquiryPricing') is not null
	Begin
	Print 'SP : EnquiryPricing - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryPricing] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryPricing - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryReceipts
If OBJECT_ID('EnquiryReceipts') is not null
	Begin
	Print 'SP : EnquiryReceipts - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryReceipts] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryReceipts - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryRelatedItemBulk
If OBJECT_ID('EnquiryRelatedItemBulk') is not null
	Begin
	Print 'SP : EnquiryRelatedItemBulk - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryRelatedItemBulk] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryRelatedItemBulk - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryRelatedItemSingle
If OBJECT_ID('EnquiryEnquiryRelatedItemSingle') is not null
	Begin
	Print 'SP : EnquiryEnquiryRelatedItemSingle - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryEnquiryRelatedItemSingle] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryEnquiryRelatedItemSingle - Skipping Permissions (D.N.E)'
	End 

-- SP EnquirySales
If OBJECT_ID('EnquirySales') is not null
	Begin
	Print 'SP : EnquirySales - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquirySales] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquirySales - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryStock
If OBJECT_ID('EnquiryStock') is not null
	Begin
	Print 'SP : EnquiryStock - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryStock] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryStock - Skipping Permissions (D.N.E)'
	End 

-- SP EnquiryStockPim
If OBJECT_ID('EnquiryStockPim') is not null
	Begin
	Print 'SP : EnquiryStockPim - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquiryStockPim] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquiryStockPim - Skipping Permissions (D.N.E)'
	End 

-- SP EnquirySupplier
If OBJECT_ID('EnquirySupplier') is not null
	Begin
	Print 'SP : EnquirySupplier - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EnquirySupplier] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EnquirySupplier - Skipping Permissions (D.N.E)'
	End 

-- SP EventBySku
If OBJECT_ID('EventBySku') is not null
	Begin
	Print 'SP : EventBySku - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EventBySku] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EventBySku - Skipping Permissions (D.N.E)'
	End 

-- SP EventBySkun
If OBJECT_ID('EventBySkun') is not null
	Begin
	Print 'SP : EventBySkun - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EventBySkun] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EventBySkun - Skipping Permissions (D.N.E)'
	End 

-- SP EventGetHeaderByEvent
If OBJECT_ID('EventGetHeaderByEvent') is not null
	Begin
	Print 'SP : EventGetHeaderByEvent - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EventGetHeaderByEvent] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EventGetHeaderByEvent - Skipping Permissions (D.N.E)'
	End 

-- SP EventGetHeaderByStock
If OBJECT_ID('EventGetHeaderByStock') is not null
	Begin
	Print 'SP : EventGetHeaderByStock - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EventGetHeaderByStock] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EventGetHeaderByStock - Skipping Permissions (D.N.E)'
	End 

-- SP EventMasterGet
If OBJECT_ID('EventMasterGet') is not null
	Begin
	Print 'SP : EventMasterGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[EventMasterGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : EventMasterGet - Skipping Permissions (D.N.E)'
	End 

-- SP FlashTotalSaleTypePersist
If OBJECT_ID('FlashTotalSaleTypePersist') is not null
	Begin
	Print 'SP : FlashTotalSaleTypePersist - Applying Permissions'
	GRANT EXECUTE ON [dbo].[FlashTotalSaleTypePersist] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : FlashTotalSaleTypePersist - Skipping Permissions (D.N.E)'
	End 

-- SP FlashTotalTenderPersist
If OBJECT_ID('FlashTotalTenderPersist') is not null
	Begin
	Print 'SP : FlashTotalTenderPersist - Applying Permissions'
	GRANT EXECUTE ON [dbo].[FlashTotalTenderPersist] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : FlashTotalTenderPersist - Skipping Permissions (D.N.E)'
	End 

-- SP GapWalkClear
If OBJECT_ID('GapWalkClear') is not null
	Begin
	Print 'SP : GapWalkClear - Applying Permissions'
	GRANT EXECUTE ON [dbo].[GapWalkClear] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GapWalkClear - Skipping Permissions (D.N.E)'
	End 

-- SP GapWalkGetItems
If OBJECT_ID('GapWalkGetItems') is not null
	Begin
	Print 'SP : GapWalkGetItems - Applying Permissions'
	GRANT EXECUTE ON [dbo].[GapWalkGetItems] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GapWalkGetItems - Skipping Permissions (D.N.E)'
	End 

-- SP GetAllStores
If OBJECT_ID('GetAllStores') is not null
	Begin
	Print 'SP : GetAllStores - Applying Permissions'
	GRANT EXECUTE ON [dbo].[GetAllStores] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GetAllStores - Skipping Permissions (D.N.E)'
	End 

-- SP GetCycleNumber
If OBJECT_ID('GetCycleNumber') is not null
	Begin
	Print 'SP : GetCycleNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[GetCycleNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GetCycleNumber - Skipping Permissions (D.N.E)'
	End 

-- SP GetEventMaster
If OBJECT_ID('GetEventMaster') is not null
	Begin
	Print 'SP : GetEventMaster - Applying Permissions'
	GRANT EXECUTE ON [dbo].[GetEventMaster] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GetEventMaster - Skipping Permissions (D.N.E)'
	End 

-- SP GetPeriodIdForToday
If OBJECT_ID('GetPeriodIdForToday') is not null
	Begin
	Print 'SP : GetPeriodIdForToday - Applying Permissions'
	GRANT EXECUTE ON [dbo].[GetPeriodIdForToday] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GetPeriodIdForToday - Skipping Permissions (D.N.E)'
	End 

-- SP GetStockPrice
If OBJECT_ID('GetStockPrice') is not null
	Begin
	Print 'SP : GetStockPrice - Applying Permissions'
	GRANT EXECUTE ON [dbo].[GetStockPrice] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GetStockPrice - Skipping Permissions (D.N.E)'
	End 

-- SP GiftVoucherGet
If OBJECT_ID('GiftVoucherGet') is not null
	Begin
	Print 'SP : GiftVoucherGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[GiftVoucherGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GiftVoucherGet - Skipping Permissions (D.N.E)'
	End 

-- SP HierarchyCategoryGetNumberNames
If OBJECT_ID('HierarchyCategoryGetNumberNames') is not null
	Begin
	Print 'SP : HierarchyCategoryGetNumberNames - Applying Permissions'
	GRANT EXECUTE ON [dbo].[HierarchyCategoryGetNumberNames] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : HierarchyCategoryGetNumberNames - Skipping Permissions (D.N.E)'
	End 

-- SP HierarchyGetAll
If OBJECT_ID('HierarchyGetAll') is not null
	Begin
	Print 'SP : HierarchyGetAll - Applying Permissions'
	GRANT EXECUTE ON [dbo].[HierarchyGetAll] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : HierarchyGetAll - Skipping Permissions (D.N.E)'
	End 

-- SP HierarchyMasterGetAll
If OBJECT_ID('HierarchyMasterGetAll') is not null
	Begin
	Print 'SP : HierarchyMasterGetAll - Applying Permissions'
	GRANT EXECUTE ON [dbo].[HierarchyMasterGetAll] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : HierarchyMasterGetAll - Skipping Permissions (D.N.E)'
	End 

-- SP IBTInsert
If OBJECT_ID('IBTInsert') is not null
	Begin
	Print 'SP : IBTInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[IBTInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : IBTInsert - Skipping Permissions (D.N.E)'
	End 

-- SP IBTInsertLine
If OBJECT_ID('IBTInsertLine') is not null
	Begin
	Print 'SP : IBTInsertLine - Applying Permissions'
	GRANT EXECUTE ON [dbo].[IBTInsertLine] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : IBTInsertLine - Skipping Permissions (D.N.E)'
	End 

-- SP IBTNumberGet
If OBJECT_ID('IBTNumberGet') is not null
	Begin
	Print 'SP : IBTNumberGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[IBTNumberGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : IBTNumberGet - Skipping Permissions (D.N.E)'
	End 

-- SP InternalUserGet
If OBJECT_ID('InternalUserGet') is not null
	Begin
	Print 'SP : InternalUserGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[InternalUserGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : InternalUserGet - Skipping Permissions (D.N.E)'
	End 

-- SP IssueGetStatusReport
If OBJECT_ID('IssueGetStatusReport') is not null
	Begin
	Print 'SP : IssueGetStatusReport - Applying Permissions'
	GRANT EXECUTE ON [dbo].[IssueGetStatusReport] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : IssueGetStatusReport - Skipping Permissions (D.N.E)'
	End 

-- SP LocalStoreDetailsGet
If OBJECT_ID('LocalStoreDetailsGet') is not null
	Begin
	Print 'SP : LocalStoreDetailsGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[LocalStoreDetailsGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : LocalStoreDetailsGet - Skipping Permissions (D.N.E)'
	End 

-- SP MarkdownsOnHandGet
If OBJECT_ID('MarkdownsOnHandGet') is not null
	Begin
	Print 'SP : MarkdownsOnHandGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[MarkdownsOnHandGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : MarkdownsOnHandGet - Skipping Permissions (D.N.E)'
	End 

-- SP MarkdownsSelectActive
If OBJECT_ID('MarkdownsSelectActive') is not null
	Begin
	Print 'SP : MarkdownsSelectActive - Applying Permissions'
	GRANT EXECUTE ON [dbo].[MarkdownsSelectActive] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : MarkdownsSelectActive - Skipping Permissions (D.N.E)'
	End 

-- SP MarkdownsSelectForSku
If OBJECT_ID('MarkdownsSelectForSku') is not null
	Begin
	Print 'SP : MarkdownsSelectForSku - Applying Permissions'
	GRANT EXECUTE ON [dbo].[MarkdownsSelectForSku] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : MarkdownsSelectForSku - Skipping Permissions (D.N.E)'
	End 

-- SP MarkdownsSelectSold
If OBJECT_ID('MarkdownsSelectSold') is not null
	Begin
	Print 'SP : MarkdownsSelectSold - Applying Permissions'
	GRANT EXECUTE ON [dbo].[MarkdownsSelectSold] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : MarkdownsSelectSold - Skipping Permissions (D.N.E)'
	End 

-- SP MarkdownsSelectWrittenOff
If OBJECT_ID('MarkdownsSelectWrittenOff') is not null
	Begin
	Print 'SP : MarkdownsSelectWrittenOff - Applying Permissions'
	GRANT EXECUTE ON [dbo].[MarkdownsSelectWrittenOff] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : MarkdownsSelectWrittenOff - Skipping Permissions (D.N.E)'
	End 

-- SP MenuGet
If OBJECT_ID('MenuGet') is not null
	Begin
	Print 'SP : MenuGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[MenuGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : MenuGet - Skipping Permissions (D.N.E)'
	End 

-- SP MenuProfileGet
If OBJECT_ID('MenuProfileGet') is not null
	Begin
	Print 'SP : MenuProfileGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[MenuProfileGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : MenuProfileGet - Skipping Permissions (D.N.E)'
	End 

-- SP MissingTransactions
If OBJECT_ID('MissingTransactions') is not null
	Begin
	Print 'SP : MissingTransactions - Applying Permissions'
	GRANT EXECUTE ON [dbo].[MissingTransactions] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : MissingTransactions - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingActiveCashierList
If OBJECT_ID('NewBankingActiveCashierList') is not null
	Begin
	Print 'SP : NewBankingActiveCashierList - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingActiveCashierList] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingActiveCashierList - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingActiveCashierSystemSale
If OBJECT_ID('NewBankingActiveCashierSystemSale') is not null
	Begin
	Print 'SP : NewBankingActiveCashierSystemSale - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingActiveCashierSystemSale] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingActiveCashierSystemSale - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingActiveCurrencyID
If OBJECT_ID('NewBankingActiveCurrencyID') is not null
	Begin
	Print 'SP : NewBankingActiveCurrencyID - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingActiveCurrencyID] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingActiveCurrencyID - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingBagCollection
If OBJECT_ID('NewBankingBagCollection') is not null
	Begin
	Print 'SP : NewBankingBagCollection - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingBagCollection] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingBagCollection - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingBagCollectionUpdate
If OBJECT_ID('NewBankingBagCollectionUpdate') is not null
	Begin
	Print 'SP : NewBankingBagCollectionUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingBagCollectionUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingBagCollectionUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingBankingValidation
If OBJECT_ID('NewBankingBankingValidation') is not null
	Begin
	Print 'SP : NewBankingBankingValidation - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingBankingValidation] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingBankingValidation - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingCashDropAndUnFloatedPickupsDisplay
If OBJECT_ID('NewBankingCashDropAndUnFloatedPickupsDisplay') is not null
	Begin
	Print 'SP : NewBankingCashDropAndUnFloatedPickupsDisplay - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingCashDropAndUnFloatedPickupsDisplay] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingCashDropAndUnFloatedPickupsDisplay - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingCashier
If OBJECT_ID('NewBankingCashier') is not null
	Begin
	Print 'SP : NewBankingCashier - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingCashier] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingCashier - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingCashierFloatVariance
If OBJECT_ID('NewBankingCashierFloatVariance') is not null
	Begin
	Print 'SP : NewBankingCashierFloatVariance - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingCashierFloatVariance] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingCashierFloatVariance - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingCurrencySymbol
If OBJECT_ID('NewBankingCurrencySymbol') is not null
	Begin
	Print 'SP : NewBankingCurrencySymbol - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingCurrencySymbol] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingCurrencySymbol - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingDates
If OBJECT_ID('NewBankingDates') is not null
	Begin
	Print 'SP : NewBankingDates - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingDates] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingDates - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingDatesXXX
If OBJECT_ID('NewBankingDatesXXX') is not null
	Begin
	Print 'SP : NewBankingDatesXXX - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingDatesXXX] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingDatesXXX - Skipping Permissions (D.N.E)'
	End

-- SP NewBankingFloatChecked
If OBJECT_ID('NewBankingFloatChecked') is not null
	Begin
	Print 'SP : NewBankingFloatChecked - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingFloatChecked] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingFloatChecked - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingFloatDenomination
If OBJECT_ID('NewBankingFloatDenomination') is not null
	Begin
	Print 'SP : NewBankingFloatDenomination - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingFloatDenomination] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingFloatDenomination - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingFloatDisplayAndCheck
If OBJECT_ID('NewBankingFloatDisplayAndCheck') is not null
	Begin
	Print 'SP : NewBankingFloatDisplayAndCheck - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingFloatDisplayAndCheck] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingFloatDisplayAndCheck - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingFloatedCashierDisplay
If OBJECT_ID('NewBankingFloatedCashierDisplay') is not null
	Begin
	Print 'SP : NewBankingFloatedCashierDisplay - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingFloatedCashierDisplay] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingFloatedCashierDisplay - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingFloatedPickupList
If OBJECT_ID('NewBankingFloatedPickupList') is not null
	Begin
	Print 'SP : NewBankingFloatedPickupList - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingFloatedPickupList] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingFloatedPickupList - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingFloatList
If OBJECT_ID('NewBankingFloatList') is not null
	Begin
	Print 'SP : NewBankingFloatList - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingFloatList] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingFloatList - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingFloatPrepareUnAssign
If OBJECT_ID('NewBankingFloatPrepareUnAssign') is not null
	Begin
	Print 'SP : NewBankingFloatPrepareUnAssign - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingFloatPrepareUnAssign] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingFloatPrepareUnAssign - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingFloatReAssign
If OBJECT_ID('NewBankingFloatReAssign') is not null
	Begin
	Print 'SP : NewBankingFloatReAssign - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingFloatReAssign] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingFloatReAssign - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingNonBankedDates
If OBJECT_ID('NewBankingNonBankedDates') is not null
	Begin
	Print 'SP : NewBankingNonBankedDates - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingNonBankedDates] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingNonBankedDates - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingPickupCashDrop
If OBJECT_ID('NewBankingPickupCashDrop') is not null
	Begin
	Print 'SP : NewBankingPickupCashDrop - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingPickupCashDrop] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingPickupCashDrop - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingPickupEndOfDay
If OBJECT_ID('NewBankingPickupEndOfDay') is not null
	Begin
	Print 'SP : NewBankingPickupEndOfDay - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingPickupEndOfDay] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingPickupEndOfDay - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingPickupSale
If OBJECT_ID('NewBankingPickupSale') is not null
	Begin
	Print 'SP : NewBankingPickupSale - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingPickupSale] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingPickupSale - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingSafeAllTenders
If OBJECT_ID('NewBankingSafeAllTenders') is not null
	Begin
	Print 'SP : NewBankingSafeAllTenders - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingSafeAllTenders] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingSafeAllTenders - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingSafeCashTender
If OBJECT_ID('NewBankingSafeCashTender') is not null
	Begin
	Print 'SP : NewBankingSafeCashTender - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingSafeCashTender] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingSafeCashTender - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingSafeChecked
If OBJECT_ID('NewBankingSafeChecked') is not null
	Begin
	Print 'SP : NewBankingSafeChecked - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingSafeChecked] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingSafeChecked - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingSafeExist
If OBJECT_ID('NewBankingSafeExist') is not null
	Begin
	Print 'SP : NewBankingSafeExist - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingSafeExist] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingSafeExist - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingSafeMaintenanceBagsHeld
If OBJECT_ID('NewBankingSafeMaintenanceBagsHeld') is not null
	Begin
	Print 'SP : NewBankingSafeMaintenanceBagsHeld - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingSafeMaintenanceBagsHeld] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingSafeMaintenanceBagsHeld - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingSafeMaintenanceValidation
If OBJECT_ID('NewBankingSafeMaintenanceValidation') is not null
	Begin
	Print 'SP : NewBankingSafeMaintenanceValidation - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingSafeMaintenanceValidation] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingSafeMaintenanceValidation - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingSecondSupervisorCheck
If OBJECT_ID('NewBankingSecondSupervisorCheck') is not null
	Begin
	Print 'SP : NewBankingSecondSupervisorCheck - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingSecondSupervisorCheck] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingSecondSupervisorCheck - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingSecondUserCheck
If OBJECT_ID('NewBankingSecondUserCheck') is not null
	Begin
	Print 'SP : NewBankingSecondUserCheck - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingSecondUserCheck] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingSecondUserCheck - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingStore
If OBJECT_ID('NewBankingStore') is not null
	Begin
	Print 'SP : NewBankingStore - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingStore] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingStore - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingTenderDenominationList
If OBJECT_ID('NewBankingTenderDenominationList') is not null
	Begin
	Print 'SP : NewBankingTenderDenominationList - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingTenderDenominationList] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingTenderDenominationList - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingUnFloatedCashier
If OBJECT_ID('NewBankingUnFloatedCashier') is not null
	Begin
	Print 'SP : NewBankingUnFloatedCashier - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingUnFloatedCashier] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingUnFloatedCashier - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingUnFloatedPickupList
If OBJECT_ID('NewBankingUnFloatedPickupList') is not null
	Begin
	Print 'SP : NewBankingUnFloatedPickupList - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingUnFloatedPickupList] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingUnFloatedPickupList - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingUniqueSealNumber
If OBJECT_ID('NewBankingUniqueSealNumber') is not null
	Begin
	Print 'SP : NewBankingUniqueSealNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingUniqueSealNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingUniqueSealNumber - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingUniqueSlipNumber
If OBJECT_ID('NewBankingUniqueSlipNumber') is not null
	Begin
	Print 'SP : NewBankingUniqueSlipNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingUniqueSlipNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingUniqueSlipNumber - Skipping Permissions (D.N.E)'
	End 

-- SP ParameterGetDecimalValueByID
If OBJECT_ID('ParameterGetDecimalValueByID') is not null
	Begin
	Print 'SP : ParameterGetDecimalValueByID - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ParameterGetDecimalValueByID] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ParameterGetDecimalValueByID - Skipping Permissions (D.N.E)'
	End 

-- SP ParametersGetBooleanValueByID
If OBJECT_ID('ParametersGetBooleanValueByID') is not null
	Begin
	Print 'SP : ParametersGetBooleanValueByID - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ParametersGetBooleanValueByID] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ParametersGetBooleanValueByID - Skipping Permissions (D.N.E)'
	End 

-- SP ParametersGetIntegerValueByID
If OBJECT_ID('ParametersGetIntegerValueByID') is not null
	Begin
	Print 'SP : ParametersGetIntegerValueByID - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ParametersGetIntegerValueByID] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ParametersGetIntegerValueByID - Skipping Permissions (D.N.E)'
	End 

-- SP PicGetStockCountSample
If OBJECT_ID('PicGetStockCountSample') is not null
	Begin
	Print 'SP : PicGetStockCountSample - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PicGetStockCountSample] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PicGetStockCountSample - Skipping Permissions (D.N.E)'
	End 

-- SP PicVarianceReport
If OBJECT_ID('PicVarianceReport') is not null
	Begin
	Print 'SP : PicVarianceReport - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PicVarianceReport] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PicVarianceReport - Skipping Permissions (D.N.E)'
	End 

-- SP PicWorksheetCount
If OBJECT_ID('PicWorksheetCount') is not null
	Begin
	Print 'SP : PicWorksheetCount - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PicWorksheetCount] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PicWorksheetCount - Skipping Permissions (D.N.E)'
	End 

-- SP PicWorksheetCycle
If OBJECT_ID('PicWorksheetCycle') is not null
	Begin
	Print 'SP : PicWorksheetCycle - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PicWorksheetCycle] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PicWorksheetCycle - Skipping Permissions (D.N.E)'
	End 

-- SP PicWorksheetHierarchy
If OBJECT_ID('PicWorksheetHierarchy') is not null
	Begin
	Print 'SP : PicWorksheetHierarchy - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PicWorksheetHierarchy] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PicWorksheetHierarchy - Skipping Permissions (D.N.E)'
	End 

-- SP PicWorksheetSupplier
If OBJECT_ID('PicWorksheetSupplier') is not null
	Begin
	Print 'SP : PicWorksheetSupplier - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PicWorksheetSupplier] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PicWorksheetSupplier - Skipping Permissions (D.N.E)'
	End 

-- SP PoConsign
If OBJECT_ID('PoConsign') is not null
	Begin
	Print 'SP : PoConsign - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoConsign] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoConsign - Skipping Permissions (D.N.E)'
	End 

-- SP PoGetConsigned
If OBJECT_ID('PoGetConsigned') is not null
	Begin
	Print 'SP : PoGetConsigned - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoGetConsigned] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoGetConsigned - Skipping Permissions (D.N.E)'
	End 

-- SP PoGetHeaderAndLines
If OBJECT_ID('PoGetHeaderAndLines') is not null
	Begin
	Print 'SP : PoGetHeaderAndLines - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoGetHeaderAndLines] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoGetHeaderAndLines - Skipping Permissions (D.N.E)'
	End 

-- SP PoGetHistoryByStock
If OBJECT_ID('PoGetHistoryByStock') is not null
	Begin
	Print 'SP : PoGetHistoryByStock - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoGetHistoryByStock] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoGetHistoryByStock - Skipping Permissions (D.N.E)'
	End 

-- SP PoGetLines
If OBJECT_ID('PoGetLines') is not null
	Begin
	Print 'SP : PoGetLines - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoGetLines] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoGetLines - Skipping Permissions (D.N.E)'
	End 

-- SP PoGetNotConsigned
If OBJECT_ID('PoGetNotConsigned') is not null
	Begin
	Print 'SP : PoGetNotConsigned - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoGetNotConsigned] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoGetNotConsigned - Skipping Permissions (D.N.E)'
	End 

-- SP PoGetOutstanding
If OBJECT_ID('PoGetOutstanding') is not null
	Begin
	Print 'SP : PoGetOutstanding - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoGetOutstanding] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoGetOutstanding - Skipping Permissions (D.N.E)'
	End 

-- SP PoInsertLine
If OBJECT_ID('PoInsertLine') is not null
	Begin
	Print 'SP : PoInsertLine - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoInsertLine] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoInsertLine - Skipping Permissions (D.N.E)'
	End 

-- SP PoInsertNewOrder
If OBJECT_ID('PoInsertNewOrder') is not null
	Begin
	Print 'SP : PoInsertNewOrder - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoInsertNewOrder] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoInsertNewOrder - Skipping Permissions (D.N.E)'
	End 

-- SP PoUpdate
If OBJECT_ID('PoUpdate') is not null
	Begin
	Print 'SP : PoUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PoUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PoUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP PriceChangesGetByStock
If OBJECT_ID('PriceChangesGetByStock') is not null
	Begin
	Print 'SP : PriceChangesGetByStock - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PriceChangesGetByStock] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PriceChangesGetByStock - Skipping Permissions (D.N.E)'
	End 

-- SP PriceChangesLabelsRequiredQuantity
If OBJECT_ID('PriceChangesLabelsRequiredQuantity') is not null
	Begin
	Print 'SP : PriceChangesLabelsRequiredQuantity - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PriceChangesLabelsRequiredQuantity] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PriceChangesLabelsRequiredQuantity - Skipping Permissions (D.N.E)'
	End 

-- SP PriceChangesOverdueCount
If OBJECT_ID('PriceChangesOverdueCount') is not null
	Begin
	Print 'SP : PriceChangesOverdueCount - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PriceChangesOverdueCount] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PriceChangesOverdueCount - Skipping Permissions (D.N.E)'
	End 

-- SP PriceChangesWaitingCount
If OBJECT_ID('PriceChangesWaitingCount') is not null
	Begin
	Print 'SP : PriceChangesWaitingCount - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PriceChangesWaitingCount] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PriceChangesWaitingCount - Skipping Permissions (D.N.E)'
	End 

-- SP PriceViolationsGet
If OBJECT_ID('PriceViolationsGet') is not null
	Begin
	Print 'SP : PriceViolationsGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PriceViolationsGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PriceViolationsGet - Skipping Permissions (D.N.E)'
	End 

-- SP QODDeliveryIncome
If OBJECT_ID('QODDeliveryIncome') is not null
	Begin
	Print 'SP : QODDeliveryIncome - Applying Permissions'
	GRANT EXECUTE ON [dbo].[QODDeliveryIncome] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : QODDeliveryIncome - Skipping Permissions (D.N.E)'
	End 

-- SP QODDeliveryIncomeSummary
If OBJECT_ID('QODDeliveryIncomeSummary') is not null
	Begin
	Print 'SP : QODDeliveryIncomeSummary - Applying Permissions'
	GRANT EXECUTE ON [dbo].[QODDeliveryIncomeSummary] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : QODDeliveryIncomeSummary - Skipping Permissions (D.N.E)'
	End 

-- SP QODDespatchedOrders
If OBJECT_ID('QODDespatchedOrders') is not null
	Begin
	Print 'SP : QODDespatchedOrders - Applying Permissions'
	GRANT EXECUTE ON [dbo].[QODDespatchedOrders] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : QODDespatchedOrders - Skipping Permissions (D.N.E)'
	End 

-- SP QODGetNonZeroStockOrOutstandingIbt
If OBJECT_ID('QODGetNonZeroStockOrOutstandingIbt') is not null
	Begin
	Print 'SP : QODGetNonZeroStockOrOutstandingIbt - Applying Permissions'
	GRANT EXECUTE ON [dbo].[QODGetNonZeroStockOrOutstandingIbt] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : QODGetNonZeroStockOrOutstandingIbt - Skipping Permissions (D.N.E)'
	End 

-- SP QODOrderDeliveryListing
If OBJECT_ID('QODOrderDeliveryListing') is not null
	Begin
	Print 'SP : QODOrderDeliveryListing - Applying Permissions'
	GRANT EXECUTE ON [dbo].[QODOrderDeliveryListing] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : QODOrderDeliveryListing - Skipping Permissions (D.N.E)'
	End 

-- SP QODOrdersAwaitingDespatch
If OBJECT_ID('QODOrdersAwaitingDespatch') is not null
	Begin
	Print 'SP : QODOrdersAwaitingDespatch - Applying Permissions'
	GRANT EXECUTE ON [dbo].[QODOrdersAwaitingDespatch] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : QODOrdersAwaitingDespatch - Skipping Permissions (D.N.E)'
	End 

-- SP QODQuoteConversions
If OBJECT_ID('QODQuoteConversions') is not null
	Begin
	Print 'SP : QODQuoteConversions - Applying Permissions'
	GRANT EXECUTE ON [dbo].[QODQuoteConversions] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : QODQuoteConversions - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptGetLines
If OBJECT_ID('ReceiptGetLines') is not null
	Begin
	Print 'SP : ReceiptGetLines - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptGetLines] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptGetLines - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptGetMaintainableOrders
If OBJECT_ID('ReceiptGetMaintainableOrders') is not null
	Begin
	Print 'SP : ReceiptGetMaintainableOrders - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptGetMaintainableOrders] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptGetMaintainableOrders - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptGetOrderVariances
If OBJECT_ID('ReceiptGetOrderVariances') is not null
	Begin
	Print 'SP : ReceiptGetOrderVariances - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptGetOrderVariances] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptGetOrderVariances - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptGetOverVarianceTypes
If OBJECT_ID('ReceiptGetOverVarianceTypes') is not null
	Begin
	Print 'SP : ReceiptGetOverVarianceTypes - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptGetOverVarianceTypes] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptGetOverVarianceTypes - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptGetReport
If OBJECT_ID('ReceiptGetReport') is not null
	Begin
	Print 'SP : ReceiptGetReport - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptGetReport] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptGetReport - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptIbtInsert
If OBJECT_ID('ReceiptIbtInsert') is not null
	Begin
	Print 'SP : ReceiptIbtInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptIbtInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptIbtInsert - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptIbtInsertLine
If OBJECT_ID('ReceiptIbtInsertLine') is not null
	Begin
	Print 'SP : ReceiptIbtInsertLine - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptIbtInsertLine] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptIbtInsertLine - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptIbtLineInsert
If OBJECT_ID('ReceiptIbtLineInsert') is not null
	Begin
	Print 'SP : ReceiptIbtLineInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptIbtLineInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptIbtLineInsert - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptIbtLineUpdate
If OBJECT_ID('ReceiptIbtLineUpdate') is not null
	Begin
	Print 'SP : ReceiptIbtLineUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptIbtLineUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptIbtLineUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptOrderInsert
If OBJECT_ID('ReceiptOrderInsert') is not null
	Begin
	Print 'SP : ReceiptOrderInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptOrderInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptOrderInsert - Skipping Permissions (D.N.E)'
	End 

-- SP ReceiptOrderUpdate
If OBJECT_ID('ReceiptOrderUpdate') is not null
	Begin
	Print 'SP : ReceiptOrderUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReceiptOrderUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReceiptOrderUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReport
If OBJECT_ID('ReportGetReport') is not null
	Begin
	Print 'SP : ReportGetReport - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReport] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReport - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReportColumns
If OBJECT_ID('ReportGetReportColumns') is not null
	Begin
	Print 'SP : ReportGetReportColumns - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReportColumns] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReportColumns - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReportGrouping
If OBJECT_ID('ReportGetReportGrouping') is not null
	Begin
	Print 'SP : ReportGetReportGrouping - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReportGrouping] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReportGrouping - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReportHyperlinks
If OBJECT_ID('ReportGetReportHyperlinks') is not null
	Begin
	Print 'SP : ReportGetReportHyperlinks - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReportHyperlinks] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReportHyperlinks - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReportParameters
If OBJECT_ID('ReportGetReportParameters') is not null
	Begin
	Print 'SP : ReportGetReportParameters - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReportParameters] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReportParameters - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReportRelations
If OBJECT_ID('ReportGetReportRelations') is not null
	Begin
	Print 'SP : ReportGetReportRelations - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReportRelations] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReportRelations - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReports
If OBJECT_ID('ReportGetReports') is not null
	Begin
	Print 'SP : ReportGetReports - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReports] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReports - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReportSummaries
If OBJECT_ID('ReportGetReportSummaries') is not null
	Begin
	Print 'SP : ReportGetReportSummaries - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReportSummaries] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReportSummaries - Skipping Permissions (D.N.E)'
	End 

-- SP ReportGetReportTables
If OBJECT_ID('ReportGetReportTables') is not null
	Begin
	Print 'SP : ReportGetReportTables - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReportGetReportTables] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReportGetReportTables - Skipping Permissions (D.N.E)'
	End 

-- SP ReturnGetLines
If OBJECT_ID('ReturnGetLines') is not null
	Begin
	Print 'SP : ReturnGetLines - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReturnGetLines] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReturnGetLines - Skipping Permissions (D.N.E)'
	End 

-- SP ReturnGetNotReleased
If OBJECT_ID('ReturnGetNotReleased') is not null
	Begin
	Print 'SP : ReturnGetNotReleased - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReturnGetNotReleased] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReturnGetNotReleased - Skipping Permissions (D.N.E)'
	End 

-- SP ReturnGetNotReleasedOld
If OBJECT_ID('ReturnGetNotReleasedOld') is not null
	Begin
	Print 'SP : ReturnGetNotReleasedOld - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReturnGetNotReleasedOld] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReturnGetNotReleasedOld - Skipping Permissions (D.N.E)'
	End 

-- SP ReturnsGetOpenReturns
If OBJECT_ID('ReturnsGetOpenReturns') is not null
	Begin
	Print 'SP : ReturnsGetOpenReturns - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReturnsGetOpenReturns] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReturnsGetOpenReturns - Skipping Permissions (D.N.E)'
	End 

-- SP ReturnsSelectDetailsForld
If OBJECT_ID('ReturnsSelectDetailsForld') is not null
	Begin
	Print 'SP : ReturnsSelectDetailsForld - Applying Permissions'
	GRANT EXECUTE ON [dbo].[ReturnsSelectDetailsForld] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : ReturnsSelectDetailsForld - Skipping Permissions (D.N.E)'
	End 

-- SP SaleCustomerGet
If OBJECT_ID('SaleCustomerGet') is not null
	Begin
	Print 'SP : SaleCustomerGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleCustomerGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleCustomerGet - Skipping Permissions (D.N.E)'
	End 

-- SP SaleCustomerInsert
If OBJECT_ID('SaleCustomerInsert') is not null
	Begin
	Print 'SP : SaleCustomerInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleCustomerInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleCustomerInsert - Skipping Permissions (D.N.E)'
	End 

-- SP SaleCustomerUpdate
If OBJECT_ID('SaleCustomerUpdate') is not null
	Begin
	Print 'SP : SaleCustomerUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleCustomerUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleCustomerUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SaleGet
If OBJECT_ID('SaleGet') is not null
	Begin
	Print 'SP : SaleGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleGet - Skipping Permissions (D.N.E)'
	End 

-- SP SaleGetMissingTransactions
If OBJECT_ID('SaleGetMissingTransactions') is not null
	Begin
	Print 'SP : SaleGetMissingTransactions - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleGetMissingTransactions] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleGetMissingTransactions - Skipping Permissions (D.N.E)'
	End 

-- SP SaleInsert
If OBJECT_ID('SaleInsert') is not null
	Begin
	Print 'SP : SaleInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleInsert - Skipping Permissions (D.N.E)'
	End 

-- SP SaleLineGet
If OBJECT_ID('SaleLineGet') is not null
	Begin
	Print 'SP : SaleLineGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleLineGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleLineGet - Skipping Permissions (D.N.E)'
	End 

-- SP SaleLineInsert
If OBJECT_ID('SaleLineInsert') is not null
	Begin
	Print 'SP : SaleLineInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleLineInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleLineInsert - Skipping Permissions (D.N.E)'
	End 

-- SP SaleLineUpdate
If OBJECT_ID('SaleLineUpdate') is not null
	Begin
	Print 'SP : SaleLineUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleLineUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleLineUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderGet
If OBJECT_ID('SaleOrderGet') is not null
	Begin
	Print 'SP : SaleOrderGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderGet - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderInsert
If OBJECT_ID('SaleOrderInsert') is not null
	Begin
	Print 'SP : SaleOrderInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderInsert - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderLineGet
If OBJECT_ID('SaleOrderLineGet') is not null
	Begin
	Print 'SP : SaleOrderLineGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderLineGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderLineGet - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderLineInsert
If OBJECT_ID('SaleOrderLineInsert') is not null
	Begin
	Print 'SP : SaleOrderLineInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderLineInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderLineInsert - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderLineUpdate
If OBJECT_ID('SaleOrderLineUpdate') is not null
	Begin
	Print 'SP : SaleOrderLineUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderLineUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderLineUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderRefundGet
If OBJECT_ID('SaleOrderRefundGet') is not null
	Begin
	Print 'SP : SaleOrderRefundGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderRefundGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderRefundGet - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderRefundInsert
If OBJECT_ID('SaleOrderRefundInsert') is not null
	Begin
	Print 'SP : SaleOrderRefundInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderRefundInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderRefundInsert - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderRefundStatusUpdate
If OBJECT_ID('SaleOrderRefundStatusUpdate') is not null
	Begin
	Print 'SP : SaleOrderRefundStatusUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderRefundStatusUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderRefundStatusUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderRefundUpdate
If OBJECT_ID('SaleOrderRefundUpdate') is not null
	Begin
	Print 'SP : SaleOrderRefundUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderRefundUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderRefundUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderTextDelete
If OBJECT_ID('SaleOrderTextDelete') is not null
	Begin
	Print 'SP : SaleOrderTextDelete - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderTextDelete] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderTextDelete - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderTextGet
If OBJECT_ID('SaleOrderTextGet') is not null
	Begin
	Print 'SP : SaleOrderTextGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderTextGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderTextGet - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderTextInsert
If OBJECT_ID('SaleOrderTextInsert') is not null
	Begin
	Print 'SP : SaleOrderTextInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderTextInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderTextInsert - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderTextUpdate
If OBJECT_ID('SaleOrderTextUpdate') is not null
	Begin
	Print 'SP : SaleOrderTextUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderTextUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderTextUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderUpdate
If OBJECT_ID('SaleOrderUpdate') is not null
	Begin
	Print 'SP : SaleOrderUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SaleOrderUpdateMaintainable
If OBJECT_ID('SaleOrderUpdateMaintainable') is not null
	Begin
	Print 'SP : SaleOrderUpdateMaintainable - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleOrderUpdateMaintainable] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleOrderUpdateMaintainable - Skipping Permissions (D.N.E)'
	End 

-- SP SalePaidGet
If OBJECT_ID('SalePaidGet') is not null
	Begin
	Print 'SP : SalePaidGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SalePaidGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SalePaidGet - Skipping Permissions (D.N.E)'
	End 

-- SP SalePaidInsert
If OBJECT_ID('SalePaidInsert') is not null
	Begin
	Print 'SP : SalePaidInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SalePaidInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SalePaidInsert - Skipping Permissions (D.N.E)'
	End 

-- SP SalesGetCreditCardDetailed
If OBJECT_ID('SalesGetCreditCardDetailed') is not null
	Begin
	Print 'SP : SalesGetCreditCardDetailed - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SalesGetCreditCardDetailed] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SalesGetCreditCardDetailed - Skipping Permissions (D.N.E)'
	End 

-- SP SalesGetCreditCardSummary
If OBJECT_ID('SalesGetCreditCardSummary') is not null
	Begin
	Print 'SP : SalesGetCreditCardSummary - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SalesGetCreditCardSummary] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SalesGetCreditCardSummary - Skipping Permissions (D.N.E)'
	End 

-- SP SalesGetHeaderLines
If OBJECT_ID('SalesGetHeaderLines') is not null
	Begin
	Print 'SP : SalesGetHeaderLines - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SalesGetHeaderLines] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SalesGetHeaderLines - Skipping Permissions (D.N.E)'
	End 

-- SP SalesGetVoucherSales
If OBJECT_ID('SalesGetVoucherSales') is not null
	Begin
	Print 'SP : SalesGetVoucherSales - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SalesGetVoucherSales] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SalesGetVoucherSales - Skipping Permissions (D.N.E)'
	End 

-- SP SalesGetZeroSales
If OBJECT_ID('SalesGetZeroSales') is not null
	Begin
	Print 'SP : SalesGetZeroSales - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SalesGetZeroSales] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SalesGetZeroSales - Skipping Permissions (D.N.E)'
	End 

-- SP SalesTransactionGet
If OBJECT_ID('SalesTransactionGet') is not null
	Begin
	Print 'SP : SalesTransactionGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SalesTransactionGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SalesTransactionGet - Skipping Permissions (D.N.E)'
	End 

-- SP SaleUpdate
If OBJECT_ID('SaleUpdate') is not null
	Begin
	Print 'SP : SaleUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SaleVendaGet
If OBJECT_ID('SaleVendaGet') is not null
	Begin
	Print 'SP : SaleVendaGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SaleVendaGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SaleVendaGet - Skipping Permissions (D.N.E)'
	End 

-- SP SecurityProfileGetAll
If OBJECT_ID('SecurityProfileGetAll') is not null
	Begin
	Print 'SP : SecurityProfileGetAll - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SecurityProfileGetAll] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SecurityProfileGetAll - Skipping Permissions (D.N.E)'
	End 

-- SP SecurityProfileGetByType
If OBJECT_ID('SecurityProfileGetByType') is not null
	Begin
	Print 'SP : SecurityProfileGetByType - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SecurityProfileGetByType] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SecurityProfileGetByType - Skipping Permissions (D.N.E)'
	End 

-- SP StartingFloat
If OBJECT_ID('StartingFloat') is not null
	Begin
	Print 'SP : StartingFloat - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StartingFloat] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StartingFloat - Skipping Permissions (D.N.E)'
	End 

-- SP StockBelowImpactCount
If OBJECT_ID('StockBelowImpactCount') is not null
	Begin
	Print 'SP : StockBelowImpactCount - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockBelowImpactCount] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockBelowImpactCount - Skipping Permissions (D.N.E)'
	End 

-- SP StockDeletedValue
If OBJECT_ID('StockDeletedValue') is not null
	Begin
	Print 'SP : StockDeletedValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockDeletedValue] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockDeletedValue - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetNonStockDeletedOptions
If OBJECT_ID('StockGetNonStockDeletedOptions') is not null
	Begin
	Print 'SP : StockGetNonStockDeletedOptions - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetNonStockDeletedOptions] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetNonStockDeletedOptions - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetNonStockDeletedWithStock
If OBJECT_ID('StockGetNonStockDeletedWithStock') is not null
	Begin
	Print 'SP : StockGetNonStockDeletedWithStock - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetNonStockDeletedWithStock] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetNonStockDeletedWithStock - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetOutOfStockItems
If OBJECT_ID('StockGetOutOfStockItems') is not null
	Begin
	Print 'SP : StockGetOutOfStockItems - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetOutOfStockItems] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetOutOfStockItems - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetPicStockCounts
If OBJECT_ID('StockGetPicStockCounts') is not null
	Begin
	Print 'SP : StockGetPicStockCounts - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetPicStockCounts] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetPicStockCounts - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetProductInformation
If OBJECT_ID('StockGetProductInformation') is not null
	Begin
	Print 'SP : StockGetProductInformation - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetProductInformation] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetProductInformation - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetReturnPolicy
If OBJECT_ID('StockGetReturnPolicy') is not null
	Begin
	Print 'SP : StockGetReturnPolicy - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetReturnPolicy] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetReturnPolicy - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetStock
If OBJECT_ID('StockGetStock') is not null
	Begin
	Print 'SP : StockGetStock - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetStock] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetStock - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetStockEans
If OBJECT_ID('StockGetStockEans') is not null
	Begin
	Print 'SP : StockGetStockEans - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetStockEans] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetStockEans - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetStocks
If OBJECT_ID('StockGetStocks') is not null
	Begin
	Print 'SP : StockGetStocks - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetStocks] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetStocks - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetStocksByNumberEan
If OBJECT_ID('StockGetStocksByNumberEan') is not null
	Begin
	Print 'SP : StockGetStocksByNumberEan - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetStocksByNumberEan] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetStocksByNumberEan - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetStocksBySupplier
If OBJECT_ID('StockGetStocksBySupplier') is not null
	Begin
	Print 'SP : StockGetStocksBySupplier - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetStocksBySupplier] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetStocksBySupplier - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetWeeklySales
If OBJECT_ID('StockGetWeeklySales') is not null
	Begin
	Print 'SP : StockGetWeeklySales - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetWeeklySales] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetWeeklySales - Skipping Permissions (D.N.E)'
	End 

-- SP StockGetZeroStocks
If OBJECT_ID('StockGetZeroStocks') is not null
	Begin
	Print 'SP : StockGetZeroStocks - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetZeroStocks] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetZeroStocks - Skipping Permissions (D.N.E)'
	End 

-- SP StockItemEnquiry
If OBJECT_ID('StockItemEnquiry') is not null
	Begin
	Print 'SP : StockItemEnquiry - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockItemEnquiry] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockItemEnquiry - Skipping Permissions (D.N.E)'
	End 

-- SP StockLogDecodeComment
If OBJECT_ID('StockLogDecodeComment') is not null
	Begin
	Print 'SP : StockLogDecodeComment - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockLogDecodeComment] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockLogDecodeComment - Skipping Permissions (D.N.E)'
	End 

-- SP StockLogGetDetailByStockGroup
If OBJECT_ID('StockLogGetDetailByStockGroup') is not null
	Begin
	Print 'SP : StockLogGetDetailByStockGroup - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockLogGetDetailByStockGroup] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockLogGetDetailByStockGroup - Skipping Permissions (D.N.E)'
	End 

-- SP StockLogGetGroups
If OBJECT_ID('StockLogGetGroups') is not null
	Begin
	Print 'SP : StockLogGetGroups - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockLogGetGroups] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockLogGetGroups - Skipping Permissions (D.N.E)'
	End 

-- SP StockLogGetWeeklySummaryByStock
If OBJECT_ID('StockLogGetWeeklySummaryByStock') is not null
	Begin
	Print 'SP : StockLogGetWeeklySummaryByStock - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockLogGetWeeklySummaryByStock] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockLogGetWeeklySummaryByStock - Skipping Permissions (D.N.E)'
	End 

-- SP StockLogInsertCheck91
If OBJECT_ID('StockLogInsertCheck91') is not null
	Begin
	Print 'SP : StockLogInsertCheck91 - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockLogInsertCheck91] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockLogInsertCheck91 - Skipping Permissions (D.N.E)'
	End 

-- SP StockMarkdownQuantity
If OBJECT_ID('StockMarkdownQuantity') is not null
	Begin
	Print 'SP : StockMarkdownQuantity - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockMarkdownQuantity] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockMarkdownQuantity - Skipping Permissions (D.N.E)'
	End 

-- SP StockMarkdownValue
If OBJECT_ID('StockMarkdownValue') is not null
	Begin
	Print 'SP : StockMarkdownValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockMarkdownValue] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockMarkdownValue - Skipping Permissions (D.N.E)'
	End 

-- SP StockNonStockValue
If OBJECT_ID('StockNonStockValue') is not null
	Begin
	Print 'SP : StockNonStockValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockNonStockValue] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockNonStockValue - Skipping Permissions (D.N.E)'
	End 

-- SP StockOnHandValue
If OBJECT_ID('StockOnHandValue') is not null
	Begin
	Print 'SP : StockOnHandValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockOnHandValue] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockOnHandValue - Skipping Permissions (D.N.E)'
	End 

-- SP StockOpenReturnQuantity
If OBJECT_ID('StockOpenReturnQuantity') is not null
	Begin
	Print 'SP : StockOpenReturnQuantity - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockOpenReturnQuantity] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockOpenReturnQuantity - Skipping Permissions (D.N.E)'
	End 

-- SP StockOpenReturnValue
If OBJECT_ID('StockOpenReturnValue') is not null
	Begin
	Print 'SP : StockOpenReturnValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockOpenReturnValue] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockOpenReturnValue - Skipping Permissions (D.N.E)'
	End 

-- SP StockOutOfStockCount
If OBJECT_ID('StockOutOfStockCount') is not null
	Begin
	Print 'SP : StockOutOfStockCount - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockOutOfStockCount] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockOutOfStockCount - Skipping Permissions (D.N.E)'
	End 

-- SP StockUpdateSoqInit
If OBJECT_ID('StockUpdateSoqInit') is not null
	Begin
	Print 'SP : StockUpdateSoqInit - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockUpdateSoqInit] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockUpdateSoqInit - Skipping Permissions (D.N.E)'
	End 

-- SP StockUpdateSoqInventory
If OBJECT_ID('StockUpdateSoqInventory') is not null
	Begin
	Print 'SP : StockUpdateSoqInventory - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockUpdateSoqInventory] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockUpdateSoqInventory - Skipping Permissions (D.N.E)'
	End 

-- SP StoreGetAll
If OBJECT_ID('StoreGetAll') is not null
	Begin
	Print 'SP : StoreGetAll - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StoreGetAll] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StoreGetAll - Skipping Permissions (D.N.E)'
	End 

-- SP StoreGetDaysOpen
If OBJECT_ID('StoreGetDaysOpen') is not null
	Begin
	Print 'SP : StoreGetDaysOpen - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StoreGetDaysOpen] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StoreGetDaysOpen - Skipping Permissions (D.N.E)'
	End 

-- SP StoreGetStore
If OBJECT_ID('StoreGetStore') is not null
	Begin
	Print 'SP : StoreGetStore - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StoreGetStore] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StoreGetStore - Skipping Permissions (D.N.E)'
	End 

-- SP StoreSaleWeightGetActiveForld
If OBJECT_ID('StoreSaleWeightGetActiveForld') is not null
	Begin
	Print 'SP : StoreSaleWeightGetActiveForld - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StoreSaleWeightGetActiveForld] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StoreSaleWeightGetActiveForld - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierDetailOrderingSelectAll
If OBJECT_ID('SupplierDetailOrderingSelectAll') is not null
	Begin
	Print 'SP : SupplierDetailOrderingSelectAll - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierDetailOrderingSelectAll] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierDetailOrderingSelectAll - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierGetAll
If OBJECT_ID('SupplierGetAll') is not null
	Begin
	Print 'SP : SupplierGetAll - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierGetAll] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierGetAll - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierGetAllForOrder
If OBJECT_ID('SupplierGetAllForOrder') is not null
	Begin
	Print 'SP : SupplierGetAllForOrder - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierGetAllForOrder] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierGetAllForOrder - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierGetByNumber
If OBJECT_ID('SupplierGetByNumber') is not null
	Begin
	Print 'SP : SupplierGetByNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierGetByNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierGetByNumber - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierGetByNumbers
If OBJECT_ID('SupplierGetByNumbers') is not null
	Begin
	Print 'SP : SupplierGetByNumbers - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierGetByNumbers] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierGetByNumbers - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierGetNumberNames
If OBJECT_ID('SupplierGetNumberNames') is not null
	Begin
	Print 'SP : SupplierGetNumberNames - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierGetNumberNames] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierGetNumberNames - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierGetOrderDetails
If OBJECT_ID('SupplierGetOrderDetails') is not null
	Begin
	Print 'SP : SupplierGetOrderDetails - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierGetOrderDetails] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierGetOrderDetails - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierGetSupplierTypes
If OBJECT_ID('SupplierGetSupplierTypes') is not null
	Begin
	Print 'SP : SupplierGetSupplierTypes - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierGetSupplierTypes] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierGetSupplierTypes - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierMasterOrderingSelectAll
If OBJECT_ID('SupplierMasterOrderingSelectAll') is not null
	Begin
	Print 'SP : SupplierMasterOrderingSelectAll - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierMasterOrderingSelectAll] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierMasterOrderingSelectAll - Skipping Permissions (D.N.E)'
	End 

-- SP SupplierMasterSelectByNumber
If OBJECT_ID('SupplierMasterSelectByNumber') is not null
	Begin
	Print 'SP : SupplierMasterSelectByNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierMasterSelectByNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierMasterSelectByNumber - Skipping Permissions (D.N.E)'
	End 

-- SP SuppliersSelectReturnPolicy
If OBJECT_ID('SuppliersSelectReturnPolicy') is not null
	Begin
	Print 'SP : SuppliersSelectReturnPolicy - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SuppliersSelectReturnPolicy] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SuppliersSelectReturnPolicy - Skipping Permissions (D.N.E)'
	End 


-- SP SuppliersSelectAllForOrdering
If OBJECT_ID('SuppliersSelectAllForOrdering') is not null
	Begin
	Print 'SP : SuppliersSelectAllForOrdering - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SuppliersSelectAllForOrdering] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SuppliersSelectAllForOrdering - Skipping Permissions (D.N.E)'
	End 


-- SP SuppliersSelectByNumber
If OBJECT_ID('SuppliersSelectByNumber') is not null
	Begin
	Print 'SP : SuppliersSelectByNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SuppliersSelectByNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SuppliersSelectByNumber - Skipping Permissions (D.N.E)'
	End 


-- SP SuppliersSelectReturnPolicy
If OBJECT_ID('SuppliersSelectReturnPolicy') is not null
	Begin
	Print 'SP : SuppliersSelectReturnPolicy - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SuppliersSelectReturnPolicy] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SuppliersSelectReturnPolicy - Skipping Permissions (D.N.E)'
	End 


-- SP SuppliersUpdateSoqInventory
If OBJECT_ID('SupplierUpdateSoqInventory') is not null
	Begin
	Print 'SP : SupplierUpdateSoqInventory - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SupplierUpdateSoqInventory] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SupplierUpdateSoqInventory - Skipping Permissions (D.N.E)'
	End 

-- SP SystemCodesSelectType
If OBJECT_ID('SystemCodesSelectType') is not null
	Begin
	Print 'SP : SystemCodesSelectType - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemCodesSelectType] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemCodesSelectType - Skipping Permissions (D.N.E)'
	End 

-- SP SystemDateGetDates
If OBJECT_ID('SystemDateGetDates') is not null
	Begin
	Print 'SP : SystemDateGetDates - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemDateGetDates] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemDateGetDates - Skipping Permissions (D.N.E)'
	End 

-- SP SystemDateUpdate
If OBJECT_ID('SystemDateUpdate') is not null
	Begin
	Print 'SP : SystemDateUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemDateUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemDateUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SystemGetParameter
If OBJECT_ID('SystemGetParameter') is not null
	Begin
	Print 'SP : SystemGetParameter - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemGetParameter] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemGetParameter - Skipping Permissions (D.N.E)'
	End 

-- SP SystemNetworkGet
If OBJECT_ID('SystemNetworkGet') is not null
	Begin
	Print 'SP : SystemNetworkGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemNetworkGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemNetworkGet - Skipping Permissions (D.N.E)'
	End 

-- SP SystemNetworkUpdate
If OBJECT_ID('SystemNetworkUpdate') is not null
	Begin
	Print 'SP : SystemNetworkUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemNetworkUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemNetworkUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SystemNumbersGetNext
If OBJECT_ID('SystemNumbersGetNext') is not null
	Begin
	Print 'SP : SystemNumbersGetNext - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemNumbersGetNext] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemNumbersGetNext - Skipping Permissions (D.N.E)'
	End 

-- SP SystemOptionGet
If OBJECT_ID('SystemOptionGet') is not null
	Begin
	Print 'SP : SystemOptionGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemOptionGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemOptionGet - Skipping Permissions (D.N.E)'
	End 

-- SP SystemOptionUpdate
If OBJECT_ID('SystemOptionUpdate') is not null
	Begin
	Print 'SP : SystemOptionUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemOptionUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemOptionUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP SystemPeriodAutoPopulate
If OBJECT_ID('SystemPeriodAutoPopulate') is not null
	Begin
	Print 'SP : SystemPeriodAutoPopulate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemPeriodAutoPopulate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemPeriodAutoPopulate - Skipping Permissions (D.N.E)'
	End 

-- SP SystemPeriodGet
If OBJECT_ID('SystemPeriodGet') is not null
	Begin
	Print 'SP : SystemPeriodGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemPeriodGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemPeriodGet - Skipping Permissions (D.N.E)'
	End 

-- SP SystemPeriodSafeGet
If OBJECT_ID('SystemPeriodSafeGet') is not null
	Begin
	Print 'SP : SystemPeriodSafeGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[SystemPeriodSafeGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : SystemPeriodSafeGet - Skipping Permissions (D.N.E)'
	End 

-- SP TillGetCurrentTotals
If OBJECT_ID('TillGetCurrentTotals') is not null
	Begin
	Print 'SP : TillGetCurrentTotals - Applying Permissions'
	GRANT EXECUTE ON [dbo].[TillGetCurrentTotals] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : TillGetCurrentTotals - Skipping Permissions (D.N.E)'
	End 

-- SP TransactionGet
If OBJECT_ID('TransactionGet') is not null
	Begin
	Print 'SP : TransactionGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[TransactionGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : TransactionGet - Skipping Permissions (D.N.E)'
	End 

-- SP TransactionLinesGet
If OBJECT_ID('TransactionLinesGet') is not null
	Begin
	Print 'SP : TransactionLinesGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[TransactionLinesGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : TransactionLinesGet - Skipping Permissions (D.N.E)'
	End 

-- SP TransactionPaymentsGet
If OBJECT_ID('TransactionPaymentsGet') is not null
	Begin
	Print 'SP : TransactionPaymentsGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[TransactionPaymentsGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : TransactionPaymentsGet - Skipping Permissions (D.N.E)'
	End 

-- SP UserGet
If OBJECT_ID('UserGet') is not null
	Begin
	Print 'SP : UserGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[UserGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : UserGet - Skipping Permissions (D.N.E)'
	End 

-- SP UserGetNextId
If OBJECT_ID('UserGetNextId') is not null
	Begin
	Print 'SP : UserGetNextId - Applying Permissions'
	GRANT EXECUTE ON [dbo].[UserGetNextId] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : UserGetNextId - Skipping Permissions (D.N.E)'
	End 

-- SP UserGetUser
If OBJECT_ID('UserGetUser') is not null
	Begin
	Print 'SP : UserGetUser - Applying Permissions'
	GRANT EXECUTE ON [dbo].[UserGetUser] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : UserGetUser - Skipping Permissions (D.N.E)'
	End 

-- SP UserGetUsers
If OBJECT_ID('UserGetUsers') is not null
	Begin
	Print 'SP : UserGetUsers - Applying Permissions'
	GRANT EXECUTE ON [dbo].[UserGetUsers] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : UserGetUsers - Skipping Permissions (D.N.E)'
	End 

-- SP UserInsert
If OBJECT_ID('UserInsert') is not null
	Begin
	Print 'SP : UserInsert - Applying Permissions'
	GRANT EXECUTE ON [dbo].[UserInsert] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : UserInsert - Skipping Permissions (D.N.E)'
	End 

-- SP UserProfileGet
If OBJECT_ID('UserProfileGet') is not null
	Begin
	Print 'SP : UserProfileGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[UserProfileGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : UserProfileGet - Skipping Permissions (D.N.E)'
	End 

-- SP UserUpdate
If OBJECT_ID('UserUpdate') is not null
	Begin
	Print 'SP : UserUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[UserUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : UserUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP usp_DeleteStockAdjustment
If OBJECT_ID('usp_DeleteStockAdjustment') is not null
	Begin
	Print 'SP : usp_DeleteStockAdjustment - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_DeleteStockAdjustment] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_DeleteStockAdjustment - Skipping Permissions (D.N.E)'
	End 

-- SP usp_EventsGetEventsBySku
If OBJECT_ID('usp_EventsGetEventsBySku') is not null
	Begin
	Print 'SP : usp_EventsGetEventsBySku - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_EventsGetEventsBySku] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_EventsGetEventsBySku - Skipping Permissions (D.N.E)'
	End 


-- SP usp_GetDRLNumber
If OBJECT_ID('usp_GetDRLNumber') is not null
	Begin
	Print 'SP : usp_GetDRLNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetDRLNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetDRLNumber - Skipping Permissions (D.N.E)'
	End 


-- SP usp_GetEmployeeCode
If OBJECT_ID('usp_GetEmployeeCode') is not null
	Begin
	Print 'SP : usp_GetEmployeeCode - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetEmployeeCode] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetEmployeeCode - Skipping Permissions (D.N.E)'
	End 


-- SP usp_GetEmployeeInitials
If OBJECT_ID('usp_GetEmployeeInitials') is not null
	Begin
	Print 'SP : usp_GetEmployeeInitials - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetEmployeeInitials] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetEmployeeInitials - Skipping Permissions (D.N.E)'
	End 


-- SP usp_GetLastStockLogForSku
If OBJECT_ID('usp_GetLastStockLogForSku') is not null
	Begin
	Print 'SP : usp_GetLastStockLogForSku - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetLastStockLogForSku] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetLastStockLogForSku - Skipping Permissions (D.N.E)'
	End 


-- SP usp_GetPeriodIdForToday
If OBJECT_ID('usp_GetPeriodIdForToday') is not null
	Begin
	Print 'SP : usp_GetPeriodIdForToday - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetPeriodIdForToday] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetPeriodIdForToday - Skipping Permissions (D.N.E)'
	End 

-- SP usp_GetReceiptQty
If OBJECT_ID('usp_GetReceiptQty') is not null
	Begin
	Print 'SP : usp_GetReceiptQty - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetReceiptQty] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : GetReceiptQty - Skipping Permissions (D.N.E)'
	End 

-- SP usp_GetReducedStockSkuns
If OBJECT_ID('usp_GetReducedStockSkuns') is not null
	Begin
	Print 'SP : usp_GetReducedStockSkuns - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetReducedStockSkuns] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetReducedStockSkuns - Skipping Permissions (D.N.E)'
	End 

-- SP usp_GetStockAdjustmentCodes
If OBJECT_ID('usp_GetStockAdjustmentCodes') is not null
	Begin
	Print 'SP : usp_GetStockAdjustmentCodes - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetStockAdjustmentCodes] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetStockAdjustmentCodes - Skipping Permissions (D.N.E)'
	End 

-- SP usp_GetStockAdjustmentDate
If OBJECT_ID('usp_GetStockAdjustmentDate') is not null
	Begin
	Print 'SP : usp_GetStockAdjustmentDate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetStockAdjustmentDate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetStockAdjustmentDate - Skipping Permissions (D.N.E)'
	End 

-- SP usp_GetStockAdjustmentForSkuToday
If OBJECT_ID('usp_GetStockAdjustmentForSkuToday') is not null
	Begin
	Print 'SP : usp_GetStockAdjustmentForSkuToday - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetStockAdjustmentForSkuToday] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetStockAdjustmentForSkuToday - Skipping Permissions (D.N.E)'
	End 

-- SP usp_GetStockAndCYHMASValuesForSku
If OBJECT_ID('usp_GetStockAndCYHMASValuesForSku') is not null
	Begin
	Print 'SP : usp_GetStockAndCYHMASValuesForSku - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetStockAndCYHMASValuesForSku] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetStockAndCYHMASValuesForSku - Skipping Permissions (D.N.E)'
	End 

-- SP usp_GetStoreNumber
If OBJECT_ID('usp_GetStoreNumber') is not null
	Begin
	Print 'SP : usp_GetStoreNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetStoreNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetStoreNumber - Skipping Permissions (D.N.E)'
	End 

-- SP usp_fillDescriptions
If OBJECT_ID('usp_fillDescriptions') is not null
	Begin
	Print 'SP : usp_fillDescriptions - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_fillDescriptions] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_fillDescriptions - Skipping Permissions (D.N.E)'
	End 

-- SP usp_HandHeldTerminalDetail
If OBJECT_ID('usp_HandHeldTerminalDetail') is not null
	Begin
	Print 'SP : usp_HandHeldTerminalDetail - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_HandHeldTerminalDetail] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_HandHeldTerminalDetail - Skipping Permissions (D.N.E)'
	End 

-- SP usp_HandHeldTerminalDetailUpdate
If OBJECT_ID('usp_HandHeldTerminalDetailUpdate') is not null
	Begin
	Print 'SP : usp_HandHeldTerminalDetailUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_HandHeldTerminalDetailUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_HandHeldTerminalDetailUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP usp_HandHeldTerminalHeaderLatest
If OBJECT_ID('usp_HandHeldTerminalHeaderLatest') is not null
	Begin
	Print 'SP : usp_HandHeldTerminalHeaderLatest - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_HandHeldTerminalHeaderLatest] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_HandHeldTerminalHeaderLatest - Skipping Permissions (D.N.E)'
	End 

-- SP usp_HandHeldTerminalHeaderUpdate
If OBJECT_ID('usp_HandHeldTerminalHeaderUpdate') is not null
	Begin
	Print 'SP : usp_HandHeldTerminalHeaderUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_HandHeldTerminalHeaderUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_HandHeldTerminalHeaderUpdate - Skipping Permissions (D.N.E)'
	End 

-- SP usp_ISUHDR_ISULIN
If OBJECT_ID('usp_ISUHDR_ISULIN') is not null
	Begin
	Print 'SP : usp_ISUHDR_ISULIN - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_ISUHDR_ISULIN] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_ISUHDR_ISULIN - Skipping Permissions (D.N.E)'
	End 

-- SP usp_RemoteStockCheck
If OBJECT_ID('usp_RemoteStockCheck') is not null
	Begin
	Print 'SP : usp_RemoteStockCheck - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_RemoteStockCheck] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_RemoteStockCheck - Skipping Permissions (D.N.E)'
	End 

-- SP usp_RemoteStockCheckADV
If OBJECT_ID('usp_RemoteStockCheckADV') is not null
	Begin
	Print 'SP : usp_RemoteStockCheckADV - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_RemoteStockCheckADV] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_RemoteStockCheckADV - Skipping Permissions (D.N.E)'
	End 


-- SP usp_ReportStockBelowImpact
If OBJECT_ID('usp_ReportStockBelowImpact') is not null
	Begin
	Print 'SP : usp_ReportStockBelowImpact - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_ReportStockBelowImpact] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_ReportStockBelowImpact - Skipping Permissions (D.N.E)'
	End 


-- SP usp_StockBelowImpactReport
If OBJECT_ID('usp_StockBelowImpactReport') is not null
	Begin
	Print 'SP : usp_StockBelowImpactReport - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_StockBelowImpactReport] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_StockBelowImpactReport - Skipping Permissions (D.N.E)'
	End 

-- SP usp_SecurityProfileUpdateSystemUsers
If OBJECT_ID('usp_SecurityProfileUpdateSystemUsers') is not null
	Begin
	Print 'SP : usp_SecurityProfileUpdateSystemUsers - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_SecurityProfileUpdateSystemUsers] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_SecurityProfileUpdateSystemUsers - Skipping Permissions (D.N.E)'
	End 

-- SP usp_STMAS_ONOR
If OBJECT_ID('usp_STMAS_ONOR') is not null
	Begin
	Print 'SP : usp_STMAS_ONOR - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_STMAS_ONOR] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_STMAS_ONOR - Skipping Permissions (D.N.E)'
	End 

-- SP usp_SystemUsersUpdateCASMAS
If OBJECT_ID('usp_SystemUsersUpdateCASMAS') is not null
	Begin
	Print 'SP : usp_SystemUsersUpdateCASMAS - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_SystemUsersUpdateCASMAS] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_SystemUsersUpdateCASMAS - Skipping Permissions (D.N.E)'
	End 

-- SP usp_SystemUsersUpdateRSCASH
If OBJECT_ID('usp_SystemUsersUpdateRSCASH') is not null
	Begin
	Print 'SP : usp_SystemUsersUpdateRSCASH - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_SystemUsersUpdateRSCASH] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_SystemUsersUpdateRSCASH - Skipping Permissions (D.N.E)'
	End 

-- SP usp_SystemUsersUpdateSYSPAS
If OBJECT_ID('usp_SystemUsersUpdateSYSPAS') is not null
	Begin
	Print 'SP : usp_SystemUsersUpdateSYSPAS - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_SystemUsersUpdateSYSPAS] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_SystemUsersUpdateSYSPAS - Skipping Permissions (D.N.E)'
	End 

-- SP usp_HandHeldTerminalHeaderByDate
If OBJECT_ID('usp_HandHeldTerminalHeaderByDate') is not null
	Begin
	Print 'SP : usp_HandHeldTerminalHeaderByDate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_HandHeldTerminalHeaderByDate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_HandHeldTerminalHeaderByDate - Skipping Permissions (D.N.E)'
	End 

-- SP usp_InsertStockAdjustment
If OBJECT_ID('usp_InsertStockAdjustment') is not null
	Begin
	Print 'SP : usp_InsertStockAdjustment - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_InsertStockAdjustment] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_InsertStockAdjustment - Skipping Permissions (D.N.E)'
	End 

-- SP usp_InsertStockLogForSku
If OBJECT_ID('usp_InsertStockLogForSku') is not null
	Begin
	Print 'SP : usp_InsertStockLogForSku - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_InsertStockLogForSku] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_InsertStockLogForSku - Skipping Permissions (D.N.E)'
	End 

-- SP usp_IsSkuInDRL
If OBJECT_ID('usp_IsSkuInDRL') is not null
	Begin
	Print 'SP : usp_IsSkuInDRL - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_IsSkuInDRL] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_IsSkuInDRL - Skipping Permissions (D.N.E)'
	End 

-- SP usp_Test
If OBJECT_ID('usp_Test') is not null
	Begin
	Print 'SP : usp_Test - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_Test] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_Test - Skipping Permissions (D.N.E)'
	End 

-- SP usp_UpdateStockAdjustment
If OBJECT_ID('usp_UpdateStockAdjustment') is not null
	Begin
	Print 'SP : usp_UpdateStockAdjustment - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_UpdateStockAdjustment] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_UpdateStockAdjustment - Skipping Permissions (D.N.E)'
	End 

-- SP usp_UpdateStockAndCYHMASValuesForSku
If OBJECT_ID('usp_UpdateStockAndCYHMASValuesForSku') is not null
	Begin
	Print 'SP : usp_UpdateStockAndCYHMASValuesForSku - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_UpdateStockAndCYHMASValuesForSku] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_UpdateStockAndCYHMASValuesForSku - Skipping Permissions (D.N.E)'
	End 

-- SP VatGet
If OBJECT_ID('VatGet') is not null
	Begin
	Print 'SP : VatGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VatGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VatGet - Skipping Permissions (D.N.E)'
	End 

-- SP VendaOrderGet
If OBJECT_ID('VendaOrderGet') is not null
	Begin
	Print 'SP : VendaOrderGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VendaOrderGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VendaOrderGet - Skipping Permissions (D.N.E)'
	End 

-- SP VisionLinePersist
If OBJECT_ID('VisionLinePersist') is not null
	Begin
	Print 'SP : VisionLinePersist - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VisionLinePersist] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VisionLinePersist - Skipping Permissions (D.N.E)'
	End 

-- SP VisionPaymentPersist
If OBJECT_ID('VisionLinePersist') is not null
	Begin
	Print 'SP : VisionLinePersist - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VisionLinePersist] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VisionLinePersist - Skipping Permissions (D.N.E)'
	End 

-- SP VisionPaymentPersistFlash
If OBJECT_ID('VisionLinePersistFlash') is not null
	Begin
	Print 'SP : VisionLinePersistFlash - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VisionLinePersistFlash] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VisionLinePersistFlash - Skipping Permissions (D.N.E)'
	End 

-- SP VisionSalePayment
If OBJECT_ID('VisionSalePayment') is not null
	Begin
	Print 'SP : VisionSalePayment - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VisionSalePayment] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VisionSalePayment - Skipping Permissions (D.N.E)'
	End 

-- SP VisionSalesGet
If OBJECT_ID('VisionSalesGet') is not null
	Begin
	Print 'SP : VisionSalesGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VisionSalesGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VisionSalesGet - Skipping Permissions (D.N.E)'
	End 

-- SP VisionSaleTotal
If OBJECT_ID('VisionSaleTotal') is not null
	Begin
	Print 'SP : VisionSaleTotal - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VisionSaleTotal] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VisionSaleTotal - Skipping Permissions (D.N.E)'
	End 

-- SP VisionTotalPersist
If OBJECT_ID('VisionTotalPersist') is not null
	Begin
	Print 'SP : VisionTotalPersist - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VisionTotalPersist] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VisionTotalPersist - Skipping Permissions (D.N.E)'
	End 

-- SP VisionTotalPersistFlash
If OBJECT_ID('VisionTotalPersistFlash') is not null
	Begin
	Print 'SP : VisionTotalPersistFlash - Applying Permissions'
	GRANT EXECUTE ON [dbo].[VisionTotalPersistFlash] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : VisionTotalPersistFlash - Skipping Permissions (D.N.E)'
	End 

-- SP WorkstationGet
If OBJECT_ID('WorkstationGet') is not null
	Begin
	Print 'SP : WorkstationGet - Applying Permissions'
	GRANT EXECUTE ON [dbo].[WorkstationGet] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : WorkstationGet - Skipping Permissions (D.N.E)'
	End 

-- SP WorkstationGetWorkstation
If OBJECT_ID('WorkstationGetWorkstation') is not null
	Begin
	Print 'SP : WorkstationGetWorkstation - Applying Permissions'
	GRANT EXECUTE ON [dbo].[WorkstationGetWorkstation] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : WorkstationGetWorkstation - Skipping Permissions (D.N.E)'
	End 


-----------------------------------------------------------------------------------
-- New Scripts for Store 120 Added Below
-----------------------------------------------------------------------------------
If OBJECT_ID('NitmasStarted') is not null
	Begin
	Print 'SP : NitmasStarted - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NitmasStarted] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NitmasStarted - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('NitmasStopped') is not null
	Begin
	Print 'SP : NitmasStopped - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NitmasStopped] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NitmasStopped - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('StockGetLatestStockLogByKey') is not null
	Begin
	Print 'SP : StockGetLatestStockLogByKey - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetLatestStockLogByKey] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetLatestStockLogByKey - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('StockGetSTKMASDataForStockLog') is not null
	Begin
	Print 'SP : StockGetSTKMASDataForStockLog - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetSTKMASDataForStockLog] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetSTKMASDataForStockLog - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('StockGetStockLogKey') is not null
	Begin
	Print 'SP : StockGetStockLogKey - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockGetStockLogKey] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockGetStockLogKey - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('StockUpdateSTKMASStkLogValues') is not null
	Begin
	Print 'SP : StockUpdateSTKMASStkLogValues - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockUpdateSTKMASStkLogValues] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockUpdateSTKMASStkLogValues - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('usp_GetNextTillTranNumber') is not null
	Begin
	Print 'SP : usp_GetNextTillTranNumber - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_GetNextTillTranNumber] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_GetNextTillTranNumber - Skipping Permissions (D.N.E)'
	End 

-- KM Modified 30-09-2011 Release 3.9.3

If OBJECT_ID('usp_CashBalCashierUpdate') is not null
	Begin
	Print 'SP : usp_CashBalCashierUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_CashBalCashierUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_CashBalCashierUpdate - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('usp_CashBalCashTenUpdate') is not null
	Begin
	Print 'SP : usp_CashBalCashTenUpdate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_CashBalCashTenUpdate] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_CashBalCashTenUpdate - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('usp_handheldterminaldetailrefund') is not null
	Begin
	Print 'SP : usp_handheldterminaldetailrefund - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_handheldterminaldetailrefund] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_handheldterminaldetailrefund - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('usp_HandHeldTerminalDetailRefundItemsDistinctSKUs') is not null
	Begin
	Print 'SP : usp_HandHeldTerminalDetailRefundItemsDistinctSKUs - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_HandHeldTerminalDetailRefundItemsDistinctSKUs] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_HandHeldTerminalDetailRefundItemsDistinctSKUs - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('usp_handheldterminaldetailrefundrandomsku') is not null
	Begin
	Print 'SP : usp_handheldterminaldetailrefundrandomsku - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_handheldterminaldetailrefundrandomsku] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_handheldterminaldetailrefundrandomsku - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('usp_OnOrderProcessTransmissions') is not null
	Begin
	Print 'SP : usp_OnOrderProcessTransmissions - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_OnOrderProcessTransmissions] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_OnOrderProcessTransmissions - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('PriceChangeLoad') is not null
	Begin
	Print 'SP : PriceChangeLoad - Applying Permissions'
	GRANT EXECUTE ON [dbo].[PriceChangeLoad] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : PriceChangeLoad - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('usp_Leadblocker') is not null
	Begin
	Print 'SP : usp_Leadblocker - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_Leadblocker] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_Leadblocker - Skipping Permissions (D.N.E)'
	End 

-----------------------------------------------------------------------------------
-- Wickes IT ASBO Process
-----------------------------------------------------------------------------------


-- SP usp_SystemNightRoutineCompleteCheck
If OBJECT_ID('usp_SystemNightRoutineCompleteCheck') is not null
	Begin
	Print 'SP : usp_SystemNightRoutineCompleteCheck - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_SystemNightRoutineCompleteCheck] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_SystemNightRoutineCompleteCheck - Skipping Permissions (D.N.E)'
	End  

-----------------------------------------------------------------------------------
-- Permissions Applied
-----------------------------------------------------------------------------------

If OBJECT_ID('KevanConversion_PERNTU') is not null
	Begin
	Print 'SP : KevanConversion_PERNTU - Applying Permissions'
	GRANT EXECUTE ON [dbo].[KevanConversion_PERNTU] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : KevanConversion_PERNTU - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('KevanConversion_PERSPS') is not null
	Begin
	Print 'SP : KevanConversion_PERSPS - Applying Permissions'
	GRANT EXECUTE ON [dbo].[KevanConversion_PERSPS] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : KevanConversion_PERSPS - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('KevanConversion_PERTAB') is not null
	Begin
	Print 'SP : KevanConversion_PERTAB - Applying Permissions'
	GRANT EXECUTE ON [dbo].[KevanConversion_PERTAB] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : KevanConversion_PERTAB - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('KevanConversion_PERSFG') is not null
	Begin
	Print 'SP : KevanConversion_PERSFG - Applying Permissions'
	GRANT EXECUTE ON [dbo].[KevanConversion_PERSFG] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : KevanConversion_PERSFG - Skipping Permissions (D.N.E)'
	End 

If OBJECT_ID('usp_PicRefundCount_RefundTransactionLines_Get') is not null
	Begin
	Print 'SP : usp_PicRefundCount_RefundTransactionLines_Get - Applying Permissions'
	GRANT EXECUTE ON [dbo].[usp_PicRefundCount_RefundTransactionLines_Get] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : usp_PicRefundCount_RefundTransactionLines_Get - Skipping Permissions (D.N.E)'
	End 
END;
GO

