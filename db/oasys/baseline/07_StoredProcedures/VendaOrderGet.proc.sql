﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VendaOrderGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VendaOrderGet'
	EXEC ('CREATE PROCEDURE [dbo].[VendaOrderGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VendaOrderGet'
GO
ALTER PROCEDURE VendaOrderGet
@DocumentNumber char(8)
AS
BEGIN
SET NOCOUNT ON;

SELECT count(*) AS RecordCount
FROM DLTOTS WHERE TCOD = 'SA' and DOCN = @DocumentNumber

END
GO

