﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetRelatedItemsRequiringAdjustments]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetRelatedItemsRequiringAdjustments'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetRelatedItemsRequiringAdjustments] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetRelatedItemsRequiringAdjustments'
GO
ALTER PROCedure [dbo].[usp_GetRelatedItemsRequiringAdjustments]
As
	Begin
		Select 
			SingleSkun = single.SKUN,
			SinglePrice = single.PRIC,
			SingleOnHand = single.ONHA,
			PackSkun = pack.SKUN, 
			PackPrice = pack.PRIC,
			PackOnHand = pack.ONHA,
			ItemPackSize = item.NSPP
		From
			RELITM item
				Inner Join
					STKMAS single
				On
					item.SPOS = single.SKUN
				inner join
					STKMAS pack
				On
					item.PPOS = pack.SKUN
		Where
			single.ONHA < 0
		And
			item.DELC = 0
End
GO

