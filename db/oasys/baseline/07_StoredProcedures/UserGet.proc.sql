﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UserGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure UserGet'
	EXEC ('CREATE PROCEDURE [dbo].[UserGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure UserGet'
GO
ALTER PROCEDURE [dbo].[UserGet]
@Id INT=null
AS
begin
	SET NOCOUNT ON;

	select 
		ID				as Id,
		EmployeeCode			as Code,
		SecurityProfileID		as ProfileId,
		Name,
		Initials,
		Position,
		PayrollID			as PayrollId,
		Password,
		PasswordExpires,
		SupervisorPassword		as SuperPassword,
		SupervisorPwdExpires		as SuperPasswordExpires,
		Outlet,
		IsManager,
		IsSupervisor,
		IsDeleted,
		DeletedDate,
		DeletedBy,
		DeletedWhere,
		TillReceiptName,
		LanguageCode
	from
		SystemUsers
	where
		(@Id is null) or (@Id is not null and ID = @Id)
	
end
GO

