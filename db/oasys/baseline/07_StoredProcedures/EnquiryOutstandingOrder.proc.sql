﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryOutstandingOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryOutstandingOrder'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryOutstandingOrder] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryOutstandingOrder'
GO
ALTER PROCEDURE [dbo].[EnquiryOutstandingOrder]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	select
		ph.numb		as PoNumber,
		pl.qtyo		as Qty,
		ph.ddat		as DateDue
	from
		purhdr ph
	inner join
		purlin pl	on pl.hkey = ph.tkey and pl.skun = @SkuNumber
	where
		ph.rcom = 0
	and ph.delm = 0
	order by
		ph.ddat desc

END
GO

