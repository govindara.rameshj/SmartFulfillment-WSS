﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleGetMissingTransactions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleGetMissingTransactions'
	EXEC ('CREATE PROCEDURE [dbo].[SaleGetMissingTransactions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleGetMissingTransactions'
GO
ALTER PROCEDURE [dbo].[SaleGetMissingTransactions]
@Date DATE, @TillId CHAR (2)
AS
begin
	SET NOCOUNT ON;

	declare @table table(Id char(4))
	declare 
		@counter int,
		@minValue int,
		@maxValue int;

	select
		@maxValue = MAX(DLTOTS.[TRAN]),
		@minValue = MIN(DLTOTS.[TRAN])
	from DLTOTS where DLTOTS.DATE1=@Date and DLTOTS.TILL=@TillId;
	
	SET @COUNTER = 1
	WHILE @COUNTER <= @maxValue
	BEGIN
		INSERT INTO @table(Id) VALUES (RIGHT('0000' + RTRIM(@COUNTER), 4))
		SET @COUNTER = @COUNTER + 1
	END

	SELECT 
		t.Id
	FROM 
		@table t
	right JOIN 
		DLTOTS dt on dt.[TRAN]=t.Id
	where 
		dt.DATE1 = @Date
		and dt.TILL = @TillId
		and dt.[TRAN] is null
	ORDER BY 
		t.ID

end
GO

