﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierGetSupplierTypes]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierGetSupplierTypes'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierGetSupplierTypes] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierGetSupplierTypes'
GO
ALTER PROCedure [dbo].[SupplierGetSupplierTypes]
AS
BEGIN
	declare @OutputTable TABLE ( Id char(1), Display char(10) )

	insert into @OutputTable values ('A', 'All');
	insert into @OutputTable values ('D', 'Direct');
	insert into @OutputTable values ('W', 'Warehouse');
	
	select * from @OutputTable
	
END
GO

