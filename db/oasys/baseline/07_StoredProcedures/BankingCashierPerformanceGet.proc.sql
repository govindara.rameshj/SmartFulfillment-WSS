﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BankingCashierPerformanceGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure BankingCashierPerformanceGet'
	EXEC ('CREATE PROCEDURE [dbo].[BankingCashierPerformanceGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure BankingCashierPerformanceGet'
GO
ALTER PROCedure [dbo].[BankingCashierPerformanceGet]
    @DateStart  date,
    @DateEnd    date
as
begin
    declare @periodStart int;
    declare @periodEnd   int;
    declare @table table(CashierId          int,
                         CashierName        varchar(50),
                         NumTransactions    int,
                         NumCorrections     int,
                         NumVoids           int,
                         NumLinesReversed   int,
                         NumTotal           dec(19,2),
                         PercentPerformance dec(19,2),
                         NumOpenDrawers     int,
                         NumLinesSold       dec(19,2),
                         NumLinesScanned    dec(19,2),
                         PercentScanning    dec(19,2),
                         ValuePickup        dec(19,2),
                         ValueFloat         dec(19,2),
                         ValueSales         dec(19,2),
                         ValueVariance      dec(19,2),
                         PercentVariance    dec(19,2));

    set @periodStart = coalesce((select ID from SystemPeriods where StartDate<=@DateStart and EndDate>=@DateStart),0);
    set @periodEnd   = coalesce((select ID from SystemPeriods where StartDate<=@DateEnd and EndDate>=@DateEnd),0);

    insert into 
        @table (
        CashierId,
        CashierName, 
        NumTransactions,
        NumCorrections,
        NumVoids,
        NumLinesReversed,
        NumTotal,
        NumOpenDrawers,
        NumLinesSold,
        NumLinesScanned,
        ValuePickup,
        ValueFloat,
        ValueSales)
    select 
        cc.CashierID,
        coalesce(su.Name, 'Unknown') as [Name],
        sum(cc.NumTransactions),
        sum(cc.NumCorrections),
        sum(cc.NumVoids),
        sum(cc.NumLinesReversed),
        sum(cc.NumTransactions+cc.NumCorrections+cc.NumVoids+cc.NumLinesReversed),
        sum(cc.NumOpenDrawer),
        sum(cc.NumLinesSold),
        sum(cc.NumLinesScanned),
        IsNull ((select sum(PickUp) from CashBalCashierTen cct where cct.CashierID=cc.CashierID and cct.PeriodID>=@periodStart and cct.PeriodID<=@periodEnd), 0),
        sum(cc.FloatIssued),
        sum(cc.GrossSalesAmount)
    from 
        CashBalCashier cc
    left join
        SystemUsers su on su.ID=cc.CashierID
    where
        cc.PeriodID>=@periodStart and
        cc.PeriodID<=@periodEnd
    group by
        cc.cashierId,
        su.Name;

    update
        @table
    set 
        PercentPerformance= case NumTotal
                                when 0 then 0
                                else NumTransactions/NumTotal*100
                            end,
        PercentScanning =   case NumLinesSold
                                when 0 then 0
                                else NumLinesScanned/NumLinesSold*100
                            end,
        ValueVariance   = ValuePickup - ValueFloat - ValueSales;

    update 
        @table 
    set 
        PercentVariance =   case ValuePickup
                            when 0 then 0
                            else ValueVariance/ValuePickup*100
                            end;
                            
    update 
        @table 
    set 
        PercentScanning = 100
    where 
        CashierId = 499                         
    
    select * from @table;
    
end
GO

