﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_HandHeldTerminalDetailRefundItemsDistinctSKUs]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_HandHeldTerminalDetailRefundItemsDistinctSKUs'
	EXEC ('CREATE PROCEDURE [dbo].[usp_HandHeldTerminalDetailRefundItemsDistinctSKUs] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_HandHeldTerminalDetailRefundItemsDistinctSKUs'
GO
-- =======================================================================================
-- Author       : Partha Dutta
-- Create date  : 22/08/2011
-- Referral No  : RF0861
-- Description  : Baseline version
--                Retrieve refunded items distinct SKUs from HHT Details for selected date
-- =======================================================================================
-- =======================================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 06/09/2011
-- Referral No  : RF0861
-- Description  : Modified to count only previous day's count
-- =======================================================================================

ALTER PROCedure usp_HandHeldTerminalDetailRefundItemsDistinctSKUs
   @SelectedDate date
as
set nocount on
begin
   select SKUN
   from HHTDET
   where ORIG = 'R'
   and ICNT   = '0'
   and DATE1 = @SelectedDate
   group by SKUN
end
GO

