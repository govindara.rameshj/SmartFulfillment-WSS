﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptLineUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptLineUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptLineUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptLineUpdate'
GO
ALTER PROCEDURE [dbo].[ReceiptLineUpdate]
(
	@ReceiptNumber	char(6),
	@Sequence		char(4),
	@SkuNumber		char(6),
	@ReceivedChange	int,
	@UserId			int
)
AS
BEGIN
	SET NOCOUNT ON;
	declare @onHand			int;
	declare @returns		int;
	declare	@markdowns		int;
	declare @writeOffs		int;
	declare @price			dec(9,2);

	--update receipt line qty received
	update		drldet
	set			recq = recq + @ReceivedChange
	where		numb = @ReceiptNumber
	and			seqn = @Sequence
	
	--get stock item values
	select	@onHand		= onha,
			@returns	= retq, 
			@markdowns	= mdnq,
			@writeOffs	= wtfq,
			@price		= pric	
	from	stkmas
	where	skun		= @SkuNumber
	
	--update stock item
	update	stkmas
	set		onha = onha + @ReceivedChange,
          --referral 363 - prevent update of ONOR
          --onor = onor - @ReceivedChange,
			treq = treq + @ReceivedChange,
			trev = trev + (@ReceivedChange * @price),
			drec = getdate(),
			tact = 1
	where	skun = @SkuNumber
	
	
	--insert stock log entry
	insert into		stklog
				(
					skun,
					dayn,
					[type],
					date1,
					[time],
					keys,
					eeid,
					sstk, estk,
					sret, eret,
					smdn, emdn,
					swtf, ewtf,
					spri, epri
				)
	values
				(
					@SkuNumber,
					datediff(d, '1900-01-01', getdate()) + 1,
					'73',
					getdate(),
					replace(convert (varchar(8), getdate(), 108),':',''),
					@ReceiptNumber + ' ' + @Sequence,					
					RIGHT('000' + Convert(varchar, @UserId), 3),
					@onHand, @onHand + @ReceivedChange,
					@returns, @returns,
					@markdowns, @markdowns,
					@writeOffs, @writeOffs,
					@price, @price
				)
	
END
GO

