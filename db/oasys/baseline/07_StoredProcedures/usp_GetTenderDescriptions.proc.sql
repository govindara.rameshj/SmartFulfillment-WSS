﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetTenderDescriptions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetTenderDescriptions'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetTenderDescriptions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetTenderDescriptions'
GO
ALTER PROCEDURE [dbo].[usp_GetTenderDescriptions]
AS
BEGIN
    SELECT TTDE1 , 
           TTDE2 , 
           TTDE3 , 
           TTDE4 , 
           TTDE5 , 
           TTDE6 , 
           TTDE7 , 
           TTDE8 , 
           TTDE9 , 
           TTDE10,
           TTDE11,
           TTDE12,
           TTDE13,
           TTDE14,
           TTDE15,
           TTDE16,
           TTDE17,
           TTDE18,
           TTDE19,
           TTDE20
      FROM RETOPT
      WHERE FKEY = '01';
END
GO

