﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeAllTenders]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeAllTenders'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeAllTenders] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeAllTenders'
GO
ALTER PROCedure NewBankingSafeAllTenders
   @PeriodID int
as
begin
set nocount on
--cash : amenable
select a.CurrencyID,
       TenderID              = a.TenderID,
       DenominationID        = a.ID,
       TenderText            = a.DisplayText,
       TenderReadOnly        = cast(0 as bit),
       BankingAmountMultiple = a.BullionMultiple,
       SystemSafe            = b.SystemValue,
       MainSafe              = b.SafeValue,
       ChangeSafe            = b.ChangeValue 
from (select *
      from SystemCurrencyDen
      where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
      and   TenderID   = 1) a
left outer join (select *
                 from SafeDenoms 
                 where PeriodID   = @PeriodID
                 and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)) b
           on  b.TenderID = a.TenderID
           and b.ID = a.ID
union all
--non cash tender : amendable
select a.CurrencyID,
       TenderID              = a.TenderID,
       DenominationID        = a.ID,
       TenderText            = a.DisplayText,
       TenderReadOnly        = cast(0 as bit),
       BankingAmountMultiple = a.BullionMultiple,
       SystemSafe            = b.SystemValue,
       MainSafe              = b.SafeValue,
       ChangeSafe            = b.ChangeValue 
from (select *
      from SystemCurrencyDen
      where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
      and   TenderID  <> 1
      and Amendable    = 1) a
left outer join (select *
                 from SafeDenoms 
                 where PeriodID   = @PeriodID
                 and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)) b
           on  b.TenderID = a.TenderID
           and b.ID = a.ID
union all
--non cash tender : read-only
select a.CurrencyID,
       TenderID              = a.TenderID,
       DenominationID        = a.ID,
       TenderText            = a.DisplayText,
       TenderReadOnly        = cast(1 as bit),
       BankingAmountMultiple = a.BullionMultiple,
       SystemSafe            = b.SystemValue,
       MainSafe              = b.SafeValue,
       ChangeSafe            = b.ChangeValue 
from (select *
      from SystemCurrencyDen
      where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
      and   TenderID  <> 1
      and Amendable    = 0) a
left outer join (select *
                 from SafeDenoms 
                 where PeriodID   = @PeriodID
                 and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)) b
           on  b.TenderID = a.TenderID
           and b.ID = a.ID
order by a.ID desc
end
GO

