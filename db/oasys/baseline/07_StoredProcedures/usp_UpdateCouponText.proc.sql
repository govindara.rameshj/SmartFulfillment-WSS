﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateCouponText]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdateCouponText'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdateCouponText] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdateCouponText'
GO
-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 06/10/2011
-- TFSID        : 2733 
-- Description  : New Stored Procedure for Coupons ProcessTransmissions
-- =====================================================================


-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================

ALTER PROCEDURE usp_UpdateCouponText
@CouponID   AS CHAR(7), 
@SequenceNo AS CHAR(2), 
@PrintSize  AS CHAR (2), 
@TextAlign  AS CHAR(1), 
@PrintText  AS CHAR(52), 
@Deleted    AS BIT 

AS 
  BEGIN 
      IF EXISTS (SELECT 1 
                 FROM   COUPONTEXT 
                 WHERE  couponid = @couponID 
                        AND sequenceno = @SequenceNo) 
        BEGIN 
            UPDATE COUPONTEXT 
            SET    printsize = @PrintSize, 
                   textalign = @TextAlign, 
                   printtext = @PrintText, 
                   deleted = @Deleted 
            WHERE  couponid = @couponID 
                   AND sequenceno = @SequenceNo 
        END 
      ELSE 
        BEGIN 
            INSERT INTO COUPONTEXT 
                        (couponid, 
                         sequenceno, 
                         printsize, 
                         textalign, 
                         printtext, 
                         deleted) 
            VALUES      (@CouponID, 
                         @SequenceNo, 
                         @PrintSize, 
                         @TextAlign, 
                         @PrintText, 
                         @Deleted) 
        END 
  END
GO

