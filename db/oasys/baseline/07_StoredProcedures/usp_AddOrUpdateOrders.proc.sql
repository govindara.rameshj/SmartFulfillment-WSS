﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_AddOrUpdateOrders]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_AddOrUpdateOrders'
	EXEC ('CREATE PROCEDURE [dbo].[usp_AddOrUpdateOrders] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_AddOrUpdateOrders'
GO
ALTER PROCEDURE dbo.usp_AddOrUpdateOrders @OrderId int , 
                                           @VehicleTypeIndex varchar( 10
                                                                    ) , 
                                           @DeliveryDate date , 
                                           @LastUpdateDateTime datetime
AS
BEGIN
    IF EXISTS( SELECT 1
                 FROM vehicleAllocation
                 WHERE OrderID
                       = 
                       @OrderId
                   AND DeliveryDate
                       = 
                       @DeliveryDate
             )
        BEGIN
            UPDATE vehicleAllocation
            SET vehicleTypeIndex = @VehicleTypeIndex , 
                lastvehicleupdateDateTime = @LastUpdateDateTime
              WHERE orderid
                    = 
                    @OrderId;
        END;
    ELSE
        BEGIN
            INSERT INTO VehicleAllocation( OrderID , 
                                           VehicleTypeIndex , 
                                           DeliveryDate , 
                                           LastvehicleUpdateDateTime
                                         )
            VALUES( @OrderId , 
                    @VehicleTypeIndex , 
                    @DeliveryDate , 
                    @LastUpdateDateTime
                  );
        END;
END;
GO

