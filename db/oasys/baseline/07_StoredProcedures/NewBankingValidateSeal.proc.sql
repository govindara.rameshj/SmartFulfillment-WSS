﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingValidateSeal]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingValidateSeal'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingValidateSeal] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingValidateSeal'
GO
ALTER PROCedure NewBankingValidateSeal

   @SealNumber char(20),
   @SafeBagsID int output

as
begin
   set nocount on

   set @SafeBagsID =  isnull((select Id from SafeBags where SealNumber = @SealNumber and State <> 'C'), 0)

end
GO

