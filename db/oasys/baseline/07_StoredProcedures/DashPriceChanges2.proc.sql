﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashPriceChanges2]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashPriceChanges2'
	EXEC ('CREATE PROCEDURE [dbo].[DashPriceChanges2] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashPriceChanges2'
GO
ALTER PROCedure [dbo].[DashPriceChanges2]
	@DateEnd Date
as
begin
   set nocount on

   declare @Output table(RowId         int, 
                         [Description] varchar(50), 
                         Qty           int,
                         Peg           int,
                         Small         int,
                         Medium        int)

   declare @PCToday	                         int,
           @OverdueQty                       int,
           @OutstandingQty                   int,
           @Peg                              int,
           @Small                            int,
           @Medium                           int,
           @PriceChangesConfirmedDayQuantity int

	DECLARE @PriceChangeDates TABLE (
	SKU char(6),
	EffectiveDate DateTime)
	
	INSERT INTO @PriceChangeDates (SKU,EffectiveDate)
	SELECT SKUN,dbo.udf_GetLatestPriceChangeDateForSku(SKUN)
	FROM PRCCHG
	WHERE PSTA = 'U'
	
	SELECT @PCToday = COUNT(*)
	FROM @PriceChangeDates pcd
	JOIN sysdat sd ON sd.TMDT = pcd.EffectiveDate

	SELECT @OverdueQty = COUNT(*)
	FROM @PriceChangeDates pcd
	JOIN sysdat sd ON sd.TMDT > pcd.EffectiveDate
	
   set @OutstandingQty                   = dbo.svf_ReportPriceChangesOutstandingQty()
   set @Peg                              = dbo.svf_ReportPriceChangesLabelsRequiredPeg()
   set @Small                            = dbo.svf_ReportPriceChangesLabelsRequiredSmall()
   set @Medium                           = dbo.svf_ReportPriceChangesLabelsRequiredMedium()
   set @PriceChangesConfirmedDayQuantity = dbo.svf_ReportPriceChangesConfirmedDAY(@DateEnd)

   insert @Output values (1, 'Waiting (Inc. AutoApply)',  @OutstandingQty,                   null, null,   null)
   insert @Output values (2, 'Overdue (Inc. AutoApply)',  @OverdueQty,                       null, null,   null)
   insert @Output values (3, 'Labels Required',                        (@Peg + @Small + @Medium),          @Peg, @Small, @Medium)
   insert @Output values (4, 'Price Changes Today',                     @PCToday,                          null, null,   null)
   insert @Output values (5, 'Price Change Confirmations',              @PriceChangesConfirmedDayQuantity, null, null,   null)
   insert @Output values (6, 'Labels Request',                          null,                              null, null,   null)

   select * from @Output order by RowId
end

If @@Error = 0
   Print 'Success: The Alter Stored Procedure DashPriceChanges2 for US19109 has been deployed successfully'
Else
   Print 'Failure: The Alter Stored Procedure DashPriceChanges2 for US19109 has not been deployed'
GO

