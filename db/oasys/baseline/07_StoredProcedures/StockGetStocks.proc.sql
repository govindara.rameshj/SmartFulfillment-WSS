﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetStocks]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetStocks'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetStocks] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetStocks'
GO
ALTER PROCEDURE [dbo].[StockGetStocks]
@SkuNumbers CHAR (8000)
AS
BEGIN
	SET NOCOUNT ON;

	select 
		st.skun				as 'SkuNumber',
		st.descr			as 'Description',
		st.prod				as 'ProductCode',
		st.pack				as 'PackSize',
		st.pric				as 'Price',
		st.COST				as 'Cost',
		st.WGHT				as 'Weight',
		st.onha				as 'OnHandQty',
		st.onor				as 'OnOrderQty',
		st.mini				as 'MinimumQty',
		st.maxi				as 'MaximumQty',
		st.retq				as 'OpenReturnsQty',
		st.mdnq				as 'MarkdownQty',
		st.wtfq				as 'WriteOffQty',
		st.INON				as 'IsNonStock',
		st.IOBS				as 'IsObsolete',
		st.IDEL				as 'IsDeleted',
		st.IRIS				as 'IsItemSingle',
		st.NOOR				as 'IsNonOrder',
		st.treq				as 'ReceivedTodayQty',
		st.trev				as 'ReceivedTodayValue',
		st.dats				as 'DateFirstStocked',
		st.IODT				as 'DateFirstOrder',
		st.FODT				as 'DateFinalOrder',
		st.drec				as 'DateLastReceived',
		st.CTGY				as 'HieCategory',
		st.GRUP				as 'HieGroup',
		st.SGRP				as 'HieSubgroup',
		st.STYL				as 'HieStyle',			
		st.tact				as 'ActivityToday',
		st.us001			as 'UnitSales1',
		st.us002			as 'UnitSales2',
		st.us003			as 'UnitSales3',
		st.us004			as 'UnitSales4',
		st.us005			as 'UnitSales5',
		st.us006			as 'UnitSales6',
		st.us007			as 'UnitSales7',
		st.us008			as 'UnitSales8',
		st.us009			as 'UnitSales9',
		st.us0010			as 'UnitSales10',
		st.us0011			as 'UnitSales11',
		st.us0012			as 'UnitSales12',
		st.us0013			as 'UnitSales13',
		st.us0014			as 'UnitSales14',
		st.do001			as 'DaysOutStock1',	
		st.do002			as 'DaysOutStock2',	
		st.do003			as 'DaysOutStock3',	
		st.do004			as 'DaysOutStock4',	
		st.do005			as 'DaysOutStock5',	
		st.do006			as 'DaysOutStock6',	
		st.do007			as 'DaysOutStock7',	
		st.do008			as 'DaysOutStock8',	
		st.do009			as 'DaysOutStock9',	
		st.do0010			as 'DaysOutStock10',	
		st.do0011			as 'DaysOutStock11',	
		st.do0012			as 'DaysOutStock12',	
		st.do0013			as 'DaysOutStock13',
		st.do0014			as 'DaysOutStock14',
		st.DateLastSoqInit,
		st.IsInitialised,
		st.ToForecast,	
		st.FlierPeriod1,
		st.FlierPeriod2,
		st.FlierPeriod3,
		st.FlierPeriod4,
		st.FlierPeriod5,
		st.FlierPeriod6,
		st.FlierPeriod7,
		st.FlierPeriod8,
		st.FlierPeriod9,
		st.FlierPeriod10,
		st.FlierPeriod11,
		st.FlierPeriod12,
		st.DemandPattern,
		st.PeriodDemand,
		st.PeriodTrend,
		st.ErrorForecast,
		st.ErrorSmoothed,
		st.BufferConversion,
		st.BufferStock,
		st.ServiceLevelOverride,
		st.ValueAnnualUsage,
		st.OrderLevel,
		st.SaleWeightSeason,
		st.SaleWeightBank,
		st.SaleWeightPromo
	from
		stkmas st
	inner join
		fnCsvToTable(@skuNumbers) skus on skus.String = st.SKUN

END
GO

