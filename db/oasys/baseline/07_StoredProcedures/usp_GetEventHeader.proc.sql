﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEventHeader]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEventHeader'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEventHeader] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEventHeader'
GO
-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 06/10/2011
-- TFSID        : 2733 
-- Description  : New Stored Procedure for Coupons ProcessTransmissions
-- =====================================================================

-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================

ALTER PROCEDURE usp_GetEventHeader 
@Number AS CHAR(6) 
AS 
  BEGIN 
      SELECT numb  AS Number, 
             descr AS [Description], 
             prio  AS Priority, 
             sdat  AS StartDate, 
             stim  AS StartTime, 
             edat  AS EndDate, 
             etim  AS EndTime, 
             dact1 AS ActiveDays1, 
             dact2 AS ActiveDays2, 
             dact3 AS ActiveDays3, 
             dact4 AS ActiveDays4, 
             dact5 AS ActiveDays5, 
             dact6 AS ActiveDays6, 
             dact7 AS ActiveDays7, 
             idel  AS IsDeleted 
      FROM   EVTHDR 
      WHERE  numb = @Number 
  END
GO

