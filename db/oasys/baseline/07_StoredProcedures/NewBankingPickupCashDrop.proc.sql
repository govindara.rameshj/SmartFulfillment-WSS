﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingPickupCashDrop]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingPickupCashDrop'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingPickupCashDrop] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingPickupCashDrop'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 26/10/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 8866
-- Description   : Alter stored procedure NewBankingPickupCashDrop
--				 : to include Comments field in returned data.
-- =============================================
ALTER PROCedure [dbo].[NewBankingPickupCashDrop]
   @PeriodID Int,
   @CashierID Int
As
Begin
	Set NoCount On
	Select
		PickupID         = a.ID,
		PickupPeriodID   = a.PickupPeriodID,
		PickupDate       = b.StartDate,
		PickupSealNumber = a.SealNumber,
		PickupValue      = a.Value,
		PickupComment	 = a.Comments
	From 
		SafeBags a
			Inner Join 
				SystemPeriods b
			On 
				b.ID = a.PickupPeriodID
	Where 
		a.[Type]              = 'P'
	And   
		a.[State]            <> 'C'
	And   
		a.PickupPeriodID      = @PeriodID
	And   
		a.AccountabilityID    = @CashierID 
	And   
		IsNull(a.CashDrop, 0) = 1
	Order By 
		a.ID Desc
End
GO

