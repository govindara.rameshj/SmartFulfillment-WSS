﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSecondSupervisorCheck]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSecondSupervisorCheck'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSecondSupervisorCheck] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSecondSupervisorCheck'
GO
ALTER PROCedure NewBankingSecondSupervisorCheck
   @UserID   int,
   @UserName varchar(50) output,
   @Password char(5) output,
   @Manager  bit output
as
begin
set nocount on
select @UserName = Name,
       @Password = SupervisorPassword,
       @Manager  = IsManager
from SystemUsers
where ID        = @UserID
and   ID       <= (select cast(HCAS as integer) from RETOPT)    --filter out non operational users
and   IsDeleted = 0
and  (IsSupervisor = 1 or IsManager = 1)
end
GO

