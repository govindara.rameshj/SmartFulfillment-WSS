﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGetReportGrouping]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReportGetReportGrouping'
	EXEC ('CREATE PROCEDURE [dbo].[ReportGetReportGrouping] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReportGetReportGrouping'
GO
ALTER PROCEDURE [dbo].[ReportGetReportGrouping]
@ReportId INT, @TableId INT
AS
BEGIN
	SET NOCOUNT ON;

	select
		rg.ReportId,
		rg.TableId,
		rc.Id,
		rc.Name,
		rg.IsDescending,
		rg.SummaryType,
		rg.ShowExpanded
	from
		ReportColumn rc
	inner join
		ReportGrouping rg on rg.ColumnId = rc.Id 
	where 
		rg.ReportId		= @ReportId
		and rg.TableId	= @TableId
	order by
		rg.Sequence

END
GO

