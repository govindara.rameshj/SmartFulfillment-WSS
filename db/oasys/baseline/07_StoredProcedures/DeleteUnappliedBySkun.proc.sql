﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeleteUnappliedBySkun]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DeleteUnappliedBySkun'
	EXEC ('CREATE PROCEDURE [dbo].[DeleteUnappliedBySkun] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DeleteUnappliedBySkun'
GO
ALTER PROCEDURE [dbo].[DeleteUnappliedBySkun]
@SkuNumber CHAR (6)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM PRCCHG where SKUN=@SkuNumber AND PSTA='U' 
END
GO

