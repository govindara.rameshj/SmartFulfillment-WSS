﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AdjustsSelectWriteOffs]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure AdjustsSelectWriteOffs'
	EXEC ('CREATE PROCEDURE [dbo].[AdjustsSelectWriteOffs] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure AdjustsSelectWriteOffs'
GO
ALTER PROCEDURE [dbo].[AdjustsSelectWriteOffs]
AS

select		STKADJ.PeriodID,
			STKADJ.DATE1,			
			STKADJ.CODE,
			STKADJ.SKUN,
			STKMAS.DESCR as SkuDescription,		
			STKADJ.SEQN,
			STKADJ.AmendId,			
			STKADJ.SSTK,
			STKADJ.QUAN,			
			SACODE.SECL as CodeSecurity,
			STKADJ.MOWT,
			STKADJ.WAUT,
			STKADJ.DAUT,
			STKADJ.TSKU,
			STKADJ.TransferStart,
			STKADJ.TransferPrice,
			STKADJ.[INIT],
			STKADJ.PRIC,
			STKADJ.COST,
			STKADJ.INFO,
			STKADJ.[TYPE],
			STKADJ.DRLN,
			STKADJ.IsReversed,
			STKADJ.RTI

FROM		STKADJ
inner join	STKMAS on STKADJ.SKUN = STKMAS.SKUN
inner join	SACODE on STKADJ.CODE = SACODE.NUMB
			
WHERE		STKADJ.MOWT='W'
and			STKADJ.IsReversed=0
and			STKADJ.AmendId=0
and			STKADJ.WAUT=0

ORDER BY	STKADJ.DATE1 DESC, STKADJ.SKUN, STKADJ.SEQN, STKADJ.AmendId;

select		STKADJ.PeriodID,
			STKADJ.DATE1,			
			STKADJ.CODE,
			STKADJ.SKUN,
			STKMAS.DESCR as SkuDescription,		
			STKADJ.SEQN,
			STKADJ.AmendId,			
			STKADJ.SSTK,
			STKADJ.QUAN,				
			SACODE.SECL as CodeSecurity,
			STKADJ.MOWT,
			STKADJ.WAUT,
			STKADJ.DAUT,
			STKADJ.TSKU,
			STKADJ.TransferStart,
			STKADJ.TransferPrice,
			STKADJ.[INIT],
			STKADJ.PRIC,
			STKADJ.COST,
			STKADJ.INFO,
			STKADJ.[TYPE],
			STKADJ.DRLN,
			STKADJ.IsReversed,
			STKADJ.RTI

FROM		STKADJ
inner join	STKMAS on STKADJ.SKUN = STKMAS.SKUN
inner join	SACODE on STKADJ.CODE = SACODE.NUMB
			
WHERE		STKADJ.MOWT='W'
and			STKADJ.IsReversed=0
and			STKADJ.AmendId<>0
and			STKADJ.WAUT=0

ORDER BY	STKADJ.DATE1 DESC, STKADJ.SKUN, STKADJ.SEQN, STKADJ.AmendId;
GO

