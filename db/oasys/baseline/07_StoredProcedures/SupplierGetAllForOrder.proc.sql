﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierGetAllForOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierGetAllForOrder'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierGetAllForOrder] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierGetAllForOrder'
GO
ALTER PROCEDURE [dbo].[SupplierGetAllForOrder]
@Type CHAR (1)='A'
AS
begin
	DECLARE @DayWeek	INT;
	SET		@DayWeek	= DATEPART(weekday, getdate());

	select distinct
		sm.SUPN			as 'Number', 
		rtrim(sm.NAME)	as 'Name', 
		sm.alph			as 'Alpha',
		sm.ODNO			as 'OrderDepot',
		sm.rdno			as 'ReturnDepot', 
		sm.DLPO			as 'DateLastOrdered',
		sd.OCSD			as 'DateOrderCloseStart',
		sd.OCED			as 'DateOrderCloseEnd',
		sm.QCTL			as 'SoqNumber', 
		sm.QFLG			as 'SoqOrdered', 
		sm.SOQDate		as 'SoqDate',
		sm.PalletCheck	as 'PalletCheck',
		sm.delc			as 'IsDeleted',
		sd.TNET			as 'IsTradanet', 
		rtrim(sd.BBCC)	as 'BbcNumber',
		sd.depo			as 'DepotNumber', 
		sd.NOTE			as 'DepotNotes',
		sd.PHO1			as 'PhoneNumber1',
		sd.ReviewDay0, 
		sd.ReviewDay1, 
		sd.ReviewDay2, 
		sd.ReviewDay3, 
		sd.ReviewDay4, 
		sd.ReviewDay5, 
		sd.ReviewDay6, 
		sd.VLED1		as 'LeadTime1', 
		sd.VLED2		as 'LeadTime2', 
		sd.VLED3		as 'LeadTime3', 
		sd.VLED4		as 'LeadTime4',
		sd.VLED5		as 'LeadTime5', 
		sd.VLED6		as 'LeadTime6',
		sd.VLED7		as 'LeadTime0', 
		sd.LEAD			as 'LeadTimeFixed', 
		rtrim(sd.MCPT)	as 'MinOrderType', 
		sd.MCPV			as 'MinOrderValue', 
		sd.MCPC			as 'MinOrderCartons', 
		sd.MCPW			as 'MinOrderWeight'

	from
		SUPMAS sm
	inner join
		SUPDET sd		ON sd.SUPN = sm.SUPN AND sd.DEPO = sm.ODNO 
	inner join
		STKMAS sk		on sk.SUP1 = sm.SUPN
	WHERE				
		sd.TYPE = 'O'
		--and (select count(skun)from stkmas where sup1=sm.supn and IDEL=0 and IOBS=0 and IRIB=0 and INON=0 and NOOR=0) > 0
				
		AND	sk.IDEL = 0 
		AND	sk.IOBS = 0
		AND	sk.IRIS = 0
		AND	sk.NOOR = 0
			
		AND		(@Type='A' 
				OR (@Type='D' AND sd.BBCC = '') 
				OR (@Type='W' AND sd.BBCC <>''))
		
		AND	(sd.OCSD IS NULL OR sd.OCSD > GETDATE()
			OR (sd.OCED IS NOT NULL AND sd.OCED < GETDATE()))			
	order by 
		rtrim(sm.NAME)
		
end
GO

