﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingFloatPrepareUnAssign]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingFloatPrepareUnAssign'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingFloatPrepareUnAssign] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingFloatPrepareUnAssign'
GO
ALTER PROCedure NewBankingFloatPrepareUnAssign
   @FloatBagID int
as
begin
set nocount on

update SafeBags set OutPeriodID = 0,
                    OutDate     = null,
                    OutUserID1  = 0,
                    OutUserID2  = 0
where ID = @FloatBagID
end
GO

