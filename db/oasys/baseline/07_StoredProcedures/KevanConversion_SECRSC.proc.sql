﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SECRSC]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SECRSC'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SECRSC] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SECRSC'
GO
ALTER PROCEDURE dbo.KevanConversion_SECRSC
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 30th June 2011
-- 
-- Task     : 40 - Apply Security Access Permissions for Support RSC
-----------------------------------------------------------------------------------
AS
BEGIN
        SET NOCOUNT ON

	IF EXISTS (SELECT * FROM sys.server_principals WHERE name = 'TPPLC\WX_RemoteStockCheck_SQL')
		Begin	
			Print ('User TPPLC\WX_RemoteStockCheck_SQL exists in this SQL Server, skipping creation..')
		End	
	ELSE	
		Begin
			Print ('Creating user TPPLC\WX_RemoteStockCheck_SQL..')
        		declare @Domain char(5)
        		declare @TpOrg char(3)
        		declare @TpDepartment char(16)
        		declare @TpGroup char(4)
        		declare @Name char (23)
        
        		DECLARE @SQL NVARCHAR(4000);
        		DECLARE @SQL2 NVARCHAR(4000);
        		DECLARE @SQLEXEC1 NVARCHAR(4000);
        		DECLARE @SQLPERM1 NVARCHAR(4000);
        		DECLARE @SQLPERM2 NVARCHAR(4000);
        
        		SET @Domain = 'TPPLC';
        		SET @TpOrg = 'WX_'; 
        		SET @TpDepartment = 'RemoteStockCheck';
        		SET @TpGroup = '_SQL';
        		SET @Name = (@TpOrg + @TpDepartment + @TpGroup);
        
        		--Use [Master]
        		SET @SQL = 'CREATE LOGIN ['+ @Domain +'\'+ @Name +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]';
        		EXECUTE(@SQL);        

        		--Use [Oasys]
        		SET @SQL2 = 'CREATE USER [' + @Domain +'\'+ @Name + '] FOR LOGIN ['+ @Domain +'\'+ @Name + ']';
        		EXECUTE(@SQL2);
        
        		-- Set Server Roles for User Login
        		SET @SQLEXEC1 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = [public]';
        		EXECUTE(@SQLEXEC1);
        
        		-- Set Permissions for User Login
        		SET @SQLPERM1 = 'GRANT EXECUTE ON [dbo].[usp_RemoteStockCheckAdv] TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION';
        		EXECUTE(@SQLPERM1); 
			SET @SQLPERM2 = 'GRANT EXECUTE ON [dbo].[usp_RemoteStockCheck] TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION';
        		EXECUTE(@SQLPERM2);
		End 

END;
GO

