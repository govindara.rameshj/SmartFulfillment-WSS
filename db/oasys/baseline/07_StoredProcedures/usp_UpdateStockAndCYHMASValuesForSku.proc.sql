﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateStockAndCYHMASValuesForSku]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdateStockAndCYHMASValuesForSku'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdateStockAndCYHMASValuesForSku] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdateStockAndCYHMASValuesForSku'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 09/06/2011
-- Description:	Update the Stock and CYHMAS values for Stock Adjustments calculations.
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateStockAndCYHMASValuesForSku] 
	-- Add the parameters for the stored procedure here
	@ProductCode Char(6),
	@StockOnHand int,
	@SaleTypeAttribute Char(1),
	@DummyItem bit,
	@HODeleted bit,
	@AllowAdjustments char(1),
	@WriteOffStock int,
	@Price as decimal(10,2),
	@Cost as decimal(10,2),
	@AdjustmentQty1 int,
	@AdjustmentValue1 decimal(10,2),
	@MarkdownQty int,
	@OpenReturnQty int,
	@CyclicalCount decimal,
	@ToadyActivityFlag bit,
	@ActivityDate date = NULL,
	@ActivityQty decimal,
	@ActivityValue decimal(10,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @rowCount int;
	set		@rowCount=0;


	UPDATE  
		STKMAS 
	SET
		ONHA	= @StockOnHand,
		SALT	= @SaleTypeAttribute,
		ICAT	= @DummyItem,
		IDEL	= @HODeleted,
		AADJ	= @AllowAdjustments,
		WTFQ	= @WriteOffStock,
		PRIC	= @Price,
		COST	= @Cost,
		MADQ1	= @AdjustmentQty1,
		MADV1	= @AdjustmentValue1,
		MDNQ	= @MarkdownQty,
		MCCV1	= @CyclicalCount,
		TACT	= @ToadyActivityFlag,
		RETQ	= @OpenReturnQty

	WHERE
	  SKUN = @ProductCode

	set @rowCount = @@ROWCOUNT

	UPDATE
		CYHMAS
	SET
		ADAT1	= @ActivityDate,
		AQ021	= @ActivityQty,
		AV021	= @ActivityValue
	WHERE
	  SKUN = @ProductCode

	Return @rowCount	

END
GO

