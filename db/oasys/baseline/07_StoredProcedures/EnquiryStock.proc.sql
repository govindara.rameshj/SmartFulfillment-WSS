﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryStock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryStock'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryStock] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryStock'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 23/07/2012
-- User Story	 : 6125
-- Project		 : P022-003 - Stock Enquiry Details - change the background colours of
--				 : specific areas based on data conditions.
-- Task Id		 : 6338
-- Description   : Add new fields called HighlightThis and EmboldenThis and use for 
--				 : the OnHand value; setting both fields to true if OnHand <= 0 and False
--               : otherwise.  Add new field called ResizeThis and set to true for the
--				 : OnHand field.  Apply the HighlightThis and EmboldenThis fields to the
--				 : Events hyperlink if there are active events to be seen.
-- _____________________________________________
-- User Story	 : 6131
-- Project		 : P022-009 - Stock Enquiry Details - amend the design of what is displayed.
-- Task Id		 : 6344
-- Description   : Move fields and links to and from other reports.
--				 :
--				 : Remove the following fields and hyperlinks and move them to different
--				 : reports.
--				 :	To Move					Move To
--				 : Display (IDEA)			N/A
--				 : Setup Date (IODT)		Stock Management (new)
--				 : First Stocked (DATS)		Stock Management (new)
--				 : Movement Details (hl)	Stock Management (new)
--				 : Movement Weekly (hl)		Stock Management (new)
--				 : PIC (hl)					Stock Management (new)
--				 : Shrinkage (hl)			Stock Management (new)
--				 : Events (hl)				Pricing
--				 :
--				 : Add the following fields and hyperlinks move from other reports
--				 : reports.
--				 :	To Add					Move From
--				 : Pack Size (PACK)			Order Details
--				 : PO History (hl)			Order Details
-- =============================================
ALTER PROCedure [dbo].[EnquiryStock]
	@SkuNumber	Char(6)
As
Begin
	Set NOCOUNT On;

	Declare 
		@Table Table
			(
				RowId Int,
				SkuNumber Char(6),
				SupplierNumber Char(5),
				HighlightThis Bit Default 0,
				EmboldenThis Bit Default 0,
				ResizeThis Bit Default 0,
				Description Varchar(50),
				Display Varchar(50)
			);
	
	Declare
		@onhand		Int,
		@markdown	Int,
		@writeoff	Int,
		@Supplier	Char(5),
		@packSize		int,
		@OnHandZeroOrLess Bit = 0;

	Select
		@onhand		= ONHA,
		@markdown	= MDNQ,
		@writeoff	= WTFQ,
		@Supplier	= SUP1,
		@packSize	= PACK,
		@OnHandZeroOrLess = 
			(
				Select Case
					When ONHA <= 0 Then 1 
					Else 0
				End
			)
	From
		STKMAS
	Where
		SKUN = @SkuNumber


	Insert Into 
		@Table
			(
				RowId,
				Description, 
				Display, 
				HighlightThis,
				EmboldenThis,
				ResizeThis
			) 
		Values 
			(
				4,
				'On Hand', 
				@onhand, 
				@OnHandZeroOrLess,
				1,
				1
			);
	Insert Into @Table(Description, Display) Values ('Markdown', @markdown);
	Insert Into @Table(Description, Display) Values ('Write Off', @writeoff);
	Insert Into @Table(Description, Display) Values ('','');
	Insert Into @Table(RowId, SkuNumber, Description) Values (1, @SkuNumber, 'General Info');
	Insert Into @Table(RowId, SupplierNumber, Description) Values (2, @Supplier, 'Returns Policy');
	Insert Into @Table(RowId, SkuNumber, Description) Values (5, @SkuNumber, 'DRL Listing');
	Insert Into @Table(Description, Display) Values ('','');
	Insert Into @Table(Description, Display) Values ('Pack Size', @packSize);
	Insert Into @Table(Description, Display) Values ('','');
	Insert Into @Table(RowId, SkuNumber, Description) Values (3, @SkuNumber, 'PO History');
	
	Select * From @Table;
End
GO

