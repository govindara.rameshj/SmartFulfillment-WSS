﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemPeriodSafeGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemPeriodSafeGet'
	EXEC ('CREATE PROCEDURE [dbo].[SystemPeriodSafeGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemPeriodSafeGet'
GO
-- =============================================
-- Author      : Partha Dutta
-- Create Date : 07/09/2010
-- Referral No : 373
-- Notes       : Banking -> Entry Of Banking
-- =============================================

--changed sort order to earliest date ascending

ALTER PROCEDURE [dbo].[SystemPeriodSafeGet]
   @StartDate DATE = null,
   @EndDate   DATE = null,
   @IsClosed  BIT
AS
begin
	exec SystemPeriodAutoPopulate;
	
	select 
		sp.ID,
		sp.StartDate,
		sp.EndDate,
		sp.IsClosed
	from 
		SystemPeriods sp
	inner join
		[Safe] s on sp.ID=s.PeriodID
	where 
		(@StartDate is null or (@StartDate is not null and sp.StartDate >= @StartDate )) and
		(@EndDate is null or (@EndDate is not null and sp.EndDate <= @EndDate)) and
		s.IsClosed=@IsClosed
	order by
		--id desc
		sp.StartDate asc
end
GO

