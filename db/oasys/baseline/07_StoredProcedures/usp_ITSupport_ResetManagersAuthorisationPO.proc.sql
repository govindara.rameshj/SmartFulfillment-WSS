﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_ResetManagersAuthorisationPO]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_ResetManagersAuthorisationPO'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_ResetManagersAuthorisationPO] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_ResetManagersAuthorisationPO'
GO
ALTER PROCEDURE dbo.usp_ITSupport_ResetManagersAuthorisationPO
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 7th March 2013
-- Description	: Reset Hiearchy Styles to Not Require Manager Authorisation on Till Price Override
-----------------------------------------------------------------------------------
As
Begin
Set NoCount On

-------------------------------------------------------------------------------------------------
-- Reset Hierarchy Maximum Override Value for Styles
-------------------------------------------------------------------------------------------------
Update	Oasys.Dbo.HIEMAS 
Set		MaxOverride = NULL 
Where	NUMB in	(	
				Select Distinct STYL 
				From [OASYS].[dbo].[HIESTY] 
				)
				
If @@ERROR = 0
	Begin
		Print('Price Overrides requiring Managers Authorisation have been reset')
	End
Else
	Begin
		Print('Problem Encountered Resolving Price Overrides')
	End
End
GO

