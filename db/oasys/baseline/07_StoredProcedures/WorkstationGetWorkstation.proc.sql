﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WorkstationGetWorkstation]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure WorkstationGetWorkstation'
	EXEC ('CREATE PROCEDURE [dbo].[WorkstationGetWorkstation] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure WorkstationGetWorkstation'
GO
ALTER PROCEDURE [dbo].[WorkstationGetWorkstation]
	@Id int
AS
begin
	SET NOCOUNT ON;

	select 
		*
	from
		WorkStationConfig
	where
		ID = @Id
	
end
GO

