﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODDeliveryIncomeSummary]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure QODDeliveryIncomeSummary'
	EXEC ('CREATE PROCEDURE [dbo].[QODDeliveryIncomeSummary] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure QODDeliveryIncomeSummary'
GO
-- =============================================
-- Author:		Mike O'Cain
-- Create date: 07/10/2009
-- Description:	Delivery Income Summary
-- =============================================
ALTER PROCEDURE [dbo].[QODDeliveryIncomeSummary] 
	-- Add the parameters for the stored procedure here
	@DateStart datetime = Null, 
	@DateEnd datetime = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT DELD					as 'Date'
	  , DATENAME(dw,DELD)	as 'Day'
	  , COUNT(DELD)			as 'QtyTotal'
	  , SUM(DCST)			as 'Income'
	  , SUM(WGHT)			as 'Weight'
	  , SUM(VOLU)			as 'Volume'
FROM vwQODTables 
WHERE DELD >= @DateStart AND DELD <= @DateEnd  And DELI=1
GROUP BY DELD

END
GO

