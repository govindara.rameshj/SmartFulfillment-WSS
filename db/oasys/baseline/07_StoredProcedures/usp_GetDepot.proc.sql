﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetDepot]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetDepot'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetDepot] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetDepot'
GO
ALTER PROCEDURE [dbo].[usp_GetDepot] (
	@DepotID INT = NULL
	)

--TODO: Remove @DepotID input Parameter

AS

BEGIN
	SET NOCOUNT ON;

	--DECLARE @DepotID INT


	/*All stores are numbered (as per "Store" table), but the format of this number (aka Depot ID) seems to differ throughout OASYS. 
	Route Optimisation simply reqires a 4 character store code, prefixed with an 8*/
	SET @DepotID = (SELECT TOP 1 '8'+ RIGHT('000'+ STOR, 3) FROM RETOPT WITH (NOLOCK))

	SELECT  @DepotID AS Id, 
	Name, 
	Address1, 
	Address3, 
	Address2, 
	Address4, 
	Address5, 
	PostCode, PhoneNumber, FaxNumber, Manager
	FROM Store WITH (NOLOCK)
	WHERE ID=(select top 1 Stor from retopt)
	ORDER BY 1 desc

	SET NOCOUNT OFF;
END
GO

