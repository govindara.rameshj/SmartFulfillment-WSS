﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetCollections]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetCollections'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetCollections] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetCollections'
GO
ALTER PROCEDURE usp_GetCollections @DeliveryDate date
AS
BEGIN

    SELECT vh.collectionId , 
           vh.Description , 
           vh.TotalWeight , 
           vh.Name , 
           vh.CustomerAddress1 , 
           vh.CustomerAddress2 , 
           vh.CustomerAddress3 , 
           vh.PostCode AS CustomerPostcode , 
           vh.VehicleTypeIndex as VehicleSelected, 
           vl.ID AS CollectionLineId , 
           vl.Description AS CollectionDescription , 
           vl.Weight AS CollectionLineWeight , 
           vl.Quantity AS CollectionLineQuantity , 
           vl.TotalWeight AS CollectionLineTotalWeight,
           vh.LastUpdateDateTime As CollectionLastUpdateDateTime
      FROM
           VehicleCollectionHeader vh INNER JOIN VehicleCollectionLine vl
           ON vh.CollectionId
              = 
              vl.CollectionId
      WHERE vh.DeliveryDate
            = 
            @DeliveryDate;
END
GO

