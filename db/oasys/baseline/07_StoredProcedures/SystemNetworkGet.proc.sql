﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemNetworkGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemNetworkGet'
	EXEC ('CREATE PROCEDURE [dbo].[SystemNetworkGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemNetworkGet'
GO
ALTER PROCEDURE [dbo].[SystemNetworkGet]

AS
BEGIN
	SET NOCOUNT ON;

	select MAST from SYSNID where FKEY='01';
	
END
GO

