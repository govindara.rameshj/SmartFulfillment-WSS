﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockMarkdownQuantity]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockMarkdownQuantity'
	EXEC ('CREATE PROCEDURE [dbo].[StockMarkdownQuantity] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockMarkdownQuantity'
GO
ALTER PROCEDURE [dbo].[StockMarkdownQuantity]
@Quantity INT OUTPUT
AS
begin
	SELECT 
		@Quantity	= SUM(S.MDNQ) 
	FROM 
		STKMAS S
	WHERE	
		S.ONHA		> 0
		AND	S.IOBS	= 0
		AND	S.INON	= 0;
	
	SELECT 
		@Quantity = coalesce(@Quantity, 0);

end
GO

