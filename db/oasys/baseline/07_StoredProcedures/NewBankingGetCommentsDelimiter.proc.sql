﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingGetCommentsDelimiter]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingGetCommentsDelimiter'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingGetCommentsDelimiter] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingGetCommentsDelimiter'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 06/11/2012
-- User Story	 : 9019: Iteration 18 Rework for pilot
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 9020
-- Description   : Create stored procedure to use same udf used
--				 : in stored procedure NewBankingFloatedCashierDisplay
--				 : to get the Comments Delimiter.
-- =============================================
ALTER PROCedure [dbo].[NewBankingGetCommentsDelimiter]
As
Begin
	Set NoCount On
	
	Declare @Return As Table
		(
			CommentsDelimiter VarChar(80)
		)

	Insert Into
		@Return
	Values
		(
			[dbo].[udf_GetCommentsDelimiter]()
		)

	Select
		CommentsDelimiter
	From
		@Return
End
GO

