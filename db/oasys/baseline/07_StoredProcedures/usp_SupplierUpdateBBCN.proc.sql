﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SupplierUpdateBBCN]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SupplierUpdateBBCN'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SupplierUpdateBBCN] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SupplierUpdateBBCN'
GO
ALTER PROCedure usp_SupplierUpdateBBCN

   @SupplierNumber char(5),
   @BBCN           char(3)

as
begin
   set nocount on

   update SUPMAS set BBCN = @BBCN where SUPN = @SupplierNumber

end
GO

