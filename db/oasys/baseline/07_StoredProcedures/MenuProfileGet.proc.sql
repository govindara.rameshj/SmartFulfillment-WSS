﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MenuProfileGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure MenuProfileGet'
	EXEC ('CREATE PROCEDURE [dbo].[MenuProfileGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure MenuProfileGet'
GO
ALTER PROCEDURE [dbo].[MenuProfileGet]
@ProfileId INT=null
AS
BEGIN
	SET NOCOUNT ON;

	select
		MenuConfigID		as MenuId,
		ID					as ProfileId,
		SecurityLevel,
		AccessAllowed,
		Parameters
	from 
		ProfilemenuAccess
	where
		(@ProfileId is null) or (@ProfileId is not null and ID=@ProfileId)

END
GO

