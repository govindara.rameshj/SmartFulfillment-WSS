﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_InsertStockLogForSku]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_InsertStockLogForSku'
	EXEC ('CREATE PROCEDURE [dbo].[usp_InsertStockLogForSku] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_InsertStockLogForSku'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 10/06/2011
-- Description:	Insert New Stock Log record for Sku
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertStockLogForSku] 
	-- Add the parameters for the stored procedure here
	@SkuNumber Char(6),
	@DayNumber dec,
	@Type char(2),
	@AdjustmentDate date,
	@AdjustmentTime char(6),
	@Keys char(30),
	@UserId char(3),
	@StartingStock decimal,
	@EndingStock decimal,
	@StartingReturns decimal,
	@EndingReturns decimal,
	@StartingMarkdown decimal,
	@EndingMarkdown decimal,
	@StartingWriteoff decimal,
	@EndingWriteoff decimal,
	@StartingPrice decimal(10,2),
	@EndingPrice decimal(10,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    -- Insert statements for procedure here
	INSERT INTO STKLOG
		(SKUN,
		DAYN,
		[TYPE],
		DATE1,
		[TIME],
		KEYS,
		EEID,
		ICOM,
		SSTK,
		ESTK,
		SRET,
		ERET,
		SMDN,
		EMDN,
		SWTF,
		EWTF,
		SPRI,
		EPRI,
		RTI)
		
	VALUES
	 (	@SkuNumber,
		@DayNumber,
		@Type,
		@AdjustmentDate,
		@AdjustmentTime,
		@Keys,
		@UserId,
		0,
		@StartingStock,
		@EndingStock,
		@StartingReturns,
		@EndingReturns,
		@StartingMarkdown,
		@EndingMarkdown,
		@StartingWriteoff,
		@EndingWriteoff,
		@StartingPrice,
		@EndingPrice,
		'S'
	)

	return @@RowCount	
	
END
GO

