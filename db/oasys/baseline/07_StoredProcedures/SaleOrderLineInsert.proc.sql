﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderLineInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderLineInsert'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderLineInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderLineInsert'
GO
ALTER PROCEDURE [dbo].[SaleOrderLineInsert]
@OrderNumber CHAR (6), @Number CHAR (4), @SkuNumber CHAR (6), @QtyOrdered DECIMAL (5)=0, @QtyTaken DECIMAL (5)=0, @QtyRefunded DECIMAL (5)=0, @Price DECIMAL (7, 2)=0, @IsDeliveryChargeItem BIT=0, @Weight DECIMAL (7, 2)=0, @Volume DECIMAL (7, 2)=0, @PriceOverrideCode INT=0, @DeliveryStatus INT=0, @SellingStoreId INT=0, @SellingStoreOrderId INT=0, @QtyToDeliver INT=0, @DeliverySource CHAR (4)=null, @DeliverySourceIbtOut CHAR (6)=null, @SellingStoreIbtIn CHAR (6)=null, @SourceOrderLineNo INT, @QtyScanned INT, @RequiredFulfiller INT = null
AS
BEGIN
    SET NOCOUNT ON;
    
    --insert order line
    insert into CORLIN 
        (
        NUMB,
        LINE,
        SKUN,
        QTYO,
        QTYT,
        QTYR,
        Price,
        IsDeliveryChargeItem,
        WGHT,
        VOLU,
        PORC,
        DeliveryStatus,
        SellingStoreId,
        SellingStoreOrderId,
        QtyToBeDelivered,
        DeliverySource,
        DeliverySourceIbtOut,
        SellingStoreIbtIn,
        -- added for hubs 2.0
        QuantityScanned,
        RequiredFulfiller
        )
    values      (
        @orderNumber,
        @number,
        @skuNumber,
        @qtyOrdered,
        @qtyTaken,
        @qtyRefunded,
        @price,
        @isDeliveryChargeItem,
        @weight,
        @volume,
        @priceOverrideCode,
        @deliveryStatus,
        @sellingStoreId,
        @sellingStoreOrderId,
        @QtyToDeliver,
        @deliverySource,
        @deliverySourceIbtOut,
        @sellingStoreIbtIn,
        @QtyScanned,
        @RequiredFulfiller
    )
    -- added for hubs 2.0
    INSERT INTO CORLIN2
    (
    NUMB,
    LINE,
    SourceOrderLineNo
    )
    VALUES
    (
     @orderNumber,
     @Number,
     @SourceOrderLineNo
    )
    
    return @@rowcount
END
GO

