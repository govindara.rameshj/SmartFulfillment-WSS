﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashActivity]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashActivity'
	EXEC ('CREATE PROCEDURE [dbo].[DashActivity] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashActivity'
GO
-- Verion Control Information
-- =============================================
-- Author:  	Kevan Madelin
-- Create date: 23/08/2010
-- Version:	3.0.0.0
-- Description: This procedure is used to get the stock data for the Managers Dashboard.
-- ************** MODIFICATIONS ****************
-- Author:  	Kevan Madelin
-- Create date: 21/09/2010
-- Version:	3.0.0.1
-- Description: Typo in Misc Out WTD Insert clause
-- Description: Added WTD Price Change Confirmed count
-- =============================================
ALTER PROCEDURE [dbo].[DashActivity]
	@DateEnd	date
as
begin
set nocount on

	declare @table table(
		Description	varchar(50),
		Qty			int,
		QtyWtd		int,
		Value		dec(9,2),
		ValueWtd	dec(9,2));
	declare
		@StartDate				date,
		@ColleagueDayQty		int,
		@ColleagueDayValue		dec(9,2),
		@ColleagueWeekQty		int,
		@ColleagueWeekValue		dec(9,2),
		@CreditDayQty			int,
		@CreditDayValue			dec(9,2),
		@CreditWeekQty			int,
		@CreditWeekValue		dec(9,2),
		@DuressDayQty			int,
		@DuressWeekQty			int,
		@GiftAllocateDayQty		int,
		@GiftAllocateDayValue	dec(9,2),
		@GiftAllocateWeekQty	int,
		@GiftAllocateWeekValue	dec(9,2),
		@GiftRedemDayQty		int,
		@GiftRedemDayValue		dec(9,2),
		@GiftRedemWeekQty		int,
		@GiftRedemWeekValue		dec(9,2),
		@MiscInDayQty			int,
		@MiscInDayValue			dec(9,2),
		@MiscInWeekQty			int,
		@MiscInWeekValue		dec(9,2),
		@MiscOutDayQty			int,
		@MiscOutDayValue		dec(9,2),
		@MiscOutWeekQty			int,
		@MiscOutWeekValue		dec(9,2),
		@PendingDayQty			int,
		@PendingDayValue		dec(9,2),
		@ConfirmDayQty			int,
		@ConfirmWeekQty			int,
		@ConsignedDayQty		int,
		@ConsignedWeekQty		int,
		@RejectedDayQty			int,
		@RejectedDayValue		dec(9,2),
		@RejectedWeekQty		int,
		@RejectedWeekValue		dec(9,2);
	
	--set startdate to sunday before enddate
	set	@StartDate = @DateEnd;
	While datepart(weekday, @StartDate) <> 1
	begin
		set @StartDate = dateadd(day, -1, @StartDate)
	end	
	
	--get colleague discount/duress/misc in&out code today info
	select
		@ColleagueDayQty	= count(case when dt.iemp=1 then dt.totl end),
	 	@ColleagueDayValue	= sum(case when dt.iemp=1 then dt.totl else 0 end),
	 	@DuressDayQty		= count(case when dt.[open]=9 then dt.totl end),
	 	@MiscInDayQty		= count(case when dt.tcod in ('M+','C+') then dt.totl end),
	 	@MiscInDayValue		= sum(case when dt.tcod	in ('M+','C+') then dt.totl else 0 end),
	 	@MiscOutDayQty		= count(case when dt.tcod in ('M-','C-') then dt.totl end),
	 	@MiscOutDayValue	= sum(case when dt.tcod	in ('M-','C-') then dt.totl else 0 end)
	from	
		dltots dt
	where	
		dt.DATE1	= convert(date, @DateEnd)
	and	dt.CASH		<> '000'
	and	dt.VOID		= 0
	and	dt.PARK		= 0
	and	dt.TMOD		= 0;
	
	--get colleague discount/duress/misc in&out week to date sales info
	select
		@ColleagueWeekQty	= count(case when dt.iemp=1 then dt.totl end),
	 	@ColleagueWeekValue	= sum(case when dt.iemp=1 then dt.totl else 0 end),
	 	@DuressWeekQty		= count(case when dt.[open]=9 then dt.totl end),
	 	@MiscInWeekQty		= count(case when dt.tcod in ('M+','C+') then dt.totl end),
	 	@MiscInWeekValue	= sum(case when dt.tcod	in ('M+','C+') then dt.totl else 0 end),
	 	@MiscOutWeekQty		= count(case when dt.tcod in ('M-','C-') then dt.totl end),
	 	@MiscOutWeekValue	= sum(case when dt.tcod	in ('M-','C-') then dt.totl else 0 end)
	from	
		dltots dt
	where	
		dt.DATE1	<= @DateEnd
	and dt.date1	>= @StartDate
	and	dt.CASH		<> '000'
	and	dt.VOID		= 0
	and	dt.PARK		= 0
	and	dt.TMOD		= 0;
	
	--get credit card summary today info
	select
		@CreditDayQty	= count(dp.amnt),	
	 	@CreditDayValue	= sum(dp.amnt)
	from	
		dlpaid dp
	inner join
		dltots dt	on	dt.date1 = dp.date1
					and dt.till	= dp.till
					and dt.[tran] = dp.[tran]
	where	
		dt.DATE1			= convert(date, @DateEnd)
	and	dt.CASH				<> '000'
	and	dt.VOID				= 0
	and	dt.PARK				= 0
	and	dt.TMOD				= 0
	and dt.iemp				= 1
	and dp.[CARD]			<>'0000000000000000000'
	and	len(rtrim(dp.ATYP)) <> 0
	and	dp.[TYPE]			IN (3,8);
	
	--get credit card summary week to date sales info
	select
		@CreditWeekQty		= count(dp.amnt),	
	 	@CreditWeekValue	= sum(dp.amnt)
	from	
		dlpaid dp
	inner join
		dltots dt	on	dt.date1 = dp.date1
					and dt.till	= dp.till
					and dt.[tran] = dp.[tran]
	where	
		dt.DATE1			<= @DateEnd
	and dt.date1			>= @StartDate
	and	dt.CASH				<> '000'
	and	dt.VOID				= 0
	and	dt.PARK				= 0
	and	dt.TMOD				= 0
	and dt.iemp				= 1
	and dp.[CARD]			<>'0000000000000000000'
	and	len(rtrim(dp.ATYP)) <> 0
	and	dp.[TYPE]			IN (3,8);

	--get gift token allocation/redemption today info
	select
		@GiftAllocateDayQty		= count(case when dl.[type]	IN ('SA','TR','CR','CC','VR','RC') then dl.amnt end),	
	 	@GiftAllocateDayValue	= sum(case when dl.[type]	IN ('SA','TR','CR','CC','VR','RC') then dl.amnt else 0 end),
		@GiftRedemDayQty		= count(case when dl.[type]	IN ('TS', 'RR', 'CS', 'VS') then dl.amnt end),	
	 	@GiftRedemDayValue		= sum(case when dl.[type]	IN ('TS', 'RR', 'CS', 'VS') then dl.amnt else 0 end)
	from	
		dlgift dl
	where	
		dl.DATE1		= convert(date, @DateEnd);

	--get gift token allocation/redemption week info
	select
		@GiftAllocateWeekQty	= count(case when dl.[type]	IN ('SA','TR','CR','CC','VR','RC') then dl.amnt end),	
	 	@GiftAllocateWeekValue	= sum(case when dl.[type]	IN ('SA','TR','CR','CC','VR','RC') then dl.amnt else 0 end),
		@GiftRedemWeekQty		= count(case when dl.[type]	IN ('TS', 'RR', 'CS', 'VS') then dl.amnt end),	
	 	@GiftRedemWeekValue		= sum(case when dl.[type]	IN ('TS', 'RR', 'CS', 'VS') then dl.amnt else 0 end)
	from	
		dlgift dl
	where	
		dl.DATE1		<= @DateEnd
		and dl.date1	>= @StartDate

	--get pending write offs
	select 
		@PendingDayQty		= abs(sum(QUAN)), 
		@PendingDayValue	= abs(SUM(STKADJ.QUAN * STKADJ.PRIC)) 
	from 
		STKADJ
	WHERE	
		STKADJ.MOWT		= 'W'
		AND	STKADJ.DAUT IS NULL;

	--get price changes confirmed
	select
		@ConfirmDayQty	= count(pc.skun)
	from
		prcchg pc
	inner join
		stkmas st	on	pc.skun = st.skun
	where
		st.inon	= 0
	and st.dprc	= @DateEnd
	
	--get price changes confirmed for week
	select
		@ConfirmWeekQty	= count(pc.skun)
	from
		prcchg pc
	inner join
		stkmas st	on	pc.skun = st.skun
	where
		st.inon	= 0
	and st.dprc	>= @DateEnd
	and st.DPRC <= @StartDate
	
	--get orders consigned not received today
	select 
		@ConsignedDayQty		= count(done)
	from
		conmas
	where 
		done	= 0;
		
	--get orders consigned not received week
	select
		@ConsignedWeekQty	= count(done)
	from
		conmas
	where
	    done	= 0;

	--get rejected orders today
	select 
		@RejectedDayQty		= count(valu),
		@RejectedDayValue	= sum(valu)
	from
		purhdr
	where
		odat		= convert(date, @DateEnd)
	and conf		= 'R';
	
	--get rejected orders week
	select
		@RejectedWeekQty	= count(valu),
		@RejectedWeekValue	= sum(valu)
	from
		purhdr
	where
		odat		<= @DateEnd
	and odat		>= @StartDate
	and conf		= 'R';
	
	--return values
	insert into @table values ('Colleague Discount', @ColleagueDayQty, @ColleagueWeekQty, @ColleagueDayValue, @ColleagueWeekValue) ;
	insert into @table values ('Credit Card Summary', @CreditDayQty, @CreditWeekQty, @CreditDayValue, @CreditWeekValue) ;
	insert into @table(Description, Qty, QtyWtd) values ('Duress Code Used', @DuressDayQty, @DuressWeekQty) ;
	insert into @table values ('Gift Token Allocations', @GiftAllocateDayQty, @GiftAllocateWeekQty, @GiftAllocateDayValue, @GiftAllocateWeekValue) ;
	insert into @table values ('Gift Token Redemptions', @GiftRedemDayQty, @GiftRedemWeekQty, @GiftRedemDayValue, @GiftRedemWeekValue) ;
	insert into @table values ('Miscellaneous Incomes', @MiscInDayQty, @MiscInWeekQty, @MiscInDayValue, @MiscInWeekValue) ;
	insert into @table values ('Miscellaneous Outgoings', @MiscOutDayQty, @MiscOutWeekQty, @MiscOutDayValue, @MiscOutWeekValue) ;
	insert into @table(Description, Qty, Value) values ('Pending Write Offs', @PendingDayQty, @PendingDayValue) ;
	insert into @table(Description, Qty, QtyWtd) values ('Price Change Confirmations', @ConfirmDayQty ,@ConfirmWeekQty) ;
	insert into @table(Description, Qty, QtyWtd) values ('Orders Consigned but not Received', @ConsignedDayQty, @ConsignedWeekQty) ;
	insert into @table values ('Rejected Orders', @RejectedDayQty, @RejectedWeekQty, @RejectedDayValue, @RejectedWeekValue) ;

	select * from @table

end
GO

