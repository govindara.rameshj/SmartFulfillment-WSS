﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEventMaster]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GetEventMaster'
	EXEC ('CREATE PROCEDURE [dbo].[GetEventMaster] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GetEventMaster'
GO
ALTER PROCEDURE [dbo].[GetEventMaster]
@Event CHAR (6)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Declare @ETYP  char(2);
set @ETYP = (select Top 1 en.ETYP FROM EVTHDR as eh INNER JOIN EVTENQ as en ON eh.NUMB=en.NUMB  where en.Numb=@Event);

if @ETYP = 'DM' Or @ETYP = 'DS'
	BEGIN
	-- Deal Group Query
		select Distinct 
			  RowId = 1,
			  @Event   as 'Item',	
			  ev.KEY2  as 'Event',
			  ed.Key1  as 'SKU',
			  st.Descr as 'Description',	
			  ev.BQTY  as 'Buy Qty',	
			  ev.PRIC  as 'Deal Price'
		FROM  EVTMAS as ev INNER JOIN
			  EVTDLG as ed ON ev.NUMB = ev.NUMB INNER JOIN
			  STKMAS as st ON ed.KEY1 = st.SKUN
		WHERE EV.NUMB = @Event
	END
else
	BEGIN
	-- Original Query
		select 
			   RowId = 1,	
			   @Event as 'Item',
			   ev.KEY2 as 'Event',
			   ev.Key1 as 'SKU',
			   st.Descr as 'Description',	
			   ev.BQTY as 'Buy Qty',	
			   ev.PRIC as 'Deal Price'
		From EVTMAS as ev inner join STKMAS as st on KEY1 = st.SKUN  
		where Numb = @Event
	END
END
GO

