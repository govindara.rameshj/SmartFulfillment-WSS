﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_FillSlotInDeliveryOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_FillSlotInDeliveryOrder'
	EXEC ('CREATE PROCEDURE [dbo].[usp_FillSlotInDeliveryOrder] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_FillSlotInDeliveryOrder'
GO
ALTER PROCEDURE [dbo].[usp_FillSlotInDeliveryOrder]
   @OrderNumber char(6),
   @DeliveryDate Date = NULL
AS
BEGIN
SET NOCOUNT ON
--    1 - Sunday
--    2 - Monday
--    3 - Tuesday
--    4 - Wednesday
--    5 - Thursday
--    6 - Friday
--    7 - Saturday
DECLARE
    @SlotId int,
    @SlotDescription varchar(50),   
    @SlotStartTime time,
    @SlotEndTime time
    
    IF @DeliveryDate IS NOT NULL 
        BEGIN
            IF (DATEPART(weekday, @DeliveryDate) + @@datefirst) % 7 = 0 
                BEGIN
                    SELECT @SlotId = [LongValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 57
                    SELECT @SlotDescription = [StringValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 58    
                    SELECT @SlotStartTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 59
                    SELECT @SlotEndTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 60 
                END
            ELSE
                BEGIN
                    SELECT @SlotId = [LongValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 53
                    SELECT @SlotDescription = [StringValue] FROM [dbo].[Parameters] WHERE [ParameterID] = 54    
                    SELECT @SlotStartTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 55
                    SELECT @SlotEndTime = CONVERT(time,[StringValue], 108) FROM [dbo].[Parameters] WHERE [ParameterID] = 56 
                END 
            
            UPDATE dbo.CORHDR
            SET [RequiredDeliverySlotID] = @SlotId,
                [RequiredDeliverySlotDescription] = @SlotDescription,
                [RequiredDeliverySlotStartTime] = @SlotStartTime,
                [RequiredDeliverySlotEndTime] = @SlotEndTime
            WHERE NUMB = @OrderNumber  AND DELI = 1
        END
END
GO

