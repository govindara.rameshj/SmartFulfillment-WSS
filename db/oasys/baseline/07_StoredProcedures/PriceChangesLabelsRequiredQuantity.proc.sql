﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PriceChangesLabelsRequiredQuantity]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PriceChangesLabelsRequiredQuantity'
	EXEC ('CREATE PROCEDURE [dbo].[PriceChangesLabelsRequiredQuantity] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PriceChangesLabelsRequiredQuantity'
GO
ALTER PROCEDURE [dbo].[PriceChangesLabelsRequiredQuantity]
	@Quantity INT OUTPUT
AS
begin
	DECLARE 
		@Result		INT,
		@Small		INT,
		@Medium		INT,
		@Large		INT;

	SET @Small	= (SELECT COUNT(PRCCHG.SKUN) 
					FROM PRCCHG INNER JOIN STKMAS ON STKMAS.SKUN = PRCCHG.SKUN
					WHERE PRCCHG.PSTA = 'U' AND PRCCHG.LABS = 1 AND STKMAS.AAPC=0);
		
	SET @Medium	= (SELECT COUNT(PRCCHG.SKUN) 
					FROM PRCCHG INNER JOIN STKMAS ON STKMAS.SKUN = PRCCHG.SKUN
					WHERE PRCCHG.PSTA = 'U' AND PRCCHG.LABM = 1 AND STKMAS.AAPC=0);
					
	SET @Large	= (SELECT COUNT(PRCCHG.SKUN) 
					FROM PRCCHG INNER JOIN STKMAS ON STKMAS.SKUN = PRCCHG.SKUN
					WHERE PRCCHG.PSTA = 'U' AND PRCCHG.LABL = 1 AND STKMAS.AAPC=0);				

	IF @Small IS NULL SET @Small = 0;
	IF @Medium IS NULL SET @Medium = 0;
	IF @Large IS NULL SET @Large = 0;

	SELECT  @Quantity	= @Small + @Medium + @Large;
end
GO

