﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SystemUsersUpdateSYSPAS]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SystemUsersUpdateSYSPAS'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SystemUsersUpdateSYSPAS] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SystemUsersUpdateSYSPAS'
GO
ALTER PROCEDURE [dbo].[usp_SystemUsersUpdateSYSPAS]
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 8th March 2011
-- 
-- Task     : Updates / Inserts Users into SYSPAS table for use by Till + HHT Systems.
-- Notes	: SP updates or inserts data as old data is sometimes present. Updating or Inserting the
--            data will prevent the stored procedure from failing. Data in Pilot stores has shown that
--            existing data can or may not already exist.
-----------------------------------------------------------------------------------
-- Author       : Michael O'Cain
-- Updated:     : 18th August 2011
-- Referral No  : CR0052
-- Description  : New Security Model RollOut
-----------------------------------------------------------------------------------
	
		@sp_EmployeeCode			char(3),
		@sp_Name				char(35),
		@sp_Initials				char(5),
		@sp_Position				char(20),
		@sp_PayrollId				char(20),
		@sp_Password				char(5),
		@sp_PasswordExpires			date,
		@sp_IsSupervisor			bit,
		@sp_SuperPassword			char(5),
		@sp_SuperPasswordExpires		date,
		@sp_Outlet				char(2),
		@sp_IsDeleted				bit,
		@sp_DeletedDate				date,
		@sp_DeletedTime				char(6),
		@sp_DeletedBy				char(3),
		@sp_DeletedWhere			char(2),
		@sp_TillReceiptName			char(35),
		@sp_DefaultAmount			dec(9,2),
		@sp_LanguageCode			char(3),
		@sp_IsManager				bit,
		@sp_SecurityProfileId			int				
	

AS
BEGIN
	SET NOCOUNT ON;

--------------------------------------------------------------------------------------------------------------------------
-- Set HHT Access
--------------------------------------------------------------------------------------------------------------------------
Declare @sp_HHTAccess char(3)
Set		@sp_HHTAccess = (Select CAST(RIGHT(RTRIM(SecurityProfileId),3) as CHAR (3)) from SystemUsers where EmployeeCode = @sp_EmployeeCode)

--------------------------------------------------------------------------------------------------------------------------
-- Insert New / Update Existing User into SYSPAS Table
--------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select EEID from [Oasys].[dbo].[SYSPAS] where EEID = @sp_EmployeeCode)
	Begin
		insert into SYSPAS 
				(
				eeid,name,init,posi,epay,pass,dpas,supv,
				auth,daut,outl,delc,ddat,dtim,dwho,dout,tnam,
				dfam,lang,mana,iafg77,SECL77
				) 
		values	( 
				@sp_EmployeeCode,@sp_Name,@sp_Initials,@sp_Position,@sp_PayrollId,@sp_Password,@sp_PasswordExpires,@sp_IsSupervisor,@sp_SuperPassword,
				@sp_SuperPasswordExpires,@sp_Outlet,@sp_IsDeleted,@sp_DeletedDate,@sp_DeletedTime,@sp_DeletedBy,@sp_DeletedWhere,@sp_TillReceiptName,
				@sp_DefaultAmount,@sp_LanguageCode,@sp_IsManager,1, @sp_HHTAccess
				)		
	End
ELSE	
	Begin
		Update	SYSPAS
		Set		eeid = @sp_EmployeeCode, name = @sp_Name, init = @sp_Initials, posi = @sp_Position, epay = @sp_PayrollId, pass = @sp_Password, 
				dpas = @sp_PasswordExpires, supv = @sp_IsSupervisor, auth	= @sp_SuperPassword, daut = @sp_SuperPasswordExpires, outl = @sp_Outlet,
				delc = @sp_IsDeleted, ddat = @sp_DeletedDate, dtim = @sp_DeletedTime, dwho = @sp_DeletedBy, dout = @sp_DeletedWhere,
				tnam = @sp_TillReceiptName, dfam = @sp_DefaultAmount, lang = @sp_LanguageCode, mana = @sp_IsManager,iafg77 = 1,
				secl77 = @sp_HHTAccess
		Where	eeid = @sp_EmployeeCode;
	End

			
END
GO

