﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SETSYC]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SETSYC'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SETSYC] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SETSYC'
GO
ALTER PROCEDURE dbo.KevanConversion_SETSYC
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 14 - Inserting Miscelleanious Income / Outgoing Reports into System Codes
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '1' and Type = 'M+')
		Begin
			Print ('System Code 1 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 1 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('1', 'M+', '1115', 'Bad Cheque')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '2' and Type = 'M+')
		Begin
			Print ('System Code 2 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 2 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('2', 'M+', '0000', 'Not Used')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '3' and Type = 'M+')
		Begin
			Print ('System Code 3 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 3 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('3', 'M+', '6520', 'Suspense')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '4' and Type = 'M+')
		Begin
			Print ('System Code 4 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 4 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('4', 'M+', '3005', 'Burger Van')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '5' and Type = 'M+')
		Begin
			Print ('System Code 5 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 5 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('5', 'M+', '8501', 'Safe Overs')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '6' and Type = 'M+')
		Begin
			Print ('System Code 6 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 6 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('6', 'M+', '6504', 'Phone Box')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '7' and Type = 'M+')
		Begin
			Print ('System Code 7 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 7 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('7', 'M+', '6524', 'Vending Machine')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '8' and Type = 'M+')
		Begin
			Print ('System Code 8 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 8 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('8', 'M+', '6704', 'Rent')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '9' and Type = 'M+')
		Begin
			Print ('System Code 9 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 9 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('9', 'M+', '1595', 'Display Sale')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '10' and Type = 'M+')
		Begin
			Print ('System Code 10 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 10 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('10', 'M+', '1001', 'Safe Change')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '11' and Type = 'M+')
		Begin
			Print ('System Code 11 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 11 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('11', 'M+', '6708', 'Council Account')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '12' and Type = 'M+')
		Begin
			Print ('System Code 12 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 12 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('12', 'M+', '0000', 'Not Used')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '13' and Type = 'M+')
		Begin
			Print ('System Code 13 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 13 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('13', 'M+', '0000', 'Not Used')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '14' and Type = 'M+')
		Begin
			Print ('System Code 14 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 14 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('14', 'M+', '6115', 'Leukaemia Fund')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '15' and Type = 'M+')
		Begin
			Print ('System Code 15 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 15 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('15', 'M+', '1003', 'Refund Till')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '16' and Type = 'M+')
		Begin
			Print ('System Code 16 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 16 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('16', 'M+', '2134', 'H/O Cheque')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '17' and Type = 'M+')
		Begin
			Print ('System Code 17 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 17 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('17', 'M+', '0000', 'Not Used')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '18' and Type = 'M+')
		Begin
			Print ('System Code 18 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 18 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('18', 'M+', '1113', 'Repay Project Loan')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '19' and Type = 'M+')
		Begin
			Print ('System Code 19 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 19 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('19', 'M+', '9876', 'Gift Voucher')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '20' and Type = 'M+')
		Begin
			Print ('System Code 20 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 20 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('20', 'M+', '0000', 'Not Used')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '1' and Type = 'M-')
		Begin
			Print ('System Code 1 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 1 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('1', 'M-', '4003', 'Customer Concern')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '2' and Type = 'M-')
		Begin
			Print ('System Code 2 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 2 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('2', 'M-', '1184', 'Wage Advance')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '3' and Type = 'M-')
		Begin
			Print ('System Code 3 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 3 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('3', 'M-', '6705', 'Breakfast Voucher')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '4' and Type = 'M-')
		Begin
			Print ('System Code 4 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 4 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('4', 'M-', '6321', 'Repair')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '5' and Type = 'M-')
		Begin
			Print ('System Code 5 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 5 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('5', 'M-', '6502', 'Travel')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '6' and Type = 'M-')
		Begin
			Print ('System Code 6 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 6 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('6', 'M-', '6508', 'Stamps')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '7' and Type = 'M-')
		Begin
			Print ('System Code 7 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 7 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('7', 'M-', '5603', 'Meeting Food')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '8' and Type = 'M-')
		Begin
			Print ('System Code 8 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 8 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('8', 'M-', '5602', 'Colleague Welfare')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '9' and Type = 'M-')
		Begin
			Print ('System Code 9 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 9 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('9', 'M-', '6709', 'Council Account')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '10' and Type = 'M-')
		Begin
			Print ('System Code 10 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 10 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('10', 'M-', '1001', 'Change Fund')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '11' and Type = 'M-')
		Begin
			Print ('System Code 11 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 11 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('11', 'M-', '6521', 'Suspense')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '12' and Type = 'M-')
		Begin
			Print ('System Code 12 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 12 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('12', 'M-', '8501', 'Safe Unders')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '13' and Type = 'M-')
		Begin
			Print ('System Code 13 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 13 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('13', 'M-', '6312', 'Cleaning')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '14' and Type = 'M-')
		Begin
			Print ('System Code 14 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 14 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('14', 'M-', '6115', 'Leukaemia Fund')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '15' and Type = 'M-')
		Begin
			Print ('System Code 15 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 15 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('15', 'M-', '1004', 'Refund Till')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '16' and Type = 'M-')
		Begin
			Print ('System Code 16 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 16 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('16', 'M-', '2134', 'H/O Cheque')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '17' and Type = 'M-')
		Begin
			Print ('System Code 17 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 17 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('17', 'M-', '6529', 'Forklift Fuel')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '18' and Type = 'M-')
		Begin
			Print ('System Code 18 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 18 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('18', 'M-', '1113', 'Project Loan')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '19' and Type = 'M-')
		Begin
			Print ('System Code 19 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 19 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('19', 'M-', '9876', 'Gift Voucher')
		End


	IF EXISTS (SELECT * FROM SystemCodes WHERE Id = '20' and Type = 'M-')
		Begin
			Print ('System Code 2 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating System Code 20 - Misc Payments..')
			INSERT [SystemCodes] ([Id], [Type], [Code], [Name]) VALUES ('20', 'M-', '5604', 'Stationary')
		End


END;
GO

