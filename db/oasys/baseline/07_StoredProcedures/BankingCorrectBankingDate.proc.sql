﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BankingCorrectBankingDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure BankingCorrectBankingDate'
	EXEC ('CREATE PROCEDURE [dbo].[BankingCorrectBankingDate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure BankingCorrectBankingDate'
GO
-- =============================================
-- Author      : Partha Dutta
-- Create Date : 07/10/2010
-- Referral No : 388
-- Notes       : Return correct banking date
-- =============================================

ALTER PROCedure BankingCorrectBankingDate
   @PeriodId int output
as
begin
    set nocount on

    exec SystemPeriodAutoPopulate

    set @PeriodId = (select min(a.ID)
                     from SystemPeriods a
                     inner join [Safe] b
                           on a.ID=b.PeriodID
                     where b.IsClosed = 0)

end
GO

