﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_ResetPermissions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_ResetPermissions'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_ResetPermissions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_ResetPermissions'
GO
ALTER PROCEDURE dbo.usp_ITSupport_ResetPermissions
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 17th May 2013
-- Description	: Resets Server Permissions to Default Model
-----------------------------------------------------------------------------------
AS
BEGIN
SET NOCOUNT ON

---------------------------------------------------------------------------------------------------
-- Reset Permissions for Stored Procedures / Functions / Views / Tables
---------------------------------------------------------------------------------------------------
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#ResetPerm_Procedures%') and [TYPE] = 'U') Drop Table #ResetPerm_Procedures
Create Table #ResetPerm_Procedures([ID] [int] IDENTITY(1,1),[Procedure] Char(100) NOT NULL)
Insert into #ResetPerm_Procedures
Select Name From Oasys.Sys.sysobjects Where XType in ('P') Order By Name
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#ResetPerm_Functions%') and [TYPE] = 'U') Drop Table #ResetPerm_Functions
Create Table #ResetPerm_Functions([ID] [int] IDENTITY(1,1),[Function] Char(100) NOT NULL)
Insert into #ResetPerm_Functions
Select Name From Oasys.Sys.sysobjects Where XType in ('IF','TF','FN') Order By Name
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#ResetPerm_Views%') and [TYPE] = 'U') Drop Table #ResetPerm_Views
Create Table #ResetPerm_Views([ID] [int] IDENTITY(1,1),[View] Char(100) NOT NULL)
Insert into #ResetPerm_Views
Select Name From Oasys.Sys.sysobjects Where XType in ('V') Order By Name
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#ResetPerm_Tables%') and [TYPE] = 'U') Drop Table #ResetPerm_Tables
Create Table #ResetPerm_Tables([ID] [int] IDENTITY(1,1),[Table] Char(100) NOT NULL)
Insert into #ResetPerm_Tables
Select Name From Oasys.Sys.sysobjects Where XType in ('U') Order By Name
Declare @KDN Char(10) = 'Oasys'
Declare @KRS NVarChar(400)
Declare @KRS1 NVarChar(400)
Declare @KRS2 NVarChar(400)
Declare @KRS3 NVarChar(400)
Declare @KRS4 NVarChar(400)
Declare @KRS5 NVarChar(400)
Declare @Store Char(4) = (Select '8' + Stor As Expr1 From RETOPT Where FKEY = '01')


------------------------------------------
-- Process Stored Procedures
------------------------------------------
Print ('Apply Execute Stored Procedure Permission to Store User Role')
Declare @RPP_ID int
Declare @RPP_MAXID int
Select  @RPP_MAXID = MAX(ID)FROM #ResetPerm_Procedures
Set		@RPP_ID = 1 
While	(@RPP_ID <= @RPP_MAXID)
	Begin
		Declare @KPN Char(100) = (Select [Procedure] From #ResetPerm_Procedures Where ID = @RPP_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KPN))) is not null
			Begin
				Print 'SP : ' + LTRIM(RTRIM(@KPN)) + ' - Applying Execute Procedure Permissions'
				Set @KRS = 'Grant Execute On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KPN)) + '] TO [role_execproc] WITH GRANT OPTION'
				Execute(@KRS);
			End
		Else
			Begin
				Print 'SP : ' + LTRIM(RTRIM(@KPN)) + ' - Skipping Execute Procedure Permissions (D.N.E)'
			End  	
	    Set @RPP_ID = @RPP_ID+1 
	End


------------------------------------------
-- Process IIS Stored Procedures
------------------------------------------
Print ('Apply Execute Stored Procedure Permission to IIS Account')
Declare @RPI_ID int
Declare @RPI_MAXID int
Select  @RPI_MAXID = MAX(ID)FROM #ResetPerm_Procedures
Set		@RPI_ID = 1 
While	(@RPI_ID <= @RPI_MAXID)
	Begin
		Declare @KIN Char(100) = (Select [Procedure] From #ResetPerm_Procedures Where ID = @RPI_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KIN))) is not null
			Begin
				Print 'SP : ' + LTRIM(RTRIM(@KIN)) + ' - Applying NT Authority Access Permissions'
				Set @KRS = 'Grant Execute On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KIN)) + '] TO [NT AUTHORITY\IUSR] WITH GRANT OPTION'
				Execute(@KRS);
			End
		Else
			Begin
				Print 'SP : ' + LTRIM(RTRIM(@KIN)) + ' - Skipping NT Authority Access Permissions (D.N.E)'
			End 	
	    Set @RPI_ID = @RPI_ID+1 
	End
Drop Table #ResetPerm_Procedures


------------------------------------------
-- Process Functions
------------------------------------------
Print ('Apply Function Permissions')
Declare @RPF_ID int
Declare @RPF_MAXID int
Select  @RPF_MAXID = MAX(ID)FROM #ResetPerm_Functions
Set		@RPF_ID = 1 
While	(@RPF_ID <= @RPF_MAXID)
	Begin
		Declare @KFN Char(100) = (Select [Function] From #ResetPerm_Functions Where ID = @RPF_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KFN))) is not null
			Begin
				Declare @KRS_S Char(1) = 'N'
				Begin TRY
					Set @KRS = 'Grant Execute On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KFN)) + '] TO [role_execproc] WITH GRANT OPTION'
					Execute(@KRS)
					Set @KRS_S = 'E'
				End TRY

				Begin CATCH
					If @@ERROR > 0
						Begin
							Set @KRS = 'Grant Select On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KFN)) + '] TO [role_execproc] WITH GRANT OPTION'
							Execute(@KRS);
							Set @KRS_S = 'S'
						End
				End CATCH;	
				Print 'FN : ' + LTRIM(RTRIM(@KFN)) + ' - Applying Execute Function Permissions (' + @KRS_S + ')'	 			
			End
		Else
			Begin
				Print 'FN : ' + LTRIM(RTRIM(@KFN)) + ' - Skipping Execute Function Permissions (D.N.E)'
			End  	
	    Set @RPF_ID = @RPF_ID+1 
	End
Drop Table #ResetPerm_Functions


------------------------------------------
-- Process Views
------------------------------------------
Print ('Apply View Permissions')
Declare @RPV_ID int
Declare @RPV_MAXID int
Select  @RPV_MAXID = MAX(ID)FROM #ResetPerm_Views
Set		@RPV_ID = 1 
While	(@RPV_ID <= @RPV_MAXID)
	Begin
		Declare @KVN Char(100) = (Select [View] From #ResetPerm_Views Where ID = @RPV_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KVN))) is not null
			Begin
				Print 'VW : ' + LTRIM(RTRIM(@KVN)) + ' - Applying View Access Permissions'
				Set @KRS1 = 'Grant Alter On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS2 = 'Grant Delete On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS3 = 'Grant Insert On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS4 = 'Grant Select On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS5 = 'Grant Update On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Execute(@KRS1);
				Execute(@KRS2);
				Execute(@KRS3);
				Execute(@KRS4);
				Execute(@KRS5);
			End
		Else
			Begin
				Print 'VW : ' + LTRIM(RTRIM(@KVN)) + ' - Skipping View Access Permissions (D.N.E)'
			End 	
	    Set @RPV_ID = @RPV_ID+1 
	End
Drop Table #ResetPerm_Views


------------------------------------------
-- Process Tables
------------------------------------------
Print ('Apply Table Permissions')
Declare @RPT_ID int
Declare @RPT_MAXID int
Select  @RPT_MAXID = MAX(ID)FROM #ResetPerm_Tables
Set		@RPT_ID = 1 
While	(@RPT_ID <= @RPT_MAXID)
	Begin
		Declare @KTN Char(100) = (Select [Table] From #ResetPerm_Tables Where ID = @RPT_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KTN))) is not null
			Begin
				Print 'TA : ' + LTRIM(RTRIM(@KTN)) + ' - Applying Legacy Access Permissions'
				Set @KRS1 = 'Grant Alter On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS2 = 'Grant Delete On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS3 = 'Grant Insert On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS4 = 'Grant Select On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS5 = 'Grant Update On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Execute(@KRS1);
				Execute(@KRS2);
				Execute(@KRS3);
				Execute(@KRS4);
				Execute(@KRS5);
			End
		Else
			Begin
				Print 'TA : ' + LTRIM(RTRIM(@KTN)) + ' - Skipping Legacy Access Permissions (D.N.E)'
			End 
	    Set @RPT_ID = @RPT_ID+1 
	End
Drop Table #ResetPerm_Tables

	
---------------------------------------------------------------------------------------------------
-- Security User Groups
---------------------------------------------------------------------------------------------------
If Exists (Select * From sys.server_principals Where Name = 'TPPLC\WX_' + LTRIM(RTRIM(@Store)) + '_SQL')
		Begin
			Print ('User TPPLC\WX_' + LTRIM(RTRIM(@Store)) + '_SQL exists in this SQL Server, skipping creation..')
		End
Else
		Begin
			Print ('Creating user TPPLC\WX_8nnn_SQL..')
			Declare @SUL_Domain char(5) = 'TPPLC'
    		Declare @SUL_TpOrg char(3) = 'WX_'
    		Declare @SUL_TpGroup char(4) = '_SQL'
    		Declare @SUL_Name char (11) = (@SUL_TpOrg + @Store + @SUL_TpGroup)
    		Declare @SUL_SQL NVARCHAR(4000)
    		Set @SUL_SQL = 'CREATE LOGIN ['+ @SUL_Domain +'\'+ @SUL_Name +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'CREATE USER [' + @SUL_Domain +'\'+ @SUL_Name + '] FOR LOGIN ['+ @SUL_Domain +'\'+ @SUL_Name + ']'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'sys.sp_addrolemember @membername = ['+ @SUL_Domain +'\'+ @SUL_Name +'], @rolename = role_execproc'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'sys.sp_addrolemember @membername = ['+ @SUL_Domain +'\'+ @SUL_Name +'], @rolename = role_legacy'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'sys.sp_addrolemember @membername = ['+ @SUL_Domain +'\'+ @SUL_Name +'], @rolename = db_backupoperator'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'GRANT ALTER TO ['+ @SUL_Domain +'\'+ @SUL_Name +'] WITH GRANT OPTION'
    		Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT BACKUP DATABASE TO ['+ @SUL_Domain +'\'+ @SUL_Name +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT CONNECT TO ['+ @SUL_Domain +'\'+ @SUL_Name +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT EXECUTE TO ['+ @SUL_Domain +'\'+ @SUL_Name +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT SELECT TO ['+ @SUL_Domain +'\'+ @SUL_Name +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT UPDATE TO ['+ @SUL_Domain +'\'+ @SUL_Name +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
		End


---------------------------------------------------------------------------------------------------
-- Security Wickes IT Support
---------------------------------------------------------------------------------------------------
If Exists (Select * From sys.server_principals Where Name = 'TPPLC\IT_StoreSupp_G')
		Begin
			Print ('User TPPLC\IT_StoreSupp_G exists in this SQL Server, skipping creation..')
		End
Else
		Begin
			Print ('Creating User TPPLC\IT_StoreSupp_G..')
			Declare @SUP_Domain char(5) = 'TPPLC'
        	Declare @SUP_TpOrg char(3)  = 'IT_'
    		Declare @SUP_TpDept char(9) = 'storesupp'
    		Declare @SUP_TpGroup char(2) = '_G'
    		Declare @SUP_Name char (14) = (@SUP_TpOrg + @SUP_TpDept + @SUP_TpGroup)
    		Declare @SUP_SQL NVARCHAR(4000)
    		Set @SUP_SQL = 'CREATE LOGIN ['+ @SUP_Domain +'\'+ @SUP_Name +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]'
    		Execute(@SUP_SQL)
    		Set @SUP_SQL = 'CREATE USER [' + @SUP_Domain +'\'+ @SUP_Name + '] FOR LOGIN ['+ @SUP_Domain +'\'+ @SUP_Name + ']'
    		Execute(@SUP_SQL)
    		Set @SUP_SQL = 'sys.sp_addrolemember @membername = ['+ @SUP_Domain +'\'+ @SUP_Name +'], @rolename = role_execproc'
    		Execute(@SUP_SQL)
    		Set @SUP_SQL = 'sys.sp_addrolemember @membername = ['+ @SUP_Domain +'\'+ @SUP_Name +'], @rolename = role_legacy'
    		Execute(@SUP_SQL)
    		Set @SUP_SQL = 'sys.sp_addrolemember @membername = ['+ @SUP_Domain +'\'+ @SUP_Name +'], @rolename = db_backupoperator'
    		Execute(@SUP_SQL)
    		Set @SUP_SQL = 'EXEC master..sp_addsrvrolemember @loginame = ['+ @SUP_Domain +'\'+ @SUP_Name +'], @rolename = sysadmin'
    		Execute(@SUP_SQL)
    		Set @SUP_SQL = 'GRANT ALTER TO ['+ @SUP_Domain +'\'+ @SUP_Name +'] WITH GRANT OPTION'
    		Execute(@SUP_SQL)
			Set @SUP_SQL = 'GRANT BACKUP DATABASE TO ['+ @SUP_Domain +'\'+ @SUP_Name +'] WITH GRANT OPTION'
			Execute(@SUP_SQL)
			Set @SUP_SQL = 'GRANT CONNECT TO ['+ @SUP_Domain +'\'+ @SUP_Name +'] WITH GRANT OPTION' 
			Execute(@SUP_SQL)
			Set @SUP_SQL = 'GRANT EXECUTE TO ['+ @SUP_Domain +'\'+ @SUP_Name +'] WITH GRANT OPTION' 
			Execute(@SUP_SQL)
			Set @SUP_SQL = 'GRANT SELECT TO ['+ @SUP_Domain +'\'+ @SUP_Name +'] WITH GRANT OPTION' 
			Execute(@SUP_SQL)
			Set @SUP_SQL = 'GRANT UPDATE TO ['+ @SUP_Domain +'\'+ @SUP_Name +'] WITH GRANT OPTION' 
			Execute(@SUP_SQL)
		End


---------------------------------------------------------------------------------------------------
-- Security TP Domain Administrators
---------------------------------------------------------------------------------------------------
If Exists (SELECT * FROM sys.server_principals WHERE name = 'TPPLC\IT_wixADadmin_S')
		Begin
			Print ('User TPPLC\IT_wixADadmin_S exists in this SQL Server, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating user TPPLC\IT_wixADadmin_S..')
			Declare @DAD_Domain char(5) = 'TPPLC'
    		Declare @DAD_TpOrg char(3) = 'IT_'
    		Declare @DAD_TpDept char(10) = 'wixADadmin'
    		Declare @DAD_TpGroup char(2) = '_S'
    		Declare @DAD_Name char (15) = (@DAD_TpOrg + @DAD_TpDept + @DAD_TpGroup)
    		Declare @DAD_SQL NVARCHAR(4000)
    		Set @DAD_SQL = 'CREATE LOGIN ['+ @DAD_Domain +'\'+ @DAD_Name +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]'
    		Execute(@DAD_SQL)
    		Set @DAD_SQL = 'CREATE USER [' + @DAD_Domain +'\'+ @DAD_Name + '] FOR LOGIN ['+ @DAD_Domain +'\'+ @DAD_Name + ']'
    		Execute(@DAD_SQL)
    		Set @DAD_SQL = 'sys.sp_addrolemember @membername = ['+ @DAD_Domain +'\'+ @DAD_Name +'], @rolename = role_execproc'
    		Execute(@DAD_SQL)
    		Set @DAD_SQL = 'sys.sp_addrolemember @membername = ['+ @DAD_Domain +'\'+ @DAD_Name +'], @rolename = role_legacy'
    		Execute(@DAD_SQL)
    		Set @DAD_SQL = 'sys.sp_addrolemember @membername = ['+ @DAD_Domain +'\'+ @DAD_Name +'], @rolename = db_backupoperator'
    		Execute(@DAD_SQL)
    		Set @DAD_SQL = 'EXEC master..sp_addsrvrolemember @loginame = ['+ @DAD_Domain +'\'+ @DAD_Name +'], @rolename = sysadmin'
    		Execute(@DAD_SQL)
    		Set @DAD_SQL = 'GRANT ALTER TO ['+ @DAD_Domain +'\'+ @DAD_Name +'] WITH GRANT OPTION'
    		Execute(@DAD_SQL)
			Set @DAD_SQL = 'GRANT BACKUP DATABASE TO ['+ @DAD_Domain +'\'+ @DAD_Name +'] WITH GRANT OPTION'
			Execute(@DAD_SQL)
			Set @DAD_SQL = 'GRANT CONNECT TO ['+ @DAD_Domain +'\'+ @DAD_Name +'] WITH GRANT OPTION';
			Execute(@DAD_SQL)
			Set @DAD_SQL = 'GRANT EXECUTE TO ['+ @DAD_Domain +'\'+ @DAD_Name +'] WITH GRANT OPTION' 
			Execute(@DAD_SQL)
			Set @DAD_SQL = 'GRANT SELECT TO ['+ @DAD_Domain +'\'+ @DAD_Name +'] WITH GRANT OPTION'
			Execute(@DAD_SQL)
			Set @DAD_SQL = 'GRANT UPDATE TO ['+ @DAD_Domain +'\'+ @DAD_Name +'] WITH GRANT OPTION';
			Execute(@DAD_SQL)
		End


---------------------------------------------------------------------------------------------------
-- Security TP Helpdesk
---------------------------------------------------------------------------------------------------
If Exists (Select * From sys.server_principals Where Name = 'TPPLC\IT_Helpdesk_G')
		Begin	
			Print ('User TPPLC\IT_Helpdesk_G exists in this SQL Server, skipping creation..')
		End	
Else	
		Begin
			Print ('Creating user TPPLC\IT_Helpdesk_G..')
			Declare @HLP_Domain char(5) = 'TPPLC'
			Declare @HLP_TpOrg char(3) = 'IT_'
			Declare @HLP_TpDepartment char(8) = 'Helpdesk'
			Declare @HLP_TpGroup char(2) = '_G'
			Declare @HLP_Name char (13) = (@HLP_TpOrg + @HLP_TpDepartment + @HLP_TpGroup)
			Declare @HLP_SQL NVARCHAR(4000)
			Set @HLP_SQL = 'CREATE LOGIN ['+ @HLP_Domain +'\'+ @HLP_Name +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]'
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'sys.sp_addsrvrolemember @loginname = ['+ @HLP_Domain +'\'+ @HLP_Name +'], @rolename = sysadmin'
			Execute(@HLP_SQL)        
			Set @HLP_SQL = 'CREATE USER [' + @HLP_Domain +'\'+ @HLP_Name + '] FOR LOGIN ['+ @HLP_Domain +'\'+ @HLP_Name + ']'
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'sys.sp_addrolemember @membername = ['+ @HLP_Domain +'\'+ @HLP_Name +'], @rolename = role_execproc'
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'sys.sp_addrolemember @membername = ['+ @HLP_Domain +'\'+ @HLP_Name +'], @rolename = role_legacy'
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'sys.sp_addrolemember @membername = ['+ @HLP_Domain +'\'+ @HLP_Name +'], @rolename = db_datawriter'
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'sys.sp_addrolemember @membername = ['+ @HLP_Domain +'\'+ @HLP_Name +'], @rolename = db_datareader'
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'sys.sp_addrolemember @membername = ['+ @HLP_Domain +'\'+ @HLP_Name +'], @rolename = db_accessadmin'
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'GRANT ALTER TO ['+ @HLP_Domain +'\'+ @HLP_Name +'] WITH GRANT OPTION'
			Execute(@HLP_SQL) 
			Set @HLP_SQL = 'GRANT CONNECT TO ['+ @HLP_Domain +'\'+ @HLP_Name +'] WITH GRANT OPTION'
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'GRANT EXECUTE TO ['+ @HLP_Domain +'\'+ @HLP_Name +'] WITH GRANT OPTION' 
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'GRANT SELECT TO ['+ @HLP_Domain +'\'+ @HLP_Name +'] WITH GRANT OPTION' 
			Execute(@HLP_SQL)
			Set @HLP_SQL = 'GRANT UPDATE TO ['+ @HLP_Domain +'\'+ @HLP_Name +'] WITH GRANT OPTION'
			Execute(@HLP_SQL)
		End

-- Final End
End
GO

