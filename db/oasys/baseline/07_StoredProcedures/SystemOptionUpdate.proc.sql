﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemOptionUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemOptionUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SystemOptionUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemOptionUpdate'
GO
ALTER PROCEDURE [dbo].[SystemOptionUpdate]
@DateLastReformat DATE
AS
begin
	update RETOPT set DOLR = convert(char(8), @DateLastReformat,3) where FKEY='01';
	return @@rowcount;
end
GO

