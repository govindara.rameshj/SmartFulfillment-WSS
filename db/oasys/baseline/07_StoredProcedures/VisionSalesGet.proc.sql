﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionSalesGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VisionSalesGet'
	EXEC ('CREATE PROCEDURE [dbo].[VisionSalesGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VisionSalesGet'
GO
ALTER PROCEDURE [dbo].[VisionSalesGet]
   @DateStart date = null,
   @DateEnd   date = null
AS
BEGIN
SET NOCOUNT ON;
select
       [Date]                 = vt.TranDate,
       [Time]                 = convert(char(8), vt.TranDateTime,8),
       vt.TillId,
       vt.TranNumber,
       vt.CashierId,
       [Cashier Name]         = su.Name,
       vt.OrderNumber,
       vt.ValueDiscount,
       [Sales Value]          = vt.Value,
       vt.IsColleagueDiscount,
       [Colleague Disc Value] = vt.ValueColleagueDiscount
from
       VisionTotal vt
left join
       SystemUsers su on su.ID = vt.CashierId
where
 (@DateStart is null or (@DateStart is not null and vt.TranDate >= @DateStart))
 and
 (@DateEnd is null or (@DateEnd  is not null and vt.TranDate <=@DateEnd))
 	
UNION 

select
	dt.DATE1							as [Date],
	convert(char(8), dt.[TIME],8)		as [Time],
	dt.TILL								as [TillId],
	dt.[TRAN]							as [TranNumber],
	dt.CASH 							as [CashierId], 
	su.Name 							as [Cashier Name],
	dt.DOCN 							as [OrderNumber],
	dt.DISC 							as [ValueDiscount],
	dt.TOTL								as [Sales Value],
	dt.IEMP 							as [IsColleagueDiscount],
	0								as [Colleague Disc Value]
from
	DLTOTS  dt 
join 
	DLPAID dp on dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL 
left join	
	SystemUsers su on su.ID = dt.CASH 
where
((dt.TCOD = 'M+' or dt.TCOD = 'M-') and (dt.DOCN is not null) and (dt.MISC =20))  
and
(@DateStart is null or (@DateStart is not null and dt.DATE1 >= @DateStart))
 and
 (@DateEnd is null or (@DateEnd  is not null and dt.DATE1 <=@DateEnd))    

order by
[Date] ,
[Time] ,
[TillId] ,
[TranNumber] 
                  
END
GO

