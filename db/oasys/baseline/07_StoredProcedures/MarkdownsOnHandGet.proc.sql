﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MarkdownsOnHandGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure MarkdownsOnHandGet'
	EXEC ('CREATE PROCEDURE [dbo].[MarkdownsOnHandGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure MarkdownsOnHandGet'
GO
ALTER PROCedure [dbo].[MarkdownsOnHandGet]
as
begin

select
	hc.DESCR			as 'HieCategoryName',
	sm.SKUN				as 'SkuNumber',
	sm.DESCR			as 'Description',
	sm.MDNQ				as 'QtyMarkdown',
	(sm.MDNQ * sm.PRIC) as 'Value'
from 
	STKMAS sm
inner join
	HIECAT hc on hc.NUMB=sm.CTGY
where
	sm.MDNQ<>0
	
end
GO

