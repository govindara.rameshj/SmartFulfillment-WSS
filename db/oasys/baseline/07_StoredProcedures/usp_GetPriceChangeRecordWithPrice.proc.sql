﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetPriceChangeRecordWithPrice]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetPriceChangeRecordWithPrice'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetPriceChangeRecordWithPrice] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetPriceChangeRecordWithPrice'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 30/03/2012
-- Description:	Select a specific Price Change Record
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetPriceChangeRecordWithPrice] 
	@Skun Char(6),
	@StartDate DATE = Null,
	@LabelPrinted bit = Null,
	@Price decimal(10,2),
	@Status Char(1)
AS
BEGIN
	SET NOCOUNT ON;

Select [SKUN]
      ,[PDAT]
      ,[PRIC]
      ,[PSTA]
      ,[SHEL]
      ,[AUDT]
      ,[AUAP]
      ,[MARK]
      ,[MCOM]
      ,[LABS]
      ,[LABM]
      ,[LABL]
      ,[EVNT]
      ,[PRIO]
      ,[EEID]
      ,[MEID]
  FROM [Oasys].[dbo].[PRCCHG]
  Where
       [Skun] = @Skun And 
	   ((@StartDate is null) or (@StartDate is not null and [PDAT]=@StartDate)) And 
	   ((@LabelPrinted is null) or (@LabelPrinted is not null and [SHEL]=@LabelPrinted)) And 
	   [PSTA] = @Status And 
       [PRIC] = @Price 
End
GO

