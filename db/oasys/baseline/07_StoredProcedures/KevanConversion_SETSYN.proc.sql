﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SETSYN]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SETSYN'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SETSYN] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SETSYN'
GO
ALTER PROCEDURE dbo.KevanConversion_SETSYN
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 03 - Updating System Numbers / Location
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

update retopt set countrycode = 'UK';
update strmas set countrycode = 'UK';

	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '1')
		Begin
			Print ('System Next Purchase Order Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for Purchase Orders..')
			insert into systemnumbers select 1, 'PurchaseOrder', next1, size1,	'','' from sysnum where sysnum.fkey='01'
		End


	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '2')
		Begin
			Print ('System Next Returns Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for Returns..')
			insert into systemnumbers select 2, 'Return',	next2, 	size2,	'99','' from sysnum where sysnum.fkey='01'
		End


	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '3')
		Begin
			Print ('System Next IBT Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for IBTs..')
			insert into systemnumbers select 3, 'InterStoreTransfer', next3, 	size3,	'','' from sysnum where sysnum.fkey='01'
		End


	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '4')
		Begin
			Print ('System Next DRL Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for DRLs..')
			insert into systemnumbers select 4, 'Receipt',	next4, 	size4,	'','' from sysnum where sysnum.fkey='01'
		End


	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '5')
		Begin
			Print ('System Next Consignment Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for Consignments..')
			insert into systemnumbers select 5, 'Consignment', next5, size5,	'','' from sysnum where sysnum.fkey='01'
		End


	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '6')
		Begin
			Print ('System Next SOQ Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for SOQs..')
			insert into systemnumbers select 6, 'SoqNumber  ',	next6, size6,	'','' from sysnum where sysnum.fkey='01'
		End


	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '7')
		Begin
			Print ('System Next PS Quote Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for PS Quotes..')
			insert into systemnumbers select 7, 'ProjectSalesQuote', 	next7, 	size7,	'','' from sysnum where sysnum.fkey='01'
		End


	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '8')
		Begin
			Print ('System Next PS Order Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for PS Orders..')
			insert into systemnumbers select 8, 'ProjectSalesOrder', 	next8, 	size8,	'','' from sysnum where sysnum.fkey='01'
		End


	IF EXISTS (SELECT Id FROM SystemNumbers WHERE Id = '10')
		Begin
			Print ('System Next Customer Order Number exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Next System Number for Customer Orders..')
			insert into systemnumbers select 10, 'CustQuoteNumber',	next15, size15,	'','' from sysnum where sysnum.fkey='01'
		End

END;
GO

