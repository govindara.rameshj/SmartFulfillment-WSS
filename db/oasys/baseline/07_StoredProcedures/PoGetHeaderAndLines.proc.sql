﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoGetHeaderAndLines]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoGetHeaderAndLines'
	EXEC ('CREATE PROCEDURE [dbo].[PoGetHeaderAndLines] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoGetHeaderAndLines'
GO
ALTER PROCEDURE [dbo].[PoGetHeaderAndLines]
	@Id	int
AS
BEGIN
	SET NOCOUNT ON;

	select		ph.tkey as 'Id',
				ph.numb as 'PoNumber',
				ph.pnum as 'ConsignNumber',
				ph.soqn as 'SoqNumber',
				ph.reln as 'ReleaseNumber',
				ph.supp as 'SupplierNumber',
				sm.name as 'SupplierName',
				ph.bbcc as 'SupplierBbc',
				ph.odat as 'DateCreated',
				ph.ddat as 'DateDue',
				ph.nocr as 'Cartons',
				ph.valu as 'Value',
				ph.qtyo as 'Units'
	
	from		purhdr ph
	inner join	supmas sm on sm.supn = ph.supp
	where		ph.tkey = @Id;

	select		pl.tkey as 'Id',
				pl.hkey	as 'OrderId',
				pl.skun as 'SkuNumber',
				sk.descr as 'SkuDescription',
				pl.prod as 'ProductCode',
				pl.qtyo as 'OrderQty',
				pl.pric as 'Price',
				pl.cost as 'Cost',
				pl.rqty as 'ReceivedQty',
				pl.rdat as 'DateReceived',
				pl.dele as 'IsDeleted'
				
	from		purlin pl
	inner join	stkmas sk on sk.skun = pl.skun
	where		pl.hkey = @Id
	order by	pl.tkey


END
GO

