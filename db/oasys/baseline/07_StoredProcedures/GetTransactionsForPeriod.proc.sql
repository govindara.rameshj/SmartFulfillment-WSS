﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetTransactionsForPeriod]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GetTransactionsForPeriod'
	EXEC ('CREATE PROCEDURE [dbo].[GetTransactionsForPeriod] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GetTransactionsForPeriod'
GO
ALTER PROCEDURE [dbo].[GetTransactionsForPeriod]
    @periodID int
as
begin
    set nocount on; 

    declare @transDate datetime
    declare @transDateVarchar varchar(8)
    declare @tempDltots table (DATE1 date,TILL char(2),[TRAN] char(4))

    set @transDate = cast((select StartDate from SystemPeriods
                        where ID = @periodID) as Date)
    set @transDateVarchar = convert(varchar(8), @transDate, 3)
    
    insert into @tempDltots
    select DATE1, TILL, [TRAN] from DLTOTS
    where
    ((CASH = 499 and cast(ReceivedDate as DATE) = @transDate) or (CASH <> 499 and DATE1 = @transDate) or ([Source]='OVC' and CBBU = @transDateVarchar))
    and VOID = 0
    and TMOD = 0

    --DLTOTS
    select dt.* from DLTOTS dt
    inner join @tempDltots t on dt.[TRAN] = t.[TRAN]
        and dt.TILL = t.TILL
        and dt.DATE1 = t.DATE1
    

    --DLLINE
    select l.* from DLLINE l
    inner join @tempDltots t on l.[TRAN] = t.[TRAN]
        and l.TILL = t.TILL
        and l.DATE1 = t.DATE1
    where 
    l.LREV = 0
        
    --DLPAID    
    select p.* from DLPAID p
    inner join @tempDltots t on p.[TRAN] = t.[TRAN]
        and p.TILL = t.TILL
        and p.DATE1 = t.DATE1

end
GO

