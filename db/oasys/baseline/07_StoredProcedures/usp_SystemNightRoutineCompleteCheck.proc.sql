﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SystemNightRoutineCompleteCheck]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SystemNightRoutineCompleteCheck'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SystemNightRoutineCompleteCheck] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SystemNightRoutineCompleteCheck'
GO
ALTER PROCEDURE [dbo].[usp_SystemNightRoutineCompleteCheck]
AS
BEGIN
	SET NOCOUNT ON;
	
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 14th October 2011
-- 
-- Task     : Verify's Nigthly Routine is Complete for Software Updates.
-- Notes	: Checks NITLOG against NITMAS and System Dates are also verified as complete.
-----------------------------------------------------------------------------------

Declare @Day char(1)
Set @Day = (Select TMDW From SYSDAT Where FKEY = '01')

--------------------------------------------------------------------------------------------
-- Get the Task Number for given day
--------------------------------------------------------------------------------------------
Declare @Task int
If @Day = 1
	Begin
		Set @Task = (Select MAX (TASK) From NITMAS Where LIVE = '1' and DAYS1 = '1') --order by TASK desc
		--Select TOP 1 (TASK) From NITMAS Where LIVE = '1' and DAYS1 = '1' order by TASK desc
	End
	Else
	If @Day = 2
		Begin
			Set @Task = (Select MAX (TASK) From NITMAS Where LIVE = '1' and DAYS2 = '1') --order by TASK desc
		End
		Else
		If @Day = 3
			Begin
				Set @Task = (Select MAX (TASK) From NITMAS Where LIVE = '1' and DAYS3 = '1') --order by TASK desc
			End
			Else
			If @Day = 4
				Begin
					Set @Task = (Select MAX (TASK) From NITMAS Where LIVE = '1' and DAYS4 = '1') --order by TASK desc
				End
					Else
					If @Day = 5
						Begin
							Set @Task = (Select MAX (TASK) From NITMAS Where LIVE = '1' and DAYS5 = '1') --order by TASK desc
						End
						Else
						If @Day = 6
							Begin
								Set @Task = (Select MAX (TASK) From NITMAS Where LIVE = '1' and DAYS6 = '1') --order by TASK desc
							End
							Else
							If @Day = 7
								Begin
									Set @Task = (Select MAX (TASK) From NITMAS Where LIVE = '1' and DAYS7 = '1') --order by TASK desc
								End
		
--------------------------------------------------------------------------------------------
-- Get the Task Number from NITLOG
--------------------------------------------------------------------------------------------
Declare @NightTask int
Set @NightTask = (Select MAX(TASK) From NITLOG Where DATE1 = (Select TODT From SYSDAT Where FKEY = '01') and EDAT is not NULL)		
		
--------------------------------------------------------------------------------------------
-- Get the Task Number for given day
--------------------------------------------------------------------------------------------
Declare @NightFinished int		
Set @NightFinished = case @Task
						when @NightTask Then '1' Else '0'
					 end
					 
--------------------------------------------------------------------------------------------
-- Verify SYSDAT NITE is 000
--------------------------------------------------------------------------------------------
Declare @NightVerify int
Set @NightVerify =	case (Select NITE From SYSDAT Where FKEY = '01')
						when 000 Then 1 Else 0
					end					 
			
--------------------------------------------------------------------------------------------
-- Verify SYSDAT RETRY is False
--------------------------------------------------------------------------------------------
Declare @NightVerifyRetry int
Set @NightVerifyRetry = case (Select RETY From SYSDAT Where FKEY = '01')
							when 0 Then 1 Else 0
						end			 

--------------------------------------------------------------------------------------------
-- Verify All Nightly Routine Records show as complete
--------------------------------------------------------------------------------------------
Declare @NightDone char(3)
Set @NightDone = case (@NightFinished & @NightVerify & @NightVerifyRetry)
						when '1' Then 'Yes' Else 'No'
				 end
		
--------------------------------------------------------------------------------------------
-- Display Information in Test
--------------------------------------------------------------------------------------------
/*
		Select	@Day as 'Day Number', 
		@Task as 'Task Number', 
		@NightTask as 'Night Task Finished', 
		@NightFinished as 'NITLOG / NITMAS Check',
		@NightVerify as 'SYSDAT NITE Check',
		@NightVerifyRetry as 'SYSDAT RETY Check',
		@NightDone as 'Nightly Routine Complete'
*/

Select LTRIM(RTRIM(@NightDone)) Output 
		
END
GO

