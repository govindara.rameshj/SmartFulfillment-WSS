﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MissingTransactions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure MissingTransactions'
	EXEC ('CREATE PROCEDURE [dbo].[MissingTransactions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure MissingTransactions'
GO
ALTER PROCEDURE [dbo].[MissingTransactions]
@DateEnd DATE
AS
BEGIN
	
	SELECT
			STOR as 'Store',
			SNAM as 'Name'
	FROM
			SYSOPT
	Where	
			FKEY = '01'
	
END
GO

