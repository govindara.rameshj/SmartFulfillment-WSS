﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_OfflineModeSet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_OfflineModeSet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_OfflineModeSet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_OfflineModeSet'
GO
-- =============================================
-- Author:		Alan Lewis
-- Create date: 3/2/2012
-- Description:	Set the Offline mode from
-- =============================================
ALTER PROCEDURE usp_OfflineModeSet
	@InOfflineMode As Bit
AS
BEGIN

	Update
		[Parameters]
	Set
		BooleanValue = @InOfflineMode
	Where
		ParameterID = 1
END
GO

