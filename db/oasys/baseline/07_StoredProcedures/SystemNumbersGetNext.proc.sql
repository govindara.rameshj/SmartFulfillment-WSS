﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemNumbersGetNext]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemNumbersGetNext'
	EXEC ('CREATE PROCEDURE [dbo].[SystemNumbersGetNext] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemNumbersGetNext'
GO
ALTER PROCEDURE [dbo].[SystemNumbersGetNext]
@Id INT
AS
BEGIN
    SET NOCOUNT ON;

    declare @Number int;
    declare @NextNumber int;
    
    --check if for qod (ie 10)
    --updated for hubs 2.0
    if @Id in (10, 16)
        begin
            if @Id = 10
            begin
                set @Number = (select next15 from SYSNUM where FKEY='01');
                set @NextNumber = @Number +1;
                if @NextNumber + 1 > 999999 set @NextNumber = 1;
                
                update SYSNUM set NEXT15 = replace(str(@NextNumber,6),' ','0') where FKEY='01';
            end
            else --
            begin
               declare @TillNo INT, @TranNo INT
               set @Number = (select NEXT16 from SYSNUM where FKEY='01');
               SET @TillNo =  CONVERT(INT,SUBSTRING(CONVERT(CHAR,@Number),0,2)) 
               SET @TranNo =  CONVERT(INT,SUBSTRING(CONVERT(CHAR,@Number),2,4))  
               If @TranNo < 9999
                 BEGIN
                  SET @TranNo = @TranNo+1
                 END
                ELSE
                 BEGIN
                  SET @TranNo = 1
                   IF @TillNo <10 
                     BEGIN
                      SET @TillNo = @TillNo +1
                     END
                     ELSE
                     BEGIN
                     SET @TillNo = 1
                     END
                  END   
              update SYSNUM 
              set NEXT16 = CONVERT(CHAR,STUFF(@TillNo, 1, 0, REPLICATE('0', 2 - LEN(@TillNo))) + STUFF(@TranNo, 1, 0, REPLICATE('0', 4 - LEN(@TranNo))))
              where FKEY='01';          
            end
            return @Number;
        end
    else
        begin
            DECLARE @result TABLE (ResultNumber int);
            
            update SystemNumbers
                set NextNumber = case
                    when NextNumber >= [Max]
                        then [Min]
                        else NextNumber + 1
                     end
                output deleted.NextNumber into @result
                where ID = @Id;
            
            return (select top 1 ResultNumber from @result);        
        end 
END
GO

