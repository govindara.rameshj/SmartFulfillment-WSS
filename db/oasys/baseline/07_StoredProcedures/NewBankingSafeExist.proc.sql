﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeExist]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeExist'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeExist] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeExist'
GO
ALTER PROCedure NewBankingSafeExist
   @PeriodID int,
   @Count    int output
as
begin
set nocount on
set @Count = (select count(*) from [Safe] where PeriodID = @PeriodID)
end
GO

