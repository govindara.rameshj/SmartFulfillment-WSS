﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptIbtInsertLine]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptIbtInsertLine'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptIbtInsertLine] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptIbtInsertLine'
GO
ALTER PROCEDURE [dbo].[ReceiptIbtInsertLine]
@Type CHAR (1), @ReceiptNumber CHAR (6), @Sequence CHAR (4), @SkuNumber CHAR (6), @OrderQty INT=0, @OrderPrice DECIMAL (9, 2)=0, @ReceivedQty INT=0, @ReceivedPrice DECIMAL (9, 2)=0, @OrderLineId INT=0, @IbtQty INT=0, @RtiState CHAR (1)=null, @IncludeInSales BIT=0, @UserId INT
AS
BEGIN
	SET NOCOUNT ON;
	declare
		@logType		char(2),
		@stockStart		int,
		@returnsStart	int,
		@markdownsStart	int,
		@writeOffsStart	int,
		@priceStart		dec(9,2);

	--insert reciept line
	insert into	DRLDET (
		NUMB,
		SEQN,
		SKUN,
		ORDQ,
		ORDP,
		RECQ,
		PRIC,
		POLN,
		IBTQ,
		RTI
	) values (
		@ReceiptNumber,
		@Sequence,
		@SkuNumber,
		@OrderQty,
		@OrderPrice,
		@ReceivedQty,
		@ReceivedPrice,
		@OrderLineId,
		@IbtQty,
		@RtiState
	);
	
	--get stock values
	select
		@logType		='41',
		@stockStart		= ONHA,
		@returnsStart	= RETQ,
		@markdownsStart = MDNQ,
		@writeOffsStart = WTFQ,
		@priceStart		= PRIC
	from 
		STKMAS
	where
		SKUN = @SkuNumber;
	
	--if type=1 then is ibt in so change ibt qty sign
	if @Type='1'
	begin
		set	@IbtQty = @IbtQty * -1;
		set @logType = '42';
	end
	
	--check if need to include in sales
	if @IncludeInSales=1
	begin
		update
			STKMAS
		set
			US001 = US001 + @IbtQty
		where
			SKUN = @SkuNumber;
	end	
	
	--update stock line
	update
		STKMAS
	set
		ONHA = ONHA - @IbtQty,
		TACT = 1
	where
		SKUN = @SkuNumber;
		
	--check for stock log 91
	exec StockLogInsertCheck91 @skuNumber, @stockstart, @UserId
		
	--insert stock log line
	insert into 
		STKLOG (
		SKUN,
		DAYN,
		[TYPE],
		DATE1,
		[TIME],
		KEYS,
		EEID,
		ICOM,
		SSTK,
		ESTK,
		SRET,
		ERET,
		SMDN,
		EMDN,
		SWTF,
		EWTF,
		SPRI,
		EPRI,
		RTI)
	values (
		@SkuNumber,
		DATEDIFF(day, '1900-01-01', getdate()),
		@logType,
		convert(date, GETDATE()),
		REPLACE(CONVERT(varchar, getdate(), 108), ':', ''),
		@ReceiptNumber + ' ' + @Sequence,
		@UserId,
		0,
		@stockStart,
		@stockStart - @IbtQty,
		@returnsStart,
		@returnsStart,
		@markdownsStart,
		@markdownsStart,
		@writeOffsStart,
		@writeOffsStart,
		@priceStart,
		@priceStart,
		'S'
	);
		
	return @@rowcount
END
GO

