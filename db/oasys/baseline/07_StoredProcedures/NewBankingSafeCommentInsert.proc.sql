﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeCommentInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeCommentInsert'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeCommentInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeCommentInsert'
GO
ALTER PROCedure NewBankingSafeCommentInsert

   @BusinessProcessID int,
   @SafePeriodID      int,
   @BagType           nchar(1),
   @Comment           nvarchar(max)

as
begin
   set nocount on
   
   insert SafeComment(BusinessProcessID, SafePeriodID, BagType, Comment) values(@BusinessProcessID, @SafePeriodID, @BagType, @Comment)

end
GO

