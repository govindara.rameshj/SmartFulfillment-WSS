﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODDespatchedOrders]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure QODDespatchedOrders'
	EXEC ('CREATE PROCEDURE [dbo].[QODDespatchedOrders] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure QODDespatchedOrders'
GO
-- =============================================
-- Author:      Mike O'Cain
-- Create date: 7/10/2009
-- Description: Despatched Order Lististing
-- =============================================
ALTER PROCEDURE [dbo].[QODDespatchedOrders] 
    -- Add the parameters for the stored procedure here
    @DateStart datetime = NULL, 
    @DateEnd datetime = NULL
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT vc.Numb                      as 'OrderNumber'
         , vc.REVI                      as 'RevisionNumber'
         , vc.NAME                      as 'CustomerName'
         , vc.POST                      as 'PostCode'
         , convert(varchar,vc.DELD,103) as 'DateRequired'
         , convert(varchar,vc.DDAT,103) as 'DateDelivered'
         , vc.QTYO                      as 'QtyOrdered'
         , vc.QTYT                      as 'QtyTaken'
         , vc.QTYR                      as 'QtyDel'
         , case vc.DELI 
                when 1 then 'Delivery'
                else 'Collection'
           end                          as 'DelCol'
         , vc.DCST                      as 'DelCharge'
         , vc.WGHT                      as 'Weight'
         , vc.VOLU                      as 'Volume'
         , coalesce(su.Name, 'Unknown') as 'OrderTaker'
         --Tables CORHDR and CORHDR4 have been replaced with the view vwCORHDRFull
         FROM vwCORHDRFull as vc 
         LEFT JOIN SystemUsers as su on vc.OEID = su.EmployeeCode
         Where vc.SHOW_IN_UI = 1 and vc.DELC = 1 and (vc.DELD >= @DateStart and vc.DELD <= @DateEnd) and vc.OEID is not null
         Order by vc.DELD
END
GO

