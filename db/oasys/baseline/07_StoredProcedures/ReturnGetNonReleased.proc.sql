﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReturnGetNonReleased]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReturnGetNonReleased'
	EXEC ('CREATE PROCEDURE [dbo].[ReturnGetNonReleased] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReturnGetNonReleased'
GO
ALTER PROCEDURE [dbo].[ReturnGetNonReleased]

AS
begin
	select		
		rh.TKEY		as Id,
		rh.NUMB		as Number,
		rh.SUPP		as SupplierNumber,
		sm.NAME		as SupplierName,
		rh.EDAT		as DateCreated,
		rh.RDAT		as DateCollected,
		rh.DRLN		as ReceiptNumber,
		isnull(sum(rl.QUAN*stk.PRIC),rh.VALU)	as Value, 
		rh.EPRT		as NotPrintedEntry, 
		rh.RPRT		as NotPrintedRelease,
		rh.IsDeleted
	from		
		RETHDR rh
		inner join	SUPMAS sm on rh.SUPP = sm.SUPN
		left join RETLIN rl on rh.TKEY = rl.HKEY
		left join STKMAS stk on rl.SKUN = stk.SKUN
	where		
		(rh.DRLN = null or rh.DRLN='000000')
		and	rh.isdeleted = 0
	group by rh.TKEY, rh.NUMB, rh.SUPP, sm.NAME, rh.EDAT, rh.RDAT, rh.DRLN, rh.EPRT, rh.RPRT, rh.IsDeleted, rh.VALU
	order by	
		sm.NAME, 
		rh.NUMB

end
GO

