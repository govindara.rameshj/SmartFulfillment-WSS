﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockUpdateSoqInventory]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockUpdateSoqInventory'
	EXEC ('CREATE PROCEDURE [dbo].[StockUpdateSoqInventory] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockUpdateSoqInventory'
GO
ALTER PROCEDURE [dbo].[StockUpdateSoqInventory]
	@SkuNumber			char(6),
	@BufferStock		int,
	@OrderLevel			int
AS
BEGIN
	SET NOCOUNT ON;

	update
		stkmas 
	set
		BufferStock		= @BufferStock,
		OrderLevel		= @OrderLevel
	where
		skun = @SkuNumber;
	
END
GO

