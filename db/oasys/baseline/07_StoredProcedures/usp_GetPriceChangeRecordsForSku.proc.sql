﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetPriceChangeRecordsForSku]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetPriceChangeRecordsForSku'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetPriceChangeRecordsForSku] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetPriceChangeRecordsForSku'
GO
ALTER PROCEDURE [dbo].[usp_GetPriceChangeRecordsForSku]
	@skun  char(6)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * from PRCCHG where skun = @skun order by PDAT desc
END
GO

