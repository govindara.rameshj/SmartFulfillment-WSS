﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_InsertCompetitorLocal]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_InsertCompetitorLocal'
	EXEC ('CREATE PROCEDURE [dbo].[usp_InsertCompetitorLocal] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_InsertCompetitorLocal'
GO
ALTER PROCEDURE [dbo].[usp_InsertCompetitorLocal] @CompetitorName char(30),  
                                                      @Success bit OUTPUT
AS
BEGIN
    BEGIN TRY

        BEGIN TRANSACTION;

        IF NOT EXISTS( SELECT Name
                         FROM CompetitorLocalList
                         where name= @CompetitorName
                       UNION
                       SELECT Name
                         FROM competitorFixedList cf
                         WHERE cf.DeleteFlag=0
                           AND cf.name= @CompetitorName
                     )
            BEGIN
                INSERT INTO dbo.CompetitorLocalList( Name
                                                   )
                VALUES( @CompetitorName
                      );

            END;
        COMMIT TRANSACTION;
        SET @Success = 1;
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;
        SET @Success = 0;
    END CATCH;
END;
GO

