﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SECSUP]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SECSUP'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SECSUP] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SECSUP'
GO
ALTER PROCEDURE dbo.KevanConversion_SECSUP
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 28 - Apply Security Access Permissions for IT Store Support
-----------------------------------------------------------------------------------
AS
BEGIN
        SET NOCOUNT ON
        
	IF EXISTS (SELECT * FROM sys.server_principals WHERE name = 'TPPLC\IT_storesupp_G')
		Begin
			Print ('User TPPLC\IT_storesupp_G exists in this SQL Server, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating user TPPLC\IT_storesupp_G..')
	
			declare @Domain char(5)
        		declare @TpOrg char(3)
        		declare @TpDept char(9)
        		declare @TpGroup char(2)
        		declare @Name char (14)
        
        		DECLARE @SQL NVARCHAR(4000);
        		DECLARE @SQL2 NVARCHAR(4000);
        		DECLARE @SQLEXEC1 NVARCHAR(4000);
        		DECLARE @SQLEXEC2 NVARCHAR(4000);
        		DECLARE @SQLEXEC3 NVARCHAR(4000);
        		DECLARE @SQLEXEC4 NVARCHAR(4000);
        		DECLARE @SQLPERM1 NVARCHAR(4000);
        		DECLARE @SQLPERM2 NVARCHAR(4000);
        		DECLARE @SQLPERM3 NVARCHAR(4000);
        		DECLARE @SQLPERM4 NVARCHAR(4000);
        		DECLARE @SQLPERM5 NVARCHAR(4000);
        		DECLARE @SQLPERM6 NVARCHAR(4000);
        
        		SET @Domain = 'TPPLC';
        		SET @TpOrg = 'IT_'; 
        		SET @TpDept = 'storesupp';
        		SET @TpGroup = '_G';
        		SET @Name = (@TpOrg + @TpDept + @TpGroup);
        
        		--Use [Master]
        		SET @SQL = 'CREATE LOGIN ['+ @Domain +'\'+ @Name +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]';
        		EXECUTE(@SQL);
        
        		--Use [Oasys]
        		SET @SQL2 = 'CREATE USER [' + @Domain +'\'+ @Name + '] FOR LOGIN ['+ @Domain +'\'+ @Name + ']';
        		EXECUTE(@SQL2);
        
        		-- Set Server Roles for User Login
        		SET @SQLEXEC1 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = role_execproc';
        		EXECUTE(@SQLEXEC1);
        		SET @SQLEXEC2 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = role_legacy';
        		EXECUTE(@SQLEXEC2);
        		SET @SQLEXEC3 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = db_backupoperator';
        		EXECUTE(@SQLEXEC3);
        		SET @SQLEXEC4 = 'EXEC master..sp_addsrvrolemember @loginame = ['+ @Domain +'\'+ @Name +'], @rolename = sysadmin';
        		EXECUTE(@SQLEXEC4);
        
        		-- Set Permissions for User Login
        		SET @SQLPERM1 = 'GRANT ALTER TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION';
        		EXECUTE(@SQLPERM1); 
			SET @SQLPERM2 = 'GRANT BACKUP DATABASE TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION';
			EXECUTE(@SQLPERM2);
			SET @SQLPERM3 = 'GRANT CONNECT TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM3);
			SET @SQLPERM4 = 'GRANT EXECUTE TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM4);
			SET @SQLPERM5 = 'GRANT SELECT TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM5);
			SET @SQLPERM6 = 'GRANT UPDATE TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM6);
		End

END;
GO

