﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockMarkdownValue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockMarkdownValue'
	EXEC ('CREATE PROCEDURE [dbo].[StockMarkdownValue] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockMarkdownValue'
GO
ALTER PROCEDURE [dbo].[StockMarkdownValue]
	@Value DECIMAL (9, 2) OUTPUT
AS
begin
	SELECT 
		@Value	= SUM(S.MDNQ*S.PRIC) 
	FROM	
		STKMAS S 
	WHERE	
		S.ONHA		> 0
		AND	S.IOBS	= 0
		AND	S.INON	= 0;
	
	SELECT @Value = coalesce(@Value, 0);
	
end
GO

