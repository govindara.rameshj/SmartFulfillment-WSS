﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_SetupPostcodeSurvey]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_SetupPostcodeSurvey'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_SetupPostcodeSurvey] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_SetupPostcodeSurvey'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_SetupPostcodeSurvey]
@sp_StartDate Date,
@sp_EndDate Date
As
Begin
Set NoCount On;
	
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 14th October 2011
-- 
-- Task     : Set Postcode Survey Dates
-----------------------------------------------------------------------------------

Declare @ReturnMessage Char(100)
Declare @Store Char(100) = (Select LTRIM(RTRIM(Cast(STOR as CHAR))) + ' ' + LTRIM(RTRIM(Cast(SNAM as CHAR))) From RETOPT Where FKEY = '01')

If @sp_StartDate Is Null 
	Begin
		Set @ReturnMessage = 'Start Date Not Supplied'
		goto ExitProcess
	End
	
If @sp_EndDate Is Null 
	Begin
		Set @ReturnMessage = 'End Date Not Supplied'
		goto ExitProcess
	End
	
Declare @SYear Char(4) = Cast(DatePart(Year, @sp_StartDate) as Char(4))
Declare @SMonth Char(2) = Right('0'  +Convert(VarChar,DatePart(MM, @sp_StartDate)),2)
Declare @SDay Char(2) = Right('0' +Convert(VarChar,DatePart(DD, @sp_StartDate)),2)
Declare @ProcessStartDate Char(8) = @SYear + @SMonth + @SDay
Declare @EYear Char(4) = Cast(DatePart(Year, @sp_EndDate) as Char(4))
Declare @EMonth Char(2) = Right('0'  +Convert(VarChar,DatePart(MM, @sp_EndDate)),2)
Declare @EDay Char(2) = Right('0' +Convert(VarChar,DatePart(DD, @sp_EndDate)),2)
Declare @ProcessEndDate Char(8) = @EYear + @EMonth + @EDay


----------------------------------------------------------------------------------------------------------------------------------
-- Data Handling
----------------------------------------------------------------------------------------------------------------------------------
If exists (Select [ParameterID] From [Parameters] Where [ParameterID] = 927)
	Begin
		Print('Updating 927 Parameter')
		Update [Parameters]
		Set [Stringvalue] = LTRIM(RTRIM(@ProcessStartDate)), [BooleanValue] = '0' -- Boolean Value Not Required
		Where [ParameterID] = 927
	End
Else
	Begin
		Print('Inserting 927 Parameter')
		Insert into [Parameters] ([ParameterId],[Description],[StringValue],[LongValue],[BooleanValue],[DecimalValue],[ValueType])
		Values ('927', 'PCode Capture Start', LTRIM(RTRIM(@ProcessStartDate)), '0', '0', '0.00', '0')
	End


If exists (Select [ParameterID] From [Parameters] Where [ParameterID] = 928)
	Begin
		Print('Updating 928 Parameter')
		Update [Parameters]
		Set [Stringvalue] = LTRIM(RTRIM(@ProcessEndDate)), [BooleanValue] = '0' -- Boolean Value Not Required
		Where [ParameterID] = 928
	End
Else
	Begin
		Print('Inserting 928 Parameter')
		Insert into [Parameters] ([ParameterId],[Description],[StringValue],[LongValue],[BooleanValue],[DecimalValue],[ValueType])
		Values ('928', 'PCode Capture Start', LTRIM(RTRIM(@ProcessEndDate)), '0', '0', '0.00', '0')
	End


----------------------------------------------------------------------------------------------------------------------------------
-- Error Handling
----------------------------------------------------------------------------------------------------------------------------------
If @@ERROR = 0
	Begin
   		Print(LTRIM(RTRIM(@Store)) +  ' Update Complete!')
	End
Else
	Begin
   		Print(LTRIM(RTRIM(@Store)) + ' Msg - Failed to Setup Postcode Survey!')
	End


ExitProcess:
End
GO

