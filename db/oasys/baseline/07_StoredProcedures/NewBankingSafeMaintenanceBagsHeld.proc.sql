﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeMaintenanceBagsHeld]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeMaintenanceBagsHeld'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeMaintenanceBagsHeld] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeMaintenanceBagsHeld'
GO
ALTER PROCedure NewBankingSafeMaintenanceBagsHeld
   @PeriodID Int
As
Begin
   Set NoCount On
	
	Select 
		BagID      = ID,
		BagType    = [Type],
		SealNumber = SealNumber,
		BagValue   = Value 
	Into 
		#Temp
	From
		SafeBags
	Where
		[Type] in ('F')
	And
		[State] <> 'C'
	And 
		(
			--all non-cancelled bags for this period
			(
				InPeriodID = @PeriodID
			)                              
		Or  --all non-cancelled bags before this period that has no been used 
			(
				InPeriodID < @PeriodID 
			And 
				OutPeriodID = 0
			)          
		Or   
			--all non-cancelled bags that straddled this period
			(
				InPeriodID < @PeriodID 
			And 
				OutPeriodID >= @PeriodID
			)      
		)


    union all
    select BagID      = ID,
           BagType    = [Type],
           SealNumber = SealNumber,
           BagValue   = Value 
    from dbo.udf_PickupBagsInSafeOnDate(@PeriodID)

    union All
    select BagID      = ID,
           BagType    = [Type],
           SealNumber = SealNumber,
           BagValue   = Value 
    from dbo.udf_BankingBagsInSafeOnDate(@PeriodID)

	Select * From #Temp Where BagType = 'F'
	Union All
	Select * From #Temp Where BagType = 'P'
	Union All
	Select * From #Temp Where BagType = 'B'
End
GO

