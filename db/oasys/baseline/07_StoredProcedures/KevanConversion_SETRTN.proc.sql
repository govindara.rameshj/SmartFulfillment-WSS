﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SETRTN]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SETRTN'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SETRTN] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SETRTN'
GO
ALTER PROCEDURE dbo.KevanConversion_SETRTN
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 02 - Updating System Returns Information
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

	IF EXISTS (SELECT * FROM SYSCOD WHERE Type = 'SR' and Code = '01')
		Begin
			Print ('Returns Reason Code 1 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Returns Reason Code 1 - Faulty..')
			Insert into syscod(type, code, descr) values('SR', '01', 'Faulty') 
		End

	
	IF EXISTS( SELECT * FROM SYSCOD WHERE Type = 'SR' and Code = '02')
		Begin
			Print ('Returns Reason Code 2 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Returns Reason Code 2 - Received Damaged..')
			insert into syscod(type, code, descr) values('SR', '02', 'Received Damaged') 
		End


	IF EXISTS (SELECT * FROM SYSCOD WHERE Type = 'SR' and Code = '03')
		Begin
			Print ('Returns Reason Code 3 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Returns Reason Code 3 - Not Ordered..')
			insert into syscod(type, code, descr) values('SR', '03', 'Not Ordered') 
		End


	IF EXISTS (SELECT * FROM SYSCOD WHERE Type = 'SR' and Code = '04')
		Begin
			Print ('Returns Reason Code 4 exists, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating Returns Reason Code 4 - Re-packaging..')
			insert into syscod(type, code, descr) values('SR', '04', 'Re-packaging')
		End

END;
GO

