﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReturnGetNonReleasedOld]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReturnGetNonReleasedOld'
	EXEC ('CREATE PROCEDURE [dbo].[ReturnGetNonReleasedOld] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReturnGetNonReleasedOld'
GO
ALTER PROCEDURE [dbo].[ReturnGetNonReleasedOld]

AS
begin
	select		
		rh.TKEY,
		rh.NUMB,
		rh.SUPP,
		sm.NAME as SupplierName,
		rh.EDAT,
		rh.RDAT,
		rh.PONO,
		rh.DRLN,
		isnull(sum(rl.QUAN*stk.PRIC),rh.VALU),
		rh.EPRT, 
		rh.RPRT,
		rh.IsDeleted,
		rh.RTI
	from		
		RETHDR rh
		inner join	SUPMAS sm on rh.SUPP = sm.SUPN
		left join RETLIN rl on rh.TKEY = rl.HKEY
		left join STKMAS stk on rl.SKUN = stk.SKUN
	where		
		rh.DRLN = '000000'
		and	rh.isdeleted = 0
	group by rh.TKEY, rh.NUMB, rh.SUPP, sm.NAME, rh.EDAT, rh.RDAT, rh.PONO, rh.DRLN, rh.VALU, rh.EPRT, rh.RPRT, rh.IsDeleted, rh.RTI
	order by	
		sm.NAME, 
		rh.NUMB

end
GO

