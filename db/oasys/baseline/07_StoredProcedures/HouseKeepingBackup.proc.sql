﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HouseKeepingBackup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure HouseKeepingBackup'
	EXEC ('CREATE PROCEDURE [dbo].[HouseKeepingBackup] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure HouseKeepingBackup'
GO
ALTER PROCedure HouseKeepingBackup
as
begin

   declare @BackupLocationAndName varchar(max)

   set @BackupLocationAndName = (select StringValue from Parameters where ParameterID = 189)

   backup database Oasys to disk = @BackupLocationAndName with format

end
GO

