﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryRelatedItemSingle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryRelatedItemSingle'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryRelatedItemSingle] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryRelatedItemSingle'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 08/08/2012
-- User Story	 : 6131
-- Project		 : P022-009 - Stock Enquiry Details - amend the design of what is displayed.
-- Task Id		 : 6344
-- Description   : Remove QPP field.
-- =============================================
ALTER PROCedure [dbo].[EnquiryRelatedItemSingle]
	@SkuNumber	Char(6)
As
Begin
	Set NOCOUNT On;

	Select
		1			As 'RowId',
		rl.SPOS		As 'SkuNumber',
		sk.DESCR	As 'Description',
		sk.PRIC		As 'Price',
--		rl.NSPP		As 'QtyPerPack',
		sk.ONHA		As 'QtyOnHand'
	From
		RELITM rl
			Inner Join
				STKMAS sk	
			On 
				sk.SKUN = rl.SPOS
	Where
		rl.PPOS = @SkuNumber

End
GO

