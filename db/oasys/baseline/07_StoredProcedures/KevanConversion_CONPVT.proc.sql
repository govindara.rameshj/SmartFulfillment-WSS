﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_CONPVT]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_CONPVT'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_CONPVT] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_CONPVT'
GO
ALTER PROCEDURE dbo.KevanConversion_CONPVT
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 20 - PV Totals Conversion
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

create table #pvTotsTemp
(
RID INT IDENTITY(1,1),
[DATE1] [char](8) NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[CASH] [char](3) NULL,
	[TIME] [char](6) NULL,
	[TCOD] [char](2) NULL,
	[MISC] [char](2) NULL,
	[DESCR] [char](20) NULL,
	[ORDN] [char](6) NULL,
	[MERC] [char](10) NULL,
	[NMER] [char](10) NULL,
	[TAXA] [char](10) NULL,
	[DISC] [char](10) NULL,
	[TOTL] [char](10) NULL,
	[IEMP] [char](1) NULL,
	[PVEM] [char](10) NULL,
	[PIVT] [char](1) NULL,
)


create table #visionTotTemp
(
[TranDate] [date] NOT NULL,
	[TillId] [int] NOT NULL,
	[TranNumber] [int] NOT NULL,
	[CashierId] [int] NULL,
	[TranDateTime] [char](8) NULL,
	[Type] [char](2) NULL,
	[ReasonCode] [char](2) NULL,
	[ReasonDescription] [varchar](50) NULL,
	[OrderNumber] [char](6) NULL,
	[Value] [decimal](9, 2) NULL,
	[ValueMerchandising] [decimal](9, 2)  NULL,
	[ValueNonMerchandising] [decimal](9, 2)  NULL,
	[ValueTax] [decimal](9, 2) NULL,
	[ValueDiscount] [decimal](9, 2)  NULL,
	[ValueColleagueDiscount] [decimal](9, 2) NULL,
	[IsColleagueDiscount] [bit]  NULL,
	[IsPivotal] [bit]  NULL,
)

INSERT INTO #pvTotsTemp
SELECT * FROM PVTOTS 

INSERT INTO #visionTotTemp
SELECT * FROM VisionTotal


    DECLARE @RID INT
	DECLARE @MAXID INT
	SELECT @MAXID = MAX(RID)FROM #pvTotsTemp -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID = 1
	DECLARE @SD_OLD CHAR(12)
	DECLARE @TILL_ID INT
	DECLARE @TRAN_NO INT


-- looping the #temp_old from first record to last record.
	WHILE(@RID<=@MAXID)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @SD_OLD = DATE1, @TILL_ID = CONVERT(INT,TILL),@TRAN_NO = CONVERT(INT,[TRAN])  FROM #pvTotsTemp WHERE RID = @RID
			-- if the Primary key exists then update, by taking the values from the #temp_old  
          	IF EXISTS(SELECT 1 FROM #visionTotTemp WHERE TranDate  = CONVERT(DATETIME,@SD_OLD,3) AND TillId = @TILL_ID AND TranNumber=@TRAN_NO)
				BEGIN
                               
					UPDATE VisionTotal
					SET	[CashierId]= CONVERT(INT,T.CASH),
					    [TranDateTime]= T.[TIME],
					    [Type]=T.[TCOD],
					    [ReasonCode]= T.[MISC],
					    [ReasonDescription]= T.[DESCR],
					    [OrderNumber]= T.[ORDN],
					    [ValueMerchandising] = CASE WHEN CHARINDEX('-',T.[MERC])>1
					                          THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[MERC],0,LEN(T.[MERC])))
					                          ELSE
					                          CONVERT(DECIMAL,T.[MERC])
					                          END,
					    [ValueNonMerchandising] = CASE WHEN CHARINDEX('-',T.[NMER])>1
					                              THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[NMER],0,LEN(T.[NMER])))
					                              ELSE
					                              CONVERT(DECIMAL,T.[NMER])
					                              END,
					    [ValueTax] = CASE WHEN CHARINDEX('-',T.[TAXA])>1
					                 THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[TAXA],0,LEN(T.[TAXA])))
					                 ELSE
					                 CONVERT(DECIMAL,T.[TAXA]) END,
					    [ValueDiscount] = CASE WHEN CHARINDEX('-',T.[DISC])>1
					                      THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[DISC],0,LEN(T.[DISC])))
					                      ELSE
					                      CONVERT(DECIMAL,T.[DISC])
					                      END,
					    [ValueColleagueDiscount] = CASE WHEN CHARINDEX('-',T.[PVEM])>1
					                               THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[PVEM],0,LEN(T.[PVEM])))
					                               ELSE
					                               CONVERT(DECIMAL,T.[PVEM])
					                               END,
					        
					    [IsColleagueDiscount]= case when T.[IEMP]='Y' then 1 else 0 end,
					    [IsPivotal] = case when T.[PIVT]='Y' then 1 else 0 end
					    				    					    										
					FROM #pvTotsTemp AS T WHERE (VisionTotal.TranDate = CONVERT(DATETIME,@SD_OLD,3) AND T.DATE1 = @SD_OLD) AND (VisionTotal.TillId = @TILL_ID and  T.TILL = CONVERT(CHAR(2),@TILL_ID)) AND (VisionTotal.TranNumber = @TRAN_NO AND  T.[TRAN]= CONVERT(CHAR(4),@TRAN_NO))

				END
			ELSE -- ELSE INSERT NEW RECORD
				BEGIN
					INSERT INTO VisionTotal (TranDate,TillId,TranNumber,CashierId,TranDateTime,[Type],ReasonCode,ReasonDescription,OrderNumber,ValueMerchandising,ValueNonMerchandising,ValueTax,ValueDiscount,ValueColleagueDiscount,IsColleagueDiscount,IsPivotal)
					SELECT CONVERT(DATETIME,T.DATE1,3),CONVERT(INT,T.TILL), CONVERT(INT,T.[TRAN]),CONVERT(INT,T.CASH), T.[TIME],T.[TCOD],T.[MISC],T.[DESCR],T.[ORDN],
					CASE WHEN CHARINDEX('-',T.[MERC])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[MERC],0,LEN(T.[MERC])))
					ELSE
					CONVERT(DECIMAL,T.[MERC])
					END,
					CASE WHEN CHARINDEX('-',T.[NMER])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[NMER],0,LEN(T.[NMER])))
					ELSE
					CONVERT(DECIMAL,T.[NMER])
					END,
					CASE WHEN CHARINDEX('-',T.[TAXA])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[TAXA],0,LEN(T.[TAXA])))
					ELSE
					CONVERT(DECIMAL,T.[TAXA])
					END,
					CASE WHEN CHARINDEX('-',T.[DISC])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[DISC],0,LEN(T.[DISC])))
					ELSE
					CONVERT(DECIMAL,T.[DISC])
					END,
					CASE WHEN CHARINDEX('-',T.[PVEM])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[PVEM],0,LEN(T.[PVEM])))
					ELSE
					CONVERT(DECIMAL,T.[PVEM])
					END,
					case when T.[IEMP]='Y' then 1 else 0 end,case when T.[PIVT]='Y' then 1 else 0 end
					FROM #pvTotsTemp  AS T 
					WHERE T.RID = @RID  
				END

			SET @RID = @RID+1 -- increment loop count
		END

END;
GO

