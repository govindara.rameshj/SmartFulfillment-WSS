﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionSalePayment]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VisionSalePayment'
	EXEC ('CREATE PROCEDURE [dbo].[VisionSalePayment] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VisionSalePayment'
GO
-- =============================================
-- Author:  Dhanesh Ramachandran
-- Create date: 09/14/2010
-- Description: This procedure is used to insert values to DLPAID table for Vision Sales
--
-- =============================================
ALTER PROCEDURE [dbo].[VisionSalePayment]
	@TranDate		date,
	@TillId			char(2),
	@TranNumber		char(4),
	@Number			int,
	@TenderTypeId	dec(3,0),
	@ValueTender	dec(9,2),
	@CreditNumber	char(19),
	@CreditExpireDate char(4)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @rowcount int;
	SET @rowcount=0;	

	--update record if exists
UPDATE 
        DLPAID 
SET
				[TYPE]	= @TenderTypeId,
				AMNT	= @ValueTender,
				[CARD] 	= @CreditNumber,
				EXDT    = @CreditExpireDate,
				COPN    = '000000',
				CLAS    = '00',
				AUTH    = '', -- Ammended KM 13/12/10
				SUPV    = '000',
				POST    = '', -- Ammended KM 13/12/10
				CKAC    = '0000000000',
				CKSC    = '000000',
				CKNO    = '000000',
				SEQN    = '0000',
				ISSU    = '',
				ATYP    = '',
				MERC    = '',
				CBAM    = 0,
				DIGC    = 0,
				ECOM    = 1 ,
				CTYP    = '',
				EFID    = '',
				EFTC    = '',
				STDT    = '0000',
				AUTHDESC = '',
				SECCODE = '',
				TENC  = '',
				MPOW  = 0,
				MRAT  = 0,
				TENV  = 0,
				RTI   = 'S',
				SPARE = ''	
			WHERE
				DATE1	= @TranDate AND
				TILL	= @TillId AND
				[TRAN]	= @TranNumber AND 
				[NUMB]	= @Number 
				
			SET @rowcount = @@ROWCOUNT;	
		--get number of rows affected
	--set @rowcount = @@ROWCOUNT;

	--if nothing updated then insert
	if @rowcount=0
      BEGIN
			INSERT INTO DLPAID 
				(
					DATE1,
					TILL,
					[TRAN],
					NUMB,
					[TYPE],
					AMNT,
					[CARD],
					EXDT,
					COPN ,
					CLAS ,
					AUTH ,
					SUPV  ,
					POST ,
					CKAC ,
					CKSC  ,
					CKNO ,
					SEQN  ,
					ISSU ,
					ATYP ,
					MERC ,
					CBAM ,
					DIGC ,
					ECOM ,
					CTYP ,
					EFID ,
					EFTC  ,
					STDT ,
					AUTHDESC ,
					SECCODE ,
					TENC ,
					MPOW,
					MRAT,
					TENV,
					RTI ,
					SPARE 	
				) 
			VALUES 
				(
					@TranDate,
					@TillId,
					@TranNumber,
					@Number,
					@TenderTypeId,
					@ValueTender,
					@CreditNumber,			
					@CreditExpireDate, 
					'000000',
					'00',
					'', -- AUTH Ammended KM 13/12/10
					'000',
					'', -- POST Ammended KM 13/12/10
					'0000000000',
					'000000',
					'000000',
					'0000',
					'',
					'',
					'',
					0,
					0,
					1 ,
					'',
					'',
					'',
					'0000',
					'',
					'',
					'',
					0,
					0,
					0,
					'S',
					''	
				);


			SET @rowcount = @@ROWCOUNT;

	END
	
IF @CreditNumber = ''

UPDATE DLPAID 
set DLPAID.[CARD] =  '0000000000000000000'                      
                         
WHERE
				DATE1	= @TranDate AND
				TILL	= @TillId AND
				[TRAN]	= @TranNumber AND
				[NUMB]	= @Number       


IF @CreditExpireDate = ''

UPDATE DLPAID
set DLPAID.[EXDT] =  '0000'                      
                         
WHERE
				DATE1	= @TranDate AND
				TILL	= @TillId AND
				[TRAN]	= @TranNumber AND
				[NUMB]	= @Number       


				
	
	RETURN @rowcount;
END
GO

