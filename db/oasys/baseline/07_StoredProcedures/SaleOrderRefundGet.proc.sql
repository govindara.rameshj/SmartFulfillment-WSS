﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderRefundGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderRefundGet'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderRefundGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderRefundGet'
GO
-- ===============================================================================
-- Author         : Partha Dutta
-- Date	          : 23/09/2011
-- Change Request : CR0054-01
-- TFS User Story : 2574
-- TFS Task ID    : 2595
-- Description    : BO-120 iteration's database layer (rollout/rollback scripts) refactored to deal with deployment to 
--                     A. Standard Store
--                     B. eStore 120
--                     C. eWarehouse 076
-- ===============================================================================

-- =============================================
-- Author:		Not Known
-- Create date: 1st March 2011
-- Description:	Baseline version
--
-- Modified by: Michael O'Cain
-- Modified date: 10-08-2011
-- Description:	DeliverySource field was missing
-- =============================================
ALTER PROCEDURE [dbo].[SaleOrderRefundGet]
@OrderNumber CHAR (6)
AS
BEGIN
	SET NOCOUNT ON;

	select
		cr.NUMB				as OrderNumber,
		cr.LINE				as Number,
		cl.SKUN				as SkuNumber,
		st.DESCR			as SkuDescription,
		st.BUYU				as SkuUnitMeasure,
		cr.RefundStoreId,
		cr.RefundDate,
		cr.RefundTill,
		cr.RefundTransaction,
		cr.QtyReturned,
		cr.QtyCancelled,
		st.ONHA				as QtyOnHand,
		st.ONOR				as QtyOnOrder,
		cl.PRICE			as Price,
		cl.DeliverySource,
		cr.RefundStatus,
		cr.SellingStoreIbtOut,
		cl.IsDeliveryChargeItem,
		cr.FulfillingStoreIbtIn
	from
		CORRefund cr (nolock)
	inner join
		CORLIN cl (nolock)
			on cl.NUMB=cr.NUMB and cl.LINE=cr.LINE
	inner join
		STKMAS st (nolock)
			on st.SKUN=cl.SKUN
	where
		cr.NUMB = @OrderNumber
	order by
		cr.numb, cr.LINE, cr.RefundStoreId, cr.RefundDate, cr.RefundTill, cr.RefundTransaction
	
END
GO

