﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_OfflineModeGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_OfflineModeGet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_OfflineModeGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_OfflineModeGet'
GO
-- =============================================
-- Author:		Alan Lewis
-- Create date: 3/2/2012
-- Description:	Retrieve the Offline mode from parameter 1
-- =============================================
ALTER PROCEDURE usp_OfflineModeGet
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		BooleanValue
	From
		[Parameters]
	Where
		[ParameterID] = 1
END
GO

