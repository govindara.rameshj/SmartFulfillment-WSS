﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ContainerGetByDateRange]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ContainerGetByDateRange'
	EXEC ('CREATE PROCEDURE [dbo].[ContainerGetByDateRange] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ContainerGetByDateRange'
GO
ALTER PROCEDURE [dbo].[ContainerGetByDateRange]
	@StartDate	date,
	@EndDate	date
AS
BEGIN
	SET NOCOUNT ON;

	select		
		cs.ADEP		as AssemblyDepotNumber,
		cs.CNUM		as Number,
		cs.DDAT		as DateDespatch,
		cs.DESCR	as 'Description',
		cs.DDEP		as DespatchDepotNumber,
		cs.VREF		as VehicleLoadReference,
		cs.TOTL		as NumberLines,
		cs.PNUM		as ParentNumber,
		cs.IREC		as IsReceived,
		cs.IPRT		as PrintRequired,
		cs.DELD		as DateDelivery,
		cs.VALU		as Value,
		cs.APRT		as AutomaticallyPrinted,
		cs.REJI		as RejectedReason
	from		
		CONSUM cs
	where
		cs.DDAT >= @StartDate
		and cs.DDAT <= @EndDate

END
GO

