﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetZeroStocks]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetZeroStocks'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetZeroStocks] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetZeroStocks'
GO
ALTER PROCEDURE [dbo].[StockGetZeroStocks]
AS
BEGIN
	SET NOCOUNT ON;

	select
		sk.SKUN					as 'SkuNumber',
		sk.DESCR				as 'Description',
		sk.DREC					as 'DateLastReceived',		
		sk.ONHA					as 'QtyOnHand',
		sk.CYDO					as 'QtyDaysZeroStock',
		''						as 'Comment',
		(select case
			when sk.inon=1 then 'Non Stock Products'
			when sk.NOOR=1 then 'Non Orderable Stock'
			when sk.IOBS=1 then 'Obsolete/ Deleted Items'
			when sk.IDEL=1 then 'Obsolete/ Deleted Items'
			else 'Stock - No Restrictions'
		end)					as 'Status'
	from
		STKMAS sk
	where
		sk.CYDO>0
	order by
		sk.SKUN

END
GO

