﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderTextInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderTextInsert'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderTextInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderTextInsert'
GO
ALTER PROCEDURE [dbo].[SaleOrderTextInsert]
@OrderNumber CHAR (6), @Number CHAR (2), @Type CHAR (2), @SellingStoreId INT=0, @SellingStoreOrderId INT=0, @Text VARCHAR (200)
AS
BEGIN
	SET NOCOUNT ON;
	
	--insert order line
	insert into	CORTXT 
		(
		NUMB,
		LINE,
		[TYPE],
		SellingStoreId,
		SellingStoreOrderId,
		[TEXT]
		)
	values		(
		@orderNumber,
		@number,
		@Type,
		@sellingStoreId,
		@sellingStoreOrderId,
		@Text
	)
	
	return @@rowcount
END
GO

