﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_INSSER]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_INSSER'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_INSSER] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_INSSER'
GO
ALTER PROCEDURE dbo.KevanConversion_INSSER
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 27th December 2010
-- 
-- Task     : 35 - Install Server Workstation ID for Back Office
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF;

IF EXISTS (Select WSID From WSOCTL Where WSID = '60')
	Begin
		Print ('Server WSID (Workstation Control) already installed..')
	End
ELSE
	Begin
		Print ('Server WSID (Workstation Control) Installing..')
		INSERT [WSOCTL] ([WSID], [DESCR], [NODE], [PFCT], [ACTV], [TGRP1], [TGRP2], [TGRP3], [TGRP4], [TGRP5], [TGRP6], [TGRP7], [TGRP8], [TGRP9], [IAFG1], [IAFG2], [IAFG3], [IAFG4], [IAFG5], [IAFG6], [IAFG7], [IAFG8], [IAFG9], [IAFG10], [IAFG11], [IAFG12], [IAFG13], [IAFG14], [IAFG15], [IAFG16], [IAFG17], [IAFG18], [IAFG19], [IAFG20], [IAFG21], [IAFG22], [IAFG23], [IAFG24], [IAFG25], [IAFG26], [IAFG27], [IAFG28], [IAFG29], [IAFG30], [IAFG31], [IAFG32], [IAFG33], [IAFG34], [IAFG35], [IAFG36], [IAFG37], [IAFG38], [IAFG39], [IAFG40], [IAFG41], [IAFG42], [IAFG43], [IAFG44], [IAFG45], [IAFG46], [IAFG47], [IAFG48], [IAFG49], [IAFG50], [IAFG51], [IAFG52], [IAFG53], [IAFG54], [IAFG55], [IAFG56], [IAFG57], [IAFG58], [IAFG59], [IAFG60], [IAFG61], [IAFG62], [IAFG63], [IAFG64], [IAFG65], [IAFG66], [IAFG67], [IAFG68], [IAFG69], [IAFG70], [IAFG71], [IAFG72], [IAFG73], [IAFG74], [IAFG75], [IAFG76], [IAFG77], [IAFG78], [IAFG79], [IAFG80], [IAFG81], [IAFG82], [IAFG83], [IAFG84], [IAFG85], [IAFG86], [IAFG87], [IAFG88], [IAFG89], [IAFG90], [IAFG91], [IAFG92], [IAFG93], [IAFG94], [IAFG95], [IAFG96], [IAFG97], [IAFG98], [IAFG99], [DLog], [BCOD], [TOUCH], [WACT], [QUOTTILL], [CSDRAWER], [USEEFT], [PRNTTYPE]) VALUES (N'60', N'Server', N'', N'04', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, N'N', N'N', 0, N'Y', N'N', N'N', N'A')
	End


IF EXISTS (Select ID From WorkstationConfig Where ID = '60')
	Begin
		Print ('Server WSID (Workstation Configuration) already installed..')
	End
ELSE
	Begin
		Print ('Server WSID (Workstation Configuration) Installing..')
		INSERT [WorkStationConfig] ([ID], [Description], [PrimaryFunction], [IsActive], [DateLastLoggedIn], [IsBarcodeBroken], [UseTouchScreen], [SecurityProfileID]) VALUES (60, N'Server', N'RF', 0, NULL, 0, 0, 40)
	End


END;
GO

