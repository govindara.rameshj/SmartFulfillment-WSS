﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashDeliveries]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashDeliveries'
	EXEC ('CREATE PROCEDURE [dbo].[DashDeliveries] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashDeliveries'
GO
ALTER PROCEDURE [dbo].[DashDeliveries]
	@DateEnd	date
as
begin
set nocount on

	declare @table table(
		Description	varchar(50),
		Qty			int,
		QtyWtd		int,
		Value		dec(11,2),
		ValueWtd	dec(11,2));
	declare
		@StartDate			date,
		@MerchantDayQty	int,
		@MerchantDayValue	dec(11,2),		
		@MerchantWeekQty	int,		
		@MerchantWeekValue	dec(11,2),
		@ChargesDayQty		int,
		@ChargesDayValue	dec(11,2),		
		@ChargesWeekQty		int,		
		@ChargesWeekValue	dec(11,2),
		@CoreDayValue		dec(11,2),
		@CoreWeekValue		dec(11,2);	
	
	--set startdate to sunday before enddate
	set	@StartDate = @DateEnd;
	While datepart(weekday, @StartDate) <> 1
	begin
		set @StartDate = dateadd(day, -1, @StartDate)
	end	
	
	--get delivery goods/charges goods today
	select
		@MerchantDayQty		= count(ch.MVST),	
	 	@MerchantDayValue	= sum(ch.MVST),
	 	@ChargesDayQty		= count(ch.DCST),
	 	@ChargesDayValue	= sum(ch.DCST)
	from	
		corhdr ch
	inner join
		quohdr qh	on	qh.numb = ch.numb 
					and qh.cust = ch.cust
					and qh.sold = 1
	where	
		ch.DATE1	= @DateEnd
		and ch.canc <> 1
	
	--get delivery goods/charges week
	select
		@MerchantWeekQty	= count(case qh.SOLD when 1 then ch.MVST end),	
	 	@MerchantWeekValue	= sum(case qh.SOLD when 1 then ch.MVST else 0 end),
	 	@ChargesWeekQty		= count(ch.DCST),
	 	@ChargesWeekValue	= sum(ch.DCST)
	from	
		corhdr ch
	inner join
		quohdr qh	on	qh.numb = ch.numb 
					and qh.cust = ch.cust
					and qh.sold = 1
	where	
		ch.DATE1	<= @DateEnd
		and ch.date1	>= @StartDate 
		and ch.canc <> 1	
	
	--get core sales today info
	select
		@CoreDayValue		= sum(dl.totl)	
	from	
		dltots dl
	where	
		dl.DATE1	= @DateEnd
	and	dl.CASH		<> '000'
	and	dl.VOID		= 0
	and	dl.PARK		= 0
	and	dl.TMOD		= 0
	and dl.TCOD		= 'SA';
	
	--get core sales week to date info
	select
		@CoreWeekValue		= sum(dl.totl)	
	from	
		dltots dl
	where	
		dl.DATE1	<= @DateEnd
	and dl.date1	>= @StartDate
	and	dl.CASH		<> '000'
	and	dl.VOID		= 0
	and	dl.PARK		= 0
	and	dl.TMOD		= 0
	and dl.TCOD		= 'SA';
	
	if @CoreDayValue <> 0 set @CoreDayValue = (@MerchantDayValue / @CoreDayValue) * 100;	
	if @CoreWeekValue <> 0 set @CoreWeekValue = (@MerchantWeekValue / @CoreWeekValue) * 100;
	
	--return values
	insert into @table values ('Delivery Goods', @MerchantDayQty, @MerchantWeekQty, @MerchantDayValue, @MerchantWeekValue) ;
	insert into @table values ('Delivery charges', @ChargesDayQty, @ChargesWeekQty, @ChargesDayValue, @ChargesWeekValue) ;
	insert into @table(Description, Value, ValueWtd) values ('Percentage Delivery to Sales', @CoreDayValue, @CoreWeekValue) ;

	select * from @table
end
GO

