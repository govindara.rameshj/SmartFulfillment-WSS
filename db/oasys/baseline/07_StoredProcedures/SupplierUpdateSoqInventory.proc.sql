﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierUpdateSoqInventory]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierUpdateSoqInventory'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierUpdateSoqInventory] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierUpdateSoqInventory'
GO
ALTER PROCEDURE [dbo].[SupplierUpdateSoqInventory]
	@Number		char(5),
	@Suggested	bit,
	@SoqDate	date,
	@SoqOrdered bit,	
	@SoqNumber	int=null output

AS
BEGIN
	SET NOCOUNT ON;

	--if suggested then get next soq number
	if @Suggested = 1
	begin
		exec	@SoqNumber = SystemNumbersGetNext @Id=6
	end

	--update supplier
	update
		SUPMAS
	set
		QCTL	= @SoqNumber,
		SOQDate = @SoqDate,
		QFLG	= @SoqOrdered
	where
		SUPN	= @Number

END
GO

