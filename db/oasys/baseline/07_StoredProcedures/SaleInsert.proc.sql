﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleInsert'
	EXEC ('CREATE PROCEDURE [dbo].[SaleInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleInsert'
GO
ALTER PROCEDURE [dbo].[SaleInsert]
@TransactionDate DATE, 
@TillNumber CHAR (2),
@TransactionNumber CHAR (4), 
@TransactionTime CHAR (6),
@CashierID CHAR (3), 
@SupervisorCashierNo CHAR (3), 
@TransactionCode CHAR (2), 
@ODCode INT, 
@ReasonCode INT, 
@Description CHAR (20), 
@OrderNumber CHAR (6), 
@AccountSale BIT=0, 
@Void BIT=0, 
@VoidSupervisor CHAR (3), 
@TrainingMode BIT=0, 
@Processed BIT=0, 
@DocumentNumber CHAR (8), 
@SupervisorUsed BIT=0,
@StoreNumber CHAR (3), 
@MerchandiseAmount DECIMAL (9, 2)=0, 
@NonMerchandiseAmount DECIMAL (9, 2)=0, 
@TaxAmount DECIMAL (9, 2)=0, 
@Discount DECIMAL (9, 2)=0, 
@DiscountSupervisor CHAR (3), 
@TotalSaleAmount DECIMAL (9, 2)=0, 
@AccountNumber CHAR (6), 
@AccountCardNumber CHAR (6), 
@PCCollection BIT=0, 
@FromDCOrders BIT=0, 
@TransactionComplete BIT=1, 
@EmployeeDiscountOnly BIT=0, 
@RefundCashierNo CHAR (3), 
@RefundSupervisor CHAR (3), 
@VATRate1 DECIMAL (5, 3)=0, 
@VATRate2 DECIMAL (5, 3)=0, 
@VATRate3 DECIMAL (5, 3)=0, 
@VATRate4 DECIMAL (5, 3)=0, 
@VATRate5 DECIMAL (5, 3)=0, 
@VATRate6 DECIMAL (5, 3)=0, 
@VATRate7 DECIMAL (5, 3)=0, 
@VATRate8 DECIMAL (5, 3)=0, 
@VATRate9 DECIMAL (5, 3)=0, 
@VATSymb1 CHAR (1),
@VATSymb2 CHAR (1), 
@VATSymb3 CHAR (1), 
@VATSymb4 CHAR (1), 
@VATSymb5 CHAR (1), 
@VATSymb6 CHAR (1), 
@VATSymb7 CHAR (1), 
@VATSymb8 CHAR (1), 
@VATSymb9 CHAR (1), 
@XVATAmnt1 DECIMAL (9, 2)=0,
@XVATAmnt2 DECIMAL (9, 2)=0,
@XVATAmnt3 DECIMAL (9, 2)=0,
@XVATAmnt4 DECIMAL (9, 2)=0,
@XVATAmnt5 DECIMAL (9, 2)=0,
@XVATAmnt6 DECIMAL (9, 2)=0,
@XVATAmnt7 DECIMAL (9, 2)=0,
@XVATAmnt8 DECIMAL (9, 2)=0,
@XVATAmnt9 DECIMAL (9, 2)=0,
@VATAmnt1 DECIMAL (9, 2)=0, 
@VATAmnt2 DECIMAL (9, 2)=0, 
@VATAmnt3 DECIMAL (9, 2)=0, 
@VATAmnt4 DECIMAL (9, 2)=0, 
@VATAmnt5 DECIMAL (9, 2)=0, 
@VATAmnt6 DECIMAL (9, 2)=0, 
@VATAmnt7 DECIMAL (9, 2)=0, 
@VATAmnt8 DECIMAL (9, 2)=0, 
@VATAmnt9 DECIMAL (9, 2)=0,
@Parked BIT=0, 
@RefundManager CHAR (3), 
@TenderOverrideCode CHAR (2), 
@UnParked BIT=0, 
@TransactionOnLine BIT=1, 
@TokensPrinted DECIMAL (3, 0)=0, 
@ColleagueNumber CHAR (9), 
@SaveStatus CHAR (2) , 
@SaveSequence CHAR (4), 
@CBBUpdate CHAR (8), 
@DiscountCardNo CHAR (19), 
@RTI CHAR (1),
@receivedDate Datetime = null

AS
BEGIN
	SET NOCOUNT ON;
	
	--insert order line
	insert into	DLTOTS 
	([DATE1]
      ,[TILL]
      ,[TRAN]
      ,[CASH]
      ,[TIME]
      ,[SUPV]
      ,[TCOD]
      ,[OPEN]
      ,[MISC]
      ,[DESCR]
      ,[ORDN]
      ,[ACCT]
      ,[VOID]
      ,[VSUP]
      ,[TMOD]
      ,[PROC]
      ,[DOCN]
      ,[SUSE]
      ,[STOR]
      ,[MERC]
      ,[NMER]
      ,[TAXA]
      ,[DISC]
      ,[DSUP]
      ,[TOTL]
      ,[ACCN]
      ,[CARD]
      ,[AUPD]
      ,[BACK]
      ,[ICOM]
      ,[IEMP]
      ,[RCAS]
      ,[RSUP]
      ,[VATR1]
      ,[VATR2]
      ,[VATR3]
      ,[VATR4]
      ,[VATR5]
      ,[VATR6]
      ,[VATR7]
      ,[VATR8]
      ,[VATR9]
      ,[VSYM1]
      ,[VSYM2]
      ,[VSYM3]
      ,[VSYM4]
      ,[VSYM5]
      ,[VSYM6]
      ,[VSYM7]
      ,[VSYM8]
      ,[VSYM9]
      ,[XVAT1]
      ,[XVAT2]
      ,[XVAT3]
      ,[XVAT4]
      ,[XVAT5]
      ,[XVAT6]
      ,[XVAT7]
      ,[XVAT8]
      ,[XVAT9]
      ,[VATV1]
      ,[VATV2]
      ,[VATV3]
      ,[VATV4]
      ,[VATV5]
      ,[VATV6]
      ,[VATV7]
      ,[VATV8]
      ,[VATV9]
      ,[PARK]
      ,[RMAN]
      ,[TOCD]
      ,[PKRC]
      ,[REMO]
      ,[GTPN]
      ,[CCRD]
      ,[SSTA]
      ,[SSEQ]
      ,[CBBU]
      ,[CARD_NO]
      ,[RTI],
      ReceivedDate)

	values		(
		@TransactionDate,
		@TillNumber,
		@TransactionNumber,
		@CashierID,
		@TransactionTime,
		@SupervisorCashierNo,
		@TransactionCode ,
		@ODCode,
		@ReasonCode,
		@Description,
		@OrderNumber,
		@AccountSale ,
		@Void,
		@VoidSupervisor,
		@TrainingMode,
		@Processed,
		@DocumentNumber,
		@SupervisorUsed,
		@StoreNumber,
		@MerchandiseAmount,
		@NonMerchandiseAmount,
		@TaxAmount,
		@Discount,
		@DiscountSupervisor,
		@TotalSaleAmount,
		@AccountNumber,
		@AccountCardNumber,
		@PCCollection,
		@FromDCOrders,
		@TransactionComplete,
		@EmployeeDiscountOnly,
		@RefundCashierNo,
		@RefundSupervisor,
		@VATRate1,
		@VATRate2,
		@VATRate3,
		@VATRate4,
		@VATRate5,
		@VATRate6,
		@VATRate7,
		@VATRate8,
		@VATRate9,
		@VATSymb1,
		@VATSymb2,
		@VATSymb3,
		@VATSymb4,
		@VATSymb5,
		@VATSymb6,
		@VATSymb7,
		@VATSymb8,
		@VATSymb9,
		@XVATAmnt1,
		@XVATAmnt2,
		@XVATAmnt3,
		@XVATAmnt4,
		@XVATAmnt5,
		@XVATAmnt6,
		@XVATAmnt7,
		@XVATAmnt8,
		@XVATAmnt9,
		@VATAmnt1,
		@VATAmnt2,
		@VATAmnt3,
		@VATAmnt4,
		@VATAmnt5,
		@VATAmnt6,
		@VATAmnt7,
		@VATAmnt8,
		@VATAmnt9,
		@Parked,
		@RefundManager,
		@TenderOverrideCode,
		@UnParked,
		@TransactionOnLine ,
		@TokensPrinted,
		@ColleagueNumber,
		@SaveStatus,
		@SaveSequence,
		@CBBUpdate,
		@DiscountCardNo,
		@RTI,
		coalesce(@receivedDate, GetDate())
	)
	
	return @@rowcount
END
GO

