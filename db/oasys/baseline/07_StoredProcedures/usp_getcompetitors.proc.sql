﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_getcompetitors]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_getcompetitors'
	EXEC ('CREATE PROCEDURE [dbo].[usp_getcompetitors] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_getcompetitors'
GO
ALTER PROCEDURE usp_getcompetitors 
AS 
  BEGIN 
      SET nocount ON; 

      SELECT [Name], 
             TpGroup 
      FROM   CompetitorLocalList
	  WHERE  DeleteFlag = 0 	  
      UNION 
      SELECT [Name], 
             TpGroup 
      FROM   competitorFixedList cf 
      WHERE  cf.DeleteFlag = 0 
      ORDER  BY Name 
  END
GO

