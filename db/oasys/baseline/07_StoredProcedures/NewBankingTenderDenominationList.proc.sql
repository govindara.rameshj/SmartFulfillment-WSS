﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingTenderDenominationList]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingTenderDenominationList'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingTenderDenominationList] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingTenderDenominationList'
GO
ALTER PROCedure NewBankingTenderDenominationList
as
begin
set nocount on
--cash : amenable
select DenominationID  = ID,
       TenderText      = DisplayText,
       TenderID        = TenderID,
       TenderReadOnly  = cast(0 as bit),
       BullionMultiple,
       SafeMinimum 
from SystemCurrencyDen
where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
and   TenderID   = 1
union all
--non cash tender : amendable
select DenominationID = ID,
       TenderText     = DisplayText,
       TenderID       = TenderID,
       TenderReadOnly = cast(0 as bit),
       BullionMultiple,
       SafeMinimum 
from SystemCurrencyDen
where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
and   TenderID  <> 1
and Amendable    = 1
union all
--non cash tender : read-only
select DenominationID = ID,
       TenderText     = DisplayText,
       TenderID       = TenderID,
       TenderReadOnly = cast(1 as bit),
       BullionMultiple,
       SafeMinimum 
from SystemCurrencyDen
where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
and   TenderID  <> 1
and Amendable    = 0
end
GO

