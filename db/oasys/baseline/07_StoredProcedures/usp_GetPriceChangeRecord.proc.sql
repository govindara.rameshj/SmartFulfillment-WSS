﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetPriceChangeRecord]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetPriceChangeRecord'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetPriceChangeRecord] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetPriceChangeRecord'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 30/03/2012
-- Description:	Select a specific Price Change Record
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetPriceChangeRecord] 
	@Skun Char(6),
	@StartDate DATE,
	@Status Char(1)
	
AS
BEGIN
	SET NOCOUNT ON;

Select [SKUN]
      ,[PDAT]
      ,[PRIC]
      ,[PSTA]
      ,[SHEL]
      ,[AUDT]
      ,[AUAP]
      ,[MARK]
      ,[MCOM]
      ,[LABS]
      ,[LABM]
      ,[LABL]
      ,[EVNT]
      ,[PRIO]
      ,[EEID]
      ,[MEID]
  FROM [Oasys].[dbo].[PRCCHG]
  Where
       [Skun]= @Skun And 
	   [PDAT] = @StartDate And 
       [PSTA] = @Status
End
GO

