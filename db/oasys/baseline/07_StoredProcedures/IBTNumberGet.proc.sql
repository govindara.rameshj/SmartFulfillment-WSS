﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[IBTNumberGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure IBTNumberGet'
	EXEC ('CREATE PROCEDURE [dbo].[IBTNumberGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure IBTNumberGet'
GO
ALTER PROCEDURE [dbo].[IBTNumberGet] 
	
AS
BEGIN
	SET NOCOUNT ON;

	--get next receipt number from system numbers
	declare @NextNumber int;

	-- call SystemNumbersGetNext, which will get the next available number and return the one to use
	exec  @NextNumber = SystemNumbersGetNext @Id=4
  
	-- return the IBT number
	Select Right('000000' + Convert(Char(6), @NextNumber), 6)

END
GO

