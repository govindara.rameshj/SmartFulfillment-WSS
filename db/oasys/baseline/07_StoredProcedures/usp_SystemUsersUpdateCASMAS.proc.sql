﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SystemUsersUpdateCASMAS]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SystemUsersUpdateCASMAS'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SystemUsersUpdateCASMAS] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SystemUsersUpdateCASMAS'
GO
ALTER PROCEDURE dbo.usp_SystemUsersUpdateCASMAS
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 8th March 2011
-- 
-- Task     : Updates / Inserts Users into CASMAS table for use by Till Systems.
-- Notes	: SP updates or inserts data as old data is sometimes present. Updating or Inserting the
--            data will prevent the stored procedure from failing. Data in Pilot stores has shown that
--            existing data can or may not already exist.
--
-- Revision : 1.1
-- Author   : Partha Dutta
-- Date	    : 16/05/2011
--
-- Reason   : Casmas->Posi typed char(15) but parameter @sp_Position typed char(20) causing a
--            "...string truncation..." error when update SecurityProfile
--
-- Solution : Pass only 15 characters to Casmas
--
-----------------------------------------------------------------------------------
	
		@sp_EmployeeCode			char(3),
		@sp_Position				char(20),
		@sp_Password				char(5),
		@sp_IsSupervisor			bit,
		@sp_IsDeleted				bit,
		@sp_TillReceiptName			char(35),
		@sp_DefaultAmount			dec(9,2)

AS
BEGIN
	SET NOCOUNT ON;

--------------------------------------------------------------------------------------------------------------------------
-- Insert New / Update Existing User into CASMAS Table
--------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (Select CASH FROM [Oasys].[dbo].[CASMAS] WHERE CASH = @sp_EmployeeCode)
	Begin
		insert into CASMAS 
				(
				cash,delc,name,posi,code,supv,flot,
				SKUL1,SKUL2,SKUL3,SKUL4,SKUL5,SKUL6,SKUL7,DPTL1,DPTL2,DPTL3,DPTL4,DPTL5,DPTL6,DPTL7,
				SKUS1,SKUS2,SKUS3,SKUS4,SKUS5,SKUS6,SKUS7,DPTS1,DPTS2,DPTS3,DPTS4,DPTS5,DPTS6,DPTS7,
				SKUC1,SKUC2,SKUC3,SKUC4,SKUC5,SKUC6,SKUC7,DPTC1,DPTC2,DPTC3,DPTC4,DPTC5,DPTC6,DPTC7,
				PVNO1,PVNO2,PVNO3,PVNO4,PVNO5,PVNO6,PVNO7,PVVA1,PVVA2,PVVA3,PVVA4,PVVA5,PVVA6,PVVA7,
				PSSF,PSNQ1,PSNQ2,PSNQ3,PSNQ4,PSNQ5,PSNQ6,PSNC1,PSNC2,PSNC3,PSNC4,PSNC5,PSNC6,
				PSNS1,PSNS2,PSNS3,PSNS4,PSNS5,PSNS6,PSNF1,PSNF2,PSNF3,PSNF4,PSNF5,PSNF6,
				PSVS1,PSVS2,PSVS3,PSVS4,PSVS5,PSVS6,PSVF1,PSVF2,PSVF3,PSVF4,PSVF5,PSVF6,
				PSIV1,PSIV2,PSIV3,PSIV4,PSIV5,PSIV6,PSAV1,PSAV2,PSAV3,PSAV4,PSAV5,PSAV6,
				PSCV1,PSCV2,PSCV3,PSCV4,PSCV5,PSCV6,PSTV1,PSTV2,PSTV3,PSTV4,PSTV5,PSTV6,
				PSCA1,PSCA2,PSCA3,PSCA4,PSCA5,PSCA6,SPARE
				) 
		values	( 
				@sp_EmployeeCode,@sp_IsDeleted,LEFT(LTRIM(@sp_TillReceiptName),22),LEFT(LTRIM(@sp_Position), 15),@sp_Password,@sp_IsSupervisor,@sp_DefaultAmount,
				'0','0','0','0','0','0','0','0','0','0','0','0','0','0',
				'0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00',
				'0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00',
				'0','0','0','0','0','0','0','0.00','0.00','0.00','0.00','0.00','0.00','0.00',
				'','0','0','0','0','0','0','0','0','0','0','0','0',
				'0','0','0','0','0','0','0','0','0','0','0','0',
				'0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00',
				'0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00',
				'0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00',
				'0.00','0.00','0.00','0.00','0.00','0.00',''
				)	
	End
ELSE
	Begin
		Update	CASMAS
		Set		CASH = @sp_EmployeeCode, DELC = @sp_IsDeleted, NAME = LEFT(LTRIM(@sp_TillReceiptName),22), POSI = LEFT(LTRIM(@sp_Position), 15), CODE = @sp_Password, SUPV = @sp_IsSupervisor, FLOT = @sp_DefaultAmount
		Where	CASH = @sp_EmployeeCode
	End

			
END
GO

