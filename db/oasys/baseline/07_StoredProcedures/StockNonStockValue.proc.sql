﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockNonStockValue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockNonStockValue'
	EXEC ('CREATE PROCEDURE [dbo].[StockNonStockValue] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockNonStockValue'
GO
ALTER PROCEDURE [dbo].[StockNonStockValue]
	@Value DECIMAL (9, 2) OUTPUT
AS
begin
	SELECT 
		@Value = SUM(S.ONHA*S.PRIC) 
	FROM 
		STKMAS S 
	WHERE	
		S.INON = 1;
	
	SELECT @Value = coalesce(@Value, 0);

end
GO

