﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[PoUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoUpdate'
GO
ALTER PROCEDURE [dbo].[PoUpdate]
	@Id			int,
	@IsDeleted	bit,
	@DateDue	date,
	@Cartons	int,
	@Value		dec(9,2),
	@Units		int,
	@IsCommed	bit=0
AS
BEGIN
	SET NOCOUNT ON;

	declare @SupplierNumber	char(5);

	--update purchase header
	update
		PURHDR
	set
		DDAT = @DateDue,
		DELM = @IsDeleted,
		NOCR = @Cartons,
		VALU = @Value,
		QTYO = @Units,
		COMM = @IsCommed
	where
		TKEY = @Id;
	
	--update supplier as ordered
	set @SupplierNumber = (select SUPP from PURHDR where TKEY = @Id);
	update
		SUPMAS
	set
		OPON = OPON + 1,
		OPOV = OPOV + @Value,
		QTYO1 = QTYO1 + @Value,
		DLPO = GETDATE(),
		QCTL = null,
		QFLG = 1
	where
		SUPN = @SupplierNumber;
		
	--reset all stock item order level for this supplier
	update
		STKMAS
	set
		OrderLevel = 0
	where
		SUP1 = @SupplierNumber;
	
END
GO

