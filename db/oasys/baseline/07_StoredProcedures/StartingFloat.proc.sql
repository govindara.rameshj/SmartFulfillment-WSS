﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StartingFloat]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StartingFloat'
	EXEC ('CREATE PROCEDURE [dbo].[StartingFloat] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StartingFloat'
GO
--"manual check" & un-assigned floats need to maintain its link to the pickup that created it
ALTER PROCEDURE [dbo].[StartingFloat]
   @FloatID      INT,
   @StartFloatID INT OUTPUT
AS
BEGIN
SET NOCOUNT ON
	DECLARE @RelatedBagID INT
	DECLARE @Iteration INT
	
	SET @Iteration = 0
	
	WHILE 1 = 1 --previous cancelled float exists
		BEGIN
			SET @Iteration = @Iteration + 1
			
			IF @Iteration > 1000
				BEGIN
					RAISERROR('Could not find StartingFloat within 1000 iterations.', 16, 3)
					RETURN -1					
				END
				
			SET @RelatedBagID = (SELECT RelatedBagID FROM SafeBags WHERE ID = @FloatID)
			
			IF @RelatedBagID IS NULL
				BEGIN
					RAISERROR('Bag is not defined.', 16, 3)
					RETURN -1
				END

			IF @RelatedBagID = 0
				BEGIN
					--start of chain
					SET @StartFloatID = @FloatID
					BREAK
				END
			ELSE
				BEGIN
					SET @FloatID = @RelatedBagID
				END			
		END	
END
GO

