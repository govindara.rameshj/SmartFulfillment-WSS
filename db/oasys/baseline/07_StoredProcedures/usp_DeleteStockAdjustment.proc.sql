﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_DeleteStockAdjustment]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_DeleteStockAdjustment'
	EXEC ('CREATE PROCEDURE [dbo].[usp_DeleteStockAdjustment] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_DeleteStockAdjustment'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 27/06/2011
-- Description:	Delete Stock Adjustment
-- =============================================
ALTER PROCEDURE [dbo].[usp_DeleteStockAdjustment] 
	-- Add the parameters for the stored procedure here
	@SKUN Char(6) , 
	@AdjustmentDate date,
	@AdjustmentCode char(2),
	@SEQN char(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM STKADJ 
	WHERE 
		DATE1 = @AdjustmentDate And 
		CODE =	@AdjustmentCode and
		SKUN = @SKUN And
		SEQN = @SEQN	
END
GO

