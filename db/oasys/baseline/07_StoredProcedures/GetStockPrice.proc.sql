﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetStockPrice]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GetStockPrice'
	EXEC ('CREATE PROCEDURE [dbo].[GetStockPrice] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GetStockPrice'
GO
ALTER PROCEDURE dbo.GetStockPrice 
	@SkuNumber Char(6)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT st.Pric From STKMAS st Where Skun = @SkuNumber 
END
GO

