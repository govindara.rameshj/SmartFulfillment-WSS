﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PicWorksheetSupplier]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PicWorksheetSupplier'
	EXEC ('CREATE PROCEDURE [dbo].[PicWorksheetSupplier] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PicWorksheetSupplier'
GO
ALTER PROCEDURE [dbo].[PicWorksheetSupplier]
@SupplierNumber CHAR (5)=null
AS
BEGIN
	SET NOCOUNT ON;

	select
		sk.SKUN				as 'SkuNumber',
		sk.DESCR			as 'Description',
		sk.PACK				as 'PackSize',		
		(case sk.IRIS when 1 then (select rl.PPOS from RELITM rl where rl.SPOS=sk.SKUN) end) as 'SkuRelated',
		''					as 'QtyShopFloor',
		''					as 'QtyWarehouse',
		''					as 'QtyTotal',
		''					as 'QtyPreSold',
		sk.ONHA				as 'QtyOnHand',
		''					as 'QtyVariance',
		sk.PRIC				as 'Price',
		'YES / NO'			as 'SELYesNo',
		sm.NAME				as 'SupplierName'
	from
		STKMAS sk
	inner join
		SUPMAS sm on sk.SUPP=sm.SUPN
	where
		(@SupplierNumber is null) or (@SupplierNumber is not null and sk.SUPP = @SupplierNumber)
	order by
		sk.SKUN

END
GO

