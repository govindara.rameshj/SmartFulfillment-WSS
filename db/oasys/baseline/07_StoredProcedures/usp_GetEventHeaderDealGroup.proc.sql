﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEventHeaderDealGroup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEventHeaderDealGroup'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEventHeaderDealGroup] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEventHeaderDealGroup'
GO
-- =============================================================================== 
-- Author         : Dhanesh Ramachandran 
-- Date           : 04/01/2012 
-- Project        : PO14-04 
-- TFS User Story : 2155 
-- TFS Task ID    : 3820 
-- Description    : Rollout Script  
-- =============================================================================== 
ALTER PROCEDURE usp_GetEventHeaderDealGroup
 --@EventNumber AS VARCHAR(7),
 --@Priority    AS VARCHAR(7)

   @EventNumber char(6), 
   @Priority    char(2) 

AS 
  BEGIN 
      SELECT sdat  startdate, 
             stim  starttime, 
             edat  enddate, 
             descr DESCRIPTION, 
             dact1 activeday1, 
             dact2 activeday2, 
             dact3 activeday3, 
             dact4 activeday4, 
             dact5 activeday5, 
             dact6 activeday6, 
             dact7 activeday7 
      FROM   evthdr 
      WHERE  numb = @EventNumber 
             AND prio = @Priority 
             AND idel = 0 
      UNION 
      SELECT startdate, 
             '000' AS starttime, 
             enddate, 
             DESCRIPTION, 
             '1'   activeday1, 
             '1'   activeday2, 
             '1'   activeday3, 
             '1'   activeday4, 
             '1'   activeday5, 
             '1'   activeday6, 
             '1'   activeday7 
      FROM   eventheaderoverride 
    --WHERE  id = @EventNumber 
      where replicate('0', 6 - len(replace(cast(900000 + ID as char(6)), ' ', ''))) + replace(cast(900000 + ID as char(6)), ' ', '') = @EventNumber

  END
GO

