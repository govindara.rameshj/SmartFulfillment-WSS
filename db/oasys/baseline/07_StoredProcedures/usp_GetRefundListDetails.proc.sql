﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetRefundListDetails]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetRefundListDetails'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetRefundListDetails] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetRefundListDetails'
GO
ALTER PROCEDURE [dbo].[usp_GetRefundListDetails] @ReportDate AS date
AS
BEGIN

    SELECT CM.EmployeeCode , 
           CM.Name , 
           DT.TCOD , 
           DT.TOTL , 
           DT.RMAN , 
           DT.TILL , 
           DT.[TIME] , 
           DT.[TRAN] ,
           DT.[OVCTranNumber] , 
           DT.SUPV , 
           DT.RSUP , 
           DT.RCAS RefundCashier , 
           coalesce(RC.Name, 'Unknown') RefundCashierName,
           coalesce(SU.Name, 'Unknown') SupervisorName,
           coalesce(RM.Name, 'Unknown') ManagerName
      FROM
           SystemUsers CM 
            INNER JOIN vwDLTOTS DT 
                ON DT.CASH = CM.EmployeeCode
                    AND DT.DATE1 = @ReportDate
                    AND DT.VOID = 0
                    AND DT.TMOD = 0
                    AND (DT.TCOD = 'SA'
                        OR DT.TCOD = 'RF')
            LEFT OUTER JOIN SystemUsers RC
                ON RC.EmployeeCode = DT.RCAS
            LEFT OUTER JOIN SystemUsers SU
                ON SU.EmployeeCode = DT.RSUP
            LEFT OUTER JOIN SystemUsers RM
                ON RM.EmployeeCode = DT.RMAN
            LEFT JOIN vwCORHDRFull CH
                ON DT.ORDN = CH.NUMB 
      WHERE CH.IS_CLICK_AND_COLLECT IS NULL OR CH.IS_CLICK_AND_COLLECT = 0
      ORDER BY CM.EmployeeCode , DT.[TIME] , DT.TILL , DT.[TRAN]

END
GO

