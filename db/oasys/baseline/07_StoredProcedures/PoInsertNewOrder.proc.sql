﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoInsertNewOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoInsertNewOrder'
	EXEC ('CREATE PROCEDURE [dbo].[PoInsertNewOrder] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoInsertNewOrder'
GO
ALTER PROCEDURE [dbo].[PoInsertNewOrder]
	@SupplierNumber		char(5),
	@SupplierBbc		char(1),
	@SupplierTradanet	bit,
	@SoqNumber			int=null,
	@EmployeeId			int,
	@Id					int			output,
	@SourceEntry		char(1)		output,
	@Number				int=null	output,
	@IsDeleted			bit			output,
	@DateCreated		date		output
AS
BEGIN
	SET NOCOUNT ON;

	--get next purchase order number	
	exec	@Number = SystemNumbersGetNext @Id=1;

	--set initial values
	select
		@SourceEntry = 'S',
		@IsDeleted	 = 1,
		@DateCreated = GETDATE();
		
	--get source entry		
	if	(@SoqNumber is null or @SoqNumber=0) set @SourceEntry= 'E';
	
	--insert into purhdr
	insert into
		PURHDR (
		NUMB,
		SUPP,
		BBCC,
		TNET,
		SOQN,
		EmployeeId,
		SRCE,
		ODAT,
		DELM,
		COMM
	) values (
		@Number,
		@SupplierNumber,
		@SupplierBbc,
		@SupplierTradanet,
		@SoqNumber,
		@EmployeeId,
		@SourceEntry,
		@DateCreated,
		@IsDeleted,
		1);
	
	set @Id = @@IDENTITY
END
GO

