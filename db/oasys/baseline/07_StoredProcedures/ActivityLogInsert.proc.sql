﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ActivityLogInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ActivityLogInsert'
	EXEC ('CREATE PROCEDURE [dbo].[ActivityLogInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ActivityLogInsert'
GO
ALTER PROCEDURE [dbo].[ActivityLogInsert]
@MenuId INT, @UserId INT, @WorkstationId INT, @loggedIn BIT, @loggedOutForced BIT, @Id INT OUTPUT
AS
begin
	set nocount on;
	
	declare @rowCount int;
	set @rowCount=0;
	
	INSERT INTO 
		ActivityLog(
		LogDate,
		EmployeeID,
		WorkstationID,
		MenuOptionID,
		LoggedIn,
		ForcedLogOut,
		StartTime,
		EndTime,
		AppStart
	) VALUES (
		GETDATE(),
		@UserId,
		convert(char(2), @WorkstationId),
		@MenuId,
		@loggedIn,
		@loggedOutForced,
		REPLACE(CONVERT(varchar, getdate(), 108), ':', ''),
		'',
		GETDATE()
		);
		
	set @rowCount = @@ROWCOUNT;
	set @Id = @@IDENTITY;		
	
	return @rowcount;
	
end
GO

