﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryReceipts]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryReceipts'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryReceipts] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryReceipts'
GO
-- Verion Control Information
-- =============================================
-- Author:  	Kevan Madelin
-- Create date: 21/09/2010
-- Version:	3.0.0.0
-- Description: This procedure is used to get the DRL Listing data for the SIE Dashboard.

ALTER PROCEDURE [dbo].[EnquiryReceipts]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	select
		ds.DATE1		as 'Date',
		ds.NUMB			as 'DrlNumber',
		case 
			when ds.TYPE = 0 then 'Receipt'
			when ds.TYPE = 1 then 'IBT In'
			when ds.TYPE = 2 then 'IBT Out'
			when DS.TYPE = 3 then 'Return'
		end as 'ReceiptType',
		case 
			when ds.TYPE = 0 then dt.RECQ
			when ds.TYPE = 1 then dt.IBTQ
			when ds.TYPE = 2 then dt.IBTQ
			when DS.TYPE = 3 then DT.RECQ
		end	as 'QtyReceived',
        case
			when ds.TYPE = 0 then 'P/O : ' + Convert(char, ds.[0PON])
			when ds.TYPE = 1 then ds.INFO
			when ds.TYPE = 2 then ds.INFO
			when ds.TYPE = 3 then 'Return Ref : ' + CONVERT(char, ds.[3PON])+ ds.info
		end as 'Comments',
        ds.EmployeeId as 'UserId'
/*
		ds.[INIT]		as 'UserInitials',
		ds.EmployeeId	as 'UserId',
		case 
			when ds.EmployeeId = 0 then '(Auto Issue Receipt)'
			when ds.EmployeeId > 0 then	su.Name
		end	as 'UserName'
*/

	from
		DRLDET dt
	inner join
		DRLSUM ds on ds.NUMB = dt.NUMB
	left outer join
		SystemUsers su on ds.EmployeeId = su.ID
	where
		dt.SKUN = @SkuNumber
	order by
		ds.DATE1 desc
END
GO

