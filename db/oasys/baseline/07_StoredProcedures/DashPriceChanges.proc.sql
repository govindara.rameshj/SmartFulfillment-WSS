﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashPriceChanges]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashPriceChanges'
	EXEC ('CREATE PROCEDURE [dbo].[DashPriceChanges] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashPriceChanges'
GO
ALTER PROCedure [dbo].[DashPriceChanges]
   @DateEnd date
as
begin
   set nocount on

	declare @table table(RowId int, Description varchar(50), Qty int);
	declare @WaitingQty	            	int,
            @OverdueQty		            int,
            @Small	            		int,
            @Medium	            		int,
            @Large			            int,
            @PriceChangeTodayQty        int,
            @PriceChangeConfirmationQty int;
	
	--get price changes waiting/overdue
	SELECT 
		@WaitingQty = COUNT(pc.PSTA),
		@OverdueQty = COUNT(case when pc.pdat < getdate() then pc.psta end) 
	FROM 
		PRCCHG pc 
	INNER JOIN 
		STKMAS sk ON sk.SKUN = pc.SKUN
	WHERE 
		pc.PSTA		= 'U'
		AND sk.AAPC	= 0;

	--get number labels required to be printed
	select
		@Small	= COUNT(case when pc.labs=1 then pc.skun end),
		@Medium	= COUNT(case when pc.labm=1 then pc.SKUN end),
		@Large	= COUNT(case when pc.labl=1 then pc.skun end)
	from 
		PRCCHG pc
	inner join
		STKMAS sk on sk.SKUN=pc.SKUN
	where
		pc.PSTA='U'
		and sk.AAPC=0

	--get price changes excluse non-stock items
    select @PriceChangeTodayQty = COUNT(*)
    from PRCCHG a
    inner join STKMAS b
          on b.SKUN = a.SKUN
    where b.INON = 0
    and   b.DPRC = (select TODT from SYSDAT)
    
    
    select @PriceChangeConfirmationQty	= count(*)
	from prcchg pc
	inner join stkmas st
          on st.skun = pc.skun
	where st.inon = 0
    and   st.dprc = @DateEnd

	--return values
	insert into @table values (1, 'Price Changes Waiting',      @WaitingQty) ;
	insert into @table values (2, 'Price Changes Overdue',      @OverdueQty) ;
	insert into @table values (3, 'Print Labels Required',      coalesce(@Small,0) + coalesce(@Medium,0) + coalesce(@Large,0)) ;
	insert into @table values (4, 'Price Changes Today',        @PriceChangeTodayQty)
	insert into @table values (5, 'Price Changes Confirmation', @PriceChangeConfirmationQty)


	select * from @table
end
GO

