﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HierarchyMasterGetAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure HierarchyMasterGetAll'
	EXEC ('CREATE PROCEDURE [dbo].[HierarchyMasterGetAll] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure HierarchyMasterGetAll'
GO
ALTER PROCEDURE [dbo].[HierarchyMasterGetAll]

AS
BEGIN
	SET NOCOUNT ON;

	select 
		hm.LEVL				as 'Level',
		hm.NUMB				as 'Number',
		hm.DESCR			as 'Description',
		hm.IDEL				as 'IsDeleted',
		hm.MaxOverride		as 'MaxOverride',
		hm.ReductionWeek1	as 'ReductionWeek1',
		hm.ReductionWeek2	as 'ReductionWeek2',
		hm.ReductionWeek3	as 'ReductionWeek3',
		hm.ReductionWeek4	as 'ReductionWeek4',
		hm.ReductionWeek5	as 'ReductionWeek5',
		hm.ReductionWeek6	as 'ReductionWeek6',
		hm.ReductionWeek7	as 'ReductionWeek7',
		hm.ReductionWeek8	as 'ReductionWeek8',
		hm.ReductionWeek9	as 'ReductionWeek9',
		hm.ReductionWeek10	as 'ReductionWeek10'
	from
		HIEMAS hm
	order by
		hm.LEVL, hm.NUMB

END
GO

