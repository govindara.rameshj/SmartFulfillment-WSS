﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_SetupStoreFinalBank]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_SetupStoreFinalBank'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_SetupStoreFinalBank] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_SetupStoreFinalBank'
GO
ALTER PROCEDURE dbo.usp_ITSupport_SetupStoreFinalBank
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 18th August 2011
-- Description	: Setup Store for Final Banking Procedure
-----------------------------------------------------------------------------------
@sp_Commit Char(1),
@sp_ManagersCode int

As
Begin
Set NoCount On

If (@sp_Commit = 'Y')
	Begin
		-----------------------------------------------------------------------------------
		-- Declare Working Variables
		-----------------------------------------------------------------------------------
		Declare @Period_Today int
		Declare @PeriodRec int = (Select LTRIM(RTRIM(Cast(LongValue as Int))) From [Parameters] Where ParameterID = '2200')  
		Declare @PeriodDat DateTime = (CAST(FLOOR(CAST(DateAdd(day, @PeriodRec*-1, GETDATE())AS FLOAT))AS DATETIME))
		Declare @Date_Today DateTime = (CAST(FLOOR(CAST(DateAdd(day, 0, GETDATE())AS FLOAT))AS DATETIME))

		If Exists (Select PeriodId From Oasys.Dbo.CashBalCashier Where CashierID = @sp_ManagersCode and PeriodID = @Period_Today)
			Begin
				Print ('ERROR - USER ASSIGNED TILL!!!')		
			End
		Else
			Begin
				Insert into Oasys.Dbo.CashBalCashier Values 
				(
				@Period_Today,'GBP',@sp_ManagersCode,'0.01','0.00','1','0.01','0','0.00','0','0.00','0','0.00','0.00','0.00',
				0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0, 
				'0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00',
				'0.01','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00', 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				'0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00',
				'0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00', 
				'0.0000','0','','1','0','0','0','0','0','0', NULL
				)
				
				Update [Parameters] Set DecimalValue = '0.01000' Where ParameterID = 2202
				Update SystemCurrencyDen Set BullionMultiple = '0.01', BankingBagLimit = '10000.00' Where ID = '0.01' and TenderID = 1 and DisplayText = '1p'
				Update SystemCurrencyDen Set BullionMultiple = '0.02', BankingBagLimit = '10000.00' Where ID = '0.02' and TenderID = 1 and DisplayText = '2p'
				Update SystemCurrencyDen Set BullionMultiple = '0.05', BankingBagLimit = '10000.00' Where ID = '0.05' and TenderID = 1 and DisplayText = '5p'
				Update SystemCurrencyDen Set BullionMultiple = '0.10', BankingBagLimit = '10000.00' Where ID = '0.10' and TenderID = 1 and DisplayText = '10p'
				Update SystemCurrencyDen Set BullionMultiple = '0.20', BankingBagLimit = '10000.00' Where ID = '0.20' and TenderID = 1 and DisplayText = '20p'
				Update SystemCurrencyDen Set BullionMultiple = '0.50', BankingBagLimit = '10000.00' Where ID = '0.50' and TenderID = 1 and DisplayText = '50p'
				Update SystemCurrencyDen Set BullionMultiple = '1.00', BankingBagLimit = '10000.00' Where ID = '1.00' and TenderID = 1 and DisplayText = '£1'
				Update SystemCurrencyDen Set BullionMultiple = '2.00', BankingBagLimit = '10000.00' Where ID = '2.00' and TenderID = 1 and DisplayText = '£2'
				Update SystemCurrencyDen Set BullionMultiple = '5.00', BankingBagLimit = '10000.00', SafeMinimum = '0.00' Where ID = '5.00' and TenderID = 1 and DisplayText = '£5'
				Update SystemCurrencyDen Set BullionMultiple = '10.00', BankingBagLimit = '10000.00' Where ID = '10.00' and TenderID = 1 and DisplayText = '£10'
				Update SystemCurrencyDen Set BullionMultiple = '20.00', BankingBagLimit = '10000.00' Where ID = '20.00' and TenderID = 1 and DisplayText = '£20'
				Update SystemCurrencyDen Set BullionMultiple = '50.00', BankingBagLimit = '10000.00' Where ID = '50.00' and TenderID = 1 and DisplayText = '£50'
				Update SystemCurrencyDen Set BullionMultiple = '100.00', BankingBagLimit = '10000.00' Where ID = '100.00' and TenderID = 1 and DisplayText = '£100'
				
				If @@ERROR = 0 Print ('Banking Procedure Configuration has Completed') Else Print ('Error - Setting Final Banking Procedure Configuration')
			End
		End
Else
	Begin
		Print ('Commit Not Enabled - DO NOT RUN THIS PROCEDURE ON LIVE STORE!!! Refer to Kevan Madelin for More Information..')
	End
End
GO

