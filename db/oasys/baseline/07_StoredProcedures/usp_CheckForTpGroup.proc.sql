﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CheckForTpGroup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_CheckForTpGroup'
	EXEC ('CREATE PROCEDURE [dbo].[usp_CheckForTpGroup] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_CheckForTpGroup'
GO
ALTER PROCEDURE usp_CheckForTpGroup @CompetitorName CHAR(30) = Null, 
                                     @Success        BIT output 
AS 
  BEGIN 
      SET nocount ON; 

      IF EXISTS(SELECT 1 
                FROM   CompetitorLocalList 
                WHERE  name = @CompetitorName 
                       AND tpgroup = 1 
                UNION 
                SELECT 1 
                FROM   competitorFixedList 
                WHERE  name = @CompetitorName 
                       AND tpgroup = 1) 
        BEGIN 
            SET @Success = 1 
        END 
      ELSE 
        BEGIN 
            SET @Success = 0 
        END 
  END
GO

