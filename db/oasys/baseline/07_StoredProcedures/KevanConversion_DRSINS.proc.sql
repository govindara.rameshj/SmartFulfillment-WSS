﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_DRSINS]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_DRSINS'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_DRSINS] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_DRSINS'
GO
ALTER PROCEDURE dbo.KevanConversion_DRSINS
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 08 - Disaster Recovery Install into NITMAS
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Task     : 08/01 - Install DR into NITMAS and make LIVE
-----------------------------------------------------------------------------------
IF EXISTS (SELECT Task FROM NITMAS WHERE Task = '005')
	Begin	
		Print ('Task 5 in Nightly Routine already exists, skipping creation..')
	End	
ELSE	
	Begin
		Print ('Task 5 in Nightly Routine does not exist, creating task..')
		INSERT [NITMAS] ([NSET], [TASK], [DESCR], [PROG], [NITE], [RETY], [WEEK], [PEND], [LIVE], [OPTN], [ABOR], [JRUN], [DAYS1], [DAYS2], [DAYS3], [DAYS4], [DAYS5], [DAYS6], [DAYS7], [BEGD], [ENDD], [SDAT], [STIM], [EDAT], [ETIM], [ADAT], [ATIM], [TTYPE]) VALUES (N'1', N'005', N'Backup Database Prior to Nightly        ', N'DRPNRBackup.exe                                                                                                                                       ', 1, 0, 0, 0, 1, NULL, 0, NULL, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
	End
END;
GO

