﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeUpdate'
GO
ALTER PROCedure NewBankingSafeUpdate

   @PeriodID                int,
   @EndOfDayCheckDone       bit,
   @EndOfDayCheckLockedFrom datetime,
   @EndOfDayCheckLockedTo   datetime,
   @EndOfDayCheckManagerID  int,
   @EndOfDayCheckWitnessID  int

as
begin
   set nocount on

   update Safe set EndOfDayCheckDone       = @EndOfDayCheckDone,
                   EndOfDayCheckCompleted  = getdate(),
                   EndOfDayCheckLockedFrom = @EndOfDayCheckLockedFrom,
                   EndOfDayCheckLockedTo   = @EndOfDayCheckLockedTo,
                   EndOfDayCheckManagerID  = @EndOfDayCheckManagerID,
                   EndOfDayCheckWitnessID  = @EndOfDayCheckWitnessID

   where PeriodID = @PeriodID

end
GO

