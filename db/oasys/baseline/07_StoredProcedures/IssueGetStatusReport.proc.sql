﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[IssueGetStatusReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure IssueGetStatusReport'
	EXEC ('CREATE PROCEDURE [dbo].[IssueGetStatusReport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure IssueGetStatusReport'
GO
ALTER PROCEDURE [dbo].[IssueGetStatusReport]
	@Date date
AS
BEGIN
	SET NOCOUNT ON;

	select
		ih.NUMB			as IssueNumber,
		ih.RECI			as IsReceived,
		ih.IDAT			as DateIssued,
		ih.SPON			as PoNumber,
		case ih.BBCC
			when 'W' then 'Warehouse'
			when 'A' then 'Alternative'
			when 'C' then 'Consolidated'
			when 'D' then 'Discreet'
			else 'Direct'
		end				as 'Type',
		ih.IMPI			as IsImport,
		ih.VALU			as Value,
		ih.SUPP			as SupplierNumber,
		sm.NAME			as SupplierName,
		cm.NUMB			as ContainerNumber,
		ih.DRLN			as ReceiptNumber,
		SUM(il.QTYI) 	as QtyIssued,
		SUM(il.QTYO)	as QtyOrdered
	from
		ISUHDR ih
	inner join
		SUPMAS sm on ih.SUPP=sm.SUPN
	inner join
		CONMAS cm on ih.NUMB=cm.BBCI
	inner join
		ISULIN il on il.NUMB=ih.NUMB
	where
		IDAT = @Date
	group by
		ih.NUMB,
		ih.RECI,
		ih.IDAT,
		ih.SPON,
		ih.BBCC,
		ih.IMPI,
		ih.VALU,
		ih.SUPP,
		sm.NAME,
		cm.NUMB,
		ih.DRLN

END
GO

