﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EventBySkun]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EventBySkun'
	EXEC ('CREATE PROCEDURE [dbo].[EventBySkun] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EventBySkun'
GO
ALTER PROCEDURE [dbo].[EventBySkun]
@EventNumber CHAR (6)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
Declare @Type as char(2);
set @Type = (select top 1 [TYPE] from EVTMAS where NUMB = @EventNumber);

If @Type = 'TS' or @Type = 'TM' or @Type = 'HS' or @Type = 'QS' or @Type = 'QM' 
	SELECT distinct @EventNumber as 'Event',
		   em.KEY1 as 'Part Code',
		   sm.DESCR as description
		   From EVTMAS as em inner join STKMAS as sm on em.Key1 = sm.skun
		   where em.numb = @EventNumber
	END
GO

