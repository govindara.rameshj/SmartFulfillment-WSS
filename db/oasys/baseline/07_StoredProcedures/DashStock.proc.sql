﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashStock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashStock'
	EXEC ('CREATE PROCEDURE [dbo].[DashStock] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashStock'
GO
-- Verion Control Information
-- =============================================
-- Author:  	Kevan Madelin
-- Create date: 23/08/2010
-- Version:	3.0.0.0
-- Description: This procedure is used to get the stock data for the Managers Dashboard.
-- =============================================
ALTER PROCEDURE [dbo].[DashStock]
	@DateEnd	date
as
begin
set nocount on

	declare @table table(Description varchar(50), Qty int, Value dec(9,2));
	declare	
		@StartDate			date,
		@OnHand				dec(9,2),
		@OnHandDash			dec(9,2),
		@MarkdownQty		int,		
		@MarkdownValue		dec(9,2),
		@MarkdownQtyDash	int,
		@MarkdownValueDash	dec(9,2),
		@DeletedValue		dec(9,2),
		@DeletedValueDash	dec(9,2),
		@NonStockValue		dec(9,2),
		@NonStockValueDash	dec(9,2),
		@ReturnsValue		dec(9,2),
		@ReturnsValueDash	dec(9,2),
		@CoreWeekValue		dec(9,2),
		@KbWeekValue		dec(9,2),
		@RefundWeekValue	dec(9,2),
		@VoucherWeekValue	dec(9,2),
		@TotalWeekValue		dec(9,2),
		@OutStockQty		int,
		@OutStockQtyDash	int,
		@BelowImpactQty		int,
		@BelowImpactQtyDash	int;
				
	--get current stock on hand/markdown/deleted/non stock/open returns/out of stock/below impact
	exec StockOnHandValue		@OnHand			output;
	exec StockMarkdownQuantity	@MarkdownQty	output; 
	exec StockMarkdownValue		@MarkdownValue	output;
	exec StockDeletedValue		@DeletedValue	output;
	exec StockNonStockValue		@NonStockValue	output;
	exec StockOpenReturnValue	@ReturnsValue	output;
	exec StockOutOfStockCount	@OutStockQty	output;
	exec StockBelowImpactCount	@BelowImpactQty	output; 
	
	--get stock movement data values
	select
		@OnHandDash			= sum(case ActivityID when 701 then Value else 0 end),
		@MarkdownQtyDash	= sum(case ActivityID when 702 then Quantity else 0 end),
		@MarkdownValueDash	= sum(case ActivityID when 702 then Value else 0 end),
		@DeletedValueDash	= sum(case ActivityID when 703 then Value else 0 end),
		@NonStockValueDash	= sum(case ActivityID when 704 then Value else 0 end),
		@ReturnsValueDash	= sum(case ActivityID when 705 then Value else 0 end),
		@OutStockQtyDash	= sum(case ActivityID when 710 then Quantity else 0 end),
		@BelowImpactQtyDash	= sum(case ActivityID when 711 then Quantity else 0 end)
	from
		StockMovement
	where
		DataDate > @DateEnd	
		
	--set startdate to sunday before enddate
	set	@StartDate = @DateEnd;
	While datepart(weekday, @StartDate) <> 1
	begin
		set @StartDate = dateadd(day, -1, @StartDate)
	end	
		
	--get core sales/refunds week to date info
	select
	 	@CoreWeekValue		= sum(case dl.tcod when 'SA' then dl.totl else 0 end),
	 	@RefundWeekValue	= sum(case dl.tcod when 'RF' then dl.totl else 0 end)
	from	
		dltots dl
	where	
		dl.DATE1	<= @DateEnd
	and dl.date1	>= @StartDate
	and	dl.CASH		<> '000'
	and	dl.VOID		= 0
	and	dl.PARK		= 0
	and	dl.TMOD		= 0;	
	
	--get K&B week values from dashboard data table
	select	
		@KbWeekValue	= sum(Value)
	from 
		StockMovement 
	where 
		ActivityID = 105 
		and DataDate >= @StartDate
		and DataDate <= @DateEnd;

	--get vouchers week to date info
	select
	 	@VoucherWeekValue	= sum(dp.amnt * -1)
	from	
		dltots dl
	inner join
		dlpaid dp	on	dp.date1 = dl.date1 
					and	dp.till = dl.till
					and	dp.[tran] = dl.[tran]
					and dp.[type] = 6
	where	
		dl.DATE1	<= @DateEnd
	and dl.date1	>= @StartDate
	and	dl.CASH		<> '000'
	and	dl.VOID		= 0
	and	dl.PARK		= 0
	and	dl.TMOD		= 0

	--calculate percentages avoiding divide by zero
	set @TotalWeekValue = (@CoreWeekValue +@RefundWeekValue -@VoucherWeekValue +@KbWeekValue);
	if @TotalWeekValue <> 0 set @TotalWeekValue = (@ReturnsValue / @TotalWeekValue) * 100;
	
	--return values
	insert into @table(Description, Value) values ('Stock On Hand', (@OnHand - coalesce(@OnHandDash,0)) );
	insert into @table(Description, Qty, Value) values ('Markdown Stock Holding', (@MarkdownQty - coalesce(@MarkdownQtyDash,0)), (@MarkdownValue - coalesce(@MarkdownValueDash,0)) );
	insert into @table(Description, Value) values ('Deleted Stock Holding', (@DeletedValue - coalesce(@DeletedValueDash,0)) );
	insert into @table(Description, Value) values ('Non-Stock Stock Holding', (@NonStockValue - coalesce(@NonStockValueDash,0)) );
	insert into @table(Description, Value) values ('Open Returns Stock Holding', (@ReturnsValue - coalesce(@ReturnsValueDash,0)) );
	insert into @table(Description, Value) values ('Percentage Returns to Weekly Sales', @TotalWeekValue );
	insert into @table(Description, Qty) values ('Number Items Out of Stock', (@OutStockQty - coalesce(@OutStockQtyDash,0)) );
	insert into @table(Description, Qty) values ('Number Items Below Impact', (@BelowImpactQty - coalesce(@BelowImpactQtyDash,0)) );

	select * from @table
end
GO

