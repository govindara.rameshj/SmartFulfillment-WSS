﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockLogGetGroups]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockLogGetGroups'
	EXEC ('CREATE PROCEDURE [dbo].[StockLogGetGroups] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockLogGetGroups'
GO
ALTER PROCEDURE [dbo].[StockLogGetGroups]
AS
BEGIN
	SET NOCOUNT ON;

	select
		Id,
		Description
	from 
		StockLogTypeGroup
	order by
		Id
		
END
GO

