﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEventOverrideTemporaryPriceChangeForSkuDetail]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEventOverrideTemporaryPriceChangeForSkuDetail'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEventOverrideTemporaryPriceChangeForSkuDetail] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEventOverrideTemporaryPriceChangeForSkuDetail'
GO
ALTER PROCedure usp_GetEventOverrideTemporaryPriceChangeForSkuDetail

   @EventTypeID int,
   @SkuNumber   char(6),
   @StartDate   date,
   @EndDate     date

as
begin
   set nocount on

   select b.ID, b.EventHeaderOverrideID, b.SkuNumber, a.Price   
   from EventHeaderOverride a
   inner join EventDetailOverride b
         on b.EventHeaderOverrideID = a.ID
   where a.EventTypeID = @EventTypeID
   and   a.StartDate  <= @StartDate
   and   a.EndDate    >= @EndDate
   and   b.SkuNumber   = @SkuNumber
 --order by a.SequenceNo, b.SkuNumber 
   order by a.Revision, b.SkuNumber 

end
GO

