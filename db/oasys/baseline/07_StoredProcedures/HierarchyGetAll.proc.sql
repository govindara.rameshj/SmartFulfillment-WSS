﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HierarchyGetAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure HierarchyGetAll'
	EXEC ('CREATE PROCEDURE [dbo].[HierarchyGetAll] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure HierarchyGetAll'
GO
ALTER PROCEDURE [dbo].[HierarchyGetAll]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		vwHierarchies

END
GO

