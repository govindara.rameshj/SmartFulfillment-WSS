﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MenuGetSingle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure MenuGetSingle'
	EXEC ('CREATE PROCEDURE [dbo].[MenuGetSingle] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure MenuGetSingle'
GO
ALTER PROCedure MenuGetSingle

   @MenuID int

as
begin

   set nocount on

   select Id            = ID,
          MasterId      = MasterID,
          [Description] = AppName,
          AssemblyName,
          ClassName,
          [LoadType]    = MenuType,
          [Parameters],
          ImagePath,
          IsMaximised   = LoadMaximised,
          IsModal,
          DisplayOrder  = DisplaySequence
   from MenuConfig
   where ID = @MenuID

end
GO

