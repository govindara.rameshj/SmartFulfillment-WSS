﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetProductInformation]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetProductInformation'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetProductInformation] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetProductInformation'
GO
ALTER PROCEDURE dbo.StockGetProductInformation
    @SkuNumber char(6)
AS
BEGIN
    SET NOCOUNT ON;

    select
        ProductInformationDescription = [TEXT]
    from
        STKTXT
    where
        skun = @SkuNumber
    order by
        SEQN
END
GO

