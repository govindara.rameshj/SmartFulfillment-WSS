﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ConsignmentGetNotReceivedCount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ConsignmentGetNotReceivedCount'
	EXEC ('CREATE PROCEDURE [dbo].[ConsignmentGetNotReceivedCount] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ConsignmentGetNotReceivedCount'
GO
ALTER PROCEDURE [dbo].[ConsignmentGetNotReceivedCount]
	@Quantity int output
AS
BEGIN
	SET NOCOUNT ON;

	set 
		@Quantity = coalesce( (select count(*)from conmas where done=0), 0);

END
GO

