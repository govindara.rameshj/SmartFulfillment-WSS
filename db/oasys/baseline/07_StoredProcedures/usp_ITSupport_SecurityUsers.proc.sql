﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_SecurityUsers]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_SecurityUsers'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_SecurityUsers] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_SecurityUsers'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_SecurityUsers]
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 11th February 2013
-- 
-- Task     : Extract System User Data.
------------------------------------------------------------------------------
	
---------------------------------------------------------------------------
-- Declare Parameters
---------------------------------------------------------------------------
@sp_Comma			char(1) = 'N'

AS
BEGIN
SET NOCOUNT ON;
Declare @Store Char(4) = '8' + (Select LTRIM(RTRIM([STOR])) From [Oasys].[dbo].[RETOPT] Where FKEY = '01') 
Declare @StoreName Char(30) = (Select LTRIM(RTRIM([SNAM])) From [Oasys].[dbo].[RETOPT] Where FKEY = '01') 
					
if @Sp_Comma in ('Y','N')
	Begin  
		if @sp_Comma = 'N'
				Begin
					-- Get Data
					Select  LTRIM(RTRIM(@Store)) as 'Store'
							,LTRIM(RTRIM(@StoreName)) as 'Store Name' 
							,su.[EmployeeCode] as 'Colleague Login ID'
							,su.[Name] as 'Colleague Name'
							,su.[Initials] as 'Colleague Initials'
							,su.[Position] as 'Colleague Position'
							,su.[PayrollID] as 'Colleague Payroll ID'
							,su.[PasswordExpires] as 'Password Expiry Date'
							,case su.[IsManager] 
								when 0 Then 'No'
								when 1 Then 'Yes'
							 End as 'Manager Priviledges'
							,case su.[IsSupervisor] 
								when 0 Then 'No'
								when 1 Then 'Yes'
							 End as 'Supervisor Priviledges'
							,case 
								when su.[SupervisorPwdExpires] IS NULL Then ''
								else LTRIM(RTRIM(CAST(su.[SupervisorPwdExpires] as CHAR)))
							End as 'Supervisor Password Expiry Date'
							,case 
								when su.[Outlet] IS NULL Then 'N/A'
								when su.[Outlet] = '' Then 'N/A'
								else LTRIM(RTRIM(CAST(su.[Outlet] as Char))) 
							 End as 'Workstation User Created'
							,case su.[IsDeleted] 
								when 0 Then 'Active'
								when 1 Then 'DELETED'
							 End as 'User Account Status'
							,case  
								when su.[DeletedDate] IS NULL Then 'N/A'
								when su.[DeletedDate] = '' Then 'N/A'
								else LTRIM(RTRIM(CAST(su.[DeletedDate] as Char)))
							 End as 'User Account Deleted Date'
							,case 
								when su.[DeletedTime] IS NULL Then 'N/A'
								when su.[DeletedTime] = '' Then 'N/A'
								else LTRIM(RTRIM(CAST(su.[DeletedTime] as Char)))
							 End as 'User Account Deleted Time'
							,case  
								when su.[DeletedBy] IS NULL Then 'N/A'
								when su.[DeletedBy] = '' Then 'N/A'
								else LTRIM(RTRIM(CAST(su.[DeletedBy] as Char)))
							 End as 'User Account Deleted by User'
							,case  
								when su.[DeletedWhere] IS NULL Then 'N/A'
								when su.[DeletedWhere] = '' Then 'N/A'
								else LTRIM(RTRIM(CAST(su.[DeletedWhere] as Char)))
							 End as 'User Account Deleted on Workstation'
							,su.[TillReceiptName] as 'Till Receipt Name'
							,LTRIM(RTRIM(CAST(su.SecurityProfileID as CHAR))) + ' - ' + LTRIM(RTRIM(sp.[Description])) as 'Security Level'
							FROM [Oasys].[dbo].[SystemUsers] as su
							Inner Join [Oasys].[dbo].[SecurityProfile] as sp on sp.ID = su.SecurityProfileID
							Where su.ID < 300		
				End
				
		if @sp_Comma = 'Y'
			Begin
					-- Get Data
					Select  LTRIM(RTRIM(@Store)) + ',' as 'Store'
							,LTRIM(RTRIM(@StoreName)) + ',' as 'Store Name' 
							,LTRIM(RTRIM(su.[EmployeeCode])) + ',' as 'Colleague Login ID'
							,LTRIM(RTRIM(su.[Name])) + ',' as 'Colleague Name'
							,LTRIM(RTRIM(su.[Initials])) + ',' as 'Colleague Initials'
							,LTRIM(RTRIM(su.[Position])) + ',' as 'Colleague Position'
							,LTRIM(RTRIM(su.[PayrollID])) + ',' as 'Colleague Payroll ID'
							,LTRIM(RTRIM(CAST(su.[PasswordExpires] as CHAR))) + ',' as 'Password Expiry Date'
							,case su.[IsManager] 
								when 0 Then 'No' + ','
								when 1 Then 'Yes' + ','
							 End as 'Manager Priviledges'
							,case su.[IsSupervisor] 
								when 0 Then 'No' + ','
								when 1 Then 'Yes' + ','
							 End as 'Supervisor Priviledges'
							,case 
								when su.[SupervisorPwdExpires] IS NULL Then ','
								when su.[SupervisorPwdExpires] = '' Then ','
								else LTRIM(RTRIM(CAST(su.[SupervisorPwdExpires] as CHAR))) + ','
							End as 'Supervisor Password Expiry Date'
							,case  
								when su.[Outlet] IS NULL Then ','
								when su.[Outlet] = '' Then ','
								else LTRIM(RTRIM(CAST(su.[Outlet] as Char))) + ',' 
							 End as 'Workstation User Created'
							,case su.[IsDeleted] 
								when 0 Then 'Active' + ','
								when 1 Then 'DELETED' + ','
							 End as 'User Account Status'
							,case  
								when su.[DeletedDate] IS NULL Then ','
								when su.[DeletedDate] = '' Then ','
								else LTRIM(RTRIM(CAST(su.[DeletedDate] as Char))) + ',' 
							 End as 'User Account Deleted Date'
							,case  
								when su.[DeletedTime] IS NULL Then ','
								when su.[DeletedTime] = '' Then ','
								else LTRIM(RTRIM(CAST(su.[DeletedTime] as Char))) + ',' 
							 End as 'User Account Deleted Time'
							,case  
								when su.[DeletedBy] IS NULL Then ','
								when su.[DeletedBy] = '' Then ','
								else LTRIM(RTRIM(CAST(su.[DeletedBy] as Char))) + ',' 
							 End as 'User Account Deleted by User'
							,case 
								when su.[DeletedWhere] IS NULL Then ','
								when su.[DeletedWhere] = '' Then ','
								else LTRIM(RTRIM(CAST(su.[DeletedWhere] as Char))) + ',' 
							 End as 'User Account Deleted on Workstation'
							,LTRIM(RTRIM(su.[TillReceiptName])) + ',' as 'Till Receipt Name'
							,LTRIM(RTRIM(CAST(su.SecurityProfileID as CHAR))) + ' - ' + LTRIM(RTRIM(sp.[Description])) + ',' as 'Security Level'
					FROM [Oasys].[dbo].[SystemUsers] as su
					Inner Join [Oasys].[dbo].[SecurityProfile] as sp on sp.ID = su.SecurityProfileID
					Where su.ID < 300	
			End 
	End
Else
	Begin
		Print(LTRIM(RTRIM(@Store)) + 'Comma Seperated File Not Specified')
	End
		
End
GO

