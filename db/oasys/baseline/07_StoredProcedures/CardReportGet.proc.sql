﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CardReportGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure CardReportGet'
	EXEC ('CREATE PROCEDURE [dbo].[CardReportGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure CardReportGet'
GO
ALTER PROCedure [dbo].[CardReportGet]
   @Date date
as
begin
set nocount on
select
       [Card Report Date]             = dl.DATE1,
       [Card Report Time]             = dl.[TIME],
       [Card Report TillId]           = dl.TILL,
       [Card Report CashierId]        = dl.CASH,
       [Card Report CashierName]      = su.Name,
       [Card Report Number]           = dl.CARD_NO,
       [Card Report CustomerName]     = ci.[COL_NAME],
       [Card Report ValueTransaction] = dl.TOTL,
       [Card Report ValueDiscount]    = dl.DISC
from
       DLTOTS dl
left join
       SystemUsers su on su.EmployeeCode = dl.CASH
left outer join
       COLCWI ci on ci.COLN = dl.CARD_NO
where
     --dl.DATE1 = @Date and (dl.CARD_NO > '1*' and dl.CARD_NO < '2*')
       dl.DATE1 = @Date and (dl.CARD_NO like '1%' or dl.CARD_NO like '2%')
order by
       dl.TILL, dl.[TRAN]
end
GO

