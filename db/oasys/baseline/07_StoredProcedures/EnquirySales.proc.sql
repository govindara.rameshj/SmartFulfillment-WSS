﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquirySales]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquirySales'
	EXEC ('CREATE PROCEDURE [dbo].[EnquirySales] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquirySales'
GO
ALTER PROCedure [dbo].[EnquirySales]
    @SkuNumber  Char(6)
As
Begin
    Set NOCOUNT On;

    Declare 
        @table table 
            (
                Date            Date,
                TillId          Char(2),
                TranNumber      VarChar(30),
                RowId           Int, 
                SkuNumber       Char(6), 
                Description     VarChar(50), 
                Display         VarChar(50)
            );
    Declare 
        @date           Date,
        @qtySold        Int,
        @qtyRefund      Int,
        @qtyMarkdown    Int,
        @lastSaleDate   Date,
        @lastSaleTime   Char(6),
        @lastSaleTill   Char(2),
        @lastSaleTran   VarChar(30),
        @lastSaleTranBO Char(4);

    Set @date = CONVERT(Date, getdate());

    Select
        @qtySold    = sum(dl.quan)
    From    
        dltots dt
    Inner Join
        dlline dl   
    On 
        dl.date1=dt.date1 
    And 
        dl.TILL=dt.TILL 
    And 
        dl.[tran]=dt.[tran] 
    And 
        dl.skun = @skuNumber
    Where   
        dt.DATE1    = @date
    And 
        dt.CASH     <> '000'
    And 
        dt.VOID     = 0
    And 
        dt.PARK     = 0
    And 
        dt.TMOD     = 0
    And 
        dt.TCOD     = 'SA';
    
    Select
        @qtyRefund  = sum(dl.quan)
    From    
        dltots dt
    Inner Join
        dlline dl   
    On 
        dl.date1=dt.date1 
    And 
        dl.TILL=dt.TILL 
    And 
        dl.[tran]=dt.[tran] 
    And 
        dl.skun = @skuNumber
    Where   
        dt.DATE1    = @date
    And 
        dt.CASH     <> '000'
    And 
        dt.VOID     = 0
    And 
        dt.PARK     = 0
    And 
        dt.TMOD     = 0
    And 
        dt.TCOD     = 'RF';

    Select
        @qtyMarkdown    = sum(dl.quan)
    From    
        dltots dt
    Inner Join
        dlline dl   
    On 
        dl.date1=dt.date1 
    And 
        dl.TILL=dt.TILL 
    And 
        dl.[tran]=dt.[tran] 
    And 
        dl.skun = @skuNumber
    And 
        dl.imdn=1
    Where   
        dt.DATE1    = @date
    And 
        dt.CASH     <> '000'
    And 
        dt.VOID     = 0
    And 
        dt.PARK     = 0
    And 
        dt.TMOD     = 0
    And 
        dt.TCOD     = 'SA';

    Select Top 1
        @lastSaleDate   = dt.date1,
        @lastSaleTime   = dt.[time],
        @lastSaleTill   = dt.till,
        @lastSaleTran   = rtrim(coalesce(dt.OVCTranNumber,dt.[Tran])),
        @lastSaleTranBO = dt.[Tran]
    From    
        vwDLTOTS dt
    Inner Join
        dlline dl   
    On  
        dt.DATE1=dl.DATE1 
    And 
        dt.TILL=dl.TILL
    And 
        dt.[TRAN]=dl.[TRAN] 
    And 
        dl.skun = @skuNumber
    Where   
        dt.CASH     <> '000'
    And 
        dt.VOID     = 0
    And 
        dt.PARK     = 0
    And 
        dt.TMOD     = 0
    And 
        dt.TCOD     = 'SA'
    Order By
        dt.date1 Desc, 
        dt.[time] Desc

    Insert Into @table (Description, Display) Values ('Sold Today',     coalesce(@qtySold, 0) );
    Insert Into @table (Description, Display) Values ('Refunded Today', coalesce(@qtyRefund, 0) );
    Insert Into @table (Description, Display) Values ('MD Sold Today',  coalesce(@qtyMarkdown,0) );
    Insert Into @table (Description, Display) Values ('Last Sale',      convert(Varchar(10), @lastSaleDate, 103) + ' ' + substring(@lastSaleTime,1,2) + ':' + substring(@lastSaleTime, 3, 2) );
    Insert Into @table (Description, Display) Values ('', '');
    Insert Into 
        @table
            (rowid, Date, TillId, TranNumber, Description, Display) 
        Values
            (2, @lastSaleDate, @lastSaleTill, @lastSaleTranBO, 'Last Sale Tran', @lastSaleTill + '-' + rtrim(@lastSaleTran));
    
    Insert Into 
        @table  
            (RowId, SkuNumber, [Description]) 
        Values
            (1, @SkuNumber, '13 Week Sales');
        
    Select * From @table;
End
GO

