﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SalesGetCreditCardDetailed]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SalesGetCreditCardDetailed'
	EXEC ('CREATE PROCEDURE [dbo].[SalesGetCreditCardDetailed] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SalesGetCreditCardDetailed'
GO
ALTER PROCEDURE [dbo].[SalesGetCreditCardDetailed] 
	@Date Datetime
AS
BEGIN
	SET NOCOUNT ON;

	select
		substring(dp.[card],1,3)	+ ' ' + 
		substring(dp.[card], 4, 4)	+ ' ' +
		substring(dp.[card], 8, 4)	+ ' ' +
		substring(dp.[card], 12, 4) + ' ' +
		substring(dp.[card], 16, 4)			as 'CardNumber',
		dp.EXDT								as DateExpiry,
          DP.AMNT*-1								as 'Value',
		DP.AUTH								as 'AuthCode',
		case DP.CKEY
			when 0 then 'Swiped'
			else		'Keyed'
		end									as 'Capture',
		case 
			When DT.TCOD = 'SC' and DT.TOTL > 0.00	then 'Sale'
			When DT.TCOD = 'SC' and DT.TOTL <= 0.00	then 'Sale Correction'
			When DT.TCOD = 'RC' and DT.TOTL >= 0.00	then 'Refund'
			When DT.TCOD = 'RC' and DT.TOTL < 0.00	then 'Refund Correction'
			When DT.TCOD = 'SA' and DT.TOTL < 0.00	then 'Sale Reverse'
			When DT.TCOD = 'SA' and DT.TOTL >= 0.00	then 'Sale'
			When DT.TCOD = 'RF' and DT.TOTL <= 0.00 then 'Refund'
			When DT.TCOD = 'RF' and DT.TOTL > 0.00	then 'Refund Reverse'
			When DT.TCOD = 'M+' and DT.TOTL >= 0.00 then 'Misc. Income'
			When DT.TCOD = 'M+' and DT.TOTL < 0.00	then 'Misc. Income Reverse'
			When DT.TCOD = 'M-' and DT.TOTL <= 0.00 then 'Paid Out'
			When DT.TCOD = 'M-' and DT.TOTL > 0.00	then 'Paid Out Reverse'
			When DT.TCOD = 'C+' and DT.TOTL < 0.00  then 'Misc. Income Correction'
			When DT.TCOD = 'C+' and DT.TOTL >= 0.00	then 'Misc. Income Correction Reverse'
			When DT.TCOD = 'C-' and DT.TOTL >= 0.00	then 'Paid Out Correction'
			When DT.TCOD = 'C-' and DT.TOTL < 0.00  then 'Paid Out Correction Reverse'			
			Else 'Unknown Sale Type - ' + DT.TCOD
		end							as 'SaleType'
	from 
		DLPAID DP 
	join 
		DLTOTS DT		on	dp.date1 = dt.date1 
						and	dp.till	 = dt.till
						and DP.[TRAN] = DT.[TRAN]
	where 
			dt.date1 = @Date
		and	DP.[TYPE] in (3,8,9)
		and DP.[TYPE] not in 
			(
			(select ACON from RETOPT where fkey='01'),
			(select WION from RETOPT where fkey='01'),
			(select CHON from RETOPT where fkey='01'),
			(select SVON from RETOPT where fkey='01')
			)
		and DT.VOID = 0 
		and DT.TMOD = 0
END
GO

