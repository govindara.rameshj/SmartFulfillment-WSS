﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_AdjustUnitsSoldInCurrentWeek]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_AdjustUnitsSoldInCurrentWeek'
	EXEC ('CREATE PROCEDURE [dbo].[usp_AdjustUnitsSoldInCurrentWeek] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_AdjustUnitsSoldInCurrentWeek'
GO
ALTER PROCedure [dbo].[usp_AdjustUnitsSoldInCurrentWeek]
	@Skun As Int,
	@Quantity As Int
As
	Begin
	
		Set NoCount On
		
		Update
			STKMAS
		Set
			US001 = US001 + @Quantity
		Where
			SKUN = @Skun
End
GO

