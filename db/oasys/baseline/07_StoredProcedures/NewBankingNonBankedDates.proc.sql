﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingNonBankedDates]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingNonBankedDates'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingNonBankedDates] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingNonBankedDates'
GO
-- =============================================
-- Author      : Partha Dutta
-- Create Date : 24/12/2010
-- Referral No : 656
-- Notes       : Banking - Pickup ReCheck Screen
--               Allow the ability to perform
--                  F1 - Assign Float
--                  F2 - Cash Drop
--                  F3 - Floated Pickup
--                  F4 - UnFloated Pickup
--                  F6 - Banking Bag Collection
-- =============================================

ALTER PROCedure NewBankingNonBankedDates
   @Today date
as
begin
set nocount on
--get today's period
declare @PeriodID                  int
declare @NextDayToBeBankedPeriodID int

set @PeriodID                  = (select ID from SystemPeriods where StartDate = @Today)
set @NextDayToBeBankedPeriodID = (select top 1 PeriodID
                                  from [Safe]
                                  where IsClosed = 0
                                  order by PeriodID asc)

select a.*
from (select PeriodId                     = a.ID,
             PeriodDate                   = a.StartDate,
             BankingComplete              = b.IsClosed,
             NextDateToBeBanked           = cast((case (select top 1 PeriodID
                                                        from [Safe]
                                                        where IsClosed = 0
                                                        order by PeriodID asc)
                                                     when a.ID then 1
                                                     else 0
                                                  end) as bit),
             LastDateBanked               = cast((case (select top 1 PeriodID
                                                        from [Safe]
                                                        where IsClosed = 1
                                                        order by PeriodID desc)
                                                     when a.ID then 1
                                                     else 0
                                                  end) as bit),
             BankingBagCollectionRequired = cast((case (select count(*)
                                                        from SafeBags a
                                                        where a.[Type]      = 'B'
                                                        and   a.[State]     = 'M'
                                                        and   a.SealNumber <> '00000000')  --banking bag containing non-cash(except cheque) tenders do not need to be physically collected
                                                   when 0 then 0                           --this bag is created for process transmission only
                                                   else 1
                                                end) as bit),
             [Description]                = ltrim(str(a.ID)) + ' - ' +
                                            replicate('0', 2 - len(ltrim(str(day(a.StartDate)))))   + ltrim(str(day(a.StartDate)))   + '/' +
                                            replicate('0', 2 - len(ltrim(str(month(a.StartDate))))) + ltrim(str(month(a.StartDate))) + '/' +
                                            ltrim(str(year(a.StartDate))) + ' - ' + case
                                                                                       when b.IsClosed is null then 'Safe record not found'
                                                                                       when b.IsClosed = 0     then 'Banking not completed'
                                                                                       when b.IsClosed = 1     then 'Banking complete'
                                                                                    end,
            SafeMaintenanceDone           = isnull(b.SafeChecked, 0)
      from SystemPeriods a
      left outer join [Safe] b
                 on b.PeriodID  = a.ID
      where a.ID <= @PeriodID
      --cutdown on the number of days being displayed
      and   a.ID >= (@PeriodID - (select LongValue from [Parameters] where ParameterID = 2200))) a
where a.PeriodId > @NextDayToBeBankedPeriodID
order by a.PeriodId desc
end
GO

