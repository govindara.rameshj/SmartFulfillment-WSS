﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SaveDataInDLGiftCard]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SaveDataInDLGiftCard'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SaveDataInDLGiftCard] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SaveDataInDLGiftCard'
GO
ALTER PROCEDURE dbo.usp_SaveDataInDLGiftCard
    @DATE1      date,
    @TILL       char(2),
    @TRAN       char(4),
    @CARDNUM    char(19),
    @EEID       char(3),
    @TYPE       char(2),
    @AMNT       decimal(9, 2),
    @AUTH       char(150),
    @MSGNUM     char(150),
    @TRANID     decimal(38,0),
    @RTIFlag    char(1)='S',
    @SEQN       int=1
AS
BEGIN
    SET NOCOUNT ON;
    
    INSERT INTO dbo.DLGIFTCARD (DATE1, TILL, [TRAN], CARDNUM, EEID, [TYPE], AMNT, AUTH, MSGNUM, TRANID, RTI, SEQN)
    VALUES (@DATE1, @TILL, @TRAN, @CARDNUM, @EEID, @TYPE, @AMNT, @AUTH, @MSGNUM, @TRANID, @RTIFlag, @SEQN)
    
    SET NOCOUNT OFF;
END
GO

