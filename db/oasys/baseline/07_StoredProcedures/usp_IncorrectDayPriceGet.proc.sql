﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_IncorrectDayPriceGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_IncorrectDayPriceGet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_IncorrectDayPriceGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_IncorrectDayPriceGet'
GO
ALTER PROCEDURE [dbo].[usp_IncorrectDayPriceGet] 
	-- Add the parameters for the stored procedure here
	@DateStart Date, 
	@DateEnd Date 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		ed.SkuNumber as 'SkuNumber',
		(select Descr from STKMAS where SKUN=ed.SkuNumber) as 'Description',
		eh.Price as 'OverridePrice',
		(select Pric from STKMAS where SKUN=ed.SkuNumber) as 'SystemPrice',
		eh.AuthorisorSecurityID  as 'AuthCode',
		(select TillReceiptName from SystemUsers where ID=eh.AuthorisorSecurityID) as 'AuthName',
		eh.CashierSecurityID as 'CashierId',
		(select TillReceiptName from SystemUsers where ID=eh.CashierSecurityID) as 'CashierName',
		eh.StartDate as Date
	From EventHeaderOverride eh inner join EventDetailOverride ed on eh.ID = ed.EventHeaderOverrideID
	where eh.StartDate >= @DateStart and eh.EndDate <= @DateEnd And eh.EventTypeId=1
END
GO

