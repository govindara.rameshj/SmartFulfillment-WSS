﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGetReportSummaries]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReportGetReportSummaries'
	EXEC ('CREATE PROCEDURE [dbo].[ReportGetReportSummaries] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReportGetReportSummaries'
GO
ALTER PROCEDURE [dbo].[ReportGetReportSummaries]
	@ReportId int,
	@TableId int
AS
BEGIN
	SET NOCOUNT ON;

	select
		rs.ReportId,
		rs.TableId,
		rc.Id,
		rc.Name,
		rs.SummaryType,
		rs.ApplyToGroups,
		rs.Format,
		rs.NumeratorId,
		rc1.Name			as NumeratorName,
		rs.DenominatorId,
		rc2.Name			as DenominatorName
	from
		ReportSummary rs
	inner join
		ReportColumn rc on rs.ColumnId = rc.Id
	left join
		ReportColumn rc1 on rs.NumeratorId=rc1.Id 
	left join
		ReportColumn rc2 on rs.DenominatorId=rc2.Id
	where 
		rs.ReportId = @ReportId
		and rs.TableId = @TableId

END
GO

