﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateFuzzyMatchData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdateFuzzyMatchData'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdateFuzzyMatchData] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdateFuzzyMatchData'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 22/08/2012
-- User Story	 : 6219
-- Project		 : P022-007 - Fuzzy Logic Search.
-- Task Id		 : 7262
-- Description   : Add in HierarchyWord population.
--				 : And use distinct word lists to
--				 : prevent duplicate words elevating
--				 : a fuzzy search that includes that
--				 : word rather than another in the
--				 : search criteria.
-- =============================================
ALTER PROCedure usp_UpdateFuzzyMatchData
	@Verbose Bit = 0
As
Begin
    Set NoCount On

    Truncate Table Sku
    Truncate Table SkuWord
	Truncate Table HierarchyWord

	--populate physical Sku Table
	Insert Into Sku
		(
			Sku, 
			DESCR, 
			SpacelessDescription
		)
	Select 
		stk.SKUN, 
		LTRIM(RTRIM(stk.DESCR)), 
		REPLACE(LTRIM(RTRIM(stk.DESCR)), ' ', '')
	From 
		STKMAS stk

	--populate SkuWord
	Declare
		@SkuPointer Int = 1,
		@MaxSkuPointer Int,
		@Sku Char(6),
		@Description VarChar(Max),
		@Ctgy Int,
		@Grou Int,
		@Sgrp Int,
		@Styl Int
	Select
		@MaxSkuPointer = Count(*)
	From 
		Sku

	While @SkuPointer <= @MaxSkuPointer
		Begin
			Select 
				@Sku = Sku,
				@Description = Sku.descr,
				@Ctgy = sm.CTGY,
				@Grou = sm.GROU,
				@Sgrp = sm.SGRP,
				@Styl = sm.STYL
			From
				Sku
					Inner Join
						STKMAS sm
					On
						Sku.sku = sm.SKUN
			Where
				id = @SkuPointer
				
			-- Insert full words
			Insert Into
				SkuWord
					(
						SkuId,
						Word,
						VowelsStripped, 
						Category, 
						[Group], 
						SubGroup, 
						Style,
						Sku
					)
			Select
				@SkuPointer,
				Item,
				0,
				@Ctgy,
				@Grou,
				@Sgrp,
				@Styl,
				@Sku
			From
				dbo.udf_SplitVarcharToTable(dbo.udf_PadNumbers(@Description, ' '), ' ')
			Where
				LEN(LTRIM(RTRIM(Item))) > 0
			And
				Item <> 'x'
			Group By
				Item

			-- Insert words with vowels stripped out
			Insert Into
				SkuWord
					(
						SkuId,
						Word,
						VowelsStripped, 
						Category, 
						[Group], 
						SubGroup, 
						Style,
						Sku
					)
			Select
				@SkuPointer,
				Item,
				1,
				@Ctgy,
				@Grou,
				@Sgrp,
				@Styl,
				@Sku
			From
				dbo.udf_SplitVarcharToTable(dbo.udf_StripVowels(dbo.udf_PadNumbers(@Description, ' ')), ' ')
			Where
				Len(LTrim(RTrim(Item))) > 0
			And 
				Item <> 'x'
			Group By
				Item

			Set @SkuPointer = @SkuPointer + 1

			If @verbose = 1
				Begin
					Print @SkuPointer
				End
		End


		-- HierarchyWord population
		Declare
			@HierarchyDescription Table
				(
					ID Int Identity,
					NUMB Int,
					GROU Int Null,
					SGRP Int Null,
					STYL Int Null,
					Description VarChar(Max),
					Source Char(3)
				)
		Insert Into @HierarchyDescription Select NUMB, Null, Null, Null, DESCR, 'CAT' From HIECAT
		Insert Into @HierarchyDescription Select NUMB, GROU, Null, Null, DESCR, 'GRP' From HIEGRP
		Insert Into @HierarchyDescription Select NUMB, GROU, SGRP, Null, DESCR, 'SGP' From HIESGP
		Insert Into @HierarchyDescription Select NUMB, GROU, SGRP, STYL, DESCR, 'STY' From HIESTY

		Declare
			@Pointer Int = 1,
			@MaxPointer Int,
			@Source Char(3)
		Select
			@MaxPointer = Count(*)
		From 
			@HierarchyDescription 

		While @Pointer <= @MaxPointer
			Begin
				Select 
					@Ctgy = NUMB, 
					@Grou = GROU, 
					@Sgrp = SGRP,
					@Styl = STYL,
					@Description = Description,
					@Source = Source
				From
					@HierarchyDescription 
				Where
					id = @Pointer

				-- Insert full words
				Insert Into
					HierarchyWord
						(
							NUMB,
							GROU,
							SGRP,
							STYL,
							Word,
							VowelsStripped,
							Source
						)
				Select
					@Ctgy,
					@Grou,
					@Sgrp,
					@Styl,
					Item,
					0,
					@Source 
				From
					dbo.udf_SplitVarcharToTable(dbo.udf_PadNumbers(@Description, ' '), ' ')
				Where
					LEN(LTRIM(RTRIM(Item))) > 0
				And
					Item <> 'x'
				And
					Item <> '&'
				Group By
					Item

				-- Insert words with vowels stripped out
				Insert Into
					HierarchyWord
						(
							NUMB,
							GROU,
							SGRP,
							STYL,
							Word,
							VowelsStripped,
							Source
						)
				Select
					@Ctgy,
					@Grou,
					@Sgrp,
					@Styl,
					Item,
					1,
					@Source 
				From
					dbo.udf_SplitVarcharToTable(dbo.udf_StripVowels(dbo.udf_PadNumbers(@Description, ' ')), ' ')
				Where
					LEN(LTRIM(RTRIM(Item))) > 0
				And
					Item <> 'x'
				And
					Item <> '&'
				Group By
					Item

				If @verbose = 1
					Begin
						Print @Pointer
					End

				Set @Pointer = @Pointer + 1
			End
End
GO

