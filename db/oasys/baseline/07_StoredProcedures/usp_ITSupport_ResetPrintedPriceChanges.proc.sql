﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_ResetPrintedPriceChanges]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_ResetPrintedPriceChanges'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_ResetPrintedPriceChanges] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_ResetPrintedPriceChanges'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_ResetPrintedPriceChanges]
@Label Char(1),
@Type Char(1)
As
Begin
Set NoCount On;
	
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 14th October 2011
-- 
-- Task     : Set Postcode Survey Dates
-----------------------------------------------------------------------------------


-- P Peg / S Small / M Medium / A ALL
-- I Increases / D Decreases / A ALL
--------------------------------------------------------------------------------
-- Check Setup Correctly
--------------------------------------------------------------------------------
If @Label not in ('P','S','M','A')
	Begin
		Print 'LABEL NOT SPECIFIED'
		Goto ExitProcess
	End
	
If @Type not in ('I','D','A')
	Begin 
		Print 'CHANGE TYPE NOT SPECIFIED'
		Goto ExitProcess
	End

--------------------------------------------------------------------------------
-- Create Holding Table
--------------------------------------------------------------------------------
If Exists (Select Name From TempDB.Sys.Objects Where Name like ('#TempPCL_%') and [Type] = 'U') Drop Table #TempPCL				
Create Table #TempPCL
(
	[ID] [int] IDENTITY(1,1),
	[SKU] [int] NOT NULL,
	[Event] [int] NOT NULL
)


--------------------------------------------------------------------------------
-- Retrieve Data
--------------------------------------------------------------------------------
If @Type = 'I'
	Begin
		Print 'Reseting Label Flags for INCREASES ONLY'
		Insert into #TempPCL
		Select pc.SKUN, pc.EVNT From PRCCHG as pc 
		Inner Join STKMAS as sm on sm.SKUN = pc.SKUN 
		Where pc.PSTA = 'U' and pc.SHEL = '1' and pc.PRIC >= sm.PRIC
	End
Else If @Type = 'D'
	Begin
		If @Type = 'D'
		Print 'Reseting Label Flags for DECREASES ONLY'
		Insert into #TempPCL
		Select pc.SKUN, pc.EVNT From PRCCHG as pc 
		Inner Join STKMAS as sm on sm.SKUN = pc.SKUN 
		Where pc.PSTA = 'U' and pc.SHEL = '1' and pc.PRIC <= sm.PRIC
	End
Else If @Type = 'A'
	Begin 
		Print 'Reseting Label Flags for ALL LABELS TYPES'
		Insert into #TempPCL
		Select pc.SKUN, pc.EVNT From PRCCHG as pc
		Where pc.PSTA = 'U' and pc.SHEL = '1'
	End
	

--------------------------------------------------------------------------------
-- Update Processing
--------------------------------------------------------------------------------
Declare @PCL_ID int
Declare @PCL_MAXID int
Select  @PCL_MAXID = MAX(ID)FROM #TempPCL
Set		@PCL_ID = 1
Declare @PCL_SKU int 
Declare @PCL_Event int 
While	(@PCL_ID <= @PCL_MAXID)
	Begin	         
		Set @PCL_SKU = (Select SKU FROM #TempPCL WHERE ID = @PCL_ID)
		Set @PCL_Event = (Select Event FROM #TempPCL WHERE ID = @PCL_ID)
		If @Label = 'P'
			Begin
				Print	'Setting SMALL Label Flag'
				Update	PRCCHG Set SHEL = '0', LABS = (Select LABN From STKMAS Where SKUN = @PCL_SKU) Where SKUN = @PCL_SKU and EVNT = @PCL_Event and PSTA = 'U' and SHEL = '1' and (Select LABN From STKMAS Where SKUN = @PCL_SKU) > 0
			End
		If @Label = 'S'
			Begin
				Print	'Setting MEDIUM Label Flag'
				Update	PRCCHG Set SHEL = '0', LABM = (Select LABM From STKMAS Where SKUN = @PCL_SKU) Where SKUN = @PCL_SKU and EVNT = @PCL_Event and PSTA = 'U' and SHEL = '1' and (Select LABM From STKMAS Where SKUN = @PCL_SKU) > 0
			End	
		If @Label = 'M'
			Begin
				Print	'Setting LARGE Label Flag'
				Update	PRCCHG Set SHEL = '0', LABL = (Select LABL From STKMAS Where SKUN = @PCL_SKU) Where SKUN = @PCL_SKU and EVNT = @PCL_Event and PSTA = 'U' and SHEL = '1' and (Select LABL From STKMAS Where SKUN = @PCL_SKU) > 0
			End	
		If @Label = 'A'
			Begin
				Print	'Setting ALL Label Flags'
				Update	PRCCHG 
				Set		SHEL = '0',
						LABS = (Select LABN From STKMAS Where SKUN = @PCL_SKU),
						LABM = (Select LABM From STKMAS Where SKUN = @PCL_SKU),
						LABL = (Select LABL From STKMAS Where SKUN = @PCL_SKU)
				Where	SKUN = @PCL_SKU and EVNT = @PCL_Event and PSTA = 'U' and SHEL = '1'
			End
	    Set @PCL_ID = @PCL_ID+1 
	End

--------------------------------------------------------------------------------
-- Return Data
--------------------------------------------------------------------------------
Select * From #TempPCL Order By SKU asc
ExitProcess:

End
GO

