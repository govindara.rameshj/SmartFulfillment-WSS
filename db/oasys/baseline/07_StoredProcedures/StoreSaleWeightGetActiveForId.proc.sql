﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StoreSaleWeightGetActiveForId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StoreSaleWeightGetActiveForId'
	EXEC ('CREATE PROCEDURE [dbo].[StoreSaleWeightGetActiveForId] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StoreSaleWeightGetActiveForId'
GO
ALTER PROCEDURE[dbo].[StoreSaleWeightGetActiveForId]
	@Id int
AS
BEGIN
	SET NOCOUNT ON;

	select
		sw.Id,
		sw.DateActive,
		sw.[Week],
		sw.Value
	from 
		StoreSaleWeight sw
	where
		sw.Id = @id
		and sw.DateActive <= getdate()

END
GO

