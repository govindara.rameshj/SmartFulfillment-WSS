﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LocalStoreDetailsGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure LocalStoreDetailsGet'
	EXEC ('CREATE PROCEDURE [dbo].[LocalStoreDetailsGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure LocalStoreDetailsGet'
GO
ALTER PROCEDURE [dbo].[LocalStoreDetailsGet]
@Id CHAR (3)
AS
BEGIN
	SET NOCOUNT ON;
IF @Id ='0'
	BEGIN
		SELECT TOP 1 Stor as Id, 
		SNAM as Address1,
		SAD1 as Address2,
		SAD2 as Address3,
		SAD3 as Address4,
		SNAM as Name,
		CTEL as PhoneNumber,
		CountryCode
		FROM RETOPT
	END
ELSE
	BEGIN
	IF NOT EXISTS (
		SELECT *
		FROM [Oasys].[dbo].[Store]
		WHERE Id = @Id )
	BEGIN
		IF NOT EXISTS (
			SELECT *
			FROM [Oasys].[dbo].[STRMAS]
			WHERE NUMB = @Id )
			BEGIN
				INSERT INTO [Oasys].[dbo].[STRMAS] (
				NUMB, ADD1, ADD2, ADD3, ADD4, TILD, PHON, SFAX, MANG, REGC, DELC, CountryCode)
				SELECT TOP 1 STOR, SNAM, SAD1, SAD2, SAD3, SNAM, CTEL, '', '', '', 0, CountryCode
				FROM [Oasys].[dbo].[RETOPT];
			END
		ELSE
			BEGIN
				INSERT INTO [Oasys].[dbo].[Store] (
				Id, Name, Address1, Address2, Address3, PhoneNumber, FaxNumber, Manager, RegionCode, IsClosed, CountryCode)
				SELECT NUMB, TILD, ADD1, ADD2, ADD3, PHON, SFAX, MANG, REGC, DELC, CountryCode
				FROM [Oasys].[dbo].[STRMAS]
				WHERE NUMB = @Id;
			END
		END

		SELECT Id, Name, Address1, Address2, Address3, PhoneNumber, FaxNumber, Manager, RegionCode, IsClosed, CountryCode
		FROM [Oasys].[dbo].[Store]
		WHERE Id = @Id
	END	
END
GO

