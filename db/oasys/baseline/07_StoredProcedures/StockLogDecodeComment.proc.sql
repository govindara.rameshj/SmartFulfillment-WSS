﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockLogDecodeComment]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockLogDecodeComment'
	EXEC ('CREATE PROCEDURE [dbo].[StockLogDecodeComment] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockLogDecodeComment'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 7th July, 2010
-- Description:	Stock Log Comment Decoder
-- Modifier: Alexey Tronov
-- Last modify date: 16th Jan, 2014
-- =============================================
ALTER PROCEDURE [dbo].[StockLogDecodeComment]
	@TKEY int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Case
			When (sl.TYPE = 1 OR sl.TYPE = 2 OR sl.TYPE = 3 OR sl.TYPE = 4 OR sl.TYPE = 5 OR sl.TYPE = 6 OR sl.TYPE = 7) then 'Date:' + SUBSTRING(sl.keys, 1,8) + ' Till:' + SUBSTRING(sl.keys, 9,2) + ' Tran:' + SUBSTRING(sl.keys,11,4) + ' Line Number:' +SUBSTRING(sl.KEYS,15,6)
			When ((sl.TYPE = 21 OR sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)=' ') then 'Header Tkey:' + SUBSTRING(sl.keys, 10, 6) + ' Initials:' + SUBSTRING(sl.keys, 25,9) 
			When ((sl.TYPE = 21 OR sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)='0') then 'Header Number:' + SUBSTRING(sl.keys, 1,6) + ' Header Tkey:' + SUBSTRING(sl.keys, 12,4) + ' Line Tkey:' + SUBSTRING(sl.keys,20,5)
			When ((sl.TYPE = 21) and SUBSTRING(sl.KEYS,2,1)<>' ') then 'Header Number:' + SUBSTRING(sl.keys, 1,6) + ' Header Tkey:' + SUBSTRING(sl.keys,11,4) + ' Line Tkey:' + SUBSTRING(sl.keys,20,6)
			When ((sl.TYPE = 22) and SUBSTRING(sl.KEYS,2,1)<>' ') then 'Header Number:' + SUBSTRING(sl.keys, 1,6) + ' Header Tkey:' + SUBSTRING(sl.keys,13,4) + ' Line Tkey:' + SUBSTRING(sl.keys,21,6)
			When ((sl.TYPE = 31 OR sl.TYPE = 32 OR sl.TYPE = 35  OR sl.TYPE = 36  OR sl.TYPE = 37  OR sl.TYPE = 66  OR sl.TYPE = 67  OR sl.TYPE = 68  OR sl.TYPE = 69) and SUBSTRING(sl.KEYS,9,1)=' ' )then 'Date:' + SUBSTRING(sl.keys, 1,8) + ' Code:' + SUBSTRING(sl.keys, 11,2) +  ' Seqn:' + SUBSTRING(sl.keys,22,9)
			When ((sl.TYPE = 31 OR sl.TYPE = 32 OR sl.TYPE = 35  OR sl.TYPE = 36  OR sl.TYPE = 37  OR sl.TYPE = 66  OR sl.TYPE = 67  OR sl.TYPE = 68  OR sl.TYPE = 69) and SUBSTRING(sl.KEYS,9,1)<>' ' )then 'Date:' + SUBSTRING(sl.keys, 1,10) + ' Code:' + SUBSTRING(sl.keys, 13,2) +  ' Seqn:' + SUBSTRING(sl.keys,24,9)
			When ((sl.TYPE = 41 OR sl.TYPE = 42 OR sl.TYPE = 73) and SUBSTRING(sl.keys,8,1) = ' ') then 'DRLN:' + SUBSTRING(sl.keys, 1,6) + ' Seqn:' + SUBSTRING(sl.keys, 9, 8) 
			When ((sl.TYPE = 41 OR sl.TYPE = 42 OR sl.TYPE = 73) and SUBSTRING(sl.keys,8,1) <> ' ') then 'DRLN:' + SUBSTRING(sl.keys, 1,6) + ' Seqn:' + SUBSTRING(sl.keys, 8, 8) 
			When (sl.TYPE = 51) then 'Single item Sku:' + SUBSTRING(sl.keys, 1,6) + ' Bulk/Package Sku:' + SUBSTRING(sl.keys, 11, 6)  
			When ((sl.TYPE = 61) and SUBSTRING(sl.KEYS,11,1)<>' ') then 'Date:' + SUBSTRING(sl.keys, 11, 8) + ' Status when applied:' + SUBSTRING(sl.keys, 21, 1) 
			When ((sl.TYPE = 61) and SUBSTRING(sl.KEYS,11,1)=' ') then 'Date:' + SUBSTRING(sl.keys, 12, 8) + ' Status when applied:' + SUBSTRING(sl.keys, 22, 1) 
			When (sl.TYPE = 71) then 'Issue No:' + SUBSTRING(sl.keys, 1,6) + ' Line No:' + SUBSTRING(sl.keys, 11,4) + ' DRL No:' + SUBSTRING(sl.keys,17,6) + ' Seqn:' + SUBSTRING(sl.keys,25,4)
			When (sl.TYPE = 72) then 'DRL No:' + SUBSTRING(sl.keys, 1,6) + ' Seqn:' + SUBSTRING(sl.keys, 11,4) + ' Consignment No:' + SUBSTRING(sl.keys,17,6)
			Else sl.KEYS
	    End as 'KEYS' 
	FROM STKLOG as sl inner join StockLogType as st on sl.TYPE = st.Id 
	WHERE sl.TKEY= @TKEY
END
GO

