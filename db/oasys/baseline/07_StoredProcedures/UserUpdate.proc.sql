﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UserUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure UserUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[UserUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure UserUpdate'
GO
-- =============================================
-- Author:		Not Known
-- Create date: 1st March 2011
-- Description:	Baseline version
--
-- Modified by  : Partha Dutta
-- Modified date: 15/06/2011
-- Description  : Referral 376
--                IsSupervisor & IsManager data now supplied, default to false if stored procedure does not receive any data
--
-- =============================================
ALTER PROCEDURE [dbo].[UserUpdate]
 @Id INT, @EmployeeCode CHAR (3), @Name CHAR (35), @Initials CHAR (5), @Position CHAR (20), @PayrollId CHAR (20),
 @Outlet CHAR (2), @TillReceiptName CHAR (35), @LanguageCode CHAR (3), @ProfileId INT, @IsDeleted BIT=0, @DeletedDate DATE=null,
 @DeletedBy CHAR (3)=null, @DeletedWhere CHAR (2)=null, @Password CHAR (5)=null,
 @PasswordExpires DATE=null, @SuperPassword CHAR (5)=null, @SuperPasswordExpires DATE=null,
 @IsManager    bit = 0,
 @IsSupervisor bit = 0
AS
begin
	set nocount on;
	
	declare @rowCount int;
	set		@rowCount=0;
	
	update 
		SystemUsers
	set 
		[EmployeeCode]		= @EmployeeCode,
		[Name]				= @Name,
		[Initials]			= @Initials,
		[Position]			= @Position,
		[PayrollID]			= @PayrollId,
		[Outlet]			= @Outlet,
		[TillReceiptName]	= @TillReceiptName,
		[LanguageCode]		= @LanguageCode,
		[SecurityProfileID]	= @ProfileId,
		[IsDeleted]			= @IsDeleted,
		[DeletedDate]		= @DeletedDate,
		[DeletedBy]			= @DeletedBy,
		[DeletedWhere]		= @DeletedWhere,
		IsManager           = @IsManager,
		IsSupervisor        = @IsSupervisor
	 where 
		Id = @Id
	
	set @rowCount = @@ROWCOUNT;
	
	if @Password is not null
	begin
		update 
			SystemUsers
		set 
			[Password]			= @Password,
			[PasswordExpires]	= @PasswordExpires
		 where 
			Id = @Id
	end
	
	if @SuperPassword is not null
	begin
		update 
			SystemUsers
		set 
			[SupervisorPassword]	= @SuperPassword,
			[SupervisorPwdExpires]	= @SuperPasswordExpires
		 where 
			Id = @Id;
	end
		
	return @rowcount;
end
GO

