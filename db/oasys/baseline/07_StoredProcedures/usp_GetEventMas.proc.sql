﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEventMas]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEventMas'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEventMas] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEventMas'
GO
-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 06/10/2011
-- TFSID        : 2733 
-- Description  : New Stored Procedure for Coupons ProcessTransmissions
-- =====================================================================


-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================


ALTER PROCEDURE usp_GetEventMas
 @EventType   AS CHAR(2), 
 @EventKey1   AS CHAR(6), 
 @EventKey2   AS CHAR(8), 
 @EventNumber AS CHAR(6) 
 
AS 
  BEGIN 
      SELECT [TYPE] AS EventType, 
             key1   AS Eventkey1, 
             key2   AS Eventkey2, 
             numb   AS EventNumber, 
             prio   AS Priority, 
             idel   AS Deleted, 
             idow   AS TimeOrDayRelated, 
             sdat   AS StartDate, 
             edat   AS EndDate, 
             pric   AS SpecialPrice, 
             bqty   AS BuyQuantity, 
             gqty   AS GetQuantity, 
             vdis   AS DiscountAmount, 
             pdis   AS PercentageDiscount, 
             buycpn AS BuyCouponID, 
             getcpn AS SellCouponID 
      FROM   EVTMAS 
      WHERE  [TYPE] = @EventType 
             AND key1 = @EventKey1 
             AND key2 = @EventKey2 
             AND numb = @EventNumber 
  END
GO

