﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_WSCUPD]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_WSCUPD'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_WSCUPD] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_WSCUPD'
GO
ALTER PROCEDURE dbo.KevanConversion_WSCUPD
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.1
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 41 - Update the Workstation Configuration
-----------------------------------------------------------------------------------

AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Task     : 41/01 - Update the Workstation ID Profiles
----------------------------------------------------------------------------------- 
UPDATE WorkstationConfig SET SecurityProfileID = '1' Where ID = '1'
UPDATE WorkstationConfig SET SecurityProfileID = '2' Where ID = '2'
UPDATE WorkstationConfig SET SecurityProfileID = '3' Where ID = '3'
UPDATE WorkstationConfig SET SecurityProfileID = '4' Where ID = '4'
UPDATE WorkstationConfig SET SecurityProfileID = '5' Where ID = '5'
UPDATE WorkstationConfig SET SecurityProfileID = '6' Where ID = '6'
UPDATE WorkstationConfig SET SecurityProfileID = '7' Where ID = '7'
UPDATE WorkstationConfig SET SecurityProfileID = '8' Where ID = '8'
UPDATE WorkstationConfig SET SecurityProfileID = '9' Where ID = '9'
UPDATE WorkstationConfig SET SecurityProfileID = '10' Where ID = '10'
UPDATE WorkstationConfig SET SecurityProfileID = '11' Where ID = '11'
UPDATE WorkstationConfig SET SecurityProfileID = '12' Where ID = '12'
UPDATE WorkstationConfig SET SecurityProfileID = '13' Where ID = '13'
UPDATE WorkstationConfig SET SecurityProfileID = '15' Where ID = '15'
UPDATE WorkstationConfig SET SecurityProfileID = '16' Where ID = '16'
UPDATE WorkstationConfig SET SecurityProfileID = '17' Where ID = '17'
UPDATE WorkstationConfig SET SecurityProfileID = '18' Where ID = '18'
UPDATE WorkstationConfig SET SecurityProfileID = '19' Where ID = '19'
UPDATE WorkstationConfig SET SecurityProfileID = '20' Where ID = '20'
UPDATE WorkstationConfig SET SecurityProfileID = '21' Where ID = '21'
UPDATE WorkstationConfig SET SecurityProfileID = '22' Where ID = '22'
UPDATE WorkstationConfig SET SecurityProfileID = '23' Where ID = '23'
UPDATE WorkstationConfig SET SecurityProfileID = '24' Where ID = '24'
UPDATE WorkstationConfig SET SecurityProfileID = '25' Where ID = '25'
UPDATE WorkstationConfig SET SecurityProfileID = '27' Where ID = '27'
UPDATE WorkstationConfig SET SecurityProfileID = '28' Where ID = '28'
UPDATE WorkstationConfig SET SecurityProfileID = '29' Where ID = '29'
UPDATE WorkstationConfig SET SecurityProfileID = '30' Where ID = '30'
UPDATE WorkstationConfig SET SecurityProfileID = '31' Where ID = '31'
UPDATE WorkstationConfig SET SecurityProfileID = '32' Where ID = '32'
UPDATE WorkstationConfig SET SecurityProfileID = '40' Where ID = '40'
UPDATE WorkstationConfig SET SecurityProfileID = '55' Where ID = '40'
UPDATE WorkstationConfig SET SecurityProfileID = '60' Where ID = '40'
UPDATE WorkstationConfig SET SecurityProfileID = '77' Where ID = '40'
UPDATE WorkstationConfig SET SecurityProfileID = '89' Where ID = '40'

END;
GO

