﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TransactionGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure TransactionGet'
	EXEC ('CREATE PROCEDURE [dbo].[TransactionGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure TransactionGet'
GO
ALTER PROCEDURE [dbo].[TransactionGet]
	@TranDate	date,
	@TillId		int,
	@TranNumber	char(4)
AS
begin
	SET NOCOUNT ON;

	select
        DATE1	as TranDate,
        TILL	as TillId,
        [TRAN]	as TranNumber,
        ORDN	as OrderNumber,
        VOID	as IsVoided,
        TMOD	as IsTraining,
        ICOM	as IsComplete
	from
		DLTOTS
	where
		DATE1 = @TranDate
		and TILL=@TillId
		and [TRAN]=@TranNumber
	
end
GO

