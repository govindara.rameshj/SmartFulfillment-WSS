﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ParametersGetIntegerValueById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ParametersGetIntegerValueById'
	EXEC ('CREATE PROCEDURE [dbo].[ParametersGetIntegerValueById] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ParametersGetIntegerValueById'
GO
ALTER PROCEDURE dbo.ParametersGetIntegerValueById
(
	@Id int
)
AS
	SET NOCOUNT ON;
SELECT     LongValue
FROM         Parameters
WHERE     (ParameterID = @Id)
GO

