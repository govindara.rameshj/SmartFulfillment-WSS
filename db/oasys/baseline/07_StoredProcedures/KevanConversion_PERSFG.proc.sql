﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_PERSFG]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_PERSFG'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_PERSFG] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_PERSFG'
GO
ALTER PROCEDURE dbo.KevanConversion_PERSFG
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 22 - Applying Security Permission to Scalar Functions..
-----------------------------------------------------------------------------------
-- Revision : 1.1
-- Author   : Kevan Madelin
-- Date	    : 3rd July 2011
-- Notes    : Changed to apply permissions if sp exists.
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Start Applying Permissions
-----------------------------------------------------------------------------------

-- SP svf_ReportGiftVoucherAllocationQtyDAY
If OBJECT_ID('svf_ReportGiftVoucherAllocationQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportGiftVoucherAllocationQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportGiftVoucherAllocationQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportGiftVoucherAllocationQtyDAY - Skipping Permissions (D.N.E)'
	End  

-- SP svf_ReportDuressCodeUsageWTD
If OBJECT_ID('svf_ReportDuressCodeUsageWTD') is not null
	Begin
	Print 'SP : svf_ReportDuressCodeUsageWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDuressCodeUsageWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDuressCodeUsageWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDuressCodeUsageDAY
If OBJECT_ID('svf_ReportDuressCodeUsageDAY') is not null
	Begin
	Print 'SP : svf_ReportDuressCodeUsageDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDuressCodeUsageDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDuressCodeUsageDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportNETSalesValueWTD
If OBJECT_ID('svf_ReportNETSalesValueWTD') is not null
	Begin
	Print 'SP : svf_ReportNETSalesValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportNETSalesValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportNETSalesValueWTD - Skipping Permissions (D.N.E)'
	End 


-- SP svf_ReportNETSalesValueDAY
If OBJECT_ID('svf_ReportNETSalesValueDAY') is not null
	Begin
	Print 'SP : svf_ReportNETSalesValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportNETSalesValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportNETSalesValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMiscOutgoingValueWTD
If OBJECT_ID('svf_ReportMiscOutgoingValueWTD') is not null
	Begin
	Print 'SP : svf_ReportMiscOutgoingValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMiscOutgoingValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMiscOutgoingValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMiscOutgoingValueDAY
If OBJECT_ID('svf_ReportMiscOutgoingValueDAY') is not null
	Begin
	Print 'SP : svf_ReportMiscOutgoingValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMiscOutgoingValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMiscOutgoingValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMiscOutgoingQtyWTD
If OBJECT_ID('svf_ReportMiscOutgoingQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportMiscOutgoingQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMiscOutgoingQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMiscOutgoingQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMiscOutgoingQtyDAY
If OBJECT_ID('svf_ReportMiscOutgoingQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportMiscOutgoingQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMiscOutgoingQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMiscOutgoingQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMiscIncomeValueWTD
If OBJECT_ID('svf_ReportMiscIncomeValueWTD') is not null
	Begin
	Print 'SP : svf_ReportMiscIncomeValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMiscIncomeValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMiscIncomeValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMiscIncomeValueDAY
If OBJECT_ID('svf_ReportMiscIncomeValueDAY') is not null
	Begin
	Print 'SP : svf_ReportMiscIncomeValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMiscIncomeValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMiscIncomeValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMiscIncomeQtyWTD
If OBJECT_ID('svf_ReportMiscIncomeQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportMiscIncomeQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMiscIncomeQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMiscIncomeQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMiscIncomeQtyDAY
If OBJECT_ID('svf_ReportMiscIncomeQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportMiscIncomeQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMiscIncomeQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMiscIncomeQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportRejectedOrdersValueWTD
If OBJECT_ID('svf_ReportRejectedOrdersValueWTD') is not null
	Begin
	Print 'SP : svf_ReportRejectedOrdersValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportRejectedOrdersValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportRejectedOrdersValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportRejectedOrdersValueDAY
If OBJECT_ID('svf_ReportRejectedOrdersValueDAY') is not null
	Begin
	Print 'SP : svf_ReportRejectedOrdersValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportRejectedOrdersValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportRejectedOrdersValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportRejectedOrdersQtyWTD
If OBJECT_ID('svf_ReportRejectedOrdersQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportRejectedOrdersQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportRejectedOrdersQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportRejectedOrdersQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportRejectedOrdersQtyDAY
If OBJECT_ID('svf_ReportRejectedOrdersQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportRejectedOrdersQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportRejectedOrdersQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportRejectedOrdersQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPriceChangesQtyDAY
If OBJECT_ID('svf_ReportPriceChangesQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportPriceChangesQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPriceChangesQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPriceChangesQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPriceChangesOverdueQty
If OBJECT_ID('svf_ReportPriceChangesOverdueQty') is not null
	Begin
	Print 'SP : svf_ReportPriceChangesOverdueQty - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPriceChangesOverdueQty] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPriceChangesOverdueQty - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPriceChangesOutstandingQty
If OBJECT_ID('svf_ReportPriceChangesOutstandingQty') is not null
	Begin
	Print 'SP : svf_ReportPriceChangesOutstandingQty - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPriceChangesOutstandingQty] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPriceChangesOutstandingQty - Skipping Permissions (D.N.E)'
	End 

-- SP svf_SystemStoreLocalID
If OBJECT_ID('svf_SystemStoreLocalID') is not null
	Begin
	Print 'SP : svf_SystemStoreLocalID - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_SystemStoreLocalID] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_SystemStoreLocalID - Skipping Permissions (D.N.E)'
	End 

-- SP svf_SystemStoreGroupID
If OBJECT_ID('svf_SystemStoreGroupID') is not null
	Begin
	Print 'SP : svf_SystemStoreGroupID - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_SystemStoreGroupID] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_SystemStoreGroupID - Skipping Permissions (D.N.E)'
	End 

-- SP svf_SystemPriceChangeVisibilityDate
If OBJECT_ID('svf_SystemPriceChangeVisibilityDate') is not null
	Begin
	Print 'SP : svf_SystemPriceChangeVisibilityDate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_SystemPriceChangeVisibilityDate] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_SystemPriceChangeVisibilityDate - Skipping Permissions (D.N.E)'
	End 

-- SP svf_SystemHouseKeepingDate
If OBJECT_ID('svf_SystemHouseKeepingDate') is not null
	Begin
	Print 'SP : svf_SystemHouseKeepingDate - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_SystemHouseKeepingDate] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_SystemHouseKeepingDate - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportStockHoldingValue
If OBJECT_ID('svf_ReportStockHoldingValue') is not null
	Begin
	Print 'SP : svf_ReportStockHoldingValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportStockHoldingValue] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportStockHoldingValue - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPriceChangesLabelsRequiredSmall
If OBJECT_ID('svf_ReportPriceChangesLabelsRequiredSmall') is not null
	Begin
	Print 'SP : svf_ReportPriceChangesLabelsRequiredSmall - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPriceChangesLabelsRequiredSmall] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPriceChangesLabelsRequiredSmall - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPriceChangesLabelsRequiredPeg
If OBJECT_ID('svf_ReportPriceChangesLabelsRequiredPeg') is not null
	Begin
	Print 'SP : svf_ReportPriceChangesLabelsRequiredPeg - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPriceChangesLabelsRequiredPeg] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPriceChangesLabelsRequiredPeg - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPriceChangesLabelsRequiredMedium
If OBJECT_ID('svf_ReportPriceChangesLabelsRequiredMedium') is not null
	Begin
	Print 'SP : svf_ReportPriceChangesLabelsRequiredMedium - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPriceChangesLabelsRequiredMedium] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPriceChangesLabelsRequiredMedium - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPriceChangesConfirmedDAY
If OBJECT_ID('svf_ReportPriceChangesConfirmedDAY') is not null
	Begin
	Print 'SP : svf_ReportPriceChangesConfirmedDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPriceChangesConfirmedDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPriceChangesConfirmedDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPendingWriteOffValueDAY
If OBJECT_ID('svf_ReportPendingWriteOffValueDAY') is not null
	Begin
	Print 'SP : svf_ReportPendingWriteOffValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPendingWriteOffValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPendingWriteOffValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportPendingWriteOffQtyDAY
If OBJECT_ID('svf_ReportPendingWriteOffQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportPendingWriteOffQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportPendingWriteOffQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportPendingWriteOffQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportOutOfStocks
If OBJECT_ID('svf_ReportOutOfStocks') is not null
	Begin
	Print 'SP : svf_ReportOutOfStocks - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportOutOfStocks] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportOutOfStocks - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportOpenReturnsValue
If OBJECT_ID('svf_ReportOpenReturnsValue') is not null
	Begin
	Print 'SP : svf_ReportOpenReturnsValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportOpenReturnsValue] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportOpenReturnsValue - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportOpenReturnsQty
If OBJECT_ID('svf_ReportOpenReturnsQty') is not null
	Begin
	Print 'SP : svf_ReportOpenReturnsQty - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportOpenReturnsQty] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportOpenReturnsQty - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportNonStockValue
If OBJECT_ID('svf_ReportNonStockValue') is not null
	Begin
	Print 'SP : svf_ReportNonStockValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportNonStockValue] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportNonStockValue - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportNonStockQty
If OBJECT_ID('svf_ReportNonStockQty') is not null
	Begin
	Print 'SP : svf_ReportNonStockQty - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportNonStockQty] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportNonStockQty - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMarkdownStockValue
If OBJECT_ID('svf_ReportMarkdownStockValue') is not null
	Begin
	Print 'SP : svf_ReportMarkdownStockValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMarkdownStockValue] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMarkdownStockValue - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportMarkdownStockQty
If OBJECT_ID('svf_ReportMarkdownStockQty') is not null
	Begin
	Print 'SP : svf_ReportMarkdownStockQty - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportMarkdownStockQty] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportMarkdownStockQty - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeliveryTransValueWTD
If OBJECT_ID('svf_ReportDeliveryTransValueWTD') is not null
	Begin
	Print 'SP : svf_ReportDeliveryTransValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeliveryTransValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeliveryTransValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeliveryTransValueDAY
If OBJECT_ID('svf_ReportDeliveryTransValueDAY') is not null
	Begin
	Print 'SP : svf_ReportDeliveryTransValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeliveryTransValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeliveryTransValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeliveryTransQtyWTD
If OBJECT_ID('svf_ReportDeliveryTransQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportDeliveryTransQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeliveryTransQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeliveryTransQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeliveryTransQtyDAY
If OBJECT_ID('svf_ReportDeliveryTransQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportDeliveryTransQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeliveryTransQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeliveryTransQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeliveryChargesValueWTD
If OBJECT_ID('svf_ReportDeliveryChargesValueWTD') is not null
	Begin
	Print 'SP : svf_ReportDeliveryChargesValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeliveryChargesValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeliveryChargesValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeliveryChargesValueDAY
If OBJECT_ID('svf_ReportDeliveryChargesValueDAY') is not null
	Begin
	Print 'SP : svf_ReportDeliveryChargesValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeliveryChargesValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeliveryChargesValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeliveryChargesQtyWTD
If OBJECT_ID('svf_ReportDeliveryChargesQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportDeliveryChargesQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeliveryChargesQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeliveryChargesQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeliveryChargesQtyDAY
If OBJECT_ID('svf_ReportDeliveryChargesQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportDeliveryChargesQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeliveryChargesQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeliveryChargesQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeletedStockValue
If OBJECT_ID('svf_ReportDeletedStockValue') is not null
	Begin
	Print 'SP : svf_ReportDeletedStockValue - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeletedStockValue] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeletedStockValue - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportDeletedStockQty
If OBJECT_ID('svf_ReportDeletedStockQty') is not null
	Begin
	Print 'SP : svf_ReportDeletedStockQty - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportDeletedStockQty] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportDeletedStockQty - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportBelowImpacts
If OBJECT_ID('svf_ReportBelowImpacts') is not null
	Begin
	Print 'SP : svf_ReportBelowImpacts - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportBelowImpacts] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportBelowImpacts - Skipping Permissions (D.N.E)'
	End 

-- SP NewBankingTotalFloatVariance
If OBJECT_ID('NewBankingTotalFloatVariance') is not null
	Begin
	Print 'SP : NewBankingTotalFloatVariance - Applying Permissions'
	GRANT EXECUTE ON [dbo].[NewBankingTotalFloatVariance] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : NewBankingTotalFloatVariance - Skipping Permissions (D.N.E)'
	End 

-- SP StockImageLocation
If OBJECT_ID('StockImageLocation') is not null
	Begin
	Print 'SP : StockImageLocation - Applying Permissions'
	GRANT EXECUTE ON [dbo].[StockImageLocation] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : StockImageLocation - Skipping Permissions (D.N.E)'
	End 

-- SP Udf_ValidFuzzyMatchLike
If OBJECT_ID('Udf_ValidFuzzyMatchLike') is not null
	Begin
	Print 'SP : Udf_ValidFuzzyMatchLike - Applying Permissions'
	GRANT EXECUTE ON [dbo].[Udf_ValidFuzzyMatchLike] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : Udf_ValidFuzzyMatchLike - Skipping Permissions (D.N.E)'
	End 

-- SP Udf_ValidFuzzyMatchSoundex
If OBJECT_ID('Udf_ValidFuzzyMatchSoundex') is not null
	Begin
	Print 'SP : Udf_ValidFuzzyMatchSoundex - Applying Permissions'
	GRANT EXECUTE ON [dbo].[Udf_ValidFuzzyMatchSoundex] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : Udf_ValidFuzzyMatchSoundex - Skipping Permissions (D.N.E)'
	End 

-- SP udf_EventWhenActive
If OBJECT_ID('udf_EventWhenActive') is not null
	Begin
	Print 'SP : udf_EventWhenActive - Applying Permissions'
	GRANT EXECUTE ON [dbo].[udf_EventWhenActive] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : udf_EventWhenActive - Skipping Permissions (D.N.E)'
	End 

-- SP udf_EventGetDiscount
If OBJECT_ID('udf_EventGetDiscount') is not null
	Begin
	Print 'SP : udf_EventGetDiscount - Applying Permissions'
	GRANT EXECUTE ON [dbo].[udf_EventGetDiscount] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : udf_EventGetDiscount - Skipping Permissions (D.N.E)'
	End 

-- SP udf_EventGetCategory
If OBJECT_ID('udf_EventGetCategory') is not null
	Begin
	Print 'SP : udf_EventGetCategory - Applying Permissions'
	GRANT EXECUTE ON [dbo].[udf_EventGetCategory] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : udf_EventGetCategory - Skipping Permissions (D.N.E)'
	End 

-- SP udf_EventIsActive
If OBJECT_ID('udf_EventIsActive') is not null
	Begin
	Print 'SP : udf_EventIsActive - Applying Permissions'
	GRANT EXECUTE ON [dbo].[udf_EventIsActive] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : udf_EventIsActive - Skipping Permissions (D.N.E)'
	End 

-- SP udf_EventGetTypeDescription
If OBJECT_ID('udf_EventGetTypeDescription') is not null
	Begin
	Print 'SP : udf_EventGetTypeDescription - Applying Permissions'
	GRANT EXECUTE ON [dbo].[udf_EventGetTypeDescription] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : udf_EventGetTypeDescription - Skipping Permissions (D.N.E)'
	End 

-- SP udf_StorePostCode
If OBJECT_ID('udf_StorePostCode') is not null
	Begin
	Print 'SP : udf_StorePostCode - Applying Permissions'
	GRANT EXECUTE ON [dbo].[udf_StorePostCode] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : udf_StorePostCode - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportNETSalesValuePrevious7Days
If OBJECT_ID('svf_ReportNETSalesValuePrevious7Days') is not null
	Begin
	Print 'SP : svf_ReportNETSalesValuePrevious7Days - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportNETSalesValuePrevious7Days] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportNETSalesValuePrevious7Days - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreVouchersValueWTD
If OBJECT_ID('svf_ReportCoreVouchersValueWTD') is not null
	Begin
	Print 'SP : svf_ReportCoreVouchersValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreVouchersValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreVouchersValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreVouchersValueDAY
If OBJECT_ID('svf_ReportCoreVouchersValueDAY') is not null
	Begin
	Print 'SP : svf_ReportCoreVouchersValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreVouchersValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreVouchersValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreVouchersQtyWTD
If OBJECT_ID('svf_ReportCoreVouchersQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportCoreVouchersQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreVouchersQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreVouchersQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreVouchersQtyDAY
If OBJECT_ID('svf_ReportCoreVouchersQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportCoreVouchersQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreVouchersQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreVouchersQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreScanningQtyWTD
If OBJECT_ID('svf_ReportCoreScanningQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportCoreScanningQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreScanningQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreScanningQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreScanningQtyDAY
If OBJECT_ID('svf_ReportCoreScanningQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportCoreScanningQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreScanningQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreScanningQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP udf_PadNumbers
If OBJECT_ID('udf_PadNumbers') is not null
	Begin
	Print 'SP : udf_PadNumbers - Applying Permissions'
	GRANT EXECUTE ON [dbo].[udf_PadNumbers] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : udf_PadNumbers - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreSalesValueWTD
If OBJECT_ID('svf_ReportCoreSalesValueWTD') is not null
	Begin
	Print 'SP : svf_ReportCoreSalesValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreSalesValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreSalesValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP udf_StripVowels
If OBJECT_ID('udf_StripVowels') is not null
	Begin
	Print 'SP : udf_StripVowels - Applying Permissions'
	GRANT EXECUTE ON [dbo].[udf_StripVowels] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : udf_StripVowels - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreSalesValueDAY
If OBJECT_ID('svf_ReportCoreSalesValueDAY') is not null
	Begin
	Print 'SP : svf_ReportCoreSalesValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreSalesValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreSalesValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreSalesQtyWTD
If OBJECT_ID('svf_ReportCoreSalesQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportCoreSalesQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreSalesQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreSalesQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreSalesQtyDAY
If OBJECT_ID('svf_ReportCoreSalesQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportCoreSalesQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreSalesQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreSalesQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreRefundsValueWTD
If OBJECT_ID('svf_ReportCoreRefundsValueWTD') is not null
	Begin
	Print 'SP : svf_ReportCoreRefundsValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreRefundsValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreRefundsValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreRefundsValueDAY
If OBJECT_ID('svf_ReportCoreRefundsValueDAY') is not null
	Begin
	Print 'SP : svf_ReportCoreRefundsValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreRefundsValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreRefundsValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreRefundsQtyWTD
If OBJECT_ID('svf_ReportCoreRefundsQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportCoreRefundsQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreRefundsQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreRefundsQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreRefundsQtyDAY
If OBJECT_ID('svf_ReportCoreRefundsQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportCoreRefundsQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreRefundsQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreRefundsQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreLinesSoldQtyWTD
If OBJECT_ID('svf_ReportCoreLinesSoldQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportCoreLinesSoldQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreLinesSoldQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreLinesSoldQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCoreLinesSoldQtyDAY
If OBJECT_ID('svf_ReportCoreLinesSoldQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportCoreLinesSoldQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCoreLinesSoldQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCoreLinesSoldQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportConsignmentsNotReceived
If OBJECT_ID('svf_ReportConsignmentsNotReceived') is not null
	Begin
	Print 'SP : svf_ReportConsignmentsNotReceived - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportConsignmentsNotReceived] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportConsignmentsNotReceived - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportColleagueDiscountValueWTD
If OBJECT_ID('svf_ReportColleagueDiscountValueWTD') is not null
	Begin
	Print 'SP : svf_ReportColleagueDiscountValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportColleagueDiscountValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportColleagueDiscountValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportColleagueDiscountValueDAY
If OBJECT_ID('svf_ReportColleagueDiscountValueDAY') is not null
	Begin
	Print 'SP : svf_ReportColleagueDiscountValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportColleagueDiscountValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportColleagueDiscountValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportColleagueDiscountQtyWTD
If OBJECT_ID('svf_ReportColleagueDiscountQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportColleagueDiscountQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportColleagueDiscountQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportColleagueDiscountQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportColleagueDiscountQtyDAY
If OBJECT_ID('svf_ReportColleagueDiscountQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportColleagueDiscountQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportColleagueDiscountQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportColleagueDiscountQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCardSummaryValueWTD
If OBJECT_ID('svf_ReportCardSummaryValueWTD') is not null
	Begin
	Print 'SP : svf_ReportCardSummaryValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCardSummaryValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCardSummaryValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCardSummaryValueDAY
If OBJECT_ID('svf_ReportCardSummaryValueDAY') is not null
	Begin
	Print 'SP : svf_ReportCardSummaryValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCardSummaryValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCardSummaryValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCardSummaryQtyWTD
If OBJECT_ID('svf_ReportCardSummaryQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportCardSummaryQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCardSummaryQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCardSummaryQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportCardSummaryQtyDAY
If OBJECT_ID('svf_ReportCardSummaryQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportCardSummaryQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportCardSummaryQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportCardSummaryQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportKBSalesValueWTD
If OBJECT_ID('svf_ReportKBSalesValueWTD') is not null
	Begin
	Print 'SP : svf_ReportKBSalesValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportKBSalesValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportKBSalesValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportKBSalesValueDAY
If OBJECT_ID('svf_ReportKBSalesValueDAY') is not null
	Begin
	Print 'SP : svf_ReportKBSalesValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportKBSalesValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportKBSalesValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportKBSalesQtyWTD
If OBJECT_ID('svf_ReportKBSalesQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportKBSalesQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportKBSalesQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportKBSalesQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportKBSalesQtyDAY
If OBJECT_ID('svf_ReportKBSalesQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportKBSalesQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportKBSalesQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportKBSalesQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportKBDepositsValueWTD
If OBJECT_ID('svf_ReportKBDepositsValueWTD') is not null
	Begin
	Print 'SP : svf_ReportKBDepositsValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportKBDepositsValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportKBDepositsValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportKBDepositsValueDAY
If OBJECT_ID('svf_ReportKBDepositsValueDAY') is not null
	Begin
	Print 'SP : svf_ReportKBDepositsValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportKBDepositsValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportKBDepositsValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportKBDepositsQtyWTD
If OBJECT_ID('svf_ReportKBDepositsQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportKBDepositsQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportKBDepositsQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportKBDepositsQtyWTD - Skipping Permissions (D.N.E)'
	End

-- SP svf_ReportKBDepositsQtyDAY
If OBJECT_ID('svf_ReportKBDepositsQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportKBDepositsQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportKBDepositsQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportKBDepositsQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportGiftVoucherRedemptionValueWTD
If OBJECT_ID('svf_ReportGiftVoucherRedemptionValueWTD') is not null
	Begin
	Print 'SP : svf_ReportGiftVoucherRedemptionValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportGiftVoucherRedemptionValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportGiftVoucherRedemptionValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportGiftVoucherRedemptionValueDAY
If OBJECT_ID('svf_ReportGiftVoucherRedemptionValueDAY') is not null
	Begin
	Print 'SP : svf_ReportGiftVoucherRedemptionValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportGiftVoucherRedemptionValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportGiftVoucherRedemptionValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportGiftVoucherRedemptionQtyWTD
If OBJECT_ID('svf_ReportGiftVoucherRedemptionQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportGiftVoucherRedemptionQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportGiftVoucherRedemptionQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportGiftVoucherRedemptionQtyWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportGiftVoucherRedemptionQtyDAY
If OBJECT_ID('svf_ReportGiftVoucherRedemptionQtyDAY') is not null
	Begin
	Print 'SP : svf_ReportGiftVoucherRedemptionQtyDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportGiftVoucherRedemptionQtyDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportGiftVoucherRedemptionQtyDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportGiftVoucherAllocationValueWTD
If OBJECT_ID('svf_ReportGiftVoucherAllocationValueWTD') is not null
	Begin
	Print 'SP : svf_ReportGiftVoucherAllocationValueWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportGiftVoucherAllocationValueWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportGiftVoucherAllocationValueWTD - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportGiftVoucherAllocationValueDAY
If OBJECT_ID('svf_ReportGiftVoucherAllocationValueDAY') is not null
	Begin
	Print 'SP : svf_ReportGiftVoucherAllocationValueDAY - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportGiftVoucherAllocationValueDAY] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportGiftVoucherAllocationValueDAY - Skipping Permissions (D.N.E)'
	End 

-- SP svf_ReportGiftVoucherAllocationQtyWTD
If OBJECT_ID('svf_ReportGiftVoucherAllocationQtyWTD') is not null
	Begin
	Print 'SP : svf_ReportGiftVoucherAllocationQtyWTD - Applying Permissions'
	GRANT EXECUTE ON [dbo].[svf_ReportGiftVoucherAllocationQtyWTD] TO [role_execproc] WITH GRANT OPTION
	End
Else
	Begin
	Print 'SP : svf_ReportGiftVoucherAllocationQtyWTD - Skipping Permissions (D.N.E)'
	End 

-----------------------------------------------------------------------------------
-- Permissions Applied
-----------------------------------------------------------------------------------

END;
GO

