﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetStocksBySupplier]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetStocksBySupplier'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetStocksBySupplier] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetStocksBySupplier'
GO
ALTER PROCEDURE [dbo].[StockGetStocksBySupplier]
	@SupplierNumber		char(5),
	@Version			int = Null OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	SET @Version = 1
	
	SELECT
		st.skun				AS 'SkuNumber',
		st.descr			AS 'Description',
		st.prod				AS 'ProductCode',
		st.pack				AS 'PackSize',
		st.pric				AS 'Price',
		st.WGHT				AS 'Weight',
		st.COST				AS 'Cost',
		st.onha				AS 'OnHandQty',
		st.onor				AS 'OnOrderQty',
		st.mini				AS 'MinimumQty',
		st.maxi				AS 'MaximumQty',
		st.retq				AS 'OpenReturnsQty',
		st.mdnq				AS 'MarkdownQty',
		st.wtfq				AS 'WriteOffQty',
		st.INON				AS 'IsNonStock',
		st.IOBS				AS 'IsObsolete',
		st.IDEL				AS 'IsDeleted',
		st.IRIS				AS 'IsItemSingle',
		st.NOOR				AS 'IsNonOrder',
		st.treq				AS 'ReceivedTodayQty',
		st.trev				AS 'ReceivedTodayValue',
		st.dats				AS 'DateFirstStocked',
		st.IODT				AS 'DateFirstOrder',
		st.FODT				AS 'DateFinalOrder',
		st.drec				AS 'DateLastReceived',
		st.SUPP				AS 'SupplierNumber',
		rtrim(sm.NAME)		AS 'SupplierName',
		st.CTGY				AS 'HieCategory',
		rtrim(hc.DESCR)		AS 'HieCategoryName',
		st.GRUP				AS 'HieGroup',
		st.SGRP				AS 'HieSubgroup',
		st.STYL				AS 'HieStyle',	
		st.tact				AS 'ActivityToday',
		st.us001			AS 'UnitSales1',
		st.us002			AS 'UnitSales2',
		st.us003			AS 'UnitSales3',
		st.us004			AS 'UnitSales4',
		st.us005			AS 'UnitSales5',
		st.us006			AS 'UnitSales6',
		st.us007			AS 'UnitSales7',
		st.us008			AS 'UnitSales8',
		st.us009			AS 'UnitSales9',
		st.us0010			AS 'UnitSales10',
		st.us0011			AS 'UnitSales11',
		st.us0012			AS 'UnitSales12',
		st.us0013			AS 'UnitSales13',
		st.us0014			AS 'UnitSales14',
		st.do001			AS 'DaysOutStock1',	
		st.do002			AS 'DaysOutStock2',	
		st.do003			AS 'DaysOutStock3',	
		st.do004			AS 'DaysOutStock4',	
		st.do005			AS 'DaysOutStock5',	
		st.do006			AS 'DaysOutStock6',	
		st.do007			AS 'DaysOutStock7',	
		st.do008			AS 'DaysOutStock8',	
		st.do009			AS 'DaysOutStock9',	
		st.do0010			AS 'DaysOutStock10',	
		st.do0011			AS 'DaysOutStock11',	
		st.do0012			AS 'DaysOutStock12',	
		st.do0013			AS 'DaysOutStock13',
		st.do0014			AS 'DaysOutStock14',
		st.DateLastSoqInit,
		st.IsInitialised,
		st.ToForecast,	
		st.FlierPeriod1,
		st.FlierPeriod2,
		st.FlierPeriod3,
		st.FlierPeriod4,
		st.FlierPeriod5,
		st.FlierPeriod6,
		st.FlierPeriod7,
		st.FlierPeriod8,
		st.FlierPeriod9,
		st.FlierPeriod10,
		st.FlierPeriod11,
		st.FlierPeriod12,
		st.DemandPattern,
		st.PeriodDemand,
		st.PeriodTrend,
		st.ErrorForecast,
		st.ErrorSmoothed,
		st.BufferConversion,
		st.BufferStock,
		st.ServiceLevelOverride,
		st.ValueAnnualUsage,
		st.OrderLevel,
		st.SaleWeightSeason,
		st.SaleWeightBank,
		st.SaleWeightPromo,
		(SELECT SUM(capacity) FROM PLANGRAM WHERE SKUN = st.SKUN) AS 'Capacity'
	FROM
		stkmas st
	INNER JOIN 
		SUPMAS sm ON st.SUPP=sm.SUPN 
	INNER JOIN 
		HIECAT hc ON st.CTGY=hc.NUMB 
	WHERE
		st.sup1		= @SupplierNumber
		and	st.idel = 0
		and	st.iobs = 0
		and	st.iris = 0
		and (st.iodt <= getdate() or st.iodt is NULL)
	ORDER BY
		st.ctgy,
		st.grup,
		st.sgrp,
		st.styl,
		st.skun
END
GO

