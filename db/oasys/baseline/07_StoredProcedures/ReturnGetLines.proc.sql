﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReturnGetLines]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReturnGetLines'
	EXEC ('CREATE PROCEDURE [dbo].[ReturnGetLines] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReturnGetLines'
GO
ALTER PROCEDURE [dbo].[ReturnGetLines]
@ReturnId INT
AS
begin
	SELECT		
		rl.TKEY		as Id, 
		rl.HKEY		as ReturnId, 
		rl.SKUN		as SkuNumber, 
		sk.DESCR	as SkuDescription, 
		rl.QUAN		as Qty, 
		rl.PRIC		as Price, 
		rl.COST		as Cost, 
		rl.REAS		as ReasonCode, 
		sc.DESCR	as ReasonDescription
	FROM        
		RETLIN rl
	INNER JOIN 
		STKMAS sk ON rl.SKUN = sk.SKUN 
	INNER JOIN 
		SYSCOD sc ON rl.REAS = sc.CODE and sc.TYPE='SR'
	WHERE		
		rl.HKEY = @ReturnId
	ORDER BY	
		SkuDescription

end
GO

