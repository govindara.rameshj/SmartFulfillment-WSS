﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetRefundType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetRefundType'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetRefundType] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetRefundType'
GO
ALTER PROCEDURE usp_GetRefundType @ReportDate AS date , 
                                  @TillId AS varchar( 2
                                                    ) , 
                                  @TranNo AS varchar( 4
                                                    )
AS
BEGIN
    SELECT COUNT( *
                )
      FROM
           DLLINE DL LEFT OUTER JOIN STKMAS IM
           ON IM.SKUN
              = 
              DL.SKUN
      WHERE DATE1
            = 
            @ReportDate
        AND TILL = @TillId
        AND [TRAN] = @TranNo
        AND QUAN > 0
        AND LREV = 0;

END;
GO

