﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetVehicle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetVehicle'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetVehicle] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetVehicle'
GO
ALTER PROCEDURE dbo.usp_GetVehicle( @VehicleID int = NULL , 
                                     @DepotID int = NULL
                                   )
AS
BEGIN
    SELECT Vehicle.ID , 
           Vehicle.DepotID , 
           Vehicle.Registration , 
           Vehicle.VehicleTypeID , 
           Vehicle.StartTime , 
           Vehicle.StopTime , 
           Vehicle.MaximumWeight AS MaximumWeight , 
           Vehicle.MaximumVolume , 
           VehicleTypeDescription = ISNULL( VehicleType.Description , ''
                                          ) , 
           Vehicle.lastUpdateTime
      FROM
           Vehicle WITH ( NOLOCK
                        ) INNER JOIN StaticData AS VehicleType
           ON Vehicle.VehicleTypeID
              = 
              VehicleType.ID
      WHERE Vehicle.ID
            = 
            COALESCE( @VehicleID , Vehicle.ID
                    )
        AND Vehicle.DepotID
            = 
            COALESCE( @DepotID , Vehicle.DepotID
                    );
END;
GO

