﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetVehicleType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetVehicleType'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetVehicleType] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetVehicleType'
GO
ALTER PROCEDURE [dbo].[usp_GetVehicleType]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT ID, Description 
	FROM  STATICDATA WITH (NOLOCK)
	WHERE STATICDATATYPEID =	(
							SELECT ID 
							FROM STATICDATATYPE ITH (NOLOCK)
							WHERE DESCRIPTION  ='VehicleType'
								)
		AND [Enabled] = 1
	ORDER BY CAST(Value as int) 
	/*
	SELECT ID, Description 
	FROM dbo.VehicleType WITH (NOLOCK)
	WHERE ENABLED=1

	*/

	SET NOCOUNT OFF;
END
GO

