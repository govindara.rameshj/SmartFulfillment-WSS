﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingFloatReAssigned]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingFloatReAssigned'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingFloatReAssigned] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingFloatReAssigned'
GO
ALTER PROCedure NewBankingFloatReAssigned
   @FloatBagID       int,
   @AccountabilityID int
as
begin
set nocount on

update SafeBags set AccountabilityID = @AccountabilityID where ID = @FloatBagID
end
GO

