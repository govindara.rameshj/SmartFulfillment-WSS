﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_VISCBT]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_VISCBT'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_VISCBT] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_VISCBT'
GO
ALTER PROCEDURE dbo.KevanConversion_VISCBT
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 26 - Updating Un-Processed Vision Sales to New Banking System - Part 2
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.1 
-- Revision : 1.0
-- Author   : Dhanesh Ramachandran
-- Date	    : 19th July 2011
-- 
-- Task     : 25 - Updating Un-Processed Vision Sales to New Banking System - Part 1
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF
        
create table #BankingTemp1
(
   
    RID INT IDENTITY(1,1),
    [PeriodID] [int] NOT NULL,
	[CashierID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[ID] [decimal](9,2) NOT NULL,	
	--[Quantity] [int] NULL,           -- use DT.TOTL & VT.Value
	[Amount] [decimal](9,2) NULL,
	[PickUp] [decimal](9, 2) NOT NULL
	        -- use DT.DISC & VT.ValueDiscount
)

create table #CashBalTenTemp
(
    [PeriodID] [int] NOT NULL,
	[CashierID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[ID] [int] NOT NULL,
	[Quantity] [decimal](5, 0) NOT NULL,
	[Amount] [decimal](9, 2) NOT NULL,
	[PickUp] [decimal](9, 2) NOT NULL
)

INSERT INTO #BankingTemp1 ([PeriodID],[CurrencyID],[CashierID] ,[ID],[Amount],[PickUp])
SELECT 

	(select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	dp.[Type] as ID,
	--dp.NUMB as Quantity,
	ISNULL(dp.AMNT,0) as Amount,
	'0'	
	
FROM DLTOTS as DT 
     inner join DLPAID DP on  dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL
	inner join CBSCTL as cc on cc.DATE1 = dt.DATE1 
	
where 
   (DT.TCOD = 'M+' and DT.MISC = '20' 
   and cc.COMM = '0'
   ) 
or (DT.TCOD = 'M-' and DT.MISC = '20' 
and cc.COMM = '0'
) -- See 1, must be changed!!

INSERT INTO #BankingTemp1([PeriodID],[CurrencyID],[CashierID] ,[ID],[Amount],[PickUp])
SELECT 
	--vt.TranDate as 'Date',
    (select id from SystemPeriods where StartDate = vt.TranDate) as PeriodID,
    (select id from SystemCurrency) as CurrencyID, 
	vt.CashierId as CashierID, 
	VP.TenderTypeId  as ID,
	--VP.Number as Quantity,
	ISNULL(VP.ValueTender,0) as Amount,
	'0'	
		
FROM VisionTotal as VT
    inner join VisionPayment VP on VT.TranDate = VP.TranDate and VT.TillId = VP.TillId and VT.TranNumber = VP.TranNumber  
	inner join CBSCTL as cc on cc.DATE1 = vt.TranDate 
	
WHERE
   (VT.TYPE = 'SA' and cc.COMM = '0')   
or (VT.TYPE = 'RF' and cc.COMM = '0') 

INSERT INTO #CashBalTenTemp 
SELECT * FROM CashBalCashierTen 

    DECLARE @RID_1 INT
	DECLARE @MAXID_1 INT
	SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp1 -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID_1 = 1
	DECLARE @PERIODID INT
	DECLARE @CURRENCY CHAR(5)
	DECLARE @CASHIER_ID INT
	DECLARE @ID INT
	 
-- looping the #temp_old from first record to last record.
	WHILE(@RID_1<=@MAXID_1)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @PERIODID = PeriodID,@CURRENCY= CurrencyID,@CASHIER_ID= CashierID, @ID = ID FROM #BankingTemp1 WHERE RID = @RID_1
			-- if the Primary key exists then update, by taking the values from the #temp_old  
			
		IF EXISTS(SELECT 1 FROM #CashBalTenTemp WHERE PeriodID  = @PERIODID AND CurrencyID = @CURRENCY AND CashierID=@CASHIER_ID AND ID = @ID)
		
		  BEGIN
		   UPDATE CashBalCashierTen 
		   SET 
			[Quantity] = CashBalCashierTen.[Quantity]+1 ,
	        [Amount] = CashBalCashierTen.[Amount]-T.Amount
	        --[PickUp] = CashBalCashierTen.[Amount]-T.Amount
	       FROM #BankingTemp1 AS T WHERE (CashBalCashierTen.PeriodID = @PERIODID AND T.PeriodID = @PERIODID) AND (CashBalCashierTen.CurrencyID = @CURRENCY and  T.CurrencyID =@CURRENCY ) AND (CashBalCashierTen.CashierID = @CASHIER_ID and  T.CashierID = @CASHIER_ID) AND (CashBalCashierTen.ID  = @ID  AND T.ID = @ID)
	     END
     ELSE
       BEGIN
        INSERT INTO CashBalCashierTen(PeriodID,CurrencyID,CashierID,ID,Quantity,Amount,PickUp)
        SELECT T.PeriodID, T.CurrencyID,T.CashierID,T.ID, 1,-(T.Amount),(T.PickUp)
      	FROM #BankingTemp1   AS T 
		WHERE T.RID = @RID_1
     END
     
      
     delete #BankingTemp1 where RID = @RID_1
	 SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp1
				
     INSERT INTO #CashBalTenTemp
      SELECT * FROM CashBalCashierTen  
     	  
SET @RID_1 = @RID_1+1 
 END
END
GO

