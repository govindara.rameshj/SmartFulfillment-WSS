﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StoreGetStore]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StoreGetStore'
	EXEC ('CREATE PROCEDURE [dbo].[StoreGetStore] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StoreGetStore'
GO
ALTER PROCEDURE [dbo].[StoreGetStore]
	@Id int
AS
begin
	SET NOCOUNT ON;
	
	SELECT     
		*
	FROM 
		Store
	WHERE
		Id = @Id
end
GO

