﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_ResetTillPermissions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_ResetTillPermissions'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_ResetTillPermissions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_ResetTillPermissions'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_ResetTillPermissions]
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 17th July 2013
-- Description	: Resets Till Permissions to Default Model
-----------------------------------------------------------------------------------
@sp_User Char(30)
AS
BEGIN
SET NOCOUNT ON

---------------------------------------------------------------------------------------------------
-- Reset Permissions for Stored Procedures / Functions / Views / Tables
---------------------------------------------------------------------------------------------------
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#ResetPerm_Procedures%') and [TYPE] = 'U') Drop Table #ResetPerm_Procedures
Create Table #ResetPerm_Procedures([ID] [int] IDENTITY(1,1),[Procedure] Char(100) NOT NULL)
Insert into #ResetPerm_Procedures
Select Name From Oasys.Sys.sysobjects Where XType in ('P') Order By Name
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#ResetPerm_Functions%') and [TYPE] = 'U') Drop Table #ResetPerm_Functions
Create Table #ResetPerm_Functions([ID] [int] IDENTITY(1,1),[Function] Char(100) NOT NULL)
Insert into #ResetPerm_Functions
Select Name From Oasys.Sys.sysobjects Where XType in ('IF','TF','FN') Order By Name
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#ResetPerm_Views%') and [TYPE] = 'U') Drop Table #ResetPerm_Views
Create Table #ResetPerm_Views([ID] [int] IDENTITY(1,1),[View] Char(100) NOT NULL)
Insert into #ResetPerm_Views
Select Name From Oasys.Sys.sysobjects Where XType in ('V') Order By Name
If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#ResetPerm_Tables%') and [TYPE] = 'U') Drop Table #ResetPerm_Tables
Create Table #ResetPerm_Tables([ID] [int] IDENTITY(1,1),[Table] Char(100) NOT NULL)
Insert into #ResetPerm_Tables
Select Name From Oasys.Sys.sysobjects Where XType in ('U') Order By Name
Declare @KDN Char(10) = 'Oasys'
Declare @KRS NVarChar(400)
Declare @KRS1 NVarChar(400)
Declare @KRS2 NVarChar(400)
Declare @KRS3 NVarChar(400)
Declare @KRS4 NVarChar(400)
Declare @KRS5 NVarChar(400)
Declare @Store Char(4) = (Select '8' + Stor As Expr1 From RETOPT Where FKEY = '01')


------------------------------------------
-- Process Stored Procedures
------------------------------------------
Print ('Apply Execute Stored Procedure Permission to Store User Role')
Declare @RPP_ID int
Declare @RPP_MAXID int
Select  @RPP_MAXID = MAX(ID)FROM #ResetPerm_Procedures
Set		@RPP_ID = 1 
While	(@RPP_ID <= @RPP_MAXID)
	Begin
		Declare @KPN Char(100) = (Select [Procedure] From #ResetPerm_Procedures Where ID = @RPP_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KPN))) is not null
			Begin
				Print 'SP : ' + LTRIM(RTRIM(@KPN)) + ' - Applying Execute Procedure Permissions'
				Set @KRS = 'Grant Execute On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KPN)) + '] TO [role_execproc] WITH GRANT OPTION'
				Execute(@KRS);
			End
		Else
			Begin
				Print 'SP : ' + LTRIM(RTRIM(@KPN)) + ' - Skipping Execute Procedure Permissions (D.N.E)'
			End  	
	    Set @RPP_ID = @RPP_ID+1 
	End


------------------------------------------
-- Process Functions
------------------------------------------
Print ('Apply Function Permissions')
Declare @RPF_ID int
Declare @RPF_MAXID int
Select  @RPF_MAXID = MAX(ID)FROM #ResetPerm_Functions
Set		@RPF_ID = 1 
While	(@RPF_ID <= @RPF_MAXID)
	Begin
		Declare @KFN Char(100) = (Select [Function] From #ResetPerm_Functions Where ID = @RPF_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KFN))) is not null
			Begin
				Declare @KRS_S Char(1) = 'N'
				Begin TRY
					Set @KRS = 'Grant Execute On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KFN)) + '] TO [role_execproc] WITH GRANT OPTION'
					Execute(@KRS)
					Set @KRS_S = 'E'
				End TRY

				Begin CATCH
					If @@ERROR > 0
						Begin
							Set @KRS = 'Grant Select On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KFN)) + '] TO [role_execproc] WITH GRANT OPTION'
							Execute(@KRS);
							Set @KRS_S = 'S'
						End
				End CATCH;	
				Print 'FN : ' + LTRIM(RTRIM(@KFN)) + ' - Applying Execute Function Permissions (' + @KRS_S + ')'	 			
			End
		Else
			Begin
				Print 'FN : ' + LTRIM(RTRIM(@KFN)) + ' - Skipping Execute Function Permissions (D.N.E)'
			End  	
	    Set @RPF_ID = @RPF_ID+1 
	End
Drop Table #ResetPerm_Functions


------------------------------------------
-- Process Views
------------------------------------------
Print ('Apply View Permissions')
Declare @RPV_ID int
Declare @RPV_MAXID int
Select  @RPV_MAXID = MAX(ID)FROM #ResetPerm_Views
Set		@RPV_ID = 1 
While	(@RPV_ID <= @RPV_MAXID)
	Begin
		Declare @KVN Char(100) = (Select [View] From #ResetPerm_Views Where ID = @RPV_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KVN))) is not null
			Begin
				Print 'VW : ' + LTRIM(RTRIM(@KVN)) + ' - Applying View Access Permissions'
				Set @KRS1 = 'Grant Alter On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS2 = 'Grant Delete On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS3 = 'Grant Insert On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS4 = 'Grant Select On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS5 = 'Grant Update On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KVN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Execute(@KRS1);
				Execute(@KRS2);
				Execute(@KRS3);
				Execute(@KRS4);
				Execute(@KRS5);
			End
		Else
			Begin
				Print 'VW : ' + LTRIM(RTRIM(@KVN)) + ' - Skipping View Access Permissions (D.N.E)'
			End 	
	    Set @RPV_ID = @RPV_ID+1 
	End
Drop Table #ResetPerm_Views


------------------------------------------
-- Process Tables
------------------------------------------
Print ('Apply Table Permissions')
Declare @RPT_ID int
Declare @RPT_MAXID int
Select  @RPT_MAXID = MAX(ID)FROM #ResetPerm_Tables
Set		@RPT_ID = 1 
While	(@RPT_ID <= @RPT_MAXID)
	Begin
		Declare @KTN Char(100) = (Select [Table] From #ResetPerm_Tables Where ID = @RPT_ID)
		If OBJECT_ID(LTRIM(RTRIM(@KTN))) is not null
			Begin
				Print 'TA : ' + LTRIM(RTRIM(@KTN)) + ' - Applying Legacy Access Permissions'
				Set @KRS1 = 'Grant Alter On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS2 = 'Grant Delete On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS3 = 'Grant Insert On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS4 = 'Grant Select On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Set @KRS5 = 'Grant Update On [' + LTRIM(RTRIM(@KDN)) + '].[dbo].[' + LTRIM(RTRIM(@KTN)) + '] TO [role_legacy] WITH GRANT OPTION'
				Execute(@KRS1);
				Execute(@KRS2);
				Execute(@KRS3);
				Execute(@KRS4);
				Execute(@KRS5);
			End
		Else
			Begin
				Print 'TA : ' + LTRIM(RTRIM(@KTN)) + ' - Skipping Legacy Access Permissions (D.N.E)'
			End 
	    Set @RPT_ID = @RPT_ID+1 
	End
Drop Table #ResetPerm_Tables

	
---------------------------------------------------------------------------------------------------
-- Security User Groups
---------------------------------------------------------------------------------------------------
If Exists (Select * From sys.server_principals Where Name = 'TPPLC\' + LTRIM(RTRIM(@sp_User)))
		Begin
			Print ('User TPPLC\' + LTRIM(RTRIM(@sp_User)) + ' exists in this SQL Server, skipping creation..')
		End
Else
		Begin
			Print ('Creating user TPPLC\' + LTRIM(RTRIM(@sp_User)) + '..')
			Declare @SUL_Domain char(5) = 'TPPLC'
    		Declare @SUL_SQL NVARCHAR(4000)
    		Set @SUL_SQL = 'CREATE LOGIN ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'CREATE USER [' + @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) + '] FOR LOGIN ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) + ']'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'sys.sp_addrolemember @membername = ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'], @rolename = role_execproc'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'sys.sp_addrolemember @membername = ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'], @rolename = role_legacy'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'sys.sp_addrolemember @membername = ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'], @rolename = db_backupoperator'
    		Execute(@SUL_SQL)
    		Set @SUL_SQL = 'GRANT ALTER TO ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'] WITH GRANT OPTION'
    		Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT BACKUP DATABASE TO ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT CONNECT TO ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT EXECUTE TO ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT SELECT TO ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
			Set @SUL_SQL = 'GRANT UPDATE TO ['+ @SUL_Domain +'\'+ LTRIM(RTRIM(@sp_User)) +'] WITH GRANT OPTION'
			Execute(@SUL_SQL)
		End

-- Final End
End
GO

