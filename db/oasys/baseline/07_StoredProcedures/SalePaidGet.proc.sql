﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SalePaidGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SalePaidGet'
	EXEC ('CREATE PROCEDURE [dbo].[SalePaidGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SalePaidGet'
GO
ALTER PROCEDURE [dbo].[SalePaidGet]
 @TranDate DATE,
 @TillId CHAR (2), 
 @TranNumber CHAR(4)

AS
BEGIN
	SET NOCOUNT ON;

	select
	
	        dP.DATE1,
			dP.TILL,
			dP.[TRAN],
			dP.NUMB,
			dP.[TYPE],
			dP.AMNT,
			dP.[CARD],
			dP.EXDT,
			dP.COPN,
			dP.CLAS,
			dP.AUTH,
			dP.CKEY,
			dP.SUPV,
			dP.POST,
			dP.CKAC,
			dP.CKSC,
			dP.CKNO,
			dP.DBRF,
			dP.SEQN,
			dP.ISSU,
			dP.ATYP,
			dP.MERC,
			dP.CBAM,
			dP.DIGC,
			dP.ECOM,
			dP.CTYP,
			dP.EFID,
			dP.EFTC,
			dP.STDT,
			dP.AUTHDESC,
			dP.SECCODE,
			dP.TENC,
			dP.MPOW,
			dP.MRAT,
			dP.TENV,
			dP.RTI
					
	from
		DLPAID   dP
	where
	dP.DATE1 = @TranDate
	and 
	dP.[TRAN] = @TranNumber
	and
	dP.TILL  = @TillId 	
END
GO

