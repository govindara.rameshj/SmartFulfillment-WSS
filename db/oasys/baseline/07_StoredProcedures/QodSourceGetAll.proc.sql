﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QodSourceGetAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure QodSourceGetAll'
	EXEC ('CREATE PROCEDURE [dbo].[QodSourceGetAll] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure QodSourceGetAll'
GO
ALTER PROCEDURE [dbo].[QodSourceGetAll]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
           [SOURCE_CODE],
           [SHOW_IN_UI],
           [SEND_UPDATES_TO_OM],
           [ALLOW_REMOTE_CREATE],
           [IS_CLICK_AND_COLLECT]
	FROM [dbo].[QodSource]

	SET NOCOUNT OFF;
END
GO

