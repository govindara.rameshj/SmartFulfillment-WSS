﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemNetworkUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemNetworkUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SystemNetworkUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemNetworkUpdate'
GO
ALTER PROCEDURE [dbo].[SystemNetworkUpdate]
@MasterOpen BIT
AS
BEGIN
	SET NOCOUNT ON;

    update
		SYSNID
	set
		MAST = @MasterOpen
	where
		FKEY='01';
		
	return @@rowcount;
END
GO

