﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateLocalCompetitor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdateLocalCompetitor'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdateLocalCompetitor] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdateLocalCompetitor'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 25-04-2012
-- Description:	Update Local Competitor List
-- =============================================
ALTER PROCEDURE usp_UpdateLocalCompetitor 
	-- Add the parameters for the stored procedure here
	@Id int,
	@Name Char(30), 
	@DeleteFlag bit = 0
AS
BEGIN
	UPDATE [dbo].[CompetitorLocalList]
		SET [Name] = @Name
      ,[DeleteFlag] = @DeleteFlag
      
 WHERE
	[Id]  = @Id
	
 Return @@Rowcount
END
GO

