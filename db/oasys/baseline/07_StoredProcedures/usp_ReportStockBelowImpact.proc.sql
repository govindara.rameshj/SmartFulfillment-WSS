﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ReportStockBelowImpact]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ReportStockBelowImpact'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ReportStockBelowImpact] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ReportStockBelowImpact'
GO
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 10th March 2011
-- 
-- Task     : This procedure is used to retrieve the Store's Items Below Impact Level Report.
-- Notes	: SP retrieves values from STKMAS and calculates SKU's which are below the minimum shelf level
--            (MINI), data is centrally controlled via the JDA system.
-----------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[usp_ReportStockBelowImpact]

AS
begin

	--------------------------------------------------------------------------------
	-- Declare System Date Variable
	--------------------------------------------------------------------------------
	DECLARE		@TODT		DATETIME;

	--------------------------------------------------------------------------------
	-- Set the Date as the System Dates (Live System Date)
	--------------------------------------------------------------------------------
	SET			@TODT = (SELECT SYSDAT.TMDT FROM SYSDAT WHERE SYSDAT.FKEY='01');

	--------------------------------------------------------------------------------
	-- Selecting the returned Information
	--------------------------------------------------------------------------------
	SELECT 
				SKUN	as	'SKU',
				DESCR	as	'Description',
				ONHA	as	'SOH',
				MINI	as	'Impact Level'
	
	FROM
				STKMAS
		
	WHERE 
				IDEL		=	0 
				AND IOBS	=	0
				AND INON	=	0
				AND IRIS	=	0
				AND ICAT	=	0
				AND NOOR	=	0 
				AND IODT	<	@TODT
				AND ONHA	<	MINI;
		
end
GO

