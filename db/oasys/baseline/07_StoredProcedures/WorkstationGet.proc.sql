﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WorkstationGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure WorkstationGet'
	EXEC ('CREATE PROCEDURE [dbo].[WorkstationGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure WorkstationGet'
GO
ALTER PROCEDURE [dbo].[WorkstationGet]
@Id INT=null
AS
begin
	SET NOCOUNT ON;

	select 
		ID					as Id,
		[Description],
		SecurityProfileID	as ProfileId,
		PrimaryFunction,
		DateLastLoggedIn,
		IsActive,
		IsBarcodeBroken,
		UseTouchScreen		as IsTouchScreen
	from
		WorkStationConfig
	where
		(@Id is null) or (@Id is not null and ID = @Id)
	
end
GO

