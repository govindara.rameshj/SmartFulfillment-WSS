﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODOrderDeliveryListing]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure QODOrderDeliveryListing'
	EXEC ('CREATE PROCEDURE [dbo].[QODOrderDeliveryListing] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure QODOrderDeliveryListing'
GO
-- =============================================
-- Author:		Mike O'Cain
-- Create date: 01-10-2009
-- Description:	List Deliveries for the period passed in
-- =============================================
ALTER PROCEDURE [dbo].[QODOrderDeliveryListing] 
	@DateStart datetime = Null, 
	@DateEnd datetime = Null
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT convert(varchar,DELD,103)	as 'Date'
		, DATENAME(dw,DELD)					as 'Day'
		,NUMB								as 'OrderNumber'
		,NAME								as 'CustomerName'
		,POST								as 'PostCode'
		,PHON								as 'PhoneNumber'
		,QTYO								as 'QtyDeliveryItems'
		,WGHT								as 'Weight'
		,VOLU								as 'Volume'
	--Table CORHDR has been replaced with the view vwCORHDRFull
	From vwCORHDRFull	
	WHERE SHOW_IN_UI = 1 and DELI = 1 and DELD >= @DateStart AND DELD <= @DateEnd
	Order by DELD
END
GO

