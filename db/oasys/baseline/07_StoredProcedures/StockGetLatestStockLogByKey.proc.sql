﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetLatestStockLogByKey]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetLatestStockLogByKey'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetLatestStockLogByKey] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetLatestStockLogByKey'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 02/08/2011
-- Description:	Get Latest Stocklog by key
-- =============================================
ALTER PROCEDURE [dbo].[StockGetLatestStockLogByKey] 
	-- Add the parameters for the stored procedure here
	@TKEY int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		st.SKUN		as PartCode,
		st.ONHA		as QuantityOnHand,
		st.RETQ		as UnitsInOpenReturns,
		st.MDNQ		as MarkDownQuantity,
		st.WTFQ		as WriteOffQuantity,
		st.PRIC		as NormalSellPrice,
		st.SALU1	as UnitsSoldYesterday,
		st.SALU2    as UnitsSoldThisWeek, 
		st.SALU4	as UnitsSoldThisPeriod,
		st.SALU6	as UnitsSoldThisYear,
		st.SALV1	as ValueSoldYesterday,
		st.SALV2	as ValueSoldThisWeek,
		st.SALV4	as ValueSoldThisPeriod,
		st.SALV6	as ValueSoldThisYear,
		st.SALT		as SaleTypeAttribute,
		sl.DATE1	as StockLogDate,
		sl.TIME  	as StockLogTime,
		sl.TYPE	    as StockLogType,
		sl.KEYS		as StockLogKeys,
		sl.EEID		as StockLogCashier,
		sl.SSTK		as StockLogStartingStock,
		sl.SRET		as StockLogStartingOpenReturns,
		sl.SPRI		as StockLogStartingPrice,
		sl.ESTK		as StockLogEndingStock,
		sl.ERET		as StockLogEndingOpenReturns,
		sl.EPRI		as StockLogEndingPrice,
		sl.SMDN		as StockLogStartingMarkDown,
		sl.SWTF		as StockLogStartingWriteOff,
		sl.EMDN		as StockLogEndingMarkdown,
		sl.EWTF		as StockLogEndingWriteOff,
		sl.DAYN     as StockLogDayNumber,
		sl.RTI		as StockLogEndingRTI,
		sl.TKEY     as MaxKey
FROM STKLOG sl Inner join STKMAS st on sl.SKUN = st.SKUN
WHERE sl.TKEY = @TKEY

Return @@RowCount
END
GO

