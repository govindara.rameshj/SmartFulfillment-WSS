﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_EnquiryRelatedItem]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_EnquiryRelatedItem'
	EXEC ('CREATE PROCEDURE [dbo].[usp_EnquiryRelatedItem] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_EnquiryRelatedItem'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 31/07/2012
-- User Story	 : 6131
-- Project		 : P022-009 - Stock Enquiry Details - amend the design of
--				 : what is displayed.
-- Task Id		 : 6344
-- Description   : Add stored procedure blank Related Item window which shows when there is no
--				 : Related Item Single or Related Item Bulk settingand does not
--				 : show otherwise.
-- =============================================
ALTER PROCedure [dbo].[usp_EnquiryRelatedItem]
	@SkuNumber	char(6)
As
Begin
	Set NOCOUNT On;

	Declare @Table Table
		(
			RowId Int,
			[Description] VarChar(16)
		)

	If
		(
			(
				Select
					COUNT(*)
				From
					RELITM rl
						Inner join
							STKMAS sk
						On
							sk.SKUN = rl.PPOS
				Where
					rl.SPOS = @SkuNumber
			)
		+
			(
				Select
					COUNT(*)
				From
					RELITM rl
						Inner join
							STKMAS sk
						On
							sk.SKUN = rl.SPOS
				where
					rl.PPOS = @SkuNumber
			)
		) = 0
		Begin
			Insert Into
				@Table
					(
						RowId,
						[Description]
					)
				Values
					(
						1,
						'No Related Items'
					)
		End
	Select * From @Table
End
GO

