﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdatePriceChangeRecordResetLabel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdatePriceChangeRecordResetLabel'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdatePriceChangeRecordResetLabel] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdatePriceChangeRecordResetLabel'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 30/03/2012
-- Description:	Update Existing Price Change Record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdatePriceChangeRecordResetLabel] 
	@Skun Char(6),
	@StartDate DATE,
	@Price decimal(10,2),
	@Status Char(1),
	@EventNumber Char(6),
	@Priority Char(2),
	@IsShelfLabel bit,
	@SmallLabel bit,
	@MediumLabel bit,
	@LargeLabel bit
	
AS
BEGIN
	SET NOCOUNT ON;

Update [Oasys].[dbo].[PRCCHG]
        set 
			[EVNT] = @EventNumber
           ,[PRIO] = @Priority
           ,[PRIC] = @Price
           ,[PSTA] = @Status
           ,[SHEL] = @IsShelfLabel
           ,[LABS] = @SmallLabel
           ,[LABM] = @MediumLabel
           ,[LABL] = @LargeLabel
        Where
            [Skun]= @Skun And 
			[PDAT] = @StartDate
Return @@Rowcount            
End
GO

