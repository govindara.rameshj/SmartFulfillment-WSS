﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemDateGetDates]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemDateGetDates'
	EXEC ('CREATE PROCEDURE [dbo].[SystemDateGetDates] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemDateGetDates'
GO
ALTER PROCEDURE [dbo].[SystemDateGetDates]

AS
BEGIN
	SET NOCOUNT ON;

	select
		FKEY		as Id,
		[DAYS]		as DaysOpen,
		WEND		as WeekEndingDay,
		TODT		as Today,
		TODW		as TodayDayNumber,
		TMDT		as Tomorrow,
		TMDW		as TomorrowDayNumber,
		WKDT		as WeekFromToday,
		PWEK		as WeekEndProcessed,
		PPER		as PeriodEndProcessed,
		TIYR		as CurrentPeriodIsYearEnd,
		TIDT		as CurrentPeriodEnd,
		CYNO		as CycleWeeksTotal,
		CYCW		as CycleWeeksCurrent,
		NITE		as NightlyTask,
		WK131	as WeekEnd1,
		WK132	as WeekEnd2,
		WK133	as WeekEnd3,
		WK134	as WeekEnd4,
		WK135	as WeekEnd5,
		WK136	as WeekEnd6,
		WK137	as WeekEnd7,
		WK138	as WeekEnd8,
		WK139	as WeekEnd9,
		WK1310	as WeekEnd10,
		WK1311	as WeekEnd11,
		WK1312	as WeekEnd12,
		WK1313	as WeekEnd13,
		PSET	as SetInUse,
		
		S1DT1	as Set1PeriodEnd1,
		S1DT2	as Set1PeriodEnd2,
		S1DT3	as Set1PeriodEnd3,
		S1DT4	as Set1PeriodEnd4,
		S1DT5	as Set1PeriodEnd5,
		S1DT6	as Set1PeriodEnd6,
		S1DT7	as Set1PeriodEnd7,
		S1DT8	as Set1PeriodEnd8,
		S1DT9	as Set1PeriodEnd9,
		S1DT10	as Set1PeriodEnd10,
		S1DT11	as Set1PeriodEnd11,
		S1DT12	as Set1PeriodEnd12,
		S1DT13	as Set1PeriodEnd13,
		S1DT14	as Set1PeriodEnd14,
		
		S2DT1	as Set2PeriodEnd1,
		S2DT2	as Set2PeriodEnd2,
		S2DT3	as Set2PeriodEnd3,
		S2DT4	as Set2PeriodEnd4,
		S2DT5	as Set2PeriodEnd5,
		S2DT6	as Set2PeriodEnd6,
		S2DT7	as Set2PeriodEnd7,
		S2DT8	as Set2PeriodEnd8,
		S2DT9	as Set2PeriodEnd9,
		S2DT10	as Set2PeriodEnd10,
		S2DT11	as Set2PeriodEnd11,
		S2DT12	as Set2PeriodEnd12,
		S2DT13	as Set2PeriodEnd13,
		S2DT14	as Set2PeriodEnd14,
		
		S1YR1	as Set1IsYearEnd1,
		S1YR2	as Set1IsYearEnd2,
		S1YR3	as Set1IsYearEnd3,
		S1YR4	as Set1IsYearEnd4,
		S1YR5	as Set1IsYearEnd5,
		S1YR6	as Set1IsYearEnd6,
		S1YR7	as Set1IsYearEnd7,
		S1YR8	as Set1IsYearEnd8,
		S1YR9	as Set1IsYearEnd9,
		S1YR10	as Set1IsYearEnd10,
		S1YR11	as Set1IsYearEnd11,
		S1YR12	as Set1IsYearEnd12,
		S1YR13	as Set1IsYearEnd13,
		S1YR14	as Set1IsYearEnd14,
		
		S2YR1	as Set2IsYearEnd1,
		S2YR2	as Set2IsYearEnd2,
		S2YR3	as Set2IsYearEnd3,
		S2YR4	as Set2IsYearEnd4,
		S2YR5	as Set2IsYearEnd5,
		S2YR6	as Set2IsYearEnd6,
		S2YR7	as Set2IsYearEnd7,
		S2YR8	as Set2IsYearEnd8,
		S2YR9	as Set2IsYearEnd9,
		S2YR10	as Set2IsYearEnd10,
		S2YR11	as Set2IsYearEnd11,
		S2YR12	as Set2IsYearEnd12,
		S2YR13	as Set2IsYearEnd13,
		S2YR14	as Set2IsYearEnd14
		
	from 
		SYSDAT
	where
		FKEY='01'
		
END
GO

