﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_OnOrderProcessTransmissions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_OnOrderProcessTransmissions'
	EXEC ('CREATE PROCEDURE [dbo].[usp_OnOrderProcessTransmissions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_OnOrderProcessTransmissions'
GO
ALTER PROCedure usp_OnOrderProcessTransmissions
as
begin
   set nocount on

   declare @SkuCount int

   set @SkuCount = (select count(*) from STKMAS where isnull(ONOR, 0) > 0)

   if @SkuCount > 0
   begin
   
      select SKUN            as SkuNumber,
             isnull(SUPP, 0) as PrimarySupplierNumber,
             isnull(ONOR, 0) as OnOrderQuantity       
      from STKMAS
      where isnull(ONOR, 0) > 0
   
   end
   else
   begin
   
      select SKUN            as SkuNumber,
             isnull(SUPP, 0) as PrimarySupplierNumber,
             isnull(ONOR, 0) as OnOrderQuantity       
      from STKMAS
      where isnull(ONOR, 0) = 0
   
   end

   select STOR as StoreNumber from RETOPT
   select TODT as RecordDate  from SYSDAT
end
GO

