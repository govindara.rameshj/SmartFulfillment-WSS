﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingActiveCurrencyID]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingActiveCurrencyID'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingActiveCurrencyID] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingActiveCurrencyID'
GO
ALTER PROCedure NewBankingActiveCurrencyID
   @CurrencyID char(3) output
as
begin
set nocount on
select @CurrencyID = ID from SystemCurrency where IsDefault = 1
end
GO

