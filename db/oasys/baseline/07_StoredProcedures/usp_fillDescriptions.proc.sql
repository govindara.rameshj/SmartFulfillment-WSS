﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_fillDescriptions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_fillDescriptions'
	EXEC ('CREATE PROCEDURE [dbo].[usp_fillDescriptions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_fillDescriptions'
GO
-------------------------------------------------------------
ALTER PROCEDURE dbo.usp_fillDescriptions
(
          @Table nvarchar(250) ,
          @Column nvarchar(250) ,
          @Descr nvarchar(250) = '',
          @Schema nvarchar(250) = 'dbo'
)

AS

DECLARE @Select nvarchar (500)    
DECLARE @Update nvarchar (500)
DECLARE @Add nvarchar (500)       
DECLARE @All nvarchar (2000)       

BEGIN 
          if (ltrim(rtrim(@Descr)) = '')
          begin
              set @Descr = @Column       
          end

          set @Select = ' SELECT name FROM ' +
                   ' ::fn_listextendedproperty (default,''schema'',''' + 
                   @Schema + ''', ''table'', ''' + 
                   @Table + ''', ''column'', ''' + 
                   @Column + ''') where name=''MS_DESCRIPTION'' '

          set @Update = 'EXEC sp_updateextendedproperty ''MS_Description'','''+ 
                   @Descr + ''',''schema'','''+ @Schema +''',''table'','''+ 
                   @Table + ''', ''column'',''' +  @Column + ''' '

          set @Add = 'EXEC sp_addextendedproperty ''MS_Description'','''+ 
                   @Descr + ''',''schema'','''+ @Schema +''',''table'','''+ 
                   @Table + ''',''column'',''' + @Column + ''' '

          set @All = ' IF EXISTS (' + @Select + ') ' +
                   ' begin ' +
                   @Update +
                   ' end ' + 
                   ' else ' + 
                   ' begin ' +
                   @Add +
                   ' end '  

          exec sp_executesql @All

END
GO

