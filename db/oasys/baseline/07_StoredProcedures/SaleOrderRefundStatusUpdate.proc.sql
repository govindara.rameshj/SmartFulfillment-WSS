﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderRefundStatusUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderRefundStatusUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderRefundStatusUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderRefundStatusUpdate'
GO
ALTER PROCEDURE [dbo].[SaleOrderRefundStatusUpdate] 
	@OrderNumber Char(6), 
	@LineNumber Char(4),
	@RefundStatus int,
	@SellingStoreIbtOut Char(6) = '0' 
AS
BEGIN
	SET NOCOUNT ON;

	Update CORRefund set RefundStatus=@RefundStatus, SellingStoreIbtOut=@SellingStoreIbtOut Where NUMB=@OrderNumber And Line= @LineNumber
END
GO

