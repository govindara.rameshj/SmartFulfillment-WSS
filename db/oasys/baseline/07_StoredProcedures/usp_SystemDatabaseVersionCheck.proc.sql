﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SystemDatabaseVersionCheck]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SystemDatabaseVersionCheck'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SystemDatabaseVersionCheck] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SystemDatabaseVersionCheck'
GO
ALTER PROCEDURE [dbo].[usp_SystemDatabaseVersionCheck]
AS
BEGIN
	SET NOCOUNT ON;
	
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 2nd November 2011
-- 
-- Task     : Verify's Database Version Installed.
-- Notes	: Reads Parameter 0.
-----------------------------------------------------------------------------------

Declare @TmpVersion char(80)
Declare @Version char(80)

Set @TmpVersion = (Select LTRIM(RTRIM(StringValue)) From [Parameters] Where ParameterID = '0')

IF @TmpVersion is NULL
	Begin
		Set @Version = '0'
	End
ELSE
	Begin
		Set @Version = (Select LTRIM(RTRIM(StringValue)) From [Parameters] Where ParameterID = '0')
	End

Select @Version Output 
		
END
GO

