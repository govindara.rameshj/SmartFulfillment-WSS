﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSecondUserCheck]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSecondUserCheck'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSecondUserCheck] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSecondUserCheck'
GO
ALTER PROCedure NewBankingSecondUserCheck
   @UserID   int,
   @UserName varchar(50) output,
   @Password char(5) output,
   @Manager  bit output
as
begin
set nocount on
select @UserName = Name,
       @Password = [Password],
       @Manager  = IsManager
from SystemUsers
where ID        = @UserID
and   ID       <= (select cast(HCAS as integer) from RETOPT)    --filter out non operational users
and   IsDeleted = 0
end
GO

