﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionLinePersist]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VisionLinePersist'
	EXEC ('CREATE PROCEDURE [dbo].[VisionLinePersist] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VisionLinePersist'
GO
ALTER PROCEDURE [dbo].[VisionLinePersist]
	@TranDate		date,
	@TillId			int,
	@TranNumber		int,
	@Number			int,
	@SkuNumber		char(6),
	@Quantity		int,
	@PriceLookup	dec(9,2),
	@Price			dec(9,2),
	@PriceExVat		dec(9,2),
	@PriceExtended	dec(9,2),
	@IsRelatedItemSingle bit,
	@VatSymbol		char(1),
	@HieCategory	char(6),
	@HieGroup		char(6),
	@HieSubgroup	char(6),
	@HieStyle		char(6),
	@SaleType		char(1),
	@IsInStoreStockItem	bit,
	@MovementTypeCode	char(2),
	@IsPivotal		bit
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @rowcount int;
	set @rowcount=0;	

	--update record if exists
	update
		VisionLine
	set
		SkuNumber		= @SkuNumber,
		Quantity		= @Quantity,
		PriceLookup		= @PriceLookup,
		Price			= @Price,
		PriceExVat		= @PriceExVat,
		PriceExtended	= @PriceExtended,
		IsRelatedItemSingle = @IsRelatedItemSingle,
		VatSymbol		= @VatSymbol,
		HieCategory		= @HieCategory,
		HieGroup		= @HieGroup,
		HieSubgroup		= @HieSubgroup,
		HieStyle		= @HieStyle,
		SaleType		= @SaleType,
		IsInStoreStockItem = @IsInStoreStockItem,
		MovementTypeCode = @MovementTypeCode,
		IsPivotal		= @IsPivotal
	where
		TranDate	= @TranDate and
		TillId		= @TillId and
		TranNumber	= @TranNumber and
		Number		= @Number;

	--get number of rows affected
	set @rowcount = @@ROWCOUNT;
	
	--if nothing updated then insert
	if @rowcount=0
	begin
		insert into 
			VisionLine (
			TranDate,
			TillId,
			TranNumber,
			Number,
			SkuNumber,
			Quantity,
			PriceLookup,
			Price,
			PriceExVat,
			PriceExtended,
			IsRelatedItemSingle,
			VatSymbol,
			HieCategory,
			HieGroup,
			HieSubgroup,
			HieStyle,
			SaleType,
			IsInStoreStockItem,
			MovementTypeCode,
			IsPivotal
		) values (
			@TranDate,
			@TillId,
			@TranNumber,
			@Number,
			@SkuNumber,
			@Quantity,
			@PriceLookup,
			@Price,
			@PriceExVat,
			@PriceExtended,
			@IsRelatedItemSingle,
			@VatSymbol,
			@HieCategory,
			@HieGroup,
			@HieSubgroup,
			@HieStyle,
			@SaleType,
			@IsInStoreStockItem,
			@MovementTypeCode,			
			@IsPivotal
		);
			
		set @rowcount = @@ROWCOUNT;
	end

	return @rowcount;
END
GO

