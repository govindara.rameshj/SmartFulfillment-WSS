﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[IBTInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure IBTInsert'
	EXEC ('CREATE PROCEDURE [dbo].[IBTInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure IBTInsert'
GO
ALTER PROCEDURE [dbo].[IBTInsert] 
    @Number Char(6) out, 
    @Type Char(1) = NULL,
    @Date date = NUll,
    @Info Char(75) = Null,
    @Value decimal(9,2),
    @1Str Char(3) = Null,
    @1IBT int = Null
AS
BEGIN
    SET NOCOUNT ON;

    If LEN(ISNULL(@Number, '')) = 0
    begin
        --get next receipt number from system numbers
        declare @NextNumber int;
        exec    @NextNumber = SystemNumbersGetNext @Id = 4
        
        --convert next number to number
        set @Number = Convert(Char(6), @NextNumber);
    end

    Insert into DRLSUM (NUMB, TYPE, DATE1, INIT, INFO, VALU, [1STR], [1PRT], [1IBT], RTI)
            Values(@Number, @Type, @Date, 'Auto', @Info, @Value, @1Str, 0, @1IBt, 'S')

    SET NOCOUNT OFF;
END
GO

