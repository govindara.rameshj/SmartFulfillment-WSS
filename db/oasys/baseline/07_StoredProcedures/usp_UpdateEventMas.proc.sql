﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateEventMas]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdateEventMas'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdateEventMas] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdateEventMas'
GO
-- ===================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 06/10/2011
-- TFSID        : 2733 
-- Description  : New Stored Procedure for Coupons ProcessTransmissions
-- ====================================================================



-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================

ALTER PROCEDURE usp_UpdateEventMas
 @EventType          AS CHAR(2), 
 @EventKey1          AS CHAR(6), 
 @EventKey2          AS CHAR(8), 
 @EventNumber        AS CHAR(6), 
 @Priority           AS CHAR(2), 
 @Deleted            AS BIT, 
 @TimeOrDayRelated   AS BIT,
 @StartDate          AS DATE = NULL, 
 @EndDate            AS DATE = NULL, 
 @SpecialPrice       AS DECIMAL(9, 2), 
 @BuyQuantity        AS DECIMAL(7, 0), 
 @GetQuantity        AS DECIMAL(7, 0), 
 @DiscountAmount     AS DECIMAL(9, 2), 
 @PercentageDiscount AS DECIMAL(7, 3), 
 @BuyCouponID        AS CHAR(7), 
 @SellCouponID       AS CHAR(7) 
 
AS 
  BEGIN 
      IF EXISTS (SELECT 1 
                 FROM   EVTMAS 
                 WHERE  [TYPE] = @EventType 
                        AND key1 = @EventKey1 
                        AND key2 = @EventKey2 
                        AND numb = @EventNumber) 
        BEGIN 
            UPDATE EVTMAS 
            SET    prio = @Priority, 
                   idel = @Deleted,
                   IDOW = @TimeOrDayRelated, 
                   sdat = @StartDate, 
                   edat = @EndDate, 
                   pric = @SpecialPrice, 
                   bqty = @BuyQuantity, 
                   gqty = @GetQuantity, 
                   vdis = @DiscountAmount, 
                   pdis = @PercentageDiscount, 
                   buycpn = @BuyCouponID, 
                   getcpn = @SellCouponID 
            WHERE  [TYPE] = @EventType 
                   AND key1 = @EventKey1 
                   AND key2 = @EventKey2 
                   AND numb = @EventNumber 
        END 
      ELSE 
        BEGIN 
            INSERT INTO EVTMAS 
                        ([TYPE], 
                         key1, 
                         key2, 
                         numb, 
                         prio, 
                         idel,
                         IDOW, 
                         sdat, 
                         edat, 
                         pric, 
                         bqty, 
                         gqty, 
                         vdis, 
                         pdis, 
                         buycpn, 
                         getcpn) 
            VALUES      ( @EventType, 
                          @EventKey1, 
                          @EventKey2, 
                          @EventNumber, 
                          @Priority, 
                          @Deleted, 
                          @TimeOrDayRelated,
                          @StartDate, 
                          @EndDate, 
                          @SpecialPrice, 
                          @BuyQuantity, 
                          @GetQuantity, 
                          @DiscountAmount, 
                          @PercentageDiscount, 
                          @BuyCouponID, 
                          @SellCouponID ) 
        END 
  END
GO

