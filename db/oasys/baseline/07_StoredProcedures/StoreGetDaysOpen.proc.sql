﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StoreGetDaysOpen]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StoreGetDaysOpen'
	EXEC ('CREATE PROCEDURE [dbo].[StoreGetDaysOpen] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StoreGetDaysOpen'
GO
ALTER PROCEDURE dbo.StoreGetDaysOpen
AS
	SET NOCOUNT ON;
SELECT     DAYS
FROM       SYSDAT
WHERE     (FKEY = '01')
GO

