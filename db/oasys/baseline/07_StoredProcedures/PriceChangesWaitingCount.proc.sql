﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PriceChangesWaitingCount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PriceChangesWaitingCount'
	EXEC ('CREATE PROCEDURE [dbo].[PriceChangesWaitingCount] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PriceChangesWaitingCount'
GO
ALTER PROCEDURE [dbo].[PriceChangesWaitingCount]
	@Quantity INT OUTPUT
AS
begin
	set nocount on;
	
	SELECT 
		@Quantity = COUNT(PRCCHG.PSTA) 
	FROM 
		PRCCHG 
	INNER JOIN 
		STKMAS ON STKMAS.SKUN = PRCCHG.SKUN
	WHERE 
		PRCCHG.PSTA		= 'U'
		AND STKMAS.AAPC	= 0;
				
	SELECT @Quantity = coalesce(@Quantity, 0);

end
GO

