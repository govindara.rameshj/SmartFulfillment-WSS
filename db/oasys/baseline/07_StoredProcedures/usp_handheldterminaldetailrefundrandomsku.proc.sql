﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_handheldterminaldetailrefundrandomsku]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_handheldterminaldetailrefundrandomsku'
	EXEC ('CREATE PROCEDURE [dbo].[usp_handheldterminaldetailrefundrandomsku] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_handheldterminaldetailrefundrandomsku'
GO
-- ======================================================================================= 
-- Author       : Dhanesh Ramachandran 
-- Create date  : 31/08/2011 
-- Referral No  : RF0861 
-- Description  :  
-- ======================================================================================= 
ALTER PROCEDURE [dbo].[usp_handheldterminaldetailrefundrandomsku] 
@SelectedDate DATE, 
@MinPrice DECIMAL, 
@RandPercent INTEGER 
AS 
  SET nocount ON 

  BEGIN 
      SELECT TOP (@RandPercent) PERCENT skun 
      FROM   dlline AS dl 
             INNER JOIN dltots AS dt 
               ON dt.date1 = dl.date1 
                  AND dt.till = dl.till 
                  AND dt.[TRAN] = dl.[TRAN] 
             INNER JOIN dlpaid AS dp 
               ON dp.date1 = dl.date1 
                  AND dp.till = dl.till 
                  AND dp.[TRAN] = dl.[TRAN] 
      WHERE  dl.quan < 0 
             AND dt.tcod = 'RF' 
             AND dt.void = '0' 
             AND dt.void = '0' 
             AND dt.tmod = '0' 
             AND dl.lrev = '0' 
             AND dl.date1 = @SelectedDate 
             AND dl.skun NOT IN (SELECT skun 
                                 FROM   dlline AS dl 
                                        INNER JOIN dltots AS dt 
                                          ON dt.date1 = dl.date1 
                                             AND dt.till = dl.till 
                                             AND dt.[TRAN] = dl.[TRAN] 
                                        INNER JOIN dlpaid AS dp 
                                          ON dp.date1 = dl.date1 
                                             AND dp.till = dl.till 
                                             AND dp.[TRAN] = dl.[TRAN] 
                                 WHERE  dl.quan < 0 
                                        AND dt.tcod = 'RF' 
                                        AND dt.void = '0' 
                                        AND dt.void = '0' 
                                        AND dt.tmod = '0' 
                                        AND dl.lrev = '0' 
                                        AND dl.date1 = @SelectedDate 
                                 GROUP  BY dl.skun, 
                                           dl.[tran] 
                                 HAVING SUM(dl.extp) <= -@MinPrice) 
      GROUP  BY dl.skun, 
                dl.[TRAN] 
      ORDER  BY Newid() 
  END
GO

