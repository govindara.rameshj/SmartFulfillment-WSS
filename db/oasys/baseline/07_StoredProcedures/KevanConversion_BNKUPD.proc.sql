﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_BNKUPD]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_BNKUPD'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_BNKUPD] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_BNKUPD'
GO
ALTER PROCEDURE dbo.KevanConversion_BNKUPD
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.2
-- Author   : Kevan Madelin
-- Date	    : 17th July 2011
-- 
-- Task     : 24 - Converting SAFE BALANCE / DATA to New Banking System
-----------------------------------------------------------------------------------
-- Revision Information
--
-- 1.1 KM : Amended Floats to be calculated from CBSHDR:TFLT not CBSHDR:FLOT
-- 1.2 KM : Added Check to Prevent Easter Sunday Banking SAFE records being created. 
-----------------------------------------------------------------------------------

AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Task     : 24/00 - Find Easter Sunday Trading Date
----------------------------------------------------------------------------------- 
Declare		@c INT,
			@n INT,
			@k INT, 
			@i INT, 
			@j INT, 
			@l INT, 
			@m INT, 
			@d INT, 
			@Easter DATE,
			@Year int
 
Set @Year = YEAR(GETDATE())
	
Set @c = (@Year / 100) 
Set @n = @Year - 19 * (@Year / 19) 
Set @k = (@c - 17) / 25 
Set @i = @c - @c / 4 - ( @c - @k) / 3 + 19 * @n + 15 
Set @i = @i - 30 * ( @i / 30 ) 
Set @i = @i - (@i / 28) * (1 - (@i / 28) * (29 / (@i + 1)) * ((21 - @n) / 11)) 
Set @j = @Year + @Year / 4 + @i + 2 - @c + @c / 4 
Set @j = @j - 7 * (@j / 7) 
Set @l = @i - @j 
Set @m = 3 + (@l + 40) / 44 	Set @d = @l + 28 - 31 * ( @m / 4 ) 
 
Set @Easter = CAST((SELECT CONVERT(CHAR(4),@Year) + '-' + RIGHT('0' + CONVERT(VARCHAR(2),@m),2) + '-' + RIGHT('0' + CONVERT(VARCHAR(2),@d),2) + 'T00:00:00') AS DATE)


-----------------------------------------------------------------------------------
-- Task     : 24/01 - Insert Old SAFE Balance into new SAFE Balance
----------------------------------------------------------------------------------- 
INSERT into SafeDenoms 
(PeriodID, CurrencyID, TenderID, ID, SafeValue, ChangeValue, SystemValue, SuggestedValue) 
values
(
(Select ID from SystemPeriods where StartDate = (Convert (date, GETDATE()))), 
'GBP',
'1',
'0.01',
(Select BANK from CBSHDR where FKEY = '01'),
'0.00',
(Select BANK from CBSHDR where FKEY = '01'),
'0.00'
)

-----------------------------------------------------------------------------------
-- Task     : 24/02 - Update new SAFE Balance with the FLOATS taken off
----------------------------------------------------------------------------------- 
Update SafeDenoms
Set 
SafeValue = SafeValue - (Select TFLT from CBSHDR where FKEY = '01'), 
SystemValue = SystemValue - (Select TFLT from CBSHDR where FKEY = '01')
Where ID = '0.01' and PeriodID = (Select ID from SystemPeriods where StartDate = (Convert (date, GETDATE())))

-----------------------------------------------------------------------------------
-- Task     : 24/03 - Insert FLOATS into £100 in new SAFE Balance
----------------------------------------------------------------------------------- 
INSERT into SafeDenoms 
(PeriodID, CurrencyID, TenderID, ID, SafeValue, ChangeValue, SystemValue, SuggestedValue) 
values
(
(Select ID from SystemPeriods where StartDate = (Convert (date, GETDATE()))), 
'GBP',
'1',
'100.00',
(Select TFLT from CBSHDR where FKEY = '01'),
'0.00',
(Select TFLT from CBSHDR where FKEY = '01'),
'0.00'
)

-----------------------------------------------------------------------------------
-- Task     : 24/04 - Remove SAFE Records from Base Schema
----------------------------------------------------------------------------------- 
Delete From [Safe]

-----------------------------------------------------------------------------------
-- Task     : 24/05 - Insert SAFE Record for CONVERSION DAY
----------------------------------------------------------------------------------- 
INSERT into [Safe]
(PeriodID, PeriodDate, UserID1, UserID2, LastAmended, IsClosed) 
values
(
(Select ID from SystemPeriods where StartDate = (Convert (date, GETDATE()))), 
(Select StartDate from SystemPeriods where StartDate = (Convert (date, GETDATE()))),
'0',
'0',
(Select StartDate from SystemPeriods where StartDate = (Convert (date, GETDATE()))),
'0'
)

-----------------------------------------------------------------------------------
-- Task     : 24/05 - Insert SAFE Records where Banking NOT COMPLETED on Old System
-----------------------------------------------------------------------------------
INSERT INTO [Safe] 
        ([PeriodID],[PeriodDate],[UserID1] ,[UserID2],[LastAmended] ,[IsClosed])
SELECT 
	(Select ID from SystemPeriods where StartDate = cc.DATE1),
        (Select StartDate from SystemPeriods where StartDate = cc.DATE1),
	'0', 
	'0', 
	(Select StartDate from SystemPeriods where StartDate = cc.DATE1), 
	'0' 
	
FROM CBSCTL as cc
	
where 
   (cc.COMM = '0') and cc.DATE1 not in (@Easter)

END;
GO

