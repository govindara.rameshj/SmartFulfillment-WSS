﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemOptionGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemOptionGet'
	EXEC ('CREATE PROCEDURE [dbo].[SystemOptionGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemOptionGet'
GO
ALTER PROCEDURE [dbo].[SystemOptionGet]

AS
begin
	SET NOCOUNT ON;

	select 
		so.STOR		as StoreNumber,
		so.SNAM		as StoreName,
		so.MAST		as MasterWorkstationId,
		ro.[TYPE]	as AccountabilityType,
		ro.DOLR		as DateLastReformat
	from
		SYSOPT so
	inner join
		RETOPT ro on so.FKEY=ro.FKEY
	
end
GO

