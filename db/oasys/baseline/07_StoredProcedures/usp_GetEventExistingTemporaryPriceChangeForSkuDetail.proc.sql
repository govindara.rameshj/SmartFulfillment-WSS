﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEventExistingTemporaryPriceChangeForSkuDetail]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEventExistingTemporaryPriceChangeForSkuDetail'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEventExistingTemporaryPriceChangeForSkuDetail] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEventExistingTemporaryPriceChangeForSkuDetail'
GO
-- ===============================================================================
-- Author         : Partha Dutta
-- Date	          : 01/12/2011
-- Project        : PO14-03
-- TFS User Story : 2154
-- TFS Task ID    : 
-- Description    : RollOut
-- ===============================================================================


-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================

ALTER PROCedure usp_GetEventExistingTemporaryPriceChangeForSkuDetail

   @EventTypeID char(2),
   @SkuNumber   char(6),
   @StartDate   date,
   @EndDate     date,
   @Deleted     bit

as
begin
   set nocount on

   select b.[TYPE], b.KEY1, b.NUMB, b.PRIO, b.IDEL, b.IDOW, b.SDAT, b.EDAT, b.PRIC       
   from EVTHDR a
   inner join EVTMAS b
         on  b.NUMB = a.NUMB
         and b.PRIO = a.PRIO
         and b.IDEL = a.IDEL
   where b.[TYPE] = @EventTypeID
   and   b.KEY1   = @SkuNumber
   and   b.SDAT  <= @StartDate
   and   b.EDAT  >= @EndDate
   and   b.IDEL   = @Deleted

   order by a.NUMB, a.PRIO

end
GO

