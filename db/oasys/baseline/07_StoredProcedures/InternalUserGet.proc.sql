﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InternalUserGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure InternalUserGet'
	EXEC ('CREATE PROCEDURE [dbo].[InternalUserGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure InternalUserGet'
GO
ALTER PROCEDURE [dbo].[InternalUserGet]
   @Id INT = null,
   @OnlyBankingIds bit = 0
AS
Begin
	SET NOCOUNT ON;
	
	Declare 
		@Idnotshow varchar(max),
		@sqltext nvarchar(max),
		@paramDef nvarchar(100) = '@paramId INT, @paramOnlyBankingIds bit'
		
	Set @Idnotshow = (select [StringValue] from [dbo].[Parameters] where [ParameterID] = 985)
	
	Set @sqltext = N'
		select 
			ID						as Id,
			EmployeeCode			as Code,
			SecurityProfileID		as ProfileId,
			Name,
			Initials,
			Position,
			PayrollID				as PayrollId,
			Password,
			PasswordExpires,
			SupervisorPassword		as SuperPassword,
			SupervisorPwdExpires	as SuperPasswordExpires,
			Outlet,
			IsManager,
			IsSupervisor,
			IsDeleted,
			DeletedDate,
			DeletedBy,
			DeletedWhere,
			TillReceiptName,
			LanguageCode
		from
			SystemUsers
		where ((@paramId is null) or (@paramId is not null and ID = @paramId)) and '  

	If len(RTRIM(@Idnotshow)) > 0
		Set @sqltext = @sqltext + '((ID < 500 and ID not in (' + @Idnotshow + ')) or (ID = 555 and @paramOnlyBankingIds = 1))'
	Else
		Set @sqltext = @sqltext + '(ID < 500 or (ID = 555 and @paramOnlyBankingIds = 1))'
	
	Exec sp_executesql @sqltext, @paramDef, @paramId = @Id, @paramOnlyBankingIds = @OnlyBankingIds

End
GO

