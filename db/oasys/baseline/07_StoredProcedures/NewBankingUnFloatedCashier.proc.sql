﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingUnFloatedCashier]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingUnFloatedCashier'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingUnFloatedCashier] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingUnFloatedCashier'
GO
ALTER PROCedure [dbo].[NewBankingUnFloatedCashier]
   @PeriodID int,
   @ChoiceOfCashier int = 2
as
begin
set nocount on

select UserID = su.ID,
       Employee = su.EmployeeCode + ' - ' + su.Name
from dbo.SystemUsers su 
where su.IsDeleted = 0                                                              --remove delete cashiers
and ((@ChoiceOfCashier = 0 and su.ID <= (select cast(HCAS as integer) from RETOPT)) --filter out non operational users
    or (@ChoiceOfCashier = 1 and su.ID in (select Mapping_CashierID from dbo.OVC_Mapping_Cashiers)) --filter add virtual cashier    
    or (@ChoiceOfCashier = 2 and (su.ID <= (select cast(HCAS as integer) from RETOPT) or su.ID in (select Mapping_CashierID from dbo.OVC_Mapping_Cashiers))))
and   su.ID not in (select AccountabilityID                                         --remove cashiers already assigned floats
                 from SafeBags
                 where [Type]      = 'F'
                 and   [State]     = 'R'
                 and   OutPeriodID = @PeriodID)
end
GO

