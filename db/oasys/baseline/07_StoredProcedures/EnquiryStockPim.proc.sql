﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryStockPim]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryStockPim'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryStockPim] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryStockPim'
GO
ALTER PROCEDURE [dbo].[EnquiryStockPim]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	--get images path
	declare @imagePath varchar(50);
	set		@imagePath = (select stringValue from Parameters where ParameterID=915);
	set		@imagePath = @imagePath + '\' + @SkuNumber +'.jpg';	
	
	select @imagePath as 'ImagePath';

END
GO

