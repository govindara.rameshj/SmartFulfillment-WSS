﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEnableStates]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GetEnableStates'
	EXEC ('CREATE PROCEDURE [dbo].[GetEnableStates] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GetEnableStates'
GO
ALTER PROCEDURE [dbo].[GetEnableStates]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		[State],
		[Description],
		[DeliveryStatus]
	FROM [dbo].[EnableState]

	SET NOCOUNT OFF;
END
GO

