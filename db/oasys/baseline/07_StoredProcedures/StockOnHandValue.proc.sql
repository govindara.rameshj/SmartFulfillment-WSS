﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockOnHandValue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockOnHandValue'
	EXEC ('CREATE PROCEDURE [dbo].[StockOnHandValue] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockOnHandValue'
GO
ALTER PROCEDURE [StockOnHandValue]
@Value DECIMAL (9, 2) OUTPUT
AS
SELECT @Value =  SUM(S.ONHA*S.PRIC) FROM STKMAS S 
	WHERE	S.ONHA > 0
	AND		S.IOBS = 0
	AND		S.INON = 0;

	IF @Value IS NULL SELECT @Value = 0;
GO

