﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ISUHDR_ISULIN]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ISUHDR_ISULIN'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ISUHDR_ISULIN] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ISUHDR_ISULIN'
GO
-- =============================================
-- Author       : Partha Dutta
-- Create date  : 06/06/2011
-- Referral No  : 563
-- Description  : Baseline version
--                Original developer for this script is Michael O Cain
--                I have created the rollout script only
-- =============================================

ALTER PROC [dbo].[usp_ISUHDR_ISULIN]
AS
BEGIN
CREATE TABLE #ISUHDRTemp
(
     RID_1  [INT] IDENTITY(1,1),
    [NUMB] [char](6) NOT NULL,
	[IDAT] [date] NULL,
	[SUPP] [char](5) NULL,
	[SPON] [char](6) NULL,
	[SOQN] [char](6) NULL,
	[BBCC] [char](1) NULL,
	[VALU] [decimal](9, 2) NULL,
	[PNUM] [char](6) NULL,
	[IMPI] [bit] NOT NULL,
	[RECI] [bit] NOT NULL,
	[DELI] [bit] NOT NULL,
	[DRLN] [char](6) NULL,
	[REJI] [char](1) NULL
)

CREATE TABLE #ISULINTemp
(
    RID [INT] IDENTITY(1,1),
    [NUMB] [char](6) NOT NULL,
	[LINE] [char](4) NOT NULL,
	[SKUN] [char](6) NULL,
	[QTYO] [int] NULL,
	[QTYI] [int] NULL,
	[QTYF] [int] NULL,
	[SRTF] [bit] NOT NULL,
	[ADDM] [bit] NOT NULL,
	[REJI] [char](1) NULL
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ISUHDRHistory') 
	SELECT * FROM ISUHDRHistory
ELSE
BEGIN 
CREATE TABLE ISUHDRHistory
(
     RID_1  [INT] IDENTITY(1,1),
    [DATEADDED] [date] Not Null,
    [NUMB] [char](6) NOT NULL,
	[IDAT] [date] NULL,
	[SUPP] [char](5) NULL,
	[SPON] [char](6) NULL,
	[SOQN] [char](6) NULL,
	[BBCC] [char](1) NULL,
	[VALU] [decimal](9, 2) NULL,
	[PNUM] [char](6) NULL,
	[IMPI] [bit] NOT NULL,
	[RECI] [bit] NOT NULL,
	[DELI] [bit] NOT NULL,
	[DRLN] [char](6) NULL,
	[REJI] [char](1) NULL
)
END 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ISULineHistory')
	SELECT * FROM ISULINEHistory
ELSE
BEGIN 
CREATE TABLE ISULineHistory
(
    RID [INT] IDENTITY(1,1),
    [DATEADDED] [date] Not Null,
    [NUMB] [char](6) NOT NULL,
	[LINE] [char](4) NOT NULL,
	[SKUN] [char](6) NULL,
	[QTYO] [int] NULL,
	[QTYI] [int] NULL,
	[QTYF] [int] NULL,
	[SRTF] [bit] NOT NULL,
	[ADDM] [bit] NOT NULL,
	[REJI] [char](1) NULL
)
END 


INSERT INTO #ISUHDRTemp ([NUMB],[IDAT],[SUPP],[SPON],[SOQN],[BBCC],[VALU],[PNUM],[IMPI],[RECI],[DELI],[DRLN],[REJI])
SELECT [NUMB],[IDAT],[SUPP],[SPON],[SOQN],[BBCC],[VALU],[PNUM],[IMPI],[RECI],[DELI],[DRLN],[REJI] FROM ISUHDR WHERE IDAT <= DATEADD(Day,-14,getdate()) AND RECI = 1

INSERT INTO #ISULINTemp ([NUMB],[LINE],[SKUN],[QTYO],[QTYI],[QTYF],[SRTF],[ADDM],[REJI])
SELECT LN.[NUMB],[LINE],[SKUN],[QTYO],[QTYI],[QTYF],[SRTF],[ADDM],LN.[REJI] FROM ISULIN LN JOIN ISUHDR HD ON HD.NUMB = LN.NUMB
WHERE HD.IDAT <= DATEADD(Day,-14,getdate()) AND HD.RECI = 1

    DECLARE @RID INT
	DECLARE @MAXID INT
	SELECT @MAXID = MAX(RID)FROM #ISULINTemp 
	SET @RID = 1
	DECLARE @NUMB1 CHAR(10)
	DECLARE @LINE CHAR(10)   

WHILE(@RID<=@MAXID)
		BEGIN
				         
		SELECT @NUMB1 = NUMB,@LINE = LINE FROM #ISULINTemp WHERE RID = @RID
				
		IF EXISTS(SELECT 1 FROM ISULIN WHERE NUMB= @NUMB1 AND LINE = @LINE)
	        BEGIN	        
	         INSERT INTO ISULINEHistory (DATEADDED,NUMB,LINE,SKUN,QTYO,QTYI,QTYF,SRTF,ADDM,REJI)
	         SELECT GETDATE(),LN.NUMB,LN.LINE,LN.SKUN,LN.QTYO,LN.QTYI,LN.QTYF,LN.SRTF,LN.ADDM,LN.REJI
	         FROM #ISULINTemp LN WHERE LN.RID  = @RID  
	                 
	         DELETE FROM ISULIN WHERE NUMB= @NUMB1 AND LINE = @LINE 	        
	        END	        
	        SET @RID = @RID+1 
	     END


    DECLARE @RID_1 INT
	DECLARE @MAXID_1 INT
	SELECT @MAXID_1 = MAX(RID_1)FROM #ISUHDRTemp 
	SET @RID_1 = 1
	DECLARE @NUMB CHAR(10)

WHILE(@RID_1<=@MAXID_1)
		BEGIN
			SELECT @NUMB = NUMB FROM #ISUHDRTemp WHERE RID_1 = @RID_1
				
		IF EXISTS(SELECT 1 FROM ISUHDR WHERE NUMB= @NUMB)
	        BEGIN	       
	         INSERT INTO ISUHDRHistory (DATEADDED,NUMB,IDAT,SUPP,SPON,SOQN,BBCC,VALU,PNUM,IMPI,RECI,DELI,DRLN,REJI)
	         SELECT GETDATE(),HD.NUMB,HD.IDAT,HD.SUPP,HD.SPON,HD.SOQN,HD.BBCC,HD.VALU, 
	         HD.PNUM,HD.IMPI,HD.RECI,HD.DELI,HD.DRLN,HD.REJI FROM #ISUHDRTemp HD WHERE HD.RID_1 = @RID_1   
	       	
	         DELETE FROM ISUHDR WHERE NUMB= @NUMB 
	         END
	       SET @RID_1 = @RID_1+1 
	     END
END
GO

