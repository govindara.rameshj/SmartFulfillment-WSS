﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SaleCouponIsExclusive]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SaleCouponIsExclusive'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SaleCouponIsExclusive] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SaleCouponIsExclusive'
GO
ALTER PROCedure [dbo].[usp_SaleCouponIsExclusive]
	@CouponId CHAR (7)
	
As
Begin
	Set NOCOUNT ON;

Select 
	cm.[EXCCOUPON]
From
	[COUPONMASTER] cm
Where
	cm.[COUPONID]= @CouponId
End
GO

