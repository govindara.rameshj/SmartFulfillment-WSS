﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SetTemporaryPriceChanges]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SetTemporaryPriceChanges'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SetTemporaryPriceChanges] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SetTemporaryPriceChanges'
GO
ALTER PROCedure usp_SetTemporaryPriceChanges

   @EventTypeID          int,
   @Description          nvarchar(1000),
   @StartDate            date,
   @EndDate              date,
   @SkuNumbers           varchar(max),
   @Quantities           varchar(max),
   @Price                decimal(9, 2),
   @CashierSecurityID    int,
   @AuthorisorSecurityID int,
   @EventHeaderID        int output,
   @Success              bit output

as
begin
   set nocount on

   begin try

      begin transaction

      insert EventHeaderOverride(EventTypeID, [Description], StartDate, EndDate, Revision, CashierSecurityID, AuthorisorSecurityID, Price)
                          values(@EventTypeID, @Description, @StartDate, @EndDate,
                                 dbo.udf_GetEventHeaderOverrideNextRevision(@EventTypeID, @StartDate, @EndDate, @SkuNumbers, @Quantities),
                                 @CashierSecurityID, @AuthorisorSecurityID, @Price)

      set @EventHeaderID = @@IDENTITY

    --insert EventDetailOverride(EventHeaderOverrideID, SkuNumber, Quantity) select @EventHeaderID, SkuNumber, Quantity
    --                                                                       from dbo.udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable(@SkuNumbers, @Quantities)
      insert EventDetailOverride(EventHeaderOverrideID,
                                 SkuNumber,
                                 Quantity,
                                 ErosionPercentage,
                                 SkuNormalSellingPrice,
                                 SkuActualSellingPrice)

                          select @EventHeaderID,
                                 SkuNumber,
                                 Quantity,
                                 null,
                                 case @EventTypeID
                                    when 3 then dbo.udf_SkuNormalSellingPrice(SkuNumber)
                                    else null
                                 end,
                                 case @EventTypeID
                                    when 3 then dbo.udf_SkuIncorrectDayPrice(SkuNumber, @StartDate, @EndDate)
                                    else null
                                 end
                          from dbo.udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable(@SkuNumbers, @Quantities)

      commit transaction
      set @Success = 1

   end try
   begin catch

      rollback transaction
      set @EventHeaderID = 0
      set @Success       = 0

   end catch

end
GO

