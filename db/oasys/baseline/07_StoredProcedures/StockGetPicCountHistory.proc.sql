﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetPicCountHistory]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetPicCountHistory'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetPicCountHistory] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetPicCountHistory'
GO
ALTER PROCedure [dbo].[StockGetPicCountHistory]
AS
BEGIN
	SELECT
		hh.DATE1 AS 'Date',
		hh.NUMB AS 'Total Count Items',
		hh.DONE AS'Physicaly Counted Items',
		hh.AUTH AS 'Authorised by',
		hh.IADJ AS 'IsAdjusted'
	FROM dbo.HHTHDR hh
	WHERE (hh.DATE1 >= DATEADD(MONTH, -3, CONVERT (DATETIME, GETDATE())))
	ORDER BY DATE1 DESC
END
GO

