﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionTotalPersistFlash]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VisionTotalPersistFlash'
	EXEC ('CREATE PROCEDURE [dbo].[VisionTotalPersistFlash] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VisionTotalPersistFlash'
GO
ALTER PROCEDURE [dbo].[VisionTotalPersistFlash]
@TranDate DATE, @TillId INT, @TranNumber INT, @CashierId INT, @TranDateTime CHAR (8), @Type CHAR (2)=null, @ReasonCode CHAR (2)=null, @ReasonDescription VARCHAR (50)=null, @OrderNumber CHAR (6)=null, @Value DECIMAL (9, 2), @ValueMerchandising DECIMAL (9, 2), @ValueNonMerchandising DECIMAL (9, 2), @ValueTax DECIMAL (9, 2), @ValueDiscount DECIMAL (9, 2), @ValueColleagueDiscount DECIMAL (9, 2), @IsColleagueDiscount BIT, @IsPivotal BIT
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @rowcount int;
	exec @rowcount=VisionTotalPersist 
						@TranDate,
						@TillId,
						@TranNumber,
						@CashierId,
						@TranDateTime,
						@Type,
						@ReasonCode,
						@ReasonDescription,
						@OrderNumber,
						@Value,
						@ValueMerchandising,
						@ValueNonMerchandising,
						@ValueTax,
						@ValueDiscount,
						@ValueColleagueDiscount,
						@IsColleagueDiscount,
						@IsPivotal

	if @rowcount>0
	begin
		exec FlashTotalSaleTypePersist @Type, @Value;
	end

	return @rowcount;
END
GO

