﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_RemoteStockCheckADV]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_RemoteStockCheckADV'
	EXEC ('CREATE PROCEDURE [dbo].[usp_RemoteStockCheckADV] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_RemoteStockCheckADV'
GO
ALTER PROCEDURE [dbo].[usp_RemoteStockCheckADV] 
	-- Declare Parameters
	@SKUN char(6) 
AS
BEGIN
	SET NOCOUNT ON;
	-- ======================================================================
	-- Author		: Kevan Madelin
	-- Create date	: 23rd January 2012
	-- Description	: Remote Access Stock Check Advanced Version
	-- ======================================================================

	-----------------------------------------------------------------------------------------------------------------------
	-- Declare the Variables
	-----------------------------------------------------------------------------------------------------------------------
	Declare @Date					date	
	
	Declare @SKU					char(6),
			@Description			char(60),
			@OnHand					int,
			@MarkdownQty			int,
			@OnOrder				int,
			@SellingUnit			char(4),
			@Price					numeric(9,2),
			@PriceEQ				decimal(11,2),
			@PriceDivider			decimal(11,6),
			@Weight					decimal(7,2),
			@Volume					decimal(7,2),
			@Category				char(36),
			@Group					char(36),
			@SubGroup				char(36),
			@Style					char(36),
			@EventNumber			char(6),
			@EventPriority			char(2),
			--EQPM as 'EquivalentMultiplier',
			--EQPU as 'EquivalentUnit',
			@SaleAttribute			char(1),
			@ItemObsolete			char(3),
			@ItemNonStocked			char(3),
			@ItemNonOrderable		char(3),
			@ItemDeleted			char(3),
			@ImpactLevel			int,
			@Capacity				int,
			@FSCContent				char(3),
			@ElectricalItem			char(3),
			@QuarantinedItem		char(3),
			@SolventItem			char(3),
			@OffensiveItem			char(3),
			@WarrantyItem			char(3),
			@StockOffSaleItem		char(3),
			@InsulationItem			char(3),
			@PalletItem				char(3),
			@Offer					char(200),
			@EANNumber				char(16),	
			@DateLastReceived		date,
			@LastReceivedQuantity	int,
			@DateNextDelivery		date,
			@NextDeliveryQuantity	int,
			@Supplier				char(5),
			@SupplierAlternate		char(5),
			@OrderDays				char(40)
	
	Set @Date = GETDATE()
	Set @Price = (Select PRIC From STKMAS Where SKUN = @SKUN)
	Set @PriceDivider = (Select EQPM From STKMAS Where SKUN = @SKUN)
	
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Select Details from Stock Master Table for Query
	-----------------------------------------------------------------------------------------------------------------------
	SELECT	@SKU						=	SKUN, 
			@Description				=	DESCR,
			@OnHand						=	ONHA,
			@MarkdownQty				=	MDNQ,
			@OnOrder					=	ONOR,
			@SellingUnit				=	BUYU,
			@Price						=	PRIC,
			@Weight						=	WGHT,
			@Volume						=	VOLU,
			@Category					=	CTGY,
			@Group						=	GRUP,
			@SubGroup					=	SGRP,
			@Style						=	STYL,
			@EventNumber				=	REVT,
			@EventPriority				=	RPRI,
			--@EquivalentMultiplier			=	EQPM,
			--@EquivalentUnit				=	EQPU,
			@SaleAttribute				=	SALT,
			@ItemObsolete				=	case 
												when IOBS = 0 Then 'No'
												when IOBS = 1 Then 'Yes'
											end,
			@ItemNonStocked				=	case 
												when INON = 0 Then 'No' 
												when INON = 1 Then 'Yes'
											end,
			@ItemNonOrderable			=	case 
												when NOOR = 0 Then 'No' 
												when NOOR = 1 Then 'Yes'
											end,
			@ItemDeleted				=	case 
												when IDEL = 0 Then 'No' 
												when IDEL = 1 Then 'Yes'
											end,
			@ImpactLevel				=	MINI,
			@Capacity					=	MAXI,
			@FSCContent					=	case 
												when TIMB = 0 Then 'No' 
												when TIMB <> 0 Then 'Yes'
											end,
			@ElectricalItem				=	case 
												when ELEC = 0 Then 'No' 
												when ELEC = 1 Then 'Yes'
											end,
			@QuarantinedItem			=	case 
												when QUAR = '' Then 'No' 
												when QUAR <> '' Then 'Yes'
											end,
			@SolventItem				=	case 
												when ISOL = 0 Then 'No' 
												when ISOL <> 0 Then 'Yes'
											end,
			@OffensiveItem				=	case 
												when IOFF = 0 Then 'No' 
												when IOFF <> 0 Then 'Yes'
											end,
			@WarrantyItem				=	case 
												when IWAR = 0 Then 'No' 
												when IWAR = 1 Then 'Yes'
											end,
			@StockOffSaleItem			=	case 
												when SOFS = 0 Then 'No' 
												when SOFS = 1 Then 'Yes'
											end,
			@InsulationItem				=	case 
												when FRAG = 0 Then 'No' 
												when FRAG = 1 Then 'Yes'
											end,
			@PalletItem					=	case 
												when IPSK = 0 Then 'No' 
												when IPSK = 1 Then 'Yes'
											end		
	From	STKMAS
	Where	SKUN = @SKUN
	
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Return the Quantity Last Received
	-----------------------------------------------------------------------------------------------------------------------
	Select TOP 1
	@DateLastReceived = DATE1,
	@LastReceivedQuantity = RECQ
	From DRLDET as dd
	Inner Join DRLSUM as ds on ds.NUMB = dd.NUMB
	Where SKUN = @SKUN and DATE1 <= @Date and ds.TYPE = '0'
	Order by ds.DATE1, dd.NUMB desc
	
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Return the Current Event Information (Offer)
	-----------------------------------------------------------------------------------------------------------------------
	Select TOP 1
		@Offer = case em.TYPE
				when 'QS' then ('Buy '+ CAST(CAST(SUBSTRING(em.KEY2,1,8)as int) as varchar(8)) +' or more units and pay £'+ RTRIM(CAST(em.PRIC as CHAR)) + ' EACH - Valid : ' + RTRIM(CAST(em.SDAT as CHAR)) + ' to ' + RTRIM(CAST(em.EDAT as CHAR)))
				when 'MS' then ('Buy '+ RTRIM(CAST(em.BQTY as CHAR)) +' Get '+ RTRIM(CAST(em.GQTY as CHAR)) + ' FREE - Valid : ' + RTRIM(CAST(em.SDAT as CHAR)) + ' to ' + RTRIM(CAST(em.EDAT as CHAR)))
				when 'HS' then ('Save '+ RTRIM(Left(CAST(em.PDIS as CHAR),4))+'% when you spend £'+ CAST(CAST(SUBSTRING(em.KEY2,1,6)as int) as varchar(6)) +'.'+ RIGHT(em.KEY2,2) +' on '+(Select RTRIM(ALPH) From HIESGP where SGRP = em.KEY1) +' - Valid : ' + RTRIM(CAST(em.SDAT as CHAR)) + ' to ' + RTRIM(CAST(em.EDAT as CHAR)))
				when 'DG' then ('Deal Group available starting from £'+ RTRIM(CAST(em.PRIC as CHAR)) +' on Deal : '+ ed.DLGN + ' ' + (Select RTRIM(DESCR) From EVTHDR where NUMB = ed.NUMB) +' - Valid : ' + RTRIM(CAST(em.SDAT as CHAR)) + ' to ' + RTRIM(CAST(em.EDAT as CHAR)))
				when 'TS' then ('Time Limited Promotion (TLP) Price at Checkout : £'+ RTRIM(CAST(em.PRIC as CHAR)) +' - Valid : ' + RTRIM(CAST(em.SDAT as CHAR)) + ' to ' + RTRIM(CAST(em.EDAT as CHAR)))
		end
	From EVTMAS AS em
	Full Outer Join EVTDLG as ed on ed.DLGN = RIGHT(em.KEY2,6) and ed.KEY1 = @SKUN
	Full Outer Join EVTHEX as ex on ex.HIER = em.KEY1 and ex.ITEM = @SKUN
	Where	(em.TYPE = 'TS' and em.KEY1 = @SKUN and em.IDEL = '0' and (em.SDAT <= @Date and em.EDAT >= @Date))
		 or (em.TYPE = 'QS' and em.KEY1 = @SKUN and em.IDEL = '0' and (em.SDAT <= @Date and em.EDAT >= @Date))
		 or (em.TYPE = 'MS' and em.KEY1 = @SKUN and em.IDEL = '0' and (em.SDAT <= @Date and em.EDAT >= @Date)) 
		 or (em.TYPE = 'DG' and ed.KEY1 = @SKUN and em.IDEL = '0' and (em.SDAT <= @Date and em.EDAT >= @Date)) 
		 or (em.TYPE = 'HS' and ex.ITEM = @SKUN and em.IDEL = '0' and (em.SDAT <= @Date and em.EDAT >= @Date))
	Order by em.PRIO desc, em.NUMB desc, em.PRIC asc
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Select EAN
	-----------------------------------------------------------------------------------------------------------------------
	Select	TOP 1 
			@EANNumber = NUMB
	From	EANMAS
	Where	SKUN = @SKUN
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Select Next Delivery Information
	-----------------------------------------------------------------------------------------------------------------------
	Select TOP 1
		@DateNextDelivery = ph.DDAT,
		@NextDeliveryQuantity = ph.QTYO
	From PURLIN as pl inner join PURHDR as ph on ph.TKEY = pl.HKEY  
	where pl.DELE = 0 
	and pl.SKUN = '220080'
	and ph.RCOM = '0' 
	and (pl.CONF = 'C' or pl.CONF = ' ' or pl.CONF = 'A' or pl.CONF is NULL)
	Order by ph.DDAT asc	
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Select Equivalent Price Data
	-----------------------------------------------------------------------------------------------------------------------
	if @PriceDivider = 0
		Begin
			Set @PriceEQ = 0
		End
	else
		Begin
			Set @PriceEQ = (@Price / @PriceDivider)
		End
	
	Declare @PE char(50)
	Set @PE = ('Price Equivalent £' + LTRIM(RTRIM(CAST(@PriceEQ as CHAR))) + ' per ' + (Select LTRIM(RTRIM(EQPU)) From STKMAS Where SKUN = @SKUN) + '.') 
	
	-- Must be in order for Application to work
	
	Select	@OrderDays = '', @Supplier = '', @SupplierAlternate = ''
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Do Select
	-----------------------------------------------------------------------------------------------------------------------
	SELECT	@SKU					as 'SKU', 
			@Description			as 'Description',
			@OnHand					as 'OnHand',
			@MarkdownQty			as 'MarkdownQty',
			@OnOrder				as 'OnOrder',
			@Price					as 'Price',
			@SellingUnit			as 'SellingUnit',
			@Weight					as 'Weight',
			@Volume					as 'Volume',
			@Category				as 'Category',
			@Group					as 'Group',
			@SubGroup				as 'SubGroup',
			@Style					as 'Style',
			@EventNumber			as 'EventNumber',
			@EventPriority			as 'EventPriority',
			@SaleAttribute			as 'SaleAttribute',
			@ItemObsolete			as 'ItemObsolete',
			@ItemNonStocked			as 'ItemNonStocked',
			@ItemNonOrderable		as 'ItemNonOrderable',
			@ItemDeleted			as 'ItemDeleted',
			@ImpactLevel			as 'ImpactLevel',
			@Capacity				as 'Capacity',
			@FSCContent				as 'FSCContent',
			@ElectricalItem			as 'ElectricalItem',
			@QuarantinedItem		as 'QuarantinedItem',
			@SolventItem			as 'SolventItem',
			@OffensiveItem			as 'OffensiveItem',
			@WarrantyItem			as 'WarrantyItem',
			@StockOffSaleItem		as 'StockOffSaleItem',
			@InsulationItem			as 'InsulationItem',
			@PalletItem				as 'PalletItem',
			@Offer					as 'Offer',
			@EANNumber				as 'EANNumber',
			@PE						as 'EquivalentPrice',
			@DateLastReceived		as 'DateLastReceived',
			@LastReceivedQuantity	as 'LastReceivedQty',
			@DateNextDelivery		as 'DateNextDelivery',
			@NextDeliveryQuantity	as 'NextDeliveryQty',
			@OrderDays				as 'OrderDays',
			@Supplier				as 'Supplier',
			@SupplierAlternate		as 'SupplierAlternate'
	
END
GO

