﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleCustomerGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleCustomerGet'
	EXEC ('CREATE PROCEDURE [dbo].[SaleCustomerGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleCustomerGet'
GO
ALTER PROCEDURE [dbo].[SaleCustomerGet]
 @TranDate DATE,
 @TillId CHAR (2), 
 @TranNumber CHAR(4)

AS
BEGIN
	SET NOCOUNT ON;
	select	
	        dc.DATE1,
			dc.TILL,
			dc.NAME,
			dc.NUMB,
			dc.HNUM,
			dc.POST,
			dc.OSTR,
			dc.ODAT,
			dc.OTIL,
			dc.OTRN,
			dc.HNAM,
			dc.HAD1,
			dc.HAD2,
			dc.HAD3,
			dc.PHON,
			dc.NUMB,
			dc.OVAL,
			dc.OTEN,
			dc.RCOD,
			dc.LABL,
			dc.MOBP,
			dc.RTI,
			dc.EmailAddress,
			dc.PhoneNumberWork
from
		DLRCUS dc
	where
	dc.DATE1 = @TranDate
	and 
	dc.[TRAN] = @TranNumber
	and
	dc.TILL  = @TillId 	
END
GO

