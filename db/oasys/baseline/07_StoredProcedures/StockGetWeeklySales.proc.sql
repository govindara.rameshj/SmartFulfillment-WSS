﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetWeeklySales]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetWeeklySales'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetWeeklySales] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetWeeklySales'
GO
ALTER PROCEDURE [dbo].[StockGetWeeklySales]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	declare @table table(WeekStart date, WeekEnding date, QtySold int, QtyDaysOutStock int, DaysOutStock varchar(50));
	declare 
		@counter			int,
		@startDate			date,
		@endDate			date,
		@sold				int,
		@daysout			int,
		@daysOutStockQty	int,
		@daysOutStock		varchar(50);

	set @endDate = GETDATE();
	set @counter=0;
	
	while @counter<14
	begin
		set @counter		= @counter + 1;
		set @daysOutStock	= '';
		set @daysOutStockQty =0;
		
		select
			@sold = (select case 
						when @counter=1 then US001
						when @counter=2 then US002
						when @counter=3 then US003
						when @counter=4 then US004
						when @counter=5 then US005
						when @counter=6 then US006
						when @counter=7 then US007
						when @counter=8 then US008
						when @counter=9 then US009
						when @counter=10 then US0010
						when @counter=11 then US0011
						when @counter=12 then US0012
						when @counter=13 then US0013
						when @counter=14 then US0014
					end),
			@daysout = (select case 
						when @counter=1 then DO001
						when @counter=2 then DO002
						when @counter=3 then DO003
						when @counter=4 then DO004
						when @counter=5 then DO005
						when @counter=6 then DO006
						when @counter=7 then DO007
						when @counter=8 then DO008
						when @counter=9 then DO009
						when @counter=10 then DO0010
						when @counter=11 then DO0011
						when @counter=12 then DO0012
						when @counter=13 then DO0013
						when @counter=14 then DO0014
					end)
		from
			STKMAS
		where
			SKUN=@SkuNumber		
	
		--get start of week date
		set	@startDate = @EndDate;
		While datepart(weekday, @StartDate) <> 2
		begin
			set @StartDate = dateadd(day, -1, @StartDate)
		end	
	
		--work out days out stock
		if @daysout>=64
		begin
			set @daysOut = @daysOut-32;		
			if @daysOutStockQty>0 set @daysOutStock = ', ' + @daysOutStock
			set @daysOutStockQty = @daysOutStockQty + 1;
			set @daysOutStock = 'Sun' + @daysOutStock
		end
		
		if @daysout>=32
		begin
			set @daysOut = @daysOut-16;		
			if @daysOutStockQty>0 set @daysOutStock = ', ' + @daysOutStock
			set @daysOutStockQty = @daysOutStockQty + 1;
			set @daysOutStock = 'Sat' + @daysOutStock;
		end		
	
		if @daysout>=16
		begin
			set @daysOut = @daysOut-8;		
			if @daysOutStockQty>0 set @daysOutStock = ', ' + @daysOutStock
			set @daysOutStockQty = @daysOutStockQty + 1;
			set @daysOutStock = 'Fri' + @daysOutStock;
		end	

		if @daysout>=8
		begin
			set @daysOut = @daysOut-4;		
			if @daysOutStockQty>0 set @daysOutStock = ', ' + @daysOutStock
			set @daysOutStockQty = @daysOutStockQty + 1;
			set @daysOutStock = 'Thu' + @daysOutStock;
		end	

		if @daysout>=4
		begin
			set @daysOut = @daysOut-2;		
			if @daysOutStockQty>0 set @daysOutStock = ', ' + @daysOutStock
			set @daysOutStockQty = @daysOutStockQty + 1;
			set @daysOutStock = 'Wed' + @daysOutStock;
		end
		
		if @daysout>=2
		begin
			set @daysOut = @daysOut-2;		
			if @daysOutStockQty>0 set @daysOutStock = ', ' + @daysOutStock
			set @daysOutStockQty = @daysOutStockQty + 1;
			set @daysOutStock = 'Tue' + @daysOutStock;
		end	

		if @daysout=1
		begin
			if @daysOutStockQty>0 set @daysOutStock = ', ' + @daysOutStock
			set @daysOutStockQty = @daysOutStockQty + 1;
			set @daysOutStock = 'Mon' + @daysOutStock;
		end	
		
		--insert into table
		insert into @table(WeekStart, WeekEnding, QtySold, QtyDaysOutStock, DaysOutStock)
		values	(@startDate, @EndDate, @sold, @daysOutStockQty, @daysOutStock);	
		
		--set enddate to startdate less one day
		set @EndDate = dateadd(day, -1, @StartDate)
	end
	
	select * from @table;
	
END
GO

