﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetLocalCompetitors]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetLocalCompetitors'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetLocalCompetitors] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetLocalCompetitors'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 25-04-2012
-- Description:	Get List of Local Competitors
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetLocalCompetitors] 
	@Deleted bit = 0
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Id]
		  ,[Name]
	FROM CompetitorLocalList
	Where DeleteFlag = @Deleted 
	Order by Name 
END
GO

