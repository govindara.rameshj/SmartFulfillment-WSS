﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderLineUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderLineUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderLineUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderLineUpdate'
GO
-- ===============================================================================
-- Author         : Partha Dutta
-- Date	          : 23/09/2011
-- Change Request : CR0054-01
-- TFS User Story : 2574
-- TFS Task ID    : 2595
-- Description    : BO-120 iteration's database layer (rollout/rollback scripts) refactored to deal with deployment to 
--                     A. Standard Store
--                     B. eStore 120
--                     C. eWarehouse 076
-- ===============================================================================

-- =============================================
-- Author:		Not Known
-- Create date: 1st March 2011
-- Description:	Baseline version
--
-- Modified by: <User Name,,Not Specified>
-- Modified date: <Date,smalldatetime,Not Specified>
-- Description:	<Description of modification,varchar(max),Not Specified>
-- =============================================
ALTER PROCEDURE [dbo].[SaleOrderLineUpdate]
@OrderNumber CHAR (6), 
@Number CHAR (4), 
@QtyOrdered DECIMAL (5), 
@QtyTaken DECIMAL (5), 
@QtyRefunded DECIMAL (5), 
@DeliveryStatus INT, 
@QtyToDeliver INT, 
@DeliverySource CHAR (4)=null, 
@DeliverySourceIbtOut CHAR (6)=null, 
@SellingStoreIbtIn CHAR (6)=null,
@QtyScanned INT=0,
@SourceOrderNumber Char(20)=null

AS
BEGIN
	SET NOCOUNT ON;

	update 
		CORLIN
	set
		QTYO			= @QtyOrdered,
		QTYT			= @QtyTaken,
		QTYR			= @QtyRefunded,
		DeliveryStatus		= @DeliveryStatus,
		QtyToBeDelivered	= @QtyToDeliver,
		DeliverySource		= @DeliverySource,
		DeliverySourceIbtOut	= @DeliverySourceIbtOut,
		SellingStoreIbtIn	= @SellingStoreIbtIn,
		QuantityScanned 	= @QtyScanned

	where
		NUMB			= @OrderNumber
		and LINE		= @Number
	
	return @@rowcount
	
END
GO

