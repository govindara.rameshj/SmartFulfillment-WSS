﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingGetLatestSafe]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingGetLatestSafe'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingGetLatestSafe] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingGetLatestSafe'
GO
ALTER PROCedure NewBankingGetLatestSafe

   @PeriodID int output

as
begin

   set @PeriodID = (select max(PeriodID) from [Safe])

   return @PeriodID

end
GO

