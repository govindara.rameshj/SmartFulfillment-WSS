﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetStockAdjustmentForSkuToday]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetStockAdjustmentForSkuToday'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetStockAdjustmentForSkuToday] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetStockAdjustmentForSkuToday'
GO
ALTER PROCEDURE [dbo].[usp_GetStockAdjustmentForSkuToday] 
	-- Add the parameters for the stored procedure here
	@ProductCode Char(6) = null, 
	@AdjustDate Date = null,
	@CodeNumber Char(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		DATE1			as 'AdjustmentDate',
		CODE			as 'AdjustmentCode',
		SKUN			as 'SKUN',
		SEQN			as 'SEQN',
		AmendId			as 'AmendId',
		[INIT]			as 'Initials',
		DEPT			as 'Department',	
		SSTK			as 'StartingStock',
		QUAN			as 'AdjustmentQty',
		PRIC			as 'AdjustmentPrice',
		COST			as 'AdjustmentCost',
		COMM			as 'IsSentToHO',
		[TYPE]			as 'AdjustmentType',
		INFO			as 'Comment',
		DRLN			as 'DRLNumber',
		RCOD			as 'ReversalCode',
		MOWT			as 'MarkDownWriteOff',
		WAUT			as 'WriteOffAuthorised',
		DAUT			as 'DateAuthorised',
		RTI			as 'RTI',
		PeriodId		as 'PeriodId',
		TSKU			as 'TransferSku',
		TVAL			as 'TransferValue',
		TransferStart	as 'TransferStart',
		TransferPrice	as 'TransferPrice',
		IsReversed		as 'IsReversed'
	FROM
		STKADJ
	WHERE
		SKUN = @ProductCode And 
		CODE = @CodeNumber AND
		WAUT  In ('000', '0') And
		(@AdjustDate is null or (@AdjustDate is not null and DATE1 = @AdjustDate))
		Order by SEQN DESC, DATE1 desc
END
GO

