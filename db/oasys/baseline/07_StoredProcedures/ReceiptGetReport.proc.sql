﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptGetReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptGetReport'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptGetReport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptGetReport'
GO
ALTER PROCedure ReceiptGetReport
   @ReceiptNumber   char(6) = null
AS
begin
   set nocount on;
   --get header info into temp table
   declare @header table (Column1 varchar(100),
                          Column2 varchar(200),
                          Column3 varchar(100),
                          Column4 varchar(100),
                          Column5 varchar(100),
                          Qty     varchar(100),
                          Value   varchar(100));
   declare  @PoNumber            int,
            @Supplier            char(5),
            @SupplierName        varchar(100),
            @CombinedSupplier    varchar(105),
            @IBTCombinedSupplier varchar(125),
            @Consignment         int,
            @OrderDate           date,
            @ReceivedDate        date,
            @DeliveryNote1       varchar(10),
            @DeliveryNote2       varchar(10),
            @DeliveryNote3       varchar(10),
            @DeliveryNote4       varchar(10),
            @DeliveryNote5       varchar(10),
            @DeliveryNote6       varchar(10),
            @DeliveryNote7       varchar(10),
            @DeliveryNote8       varchar(10),
            @DeliveryNote9       varchar(10),
            @DeliveryNotes       varchar(110),
            @Comment             varchar(20),
            @RawType             char(1),
            @Type                varchar(20),
            @UnitsRec            int,
            @UnitsOrd            int,
            @ValueRec            dec(9,2),
            @ValueOrd            dec(9,2);

	select

		@PoNumber		= ds.[0PON],
		@Supplier		= case
								when ds.[TYPE] = 0 then ds.[0SUP]
								when ds.[TYPE] = 1 then ds.[1STR]
								when ds.[TYPE] = 2 then ds.[1STR]
								when ds.[TYPE] = 3 then ds.[3SUP]
							end,
		@SupplierName	= case
								when ds.[TYPE] = 0 then (Select NAME from SUPMAS where SUPN=ds.[0SUP])
								when ds.[TYPE] = 1 then (Select Name from Store where Id=ds.[1STR])
								when ds.[TYPE] = 2 then (Select Name from Store where Id=ds.[1STR])
								when ds.[TYPE] = 3 then (Select NAME from SUPMAS where SUPN=ds.[3SUP])
							end,
		@Consignment	= case
								when ds.[TYPE] = 0 then ds.[0CON]
								when ds.[TYPE] = 1 then ds.[1CON]
								when ds.[TYPE] = 2 then ''
								when ds.[TYPE] = 3 then ''
							end,
		@OrderDate		= ds.[0DAT],
		@ReceivedDate	= ds.DATE1,
		@DeliveryNote1	= case
								when ds.[TYPE] = 0 then ds.[0DL1]
								when ds.[TYPE] = 1 then cast(ds.[1IBT] as varchar(10))
								when ds.[TYPE] = 2 then ''
								when ds.[TYPE] = 3 then ''
							end,
		@DeliveryNote2	= ds.[0DL2],
		@DeliveryNote3	= ds.[0DL3],
		@DeliveryNote4	= ds.[0DL4],
		@DeliveryNote5	= ds.[0DL5],
		@DeliveryNote6	= ds.[0DL6],
		@DeliveryNote7	= ds.[0DL7],
		@DeliveryNote8	= ds.[0DL8],
		@DeliveryNote9	= ds.[0DL9],
		@Comment		= ds.INFO,
		@RawType        = ds.[TYPE],
		@Type			=	case ds.[TYPE]
								when 0 then 'Receipt'
								when 1 then 'IBT-In'
								when 2 then 'IBT-Out'
								when 3 then 'Return'
							end
	from
		DRLSUM ds
	where
		ds.NUMB=@ReceiptNumber;

    select @UnitsRec = case
                          when ds.[TYPE] = 0 then SUM(dt.RECQ)
                          when ds.[TYPE] = 1 then SUM(dt.IBTQ)
                          when ds.[TYPE] = 2 then SUM(dt.IBTQ)
                          when ds.[TYPE] = 3 then SUM(dt.RETQ)
                       end, 
           @UnitsOrd = case
                          when ds.[TYPE] = 0 then SUM(dt.ORDQ)
                          when ds.[TYPE] = 1 then 0
                          when ds.[TYPE] = 2 then 0
                          when ds.[TYPE] = 3 then 0
                       end, 
           @ValueRec = case
                          when ds.[TYPE] = 0 then SUM(dt.RECQ * dt.PRIC)
                          when ds.[TYPE] = 1 then SUM(dt.IBTQ * dt.PRIC)
                          when ds.[TYPE] = 2 then SUM(dt.IBTQ * dt.PRIC)
                          when ds.[TYPE] = 3 then SUM(dt.RETQ * dt.PRIC)
                       end,                                                
           @ValueOrd = case
                          when ds.[TYPE] = 0 then SUM(dt.ORDQ*ORDP)
                          when ds.[TYPE] = 1 then 0
                          when ds.[TYPE] = 2 then 0
                          when ds.[TYPE] = 3 then 0
                       end 
	from
		DRLDET dt
    inner join
        DRLSUM ds on ds.NUMB = dt.NUMB
	where
		dt.NUMB = @ReceiptNumber
    group by
        dt.NUMB, ds.TYPE

	set	@DeliveryNotes = @DeliveryNote1;
	if len(@DeliveryNote2) > 0 set @DeliveryNotes += ', ' + @DeliveryNote2;
	if len(@DeliveryNote3) > 0 set @DeliveryNotes += ', ' + @DeliveryNote3;
	if len(@DeliveryNote4) > 0 set @DeliveryNotes += ', ' + @DeliveryNote4;
	if len(@DeliveryNote5) > 0 set @DeliveryNotes += ', ' + @DeliveryNote5;
	if len(@DeliveryNote6) > 0 set @DeliveryNotes += ', ' + @DeliveryNote6;
	if len(@DeliveryNote7) > 0 set @DeliveryNotes += ', ' + @DeliveryNote7;
	if len(@DeliveryNote8) > 0 set @DeliveryNotes += ', ' + @DeliveryNote8;
	if len(@DeliveryNote9) > 0 set @DeliveryNotes += ', ' + @DeliveryNote9;

    --supplier info displayed differnetly depending on type
    if @RawType = 1 or @RawType = 2
    begin
       --type "ibt in" or "ibt out"
       set @CombinedSupplier    = null
       set @IBTCombinedSupplier = @Type + ' (' + @Supplier + ' ' + @SupplierName + ')'
    end
    else if @RawType = 3
    begin
       --type "return"
       set @CombinedSupplier = @Supplier + ' ' + @SupplierName
    end
    else
    begin
       --type "normal" or "receipt"
       set @CombinedSupplier = @Supplier + ' ' + @SupplierName
    end

	insert into @header values ('PO Number:',   @PoNumber,            'Order Date:',     convert(varchar(10), @OrderDate, 103),    'Units Rec/Ord:', @UnitsRec, @UnitsOrd);
	insert into @header values ('Supplier:',    @CombinedSupplier,    'Received Date:',  convert(varchar(10), @ReceivedDate, 103), 'Value Rec/Ord:', @ValueRec, @ValueOrd);
	insert into @header values ('DRL Number:',  @ReceiptNumber,       'Delivery Notes:', @DeliveryNotes,                           null,             null,      null);
	insert into @header values ('Consignment:', @Consignment,         'Comment:',        @Comment,                                 null,             null,      null);
	insert into @header values ('Type:',        @IBTCombinedSupplier, null,              null,                                     null,             null,      null);
	select * from @header;

	select Sequence      = dt.seqn,
           SkuNumber     = dt.skun,
           [Description] = st.descr,
           ProductCode   = st.prod,
           PackSize      = st.pack,
           QtyOrdered    = dt.ordq,
           /*PriceOrdered  = dt.ordp*/
           /*QtyReceived   = dt.recq,*/
           PriceReceived = dt.pric,
           [Quantity Received / IBT / Returned] = case
                                                     when ds.TYPE = 0 then dt.RECQ     /* 'Quantity Received'  */
                                                     when ds.TYPE = 1 then dt.IBTQ     /* 'Quantity IBT In'    */
                                                     when ds.TYPE = 2 then dt.IBTQ     /* 'Quantity IBT Out'   */
                                                     when DS.TYPE = 3 then DT.RETQ     /* 'Quantity Returned'  */
                                                  end,
		   dt.ReasonCode
	from		
		drldet dt
	inner join	
		stkmas st on st.skun = dt.skun
    inner join
        DRLSUM ds on ds.NUMB = dt.NUMB
	where		
		dt.numb = @ReceiptNumber
	order by	
		dt.seqn
end
GO

