﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_TemporaryDealGroupsGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_TemporaryDealGroupsGet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_TemporaryDealGroupsGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_TemporaryDealGroupsGet'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 23 January 2012
-- Description:	Temporary Deal Groups Report
-- =============================================
ALTER PROCEDURE [dbo].[usp_TemporaryDealGroupsGet] 
	-- Add the parameters for the stored procedure here
	@DateStart Date, 
	@DateEnd Date 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here    -- Insert statements for procedure here
	SELECT distinct
		eh.ID as 'Id',
		'9' + RIGHT('0000'+ CONVERT(VARCHAR,eh.id),5) as 'DealGroupNo',
		et.ShortDescription as 'Code',
		et.CompleteDescription as 'Description',
		eh.Price as 'DealPrice',
		eh.AuthorisorSecurityID  as 'AuthCode',
		(select TillReceiptName from SystemUsers where ID=eh.AuthorisorSecurityID) as 'AuthName',
		eh.CashierSecurityID as 'CashierId',
		(select TillReceiptName from SystemUsers where ID=eh.CashierSecurityID) as 'CashierName',
		eh.StartDate as 'Date'
	From EventHeaderOverride eh inner join EventDetailOverride ed on eh.ID = ed.EventHeaderOverrideID
	inner join EventType et on eh.EventTypeID = et.ID 
	where eh.StartDate >= @DateStart and eh.EndDate <= @DateEnd 
	And eh.EventTypeId=3

	SELECT 
		EventHeaderOverrideId as 'Id',
		SkuNumber as 'SkuNumber',
		SkuNormalSellingPrice as 'SystemPrice',
		Quantity as 'Qty',
		(select Descr from STKMAS where SKUN=SkuNumber) as 'Description'
	From EventHeaderOverride eh inner join EventDetailOverride ed on eh.ID = ed.EventHeaderOverrideID
	where eh.StartDate >= @DateStart and eh.EndDate <= @DateEnd 
	And eh.EventTypeId=3

END
GO

