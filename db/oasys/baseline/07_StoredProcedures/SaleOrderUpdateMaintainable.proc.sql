﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderUpdateMaintainable]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderUpdateMaintainable'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderUpdateMaintainable] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderUpdateMaintainable'
GO
ALTER PROCEDURE [dbo].[SaleOrderUpdateMaintainable]
@OrderNumber CHAR (6), 
@DateDelivery DATE=null, 
@DateDespatch DATE=null, 
@DeliveryAddress1 VARCHAR (60), 
@DeliveryAddress2 VARCHAR (30)=null, 
@DeliveryAddress3 VARCHAR (30), 
@DeliveryAddress4 VARCHAR (30)=null, 
@DeliveryPostCode VARCHAR (8)=null, 
@PhoneNumber VARCHAR (20)=null, 
@PhoneNumberMobile VARCHAR (15)=null, 
@PhoneNumberWork VARCHAR (20)=null, 
@CustomerEmail VARCHAR (100)=null, 
@IsSuspended BIT, 
@RevisionNumber INT, 
@IsPrinted BIT, 
@NumberReprints DECIMAL (3), 
@DeliveryConfirmed BIT=0

AS
BEGIN
	SET NOCOUNT ON;
	
	declare @rowcount int;
	set @rowcount=0;	

	--update corhdr with delivery date
	update
		CORHDR
	set
		DELD	= @DateDelivery,
		DELC	= @DeliveryConfirmed,
		ADDR1	= @DeliveryAddress1,
		ADDR2	= @DeliveryAddress2,
		ADDR3	= @DeliveryAddress3,
		ADDR4	= @DeliveryAddress4,
		POST	= @DeliveryPostCode,
		PHON	= @PhoneNumber,
		MOBP	= @PhoneNumberMobile,
		REVI	= @RevisionNumber,
		PRNT	= @IsPrinted,
		RPRN	= @NumberReprints
	where
		NUMB	= @OrderNumber;

	--get number of rows affected
	set @rowcount = @@ROWCOUNT;

	--update corhdr4 with info
	update
		CORHDR4
	set
		DELD			= @DateDelivery,
		DDAT			= @DateDespatch,
		CustomerEmail	= @CustomerEmail,
		PhoneNumberWork	= @PhoneNumberWork,
		IsSuspended		= @IsSuspended
	where
		NUMB	= @OrderNumber;

	--get number of rows affected and return
	set @rowcount = @rowcount + @@ROWCOUNT;	
	return @@rowcount
END
GO

