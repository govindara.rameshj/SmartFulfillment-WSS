﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockOutOfStockCount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockOutOfStockCount'
	EXEC ('CREATE PROCEDURE [dbo].[StockOutOfStockCount] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockOutOfStockCount'
GO
-- Verion Control Information
-- =============================================
-- Author:  	Kevan Madelin
-- Create date: 23/08/2010
-- Version:	3.0.0.0
-- Description: This procedure is used to get the stock data for the Managers Dashboard.
-- =============================================
ALTER PROCEDURE [dbo].[StockOutOfStockCount]
@Quantity INT OUTPUT
AS
set nocount on
DECLARE @TODT		DATETIME;
	SET @TODT	= (SELECT SYSDAT.TODT FROM SYSDAT WHERE SYSDAT.FKEY='01');
	
	SELECT @Quantity = COUNT(STKMAS.SKUN) FROM STKMAS 
					WHERE STKMAS.ONHA <= 0
					AND STKMAS.IDEL=0 
					AND STKMAS.IOBS=0
					AND STKMAS.INON=0
					AND STKMAS.IRIS=0
					AND STKMAS.ICAT=0
					AND STKMAS.NOOR=0 
					AND STKMAS.DATS IS NOT NULL
					AND STKMAS.FODT IS NULL 
					AND STKMAS.FODT<@TODT;
					
	IF @Quantity IS NULL SELECT @Quantity = 0;
GO

