﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GiftVoucherGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GiftVoucherGet'
	EXEC ('CREATE PROCEDURE [dbo].[GiftVoucherGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GiftVoucherGet'
GO
ALTER PROCedure [dbo].[GiftVoucherGet]
    @Date   date = null
as
begin

select
    dg.DATE1    as 'Date',
    dg.TILL     as 'TillId',
    dg.[TRAN]   as 'TranNumber',
    dt.OVCTranNumber as 'OVCTranNumber',
    dg.SERI     as 'SerialNumber',
    case dg.[TYPE]
        when 'SA' then 'Allocation'
        when 'TR' then 'Allocation'
        when 'CR' then 'Allocation'
        when 'CC' then 'Allocation'
        when 'VR' then 'Allocation'
        when 'TS' then 'Redemption'
        when 'RR' then 'Redemption'
        when 'CS' then 'Redemption'
        when 'VS' then 'Redemption'
    end         as 'VoucherType'
from
    DLGIFT dg
    inner join vwDLTOTS dt on dg.DATE1 = dt.DATE1 and dg.TILL = dt.TILL and dg.[TRAN] = dt.[TRAN]
where
    @Date is null or (@Date is not null and dg.DATE1 = @Date) 
union
select
    gc.DATE1    as 'Date',
    gc.TILL as 'TillId',
    gc.[TRAN]   as 'TranNumber',
    dt.OVCTranNumber as 'OVCTranNumber',
    gc.CARDNUM  as 'SerialNumber',
    case [TYPE]
        when 'SA' then 'Allocation'
        when 'TR' then 'Allocation'
        when 'CR' then 'Allocation'
        when 'CC' then 'Allocation'
        when 'VR' then 'Allocation'
        when 'TS' then 'Redemption'
        when 'RR' then 'Redemption'
        when 'CS' then 'Redemption'
        when 'VS' then 'Redemption'
    end     as 'VoucherType'
from 
    dlgiftcard gc
    inner join vwDLTOTS dt on gc.DATE1 = dt.DATE1 and gc.TILL = dt.TILL and gc.[TRAN] = dt.[TRAN]
where
    @Date is null or (@Date is not null and gc.DATE1 = @Date) 
order by
    1,2,3
end
GO

