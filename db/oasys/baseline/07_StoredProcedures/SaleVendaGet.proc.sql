﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleVendaGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleVendaGet'
	EXEC ('CREATE PROCEDURE [dbo].[SaleVendaGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleVendaGet'
GO
ALTER PROCEDURE [dbo].[SaleVendaGet]
 @DocumentNumber CHAR(8)  
 AS
BEGIN
	SET NOCOUNT ON;	
	
SELECT
	 
            dt.DATE1,
            dt.[TRAN],
            dt.TILL,
     		dt.CASH ,
			dt.[TIME],
			dt.SUPV,
			dt.TCOD,
			dt.[OPEN],
			dt.MISC,
			dt.DESCR,
			dt.ORDN ,
			dt.ACCT,
			dt.VOID,
			dt.VSUP,
			dt.TMOD,
			dt.[PROC],
			dt.DOCN,
			dt.SUSE,
			dt.STOR,
			dt.MERC,
			dt.NMER,
			dt.TAXA,
			dt.DISC,
			dt.DSUP,
			dt.TOTL,
			dt.ACCN,
			dt.[CARD],
			dt.AUPD,
			dt.BACK,
			dt.ICOM,
			dt.IEMP,
			dt.RCAS,
			dt.RSUP,
			dt.PARK,
			dt.RMAN,
			dt.TOCD,
			dt.PKRC,
			dt.REMO,
			dt.GTPN,
			dt.CCRD,
			dt.SSTA,
			dt.SSEQ,
			dt.CBBU,
			dt.CARD_NO,
			dt.RTI ,
			dt.VATR1 ,
			dt.VATR2 ,
			dt.VATR3,
			dt.VATR4 ,
			dt.VATR5 ,
			dt.VATR6 ,
			dt.VATR7,
			dt.VATR8 ,
			dt.VATR9 ,
			dt.VSYM1,
			dt.VSYM2 ,
			dt.VSYM3 ,
			dt.VSYM4 ,
			dt.VSYM5 ,
			dt.VSYM6 ,
			dt.VSYM7,
			dt.VSYM8 ,
			dt.VSYM9 ,
			dt.XVAT1,
			dt.XVAT2,
			dt.XVAT3 ,
			dt.XVAT4 ,
			dt.XVAT5,
			dt.XVAT6 ,
			dt.XVAT7 ,
			dt.XVAT8 ,
			dt.XVAT9,
			dt.VATV1 ,
			dt.VATV2 ,
			dt.VATV3 ,
			dt.VATV4 ,
			dt.VATV5 ,
			dt.VATV6,
			dt.VATV7 ,
			dt.VATV8,
			dt.VATV9 			

FROM DLTOTS dt   
			
			WHERE
			TCOD 	= 'SA' AND
			DOCN	= @DocumentNumber			
END
GO

