﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGetReportTables]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReportGetReportTables'
	EXEC ('CREATE PROCEDURE [dbo].[ReportGetReportTables] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReportGetReportTables'
GO
ALTER PROCEDURE [dbo].[ReportGetReportTables]
@ReportId INT
AS
BEGIN
	SET NOCOUNT ON;

	select
		rt.ReportId,
		rt.id,
		rt.Name,
		rt.AllowGrouping,
		rt.AllowSelection,
		rt.ShowHeaders,
		rt.ShowIndicator,
		rt.showBorder,
		rt.ShowInPrintOnly,
		rt.AutoFitColumns,
		rt.HeaderHeight,
		rt.RowHeight,
		rt.ShowLinesVertical,
		rt.ShowLinesHorizontal,
		rt.AnchorTop,
		rt.AnchorBottom
	from
		ReportTable rt
	where
		rt.ReportId = @ReportId
	order by
		rt.Id

END
GO

