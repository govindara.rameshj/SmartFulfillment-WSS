﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UserGetUsers]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure UserGetUsers'
	EXEC ('CREATE PROCEDURE [dbo].[UserGetUsers] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure UserGetUsers'
GO
ALTER PROCEDURE [dbo].[UserGetUsers]
AS
begin
	SET NOCOUNT ON;

	select 
		*
	from
		SystemUsers
		
end
GO

