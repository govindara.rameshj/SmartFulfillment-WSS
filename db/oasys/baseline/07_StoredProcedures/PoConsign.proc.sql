﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoConsign]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoConsign'
	EXEC ('CREATE PROCEDURE [dbo].[PoConsign] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoConsign'
GO
ALTER PROCEDURE [dbo].[PoConsign]
(
	@OrderId		int,
	@PoNumber		int,
	@ReleaseNumber	smallint,
	@SupplierNumber	char(5),
	@EmployeeId		int,
	@PalletsIn		int,
	@PalletsOut		int,
	@DeliveryNote1	char(10),
	@DeliveryNote2	char(10),
	@DeliveryNote3	char(10),
	@DeliveryNote4	char(10),
	@DeliveryNote5	char(10),
	@DeliveryNote6	char(10),
	@DeliveryNote7	char(10),
	@DeliveryNote8	char(10),
	@DeliveryNote9	char(10),
	@Number			int output
)
AS
BEGIN
	SET NOCOUNT ON;

	-- get next consignment number from system numbers
	exec	@Number = SystemNumbersGetNext @Id=5
		
	-- insert new conmas record
	insert into conmas
	(
		numb,
		pono,
		prel,
		supp,
		eeid,
		edat,
		rcvd,
		rtnd,
		dnot1,
		dnot2,	
		dnot3,
		dnot4,
		dnot5,
		dnot6,
		dnot7,
		dnot8,
		dnot9
	)
	values
	(
		@Number,
		@PoNumber,
		@ReleaseNumber,
		@SupplierNumber,
		@EmployeeId,
		getdate(),
		@PalletsIn,
		@PalletsOut,
		@DeliveryNote1,
		@DeliveryNote2,
		@DeliveryNote3,
		@DeliveryNote4,
		@DeliveryNote5,
		@DeliveryNote6,
		@DeliveryNote7,
		@DeliveryNote8,
		@DeliveryNote9
	)

	-- update purchase order header as consigned
	update	purhdr
	set		pnum = @Number
	where	tkey = @OrderId


END
GO

