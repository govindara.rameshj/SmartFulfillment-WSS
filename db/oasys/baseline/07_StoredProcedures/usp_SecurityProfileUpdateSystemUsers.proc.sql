﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SecurityProfileUpdateSystemUsers]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SecurityProfileUpdateSystemUsers'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SecurityProfileUpdateSystemUsers] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SecurityProfileUpdateSystemUsers'
GO
-- =============================================
-- Author     :	Alan Lewis
-- Create date:	10/05/2011
-- Description:	Use the IsSupervisor and IsManager values to update
--	            any SystemUser entries that have their SecurityProfileId
--	            equal to the SecurityProfileId value
--
-- Author     : Partha Dutta
-- Modify Date: 16/05/2011
-- Description: Performing an update on multiple records is causing the trigger on SystemUsers to fail
--              This is due to a sub-query issue
--              Solution: Update one record at a time; for the volumn of records this will not result in any performance issues
-- =============================================

ALTER PROCEDURE [dbo].[usp_SecurityProfileUpdateSystemUsers]
   @SecurityProfileId	int,
   @IsSupervisor		bit,
   @IsManager		bit
AS
BEGIN
	SET NOCOUNT ON;
/*	
	UPDATE
		SystemUsers
	SET
		[IsSupervisor]		= @IsSupervisor,
		[IsManager]		= @IsManager
	WHERE
		[SecurityProfileId] = @SecurityProfileId
*/

   declare @CursorID int

   declare @Temp         table(ID         int          not null,
                               GuidID     nvarchar(50) not null,
                               BuildOrder int          not null)

   declare TempCursor cursor for
   select ID from SystemUsers where SecurityProfileId = SecurityProfileId
   open TempCursor
   fetch next from TempCursor into @CursorID
   while @@fetch_status = 0
   begin
      update SystemUsers set IsSupervisor = @IsSupervisor,
                             IsManager    = @IsManager
      where ID = @CursorID 

      --next record
      fetch next from TempCursor into @CursorID
   end
   close TempCursor
   deallocate TempCursor

END
GO

