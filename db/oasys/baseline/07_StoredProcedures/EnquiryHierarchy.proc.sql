﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryHierarchy]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryHierarchy'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryHierarchy] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryHierarchy'
GO
ALTER PROCEDURE [dbo].[EnquiryHierarchy]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	declare @table table (Description varchar(50), Display varchar(50));
	declare
		@ctgy			char(6),
		@grup			char(6),
		@sgrp			char(6),
		@styl			char(6),
		@hieCategory	varchar(50),
		@hieGroup		varchar(50),
		@hieSubgroup	varchar(50),
		@hieStyle		varchar(50);

	select
		@ctgy		= CTGY,
		@grup		= GRUP,
		@sgrp		= SGRP,
		@styl		= STYL
	from
		STKMAS
	where
		SKUN = @SkuNumber;

	set @hieCategory	= (select DESCR from HIEMAS where LEVL=5 and NUMB=@ctgy);
	set @hieGroup		= (select DESCR from HIEMAS where LEVL=4 and NUMB=@grup);
	set @hieSubgroup	= (select DESCR from HIEMAS where LEVL=3 and NUMB=@sgrp);
	set @hieStyle		= (select DESCR from HIEMAS where LEVL=2 and NUMB=@styl);
	
	insert into @table values ('Category',	rtrim(@hieCategory) );
	insert into @table values ('Group',		rtrim(@hieGroup) );
	insert into @table values ('Sub-group',	rtrim(@hieSubgroup) );
	insert into @table values ('Style',		rtrim(@hieStyle) );
	
	select * from @table;
END
GO

