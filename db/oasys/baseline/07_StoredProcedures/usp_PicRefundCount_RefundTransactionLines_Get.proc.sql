﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_PicRefundCount_RefundTransactionLines_Get]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_PicRefundCount_RefundTransactionLines_Get'
	EXEC ('CREATE PROCEDURE [dbo].[usp_PicRefundCount_RefundTransactionLines_Get] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_PicRefundCount_RefundTransactionLines_Get'
GO
-- =============================================
-- Author       : Alan Lewis
-- Create date  : 17/04/2012
-- User Story	: 4787
--Change Request: CR0081 - Adding Refund SKUS to PIC Count
-- Task Id		: 4903
-- Description  : Create stored procedure to retrieve refund details for a given range of dates
-- =============================================
-- =============================================
-- Author       : Alan Lewis
-- Update date  : 12/07/2012
-- Referral     : RF1070 - Remedial work on CR0081
-- Task Id		: 6044, 5863
-- Description  : Add filter for not including QOD orders - i.e. DTLTOTS.ORDN = '000000'
-- =============================================
ALTER PROCedure usp_PicRefundCount_RefundTransactionLines_Get
	@StartDate as Date,
	@EndDate as Date
As
	Begin
		Select
			L.DATE1 As 'Transaction Date',
			L.TILL As 'Transaction Till',
			L.[TRAN] As 'Transaction Number',
			L.NUMB As 'Transaction Line Number',
			L.SKUN As 'Item Sku Number',
			L.PRIC As 'Item Price',
			L.QUAN As 'Item Quantity',
			ISNULL(C.OTRN, '0000') As 'Original Transaction Number',
			(
				Select
					COUNT(*)
				From
					DLPAID P
				Where
					P.DATE1 = T.DATE1
				And
					P.TILL = T.TILL
				And
					P.[TRAN] = T.[TRAN]
				And
					P.[TYPE] = 1
			)
			As 'Total Number Of Cash And Gift Token Refund Payments'
		From
			DLLINE As L
				Inner Join
					DLTOTS T
				On
					L.DATE1 = T.DATE1
				And
					L.TILL = T.TILL
				And
					L.[TRAN] = T.[TRAN]
				Left Outer Join
					DLRCUS C
				On
					L.DATE1 = C.DATE1
				And
					L.TILL = C.TILL
				And
					L.[TRAN] = C.[TRAN]
				And
					L.NUMB = C.NUMB
		Where
			L.QUAN < 0
		And
			L.LREV = 0
		And
			L.SALT <> 'D'
		And
			L.SALT <> 'V'
		And
			DATEDIFF(d, @StartDate, L.DATE1) >= 0
		And
			DATEDIFF(d, @EndDate, L.DATE1) <= 0
		And
			T.VOID = 0
		And
			T.PARK = 0
		And
			T.TMOD = 0
		And
			IsNull(T.ORDN, '000000') = '000000'
		And
			(
				Select
					COUNT(*)
				From
					DLLINE DL
				Where
					DL.DATE1 = T.DATE1
				And
					DL.TILL = T.TILL
				And
					DL.[TRAN] = T.[TRAN]
				And
					DL.QUAN >= 0
				And
					DL.LREV = 0
				And
					DL.SALT <> 'V'
			) = 0
		Order By
			L.DATE1,
			L.TILL,
			L.[TRAN],
			L.NUMB
	End
GO

