﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ClickAndCollectCashierAutoPickup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ClickAndCollectCashierAutoPickup'
	EXEC ('CREATE PROCEDURE [dbo].[ClickAndCollectCashierAutoPickup] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ClickAndCollectCashierAutoPickup'
GO
ALTER PROCEDURE dbo.ClickAndCollectCashierAutoPickup
	@periodId int,
	@userId int
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE
		@sum decimal(9, 2),
		@sumid decimal(9, 2),
		@currencyid char(3),
		@cashierid int,
		@tenderid int,
		@sealnumber varchar(20),
		@ts datetime,
		@today datetime,
		@tomorrow datetime,
		@currperiod int,
		@bagid int
	
	SET @tenderid = 12
	SET @cashierid = 499
	SET @sealnumber = '9' + RIGHT(REPLICATE('0', 12) + CAST(@periodId AS varchar), 8)
	
	SELECT @sumid = [ID], @currencyid = [CurrencyID]
	FROM [dbo].[SystemCurrencyDen]
	WHERE TenderID = @tenderid
	
	SELECT @sum = [Amount]
	FROM dbo.CashBalCashierTen
	WHERE [PeriodID] = @periodId AND [CashierID] = @cashierid AND [CurrencyID] = @currencyid
	
	IF @@ROWCOUNT = 0
		RETURN

	DECLARE @sysperiods table
	(
		[ID] [int],
		[StartDate] [datetime],
		[EndDate] [datetime],
		[IsClosed] [bit]
	)
	
	SET @ts = GETDATE()
	SET @today = DATEADD(DAY, DATEDIFF(DAY, 0, @ts), 0)
	SET @tomorrow = DATEADD(DAY, 1, @today)
	
	INSERT INTO @sysperiods
	EXEC SystemPeriodGet @StartDate=@today, @EndDate=@tomorrow, @IsClosed=0

	SELECT @currperiod = [ID]
	FROM @sysperiods
	
	SELECT @bagid = [ID]
	FROM dbo.SafeBags
	WHERE [PickupPeriodID] = @periodId AND [AccountabilityID] = @cashierid
	
	IF @bagid IS NULL
	BEGIN
		INSERT INTO dbo.SafeBags ([SealNumber], [InPeriodID], [InDate], [InUserID1], [InUserID2],
								[OutPeriodID], [OutDate], [OutUserID1], [OutUserID2], [AccountabilityType], [AccountabilityID],
								[Type], [State], [Value], [FloatValue], [RelatedBagId], [PickupPeriodID], [Comments])
		VALUES (@sealnumber, @currperiod, @ts, @userId, @userId,
				0, NULL, 0, 0, 'C', @cashierid,
				'P', 'S', @sum, 0, 0, @periodId, '')

		SET @bagid = SCOPE_IDENTITY()
		
		DELETE FROM dbo.SafeBagsDenoms WHERE [BagID] = @bagid
		INSERT INTO dbo.SafeBagsDenoms ([BagID], [CurrencyID], [TenderID], [ID], [Value], [SlipNumber])
		VALUES  (@bagid, @currencyid, @tenderid, @sumid, @sum, '')
	END
	ELSE
	BEGIN
		UPDATE dbo.SafeBags
		SET [Value] = @sum
		WHERE ID = @bagid
		
		UPDATE dbo.SafeBagsDenoms
		SET [Value] = @sum
		WHERE [BagID] = @bagid
	END
	
	UPDATE dbo.CashBalCashier
	SET FloatVariance = 0
	WHERE [PeriodID] = @periodId AND [CurrencyID] = @currencyid AND [CashierID] = @cashierid
	
	UPDATE dbo.CashBalCashierTen
	SET PickUp = Amount
	WHERE [PeriodID] = @periodId AND [CurrencyID] = @currencyid AND [CashierID] = @cashierid

	SET NOCOUNT OFF
END
GO

