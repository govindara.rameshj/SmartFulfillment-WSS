﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingFloatDenomination]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingFloatDenomination'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingFloatDenomination] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingFloatDenomination'
GO
ALTER PROCedure NewBankingFloatDenomination
   @FloatID int
as
begin
set nocount on
select TenderID       = a.TenderID,
       DenominationID = a.ID,
       FloatValue     = b.Value 
from (select TenderID, ID
      from SystemCurrencyDen
      where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
      and   TenderID = 1) a
left outer join (select TenderID, ID, Value 
                 from SafeBagsDenoms 
                 where BagID = @FloatID
                 and CurrencyID = (select ID from SystemCurrency where IsDefault = 1)) b
           on  b.TenderID = a.TenderID
           and b.ID       = a.ID
end
GO

