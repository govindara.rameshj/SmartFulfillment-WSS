﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingBagCollectionUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingBagCollectionUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingBagCollectionUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingBagCollectionUpdate'
GO
ALTER PROCedure NewBankingBagCollectionUpdate
   @BagID   int,
   @SlipNo  varchar(13),
   @Comment varchar(100)
as
begin
set nocount on
update SafeBags set BagCollectionSlipNo  = @SlipNo,
                    BagCollectionComment = @Comment
where ID = @BagID 
end
GO

