﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UserProfileGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure UserProfileGet'
	EXEC ('CREATE PROCEDURE [dbo].[UserProfileGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure UserProfileGet'
GO
ALTER PROCEDURE [dbo].[UserProfileGet]
@Id INT=null
AS
begin
	SET NOCOUNT ON;

	select 
		Id,
		Description,
		PasswordValidFor	as DaysPasswordValid,
		IsDeleted
	from
		SecurityProfile
	where
		ProfileType='U'
		and ((@Id is null) or (@Id is not null and ID = @Id))
	
end
GO

