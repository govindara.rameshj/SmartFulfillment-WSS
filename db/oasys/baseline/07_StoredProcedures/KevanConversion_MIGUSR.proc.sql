﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_MIGUSR]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_MIGUSR'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_MIGUSR] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_MIGUSR'
GO
ALTER PROCEDURE dbo.KevanConversion_MIGUSR
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 01 - Migrate Users
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF;

disable trigger SystemUsers.tgSystemUsersInsert on SystemUsers;
disable trigger SystemUsers.tgSystemUsersInsert on SystemUsers;

delete from SystemUsers

create table #sysPassTemp
(
    RID INT IDENTITY(1,1),
    [EEID] [char](3) NOT NULL,
	[NAME] [char](35) NULL,
	[INIT] [char](5) NULL,
	[POSI] [char](20) NULL,
	[EPAY] [char](20) NULL,
	[PASS] [char](5) NULL,
	[DPAS] [date] NULL,
	[SUPV] [bit] ,
	[MANA] [bit] ,
	[AUTH] [char](5) ,
	[DAUT] [date],
	[OUTL] [char](2),
	[DELC] [bit] ,
	[DDAT] [date] NULL,
	[DTIM] [char](6) NULL,
	[DWHO] [char](3) NULL,
	[DOUT] [char](2) NULL,
	[TNAM] [char](35) NULL,
	[DFAM] [decimal](9, 2) NULL,
	[LANG] [char](3) NULL
	
)

create table #systemUsersTemp
(
    [ID] [int] NOT NULL,
	[EmployeeCode] [char](3) NOT NULL,
	[Name] [varchar](50) ,
	[Initials] [varchar](5) ,
	[Position] [varchar](20) ,
	[PayrollID] [char](20),
	[Password] [char](5),
	[PasswordExpires] [date],
	[IsSupervisor] [bit],
	[IsManager] [bit],
	[SupervisorPassword] [char](5),
	[SupervisorPwdExpires] [date] ,
	[Outlet] [char](2) ,
	[IsDeleted] [bit] ,
	[DeletedDate] [date] ,
	[DeletedTime] [char](6),
	[DeletedBy] [char](3) ,
	[DeletedWhere] [char](2),
	[TillReceiptName] [char](35) ,
	[DefaultAmount] [decimal](9, 2),
	[LanguageCode] [char](3),
	[SecurityProfileID] [int] 
)

INSERT INTO #sysPassTemp
SELECT T.[EEID],T.[NAME],T.[INIT],T.[POSI],T.[EPAY],T.[PASS],T.[DPAS],T.[SUPV],T.[MANA],T.[AUTH],T.[DAUT],T.[OUTL],T.[DELC],T.[DDAT],T.[DTIM],T.[DWHO],T.[DOUT],T.[TNAM],T.[DFAM],T.[LANG] FROM SYSPAS T

INSERT INTO #systemUsersTemp
SELECT * FROM SystemUsers



    DECLARE @RID INT
	DECLARE @MAXID INT
	SELECT @MAXID = MAX(RID)FROM #sysPassTemp -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID = 1
	DECLARE @EMP_ID CHAR(5)


-- looping the #temp_old from first record to last record.
	WHILE(@RID<=@MAXID)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @EMP_ID = EEID  FROM #sysPassTemp WHERE RID = @RID
			-- if the Primary key exists then update, by taking the values from the #temp_old  
          	IF EXISTS(SELECT 1 FROM #systemUsersTemp WHERE [EmployeeCode]  = @EMP_ID)
				BEGIN
                               
					UPDATE SystemUsers
					SET	
					   [Name] = T.[NAME] ,					   
					   [Initials] = T.[INIT],
	                   [Position] = T.[POSI],
	                   [PayrollID] = T.[EPAY] ,
	                   [Password] = T.[PASS],
	                   [PasswordExpires] = T.[DPAS],
	                   [IsSupervisor] = T.[SUPV],
	                   [IsManager] = T.[MANA] ,
	                   [SupervisorPassword]= T.[AUTH] ,
	                   [SupervisorPwdExpires]= T.[DAUT],
	                   [Outlet]= T.[OUTL] ,
	                   [IsDeleted] = T.[DELC],
	                   [DeletedDate] = T.[DDAT],
	                   [DeletedTime] = T.[DTIM] ,
	                   [DeletedBy] = T.[DWHO],
	                   [DeletedWhere] = T.[DOUT],
	                   [TillReceiptName] = T.[TNAM],
	                   [DefaultAmount] =T.[DFAM],
	                   [LanguageCode] = T.[LANG], 
	                   [SecurityProfileID] = 108
					   					    				    					    										
					FROM #sysPassTemp AS T WHERE SystemUsers.EmployeeCode = @EMP_ID AND T.EEID  = @EMP_ID 

				END
			ELSE -- ELSE INSERT NEW RECORD
				BEGIN
					INSERT INTO SystemUsers ([ID],[EmployeeCode],[Name],[Initials],[Position],[PayrollID],[Password],[PasswordExpires],[IsSupervisor],[IsManager],[SupervisorPassword],[SupervisorPwdExpires],[Outlet],[IsDeleted],[DeletedDate],[DeletedTime],[DeletedBy],[DeletedWhere],[TillReceiptName],[DefaultAmount],[LanguageCode],[SecurityProfileID])
				    SELECT T.[EEID],T.[EEID],T.[NAME],T.[INIT],T.[POSI],T.[EPAY],T.[PASS],T.[DPAS],T.[SUPV],T.[MANA],T.[AUTH],T.[DAUT],T.[OUTL],T.[DELC],T.[DDAT],T.[DTIM],T.[DWHO],T.[DOUT],T.[TNAM],T.[DFAM],T.[LANG],108
					FROM #sysPassTemp  AS T 
					WHERE T.RID = @RID  
				END

			SET @RID = @RID+1 -- increment loop count
		END;


enable trigger SystemUsers.tgSystemUsersInsert on SystemUsers;
enable trigger SystemUsers.tgSystemUsersInsert on SystemUsers;

END;
GO

