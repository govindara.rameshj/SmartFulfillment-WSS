﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEventMasterDealGroup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEventMasterDealGroup'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEventMasterDealGroup] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEventMasterDealGroup'
GO
ALTER PROCEDURE [dbo].[usp_GetEventMasterDealGroup]

  --@EventType    AS VARCHAR(7),
  --@Priority     AS VARCHAR(7),
  --@Skulist      AS VARCHAR(MAX),
  --@SelectedDate AS DATE
    @SelectedDate    date,
    @SkuNumbers      varchar(max)

AS
  BEGIN 
      SELECT numb eventnumber, 
             prio priority, 
             key2 eventkey2, 
             idow timeordayrelated, 
             pric specialprice, 
             edat, 
             buycpn, 
             getcpn 
      FROM   evtmas 
      WHERE  sdat <= @SelectedDate --CONVERT(DATETIME, Getdate(), 3) 
             AND edat >= @SelectedDate --CONVERT(DATETIME, Getdate(), 3) 
             AND TYPE = 'DG' 
             AND idel = 0 
             AND numb IN(SELECT numb eventnumber 
                         FROM   evtdlg 
                         WHERE  numb + dlgn IN ((SELECT DISTINCT numb + dlgn 
                                                 FROM   evtdlg 
                                                 WHERE 
                                key1 IN (SELECT item 
                                         FROM   Udf_splitvarchartotable ( 
                                                @SkuNumbers, ',')) 
                                AND [Type] = 'S') 
                                                UNION 
                                                SELECT DISTINCT numb + dlgn 
                                                FROM   evtmmg 
                                                       INNER JOIN evtdlg 
                                                         ON evtdlg.key1 = 
                                                            evtmmg.mmgn 
                                                            AND 
                                                evtdlg.TYPE = 'M' 
                                                WHERE 
                                skun IN (SELECT item 
                                         FROM   Udf_splitvarchartotable ( 
                                                @SkuNumbers 
                                                , ',')))) 
      UNION
    --SELECT hdr.id        eventnumber,
      select EventNumber      = replicate('0', 6 - len(replace(cast(900000 + hdr.ID as char(6)), ' ', ''))) + replace(cast(900000 + hdr.ID as char(6)), ' ', ''),
             Priority         = cast('00' as char(2)),
             EventKey2        = replicate('0', 8 - len(replace(cast(900000 + hdr.ID as char(8)), ' ', ''))) + replace(cast(900000 + hdr.ID as char(8)), ' ', ''),
             TimeOrDayRelated = cast(1 as bit),
             SpecialPrice     = hdr.Price,
             EDAT             = hdr.EndDate,
             BUYCPN           = cast('0000000' as char(7)),
             GETCPN           = cast('0000000' as char(7))

      from (
            select a.Id,
                   a.Price,
                   a.EndDate
            from EventHeaderOverride a
            inner join EventDetailOverride b
                  on b.EventHeaderOverrideID = a.ID
            --work out header id based max revsion and any sku match
            where a.ID in (
                            select a.ID
                            from (
                                  select ID, Revision, SkuList = dbo.udf_EventDetailOverrideSkuDelimitedList(ID)
                                  from EventHeaderOverride
                                  where EventTypeID = 3
                                  and   StartDate  <= @SelectedDate
                                  and   EndDate    >= @SelectedDate
                                  and   ID         in (
                                                       select EventHeaderOverrideID
                                                       from EventDetailOverride
                                                       where SkuNumber in (select SkuNumber = Item from udf_splitvarchartotable(@SkuNumbers, ','))
                                                      )
                                 ) a
                            inner join (
                                        select SkuList, MaxRevision = max(Revision)
                                        from (
                                              select ID, Revision, SkuList = dbo.udf_EventDetailOverrideSkuDelimitedList(ID)
                                              from EventHeaderOverride
                                              where EventTypeID = 3
                                              and   StartDate  <= @SelectedDate
                                              and   EndDate    >= @SelectedDate
                                              and   ID         in (
                                                                   select EventHeaderOverrideID
                                                                   from EventDetailOverride
                                                                   where SkuNumber in (select SkuNumber = Item from udf_splitvarchartotable(@SkuNumbers, ','))
                                                                  )
                                             ) a
                                        group by SkuList
                                       ) b
                                  on  b.SkuList     = a.SkuList
                                  and b.MaxRevision = a.Revision
                           )
           ) hdr
      order by EventNumber desc

  END
GO

