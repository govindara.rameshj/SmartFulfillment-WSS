﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GapWalkGetItems]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GapWalkGetItems'
	EXEC ('CREATE PROCEDURE [dbo].[GapWalkGetItems] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GapWalkGetItems'
GO
ALTER PROCEDURE [dbo].[GapWalkGetItems]
    @Date date
AS
BEGIN
    SET NOCOUNT ON;

    WITH SkuLastTimeSold (SkuNumber, DateLastSold)
    AS
    (
        select SKUN as SkuNumber, max(d.DATE1) as DateLastSold
        from DLLINE d inner join GapWalk gw on d.skun = gw.SkuNumber
        where d.QUAN > 0 AND d.LREV=0
        and gw.DateCreated = @Date
        group by SKUN
    )
    
    select
        gw.SkuNumber,
        sk.DESCR                        as 'Description',
        sk.ONHA                         as 'QtyOnHand',
        sk.MDNQ                         as 'QtyMarkdown',
        gw.QuantityRequired             as 'QtyRequired',
        ''                              as 'QtyPick',
        coalesce(
            slts.DateLastSold,
            sk.DateLastSold
        )                               as 'DateLastSold',
        sk.DREC                         as 'DateLastReceived',
        ''                              as 'Comment',
        (select case
            when sk.inon=1 then 'Non Stock Products'
            when sk.NOOR=1 then 'Non Orderable Stock'
            when sk.IOBS=1 then 'Obsolete/ Deleted Items'
            when sk.IDEL=1 then 'Obsolete/ Deleted Items'
            else 'Stock - No Restrictions'
        end)                            as 'Status'
    from
        GapWalk gw
    inner join
        STKMAS sk on sk.SKUN=gw.SkuNumber
    left join 
        SkuLastTimeSold slts on slts.SkuNumber=gw.SkuNumber
    where
        gw.DateCreated = @Date
    order by gw.id
END
GO

