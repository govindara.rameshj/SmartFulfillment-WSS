﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ConsignmentGetByNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ConsignmentGetByNumber'
	EXEC ('CREATE PROCEDURE [dbo].[ConsignmentGetByNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ConsignmentGetByNumber'
GO
ALTER PROCEDURE [dbo].[ConsignmentGetByNumber]
	@Number	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	select		
		cm.numb as 'Number',
		cm.pono as 'PoNumber',
		cm.prel as 'ReleaseNumber',
		cm.supp as 'SupplierNumber',
		cm.eeid as 'EmployeeId',
		cm.rcvd as 'PalletsIn',
		cm.rtnd as 'PalletsOut',
		cm.dnot1 as 'DeliveryNote1',
		cm.dnot2 as 'DeliveryNote2',
		cm.dnot3 as 'DeliveryNote3',
		cm.dnot4 as 'DeliveryNote4',
		cm.dnot5 as 'DeliveryNote5',
		cm.dnot6 as 'DeliveryNote6',
		cm.dnot7 as 'DeliveryNote7',
		cm.dnot8 as 'DeliveryNote8',
		cm.dnot9 as 'DeliveryNote9'
	from		
		conmas cm
	where
		cm.numb = @Number

END
GO

