﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODQuoteConversions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure QODQuoteConversions'
	EXEC ('CREATE PROCEDURE [dbo].[QODQuoteConversions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure QODQuoteConversions'
GO
-- =============================================
-- Author:		Mike O'Cain
-- Create date: 07/10/2009
-- Description:	Quote Conversions
-- =============================================
ALTER PROCEDURE [dbo].[QODQuoteConversions] 
	@DateStart datetime = Null, 
	@DateEnd datetime = Null
AS
BEGIN
	Drop Table TMP_QOD_TABLE
	
	SELECT
		 convert(varchar,vc.DELD,103)				as 'DelDate'
		 , DATENAME(dw,vc.DELD)						as 'DelDay'	 
		 , count(qh.NUMB)							as 'QuotesRaised'
		 , Count(vc.Numb)							as 'OrdersRaised'
		 ,(Count(vc.Numb) / Count(qh.Numb) * 100)	as 'PercSold'
		 , SUM(convert(int,vc.DELI))				as 'QuotesDelivered'
	INTO TMP_QOD_TABLE
	--Tables CORHDR has been replaced with the view vwCORHDRFull
	FROM vwCORHDRFull as vc, QUOHDR as qh
	Where vc.SHOW_IN_UI = 1 and vc.DELD >= @DateStart AND vc.DELD <= @DateEnd 
	GROUP BY vc.DELD

   	SELECT  DelDate									as 'Date'
		  ,	DelDay									as 'Day'
		  , QuotesRaised							as 'QuotesRaised'
 		  , OrdersRaised							as 'OrdersRaised'
		  , PercSold								as 'PercSold'
		  , QuotesDelivered							as 'QuotesDelivered'
		  , (QuotesDelivered / QuotesRaised) * 100	as 'PercDel'
	From TMP_QOD_TABLE
END
GO

