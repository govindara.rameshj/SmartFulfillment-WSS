﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSpotCheckList]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSpotCheckList'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSpotCheckList] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSpotCheckList'
GO
ALTER PROCedure [dbo].[NewBankingSpotCheckList]
   @PeriodID Int
As
Begin
    Set NoCount On

    declare @t table (
        ID int identity(1,1),
        PickupID int NULL,
        PickupSealNumber char(20) COLLATE DATABASE_DEFAULT NOT NULL,
        PickupComment char(255) COLLATE DATABASE_DEFAULT NOT NULL,
        StartFloatID int NULL,
        StartFloatSealNumber char(20) COLLATE DATABASE_DEFAULT NULL,
        StartFloatValue decimal(9,2) NULL,
        SaleTaken bit NOT NULL DEFAULT(1),
        CashierID int NOT NULL,
        CashierUserName varchar(50) COLLATE DATABASE_DEFAULT NOT NULL,
        CashierEmployeeCode char(3) COLLATE DATABASE_DEFAULT NOT NULL,
        LoggedOnTillId char(2) COLLATE DATABASE_DEFAULT NULL
    )

    insert into @t
    exec [dbo].[NewBankingFloatedPickupList] @PeriodID

    insert into @t (PickupID, PickupSealNumber, PickupComment, CashierID, CashierUserName, CashierEmployeeCode, LoggedOnTillId)
    exec [dbo].[NewBankingUnFloatedPickupList] @PeriodID

    select
        PickupID,
        PickupSealNumber,
        PickupComment,
        StartFloatID,
        StartFloatSealNumber,
        StartFloatValue,
        SaleTaken,
        CashierID,
        CashierUserName,
        CashierEmployeeCode,
        LoggedOnTillId
    from @t

    Set NoCount Off
End
GO

