﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingCashierFloatVariance]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingCashierFloatVariance'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingCashierFloatVariance] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingCashierFloatVariance'
GO
-- =============================================
-- Author       : Partha Dutta
-- Create date  : 01/06/2011
-- Referral No  : 811
-- Description  : Baseline version
--                Write cashier's float variance to db
-- =============================================

ALTER PROCedure NewBankingCashierFloatVariance
   @PeriodID      int,
   @CashierID     int,
   @CurrencyID    nvarchar(3),
   @FloatVariance decimal(8, 2)
as
set nocount on

update CashBalCashier set FloatVariance = @FloatVariance
where PeriodID      = @PeriodID
and   CashierID     = @CashierID
and   CurrencyID    = @CurrencyID
GO

