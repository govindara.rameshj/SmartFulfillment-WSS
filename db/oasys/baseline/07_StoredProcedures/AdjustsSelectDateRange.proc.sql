﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AdjustsSelectDateRange]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure AdjustsSelectDateRange'
	EXEC ('CREATE PROCEDURE [dbo].[AdjustsSelectDateRange] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure AdjustsSelectDateRange'
GO
ALTER PROCEDURE [dbo].[AdjustsSelectDateRange]
@StartDate	date,
@EndDate	date

as
select		STKADJ.PeriodID,
			STKADJ.DATE1,			
			STKADJ.CODE,
			STKADJ.SKUN,
			STKMAS.DESCR as SkuDescription,		
			STKADJ.SEQN,
			STKADJ.AmendId,			
			STKADJ.SSTK,
			STKADJ.QUAN,			
			SACODE.SECL as CodeSecurity,
			STKADJ.MOWT,
			STKADJ.WAUT,
			STKADJ.DAUT,
			STKADJ.TSKU,
			STKADJ.TransferStart,
			STKADJ.TransferPrice,
			STKADJ.[INIT],
			STKADJ.PRIC,
			STKADJ.COST,
			STKADJ.INFO,
			STKADJ.[TYPE],
			STKADJ.DRLN,
			STKADJ.IsReversed,
			STKADJ.RTI

FROM		STKADJ
inner join	STKMAS on STKADJ.SKUN = STKMAS.SKUN
inner join	SACODE on STKADJ.CODE = SACODE.NUMB
			
WHERE		STKADJ.IsReversed=0
and			STKADJ.AmendId=0
and			STKADJ.DATE1 >= @StartDate
and			STKADJ.DATE1 <= @EndDate

ORDER BY	STKADJ.DATE1 DESC, STKADJ.SKUN, STKADJ.SEQN, STKADJ.AmendId;

select		STKADJ.PeriodID,
			STKADJ.DATE1,			
			STKADJ.CODE,
			STKADJ.SKUN,
			STKMAS.DESCR as SkuDescription,		
			STKADJ.SEQN,
			STKADJ.AmendId,			
			STKADJ.SSTK,
			STKADJ.QUAN,				
			SACODE.SECL as CodeSecurity,
			STKADJ.MOWT,
			STKADJ.WAUT,
			STKADJ.DAUT,
			STKADJ.TSKU,
			STKADJ.TransferStart,
			STKADJ.TransferPrice,
			STKADJ.[INIT],
			STKADJ.PRIC,
			STKADJ.COST,
			STKADJ.INFO,
			STKADJ.[TYPE],
			STKADJ.DRLN,
			STKADJ.IsReversed,
			STKADJ.RTI

FROM		STKADJ
inner join	STKMAS on STKADJ.SKUN = STKMAS.SKUN
inner join	SACODE on STKADJ.CODE = SACODE.NUMB
			
WHERE		STKADJ.IsReversed=0
and			STKADJ.AmendId<>0
and			STKADJ.DATE1 >= @StartDate
and			STKADJ.DATE1 <= @EndDate

ORDER BY	STKADJ.DATE1 DESC, STKADJ.SKUN, STKADJ.SEQN, STKADJ.AmendId;
GO

