﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptGetOverVarianceTypes]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptGetOverVarianceTypes'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptGetOverVarianceTypes] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptGetOverVarianceTypes'
GO
ALTER PROCEDURE [dbo].[ReceiptGetOverVarianceTypes]

AS
BEGIN
	declare @OutputTable TABLE ( Id char(1), Display char(10) )

	insert into @OutputTable values ('S', 'Short');
	insert into @OutputTable values ('O', 'Over');
	
	select * from @OutputTable
	
END
GO

