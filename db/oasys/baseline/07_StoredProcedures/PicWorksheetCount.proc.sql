﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PicWorksheetCount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PicWorksheetCount'
	EXEC ('CREATE PROCEDURE [dbo].[PicWorksheetCount] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PicWorksheetCount'
GO
ALTER PROCEDURE [dbo].[PicWorksheetCount]
@Date DATE
AS
BEGIN
	SET NOCOUNT ON;

	select
		hd.DATE1			as 'Date',
		hd.SKUN				as 'SkuNumber',
		sk.DESCR			as 'Description',
		sk.PACK				as 'PackSize',		
		(case sk.IRIS when 1 then (select rl.PPOS from RELITM rl where rl.SPOS=sk.SKUN) end) as 'SkuRelated',
		''					as 'QtyShopFloor',
		''					as 'QtyWarehouse',
		''					as 'QtyTotal',
		''					as 'QtyPreSold',
		hd.ONHA				as 'QtyOnHand',
		''					as 'QtyVariance',
		sk.PRIC				as 'Price',
		'YES / NO'			as 'SELYesNo'
	from
		HHTDET hd
	inner join
		STKMAS sk	on sk.SKUN = hd.SKUN
	where
		hd.DATE1 = @Date
	order by
		sk.SKUN

END
GO

