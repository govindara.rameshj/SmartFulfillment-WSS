﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_BankingExtract]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_BankingExtract'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_BankingExtract] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_BankingExtract'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_BankingExtract]
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 25th September 2012
-- 
-- Task     : Extract Banking Data.
-----------------------------------------------------------------------------------
-- Revision	: 1.1
-- Author	: Kevan Madelin
-- Date		: 26th September 2012
-- Notes	: Added Summary Option as Requested by Jim Bunce
-----------------------------------------------------------------------------------
-- Revision : 1.2
-- Author   : Kevan Madelin
-- Date     : 27th September 2012
-- Notes    : Added Literal Database Output for Analysis
-----------------------------------------------------------------------------------
	
---------------------------------------------------------------------------
-- Declare Parameters
---------------------------------------------------------------------------
@sp_Period			int,
@sp_Report			char(1) = 'D',
@sp_Comma			char(1) = 'N'

AS
BEGIN
SET NOCOUNT ON;

if @Sp_Report in ('L','H')
	Begin
		-- Line and Header Detail (as per CashBalCashier* Tables

		if @Sp_Comma = 'N' and @sp_Report = 'H'
			Begin
					Select	(Select dbo.svf_SystemStoreGroupID()), cc.CurrencyID, 
								cc.CashierID,su.Name,cc.GrossSalesAmount,
								cc.DiscountAmount,cc.SalesCount,cc.SalesAmount,
								cc.SalesCorrectCount,cc.SalesCorrectAmount,cc.RefundCount,
								cc.RefundAmount,cc.RefundCorrectCount,cc.RefundCorrectAmount,
								cc.FloatIssued,cc.FloatReturned,cc.MiscIncomeCount01,
								cc.MiscIncomeCount02,cc.MiscIncomeCount03,cc.MiscIncomeCount04,
								cc.MiscIncomeCount05,cc.MiscIncomeCount06,cc.MiscIncomeCount07,
								cc.MiscIncomeCount08,cc.MiscIncomeCount09,cc.MiscIncomeCount10,
								cc.MiscIncomeCount11,cc.MiscIncomeCount12,cc.MiscIncomeCount13,
								cc.MiscIncomeCount14,cc.MiscIncomeCount15,cc.MiscIncomeCount16,
								cc.MiscIncomeCount17,cc.MiscIncomeCount18,cc.MiscIncomeCount19,
								cc.MiscIncomeCount20,cc.MiscIncomeValue01,cc.MiscIncomeValue02,
								cc.MiscIncomeValue03,cc.MiscIncomeValue04,cc.MiscIncomeValue05,
								cc.MiscIncomeValue06,cc.MiscIncomeValue07,cc.MiscIncomeValue08,
								cc.MiscIncomeValue09,cc.MiscIncomeValue10,cc.MiscIncomeValue11,
								cc.MiscIncomeValue12,cc.MiscIncomeValue13,cc.MiscIncomeValue14,
								cc.MiscIncomeValue15,cc.MiscIncomeValue16,cc.MiscIncomeValue17,
								cc.MiscIncomeValue18,cc.MiscIncomeValue19,cc.MiscIncomeValue20,
								cc.MisOutCount01,
								cc.MisOutCount02,cc.MisOutCount03,cc.MisOutCount04,
								cc.MisOutCount05,cc.MisOutCount06,cc.MisOutCount07,
								cc.MisOutCount08,cc.MisOutCount09,cc.MisOutCount10,
								cc.MisOutCount11,cc.MisOutCount12,cc.MisOutCount13,
								cc.MisOutCount14,cc.MisOutCount15,cc.MisOutCount16,
								cc.MisOutCount17,cc.MisOutCount18,cc.MisOutCount19,
								cc.MisOutCount20,cc.MisOutValue01,cc.MisOutValue02,
								cc.MisOutValue03,cc.MisOutValue04,cc.MisOutValue05,
								cc.MisOutValue06,cc.MisOutValue07,cc.MisOutValue08,
								cc.MisOutValue09,cc.MisOutValue10,cc.MisOutValue11,
								cc.MisOutValue12,cc.MisOutValue13,cc.MisOutValue14,
								cc.MisOutValue15,cc.MisOutValue16,cc.MisOutValue17,
								cc.MisOutValue18,cc.MisOutValue19,cc.MisOutValue20,
								cc.ExchangeRate,cc.ExchangePower,cc.NumTransactions,
								cc.NumCorrections,cc.NumVoids,cc.NumOpenDrawer,
								cc.NumLinesReversed,cc.NumLinesSold,cc.NumLinesScanned,
								cc.FloatVariance
					From	CashBalCashier as cc
					Inner Join SystemUsers as su on su.ID = cc.CashierID 
					Where	cc.PeriodID = @sp_Period
			End	
	
		if @Sp_Comma = 'Y' and @sp_Report = 'H'
			Begin
					Select	(Select dbo.svf_SystemStoreGroupID()),',', cc.CurrencyID,',', 
								cc.CashierID,',',su.Name,',',cc.GrossSalesAmount,',',
								cc.DiscountAmount,',',cc.SalesCount,',',cc.SalesAmount,',',
								cc.SalesCorrectCount,',',cc.SalesCorrectAmount,',',cc.RefundCount,',',
								cc.RefundAmount,',',cc.RefundCorrectCount,',',cc.RefundCorrectAmount,',',
								cc.FloatIssued,',',cc.FloatReturned,',',cc.MiscIncomeCount01,',',
								cc.MiscIncomeCount02,',',cc.MiscIncomeCount03,',',cc.MiscIncomeCount04,',',
								cc.MiscIncomeCount05,',',cc.MiscIncomeCount06,',',cc.MiscIncomeCount07,',',
								cc.MiscIncomeCount08,',',cc.MiscIncomeCount09,',',cc.MiscIncomeCount10,',',
								cc.MiscIncomeCount11,',',cc.MiscIncomeCount12,',',cc.MiscIncomeCount13,',',
								cc.MiscIncomeCount14,',',cc.MiscIncomeCount15,',',cc.MiscIncomeCount16,',',
								cc.MiscIncomeCount17,',',cc.MiscIncomeCount18,',',cc.MiscIncomeCount19,',',
								cc.MiscIncomeCount20,',',cc.MiscIncomeValue01,',',cc.MiscIncomeValue02,',',
								cc.MiscIncomeValue03,',',cc.MiscIncomeValue04,',',cc.MiscIncomeValue05,',',
								cc.MiscIncomeValue06,',',cc.MiscIncomeValue07,',',cc.MiscIncomeValue08,',',
								cc.MiscIncomeValue09,',',cc.MiscIncomeValue10,',',cc.MiscIncomeValue11,',',
								cc.MiscIncomeValue12,',',cc.MiscIncomeValue13,',',cc.MiscIncomeValue14,',',
								cc.MiscIncomeValue15,',',cc.MiscIncomeValue16,',',cc.MiscIncomeValue17,',',
								cc.MiscIncomeValue18,',',cc.MiscIncomeValue19,',',cc.MiscIncomeValue20,',',
								cc.MisOutCount01,',',
								cc.MisOutCount02,',',cc.MisOutCount03,',',cc.MisOutCount04,',',
								cc.MisOutCount05,',',cc.MisOutCount06,',',cc.MisOutCount07,',',
								cc.MisOutCount08,',',cc.MisOutCount09,',',cc.MisOutCount10,',',
								cc.MisOutCount11,',',cc.MisOutCount12,',',cc.MisOutCount13,',',
								cc.MisOutCount14,',',cc.MisOutCount15,',',cc.MisOutCount16,',',
								cc.MisOutCount17,',',cc.MisOutCount18,',',cc.MisOutCount19,',',
								cc.MisOutCount20,',',cc.MisOutValue01,',',cc.MisOutValue02,',',
								cc.MisOutValue03,',',cc.MisOutValue04,',',cc.MisOutValue05,',',
								cc.MisOutValue06,',',cc.MisOutValue07,',',cc.MisOutValue08,',',
								cc.MisOutValue09,',',cc.MisOutValue10,',',cc.MisOutValue11,',',
								cc.MisOutValue12,',',cc.MisOutValue13,',',cc.MisOutValue14,',',
								cc.MisOutValue15,',',cc.MisOutValue16,',',cc.MisOutValue17,',',
								cc.MisOutValue18,',',cc.MisOutValue19,',',cc.MisOutValue20,',',
								cc.ExchangeRate,',',cc.ExchangePower,',',cc.NumTransactions,',',
								cc.NumCorrections,',',cc.NumVoids,',',cc.NumOpenDrawer,',',
								cc.NumLinesReversed,',',cc.NumLinesSold,',',cc.NumLinesScanned,',',
								cc.FloatVariance
					From	CashBalCashier as cc
					Inner Join SystemUsers as su on su.ID = cc.CashierID 
					Where	cc.PeriodID = @sp_Period
			End	
	
		if @Sp_Comma = 'N' and @sp_Report = 'L'
			Begin
					Select	(Select dbo.svf_SystemStoreGroupID()), 
								LTRIM(RTRIM(ct.CashierID)),LTRIM(RTRIM(su.Name)),LTRIM(RTRIM(ct.CurrencyID)),
								LTRIM(RTRIM(ct.ID)),LTRIM(RTRIM(Quantity)),LTRIM(RTRIM(Amount)),
								LTRIM(RTRIM(ct.Pickup))
					From	CashBalCashierTen as ct
					Inner Join SystemUsers as su on su.ID = ct.CashierID 
					Where	ct.PeriodID = @sp_Period
			End		
		
		if @Sp_Comma = 'Y' and @sp_Report = 'L'
			Begin
					Select	(Select dbo.svf_SystemStoreGroupID()),',', 
								LTRIM(RTRIM(ct.CashierID)),',',LTRIM(RTRIM(su.Name)),',',LTRIM(RTRIM(ct.CurrencyID)),',',
								LTRIM(RTRIM(ct.ID)),',',LTRIM(RTRIM(Quantity)),',',LTRIM(RTRIM(Amount)),',',
								LTRIM(RTRIM(ct.Pickup))
					From	CashBalCashierTen as ct
					Inner Join SystemUsers as su on su.ID = ct.CashierID 
					Where	ct.PeriodID = @sp_Period
			End
	End
else
	Begin

-- Version Control --------------------------------------------------------------------------
-- 1.0 - Kevan Madelin - 25/09/2012 - Initial Data Extract Design
---------------------------------------------------------------------------------------------

Declare @PeriodID int = @Sp_Period
Declare @Report char(1) = @Sp_Report
Declare @Comma char(1) = @Sp_Comma
---------------------------------------------------------------------------------------------
-- Get Cashier List and Build Basic Report Output
---------------------------------------------------------------------------------------------
If Exists (Select Table_Name From Information_Schema.Tables Where Table_Name='#TempWCL') Drop Table #TempWCL
If Exists (Select Table_Name From Information_Schema.Tables Where Table_Name='#TempWorkingCashiers') Drop Table #TempWorkingCashiers

-- Create the List of Cashiers to Generate Report
Create Table #TempWorkingCashiers
(
	[ID] [int] IDENTITY(1,1),
	[Cashier] [int] NOT NULL,
	[Float] [int] NOT NULL
)
Insert into #TempWorkingCashiers
Select Distinct CashierID, 
		FloatIssued = case
			When FloatIssued <> '0.00' Then 1
			Else 0
		End
From CashBalCashier Where PeriodID = @PeriodID and GrossSalesAmount <> '0.00'

---------------------------------------------------------------------------------------------
-- Create the Basic Reporting Table
---------------------------------------------------------------------------------------------
Create Table #TempWCL
(
	[ID] [int] IDENTITY(1,1),
	[Store] [int] NOT NULL,
	[Store_Name] [char](50) NULL,
	[Cashier] [int] NOT NULL,
	[Cashier_Name] [char](50) NULL,
	[Tender_ID] [int] NOT NULL,
	[Tender_Name] [char](20) NULL,
	[Tender_Quantity] [int] NULL,
	[BankingSystem_Value] [numeric](9,2) NULL,
	[BankingPickup_Keyed] [numeric](9,2) NULL,
	[Float_Issued] [numeric] (9,2) NULL,
	[Float_Returned] [numeric] (9,2) NULL,
	[DLTOTS_Sales] [numeric] (9,2) NULL,
	[BankingVariance] [numeric] (9,2) NULL
)

---------------------------------------------------------------------------------------------
-- Produce Basic Records based on Active Cashiers
---------------------------------------------------------------------------------------------
Declare @WorkingCashier_ID int
Declare @WorkingCashier_MAXID int
Select  @WorkingCashier_MAXID = MAX(ID)FROM #TempWorkingCashiers
Set		@WorkingCashier_ID = 1
Declare @WorkingCashier_CashierNumber int
Declare @Store char(4) = (Select '8' + LTRIM(RTRIM(STOR)) From RETOPT Where FKEY = '01')
Declare @Store_Name char(50) = (Select LTRIM(RTRIM(SNAM)) From RETOPT Where FKEY = '01') 
  
While	(@WorkingCashier_ID <= @WorkingCashier_MAXID)
	Begin
				         
		Set @WorkingCashier_CashierNumber = (Select Cashier FROM #TempWorkingCashiers WHERE ID = @WorkingCashier_ID)
		
		Insert into #TempWCL
		Select	@Store as 'Store',
				@Store_Name as 'Store_Name',
				CashierID as 'Cashier',
				'' as 'Cashier_Name',
				ID as 'Tender_ID',
				'' as 'Tender_Name',
				Quantity as 'Tender_Quantity',
				CAST(Amount as numeric(9,2)) as 'BankingSystem_Value',
				CAST(Pickup as numeric(9,2)) as 'BankingPickup_Keyed',
				'0.00' as 'Float_Issued',
				'0.00' as 'Float_Returned',
				'0.00' as 'DLTOTS_Sales',
				'0.00' as 'BankingVariance'
				From	CashBalCashierTen 
				Where	PeriodId = @PeriodID and CashierID = @WorkingCashier_CashierNumber and ID < 99
				
		-- Combine Change & Cash 		
		Declare @NinetyNine numeric(9,2)
		Set @NinetyNine = (Select Amount From CashBalCashierTen Where PeriodId = @PeriodID and CashierID = @WorkingCashier_CashierNumber and ID = 99)
		if @NinetyNine is null set @NinetyNine = '0.00'
		if @NinetyNine <> '0.00'
			Begin		
				Update	#TempWCL 
				Set		BankingSystem_Value = BankingSystem_Value + (Select Amount From CashBalCashierTen Where PeriodId = @PeriodID and CashierID = @WorkingCashier_CashierNumber and ID = 99)
				Where	Cashier = @WorkingCashier_CashierNumber and Tender_ID = 1
			End
	    
	    Insert into #TempWCL
	    Select	@Store as 'Store',
				@Store_Name as 'Store_Name',
				@WorkingCashier_CashierNumber as 'Cashier',
				'TOTAL - Cashier ' + CAST(@WorkingCashier_CashierNumber as CHAR) as 'Cashier_Name',
				'0' as 'Tender_ID',
				'TOTAL' as 'Tender_Name',
				'0' as 'Tender_Quantity',
				'0.00' as 'BankingSystem_Value',
				'0.00' as 'BankingPickup_Keyed',
				'0.00' as 'Float_Issued',
				'0.00' as 'Float_Returned',
				'0.00' as 'DLTOTS_Sales',
				'0.00' as 'BankingVariance'
	    
	    Set @WorkingCashier_ID = @WorkingCashier_ID+1    
	End


---------------------------------------------------------------------------------------------
-- Update Data Calculations
---------------------------------------------------------------------------------------------
Declare @WCL_ID int
Declare @WCL_MAXID int
Select  @WCL_MAXID = MAX(ID)FROM #TempWCL
Set		@WCL_ID = 1
Declare @WCL_Cashier int 
Declare @WCL_BankingSystem_Value numeric(9,2) 
Declare @WCL_BankingPickup_Keyed numeric(9,2) 
Declare @WCL_Tender_ID int
Declare @WCL_Float_Issued numeric(9,2)
Declare @WCL_Float_Returned numeric(9,2)
Declare @WCL_DLTOTS_Sales numeric(9,2)
Declare @WCL_BankingVariance numeric(9,2)
  
While	(@WCL_ID <= @WCL_MAXID)
	Begin
				         
		Set @WCL_Cashier = (Select Cashier FROM #TempWCL WHERE ID = @WCL_ID)
		Set @WCL_Tender_ID = (Select Tender_ID FROM #TempWCL WHERE ID = @WCL_ID)
		Set @WCL_BankingSystem_Value = (Select BankingSystem_Value FROM #TempWCL WHERE ID = @WCL_ID)
		Set @WCL_BankingPickup_Keyed = (Select BankingPickup_Keyed FROM #TempWCL WHERE ID = @WCL_ID)
		Set @WCL_DLTOTS_Sales = (Select SUM(dp.AMNT*-1) From DLPAID as dp inner join DLTOTS as dt on dt.DATE1 = dp.DATE1 and dt.TILL = dp.TILL and dt.[TRAN] = dp.[TRAN] Where dp.DATE1 = (Select CAST(SUBSTRING(CAST(StartDate as CHAR),0,12) as DATE) From SystemPeriods Where ID = @PeriodID) and dt.VOID = '0' and dt.TMOD = '0' and dt.PARK = '0' and dt.CASH = @WCL_Cashier and dp.[TYPE] = @WCL_Tender_ID)
		-- Float Issued Information (if any)
		if @WCL_Tender_ID = 1
			Begin
				Set @WCL_Float_Issued = (Select FloatIssued FROM CashBalCashier WHERE PeriodID = @PeriodID and CashierID = @WCL_Cashier)
			End
		else
			Begin
				Set @WCL_Float_Issued = '0.00'
			End
		
		-- Float Returned Information (if any)
		if @WCL_Tender_ID = 1
			Begin	
				Set @WCL_Float_Returned = (Select (FloatIssued + FloatVariance) FROM CashBalCashier WHERE PeriodID = @PeriodID and CashierID = @WCL_Cashier)
			End
		else
			Begin
				Set @WCL_Float_Returned = '0.00'
			End
		
		-- Workout Variance!!!
		Declare @WCL_Banking_TMP_SystemFigure numeric(9,2)
		Set @WCL_Banking_TMP_SystemFigure = (@WCL_BankingSystem_Value + @WCL_Float_Issued)
		Set @WCL_BankingVariance = (@WCL_BankingPickup_Keyed - @WCL_Banking_TMP_SystemFigure)
		
		Update #TempWCL
		Set		Float_Issued = @WCL_Float_Issued,
				Float_Returned = @WCL_Float_Returned,
				DLTOTS_Sales = @WCL_DLTOTS_Sales,
				BankingVariance = @WCL_BankingVariance	
		Where	ID = @WCL_ID
		
		if @WCL_Tender_ID = '0'
			Begin
				Update #TempWCL
		
				Set		Tender_Quantity = (Select SUM(Tender_Quantity) From #TempWCL Where Cashier = @WCL_Cashier and Tender_ID <> 0),
						BankingSystem_Value = (Select SUM(BankingSystem_Value) From #TempWCL Where Cashier = @WCL_Cashier and Tender_ID <> 0),
						BankingPickup_Keyed = (Select SUM(BankingPickup_Keyed) From #TempWCL Where Cashier = @WCL_Cashier and Tender_ID <> 0),
						Float_Issued = (Select SUM(Float_Issued) From #TempWCL Where Cashier = @WCL_Cashier and Tender_ID <> 0),
						Float_Returned = (Select SUM(Float_Returned) From #TempWCL Where Cashier = @WCL_Cashier and Tender_ID <> 0),
						DLTOTS_Sales = (Select SUM(DLTOTS_Sales) From #TempWCL Where Cashier = @WCL_Cashier and Tender_ID <> 0),
						BankingVariance = (Select SUM(BankingVariance) From #TempWCL Where Cashier = @WCL_Cashier and Tender_ID <> 0)

				Where	ID = @WCL_ID
			End
			
				
	    Set @WCL_ID = @WCL_ID+1 
	End
			
---------------------------------------------------------------------------------------------
-- Insert Blank Tender Records
---------------------------------------------------------------------------------------------
if @Report = 'S'
	Begin
	    Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','1' as 'Tender_ID','' as 'Tender_Name','0' as 'Tender_Quantity','0.00' as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','2' as 'Tender_ID','' as 'Tender_Name','0' as 'Tender_Quantity','0.00' as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','3' as 'Tender_ID','' as 'Tender_Name','0' as 'Tender_Quantity','0.00' as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','4' as 'Tender_ID','' as 'Tender_Name','0' as 'Tender_Quantity','0.00' as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','5' as 'Tender_ID','' as 'Tender_Name','0' as 'Tender_Quantity','0.00' as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','6' as 'Tender_ID','' as 'Tender_Name','0' as 'Tender_Quantity','0.00' as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','7' as 'Tender_ID','' as 'Tender_Name','0' as 'Tender_Quantity','0.00' as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','8' as 'Tender_ID','' as 'Tender_Name',0 as 'Tender_Quantity',0.00 as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','9' as 'Tender_ID','' as 'Tender_Name',0 as 'Tender_Quantity',0.00 as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
		Insert into #TempWCL Select	@Store as 'Store',@Store_Name as 'Store_Name',@Store as 'Cashier','Store' as 'Cashier_Name','10' as 'Tender_ID','' as 'Tender_Name',0 as 'Tender_Quantity',0.00 as 'BankingSystem_Value','0.00' as 'BankingPickup_Keyed','0.00' as 'Float_Issued','0.00' as 'Float_Returned','0.00' as 'DLTOTS_Sales','0.00' as 'BankingVariance'		
	End	

---------------------------------------------------------------------------------------------
-- Insert Blank Store Record
---------------------------------------------------------------------------------------------

	    Insert into #TempWCL
	    Select	@Store as 'Store',
				@Store_Name as 'Store_Name',
				@Store as 'Cashier',
				'TOTAL - Store ' + CAST(LTRIM(RTRIM(@Store)) as CHAR) as 'Cashier_Name',
				CAST(@Store as int) as 'Tender_ID',
				'TOTAL' as 'Tender_Name',
				'0' as 'Tender_Quantity',
				'0.00' as 'BankingSystem_Value',
				'0.00' as 'BankingPickup_Keyed',
				'0.00' as 'Float_Issued',
				'0.00' as 'Float_Returned',
				'0.00' as 'DLTOTS_Sales',
				'0.00' as 'BankingVariance'
					
---------------------------------------------------------------------------------------------
-- Update Report Data (Summary Option)
---------------------------------------------------------------------------------------------
if @Report = 'S'
	Begin
		Declare @Summary_WCL_ID int
		Declare @Summary_WCL_MAXID int
		Select  @Summary_WCL_MAXID = MAX(ID)FROM #TempWCL
		Set		@Summary_WCL_ID = 1
		Declare @Summary_WCL_CashierID int 
		Declare @Summary_WCL_Tender_ID int
		Declare @Summary_WCL_Tender_Name char(20)
		  
		While	(@Summary_WCL_ID <= @Summary_WCL_MAXID)
			Begin
				
				Set @Summary_WCL_CashierID = (Select Cashier From #TempWCL Where ID = @Summary_WCL_ID)  
				Set @Summary_WCL_Tender_ID = (Select Tender_ID From #TempWCL Where ID = @Summary_WCL_ID)
				Set @Summary_WCL_Tender_Name = (Select 
												case Tender_ID
														When 0 Then ''
														When 1 Then 'Cash'
														When 2 Then 'Cheque'
														When 3 Then 'Access / VISA'
														When 4 Then 'Old Token'
														When 5 Then 'Project Loan'
														When 6 Then 'Voucher'
														When 7 Then 'Gift Token'
														When 8 Then 'Maestro'
														When 9 Then 'American Express'
														When 10 Then 'Head Office Cheque'
													End
													FROM #TempWCL WHERE ID = @Summary_WCL_ID)
			
				if @Summary_WCL_CashierID = @Store and @Summary_WCL_Tender_ID != @Store
					Begin			
						Update #TempWCL
				
						Set		Tender_Name = @Summary_WCL_Tender_Name,
								Tender_Quantity = (Select SUM(Tender_Quantity) From #TempWCL Where Tender_ID = @Summary_WCL_Tender_ID and Cashier < '500'),
								BankingSystem_Value = (Select SUM(BankingSystem_Value) From #TempWCL Where Tender_ID = @Summary_WCL_Tender_ID and Cashier < '500'),
								BankingPickup_Keyed = (Select SUM(BankingPickup_Keyed) From #TempWCL Where Tender_ID = @Summary_WCL_Tender_ID and Cashier < '500'),
								Float_Issued = (Select SUM(Float_Issued) From #TempWCL Where Tender_ID = @Summary_WCL_Tender_ID and Cashier < '500'),
								Float_Returned = (Select SUM(Float_Returned) From #TempWCL Where Tender_ID = @Summary_WCL_Tender_ID and Cashier < '500'),
								DLTOTS_Sales = (Select SUM(DLTOTS_Sales) From #TempWCL Where Tender_ID = @Summary_WCL_Tender_ID and Cashier < '500'),
								BankingVariance = (Select SUM(BankingVariance) From #TempWCL Where Tender_ID = @Summary_WCL_Tender_ID and Cashier < '500')

						Where	ID = @Summary_WCL_ID and Cashier = @Summary_WCL_CashierID and Tender_ID = @Summary_WCL_Tender_ID
					End
								
				Set @Summary_WCL_ID = @Summary_WCL_ID+1 
			End
	End

---------------------------------------------------------------------------------------------
-- Update Final Data Clean-Up
---------------------------------------------------------------------------------------------
Declare @Final_WCL_ID int
Declare @Final_WCL_MAXID int
Select  @Final_WCL_MAXID = MAX(ID)FROM #TempWCL
Set		@Final_WCL_ID = 1
Declare @Final_WCL_Cashier char(50) 
Declare @Final_WCL_Name char(50)
Declare @Final_WCL_CashierID int 
Declare @Final_WCL_Tender_ID int
Declare @Final_WCL_Tender_Name char(20)
-- Data Tidy
Declare @Final_WCL_Tender_Quantity int
Declare @Final_WCL_BankingSystem_Value numeric(9,2)
Declare @Final_WCL_BankingPickup_Keyed numeric(9,2)
Declare @Final_WCL_Float_Issued numeric(9,2)
Declare @Final_WCL_Float_Returned numeric(9,2)
Declare @Final_WCL_DLTOTS_Sales numeric(9,2)
Declare @Final_WCL_BankingVariance numeric(9,2)

  
While	(@Final_WCL_ID <= @Final_WCL_MAXID)
	Begin
		
		Set @Final_WCL_CashierID = (Select Cashier From #TempWCL Where ID = @Final_WCL_ID) 		         
		Set @Final_WCL_Name = (Select Cashier_Name From #TempWCL Where ID = @Final_WCL_ID) 
		Set @Final_WCL_Tender_ID = (Select Tender_ID From #TempWCL Where ID = @Final_WCL_ID)
		
		Set @Final_WCL_Tender_Quantity = (Select Tender_Quantity From #TempWCL Where ID = @Final_WCL_ID) 
		Set @Final_WCL_BankingSystem_Value = (Select BankingSystem_Value From #TempWCL Where ID = @Final_WCL_ID) 
		Set @Final_WCL_BankingPickup_Keyed = (Select BankingPickup_Keyed From #TempWCL Where ID = @Final_WCL_ID) 
		Set @Final_WCL_Float_Issued = (Select Float_Issued From #TempWCL Where ID = @Final_WCL_ID) 
		Set @Final_WCL_Float_Returned = (Select Float_Returned From #TempWCL Where ID = @Final_WCL_ID) 
		Set @Final_WCL_DLTOTS_Sales = (Select DLTOTS_Sales From #TempWCL Where ID = @Final_WCL_ID) 
		Set @Final_WCL_BankingVariance = (Select BankingVariance From #TempWCL Where ID = @Final_WCL_ID) 
				
		if @Final_WCL_Name = ''
			Begin
				Set @Final_WCL_Cashier = (Select LTRIM(RTRIM(Name)) FROM SystemUsers WHERE ID = @Final_WCL_CashierID)
			End
		else
			Begin
				Set @Final_WCL_Cashier = @Final_WCL_Name
			End
		Set @Final_WCL_Tender_Name = (Select 
										case Tender_ID
												When 0 Then ''
												When 1 Then 'Cash'
												When 2 Then 'Cheque'
												When 3 Then 'Access / VISA'
												When 4 Then 'Old Token'
												When 5 Then 'Project Loan'
												When 6 Then 'Voucher'
												When 7 Then 'Gift Token'
												When 8 Then 'Maestro'
												When 9 Then 'American Express'
												When 10 Then 'Head Office Cheque'
												else ''
											End
											FROM #TempWCL WHERE ID = @Final_WCL_ID)
		
		-- Ensure No NULLS!
		If @Final_WCL_Tender_Quantity is null Set @Final_WCL_Tender_Quantity = 0
		If @Final_WCL_BankingSystem_Value is null Set @Final_WCL_BankingSystem_Value = 0.00
		If @Final_WCL_BankingPickup_Keyed is null Set @Final_WCL_BankingPickup_Keyed = 0.00
		If @Final_WCL_Float_Issued is null Set @Final_WCL_Float_Issued = 0.00
		If @Final_WCL_Float_Returned is null Set @Final_WCL_Float_Returned = 0.00
		If @Final_WCL_DLTOTS_Sales is null Set @Final_WCL_DLTOTS_Sales = 0.00
		If @Final_WCL_BankingVariance is null Set @Final_WCL_BankingVariance = 0.00
				
		Update	#TempWCL
		Set		Cashier_Name = @Final_WCL_Cashier,
				Tender_Name = @Final_WCL_Tender_Name,
				Tender_Quantity = @Final_WCL_Tender_Quantity,
				BankingSystem_Value = @Final_WCL_BankingSystem_Value,
				BankingPickup_Keyed = @Final_WCL_BankingPickup_Keyed,
				Float_Issued = @Final_WCL_Float_Issued,
				Float_Returned = @Final_WCL_Float_Returned,
				DLTOTS_Sales = @Final_WCL_DLTOTS_Sales,
				BankingVariance = @Final_WCL_BankingVariance
		Where	ID = @Final_WCL_ID
				
		if @Final_WCL_Tender_ID = @Store
			Begin
				Update #TempWCL
		
				Set		Tender_Quantity = (Select SUM(Tender_Quantity) From #TempWCL Where  Tender_ID = 0),
						BankingSystem_Value = (Select SUM(BankingSystem_Value) From #TempWCL Where  Tender_ID = 0),
						BankingPickup_Keyed = (Select SUM(BankingPickup_Keyed) From #TempWCL Where Tender_ID = 0),
						Float_Issued = (Select SUM(Float_Issued) From #TempWCL Where Tender_ID = 0),
						Float_Returned = (Select SUM(Float_Returned) From #TempWCL Where Tender_ID = 0),
						DLTOTS_Sales = (Select SUM(DLTOTS_Sales) From #TempWCL Where Tender_ID = 0),
						BankingVariance = (Select SUM(BankingVariance) From #TempWCL Where Tender_ID = 0)

				Where	ID = @Final_WCL_ID
			End
				
	    Set @Final_WCL_ID = @Final_WCL_ID+1 
	End

---------------------------------------------------------------------------------------------
-- Return Data
---------------------------------------------------------------------------------------------
if @Comma not in ('Y','N') or @Report not in ('D','S')	
	Begin
		Print 'Error Incorrect Parameters Provided'
		--Select 'Error Incorrect Parameters Provided' goto ExitScript
	End
Else
	Begin
		if @Comma = 'N' and @Report = 'D'
				Begin
						Select	Store,Store_Name,Cashier,Cashier_Name,Tender_ID,Tender_Name,Tender_Quantity,BankingSystem_Value,BankingPickup_Keyed,Float_Issued,Float_Returned,DLTOTS_Sales,BankingVariance
						From	#TempWCL 
						--Order By Store, Cashier asc
				End
				
		if @Comma = 'N' and @Report = 'S'		
				Begin
						Select	Store,Store_Name,Cashier,Cashier_Name,Tender_ID,Tender_Name,Tender_Quantity,BankingSystem_Value,BankingPickup_Keyed,Float_Issued,Float_Returned,DLTOTS_Sales,BankingVariance
						From	#TempWCL 
						Where	Cashier = @Store
				End

		if @Comma = 'Y' and @Report = 'D'
				Begin
					Select	Store,',',LTRIM(RTRIM(Store_Name)),',',Cashier,',',LTRIM(RTRIM(Cashier_Name)),',',Tender_ID,',',LTRIM(RTRIM(Tender_Name)),',',
							LTRIM(RTRIM(Tender_Quantity)),',',LTRIM(RTRIM(BankingSystem_Value)),',',LTRIM(RTRIM(BankingPickup_Keyed)),',',
							LTRIM(RTRIM(Float_Issued)),',',LTRIM(RTRIM(Float_Returned)),',',LTRIM(RTRIM(DLTOTS_Sales)),',',LTRIM(RTRIM(BankingVariance))
					From	#TempWCL 
					--Order By Store, Cashier asc
				End
				
		if @Comma = 'Y' and @Report = 'S'	
				Begin	
					Select	Store,',', 
								LTRIM(RTRIM(Store_Name)),',',Cashier,',',LTRIM(RTRIM(Cashier_Name)),',',Tender_ID,',',LTRIM(RTRIM(Tender_Name)),',',LTRIM(RTRIM(Tender_Quantity)),',',LTRIM(RTRIM(BankingSystem_Value)),',',
								LTRIM(RTRIM(BankingPickup_Keyed)),',',LTRIM(RTRIM(Float_Issued)),',',LTRIM(RTRIM(Float_Returned)),',',LTRIM(RTRIM(DLTOTS_Sales)),',',LTRIM(RTRIM(BankingVariance))
					From	#TempWCL 
					Where	Cashier = @Store
				End
	End
End	

END
GO

