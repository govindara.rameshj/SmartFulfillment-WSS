﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderTextDelete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderTextDelete'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderTextDelete] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderTextDelete'
GO
ALTER PROCEDURE [dbo].[SaleOrderTextDelete]
@OrderNumber CHAR (6), @Number CHAR (2), @Type CHAR (2)
AS
BEGIN
	SET NOCOUNT ON;

	delete 
		CORTXT
	where
		NUMB		= @OrderNumber
		and LINE	= @Number
		and [TYPE]	= @Type
		
	return @@rowcount
END
GO

