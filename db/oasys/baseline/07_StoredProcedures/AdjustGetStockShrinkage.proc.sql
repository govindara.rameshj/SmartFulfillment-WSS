﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AdjustGetStockShrinkage]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure AdjustGetStockShrinkage'
	EXEC ('CREATE PROCEDURE [dbo].[AdjustGetStockShrinkage] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure AdjustGetStockShrinkage'
GO
-- =============================================
-- Author      : Partha Dutta
-- Create Date : 11/10/2010
-- Referral No : 431
-- Notes       : Desk Enquires -> Stock Item Enquiry -> Details -> Stock Report -> Shrinkage Report
--               Code supplied
-- =============================================


-- Verion Control Information
-- =============================================
-- Author:  	Kevan Madelin
-- Create date: 30/09/2010
-- Version:	3.0.0.0
-- Description: This procedure is used to get the shrinkage data for the SIE.
-- =============================================

ALTER PROCEDURE [dbo].[AdjustGetStockShrinkage]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	select
		sj.DATE1			as 'Date',
		sj.SEQN				as 'Sequence',
		sj.CODE				as 'Code',
		sj.INIT				as 'Initials',
		sj.SSTK				as 'StockStart',
		sj.QUAN				as 'QtyAdjusted',
		(sj.SSTK + sj.QUAN)	as 'StockEnd',
		(sj.QUAN * sj.PRIC) as 'Value',
		sj.INFO				as 'Comment'
	from
		STKADJ sj 
	inner join 
		SACODE sa	on sa.NUMB = sj.CODE /*and sa.IsStockLoss=1*/
	where
		sj.SKUN = @SkuNumber
		and sj.IsReversed=0
	order by
		sj.DATE1 desc,
		sj.SEQN,
		sj.AmendId
			
END
GO

