﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingCashier]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingCashier'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingCashier] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingCashier'
GO
-- =============================================
-- Author        : Alan Lewis
-- Update date   : 08/11/2012
-- User Story    : 8924
-- Project       : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--               : Dropdown List
-- Task Id       : 8985
-- Description   : Modify to use new udf_CashierHasTakenSaleToday
--               : to determine whether a cashier has taken a sale
--               : for the banking period and filter out those that
--               : have not.
-- =============================================
ALTER PROCedure [dbo].[NewBankingCashier]
    @PeriodID As Integer = Null
As
Begin
    Set NoCount On

    Select
        UserID = ID,
        Employee = EmployeeCode + ' - ' + Name
    From
        SystemUsers
    Where
        IsDeleted = 0   --remove delete cashiers
    And
        (ID <= (Select Cast(HCAS As Integer) From RETOPT) --filter out non operational users
        or 
        ID in (select Mapping_CashierID from dbo.OVC_Mapping_Cashiers) --filter add virtual cashier
        )   
    And   --filter out cashiers that have not taken a sale
        (
            [dbo].[udf_CashierHasTakenSaleOnDay](ID, @PeriodID) = 1
        Or
            @PeriodID Is Null
        )
End
GO

