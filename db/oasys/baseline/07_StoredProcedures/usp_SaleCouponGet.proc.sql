﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SaleCouponGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SaleCouponGet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SaleCouponGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SaleCouponGet'
GO
ALTER PROCedure [dbo].[usp_SaleCouponGet]
			@TransactionDate DATE,
			@TillNumber CHAR (2), 
			@TransactionNumber CHAR(4),
			@SequenceNumber CHAR(4) = Null
			
		As
		Begin
			Set NOCOUNT ON;

		Select 
			dc.[TranDate],
			dc.[TranTillID],
			dc.[TranNo],
			dc.[Sequence],
			dc.[CouponID],
			dc.[Status],
			dc.[ExcCoupon],
			dc.[MarketRef],
			dc.[ReUsable],
			dc.[IssueStoreNo],
			dc.[SerialNo],
			dc.[ManagerID],
			dc.[RTI]					
		From
			DLCOUPON  dc
		Where
			dc.[TranDate]= @TransactionDate
		And
			dc.[TranNo]= @TransactionNumber
		And
			dc.[TranTillID] = @TillNumber
		And
			(
				@SequenceNumber Is Null
			Or
				(
					dc.Sequence = @SequenceNumber
				And
					Not dc.Sequence Is Null
				)
			)
		End
GO

