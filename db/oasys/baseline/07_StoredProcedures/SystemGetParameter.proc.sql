﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemGetParameter]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemGetParameter'
	EXEC ('CREATE PROCEDURE [dbo].[SystemGetParameter] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemGetParameter'
GO
ALTER PROCEDURE [dbo].[SystemGetParameter]
	@Id int
AS
begin
	SET NOCOUNT ON;

	select 
		*
	from
		Parameters
	where
		ParameterId = @Id
	
end
GO

