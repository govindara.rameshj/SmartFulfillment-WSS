﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetReceiptQty]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetReceiptQty'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetReceiptQty] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetReceiptQty'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 15/06/2011
-- Description:	Get the Receipt Quantity for the DRL and Sku entered
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetReceiptQty] 
	-- Add the parameters for the stored procedure here
	@DRLNumber Char(6) , 
	@SkuNumber Char(6)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		RECQ as 'ReceiptQty'
	FROM
		DRLDET
	WHERE 
		NUMB = @DRLNumber ANd SKUN = @SkuNumber
END
GO

