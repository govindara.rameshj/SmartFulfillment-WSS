﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetTransactionNumberForTill]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GetTransactionNumberForTill'
	EXEC ('CREATE PROCEDURE [dbo].[GetTransactionNumberForTill] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GetTransactionNumberForTill'
GO
ALTER PROCEDURE dbo.GetTransactionNumberForTill 
    @Id int 
AS
    BEGIN
        DECLARE @TranNumber int
        SET @Id = @Id + 9000
        IF EXISTS (SELECT 1 FROM dbo.SystemNumbers WHERE Id = @Id)
            BEGIN
                EXEC @TranNumber = dbo.SystemNumbersGetNext @Id
            END 
        ELSE
            BEGIN
                declare @min as int
                select @min = LongValue FROM dbo.Parameters WHERE ParameterId = 6500
                declare @max as int
                select @max = LongValue FROM dbo.Parameters WHERE ParameterId = 6501
                
                INSERT INTO dbo.SystemNumbers([ID],[Description],[NextNumber],[Size],[Min],[Max])
                    VALUES (@Id, CAST(@Id AS char(20)), @min, len(cast(@max as varchar)), @min, @max)
                    
                EXEC @TranNumber = dbo.SystemNumbersGetNext @Id
            END
        RETURN @TranNumber
    END
GO

