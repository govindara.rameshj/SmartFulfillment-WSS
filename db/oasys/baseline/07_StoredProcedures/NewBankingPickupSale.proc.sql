﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingPickupSale]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingPickupSale'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingPickupSale] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingPickupSale'
GO
ALTER PROCedure NewBankingPickupSale
   @PickupID int 
as
begin
set nocount on

select TenderID       = a.TenderID,
       DenominationID = a.ID,
       PickupValue    = b.Value
from (select *
      from SystemCurrencyDen
      where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)) a
left outer join (select * from SafeBagsDenoms where BagID = @PickupID) b
           on  b.TenderID = a.TenderID 
           and b.ID       = a.ID
end
GO

