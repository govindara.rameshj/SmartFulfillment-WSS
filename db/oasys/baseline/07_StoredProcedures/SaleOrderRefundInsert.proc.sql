﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderRefundInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderRefundInsert'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderRefundInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderRefundInsert'
GO
ALTER PROCEDURE [dbo].[SaleOrderRefundInsert]
@OrderNumber CHAR (6), @Number CHAR (4), @RefundStoreId INT, @RefundDate DATE, @RefundTill CHAR (2), @RefundTransaction CHAR (4), @QtyReturned INT=0, @QtyCancelled INT=0, @RefundStatus INT=0, @SellingStoreIbtOut CHAR (6)=null, @FulfillingStoreIbtIn CHAR (6)=null
AS
BEGIN
	SET NOCOUNT ON;
	
	insert into	CORRefund (
		NUMB,
		LINE,
		RefundStoreId,
		RefundDate,
		RefundTill,
		RefundTransaction,
		QtyReturned,
		QtyCancelled,
		RefundStatus,
		SellingStoreIbtOut,
		FulfillingStoreIbtIn
	) values (
		@orderNumber,
		@number,
		@RefundStoreId,
		@RefundDate,
		@RefundTill,
		@RefundTransaction,
		@QtyReturned,
		@QtyCancelled,
		@RefundStatus,
		@SellingStoreIbtOut,
		@FulfillingStoreIbtIn
	)
	
	return @@rowcount
END
GO

