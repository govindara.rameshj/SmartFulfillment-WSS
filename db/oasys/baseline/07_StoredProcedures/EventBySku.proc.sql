﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EventBySku]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EventBySku'
	EXEC ('CREATE PROCEDURE [dbo].[EventBySku] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EventBySku'
GO
ALTER PROCEDURE [dbo].[EventBySku]
	@EventNumber char(6)
AS
BEGIN
	SET NOCOUNT ON;

	Declare @Type as char(2);
	set @Type = (select top 1 [TYPE] from EVTMAS where NUMB = @EventNumber);

	If @Type in ('TS','TM','HS','QS','QM')
	begin
		SELECT 
			distinct @EventNumber	as 'Event',
			em.KEY1					as 'SKU',
			sm.DESCR				as 'Description'
		From 
			EVTMAS as em 
		inner join 
			STKMAS as sm on em.Key1 = sm.skun
		where 
			em.numb = @EventNumber
	end

END
GO

