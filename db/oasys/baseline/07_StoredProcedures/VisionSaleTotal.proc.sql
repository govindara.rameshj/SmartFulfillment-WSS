﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionSaleTotal]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VisionSaleTotal'
	EXEC ('CREATE PROCEDURE [dbo].[VisionSaleTotal] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VisionSaleTotal'
GO
-- =============================================
-- Author:  Dhanesh Ramachandran
-- Create date: 09/14/2010
-- Description: This procedure is used to insert values to DLTOTS table for Vision Sales
--
-- =============================================
ALTER PROCEDURE [dbo].[VisionSaleTotal]
    @TranDate DATE ,
    @TillId CHAR(2) ,
    @TranNumber CHAR(4) ,
    @CashierId CHAR(3) ,
    @TranDateTime CHAR(6) ,
    @TransactionCode CHAR(2) ,
    @OrderNumber CHAR(6) ,
    @ExternalDocumentNumber CHAR(8) ,
    @MerchandiseAmount DECIMAL(9, 2) ,
    @NonMerchandising DECIMAL(9, 2) ,
    @TaxAmount DECIMAL(9, 2) ,
    @Discount DECIMAL(9, 2) ,
    @Total DECIMAL(9, 2) ,
    @EmployeeDiscount BIT ,
    @RefundCshrNumber CHAR(8)
AS 
    BEGIN
        SET NOCOUNT ON ;
	
        DECLARE @rowcount INT ;
        SET @rowcount = 0 ;	

	--update record if exists
	
        UPDATE  DLTOTS
        SET     CASH = @CashierId ,
                [TIME] = @TranDateTime ,
                SUPV = '000' ,
                TCOD = @TransactionCode ,
                [OPEN] = 0 ,
                ORDN = '000000' ,
                VSUP = '000' ,
                DOCN = @OrderNumber ,
                STOR = '000' ,
                MERC = @MerchandiseAmount ,
                NMER = @NonMerchandising ,
                TAXA = '0' ,
                DISC = '0' ,
                DSUP = '000' ,
                TOTL = @Total ,
                ACCN = '000000' ,
                [CARD] = '000000',
                IEMP = @EmployeeDiscount ,
                RCAS = '000' ,
                RSUP = '000' ,
                VATR1 = (SELECT  RETOPT.VATR1  FROM    RETOPT) ,
                VATR2 = (SELECT  RETOPT.VATR2  FROM    RETOPT) ,
                VATR3 = (SELECT  RETOPT.VATR3  FROM    RETOPT),
                VATR4 = (SELECT  RETOPT.VATR4  FROM    RETOPT),
                VATR5 = (SELECT  RETOPT.VATR5  FROM    RETOPT) ,
                VATR6 = (SELECT  RETOPT.VATR6  FROM    RETOPT) ,
                VATR7 = (SELECT  RETOPT.VATR7  FROM    RETOPT) ,
                VATR8 = (SELECT  RETOPT.VATR8  FROM    RETOPT) ,
                VATR9 = (SELECT  RETOPT.VATR9  FROM    RETOPT) ,
                VSYM1 = 'a' ,
                VSYM2 = 'b' ,
                VSYM3 = 'c' ,
                VSYM4 = 'd' ,
                VSYM5 = 'e' ,
                VSYM6 = 'f' ,
                VSYM7 = 'g' ,
                VSYM8 = 'h' ,
                VSYM9 = 'i' ,
                XVAT1 = 0 ,
                XVAT2 = 0 ,
                XVAT3 = 0 ,
                XVAT4 = 0 ,
                XVAT5 = 0 ,
                XVAT6 = 0 ,
                XVAT7 = 0 ,
                XVAT8 = 0 ,
                XVAT9 = 0 ,
                VATV1 = 0 ,
                VATV2 = 0 ,
                VATV3 = 0 ,
                VATV4 = 0 ,
                VATV5 = 0 ,
                VATV6 = 0 ,
                VATV7 = 0 ,
                VATV8 = 0 ,
                VATV9 = 0 ,
                RMAN = '000' ,
                TOCD = '00' ,
                GTPN = 0 ,
                CCRD = '000000000' ,
                SSTA = '00' ,
                SSEQ = '0000' ,
                CBBU = @RefundCshrNumber ,
                CARD_NO = '' ,
                RTI = 'S',
                ICOM = 1,
                REMO=1
        WHERE   DATE1 = @TranDate
                AND TILL = @TillId
                AND [TRAN] = @TranNumber
			
		--get number of rows affected
        SET @rowcount = @@ROWCOUNT ;
		--if nothing updated then insert
        IF @rowcount = 0 
            BEGIN
                INSERT  INTO DLTOTS
                        ( DATE1 ,
                          TILL ,
                          [TRAN] ,
                          CASH ,
                          [TIME] ,
                          SUPV ,
                          TCOD ,
                          [OPEN] ,
                          ORDN ,
                          VSUP ,
                          DOCN ,
                          STOR ,
                          MERC ,
                          NMER ,
                          TAXA ,
                          DISC ,
                          DSUP ,
                          TOTL ,
                          ACCN ,
                          [CARD] ,
                          IEMP ,
                          RCAS ,
                          RSUP ,
                          VATR1 ,
                          VATR2 ,
                          VATR3 ,
                          VATR4 ,
                          VATR5 ,
                          VATR6 ,
                          VATR7 ,
                          VATR8 ,
                          VATR9 ,
                          VSYM1 ,
                          VSYM2 ,
                          VSYM3 ,
                          VSYM4 ,
                          VSYM5 ,
                          VSYM6 ,
                          VSYM7 ,
                          VSYM8 ,
                          VSYM9 ,
                          XVAT1 ,
                          XVAT2 ,
                          XVAT3 ,
                          XVAT4 ,
                          XVAT5 ,
                          XVAT6 ,
                          XVAT7 ,
                          XVAT8 ,
                          XVAT9 ,
                          VATV1 ,
                          VATV2 ,
                          VATV3 ,
                          VATV4 ,
                          VATV5 ,
                          VATV6 ,
                          VATV7 ,
                          VATV8 ,
                          VATV9 ,
                          RMAN ,
                          TOCD ,
                          GTPN ,
                          CCRD ,
                          SSTA ,
                          SSEQ ,
                          CBBU ,
                          CARD_NO ,
                          RTI,
                          ICOM,
                          REMO   
			      )
                VALUES  ( @TranDate ,
                          @TillId ,
                          @TranNumber ,
                          @CashierId ,
                          @TranDateTime ,
                          '000' ,
                          @TransactionCode ,
                          0 ,
                          '000000' ,
                          '000' ,
                          @OrderNumber ,
                          '000' ,
                          @MerchandiseAmount ,
                          @NonMerchandising ,
                          '0' ,
                          '0' ,
                          '000' ,
                          @Total ,
                          '000000' ,
                          '000000' ,
                          @EmployeeDiscount ,
                          '000' ,
                          '000' ,
                          (SELECT  RETOPT.VATR1  FROM    RETOPT) ,
                          (SELECT  RETOPT.VATR2  FROM    RETOPT) ,
                          (SELECT  RETOPT.VATR3  FROM    RETOPT) ,
                          (SELECT  RETOPT.VATR4  FROM    RETOPT) ,
                          (SELECT  RETOPT.VATR5  FROM    RETOPT) ,
                          (SELECT  RETOPT.VATR6  FROM    RETOPT) ,
                          (SELECT  RETOPT.VATR7  FROM    RETOPT) ,
                          (SELECT  RETOPT.VATR8  FROM    RETOPT) ,
                          (SELECT  RETOPT.VATR9  FROM    RETOPT) ,
                          'a' ,
                          'b' ,
                          'c' ,
                          'd' ,
                          'e' ,
                          'f' ,
                          'g' ,
                          'h' ,
                          'i' ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          0 ,
                          '000' ,
                          '00' ,
                          0 ,
                          '000000000' ,
                          '00' ,
                          '0000' ,
                          @RefundCshrNumber ,
                          '' ,
                          'S',
                          1,
                          1  
			      ) ;

                SET @rowcount = @@ROWCOUNT ;
            END		
	
        IF @TransactionCode = 'M+' 
            UPDATE  DLTOTS
            SET     DLTOTS.MISC = ( SELECT  RETOPT.SDIO
                                    FROM    RETOPT
                                  ) ,
                    DLTOTS.DESCR = ( SELECT RETOPT.MPRC20
                                     FROM   RETOPT
                                   )
            WHERE   DATE1 = @TranDate
                    AND TILL = @TillId
                    AND [TRAN] = @TranNumber	 
		     
		
        ELSE 
            IF @TransactionCode = 'M-' 
                UPDATE  DLTOTS
                SET     DLTOTS.MISC = ( SELECT  RETOPT.SDOO
                                        FROM    RETOPT
                                      ) ,
                        DESCR = ( SELECT    RETOPT.MMRC20
                                  FROM      RETOPT
                                )
                WHERE   DATE1 = @TranDate
                        AND TILL = @TillId
                        AND [TRAN] = @TranNumber	    
                        
         IF @RefundCshrNumber = ''
                  UPDATE DLTOTS
                   SET CBBU = NULL
                      WHERE       DATE1 = @TranDate
                                  AND TILL = @TillId
                                  AND [TRAN] = @TranNumber
		 
        RETURN @rowcount ;
    END
GO

