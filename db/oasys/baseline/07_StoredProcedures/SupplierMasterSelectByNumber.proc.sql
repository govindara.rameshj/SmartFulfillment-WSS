﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierMasterSelectByNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierMasterSelectByNumber'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierMasterSelectByNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierMasterSelectByNumber'
GO
ALTER PROCEDURE [dbo].[SupplierMasterSelectByNumber]
@Number CHAR (5)
AS

SELECT		SUPMAS.* 
FROM		SUPMAS 
where		supmas.supn = @number
GO

