﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_OrderDataExtractReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_OrderDataExtractReport'
	EXEC ('CREATE PROCEDURE [dbo].[usp_OrderDataExtractReport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_OrderDataExtractReport'
GO
ALTER PROCEDURE [dbo].[usp_OrderDataExtractReport] 
     @StartDate AS date,
     @EndDate AS date = NULL
AS
BEGIN

    SELECT
        SellingStoreId AS 'Selling Store Id', 
        OrderLine.SKUN AS 'SKU No',
        SourceOrderNumber AS 'OVC Reference', 
        OrderLine.NUMB AS 'Back Office Order Number', 
        DELD AS 'Delivery Date', 
        OrderLine.QTYO * OrderLine.Price AS 'Line Value', 
        'Fulfiller Store Id' = 
            CASE 
                WHEN OrderLine.DeliverySource IS NULL THEN ''
                WHEN OrderLine.DeliverySource = '0' THEN ''
                ELSE OrderLine.DeliverySource
            END
    FROM vwCORHDRFull as OrderHeaderFull
        JOIN (SELECT SKUN, NUMB, QTYO, Price, DeliverySource FROM CORLIN) AS OrderLine ON OrderLine.NUMB = OrderHeaderFull.NUMB
    WHERE ((OrderHeaderFull.DATE1 = @StartDate AND @EndDate IS NULL) OR (OrderHeaderFull.DATE1 >= @StartDate AND OrderHeaderFull.DATE1 <= @EndDate)) AND SourceOrderNumber IS NOT NULL AND OrderHeaderFull.Source = 'OV'
END
GO

