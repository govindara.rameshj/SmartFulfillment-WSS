﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QodGetOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure QodGetOrder'
	EXEC ('CREATE PROCEDURE [dbo].[QodGetOrder] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure QodGetOrder'
GO
ALTER PROCEDURE QodGetOrder
	@orderNumber char(6)
as
begin
	set nocount on;
	SELECT DISTINCT
		vc.NUMB,
		vc.DATE1,
		vc.DELC,
		vc.DELD,
		vc.CUST,
		vc.NAME,
		vc.ADDR1,
		vc.ADDR2, 
		vc.ADDR3, 
		vc.ADDR4, 
		vc.POST, 
		vc.PHON,
		vc.DELI,
		vc.STIL,
		vc.SDAT,
		vc.STRN,
		vc.DeliveryStatus,
		lin.DeliverySource
		FROM vwCORHDRFull as vc
			INNER JOIN QUOHDR as q ON vc.NUMB = q.NUMB
			INNER JOIN CORLIN as lin ON vc.NUMB = lin.NUMB
		WHERE vc.NUMB = @orderNumber and vc.SHOW_IN_UI = 1;
		
end
GO

