﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryLabel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryLabel'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryLabel] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryLabel'
GO
ALTER PROCEDURE [dbo].[EnquiryLabel]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	declare @table table (Description varchar(50), Display varchar(50));
	declare
		@peg		int,
		@small		int,
		@medium 	int;
		
	select
		@peg		= LABN,
		@small		= LABM,
		@medium		= LABL
	from
		stkmas
	where
		skun = @SkuNumber

	insert into @table(Description, Display) values ('Peg',		@peg );
	insert into @table(Description, Display) values ('Small',	@small );
	insert into @table(Description, Display) values ('Medium',	@medium );
	
	select * from @table;
END
GO

