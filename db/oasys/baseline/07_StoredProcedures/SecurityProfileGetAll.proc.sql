﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SecurityProfileGetAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SecurityProfileGetAll'
	EXEC ('CREATE PROCEDURE [dbo].[SecurityProfileGetAll] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SecurityProfileGetAll'
GO
ALTER PROCEDURE [dbo].[SecurityProfileGetAll]
@ProfileType CHAR (1)=null
AS
begin
	SET NOCOUNT ON;

	select 
		ID					as Id,
		ProfileType,
		Description,
		Position,
		IsAreaManager,
		PasswordValidFor	as DaysPasswordValid,
		IsDeleted,
		DeletedBy,
		DeletedDate			as DateDeleted,
		DateLastEdited,
		IsSystemAdmin,
		IsHelpDeskUser
	from
		SecurityProfile
	where
	 	((@ProfileType is null) or  (@ProfileType is not null and ProfileType=@ProfileType))
		
end
GO

