﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderTextUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderTextUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderTextUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderTextUpdate'
GO
ALTER PROCEDURE [dbo].[SaleOrderTextUpdate]
@OrderNumber CHAR (6), @Number CHAR (2), @Type CHAR (2), @Text VARCHAR (200)
AS
BEGIN
	SET NOCOUNT ON;

	update 
		CORTXT
	set
		[TEXT]	= @Text
	where
		NUMB		= @OrderNumber
		and LINE	= @Number
		and [TYPE]	= @Type
		
	return @@rowcount
END
GO

