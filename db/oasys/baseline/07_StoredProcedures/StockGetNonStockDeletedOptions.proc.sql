﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetNonStockDeletedOptions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetNonStockDeletedOptions'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetNonStockDeletedOptions] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetNonStockDeletedOptions'
GO
ALTER PROCedure [dbo].[StockGetNonStockDeletedOptions]
AS
BEGIN
	declare @OutputTable TABLE ( Id char(1), Display char(20) )

	insert into @OutputTable values ('A', 'All');
	insert into @OutputTable values ('D', 'Deleted Only');
	insert into @OutputTable values ('N', 'Non-Stock Only');
	
	select * from @OutputTable
	
END
GO

