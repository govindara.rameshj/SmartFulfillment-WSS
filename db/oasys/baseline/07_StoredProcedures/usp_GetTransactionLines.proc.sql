﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetTransactionLines]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetTransactionLines'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetTransactionLines] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetTransactionLines'
GO
ALTER PROCEDURE usp_GetTransactionLines
@ReportDate AS Date,
@TillId As varchar(2),
@TranNo As varchar(4)
as
begin

SELECT DL.SKUN , 
       DL.PRIC , 
       DL.QUAN , 
       DL.EXTP , 
       IM.DESCR , 
       IM.INON , 
       IM.SALT , 
       DL.QBPD , 
       DL.DGPD , 
       DL.MBPD , 
       DL.HSPD , 
       DL.ESEV
  FROM
       DLLINE DL LEFT OUTER JOIN STKMAS IM
       ON IM.SKUN
          = 
          DL.SKUN
  WHERE DATE1
        = 
        @ReportDate
    AND TILL
        = 
        @TillId
    AND [TRAN]
        = 
        @TranNo
    AND QUAN < 0
    AND LREV = 0
  ORDER BY NUMB;
  
end
GO

