﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_PVTCLN]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_PVTCLN'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_PVTCLN] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_PVTCLN'
GO
ALTER PROCEDURE dbo.KevanConversion_PVTCLN
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 17 - Kevan's PV Table Clean Up Routine
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

Update PVTOTS
Set CASH = '555'
Where CASH in (
SELECT CASH
FROM PVTOTS as pt
where (pt.CASH like '%<%' or pt.CASH like '%>'))

END;
GO

