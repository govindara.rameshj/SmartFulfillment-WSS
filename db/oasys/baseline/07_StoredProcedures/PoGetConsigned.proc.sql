﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoGetConsigned]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoGetConsigned'
	EXEC ('CREATE PROCEDURE [dbo].[PoGetConsigned] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoGetConsigned'
GO
-- =============================================
-- Author      : Partha Dutta
-- Create Date : 14/10/2010
-- Referral No : 432
-- Notes       : Menu : Purchase Orders -> Consign Purchase Orders
-- =============================================

ALTER PROCedure [dbo].[PoGetConsigned]
    @StartDate date,
    @EndDate   date
as
begin
    set nocount on

    select		
        ph.tkey as 'Id',
		ph.numb as 'PoNumber',
		ph.pnum as 'ConsignNumber',
		ph.soqn as 'SoqNumber',
		ph.reln as 'ReleaseNumber',
		ph.supp as 'SupplierNumber',
		sm.name as 'SupplierName',
		ph.bbcc as 'SupplierBbc',
		ph.odat as 'DateCreated',
		ph.ddat as 'DateDue',
		ph.nocr as 'Cartons',
		ph.valu as 'Value',
		ph.qtyo as 'Units',
		[Time] = case
                 when len(coalesce(con.time1,'')) = 4 then SUBSTRING(con.time1, 1, 2) + ':' + SUBSTRING(con.time1, 3, 2)
                 else ''
		      end
	from	
		purhdr ph
	inner join	
		supmas sm on sm.supn = ph.supp
    left outer join
        conmas con on con.pono = ph.numb
	where	
		ph.bbcc = ''
		and	ph.delm = 0
		and	(ph.pnum <> 0 or ph.PNUM is not null)
		and	ph.odat >= @StartDate
		and	ph.odat <= @EndDate
	order by	
		ph.odat desc
end
GO

