﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetCouponMaster]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetCouponMaster'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetCouponMaster] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetCouponMaster'
GO
-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 06/10/2011
-- TFSID        : 2733 
-- Description  : New Stored Procedure for Coupons ProcessTransmissions
-- =====================================================================

-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================

ALTER PROCEDURE usp_GetCouponMaster
@couponID AS CHAR(7) 
AS 
  BEGIN 
      SELECT couponid, 
             [DESCRIPTION], 
             storegen  AS StoreGenerated, 
             serialno, 
             exccoupon AS ExclusiveCoupon, 
             reusable, 
             collectinfo, 
             deleted 
      FROM   COUPONMASTER 
      WHERE  couponid = @couponID 
  END
GO

