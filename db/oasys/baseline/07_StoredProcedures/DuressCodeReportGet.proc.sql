﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DuressCodeReportGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DuressCodeReportGet'
	EXEC ('CREATE PROCEDURE [dbo].[DuressCodeReportGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DuressCodeReportGet'
GO
ALTER PROCEDURE [dbo].[DuressCodeReportGet]
    @DateStart datetime = NULL, 
    @DateEnd datetime = NULL
AS
BEGIN
    SET NOCOUNT ON;
    
    SELECT
        DATE1 as 'Date',
        CASH as 'Cashier',
        TILL as 'Till',
        [TRAN] as 'Transaction',
        OVCTranNumber as 'OVCTranNumber'
    FROM vwDLTOTS 
    WHERE   [OPEN]          =       '9'
            AND CASH        <>      '000'
            AND VOID        =       0
            AND PARK        =       0
            AND TMOD        =       0  
            AND (Date1 >= @DateStart and Date1 <= @DateEnd)
END
GO

