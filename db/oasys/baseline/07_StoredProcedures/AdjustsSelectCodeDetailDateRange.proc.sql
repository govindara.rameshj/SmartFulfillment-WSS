﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AdjustsSelectCodeDetailDateRange]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure AdjustsSelectCodeDetailDateRange'
	EXEC ('CREATE PROCEDURE [dbo].[AdjustsSelectCodeDetailDateRange] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure AdjustsSelectCodeDetailDateRange'
GO
-- ===============================================================================
-- Author         : Michael O'Cain
-- Date	          : 16/02/2012
-- TFS User Story : 3925
-- Description    : Markdown not displaying correctly on Adjustments report.
-- TFS Task ID    : 4175/RF0919
-- ===============================================================================
ALTER PROCedure [dbo].[AdjustsSelectCodeDetailDateRange]

   @StartDate date,
   @EndDate   date

as
begin
   set nocount on

   select CodeNumber          = SACODE.NUMB,
          CodeDescription     = SACODE.DESCR,
          SkuDescription      = rtrim(STKMAS.DESCR),
          CategoryNumber      = HIECAT.NUMB,
          CategoryDescription = rtrim(HIECAT.DESCR),
          GroupNumber         = HIEGRP.GROU,
          GroupDescription    = rtrim(HIEGRP.DESCR),
          SubgroupNumber      = HIESGP.SGRP,
          SubgroupDescription = rtrim(HIESGP.DESCR),
          StyleNumber         = HIESTY.STYL,
          StyleDescription    = rtrim(HIESTY.DESCR),
          Value               = (
								   Case ltrim(rtrim(STKADJ.INFO))
									when 'Till Markdown Sale' then
										(select coalesce(sum((a.QUAN) * a.PRIC), 0)
										from STKADJ a
										where a.DATE1   = STKADJ.DATE1
										and   a.CODE    = STKADJ.CODE
										and   a.SKUN    = STKADJ.SKUN
										and   a.SEQN    = STKADJ.SEQN
										and   a.AmendId = STKADJ.AmendId
										and not (a.MOWT = 'W' and ltrim(rtrim(a.WAUT)) = '0'))
									else
										(select coalesce(sum(a.QUAN * a.PRIC), 0)
										from STKADJ a
										where a.DATE1   = STKADJ.DATE1
										and   a.CODE    = STKADJ.CODE
										and   a.SKUN    = STKADJ.SKUN
										and   a.SEQN    = STKADJ.SEQN
										and   a.AmendId = STKADJ.AmendId
										and not (STKADJ.MOWT = 'M')                                 
										and not (a.MOWT = 'W' and ltrim(rtrim(a.WAUT)) = '0'))
								     end),
          QtyAdjust = (STKADJ.QUAN),
          QtyMarkdown         = case STKADJ.MOWT
                                   when 'M' then (STKADJ.QUAN * -1)
                                   else 0
                                end,
          QtyWrittenOff       = case STKADJ.MOWT
                                   when 'W' then (STKADJ.QUAN * -1)
                                   else 0
                                end,
          STKADJ.DATE1,
          STKADJ.SKUN,
          STKADJ.[INIT],
          STKADJ.PRIC,
          STKADJ.SSTK,
          STKADJ.INFO
   from STKADJ
   inner join   SACODE on SACODE.NUMB = STKADJ.CODE
   inner join   STKMAS on STKMAS.SKUN = STKADJ.SKUN
   inner join   HIECAT on STKMAS.CTGY = HIECAT.NUMB
   inner join   HIEGRP on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU
   inner join   HIESGP on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU and STKMAS.SGRP = HIESGP.SGRP 
   inner join   HIESTY on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU and STKMAS.SGRP = HIESGP.SGRP and STKMAS.STYL = HIESTY.STYL
   where STKADJ.DATE1 >= @StartDate
   and   STKADJ.DATE1 <= @EndDate
   order by SACODE.NUMB, STKADJ.DATE1, STKMAS.DESCR;
end
If @@ERROR = 0
   Print 'Success: The Store Procedure Change (RF0919) to AdjustsSelectCodeDetailDateRange has been successfully deployed.'
Else
   Print 'Failure: The Store Procedure Change (RF0919) to AdjustsSelectCodeDetailDateRange has NOT been successfully deployed.'
GO

