﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingFloatChecked]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingFloatChecked'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingFloatChecked] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingFloatChecked'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 19/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : When rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7971
-- Description   : Add SafeBag Comments column Update criteria.
-- =============================================
ALTER PROCedure [dbo].[NewBankingFloatChecked]
   @FloatBagID Int,
   @UserID1    Int,
   @UserID2    Int,
   @Comments   Char(255) = Null
As
Begin
	Set NoCount On

	If @Comments Is Null
		Begin
			Update
				SafeBags
			Set
				FloatChecked        = 1,
				FloatCheckedUserID1 = @UserID1,
				FloatCheckedUserID2 = @UserID2
			Where
				ID = @FloatBagID
		End
	Else
		Begin
			Update
				SafeBags
			Set
				FloatChecked        = 1,
				FloatCheckedUserID1 = @UserID1,
				FloatCheckedUserID2 = @UserID2,
				Comments	        = @Comments
			Where
				ID = @FloatBagID
		End
End
GO

