﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AdjustsSelectCodeSummaryDateRange]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure AdjustsSelectCodeSummaryDateRange'
	EXEC ('CREATE PROCEDURE [dbo].[AdjustsSelectCodeSummaryDateRange] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure AdjustsSelectCodeSummaryDateRange'
GO
-- ===============================================================================
-- Author         : Michael O'Cain
-- Date	          : 16/02/2012
-- TFS User Story : 3925
-- Description    : Markdown not displaying correctly on Adjustments report.
-- TFS Task ID    : 4175/RF0919
-- ===============================================================================
ALTER PROCedure [dbo].[AdjustsSelectCodeSummaryDateRange]

   @StartDate date,
   @EndDate   date

as
begin
   set nocount on

   select CodeNumber      = SACODE.NUMB,
          CodeDescription = SACODE.DESCR,
          QtyAdjust       = (
                             select coalesce(sum(STKADJ.QUAN),0)
                             from STKADJ
                             where STKADJ.CODE       = SACODE.NUMB
                              and   STKADJ.IsReversed = 0
                             and   STKADJ.DATE1     >= @StartDate
                             and   STKADJ.DATE1     <= @EndDate
                            ),
          QtyMarkdown     = (
                             select coalesce(sum(STKADJ.QUAN * -1),0)
                             from STKADJ
                             where STKADJ.CODE       = SACODE.NUMB
                             and   STKADJ.MOWT       = 'M'
                             and   STKADJ.IsReversed = 0
                             and   STKADJ.DATE1     >= @StartDate
                             and   STKADJ.DATE1     <= @EndDate
                            ),
          QtyWrittenOff   = (
                             select coalesce(sum(STKADJ.QUAN * -1),0)
                             from STKADJ 
                             where STKADJ.CODE       = SACODE.NUMB
                             and   STKADJ.MOWT       = 'W'
                             and   STKADJ.IsReversed = 0
                             and   STKADJ.DATE1     >= @StartDate
                             and   STKADJ.DATE1     <= @EndDate),
          Value           = (
                             select coalesce(sum(STKADJ.QUAN * STKADJ.PRIC),0)
                             from STKADJ
                             where STKADJ.CODE       = SACODE.NUMB
                             and   STKADJ.IsReversed = 0
                             and   STKADJ.DATE1     >= @StartDate
                             and   STKADJ.DATE1     <= @EndDate
                             and not (STKADJ.MOWT = 'M' And ltrim(rtrim(STKADJ.INFO))<> 'Till Markdown Sale')                             
                             and not (STKADJ.MOWT = 'W' and ltrim(rtrim(STKADJ.WAUT)) = '0')
                             )


   from SACODE
   order by SACODE.NUMB
end
GO

