﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NitmasStarted]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NitmasStarted'
	EXEC ('CREATE PROCEDURE [dbo].[NitmasStarted] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NitmasStarted'
GO
ALTER PROCEDURE [dbo].[NitmasStarted]
   @NitmasPending datetime,
   @Started       bit output
--with encryption
as
set nocount on

DECLARE @NITMASPending_Date SMALLDATETIME
DECLARE @NITMASPending_Time VARCHAR(6)

SET @NITMASPending_Date = @NitmasPending
SET @NITMASPending_Date = DATEADD(hour, -DATEPART(hour, @NitmasPending), @NITMASPending_Date)
SET @NITMASPending_Date = DATEADD(minute, -DATEPART(minute, @NitmasPending), @NITMASPending_Date)
SET @NITMASPending_Date = DATEADD(second, -DATEPART(second, @NitmasPending), @NITMASPending_Date)

SELECT @NITMASPending_Time = RIGHT('00' + ltrim(str(DATEPART(hour, @NitmasPending), 2)), 2) + 
                             RIGHT('00' + ltrim(str(DATEPART(minute, @NitmasPending), 2)), 2) + 
                             RIGHT('00' + ltrim(str(DATEPART(second, @NitmasPending), 2)), 2)

SELECT @Started = CASE WHEN EXISTS(SELECT *
                                   FROM Oasys..NITLOG
                                   WHERE SDAT >= @NITMASPending_Date
                                   AND   STIM >= @NITMASPending_Time)
                     THEN 1
                     ELSE 0
END
GO

