﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_CONPVL]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_CONPVL'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_CONPVL] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_CONPVL'
GO
ALTER PROCEDURE dbo.KevanConversion_CONPVL
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 18 - PV Line Conversion
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

create table #pvLineTemp
(
RID INT IDENTITY(1,1),
[DATE1] [char](8) NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [char](4) NOT NULL,
	[SKUN] [char](6) NULL,
	[QUAN] [char](7) NULL,
	[SPRI] [char](10) NULL,
	[PRIC] [char](10) NULL,
	[PRVE] [char](10) NULL,
	[EXTP] [char](10) NULL,
	[RITM] [char](1) NULL,
	[VSYM] [char](1) NULL,
	[CTGY] [char](6) NULL,
	[GRUP] [char](6) NULL,
	[SGRP] [char](6) NULL,
	[STYL] [char](6) NULL,
	[SALT] [char](1) NULL,
	[PIVI] [char](1) NULL,
	[PIVM] [char](2) NULL,
	[PIVT] [char](1) NULL,
)

create table #visionLineTemp
(

[TranDate] [date] NOT NULL,
	[TillId] [int] NOT NULL,
	[TranNumber] [int] NOT NULL,
	[Number] [int] NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[Quantity] [int] ,
	[PriceLookup] [decimal](9, 2) ,
	[Price] [decimal](9, 2) ,
	[PriceExVat] [decimal](9, 2) ,
	[PriceExtended] [decimal](9, 2) ,
	[IsRelatedItemSingle] [bit] ,
	[VatSymbol] [char](1) ,
	[HieCategory] [char](6) ,
	[HieGroup] [char](6),
	[HieSubgroup] [char](6),
	[HieStyle] [char](6),
	[SaleType] [char](1),
	[IsInStoreStockItem] [bit],
	[MovementTypeCode] [char](2),
	[IsPivotal] [bit] ,
)

INSERT INTO #pvLineTemp
SELECT * FROM PVLINE  

INSERT INTO #visionLineTemp
SELECT * FROM VisionLine 

    DECLARE @RID_1 INT
	DECLARE @MAXID_1 INT
	SELECT @MAXID_1 = MAX(RID)FROM #pvLineTemp -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID_1 = 1
	DECLARE @DATE CHAR (15)
	DECLARE @TILL_ID_1 INT
	DECLARE @TRAN_NO_1 INT
	DECLARE @NUMB INT


-- looping the #temp_old from first record to last record.
	WHILE(@RID_1<=@MAXID_1)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @DATE = DATE1,@TILL_ID_1= CONVERT(INT,TILL),@TRAN_NO_1= CONVERT(INT,[TRAN]),@NUMB = CONVERT(INT,[NUMB])  FROM #pvLineTemp WHERE RID = @RID_1
			-- if the Primary key exists then update, by taking the values from the #temp_old  

			IF EXISTS(SELECT 1 FROM #visionLineTemp WHERE TranDate  = CONVERT(DATETIME,@DATE,3) AND TillId = @TILL_ID_1 AND TranNumber=@TRAN_NO_1 AND Number =@NUMB)
				BEGIN

					UPDATE VisionLine 
					SET	[SkuNumber]= T.[SKUN],
					    [Quantity]= CASE WHEN CHARINDEX('-',T.[QUAN])>1
					                      THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[QUAN],0,LEN(T.[QUAN])))
					                          ELSE
					                           CONVERT(DECIMAL,T.[QUAN])
					                          END,
					    [PriceLookup]= CASE WHEN CHARINDEX('-',T.[SPRI])>1
					                      THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[SPRI],0,LEN(T.[SPRI])))
					                          ELSE
					                           CONVERT(DECIMAL,T.[SPRI])
					                          END,
                        [Price]= CASE WHEN CHARINDEX('-',T.[PRIC])>1
					               THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[PRIC],0,LEN(T.[PRIC])))
					                    ELSE
					                     CONVERT(DECIMAL,T.[PRIC])
					                    END,
					    [PriceExVat]= CASE WHEN CHARINDEX('-',T.[PRVE])>1
					                         THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[PRVE],0,LEN(T.[PRVE])))
					                       ELSE
					                           CONVERT(DECIMAL,T.[PRVE])
					                        END,
					    [PriceExtended]= CASE WHEN CHARINDEX('-',T.[EXTP])>1
					                         THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[EXTP],0,LEN(T.[EXTP])))
					                          ELSE
					                           CONVERT(DECIMAL,T.[EXTP])
					                          END,
					    [IsRelatedItemSingle] = case when T.[RITM]='Y' then 1 else 0 end,
					    [VatSymbol] = T.[VSYM],
					    [HieCategory] = T.[CTGY],
					    [HieGroup] = T.[GRUP],
					    [HieSubgroup] = T.[SGRP],
					    [HieStyle]= T.[STYL],
					    [SaleType] = T.[SALT],
					    [IsInStoreStockItem] = case when T.[PIVI]='Y' then 1 else 0 end,
					    [MovementTypeCode] = T.[PIVM],
					    [IsPivotal] = case when T.[PIVT]='Y' then 1 else 0 end 
					    					    					    				    					    										
					FROM #pvLineTemp AS T WHERE (VisionLine.TranDate = CONVERT(DATETIME,@DATE,3) AND T.DATE1 = @DATE) AND (VisionLine.TillId = @TILL_ID_1 and  T.TILL = CONVERT(INT,@TILL_ID_1)) AND (VisionLine.TranNumber = @TRAN_NO_1) AND  (T.[TRAN]= CONVERT(INT,@TRAN_NO_1) AND VisionLine.Number = @NUMB AND  T.NUMB= CONVERT(INT,@NUMB))


				END
			ELSE -- ELSE INSERT NEW RECORD
				BEGIN
					INSERT INTO VisionLine (TranDate,TillId,TranNumber,Number,SkuNumber,Quantity,PriceLookup,Price,PriceExVat,PriceExtended,IsRelatedItemSingle,VatSymbol,HieCategory,HieGroup,HieSubgroup,HieStyle,SaleType,IsInStoreStockItem,MovementTypeCode,IsPivotal)
					SELECT CONVERT(DATETIME,T.DATE1,3),CONVERT(INT,T.TILL), CONVERT(INT,T.[TRAN]),CONVERT(INT,T.[NUMB]),T.[SKUN], 
					CASE WHEN CHARINDEX('-',T.[QUAN])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[QUAN],0,LEN(T.[QUAN])))
					ELSE
					CONVERT(DECIMAL,T.[QUAN])
					END,					
					CASE WHEN CHARINDEX('-',T.[SPRI])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[SPRI],0,LEN(T.[SPRI])))
					ELSE
					CONVERT(DECIMAL,T.[SPRI])
					END,
					CASE WHEN CHARINDEX('-',T.[PRIC])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[PRIC],0,LEN(T.[PRIC])))
					ELSE
					CONVERT(DECIMAL,T.[PRIC])
					END,
					CASE WHEN CHARINDEX('-',T.[PRVE])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[PRVE],0,LEN(T.[PRVE])))
					ELSE
					CONVERT(DECIMAL,T.[PRVE])
					END,
					CASE WHEN CHARINDEX('-',T.[EXTP])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[EXTP],0,LEN(T.[EXTP])))
					ELSE
					CONVERT(DECIMAL,T.[EXTP])
					END,
					case when T.[RITM]='Y' then 1 else 0 end,
					T.[VSYM],T.[CTGY],T.[GRUP],T.[SGRP],T.[STYL],T.[SALT],
					case when T.[PIVI]='Y' then 1 else 0 end,
					T.[PIVM],case when T.[PIVT]='Y' then 1 else 0 end 
					FROM #pvLineTemp   AS T 
					WHERE T.RID = @RID_1 
				END

			SET @RID_1 = @RID_1+1 -- increment loop count
		END

END;
GO

