﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_InsertPriceChangeRecord]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_InsertPriceChangeRecord'
	EXEC ('CREATE PROCEDURE [dbo].[usp_InsertPriceChangeRecord] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_InsertPriceChangeRecord'
GO
ALTER PROCEDURE [dbo].[usp_InsertPriceChangeRecord] 
    @Skun Char(6),
    @StartDate DATE,
    @Price decimal(10,2),
    @Status Char(1),
    @IsShelfLabel bit,
    @AutoApplyDate DATE,
    @SmallLabel bit,
    @MediumLabel bit,
    @LargeLabel bit,
    @EventNumber Char(6),
    @Priority Char(2)
    
AS
BEGIN

IF NOT EXISTS(SELECT 1 FROM [Oasys].[dbo].[PRCCHG] WHERE SKUN = @Skun AND PDAT = @StartDate AND PRIC = @Price AND PSTA = @Status AND EVNT = @EventNumber)
    INSERT INTO [Oasys].[dbo].[PRCCHG]
               ([SKUN]
               ,[PDAT]
               ,[PRIC]
               ,[PSTA]
               ,[SHEL]
               ,[AUDT]
               ,[AUAP]
               ,[MARK]
               ,[MCOM]
               ,[LABS]
               ,[LABM]
               ,[LABL]
               ,[EVNT]
               ,[PRIO]
               ,[EEID]
               ,[MEID])
         VALUES
               (@Skun 
               ,@StartDate
               ,@Price
               ,@Status
               ,@IsShelfLabel
               ,@AutoApplyDate
               ,Null
               ,Null
               ,0
               ,@SmallLabel
               ,@MediumLabel
               ,@LargeLabel
               ,@EventNumber
               ,@Priority
               ,Null
               ,Null)
RETURN @@RowCount
END
GO

