﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SETSQL]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SETSQL'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SETSQL] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SETSQL'
GO
ALTER PROCEDURE dbo.KevanConversion_SETSQL
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 10 - Switch ON SQL Use
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

UPDATE Parameters
SET LongValue = '1'
WHERE ParameterID = '140'

END;
GO

