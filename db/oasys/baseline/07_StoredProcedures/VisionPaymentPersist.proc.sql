﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionPaymentPersist]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VisionPaymentPersist'
	EXEC ('CREATE PROCEDURE [dbo].[VisionPaymentPersist] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VisionPaymentPersist'
GO
ALTER PROCEDURE [dbo].[VisionPaymentPersist]
	@TranDate		date,
	@TillId			int,
	@TranNumber		int,
	@Number			int,
	@TenderTypeId	int,
	@ValueTender	dec(9,2),
	@IsPivotal		bit
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @rowcount int;
	set @rowcount=0;	

	--update record if exists
	update
		VisionPayment
	set
		TenderTypeId	= @TenderTypeId,
		ValueTender		= @ValueTender,
		IsPivotal		= @IsPivotal
	where
		TranDate	= @TranDate and
		TillId		= @TillId and
		TranNumber	= @TranNumber and
		Number		= @Number;

	--get number of rows affected
	set @rowcount = @@ROWCOUNT;
	
	--if nothing updated then insert
	if @rowcount=0
	begin
		insert into 
			VisionPayment (
			TranDate,
			TillId,
			TranNumber,
			Number,
			TenderTypeId,
			ValueTender,
			IsPivotal
		) values (
			@TranDate,
			@TillId,
			@TranNumber,
			@Number,
			@TenderTypeId,
			@ValueTender,			
			@IsPivotal
		);
			
		set @rowcount = @@ROWCOUNT;
		
		--if inserted then update flash totals
		if @rowcount>0
		begin
			exec FlashTotalTenderPersist @TenderTypeId, @ValueTender;
		end		
	end

	return @rowcount;
END
GO

