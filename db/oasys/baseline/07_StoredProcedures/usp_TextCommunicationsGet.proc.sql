﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_TextCommunicationsGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_TextCommunicationsGet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_TextCommunicationsGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_TextCommunicationsGet'
GO
ALTER PROCedure [dbo].[usp_TextCommunicationsGet]
	@FunctionalAreaId INT,
	@TypeId			  INT,
	@Sequence		  INT = Null
	
As
Begin
	Set NOCOUNT ON;

Select 
	tc.[Id],
	tc.[FunctionalAreaId],
	tc.[TypeId],
	tc.[Sequence],
	tc.[Description],
	tc.[SummaryText],
	tc.[FullText],
	tc.[LineDelimiter],
	tc.[ParagraphDelimiter],
	tc.[FieldStartDelimiter],
	tc.[FieldEndDelimiter],
	tc.[Deleted]
From
	[dbo].[TextCommunications]  tc
Where
	tc.[FunctionalAreaId] = @FunctionalAreaId
And
	tc.[TypeId] = @TypeId
And
	(
		@Sequence Is Null
	Or
		(
			tc.[Sequence] = @Sequence
		And
			Not tc.[Sequence] Is Null
		)
	)
And
	tc.[Deleted] = 0
Order By
	tc.[Sequence]
End
GO

