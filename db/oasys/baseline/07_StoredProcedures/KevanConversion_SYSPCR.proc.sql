﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SYSPCR]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SYSPCR'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SYSPCR] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SYSPCR'
GO
ALTER PROCEDURE dbo.KevanConversion_SYSPCR
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 35 - Post Conversion Table Clear Up Routine
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF;

Delete From CBSCAS
Delete From CBSCSA
Delete From CBSCTL
Delete From CBSHDR
Delete From PVLINE
Delete From PVPAID
Delete From PVTOTS

END;
GO

