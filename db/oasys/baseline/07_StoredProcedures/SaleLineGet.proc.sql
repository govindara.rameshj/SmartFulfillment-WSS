﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleLineGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleLineGet'
	EXEC ('CREATE PROCEDURE [dbo].[SaleLineGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleLineGet'
GO
ALTER PROCEDURE [dbo].[SaleLineGet]
 @TranDate DATE,
 @TillId CHAR (2), 
 @TranNumber CHAR(4)

AS
BEGIN
	SET NOCOUNT ON;

	select
	
	        dl.DATE1,
			dl.TILL,
			dl.[TRAN],
			dl.NUMB,
			dl.SKUN,
			dl.DEPT,
			dl.IBAR,
			dl.SUPV,
			dl.QUAN,
			dl.SPRI,
			dl.PRIC,
			dl.PRVE,
			dl.EXTP,
			dl.EXTC,
			dl.RITM,
			dl.PORC,
			dl.ITAG,
			dl.CATA,
			dl.VSYM,
			dl.TPPD,
			dl.TPME,
			dl.POPD,
			dl.POME,
			dl.QBPD,
			dl.QBME,
			dl.DGPD,
			dl.DGME,
			dl.MBPD,
			dl.MBME,
			dl.HSPD,
			dl.HSME,
			dl.ESPD,
			dl.ESME,
			dl.LREV,
			dl.ESEQ,
			dl.CTGY,
			dl.GRUP,
			dl.SGRP,
			dl.STYL,
			dl.QSUP,
			dl.ESEV,
			dl.IMDN,
			dl.SALT,
			dl.VATN,
			dl.VATV,
			dl.BDCO,
			dl.BDCOInd,
			dl.RCOD,
			st.WSEQ,
			dl.RTI ,
			st.WRAT,
			dl.WEECOST 
					
	from
		DLLINE  dl
	inner join
		STKMAS st on st.SKUN=dl.SKUN			
	where
	dl.DATE1 = @TranDate
	and 
	dl.[TRAN] = @TranNumber
	and
	dl.TILL  = @TillId 	
END
GO

