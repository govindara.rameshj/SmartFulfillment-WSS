﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGetReports]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReportGetReports'
	EXEC ('CREATE PROCEDURE [dbo].[ReportGetReports] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReportGetReports'
GO
ALTER PROCEDURE [dbo].[ReportGetReports]
	@Ids	varchar(100)=null
AS
BEGIN
	SET NOCOUNT ON;

	if (@Ids is null)
	begin
		select Report.* from Report order by id
	end
	else
	begin
		select
			Report.*
		from
			Report
		inner join	
			fnCsvToTable(@Ids) numbers on Report.Id = numbers.String	
		order by
			id
	end
	
END
GO

