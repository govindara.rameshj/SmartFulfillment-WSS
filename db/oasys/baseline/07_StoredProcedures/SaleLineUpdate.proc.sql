﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleLineUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleLineUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SaleLineUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleLineUpdate'
GO
ALTER PROCEDURE [dbo].[SaleLineUpdate]
(
@TransactionDate DATE,
@TillNumber CHAR (2),
@TransactionNumber CHAR(4), 
@LineNumber INT,
@SkuNumber CHAR (6),
@Department CHAR (2),
@BarcodeUsed BIT=0,
@SupervisorNumber CHAR(3),
@Quantity DECIMAL (5),
@SystemLookupPrice DECIMAL(9,2),
@ItemPrice DECIMAL(9,2),
@ItemExcVatPrice DECIMAL(9,2),
@ExtendedValue DECIMAL(9,2),
@ExtendedCost DECIMAL(9,3),
@RelatedItemsSingle BIT=0,
@PriceOverrideCode INT,
@TaggedItem BIT=0,
@CatchAllItem BIT=0,
@VatSymbol CHAR(1)= NULL,
@TempPriceChangeDiff DECIMAL(9,2),
@TPMarginErosionCode CHAR(6)=NULL,
@PriceOverrideDiff DECIMAL(9,2)=0,
@POMarginErosionCode CHAR (6)=NULL,
@QuantityBreakErosion DECIMAL(9,2),
@QBMarginErosionCode CHAR(6)=NULL,
@DealGroupErosion DECIMAL(9,2)=0,
@DGMarginErosionCode CHAR(6)=NULL,
@MultibuyErosion DECIMAL(9,2)=0,
@MBMarginErosionCode CHAR(6)=NULL,
@HierarchyErosion DECIMAL(9,2)=0,
@HIMarginErosionCode CHAR(6)=NULL,
@PriEmpDiscPriceDiff DECIMAL(9,2),
@EmpMarginErosionCode CHAR(6)=NULL,
@LineReversed BIT=0,
@LastEventSeqNo CHAR(6)=NULL,
@HierCategory CHAR(6)=NULL,
@HierGroup CHAR(6)=NULL,
@HierSubGroup CHAR(6)=NULL,
@HierStyle CHAR (6)=NULL,
@PartialQuarantine CHAR(3)=NULL,
@SecondEmpSale DECIMAL(9,2),
@MarkDownStock BIT=0,
@SaleTypeAtt CHAR(1)= NULL,
@VatCode DECIMAL(3,0),
@VatValue DECIMAL(9,2),
@BackDoor CHAR(1),
@BackDoorBool BIT=0,
@LineReverseCode CHAR(2)= NULL,
@RTIFlag CHAR(1),
@WeeRate CHAR(2),
@WeeSequence CHAR(3),
@WeeCost DECIMAL(9,2)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	
	UPDATE DLLINE
	SET
	
	    SKUN = @SkuNumber ,
		DEPT = @Department ,
		IBAR = @BarcodeUsed,
		SUPV = @SupervisorNumber,
		QUAN = @Quantity ,
		SPRI = @SystemLookupPrice ,
		PRIC = @ItemPrice,
		PRVE = @ItemExcVatPrice ,
		EXTP = @ExtendedValue,
		EXTC = @ExtendedCost ,
		RITM = @RelatedItemsSingle ,
		PORC = @PriceOverrideCode ,
		ITAG = @TaggedItem ,
		CATA = @CatchAllItem ,
		VSYM = @VatSymbol,
		TPPD = @TempPriceChangeDiff,
		TPME = @TPMarginErosionCode,
		POPD = @PriceOverrideDiff ,
		POME = @POMarginErosionCode ,
		QBPD = @QuantityBreakErosion,
		QBME = @QBMarginErosionCode,
		DGPD = @DealGroupErosion,
		DGME = @DGMarginErosionCode,
		MBPD = @MultibuyErosion ,
		MBME = @MBMarginErosionCode ,
		HSPD = @HierarchyErosion ,
		HSME = @HIMarginErosionCode,
		ESPD = @PriEmpDiscPriceDiff ,
		ESME = @EmpMarginErosionCode,
		LREV = @LineReversed,
		ESEQ = @LastEventSeqNo,
		CTGY = @HierCategory,
		GRUP = @HierGroup,
		SGRP = @HierSubGroup ,
		STYL = @HierStyle ,
		QSUP = @PartialQuarantine ,
		ESEV = @SecondEmpSale,
		IMDN = @MarkDownStock,
		SALT = @SaleTypeAtt,
		VATN = @VatCode,
		VATV = @VatValue ,
		BDCO = @BackDoor  ,
		BDCOInd = @BackDoorBool  ,
		RCOD = @LineReverseCode ,
		RTI = @RTIFlag,
		WEERATE = @WeeRate ,
		WEESEQN = @WeeSequence ,
		WEECOST = @WeeCost

WHERE
     
	DATE1 = @TransactionDate
    AND TILL  = @TillNumber 
    AND [TRAN]  = @TransactionNumber 
    AND NUMB = @LineNumber
	
return @@rowcount

END
GO

