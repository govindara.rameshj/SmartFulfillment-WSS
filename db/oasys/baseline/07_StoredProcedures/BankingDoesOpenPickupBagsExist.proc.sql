﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BankingDoesOpenPickupBagsExist]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure BankingDoesOpenPickupBagsExist'
	EXEC ('CREATE PROCEDURE [dbo].[BankingDoesOpenPickupBagsExist] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure BankingDoesOpenPickupBagsExist'
GO
ALTER PROCedure BankingDoesOpenPickupBagsExist
   @BankingPeriodId int,
   @Result          bit output
as
begin
    set nocount on

    if exists (select *
               from SafeBags
               where PickupPeriodID = @BankingPeriodId
               and   [Type]         = 'P'
               and   [State]        = 'S')
        set @Result = 1
    else
        set @Result = 0

end
GO

