﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingUniqueSealNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingUniqueSealNumber'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingUniqueSealNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingUniqueSealNumber'
GO
-----------------------------------------------------
--                 GENERAL                         --
-----------------------------------------------------

ALTER PROCedure NewBankingUniqueSealNumber
   @SealNumber char(20),
   @UniqueSeal  bit output
as
begin
set nocount on
if exists (select * from SafeBags where SealNumber = @SealNumber)
   --not unique
   set @UniqueSeal = 0
else
   --unique
   set @UniqueSeal = 1
end
GO

