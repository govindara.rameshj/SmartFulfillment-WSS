﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_PING]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_PING'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_PING] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_PING'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_PING]
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 2nd January 2013
-- 
-- Task     : Respond to Requests.
-----------------------------------------------------------------------------------

AS
BEGIN
SET NOCOUNT ON;

Declare @Store Char(3) = (Select LTRIM(RTRIM(STOR)) From RETOPT Where FKEY = '01')
If @Store is Null Set @Store = '000'

END
GO

