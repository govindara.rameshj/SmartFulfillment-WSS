﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockUpdateSTKMASStkLogValues]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockUpdateSTKMASStkLogValues'
	EXEC ('CREATE PROCEDURE [dbo].[StockUpdateSTKMASStkLogValues] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockUpdateSTKMASStkLogValues'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 02/08/2011
-- Description:	Update Stocllog STKMAS values
-- =============================================
ALTER PROCEDURE StockUpdateSTKMASStkLogValues 
-- Add the parameters for the stored procedure here
   @QuantityOnHand int,
   @MarkDownQuantity int,
   @UnitsSoldYesterday int, 
   @UnitsSoldThisWeek int,  
   @UnitsSoldThisPeriod int,
   @UnitsSoldThisYear int,
   @ValueSoldYesterday decimal(9,2), 
   @ValueSoldThisWeek decimal(9,2), 
   @ValueSoldThisPeriod decimal(9,2),
   @ValueSoldThisYear decimal(9,2),
   @PartCode char(6)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE STKMAS SET 
        ONHA = @QuantityOnHand, 
        MDNQ = @MarkDownQuantity,
        SALU1 = @UnitsSoldYesterday,
        SALU2 = @UnitsSoldThisWeek, 
        SALU4 = @UnitsSoldThisPeriod,
        SALU6 = @UnitsSoldThisYear,
        SALV1 = @ValueSoldYesterday,
        SALV2 = @ValueSoldThisWeek,
        SALV4 = @ValueSoldThisPeriod,
        SALV6 = @ValueSoldThisYear
    WHERE SKUN = @PartCode

    return @@Rowcount

END
GO

