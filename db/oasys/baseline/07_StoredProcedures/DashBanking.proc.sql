﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashBanking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashBanking'
	EXEC ('CREATE PROCEDURE [dbo].[DashBanking] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashBanking'
GO
ALTER PROCedure DashBanking
@DateEnd	date =null
as
begin
   set nocount on

   declare @Output table(
                         RowId int,
                         [Description]   varchar(50),
                         [Date]           date,
                         TodayValue       dec(9,2),
                         WeekToTodayValue dec(9,2),
                         SelectedDate Date
                         )

   declare @LastDate date

   set @LastDate = (select max(PeriodDate) from Safe where Safe.IsClosed = 1)

   insert into @Output values (1,'Last Daily Banking sent to HQ', @LastDate, null, null,@DateEnd)
   insert into @Output values (2,'Safe Check',                    null,      null, null,@DateEnd)
/* insert into @Output values (3,'Cash Shorts (Cash Perf)',       null,      null, null,@DateEnd)*/

   select
          RowId,    
          [Description],
          [Date],
          [Day] = TodayValue,
          WTD   = WeekToTodayValue,
          SelectedDate
   from @Output

end
GO

