﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptOrderUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptOrderUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptOrderUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptOrderUpdate'
GO
ALTER PROCEDURE [dbo].[ReceiptOrderUpdate]
(
    @Number         char(6),
    @DeliveryNote1  char(10),
    @DeliveryNote2  char(10),
    @DeliveryNote3  char(10),
    @DeliveryNote4  char(10),
    @DeliveryNote5  char(10),
    @DeliveryNote6  char(10),
    @DeliveryNote7  char(10),
    @DeliveryNote8  char(10),
    @DeliveryNote9  char(10),
    @Value          dec(9,2),
    @Comments       char(75)
)   
AS
BEGIN
    SET NOCOUNT ON;

    update      drlsum
    
    set         [0dl1] = @DeliveryNote1,
                [0dl2] = @DeliveryNote2,
                [0dl3] = @DeliveryNote3,
                [0dl4] = @DeliveryNote4,
                [0dl5] = @DeliveryNote5,
                [0dl6] = @DeliveryNote6,
                [0dl7] = @DeliveryNote7,
                [0dl8] = @DeliveryNote8,
                [0dl9] = @DeliveryNote9,
                valu = @Value,
                info = @Comments,
                rti = 'N'
    
    where       numb = @Number  

END
GO

