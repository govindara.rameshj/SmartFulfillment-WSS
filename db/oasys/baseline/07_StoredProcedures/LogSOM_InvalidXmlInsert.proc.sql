﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LogSOM_InvalidXmlInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure LogSOM_InvalidXmlInsert'
	EXEC ('CREATE PROCEDURE [dbo].[LogSOM_InvalidXmlInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure LogSOM_InvalidXmlInsert'
GO
-- ===============================================================================
-- Author         : Partha Dutta
-- Date	          : 20/09/2011
-- Change Request : CR0054-02
-- TFS User Story : 2290
-- TFS Task ID    : 2374
-- Description    : Log Sales Order monitor invalid requests to Order Manager
-- ===============================================================================

ALTER PROCedure LogSOM_InvalidXmlInsert

   @VendaOrderNo        nvarchar(20) = null,
   @StoreOrderNo        nvarchar(6),
   @OrderManagerOrderNo int          = null,
   @DateLogged          datetime,
   @RequestTypeID       int,
   @FailedXML           xml          = null,
   @IdentityValue       int output

as
begin
   set nocount on

   insert LogSOM_InvalidXml(VendaOrderNo, StoreOrderNo, OrderManagerOrderNo, DateLogged, RequestTypeID, FailedXML)
                   values (@VendaOrderNo, @StoreOrderNo, @OrderManagerOrderNo, @DateLogged, @RequestTypeID, @FailedXML)

   set @IdentityValue = @@IDENTITY

end
GO

