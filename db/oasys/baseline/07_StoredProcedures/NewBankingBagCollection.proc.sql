﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingBagCollection]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingBagCollection'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingBagCollection] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingBagCollection'
GO
-----------------------------------------------------
--                 BANKING BAG COLLECTION          --
-----------------------------------------------------

ALTER PROCedure NewBankingBagCollection
as
begin
set nocount on
--uncollected banking bags
select BagID         = a.ID,
       BagPeriodID   = a.PickupPeriodID,
       BagPeriodDate = (select StartDate from SystemPeriods where ID = a.PickupPeriodID),
       BagSealNumber = a.SealNumber ,
       BagValue      = a.Value,
       BagComment    = a.Comments 
from SafeBags a
where a.[Type]      = 'B'
and   a.[State]     = 'M'
and   a.SealNumber <> '00000000'   --banking bag containing non-cash(except cheque) tenders do not need to be physically collected
                                   --this bag is created for process transmission only
end
GO

