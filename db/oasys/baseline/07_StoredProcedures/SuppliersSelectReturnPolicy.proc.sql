﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SuppliersSelectReturnPolicy]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SuppliersSelectReturnPolicy'
	EXEC ('CREATE PROCEDURE [dbo].[SuppliersSelectReturnPolicy] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SuppliersSelectReturnPolicy'
GO
-- =============================================
-- Author:		Darryl James Heath
-- Create date: 19/05/2009
-- Description:	Returns supplier return policy
-- =============================================
ALTER PROCEDURE [SuppliersSelectReturnPolicy]
	@SupplierNumber		CHAR(5)
AS
SELECT		SUPNOT.SUPN, SUPNOT.[TYPE], SUPNOT.SEQN, SUPNOT.[TEXT]

FROM        SUPNOT

WHERE		SUPNOT.SUPN = @SupplierNumber
AND			SUPNOT.[TYPE] = '001'

ORDER BY	SUPNOT.SEQN
GO

