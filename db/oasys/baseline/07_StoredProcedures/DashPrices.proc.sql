﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashPrices]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashPrices'
	EXEC ('CREATE PROCEDURE [dbo].[DashPrices] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashPrices'
GO
-- Verion Control Information
-- =============================================
-- Author:  	Kevan Madelin
-- Create date: 23/08/2010
-- Version:	3.0.0.0
-- Description: This procedure is used to get the Price Overrides data for the Managers Dash.
-- =============================================
-- Author:  	Sean Moir
-- Create date: 7/7/2011
-- Version:	3.0.0.1
-- Description: Change polarity of price and receipt values
--				Changed description for Markdowns
-- =============================================

ALTER PROCEDURE [dbo].[DashPrices]
	@DateEnd	date
as
begin
set nocount on

	declare @table table(
		Description	varchar(50),
		Qty			int,
		QtyWtd		int,
		Value		dec(9,2),
		ValueWtd	dec(9,2));
	declare
		@StartDate			date,
		@PriceDayQty		int,
		@PriceDayValue		dec(9,2),
		@PriceWeekQty		int,		
		@PriceWeekValue		dec(9,2),
		@ReceiptDayQty		int,
		@ReceiptDayValue	dec(9,2),		
		@ReceiptWeekQty		int,		
		@ReceiptWeekValue	dec(9,2);;
	
	--set startdate to sunday before enddate
	set	@StartDate = @DateEnd;
	While datepart(weekday, @StartDate) <> 1
	begin
		set @StartDate = dateadd(day, -1, @StartDate)
	end	
	
	--get price violation today
	select
		@PriceDayQty	= count(dl.quan),
	 	@PriceDayValue	= sum(dl.popd * dl.quan)*-1
	from	
		dltots dt
	inner join
		dlline dl	on	dl.date1	= dt.date1 
					and	dl.till		= dt.till
					and	dl.[tran]	= dt.[tran]
					and dl.imdn		= 0
					and dl.popd		> 0
					and dl.lrev		= 0
					and dl.porc		<> 0	
	where	
		dt.DATE1		= @DateEnd
		and	dt.CASH		<> '000'
		and	dt.VOID		= 0
		and	dt.PARK		= 0
		and	dt.TMOD		= 0
		and dl.IMDN		= 0

	--get price violation week
	select
		@PriceWeekQty	= count(dl.quan),	
	 	@PriceWeekValue	= sum(dl.popd * dl.quan)*-1
	from	
		dltots dt
	inner join
		dlline dl	on	dl.date1	= dt.date1 
					and	dl.till		= dt.till
					and	dl.[tran]	= dt.[tran]
					and dl.imdn		= 0
					and dl.popd		> 0
					and dl.lrev		= 0
					and dl.porc		<> 0	
	where	
		dt.DATE1		<= @DateEnd
		and dt.date1	>= @StartDate
		and	dt.CASH		<> '000'
		and	dt.VOID		= 0
		and	dt.PARK		= 0
		and	dt.TMOD		= 0
		and dl.IMDN		= 0
	
	--get markdown sales for the day
	select
		@ReceiptDayQty		= count(dl.quan),	
	 	@ReceiptDayValue	= sum(dl.quan * dl.popd)*-1
	from	
		DLLINE dl
	where	
		dl.DATE1	= @DateEnd
		and dl.IMDN		= 1
	
	--get markdown adjustment week
	select
		@ReceiptWeekQty		= count(dl.quan),	
	 	@ReceiptWeekValue	= sum(dl.quan * dl.popd)*-1
	from	
		dlline dl
	where	
		dl.DATE1		<= @DateEnd
		and dl.date1	>= @StartDate		
		and dl.IMDN		= 1

	--return values
	insert into @table values ('Price Violations',	@PriceDayQty, @PriceWeekQty, @PriceDayValue * -1, @PriceWeekValue * -1) ;
	insert into @table values ('Markdowns', @ReceiptDayQty, @ReceiptWeekQty, @ReceiptDayValue * -1, @ReceiptWeekValue * -1) ;

	select * from @table
end
GO

