﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_PVTHIS]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_PVTHIS'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_PVTHIS] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_PVTHIS'
GO
ALTER PROCEDURE dbo.KevanConversion_PVTHIS
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 16 - Kevan's Delete Historical PV Records from Database..
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

Delete From PVTOTS where CONVERT(DATETIME, DATE1 ,3) <= DATEADD(Day,-365,getdate()) 
Delete From PVPAID where CONVERT(DATETIME, DATE1 ,3) <= DATEADD(Day,-365,getdate()) 
Delete From PVLINE where CONVERT(DATETIME, DATE1 ,3) <= DATEADD(Day,-365,getdate())

END;
GO

