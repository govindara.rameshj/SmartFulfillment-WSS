﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdatePriceChangeRecord]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdatePriceChangeRecord'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdatePriceChangeRecord] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdatePriceChangeRecord'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 30/03/2012
-- Description:	Update Existing Price Change Record
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdatePriceChangeRecord] 
	@Skun Char(6),
	@StartDate DATE,
	@Price decimal(10,2),
	@Status Char(1),
	@EventNumber Char(6),
	@Priority Char(2),
	@AutoApplyDate DATE
AS
BEGIN
	SET NOCOUNT ON;

Update [Oasys].[dbo].[PRCCHG]
        set 
			[EVNT] = @EventNumber
           ,[PRIC] = @Price
           ,[PSTA] = @Status
           ,[PRIO] = @Priority
        Where
            [Skun]= @Skun And 
            [PDAT] = @StartDate And
			[PSTA] = @Status
			
Return @@RowCount
End
GO

