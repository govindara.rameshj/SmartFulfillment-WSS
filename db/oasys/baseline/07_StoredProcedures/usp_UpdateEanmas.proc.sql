﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateEanmas]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdateEanmas'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdateEanmas] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdateEanmas'
GO
ALTER PROCEDURE usp_UpdateEanmas @SkuNumber varchar( 6
                                                    ) , 
                                  @BarCode varchar( 16
                                                  )
AS
BEGIN

    INSERT INTO EANMAS( NUMB , 
                        SKUN
                      )
    VALUES( @BarCode , 
            @SkuNumber
          );

END;
GO

