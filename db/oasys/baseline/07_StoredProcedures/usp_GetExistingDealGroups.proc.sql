﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetExistingDealGroups]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetExistingDealGroups'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetExistingDealGroups] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetExistingDealGroups'
GO
-- ===============================================================================
-- Author         : Dhanesh Ramachandran
-- Date	          : 03/01/2012
-- Project        : PO14-04
-- TFS User Story : 2155
-- TFS Task ID    : 3820
-- Description    : Rollout Script for Override Deal Groups. Pero field should be
--                  added to eventdetailoverride table
-- ===============================================================================
ALTER PROCEDURE usp_GetExistingDealGroups 

  --@Skulist AS VARCHAR(MAX)
    @SkuNumbers varchar(max)

AS 
  BEGIN 
      SET @SkuNumbers = REPLACE(@SkuNumbers, ' ', '') 

      SELECT numb   eventnumber, 
             dlgn   dealgroupnumber, 
             [TYPE] dealtype, 
             key1   itemkey, 
             idel   deleted, 
             quan   quantity, 
             vero   erosionvalue, 
             pero   erosionpercentage 
      FROM   evtdlg 
      WHERE  numb + dlgn IN ((SELECT DISTINCT numb + dlgn 
                              FROM   evtdlg 
                              WHERE  key1 IN (SELECT item 
                                              FROM   Udf_splitvarchartotable ( 
                                                     @SkuNumbers 
                                                     , ',')) 
                                     AND [Type] = 'S') 
                             UNION 
                             SELECT DISTINCT numb + dlgn 
                             FROM   evtmmg 
                                    INNER JOIN evtdlg 
                                      ON evtdlg.key1 = evtmmg.mmgn 
                                         AND evtdlg.TYPE = 'M' 
                             WHERE  skun IN (SELECT item 
                                             FROM   Udf_splitvarchartotable ( 
                                                    @SkuNumbers, 
                                                    ','))) 
             
   
  END
GO

