﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierMasterOrderingSelectAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierMasterOrderingSelectAll'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierMasterOrderingSelectAll] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierMasterOrderingSelectAll'
GO
ALTER PROCEDURE [dbo].[SupplierMasterOrderingSelectAll]
@Type CHAR (1)='A'
AS
DECLARE @DayWeek	INT;
SET @DayWeek = DATEPART(weekday, getdate());

SELECT		SUPMAS.* 
			
FROM		SUPMAS left outer JOIN
            SUPDET ON SUPDET.SUPN = SUPMAS.SUPN AND SUPDET.DEPO = SUPMAS.ODNO

WHERE		SUPDET.TYPE = 'O'
and			(	select count(stkmas.skun) from stkmas 
				where	stkmas.idel=0 
				and		stkmas.iobs=0
				and		stkmas.iris=0
				and		stkmas.inon=0
				and		stkmas.noor=0
				and		stkmas.sup1=supmas.supn ) > 0

AND			CASE @DayWeek
				WHEN 1 THEN SUPDET.ReviewDay0
				WHEN 2 THEN SUPDET.ReviewDay1
				WHEN 3 THEN SUPDET.ReviewDay2
				WHEN 4 THEN SUPDET.ReviewDay3
				WHEN 5 THEN SUPDET.ReviewDay4
				WHEN 6 THEN SUPDET.ReviewDay5
				WHEN 7 THEN SUPDET.ReviewDay6
			END = 1	

AND			(@Type='A' 
			OR (@Type='D' AND SUPDET.BBCC = '') 
			OR (@Type='W' AND SUPDET.BBCC <>''))
			
AND			(SUPDET.OCSD IS NULL OR SUPDET.OCSD > GETDATE()
			OR (SUPDET.OCED IS NOT NULL AND SUPDET.OCED < GETDATE()))			

ORDER BY SUPMAS.NAME
GO

