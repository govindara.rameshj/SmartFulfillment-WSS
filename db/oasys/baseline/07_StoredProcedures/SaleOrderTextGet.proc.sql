﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleOrderTextGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleOrderTextGet'
	EXEC ('CREATE PROCEDURE [dbo].[SaleOrderTextGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleOrderTextGet'
GO
ALTER PROCEDURE [dbo].[SaleOrderTextGet]
@OrderNumber CHAR (6), @Type CHAR (2)=null
AS
BEGIN
	SET NOCOUNT ON;

	select
		ct.NUMB		as OrderNumber,
		ct.LINE		as Number,
		ct.[TYPE]	as 'Type',
		ct.[TEXT]	as 'Text',
		ct.SellingStoreId,
		ct.SellingStoreOrderId
	from
		CORTXT ct
	where
		ct.NUMB = @OrderNumber
		and (@Type is null or (@type is not null and ct.[TYPE] = @Type))
	order by
		ct.LINE
	
END
GO

