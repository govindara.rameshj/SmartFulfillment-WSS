﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGetReportHyperlinks]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReportGetReportHyperlinks'
	EXEC ('CREATE PROCEDURE [dbo].[ReportGetReportHyperlinks] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReportGetReportHyperlinks'
GO
ALTER PROCEDURE [dbo].[ReportGetReportHyperlinks]
	@ReportId	int,
	@TableId	int
AS
BEGIN
	SET NOCOUNT ON;

	select
		rh.ColumnName,
		rh.ReportId,
		rh.RowId,
		rh.Value
	from
		ReportHyperlink rh
	where 
			rh.ReportId=@ReportId
		and rh.TableId = @TableId

END
GO

