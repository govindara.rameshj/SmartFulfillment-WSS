﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleLineInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleLineInsert'
	EXEC ('CREATE PROCEDURE [dbo].[SaleLineInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleLineInsert'
GO
-- ===============================================================================
-- Author         : Partha Dutta
-- Date	          : 23/09/2011
-- Change Request : CR0054-01
-- TFS User Story : 2574
-- TFS Task ID    : 2595
-- Description    : BO-120 iteration's database layer (rollout/rollback scripts) refactored to deal with deployment to 
--                     A. Standard Store
--                     B. eStore 120
--                     C. eWarehouse 076
-- ===============================================================================

ALTER PROCEDURE [dbo].[SaleLineInsert]
(
@TransactionDate DATE,
@TillNumber CHAR (2),
@TransactionNumber CHAR(4), 
@LineNumber INT,
@SkuNumber CHAR (6),
@Department CHAR (2)='00',
@BarcodeUsed BIT=0,
@SupervisorNumber CHAR(3)='000',
@Quantity DECIMAL (5),
@SystemLookupPrice DECIMAL(9,2),
@ItemPrice DECIMAL(9,2),
@ItemExcVatPrice DECIMAL(9,2),
@ExtendedValue DECIMAL(9,2),
@ExtendedCost DECIMAL(9,3),
@RelatedItemsSingle BIT=0,
@PriceOverrideCode INT = 0,
@TaggedItem BIT=0,
@CatchAllItem BIT=0,
@VatSymbol CHAR(1) = NULL,
@TempPriceChangeDiff DECIMAL(9,2)=0,
@TPMarginErosionCode CHAR(6)=NULL,
@PriceOverrideDiff DECIMAL(9,2)=0,
@POMarginErosionCode CHAR (6)=NULL,
@QuantityBreakErosion DECIMAL(9,2)=0,
@QBMarginErosionCode CHAR(6)=NULL,
@DealGroupErosion DECIMAL(9,2)=0,
@DGMarginErosionCode CHAR(6)=NULL,
@MultibuyErosion DECIMAL(9,2)=0,
@MBMarginErosionCode CHAR(6)=NULL,
@HierarchyErosion DECIMAL(9,2)=0,
@HIMarginErosionCode CHAR(6)=NULL,
@PriEmpDiscPriceDiff DECIMAL(9,2)=0,
@EmpMarginErosionCode CHAR(6)=NULL,
@LineReversed BIT=0,
@LastEventSeqNo CHAR(6)=NULL,
@HierCategory CHAR(6)=NULL,
@HierGroup CHAR(6)=NULL,
@HierSubGroup CHAR(6)=NULL,
@HierStyle CHAR (6)=NULL,
@PartialQuarantine CHAR(3)=NULL,
@SecondEmpSale DECIMAL(9,2)=0,
@MarkDownStock BIT=0,
@SaleTypeAtt CHAR(1) = NULL,
@VatCode DECIMAL(3,0),
@VatValue DECIMAL(9,2),
@BackDoor CHAR(1)='N',
@BackDoorBool BIT=0,
@LineReverseCode CHAR(2)= NULL,
@RTIFlag CHAR(1)='S',
@WeeRate CHAR(2)='0',
@WeeSequence CHAR(3)='0',
@WeeCost DECIMAL(9,2)='0'
)
AS
BEGIN
	SET NOCOUNT ON;
	
	--insert 
	insert into	DLLINE 
		(
		DATE1,
		TILL,
		[TRAN],
		NUMB,
		SKUN,
		DEPT,
		IBAR,
		SUPV,
		QUAN,
		SPRI,
		PRIC,
		PRVE,
		EXTP,
		EXTC,
		RITM,
		PORC,
		ITAG,
		CATA,
		VSYM,
		TPPD,
		TPME,
		POPD,
		POME,
		QBPD,
		QBME,
		DGPD,
		DGME,
		MBPD,
		MBME,
		HSPD,
		HSME,
		ESPD,
		ESME,
		LREV,
		ESEQ,
		CTGY,
		GRUP,
		SGRP,
		STYL,
		QSUP,
		ESEV,
		IMDN,
		SALT,
		VATN,
		VATV,
		BDCO,
		BDCOInd,
		RCOD,
		RTI,
		WEERATE,
		WEESEQN,
		WEECOST
		)
	values		
	
	(
@TransactionDate,
@TillNumber,
@TransactionNumber, 
@LineNumber,
@SkuNumber ,
@Department,
@BarcodeUsed,
@SupervisorNumber ,
@Quantity ,
@SystemLookupPrice ,
@ItemPrice,
@ItemExcVatPrice ,
@ExtendedValue,
@ExtendedCost ,
@RelatedItemsSingle ,
@PriceOverrideCode ,
@TaggedItem ,
@CatchAllItem ,
@VatSymbol,
@TempPriceChangeDiff,
@TPMarginErosionCode ,
@PriceOverrideDiff ,
@POMarginErosionCode ,
@QuantityBreakErosion,
@QBMarginErosionCode,
@DealGroupErosion ,
@DGMarginErosionCode ,
@MultibuyErosion ,
@MBMarginErosionCode ,
@HierarchyErosion ,
@HIMarginErosionCode,
@PriEmpDiscPriceDiff ,
@EmpMarginErosionCode,
@LineReversed,
@LastEventSeqNo,
@HierCategory ,
@HierGroup ,
@HierSubGroup ,
@HierStyle ,
@PartialQuarantine ,
@SecondEmpSale,
@MarkDownStock,
@SaleTypeAtt,
@VatCode ,
@VatValue ,
@BackDoor ,
@BackDoorBool ,
@LineReverseCode ,
@RTIFlag,
@WeeRate ,
@WeeSequence ,
@WeeCost 

)
return @@rowcount		
END
GO

