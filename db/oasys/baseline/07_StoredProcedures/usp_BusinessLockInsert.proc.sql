﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_BusinessLockInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_BusinessLockInsert'
	EXEC ('CREATE PROCEDURE [dbo].[usp_BusinessLockInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_BusinessLockInsert'
GO
ALTER PROCedure usp_BusinessLockInsert

   @WorkStationID int,
   @AssemblyName  varchar(100),
   @Parameters    varchar(50),
   @RecordID      int output
as
begin
   set nocount on

   insert BusinessLock (WorkStationID, AssemblyName, Parameters) values (@WorkStationID, @AssemblyName, @Parameters)

   set @RecordID = @@Identity

end
GO

