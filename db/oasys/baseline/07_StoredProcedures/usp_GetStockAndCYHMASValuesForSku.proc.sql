﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetStockAndCYHMASValuesForSku]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetStockAndCYHMASValuesForSku'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetStockAndCYHMASValuesForSku] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetStockAndCYHMASValuesForSku'
GO
-- =============================================
-- Author		Michael O'Cain
-- Create date 09062011
-- Description	Get Stock and CYHMAS values for 
-- Stock Adjustments calculations
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetStockAndCYHMASValuesForSku] 
	-- Add the parameters for the stored procedure here
	@ProductCode Char(6) = Null,
	@SaltCode Char(1) = null
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	If not exists (select skun from CYHMAS where skun=@ProductCode)
		Insert into CYHMAS (SKUN,ADAT1) 
					Values(@ProductCode,GetDate())


	SELECT 
		st.SKUN		as 'SKUN',
		st.ONHA		as 'OnHand',
		st.SALT		as 'SaleTypeAttribute',
		st.ICAT		as 'DummyItem',
		st.IDEL		as 'HODeleted',
		st.AADJ		as 'AllowAdjustment',
		st.WTFQ		as 'WriteOffStock',
		st.PRIC		as 'Price',
		st.COST		as 'Cost',
		st.MADQ1	as 'AdjustmentQty1',
		st.MADV1	as 'AdjustmentValue1',
		st.MDNQ		as 'MarkDownQty',
		st.MCCV1	as 'CyclicalCount',
		st.TACT		as 'TodayActivityFlag',
		st.RETQ		as 'OpenReturnQty',
		st.DEPT		as 'Department',
		cy.ADAT1	as 'ActivityDate',
		cy.AQ021	as 'ActivityAdjQty',
		cy.AV021	as 'ActivityAdjValue'
			
	From 
		STKMAS st inner join CYHMAS cy on st.SKUN = cy.SKUN 
	where
		((@ProductCode is null) or (@ProductCode is not null and st.SKUN=@ProductCode))
		And ((@SaltCode is null) or (@SaltCode is not null and st.SALT=@SaltCode))

END
GO

