﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SuppliersSelectByNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SuppliersSelectByNumber'
	EXEC ('CREATE PROCEDURE [dbo].[SuppliersSelectByNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SuppliersSelectByNumber'
GO
ALTER PROCEDURE [dbo].[SuppliersSelectByNumber]
@numbers varchar (8000)
AS

SELECT		SUPN, ALPH, NAME, DELC, HLIN, MERC, TYPE, ODNO, BBCN, 
			QCTL, QFLG, RDNO, DLPO, DLRE, OPON, OPOV, QTYO1, QTYO2, 
			QTYO3, QTYO4, QTYO5, QTYR1, QTYR2, QTYR3, QTYR4, QTYR5, 
			LREC1, LREC2, LREC3, LREC4, LREC5, LNRE1, LNRE2, LNRE3, LNRE4, LNRE5, 
			LOVR1, LOVR2, LOVR3, LOVR4, LOVR5, LUND1, LUND2, LUND3, LUND4, LUND5, 
			PREC1, PREC2, PREC3, PREC4, PREC5, VREC1, VREC2, VREC3, VREC4, VREC5, 
			DYTR1, DYTR2, DYTR3, DYTR4, DYTR5, DYSH1, DYSH2, DYSH3, DYSH4, DYSH5, 
			DYLO1, DYLO2, DYLO3, DYLO4, DYLO5, VPCC, SODT, SOQDate, PalletCheck

FROM		SUPMAS
inner join	fnCsvToTable(@numbers) numbers on SUPMAS.SUPN= numbers.String		

ORDER BY SUPMAS.NAME
GO

