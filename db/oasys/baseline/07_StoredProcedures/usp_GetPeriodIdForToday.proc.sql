﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetPeriodIdForToday]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetPeriodIdForToday'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetPeriodIdForToday] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetPeriodIdForToday'
GO
-- =============================================
-- Author:	Michael O'Cain
-- Create date: 05-07-2011
-- Description:	Return the Period Id for todays date.
-- =============================================
ALTER PROCEDURE usp_GetPeriodIdForToday 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		id
	FROM
		SystemPeriods
	WHERE
		StartDate <= GETDATE() and 
		EndDate >= GETDATE()
END
GO

