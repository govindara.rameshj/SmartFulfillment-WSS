﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEmployeeCode]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEmployeeCode'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEmployeeCode] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEmployeeCode'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 23/06/2011
-- Description:	Get Employees Initials
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetEmployeeCode] 
	-- Add the parameters for the stored procedure here
	@Id int 
	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		EmployeeCode
	FROM
		SystemUsers	
	WHERE 
		Id = @Id
END
GO

