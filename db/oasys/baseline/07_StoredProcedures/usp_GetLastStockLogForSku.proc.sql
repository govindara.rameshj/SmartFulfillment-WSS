﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetLastStockLogForSku]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetLastStockLogForSku'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetLastStockLogForSku] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetLastStockLogForSku'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 09/06/2011
-- Description:	Gets the last Stock Log record 
--				for the Skun provided.
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetLastStockLogForSku] 
	-- Add the parameters for the stored procedure here
	@ProductCode Char(6) = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		TOP 1
		TKEY	as 'Id',
		SKUN	as 'SKUN',
		DAYN	as 'DayNumber',
		[TYPE]	as 'TYPE',
		DATE1	as 'LogDate',
		[TIME]	as 'LogTime',
		KEYS	as 'Keys',
		EEID	as 'EmployeeId',
		ICOM	as 'IsSentToHO',
		SSTK	as 'StartingStock',
		SRET	as 'StartingOpenReturns',
		SPRI	as 'StartingPrice',
		ESTK	as 'EndingStock',
		ERET	as 'EndingOpenReturns',
		EPRI	as 'EndingPrice',
		SMDN	as 'StartingMarkDown',
		SWTF	as 'StartingWriteOff',
		EMDN	as 'EndingMarkdown',
		EWTF	as 'EndingWriteOff',
		RTI		as 'RTI'
		 
	FROM
		STKLOG
	WHERE
		SKUN = @ProductCode
	ORDER BY
		TKEY DESC
END
GO

