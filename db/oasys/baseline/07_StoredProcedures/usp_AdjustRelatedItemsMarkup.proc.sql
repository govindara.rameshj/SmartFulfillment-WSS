﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_AdjustRelatedItemsMarkup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_AdjustRelatedItemsMarkup'
	EXEC ('CREATE PROCEDURE [dbo].[usp_AdjustRelatedItemsMarkup] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_AdjustRelatedItemsMarkup'
GO
ALTER PROCedure [dbo].[usp_AdjustRelatedItemsMarkup]
	@Skun As Int,
	@Value As Decimal(9, 2)
As
	Begin
	
		Set NoCount On
		
		Update
			RELITM
		Set
			WTDS = WTDS + NSPP,
			WTDM = WTDM + @Value,
			PTDM = PTDM + @Value,
			YTDM = YTDM + @Value
		Where
			SPOS = @Skun
End
GO

