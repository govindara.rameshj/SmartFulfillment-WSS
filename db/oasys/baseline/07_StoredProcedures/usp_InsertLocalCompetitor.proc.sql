﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_InsertLocalCompetitor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_InsertLocalCompetitor'
	EXEC ('CREATE PROCEDURE [dbo].[usp_InsertLocalCompetitor] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_InsertLocalCompetitor'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 25-04-2012
-- Description:	Insert New Local Competitor
-- =============================================
ALTER PROCEDURE usp_InsertLocalCompetitor 
	-- Add the parameters for the stored procedure here
	@Name Char(30)
AS
BEGIN
	INSERT INTO [dbo].[CompetitorLocalList]
           ([Name]
           ,[DeleteFlag]
           ,[TPGroup])
     VALUES
           (@Name
           ,0
           ,0)
		   
	Return @@Rowcount
END
GO

