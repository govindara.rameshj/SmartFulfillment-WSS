﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingCashDropAndUnFloatedPickupsDisplay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingCashDropAndUnFloatedPickupsDisplay'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingCashDropAndUnFloatedPickupsDisplay] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingCashDropAndUnFloatedPickupsDisplay'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 20/09/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7972
-- Description   : Add the SafeBag.Comments column from the
--				 : returned data.
-- =============================================
ALTER PROCedure [dbo].[NewBankingCashDropAndUnFloatedPickupsDisplay]
   @PeriodID Int
As
Begin
Set NoCount On
--cashier Accountability model Only
--cash Drops - cashiers (floated Or unfloated) Or Design consultants
Select
	AssignedToUserID               = b.ID,
	AssignedToUserName             = b.Name,
	TillVariance                   = Cast(Null As Decimal(9,2)),
	PickupSealNumber               = a.SealNumber,
	PickupAssignedByUserID         = c.ID,
	PickupAssignedByUserName       = c.Name,
	PickupAssignedBySecondUserID   = d.ID,
	PickupAssignedBySecondUserName = d.Name,
	Comments					   = a.Comments
From
	SafeBags a
	--cashier / Design consultant 
	Inner Join
		SystemUsers b
	On
		b.ID = a.AccountabilityID
	--created by
	Inner Join
		SystemUsers c
	On
		c.ID = a.InUserID1
	--created by Second check
	Inner Join
		SystemUsers d
	On
		d.ID = a.InUserID2
Where
	a.[Type]              = 'P'
And
	a.[State]            <> 'C'
And
	a.PickupPeriodID      = @PeriodID
And
	IsNull(a.CashDrop, 0) = 1         --cash Drops
Union All
--unfloated cashiers / Design consultants with pickup bags Assigned
Select
	AssignedToUserID = b.ID,
	AssignedToUserName = b.Name,
	TillVariance = 
		(
			Select
				Sum(Value)                                        --total pickup (cash Drops & e.o.d pickup)
			From
				SafeBags
			Where
				[Type]           = 'P'
			And
				[State]         <> 'C'
			And
				PickupPeriodID   = @PeriodID
			And
				AccountabilityID = a.AccountabilityID
		) -
		(
			Select
				GrossSalesAmount                                  --system Sales
			From
				CashBalCashier
			Where
				PeriodID   = @PeriodID
			And
				CurrencyID = 
					(
						Select
							ID
						From
							SystemCurrency
						Where
							IsDefault = 1
					)
			And
				CashierID  = a.AccountabilityID
		),
       PickupSealNumber               = a.SealNumber,
       PickupAssignedByUserID         = c.ID,
       PickupAssignedByUserName       = c.Name,
       PickupAssignedBySecondUserID   = d.ID,
       PickupAssignedBySecondUserName = d.Name,
       Comments						  = a.Comments
From
	SafeBags a
	--cashier / Design consultant 
	Inner Join
		SystemUsers b
	On
		b.ID = a.AccountabilityID 
	--created by
	Inner Join
		SystemUsers c
	On
		c.ID = a.InUserID1
	--created by Second check
	Inner Join
		SystemUsers d
	On
		d.ID = a.InUserID2
	--filter Out floated cashiers with pickup bag
	Left Outer Join
		(
			Select 
				*
			From 
				SafeBags 
			Where
				[Type] = 'F' 
			And 
				[State] = 'R'
		) e
	On
		e.OutPeriodID = a.PickupPeriodID
	And
		e.AccountabilityID = a.AccountabilityID
Where
	a.[Type] = 'P'
And
	a.[State] <> 'C'
And
	a.PickupPeriodID = @PeriodID
And
	IsNull(a.CashDrop, 0) = 0         --end Of Day pickup bag, Should Only have One entry Only
And
	e.ID Is Null
End
GO

