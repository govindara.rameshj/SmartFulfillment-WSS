﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_STMAS_ONOR]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_STMAS_ONOR'
	EXEC ('CREATE PROCEDURE [dbo].[usp_STMAS_ONOR] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_STMAS_ONOR'
GO
ALTER PROCEDURE [dbo].[usp_STMAS_ONOR]
AS
BEGIN
create table #purlinTemp
(
    RID INT IDENTITY(1,1),
    [SKUN] [char](6) NOT NULL,
	[QTYO] [int] NULL,
	--[DELE] [bit] NOT NULL,	
)

create table #stkmasTemp
(
    [SKUN] [char](6) NOT NULL,
    [ONOR] [int] NULL
	
)

INSERT INTO #purlinTemp
SELECT SKUN, SUM(pl.QTYO)
From PURLIN as pl inner join PURHDR as ph on ph.TKEY = pl.HKEY  
where pl.DELE = 0 and ph.RCOM = '0' and (pl.CONF = 'C' or pl.CONF = ' ' or pl.CONF = 'A' or pl.CONF is NULL)
group by pl.SKUN
order by SKUN asc

INSERT INTO #stkmasTemp
SELECT SKUN,ONOR FROM STKMAS

    DECLARE @RID INT
	DECLARE @MAXID INT
	SELECT @MAXID = MAX(RID)FROM #purlinTemp -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID = 1
	DECLARE @SKUN CHAR(6)	

UPDATE STKMAS
SET ONOR = 0

-- looping from first record to last record.
WHILE(@RID<=@MAXID)
		BEGIN
			-- selecting the old record to check whether it ther in the new table	
			SELECT @SKUN = SKUN FROM #purlinTemp  WHERE RID = @RID
	        -- if exists then update, by taking the values from #purlintemp
          	IF EXISTS(SELECT 1 FROM #stkmasTemp  WHERE SKUN  = @SKUN)
				BEGIN 
				    UPDATE STKMAS SET [ONOR]  = T.[QTYO]
 				    FROM #purlinTemp AS T WHERE (STKMAS.SKUN = @SKUN AND T.SKUN = @SKUN)
				END	
				SET @RID = @RID+1
		END
	END
GO

