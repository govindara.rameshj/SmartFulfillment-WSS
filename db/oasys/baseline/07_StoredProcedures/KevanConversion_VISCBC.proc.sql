﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_VISCBC]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_VISCBC'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_VISCBC] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_VISCBC'
GO
ALTER PROCEDURE dbo.KevanConversion_VISCBC
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 25 - Updating Un-Processed Vision Sales to New Banking System - Part 1
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.1 
-- Revision : 1.0
-- Author   : Dhanesh Ramachandran
-- Date	    : 20th July 2011
-- 
-- Task     : 25 - Updating Un-Processed Vision Sales to New Banking System - Part 1
-----------------------------------------------------------------------------------


AS
BEGIN

SET NOCOUNT OFF
        
   
create table #BankingTemp
(
   
    RID INT IDENTITY(1,1),
    [PeriodID] [int] NOT NULL,
	[CashierID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	--[ID] [int] NOT NULL,	
	[Value] [decimal](9,2) NULL,           -- use DT.TOTL & VT.Value
	[Discount] [decimal](9,2) NULL,        -- use DT.DISC & VT.ValueDiscount
	[Type] [char](9) NULL,	  -- use DT.TCOD & VT.Type (Convert Data)
	[Misc] [decimal](9, 2)  NULL,	  -- use DT.MISC & Add '20' as Data
    --[Updated] [decimal](9, 2)  NULL,  -- use DT.CBBU & Add ' ' Blank Data
)

create table #CashBalCashTemp
(
    [PeriodID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[CashierID] [int] NOT NULL,
	[GrossSalesAmount] [decimal](8, 2) NOT NULL,
	[DiscountAmount] [decimal](8, 2) NOT NULL,
	[SalesCount] [decimal](4, 0) NOT NULL,
	[SalesAmount] [decimal](8, 2) NOT NULL,
	[SalesCorrectCount] [decimal](4, 0) NOT NULL,
	[SalesCorrectAmount] [decimal](8, 2) NOT NULL,
	[RefundCount] [decimal](4, 0) NOT NULL,
	[RefundAmount] [decimal](8, 2) NOT NULL,
	[RefundCorrectCount] [decimal](4, 0) NOT NULL,
	[RefundCorrectAmount] [decimal](8, 2) NOT NULL,
	[FloatIssued] [decimal](8, 2) NOT NULL,
	[FloatReturned] [decimal](8, 2) NOT NULL,
	[MiscIncomeCount01] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount02] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount03] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount04] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount05] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount06] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount07] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount08] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount09] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount10] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount11] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount12] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount13] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount14] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount15] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount16] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount17] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount18] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount19] [decimal](4, 0) NOT NULL,
	[MiscIncomeCount20] [decimal](4, 0) NOT NULL,
	[MiscIncomeValue01] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue02] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue03] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue04] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue05] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue06] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue07] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue08] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue09] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue10] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue11] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue12] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue13] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue14] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue15] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue16] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue17] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue18] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue19] [decimal](8, 2) NOT NULL,
	[MiscIncomeValue20] [decimal](8, 2) NOT NULL,
	[MisOutCount01] [decimal](4, 0) NOT NULL,
	[MisOutCount02] [decimal](4, 0) NOT NULL,
	[MisOutCount03] [decimal](4, 0) NOT NULL,
	[MisOutCount04] [decimal](4, 0) NOT NULL,
	[MisOutCount05] [decimal](4, 0) NOT NULL,
	[MisOutCount06] [decimal](4, 0) NOT NULL,
	[MisOutCount07] [decimal](4, 0) NOT NULL,
	[MisOutCount08] [decimal](4, 0) NOT NULL,
	[MisOutCount09] [decimal](4, 0) NOT NULL,
	[MisOutCount10] [decimal](4, 0) NOT NULL,
	[MisOutCount11] [decimal](4, 0) NOT NULL,
	[MisOutCount12] [decimal](4, 0) NOT NULL,
	[MisOutCount13] [decimal](4, 0) NOT NULL,
	[MisOutCount14] [decimal](4, 0) NOT NULL,
	[MisOutCount15] [decimal](4, 0) NOT NULL,
	[MisOutCount16] [decimal](4, 0) NOT NULL,
	[MisOutCount17] [decimal](4, 0) NOT NULL,
	[MisOutCount18] [decimal](4, 0) NOT NULL,
	[MisOutCount19] [decimal](4, 0) NOT NULL,
	[MisOutCount20] [decimal](4, 0) NOT NULL,
	[MisOutValue01] [decimal](8, 2) NOT NULL,
	[MisOutValue02] [decimal](8, 2) NOT NULL,
	[MisOutValue03] [decimal](8, 2) NOT NULL,
	[MisOutValue04] [decimal](8, 2) NOT NULL,
	[MisOutValue05] [decimal](8, 2) NOT NULL,
	[MisOutValue06] [decimal](8, 2) NOT NULL,
	[MisOutValue07] [decimal](8, 2) NOT NULL,
	[MisOutValue08] [decimal](8, 2) NOT NULL,
	[MisOutValue09] [decimal](8, 2) NOT NULL,
	[MisOutValue10] [decimal](8, 2) NOT NULL,
	[MisOutValue11] [decimal](8, 2) NOT NULL,
	[MisOutValue12] [decimal](8, 2) NOT NULL,
	[MisOutValue13] [decimal](8, 2) NOT NULL,
	[MisOutValue14] [decimal](8, 2) NOT NULL,
	[MisOutValue15] [decimal](8, 2) NOT NULL,
	[MisOutValue16] [decimal](8, 2) NOT NULL,
	[MisOutValue17] [decimal](8, 2) NOT NULL,
	[MisOutValue18] [decimal](8, 2) NOT NULL,
	[MisOutValue19] [decimal](8, 2) NOT NULL,
	[MisOutValue20] [decimal](8, 2) NOT NULL,
	[ExchangeRate] [decimal](7, 5) NOT NULL,
	[ExchangePower] [decimal](3, 0) NOT NULL,
	[Comment] [char](40) NULL,
	[NumTransactions] [int] NOT NULL,
	[NumCorrections] [int] NOT NULL,
	[NumVoids] [int] NOT NULL,
	[NumOpenDrawer] [int] NOT NULL,
	[NumLinesReversed] [int] NOT NULL,
	[NumLinesSold] [int] NOT NULL,
	[NumLinesScanned] [int] NOT NULL,
	[FloatVariance] [decimal](8,2) NULL
	
)

INSERT INTO #BankingTemp ([PeriodID],[CurrencyID],[CashierID] ,[Value],[Discount] ,[Type],[Misc])
SELECT 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	ISNULL(dt.TOTL,0) as Value, 
	ISNULL(dt.DISC,0) as Discount, 
	dt.TCOD as Type, 
	DT.MISC as Misc 
	
FROM DLTOTS as DT 
	inner join CBSCTL as cc on cc.DATE1 = dt.DATE1 
	
where 
   (DT.TCOD = 'M+' and DT.MISC = '20' 
   and cc.COMM = '0'
   ) 
or (DT.TCOD = 'M-' and DT.MISC = '20'
 and cc.COMM = '0'
 ) -- See 1, must be changed!!


INSERT INTO #BankingTemp([PeriodID],[CurrencyID],[CashierID] ,[Value],[Discount] ,[Type],[Misc])
SELECT 
    (select id from SystemPeriods where StartDate = vt.TranDate) as PeriodID,
    (select id from SystemCurrency) as CurrencyID, 
	vt.CashierId as CashierID, 
	ISNULL(vt.ValueMerchandising,0) as Value, 
	ISNULL(vt.ValueDiscount,0) as Discount, 
	VT.[Type],
	'20' as 'Misc' 
	
FROM VisionTotal as VT 
	inner join CBSCTL as cc on cc.DATE1 = vt.TranDate 
	
WHERE
   (VT.TYPE = 'SA' and cc.COMM = '0')   
or (VT.TYPE = 'RF' and cc.COMM = '0')  

INSERT INTO #CashBalCashTemp
SELECT * FROM CashBalCashier 

    DECLARE @RID_1 INT
	DECLARE @MAXID_1 INT
	SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID_1 = 1
	DECLARE @PERIODID INT
	DECLARE @CURRENCY CHAR(5)
	DECLARE @CASHIER_ID INT
	DECLARE @GrossAmount INT
	
	
 
-- looping the #temp_old from first record to last record.
	WHILE(@RID_1<=@MAXID_1)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @PERIODID = PeriodID,@CURRENCY= CurrencyID,@CASHIER_ID= CashierID  FROM #BankingTemp WHERE RID = @RID_1
			-- if the Primary key exists then update, by taking the values from the #temp_old  

			IF EXISTS(SELECT 1 FROM #CashBalCashTemp WHERE PeriodID  = @PERIODID AND CurrencyID = @CURRENCY AND CashierID=@CASHIER_ID)
				BEGIN
                    
                    UPDATE CashBalCashier  
					SET	
					    [DiscountAmount]  = T.Discount,
	                    [SalesCount]  = (Case  when T.[type] = 'M+' or T.[type]= 'SA'
	                                           then [SalesCount] + 1 
	                                           when T.[type] = 'M-' or T.[type]= 'RF' then  0.00 end),
	                    [SalesAmount]  = (Case  when T.[type] = 'M+' or T.[type]= 'SA'
	                                           then [GrossSalesAmount] + T.Value 
	                                           when T.[type] = 'M-' or T.[type]= 'RF' then  0.00 end),
	                    [SalesCorrectCount] = 0.00,
	                    [SalesCorrectAmount] =0.00,
	                    [RefundCount] = (Case  when T.[type] = 'M-' or T.[type]= 'RF'
	                                           then [RefundCount] + 1 
	                                           when T.[type] = 'M+' or T.[type]= 'SA' then  0.00 end),
	                    [RefundAmount]  = (Case  when T.[type] = 'M-' or T.[type]= 'RF'
	                                           then [RefundAmount] + T.Value 
	                                           when T.[type] = 'M+' or T.[type]= 'SA' then  0.00 end),
	                     
	                    --[GrossSalesAmount] = [SalesAmount]+ [RefundAmount],
	                   [GrossSalesAmount] =  (Case  when T.[type] = 'M+' or T.[type]= 'SA'
	                                           then [GrossSalesAmount] + T.Value 
	                                           when T.[type] = 'M-' or T.[type]= 'RF' then [RefundAmount] + T.Value end),
	                    [RefundCorrectCount]=0.00,
	                    [RefundCorrectAmount]= 0.00,
	                    [FloatIssued]= 0.00,
	                    [FloatReturned] = 0.00,
	                    [MiscIncomeCount01]= 0.00, 
	                    [MiscIncomeCount02]=0.00,
	                    [MiscIncomeCount03]= 0.00, 
	                    [MiscIncomeCount04]=0.00,
	                    [MiscIncomeCount05]= 0.00, 
	                    [MiscIncomeCount06]=0.00,
	                    [MiscIncomeCount07]= 0.00, 
	                    [MiscIncomeCount08]=0.00,
	                    [MiscIncomeCount09]= 0.00, 
	                    [MiscIncomeCount10]= 0.00, 
	                    [MiscIncomeCount11]= 0.00, 
	                    [MiscIncomeCount12]=0.00,
	                    [MiscIncomeCount13]=0.00,
	                    [MiscIncomeCount14]=0.00,
	                    [MiscIncomeCount15]=0.00,
	                    [MiscIncomeCount16]= 0.00, 
	                    [MiscIncomeCount17]= 0.00, 
	                    [MiscIncomeCount18]= 0.00, 
	                    [MiscIncomeCount19]=0.00,
	                    [MiscIncomeCount20]=(Case  when T.[type] = 'M+'or T.[type]= 'SA'
	                                           then CashBalCashier.[MiscIncomeCount20]+ 1
	                                           when T.[type] = 'M-' or T.[type]= 'RF'then  0.00 end),---> need to check this
	                    [MiscIncomeValue01]=0.00,
 	                    [MiscIncomeValue02]=0.00 ,
						[MiscIncomeValue03]=0.00,
	                    [MiscIncomeValue04]=0.00,
	                    [MiscIncomeValue05]=0.00,
 	                    [MiscIncomeValue06]=0.00 ,
						[MiscIncomeValue07]=0.00,
	                    [MiscIncomeValue08]=0.00,
	                    [MiscIncomeValue09]=0.00,
 	                    [MiscIncomeValue10]=0.00 ,
						[MiscIncomeValue11]=0.00,
	                    [MiscIncomeValue12]=0.00,
	                    [MiscIncomeValue13]=0.00,
 	                    [MiscIncomeValue14]=0.00 ,
						[MiscIncomeValue15]=0.00,
	                    [MiscIncomeValue16]=0.00,
	                    [MiscIncomeValue17]=0.00,
 	                    [MiscIncomeValue18]=0.00 ,
						[MiscIncomeValue19]=0.00,
	                    [MiscIncomeValue20]= (Case  when T.[type] = 'M+' or T.[type]= 'SA'
	                                           then CashBalCashier.[MiscIncomeValue20]+ T.Value
	                                           when T.[type] = 'M-' or T.[type]= 'RF' then  0.00 end),
	                	[MisOutCount01]= 0.00,
	                	[MisOutCount02]= 0.00,
	                	[MisOutCount03]= 0.00,
	                	[MisOutCount04]= 0.00,
	                	[MisOutCount05]= 0.00,
	                	[MisOutCount06]= 0.00,
	                	[MisOutCount07]= 0.00,
	                	[MisOutCount08]= 0.00,
	                	[MisOutCount09]= 0.00,
	                	[MisOutCount10]= 0.00,
	                	[MisOutCount11]= 0.00,
	                	[MisOutCount12]= 0.00,
	                	[MisOutCount13]= 0.00,
	                	[MisOutCount14]= 0.00,
	                	[MisOutCount15]= 0.00,
	                	[MisOutCount16]= 0.00,
	                	[MisOutCount17]= 0.00,
	                	[MisOutCount18]= 0.00,
	                	[MisOutCount19]= 0.00,
	                	[MisOutCount20]= (Case  when T.[type] = 'M-' or T.[type]= 'RF'
	                                           then CashBalCashier.[MisOutCount20]+ 1
	                                           when T.[type] = 'M+'or T.[type]= 'SA' then  0.00 end),
	                	[MisOutValue01]= 0.00,
	                    [MisOutValue02]= 0.00,
	                    [MisOutValue03]= 0.00,
	                    [MisOutValue04]=0.00,
	                    [MisOutValue05]= 0.00,
	                    [MisOutValue06]= 0.00,
	                    [MisOutValue07]= 0.00,
	                    [MisOutValue08]=0.00,
	                    [MisOutValue09]= 0.00,
	                    [MisOutValue10]= 0.00,
	                    [MisOutValue11]= 0.00,
	                    [MisOutValue12]=0.00,
	                    [MisOutValue13]= 0.00,
	                    [MisOutValue14]= 0.00,
	                    [MisOutValue15]= 0.00,
	                    [MisOutValue16]=0.00,
	                    [MisOutValue17]= 0.00,
	                    [MisOutValue18]= 0.00,
	                    [MisOutValue19]= 0.00,
	                    [MisOutValue20]= (Case  when T.[type] = 'M-' or T.[type]= 'RF'
	                                           then CashBalCashier.[MisOutValue20]+ T.Value
	                                           when T.[type] = 'M+' or T.[type]= 'SA' then  0.00 end),
	                    [ExchangeRate]= 0.00,
	                    [ExchangePower]= 0.00,
	                    [Comment]= '',
	                    [NumTransactions]= [NumTransactions]+1, --->need to check this
	                    [NumCorrections] = 0,
	                    [NumVoids] = 0,
	                    [NumOpenDrawer]=0,
	                    [NumLinesReversed]=0,
	                    [NumLinesSold] = 0,
	                    [NumLinesScanned]= 0,
			            [FloatVariance]= 0             
					    
					    					    					    				    					    										
					FROM #BankingTemp AS T WHERE (CashBalCashier.PeriodID = @PERIODID AND T.PeriodID = @PERIODID) AND (CashBalCashier.CurrencyID = @CURRENCY and  T.CurrencyID =@CURRENCY ) AND (CashBalCashier.CashierID = @CASHIER_ID and  T.CashierID = @CASHIER_ID )
				END
			ELSE -- ELSE INSERT NEW RECORD
				BEGIN
					INSERT INTO CashBalCashier (PeriodID,CurrencyID,CashierID,[GrossSalesAmount],
					[DiscountAmount],[SalesCount],[SalesAmount],[SalesCorrectCount],[SalesCorrectAmount],[RefundCount]
					,[RefundAmount],[RefundCorrectCount], [RefundCorrectAmount], [FloatIssued],[FloatReturned],
					[MiscIncomeCount01],[MiscIncomeCount02],[MiscIncomeCount03],[MiscIncomeCount04],[MiscIncomeCount05],
					[MiscIncomeCount06],[MiscIncomeCount07],[MiscIncomeCount08],[MiscIncomeCount09],[MiscIncomeCount10],
					[MiscIncomeCount11],[MiscIncomeCount12],[MiscIncomeCount13],[MiscIncomeCount14],[MiscIncomeCount15],[MiscIncomeCount16],[MiscIncomeCount17],
					[MiscIncomeCount18],[MiscIncomeCount19],[MiscIncomeCount20],[MiscIncomeValue01],[MiscIncomeValue02],[MiscIncomeValue03],
					[MiscIncomeValue04],[MiscIncomeValue05],[MiscIncomeValue06],[MiscIncomeValue07],[MiscIncomeValue08],[MiscIncomeValue09],[MiscIncomeValue10],
					[MiscIncomeValue11],[MiscIncomeValue12],[MiscIncomeValue13],[MiscIncomeValue14],[MiscIncomeValue15],[MiscIncomeValue16],[MiscIncomeValue17],[MiscIncomeValue18],
					[MiscIncomeValue19],[MiscIncomeValue20],[MisOutCount01],[MisOutCount02],[MisOutCount03],[MisOutCount04],[MisOutCount05],
					[MisOutCount06],[MisOutCount07],[MisOutCount08],[MisOutCount09],[MisOutCount10],[MisOutCount11],[MisOutCount12],[MisOutCount13],[MisOutCount14],
					[MisOutCount15],[MisOutCount16],[MisOutCount17],[MisOutCount18],[MisOutCount19],[MisOutCount20],[MisOutValue01],[MisOutValue02],[MisOutValue03],
					[MisOutValue04],[MisOutValue05],[MisOutValue06],[MisOutValue07],[MisOutValue08],[MisOutValue09],[MisOutValue10],[MisOutValue11],[MisOutValue12],[MisOutValue13],
					[MisOutValue14],[MisOutValue15],[MisOutValue16],[MisOutValue17],[MisOutValue18],[MisOutValue19],[MisOutValue20], [ExchangeRate],[ExchangePower],
					[Comment],[NumTransactions], [NumCorrections],[NumVoids],[NumOpenDrawer],[NumLinesReversed],[NumLinesSold],[NumLinesScanned],[FloatVariance])
					
					SELECT T.PeriodID,T.CurrencyID,T.CashierID,T.Value,T.Discount,(Case  when T.[type] = 'M+' or T.[type]= 'SA'then 1 when T.[type] = 'M-' or T.[type]= 'RF' then  0.00 end),
					(Case  when T.[type] = 'M+' or T.[type]= 'SA'then T.Value when T.[type] = 'M-' or T.[type]= 'RF' then  0.00 end),0.00,0.00,
					(Case  when T.[type] = 'M-' or T.[type]= 'RF'then 1 when T.[type] = 'M+' or T.[type]= 'SA' then  0.00 end),
					(Case  when T.[type] = 'M-' or T.[type]= 'RF'then T.Value when T.[type] = 'M+' or T.[type]= 'SA' then  0.00 end),
					0.00,0.00,0.00,0.00,0.00,0.00,0.00,
					0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,(Case  when T.[type] = 'M+'or T.[type]= 'SA'then 1 when T.[type] = 'M-' or T.[type]= 'RF'then  0.00 end),
					0.00,0.00,0.00,0.00,0.00,0.00,
					0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,(Case when T.[type] = 'M+' or T.[type]= 'SA'then T.Value when T.[type] = 'M-' or T.[type]= 'RF' then  0.00 end),
					0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,
					0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,(Case  when T.[type] = 'M-' or T.[type]= 'RF'then 1 when T.[type] = 'M+'or T.[type]= 'SA' then  0.00 end),					
					0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,(Case  when T.[type] = 'M-' or T.[type]= 'RF'then T.Value when T.[type] = 'M+' or T.[type]= 'SA' then  0.00 end),
					0.00,0.00,'',1,0,0,0,0,0,0,0
	                					
					FROM #BankingTemp   AS T 
					WHERE T.RID = @RID_1
				SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp
				END
             delete #BankingTemp where RID = @RID_1 
             delete #CashBalCashTemp  
            INSERT INTO #CashBalCashTemp
            SELECT * FROM CashBalCashier               
               
			SET @RID_1 = @RID_1+1 -- increment loop count
		END


END;
GO

