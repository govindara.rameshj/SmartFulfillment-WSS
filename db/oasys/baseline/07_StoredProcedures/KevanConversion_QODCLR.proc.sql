﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_QODCLR]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_QODCLR'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_QODCLR] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_QODCLR'
GO
ALTER PROCEDURE dbo.KevanConversion_QODCLR
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 21 - Running Kevan's Clear-up QOD Script..
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Task     : 21/01 - Update DELC where Order is at Delivery Status 999
----------------------------------------------------------------------------------- 
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '999' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/02 - Update Delivery Status where DELC (True) and Status is 910
----------------------------------------------------------------------------------- 
Update CORHDR4
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '910' and ch.DELC = '1')

-----------------------------------------------------------------------------------
-- Task     : 21/03 - Update Delivery Status where DELC (True) and Status is 920
----------------------------------------------------------------------------------- 
Update CORHDR4
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '920' and ch.DELC = '1')

-----------------------------------------------------------------------------------
-- Task     : 21/04 - Update Delivery Status where DELC (True) and Status is 930
----------------------------------------------------------------------------------- 
Update CORHDR4
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '930' and ch.DELC = '1')

-----------------------------------------------------------------------------------
-- Task     : 21/05 - Update Delivery Status where DELC (True) and Status is 940
----------------------------------------------------------------------------------- 
Update CORHDR4
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '940' and ch.DELC = '1')

-----------------------------------------------------------------------------------
-- Task     : 21/06 - Update Delivery Status where DELC (True) and Status is 950
----------------------------------------------------------------------------------- 
Update CORHDR4
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '950' and ch.DELC = '1')

-----------------------------------------------------------------------------------
-- Task     : 21/07 - Update Delivery Status where DELC (True) and Status is 960
----------------------------------------------------------------------------------- 
Update CORHDR4
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '960' and ch.DELC = '1')

-----------------------------------------------------------------------------------
-- Task     : 21/08 - Update Delivery Status where DELC (True) and Status is 970
----------------------------------------------------------------------------------- 
Update CORHDR4
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '970' and ch.DELC = '1')

-----------------------------------------------------------------------------------
-- Task     : 21/09 - Update Delivery Status where DELC (True) and Status is 980
----------------------------------------------------------------------------------- 
Update CORHDR4
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '970' and ch.DELC = '1')

-----------------------------------------------------------------------------------
-- Task     : 21/10 - Update DELC where Order is at Delivery Status 910
-----------------------------------------------------------------------------------
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '910' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/11 - Update DELC where Order is at Delivery Status 920
-----------------------------------------------------------------------------------
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '920' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/12 - Update DELC where Order is at Delivery Status 930
-----------------------------------------------------------------------------------
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '930' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/13 - Update DELC where Order is at Delivery Status 940
-----------------------------------------------------------------------------------
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '940' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/14 - Update DELC where Order is at Delivery Status 950
-----------------------------------------------------------------------------------
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '950' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/15 - Update DELC where Order is at Delivery Status 960
-----------------------------------------------------------------------------------
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '960' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/16 - Update DELC where Order is at Delivery Status 970
-----------------------------------------------------------------------------------
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '970' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/17 - Update DELC where Order is at Delivery Status 980
-----------------------------------------------------------------------------------
Update CORHDR
Set DELC = '1'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '980' and ch.DELC = '0')

-----------------------------------------------------------------------------------
-- Task     : 21/18 - Update LINES with Updated Order Status
-----------------------------------------------------------------------------------
Update CORLIN
Set DeliveryStatus = '999'
Where NUMB in (
SELECT c4.NUMB
FROM CORHDR4 as c4
inner join CORHDR as ch on c4.NUMB = ch.NUMB
where c4.DeliveryStatus = '999' and ch.DELC = '1')

END;
GO

