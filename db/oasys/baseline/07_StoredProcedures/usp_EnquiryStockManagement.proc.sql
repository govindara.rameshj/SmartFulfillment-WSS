﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_EnquiryStockManagement]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_EnquiryStockManagement'
	EXEC ('CREATE PROCEDURE [dbo].[usp_EnquiryStockManagement] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_EnquiryStockManagement'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 01/08/2012
-- User Story	 : 6131
-- Project		 : P022-009 - Stock Enquiry Details - amend the design of what is displayed.
-- Task Id		 : 6344
-- Description   : Add new Stock Management report to take some of the fields and links
--				 : from other reports on the Stock Item Details dashboard.
-- =============================================
ALTER PROCedure [dbo].[usp_EnquiryStockManagement]
	@SkuNumber	char(6)
As
Begin
	Set NOCOUNT On;

	Declare 
		@Table Table
			(
				RowId Int,
				SkuNumber Char(6),
				SupplierNumber Char(5),
				Description Varchar(50),
				Display Varchar(50)
			);

	Declare
		@isTagged	VarChar(3),
		@returnQty	Int,
		@dateSetup	Date,
		@dateFirst	Date

	Select
		@isTagged	= (Select Case ITAG When 1 Then 'Yes' Else 'No' End),
		@returnQty	= RETQ,
		@dateSetup	= IODT,
		@dateFirst	= DATS
	From
		STKMAS
	Where
		SKUN = @SkuNumber

	Insert Into @Table(RowId, SkuNumber, Description) Values (1, @SkuNumber, 'Movement Details');
	Insert Into @Table(RowId, SkuNumber, Description) Values (4, @SkuNumber, 'Movement Weekly');
	Insert Into @Table(RowId, SkuNumber, Description) Values (2, @SkuNumber, 'PIC');
	Insert Into @Table(RowId, SkuNumber, Description) Values (3, @SkuNumber, 'Shrinkage');
	Insert Into @Table([Description], Display) Values ('','');
	Insert Into @Table([Description], Display) values ('Open Returns',	@returnQty );
	Insert Into @Table([Description], Display) Values ('Tagged', @isTagged);
	Insert Into @Table([Description], Display) Values ('Setup Date', Convert(Varchar(10), @dateSetup, 103));
	Insert Into @Table([Description], Display) Values ('First Stocked', Convert(Varchar(10), @dateFirst, 103));

	Select * From @Table;
End
GO

