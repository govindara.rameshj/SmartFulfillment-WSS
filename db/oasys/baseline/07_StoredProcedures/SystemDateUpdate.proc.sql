﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemDateUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemDateUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SystemDateUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemDateUpdate'
GO
ALTER PROCEDURE [dbo].[SystemDateUpdate]
@Id CHAR (2), @Today DATE, @TodayDayNumber INT, @Tomorrow DATE, @TomorrowDayNumber INT, @WeekFromToday DATE, @WeekEndProcessed BIT, @PeriodEndProcessed BIT, @CurrentPeriodIsYearEnd BIT, @CurrentPeriodEnd DATE, @SetInUse INT, @WeekEnd1 DATE, @WeekEnd2 DATE, @WeekEnd3 DATE, @WeekEnd4 DATE, @WeekEnd5 DATE, @WeekEnd6 DATE, @WeekEnd7 DATE, @WeekEnd8 DATE, @WeekEnd9 DATE, @WeekEnd10 DATE, @WeekEnd11 DATE, @WeekEnd12 DATE, @WeekEnd13 DATE
AS
begin
	update
		SYSDAT
	set
		TODT	= @Today,
		TODW	= @TodayDayNumber,
		TMDT	= @Tomorrow,
		TMDW	= @TomorrowDayNumber,
		WKDT	= @WeekFromToday,
		PWEK	= @WeekEndProcessed,
		PPER	= @PeriodEndProcessed,
		TIYR	= @CurrentPeriodIsYearEnd,
		TIDT	= @CurrentPeriodEnd,
		PSET	= @SetInUse,
		WK131	= @WeekEnd1,
		WK132	= @WeekEnd2,
		WK133	= @WeekEnd3,
		WK134	= @WeekEnd4,
		WK135	= @WeekEnd5,
		WK136	= @WeekEnd6,
		WK137	= @WeekEnd7,
		WK138	= @WeekEnd8,
		WK139	= @WeekEnd9,
		WK1310	= @WeekEnd10,
		WK1311	= @WeekEnd11,
		WK1312	= @WeekEnd12,
		WK1313	= @WeekEnd13
	where
		FKEY = @Id

end
GO

