﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CouponMasterGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_CouponMasterGet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_CouponMasterGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_CouponMasterGet'
GO
ALTER PROCedure [dbo].[usp_CouponMasterGet]
	@CouponId		CHAR (7),
	@IncludeDeleted	BIT = 0
	
As
Begin
	Set NOCOUNT ON;

Select 
	cm.[COUPONID],
	cm.[DESCRIPTION],
	cm.[STOREGEN],
	cm.[SERIALNO],
	cm.[EXCCOUPON],
	cm.[REUSABLE],
	cm.[COLLECTINFO],
	cm.[DELETED]					
From
	COUPONMASTER  cm
Where
	cm.[COUPONID]= @CouponId
And
	(
		@IncludeDeleted = 1
	Or
		cm.[DELETED] = 0
	)
End
GO

