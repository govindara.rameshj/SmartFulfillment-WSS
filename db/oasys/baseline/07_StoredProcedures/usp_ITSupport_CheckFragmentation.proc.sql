﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_CheckFragmentation]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_CheckFragmentation'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_CheckFragmentation] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_CheckFragmentation'
GO
ALTER PROCEDURE dbo.usp_ITSupport_CheckFragmentation
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 21st May 2013
-- Description	: Checks Server Database Fragmentation Levels
-----------------------------------------------------------------------------------
@Report Char(1) = 'D'

As
Begin
Set NoCount On

-------------------------------------------------------------------------------------------------
-- Kevan Madelin - v1.0 - 01/06/2012 - Support Analysis of SQL Server Performance
-------------------------------------------------------------------------------------------------
If @Report = 'S'
	Begin
		Select				Object_Name(dm.Object_Id) as 'Table',
							Avg_Fragmentation_In_Percent as 'Fragmentation Percentage'
		From				sys.dm_db_index_physical_stats(db_id(),null,null,null,'sampled') dm
		Order By			Avg_Fragmentation_In_Percent Desc
	End
	
If @Report = 'D'
	Begin
		Select				Object_Name(dm.Object_Id) as 'Table',
							i.Name as 'Index Name',
							dm.Index_Type_Desc as 'Index Type',
							Avg_Fragmentation_In_Percent as 'Fragmentation Percentage',
							Avg_Fragment_Size_In_Pages as 'Fragmentation in Pages',
							page_count as 'Page Count',
							record_count as 'Record Count',
							allow_row_locks as 'Row Locks Allowed',
							allow_page_locks as 'Page Locks Allowed',
							Avg_Record_Size_In_Bytes as 'Average Record Size (Bytes)'
		From				sys.dm_db_index_physical_stats(db_id(),null,null,null,'sampled') dm
		Join				sys.indexes i on dm.Object_Id=i.Object_Id and dm.Index_Id=i.Index_Id 
		Order By			Avg_Fragmentation_In_Percent Desc
	End
	
If @Report not in ('S','D') Print ('Summary (S) or Detail (D) not specified')

End
GO

