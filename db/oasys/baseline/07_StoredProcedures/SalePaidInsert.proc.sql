﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SalePaidInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SalePaidInsert'
	EXEC ('CREATE PROCEDURE [dbo].[SalePaidInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SalePaidInsert'
GO
ALTER PROCEDURE [dbo].[SalePaidInsert]
 @TransactionDate DATE,
 @TillNumber CHAR (2), 
 @TransactionNumber CHAR(4),
 @SequenceNumber INT,
 @TenderType Decimal(3,0)=1,
 @TenderAmount DECIMAL(9,2),
 @CreditCardNumber CHAR(19)='0000000000000000000',
 @CreditCardExp CHAR(4)='0000',
 @CouponNumber CHAR(6)='000000',
 @CouponClass CHAR(2)='00',
 @CCAuthCode CHAR(9)=NULL,
 @CCKeyed BIT=1,
 @SupervisorNumber CHAR(3)='000',
 @CustomerPostCode CHAR(8)=NULL,
 @ChequeAccNo CHAR(10)='0000000000',
 @SortCode CHAR(6)='000000',
 @ChequeNumber CHAR(6)='000000',
 @DBRProcessed BIT=0,
 @VoucherSeqNo CHAR(4)='0000',
 @DCIssueNo CHAR(2)= NULL,
 @AuthType CHAR(1)=NULL,
 @MerchantNo CHAR(15)=NULL,
 @CashBackAmount DECIMAL(9,2)=0,
 @DigitCount INT=0,
 @CommsPrepared BIT=1,
 @CardDescription CHAR(30)=NULL,
 @EFTPOSTransID CHAR(6)=NULL,
 @PinAcknowledge CHAR(1)=NULL,
 @CardStartDate CHAR(4)='0000',
 @AuthDescription CHAR(20)=NULL,
 @SecurityCode CHAR(3)=NULL,
 @TenderStatusCode CHAR(3)=NULL,
 @ConversionFactor INT=0,
 @ConversionRate DECIMAL(7,5)=0,
 @Value DECIMAL(9,2)=0,
 @RTIFlag CHAR(1)='S'
 

AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO DLPAID 
	(
	    DATE1,
	    TILL,
	    [TRAN],
		NUMB,
		[TYPE],
		AMNT,
		[CARD],
		EXDT,
		COPN,
		CLAS,
		AUTH,
		CKEY,
		SUPV,
		POST,
		CKAC,
		CKSC,
		CKNO,
		DBRF,
		SEQN,
		ISSU,
		ATYP,
		MERC,
		CBAM,
		DIGC,
		ECOM,
		CTYP,
		EFID,
		EFTC,
		STDT,
		AUTHDESC,
		SECCODE,
		TENC,
		MPOW,
		MRAT,
		TENV,
		RTI
	
	)
VALUES
(
 @TransactionDate,
 @TillNumber , 
 @TransactionNumber,
 @SequenceNumber,
 @TenderType ,
 @TenderAmount ,
 @CreditCardNumber ,
 @CreditCardExp ,
 @CouponNumber ,
 @CouponClass ,
 @CCAuthCode ,
 @CCKeyed ,
 @SupervisorNumber ,
 @CustomerPostCode,
 @ChequeAccNo ,
 @SortCode,
 @ChequeNumber,
 @DBRProcessed ,
 @VoucherSeqNo ,
 @DCIssueNo ,
 @AuthType,
 @MerchantNo ,
 @CashBackAmount ,
 @DigitCount ,
 @CommsPrepared ,
 @CardDescription ,
 @EFTPOSTransID ,
 @PinAcknowledge ,
 @CardStartDate ,
 @AuthDescription ,
 @SecurityCode,
 @TenderStatusCode ,
 @ConversionFactor,
 @ConversionRate,
 @Value ,
 @RTIFlag 
)
return @@rowcount
END
GO

