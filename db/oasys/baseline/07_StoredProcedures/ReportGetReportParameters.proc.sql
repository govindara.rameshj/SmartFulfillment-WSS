﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGetReportParameters]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReportGetReportParameters'
	EXEC ('CREATE PROCEDURE [dbo].[ReportGetReportParameters] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReportGetReportParameters'
GO
ALTER PROCEDURE [dbo].[ReportGetReportParameters]
	@ReportId int
AS
BEGIN
	SET NOCOUNT ON;

	select
		rp.id,
		rp.Description,
		rp.Name,
		rp.DataType,
		rp.Size,
		rp.LookupValuesProcedure,
		rp.Mask,
		rps.Sequence,
		rps.AllowMultiple,
		rps.DefaultValue
	from
		ReportParameter rp
	inner join
		ReportParameters rps	on rps.ParameterId = rp.Id 
	where	
		rps.ReportId = @ReportId
	order by
		rps.Sequence

END
GO

