﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EventGetHeaderByStock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EventGetHeaderByStock'
	EXEC ('CREATE PROCEDURE [dbo].[EventGetHeaderByStock] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EventGetHeaderByStock'
GO
ALTER PROCEDURE [dbo].[EventGetHeaderByStock]
@SkuNumber CHAR (6)
AS
BEGIN
	SET NOCOUNT ON;

Declare @strDays   CHAR (28)

SELECT  RowId = 1,
		eh.NUMB as 'Event',
		en.SKUN as 'SkuNumber',
		eh.PRIO as 'Priority',
		eh.DESCR as 'Description',
		case 
			When eh.SDAT <= GETDATE() or eh.EDAT <= GETDATE()	then 'Active'
			Else 'Inactive'
		end	as 'Status',
	    convert(varchar, eh.SDAT, 103) as 'From',
	    eh.STIM as 'StartTime',
		convert(varchar, eh.EDAT, 103) as 'To',
		eh.ETIM as 'EndTime',
		Case
			when eh.DACT1 = 1 then 'Mon'
		end as 'Mon',
		Case
			when eh.DACT2 = 1 then + 'Tue'
		end as 'Tue',
		Case
			when eh.DACT3 = 1 then + 'Wed'
		end as 'Wed',
		Case
			when eh.DACT4 = 1 then + 'Thr'
		end as 'Thr',
		Case
			when eh.DACT5 = 1 then + 'Fri'
		end as 'Fri',
		Case
			when eh.DACT6 = 1 then + 'Sat'
		end as 'Sat',
		Case
			when eh.DACT7 = 1 then + 'Sun'
		end as 'Sun'
FROM EVTHDR as eh INNER JOIN EVTENQ as en ON eh.NUMB=en.NUMB
where en.SKUN = @SkuNumber

END
GO

