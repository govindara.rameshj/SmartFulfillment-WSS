﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_HandHeldTerminalHeaderLatest]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_HandHeldTerminalHeaderLatest'
	EXEC ('CREATE PROCEDURE [dbo].[usp_HandHeldTerminalHeaderLatest] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_HandHeldTerminalHeaderLatest'
GO
-- =============================================
-- Author       : Partha Dutta
-- Create date  : 18/06/2011
-- Referral No  : 773
-- Description  : Baseline version
--                Retrieve latest HHT Header record
-- =============================================

ALTER PROCedure dbo.usp_HandHeldTerminalHeaderLatest
as
set nocount on

select top 1 * from HHTHDR order by DATE1 desc
GO

