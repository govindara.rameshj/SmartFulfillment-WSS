﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeMaintenanceValidation]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeMaintenanceValidation'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeMaintenanceValidation] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeMaintenanceValidation'
GO
ALTER PROCedure NewBankingSafeMaintenanceValidation
   @PeriodID int
as
begin
set nocount on

declare @FloatedCashierNoSales            int
declare @FloatedCashierSalesNoPickupBag   int
declare @UnFloatedCashierSalesNoPickupBag int

--floated cashiers with no sales
set @FloatedCashierNoSales = (select count(*)
                              from SafeBags a
                              inner join CashBalCashier b
                                    on  b.PeriodID   = @PeriodID
                                    and b.CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                    and b.CashierID  = a.AccountabilityID 
                              left outer join (select * from SafeBags where Type = 'P' and [State] <> 'C') c
                                         on  c.PickupPeriodID   = a.OutPeriodID
                                         and c.AccountabilityID = a.AccountabilityID
                              where a.[Type]      = 'F'
                              and   a.[State]     = 'R'
                              and   a.OutPeriodID = @PeriodID 
                              and   b.SalesCount  = 0
                              and   c.ID is null)
--floated cashiers with sales and no pickup (exclude cash drops)
set @FloatedCashierSalesNoPickupBag = (select count(*)
                                       from SafeBags a
                                       inner join CashBalCashier b
                                             on  b.PeriodID   = @PeriodID 
                                             and b.CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                                             and b.CashierID  = a.AccountabilityID
                                       --end of day pickup for this cashier and period
                                       left outer join (select *
                                                        from SafeBags
                                                        where [Type]            = 'P'
                                                        and   [State]          <> 'C'
                                                        and isnull(CashDrop, 0) = 0) c
                                                  on  c.PickupPeriodID   = a.OutPeriodID
                                                  and c.AccountabilityID = a.AccountabilityID
                                       where a.[Type]      = 'F'
                                       and   a.[State]     = 'R'
                                       and   a.OutPeriodID = @PeriodID
                                       and   b.SalesCount  <> 0
                                       and   c.ID is null)
--unfloated cashiers with sales and no pickup (exclude cash drops)
set @UnFloatedCashierSalesNoPickupBag = (select count(*)
                                         from CashBalCashier a
                                         --float for this cashier and period
                                         left outer join (select * from SafeBags where Type = 'F' and [State] = 'R' and OutPeriodID = @PeriodID) b
                                                    on  b.AccountabilityID = a.CashierID
                                         --end of day pickup for this cashier and period
                                         left outer join (select *
                                                          from SafeBags
                                                          where [Type]            = 'P'
                                                          and   [State]          <> 'C'
                                                          and PickupPeriodID      = @PeriodID
                                                          and isnull(CashDrop, 0) = 0) c
                                                    on  c.AccountabilityID = a.CashierID
                                         where a.PeriodID    = @PeriodID
                                         and   a.CurrencyID  = (select ID from SystemCurrency where IsDefault = 1)
                                         and   a.SalesCount <> 0
                                         and   b.ID is null
                                         and   c.ID is null)

select FloatedCashierNoSales            = cast((case @FloatedCashierNoSales
                                                   when 0 then 0
                                                   else 1
                                                 end) as bit),
       FloatedCashierSalesNoPickupBag   = cast((case @FloatedCashierSalesNoPickupBag
                                                    when 0 then 0
                                                    else 1
                                                 end) as bit),
       UnFloatedCashierSalesNoPickupBag = cast((case @UnFloatedCashierSalesNoPickupBag
                                                    when 0 then 0
                                                    else 1
                                                 end) as bit)
end
GO

