﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MarkdownsSelectActive]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure MarkdownsSelectActive'
	EXEC ('CREATE PROCEDURE [dbo].[MarkdownsSelectActive] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure MarkdownsSelectActive'
GO
ALTER PROCedure [dbo].[MarkdownsSelectActive]
as

select		MarkdownStock.SkuNumber,
			rtrim(STKMAS.DESCR) as SkuDescription,
			HIECAT.NUMB as CategoryNumber,
			rtrim(HIECAT.DESCR) as CategoryDescription,
			HIEGRP.GROU as GroupNumber,
			rtrim(HIEGRP.DESCR) as GroupDescription,
			HIESGP.SGRP as SubgroupNumber,
			rtrim(HIESGP.DESCR) as SubgroupDescription,
			HIESTY.STYL as StyleNumber,
			rtrim(HIESTY.DESCR) as StyleDescription,
			MarkdownStock.DateCreated,
			MarkdownStock.Serial,
			MarkdownStock.ReasonCode,
			MarkdownStock.Price,
			STKMAS.PRIC as SkuPrice,
			MarkdownStock.IsLabelled,			
			MarkdownStock.WeekNumber,
			MarkdownStock.ReductionWeek1,
			MarkdownStock.ReductionWeek2,
			MarkdownStock.ReductionWeek3,
			MarkdownStock.ReductionWeek4,
			MarkdownStock.ReductionWeek5,
			MarkdownStock.ReductionWeek6,
			MarkdownStock.ReductionWeek7,
			MarkdownStock.ReductionWeek8,
			MarkdownStock.ReductionWeek9,
			MarkdownStock.ReductionWeek10,			
			case MarkdownStock.WeekNumber
				when 1 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek1/100),2) 
				when 2 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek2/100),2)
				when 3 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek3/100),2)
				when 4 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek4/100),2)
				when 5 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek5/100),2)
				when 6 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek6/100),2)
				when 7 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek7/100),2)
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent,
			case MarkdownStock.WeekNumber + 1 
				when 2 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek2/100),2)
				when 3 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek3/100),2)
				when 4 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek4/100),2)
				when 5 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek5/100),2)
				when 6 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek6/100),2)
				when 7 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek7/100),2)
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent1,
			case MarkdownStock.WeekNumber + 2 
				when 3 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek3/100),2)
				when 4 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek4/100),2)
				when 5 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek5/100),2)
				when 6 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek6/100),2)
				when 7 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek7/100),2)
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent2,
			case MarkdownStock.WeekNumber + 3 
				when 4 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek4/100),2)
				when 5 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek5/100),2)
				when 6 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek6/100),2)
				when 7 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek7/100),2)
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent3,
			case MarkdownStock.WeekNumber + 4 
				when 5 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek5/100),2)
				when 6 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek6/100),2)
				when 7 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek7/100),2)
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent4,
			case MarkdownStock.WeekNumber + 5 
				when 6 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek6/100),2)
				when 7 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek7/100),2)
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent5,
			case MarkdownStock.WeekNumber + 6 
				when 7 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek7/100),2)
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent6,
			case MarkdownStock.WeekNumber + 7 
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent7,
			case MarkdownStock.WeekNumber + 8 
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent8,
			case MarkdownStock.WeekNumber + 9 
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent9

from		MarkdownStock
inner join	STKMAS on MarkdownStock.SkuNumber=STKMAS.SKUN
inner join	HIECAT on STKMAS.CTGY = HIECAT.NUMB
inner join	HIEGRP on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU
inner join	HIESGP on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU and STKMAS.SGRP = HIESGP.SGRP 
inner join	HIESTY on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU and STKMAS.SGRP = HIESGP.SGRP and STKMAS.STYL = HIESTY.STYL

where		MarkdownStock.Solddate is null
and			MarkdownStock.DateWrittenOff is null
and			MarkdownStock.IsReserved=0

order by	MarkdownStock.DateCreated, STKMAS.DESCR, MarkdownStock.Serial
GO

