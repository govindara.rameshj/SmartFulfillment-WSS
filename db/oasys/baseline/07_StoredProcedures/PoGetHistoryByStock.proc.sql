﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoGetHistoryByStock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoGetHistoryByStock'
	EXEC ('CREATE PROCEDURE [dbo].[PoGetHistoryByStock] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoGetHistoryByStock'
GO
ALTER PROCEDURE [dbo].[PoGetHistoryByStock]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	select
		ph.PNUM				as 'PoNumber',
		pl.QTYO				as 'QtyOrdered',
		pl.RQTY				as 'QtyReceived',
		ph.DDAT				as 'DateDue',
		(select case ph.CONF
			when 'A' then 'Amendment'
			when 'C' then 'Confirmed'
			when 'R' then 'Rejection'
		end)				as 'Status',
		ph.RCOM				as 'IsReceived'
	from 
		PURHDR ph
	inner join
		PURLIN pl on ph.TKEY = pl.HKEY and pl.SKUN=@SkuNumber
	where
		ph.DELM=0
	order by
		ph.PNUM

END
GO

