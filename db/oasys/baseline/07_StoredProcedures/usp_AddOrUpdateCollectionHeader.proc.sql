﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_AddOrUpdateCollectionHeader]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_AddOrUpdateCollectionHeader'
	EXEC ('CREATE PROCEDURE [dbo].[usp_AddOrUpdateCollectionHeader] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_AddOrUpdateCollectionHeader'
GO
ALTER PROCEDURE [dbo].[usp_AddOrUpdateCollectionHeader] @CollectionId int = null , 
                                               @Description varchar( 100
                                                                   ) , 
                                               @TotalWeight int , 
                                               @Name varchar( 50
                                                            ) , 
                                               @Address1 varchar( 50
                                                                ) , 
                                               @Address2 varchar( 50
                                                                ) , 
                                               @Address3 varchar( 50
                                                                ) , 
                                               @PostCode varchar( 8
                                                                ) , 
                                               @VehicleTypeIndex varchar( 10
                                                                        ) , 
                                               @DeliveryDate date,
                                               
                                               @LastUpdateDateTime datetime
                                               
AS
BEGIN
    IF EXISTS( SELECT 1
                 FROM vehicleCollectionHeader
                 WHERE CollectionID
                       = 
                       @collectionid
                   AND DeliveryDate
                       = 
                       @DeliveryDate
             )
        BEGIN
            UPDATE vehicleCollectionHeader
            SET vehicleTypeIndex = @VehicleTypeIndex , 
                Description = @Description , 
                TotalWeight = @TotalWeight , 
                Name = @Name , 
                CustomerAddress1 = @Address1 , 
                CustomerAddress2 = @Address2 , 
                CustomerAddress3 = @Address3 , 
                PostCode = @PostCode,
                LastUpdateDateTime = @LastUpdateDateTime
              WHERE CollectionID
                    = 
                    @CollectionID;
        END;
    ELSE
        BEGIN
            INSERT INTO vehicleCollectionHeader( Description , 
                                                 TotalWeight , 
                                                 Name , 
                                                 CustomerAddress1 , 
                                                 CustomerAddress2 , 
                                                 CustomerAddress3 , 
                                                 PostCode , 
                                                 VehicleTypeIndex , 
                                                 DeliveryDate,
                                                 LastUpdateDateTime
                                               )
            VALUES( @Description , 
                    @TotalWeight , 
                    @Name , 
                    @Address1 , 
                    @Address2 , 
                    @Address3 , 
                    @PostCode , 
                    @VehicleTypeIndex , 
                    @DeliveryDate,
                    @LastUpdateDateTime
                  );
        END;
        
 SELECT SCOPE_IDENTITY()
 
END;
GO

