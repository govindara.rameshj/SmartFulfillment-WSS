﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetOutOfStockItems]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetOutOfStockItems'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetOutOfStockItems] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetOutOfStockItems'
GO
-- =============================================
-- Author:        Alex Milne
-- Create date: 28/09/2009
-- Description:   Procedure is used to get all items that are out of stock relevant to the
--                      parameters entered
-- =============================================
ALTER PROCEDURE [dbo].[StockGetOutOfStockItems] 
      @SupplierType		char(1),			--Used to enter which type of supplier type to search with
      @SupplierNumbers	varchar(8000)=null	--Used to enter a set of supplier numbers if no type is selected
AS
BEGIN
	SET NOCOUNT ON;

	Select
		sup.name	as 'SupplierName', 
		S.SKUN		as 'SkuNumber',
		S.DESCR		as 'Description',
		S.DSOL		as 'DateLastSold',
		S.DREC		as 'DateLastReceived',
		(select 
			top 1 DDAT from PURHDR 
		 join 
			purlin		on PURHDR.TKEY = PURLIN.HKEY 
						where SKUN = S.SKUN 
						and PURLIN.DELE = 0 
						and PURHDR.RCOM = 0 
						and PURHDR.DELM = 0 
						order by DDAT desc) as 'DateNextReceipt' ,
		S.ONOR		as 'QtyOnOrder',
		S.AWSF		as 'ValueAverageSales4Weeks',
		S.ONHA		as 'QtyOnHand' 
	from 
		STKMAS S
	inner join
		supmas sup		on sup.supn = s.sup1
	left outer join
		fnSupplierGetFromString( @SupplierNumbers ) fn	on s.sup1= fn.Id
	left outer join
		supmas sm		on sm.supn = s.sup1 and len(@SupplierNumbers)=0
	where
		(sm.supn is not null or fn.id is not null)  
		and S.ONHA <= 0
		And S.IOBS = 0
		And S.IDEL = 0
		And S.IRIS = 0
		And S.ICAT =0
		And S.IMDN =0
		And S.NOOR =0
		And S.INON = 0
		and S.DATS is not null
		and (S.FODT is null or S.FODT >= (select TODT from SYSDAT))

		and (	(@SupplierType='D' AND S.SUPP = S.SUP1) 
			 OR (@SupplierType='W' AND S.SUPP <> S.SUP1)
			 or	(@SupplierType='A'))
            
end
GO

