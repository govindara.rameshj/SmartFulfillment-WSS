﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleCustomerUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SaleCustomerUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[SaleCustomerUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SaleCustomerUpdate'
GO
-- ===============================================================================
-- Author         : Partha Dutta
-- Date	          : 23/09/2011
-- Change Request : CR0054-01
-- TFS User Story : 2574
-- TFS Task ID    : 2595
-- Description    : BO-120 iteration's database layer (rollout/rollback scripts) refactored to deal with deployment to 
--                     A. Standard Store
--                     B. eStore 120
--                     C. eWarehouse 076
-- ===============================================================================

ALTER PROCEDURE [dbo].[SaleCustomerUpdate]
 @TransactionDate DATE,
 @TillNumber CHAR (2), 
 @TransactionNumber CHAR(4),
 @CustomerName CHAR(30),
 @HouseNumber CHAR(4) = null,
 @PostCode CHAR(8),
 @StoreNumber CHAR(3),
 @SaleDate DATE=null,
 @SaleTill CHAR(2),
 @SaleTransaction CHAR(4) ,
 @HouseNameNo CHAR(15),
 @Address1 CHAR(30),
 @Address2 CHAR(30),
 @Address3 CHAR(30),
 @PhoneNumber CHAR(15),
 @LineNumber INT = 0,
 @TransactionValidated BIT=0,
 @OriginalTenderType DECIMAL(3,0),
 @RefundReasonCode CHAR(2),
 @Labels BIT=0,
 @PhoneNumberMobile CHAR(15),
 @RTIFlag CHAR(1)='S',
 @EmailAddress CHAR(1),
 @PhoneNumberWork CHAR(1)
 
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE DLRCUS
	SET
		NAME = @CustomerName,
		HNUM = @HouseNumber,
		POST = @PostCode,
		OSTR = @StoreNumber,
		ODAT = @SaleDate,
		OTIL = @SaleTill,
		OTRN = @SaleTransaction ,
		HNAM = @HouseNameNo,
		HAD1 = @Address1 ,
		HAD2 = @Address2 ,
		HAD3 = @Address3 ,
		PHON = @PhoneNumber,
		OVAL = @TransactionValidated ,
		OTEN = @OriginalTenderType,
		RCOD = @RefundReasonCode,
		LABL = @Labels,
		MOBP = @PhoneNumberMobile ,
		RTI  = @RTIFlag,
		EmailAddress = @EmailAddress,
		PhoneNumberWork = @PhoneNumberWork 
	WHERE
		DATE1 = @TransactionDate and 
	   	TILL = @TillNumber and 
		[TRAN] = @TransactionNumber and
		NUMB = @LineNumber
	
return @@rowcount
END
GO

