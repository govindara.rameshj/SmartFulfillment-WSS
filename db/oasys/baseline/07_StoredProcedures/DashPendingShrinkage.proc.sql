﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashPendingShrinkage]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashPendingShrinkage'
	EXEC ('CREATE PROCEDURE [dbo].[DashPendingShrinkage] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashPendingShrinkage'
GO
ALTER PROCedure DashPendingShrinkage
as
begin
   set nocount on

   declare @Output table(RowId               int,
                         [Description]       varchar(50),
                         EmboldenThis        bit,
                         HighlightThis       bit,
                         TodayQuantity       int,
                         WeekToTodayQuantity int,
                         TodayValue          dec(9, 2),
                         WeekToTodayValue    dec(9, 2))

   declare @PendingDayQuantity int
   declare @PendingDayValue    dec(9, 2)

   set @PendingDayQuantity = dbo.svf_ReportPendingWriteOffQtyDAY()
   set @PendingDayValue    = dbo.svf_ReportPendingWriteOffValueDAY()

   insert into @Output values (1, 'Primary Count',      null, null, null,                null, null,             null)
   insert into @Output values (2, 'Pending Write Offs', null, null, @PendingDayQuantity, null, @PendingDayValue, null)

   update @Output set EmboldenThis  = case
                                         when RowId = 1 then 0
                                         when RowId = 2 and TodayQuantity > 0 then 1
                                         else 0
                                     end,
                     HighlightThis  = case
                                         when RowId = 1 then 0
                                         when RowId = 2 and TodayQuantity > 0 then 1
                                         else 0
                                     end

   select RowId,
          [Description],
          EmboldenThis,
          HighlightThis,
          [Today Qty]   = TodayQuantity,
          [WTD Qty]     = WeekToTodayQuantity,
          [Today Value] = TodayValue,
          [WTD Value]   = WeekToTodayValue
   from @Output

end
GO

