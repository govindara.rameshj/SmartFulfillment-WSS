﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SETPCE]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SETPCE'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SETPCE] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SETPCE'
GO
ALTER PROCEDURE dbo.KevanConversion_SETPCE
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 15 - Set Price Change Effective Days to 0..
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

Update SYSDAT
Set DPCA = '0'
Where FKEY = '01'

END;
GO

