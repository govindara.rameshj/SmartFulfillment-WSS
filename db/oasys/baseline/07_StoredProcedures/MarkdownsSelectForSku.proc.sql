﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MarkdownsSelectForSku]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure MarkdownsSelectForSku'
	EXEC ('CREATE PROCEDURE [dbo].[MarkdownsSelectForSku] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure MarkdownsSelectForSku'
GO
ALTER PROCEDURE [dbo].[MarkdownsSelectForSku]
	@skuNumber		char(6)
AS

SELECT		MarkdownStock.SkuNumber,
			MarkdownStock.DateCreated,
			MarkdownStock.Serial,
			MarkdownStock.ReasonCode,
			MarkdownStock.Price,
			MarkdownStock.WeekNumber,
			MarkdownStock.ReductionWeek1,
			MarkdownStock.ReductionWeek2,
			MarkdownStock.ReductionWeek3,
			MarkdownStock.ReductionWeek4,
			MarkdownStock.ReductionWeek5,
			MarkdownStock.ReductionWeek6,
			MarkdownStock.ReductionWeek7,
			MarkdownStock.ReductionWeek8,
			MarkdownStock.ReductionWeek9,
			MarkdownStock.ReductionWeek10,
			MarkdownStock.IsReserved,
			MarkdownStock.DateWrittenOff,
			MarkdownStock.SoldDate,
			MarkdownStock.SoldTill,
			MarkdownStock.SoldTransaction
			
FROM		MarkdownStock

WHERE		MarkdownStock.SkuNumber = @skunumber

ORDER BY	MarkdownStock.Serial
GO

