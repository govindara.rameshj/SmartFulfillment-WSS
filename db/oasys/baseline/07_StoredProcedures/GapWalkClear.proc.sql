﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GapWalkClear]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GapWalkClear'
	EXEC ('CREATE PROCEDURE [dbo].[GapWalkClear] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GapWalkClear'
GO
ALTER PROCedure GapWalkClear
   @Today as date
as
--set nocount on
delete GapWalk where DateCreated = @Today

return @@Rowcount
GO

