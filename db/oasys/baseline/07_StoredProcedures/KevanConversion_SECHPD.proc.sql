﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SECHPD]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SECHPD'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SECHPD] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SECHPD'
GO
ALTER PROCEDURE dbo.KevanConversion_SECHPD
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 8th July 2011
-- 
-- Task     : 31A - Apply Security Access Permissions for TP Helpdesk
-----------------------------------------------------------------------------------
AS
BEGIN
    
    SET NOCOUNT ON
        
	IF EXISTS (SELECT * FROM sys.server_principals WHERE name = 'TPPLC\IT_Helpdesk_G')
		Begin	
			Print ('User TPPLC\IT_Helpdesk_G exists in this SQL Server, skipping creation..')
		End	
	ELSE	
		Begin
			Print ('Creating user TPPLC\IT_Helpdesk_G..')
			declare @Domain char(5)
			declare @TpOrg char(3)
			declare @TpDepartment char(8)
			declare @TpGroup char(2)
			declare @Name char (13)
        
			DECLARE @SQL NVARCHAR(4000);
			DECLARE @SQL2 NVARCHAR(4000);
			DECLARE @SQLARM NVARCHAR(4000);
			DECLARE @SQLEXEC1 NVARCHAR(4000);
			DECLARE @SQLEXEC2 NVARCHAR(4000);
			DECLARE @SQLEXEC3 NVARCHAR(4000);
			DECLARE @SQLEXEC4 NVARCHAR(4000);
			DECLARE @SQLEXEC5 NVARCHAR(4000);
			DECLARE @SQLPERM1 NVARCHAR(4000);
			DECLARE @SQLPERM3 NVARCHAR(4000);
			DECLARE @SQLPERM4 NVARCHAR(4000);
			DECLARE @SQLPERM5 NVARCHAR(4000);
			DECLARE @SQLPERM6 NVARCHAR(4000);
        
			SET @Domain = 'TPPLC';
			SET @TpOrg = 'IT_'; 
			SET @TpDepartment = 'Helpdesk';
			SET @TpGroup = '_G';
			SET @Name = (@TpOrg + @TpDepartment + @TpGroup);
        
			--Use [Master]
			SET @SQL = 'CREATE LOGIN ['+ @Domain +'\'+ @Name +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]';
			EXECUTE(@SQL);
			SET @SQLARM = 'sys.sp_addsrvrolemember @loginname = ['+ @Domain +'\'+ @Name +'], @rolename = sysadmin';
			EXECUTE(@SQLARM);        

			--Use [Oasys]
			SET @SQL2 = 'CREATE USER [' + @Domain +'\'+ @Name + '] FOR LOGIN ['+ @Domain +'\'+ @Name + ']';
			EXECUTE(@SQL2);
        
			-- Set Server Roles for User Login
			SET @SQLEXEC1 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = role_execproc';
			EXECUTE(@SQLEXEC1);
			SET @SQLEXEC2 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = role_legacy';
			EXECUTE(@SQLEXEC2);
			SET @SQLEXEC3 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = db_datawriter';
			EXECUTE(@SQLEXEC3);
			SET @SQLEXEC4 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = db_datareader';
			EXECUTE(@SQLEXEC4);
			SET @SQLEXEC5 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = db_accessadmin';
			EXECUTE(@SQLEXEC5);
        
			-- Set Permissions for User Login
			SET @SQLPERM1 = 'GRANT ALTER TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION';
			EXECUTE(@SQLPERM1); 
			SET @SQLPERM3 = 'GRANT CONNECT TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM3);
			SET @SQLPERM4 = 'GRANT EXECUTE TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM4);
			SET @SQLPERM5 = 'GRANT SELECT TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM5);
			SET @SQLPERM6 = 'GRANT UPDATE TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM6);
		End	

END;
GO

