﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetNonStockDeletedWithStock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetNonStockDeletedWithStock'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetNonStockDeletedWithStock] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetNonStockDeletedWithStock'
GO
ALTER PROCEDURE dbo.StockGetNonStockDeletedWithStock @Status char( 1
                                                                 ) = 'A'
AS
BEGIN
    SET NOCOUNT ON;
    SELECT *
      FROM dbo.udf_GetDeletedOrNonStockWithOnHandStock( @Status
                                                      );
END;
GO

