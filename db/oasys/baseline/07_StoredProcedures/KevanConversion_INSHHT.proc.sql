﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_INSHHT]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_INSHHT'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_INSHHT] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_INSHHT'
GO
ALTER PROCEDURE dbo.KevanConversion_INSHHT
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 32 - Install HHT Information for Back Office
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF;

-- HHT Walkround
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '01')
	Begin
		Print ('HHT : Walkround already installed..')
	End
ELSE
	Begin
		Print ('HHT : Walkround Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) 
		VALUES (N'000', N'77', N'01', N'Walkaround', N'2', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Primary Count
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '02')
	Begin
		Print ('HHT : Primary Count already installed..')
	End
ELSE
	Begin
		Print ('HHT : Primary Count Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'02', N'Primary Count', N'6', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Price Changes
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '03')
	Begin
		Print ('HHT : Price Changes already installed..')
	End
ELSE
	Begin
		Print ('HHT : Price Changes Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'03', N'Price Changes', N'2', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Markdown/Writeoff
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '04')
	Begin
		Print ('HHT : Markdown/Writeoff already installed..')
	End
ELSE
	Begin
		Print ('HHT : Markdown/Writeoff Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'04', N'Markdown/Writeoff', N'6', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Gap Walk
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '05')
	Begin
		Print ('HHT : Gap Walk already installed..')
	End
ELSE
	Begin
		Print ('HHT : Gap Walk Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'05', N'Gap Walk', N'2', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Label Check
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '06')
	Begin
		Print ('HHT : Label Check already installed..')
	End
ELSE
	Begin
		Print ('HHT : Label Check Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'06', N'Label Check', N'2', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Label Setup
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '07')
	Begin
		Print ('HHT : Label Setup already installed..')
	End
ELSE
	Begin
		Print ('HHT : Label Setup Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'07', N'Label Set-up', N'2', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Label Check
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '11')
	Begin
		Print ('HHT : Walkround already installed..')
	End
ELSE
	Begin
		Print ('HHT : Walkround Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'11', N'Label Check', N'2', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Gap Walk
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '12')
	Begin
		Print ('HHT : Gap Walk already installed..')
	End
ELSE
	Begin
		Print ('HHT : Gap Walk Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'12', N'Gap Walk', N'2', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Markdown/Writeoff
IF EXISTS (Select AFGN From AFPCTL Where AFGN = '77' and NUMB = '13')
	Begin
		Print ('HHT : Markdown/Writeoff already installed..')
	End
ELSE
	Begin
		Print ('HHT : Markdown/Writeoff Installing..')
		INSERT [AFPCTL] ([LANG], [AFGN], [NUMB], [DESCR], [SECL], [SDPW], [DOSA], [DOSE], [IDOC], [SCOL], [PRUN], [LOCA], [SIGN], [SPAR]) VALUES (N'000', N'77', N'13', N'Markdown/Writeoff', N'6', 0, 0, CAST(0 AS Decimal(3, 0)), 0, CAST(11 AS Decimal(3, 0)), N'', 0, 0, N'')
	End


-- HHT Main Menu
IF EXISTS (Select NUMB From AFGCTL Where NUMB = '77')
	Begin
		Print ('HHT : Main Menu already installed..')
	End
ELSE
	Begin
		Print ('HHT : Main Menu Installing..')
		INSERT [AFGCTL] ([LANG], [NUMB], [DESCR], [DSCL], [SCOL], [DCOL], [TEXT1], [TEXT2], [TEXT3], [TEXT4], [TEXT5], [TEXT6], [TEXT7], [TEXT8], [TEXT9], [TEXT10], [TEXT11], [TEXT12], [TEXT13], [TEXT14], [TEXT15], [LOCA], [SPAR]) VALUES (N'000', N'77', N'HHT Main Menu', N'2', CAST(11 AS Decimal(3, 0)), CAST(14 AS Decimal(3, 0)), N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'')
	End


END;
GO

