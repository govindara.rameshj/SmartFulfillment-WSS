﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptGetLines]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptGetLines'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptGetLines] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptGetLines'
GO
ALTER PROCEDURE [dbo].[ReceiptGetLines]
@ReceiptNumber CHAR (6)
AS
BEGIN
	set nocount on;

	select		
		dt.numb		as 'ReceiptNumber',
		dt.seqn		as 'Sequence',
		dt.skun		as 'SkuNumber',
		st.descr	as 'SkuDescription',
		st.prod		as 'SkuProductCode',
		st.pack		as 'SkuPack',
		dt.ordq		as 'OrderQty',
		dt.ordp		as 'OrderPrice',
		dt.poln		as 'OrderLineId',
		dt.recq		as 'ReceivedQty',
		dt.pric		as 'ReceivedPrice',
		dt.retq		as 'ReturnQty',
		dt.IBTQ		as 'IbtQty',
		dt.RTI		as 'RtiState',
		dt.ReasonCode

	from		
		drldet dt
	inner join	
		stkmas st on st.skun = dt.skun
	where		
		dt.numb = @ReceiptNumber
	order by	
		dt.seqn

END
GO

