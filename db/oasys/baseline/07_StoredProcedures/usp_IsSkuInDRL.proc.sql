﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_IsSkuInDRL]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_IsSkuInDRL'
	EXEC ('CREATE PROCEDURE [dbo].[usp_IsSkuInDRL] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_IsSkuInDRL'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 15/06/2011
-- Description:	Is the Sku number in the DRL defined by DRLNumber
-- =============================================
ALTER PROCEDURE [dbo].[usp_IsSkuInDRL] 
	-- Add the parameters for the stored procedure here
	@DRLNumber Char(6) , 
	@SkuNumber Char(6)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		SKUN 
	FROM 
		DRLDET dt 
	WHERE 
		dt.NUMB =	@DRLNumber AND dt.SKUN = @SkuNumber
END
GO

