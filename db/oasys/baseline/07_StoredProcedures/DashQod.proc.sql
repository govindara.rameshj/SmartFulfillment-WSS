﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashQod]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashQod'
	EXEC ('CREATE PROCEDURE [dbo].[DashQod] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashQod'
GO
ALTER PROCedure [dbo].[DashQod]
   @DateEnd date
as
begin
   set nocount on

   declare @Today  table(RowId int, Quantity int, Value dec(11,2))
   declare @Total  table(RowId int, Quantity int, Value dec(11,2))
   declare @Output table(RowId         int,
                         EmboldenThis  bit,
                         HighlightThis bit,
                         [Description] varchar(50),
                         TodayQuantity int,
                         TotalQuantity int,
                         TodayValue    dec(11,2),
                         TotalValue    dec(11,2))

   insert @Today(RowId, Quantity, Value)   select 1, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus <  300                          and DateDelivery <= dateadd(d, 1, @DateEnd)
   insert @Today(RowId, Quantity, Value)   select 2, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 300 and DeliveryStatus < 500 and DateDelivery <= dateadd(d, 1, @DateEnd)
   insert @Today(RowId, Quantity, Value)   select 3, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 500 and DeliveryStatus < 600 and DateDelivery <= dateadd(d, 1, @DateEnd)
   insert @Today(RowId, Quantity, Value)   select 4, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 600 and DeliveryStatus < 700 and DateDelivery = @DateEnd
   insert @Today(RowId, Quantity, Value)   select 5, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 700 and DeliveryStatus < 800 and DateDelivery = @DateEnd
   insert @Today(RowId, Quantity, Value)   select 6, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 800 and DeliveryStatus < 900 and DateDelivery = @DateEnd
   insert @Today(RowId, Quantity, Value)   select 7, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 900 and DeliveryStatus < 999 and DateDelivery = @DateEnd
   insert @Today(RowId, Quantity, Value)   select 8, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 999                          and DateDelivery = @DateEnd
   insert @Today(RowId, Quantity, Value)   select 9, count(*), coalesce(sum(value), 0) from vwQod where IsSuspended     = 1                            and DateDelivery = @DateEnd

   insert @Total(RowId, Quantity, Value) select 1, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus  < 300
   insert @Total(RowId, Quantity, Value) select 2, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus  > 299 and DeliveryStatus < 500
   insert @Total(RowId, Quantity, Value) select 3, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 500 and DeliveryStatus < 600
   insert @Total(RowId, Quantity, Value) select 4, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 600 and DeliveryStatus < 700
   insert @Total(RowId, Quantity, Value) select 5, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 700 and DeliveryStatus < 800
   insert @Total(RowId, Quantity, Value) select 6, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 800 and DeliveryStatus < 900
   insert @Total(RowId, Quantity, Value) select 7, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 900 and DeliveryStatus < 999
   insert @Total(RowId, Quantity, Value) select 8, count(*), coalesce(sum(value), 0) from vwQod where DeliveryStatus >= 999
   insert @Total(RowId, Quantity, Value) select 9, count(*), coalesce(sum(value), 0) from vwQod where IsSuspended     = 1

   insert @Output(RowId, TotalQuantity, TotalValue) select RowId, Quantity, Value from @Total

   update @Output set TodayQuantity = b.Quantity,
                      TodayValue    = b.Value
   from @Output a
   inner join @Today b
         on b.RowId = a.RowId

   update @Output set [Description] = case RowId
                                         when 1 then 'Unconfirmed Orders'
                                         when 2 then 'Confirmed Orders'
                                         when 3 then 'Orders in Picking'
                                         when 4 then 'Confirmed Picked Orders'
                                         when 5 then 'Orders in Despatch'
                                         when 6 then 'Undelivered'
                                         when 7 then 'Delivered'
                                         when 8 then 'Completed'
                                         when 9 then 'Suspended'
                                      end,
                      EmboldenThis  = case
                                         when RowId = 1 and (TodayQuantity > 0 or TotalQuantity > 0) then 1
                                         when RowId = 6 and (TodayQuantity > 0 or TotalQuantity > 0) then 1
                                         when RowId = 9 and (TodayQuantity > 0 or TotalQuantity > 0) then 1
                                         else 0
                                      end,
                      HighlightThis = case
                                         when RowId = 1 and (TodayQuantity > 0 or TotalQuantity > 0) then 1
                                         when RowId = 6 and (TodayQuantity > 0 or TotalQuantity > 0) then 1
                                         when RowId = 9 and (TodayQuantity > 0 or TotalQuantity > 0) then 1
                                         else 0
                                      end

   select RowId,
          EmboldenThis,
          HighlightThis,
          [Description],
          [Today Qty]   = TodayQuantity,
          [Total Qty]   = TotalQuantity,
          [Today Value] = TodayValue,
          [Total Value] = TotalValue
    from @Output order by RowId

end
GO

