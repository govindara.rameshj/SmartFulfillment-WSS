﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_AddOrUpdateCollectionLines]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_AddOrUpdateCollectionLines'
	EXEC ('CREATE PROCEDURE [dbo].[usp_AddOrUpdateCollectionLines] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_AddOrUpdateCollectionLines'
GO
ALTER PROCEDURE usp_AddOrUpdateCollectionLines  @Id int = null,
                                              @CollectionId int , 
                                              @Description varchar( 100
                                                                  ) , 
                                              @Weight int , 
                                              @Quantity int , 
                                              @TotalWeight int , 
                                              @DeliveryDate date
AS
BEGIN
    IF EXISTS( SELECT 1
                 FROM VehicleCollectionLine
                 WHERE ID
                       = 
                       @Id
                 AND
                      CollectionID
                       = 
                       @collectionid
                  AND DeliveryDate
                       = 
                       @DeliveryDate
             )
        BEGIN
            UPDATE VehicleCollectionLine
            SET Description = @Description , 
                Weight = @Weight , 
                Quantity = @Quantity , 
                TotalWeight = @TotalWeight
              WHERE CollectionID
                    = 
                    @CollectionID
             AND ID = @Id;
        END;
    ELSE
        BEGIN
            INSERT INTO VehicleCollectionLine( CollectionID,
             [Description] , 
                                                Weight , 
                                                Quantity , 
                                                TotalWeight , 
                                                DeliveryDate
                                              )
            VALUES( @CollectionId,
                    @Description , 
                    @Weight , 
                    @Quantity , 
                    @TotalWeight , 
                    @DeliveryDate
                  );
        END;
 SELECT SCOPE_IDENTITY()
END;
GO

