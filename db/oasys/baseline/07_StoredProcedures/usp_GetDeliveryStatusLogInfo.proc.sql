﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetDeliveryStatusLogInfo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetDeliveryStatusLogInfo'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetDeliveryStatusLogInfo] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetDeliveryStatusLogInfo'
GO
ALTER PROCEDURE [dbo].[usp_GetDeliveryStatusLogInfo]
@DateInterestedIn DateTime
AS
BEGIN
	SELECT 
		chl.NewStatus, 
		COUNT(*) as Total 
	FROM CORHDR4LOG chl
	LEFT JOIN vwCORHDRFull vch ON vch.NUMB = isnull(chl.NUMB, '')
	WHERE chl.DateChanged = @DateInterestedIn and (chl.NUMB is null or vch.SHOW_IN_UI = 1) 
	GROUP BY chl.NewStatus 
END
GO

