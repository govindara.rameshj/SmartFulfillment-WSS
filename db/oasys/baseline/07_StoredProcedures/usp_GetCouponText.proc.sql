﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetCouponText]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetCouponText'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetCouponText] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetCouponText'
GO
-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 06/10/2011
-- TFSID        : 2733 
-- Description  : New Stored Procedure for Coupons ProcessTransmissions
-- =====================================================================

-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================


ALTER PROCEDURE usp_GetCouponText
@couponID   AS CHAR(7), 
@SequenceNo AS CHAR(2) 
AS 
  BEGIN 
      SELECT couponid, 
             sequenceno, 
             printsize, 
             textalign, 
             printtext, 
             deleted 
      FROM   COUPONTEXT 
      WHERE  couponid = @couponID 
             AND sequenceno = @SequenceNo 
  END
GO

