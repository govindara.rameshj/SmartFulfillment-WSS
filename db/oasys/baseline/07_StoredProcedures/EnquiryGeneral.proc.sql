﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryGeneral]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryGeneral'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryGeneral] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryGeneral'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 05/09/2012
-- User Story	 : 7677
-- Project		 : Stock Item Enquiry - Secondary Supplier
-- Task Id		 : 7678
-- Description   : Add new Warehouse (2ndary supplier) hyperlink row
--				 : and correct supplier number in current supplier link
-- =============================================
ALTER PROCedure [dbo].[EnquiryGeneral]
	@SkuNumber Char (6)
As
Begin
	Set NOCOUNT On;

	Declare 
		@Table Table 
			(
				RowId			Int,
				SkuNumber		Char(6), 
				SupplierNumber	Char(5),
				HighlightThis	Bit Default 0,
				EmboldenThis	Bit Default 0,
				[Description]	VarChar(70),
				Display			VarChar(10),
				[Date]			Date
			);
	Declare
		@isObsolete		 VarChar(3),
		@dateObsolete	 Date,
		@isDeleted		 VarChar(3),
		@dateDeleted	 Date,
		@SupplierNumber	 Char(5),
		@WarehouseNumber Char(5),
		@WarehouseNumberCaption Char(5),
		@isNonStock		 VarChar(3),
		@isNonOrder		 VarChar(3),
		@serviceLevel	 Dec(5,2),
		@OverrideColour  Int,
		@isObsoleteHightlighted Bit,
		@isObsoleteEmboldened   Bit,
		@isDeletedHightlighted  Bit,
		@isDeletedEmboldened    Bit,
		@isNonStockHightlighted Bit,
		@isNonStockEmboldened   Bit,
		@isNonOrderHightlighted Bit,
		@isNonOrderEmboldened   Bit
		;

	Select
		@isObsolete				= (Select Case IOBS When 1 Then 'Yes' Else 'No' End),
		@isObsoleteHightlighted = IOBS,
		@isObsoleteEmboldened   = IOBS,
		@dateObsolete			= DOBS,
		@isDeleted				= (Select Case IDEL When 1 Then 'Yes' Else 'No' End),
		@isDeletedHightlighted  = IDEL,
		@isDeletedEmboldened    = IDEL,
		@dateDeleted			= DDEL,
		@isNonStock				= (Select Case INON When 1 Then 'Yes' Else 'No' End),
		@isNonStockHightlighted = INON,
		@isNonStockEmboldened   = INON,
		@OverrideColour			= (Select Case INON When 1 Then -1074417 Else Null End),
		@isNonOrder				= (Select Case NOOR When 1 Then 'Yes' Else 'No' End),
		@isNonOrderHightlighted = NOOR,
		@isNonOrderEmboldened   = NOOR,
		@serviceLevel			= ServiceLevelOverride,
		@SupplierNumber			= SUPP,
		@WarehouseNumber		= 
			(
				Select
					Case When SUP1 = SUPP Then Null
					Else SUP1
				End
			),
		@WarehouseNumberCaption	= 
			(
				Select
					Case When SUP1 = SUPP Then 'N/A'
					Else SUP1
				End
			)
	From
		STKMAS
	Where
		SKUN = @SkuNumber;
		
	Insert Into 
		@Table 
			(
				RowId,
				HighlightThis,
				EmboldenThis,
				[Description], 
				Display, 
				[Date]
			) 
		Values 
			(
				1,
				@isObsoleteHightlighted,
				@isObsoleteEmboldened,
				'Obsolete',
				 @isObsolete, 
				 @dateObsolete
			);
	Insert Into 
		@Table 
			(
				RowId,
				HighlightThis,
				EmboldenThis,				
				[Description],
				Display,
				[Date]
			) 
		Values 
			(
				2,
				@isDeletedHightlighted,
				@isDeletedEmboldened,
				'Deleted',
				@isDeleted,
				@dateDeleted
			);
	Insert Into 
		@Table 
			(
				RowId,
				HighlightThis,
				EmboldenThis,
				[Description],
				Display
			) 
		Values 
			(
				3,
				@isNonStockHightlighted,
				@isNonStockEmboldened,
				'Item is Non Stocked',
				@isNonStock
			);
	Insert Into 
		@Table 
			(
				RowId,
				HighlightThis,
				EmboldenThis,				
				[Description],
				Display
			) 
		Values 
			(
				4,
				@isNonOrderHightlighted,
				@isNonOrderEmboldened,
				'Item is Non Orderable',
				@isNonOrder
			);
	Insert Into @Table([Description], Display) Values ('Service Level', @serviceLevel);
	Insert Into @Table(Description, Display) Values ('', '');
	Insert Into @Table(RowId, SupplierNumber, Description, Display) Values (6, @SupplierNumber, 'Supplier', @SupplierNumber);
	Insert Into @Table(RowId, SupplierNumber, Description, Display) Values (7, @WarehouseNumber, 'Warehouse', @WarehouseNumberCaption);
	Insert Into @Table(Description, Display) Values ('', '');
	Insert Into @Table(RowId, SkuNumber, Description) Values (5, @SkuNumber, 'Hierarchy');
	
	Select * From @table;
End
GO

