﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingActiveCashierSystemSale]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingActiveCashierSystemSale'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingActiveCashierSystemSale] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingActiveCashierSystemSale'
GO
ALTER PROCedure NewBankingActiveCashierSystemSale
   @PeriodID  int,
   @CashierID int
as
begin
set nocount on
--cash tender only "over tender" situation now catered for
--all other tenders have been ignored!
select TenderID    = a.TenderID,
       TenderValue = b.Amount
from (select TenderID
      from SystemCurrencyDen
      where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
      group by TenderID) a
left outer join (select ID     = a.ID,
                        Amount = sum(a.Amount)
                 from (select ID = case ID
                                     when 99 then 1
                                     else ID
                                   end,
                                   Amount
                       from CashBalCashierTen 
                       where PeriodID   = @PeriodID
                       and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                       and   CashierID  = @CashierID) a
                 group by a.ID) b
           on b.ID = a.TenderID
end
GO

