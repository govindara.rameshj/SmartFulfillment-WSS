﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SYSUSR]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SYSUSR'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SYSUSR] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SYSUSR'
GO
ALTER PROCEDURE dbo.KevanConversion_SYSUSR
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.1
-- Author   : Kevan Madelin
-- Date	    : 1st July 2011
-- 
-- Task     : 42 - Update the User Configurations
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Task     : 42/01 - Insert/Update User 555 (Generic Vision User)
----------------------------------------------------------------------------------- 
IF NOT EXISTS (Select Id FROM [Oasys].[dbo].[SystemUsers] WHERE Id = 555)
	Begin
		Insert Into SystemUsers 
				(
				ID, EmployeeCode, Name, Initials, Position, PayrollID, Password, PasswordExpires, 
				IsManager, IsSupervisor, SupervisorPassword, SupervisorPwdExpires, Outlet, IsDeleted, 
				DeletedDate, DeletedTime, DeletedBy, DeletedWhere, TillReceiptName, DefaultAmount, 
				LanguageCode, SecurityProfileID
				) 
		values		( 
				555,'555','Temp Vision User','SYS','System','000000','14725',CAST(0xFD410B00 AS Date),
				0,0,'',NULL,'60',0,
				NULL,'000000','000','00','Vision User',CAST(100.00 AS Decimal(9, 2)),
				'000','109'
				)	
	End
ELSE
	Begin
		Update		SystemUsers
		Set		Name 			= 	'Temp Vision User',
				Initials		=	'SYS',
				Position		= 	'System',
				PayrollID		=	'000000',
				Password		=	'14725',
				PasswordExpires		=	CAST(0xFD410B00 AS Date),
				IsManager		=	0,
				IsSupervisor		=	0,
				SupervisorPassword	=	'',
				SupervisorPwdExpires	=	NULL,
				Outlet			=	'60',
				IsDeleted		=	0,
				DeletedDate		=	NULL,
				DeletedTime		=	'000000',
				DeletedBy		=	'000',
				DeletedWhere		=	'00',
				TillReceiptName		=	'Vision User',
				DefaultAmount		=	CAST(100.00 AS Decimal(9, 2)),
				LanguageCode		=	'000',
				SecurityProfileID 	= 	'109'
		Where		Id = 555
	End


-----------------------------------------------------------------------------------
-- Task     : 42/01 - Insert/Update User 844 (TP IT Helpdesk)
----------------------------------------------------------------------------------- 
IF NOT EXISTS (Select Id FROM [Oasys].[dbo].[SystemUsers] WHERE Id = 844)
	Begin
		Insert Into SystemUsers 
				(
				ID, EmployeeCode, Name, Initials, Position, PayrollID, Password, PasswordExpires, 
				IsManager, IsSupervisor, SupervisorPassword, SupervisorPwdExpires, Outlet, IsDeleted, 
				DeletedDate, DeletedTime, DeletedBy, DeletedWhere, TillReceiptName, DefaultAmount, 
				LanguageCode, SecurityProfileID
				) 
		values		( 
				844,'844','TP Support Desk','SYS','Support','000000','01604',CAST(0x8D4A0B00 AS Date),
				1,1,'92592',CAST(0x8D4A0B00 AS Date),'60',0,
				NULL,'000000','000','00','TP Support Desk',CAST(100.00 AS Decimal(9, 2)),
				'000','108'
				)	
	End
ELSE
	Begin
		Update		SystemUsers
		Set		Name 			= 	'TP Support Desk',
				Initials		=	'SYS',
				Position		= 	'Support',
				PayrollID		=	'000000',
				Password		=	'01604',
				PasswordExpires		=	CAST(0x8D4A0B00 AS Date),
				IsManager		=	1,
				IsSupervisor		=	1,
				SupervisorPassword	=	'92592',
				SupervisorPwdExpires	=	CAST(0x8D4A0B00 AS Date),
				Outlet			=	'60',
				IsDeleted		=	0,
				DeletedDate		=	NULL,
				DeletedTime		=	'000000',
				DeletedBy		=	'000',
				DeletedWhere		=	'00',
				TillReceiptName		=	'TP Support Desk',
				DefaultAmount		=	CAST(100.00 AS Decimal(9, 2)),
				LanguageCode		=	'000',
				SecurityProfileID 	= 	'108'
		Where		Id = 844
	End


-----------------------------------------------------------------------------------
-- Task     : 42/01 - Insert/Update User 888 (IT Support)
----------------------------------------------------------------------------------- 
IF NOT EXISTS (Select Id FROM [Oasys].[dbo].[SystemUsers] WHERE Id = 888)
	Begin
		Insert Into SystemUsers 
				(
				ID, EmployeeCode, Name, Initials, Position, PayrollID, Password, PasswordExpires, 
				IsManager, IsSupervisor, SupervisorPassword, SupervisorPwdExpires, Outlet, IsDeleted, 
				DeletedDate, DeletedTime, DeletedBy, DeletedWhere, TillReceiptName, DefaultAmount, 
				LanguageCode, SecurityProfileID
				) 
		values		( 
				888,'888','IT Support','SYS','Support','000000','01604',CAST(0x8D4A0B00 AS Date),
				1,1,'92592',CAST(0x8D4A0B00 AS Date),'60',0,
				NULL,'000000','000','00','IT Support',CAST(100.00 AS Decimal(9, 2)),
				'000','109'
				)	
	End
ELSE
	Begin
		Update		SystemUsers
		Set		Name 			= 	'IT Support',
				Initials		=	'SYS',
				Position		= 	'Support',
				PayrollID		=	'000000',
				Password		=	'01604',
				PasswordExpires		=	CAST(0x8D4A0B00 AS Date),
				IsManager		=	1,
				IsSupervisor		=	1,
				SupervisorPassword	=	'92592',
				SupervisorPwdExpires	=	CAST(0x8D4A0B00 AS Date),
				Outlet			=	'60',
				IsDeleted		=	0,
				DeletedDate		=	NULL,
				DeletedTime		=	'000000',
				DeletedBy		=	'000',
				DeletedWhere		=	'00',
				TillReceiptName		=	'IT Support',
				DefaultAmount		=	CAST(100.00 AS Decimal(9, 2)),
				LanguageCode		=	'000',
				SecurityProfileID 	= 	'109'
		Where		Id = 888
	End

END;
GO

