﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReturnsSelectDetailsForId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReturnsSelectDetailsForId'
	EXEC ('CREATE PROCEDURE [dbo].[ReturnsSelectDetailsForId] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReturnsSelectDetailsForId'
GO
ALTER PROCEDURE [dbo].[ReturnsSelectDetailsForId]
@Id INT
AS
begin
	SELECT		
		rl.TKEY, 
		rl.HKEY, 
		rl.SKUN, 
		sk.DESCR AS SkuDescription, 
		rl.QUAN, 
		sk.PRIC, 
		rl.COST, 
		rl.QUAN * sk.PRIC AS Value,
		rl.REAS, 
		sc.DESCR AS ReasonDescription, 
		rl.RTI
	FROM        
		RETLIN rl 
	INNER JOIN 
		STKMAS sk ON rl.SKUN = sk.SKUN 
	INNER JOIN 
		SYSCOD sc ON right('00' + rtrim(rl.REAS), 2) = sc.CODE and sc.TYPE = 'SR'
	WHERE
		rl.HKEY = @Id
	ORDER BY	
		SkuDescription

end
GO

