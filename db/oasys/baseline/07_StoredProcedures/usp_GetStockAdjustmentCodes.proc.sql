﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetStockAdjustmentCodes]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetStockAdjustmentCodes'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetStockAdjustmentCodes] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetStockAdjustmentCodes'
GO
ALTER PROCedure usp_GetStockAdjustmentCodes
    @SecurityLevel char(3) = '009',
    @CodeNumber    char(2) = null
as
begin
    set nocount on

select 
		NUMB as 'CodeNumber',
		DESCR as 'Description',
		TYPE as 'CodeType',
		SIGN as 'CodeSign',
		SECL as 'SecurityLevel',
		IMDN as 'IsMarkDown',
		IWTF as 'IsWriteOff',
		IsAuthRequired,
		IsPassRequired,
		IsReserved,
		IsCommentable,
		IsStockLoss,
		IsKnownTheft,
		IsIssueQuery,
		AllowCodes
from SACODE sa
where cast(sa.SECL as int) <= cast(@SecurityLevel as int)
and   @CodeNumber is null or (@CodeNumber is not null and sa.NUMB = @CodeNumber and cast(sa.SECL as int) <= cast(@SecurityLevel as int))
end
GO

