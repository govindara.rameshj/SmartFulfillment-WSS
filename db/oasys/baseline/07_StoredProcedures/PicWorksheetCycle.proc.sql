﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PicWorksheetCycle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PicWorksheetCycle'
	EXEC ('CREATE PROCEDURE [dbo].[PicWorksheetCycle] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PicWorksheetCycle'
GO
ALTER PROCEDURE [dbo].[PicWorksheetCycle]
@CycleNumber INT
AS
BEGIN
	SET NOCOUNT ON;

	select
		sk.SKUN				as 'SkuNumber',
		sk.DESCR			as 'Description',
		sk.PACK				as 'PackSize',		
		(case sk.IRIS when 1 then (select rl.PPOS from RELITM rl where rl.SPOS=sk.SKUN) end) as 'SkuRelated',
		''					as 'QtyShopFloor',
		''					as 'QtyWarehouse',
		''					as 'QtyTotal',
		''					as 'QtyPreSold',
		sk.ONHA				as 'QtyOnHand',
		''					as 'QtyVariance',
		sk.PRIC				as 'Price',
		'YES / NO'			as 'SELYesNo',
		pc.DAYN				as 'CycleNumber'
	from
		STKMAS sk
	inner join
		PICCTL pc	on pc.CTGY=sk.CTGY and pc.DAYN = @CycleNumber
	where
		(pc.GRUP='000000' or (pc.GRUP <>'000000' and pc.GRUP=sk.GRUP))
		and (pc.SGRP='000000' or (pc.SGRP <>'000000' and pc.SGRP=sk.SGRP))
		and (pc.STYL='000000' or (pc.STYL <>'000000' and pc.STYL=sk.STYL))
	order by
		sk.SKUN

END
GO

