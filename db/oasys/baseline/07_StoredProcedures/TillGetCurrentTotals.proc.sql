﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TillGetCurrentTotals]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure TillGetCurrentTotals'
	EXEC ('CREATE PROCEDURE [dbo].[TillGetCurrentTotals] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure TillGetCurrentTotals'
GO
ALTER PROCEDURE [dbo].[TillGetCurrentTotals]
AS
BEGIN
	SET NOCOUNT ON;

	declare 
		@PeriodId	int,
		@CurrencyId	char(3);
	
	set @PeriodId = (select ID from SystemPeriods where StartDate <= GETDATE() and GETDATE() <= EndDate);
	set @CurrencyId = (select ID from SystemCurrency where IsDefault=1);
	
	select
		ctt.tillid														as TillId,
		ws.Description													as [Description],
		sum(ctt.Quantity)												as [Count],
		sum(case ctt.ID when 1 then ctt.Amount else 0 end)				as ValueCash,
		sum(case ctt.id when 1 then 0 else ctt.Amount end)				as ValueNonCash,
		(ct.FloatIssued-ct.FloatReturned)								as [Float],
		SUM(ctt.PickUp)													as Pickups,
		(SUM(ctt.Amount-ctt.PickUp)+(ct.FloatIssued-ct.FloatReturned))	as Balance
	from 
		CashBalTillTen ctt
	inner join
		CashBalTill ct on ct.TillID=ctt.TillID and ct.PeriodID = ctt.PeriodID and ct.CurrencyID = ctt.CurrencyID
	inner join
		WorkStationConfig ws on ws.ID = ctt.TillID
	where
		ctt.PeriodID = @PeriodId
		and ctt.CurrencyID = @CurrencyId
	group by
		ctt.TillID,
		ws.Description,
		ct.FloatIssued-ct.FloatReturned
			
END
GO

