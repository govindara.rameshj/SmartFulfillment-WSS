﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashShrinkage]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashShrinkage'
	EXEC ('CREATE PROCEDURE [dbo].[DashShrinkage] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashShrinkage'
GO
ALTER PROCedure [dbo].[DashShrinkage]
   @DateEnd date
as
begin
   set nocount on

   declare @Output table(
                         RowId Int,
                         [Description]       varchar(50),
                         TodayQuantity       int,
                         WeekToTodayQuantity int,
                         TodayValue          dec(9,2),
                         WeekToTodayValue    dec(9,2),
                         SelectedDate Date
                         )

   declare @StartDate                         date,
           @PriceViolationTodayQuantity       int,
           @PriceViolationWeekToTodayQuantity int,
           @PriceViolationTodayValue          dec(9, 2),
           @PriceViolationWeekToTodayValue    dec(9, 2),
           @MarkdownsTodayQuantity            int,
           @MarkdownsWeekToTodayQuantity      dec(9, 2),
           @MarkdownsTodayValue               int,
           @MarkdownsWeekToTodayValue         dec(9, 2),
           @StockLossDayQty                   int,
           @StockLossDayValue                 dec(9,2),
           @StockLossWeekQty                  int,
           @StockLossWeekValue                dec(9,2),
           @KnownDayQty                       int,
           @KnownDayValue                     dec(9,2),
           @KnownWeekQty                      int,
           @KnownWeekValue                    dec(9,2),
           @WriteOffDayQty                    int,
           @WriteOffDayValue                  dec(9,2),
           @WriteOffWeekQty                   int,
           @WriteOffWeekValue                 dec(9,2)
	
	--set startdate to sunday before enddate
	set	@StartDate = @DateEnd;
	While datepart(weekday, @StartDate) <> 1
	begin
		set @StartDate = dateadd(day, -1, @StartDate)
	end	
	
	--get stock loss/known theft adjustment today
	select
		@StockLossDayQty	= sum(case sc.IsStockLoss when 1 then sa.quan else 0 end),	
	 	@StockLossDayValue	= sum(case sc.IsStockLoss when 1 then sa.quan * sa.pric else 0 end),
	 	@KnownDayQty		= sum(case sc.IsKnownTheft when 1 then sa.quan else 0 end),
	 	@KnownDayValue		= sum(case sc.IsKnownTheft when 1 then sa.quan * sa.pric else 0 end),
		@WriteOffDayQty		= sum(case sa.mowt when 'W' then sa.quan else 0 end)
	from	
		stkadj sa
	inner join
		sacode sc	on sc.numb = sa.code
	where	
		sa.DATE1	= @DateEnd
		
	-- get write offs adjustment today
	select
		@WriteOffDayQty		= isnull(sum(case sa.mowt when 'W' then sa.quan else 0 end), 0),	
	 	@WriteOffDayValue	= isnull(sum(case sa.mowt when 'W' then sa.quan * sa.pric else 0 end), 0)
	from	
		stkadj sa
	inner join
		sacode sc	on sc.numb = sa.code
	where	
		sa.DAUT = @DateEnd
	
	--get stock loss/known adjustment week
	select
		@StockLossWeekQty	= sum(case sc.IsStockLoss when 1 then sa.quan else 0 end),	
	 	@StockLossWeekValue	= sum(case sc.IsStockLoss when 1 then sa.quan * sa.pric else 0  end),
	 	@KnownWeekQty		= sum(case sc.IsKnownTheft when 1 then sa.quan else 0 end),
	 	@KnownWeekValue		= sum(case sc.IsKnownTheft when 1 then sa.quan * sa.pric else 0 end)
	from	
		stkadj sa
	inner join
		sacode sc	on sc.numb = sa.code /*and sc.IsStockLoss = 1*/
	where	
		sa.DATE1		<= @DateEnd
		and sa.DATE1	>= @StartDate		
	
	--get write offs adjustment week
	select
		@WriteOffWeekQty	= isnull(sum(case sa.MOWT when 'W' then sa.quan else 0 end), 0),	
	 	@WriteOffWeekValue	= isnull(sum(case sa.MOWT when 'W' then sa.quan * sa.pric else 0 end), 0)
	from	
		stkadj sa
	inner join
		sacode sc	on sc.numb = sa.code /*and sc.IsStockLoss = 1*/
	where	
		sa.DAUT <= @DateEnd
		and sa.DAUT >= @StartDate	




   select @PriceViolationTodayQuantity = count(dl.QUAN),
          @PriceViolationTodayValue    = sum(dl.POPD * dl.QUAN) * -1
   from DLTOTS dt
   inner join DLLINE dl
         on  dl.DATE1  = dt.DATE1
         and dl.TILL   = dt.TILL
         and dl.[TRAN] = dt.[TRAN]
   where dt.DATE1 = @DateEnd
   and   dt.CASH  <> '000'
   and   dt.VOID  = 0
   and   dt.PARK  = 0
   and   dt.TMOD  = 0
   and   dl.IMDN  = 0
   and   dl.POPD  > 0
   and   dl.LREV  = 0
   and   dl.PORC <> 0

   select @PriceViolationWeekToTodayQuantity = count(dl.QUAN),
          @PriceViolationWeekToTodayValue    = sum(dl.POPD * dl.QUAN) * -1
   from DLTOTS dt
   inner join DLLINE dl
         on  dl.DATE1  = dt.DATE1 
         and dl.TILL   = dt.TILL
         and dl.[TRAN] = dt.[TRAN]
   where dt.DATE1 <= @DateEnd
   and   dt.DATE1 >= @StartDate
   and   dt.CASH <> '000'
   and   dt.VOID  = 0
   and   dt.PARK  = 0
   and   dt.TMOD  = 0
   and   dl.IMDN  = 0
   and   dl.POPD  > 0
   and   dl.LREV  = 0
   and   dl.PORC <> 0

   select @MarkdownsTodayQuantity = count(QUAN),
          @MarkdownsTodayValue    = sum(POPD * QUAN) * -1
   from DLLINE
   where DATE1 = @DateEnd
   and   IMDN  = 1

   select @MarkdownsWeekToTodayQuantity = count(QUAN),
          @MarkdownsWeekToTodayValue    = sum(POPD * QUAN) * -1
   from DLLINE
   where DATE1 <= @DateEnd
   and   DATE1 >= @StartDate
   and   IMDN   = 1

   insert into @Output values (1,'Stock Loss',            @StockLossDayQty,             @StockLossWeekQty,                  @StockLossDayValue,             @StockLossWeekValue,@DateEnd)
   insert into @Output values (2,'Known Theft',           @KnownDayQty,                 @KnownWeekQty,                      @KnownDayValue,                 @KnownWeekValue,@DateEnd)
   insert into @Output values (3,'Write Off Adjustments', @WriteOffDayQty,              @WriteOffWeekQty,                   @WriteOffDayValue,              @WriteOffWeekValue,@DateEnd)
   insert into @Output values (4,'Markdowns',             @MarkdownsTodayQuantity,      @MarkdownsWeekToTodayQuantity,      @MarkdownsTodayValue * -1,      @MarkdownsWeekToTodayValue * -1,@DateEnd)
   insert into @Output values (5,'Price Violations',	    @PriceViolationTodayQuantity, @PriceViolationWeekToTodayQuantity, @PriceViolationTodayValue * -1, @PriceViolationWeekToTodayValue * -1,@DateEnd)
   insert into @Output values (6,'Incorrect Day Price',   null,                         null,                               null,                           null,@DateEnd)
   insert into @Output values (7,'Temporary Deal Group',  null,                         null,                               null,                           null,@DateEnd)

   select 
          RowId,
          [Description],
          [No. Adj. Day] = TodayQuantity,
          [No. Adj. WTD] = WeekToTodayQuantity,
          Total          = TodayValue,
          WTD            = WeekToTodayValue,
          SelectedDate
   from @Output

end
GO

