﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TenderTypeGetTypes]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure TenderTypeGetTypes'
	EXEC ('CREATE PROCEDURE [dbo].[TenderTypeGetTypes] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure TenderTypeGetTypes'
GO
ALTER PROCedure [dbo].[TenderTypeGetTypes]
AS
BEGIN
	declare @Table TABLE ( Id char(6), TenderDescription char(30) )

	insert into @Table values (null, '---All---');
	insert into @Table select TTID, TTDE from dbo.TENCTL WHERE IUSE = 1 AND TTID NOT IN (8, 9) UNION select TenderID, DisplayText FROM SystemCurrencyDen WHERE TenderID IN (10, 12)
	
	UPDATE @Table SET TenderDescription = 'Credit/Debit Card' WHERE TenderDescription LIKE 'Cr/Dr Card'
	
	select * from @Table
END
GO

