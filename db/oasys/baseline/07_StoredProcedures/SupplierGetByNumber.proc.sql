﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierGetByNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierGetByNumber'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierGetByNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierGetByNumber'
GO
ALTER PROCEDURE [dbo].[SupplierGetByNumber]
	@Type		char(1),
	@Number		char(5)
AS
begin
	select 
		sm.SUPN				as 'Number', 
		sm.NAME				as 'Name',
		sm.alph				as 'Alpha', 
		sm.ODNO				as 'OrderDepot',
		sm.rdno				as 'ReturnDepot', 
		sm.DLPO				as 'DateLastOrdered',
		sd.OCSD				as 'DateOrderCloseStart',
		sd.OCED				as 'DateOrderCloseEnd', 
		sm.QCTL				as 'SoqNumber', 
		sm.QFLG				as 'SoqOrdered', 
		sm.SOQDate			as 'SoqDate',
		sm.PalletCheck		as 'PalletCheck',
		sm.delc				as 'IsDeleted',
		sd.TNET				as 'IsTradanet', 
		sd.BBCC				as 'BbcNumber', 
		sd.depo				as 'DepotNumber',		
		sd.NOTE				as 'DepotNotes',
		sd.PHO1				as 'PhoneNumber1',
		sd.ReviewDay0, 
		sd.ReviewDay1, 
		sd.ReviewDay2, 
		sd.ReviewDay3, 
		sd.ReviewDay4, 
		sd.ReviewDay5, 
		sd.ReviewDay6, 
		sd.VLED1			as 'LeadTime1', 
		sd.VLED2			as 'LeadTime2', 
		sd.VLED3			as 'LeadTime3', 
		sd.VLED4			as 'LeadTime4',
		sd.VLED5			as 'LeadTime5', 
		sd.VLED6			as 'LeadTime6',
		sd.VLED7			as 'LeadTime0', 
		sd.LEAD				as 'LeadTimeFixed', 
		sd.MCPT				as 'MinOrderType', 
		sd.MCPV				as 'MinOrderValue', 
		sd.MCPC				as 'MinOrderCartons', 
		sd.MCPW				as 'MinOrderWeight'
	from
		supmas sm
	inner join
		supdet sd			ON sd.SUPN = sm.SUPN 
	WHERE
		sm.supn		= @Number
	and	sd.[TYPE]	= @Type
	and sd.depo		= (select case @Type
							when 'O' then (select odno from supmas where supn = @Number)
							when 'R' then (select rdno from supmas where supn = @Number)
							when 'S' then 999
						end);
		
end
GO

