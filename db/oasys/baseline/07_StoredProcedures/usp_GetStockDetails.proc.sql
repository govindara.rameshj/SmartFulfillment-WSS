﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetStockDetails]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetStockDetails'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetStockDetails] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetStockDetails'
GO
ALTER PROCEDURE usp_GetStockDetails 
AS 
  BEGIN 
      SELECT skun            skunumber, 
             ( onha - wqty ) AS quantity 
      FROM   stkmas 
  END
GO

