﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetStockAdjustmentDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetStockAdjustmentDate'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetStockAdjustmentDate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetStockAdjustmentDate'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 16/06/2011
-- Description:	Return the Stores Open date
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetStockAdjustmentDate] 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		TMDT as 'TommorowsDate'
	FROM
		SYSDAT 
	WHERE
		FKEY = '01'		
END
GO

