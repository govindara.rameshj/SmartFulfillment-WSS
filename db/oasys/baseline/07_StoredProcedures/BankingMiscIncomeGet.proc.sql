﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BankingMiscIncomeGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure BankingMiscIncomeGet'
	EXEC ('CREATE PROCEDURE [dbo].[BankingMiscIncomeGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure BankingMiscIncomeGet'
GO
ALTER PROCedure [dbo].[BankingMiscIncomeGet]
    @DateStart date=null, @DateEnd date=null
as
begin

select
    dt.CASH          CashierId,
    coalesce(su.Name, 'Unknown')    CashierName,    
    case dt.TCOD
        when 'M+' then 'In'
        when 'M-' then 'Out'
        when 'C+' then 'Correction In'
        when 'C-' then 'Correction Out'
    end              'Type',
    dt.DATE1         'Date', 
    dt.TILL          TillId,
    dt.[TRAN]        TranNumber,    
    dt.OVCTranNumber OVCTranNumber,
    sc.Code          AccountCode,
    sc.Name          AccountName,
    dt.TOTL          Value,
    dt.DOCN          DocumentNumber 
from 
    vwDLTOTS dt
left join
    SystemUsers su on su.ID=CASH
inner join
    SystemCodes sc on sc.Id=dt.MISC and sc.[Type]=dt.TCOD
where 
    dt.TCOD in ('M+','M-', 'C-', 'C+') and
    dt.CASH<>'000' and
    dt.TMOD=0 and
    dt.VOID=0 and
    dt.PARK=0 and
    (@DateStart is null or (@DateStart is not null and dt.DATE1>=@DateStart)) and
    (@DateEnd is null or (@DateEnd is not null and dt.DATE1<=@DateEnd))
order by
    dt.CASH, dt.DATE1

end
GO

