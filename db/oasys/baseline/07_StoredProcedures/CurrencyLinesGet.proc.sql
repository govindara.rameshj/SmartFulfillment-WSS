﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CurrencyLinesGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure CurrencyLinesGet'
	EXEC ('CREATE PROCEDURE [dbo].[CurrencyLinesGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure CurrencyLinesGet'
GO
ALTER PROCedure [dbo].[CurrencyLinesGet]
	@CurrencyId		char(3)
as
begin
	select 
		cd.ID			as Id,
		cd.CurrencyID	as CurrencyId,
		cd.TenderID		as TenderId,
		cd.DisplayText,
		cd.BullionMultiple,
		cd.BankingBagLimit,
		cd.SafeMinimum	
	from
		SystemCurrencyDen cd
	where
		cd.CurrencyID=@CurrencyId
	order by
		cd.ID desc,
		cd.TenderID
end
GO

