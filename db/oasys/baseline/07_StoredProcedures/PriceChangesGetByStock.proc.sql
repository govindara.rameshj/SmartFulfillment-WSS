﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PriceChangesGetByStock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PriceChangesGetByStock'
	EXEC ('CREATE PROCEDURE [dbo].[PriceChangesGetByStock] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PriceChangesGetByStock'
GO
ALTER PROCEDURE [dbo].[PriceChangesGetByStock]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	select
		pc.PDAT									as 'DateEffective',
		pc.PRIC									as 'Price',
		(select case pc.PSTA
			when 'U' then 'Unapplied'
			when 'A' then 'Applied'
			when 'S' then 'System Applied'
		end)									as 'Status',
		pc.AUDT									as 'DateAuto',
		pc.AUAP									as 'DateApplied',
		pc.SHEL									as 'LabelShelfPrinted',
		pc.LABS 								as 'LabelPegPrinted',
		pc.LABM 								as 'LabelSmallPrinted',
		pc.LABL 								as 'LabelMediumPrinted',
		pc.EEID									as 'UserId',
		pc.MEID									as 'ManagerId'
		
	from
		PRCCHG pc
	where
		pc.SKUN=@SkuNumber
	order by
		pc.PDAT desc	

END
GO

