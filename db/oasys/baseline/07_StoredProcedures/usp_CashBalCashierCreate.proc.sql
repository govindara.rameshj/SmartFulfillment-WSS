﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CashBalCashierCreate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_CashBalCashierCreate'
	EXEC ('CREATE PROCEDURE [dbo].[usp_CashBalCashierCreate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_CashBalCashierCreate'
GO
ALTER PROCEDURE [dbo].[usp_CashBalCashierCreate]
@TransactionDate date,
@EndDate date=NULL 


AS
BEGIN

SET NOCOUNT OFF

create table #BankingTemp
(
   
    RID INT IDENTITY(1,1),
    [PeriodID] [int] NOT NULL,
	[CashierID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[Value] [decimal](9,2) NOT NULL,           
	[Discount] [decimal](9,2) NULL,       
	[Type] [char](9) NULL,
	--[Voids] INT,
	--[LinesReversed] INT,
	--[LinesScanned] INT,
	--[LinesSold] INT,
	[Misc] [decimal](9, 2)  NULL,
		
)

create table #CashBalCashTemp
(
    [PeriodID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[CashierID] [int] NOT NULL,
	[GrossSalesAmount] [decimal](8, 2) NOT NULL,
	[DiscountAmount] [decimal](8, 2) NOT NULL,
	[SalesCount] [decimal](4, 0) NOT NULL,
	[SalesAmount] [decimal](8, 2) NOT NULL,
	[RefundCount] [decimal](4, 0) NOT NULL,
	[RefundAmount] [decimal](8, 2) NOT NULL,
	[NumTransactions] [int] NOT NULL,
	--[NumLinesReversed] int NOT NULL,
	--[NumLinesScanned] int NOT NULL,
	--[NumLinesSold] INT NOT NULL
	
)

IF @EndDate IS NOT NULL
BEGIN

 SELECT 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	ISNULL(dt.TOTL,0) as Value, 
	ISNULL(dt.DISC,0) as Discount,
	dt.TCOD as Type,
	dt.MISC as MiscType
		
	into #TEMPTABLE1
		
FROM DLTOTS as dt 
 
 where dt.DATE1 BETWEEN @TransactionDate AND @EndDate and dt.VOID =0 and dt.TMOD = 0
 
 
 
  
SELECT 
    IDENTITY(INT, 1, 1)                    AS RID, 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	count(*) as NumLinesReversed
	
	
	into #TEMPTABLE2
		
FROM DLTOTS as dt 
 inner join DLLINE DL on  dt.[TRAN] = DL.[TRAN] and dt.DATE1 = DL.DATE1 and dt.TILL =DL.TILL	
 
 where dt.DATE1 BETWEEN @TransactionDate AND @EndDate
 AND DL.LREV = 1 and dl.QUAN >0
 and dt.VOID = 0 and dt.TMOD = 0
 
 group by dt.DATE1,dt.CASH
 
 
 
SELECT 
    IDENTITY(INT, 1, 1)                    AS RID, 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	count(*) as NumLinesScanned
		
	into #TEMPTABLE6
		
FROM DLTOTS as dt 
 inner join DLLINE DL on  dt.[TRAN] = DL.[TRAN] and dt.DATE1 = DL.DATE1 and dt.TILL =DL.TILL	
 
 where dt.DATE1 BETWEEN @TransactionDate AND @EndDate
 and dl.IBAR = 1 and dl.QUAN > 0 and dl.LREV = 0
  and dt.VOID = 0 and dt.TMOD = 0
group by dt.date1,dt.cash
 
  
 SELECT
    IDENTITY(INT, 1, 1)                    AS RID, 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	count(*) as NumLinesSold
	
	into #TEMPTABLE7
		
FROM DLTOTS as dt 
 inner join DLLINE DL on  dt.[TRAN] = DL.[TRAN] and dt.DATE1 = DL.DATE1 and dt.TILL =DL.TILL	
 
 where dt.DATE1 BETWEEN @TransactionDate AND @EndDate
 and dl.LREV = 0 and dl.QUAN > 0 
 and dt.VOID = 0 and dt.TMOD = 0
 and dl.SALT NOT IN ('V','D')
 group by dt.date1,dt.cash
 
 -- num of voids
 
 
 SELECT 
     IDENTITY(INT, 1, 1)                    AS RID, 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	COUNT(*) as NumberOfVoids
			
     into #TEMPTABLE10
		
FROM DLTOTS as dt 
 
 where dt.DATE1 BETWEEN @TransactionDate AND @EndDate
 and dt.VOID =1
 and dt.SSTA != 20
 and dt.PKRC = 0
 group by dt.DATE1,dt.CASH
 
 
 INSERT INTO #BankingTemp ([PeriodID],[CurrencyID],[CashierID] ,[Value],[Discount] ,[Type],Misc)
 
 select t1.PeriodID,t1.CurrencyID,t1.CashierID,ISNULL(t1.Value,0),t1.Discount,t1.[Type],
 t1.MiscType  
 from #TEMPTABLE1 t1
 
 
END
ELSE
BEGIN

SELECT 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	ISNULL(dt.TOTL,0) as Value, 
	ISNULL(dt.DISC,0) as Discount,
	dt.TCOD as Type,
	dt.MISC as MiscType
		
	into #TEMPTABLE3
		
FROM DLTOTS as dt 
 
 where dt.DATE1 = @TransactionDate and dt.VOID = 0 and dt.TMOD = 0
 
 
 
  
SELECT 
     IDENTITY(INT, 1, 1)                    AS RID, 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID,
	count(*) as LinesReversed
		
	into #TEMPTABLE4
		
FROM DLTOTS as dt 
 inner join DLLINE DL on  dt.[TRAN] = DL.[TRAN] and dt.DATE1 = DL.DATE1 and dt.TILL =DL.TILL	
 
 where dt.DATE1 = @TransactionDate 
 AND DL.LREV = 1 and dl.QUAN >0
 AND dt.VOID = 0 and dt.TMOD = 0
 group by dt.DATE1,dt.CASH
 
 
 
  
 
SELECT 
    IDENTITY(INT, 1, 1)                    AS RID, 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID,
	count(*) as NumLinesScanned
		
	into #TEMPTABLE8
		
FROM DLTOTS as dt 
 inner join DLLINE DL on  dt.[TRAN] = DL.[TRAN] and dt.DATE1 = DL.DATE1 and dt.TILL =DL.TILL	
 
 where dt.DATE1 = @TransactionDate
 and dl.IBAR = 1 and dl.QUAN >0 and dl.LREV = 0
 and dt.VOID = 0 and dt.TMOD = 0
group by dt.date1,dt.cash


 
  
 SELECT
    IDENTITY(INT, 1, 1)                    AS RID,  
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID,
	count(*) as NumLinesSold
	
	into #TEMPTABLE9
		
FROM DLTOTS as dt 
 inner join DLLINE DL on  dt.[TRAN] = DL.[TRAN] and dt.DATE1 = DL.DATE1 and dt.TILL =DL.TILL	
 
 where dt.DATE1 = @TransactionDate 
 and dl.LREV = 0 and dl.QUAN >0
 and dt.VOID = 0 and dt.TMOD = 0
 and dl.SALT NOT IN ('V','D')
 group by dt.date1,dt.cash
 
 
 
 -- num of voids
 
 
 SELECT 
     IDENTITY(INT, 1, 1)                    AS RID, 
    (select id from SystemPeriods where StartDate = dt.date1) as PeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.CASH as int) as CashierID, 
	COUNT(*) as NumberOfVoids
			
     into #TEMPTABLE11
		
FROM DLTOTS as dt 
 
 where dt.DATE1 = @TransactionDate 
 and dt.VOID =1
 and dt.SSTA != 20
 and dt.PKRC = 0
 group by dt.DATE1,dt.CASH
  
 
 
 INSERT INTO #BankingTemp ([PeriodID],[CurrencyID],[CashierID] ,[Value],[Discount] ,[Type],Misc )
 
 select t3.PeriodID,t3.CurrencyID,t3.CashierID,ISNULL(t3.Value,0),t3.Discount,t3.[Type],
 T3.MiscType 
 from #TEMPTABLE3 t3
  
END
 

 
DECLARE @RID_2 INT
DECLARE @MAXID_2 INT
SELECT @MAXID_2 = MAX(RID)FROM #BankingTemp -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_2 = 1
DECLARE @PERIODID_1 INT
DECLARE @CURRENCY_1 CHAR(5)
DECLARE @CASHIER_ID_1 INT

WHILE(@RID_2<=@MAXID_2)
		BEGIN
		SELECT @PERIODID_1 = PeriodID,@CURRENCY_1= CurrencyID,@CASHIER_ID_1= CashierID  FROM #BankingTemp WHERE RID = @RID_2
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_1 AND CurrencyID = @CURRENCY_1 AND CashierID=@CASHIER_ID_1)
		   DELETE FROM CashBalCashier WHERE PeriodID  = @PERIODID_1 AND CurrencyID = @CURRENCY_1 AND CashierID=@CASHIER_ID_1
		SET @RID_2 = @RID_2+1 -- increment loop count
		END
 
 
INSERT INTO #CashBalCashTemp
SELECT [PeriodID],[CurrencyID],[CashierID],[GrossSalesAmount],[DiscountAmount],[SalesCount],[SalesAmount],[RefundCount],[RefundAmount],[NumTransactions]  
 FROM CashBalCashier 

    DECLARE @RID_1 INT
	DECLARE @MAXID_1 INT
	SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID_1 = 1
	DECLARE @PERIODID INT
	DECLARE @CURRENCY CHAR(5)
	DECLARE @CASHIER_ID INT
	DECLARE @GrossAmount INT
	
 
-- looping the #temp_old from first record to last record.
	WHILE(@RID_1<=@MAXID_1)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @PERIODID = PeriodID,@CURRENCY= CurrencyID,@CASHIER_ID= CashierID  FROM #BankingTemp WHERE RID = @RID_1
			-- if the Primary key exists then update, by taking the values from the #temp_old  
            
			IF EXISTS(SELECT 1 FROM #CashBalCashTemp WHERE PeriodID  = @PERIODID AND CurrencyID = @CURRENCY AND CashierID=@CASHIER_ID)
				BEGIN
                    
                    UPDATE CashBalCashier  
					SET	
					    [DiscountAmount]  = T.Discount,
	                    [SalesCount]  = (Case  when T.[type]= 'SA'
	                                           then [SalesCount] + 1 else [SalesCount]
	                                     end),
	                    [SalesAmount]  = (Case  when T.[type]= 'SA'
	                                           then  [SalesAmount] + T.Value else [SalesAmount]
	                                      end),
	                    [RefundCount] = (Case  when T.[type]= 'RF'
	                                           then [RefundCount] + 1 else [RefundCount]
	                                      end),
	                    [RefundAmount]  = (Case  when T.[type]= 'RF'
	                                           then [RefundAmount] + T.Value else [RefundAmount]
	                                       end),
	                     
	                    [GrossSalesAmount] =  (Case  when  T.[type]IN('SA','RF','M+','M-','C+','C-')
	                                           then [GrossSalesAmount] + T.Value 
	                                           else [GrossSalesAmount]  end),
	                    
	                    [NumTransactions]= (Case when T.[Type] IN  ('CO','CC','OD') then [NumTransactions]
	                                         else [NumTransactions]+1 end),
	                    --[NumTransactions]+1,
	                    
	                    [NumOpenDrawer] = (Case  when  T.[type]= 'OD'
	                                           then [NumOpenDrawer] + 1 else [NumOpenDrawer]
	                                            end  ),
	                                            
	                                    
	                    	                                        	                                            
	                    [MiscIncomeCount01]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 1 
	                                          then [MiscIncomeCount01]+ 1
	                                           else [MiscIncomeCount01] end), 
	                                           
	                    [MiscIncomeCount02]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 2 
	                                          then [MiscIncomeCount02]+ 1
	                                           else [MiscIncomeCount02] end),
	                    [MiscIncomeCount03]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 3 
	                                          then [MiscIncomeCount03]+ 1
	                                           else [MiscIncomeCount03] end), 
	                    [MiscIncomeCount04]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 4 
	                                          then [MiscIncomeCount04]+ 1
	                                           else [MiscIncomeCount04] end), 
	                    [MiscIncomeCount05]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 5 
	                                          then [MiscIncomeCount05]+ 1
	                                           else [MiscIncomeCount05] end), 
	                    [MiscIncomeCount06]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 6 
	                                          then [MiscIncomeCount06]+ 1
	                                           else [MiscIncomeCount06] end), 
	                    [MiscIncomeCount07]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 7 
	                                          then [MiscIncomeCount07]+ 1
	                                           else [MiscIncomeCount07] end), 
	                    [MiscIncomeCount08]=(Case  when T.[type]IN('M+','C+') AND T.Misc  = 8 
	                                          then [MiscIncomeCount08]+ 1
	                                           else [MiscIncomeCount08] end), 
	                    [MiscIncomeCount09]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 9 
	                                          then [MiscIncomeCount09]+ 1
	                                           else [MiscIncomeCount09] end), 
	                    [MiscIncomeCount10]= (Case  when T.[type]IN('M+','C+') AND T.Misc  = 10 
	                                          then [MiscIncomeCount10]+ 1
	                                           else [MiscIncomeCount10] end), 
	                    [MiscIncomeCount11]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 11 
	                                          then [MiscIncomeCount11]+ 1
	                                           else [MiscIncomeCount11] end), 
	                    [MiscIncomeCount12]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 12 
	                                          then [MiscIncomeCount12]+ 1
	                                           else [MiscIncomeCount12] end), 
	                    [MiscIncomeCount13]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 13 
	                                          then [MiscIncomeCount13]+ 1
	                                           else [MiscIncomeCount13] end), 
	                    [MiscIncomeCount14]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 14 
	                                          then [MiscIncomeCount14]+ 1
	                                           else [MiscIncomeCount14] end), 
	                    [MiscIncomeCount15]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 15 
	                                          then [MiscIncomeCount15]+ 1
	                                           else [MiscIncomeCount15] end), 
	                    [MiscIncomeCount16]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 16 
	                                          then [MiscIncomeCount16]+ 1
	                                           else [MiscIncomeCount16] end), 
	                    [MiscIncomeCount17]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 17 
	                                          then [MiscIncomeCount17]+ 1
	                                           else [MiscIncomeCount17] end), 
	                    [MiscIncomeCount18]= (Case  when T.[type] IN('M+','C+') AND T.Misc  = 18 
	                                          then [MiscIncomeCount18]+ 1
	                                           else [MiscIncomeCount18] end), 
	                    [MiscIncomeCount19]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 19 
	                                          then [MiscIncomeCount19]+ 1
	                                           else [MiscIncomeCount19] end), 
	                    [MiscIncomeCount20]=(Case  when T.[type]IN('M+','C+') AND T.Misc  = 20 
	                                          then [MiscIncomeCount20]+ 1
	                                           else [MiscIncomeCount20] end), 
	                                           
	                    [MiscIncomeValue01]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 1 
	                                          then [MiscIncomeValue01]+ T.Value
	                                           else [MiscIncomeValue01] end), 
 	                    [MiscIncomeValue02]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 2 
	                                          then [MiscIncomeValue02]+ T.Value
	                                           else [MiscIncomeValue02] end),
						[MiscIncomeValue03]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 3 
	                                          then [MiscIncomeValue03]+ T.Value
	                                           else [MiscIncomeValue03] end),
	                    [MiscIncomeValue04]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 4 
	                                          then [MiscIncomeValue04]+ T.Value
	                                           else [MiscIncomeValue04] end),
	                    [MiscIncomeValue05]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 5 
	                                          then [MiscIncomeValue05]+ T.Value
	                                           else [MiscIncomeValue05] end),
 	                    [MiscIncomeValue06]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 6 
	                                          then [MiscIncomeValue06]+ T.Value
	                                           else [MiscIncomeValue06] end),
						[MiscIncomeValue07]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 7 
	                                          then [MiscIncomeValue07]+ T.Value
	                                           else [MiscIncomeValue07] end),
	                    [MiscIncomeValue08]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 8 
	                                          then [MiscIncomeValue08]+ T.Value
	                                           else [MiscIncomeValue08] end),
	                    [MiscIncomeValue09]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 9 
	                                          then [MiscIncomeValue09]+ T.Value
	                                           else [MiscIncomeValue09] end),
 	                    [MiscIncomeValue10]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 10 
	                                          then [MiscIncomeValue10]+ T.Value
	                                           else [MiscIncomeValue10] end),
						[MiscIncomeValue11]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 11 
	                                          then [MiscIncomeValue11]+ T.Value
	                                           else [MiscIncomeValue11] end),
	                    [MiscIncomeValue12]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 12 
	                                          then [MiscIncomeValue12]+ T.Value
	                                           else [MiscIncomeValue12] end),
	                    [MiscIncomeValue13]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 13 
	                                          then [MiscIncomeValue13]+ T.Value
	                                           else [MiscIncomeValue13] end),
 	                    [MiscIncomeValue14]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 14 
	                                          then [MiscIncomeValue14]+ T.Value
	                                           else [MiscIncomeValue14] end),
						[MiscIncomeValue15]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 15 
	                                          then [MiscIncomeValue15]+ T.Value
	                                           else [MiscIncomeValue15] end),
	                    [MiscIncomeValue16]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 16 
	                                          then [MiscIncomeValue16]+ T.Value
	                                           else [MiscIncomeValue16] end),
	                    [MiscIncomeValue17]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 17 
	                                          then [MiscIncomeValue17]+ T.Value
	                                           else [MiscIncomeValue17] end),
 	                    [MiscIncomeValue18]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 18 
	                                          then [MiscIncomeValue18]+ T.Value
	                                           else [MiscIncomeValue18] end),
						[MiscIncomeValue19]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 19 
	                                          then [MiscIncomeValue19]+ T.Value
	                                           else [MiscIncomeValue19] end),
	                    [MiscIncomeValue20]=(Case  when T.[type] IN('M+','C+') AND T.Misc  = 20 
	                                          then [MiscIncomeValue20]+ T.Value
	                                           else [MiscIncomeValue20] end),
	                                           
	                	[MisOutCount01]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 1 
	                                          then [MisOutCount01]+ 1
	                                           else [MisOutCount01] end), 
	                	[MisOutCount02]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 2 
	                                          then [MisOutCount02]+ 1
	                                           else [MisOutCount02] end), 
	                	[MisOutCount03]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 3 
	                                          then [MisOutCount03]+ 1
	                                           else [MisOutCount03] end), 
	                	[MisOutCount04]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 4 
	                                          then [MisOutCount04]+ 1
	                                           else [MisOutCount04] end), 
	                	[MisOutCount05]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 5 
	                                          then [MisOutCount05]+ 1
	                                           else [MisOutCount05] end), 
	                	[MisOutCount06]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 6 
	                                          then [MisOutCount06]+ 1
	                                           else [MisOutCount06] end), 
	                	[MisOutCount07]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 7 
	                                          then [MisOutCount07]+ 1
	                                           else [MisOutCount07] end), 
	                	[MisOutCount08]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 8 
	                                          then [MisOutCount08]+ 1
	                                           else [MisOutCount08] end), 
	                	[MisOutCount09]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 9 
	                                          then [MisOutCount09]+ 1
	                                           else [MisOutCount09] end), 
	                	[MisOutCount10]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 10 
	                                          then [MisOutCount10]+ 1
	                                           else [MisOutCount10] end), 
	                	[MisOutCount11]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 11 
	                                          then [MisOutCount11]+ 1
	                                           else [MisOutCount11] end), 
	                	[MisOutCount12]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 12 
	                                          then [MisOutCount12]+ 1
	                                           else [MisOutCount12] end), 
	                	[MisOutCount13]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 13 
	                                          then [MisOutCount13]+ 1
	                                           else [MisOutCount13] end), 
	                	[MisOutCount14]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 14 
	                                          then [MisOutCount14]+ 1
	                                           else [MisOutCount14] end), 
	                	[MisOutCount15]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 15 
	                                          then [MisOutCount15]+ 1
	                                           else [MisOutCount15] end), 
	                	[MisOutCount16]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 16 
	                                          then [MisOutCount16]+ 1
	                                           else [MisOutCount16] end), 
	                	[MisOutCount17]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 17 
	                                          then [MisOutCount17]+ 1
	                                           else [MisOutCount17] end), 
	                	[MisOutCount18]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 18 
	                                          then [MisOutCount18]+ 1
	                                           else [MisOutCount18] end), 
	                	[MisOutCount19]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 19 
	                                          then [MisOutCount19]+ 1
	                                           else [MisOutCount19] end), 
	                	[MisOutCount20]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 20 
	                                          then [MisOutCount20]+ 1
	                                           else [MisOutCount20] end), 
	                	[MisOutValue01]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 1 
	                                          then [MisOutValue01]+ T.Value
	                                           else [MisOutValue01] end), 
	                    [MisOutValue02]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 2 
	                                          then [MisOutValue02]+ T.Value
	                                           else [MisOutValue02] end), 
	                    [MisOutValue03]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 3 
	                                          then [MisOutValue03]+ T.Value
	                                           else [MisOutValue03] end), 
	                    [MisOutValue04]=(Case  when T.[type] IN('M-','C-') AND T.Misc  = 4 
	                                          then [MisOutValue04]+ T.Value
	                                           else [MisOutValue04] end), 
	                    [MisOutValue05]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 5
	                                          then [MisOutValue05]+ T.Value
	                                           else [MisOutValue05] end), 
	                    [MisOutValue06]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 6 
	                                          then [MisOutValue06]+ T.Value
	                                           else [MisOutValue06] end), 
	                    [MisOutValue07]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 7 
	                                          then [MisOutValue07]+ T.Value
	                                           else [MisOutValue07] end), 
	                    [MisOutValue08]=(Case  when T.[type] IN('M-','C-') AND T.Misc  = 8 
	                                          then [MisOutValue08]+ T.Value
	                                           else [MisOutValue08] end), 
	                    [MisOutValue09]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 9 
	                                          then [MisOutValue09]+ T.Value
	                                           else [MisOutValue09] end), 
	                    [MisOutValue10]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 10 
	                                          then [MisOutValue10]+ T.Value
	                                           else [MisOutValue10] end), 
	                    [MisOutValue11]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 11 
	                                          then [MisOutValue11]+ T.Value
	                                           else [MisOutValue11] end), 
	                    [MisOutValue12]=(Case  when T.[type] IN('M-','C-') AND T.Misc  = 12 
	                                          then [MisOutValue12]+ T.Value
	                                           else [MisOutValue12] end), 
	                    [MisOutValue13]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 13 
	                                          then [MisOutValue13]+ T.Value
	                                           else [MisOutValue13] end), 
	                    [MisOutValue14]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 14 
	                                          then [MisOutValue14]+ T.Value
	                                           else [MisOutValue14] end), 
	                    [MisOutValue15]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 15 
	                                          then [MisOutValue15]+ T.Value
	                                           else [MisOutValue15] end), 
	                    [MisOutValue16]=(Case  when T.[type] IN('M-','C-') AND T.Misc  = 16 
	                                          then [MisOutValue16]+ T.Value
	                                           else [MisOutValue16] end), 
	                    [MisOutValue17]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 17 
	                                          then [MisOutValue17]+ T.Value
	                                           else [MisOutValue17] end), 
	                    [MisOutValue18]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 18 
	                                          then [MisOutValue18]+ T.Value
	                                           else [MisOutValue18] end), 
	                    [MisOutValue19]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 19 
	                                          then [MisOutValue19]+ T.Value
	                                           else [MisOutValue19] end), 
	                    [MisOutValue20]= (Case  when T.[type] IN('M-','C-') AND T.Misc  = 20 
	                                          then [MisOutValue20]+ T.Value
	                                           else [MisOutValue20] end)                                          
	                    
	                     
					    
					    					    					    				    					    										
					FROM #BankingTemp AS T WHERE (CashBalCashier.PeriodID = @PERIODID AND T.PeriodID = @PERIODID) AND (CashBalCashier.CurrencyID = @CURRENCY and  T.CurrencyID =@CURRENCY ) AND (CashBalCashier.CashierID = @CASHIER_ID and  T.CashierID = @CASHIER_ID )
					
				END
			ELSE -- ELSE INSERT NEW RECORD
				BEGIN
					INSERT INTO CashBalCashier (PeriodID,CurrencyID,CashierID,[GrossSalesAmount],
					[DiscountAmount],[SalesCount],[SalesAmount],[RefundCount]
					,[RefundAmount],[NumTransactions],[NumOpenDrawer], 
					[MiscIncomeCount01],[MiscIncomeCount02],[MiscIncomeCount03],[MiscIncomeCount04],[MiscIncomeCount05],
					[MiscIncomeCount06],[MiscIncomeCount07],[MiscIncomeCount08],[MiscIncomeCount09],[MiscIncomeCount10],
					[MiscIncomeCount11],[MiscIncomeCount12],[MiscIncomeCount13],[MiscIncomeCount14],[MiscIncomeCount15],[MiscIncomeCount16],[MiscIncomeCount17],
					[MiscIncomeCount18],[MiscIncomeCount19],[MiscIncomeCount20],[MiscIncomeValue01],[MiscIncomeValue02],[MiscIncomeValue03],
					[MiscIncomeValue04],[MiscIncomeValue05],[MiscIncomeValue06],[MiscIncomeValue07],[MiscIncomeValue08],[MiscIncomeValue09],[MiscIncomeValue10],
					[MiscIncomeValue11],[MiscIncomeValue12],[MiscIncomeValue13],[MiscIncomeValue14],[MiscIncomeValue15],[MiscIncomeValue16],[MiscIncomeValue17],[MiscIncomeValue18],
					[MiscIncomeValue19],[MiscIncomeValue20],[MisOutCount01],[MisOutCount02],[MisOutCount03],[MisOutCount04],[MisOutCount05],
					[MisOutCount06],[MisOutCount07],[MisOutCount08],[MisOutCount09],[MisOutCount10],[MisOutCount11],[MisOutCount12],[MisOutCount13],[MisOutCount14],
					[MisOutCount15],[MisOutCount16],[MisOutCount17],[MisOutCount18],[MisOutCount19],[MisOutCount20],[MisOutValue01],[MisOutValue02],[MisOutValue03],
					[MisOutValue04],[MisOutValue05],[MisOutValue06],[MisOutValue07],[MisOutValue08],[MisOutValue09],[MisOutValue10],[MisOutValue11],[MisOutValue12],[MisOutValue13],
					[MisOutValue14],[MisOutValue15],[MisOutValue16],[MisOutValue17],[MisOutValue18],[MisOutValue19],[MisOutValue20])
					
					SELECT T.PeriodID,T.CurrencyID,T.CashierID,T.Value,T.Discount,(Case  when T.[type]= 'SA'then 1 else 0 end),
					(Case  when T.[type]= 'SA'then T.Value else 0  end),(Case  when T.[type]= 'RF'then 1 else 0 end),
					(Case  when T.[type]= 'RF'then T.Value else 0  end),
					(Case when T.[Type] IN  ('CO','CC','OD') then 0 else 1 end),
					(Case  when T.[type]= 'OD'then 1 else 0  end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 1 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 2 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 3 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 4 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 5 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 6 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 7 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 8 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 9 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 10 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 11 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 12 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 13 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 14 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 15 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 16 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 17 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 18 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 19 then 1 else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 20 then 1 else 0 end), 
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 1 then T.Value else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 2 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 3 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 4 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 5 then T.Value else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 6 then T.Value else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 7 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 8 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 9 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 10 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 11 then T.Value else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 12 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 13 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 14 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 15 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 16 then T.Value else 0 end),
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 17 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 18 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 19 then T.Value else 0 end), 	                					
					 (Case  when T.[type] IN('M+','C+') AND T.Misc  = 20 then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 1  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 2  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 3  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 4  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 5  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 6  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 7  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 8  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 9  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 10  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 11  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 12  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 13  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 14  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 15  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 16  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 17  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 18  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 19  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 20  then 1 else 0 end),
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 1   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 2   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 3   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 4   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 5   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 6   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 7   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 8   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 9   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 10   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 11   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 12   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 13   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 14   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 15   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 16   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 17   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 18   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 19   then T.Value else 0 end), 
					 (Case  when T.[type] IN('M-','C-') AND T.Misc  = 20   then T.Value else 0 end) 					  	                					 	                					
					  	                					
					FROM #BankingTemp   AS T 
					WHERE T.RID = @RID_1
					
				SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp
				END
             delete #BankingTemp where RID = @RID_1 
             delete #CashBalCashTemp  
            INSERT INTO #CashBalCashTemp
            SELECT [PeriodID],[CurrencyID],[CashierID],[GrossSalesAmount],[DiscountAmount],[SalesCount],[SalesAmount],[RefundCount],[RefundAmount],[NumTransactions]
            FROM CashBalCashier              
               
			SET @RID_1 = @RID_1+1 -- increment loop count
		END
		

 
 IF @EndDate IS NOT NULL
 BEGIN
 
DECLARE @RID_3 INT
DECLARE @MAXID_3 INT
SELECT @MAXID_3 = MAX(RID)FROM #TEMPTABLE10  -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_3 = 1
DECLARE @PERIODID_2 INT
DECLARE @CURRENCY_2 CHAR(5)
DECLARE @CASHIER_ID_2 INT
DECLARE @NUMVOIDS INT



WHILE(@RID_3<=@MAXID_3)
		BEGIN
		SELECT @PERIODID_2 = PeriodID,@CURRENCY_2= CurrencyID,@CASHIER_ID_2= CashierID,@NUMVOIDS = NumberOfVoids   FROM #TEMPTABLE10 WHERE RID = @RID_3
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_2 AND CurrencyID = @CURRENCY_2 AND CashierID=@CASHIER_ID_2)
		   UPDATE CashBalCashier 
		   SET NumVoids  = NumVoids + @NUMVOIDS
		   WHERE PeriodID  = @PERIODID_2 AND CurrencyID = @CURRENCY_2 AND CashierID=@CASHIER_ID_2
		SET @RID_3 = @RID_3+1 -- increment loop count
		END
		
		

DECLARE @RID_4 INT
DECLARE @MAXID_4 INT
SELECT @MAXID_4 = MAX(RID)FROM #TEMPTABLE7  -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_4 = 1
DECLARE @PERIODID_3 INT
DECLARE @CURRENCY_3 CHAR(5)
DECLARE @CASHIER_ID_3 INT
DECLARE @NUMLINESOLD INT



WHILE(@RID_4<=@MAXID_4)
		BEGIN
		SELECT @PERIODID_3 = PeriodID,@CURRENCY_3= CurrencyID,@CASHIER_ID_3= CashierID,@NUMLINESOLD = NumLinesSold   FROM #TEMPTABLE7 WHERE RID = @RID_4
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_3 AND CurrencyID = @CURRENCY_3 AND CashierID=@CASHIER_ID_3)
		   UPDATE CashBalCashier 
		   SET NumLinesSold  = NumLinesSold + @NUMLINESOLD
		   WHERE PeriodID  = @PERIODID_3 AND CurrencyID = @CURRENCY_3 AND CashierID=@CASHIER_ID_3
		SET @RID_4 = @RID_4+1 -- increment loop count
		END
		
		
DECLARE @RID_6 INT
DECLARE @MAXID_6 INT
SELECT @MAXID_6 = MAX(RID)FROM #TEMPTABLE2  -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_6 = 1
DECLARE @PERIODID_6 INT
DECLARE @CURRENCY_6 CHAR(5)
DECLARE @CASHIER_ID_6 INT
DECLARE @NUMLINEREVERSED INT



WHILE(@RID_6<=@MAXID_6)
		BEGIN
		SELECT @PERIODID_6 = PeriodID,@CURRENCY_6= CurrencyID,@CASHIER_ID_6= CashierID,@NUMLINEREVERSED = NumLinesReversed   FROM #TEMPTABLE2 WHERE RID = @RID_6
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_6 AND CurrencyID = @CURRENCY_6 AND CashierID=@CASHIER_ID_6)
		   UPDATE CashBalCashier 
		   SET NumLinesReversed   = NumLinesReversed + @NUMLINEREVERSED
		   WHERE PeriodID  = @PERIODID_6 AND CurrencyID = @CURRENCY_6 AND CashierID=@CASHIER_ID_6
		SET @RID_6 = @RID_6+1 -- increment loop count
		END
		
DECLARE @RID_7 INT
DECLARE @MAXID_7 INT
SELECT @MAXID_7 = MAX(RID)FROM #TEMPTABLE6  -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_7 = 1
DECLARE @PERIODID_7 INT
DECLARE @CURRENCY_7 CHAR(5)
DECLARE @CASHIER_ID_7 INT
DECLARE @NUMLINESSCANNED INT



WHILE(@RID_7<=@MAXID_7)
		BEGIN
		SELECT @PERIODID_7 = PeriodID,@CURRENCY_7= CurrencyID,@CASHIER_ID_7= CashierID,@NUMLINESSCANNED = NumLinesScanned   FROM #TEMPTABLE6 WHERE RID = @RID_7
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_7 AND CurrencyID = @CURRENCY_7 AND CashierID=@CASHIER_ID_7)
		   UPDATE CashBalCashier 
		   SET NumLinesScanned   = NumLinesScanned + @NUMLINESSCANNED
		   WHERE PeriodID  = @PERIODID_7 AND CurrencyID = @CURRENCY_7 AND CashierID=@CASHIER_ID_7
		SET @RID_7 = @RID_7+1 -- increment loop count
		END 
 

END


ELSE
BEGIN
 
DECLARE @RID_5 INT
DECLARE @MAXID_5 INT
SELECT @MAXID_5 = MAX(RID)FROM #TEMPTABLE11  -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_5 = 1
DECLARE @PERIODID_4 INT
DECLARE @CURRENCY_4 CHAR(5)
DECLARE @CASHIER_ID_4 INT
DECLARE @NUMVOIDS1 INT



WHILE(@RID_5<=@MAXID_5)
		BEGIN
		SELECT @PERIODID_4 = PeriodID,@CURRENCY_4= CurrencyID,@CASHIER_ID_4= CashierID,@NUMVOIDS1 = NumberOfVoids    FROM #TEMPTABLE11 WHERE RID = @RID_5
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_4 AND CurrencyID = @CURRENCY_4 AND CashierID=@CASHIER_ID_4)
		   UPDATE CashBalCashier 
		   SET NumVoids   = NumVoids + @NUMVOIDS1
		   WHERE PeriodID  = @PERIODID_4 AND CurrencyID = @CURRENCY_4 AND CashierID=@CASHIER_ID_4
		SET @RID_5 = @RID_5+1 -- increment loop count
		END
		
		

DECLARE @RID_8 INT
DECLARE @MAXID_8 INT
SELECT @MAXID_8 = MAX(RID)FROM #TEMPTABLE9  -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_8 = 1
DECLARE @PERIODID_8 INT
DECLARE @CURRENCY_8 CHAR(5)
DECLARE @CASHIER_ID_8 INT
DECLARE @NUMLINESOLD1 INT



WHILE(@RID_8<=@MAXID_8)
		BEGIN
		SELECT @PERIODID_8 = PeriodID,@CURRENCY_8= CurrencyID,@CASHIER_ID_8= CashierID,@NUMLINESOLD1 = NumLinesSold   FROM #TEMPTABLE9 WHERE RID = @RID_8
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_8 AND CurrencyID = @CURRENCY_8 AND CashierID=@CASHIER_ID_8)
		   UPDATE CashBalCashier 
		   SET NumLinesSold  = NumLinesSold + @NUMLINESOLD1
		   WHERE PeriodID  = @PERIODID_8 AND CurrencyID = @CURRENCY_8 AND CashierID=@CASHIER_ID_8
		SET @RID_8 = @RID_8+1 -- increment loop count
		END
		
		
DECLARE @RID_9 INT
DECLARE @MAXID_9 INT
SELECT @MAXID_9 = MAX(RID)FROM #TEMPTABLE4  -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_9 = 1
DECLARE @PERIODID_9 INT
DECLARE @CURRENCY_9 CHAR(5)
DECLARE @CASHIER_ID_9 INT
DECLARE @NUMLINEREVERSED1 INT



WHILE(@RID_9<=@MAXID_9)
		BEGIN
		SELECT @PERIODID_9 = PeriodID,@CURRENCY_9= CurrencyID,@CASHIER_ID_9= CashierID,@NUMLINEREVERSED1 =  LinesReversed  FROM #TEMPTABLE4 WHERE RID = @RID_9
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_9 AND CurrencyID = @CURRENCY_9 AND CashierID=@CASHIER_ID_9)
		   UPDATE CashBalCashier 
		   SET NumLinesReversed   = NumLinesReversed + @NUMLINEREVERSED1
		   WHERE PeriodID  = @PERIODID_9 AND CurrencyID = @CURRENCY_9 AND CashierID=@CASHIER_ID_9
		SET @RID_9 = @RID_9+1 -- increment loop count
		END
		
DECLARE @RID_10 INT
DECLARE @MAXID_10 INT
SELECT @MAXID_10 = MAX(RID)FROM #TEMPTABLE8  -- SELECTING THE MAX RID FROM TEMP TABLE
SET @RID_10 = 1
DECLARE @PERIODID_10 INT
DECLARE @CURRENCY_10 CHAR(5)
DECLARE @CASHIER_ID_10 INT
DECLARE @NUMLINESSCANNED1 INT



WHILE(@RID_10<=@MAXID_10)
		BEGIN
		SELECT @PERIODID_10 = PeriodID,@CURRENCY_10= CurrencyID,@CASHIER_ID_10= CashierID,@NUMLINESSCANNED1 = NumLinesScanned   FROM #TEMPTABLE8 WHERE RID = @RID_10
		IF EXISTS(SELECT 1 FROM CashBalCashier WHERE PeriodID  = @PERIODID_10 AND CurrencyID = @CURRENCY_10 AND CashierID=@CASHIER_ID_10)
		   UPDATE CashBalCashier 
		   SET NumLinesScanned   = NumLinesScanned + @NUMLINESSCANNED1
		   WHERE PeriodID  = @PERIODID_10 AND CurrencyID = @CURRENCY_10 AND CashierID=@CASHIER_ID_10
		SET @RID_10 = @RID_10+1 -- increment loop count
		END 
 

		
END
END;
GO

