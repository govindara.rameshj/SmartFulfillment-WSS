﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeBagScannedInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeBagScannedInsert'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeBagScannedInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeBagScannedInsert'
GO
ALTER PROCedure NewBankingSafeBagScannedInsert

   @BusinessProcessID int,
   @SafePeriodID      int,
   @SafeBagsID        int,
   @SealNumber        char(20),
   @BagType           nchar(1),
   @Comment           nvarchar(max) = null

as
begin
   set nocount on
   
   if isnull(@SafeBagsID, 0) <> 0
   begin
      insert SafeBagScanned(BusinessProcessID, SafePeriodID, SafeBagsID, Comment) values(@BusinessProcessID, @SafePeriodID, @SafeBagsID, @Comment)
   end
   else
   begin
      insert SafeBagScanned(BusinessProcessID, SafePeriodID, SealNumber, BagType, Comment) values(@BusinessProcessID, @SafePeriodID, @SealNumber, @BagType, @Comment)
   end

end
GO

