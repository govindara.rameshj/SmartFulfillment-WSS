﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoGetOutstanding]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoGetOutstanding'
	EXEC ('CREATE PROCEDURE [dbo].[PoGetOutstanding] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoGetOutstanding'
GO
ALTER PROCEDURE [dbo].[PoGetOutstanding]
AS
BEGIN
	SET NOCOUNT ON;

	select		
		ph.tkey			as 'Id',
		ph.numb			as 'PoNumber',
		ph.pnum			as 'ConsignNumber',
		ph.soqn			as 'SoqNumber',
		ph.reln			as 'ReleaseNumber',
		ph.supp			as 'SupplierNumber',
		sm.name			as 'SupplierName',
		ph.bbcc			as 'SupplierBbc',
		ph.odat			as 'DateCreated',
		ph.ddat			as 'DateDue',
		ph.nocr			as 'Cartons',
		ph.valu			as 'Value',
		ph.qtyo			as 'Units',
		sm.PalletCheck
	from		
		purhdr ph
	inner join	
		supmas sm on sm.supn = ph.supp
	where
		ph.bbcc <> 'W'
		and	ph.bbcc <> 'A'
		and	ph.bbcc <> 'C'
		and	ph.rcom = 0
		and	ph.delm = 0
	order by
		ph.numb desc

END
GO

