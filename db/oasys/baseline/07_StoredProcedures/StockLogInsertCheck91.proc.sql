﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockLogInsertCheck91]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockLogInsertCheck91'
	EXEC ('CREATE PROCEDURE [dbo].[StockLogInsertCheck91] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockLogInsertCheck91'
GO
ALTER PROCEDURE [dbo].[StockLogInsertCheck91]
@SkuNumber CHAR (6), @StockStart INT, @UserId INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		@stockEnd INT,
		@markdownEnd INT,
		@writeoffEnd INT,
		@returnsEnd INT,
		@priceEnd DEC(9,2),
		@normalPrice DEC(9,2);
		
	--check that last log qty = last qty for sku
	SELECT TOP 1
		@stockEnd = ESTK,
		@markdownEnd = EMDN,
		@writeoffEnd = EWTF,
		@returnsEnd = ERET,
		@priceEnd = EPRI
	FROM 
		STKLOG WHERE SKUN=@SkuNumber 
	ORDER BY 
		TKEY DESC;
		
	SELECT TOP 1
		@normalPrice = PRIC
	FROM STKMAS 
	WHERE SKUN = @SkuNumber 
	
	--insert adjustment if needed
	IF (@stockEnd is not null AND @stockEnd <> @StockStart)
		OR (@stockEnd is null AND @StockStart <> 0)
	BEGIN
		INSERT INTO 
			STKLOG (
			SKUN,
			DAYN,
			[TYPE],
			DATE1,
			[TIME],
			KEYS,
			EEID,
			ICOM,
			SSTK,
			ESTK,
			SRET,
			ERET,
			SMDN,
			EMDN,
			SWTF,
			EWTF,
			SPRI,
			EPRI,
			RTI)
		VALUES (
			@SkuNumber,
			DATEDIFF(day, '1900-01-01', getdate()),
			'91',
			convert(date, GETDATE()),
			REPLACE(CONVERT(varchar, getdate(), 108), ':', ''),
			'System Adjustment 91',
			@UserId,
			0,
			ISNULL(@stockEnd, 0),
			@stockStart,
			ISNULL(@returnsEnd, 0),
			ISNULL(@returnsEnd, 0),
			ISNULL(@markdownEnd, 0),
			ISNULL(@markdownEnd, 0),
			ISNULL(@writeoffEnd, 0),
			ISNULL(@writeoffEnd, 0),
			ISNULL(@priceEnd, 0),
			ISNULL(@priceEnd, @normalPrice),
			'S'
		);
	END
	
END
GO

