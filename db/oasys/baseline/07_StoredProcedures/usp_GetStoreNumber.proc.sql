﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetStoreNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetStoreNumber'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetStoreNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetStoreNumber'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 28/06/2011
-- Description:	Gets Store Number from SYSOPT
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetStoreNumber] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		Stor
	From 
		SYSOPT
END
GO

