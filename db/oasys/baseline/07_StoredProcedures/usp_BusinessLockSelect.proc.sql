﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_BusinessLockSelect]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_BusinessLockSelect'
	EXEC ('CREATE PROCEDURE [dbo].[usp_BusinessLockSelect] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_BusinessLockSelect'
GO
ALTER PROCedure usp_BusinessLockSelect

as
begin
   set nocount on

   select ID, WorkStationID, AssemblyName, [Parameters], DateTimeStamp from BusinessLock

end
GO

