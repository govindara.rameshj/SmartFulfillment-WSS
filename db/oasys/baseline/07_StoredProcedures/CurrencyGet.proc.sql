﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CurrencyGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure CurrencyGet'
	EXEC ('CREATE PROCEDURE [dbo].[CurrencyGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure CurrencyGet'
GO
ALTER PROCedure [dbo].[CurrencyGet]
	@CurrencyId		char(3)=null
as
begin
	select 
		c.ID		as Id,
		c.Description,
		c.Symbol,
		c.IsDefault,
		c.DisplayOrder
	from
		SystemCurrency c
	where
		@CurrencyId is null or (@CurrencyId is not null and c.ID=@CurrencyId)
	order by
		c.DisplayOrder
		
end
GO

