﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingFloatDisplayAndCheck]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingFloatDisplayAndCheck'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingFloatDisplayAndCheck] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingFloatDisplayAndCheck'
GO
ALTER PROCedure NewBankingFloatDisplayAndCheck
   @FloatBagID int
as
begin
set nocount on
--display cash tenders only
select DenominationID   = a.ID,
       DenominationText = a.DisplayText,
       SystemFloatValue = b.Value
from (select *
      from SystemCurrencyDen
      where CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
      and   TenderID   = 1) a
left outer join (select *
                 from SafeBagsDenoms
                 where BagID      = @FloatBagID
                 and   CurrencyID = (select ID from SystemCurrency where IsDefault = 1)
                 and   TenderID   = 1) b
           on b.ID = a.ID
end
GO

