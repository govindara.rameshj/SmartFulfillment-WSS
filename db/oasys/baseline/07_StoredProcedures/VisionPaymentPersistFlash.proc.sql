﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionPaymentPersistFlash]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure VisionPaymentPersistFlash'
	EXEC ('CREATE PROCEDURE [dbo].[VisionPaymentPersistFlash] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure VisionPaymentPersistFlash'
GO
ALTER PROCEDURE [dbo].[VisionPaymentPersistFlash]
	@TranDate		date,
	@TillId			int,
	@TranNumber		int,
	@Number			int,
	@TenderTypeId	int,
	@ValueTender	dec(9,2),
	@IsPivotal		bit
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @rowcount int;
	exec @rowcount = VisionPaymentPersist
						@TranDate,
						@TillId,
						@TranNumber,
						@Number,
						@TenderTypeId,
						@ValueTender,			
						@IsPivotal;	

	--if inserted then update flash totals
	if @rowcount>0
	begin
		exec FlashTotalTenderPersist @TenderTypeId, @ValueTender;
	end		

	return @rowcount;
END
GO

