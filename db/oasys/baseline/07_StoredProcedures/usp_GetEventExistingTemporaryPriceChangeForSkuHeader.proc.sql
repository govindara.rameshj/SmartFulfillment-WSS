﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetEventExistingTemporaryPriceChangeForSkuHeader]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetEventExistingTemporaryPriceChangeForSkuHeader'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetEventExistingTemporaryPriceChangeForSkuHeader] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetEventExistingTemporaryPriceChangeForSkuHeader'
GO
-- ===============================================================================
-- Author         : Partha Dutta
-- Date	          : 01/12/2011
-- Project        : PO14-03
-- TFS User Story : 2154
-- TFS Task ID    : 
-- Description    : RollOut
-- ===============================================================================


-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================

ALTER PROCedure usp_GetEventExistingTemporaryPriceChangeForSkuHeader

   @EventTypeID char(2),
   @SkuNumber   char(6),
   @StartDate   date,
   @EndDate     date,
   @Deleted     bit

as
begin
   set nocount on

   select a.NUMB, a.PRIO, a.DACT1, a.DACT2, a.DACT3, a.DACT4, a.DACT5, a.DACT6, a.DACT7, a.IDEL 
   from EVTHDR a
   inner join EVTMAS b
         on  b.NUMB = a.NUMB
         and b.PRIO = a.PRIO
         and b.IDEL = a.IDEL
   where b.[TYPE] = @EventTypeID
   and   b.KEY1   = @SkuNumber
   and   b.SDAT  <= @StartDate
   and   b.EDAT  >= @EndDate
   and   b.IDEL   = @Deleted

   order by a.NUMB, a.PRIO

end
GO

