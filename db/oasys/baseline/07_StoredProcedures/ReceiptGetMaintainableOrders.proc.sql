﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReceiptGetMaintainableOrders]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReceiptGetMaintainableOrders'
	EXEC ('CREATE PROCEDURE [dbo].[ReceiptGetMaintainableOrders] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReceiptGetMaintainableOrders'
GO
ALTER PROCEDURE [dbo].[ReceiptGetMaintainableOrders]
AS
BEGIN
	SET NOCOUNT ON;

select		ds.numb as 'Number',
			ds.tkey as 'PoId',
			ds.valu as 'Value',
			ds.date1 as 'DateReceipt',
			ds.[0pon] as 'PoNumber',
			ds.[0con] as 'PoConsignNumber',
			ds.[0sup] as 'PoSupplierNumber',
			ds.[0soq] as 'PoSoqNumber',
			ds.[0bbc] as 'PoSupplierBbc',
			sm.name as 'PoSupplierName',
			ds.[0dat] as 'PoOrderDate',
			ds.info as 'Comments',
			ds.[0dl1] as 'PoDeliveryNote1',
			ds.[0dl2] as 'PoDeliveryNote2',
			ds.[0dl3] as 'PoDeliveryNote3',
			ds.[0dl4] as 'PoDeliveryNote4',
			ds.[0dl5] as 'PoDeliveryNote5',
			ds.[0dl6] as 'PoDeliveryNote6',
			ds.[0dl7] as 'PoDeliveryNote7',
			ds.[0dl8] as 'PoDeliveryNote8',
			ds.[0dl9] as 'PoDeliveryNote9',
			ds.EmployeeId

from		drlsum ds
inner join	supmas sm on ds.[0sup] = sm.supn
inner join	supdet sd on ds.[0sup] = sd.supn and sm.odno = sd.depo and sd.[type] = 'O'

where		ds.[0bbc] = 0
and			ds.[type] = 0
and			ds.rti = 'N'
and			sd.bbcc <> 'W'
and			sd.bbcc <> 'A'

order by	ds.date1 desc

END
GO

