﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetPurLineOnOrderValue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetPurLineOnOrderValue'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetPurLineOnOrderValue] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetPurLineOnOrderValue'
GO
ALTER PROCEDURE usp_GetPurLineOnOrderValue
 @skuNumber AS varchar( 6 ) , 
 @poNumber AS varchar( 6 )
 
AS
BEGIN
    SELECT pl.QTYO
      FROM
           purlin pl INNER JOIN purhdr ph
           ON pl.HKEY
              = 
              ph.TKEY
          AND pl.HKEY
              = 
              ( 
                SELECT TKEY
                  FROM PURHDR
                  WHERE numb
                        = 
                        ( 
                SELECT SPON
                  FROM ISUHDR
                  WHERE numb = @poNumber
                        )
              )
          AND pl.SKUN
              = 
              @skuNumber;

END;
GO

