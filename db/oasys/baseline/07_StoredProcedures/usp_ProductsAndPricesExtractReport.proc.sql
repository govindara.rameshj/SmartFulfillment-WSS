﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ProductsAndPricesExtractReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ProductsAndPricesExtractReport'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ProductsAndPricesExtractReport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ProductsAndPricesExtractReport'
GO
ALTER PROCEDURE [dbo].[usp_ProductsAndPricesExtractReport] 
AS
BEGIN
	SELECT 
		StockMaster.SKUN AS 'SKU',
		StockMaster.DESCR AS 'Product Name',
		Suppliers.NAME AS 'Brand Name',
		StockMaster.STYL AS 'Style Code',
		HierarchyStyle.DESCR AS 'Style (Category) Description',
		StockMaster.SGRP AS 'Sub Group Code',
		HierarchySubGroup.DESCR AS 'Sub Group description',
		StockMaster.GRUP AS 'Group Code',
		HierarchyGroup.DESCR AS 'Group Description',
		StockMaster.CTGY AS 'Category Code',
		HierarchyCategory.DESCR AS 'Catagory Description',
		StockMaster.PRIC AS 'ActivePrice',
		StockMaster.ONHA AS 'On Hand Qty',
		'Tax Code' =
			CASE 
				WHEN StockMaster.VATC = 1 THEN 'a'
				WHEN StockMaster.VATC = 2 THEN 'b'
				WHEN StockMaster.VATC = 3 THEN 'c'
				WHEN StockMaster.VATC = 4 THEN 'd'
				WHEN StockMaster.VATC = 5 THEN 'e'
				WHEN StockMaster.VATC = 6 THEN 'f'
				WHEN StockMaster.VATC = 7 THEN 'g'
				WHEN StockMaster.VATC = 8 THEN 'h'
				WHEN StockMaster.VATC = 9 THEN 'i'
				ELSE ''
			END
	FROM STKMAS AS StockMaster
	JOIN (SELECT SUPN, NAME	FROM SUPMAS) AS Suppliers ON Suppliers.SUPN = StockMaster.SUPP
	JOIN (SELECT NUMB, GROU, SGRP, STYL, DESCR FROM HIESTY) AS HierarchyStyle ON HierarchyStyle.NUMB = StockMaster.CTGY AND HierarchyStyle.GROU = StockMaster.GRUP AND HierarchyStyle.SGRP = StockMaster.SGRP AND HierarchyStyle.STYL = StockMaster.STYL
	JOIN (SELECT NUMB, GROU, SGRP,DESCR FROM HIESGP) AS HierarchySubGroup ON HierarchySubGroup.NUMB = StockMaster.CTGY AND HierarchySubGroup.GROU = StockMaster.GRUP AND HierarchySubGroup.SGRP = StockMaster.SGRP
	JOIN (SELECT NUMB, GROU, DESCR FROM HIEGRP) AS HierarchyGroup ON HierarchyGroup.NUMB = StockMaster.CTGY AND HierarchyGroup.GROU = StockMaster.GRUP
	JOIN (SELECT NUMB, DESCR FROM HIECAT) AS HierarchyCategory ON HierarchyCategory.NUMB = StockMaster.CTGY
END
GO

