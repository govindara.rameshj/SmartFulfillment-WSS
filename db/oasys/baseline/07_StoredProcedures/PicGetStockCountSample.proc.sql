﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PicGetStockCountSample]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PicGetStockCountSample'
	EXEC ('CREATE PROCEDURE [dbo].[PicGetStockCountSample] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PicGetStockCountSample'
GO
-- =============================================
-- Author      : Partha Dutta
-- Create Date : 27/08/2010
-- Referral No : 178C
-- Notes       : Stock Admin -> Stock Admin reports -> PIC Count History
-- =============================================

--Not using default report engine, means either columns cannot be cannot centred via metadata or have no idea how to do it 

-- Version Control Information
-- =============================================
-- Author:  Kevan Madelin
-- Create date: 20/08/2010
-- Version: 3.0.0.0
-- Description: This procedure is used to get the PIC Sample Check information
---- =============================================

-- Version Control Information
-- =============================================
-- Author:  Partha
-- Create date: 21/09/2010
-- Version: 3.0.0.1
-- Description: Fixing a "divide by zero" error
---- =============================================

ALTER PROCEDURE [dbo].[PicGetStockCountSample]
    @Percentage INT = 5
AS
BEGIN
    SET NOCOUNT ON;

CREATE TABLE #TempHHTDET(
    [DATE1] [date] NOT NULL,
    [SKUN] [char](6) NOT NULL,
    [INON] [bit] NOT NULL,
    [ONHA] [decimal](7, 0) NULL,
    [SCNT1] [decimal](7, 0) NULL,
    [SCNT2] [decimal](7, 0) NULL,
    [SCNT3] [decimal](7, 0) NULL,
    [SCNT4] [decimal](7, 0) NULL,
    [SCNT5] [decimal](7, 0) NULL,
    [SCNT6] [decimal](7, 0) NULL,
    [SCNT7] [decimal](7, 0) NULL,
    [SCNT8] [decimal](7, 0) NULL,
    [SCNT9] [decimal](7, 0) NULL,
    [SCNT10] [decimal](7, 0) NULL,
    [TSCN] [decimal](7, 0) NULL,
    [WCNT1] [decimal](7, 0) NULL,
    [WCNT2] [decimal](7, 0) NULL,
    [WCNT3] [decimal](7, 0) NULL,
    [WCNT4] [decimal](7, 0) NULL,
    [WCNT5] [decimal](7, 0) NULL,
    [WCNT6] [decimal](7, 0) NULL,
    [WCNT7] [decimal](7, 0) NULL,
    [WCNT8] [decimal](7, 0) NULL,
    [WCNT9] [decimal](7, 0) NULL,
    [WCNT10] [decimal](7, 0) NULL,
    [TWCN] [decimal](7, 0) NULL,
    [TPRE] [decimal](7, 0) NULL,
    [TCNT] [decimal](7, 0) NULL,
    [ICNT] [bit] NOT NULL,
    [IADJ] [bit] NOT NULL,
    [MDNQ] [decimal](7, 0) NULL,
    [MDNC1] [decimal](7, 0) NULL,
    [MDNC2] [decimal](7, 0) NULL,
    [MDNC3] [decimal](7, 0) NULL,
    [TMDC] [decimal](7, 0) NULL,
    [LBOK] [bit] NOT NULL,
    [ORIG] [char](1) NULL,
    [StockSold] [int] NOT NULL)

CREATE TABLE #TempHHTDETHolding(
    [DATE1] [date] NOT NULL,
    [SKUN] [char](6) NOT NULL,
    [INON] [bit] NOT NULL,
    [ONHA] [decimal](7, 0) NULL,
    [SCNT1] [decimal](7, 0) NULL,
    [SCNT2] [decimal](7, 0) NULL,
    [SCNT3] [decimal](7, 0) NULL,
    [SCNT4] [decimal](7, 0) NULL,
    [SCNT5] [decimal](7, 0) NULL,
    [SCNT6] [decimal](7, 0) NULL,
    [SCNT7] [decimal](7, 0) NULL,
    [SCNT8] [decimal](7, 0) NULL,
    [SCNT9] [decimal](7, 0) NULL,
    [SCNT10] [decimal](7, 0) NULL,
    [TSCN] [decimal](7, 0) NULL,
    [WCNT1] [decimal](7, 0) NULL,
    [WCNT2] [decimal](7, 0) NULL,
    [WCNT3] [decimal](7, 0) NULL,
    [WCNT4] [decimal](7, 0) NULL,
    [WCNT5] [decimal](7, 0) NULL,
    [WCNT6] [decimal](7, 0) NULL,
    [WCNT7] [decimal](7, 0) NULL,
    [WCNT8] [decimal](7, 0) NULL,
    [WCNT9] [decimal](7, 0) NULL,
    [WCNT10] [decimal](7, 0) NULL,
    [TWCN] [decimal](7, 0) NULL,
    [TPRE] [decimal](7, 0) NULL,
    [TCNT] [decimal](7, 0) NULL,
    [ICNT] [bit] NOT NULL,
    [IADJ] [bit] NOT NULL,
    [MDNQ] [decimal](7, 0) NULL,
    [MDNC1] [decimal](7, 0) NULL,
    [MDNC2] [decimal](7, 0) NULL,
    [MDNC3] [decimal](7, 0) NULL,
    [TMDC] [decimal](7, 0) NULL,
    [LBOK] [bit] NOT NULL,
    [ORIG] [char](1) NULL,
    [StockSold] [int] NOT NULL)
    
CREATE TABLE #TempHHTDETResults(
    [DATE1] [date] NOT NULL,
    [SKUN] [char](6) NOT NULL,
    [INON] [bit] NOT NULL,
    [ONHA] [decimal](7, 0) NULL,
    [SCNT1] [decimal](7, 0) NULL,
    [SCNT2] [decimal](7, 0) NULL,
    [SCNT3] [decimal](7, 0) NULL,
    [SCNT4] [decimal](7, 0) NULL,
    [SCNT5] [decimal](7, 0) NULL,
    [SCNT6] [decimal](7, 0) NULL,
    [SCNT7] [decimal](7, 0) NULL,
    [SCNT8] [decimal](7, 0) NULL,
    [SCNT9] [decimal](7, 0) NULL,
    [SCNT10] [decimal](7, 0) NULL,
    [TSCN] [decimal](7, 0) NULL,
    [WCNT1] [decimal](7, 0) NULL,
    [WCNT2] [decimal](7, 0) NULL,
    [WCNT3] [decimal](7, 0) NULL,
    [WCNT4] [decimal](7, 0) NULL,
    [WCNT5] [decimal](7, 0) NULL,
    [WCNT6] [decimal](7, 0) NULL,
    [WCNT7] [decimal](7, 0) NULL,
    [WCNT8] [decimal](7, 0) NULL,
    [WCNT9] [decimal](7, 0) NULL,
    [WCNT10] [decimal](7, 0) NULL,
    [TWCN] [decimal](7, 0) NULL,
    [TPRE] [decimal](7, 0) NULL,
    [TCNT] [decimal](7, 0) NULL,
    [ICNT] [bit] NOT NULL,
    [IADJ] [bit] NOT NULL,
    [MDNQ] [decimal](7, 0) NULL,
    [MDNC1] [decimal](7, 0) NULL,
    [MDNC2] [decimal](7, 0) NULL,
    [MDNC3] [decimal](7, 0) NULL,
    [TMDC] [decimal](7, 0) NULL,
    [LBOK] [bit] NOT NULL,
    [ORIG] [char](1) NULL,
    [StockSold] [int] NOT NULL)

CREATE TABLE #TempSTKMAS(
    [SKUN] [char](6) NOT NULL,
    [PLUD] [char](20) NULL,
    [DESCR] [char](40) NULL,
    [FILL] [char](10) NULL,
    [DEPT] [char](2) NULL,
    [GROU] [char](3) NULL,
    [VATC] [char](1) NULL,
    [BUYU] [char](4) NULL,
    [SUPP] [char](5) NULL,
    [PROD] [char](10) NULL,
    [PACK] [int] NULL,
    [PRIC] [decimal](9, 2) NULL,
    [PPRI] [decimal](9, 2) NULL,
    [COST] [decimal](11, 4) NULL,
    [DSOL] [date] NULL,
    [DREC] [date] NULL,
    [DORD] [date] NULL,
    [DPRC] [date] NULL,
    [DSET] [date] NULL,
    [DOBS] [date] NULL,
    [DDEL] [date] NULL,
    [ONHA] [int] NULL,
    [ONOR] [int] NULL,
    [MINI] [int] NULL,
    [MAXI] [int] NULL,
    [SOLD] [decimal](7, 0) NULL,
    [ISTA] [char](1) NULL,
    [IRIS] [bit] NOT NULL,
    [IRIB] [smallint] NULL,
    [IMDN] [bit] NOT NULL,
    [ICAT] [bit] NOT NULL,
    [IOBS] [bit] NOT NULL,
    [IDEL] [bit] NOT NULL,
    [ILOC] [smallint] NULL,
    [IEAN] [smallint] NULL,
    [IPPC] [smallint] NULL,
    [ITAG] [bit] NOT NULL,
    [INON] [bit] NOT NULL,
    [SALV1] [decimal](9, 2) NULL,
    [SALV2] [decimal](9, 2) NULL,
    [SALV3] [decimal](9, 2) NULL,
    [SALV4] [decimal](9, 2) NULL,
    [SALV5] [decimal](9, 2) NULL,
    [SALV6] [decimal](9, 2) NULL,
    [SALV7] [decimal](9, 2) NULL,
    [SALU1] [int] NULL,
    [SALU2] [int] NULL,
    [SALU3] [int] NULL,
    [SALU4] [int] NULL,
    [SALU5] [int] NULL,
    [SALU6] [int] NULL,
    [SALU7] [int] NULL,
    [CPDO] [smallint] NULL,
    [CYDO] [smallint] NULL,
    [AWSF] [decimal](9, 2) NULL,
    [AW13] [decimal](9, 2) NULL,
    [DATS] [date] NULL,
    [UWEK] [smallint] NULL,
    [FLAG] [bit] NOT NULL,
    [CFLG] [bit] NOT NULL,
    [US001] [int] NULL,
    [US002] [int] NULL,
    [US003] [int] NULL,
    [US004] [int] NULL,
    [US005] [int] NULL,
    [US006] [int] NULL,
    [US007] [int] NULL,
    [US008] [int] NULL,
    [US009] [int] NULL,
    [US0010] [int] NULL,
    [US0011] [int] NULL,
    [US0012] [int] NULL,
    [US0013] [int] NULL,
    [US0014] [int] NULL,
    [DO001] [smallint] NULL,
    [DO002] [smallint] NULL,
    [DO003] [smallint] NULL,
    [DO004] [smallint] NULL,
    [DO005] [smallint] NULL,
    [DO006] [smallint] NULL,
    [DO007] [smallint] NULL,
    [DO008] [smallint] NULL,
    [DO009] [smallint] NULL,
    [DO0010] [smallint] NULL,
    [DO0011] [smallint] NULL,
    [DO0012] [smallint] NULL,
    [DO0013] [smallint] NULL,
    [DO0014] [smallint] NULL,
    [SQ04] [int] NULL,
    [SQ13] [int] NULL,
    [SQ01] [int] NULL,
    [SQOR] [bit] NOT NULL,
    [TREQ] [int] NULL,
    [TREV] [decimal](9, 2) NULL,
    [TACT] [bit] NOT NULL,
    [RETQ] [int] NULL,
    [RETV] [decimal](9, 2) NULL,
    [CHKD] [char](1) NULL,
    [LABN] [smallint] NULL,
    [LABS] [char](1) NULL,
    [STON] [char](6) NULL,
    [STOQ] [int] NULL,
    [SOD1] [char](1) NULL,
    [SOD2] [char](1) NULL,
    [SOD3] [char](1) NULL,
    [LABM] [smallint] NULL,
    [LABL] [smallint] NULL,
    [WGHT] [decimal](7, 2) NULL,
    [VOLU] [decimal](7, 2) NULL,
    [NOOR] [bit] NOT NULL,
    [SUP1] [char](5) NULL,
    [IODT] [date] NULL,
    [FODT] [date] NULL,
    [PMIN] [int] NULL,
    [PMSD] [date] NULL,
    [PMED] [date] NULL,
    [PMCP] [char](1) NULL,
    [SMAN] [int] NULL,
    [DFLC] [date] NULL,
    [IMCP] [bit] NOT NULL,
    [FOLT] [int] NULL,
    [IDEA] [int] NULL,
    [QADJ] [decimal](5, 2) NULL,
    [TAGF] [char](1) NULL,
    [ALPH] [char](20) NULL,
    [CTGY] [char](6) NOT NULL,
    [GRUP] [char](6) NOT NULL,
    [SGRP] [char](6) NOT NULL,
    [STYL] [char](6) NOT NULL,
    [EQUV] [char](40) NULL,
    [HFIL] [char](12) NULL,
    [REVT] [char](6) NULL,
    [RPRI] [char](2) NULL,
    [IWAR] [bit] NOT NULL,
    [IOFF] [smallint] NULL,
    [ISOL] [smallint] NULL,
    [QUAR] [char](1) NULL,
    [IPRD] [bit] NOT NULL,
    [EQPM] [decimal](11, 6) NULL,
    [EQPU] [char](7) NULL,
    [MDNQ] [int] NULL,
    [WTFQ] [int] NULL,
    [SALT] [char](1) NULL,
    [MODT] [char](1) NULL,
    [AAPC] [bit] NOT NULL,
    [IPSK] [bit] NOT NULL,
    [AADJ] [char](1) NULL,
    [SUP2] [char](5) NULL,
    [FRAG] [bit] NOT NULL,
    [TIMB] [smallint] NULL,
    [ELEC] [bit] NOT NULL,
    [SOFS] [int] NOT NULL,
    [WQTY] [int] NULL,
    [WRAT] [char](2) NULL,
    [WSEQ] [char](2) NULL,
    [BALI] [char](1) NULL,
    [PRFSKU] [char](6) NULL,
    [MSPR1] [decimal](9, 2) NOT NULL,
    [MSPR2] [decimal](9, 2) NOT NULL,
    [MSPR3] [decimal](9, 2) NOT NULL,
    [MSPR4] [decimal](9, 2) NOT NULL,
    [MSTQ1] [int] NOT NULL,
    [MSTQ2] [int] NOT NULL,
    [MSTQ3] [int] NOT NULL,
    [MSTQ4] [int] NOT NULL,
    [MREQ1] [int] NOT NULL,
    [MREQ2] [int] NOT NULL,
    [MREQ3] [int] NOT NULL,
    [MREQ4] [int] NOT NULL,
    [MREV1] [decimal](9, 2) NOT NULL,
    [MREV2] [decimal](9, 2) NOT NULL,
    [MREV3] [decimal](9, 2) NOT NULL,
    [MREV4] [decimal](9, 2) NOT NULL,
    [MADQ1] [int] NOT NULL,
    [MADQ2] [int] NOT NULL,
    [MADQ3] [int] NOT NULL,
    [MADQ4] [int] NOT NULL,
    [MADV1] [decimal](9, 2) NOT NULL,
    [MADV2] [decimal](9, 2) NOT NULL,
    [MADV3] [decimal](9, 2) NOT NULL,
    [MADV4] [decimal](9, 2) NOT NULL,
    [MPVV1] [decimal](9, 2) NOT NULL,
    [MPVV2] [decimal](9, 2) NOT NULL,
    [MPVV3] [decimal](9, 2) NOT NULL,
    [MPVV4] [decimal](9, 2) NOT NULL,
    [MIBQ1] [int] NOT NULL,
    [MIBQ2] [int] NOT NULL,
    [MIBQ3] [int] NOT NULL,
    [MIBQ4] [int] NOT NULL,
    [MIBV1] [decimal](9, 2) NOT NULL,
    [MIBV2] [decimal](9, 2) NOT NULL,
    [MIBV3] [decimal](9, 2) NOT NULL,
    [MIBV4] [decimal](9, 2) NOT NULL,
    [MRTQ1] [int] NOT NULL,
    [MRTQ2] [int] NOT NULL,
    [MRTQ3] [int] NOT NULL,
    [MRTQ4] [int] NOT NULL,
    [MRTV1] [decimal](9, 2) NOT NULL,
    [MRTV2] [decimal](9, 2) NOT NULL,
    [MRTV3] [decimal](9, 2) NOT NULL,
    [MRTV4] [decimal](9, 2) NOT NULL,
    [MCCV1] [decimal](9, 2) NOT NULL,
    [MCCV2] [decimal](9, 2) NOT NULL,
    [MCCV3] [decimal](9, 2) NOT NULL,
    [MCCV4] [decimal](9, 2) NOT NULL,
    [MDRV1] [decimal](9, 2) NOT NULL,
    [MDRV2] [decimal](9, 2) NOT NULL,
    [MDRV3] [decimal](9, 2) NOT NULL,
    [MDRV4] [decimal](9, 2) NOT NULL,
    [MBSQ1] [int] NOT NULL,
    [MBSQ2] [int] NOT NULL,
    [MBSQ3] [int] NOT NULL,
    [MBSQ4] [int] NOT NULL,
    [MBSV1] [decimal](9, 2) NOT NULL,
    [MBSV2] [decimal](9, 2) NOT NULL,
    [MBSV3] [decimal](9, 2) NOT NULL,
    [MBSV4] [decimal](9, 2) NOT NULL,
    [MSTP1] [int] NOT NULL,
    [MSTP2] [int] NOT NULL,
    [MSTP3] [int] NOT NULL,
    [MSTP4] [int] NOT NULL,
    [FIL11] [decimal](9, 2) NOT NULL,
    [FIL12] [decimal](9, 2) NOT NULL,
    [FIL13] [decimal](9, 2) NOT NULL,
    [FIL14] [decimal](9, 2) NOT NULL,
    [ToForecast] [bit] NOT NULL,
    [IsInitialised] [bit] NOT NULL,
    [DemandPattern] [char](10) NULL,
    [PeriodDemand] [decimal](9, 2) NOT NULL,
    [PeriodTrend] [decimal](7, 2) NOT NULL,
    [ErrorForecast] [decimal](7, 2) NOT NULL,
    [ErrorSmoothed] [decimal](7, 2) NOT NULL,
    [BufferConversion] [decimal](5, 2) NOT NULL,
    [BufferStock] [int] NOT NULL,
    [ServiceLevelOverride] [decimal](5, 2) NOT NULL,
    [ValueAnnualUsage] [char](2) NULL,
    [OrderLevel] [int] NOT NULL,
    [SaleWeightSeason] [int] NOT NULL,
    [SaleWeightBank] [int] NOT NULL,
    [SaleWeightPromo] [int] NOT NULL,
    [DateLastSoqInit] [date] NULL,
    [StockLossPerWeek] [decimal](11, 4) NOT NULL,
    [FlierPeriod1] [char](1) NULL,
    [FlierPeriod2] [char](1) NULL,
    [FlierPeriod3] [char](1) NULL,
    [FlierPeriod4] [char](1) NULL,
    [FlierPeriod5] [char](1) NULL,
    [FlierPeriod6] [char](1) NULL,
    [FlierPeriod7] [char](1) NULL,
    [FlierPeriod8] [char](1) NULL,
    [FlierPeriod9] [char](1) NULL,
    [FlierPeriod10] [char](1) NULL,
    [FlierPeriod11] [char](1) NULL,
    [FlierPeriod12] [char](1) NULL,
    [SalesBias0] [decimal](5, 3) NOT NULL,
    [SalesBias1] [decimal](5, 3) NOT NULL,
    [SalesBias2] [decimal](5, 3) NOT NULL,
    [SalesBias3] [decimal](5, 3) NOT NULL,
    [SalesBias4] [decimal](5, 3) NOT NULL,
    [SalesBias5] [decimal](5, 3) NOT NULL,
    [SalesBias6] [decimal](5, 3) NOT NULL,
    [DateLastSold] [date] NULL)
    
    Declare 
        @TotalRows as integer,
        @Index as integer,
        @BlockSize as integer;
        
    Insert into #TempHHTDET select * from HHTDET where DATE1 = convert(date, GETDATE());

    set     @Index = 0;
    select  @TotalRows = COUNT(skun) from #TempHHTDET;
        
    if @TotalRows > 0 
    begin
        set     @TotalRows = @TotalRows * @Percentage / 100;

        --if no of records less than 100 then a division by zero will occurr
        if @TotalRows = 0
           set @BlockSize = (select COUNT(skun) from #TempHHTDET)
        else
           set @BlockSize = (select COUNT(skun)/ @TotalRows from #TempHHTDET)

        --old code causing the "divide by zero" error
        --select    @BlockSize = COUNT(skun)/ @TotalRows from #TempHHTDET;



        while @Index <= @TotalRows
        begin
            insert into #TempHHTDETHolding
            select top (@BlockSize) * from #TempHHTDET

            insert into #TempHHTDETResults
            select top 1 * from #TempHHTDETHolding
            ORDER BY NEWID()
            
            delete from #TempHHTDET where SKUN in (select SKUN from #TempHHTDETHolding)
                                      and DATE1 in (select DATE1 from #TempHHTDETHolding)
            delete from #TempHHTDETHolding
            
            set @Index = @Index + 1
        end
    end 
    drop table #TempHHTDET
    drop table #TempHHTDETHolding

            
declare @SKUN varchar(10)

DECLARE Item_Cursor CURSOR for 
select Skun from #TempHHTDETResults

            
OPEN Item_Cursor
FETCH NEXT FROM Item_Cursor
into @SKUN

WHILE @@FETCH_STATUS = 0
BEGIN
    
    insert into #TempSTKMAS 
    select * from STKMAS where SKUN = @SKUN
    
    FETCH NEXT FROM Item_Cursor
    into @SKUN


 end
 
 CLOSE Item_Cursor
DEALLOCATE Item_Cursor


                                 
                             
select distinct
      THTD.SKUN as SKU,
      SM.DESCR      as Description,
      SM.PRIC       as Price,
      SM.ONHA       as QtyOnHand,
      ''            as 'SF Count',
      '' as 'WH Count',
      '' as 'P/S Count',
      '' as 'Variance',
      SM.MDNQ       as QtyMarkdown,
      ''            as MarkdownCount,
      'YES / NO'    as SELYesNo
from  #TempHHTDETResults THTD
join #TempSTKMAS  SM on THTD.SKUN = SM.SKUN 

drop table #TempHHTDETResults
drop table #TempSTKMAS  
    
END
GO

