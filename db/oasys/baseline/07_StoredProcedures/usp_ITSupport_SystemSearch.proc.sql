﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_SystemSearch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_SystemSearch'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_SystemSearch] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_SystemSearch'
GO
ALTER PROCEDURE dbo.usp_ITSupport_SystemSearch
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 21st May 2013
-- Description	: System Search Procedure
-----------------------------------------------------------------------------------
@Search Char(200),
@Columns Char(1) = '1',
@StoredProcedures Char(1) = '1',
@Rows Char(1) = '0'

As
Begin
Set Transaction Isolation Level Read Uncommitted
Begin Tran
Set DeadLock_Priority Low
Set NoCount On

-- Put SQL to enquire against database here
Declare @SearchLength Int = (Select LEN(@Search))
Declare @Debug int = '1'

-- Create Holding List of Tables
If Exists (Select Name From TempDB.Sys.Objects Where Name like ('#Temp_ITSearch_Tables_%') and [Type] = 'U') Drop Table #Temp_ITSearch_Tables				
Create Table #Temp_ITSearch_Tables	
	(
	Reference [INT] IDENTITY(1,1),
	[Table] Char(100),
	[Id] int
	)
If Exists (Select Name From TempDB.Sys.Objects Where Name like ('#Temp_ITSearch_Results_%') and [Type] = 'U') Drop Table #Temp_ITSearch_Results				
Create Table #Temp_ITSearch_Results	
	(
	Reference [INT] IDENTITY(1,1),
	[Search Type] Char(50),
	[Source] Char(50),
	[Data] Char(4000)
	)

If @StoredProcedures = '1'
	Begin
		Insert Into #Temp_ITSearch_Results
		Select 'Procedure', pro.name as 'Procedure', com.[text] as 'Contents' From SYS.procedures pro
		Inner Join SYS.syscomments com on com.id = pro.object_id  
		where text like ('%' + LTRIM(RTRIM(@Search)) + '%')
	End

If @Columns = '1'
	Begin
		Insert Into #Temp_ITSearch_Results
		Select 'Table', obj.Name as 'Table',col.name as 'Field' From SYS.columns col 
		Inner Join SYS.objects obj on obj.object_id = col.object_id
		where col.NAME like ('%' + LTRIM(RTRIM(@Search)) + '%')
	End
	
If @Rows = '1'
	Begin
		Declare @SearchCMD VarChar(2000)
		Insert Into #Temp_ITSearch_Tables
		Select Name, object_id From Sys.Objects Where Type = 'U'
		If @Debug = 1
			Begin
				Select * From #Temp_ITSearch_Tables
			End
		Declare @Temp_ProcTable_ID int
		Declare @Temp_ProcTable_MAXID int
		Set		@Temp_ProcTable_MAXID = (Select MAX(Reference)FROM #Temp_ITSearch_Tables)
		Set		@Temp_ProcTable_ID = 1
		Declare @Temp_ProcTable_Table Char(100)
		Declare @Temp_ProcTable_RefId Int
		While (@Temp_ProcTable_ID <= @Temp_ProcTable_MAXID)
			Begin	         
				Set @Temp_ProcTable_Table = (Select LTRIM(RTRIM([Table])) From #Temp_ITSearch_Tables Where Reference = @Temp_ProcTable_ID)
				Set @Temp_ProcTable_RefId = (Select [Id] From #Temp_ITSearch_Tables Where Reference = @Temp_ProcTable_ID)
				Print ('Searching Table : ' + LTRIM(RTRIM(Cast(@Temp_ProcTable_Table as Char))) + ' (' + LTRIM(RTRIM(Cast(@Temp_ProcTable_RefID as Char))) + ')')
				If Exists (Select Name From TempDB.Sys.Objects Where Name like ('#Temp_ITSearch_Columns_%') and [Type] = 'U') Drop Table #Temp_ITSearch_Columns				
				If @@ERROR > 0 Print ('Error Deleting Temporary Columns Table')
				Create Table #Temp_ITSearch_Columns	
						(
						Reference [INT] IDENTITY(1,1),
						[Column] Char(200)
						)
				Insert Into #Temp_ITSearch_Columns Select LTRIM(RTRIM(Name)) From Sys.Columns Where Object_Id = @Temp_ProcTable_RefId and System_Type_Id In (167,175,231,239) and Max_Length >= @SearchLength
				If @Debug = 1
					Begin
						Select * From #Temp_ITSearch_Columns
					End
    			Declare @Temp_ProcColumn_ID int
				Declare @Temp_ProcColumn_MAXID int
				Set		@Temp_ProcColumn_MAXID = (Select MAX(Reference) From #Temp_ITSearch_Tables)
				Set		@Temp_ProcColumn_ID = 1
				Declare @Temp_ProcColumn_Column Char(100)
				While (@Temp_ProcColumn_ID <= @Temp_ProcColumn_MAXID)
					Begin       
						Set @Temp_ProcColumn_Column = (Select LTRIM(RTRIM([Column])) From #Temp_ITSearch_Columns Where Reference = @Temp_ProcColumn_ID)
						Print ('Searching Column : [' + LTRIM(RTRIM(Cast(@Temp_ProcColumn_Column as Char))) + ']')
						If Exists (Select Name From TempDB.Sys.Objects Where Name like ('#Temp_ITSearchResult_%') and [Type] = 'U') Drop Table #Temp_ITSearchResult				
						Create Table #Temp_ITSearchResult ([RowCount] int);
						Set @SearchCMD = 'If Exists (Select * From ' + @Temp_ProcTable_Table + ' Where [' + @Temp_ProcColumn_Column + '] Like ''%' + @Search + '%'') Select ''1'' Else Select ''0''' --Print ''Rows''' + ', ' + @Temp_ProcTable_Table + ', ' + @Temp_ProcColumn_Column + ''''
    					Insert into #Temp_ITSearchResult Execute(@SearchCMD)
    					Declare @ReturnCount Int = (Select Top(1)[RowCount] from #Temp_ITSearchResult)
						If @ReturnCount = 1 
    						Begin
    							Insert Into #Temp_ITSearch_Results Values ('Rows', @Temp_ProcTable_Table, @Temp_ProcColumn_Column)
							End
						Set @Temp_ProcColumn_ID = @Temp_ProcColumn_ID+1 
						Print ('Finished Searching : [' + LTRIM(RTRIM(Cast(@Temp_ProcColumn_Column as Char))) + ']')
					End
				Set @Temp_ProcTable_ID = @Temp_ProcTable_ID+1 
			End
	End

Select * From #Temp_ITSearch_Results
Rollback Tran

End
GO

