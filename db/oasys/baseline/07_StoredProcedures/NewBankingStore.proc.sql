﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingStore]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingStore'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingStore] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingStore'
GO
ALTER PROCedure NewBankingStore
   @StoreID   char(3)  output,
   @StoreName char(30) output
   
as
begin
set nocount on

set @StoreID   = (select STOR from RETOPT)
set @StoreName = (select SNAM from RETOPT)
end
GO

