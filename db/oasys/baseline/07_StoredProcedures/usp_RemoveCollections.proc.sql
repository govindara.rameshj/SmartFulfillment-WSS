﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_RemoveCollections]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_RemoveCollections'
	EXEC ('CREATE PROCEDURE [dbo].[usp_RemoveCollections] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_RemoveCollections'
GO
ALTER PROCEDURE usp_RemoveCollections @CollectionId int
AS
BEGIN
    DELETE FROM VehicleCollectionHeader
      WHERE CollectionId
            = 
            @CollectionId;
END;
GO

