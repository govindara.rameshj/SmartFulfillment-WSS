﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HierarchyCategoryGetNumberNames]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure HierarchyCategoryGetNumberNames'
	EXEC ('CREATE PROCEDURE [dbo].[HierarchyCategoryGetNumberNames] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure HierarchyCategoryGetNumberNames'
GO
ALTER PROCEDURE [dbo].[HierarchyCategoryGetNumberNames]

AS
BEGIN
	declare @OutputTable TABLE ( Id char(6), Display varchar(150) )

	insert into @OutputTable values (null, '---All---');
	insert into @OutputTable select NUMB, NUMB + ' ' + DESCR from HIECAT;
	
	select * from @OutputTable
	
END
GO

