﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetXmlFileValue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetXmlFileValue'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetXmlFileValue] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetXmlFileValue'
GO
ALTER PROCEDURE usp_GetXmlFileValue 
@ParameterId CHAR(9) 
AS 
  BEGIN 
      SELECT stringvalue 
      FROM   parameters 
      WHERE  parameterid = @ParameterId 
  END
GO

