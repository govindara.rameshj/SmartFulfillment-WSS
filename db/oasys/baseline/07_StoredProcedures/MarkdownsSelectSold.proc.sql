﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MarkdownsSelectSold]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure MarkdownsSelectSold'
	EXEC ('CREATE PROCEDURE [dbo].[MarkdownsSelectSold] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure MarkdownsSelectSold'
GO
ALTER PROCedure [MarkdownsSelectSold]
@StartDate	date,
@EndDate	date
as

select		MarkdownStock.SkuNumber,
			STKMAS.DESCR as SkuDescription,
			HIECAT.NUMB as CategoryNumber,
			HIECAT.DESCR as CategoryDescription,
			HIEGRP.GROU as GroupNumber,
			HIEGRP.DESCR as GroupDescription,
			HIESGP.SGRP as SubgroupNumber,
			HIESGP.DESCR as SubgroupDescription,
			HIESTY.STYL as StyleNumber,
			HIESTY.DESCR as StyleDescription,
			MarkdownStock.DateCreated,
			MarkdownStock.Serial,
			MarkdownStock.ReasonCode,
			MarkdownStock.Price,
			MarkdownStock.WeekNumber,
			MarkdownStock.ReductionWeek1,
			MarkdownStock.ReductionWeek2,
			MarkdownStock.ReductionWeek3,
			MarkdownStock.ReductionWeek4,
			MarkdownStock.ReductionWeek5,
			MarkdownStock.ReductionWeek6,
			MarkdownStock.ReductionWeek7,
			MarkdownStock.ReductionWeek8,
			MarkdownStock.ReductionWeek9,
			MarkdownStock.ReductionWeek10,
			case MarkdownStock.WeekNumber
				when 1 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek1/100),2) 
				when 2 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek2/100),2)
				when 3 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek3/100),2)
				when 4 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek4/100),2)
				when 5 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek5/100),2)
				when 6 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek6/100),2)
				when 7 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek7/100),2)
				when 8 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek8/100),2)
				when 9 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek9/100),2)
				when 10 then round(MarkdownStock.Price - (MarkdownStock.Price * MarkdownStock.ReductionWeek10/100),2)
			end as WeekCurrent,
			MarkdownStock.SoldDate,
			MarkdownStock.SoldTill,
			MarkdownStock.SoldTransaction
			
from		MarkdownStock
inner join	STKMAS on MarkdownStock.SkuNumber=STKMAS.SKUN
inner join	HIECAT on STKMAS.CTGY = HIECAT.NUMB
inner join	HIEGRP on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU
inner join	HIESGP on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU and STKMAS.SGRP = HIESGP.SGRP 
inner join	HIESTY on STKMAS.CTGY = HIEGRP.NUMB and STKMAS.GRUP = HIEGRP.GROU and STKMAS.SGRP = HIESGP.SGRP and STKMAS.STYL = HIESTY.STYL

where		MarkdownStock.Solddate is not null
and			MarkdownStock.DateWrittenOff is null
and			MarkdownStock.IsReserved=0
and			MarkdownStock.DateCreated >= @StartDate
and			MarkdownStock.DateCreated <= @EndDate

order by	MarkdownStock.DateCreated, STKMAS.DESCR, MarkdownStock.Serial
GO

