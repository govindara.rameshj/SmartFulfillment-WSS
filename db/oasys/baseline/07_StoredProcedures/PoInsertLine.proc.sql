﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoInsertLine]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoInsertLine'
	EXEC ('CREATE PROCEDURE [dbo].[PoInsertLine] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoInsertLine'
GO
ALTER PROCEDURE [dbo].[PoInsertLine]
	@OrderId		int,
	@SkuNumber		char(6),
	@SkuProductCode	char(10),
	@OrderQty		int,
	@Price			dec(9,2),
	@Cost			dec(11,4)
AS
BEGIN
	SET NOCOUNT ON;

	--insert order line
	insert into		
		PURLIN(
		HKEY,
		SKUN,
		PROD,
		QTYO,
		PRIC,
		COST
	) values (
		@OrderId,
		@SkuNumber,
		@SkuProductCode,
		@OrderQty,
		@Price,
		@Cost
	);
	
		
	--update stock item as ordered
	update	
		stkmas 
	set		
		onor		= onor + @OrderQty,
		OrderLevel	= 0,
		DORD		= getdate(),
		tact		= 1
	where	
		skun = @SkuNumber;
	
END
GO

