﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetStockEans]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetStockEans'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetStockEans] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetStockEans'
GO
ALTER PROCEDURE [dbo].[StockGetStockEans]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	select 
		ea.numb		as 'Number',		
		ea.skun		as 'SkuNumber'
	from
		eanmas ea
	where
		ea.skun = @SkuNumber

END
GO

