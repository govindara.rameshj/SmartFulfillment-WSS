﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_InsertVehicle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_InsertVehicle'
	EXEC ('CREATE PROCEDURE [dbo].[usp_InsertVehicle] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_InsertVehicle'
GO
ALTER PROCEDURE [dbo].[usp_InsertVehicle]

(
	@Registration VARCHAR(50)='NO REGISTRATION', 
	@Make VARCHAR(50)='NO MAKE',
	@Model VARCHAR(50)='NO MODEL',
	@VehicleTypeID INT=-1,
	@DriverName VARCHAR(50)='NO DRIVER',
	@StartTime TIME='00:00:00',
	@StopTime TIME='00:00:00',
	@MaximumWeight DECIMAL(19,2)=0,
	@MaximumVolume DECIMAL(19,2)=0,
	@LastUpdateTime DATETIME)

AS

BEGIN
	DECLARE @DepotID INT


	/*All stores are numbered (as per "Store" table), but the format of this number (aka Depot ID) seems to differ throughout OASYS. 
	Route Optimisation simply reqires a 4 character store code, prefixed with an 8*/
	SET @DepotID = (SELECT TOP 1 '8'+ RIGHT('000'+ STOR, 3) FROM RETOPT WITH (NOLOCK))

	INSERT INTO dbo.Vehicle
			   ([DepotID]
			   ,[Registration]
			   ,[VehicleTypeID]
			   ,[StartTime]
			   ,[StopTime]
			   ,[MaximumWeight]
			   ,[MaximumVolume]
			   ,[LastUpdateTime])
	VALUES(@DepotID,
				ISNULL(@Registration,'NO REGISTRATION'), 
				ISNULL(@VehicleTypeID,-1), 
				CONVERT(VARCHAR(10), @StartTime, 108),
				CONVERT(VARCHAR(10), @StopTime, 108),
				ISNULL(@MaximumWeight,0) ,
				ISNULL(@MaximumVolume,0),
				@LastUpdateTime)
				
	SELECT SCOPE_IDENTITY()
END
GO

