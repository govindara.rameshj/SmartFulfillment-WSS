﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_RemoteStockCheck]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_RemoteStockCheck'
	EXEC ('CREATE PROCEDURE [dbo].[usp_RemoteStockCheck] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_RemoteStockCheck'
GO
-- ======================================================================
-- Author	: Kevan Madelin (RSC)
-- Create date	: 28th March 2011
-- Description	: Remote Access Stock Check Standard Version
-- ======================================================================
ALTER PROCEDURE [dbo].[usp_RemoteStockCheck] 
	-- Add the parameters for the stored procedure here
	@SKUN char(6) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	SKUN	as 'SKU', 
			DESCR	as 'Description',
			ONHA	as 'On Hand'
	FROM	STKMAS
	WHERE	SKUN = @SKUN
	
END
GO

