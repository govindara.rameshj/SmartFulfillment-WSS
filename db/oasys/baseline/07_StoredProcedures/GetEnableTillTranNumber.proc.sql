﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEnableTillTranNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GetEnableTillTranNumber'
	EXEC ('CREATE PROCEDURE [dbo].[GetEnableTillTranNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GetEnableTillTranNumber'
GO
ALTER PROCEDURE [dbo].[GetEnableTillTranNumber]
      @TillTranNumber char(6)='900000' out
AS
begin
	set nocount on
	
	declare @ID int

	insert into EnableTillTranNumber([DateInserted]) values (GETDATE())
	set @ID = @@IDENTITY
	
	set @TillTranNumber = CAST(@ID as varchar(10))

	-- check that Transaction part is not zero
	if RIGHT(@TillTranNumber, 4) = '0000'
		begin
			insert into EnableTillTranNumber([DateInserted]) values (GETDATE())
			set @ID = @@IDENTITY 

			set @TillTranNumber = CAST(@ID as varchar(10))
		end

	set nocount off
end
GO

