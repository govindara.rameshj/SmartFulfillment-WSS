﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierGetAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierGetAll'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierGetAll] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierGetAll'
GO
ALTER PROCEDURE [dbo].[SupplierGetAll]
AS
begin
	select 
		sm.SUPN				as 'Number', 
		sm.NAME				as 'Name',
		sm.alph				as 'Alpha', 
		sm.ODNO				as 'OrderDepot',
		sm.rdno				as 'ReturnDepot', 
		sm.DLPO				as 'DateLastOrdered', 
		sm.QCTL				as 'SoqNumber', 
		sm.QFLG				as 'SoqOrdered', 
		sm.SOQDate			as 'SoqDate',
		sm.PalletCheck		as 'PalletCheck',
		sm.delc				as 'IsDeleted'
	from
		supmas sm
	order by
		sm.supn 
		
end
GO

