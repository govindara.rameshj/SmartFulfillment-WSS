﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateVehicle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdateVehicle'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdateVehicle] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdateVehicle'
GO
ALTER PROCEDURE [dbo].[usp_UpdateVehicle] (
	@VehicleID INT,
	@Registration VARCHAR(50)='NO REGISTRATION',
	@VehicleTypeID INT=-1,
	@StartTime TIME='00:00:00',
	@StopTime TIME='00:00:00',
	@MaximumWeight VARCHAR(50)='0',
	@LastUpdateTime Datetime
	)
AS
BEGIN
	UPDATE Vehicle SET 
		[Registration] = @Registration,
		[VehicleTypeID] = @VehicleTypeID,
		[StartTime] = @StartTime,
		[StopTime] = @StopTime,
		[MaximumWeight] = @MaximumWeight,
		[LastUpdateTime]= @LastUpdateTime
	WHERE Vehicle.ID = @VehicleID

	RETURN @@ROWCOUNT
END
GO

