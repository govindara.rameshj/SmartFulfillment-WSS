﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_AdjustStockForPackSplit]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_AdjustStockForPackSplit'
	EXEC ('CREATE PROCEDURE [dbo].[usp_AdjustStockForPackSplit] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_AdjustStockForPackSplit'
GO
ALTER PROCedure [dbo].[usp_AdjustStockForPackSplit]
	@Skun As Int,
	@Quantity As Int,
	@Value As Decimal(9,2)
As
	Begin
	
		Set NoCount On
		
		Update
			STKMAS
		Set
			ONHA = ONHA + @Quantity,
			MBSQ1 = MBSQ1 + @Quantity,
			MBSV1 = MBSV1 + @Value
		Where
			SKUN = @Skun
End
GO

