﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SupplierGetOrderDetails]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SupplierGetOrderDetails'
	EXEC ('CREATE PROCEDURE [dbo].[SupplierGetOrderDetails] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SupplierGetOrderDetails'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 08/08/2012
-- User Story	 : 6131
-- Project		 : P022-009 - Stock Enquiry Details - amEnd the design of what is displayed.
-- Task Id		 : 6344
-- Description   : Move address fields from EnquirySupplier stored procedure to here.
-- =============================================
ALTER PROCedure [dbo].[SupplierGetOrderDetails]
	@SupplierNumber		Char(5)
As
Begin
	Set NOCOUNT On;

	Declare 
		@table Table 
			(
				Description VarChar(50),
				Display VarChar(50)
			);
	Declare
		@name			VarChar(30),
		@orderClose		Date,
		@orderOpen		Date,
		@delClose		Date,
		@delOpen		Date,
		@lastOrdered	Date,
		@lastReceived	Date,
		@lead1			Int,
		@lead2			Int,
		@lead3			Int,
		@lead4			Int,
		@lead5			Int,
		@lead6			Int,
		@lead7			Int,
		@review0		Bit,
		@review1		Bit,
		@review2		Bit,
		@review3		Bit,
		@review4		Bit,
		@review5		Bit,
		@review6		Bit,
		@reviewDays		VarChar(40),
		@mcpType		VarChar(1),
		@mcpValue		Int,
		@mcpCartons		Int,
		@mcpWeight		Int,
		@mcpTypeString	VarChar(40),
		@mcpValueWeight	VarChar(40),
		@warehouse		char(5),
		@address1		varchar(30),
		@address2		varchar(30),
		@address3		varchar(30),
		@address4		varchar(30),
		@address5		varchar(30),
		@postcode		varchar(8),
		@phone			varchar(15),
		@fax			varchar(15),
		@helpline		varchar(15),
		@contact		varchar(30),
		@type			varchar(10),
		@tradanet		char(3);

	Select
		@name			= sm.NAME,
		@orderClose		= sd.OCSD,
		@orderOpen		= sd.OCSD,
		@delClose		= sd.DCED,
		@delOpen		= sd.DCSD,
		@lastOrdered	= sm.DLPO,
		@lastReceived	= sm.DLRE,
		@lead1			= sd.VLED1,
		@lead2			= sd.VLED2,
		@lead3			= sd.VLED3,
		@lead4			= sd.VLED4,
		@lead5			= sd.VLED6,
		@lead6			= sd.VLED6,
		@lead7			= sd.VLED7,
		@review0		= sd.ReviewDay0,
		@review1		= sd.ReviewDay1,
		@review2		= sd.ReviewDay2,
		@review3		= sd.ReviewDay3,
		@review4		= sd.ReviewDay4,
		@review5		= sd.ReviewDay5,
		@review6		= sd.ReviewDay6,
		@mcpType		= sd.MCPT,
		@mcpValue		= sd.MCPV,
		@mcpCartons		= sd.MCPC,
		@mcpWeight		= sd.MCPW,
		@address1		= sd.ADD1,
		@address2		= sd.ADD2,
		@address3		= sd.ADD3,
		@address4		= sd.ADD4,
		@address5		= sd.ADD5,
		@postcode		= sd.POST,
		@phone			= sd.PHO1,
		@fax			= sd.FAX1,
		@helpline		= sm.HLIN,
		@contact		= sd.CON1,	
		@type			= 
							(
								Select Case sd.BBCC
									When '' Then 'Direct'
									When 'C' Then 'Consolidated'
									When 'D' Then 'Discreet'
									When 'W' Then 'Warehouse'
									When 'A' Then 'Alternative'
								End
							),
		@tradanet		= 
							(
								Select Case sd.TNET
									When 1 Then 'Yes'
									When 0 Then 'No'
								End
							)
	From
		SUPMAS sm
		Inner Join
			SUPDET sd	
		On 
			sd.SUPN = sm.SUPN 
		And 
			sd.DEPO = sm.ODNO
	Where
		sm.SUPN = @SupplierNumber

	--Set order days
	Set @reviewDays='';
	If @review1=1 
		Begin
			Set @reviewDays='Mon';
		End
	If @review2=1
		Begin
			If LEN(@reviewdays)>0 
				Begin
					Set @reviewDays = @reviewDays + ', ';
				End
			Set @reviewDays = @reviewDays + 'Tue';
		End
	If @review3=1
		Begin
			If LEN(@reviewdays)>0 
				Begin
					Set @reviewDays = @reviewDays + ', ';
				End
			Set @reviewDays = @reviewDays + 'Wed';
		End
	If @review4=1
		Begin
			If LEN(@reviewdays)>0 
				Begin
					Set @reviewDays = @reviewDays + ', ';
				End
			Set @reviewDays = @reviewDays + 'Thu';
		End
	If @review5=1
		Begin
			If LEN(@reviewdays)>0 
				Begin
					Set @reviewDays = @reviewDays + ', ';
				End
			Set @reviewDays = @reviewDays + 'Fri';
		End
	If @review6=1
		Begin
			If LEN(@reviewdays)>0 
				Begin
					Set @reviewDays = @reviewDays + ', ';
				End
			Set @reviewDays = @reviewDays + 'Sat';
		End
	If @review0=1
		Begin
			If LEN(@reviewdays)>0 
				Begin
					Set @reviewDays = @reviewDays + ', ';
				End
			Set @reviewDays = @reviewDays + 'Sun';
		End	
	
	Set	@mcpTypeString='';
	If @mcpType='M'
		Begin
			Set @mcpTypeString = 'Money';
			Set @mcpValueWeight = @mcpValue;
		End
	If @mcpType='C'
		Begin
			Set @mcpTypeString = 'Cartons';
			Set @mcpValueWeight = @mcpCartons;
		End
	If @mcpType='U'
		Begin
			Set @mcpTypeString = 'Units';
			Set @mcpValueWeight = @mcpCartons;
		End
	If @mcpType='E'
		Begin
			Set	@mcpTypeString = 'Either Money or Cartons';
			Set @mcpValueWeight = @mcpValue + ' Money OR ' + @mcpCartons + ' Cartons';
		End
	If @mcpType='T'
		Begin
			Set @mcpTypeString='Weight';
			Set @mcpValueWeight=@mcpWeight;
		End
	
	Insert Into @table Values ('ID',				@SupplierNumber );
	Insert Into @table Values ('Name',				RTrim(@name) );
	Insert Into @table Values ('Address',	rtrim(@address1) );
	Insert Into @table Values ('',			rtrim(@address2) );
	Insert Into @table Values ('',			rtrim(@address3) );
	Insert Into @table Values ('',			rtrim(@address4) );
	Insert Into @table Values ('',			rtrim(@address5) );
	Insert Into @table Values ('',			rtrim(@postcode) );
	Insert Into @table Values ('','');
	Insert Into @table Values ('Helpline (Cust. Serv.)',	rtrim(@helpline) );
	Insert Into @table Values ('Telephone',					rtrim(@phone) );
	Insert Into @table Values ('Fax',						rtrim(@fax) );
	Insert Into @table Values ('Supplier Contact',			RTRIM(@contact) );
	Insert Into @table Values ('Supplier Warehouse',	@warehouse );
	Insert Into @table Values ('Type',			rtrim(@type) );
	Insert Into @table Values ('Tradanet',		@tradanet );
	Insert Into @table Values ('','');
	Insert Into @table Values ('Order Closedown',	Convert(VarChar(10), @orderClose, 103) );
	Insert Into @table Values ('Order Reopen',		Convert(VarChar(10), @orderOpen, 103) );
	Insert Into @table Values ('Delivery Closedown',Convert(VarChar(10), @delClose, 103) );
	Insert Into @table Values ('Delivery Reopen',	Convert(VarChar(10), @delOpen, 103) );
	Insert Into @table Values ('','');
	Insert Into @table Values ('Last Ordered',	Convert(VarChar(10), @lastOrdered, 103) );
	Insert Into @table Values ('Last Received', Convert(VarChar(10), @lastReceived, 103) );
	Insert Into @table Values ('Lead Time: Mon', @lead1 );
	Insert Into @table Values ('Lead Time: Tue', @lead2 );
	Insert Into @table Values ('Lead Time: Wed', @lead3 );
	Insert Into @table Values ('Lead Time: Thu', @lead4 );
	Insert Into @table Values ('Lead Time: Fri', @lead5 );
	Insert Into @table Values ('Lead Time: Sat', @lead6 );
	Insert Into @table Values ('Lead Time: Sun', @lead7 );
	Insert Into @table Values ('Order Days',	@reviewDays );
	Insert Into @table Values ('','');
	Insert Into @table Values ('MCP Type',		@mcpTypeString );
	Insert Into @table Values ('MCP Value/Weight', @mcpValueWeight );
		
	Select * From @table;
End
GO

