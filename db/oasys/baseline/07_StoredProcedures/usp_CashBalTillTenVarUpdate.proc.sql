﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_CashBalTillTenVarUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_CashBalTillTenVarUpdate'
	EXEC ('CREATE PROCEDURE [dbo].[usp_CashBalTillTenVarUpdate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_CashBalTillTenVarUpdate'
GO
ALTER PROCEDURE [dbo].[usp_CashBalTillTenVarUpdate]
@TransactionDate date,
@TillNumber CHAR (2),
@TransactionNumber CHAR (4)
AS
BEGIN

SET NOCOUNT OFF
        
create table #BankingTemp2
(
   
    RID INT IDENTITY(1,1),
    [PeriodID] [int] NOT NULL,
    [TradingPeriodID] [int] NOT NULL,
	[TillID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[ID] [decimal](9,2) NOT NULL,	
	[Amount] [decimal](9,2) NULL,
	[PickUp] [decimal](9, 2) NOT NULL	        
)

create table #CashBalTenVarTemp
(
    [PeriodID] [int] NOT NULL,
    [TradingPeriodID] [int] NOT NULL,
	[TillID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[ID] [int] NOT NULL,
	[Quantity] [decimal](5, 0) NOT NULL,
	[Amount] [decimal](9, 2) NOT NULL,
	[PickUp] [decimal](9, 2) NOT NULL
)

INSERT INTO #BankingTemp2 ([PeriodID],[TradingPeriodID],[CurrencyID],[TillID] ,[ID],[Amount],[PickUp])
SELECT 

    (select id from SystemPeriods where StartDate = dt.DATE1) as PeriodID,
    (select id from SystemPeriods
     where
		(DT.CASH = '800' and StartDate = dt.DATE1 or 
		 DT.CASH = '499' and StartDate = DATEADD(DAY, DATEDIFF(DAY, 0, dt.ReceivedDate), 0))
    ) as TradingPeriodID,
    (select id from SystemCurrency) as CurrencyID,
	cast(dt.TILL as int) as TillID, 
	dp.[Type] as ID,
	ISNULL(dp.AMNT,0) as Amount,
	'0'	
	
FROM DLTOTS as DT 
     inner join DLPAID DP on  dt.[TRAN] = dp.[TRAN] and dt.DATE1 = dp.DATE1 and dt.TILL =dp.TILL	
where 
DT.CASH in ('800', '499') and DT.DATE1 = @TransactionDate and DT.TILL = @TillNumber and dt.[TRAN] = @TransactionNumber 


INSERT INTO #CashBalTenVarTemp 
SELECT * FROM CashBalTillTenVar

    DECLARE @RID_1 INT
	DECLARE @MAXID_1 INT
	SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp2 -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID_1 = 1
	DECLARE @PERIODID INT
	DECLARE @TRADINGPERIODID INT
	DECLARE @CURRENCY CHAR(5)
	DECLARE @TILL_ID INT
	DECLARE @ID INT
	DECLARE @LINEUPDATED AS INT
	 
-- looping the #temp_old from first record to last record.
	WHILE(@RID_1<=@MAXID_1)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @PERIODID = PeriodID,@TRADINGPERIODID=TradingPeriodID,@CURRENCY= CurrencyID,@TILL_ID= TillID, @ID = ID FROM #BankingTemp2 WHERE RID = @RID_1
			-- if the Primary key exists then update, by taking the values from the #temp_old  
	    SET @LINEUPDATED = 0
		IF EXISTS(SELECT 1 FROM #CashBalTenVarTemp WHERE PeriodID = @PERIODID AND TradingPeriodID = @TRADINGPERIODID AND CurrencyID = @CURRENCY AND TillID=@TILL_ID AND ID = @ID)
		
		  BEGIN
		   UPDATE CashBalTillTenVar
		   SET 
			[Quantity] = CashBalTillTenVar.[Quantity]+1 ,
	        [Amount] = CashBalTillTenVar.[Amount]-T.Amount
	        FROM #BankingTemp2 AS T WHERE (CashBalTillTenVar.PeriodID = @PERIODID AND T.PeriodID = @PERIODID) AND (CashBalTillTenVar.TradingPeriodID = @TRADINGPERIODID AND T.TradingPeriodID = @TRADINGPERIODID) AND (CashBalTillTenVar.CurrencyID = @CURRENCY and  T.CurrencyID =@CURRENCY ) AND (CashBalTillTenVar.TillID = @TILL_ID and  T.TillID = @TILL_ID) AND (CashBalTillTenVar.ID  = @ID  AND T.ID = @ID)
	        SET @LINEUPDATED = @LINEUPDATED +@@ROWCOUNT 
	     END
     ELSE
       BEGIN
        INSERT INTO CashBalTillTenVar(PeriodID,TradingPeriodID,CurrencyID,TillID,ID,Quantity,Amount,PickUp)
        SELECT T.PeriodID, T.TradingPeriodID,T.CurrencyID,T.TillID,T.ID, 1,-(T.Amount),T.PickUp
      	FROM #BankingTemp2   AS T 
		WHERE T.RID = @RID_1
		SET @LINEUPDATED = @LINEUPDATED +@@ROWCOUNT 
     END
     
     delete #BankingTemp2 where RID = @RID_1
	 SELECT @MAXID_1 = MAX(RID)FROM #BankingTemp2
				
     delete #CashBalTenVarTemp  
     INSERT INTO #CashBalTenVarTemp
     SELECT * FROM CashBalTillTenVar  
     	  
	SET @RID_1 = @RID_1+1 
END
RETURN @LINEUPDATED
END
GO

