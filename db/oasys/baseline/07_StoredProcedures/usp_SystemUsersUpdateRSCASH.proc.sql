﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SystemUsersUpdateRSCASH]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SystemUsersUpdateRSCASH'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SystemUsersUpdateRSCASH] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SystemUsersUpdateRSCASH'
GO
ALTER PROCEDURE [dbo].[usp_SystemUsersUpdateRSCASH]
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 8th March 2011
-- 
-- Task     : Updates / Inserts Users into RSCASH table for use by Till Systems.
-- Notes	: SP updates or inserts data as old data is sometimes present. Updating or Inserting the
--            data will prevent the stored procedure from failing. Data in Pilot stores has shown that
--            existing data can or may not already exist.
-----------------------------------------------------------------------------------
	
		@sp_EmployeeCode			char(3),
		@sp_Name					char(35),
		@sp_Password				char(5)

AS
BEGIN
	SET NOCOUNT ON;

--------------------------------------------------------------------------------------------------------------------------
-- Update Existing User into CASMAS Table
--------------------------------------------------------------------------------------------------------------------------
IF EXISTS (Select CASH FROM [Oasys].[dbo].[RSCASH] WHERE CASH = @sp_EmployeeCode)
	Begin
		Update	RSCASH
		Set		SECC = @sp_Password
		Where	CASH = @sp_EmployeeCode
	End			
END
GO

