﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EventGetHeaderByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EventGetHeaderByEvent'
	EXEC ('CREATE PROCEDURE [dbo].[EventGetHeaderByEvent] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EventGetHeaderByEvent'
GO
ALTER PROCEDURE [dbo].[EventGetHeaderByEvent]
	@Event CHAR (6), @SkuNumber CHAR (6)
AS
BEGIN
	SET NOCOUNT ON;

--20100924 PD : not required & sub-query is returing multiple records
/*
    -- Insert statements for procedure here
Declare @ETYP char(2);
set @ETYP = (select en.ETYP FROM  EVTHDR AS eh INNER JOIN EVTENQ AS en ON eh.NUMB=en.NUMB WHERE en.SKUN=@SkuNumber);
*/

SELECT 
 	   RowId = 1,
	   @Event   AS 'Event',
	   Key1     AS 'Part Code',
	   Key2     AS 'Item #',
	   eh.DESCR AS Description,
	   CASE
			WHEN TYPE = 'TS' THEN 'Temporary Price Change' 
			WHEN TYPE = 'TM' THEN 'M&M Temporary Price Change' 
			WHEN TYPE = 'QS' THEN 'Sku Quantity Break' 
			WHEN TYPE = 'QM' THEN 'M&M Quantity Break' 
			WHEN TYPE = 'QD' THEN 'Deal Quantity Break' 
			WHEN TYPE = 'MS' THEN 'Sku Multibuy' 
			WHEN TYPE = 'MM' THEN 'M&M Multibuy' 
			WHEN TYPE = 'DG' THEN 'Deal Group' 
			WHEN TYPE = 'HS' THEN 'Hierarchy Spend' 
	   End  AS DealType,		
	   PRIC AS 'Deal Price' 
FROM EVTMAS em Inner Join EVTHDR eh on em.NUMB = eh.NUMB  
WHERE em.numb = @Event and eM.KEY1 = @SkuNumber

END
GO

