﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_FKeys]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_FKeys'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_FKeys] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_FKeys'
GO
ALTER PROCEDURE dbo.KevanConversion_FKeys
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 00 - Install Foreign Keys
-----------------------------------------------------------------------------------
AS
BEGIN

Delete from DRLDET  where DRLDET.NUMB not in (select ds.NUMB from DRLSUM ds)


-- DRLDET Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DRLDET_DRLSUM]') AND parent_object_id = OBJECT_ID(N'[dbo].[DRLDET]'))
	Begin
		Print ('Constraint Exists (FK_DRLDET_DRLSUM)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_DRLDET_DRLSUM..')
		ALTER TABLE [dbo].[DRLDET]  WITH CHECK ADD  CONSTRAINT [FK_DRLDET_DRLSUM] FOREIGN KEY([NUMB]) REFERENCES [dbo].[DRLSUM] ([NUMB]) ON DELETE CASCADE
		ALTER TABLE [dbo].[DRLDET] CHECK CONSTRAINT [FK_DRLDET_DRLSUM]
	End


-- HHTDET Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HHTDET_HHTHDR]') AND parent_object_id = OBJECT_ID(N'[dbo].[HHTDET]'))
	Begin
		Print ('Constraint Exists (FK_HHTDET_HHTHDR)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_HHTDET_HHTHDR..')
		ALTER TABLE [dbo].[HHTDET]  WITH CHECK ADD  CONSTRAINT [FK_HHTDET_HHTHDR] FOREIGN KEY([DATE1]) REFERENCES [dbo].[HHTHDR] ([DATE1]) ON UPDATE CASCADE ON DELETE CASCADE
		ALTER TABLE [dbo].[HHTDET] CHECK CONSTRAINT [FK_HHTDET_HHTHDR]
	End


-- HhtLocation Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HhtLocation_HHTDET]') AND parent_object_id = OBJECT_ID(N'[dbo].[HhtLocation]'))
	Begin
		Print ('Constraint Exists (FK_HhtLocation_HHTDET)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_HhtLocation_HHTDET..')
		ALTER TABLE [dbo].[HhtLocation]  WITH CHECK ADD  CONSTRAINT [FK_HhtLocation_HHTDET] FOREIGN KEY([DateCreated], [SkuNumber]) REFERENCES [dbo].[HHTDET] ([DATE1], [SKUN]) ON UPDATE CASCADE ON DELETE CASCADE
		ALTER TABLE [dbo].[HhtLocation] CHECK CONSTRAINT [FK_HhtLocation_HHTDET]
	End


-- Islands Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Islands_IslandTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Islands]'))
	Begin
		Print ('Constraint Exists (FK_Islands_IslandTypes)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_Islands_IslandTypes..')
		ALTER TABLE [dbo].[Islands]  WITH CHECK ADD  CONSTRAINT [FK_Islands_IslandTypes] FOREIGN KEY([Code]) REFERENCES [dbo].[IslandTypes] ([Code])
		ALTER TABLE [dbo].[Islands] CHECK CONSTRAINT [FK_Islands_IslandTypes]
	End


-- ISULIN Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ISULIN_ISUHDR]') AND parent_object_id = OBJECT_ID(N'[dbo].[ISULIN]'))
	Begin
		Print ('Constraint Exists (FK_ISULIN_ISUHDR)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_ISULIN_ISUHDR..')
		ALTER TABLE [dbo].[ISULIN]  WITH CHECK ADD  CONSTRAINT [FK_ISULIN_ISUHDR] FOREIGN KEY([NUMB]) REFERENCES [dbo].[ISUHDR] ([NUMB]) ON DELETE CASCADE
		ALTER TABLE [dbo].[ISULIN] CHECK CONSTRAINT [FK_ISULIN_ISUHDR]
	End


-- PlanSegments Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PlanSegments_PlanNumbers]') AND parent_object_id = OBJECT_ID(N'[dbo].[PlanSegments]'))
	Begin
		Print ('Constraint Exists (FK_PlanSegments_PlanNumbers)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_PlanSegments_PlanNumbers..')
		ALTER TABLE [dbo].[PlanSegments]  WITH CHECK ADD  CONSTRAINT [FK_PlanSegments_PlanNumbers] FOREIGN KEY([Number]) REFERENCES [dbo].[PlanNumbers] ([Number]) ON DELETE CASCADE
		ALTER TABLE [dbo].[PlanSegments] CHECK CONSTRAINT [FK_PlanSegments_PlanNumbers]
	End


-- PlanStocks Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PlanStocks_PlanSegments]') AND parent_object_id = OBJECT_ID(N'[dbo].[PlanStocks]'))
	Begin
		Print ('Constraint Exists (FK_PlanStocks_PlanSegments)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_PlanStocks_PlanSegments..')
		ALTER TABLE [dbo].[PlanStocks]  WITH CHECK ADD  CONSTRAINT [FK_PlanStocks_PlanSegments] FOREIGN KEY([Number], [Segment]) REFERENCES [dbo].[PlanSegments] ([Number], [Segment]) ON DELETE CASCADE
		ALTER TABLE [dbo].[PlanStocks] CHECK CONSTRAINT [FK_PlanStocks_PlanSegments]
	End


-- ProfileMenuAccess Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProfileMenuAccess_MenuConfig]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProfileMenuAccess]'))
	Begin
		Print ('Constraint Exists (FK_ProfileMenuAccess_MenuConfig)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_ProfileMenuAccess_MenuConfig..')
		ALTER TABLE [dbo].[ProfilemenuAccess]  WITH CHECK ADD  CONSTRAINT [FK_ProfileMenuAccess_MenuConfig] FOREIGN KEY([MenuConfigID]) REFERENCES [dbo].[MenuConfig] ([ID]) ON DELETE CASCADE
		ALTER TABLE [dbo].[ProfilemenuAccess] CHECK CONSTRAINT [FK_ProfileMenuAccess_MenuConfig]
	End


-- PURLIN Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PURLIN_PURHDR]') AND parent_object_id = OBJECT_ID(N'[dbo].[PURLIN]'))
	Begin
		Print ('Constraint Exists (FK_PURLIN_PURHDR)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_PURLIN_PURHDR..')
		ALTER TABLE [dbo].[PURLIN]  WITH CHECK ADD  CONSTRAINT [FK_PURLIN_PURHDR] FOREIGN KEY([HKEY]) REFERENCES [dbo].[PURHDR] ([TKEY]) ON DELETE CASCADE
		ALTER TABLE [dbo].[PURLIN] CHECK CONSTRAINT [FK_PURLIN_PURHDR]
	End


-- QUOLIN Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QUOLIN_QUOHDR]') AND parent_object_id = OBJECT_ID(N'[dbo].[QUOLIN]'))
	Begin
		Print ('Constraint Exists (FK_QUOLIN_QUOHDR)..')
	End
ELSE	
	Begin
		Print ('Creating Constraint FK_QUOLIN_QUOHDR')
		ALTER TABLE [dbo].[QUOLIN]  WITH CHECK ADD  CONSTRAINT [FK_QUOLIN_QUOHDR] FOREIGN KEY([NUMB]) REFERENCES [dbo].[QUOHDR] ([NUMB]) ON DELETE CASCADE
		ALTER TABLE [dbo].[QUOLIN] CHECK CONSTRAINT [FK_QUOLIN_QUOHDR]
	End


-- STKTXT Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_STKTXT_STKMAS]') AND parent_object_id = OBJECT_ID(N'[dbo].[STKTXT]'))
	Begin
		Print ('Constraint Exists (FK_STKTXT_STKMAS)..')
	End
ELSE
	Begin
		Print ('Creating Constraint FK_STKTXT_STKMAS..')
		ALTER TABLE [dbo].[STKTXT]  WITH CHECK ADD  CONSTRAINT [FK_STKTXT_STKMAS] FOREIGN KEY([SKUN]) REFERENCES [dbo].[STKMAS] ([SKUN]) ON DELETE CASCADE
		ALTER TABLE [dbo].[STKTXT] CHECK CONSTRAINT [FK_STKTXT_STKMAS]
	End


-- SUPDET Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SUPDET_SUPMAS]') AND parent_object_id = OBJECT_ID(N'[dbo].[SUPDET]'))
	Begin
		Print ('Constraint Exists (FK_SUPDET_SUPMAS)..')
	End
ELSE
	Begin
		Print ('Creating Constraint FK_SUPDET_SUPMAS..')
		ALTER TABLE [dbo].[SUPDET]  WITH CHECK ADD  CONSTRAINT [FK_SUPDET_SUPMAS] FOREIGN KEY([SUPN]) REFERENCES [dbo].[SUPMAS] ([SUPN]) ON DELETE CASCADE
		ALTER TABLE [dbo].[SUPDET] CHECK CONSTRAINT [FK_SUPDET_SUPMAS]
	End


-- SUPNOT Table
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SUPNOT_SUPMAS]') AND parent_object_id = OBJECT_ID(N'[dbo].[SUPNOT]'))
	Begin
		Print ('Constraint Exists (FK_SUPNOT_SUPMAS)..')
	End
ELSE
	Begin
		Print ('Creating Constraint FK_SUPNOT_SUPMAS..')
		ALTER TABLE [dbo].[SUPNOT]  WITH CHECK ADD  CONSTRAINT [FK_SUPNOT_SUPMAS] FOREIGN KEY([SUPN]) REFERENCES [dbo].[SUPMAS] ([SUPN]) ON DELETE CASCADE
		ALTER TABLE [dbo].[SUPNOT] CHECK CONSTRAINT [FK_SUPNOT_SUPMAS]
	End


END;
GO

