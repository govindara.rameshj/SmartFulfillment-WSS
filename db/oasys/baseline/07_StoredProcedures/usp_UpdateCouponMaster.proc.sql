﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_UpdateCouponMaster]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_UpdateCouponMaster'
	EXEC ('CREATE PROCEDURE [dbo].[usp_UpdateCouponMaster] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_UpdateCouponMaster'
GO
-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 06/10/2011
-- TFSID        : 2733 
-- Description  : New Stored Procedure for Coupons ProcessTransmissions
-- =====================================================================


-- ====================================================================
-- Author       : Dhanesh Ramachandran
-- Create date  : 16/12/2011
-- Description  : Added Permissions to the stored procedure
-- =====================================================================

ALTER PROCEDURE usp_UpdateCouponMaster
 @CouponID        AS CHAR(7), 
 @Description     AS CHAR(40), 
 @StoreGenerated  AS BIT, 
 @SerialNumber    AS INT, 
 @ExclusiveCoupon AS BIT, 
 @Reusable        AS INT, 
 @CollecInfo      AS BIT, 
 @Deleted         AS INT 
 
AS 
  BEGIN 
      IF EXISTS (SELECT 1 
                 FROM   COUPONMASTER 
                 WHERE  couponid = @couponID) 
        BEGIN 
            UPDATE COUPONMASTER 
            SET    [DESCRIPTION] = @Description, 
                   storegen = @StoreGenerated, 
                   serialno = @SerialNumber, 
                   exccoupon = @ExclusiveCoupon, 
                   reusable = @Reusable, 
                   collectinfo = @CollecInfo, 
                   deleted = @Deleted 
            WHERE  couponid = @couponID 
        END 
      ELSE 
        BEGIN 
            INSERT INTO COUPONMASTER 
                        (couponid, 
                         [DESCRIPTION], 
                         storegen, 
                         serialno, 
                         exccoupon, 
                         reusable, 
                         collectinfo, 
                         deleted) 
            VALUES      ( @CouponID, 
                          @Description, 
                          @StoreGenerated, 
                          @SerialNumber, 
                          @ExclusiveCoupon, 
                          @Reusable, 
                          @CollecInfo, 
                          @Deleted ) 
        END 
  END
GO

