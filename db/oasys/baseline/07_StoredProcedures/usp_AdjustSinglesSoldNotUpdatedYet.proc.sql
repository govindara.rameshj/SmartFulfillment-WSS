﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_AdjustSinglesSoldNotUpdatedYet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_AdjustSinglesSoldNotUpdatedYet'
	EXEC ('CREATE PROCEDURE [dbo].[usp_AdjustSinglesSoldNotUpdatedYet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_AdjustSinglesSoldNotUpdatedYet'
GO
ALTER PROCedure [dbo].[usp_AdjustSinglesSoldNotUpdatedYet]
	@Skun As Int
As
	Begin
	
		Set NoCount On
		
		Update
			RELITM
		Set
			QTYS = QTYS - NSPP
		Where
			SPOS = @Skun
End
GO

