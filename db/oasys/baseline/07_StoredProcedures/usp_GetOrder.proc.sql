﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetOrder'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetOrder] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetOrder'
GO
ALTER PROCEDURE [dbo].[usp_GetOrder] (
	@DeliveryDate DATETIME = NULL
	)

AS

BEGIN
	SET NOCOUNT ON;

	SELECT 
		NUMB = cl.NUMB, 
		LINE = cl.LINE, 
		QtyToBeDelivered = cl.QtyToBeDelivered, 
	--	DeliveryStatus = ch4.DeliveryStatus, 
		DeliveryStatusDescription = (
			select Description 
			from StaticData 
			where CONVERT(VARCHAR(10),StaticData.Value) = CONVERT(VARCHAR(10), ch4.DeliveryStatus)),
		NAME = ch4.NAME,
		OMOrderNumber = ch4.OMOrderNumber, 
		CustomerAddress1 = ISNULL(ch.ADDR1,''),
		CustomerAddress2 = ISNULL(ch.ADDR2,''),
		CustomerAddress3 = ISNULL(ch.ADDR3,''),
		CustomerAddress4 = ISNULL(ch.ADDR4,''),
		CustomerPostcode = ISNULL(ch.POST,''),
		SKUN = cl.SKUN, 
		DELD = left(ch.DELD,11), 
		VOLU = stk.VOLU, 
		WGHT = stk.WGHT,
		TotalOrderItemWeight = stk.WGHT * cl.QtyToBeDelivered,
		PLUD,
		DESCR,
		Notes = ISNULL(REPLACE(REPLACE((
			SELECT COALESCE(txt.[TEXT],'') AS Notes 
			FROM  cortxt txt
			WHERE numb= ch4.numb FOR XML PATH('') ),'<Notes>',' '),'</Notes>',''),''),
		ISNULL(ch.PHON,'') AS PHON ,
		va.VehicleTypeIndex as VechileSelected, 
		va.LastVehicleUpdateDateTime as LastUpdateDateTime
	FROM CORHDR ch
		INNER JOIN CORHDR4 ch4
			ON ch.NUMB = ch4.NUMB 
		INNER JOIN CORLIN cl WITH (NOLOCK)
			ON ch4.NUMB = cl.NUMB
		INNER JOIN STKMAS stk
			ON cl.SKUN = stk.SKUN 
	     LEFT OUTER JOIN VehicleAllocation va
	          on ch4.OMOrderNumber = va.OrderId	
	        and va.DeliveryDate =  ch4.DELD        
	WHERE ((cl.DeliveryStatus > 499 AND cl.DeliveryStatus < 700) OR (cl.DeliveryStatus = 899))
		AND cl.DeliverySource IN ('8' + (SELECT TOP 1 STOR FROM RETOPT WITH (NOLOCK) ))
		AND ch4.DELD = @DeliveryDate
		AND cl.IsDeliveryChargeItem = 0
		AND cl.QtyToBeDelivered > 0
		AND ch.DELI = 1		
	ORDER BY cl.NUMB, cl.LINE

	SET NOCOUNT OFF;
END
GO

