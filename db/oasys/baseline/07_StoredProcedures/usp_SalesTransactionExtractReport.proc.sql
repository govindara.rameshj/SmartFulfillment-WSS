﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SalesTransactionExtractReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SalesTransactionExtractReport'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SalesTransactionExtractReport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SalesTransactionExtractReport'
GO
ALTER PROCEDURE [dbo].[usp_SalesTransactionExtractReport] 
    @StartDate AS date,
    @EndDate AS date = NULL
AS
BEGIN	
    declare @StoreNumber AS char (3) = (SELECT TOP 1 STOR FROM RETOPT)
    
    SELECT
        Totals.DATE1 [Transaction Date / Time],
        @StoreNumber [Store Id],
        Totals.TILL [Till Id],
        Totals.CASH [Operator Id],
        ISNULL(SUM(DailyLines.[Initial Price TotalValue]), 0.00) [Total Transaction Value (inc VAT)],
        ISNULL(SUM(DailyLines.[Initial Price TotalValue] - DailyLines.[Lines Variance]), 0.00) [Total Value of Lines],
        ISNULL(SUM(Totals.TOTL), 0.00) [Total Value of Transaction Charged (inc VAT)],
        ISNULL(SUM(DailyLines.[Initial Price TotalValue] - Totals.TOTL), 0.00) [Totals Variance],
        ISNULL(SUM(Totals.VATV1 + Totals.VATV2 + Totals.VATV3 + Totals.VATV4 + Totals.VATV5 + Totals.VATV6 + Totals.VATV7 +  Totals.VATV8 +  Totals.VATV9), 0.00) [Total Value of VAT],
        COUNT(DailyLines.[TRAN]) [Total Number of Transactions],
        '' [Paid In or Paid Out],
        ISNULL(SUM(DailyPayments.[Card Amount]), 0.00) [Total Card Payment Value],
        ISNULL(SUM(DailyPayments.[Cash Amount]), 0.00) [Total Cash Payment Value],
        ISNULL(SUM(DailyPayments.[Gift Card Amount]), 0.00) [Total Gift Card Value],
        --ISNULL(SUM(DailyPayments.[Voucher Amount]), 0.00) [Total Voucher Value],
        ISNULL(SUM(DailyPayments.[Cheque Amount]), 0.00) [Total Cheque Value],
        ISNULL(SUM(DailyPayments.[Total Amount]), 0.00) [Total of Payments]
    FROM DLTOTS AS Totals
    LEFT JOIN 
        (
            SELECT 
                DATE1, 
                TILL, 
                [TRAN],
                ISNULL(SUM(SPRI*QUAN), 0.00) [Initial Price TotalValue],
                ISNULL(SUM((ESPD+TPPD+POPD)*QUAN+QBPD+DGPD+MBPD+HSPD), 0.00) [Lines Variance]
            FROM DLLINE
            WHERE SKUN <> '769999'
            GROUP BY DATE1, TILL, [TRAN]
        ) AS DailyLines ON DailyLines.DATE1 = Totals.DATE1 AND DailyLines.[TRAN] = Totals.[TRAN] AND DailyLines.TILL = Totals.TILL
    LEFT JOIN 
        (
            SELECT
                DATE1, 
                TILL, 
                [TRAN],
                -1 * SUM(CASE WHEN [TYPE] = 3 OR [TYPE] = 8 OR [TYPE] = 9 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Card Amount],
                -1 * SUM(CASE WHEN [TYPE] = 1 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) -1 * SUM(CASE WHEN [TYPE] = 99 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Cash Amount],
                -1* SUM(CASE WHEN [TYPE] = 13 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Gift Card Amount],
                ---1 * SUM(CASE WHEN [TYPE] = 6 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Voucher Amount],
                -1 * SUM(CASE WHEN [TYPE] = 2 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Cheque Amount],
                -1 * SUM(CASE WHEN [TYPE] = 10 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [H/O ChequeAmount],
                -1 * SUM(AMNT) [Total Amount]
            FROM DLPAID
            GROUP BY DATE1, TILL, [TRAN]
        ) AS DailyPayments ON DailyPayments.DATE1 = Totals.DATE1 AND DailyPayments.[TRAN] = Totals.[TRAN] AND DailyPayments.TILL = Totals.TILL
    WHERE (Totals.TCOD = 'SA' OR Totals.TCOD = 'RF') AND (Totals.VOID = 0) AND ((Totals.DATE1 = @StartDate AND @EndDate IS NULL) OR (Totals.DATE1 between @StartDate AND @EndDate)) AND Totals.Source = 'OVC'
    GROUP BY Totals.DATE1, Totals.CASH, Totals.TILL
    HAVING COUNT(DailyLines.[TRAN]) > 0 
    UNION
        SELECT
        Totals.DATE1 [Transaction Date / Time],
        @StoreNumber [Store Id],
        Totals.TILL [Till Id],
        Totals.CASH [Operator Id],
        ISNULL(SUM(Totals.TOTL), 0.00) [Total Transaction Value (inc VAT)],
        0.00 [Total Value of Lines],
        ISNULL(SUM(Totals.TOTL), 0.00) [Total Value of Transaction Charged (inc VAT)],
        0.00 [Totals Variance],
        0.00 [Total Value of VAT],
        COUNT(*) [Total Number of Transactions],
        'Paid In' [Paid In or Paid Out],
        ISNULL(SUM(DailyPayments.[Card Amount]), 0.00) [Total Card Payment Value],
        ISNULL(SUM(DailyPayments.[Cash Amount]), 0.00) [Total Cash Payment Value],
        ISNULL(SUM(DailyPayments.[Gift Card Amount]), 0.00) [Total Gift Card Value],
        --ISNULL(SUM(DailyPayments.[Voucher Amount]), 0.00) [Total Voucher Value],
        ISNULL(SUM(DailyPayments.[Cheque Amount]), 0.00) [Total Cheque Value],
		ISNULL(SUM(DailyPayments.[Total Amount]), 0.00) [Total of Payments]
	FROM DLTOTS AS Totals
	LEFT JOIN 
		(
			SELECT
                DATE1, 
                TILL, 
                [TRAN],
                -1 * SUM(CASE WHEN [TYPE] = 3 OR [TYPE] = 8 OR [TYPE] = 9 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Card Amount],
                -1 * SUM(CASE WHEN [TYPE] = 1 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) -1 * SUM(CASE WHEN [TYPE] = 99 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Cash Amount],
                -1* SUM(CASE WHEN [TYPE] = 13 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Gift Card Amount],
                ---1 * SUM(CASE WHEN [TYPE] = 6 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Voucher Amount],
                -1 * SUM(CASE WHEN [TYPE] = 2 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Cheque Amount],
                -1 * SUM(CASE WHEN [TYPE] = 10 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [H/O ChequeAmount],
                -1 * SUM(AMNT) [Total Amount]
            FROM DLPAID
            GROUP BY DATE1, TILL, [TRAN]
		) AS DailyPayments ON DailyPayments.DATE1 = Totals.DATE1 AND DailyPayments.[TRAN] = Totals.[TRAN] AND DailyPayments.TILL = Totals.TILL
	WHERE Totals.TCOD = 'M+' AND ((Totals.DATE1 = @StartDate AND @EndDate IS NULL) OR (Totals.DATE1 between @StartDate AND @EndDate)) AND Totals.Source = 'OVC'
	GROUP BY Totals.DATE1, Totals.CASH, Totals.TILL
	UNION
        SELECT
        Totals.DATE1 [Transaction Date / Time],
        @StoreNumber [Store Id],
        Totals.TILL [Till Id],
        Totals.CASH [Operator Id],
        ISNULL(SUM(Totals.TOTL), 0.00) [Total Transaction Value (inc VAT)],
        0.00 [Total Value of Lines],
        ISNULL(SUM(Totals.TOTL), 0.00) [Total Value of Transaction Charged (inc VAT)],
        0.00 [Totals Variance],
        0.00 [Total Value of VAT],
        COUNT(*) [Total Number of Transactions],
        'Paid Out' [Paid In or Paid Out],
        ISNULL(SUM(DailyPayments.[Card Amount]), 0.00) [Total Card Payment Value],
        ISNULL(SUM(DailyPayments.[Cash Amount]), 0.00) [Total Cash Payment Value],
        ISNULL(SUM(DailyPayments.[Gift Card Amount]), 0.00) [Total Gift Card Value],
        --ISNULL(SUM(DailyPayments.[Voucher Amount]), 0.00) [Total Voucher Value],
        ISNULL(SUM(DailyPayments.[Cheque Amount]), 0.00) [Total Cheque Value],
		ISNULL(SUM(DailyPayments.[Total Amount]), 0.00) [Total of Payments]
	FROM DLTOTS AS Totals
	LEFT JOIN 
		(
			SELECT
                DATE1, 
                TILL, 
                [TRAN],
                -1 * SUM(CASE WHEN [TYPE] = 3 OR [TYPE] = 8 OR [TYPE] = 9 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Card Amount],
                -1 * SUM(CASE WHEN [TYPE] = 1 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) -1 * SUM(CASE WHEN [TYPE] = 99 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Cash Amount],
                -1* SUM(CASE WHEN [TYPE] = 13 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Gift Card Amount],
                ---1 * SUM(CASE WHEN [TYPE] = 6 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Voucher Amount],
                -1 * SUM(CASE WHEN [TYPE] = 2 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [Cheque Amount],
                -1 * SUM(CASE WHEN [TYPE] = 10 THEN ISNULL(AMNT, 0.00) ELSE 0.00 END) [H/O ChequeAmount],
                -1 * SUM(AMNT) [Total Amount]
            FROM DLPAID
            GROUP BY DATE1, TILL, [TRAN]
		) AS DailyPayments ON DailyPayments.DATE1 = Totals.DATE1 AND DailyPayments.[TRAN] = Totals.[TRAN] AND DailyPayments.TILL = Totals.TILL
	WHERE Totals.TCOD = 'M-' AND ((Totals.DATE1 = @StartDate AND @EndDate IS NULL) OR (Totals.DATE1 between @StartDate AND @EndDate)) AND Totals.Source = 'OVC'
	GROUP BY Totals.DATE1, Totals.CASH, Totals.TILL	
	ORDER BY [Transaction Date / Time], [Till Id], [Operator Id], [Paid In or Paid Out]
END
GO

