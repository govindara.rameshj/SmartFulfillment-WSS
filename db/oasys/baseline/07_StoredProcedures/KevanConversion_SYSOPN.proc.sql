﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SYSOPN]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SYSOPN'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SYSOPN] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SYSOPN'
GO
ALTER PROCEDURE dbo.KevanConversion_SYSOPN
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 37 - Setting System to Open
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Task     : 37/01 - Update SYSNID Information
----------------------------------------------------------------------------------- 

UPDATE SYSNID
Set MAST = '1', DATE1 = (Convert (date, GETDATE()))
where FKEY = '01'


-----------------------------------------------------------------------------------
-- Task     : 37/02 - Update SYSDAT Information
----------------------------------------------------------------------------------- 
Update SYSDAT
Set 
TMDT = (Convert (date, GETDATE())),
TMDW = (Select
		case 
			When (DATENAME(Weekday, GETDATE())) = 'Monday'	then '1'
			When (DATENAME(Weekday, GETDATE())) = 'Tuesday' then '2'
			When (DATENAME(Weekday, GETDATE())) = 'Wednesday' then '3'
			When (DATENAME(Weekday, GETDATE())) = 'Thursday' then '4'
			When (DATENAME(Weekday, GETDATE())) = 'Friday' then '5'
			When (DATENAME(Weekday, GETDATE())) = 'Saturday' then '6'
			When (DATENAME(Weekday, GETDATE())) = 'Sunday' then '7'			
		end as 'TMDW'
	),
TODT = (Convert (date, DATEADD(Day,-1,GETDATE()))),
TODW = (Select
		case 
			When (DATENAME(Weekday, DATEADD(Day,-1,GETDATE()))) = 'Monday'	then '1'
			When (DATENAME(Weekday, DATEADD(Day,-1,GETDATE()))) = 'Tuesday' then '2'
			When (DATENAME(Weekday, DATEADD(Day,-1,GETDATE()))) = 'Wednesday' then '3'
			When (DATENAME(Weekday, DATEADD(Day,-1,GETDATE()))) = 'Thursday' then '4'
			When (DATENAME(Weekday, DATEADD(Day,-1,GETDATE()))) = 'Friday' then '5'
			When (DATENAME(Weekday, DATEADD(Day,-1,GETDATE()))) = 'Saturday' then '6'
			When (DATENAME(Weekday, DATEADD(Day,-1,GETDATE()))) = 'Sunday' then '7'			
		end as 'TDTW'
	) 
Where FKEY = '01'


-----------------------------------------------------------------------------------
-- Task     : 37/03 - Set the Active Workstations
----------------------------------------------------------------------------------- 
UPDATE WorkstationConfig
Set IsActive = '1'
Where ID = '20' --or ID = '60'

END;
GO

