﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetDeliverySlots]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure GetDeliverySlots'
	EXEC ('CREATE PROCEDURE [dbo].[GetDeliverySlots] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure GetDeliverySlots'
GO
ALTER PROCEDURE [dbo].[GetDeliverySlots]
AS
BEGIN
    SET NOCOUNT ON
    select
        cast(p1.LongValue as int) [Id],
        p2.StringValue [Description],
        cast(p3.StringValue as time) [StartTime],
        cast(p4.StringValue as time) [EndTime]
    from dbo.Parameters p1
    inner join dbo.Parameters p2 on p2.ParameterID = 54
    inner join dbo.Parameters p3 on p3.ParameterID = 55
    inner join dbo.Parameters p4 on p4.ParameterID = 56
    where p1.ParameterID = 53

    union all

    select
        cast(p1.LongValue as int),
        p2.StringValue,
        cast(p3.StringValue as time),
        cast(p4.StringValue as time)
    from dbo.Parameters p1
    inner join dbo.Parameters p2 on p2.ParameterID = 58
    inner join dbo.Parameters p3 on p3.ParameterID = 59
    inner join dbo.Parameters p4 on p4.ParameterID = 60
    where p1.ParameterID = 57
END
GO

