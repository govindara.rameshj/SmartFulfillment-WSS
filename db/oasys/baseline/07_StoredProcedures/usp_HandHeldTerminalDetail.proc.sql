﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_HandHeldTerminalDetail]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_HandHeldTerminalDetail'
	EXEC ('CREATE PROCEDURE [dbo].[usp_HandHeldTerminalDetail] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_HandHeldTerminalDetail'
GO
-- =============================================
-- Author       : Partha Dutta
-- Create date  : 18/06/2011
-- Referral No  : 773
-- Description  : Baseline version
--                Retrieve HHT Details for selected date
-- =============================================

ALTER PROCedure dbo.usp_HandHeldTerminalDetail
   @SelectedDate date
as
set nocount on

select a.DATE1,
       a.SKUN,
       a.ONHA,
       a.IADJ,
       a.MDNQ,
       a.TCNT,
       a.TMDC,
       a.TPRE,
       a.LBOK,
       a.StockSold,
       --LocationCount = (select count(*)
       --                 from HHTLocation b
       --                 where DateCreated = a.DATE1
       --                 and   SkuNumber   = a.SKUN),
       StockExist = convert(bit, (case
                                     when b.SKUN is null then 0
                                     when b.SKUN is not null then 1
                                  end)),
       b.DESCR,
       b.SUPP,
       b.PROD,
       b.PRIC,
	   a.ORIG
from HHTDET a
left outer join STKMAS b
      on b.SKUN = a.SKUN
where DATE1 = @SelectedDate
GO

