﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeBagPersistComments]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeBagPersistComments'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeBagPersistComments] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeBagPersistComments'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 02/10/2012
-- User Story	 : 6239
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 7975
-- Description   : Create new stored procedure NewBankingSafeBagPersistComments
--				 : to update SafeBag entry's (identified by BagId parameter)
--				 : Comments field with Comments parameter.
-- =============================================
ALTER PROCedure [dbo].[NewBankingSafeBagPersistComments]
   @BagID int,
   @Comments Char(255)
As
Begin
	Set NoCount On

	Update
		SafeBags
	Set
		Comments = @Comments
	Where
		ID = @BagID
End
GO

