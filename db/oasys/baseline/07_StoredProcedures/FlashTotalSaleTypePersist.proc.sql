﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FlashTotalSaleTypePersist]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure FlashTotalSaleTypePersist'
	EXEC ('CREATE PROCEDURE [dbo].[FlashTotalSaleTypePersist] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure FlashTotalSaleTypePersist'
GO
ALTER PROCEDURE [dbo].[FlashTotalSaleTypePersist]
	@SaleType	char(2),
	@Total		dec(9,2)
AS
BEGIN
	SET NOCOUNT ON;

	declare @time char(4);
	set @time = CONVERT(char(2), datepart(hh, getdate())) + CONVERT(char(2), DATEPART(n, getdate()));

	if @SaleType='SA'
	begin
		update RSFLAS
		set NUMB1 = NUMB1 + 1, AMNT1 = AMNT1 + @Total, TIME1 = @time
		where FKEY = 1;		
	end

	if @SaleType='SC'
	begin
		update RSFLAS
		set NUMB2 = NUMB2 + 1, AMNT2 = AMNT2 + @Total, TIME2 = @time
		where FKEY = 1;		
	end

	if @SaleType='RF'
	begin
		update RSFLAS
		set NUMB3 = NUMB3 + 1, AMNT3 = AMNT3 + @Total, TIME3 = @time
		where FKEY = 1;		
	end
	
	if @SaleType='RC'
	begin
		update RSFLAS
		set NUMB4 = NUMB4 + 1, AMNT4 = AMNT4 + @Total, TIME4 = @time
		where FKEY = 1;		
	end
	
	if @SaleType='CO'
	begin
		update RSFLAS
		set NUMB5 = NUMB5 + 1, AMNT5 = AMNT5 + @Total, TIME5 = @time
		where FKEY = 1;		
	end
			
	if @SaleType='CC'
	begin
		update RSFLAS
		set NUMB6 = NUMB6 + 1, AMNT6 = AMNT6 + @Total, TIME6 = @time
		where FKEY = 1;		
	end

	if @SaleType in ('M+','C+')
	begin
		update RSFLAS
		set NUMB7 = NUMB7 + 1, AMNT7 = AMNT7 + @Total, TIME7 = @time
		where FKEY = 1;		
	end

	if @SaleType in ('M-','C-')
	begin
		update RSFLAS
		set NUMB8 = NUMB8 + 1, AMNT8 = AMNT8 + @Total, TIME8 = @time
		where FKEY = 1;		
	end
	
	if @SaleType='OD'
	begin
		update RSFLAS
		set NUMB9 = NUMB9 + 1, AMNT9 = AMNT9 + @Total, TIME9 = @time
		where FKEY = 1;		
	end	
	
	if @SaleType not in ('SA','SC','RF','RC','CO','CC','M+','C+','M-','C-','OD')
	begin
		update RSFLAS
		set NUMB13 = NUMB13 + 1, AMNT13 = AMNT13 + @Total, TIME13 = @time
		where FKEY = 1;		
	end			

END
GO

