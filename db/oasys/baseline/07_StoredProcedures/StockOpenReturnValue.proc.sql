﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockOpenReturnValue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockOpenReturnValue'
	EXEC ('CREATE PROCEDURE [dbo].[StockOpenReturnValue] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockOpenReturnValue'
GO
ALTER PROCEDURE [StockOpenReturnValue]
@Value DECIMAL (9, 2) OUTPUT
AS
SELECT @Value = SUM(STKMAS.RETV) FROM STKMAS;
	IF @Value IS NULL SELECT @Value =0;
GO

