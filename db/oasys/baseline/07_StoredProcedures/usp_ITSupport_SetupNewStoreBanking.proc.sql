﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_SetupNewStoreBanking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_SetupNewStoreBanking'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_SetupNewStoreBanking] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_SetupNewStoreBanking'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_SetupNewStoreBanking]
-----------------------------------------------------------------------------------
-- Version		: 1.0 
-- Revision		: 1.0
-- Author		: Kevan Madelin
-- Date			: 18th August 2011
-- Description	: Setup Store for New Banking Procedure
-----------------------------------------------------------------------------------
@sp_Commit Char(1),
@sp_Step int

As
Begin
Set NoCount On

If (@sp_Commit = 'Y')
	Begin
		-- Step 1 = PRE BANKING
		-- Step 2 = POST BANKING
		-----------------------------------------------------------------------------------
		-- Declare Working Variables
		-----------------------------------------------------------------------------------
		Declare @Period_Today int
		Declare @PeriodRec int = (Select LTRIM(RTRIM(Cast(LongValue as Int))) From [Parameters] Where ParameterID = '2200')  
		Declare @PeriodDat DateTime = (CAST(FLOOR(CAST(DateAdd(day, @PeriodRec*-1, GETDATE())AS FLOAT))AS DATETIME))
		Declare @Date_Today DateTime = (CAST(FLOOR(CAST(DateAdd(day, 0, GETDATE())AS FLOAT))AS DATETIME))

		-----------------------------------------------------------------------------------
		-- Process SETP 1
		-----------------------------------------------------------------------------------
		If @sp_Step = 1
			Begin
				-----------------------------------------------------------------------------------
				-- New Store First Day Safe Change into Safe
				-----------------------------------------------------------------------------------
				Print ('Retrieving Parameter (Banking Days Displayed): ' + LTRIM(RTRIM(Cast(@PeriodRec as Char))))
				Print ('Collecting System Period Id''s between ' + LTRIM(RTRIM(Cast(@PeriodDat as Char))) + ' until ' + LTRIM(RTRIM(Cast(@Date_Today as Char))) + '.')
				Print ('Checking if Processing Table Exists..')
				If Exists (Select LTRIM(RTRIM(Name)) From TempDB.Sys.objects Where name like ('#NewBank_Safe_%') and [TYPE] = 'U') 
					Begin
						Print ('Dropping Processing Table')
						Drop Table #NewBank_Safe
					End
				
				Print ('Creating Processing Table')		
				Create Table #NewBank_Safe([ID] [int] IDENTITY(1,1),[Period] int NOT NULL)
				Insert into #NewBank_Safe 
				Select Id From SystemPeriods Where StartDate >= @PeriodDat and EndDate < GETDATE() 
				
				Print ('Collecting Todays Period Id')
				Set @Period_Today = (Select Id From SystemPeriods Where StartDate = @Date_Today)
				Print ('Todays Period Id: ' + LTRIM(RTRIM(Cast(@Period_Today as Char))))
				
				-- Set Safe Periods As Closed (or Insert Missing Days)
				Print ('Starting Safe Update')
				Declare @NBFD_ID int
				Declare @NBFD_MAXID int
				Select  @NBFD_MAXID = MAX(ID)FROM #NewBank_Safe
				Set		@NBFD_ID = 1
				While	(@NBFD_ID <= @NBFD_MAXID)
					Begin
						Declare @ProcessPeriod int = (Select Period From #NewBank_Safe Where ID = @NBFD_ID)
						Declare @ProcessDate Date = (Select Convert(Date, Cast(StartDate as DateTime)) From SystemPeriods Where ID = @ProcessPeriod)
						Print('Processing Safe Period : ' + LTRIM(RTRIM(Cast(@ProcessPeriod as Char)))) 
						--Print('Processing Safe Date   : ' + LTRIM(RTRIM(Cast(@ProcessDate as Char)))) 
						If Exists (Select PeriodId From Safe Where PeriodId = @ProcessPeriod)
							Begin
								Update Safe 
								Set IsClosed = '1',
								UserID1 = '888',
								UserID2 = '888',
								SafeChecked = '1',
								EndOfDayCheckCompleted = @ProcessDate,
								EndOfDayCheckDone = '1',
								EndOfDayCheckManagerID = '888',
								EndOfDayCheckWitnessID = '888' 
								Where PeriodID = @ProcessPeriod
							End
						Else
							Begin
								Insert into Safe Values (@ProcessPeriod, @ProcessDate, '888', '888', GETDATE(), '1', '1', '1', @ProcessDate, '888', '888', NULL, NULL)
							End
						Set @NBFD_ID = @NBFD_ID+1
					End 
					
				-- Continue Safe Setup
				Print ('Updating Todays Banking as Safe Checked') 
				Update Safe Set SafeChecked = '1' Where PeriodID = @Period_Today
				
				-- Configuring Safe Denominations
				Print ('Configuring Safe Denominations')
				
				Update Parameters 
				Set DecimalValue = '0.01000' 
				Where ParameterID = 2202
				
				Update SystemCurrencyDen 
				Set BullionMultiple = '0.01', 
				BankingBagLimit = '10000.00' 
				Where ID = '0.01' and TenderID = 1 and DisplayText = '1p'
			
				If @@Error = 0 Print ('STEP 1 - Process Complete') Else Print ('Error STEP 1')
			End

		-----------------------------------------------------------------------------------
		-- Process SETP 2
		-----------------------------------------------------------------------------------
		If @sp_Step = 2
			Begin
				--Post Banking Safe Change
				Print ('Configuring Safe Denominations to Default')
				
				Update Parameters 
				Set DecimalValue = '2.00000' 
				Where ParameterID = 2202
				
				Update SystemCurrencyDen 
				Set BullionMultiple = '0.00', 
				BankingBagLimit = '0.00' 
				Where ID = '0.01' and TenderID = 1 and DisplayText = '1p'
				
				If @@Error = 0 Print ('STEP 2 - Process Complete') Else Print ('Error STEP 2')	
			End
	End
Else
	Begin
		Print ('Commit Not Enabled - DO NOT RUN THIS PROCEDURE ON LIVE STORE!!! Refer to Kevan Madelin for More Information..')
	End
End
GO

