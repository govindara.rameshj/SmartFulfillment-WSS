﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LogSOM_InvalidXmlDetailInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure LogSOM_InvalidXmlDetailInsert'
	EXEC ('CREATE PROCEDURE [dbo].[LogSOM_InvalidXmlDetailInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure LogSOM_InvalidXmlDetailInsert'
GO
ALTER PROCedure LogSOM_InvalidXmlDetailInsert

   @LogID         int,
   @FailureReason nvarchar(255)

as
begin
   set nocount on

   insert LogSOM_InvalidXmlDetail(LogID, FailureReason) values (@LogID, @FailureReason)

end
GO

