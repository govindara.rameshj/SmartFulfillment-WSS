﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PoGetLines]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure PoGetLines'
	EXEC ('CREATE PROCEDURE [dbo].[PoGetLines] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure PoGetLines'
GO
ALTER PROCEDURE [dbo].[PoGetLines]
	@OrderId	int
AS
BEGIN
	SET NOCOUNT ON;

	select		
		pl.tkey		as 'Id',
		pl.hkey		as 'OrderId',
		pl.skun		as 'SkuNumber',
		sk.descr	as 'SkuDescription',
		pl.prod		as 'SkuProductCode',
		sk.pack		as 'SkuPackSize',
		pl.qtyo		as 'OrderQty',
		pl.pric		as 'Price'
	from
		purlin pl
	inner join	
		stkmas sk on sk.skun = pl.skun
	where
			pl.hkey = @OrderId
		and	pl.dele = 0
	order by
		pl.tkey

END
GO

