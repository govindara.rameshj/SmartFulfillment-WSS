﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashSales]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashSales'
	EXEC ('CREATE PROCEDURE [dbo].[DashSales] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashSales'
GO
-- Version Control Information
-- =============================================
-- Author:  	Kevan Madelin
-- Create date: 23/08/2010
-- Version:	3.0.0.0
-- Description: This procedure is used to get the sales data for the Managers Dashboard.
-- =============================================
-- =============================================
-- Author:  	Partha Dutta
-- Create date: 29/09/2010
-- Description: Percentage statistics showing pound signs
-- =============================================
-- Author    : Partha
-- Alter Date: 15/10/2010
-- Version   : 3.0.1.0
-- Notes     : Some calculations are wrong because of NULL values not being dealth with correctly
-- =============================================
-- Author    : AlanL
-- Alter Date: 03/11/2010
-- Version   : 3.0.1.1
-- Notes     : Set field 'Null' values to 0 so they get displayed on report as 0 rather than an empty cell.
--           : This moves isnull checking to earlier is proc, so removed from later.
--           : Corrected KB calc. sums so use 0 when field is null rather than the field value, i.e. NULL
--           : value (ie when and else calc.s were wrong way round.
--			 : Correct 'Core Sales + K&B Sales' entries for week values - was calculating CoreWeek... then
--           : adding/subtracting ..Day... values instead of ...Week... values.
--           : Use DLLINE.IBAR = 1 for scanned weekly calc, as per the scanned daily calc.
-- =============================================
-- Author    : Kevan Madelin
-- Alter Date: 16/11/2010
-- Version   : 3.0.1.2
-- Notes     : Changed K&B Sales to look at new Vision Tables.
-- =============================================
-- Author    : Kevan Madelin
-- Alter Date: 24/11/2010
-- Version   : 3.0.1.3
-- Notes     : Added new K&B Deposits to look at DLTOTS Table.
-- =============================================
ALTER PROCEDURE [dbo].[DashSales]
	@DateEnd	date
as
begin
    set nocount on

    declare @table table([Description]        varchar(50),
                         Qty                  int,
                         QtyWtd	              int,
                         Value                dec(9,2),
                         ValueWtd             dec(9,2),
                         PercentageSalesValue dec(9,2),
		                 PercentageWtdValue   dec(9,2));
    declare @StartDate        date,
            @CoreDayQty	      dec(9,2),
            @CoreDayValue     dec(9,2),
            @CoreWeekQty      dec(9,2),		
            @CoreWeekValue    dec(9,2),
            @RefundDayQty     dec(9,2),
            @RefundDayValue	  dec(9,2),
            @RefundWeekQty    dec(9,2),
            @RefundWeekValue  dec(9,2),
            @VoucherDayQty    int,
            @VoucherDayValue  dec(9,2),
            @VoucherWeekQty	  int,
            @VoucherWeekValue dec(9,2),
            @KbDayQty         int,
            @KbDayValue	      dec(9,2),
            @KbWeekQty        int,
            @KbWeekValue      dec(9,2),
            @LinesDay         dec(9,2),
            @LinesWeek        dec(9,2),
            @ScannedDay	      dec(9,2),
            @ScannedWeek      dec(9,2),
            @KbDepDayQty      int,
            @KbDepDayValue	  dec(9,2),
            @KbDepWeekQty     int,
            @KbDepWeekValue   dec(9,2);
	
    --set startdate to sunday before enddate
	set	@StartDate = @DateEnd;
	While datepart(weekday, @StartDate) <> 1
	begin
		set @StartDate = dateadd(day, -1, @StartDate)
	end	
	
	--get core sales/refunds today info
	select
		@CoreDayQty		= count(case dl.tcod when 'SA' then dl.totl end),	
	 	@CoreDayValue	= sum(case dl.tcod when 'SA' then dl.totl else 0 end),
	 	@RefundDayQty	= count(case dl.tcod when 'RF' then dl.totl end),	
	 	@RefundDayValue	= sum(case dl.tcod when 'RF' then dl.totl else 0 end)
	from	
		dltots dl
	where	
		dl.DATE1	= @DateEnd
	and	dl.CASH		<> '000'
	and	dl.VOID		= 0
	and	dl.PARK		= 0
	and	dl.TMOD		= 0;
	-- Make sure display 0 if no values (null).  Also makes calculations easier later
	set @CoreDayValue = isnull(@CoreDayValue , 0);
	set @RefundDayValue = isnull(@RefundDayValue, 0);
	
	--get core sales/refunds week to date info
	select
		@CoreWeekQty		= count(case dl.tcod when 'SA' then dl.totl end),	
	 	@CoreWeekValue		= sum(case dl.tcod when 'SA' then dl.totl else 0 end),
	 	@RefundWeekQty		= count(case dl.tcod when 'RF' then dl.totl end),	
	 	@RefundWeekValue	= sum(case dl.tcod when 'RF' then dl.totl else 0 end)
	from	
		dltots dl
	where	
		dl.DATE1	<= @DateEnd
	and dl.date1	>= @StartDate
	and	dl.CASH		<> '000'
	and	dl.VOID		= 0
	and	dl.PARK		= 0
	and	dl.TMOD		= 0;
	-- Make sure display 0 if no values (null).  Also makes calculations easier later
	set @CoreWeekValue = isnull(@CoreWeekValue, 0);
	set @RefundWeekValue = isnull(@RefundWeekValue, 0);
	
	--get vouchers today info
	select
		@VoucherDayQty		= count(dp.amnt),	
	 	@VoucherDayValue	= sum(dp.amnt * -1)
	from	
		dltots dl
	inner join
		dlpaid dp	on	dp.date1 = dl.date1 
					and	dp.till = dl.till
					and	dp.[tran] = dl.[tran]
					and dp.[type] = 6	
	where	
		dl.DATE1	= @DateEnd
		and	dl.CASH		<> '000'
		and	dl.VOID		= 0
		and	dl.PARK		= 0
		and	dl.TMOD		= 0
	-- Make sure display 0 if no values (null).  Also makes calculations easier later
	set @VoucherDayValue = isnull(@VoucherDayValue, 0);

	--get vouchers week to date info
	select
		@VoucherWeekQty		= count(dp.amnt),	
	 	@VoucherWeekValue	= sum(dp.amnt * -1)
	from	
		dltots dl
	inner join
		dlpaid dp	on	dp.date1 = dl.date1 
					and	dp.till = dl.till
					and	dp.[tran] = dl.[tran]
					and dp.[type] = 6
	where	
		dl.DATE1	<= @DateEnd
	and dl.date1	>= @StartDate
	and	dl.CASH		<> '000'
	and	dl.VOID		= 0
	and	dl.PARK		= 0
	and	dl.TMOD		= 0
	-- Make sure display 0 if no values (null).  Also makes calculations easier later
	set @VoucherWeekValue = isnull(@VoucherWeekValue, 0);

    insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Core Sales',                     @CoreDayQty, @CoreWeekQty, @CoreDayValue, @CoreWeekValue) ;
    insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Core Refunds',                   @RefundDayQty, @RefundWeekQty, @RefundDayValue, @RefundWeekValue) ;
    insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Voucher Value',                  @VoucherDayQty, @VoucherWeekQty, @VoucherDayValue, @VoucherWeekValue) ;
    insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Core Sales + Refund - Vouchers', @CoreDayQty    + @RefundDayQty    - @VoucherDayQty,
                                                                                                              @CoreWeekQty   + @RefundWeekQty   - @VoucherWeekQty,
                                                                                                              @CoreDayValue  + @RefundDayValue  - @VoucherDayValue,
                                                                                                              @CoreWeekValue + @RefundWeekValue - @VoucherWeekQty);

	--get K&B today values from dashboard data table
	select	
		@KbDayQty		= count(vp.ValueTender),
		@KbDayValue		= sum(vp.ValueTender * -1)
	from 
		VisionPayment as vp 
	where 
		TranDate = @DateEnd;

	--get K&B week values from dashboard data table
	select	
		@KbWeekQty		= count(vp.ValueTender),
		@KbWeekValue	= sum(vp.ValueTender * -1)
	from 
		VisionPayment as vp 
	where 
		TranDate >= @StartDate
		and TranDate <= @DateEnd;
	
	--get K&B DEPOSITS today values from dashboard data table
	select	
		@KbDepDayQty		= count(dt.TOTL),
		@KbDepDayValue		= sum(dt.TOTL)
	from 
		DLTOTS as dt 
	where 
		DATE1 = @DateEnd and (TCOD = 'M+' or TCOD = 'M-') and MISC = '20';

	--get K&B DEPOSITS week values from dashboard data table
	select	
		@KbDepWeekQty		= count(dt.TOTL),
		@KbDepWeekValue		= sum(dt.TOTL)
	from 
		DLTOTS as dt 
	where 
		DATE1 >= @DateEnd and (TCOD = 'M+' or TCOD = 'M-') and MISC = '20'
		and DATE1 <= @DateEnd;
	
	
	-- Make sure display 0 if no values (null).  Also makes calculations easier later
	set @KbDayQty = isnull(@KbDayQty, 0);
	set @KbDayValue = isnull(@KbDayValue, 0);
	set @KbWeekQty = isnull(@KbWeekQty, 0);
	set @KbWeekValue = isnull(@KbWeekValue, 0);
	set @KbDepDayQty = isnull(@KbDepDayQty, 0);
	set @KbDepDayValue = isnull(@KbDepDayValue, 0);
	set @KbDepWeekQty = isnull(@KbDepWeekQty, 0);
	set @KbDepWeekValue = isnull(@KbDepWeekValue, 0);
	
	insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('K&B Sales', @KbDayQty, @KbWeekQty, @KbDayValue, @KbWeekValue) ;
	insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('K&B Deposits', @KbDepDayQty, @KbDepWeekQty, @KbDepDayValue, @KbDepWeekValue) ;
	--update the totals
	insert into @table([Description], Qty, QtyWtd, Value, ValueWtd) values ('Core Sales + K&B Sales', @CoreDayQty    + @RefundDayQty   -  @VoucherDayQty    + @KbDayQty,
                                                                                                      @CoreWeekQty   + @RefundWeekQty  -  @VoucherWeekQty   + @KbWeekQty,
                                                                                                      @CoreDayValue  + @RefundDayValue -  @VoucherDayValue  + @KbDayValue,
                                                                                                      @CoreWeekValue + @RefundWeekValue - @VoucherWeekValue + @KbWeekValue) ;
	
	--get total number/scanned lines today
	select 
		@LinesDay	= coalesce(count(dl.date1),0),
		@ScannedDay	= count(case when dl.ibar=1 then dl.date1 end)
	from	
		dltots dt
	inner join
		dlline dl	on	dl.date1	= dt.date1 
					and	dl.till		= dt.till
					and	dl.[tran]	= dt.[tran]
					and dl.lrev		= 0
	where	
		dt.DATE1	= @DateEnd
		and	dt.CASH		<> '000'
		and	dt.VOID		= 0
		and	dt.PARK		= 0
		and	dt.TMOD		= 0

	--get total number/scanned lines week
	select 
		@LinesWeek		= coalesce(count(dl.date1),0),
		@ScannedWeek	= count(case when dl.ibar=1 then dl.date1 end)
	from	
		dltots dt
	inner join
		dlline dl	on	dl.date1	= dt.date1 
					and	dl.till		= dt.till
					and	dl.[tran]	= dt.[tran]
					and dl.lrev		= 0
	where	
		dt.DATE1	<= @DateEnd
		and dt.date1	>= @StartDate
		and	dt.CASH		<> '000'
		and	dt.VOID		= 0
		and	dt.PARK		= 0
		and	dt.TMOD		= 0

	--calculate scanned %'s allowing for divide by zero
	if @LinesDay <> 0 set @LinesDay = (@ScannedDay / @LinesDay) * 100
	if @LinesWeek <> 0 set @LinesWeek = (@ScannedWeek /  @LinesWeek) * 100
	
	--calculate refund %'s avoiding divide by zero
	if @CoreDayQty <> 0 set @CoreDayValue = (@RefundDayQty / @CoreDayQty) * 100;	
	if @CoreWeekQty <> 0 set @CoreWeekValue = (@RefundWeekQty / @CoreWeekQty) * 100;
		
	insert into @table(Description, PercentageSalesValue, PercentageWtdValue) values ('Percentage Lines Scanned', @LinesDay, @LinesWeek) ;
	insert into @table(Description, PercentageSalesValue, PercentageWtdValue) values ('Percentage Refunds to Sales', @CoreDayValue, @CoreWeekValue) ;

	select * from @table
		
end
GO

