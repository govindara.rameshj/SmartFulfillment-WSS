﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockUpdateSoqInit]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockUpdateSoqInit'
	EXEC ('CREATE PROCEDURE [dbo].[StockUpdateSoqInit] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockUpdateSoqInit'
GO
-- ===============================================================================
-- Author         : Dhanesh Ramachandran
-- Date	          : 05/10/2011
-- Change Request : RF0875
-- TFS User Story : 2755
-- TFS Task ID    : 2756
-- Description    : Modified the stored procedure to avoid rolling over of unitsales
--                  and daysoutofstock
-- ===============================================================================

ALTER PROCEDURE [dbo].[StockUpdateSoqInit]
	@SkuNumber			char(6),
	@DemandPattern		char(11),
	@PeriodDemand		dec(9,2),
	@PeriodTrend		dec(9,2),
	@ErrorForecast		dec(9,2),
	@ErrorSmoothed		dec(9,2),
	@BufferConversion	dec(9,2),
	@BufferStock		int,
	@OrderLevel			int,
	@ValueAnnualUsage	char(2),
	@FlierPeriod1		char(1),
	@FlierPeriod2		char(1),
	@FlierPeriod3		char(1),
	@FlierPeriod4		char(1),
	@FlierPeriod5		char(1),
	@FlierPeriod6		char(1),
	@FlierPeriod7		char(1),
	@FlierPeriod8		char(1),
	@FlierPeriod9		char(1),
	@FlierPeriod10		char(1),
	@FlierPeriod11		char(1),
	@FlierPeriod12		char(1)
AS
BEGIN
	SET NOCOUNT ON;

	declare	@ToForecast	bit;
	set		@ToForecast	= (select ToForecast from stkmas where skun=@SkuNumber);

	update
		stkmas 
	set
		IsInitialised = 1,
		DateLastSoqInit	= getdate(),
		DemandPattern	= @DemandPattern,
		PeriodDemand	= @PeriodDemand,
		PeriodTrend		= @PeriodTrend,
		ErrorForecast	= @ErrorForecast,
		ErrorSmoothed	= @ErrorSmoothed,
		BufferConversion = @BufferConversion,
		BufferStock		= @BufferStock,
		OrderLevel		= @OrderLevel,
		ValueAnnualUsage = @ValueAnnualUsage,
		FlierPeriod1	= @FlierPeriod1,
		FlierPeriod2	= @FlierPeriod2,
		FlierPeriod3	= @FlierPeriod3,
		FlierPeriod4	= @FlierPeriod4,
		FlierPeriod5	= @FlierPeriod5,
		FlierPeriod6	= @FlierPeriod6,
		FlierPeriod7	= @FlierPeriod7,
		FlierPeriod8	= @FlierPeriod8,
		FlierPeriod9	= @FlierPeriod9,
		FlierPeriod10	= @FlierPeriod10,
		FlierPeriod11	= @FlierPeriod11,
		FlierPeriod12	= @FlierPeriod12
	where
		skun = @SkuNumber;
		
	--check if need to roll on periods due to forecasting	
	if @ToForecast = 1
	begin
		update
			stkmas
		set
			ToForecast	= 0,
			FlierPeriod12	= FlierPeriod11,
			FlierPeriod11	= FlierPeriod10,
			FlierPeriod10	= FlierPeriod9,
			FlierPeriod9	= FlierPeriod8,
			FlierPeriod8	= FlierPeriod7,
			FlierPeriod7	= FlierPeriod6,
			FlierPeriod6	= FlierPeriod5,
			FlierPeriod5	= FlierPeriod4,
			FlierPeriod4	= FlierPeriod3,
			FlierPeriod3	= FlierPeriod2,
			FlierPeriod2	= FlierPeriod1,
			FlierPeriod1	= null
		where
			skun = @SkuNumber
	end
	
END
GO

