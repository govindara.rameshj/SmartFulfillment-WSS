﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SOQREV]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SOQREV'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SOQREV] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SOQREV'
GO
ALTER PROCEDURE dbo.KevanConversion_SOQREV
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.1
-- Author   : Kevan Madelin
-- Date	    : 17th July 2011
-- 
-- Task     : 04 - Update the Supplier Ordering 'Review' Days
-----------------------------------------------------------------------------------
-- Revision Information
--
-- 1.1 KM : Amended to update SKUs which do not have an Initial Ordering Date
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

Print ('Setting Ordering Review Flags')
UPDATE SUPDET SET ReviewDay0=1 WHERE SOQF>= 64;
UPDATE SUPDET SET SOQF = SOQF-64 WHERE SOQF>= 64;
UPDATE SUPDET SET ReviewDay6=1 WHERE SOQF>= 32;
UPDATE SUPDET SET SOQF = SOQF-32 WHERE SOQF>= 32;
UPDATE SUPDET SET ReviewDay5=1 WHERE SOQF>= 16;
UPDATE SUPDET SET SOQF = SOQF-16 WHERE SOQF>= 16;
UPDATE SUPDET SET ReviewDay4=1 WHERE SOQF>= 8;
UPDATE SUPDET SET SOQF = SOQF-8 WHERE SOQF>= 8;
UPDATE SUPDET SET ReviewDay3=1 WHERE SOQF>= 4;
UPDATE SUPDET SET SOQF = SOQF-4 WHERE SOQF>= 4;
UPDATE SUPDET SET ReviewDay2=1 WHERE SOQF>= 2;
UPDATE SUPDET SET SOQF = SOQF-2 WHERE SOQF>= 2;
UPDATE SUPDET SET ReviewDay1=1 WHERE SOQF>= 1;
UPDATE SUPDET SET SOQF = SOQF-1 WHERE SOQF>= 1;


Print ('Resetting STKMAS SKU''s with no Initial Ordering Date')
UPDATE STKMAS SET IODT = (SELECT (Convert (Date, DATEADD(Day,-1,GETDATE())))) Where (IODT is NULL)

END;
GO

