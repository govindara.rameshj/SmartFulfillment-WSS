﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemPeriodGet]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemPeriodGet'
	EXEC ('CREATE PROCEDURE [dbo].[SystemPeriodGet] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemPeriodGet'
GO
ALTER PROCEDURE [dbo].[SystemPeriodGet]
@Id INT=null, @StartDate DATE=null, @EndDate DATE=null, @IsClosed BIT=null
AS
begin
	exec SystemPeriodAutoPopulate;
	
	select 
		sp.ID,
		sp.StartDate,
		sp.EndDate,
		sp.IsClosed
	from 
		SystemPeriods sp
	where 
		(@Id is null or (@Id is not null and sp.ID=@Id)) and
		(@StartDate is null or (@StartDate is not null and sp.StartDate >= @StartDate )) and
		(@EndDate is null or (@EndDate is not null and sp.EndDate <= @EndDate)) and
		(@IsClosed is null or (@IsClosed is not null and sp.IsClosed=@IsClosed))
	order by
		id desc
end
GO

