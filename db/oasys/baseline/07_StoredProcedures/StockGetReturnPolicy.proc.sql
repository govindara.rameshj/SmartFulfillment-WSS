﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockGetReturnPolicy]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockGetReturnPolicy'
	EXEC ('CREATE PROCEDURE [dbo].[StockGetReturnPolicy] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockGetReturnPolicy'
GO
ALTER PROCEDURE [dbo].[StockGetReturnPolicy]
    @SupplierNumber char(5)
AS
BEGIN
    SET NOCOUNT ON;
    select
        ReturnPolicyDescription = [TEXT]
    from
        SUPNOT
	where
        SUPN     = @SupplierNumber
        and TYPE = '001'
    order by
        SEQN
END
GO

