﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGetReportColumns]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReportGetReportColumns'
	EXEC ('CREATE PROCEDURE [dbo].[ReportGetReportColumns] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReportGetReportColumns'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 23/07/2012
-- User Story	 : 6125
-- Project		 : P022-003 - Stock Enquiry Details - change the background colours of
--				 : specific areas based on data conditions.
-- Task Id		 : 6338
-- Description   : Allow for new fields called IsHighlight, HighlightColour and Fontsize
--				 : in the ReportColumns table
-- =============================================
ALTER PROCedure [dbo].[ReportGetReportColumns]
	@ReportId	int,
	@TableId	int
As
Begin
	Set NOCOUNT On;

	Select
		rc.Id,
		rc.Name,
		rc.Caption,
		rc.FormatType,
		rc.Format,
		rc.IsImagePath,
		rc.MinWidth,
		rc.MaxWidth,
		rc.Alignment,
		rcs.IsVisible,
		rcs.IsBold,
		rcs.IsHighlight,
		rcs.HighlightColour,
		rcs.Fontsize
	From
		ReportColumn rc
			Inner join
				ReportColumns rcs	
			On 
				rcs.ColumnId = rc.Id
	Where
		rcs.ReportId = @ReportId
	And 
		rcs.TableId	 = @TableId
	Order By
		rcs.Sequence
End
GO

