﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ActivityLogEnded]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ActivityLogEnded'
	EXEC ('CREATE PROCEDURE [dbo].[ActivityLogEnded] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ActivityLogEnded'
GO
ALTER PROCEDURE [dbo].[ActivityLogEnded]
@Id INT
AS
begin
	set nocount on;
	
	declare @rowCount int;
	set @rowCount=0;
	
	update
		ActivityLog
	set
		AppEnd	= GETDATE(),
		EndTime = REPLACE(CONVERT(varchar, getdate(), 108), ':', '')
	where
		ID=@Id
	
	set @rowCount = @@ROWCOUNT;
	return @rowcount;
	
end
GO

