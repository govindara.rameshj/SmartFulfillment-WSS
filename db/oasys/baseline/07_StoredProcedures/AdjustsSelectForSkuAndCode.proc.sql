﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AdjustsSelectForSkuAndCode]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure AdjustsSelectForSkuAndCode'
	EXEC ('CREATE PROCEDURE [dbo].[AdjustsSelectForSkuAndCode] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure AdjustsSelectForSkuAndCode'
GO
ALTER PROCEDURE [dbo].[AdjustsSelectForSkuAndCode]
@skuNumber CHAR (6), @code CHAR (2), @date DATE=null
AS
begin
	SELECT		
		STKADJ.PeriodID,
		STKADJ.CODE,
		STKADJ.DATE1,			
		STKADJ.SKUN, 
		STKADJ.SEQN,
		STKADJ.AmendId,
		STKADJ.TSKU,
		STKADJ.TransferStart,
		STKADJ.TransferPrice,
		STKADJ.[INIT],
		STKADJ.SSTK,
		STKADJ.QUAN,
		STKADJ.SSTK + STKADJ.QUAN AS EndStock,		
		STKADJ.PRIC,
		STKADJ.COST,
		STKADJ.INFO,
		STKADJ.[TYPE],
		STKADJ.DRLN,
		STKADJ.IsReversed,
		STKADJ.MOWT,
		STKADJ.WAUT,
		STKADJ.DAUT,
		STKADJ.ParentSeq, -- Modifed by Dhanesh Ramachandran 15/12/2010 for Stock Adjustment
		STKADJ.RTI
	FROM		
		STKADJ
	WHERE
		STKADJ.CODE = @code
		AND	STKADJ.SKUN = @skuNumber
		and	(@date is null or (@date is not null and STKADJ.DATE1=@date))	
		and	STKADJ.AmendId=0
	ORDER BY	
		STKADJ.DATE1 DESC, STKADJ.SKUN, STKADJ.SEQN;

	SELECT		
		STKADJ.PeriodID,
		STKADJ.CODE,
		STKADJ.SKUN, 
		STKADJ.SEQN,
		STKADJ.AmendId,
		STKADJ.TSKU,
		STKADJ.TransferStart,
		STKADJ.TransferPrice,
		STKADJ.DATE1,
		STKADJ.[INIT],
		STKADJ.SSTK,
		STKADJ.QUAN,
		STKADJ.SSTK + STKADJ.QUAN AS EndStock,		
		STKADJ.PRIC,
		STKADJ.COST,
		STKADJ.INFO,
		STKADJ.[TYPE],
		STKADJ.DRLN,
		STKADJ.IsReversed,
		STKADJ.MOWT,
		STKADJ.WAUT,
		STKADJ.DAUT,
		STKADJ.ParentSeq, -- Modifed by Dhanesh Ramachandran 15/12/2010 for Stock Adjustment
		STKADJ.RTI
	FROM		
		STKADJ
	WHERE		
		STKADJ.CODE = @code
		AND	STKADJ.SKUN = @skuNumber
		and	(@date is null or (@date is not null and STKADJ.DATE1=@date))	
		and	STKADJ.AmendId<>0
	ORDER BY	
		STKADJ.DATE1 DESC, STKADJ.SKUN, STKADJ.SEQN;

end
GO

