﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_SaleCouponIsReuseable]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_SaleCouponIsReuseable'
	EXEC ('CREATE PROCEDURE [dbo].[usp_SaleCouponIsReuseable] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_SaleCouponIsReuseable'
GO
ALTER PROCedure [dbo].[usp_SaleCouponIsReuseable]
	@CouponId CHAR (7)
	
As
Begin
	Set NOCOUNT ON;

Select 
	cm.[REUSABLE]
From
	[COUPONMASTER] cm
Where
	cm.[COUPONID]= @CouponId
End
GO

