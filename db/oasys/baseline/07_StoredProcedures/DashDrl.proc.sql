﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DashDrl]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure DashDrl'
	EXEC ('CREATE PROCEDURE [dbo].[DashDrl] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure DashDrl'
GO
ALTER PROCedure DashDrl
   @DateEnd   date
as
begin
   set nocount on

   declare @Output table(
                         RowId Int,
                         [Description]       varchar(50),
                         TodayQuantity       int,
                         WeekToTodayQuantity int,
                         TodayValue          dec(9,2),
                         WeekToTodayValue    dec(9,2))

   declare @StartDate                  date,

           @DrlTodayQuantity           int,
           @DrlWeekToTodayQuantity     int,      
           @DrlTodayValue              dec(9, 2),
           @DrlWeekToTodayValue        dec(9, 2),

           @ReceiptTodayQuantity       int,
           @ReceiptWeekToTodayQuantity int,      
           @ReceiptTodayValue          dec(9, 2),      
           @ReceiptWeekToTodayValue    dec(9, 2),  

           @ConsignedDayQty            int,
           @ConsignedWeekQty           int

   set @StartDate = @DateEnd
   while datepart(weekday, @StartDate) <> 1
   begin
      set @StartDate = dateadd(day, -1, @StartDate)
   end   

   select @DrlTodayQuantity = count(VALU),
          @DrlTodayValue    = (select sum(VALU)
                               from DRLSUM 
                               where [TYPE] in ('0', '1')
                               and   DATE1   = @DateEnd) - (select sum(VALU)
                                                            from DRLSUM 
                                                            where [TYPE] in ('2', '3')
                                                            and   DATE1   = @DateEnd)
   from DRLSUM
   where DATE1 = @DateEnd
   
   select @DrlWeekToTodayQuantity = count(VALU),
          @DrlWeekToTodayValue    = (select sum(VALU)
                                     from DRLSUM 
                                     where [TYPE] in ('0', '1')
                                     and   DATE1  <= @DateEnd
                                     and   DATE1  >= @StartDate) - (select sum(VALU)
                                                                    from DRLSUM 
                                                                    where [TYPE] in ('2', '3')
                                                                    and   DATE1  <= @DateEnd
                                                                    and   DATE1  >= @StartDate)
   from DRLSUM
   where DATE1 <= @DateEnd
   and   DATE1 >= @StartDate

   select @ReceiptTodayQuantity = sum(QUAN),   
          @ReceiptTodayValue    = sum(QUAN * PRIC)
   from STKADJ
   where DATE1 = convert(date, @DateEnd)
   and   CODE  = '04'
   
   select @ReceiptWeekToTodayQuantity = sum(QUAN),   
          @ReceiptWeekToTodayValue    = sum(QUAN * PRIC)
   from STKADJ
   where DATE1 <= @DateEnd
   and   DATE1 >= @StartDate      
   and   CODE  = '04'

   set @DrlTodayQuantity           = coalesce(@DrlTodayQuantity, 0)
   set @DrlWeekToTodayQuantity     = coalesce(@DrlWeekToTodayQuantity, 0)
   set @DrlTodayValue              = coalesce(@DrlTodayValue, 0)
   set @DrlWeekToTodayValue        = coalesce(@DrlWeekToTodayValue, 0)

   set @ReceiptTodayQuantity       = coalesce(@ReceiptTodayQuantity, 0)
   set @ReceiptWeekToTodayQuantity = coalesce(@ReceiptWeekToTodayQuantity, 0)
   set @ReceiptTodayValue          = coalesce(@ReceiptTodayValue, 0)
   set @ReceiptWeekToTodayValue    = coalesce(@ReceiptWeekToTodayValue, 0)

   set @ConsignedDayQty  = dbo.svf_ReportConsignmentsNotReceived()
   set @ConsignedWeekQty = dbo.svf_ReportConsignmentsNotReceived()

   insert into @Output values (1,'DRL Value',                         @DrlTodayQuantity,     @DrlWeekToTodayQuantity,     @DrlTodayValue,     @DrlWeekToTodayValue)
   insert into @Output values (2,'Code 4 Receipt Adjustments',               @ReceiptTodayQuantity, @ReceiptWeekToTodayQuantity, @ReceiptTodayValue, @ReceiptWeekToTodayValue)
   insert into @Output values (3,'Outstanding Direct Deliveries',     null,                  null,                        null,               null)
   insert into @Output values (4,'Direct Deliveries Overdue',         null,                  null,                        null,               null)
   insert into @Output values (5,'Orders Consigned but not Received', @ConsignedDayQty,      @ConsignedWeekQty,           null,               null)

   select 
          RowId,
          [Description],
          Qty      = TodayQuantity,
          QtyWtd   = WeekToTodayQuantity,
          Today    = TodayValue,
          ValueWtd = WeekToTodayValue
   from @Output

end
GO

