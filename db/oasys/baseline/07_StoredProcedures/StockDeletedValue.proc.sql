﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockDeletedValue]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockDeletedValue'
	EXEC ('CREATE PROCEDURE [dbo].[StockDeletedValue] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockDeletedValue'
GO
ALTER PROCEDURE [dbo].[StockDeletedValue]
	@Value DECIMAL (9, 2) OUTPUT
AS
begin
	SELECT 
		@Value	= SUM(S.ONHA * S.PRIC) 
	FROM 
		STKMAS S 
	WHERE	
		S.IOBS = 1;
		
	SELECT @Value = coalesce(@Value, 0);
	
end
GO

