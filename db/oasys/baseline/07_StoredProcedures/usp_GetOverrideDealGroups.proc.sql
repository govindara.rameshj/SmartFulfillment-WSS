﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetOverrideDealGroups]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetOverrideDealGroups'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetOverrideDealGroups] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetOverrideDealGroups'
GO
-- ===============================================================================
-- Author         : Dhanesh Ramachandran
-- Date	          : 03/01/2012
-- Project        : PO14-04
-- TFS User Story : 2155
-- TFS Task ID    : 3820
-- Description    : Rollout Script for Override Deal Groups. Pero field should be
--                  added to eventdetailoverride table
-- ===============================================================================
ALTER PROCedure usp_GetOverrideDealGroups

    @SelectedDate date,
    @SkuNumbers   varchar(max)

as
begin
   set @SkuNumbers = replace(@SkuNumbers, ' ', '') 
                  
   --deal group with latest revision
 --select EventNumber       = dt.ID,
 --       DealGroupNumber   = EventHeaderOverrideID,
   select EventNumber       = replicate('0', 6 - len(replace(cast(900000 + hd.ID as char(6)), ' ', ''))) + replace(cast(900000 + hd.ID as char(6)), ' ', ''),
          DealGroupNumber   = replicate('0', 6 - len(replace(cast(900000 + hd.ID as char(6)), ' ', ''))) + replace(cast(900000 + hd.ID as char(6)), ' ', ''),
          DealType          = cast('S' as char(1)),
          ItemKey           = dt.SkuNumber,
          Deleted           = cast(0 as bit),
          dt.Quantity, 
          ErosionValue      = cast(0 as decimal(9, 2)),
          ErosionPercentage = cast(0 as decimal(7, 3)),
          SellingPrice      = coalesce(dt.SkuActualSellingPrice, dt.SkuNormalSellingPrice)          
  from EventHeaderOverride hd
  inner join EventDetailOverride dt
        on dt.EventHeaderOverrideID = hd.ID

  ----work out header id based max revsion and EXACT MATCH
  --where hd.ID = (
  --               select top 1 max(hd.ID)
  --               from EventHeaderOverride hd 
  --               inner join EventDetailOverride dt
  --                     on dt.EventHeaderOverrideID = hd.ID
  --               where hd.EventTypeID = 3
  --               and   hd.StartDate  <= @SelectedDate
  --               and   hd.EndDate    >= @SelectedDate
  --               and   (select count(*)
  --                      from (select SkuNumber = Item
  --                            from udf_splitvarchartotable(@SkuNumbers, ',')
  --                            except
  --                            select SkuNumber
  --                            from EventDetailOverride
  --                            where EventHeaderOverrideID = dt.EventHeaderOverrideID) a) = 0
  --               and   (select count(*)
  --                      from (select SkuNumber
  --                            from EventDetailOverride
  --                            where EventHeaderOverrideID = dt.EventHeaderOverrideID            
  --                            except
  --                            select SkuNumber = Item
  --                            from udf_splitvarchartotable(@SkuNumbers, ',')) a) = 0
  --               group by hd.Revision
  --               order by hd.Revision desc
  --              )

  --work out header id based max revsion and any sku match
  where hd.ID in (
                  select a.ID
                  from (
                        select ID, Revision, SkuList = dbo.udf_EventDetailOverrideSkuDelimitedList(ID)
                        from EventHeaderOverride
                        where EventTypeID = 3
                        and   StartDate  <= @SelectedDate
                        and   EndDate    >= @SelectedDate
                        and   ID         in (
                                             select EventHeaderOverrideID
                                             from EventDetailOverride
                                             where SkuNumber in (select SkuNumber = Item from udf_splitvarchartotable(@SkuNumbers, ','))
                                            )
                       ) a
                  inner join (
                              select SkuList, MaxRevision = max(Revision)
                              from (
                                    select ID, Revision, SkuList = dbo.udf_EventDetailOverrideSkuDelimitedList(ID)
                                    from EventHeaderOverride
                                    where EventTypeID = 3
                                    and   StartDate  <= @SelectedDate
                                    and   EndDate    >= @SelectedDate
                                    and   ID         in (
                                                         select EventHeaderOverrideID
                                                         from EventDetailOverride
                                                         where SkuNumber in (select SkuNumber = Item from udf_splitvarchartotable(@SkuNumbers, ','))
                                                        )
                                   ) a
                              group by SkuList
                             ) b
                        on  b.SkuList     = a.SkuList
                        and b.MaxRevision = a.Revision
                 )
end
GO

