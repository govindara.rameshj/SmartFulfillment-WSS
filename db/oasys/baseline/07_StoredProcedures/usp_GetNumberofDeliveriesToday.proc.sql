﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetNumberofDeliveriesToday]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetNumberofDeliveriesToday'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetNumberofDeliveriesToday] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetNumberofDeliveriesToday'
GO
ALTER PROCEDURE [dbo].[usp_GetNumberofDeliveriesToday] 
	-- Add the parameters for the stored procedure here
	@DateInterestedIn Date 
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT COUNT(*) as TotalDeliveries from vwCORHDRFull where SHOW_IN_UI = 1 and DELD = @DateInterestedIn --Table CORHDR4 has been replaced with the view vwCORHDRFull 
END
GO

