﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockLogGetWeeklySummaryByStock]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure StockLogGetWeeklySummaryByStock'
	EXEC ('CREATE PROCEDURE [dbo].[StockLogGetWeeklySummaryByStock] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure StockLogGetWeeklySummaryByStock'
GO
ALTER PROCEDURE [dbo].[StockLogGetWeeklySummaryByStock]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	-- Use local variable rather than parameter, dramatically speeds up the running of this procedure (15x!)	
	declare	@PartNumber	char(6)=@SkuNumber;

	declare @table table(
		WeekStart		date, 
		WeekEnding		date, 
		StockOpen		int,
		QtySales		int,
		QtyBulkToSingle	int,
		QtyReceipt		int,
		QtyIbts			int,
		QtyReturns		int,	
		QtyAdjusts		int,
		StockClose		int,
		QtyVariance		int);	
	declare 
		@earliestDate   date,
		@startDate		date,
		@endDate		date,
		@openStock		int,
		@sales			int,
		@bulkSingle		int,
		@receipt		int,
		@ibts			int,
		@returns		int,
		@adjusts		int,
		@endStock		int;

	set @endDate = GETDATE();

	begin
		-- get the start of the 1st week there are records for (or this week)
		set @earliestDate =	coalesce((select MIN(DATE1) from STKLOG where SKUN = @PartNumber), getdate());
		While datepart(weekday, @earliestDate) <> 2
		begin
			set @earliestDate = dateadd(day, -1, @earliestDate)
		end	
		
		--get start of week date
		set	@startDate = @EndDate;
		While datepart(weekday, @StartDate) <> 2
		begin
			set @StartDate = dateadd(day, -1, @StartDate)
		end	

		--Repeat for every week, starting with latest and going backwards
		While datediff(day, @earliestDate, @startdate) >= 0
		begin	
			--get values to insert into table
			set @openStock =	(select top 1 SSTK from STKLOG where SKUN = @PartNumber and DATE1 >= @startDate order by TKEY);
			set @endStock =		(select top 1 ESTK from STKLOG where SKUN = @PartNumber and DATE1 <= @endDate order by TKEY desc);
			
			set	@sales =		coalesce((	select 
												SUM(ESTK-SSTK) from STKLOG 
											where 
												TYPE in (1,2,3,4,6,7,11,12)
												and SKUN = @PartNumber
												and DATE1 >= @startDate 
												and DATE1 <= @endDate), 0);
		
			set	@bulkSingle =	coalesce((	select 
												SUM(ESTK-SSTK) from STKLOG 
											where 
												TYPE = 51
												and SKUN = @PartNumber
												and DATE1 >= @startDate 
												and DATE1 <= @endDate), 0);
											
			set	@receipt =		coalesce((	select 
												SUM(ESTK-SSTK) from STKLOG 
											where 
												TYPE in (71,72,73,74)
												and SKUN = @PartNumber
												and DATE1 >= @startDate 
												and DATE1 <= @endDate), 0);
		
			set	@ibts =			coalesce((	select 
												SUM(ESTK-SSTK) from STKLOG 
											where 
												TYPE in (41,42,43,44)
												and SKUN = @PartNumber
												and DATE1 >= @startDate 
												and DATE1 <= @endDate), 0);
	
			set	@returns =		coalesce((	select 
												SUM(ESTK-SSTK) from STKLOG 
											where 
												TYPE in (21,22)
												and SKUN = @PartNumber
												and DATE1 >= @startDate 
												and DATE1 <= @endDate), 0);

			set	@adjusts =		coalesce((	select 
												SUM(ESTK-SSTK) from STKLOG 
											where 
												TYPE in (31,32,33,34,35,36,37,66,67,68,69)
												and SKUN = @PartNumber
												and DATE1 >= @startDate 
												and DATE1 <= @endDate), 0);														

			--insert into table
			insert into 
				@table
			values	(
				@startDate, 
				@EndDate, 
				@openStock,
				@sales,
				@bulkSingle,
				@receipt,
				@ibts,
				@returns,
				@adjusts,
				@endStock,
				(@endStock-@openStock-@sales-@bulkSingle-@receipt-@ibts-@returns-@adjusts));	

			--set enddate to startdate less one day
			set @EndDate = dateadd(day, -1, @StartDate)
			--get start of week date
			set	@startDate = @EndDate;
			While datepart(weekday, @StartDate) <> 2
			begin
				set @StartDate = dateadd(day, -1, @StartDate)
			end	
		end
	end

	select * from @table;	
END
GO

