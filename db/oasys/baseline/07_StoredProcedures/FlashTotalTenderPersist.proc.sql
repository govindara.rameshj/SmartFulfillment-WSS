﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FlashTotalTenderPersist]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure FlashTotalTenderPersist'
	EXEC ('CREATE PROCEDURE [dbo].[FlashTotalTenderPersist] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure FlashTotalTenderPersist'
GO
ALTER PROCEDURE [dbo].[FlashTotalTenderPersist]
	@TenderTypeId	int,
	@TotalTender	dec(9,2)
AS
BEGIN
	SET NOCOUNT ON;

		if @TenderTypeId=1
		begin
			update RSFLAS set TTCO1=TTCO1+1, TTAM1=TTAM1+@TotalTender where FKEY=1;
		end

		if @TenderTypeId=2
		begin
			update RSFLAS set TTCO2=TTCO2+1, TTAM2=TTAM2+@TotalTender where FKEY=1;
		end

		if @TenderTypeId=3
		begin
			update RSFLAS set TTCO3=TTCO3+1, TTAM3=TTAM3+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=4
		begin
			update RSFLAS set TTCO4=TTCO4+1, TTAM4=TTAM4+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=5
		begin
			update RSFLAS set TTCO5=TTCO5+1, TTAM5=TTAM5+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=6
		begin
			update RSFLAS set TTCO6=TTCO6+1, TTAM6=TTAM6+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=7
		begin
			update RSFLAS set TTCO7=TTCO7+1, TTAM7=TTAM7+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=8
		begin
			update RSFLAS set TTCO8=TTCO8+1, TTAM8=TTAM8+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=9
		begin
			update RSFLAS set TTCO9=TTCO9+1, TTAM9=TTAM9+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=10
		begin
			update RSFLAS set TTCO10=TTCO10+1, TTAM10=TTAM10+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=11
		begin
			update RSFLAS set TTCO11=TTCO11+1, TTAM11=TTAM11+@TotalTender where FKEY=1;
		end

		if @TenderTypeId=12
		begin
			update RSFLAS set TTCO12=TTCO12+1, TTAM12=TTAM12+@TotalTender where FKEY=1;
		end

		if @TenderTypeId=13
		begin
			update RSFLAS set TTCO13=TTCO13+1, TTAM13=TTAM13+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=14
		begin
			update RSFLAS set TTCO14=TTCO14+1, TTAM14=TTAM14+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=15
		begin
			update RSFLAS set TTCO15=TTCO15+1, TTAM15=TTAM15+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=16
		begin
			update RSFLAS set TTCO16=TTCO16+1, TTAM16=TTAM16+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=17
		begin
			update RSFLAS set TTCO17=TTCO17+1, TTAM17=TTAM17+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=18
		begin
			update RSFLAS set TTCO18=TTCO18+1, TTAM18=TTAM18+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=19
		begin
			update RSFLAS set TTCO19=TTCO19+1, TTAM19=TTAM19+@TotalTender where FKEY=1;
		end
		
		if @TenderTypeId=20
		begin
			update RSFLAS set TTCO20=TTCO20+1, TTAM20=TTAM20+@TotalTender where FKEY=1;
		end

END
GO

