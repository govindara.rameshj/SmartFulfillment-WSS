﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UserGetNextId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure UserGetNextId'
	EXEC ('CREATE PROCEDURE [dbo].[UserGetNextId] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure UserGetNextId'
GO
ALTER PROCEDURE [dbo].[UserGetNextId]
AS
begin
	SET NOCOUNT ON;

	declare @table table(Id int)
	declare @counter int

	SET @COUNTER = 1
	WHILE @COUNTER <= 1000
	BEGIN
		INSERT INTO @table(Id) VALUES (@COUNTER)
		SET @COUNTER = @COUNTER + 1
	END

	SELECT 
		top 1 t.id 
	FROM 
		@table t
	LEFT JOIN 
		SystemUsers su ON t.ID = su.ID
	where 
		su.ID is null
	ORDER BY 
		t.ID

end
GO

