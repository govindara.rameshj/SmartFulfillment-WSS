﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquirySupplier]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquirySupplier'
	EXEC ('CREATE PROCEDURE [dbo].[EnquirySupplier] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquirySupplier'
GO
ALTER PROCEDURE [dbo].[EnquirySupplier]
	@SkuNumber	char(6)
AS
BEGIN
	SET NOCOUNT ON;

	declare @table table 
		(RowId			int,
		Description		varchar(50), 
		Display			varchar(50));
	declare
		@id				char(5),
		@warehouse		char(5),
		@name			varchar(30),
		@address1		varchar(30),
		@address2		varchar(30),
		@address3		varchar(30),
		@address4		varchar(30),
		@address5		varchar(30),
		@postcode		varchar(8),
		@phone			varchar(15),
		@fax			varchar(15),
		@helpline		varchar(15),
		@contact		varchar(30),
		@type			varchar(10),
		@tradanet		char(3);
		
	select
		@id				= SUPP,
		@warehouse		= SUP1
	from
		STKMAS
	where
		SKUN = @SkuNumber;

	select
		@name			= sm.NAME,
		@address1		= sd.ADD1,
		@address2		= sd.ADD2,
		@address3		= sd.ADD3,
		@address4		= sd.ADD4,
		@address5		= sd.ADD5,
		@postcode		= sd.POST,
		@phone			= sd.PHO1,
		@fax			= sd.FAX1,
		@helpline		= sm.HLIN,
		@contact		= sd.CON1,	
		@type			= (	select case sd.bbcc
								when '' then 'Direct'
								when 'C' then 'Consolidated'
								when 'D' then 'Discreet'
								when 'W' then 'Warehouse'
								when 'A' then 'Alternative'
							end),
		@tradanet		= (	select case  sd.tnet
								when 1 then 'Yes'
								when 0 then 'No'
							end)
	from
		SUPMAS sm
	inner join
		SUPDET sd	on sd.SUPN = sm.SUPN and sd.DEPO = sm.ODNO
	where
		sm.SUPN = @id

	
	insert into @table(rowid, Description, Display) values (1, 'ID', @id );
	insert into @table(Description, Display) values ('Name',		rtrim(@name) );
	insert into @table(Description, Display) values ('Address',	rtrim(@address1) );
	insert into @table(Description, Display) values ('',			rtrim(@address2) );
	insert into @table(Description, Display) values ('',			rtrim(@address3) );
	insert into @table(Description, Display) values ('',			rtrim(@address4) );
	insert into @table(Description, Display) values ('',			rtrim(@address5) );
	insert into @table(Description, Display) values ('',			rtrim(@postcode) );
	
	insert into @table(Description, Display) values ('','');
	insert into @table(Description, Display) values ('Helpline (Cust. Serv.)',	rtrim(@helpline) );
	insert into @table(Description, Display) values ('Telephone',					rtrim(@phone) );
	insert into @table(Description, Display) values ('Fax',						rtrim(@fax) );
	insert into @table(Description, Display) values ('Supplier Contact',			RTRIM(@contact) );
	insert into @table(rowid, Description, Display) values (1, 'Supplier Warehouse',	@warehouse );
	insert into @table(Description, Display) values ('Type',			rtrim(@type) );
	insert into @table(Description, Display) values ('Tradanet',		@tradanet );
	
	select * from @table;
END
GO

