﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingEndOfDayCheckSummary]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingEndOfDayCheckSummary'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingEndOfDayCheckSummary] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingEndOfDayCheckSummary'
GO
ALTER PROCedure NewBankingEndOfDayCheckSummary

   @PeriodID int

as
begin
   set nocount on

   declare @Data             table(ID int, SafeBagsID int , SafeBagsType nchar(1))
   declare @BankingCashBag   table(ID int)
   declare @BankingChequeBag table(ID int)
   declare @AuthoriserName   table(ManagerName varchar(50), WitnessName varchar(50))

   insert @Data             select a.ID, a.SafeBagsID, isnull(b.[Type], a.BagType)
                            from SafeBagScanned a
                            left outer join SafeBags b
                                  on b.ID = a.SafeBagsID
                            where a.SafePeriodID = @PeriodID

   insert @BankingCashBag   select BagID from SafeBagsDenoms where TenderID = 1 group by BagID
   insert @BankingChequeBag select BagID from SafeBagsDenoms where TenderID = 2 group by BagID

   insert @AuthoriserName   select b.Name, c.Name 
                            from Safe a
                            inner join SystemUsers b
                                  on b.ID = a.EndOfDayCheckManagerID 
                            inner join SystemUsers c
                                  on c.ID = a.EndOfDayCheckWitnessID 
                            where a.PeriodID = @PeriodId

   select PickupBagCount           = (select count(*) from @Data where SafeBagsType = 'P'),
          PickupBagSystemCount     = (select count(*) from dbo.udf_PickupBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID))),

          BankingCashCount         = (select count(*) from @Data where SafeBagsType = 'B' and SafeBagsID in (select ID from @BankingCashBag)),
          BankingCashSystemCount   = (select count(*) from dbo.udf_BankingBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID)) where ID in (select ID from @BankingCashBag)),

          BankingChequeCount       = (select count(*) from @Data where SafeBagsType = 'B' and SafeBagsID in (select ID from @BankingChequeBag)),
          BankingChequeSystemCount = (select count(*) from dbo.udf_BankingBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID)) where ID in (select ID from @BankingChequeBag)),

          Manager                  = (select ManagerName from @AuthoriserName),
          [Witness]                = (select WitnessName from @AuthoriserName)

end
GO

