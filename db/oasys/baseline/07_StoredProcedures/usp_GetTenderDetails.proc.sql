﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetTenderDetails]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetTenderDetails'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetTenderDetails] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetTenderDetails'
GO
ALTER PROCEDURE usp_GetTenderDetails @ReportDate AS date , 
                                      @TillId AS varchar( 2
                                                        ) , 
                                      @TranNo AS varchar( 4
                                                        )
AS
BEGIN
    SELECT DP.AMNT , 
           DP.TYPE
      FROM DLPAID DP
      WHERE DATE1
            = 
            @ReportDate
        AND TILL = @TillId
        AND [TRAN] = @TranNo
        AND TYPE <> 1
        AND TYPE <> 99;

END;
GO

