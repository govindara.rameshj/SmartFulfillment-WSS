﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CreateSafeForNonTradingDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure CreateSafeForNonTradingDay'
	EXEC ('CREATE PROCEDURE [dbo].[CreateSafeForNonTradingDay] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure CreateSafeForNonTradingDay'
GO
ALTER PROCEDURE [dbo].[CreateSafeForNonTradingDay]
	@periodDate date
AS
BEGIN

IF NOT EXISTS (SELECT 1 FROM [dbo].[Safe] s where s.[PeriodDate] = DATEADD(dd, -1, @periodDate))
BEGIN
	
	declare @PeriodsID table (ID int)
	
	INSERT into @PeriodsID
	select ID
	FROM [dbo].[SystemPeriods] sp
	CROSS JOIN (SELECT TOP(1) [PeriodDate] FROM [dbo].[Safe] s WHERE [PeriodDate] < @periodDate ORDER BY [PeriodDate] DESC) s
	WHERE sp.[StartDate] < @periodDate
	AND sp.[StartDate] > s.[PeriodDate]
	ORDER BY sp.[StartDate]

	INSERT INTO [dbo].[Safe] 
		(
			[PeriodID],
			[PeriodDate],
			[UserID1],
			[UserID2],
			[LastAmended],
			[IsClosed],
			[EndOfDayCheckDone]
		)
	SELECT 
		sp.[ID]			AS [PeriodID], 
		sp.[StartDate]	AS [PeriodDate], 
		0				AS [UserID1], 
		0				AS [UserID2], 
		GETDATE()		AS [LastAmended], 
		0				AS [IsClosed], 
		0				AS [EndOfDayCheckDone]
	FROM [dbo].[SystemPeriods] sp
	inner join @PeriodsID per on sp.ID=per.ID
	
	INSERT INTO [dbo].[SafeDenoms]
	SELECT 
		per.ID as PeriodID,
		CurrencyID,
		TenderID,
		sd.ID,
		SafeValue,
		ChangeValue,
		SystemValue,
		SuggestedValue
	FROM [dbo].[SafeDenoms] sd
	CROSS JOIN @PeriodsID per
	where PeriodID = (select MAX(PeriodID) from SafeDenoms)
	
	RETURN @@ROWCOUNT
END
END
GO

