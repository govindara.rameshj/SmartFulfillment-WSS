﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SETSYP]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SETSYP'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SETSYP] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SETSYP'
GO
ALTER PROCEDURE dbo.KevanConversion_SETSYP
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 29th December 2010
-- 
-- Task     : 36 - Setting System Parameters
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

-----------------------------------------------------------------------------------
-- Task     : 36/01 - Reset Store ID & Name from Retail Options
----------------------------------------------------------------------------------- 
UPDATE Parameters
SET StringValue = (Select SNAM From RETOPT where FKEY = '01'), 
LongValue = (Select STOR From RETOPT where FKEY = '01')
Where ParameterID = '100'

UPDATE SYSOPT
SET SNAM = (Select SNAM From RETOPT where FKEY = '01'), 
STOR = (Select STOR From RETOPT where FKEY = '01')
Where FKey = '01'

-----------------------------------------------------------------------------------
-- Task     : 36/02 - Setting CommIdea Login Details
----------------------------------------------------------------------------------- 

declare @StoreNumber char(4)

SET @StoreNumber = (Select '8' + Stor As Expr1 From [Oasys].[dbo].[RETOPT]);

UPDATE Parameters
SET StringValue = (@StoreNumber+ ',' +@StoreNumber)
Where ParameterID = '970003'

END;
GO

