﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ParametersGetBooleanValueById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ParametersGetBooleanValueById'
	EXEC ('CREATE PROCEDURE [dbo].[ParametersGetBooleanValueById] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ParametersGetBooleanValueById'
GO
ALTER PROCEDURE [dbo].[ParametersGetBooleanValueById]
(
	@Id int
)
AS
	SET NOCOUNT ON;
SELECT     BooleanValue
FROM         Parameters
WHERE     (ParameterID = @Id)
GO

