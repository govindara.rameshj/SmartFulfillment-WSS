﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EnquiryOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure EnquiryOrder'
	EXEC ('CREATE PROCEDURE [dbo].[EnquiryOrder] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure EnquiryOrder'
GO
ALTER PROCEDURE [dbo].[EnquiryOrder]
	@SkuNumber		char(6)
AS
BEGIN
	SET NOCOUNT ON;

	declare @table table 
		(RowId int, 
		SkuNumber char(6), 
		Description varchar(50), 
		Display varchar(50));
		
	declare
		@returnQty		int,
		@buyingUnit		char(4),
		@packSize		int,
		@initialOrder	date,
		@finalOrder		date;

	select
		@returnQty		= RETQ,
		@initialOrder	= IODT,
		@finalOrder		= FODT,
		@buyingUnit		= BUYU,
		@packSize		= PACK
	from
		STKMAS
	where
		SKUN = @SkuNumber;
	
	insert into @table(Description, Display) values ('Open Returns',	@returnQty );
	insert into @table(Description, Display) values ('Initial Order',	convert(varchar(10), @initialOrder, 103) );
	insert into @table(Description, Display) values ('Final Order',		convert(varchar(10), @finalOrder, 103) );
	insert into @table(Description, Display) values ('Buying Unit',		@buyingUnit);
	insert into @table(Description, Display) values ('Pack Size',		@packSize);
	insert into @table(RowId, SkuNumber, Description) values (1, @SkuNumber, 'PO History' );
	
	select * from @table;
END
GO

