﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_ITSupport_DeleteDuplicatePriceChanges]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_ITSupport_DeleteDuplicatePriceChanges'
	EXEC ('CREATE PROCEDURE [dbo].[usp_ITSupport_DeleteDuplicatePriceChanges] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_ITSupport_DeleteDuplicatePriceChanges'
GO
ALTER PROCEDURE [dbo].[usp_ITSupport_DeleteDuplicatePriceChanges]
As
Begin
Set NoCount On;
	
-----------------------------------------------------------------------------------
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 14th October 2011
-- 
-- Task     : Delete Duplicated Price Changes
-----------------------------------------------------------------------------------


---------------------------------
-- Retrieve Duplicates
---------------------------------
Create Table #Temp_DuplicatePriceChanges
(
DuplicateID [INT] IDENTITY(1,1),
[Sku] [Char](6) NOT NULL,
[Count] [int] NOT NULL,
)
Insert Into #Temp_DuplicatePriceChanges
Select SKUN, Count (SKUN) As [Count]
From Oasys.dbo.PRCCHG
Where PSTA = 'U' 
Group By SKUN
Having Count(SKUN) > 1
Select * From #Temp_DuplicatePriceChanges
---------------------------------
-- Clear-Up Duplicates
---------------------------------
Declare @DuplicateRID INT
Declare @DuplicateMAXID INT
Select @DuplicateMAXID = MAX(DuplicateID)FROM #Temp_DuplicatePriceChanges 
Set @DuplicateRID = 1  
While(@DuplicateRID<=@DuplicateMAXID)
Begin		         
		Declare @Process int = (Select [COUNT] FROM #Temp_DuplicatePriceChanges WHERE DuplicateID = @DuplicateRID)
		Declare @DUP_Sku Char(6) = (Select LTRIM(RTRIM(Sku)) FROM #Temp_DuplicatePriceChanges WHERE DuplicateID = @DuplicateRID)
		While (@Process>1)		
			Begin  
				-- Process Duplicate Information
				Declare @DUP_PDAT date = (Select TOP(1) PDAT From Oasys.Dbo.PRCCHG Where SKUN = @DUP_Sku and PSTA = 'U' Order By PDAT Asc)
				Delete From Oasys.Dbo.PRCCHG Where SKUN = @DUP_Sku and PDAT = @DUP_PDAT and PSTA = 'U' 
				If @@ERROR = 0
					Begin 
						Print('Deleted: ' + LTRIM(RTRIM(@DUP_SKU)) + ' Dated: ' + Cast(@DUP_PDAT as Char))
					End
				Else
					Begin
						Print('Failed to Delete: ' + LTRIM(RTRIM(@DUP_SKU)) + ' Dated: ' + Cast(@DUP_PDAT as Char))        
					End   
				Set @Process = (@Process - 1)
			End 
		Set @DuplicateRID = @DuplicateRID+1

End
End
GO

