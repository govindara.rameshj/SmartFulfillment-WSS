﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingSafeChecked]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingSafeChecked'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingSafeChecked] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingSafeChecked'
GO
-----------------------------------------------------
--                 SAFE MAINTENANCE                --
-----------------------------------------------------

ALTER PROCedure NewBankingSafeChecked
   @PeriodID int
as
begin
set nocount on

update Safe set SafeChecked = 1 where PeriodID = @PeriodID
end
GO

