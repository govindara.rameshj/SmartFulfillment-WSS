﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SalesGetHeaderLines]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SalesGetHeaderLines'
	EXEC ('CREATE PROCEDURE [dbo].[SalesGetHeaderLines] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SalesGetHeaderLines'
GO
ALTER PROCEDURE [dbo].[SalesGetHeaderLines] 
    @Date           date,
    @TillNumber     char(2),
    @TranNumber     char(4)
AS
BEGIN
    SET NOCOUNT ON;

    select
        dt.DATE1         as 'Date',
        dt.TILL          as 'TillId',
        dt.[TRAN]        as 'TranNumber',
        dt.OVCTranNumber as 'OVCTranNumber',    
        dt.[TIME]        as 'Time',     
        dt.CASH          as 'UserId',
        coalesce(su.Name, 'Unknown') as 'UserName',
        dt.TCOD          as 'SaleType',
        dt.VOID          as 'IsVoid',
        dt.TMOD          as 'IsTraining',
        dt.DISC          as 'Discount',     
        dt.TOTL          as 'TotalAmount'   
    from
        vwDLTOTS dt
    left join
        SystemUsers su on su.ID = dt.CASH
    where
            dt.DATE1    = convert(date, @Date)
        and dt.TILL     = @TillNumber
        and dt.[TRAN]   = @TranNumber
    
    
    select
        dl.DATE1    as 'Date',
        dl.TILL     as 'TillId',
        dl.[TRAN]   as 'TranNumber',
        dl.NUMB     as 'Number',
        dl.SKUN     as 'SkuNumber',
        sk.DESCR    as 'Description',
        dl.QUAN     as 'Quantity',
        dl.SPRI     as 'SalePrice',
        dl.PRIC     as 'Price',
        dl.EXTP     as 'ExtendedPrice'
    from
        DLLINE dl
    inner join
        STKMAS sk on sk.SKUN = dl.SKUN
    where
            dl.DATE1    = convert(date, @Date)
        and dl.TILL     = @TillNumber
        and dl.[TRAN]   = @TranNumber       
    
END
GO

