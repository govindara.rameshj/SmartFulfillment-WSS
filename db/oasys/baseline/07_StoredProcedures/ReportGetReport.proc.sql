﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGetReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReportGetReport'
	EXEC ('CREATE PROCEDURE [dbo].[ReportGetReport] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReportGetReport'
GO
ALTER PROCEDURE [dbo].[ReportGetReport]
	@Id int
AS
BEGIN
	SET NOCOUNT ON;

	select
		*
	from
		report
	where 
		Id = @Id

END
GO

