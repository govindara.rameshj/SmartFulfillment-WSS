﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemPeriodAutoPopulate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure SystemPeriodAutoPopulate'
	EXEC ('CREATE PROCEDURE [dbo].[SystemPeriodAutoPopulate] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure SystemPeriodAutoPopulate'
GO
ALTER PROCedure [dbo].[SystemPeriodAutoPopulate]
as
begin

	if not exists (select * from SystemPeriods where StartDate <= GETDATE() and EndDate >= GETDATE())
	begin
		declare @startdate date = 
			coalesce( (select MAX(EndDate) from SystemPeriods), 
			CAST(CAST(YEAR(getdate()) AS VARCHAR(4)) + '/' + '01/01' AS DATETIME));

		declare @stopdate datetime =
			coalesce( (select min(StartDate) from SystemPeriods where StartDate>@startdate), 
			CAST(CAST(YEAR(getdate()) AS VARCHAR(4)) + '/' + '12/31' AS DATETIME));
		
		while (@startdate<@stopdate)
		begin
			declare @isClosed bit = 0;
			declare @enddate datetime = @startdate;
			set @enddate = DATEADD(hh, 23, @enddate);
			set @enddate = DATEADD(mi, 59, @enddate);
			
			if @enddate<GETDATE() set @isClosed=1;
			
			insert into SystemPeriods values (@startdate, @enddate, @isClosed);
				
			set @startdate = DATEADD(day,1, @startdate);
		end
		
	end
end
GO

