﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_GetDRLNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure usp_GetDRLNumber'
	EXEC ('CREATE PROCEDURE [dbo].[usp_GetDRLNumber] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure usp_GetDRLNumber'
GO
-- =============================================
-- Author:		Michael OCain
-- Create date: 15/06/2011
-- Description:	Get DRL Number
-- 
-- Modified by: Sean Moir
-- Modified date: 24/06/2011
-- Description:	Added a Reversible flag which is 0 if DRL date = SYSDAT.TMDT
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetDRLNumber] 
	-- Add the parameters for the stored procedure here
	@DRLNumber char(6)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---- if the DRL is found in DRLSUM and PURHDR, use that data
	--IF EXISTS (SELECT * FROM 
	--			DRLSUM DRL 
	--				INNER JOIN PURHDR PUR
	--					ON DRL.[0PON] = PUR.NUMB
	--			WHERE DRL.NUMB = @DRLNumber)
	--	BEGIN
	--		SELECT 
	--			ds.[TYPE] as 'DRLType',
	--			ds.Numb as 'DRLNumber',
	--			ph.BBCC as 'BBCC',
	--			ds.COMM as 'IsSentToHO' 			   
	--		FROM DRLSUM	 ds 
	--			inner join PURHDR ph 
	--				on ph.NUMB = ds.[0PON]
	--		WHERE 
	--			ds.NUMB = @DRLNumber
	--			AND ds.[TYPE] = '0'
	--			AND ph.BBCC NOT IN ('A', 'W')
	--			AND ds.COMM = 1
		
	--	END	
	---- else use SUPMAS and SUPDET tables
	--ELSE
	--	BEGIN
	--		SELECT 
	--			ds.[TYPE] as 'DRLType',
	--			ds.Numb as 'DRLNumber',
	--			sd.BBCC as 'BBCC',
	--			ds.COMM as 'IsSentToHO' 			   
	--		FROM DRLSUM	 ds 
	--			inner join SUPDET sd 
	--				on sd.SUPN = ds.[0SUP]
	--		WHERE 
	--			ds.NUMB = @DRLNumber
	--			AND ds.[TYPE] = '0'
	--			AND sd.BBCC NOT IN ('A', 'W')
	--			AND ds.COMM = 1
	--	END
		
	DECLARE @SYSDAT_TMDT as smalldatetime
	
	SELECT @SYSDAT_TMDT = TMDT FROM SYSDAT

	SELECT 
		ds.[TYPE] as 'DRLType',
		ds.Numb as 'DRLNumber',
		sd.BBCC as 'supBBCC',
		ph.BBCC as 'purBBCC',
		ds.COMM as 'IsSentToHO',
		case when DATEPART(year, @SYSDAT_TMDT) = DATEPART(year, ds.DATE1)
			AND DATEPART(month, @SYSDAT_TMDT) = DATEPART(month, ds.DATE1)
			AND DATEPART(day, @SYSDAT_TMDT) = DATEPART(day, ds.DATE1)
			then 0
			else 1
		end 'Reversible'
    FROM DRLSUM	 ds 
		left outer join SUPDET sd on ds.[0SUP] = sd.SUPN
		left outer join PURHDR ph on ds.[0PON] = ph.NUMB
	WHERE 
		ds.NUMB = @DRLNumber
		
END
GO

