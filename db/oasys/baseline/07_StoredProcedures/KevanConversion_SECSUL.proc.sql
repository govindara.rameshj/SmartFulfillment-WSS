﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_SECSUL]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_SECSUL'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_SECSUL] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_SECSUL'
GO
ALTER PROCEDURE dbo.KevanConversion_SECSUL
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 31 - Apply Security Access Permissions for Store SQL Access Login
-----------------------------------------------------------------------------------
AS
BEGIN
        SET NOCOUNT ON

	IF EXISTS (SELECT * FROM sys.server_principals WHERE name = 'TPPLC\WX_' + (Select '8' + Stor As Expr1 From RETOPT) + '_SQL')
		Begin
			Print ('User TPPLC\WX_8nnn_SQL exists in this SQL Server, skipping creation..')
		End
	ELSE
		Begin
			Print ('Creating user TPPLC\WX_8nnn_SQL..')
		
        		declare @Domain char(5)
        		declare @TpOrg char(3)
        		declare @StoreNumber char(4)
        		declare @TpGroup char(4)
        		declare @Name char (11)
        
        		DECLARE @SQL NVARCHAR(4000);
        		DECLARE @SQL2 NVARCHAR(4000);
        		DECLARE @SQLEXEC1 NVARCHAR(4000);
        		DECLARE @SQLEXEC2 NVARCHAR(4000);
        		DECLARE @SQLEXEC3 NVARCHAR(4000);
        		DECLARE @SQLPERM1 NVARCHAR(4000);
        		DECLARE @SQLPERM2 NVARCHAR(4000);
        		DECLARE @SQLPERM3 NVARCHAR(4000);
        		DECLARE @SQLPERM4 NVARCHAR(4000);
        		DECLARE @SQLPERM5 NVARCHAR(4000);
        		DECLARE @SQLPERM6 NVARCHAR(4000);
        
        		SET @Domain = 'TPPLC';
        		SET @TpOrg = 'WX_'; 
        		SET @StoreNumber = (Select '8' + Stor As Expr1 From [Oasys].[dbo].[RETOPT]);
        		SET @TpGroup = '_SQL';
        		SET @Name = (@TpOrg + @StoreNumber + @TpGroup);
        
        		--Use [Master]
        		SET @SQL = 'CREATE LOGIN ['+ @Domain +'\'+ @Name +'] FROM WINDOWS WITH DEFAULT_DATABASE = [Oasys]';
        		EXECUTE(@SQL);
        
        		--Use [Oasys]
        		SET @SQL2 = 'CREATE USER [' + @Domain +'\'+ @Name + '] FOR LOGIN ['+ @Domain +'\'+ @Name + ']';
        		EXECUTE(@SQL2);
        
        		-- Set Server Roles for User Login
        		SET @SQLEXEC1 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = role_execproc';
        		EXECUTE(@SQLEXEC1);
        		SET @SQLEXEC2 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = role_legacy';
        		EXECUTE(@SQLEXEC2);
        		SET @SQLEXEC3 = 'sys.sp_addrolemember @membername = ['+ @Domain +'\'+ @Name +'], @rolename = db_backupoperator';
        		EXECUTE(@SQLEXEC3);
        
        		-- Set Permissions for User Login
        		SET @SQLPERM1 = 'GRANT ALTER TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION';
        		EXECUTE(@SQLPERM1); 
			SET @SQLPERM2 = 'GRANT BACKUP DATABASE TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION';
			EXECUTE(@SQLPERM2);
			SET @SQLPERM3 = 'GRANT CONNECT TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM3);
			SET @SQLPERM4 = 'GRANT EXECUTE TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM4);
			SET @SQLPERM5 = 'GRANT SELECT TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM5);
			SET @SQLPERM6 = 'GRANT UPDATE TO ['+ @Domain +'\'+ @Name +'] WITH GRANT OPTION'; 
			EXECUTE(@SQLPERM6);
		End

END;
GO

