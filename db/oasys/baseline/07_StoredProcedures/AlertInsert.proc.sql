﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AlertInsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure AlertInsert'
	EXEC ('CREATE PROCEDURE [dbo].[AlertInsert] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure AlertInsert'
GO
ALTER PROCEDURE [dbo].[AlertInsert] 
    @OrderNumber char(6),
    @ReceivedDate datetime,
    @AlertType tinyint
AS
BEGIN

INSERT INTO [dbo].[Alert]
           ([OrderNumb],
            [ReceivedDate],
            [AlertType]
           )
     VALUES
           (@OrderNumber,
            @ReceivedDate,
            @AlertType
            )
Return @@RowCount
End
GO

