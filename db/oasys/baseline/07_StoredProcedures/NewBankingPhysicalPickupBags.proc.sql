﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewBankingPhysicalPickupBags]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure NewBankingPhysicalPickupBags'
	EXEC ('CREATE PROCEDURE [dbo].[NewBankingPhysicalPickupBags] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure NewBankingPhysicalPickupBags'
GO
ALTER PROCedure NewBankingPhysicalPickupBags

   @PeriodID int 

as
begin
   set nocount on

   select * from dbo.udf_PickupBagsInSafeAtDateAndTime(dbo.udf_ActualPhysicalBagDate(null, @PeriodID))

end
GO

