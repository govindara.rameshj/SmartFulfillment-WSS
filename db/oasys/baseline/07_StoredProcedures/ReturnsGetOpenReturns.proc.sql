﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReturnsGetOpenReturns]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure ReturnsGetOpenReturns'
	EXEC ('CREATE PROCEDURE [dbo].[ReturnsGetOpenReturns] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure ReturnsGetOpenReturns'
GO
ALTER PROCEDURE [dbo].[ReturnsGetOpenReturns]
	@SupplierNumber		VARCHAR(5)	=Null,
	@Date				DATETIME	=Null
AS
BEGIN
	SET NOCOUNT ON;

	select * from dbo.udf_GetOpenReturns(@SupplierNumber,@Date)
		
END
GO

