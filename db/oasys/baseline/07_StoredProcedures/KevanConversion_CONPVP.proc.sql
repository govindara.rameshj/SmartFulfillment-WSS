﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[KevanConversion_CONPVP]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	PRINT 'Creating procedure KevanConversion_CONPVP'
	EXEC ('CREATE PROCEDURE [dbo].[KevanConversion_CONPVP] AS RAISERROR(''Not implemented NULL'', 16, 1)')
END
GO

PRINT 'Altering procedure KevanConversion_CONPVP'
GO
ALTER PROCEDURE dbo.KevanConversion_CONPVP
-----------------------------------------------------------------------------------
-- Kevan's Conversion Scripts 
--
-- Version  : 1.0 
-- Revision : 1.0
-- Author   : Kevan Madelin
-- Date	    : 17th December 2010
-- 
-- Task     : 19 - PV Paid Conversion
-----------------------------------------------------------------------------------
AS
BEGIN

SET NOCOUNT OFF

create table #pvPaidTemp
(
    RID INT IDENTITY(1,1),
    [DATE1] [char](8) NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [char](4) NOT NULL,
	[TYPE] [char](2) NULL,
	[AMNT] [char](10) NULL,
	[PIVT] [char](1) NULL,
)

create table #visionPaymentTemp
(

    [TranDate] [date] NOT NULL,
	[TillId] [int] NOT NULL,
	[TranNumber] [int] NOT NULL,
	[Number] [int] NOT NULL,
	[TenderTypeId] [int] ,
	[ValueTender] [decimal](9, 2) ,
	[IsPivotal] [bit] ,
)

INSERT INTO #pvPaidTemp
SELECT * FROM PVPAID  

INSERT INTO #visionPaymentTemp
SELECT * FROM VisionPayment 

    DECLARE @RID_2 INT
	DECLARE @MAXID_2 INT
	SELECT @MAXID_2 = MAX(RID)FROM #pvPaidTemp -- SELECTING THE MAX RID FROM TEMP TABLE
	SET @RID_2 = 1
	DECLARE @DATE_2 CHAR(15)
	DECLARE @TILL_ID_2 INT
	DECLARE @TRAN_NO_2 INT
	DECLARE @NUMB_2 INT


-- looping the #temp_old from first record to last record.
	WHILE(@RID_2<=@MAXID_2)
		BEGIN
			-- selecting the PRIMARY key of old record to check whether it ther in the new table
			SELECT @DATE_2 = DATE1, @TILL_ID_2 = CONVERT(INT,TILL), @TRAN_NO_2 = CONVERT(INT,[TRAN]),@NUMB_2 = CONVERT(INT,[NUMB])   FROM #pvPaidTemp WHERE RID = @RID_2
			-- if the Primary key exists then update, by taking the values from the #temp_old  

			IF EXISTS(SELECT 1 FROM #visionPaymentTemp WHERE TranDate  = CONVERT(DATETIME,@DATE_2,3) AND TillId = @TILL_ID_2 AND TranNumber=@TRAN_NO_2 AND Number =@NUMB_2)
				BEGIN

					UPDATE VisionPayment 
					SET	[TenderTypeId]= CONVERT(INT,T.[TYPE]),
					   [ValueTender] = CASE WHEN CHARINDEX('-',T.[AMNT])>1
					                      THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[AMNT],0,LEN(T.[AMNT])))
					                          ELSE
					                           CONVERT(DECIMAL,T.[AMNT])
					                       END, 
					   [IsPivotal]=case when T.[PIVT]='Y' then 1 else 0 end				    					    				    					    										
					FROM #pvPaidTemp AS T WHERE (VisionPayment.TranDate = CONVERT(DATETIME,@DATE_2,3) AND T.DATE1 = @DATE_2) AND (VisionPayment.TillId = @TILL_ID_2 and  T.TILL = CONVERT(CHAR(2),@TILL_ID_2)) AND (VisionPayment.TranNumber = @TRAN_NO_2 AND  T.[TRAN]= CONVERT(CHAR(4),@TRAN_NO_2)) AND (VisionPayment.Number = @NUMB_2 AND  T.NUMB= CONVERT(CHAR(4),@NUMB_2))

			END
			ELSE -- ELSE INSERT NEW RECORD
				BEGIN
					INSERT INTO VisionPayment (TranDate,TillId,TranNumber,Number,TenderTypeId,ValueTender,IsPivotal)
					SELECT CONVERT(DATETIME,T.DATE1,3),CONVERT(INT,T.TILL),CONVERT(INT,T.[TRAN]),CONVERT(INT,T.[NUMB]),CONVERT(INT,T.[TYPE]),
					CASE WHEN CHARINDEX('-',T.[AMNT])>1
					THEN CONVERT(DECIMAL,'-'+SUBSTRING(T.[AMNT],0,LEN(T.[AMNT])))
					ELSE
					CONVERT(DECIMAL,T.[AMNT])
					END, 
					case when T.[PIVT]='Y' then 1 else 0 end	
					FROM #pvPaidTemp   AS T 
					WHERE T.RID = @RID_2 
				END

			SET @RID_2 = @RID_2+1 -- increment loop count
		END

END;
GO

