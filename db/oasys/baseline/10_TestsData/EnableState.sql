﻿INSERT [dbo].[EnableState] ([State], [Description], [DeliveryStatus]) VALUES (N'001', N'New', 0)
INSERT [dbo].[EnableState] ([State], [Description], [DeliveryStatus]) VALUES (N'002', N'Pick Note Printed', 400)
INSERT [dbo].[EnableState] ([State], [Description], [DeliveryStatus]) VALUES (N'003', N'Order Picked', 500)
INSERT [dbo].[EnableState] ([State], [Description], [DeliveryStatus]) VALUES (N'004', N'Order Picked Customer Notified', 600)
INSERT [dbo].[EnableState] ([State], [Description], [DeliveryStatus]) VALUES (N'005', N'Collected & Confirmed', 900)
INSERT [dbo].[EnableState] ([State], [Description], [DeliveryStatus]) VALUES (N'006', N'Order Cancelled & Refunded', 800)
GO
