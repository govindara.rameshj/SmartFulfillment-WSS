﻿SET IDENTITY_INSERT [dbo].[Vehicle] ON 

INSERT [dbo].[Vehicle] ([ID], [DepotID], [Registration], [VehicleTypeID], [StartTime], [StopTime], [MaximumWeight], [MaximumVolume], [LastUpdateTime]) VALUES (1, N'8853', N'YH10HGC', 1, CAST(0x0700D85EAC3A0000 AS Time), CAST(0x070050CFDF960000 AS Time), CAST(860.00 AS Decimal(19, 2)), CAST(0.00 AS Decimal(19, 2)), CAST(0x0000908300DEFA39 AS DateTime))
INSERT [dbo].[Vehicle] ([ID], [DepotID], [Registration], [VehicleTypeID], [StartTime], [StopTime], [MaximumWeight], [MaximumVolume], [LastUpdateTime]) VALUES (2, N'8853', N'YH10HEJ', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(860.00 AS Decimal(19, 2)), CAST(0.00 AS Decimal(19, 2)), CAST(0x0000909100E8DC3C AS DateTime))
INSERT [dbo].[Vehicle] ([ID], [DepotID], [Registration], [VehicleTypeID], [StartTime], [StopTime], [MaximumWeight], [MaximumVolume], [LastUpdateTime]) VALUES (3, N'8853', N'FG12BNZ', 4, CAST(0x0700D85EAC3A0000 AS Time), CAST(0x070050CFDF960000 AS Time), CAST(7500.00 AS Decimal(19, 2)), CAST(0.00 AS Decimal(19, 2)), CAST(0x0000908300DEFA3E AS DateTime))
INSERT [dbo].[Vehicle] ([ID], [DepotID], [Registration], [VehicleTypeID], [StartTime], [StopTime], [MaximumWeight], [MaximumVolume], [LastUpdateTime]) VALUES (4, N'8853', N'FE11UDG', 66, CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(6500.00 AS Decimal(19, 2)), CAST(0.00 AS Decimal(19, 2)), CAST(0x0000909100E8DC4F AS DateTime))
SET IDENTITY_INSERT [dbo].[Vehicle] OFF
GO
