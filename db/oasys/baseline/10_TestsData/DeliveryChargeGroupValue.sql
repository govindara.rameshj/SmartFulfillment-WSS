﻿INSERT [dbo].[DeliveryChargeGroupValue] ([Id], [Group], [Value]) VALUES (1, N'A', CAST(7.95 AS Decimal(9, 2)))
INSERT [dbo].[DeliveryChargeGroupValue] ([Id], [Group], [Value]) VALUES (1, N'A', CAST(12.95 AS Decimal(9, 2)))
INSERT [dbo].[DeliveryChargeGroupValue] ([Id], [Group], [Value]) VALUES (2, N'B', CAST(9.95 AS Decimal(9, 2)))
GO
