﻿INSERT INTO dbo.Safe ([PeriodID],[PeriodDate],[UserID1],[USERID2],[LastAmended],[IsClosed], [EndOfDayCheckDone])
SELECT 
		ID, 
		StartDate, 
		0, 
		0, 
		EndDate, 
		0, 
		0
FROM dbo.SystemPeriods WHERE StartDate = DATEADD(dd, DATEDIFF(dd, 3, GETDATE()), 0)
