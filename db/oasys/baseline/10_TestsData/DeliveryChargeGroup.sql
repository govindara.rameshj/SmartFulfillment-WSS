﻿SET IDENTITY_INSERT [dbo].[DeliveryChargeGroup] ON 

INSERT [dbo].[DeliveryChargeGroup] ([Id], [Group], [SkuNumber], [IsDefault]) VALUES (1, N'A', N'805002', 0)
INSERT [dbo].[DeliveryChargeGroup] ([Id], [Group], [SkuNumber], [IsDefault]) VALUES (2, N'B', N'805006', 0)
INSERT [dbo].[DeliveryChargeGroup] ([Id], [Group], [SkuNumber], [IsDefault]) VALUES (3, N'C', N'805000', 1)
SET IDENTITY_INSERT [dbo].[DeliveryChargeGroup] OFF
GO
