﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_SplitVarcharToTable]', N'TF') IS NULL
BEGIN
	PRINT 'Creating function udf_SplitVarcharToTable'
	EXEC ('CREATE FUNCTION [dbo].[udf_SplitVarcharToTable]() RETURNS @t TABLE ([Col] int) AS BEGIN RETURN END')
END
GO

PRINT 'Altering function udf_SplitVarcharToTable'
GO
ALTER FUNCTION [dbo].[udf_SplitVarcharToTable]
	(
	 @StringArray VARCHAR(8000),
	 @Delimiter VARCHAR(10)
	)
RETURNS @Results TABLE
(
	ID INT IDENTITY(1, 1), Item VARCHAR(8000)
)
AS

BEGIN
	DECLARE @Splitpoint INT
	DECLARE @lenDelimiter INT
	--initialise everything
	SELECT @lenDelimiter=LEN(REPLACE(@Delimiter,' ','|'))

	--notice we have to be cautious about LEN with trailing spaces!

	--while there is more of the string
	WHILE 1=1
	   BEGIN
		   SELECT @splitpoint=CHARINDEX(@Delimiter,@StringArray)

		   IF @SplitPoint=0
			   BEGIN
				   INSERT INTO @Results (Item) SELECT @StringArray
				   BREAK
			   END

		   INSERT INTO @Results (Item)
		   SELECT LEFT(@StringArray,@Splitpoint-1)

		   --use STUFF to delete the first x characters of the string!
		   SELECT @StringArray=
		   STUFF(@StringArray,1,@Splitpoint+@lenDelimiter-1,'')
	   END

  RETURN
END
GO

