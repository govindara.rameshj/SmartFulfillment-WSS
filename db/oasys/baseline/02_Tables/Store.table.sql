﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Store]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table Store'
CREATE TABLE [dbo].[Store](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[Address5] [varchar](50) NULL,
	[PostCode] [varchar](20) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[FaxNumber] [varchar](20) NULL,
	[Manager] [varchar](100) NULL,
	[RegionCode] [char](3) NULL,
	[CountryCode] [char](3) NULL,
	[IsClosed] [bit] NOT NULL,
	[IsOpenMon] [bit] NOT NULL,
	[IsOpenTue] [bit] NOT NULL,
	[IsOpenWed] [bit] NOT NULL,
	[IsOpenThu] [bit] NOT NULL,
	[IsOpenFri] [bit] NOT NULL,
	[IsOpenSat] [bit] NOT NULL,
	[IsOpenSun] [bit] NOT NULL,
	[IsHeadOffice] [bit] NOT NULL,
 CONSTRAINT [PK_Stores] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

