﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QUOLIN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table QUOLIN'
CREATE TABLE [dbo].[QUOLIN](
	[NUMB] [char](6) NOT NULL,
	[LINE] [char](4) NOT NULL,
	[SKUN] [char](6) NULL,
	[QUAN] [int] NULL,
	[WGHT] [decimal](7, 2) NULL,
	[VOLU] [decimal](7, 2) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_QUOLIN] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[LINE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Quote Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOLIN', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line Number - 0001 to 9999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOLIN', @level2type=N'COLUMN',@level2name=N'LINE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOLIN', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOLIN', @level2type=N'COLUMN',@level2name=N'QUAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total line weight - future' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOLIN', @level2type=N'COLUMN',@level2name=N'WGHT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total M3 volume of line - future
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOLIN', @level2type=N'COLUMN',@level2name=N'VOLU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOLIN', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Q U O L I N  =  Quote Lines File  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOLIN'
END
GO

