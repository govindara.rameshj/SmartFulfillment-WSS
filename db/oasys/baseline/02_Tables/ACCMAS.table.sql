﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ACCMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ACCMAS'
CREATE TABLE [dbo].[ACCMAS](
	[NUMB] [char](6) NOT NULL,
	[DEPO] [char](3) NULL,
	[ATYP] [char](1) NULL,
	[NAME] [char](36) NULL,
	[ALPH] [char](10) NULL,
	[ADD1] [char](24) NULL,
	[ADD2] [char](20) NULL,
	[ADD3] [char](20) NULL,
	[ADD4] [char](20) NULL,
	[PHON] [char](15) NULL,
	[CLIM] [decimal](7, 0) NULL,
	[ABAL] [decimal](9, 2) NULL,
	[STAT] [char](1) NULL,
	[TYPE] [char](2) NULL,
	[DLCS] [date] NULL,
	[TCSA] [decimal](9, 2) NULL,
	[TNCS] [decimal](9, 2) NULL,
	[NCSA1] [decimal](11, 2) NULL,
	[NCSA2] [decimal](11, 2) NULL,
	[NCSA3] [decimal](11, 2) NULL,
	[NCSA4] [decimal](11, 2) NULL,
	[NCSA5] [decimal](11, 2) NULL,
	[NCSA6] [decimal](11, 2) NULL,
	[NCSA7] [decimal](11, 2) NULL,
	[CRSA1] [decimal](11, 2) NULL,
	[CRSA2] [decimal](11, 2) NULL,
	[CRSA3] [decimal](11, 2) NULL,
	[CRSA4] [decimal](11, 2) NULL,
	[CRSA5] [decimal](11, 2) NULL,
	[CRSA6] [decimal](11, 2) NULL,
	[CRSA7] [decimal](11, 2) NULL,
	[DSTP] [bit] NOT NULL,
	[DTST] [date] NULL,
	[URCS] [bit] NOT NULL,
	[DURS] [date] NULL,
	[DELC] [bit] NOT NULL,
	[DDEL] [date] NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_ACCMAS] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Account Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Depot Number for this Account - RO:STOR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'DEPO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Type - P=Premier  Trade Card (Credit)
S=Standard Trade Card (Non-Credit)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'ATYP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alpha Lookup - First 10 Characters of the Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line One' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'ADD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line Two' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'ADD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line Three' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'ADD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line Three' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'ADD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Phone Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'PHON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Credit Limit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'CLIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Account Balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'ABAL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Status - Blank = OK, R = Refer, S=Stop,
L = Legal Stop' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'STAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Account - NOT USED at this time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of Last Credit Sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS', @level2type=N'COLUMN',@level2name=N'DLCS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A C C M A S  =   Customer Account Master File  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCMAS'
END
GO

