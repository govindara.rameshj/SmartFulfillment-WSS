﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EVTHDR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table EVTHDR'
CREATE TABLE [dbo].[EVTHDR](
	[NUMB] [char](6) NOT NULL,
	[DESCR] [char](40) NULL,
	[PRIO] [char](2) NULL,
	[SDAT] [date] NULL,
	[STIM] [char](4) NULL,
	[EDAT] [date] NULL,
	[ETIM] [char](4) NULL,
	[DACT1] [bit] NOT NULL,
	[DACT2] [bit] NOT NULL,
	[DACT3] [bit] NOT NULL,
	[DACT4] [bit] NOT NULL,
	[DACT5] [bit] NOT NULL,
	[DACT6] [bit] NOT NULL,
	[DACT7] [bit] NOT NULL,
	[IDEL] [bit] NOT NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_EVTHDR] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Number - 100 to 999999 (1 to 99 are reserved
for price override erosions)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Priority    - Highest Number = Highest Priority' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'PRIO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start Date  - DD/MM/YY (null date not allowed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'SDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start Time  - HHMM - 0001 to 2359
- zero if no time limits' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'STIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending Date - DD/MM/YY (null date implies permanent)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'EDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending Time - HHMM - 0001 to 2359
- zero if no time limits' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'ETIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days Activt if Start and End Time Specified
Occurance 1 = Monday ..... 7 = Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'DACT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days Activt if Start and End Time Specified
Occurance 1 = Monday ..... 7 = Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'DACT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days Activt if Start and End Time Specified
Occurance 1 = Monday ..... 7 = Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'DACT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days Activt if Start and End Time Specified
Occurance 1 = Monday ..... 7 = Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'DACT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days Activt if Start and End Time Specified
Occurance 1 = Monday ..... 7 = Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'DACT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days Activt if Start and End Time Specified
Occurance 1 = Monday ..... 7 = Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'DACT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days Activt if Start and End Time Specified
Occurance 1 = Monday ..... 7 = Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'DACT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delete Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E V T H D R  =  Event Header File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTHDR'
END
GO

