﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RSCASH]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RSCASH'
CREATE TABLE [dbo].[RSCASH](
	[CASH] [char](3) NOT NULL,
	[FTKY] [decimal](9, 0) NULL,
	[TILL] [char](2) NULL,
	[NAME] [varchar](35) NULL,
	[SECC] [char](5) NULL,
	[NUMB] [decimal](5, 0) NULL,
	[AMNT] [decimal](9, 2) NULL,
 CONSTRAINT [PK_RSCASH] PRIMARY KEY CLUSTERED 
(
	[CASH] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Number - Key to File 001 to 999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSCASH', @level2type=N'COLUMN',@level2name=N'CASH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key to Flash Totals if Type = C - Assigned by Sys.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSCASH', @level2type=N'COLUMN',@level2name=N'FTKY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number Currently On - Else set to 999999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSCASH', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Name - From 1st Sign On' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSCASH', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security code - From 1st Sign On' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSCASH', @level2type=N'COLUMN',@level2name=N'SECC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Transactions for this Cashier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSCASH', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Transactions for this Cashier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSCASH', @level2type=N'COLUMN',@level2name=N'AMNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R S C A S H  =   Cashier Flash Totals File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSCASH'
END
GO

