﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PLANGRAM]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PLANGRAM'
CREATE TABLE [dbo].[PLANGRAM](
	[STORE] [char](3) NOT NULL,
	[PLANNO] [char](10) NOT NULL,
	[PLANNAME] [char](30) NOT NULL,
	[PLANSEGN] [char](10) NOT NULL,
	[FIXTURENO] [char](10) NOT NULL,
	[SKUN] [char](6) NOT NULL,
	[FACINGS] [char](10) NOT NULL,
	[DELE] [bit] NOT NULL,
	[CAPACITY] [int] NOT NULL,
	[PACK] [int] NOT NULL,
	[FIXTSEQN] [char](10) NOT NULL,
 CONSTRAINT [PK_PLANGRAM] PRIMARY KEY CLUSTERED 
(
	[STORE] ASC,
	[PLANNO] ASC,
	[PLANSEGN] ASC,
	[FIXTURENO] ASC,
	[FIXTSEQN] ASC,
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'STORE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Planogram Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'PLANNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Planogram Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'PLANNAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Planogram Segment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'PLANSEGN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fixture (shelf) number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'FIXTURENO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Facings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'FACINGS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delete Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'DELE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Shelf Capacity for this Planogram entry
   * Where there are multiple entries for the
   * same SKU in the Planogram tables, these
   * must be added together to obtain the total
   * shelf capacity for a SKU.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'CAPACITY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pack size for this item.
  * Where there are multiple entries for the
  * same SKU in the Planogram tables, the
  * pack size will be the same for all entries.
  * shelf capacity for a SKU.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'PACK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Sequence on fixture
  * This field will be generated as the PLANGRAM
  * file is created from a HOSTF file. It will
  * reflect the position of each item on any
  * fixture on which it appears.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM', @level2type=N'COLUMN',@level2name=N'FIXTSEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P L A N G R A M  =  Planogram Data File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PLANGRAM'
END
GO

