﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QUOHDR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table QUOHDR'
CREATE TABLE [dbo].[QUOHDR](
	[NUMB] [char](6) NOT NULL,
	[CUST] [char](10) NULL,
	[DATE1] [date] NULL,
	[CANC] [bit] NOT NULL,
	[EXPD] [date] NULL,
	[SOLD] [bit] NOT NULL,
	[DELI] [bit] NOT NULL,
	[MVST] [decimal](9, 2) NULL,
	[DCST] [decimal](9, 2) NULL,
	[WGHT] [decimal](7, 2) NULL,
	[VOLU] [decimal](7, 2) NULL,
	[QDAT] [date] NULL,
	[QTIL] [char](2) NULL,
	[QTRN] [char](4) NULL,
	[CARD_NO] [char](19) NULL,
	[FOCDEL] [char](3) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_QUOHDR] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Quote Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Number - See CUSMAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'CUST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Quote CANCELLED - ie, customer walked out' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'CANC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expiry Date - currently set at 24 hours' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'EXPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = This quote was sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'SOLD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = will be delivered
*
V A L U E S
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'DELI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Merchandise Value - Store' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'MVST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Charge - Store - total of delivery SKU
included in MVST value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'DCST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total transaction weight - future' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'WGHT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total transaction M3 volume - future
*
 Q U O T E   T R A N S A C T I O N    I N F O  
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'VOLU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sale date - DLTOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'QDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till number - DLTOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'QTIL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number - DLTOTS
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'QTRN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount Scheme Card Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'CARD_NO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Free Of Charge Delivery Supervisor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'FOCDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Q U O H D R  =  Store customer quote header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QUOHDR'
END
GO

