﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[COUPONMASTER]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table COUPONMASTER'
CREATE TABLE [dbo].[COUPONMASTER](
	[COUPONID] [char](7) NOT NULL,
	[DESCRIPTION] [char](40) NOT NULL,
	[STOREGEN] [bit] NOT NULL,
	[SERIALNO] [bit] NULL,
	[EXCCOUPON] [bit] NOT NULL,
	[REUSABLE] [bit] NULL,
	[COLLECTINFO] [bit] NOT NULL,
	[DELETED] [bit] NULL,
 CONSTRAINT [PK_COUPONMASTER] PRIMARY KEY CLUSTERED 
(
	[COUPONID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Coupon Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER', @level2type=N'COLUMN',@level2name=N'COUPONID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER', @level2type=N'COLUMN',@level2name=N'DESCRIPTION'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if Store generated False if Not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER', @level2type=N'COLUMN',@level2name=N'STOREGEN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Serial Number of the Coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER', @level2type=N'COLUMN',@level2name=N'SERIALNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Exclude coupon True = Excluded False = Included' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER', @level2type=N'COLUMN',@level2name=N'EXCCOUPON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Re-Usable True = Coupon is re-uasable False = Used only once' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER', @level2type=N'COLUMN',@level2name=N'REUSABLE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Collection Information ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER', @level2type=N'COLUMN',@level2name=N'COLLECTINFO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if the coupon has been deleted False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER', @level2type=N'COLUMN',@level2name=N'DELETED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O U P O N M A S T E R = Coupon Master File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPONMASTER'
END
GO

