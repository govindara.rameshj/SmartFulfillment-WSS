﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLOLIN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLOLIN'
CREATE TABLE [dbo].[DLOLIN](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [smallint] NOT NULL,
	[CNAM] [char](30) NULL,
	[CAD1] [char](30) NULL,
	[CAD2] [char](30) NULL,
	[CAD3] [char](30) NULL,
	[CAD4] [char](30) NULL,
	[CPST] [char](8) NULL,
	[CPHN] [char](15) NULL,
	[CFAX] [char](15) NULL,
	[EPRI] [decimal](7, 2) NULL,
	[EVAT] [bit] NOT NULL,
	[CPRI] [decimal](7, 2) NULL,
	[PREV] [bit] NOT NULL,
	[OSTR] [char](3) NULL,
	[OTIL] [char](2) NULL,
	[OTRN] [char](4) NULL,
	[FILL] [bit] NOT NULL,
	[ODAT] [date] NULL,
	[OPRI] [decimal](7, 2) NULL,
	[RTI] [char](1) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_DLOLIN] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line item number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CNAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CAD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CAD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CAD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CAD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Post Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CPST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CPHN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fax   Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CFAX'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Competitors price (Entered)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'EPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Entered Prices are VAT Inclusive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'EVAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Competitors price (Converted for comparison)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'CPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promise only.  ON = from a previous purchase' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'PREV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original Store Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'OSTR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original Till  Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'OTIL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original Trans Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'OTRN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Filler to ensure compatability with DDFs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'FILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'ODAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original Wickes Selling Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'OPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for Future Use (Was A98)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L O L I N  =   Price Match or Promise Line Item Information ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLOLIN'
END
GO

