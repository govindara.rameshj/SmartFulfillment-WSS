﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PlanSegments]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PlanSegments'
CREATE TABLE [dbo].[PlanSegments](
	[Number] [int] NOT NULL,
	[Segment] [tinyint] NOT NULL,
	[Name] [char](30) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_PlanSegments] PRIMARY KEY CLUSTERED 
(
	[Number] ASC,
	[Segment] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Segment Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanSegments', @level2type=N'COLUMN',@level2name=N'Number'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Segment ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanSegments', @level2type=N'COLUMN',@level2name=N'Segment'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Segment Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanSegments', @level2type=N'COLUMN',@level2name=N'Name'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if active false if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanSegments', @level2type=N'COLUMN',@level2name=N'IsActive'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P L A N S E G M E N T S = Plan Segments File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanSegments'
END
GO

