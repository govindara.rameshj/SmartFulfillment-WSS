﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CBSCSA]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CBSCSA'
CREATE TABLE [dbo].[CBSCSA](
	[DATE1] [date] NOT NULL,
	[CTGY] [char](6) NOT NULL,
	[ALPH] [char](50) NULL,
	[DESCR] [char](50) NULL,
	[SALV] [decimal](9, 2) NULL,
	[DONE] [bit] NOT NULL,
	[SPARE] [char](17) NULL,
 CONSTRAINT [PK_CBSCSA] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[CTGY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCSA', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCSA', @level2type=N'COLUMN',@level2name=N'CTGY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category Description (For Index sequencing)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCSA', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCSA', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net sales  value for Category' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCSA', @level2type=N'COLUMN',@level2name=N'SALV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if done false if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCSA', @level2type=N'COLUMN',@level2name=N'DONE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C B S C A S  =   Cash Balancing Cashier Record  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCSA'
END
GO

