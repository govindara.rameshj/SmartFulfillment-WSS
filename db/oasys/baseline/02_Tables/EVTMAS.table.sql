﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EVTMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table EVTMAS'
CREATE TABLE [dbo].[EVTMAS](
	[TYPE] [char](2) NOT NULL,
	[KEY1] [char](6) NOT NULL,
	[KEY2] [char](8) NOT NULL,
	[NUMB] [char](6) NOT NULL,
	[PRIO] [char](2) NULL,
	[IDEL] [bit] NOT NULL,
	[IDOW] [bit] NOT NULL,
	[SDAT] [date] NULL,
	[EDAT] [date] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[BQTY] [decimal](7, 0) NULL,
	[GQTY] [decimal](7, 0) NULL,
	[VDIS] [decimal](9, 2) NULL,
	[PDIS] [decimal](7, 3) NULL,
	[BUYCPN] [char](7) NOT NULL,
	[GETCPN] [char](7) NOT NULL,
 CONSTRAINT [PK_EVTMAS] PRIMARY KEY CLUSTERED 
(
	[TYPE] ASC,
	[KEY1] ASC,
	[KEY2] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Type - TS = Sku Temporary Price Change
TM = M&M Temporary Price Change
QS = Sku Quantity Break
QM = M&M Quantity Break - Future Use
QD = Deal Quantity Break - Future Use
MS = Sku Multibuy
MM = M&M Multibuy - Future Use
DG = Deal Group
W' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Key 1
for TS type = Sku Number
for TM type = M&M number
for QS type = Sku Number
for QM type = M&M Number
for QD type = Deal Number
for MS type = Buy Sku
for MM type = M&M Number
for DG type = Zero
for HS type = Hierarchy Category/Grou' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'KEY1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Key 2
for TS type = Zero
for TM type = Zero
for QS type = Sku Buy Quantity
for QM type = M&M Buy Quantity
for QD type = Deal Quantity
for MS type = Total Buy Quantity, i.e. BQTY + GQTY
for MM type = Total Buy Quantity, i.e. BQTY + GQTY
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'KEY2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Number (See EVTHDR)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Priority    - Highest Number = Highest Priority' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'PRIO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deleted Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This event is a Time of Day/Day of Week Event
(Set by Entry in EVTHDR:STIM/ETIM/DACT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'IDOW'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start Date  - from EVTHDR:SDAT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'SDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending Date - from EVTHDR:EDAT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'EDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price or Value - nnnnnn.nn-
for TS & TM types      = Item Price
for QS & QM & QD types = Total Buy Quantity Value
for MS & MM types = Zero
for DG type            = Total Deal Price
for HS type = Spend Level
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Buy Quantity - Does NOT Include Reward Quantity
(For MS & MM ONLY)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'BQTY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Get Quantity - NOT Included in Buy Quantity
(For MS & MM ONLY)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'GQTY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Discount - Zero if Discount Percentage is specified.
(For MS, MM and HS ONLY)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'VDIS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentage Discount
(For MS & MM and HS ONLY)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'PDIS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon number required to trigger this event
    * Zero = No coupon required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'BUYCPN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon number to be given as reward for event
    * Zero = No coupon given' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS', @level2type=N'COLUMN',@level2name=N'GETCPN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E V T M A S  =  Event Master File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTMAS'
END
GO

