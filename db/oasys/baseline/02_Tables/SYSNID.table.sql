﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SYSNID]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SYSNID'
CREATE TABLE [dbo].[SYSNID](
	[FKEY] [char](2) NOT NULL,
	[MAST] [bit] NOT NULL,
	[DATE1] [date] NULL,
	[SLAV1] [bit] NOT NULL,
	[SLAV2] [bit] NOT NULL,
	[SLAV3] [bit] NOT NULL,
	[SLAV4] [bit] NOT NULL,
	[SLAV5] [bit] NOT NULL,
	[SLAV6] [bit] NOT NULL,
	[SLAV7] [bit] NOT NULL,
	[SLAV8] [bit] NOT NULL,
	[SLAV9] [bit] NOT NULL,
	[SLAV10] [bit] NOT NULL,
	[SLAV11] [bit] NOT NULL,
	[SLAV12] [bit] NOT NULL,
	[SLAV13] [bit] NOT NULL,
	[SLAV14] [bit] NOT NULL,
	[SLAV15] [bit] NOT NULL,
	[SLAV16] [bit] NOT NULL,
	[SLAV17] [bit] NOT NULL,
	[SLAV18] [bit] NOT NULL,
	[SLAV19] [bit] NOT NULL,
	[SLAV20] [bit] NOT NULL,
	[SLAV21] [bit] NOT NULL,
	[SLAV22] [bit] NOT NULL,
	[SLAV23] [bit] NOT NULL,
	[SLAV24] [bit] NOT NULL,
	[SLAV25] [bit] NOT NULL,
	[SLAV26] [bit] NOT NULL,
	[SLAV27] [bit] NOT NULL,
	[SLAV28] [bit] NOT NULL,
	[SLAV29] [bit] NOT NULL,
	[SLAV30] [bit] NOT NULL,
	[SLAV31] [bit] NOT NULL,
	[TSPD] [decimal](3, 0) NULL,
	[MPRT] [char](4) NULL,
	[BPRT] [char](4) NULL,
 CONSTRAINT [PK_SYSNID] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'File Key - Always 01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Master opened for below date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'MAST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date master opened' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Slave has run GDIDAY , ie OPENED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'SLAV31'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UX TRANSFER speed - first three digits
   only are required;
2400,4800,9600,19200,38400,57600,115200
~~~  ~~~  ~~~  ~~~   ~~~   ~~~   ~~~' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'TSPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Serial port used by Main PC = COM1 or COM2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'MPRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Serial port used by B/Up PC = COM1 or COM2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID', @level2type=N'COLUMN',@level2name=N'BPRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S N I D    =  Network ID system record ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNID'
END
GO

