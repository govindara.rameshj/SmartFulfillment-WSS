﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MODDET]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table MODDET'
CREATE TABLE [dbo].[MODDET](
	[NUMB] [char](6) NOT NULL,
	[COMP] [char](6) NOT NULL,
	[CPER] [decimal](5, 0) NULL,
 CONSTRAINT [PK_MODDET] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[COMP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Model Number - Second digit must be a 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODDET', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Component Sku Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODDET', @level2type=N'COLUMN',@level2name=N'COMP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity of Component Sku per Model' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODDET', @level2type=N'COLUMN',@level2name=N'CPER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'M O D D E T  =  Model Detail File Definition ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODDET'
END
GO

