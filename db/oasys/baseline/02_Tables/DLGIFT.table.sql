﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLGIFT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLGIFT'
CREATE TABLE [dbo].[DLGIFT](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[LINE] [smallint] NOT NULL,
	[SERI] [char](8) NOT NULL,
	[EEID] [char](3) NULL,
	[TYPE] [char](2) NULL,
	[AMNT] [decimal](9, 2) NULL,
	[COMM] [bit] NOT NULL,
	[RTI] [char](1) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_DLGIFT] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[LINE] ASC,
	[SERI] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id - Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number from PC Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'LINE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Serial Number of Gift Token' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'SERI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee ID performing sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Type
"SA" - Sale Of Voucher                - Allocation
"TS" - Tender In Sale                 - Redemption
"TR" - Tender In Refund               - Allocation
"RR" - Refund Of Voucher              - Redemption
"CS" - Cancel/line rever' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Amount - Opposite sign of Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'AMNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02  Indicate COMMED to H/O.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'COMM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Reduced from A99) *W02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L G I F T  =   Daily Gift Token Transaction Lines.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFT'
END
GO

