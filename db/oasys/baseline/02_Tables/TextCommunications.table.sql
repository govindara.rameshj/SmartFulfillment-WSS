﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TextCommunications]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TextCommunications'
CREATE TABLE [dbo].[TextCommunications](
	[Id] [int] NOT NULL,
	[FunctionalAreaId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Description] [varchar](100) NULL,
	[SummaryText] [varchar](50) NULL,
	[FullText] [text] NOT NULL,
	[LineDelimiter] [varchar](10) NULL,
	[ParagraphDelimiter] [varchar](10) NULL,
	[FieldStartDelimiter] [varchar](10) NULL,
	[FieldEndDelimiter] [varchar](10) NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_TextCommunications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

