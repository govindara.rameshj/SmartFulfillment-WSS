﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[IslandTypes]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table IslandTypes'
CREATE TABLE [dbo].[IslandTypes](
	[Code] [char](1) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[AllowedCodes] [varchar](100) NOT NULL,
	[Zorder] [int] NOT NULL,
	[BorderColour] [varchar](30) NOT NULL,
 CONSTRAINT [PK_IslandTypes] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Code Number 1 character' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandTypes', @level2type=N'COLUMN',@level2name=N'Code'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandTypes', @level2type=N'COLUMN',@level2name=N'Description'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Display Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandTypes', @level2type=N'COLUMN',@level2name=N'DisplayOrder'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alowed Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandTypes', @level2type=N'COLUMN',@level2name=N'AllowedCodes'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Zorder on Window' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandTypes', @level2type=N'COLUMN',@level2name=N'Zorder'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Border Colour' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandTypes', @level2type=N'COLUMN',@level2name=N'BorderColour'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'I S L A N D T Y P E S = Island Types File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'IslandTypes'
END
GO

