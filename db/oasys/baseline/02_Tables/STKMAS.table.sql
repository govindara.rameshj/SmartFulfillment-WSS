﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[STKMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table STKMAS'
CREATE TABLE [dbo].[STKMAS](
	[SKUN] [char](6) NOT NULL,
	[PLUD] [char](20) NULL,
	[DESCR] [char](40) NULL,
	[FILL] [char](10) NULL,
	[DEPT] [char](2) NULL,
	[GROU] [char](3) NULL,
	[VATC] [char](1) NULL,
	[BUYU] [char](4) NULL,
	[SUPP] [char](5) NULL,
	[PROD] [char](10) NULL,
	[PACK] [int] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[PPRI] [decimal](9, 2) NULL,
	[COST] [decimal](11, 4) NULL,
	[DSOL] [date] NULL,
	[DREC] [date] NULL,
	[DORD] [date] NULL,
	[DPRC] [date] NULL,
	[DSET] [date] NULL,
	[DOBS] [date] NULL,
	[DDEL] [date] NULL,
	[ONHA] [int] NULL,
	[ONOR] [int] NULL,
	[MINI] [int] NULL,
	[MAXI] [int] NULL,
	[SOLD] [decimal](7, 0) NULL,
	[ISTA] [char](1) NULL,
	[IRIS] [bit] NOT NULL,
	[IRIB] [smallint] NULL,
	[IMDN] [bit] NOT NULL,
	[ICAT] [bit] NOT NULL,
	[IOBS] [bit] NOT NULL,
	[IDEL] [bit] NOT NULL,
	[ILOC] [smallint] NULL,
	[IEAN] [smallint] NULL,
	[IPPC] [smallint] NULL,
	[ITAG] [bit] NOT NULL,
	[INON] [bit] NOT NULL,
	[SALV1] [decimal](9, 2) NULL,
	[SALV2] [decimal](9, 2) NULL,
	[SALV3] [decimal](9, 2) NULL,
	[SALV4] [decimal](9, 2) NULL,
	[SALV5] [decimal](9, 2) NULL,
	[SALV6] [decimal](9, 2) NULL,
	[SALV7] [decimal](9, 2) NULL,
	[SALU1] [int] NULL,
	[SALU2] [int] NULL,
	[SALU3] [int] NULL,
	[SALU4] [int] NULL,
	[SALU5] [int] NULL,
	[SALU6] [int] NULL,
	[SALU7] [int] NULL,
	[CPDO] [smallint] NULL,
	[CYDO] [smallint] NULL,
	[AWSF] [decimal](9, 2) NULL,
	[AW13] [decimal](9, 2) NULL,
	[DATS] [date] NULL,
	[UWEK] [smallint] NULL,
	[FLAG] [bit] NOT NULL,
	[CFLG] [bit] NOT NULL,
	[US001] [int] NULL,
	[US002] [int] NULL,
	[US003] [int] NULL,
	[US004] [int] NULL,
	[US005] [int] NULL,
	[US006] [int] NULL,
	[US007] [int] NULL,
	[US008] [int] NULL,
	[US009] [int] NULL,
	[US0010] [int] NULL,
	[US0011] [int] NULL,
	[US0012] [int] NULL,
	[US0013] [int] NULL,
	[US0014] [int] NULL,
	[DO001] [smallint] NULL,
	[DO002] [smallint] NULL,
	[DO003] [smallint] NULL,
	[DO004] [smallint] NULL,
	[DO005] [smallint] NULL,
	[DO006] [smallint] NULL,
	[DO007] [smallint] NULL,
	[DO008] [smallint] NULL,
	[DO009] [smallint] NULL,
	[DO0010] [smallint] NULL,
	[DO0011] [smallint] NULL,
	[DO0012] [smallint] NULL,
	[DO0013] [smallint] NULL,
	[DO0014] [smallint] NULL,
	[SQ04] [int] NULL,
	[SQ13] [int] NULL,
	[SQ01] [int] NULL,
	[SQOR] [bit] NOT NULL,
	[TREQ] [int] NULL,
	[TREV] [decimal](9, 2) NULL,
	[TACT] [bit] NOT NULL,
	[RETQ] [int] NULL,
	[RETV] [decimal](9, 2) NULL,
	[CHKD] [char](1) NULL,
	[LABN] [smallint] NOT NULL,
	[LABS] [char](1) NULL,
	[STON] [char](6) NULL,
	[STOQ] [int] NULL,
	[SOD1] [char](1) NULL,
	[SOD2] [char](1) NULL,
	[SOD3] [char](1) NULL,
	[LABM] [smallint] NOT NULL,
	[LABL] [smallint] NOT NULL,
	[WGHT] [decimal](7, 2) NOT NULL,
	[VOLU] [decimal](7, 2) NOT NULL,
	[NOOR] [bit] NOT NULL,
	[SUP1] [char](5) NULL,
	[IODT] [date] NULL,
	[FODT] [date] NULL,
	[PMIN] [int] NULL,
	[PMSD] [date] NULL,
	[PMED] [date] NULL,
	[PMCP] [char](1) NULL,
	[SMAN] [int] NULL,
	[DFLC] [date] NULL,
	[IMCP] [bit] NOT NULL,
	[FOLT] [int] NULL,
	[IDEA] [int] NULL,
	[QADJ] [decimal](5, 2) NULL,
	[TAGF] [char](1) NULL,
	[ALPH] [char](20) NULL,
	[CTGY] [char](6) NOT NULL,
	[GRUP] [char](6) NOT NULL,
	[SGRP] [char](6) NOT NULL,
	[STYL] [char](6) NOT NULL,
	[EQUV] [char](40) NULL,
	[HFIL] [char](12) NULL,
	[REVT] [char](6) NULL,
	[RPRI] [char](2) NULL,
	[IWAR] [bit] NOT NULL,
	[IOFF] [smallint] NULL,
	[ISOL] [smallint] NULL,
	[QUAR] [char](1) NULL,
	[IPRD] [bit] NOT NULL,
	[EQPM] [decimal](11, 6) NULL,
	[EQPU] [char](7) NULL,
	[MDNQ] [int] NULL,
	[WTFQ] [int] NULL,
	[SALT] [char](1) NULL,
	[MODT] [char](1) NULL,
	[AAPC] [bit] NOT NULL,
	[IPSK] [bit] NOT NULL,
	[AADJ] [char](1) NULL,
	[SUP2] [char](5) NULL,
	[FRAG] [bit] NOT NULL,
	[TIMB] [smallint] NULL,
	[ELEC] [bit] NOT NULL,
	[SOFS] [int] NOT NULL,
	[WQTY] [int] NULL,
	[WRAT] [char](2) NULL,
	[WSEQ] [char](2) NULL,
	[BALI] [char](1) NULL,
	[PRFSKU] [char](6) NULL,
	[MSPR1] [decimal](9, 2) NOT NULL,
	[MSPR2] [decimal](9, 2) NOT NULL,
	[MSPR3] [decimal](9, 2) NOT NULL,
	[MSPR4] [decimal](9, 2) NOT NULL,
	[MSTQ1] [int] NOT NULL,
	[MSTQ2] [int] NOT NULL,
	[MSTQ3] [int] NOT NULL,
	[MSTQ4] [int] NOT NULL,
	[MREQ1] [int] NOT NULL,
	[MREQ2] [int] NOT NULL,
	[MREQ3] [int] NOT NULL,
	[MREQ4] [int] NOT NULL,
	[MREV1] [decimal](9, 2) NOT NULL,
	[MREV2] [decimal](9, 2) NOT NULL,
	[MREV3] [decimal](9, 2) NOT NULL,
	[MREV4] [decimal](9, 2) NOT NULL,
	[MADQ1] [int] NOT NULL,
	[MADQ2] [int] NOT NULL,
	[MADQ3] [int] NOT NULL,
	[MADQ4] [int] NOT NULL,
	[MADV1] [decimal](9, 2) NOT NULL,
	[MADV2] [decimal](9, 2) NOT NULL,
	[MADV3] [decimal](9, 2) NOT NULL,
	[MADV4] [decimal](9, 2) NOT NULL,
	[MPVV1] [decimal](9, 2) NOT NULL,
	[MPVV2] [decimal](9, 2) NOT NULL,
	[MPVV3] [decimal](9, 2) NOT NULL,
	[MPVV4] [decimal](9, 2) NOT NULL,
	[MIBQ1] [int] NOT NULL,
	[MIBQ2] [int] NOT NULL,
	[MIBQ3] [int] NOT NULL,
	[MIBQ4] [int] NOT NULL,
	[MIBV1] [decimal](9, 2) NOT NULL,
	[MIBV2] [decimal](9, 2) NOT NULL,
	[MIBV3] [decimal](9, 2) NOT NULL,
	[MIBV4] [decimal](9, 2) NOT NULL,
	[MRTQ1] [int] NOT NULL,
	[MRTQ2] [int] NOT NULL,
	[MRTQ3] [int] NOT NULL,
	[MRTQ4] [int] NOT NULL,
	[MRTV1] [decimal](9, 2) NOT NULL,
	[MRTV2] [decimal](9, 2) NOT NULL,
	[MRTV3] [decimal](9, 2) NOT NULL,
	[MRTV4] [decimal](9, 2) NOT NULL,
	[MCCV1] [decimal](9, 2) NOT NULL,
	[MCCV2] [decimal](9, 2) NOT NULL,
	[MCCV3] [decimal](9, 2) NOT NULL,
	[MCCV4] [decimal](9, 2) NOT NULL,
	[MDRV1] [decimal](9, 2) NOT NULL,
	[MDRV2] [decimal](9, 2) NOT NULL,
	[MDRV3] [decimal](9, 2) NOT NULL,
	[MDRV4] [decimal](9, 2) NOT NULL,
	[MBSQ1] [int] NOT NULL,
	[MBSQ2] [int] NOT NULL,
	[MBSQ3] [int] NOT NULL,
	[MBSQ4] [int] NOT NULL,
	[MBSV1] [decimal](9, 2) NOT NULL,
	[MBSV2] [decimal](9, 2) NOT NULL,
	[MBSV3] [decimal](9, 2) NOT NULL,
	[MBSV4] [decimal](9, 2) NOT NULL,
	[MSTP1] [int] NOT NULL,
	[MSTP2] [int] NOT NULL,
	[MSTP3] [int] NOT NULL,
	[MSTP4] [int] NOT NULL,
	[FIL11] [decimal](9, 2) NOT NULL,
	[FIL12] [decimal](9, 2) NOT NULL,
	[FIL13] [decimal](9, 2) NOT NULL,
	[FIL14] [decimal](9, 2) NOT NULL,
	[ToForecast] [bit] NOT NULL,
	[IsInitialised] [bit] NOT NULL,
	[DemandPattern] [char](10) NULL,
	[PeriodDemand] [decimal](9, 2) NOT NULL,
	[PeriodTrend] [decimal](7, 2) NOT NULL,
	[ErrorForecast] [decimal](7, 2) NOT NULL,
	[ErrorSmoothed] [decimal](7, 2) NOT NULL,
	[BufferConversion] [decimal](5, 2) NOT NULL,
	[BufferStock] [int] NOT NULL,
	[ServiceLevelOverride] [decimal](5, 2) NOT NULL,
	[ValueAnnualUsage] [char](2) NULL,
	[OrderLevel] [int] NOT NULL,
	[SaleWeightSeason] [int] NOT NULL,
	[SaleWeightBank] [int] NOT NULL,
	[SaleWeightPromo] [int] NOT NULL,
	[DateLastSoqInit] [date] NULL,
	[StockLossPerWeek] [decimal](11, 4) NOT NULL,
	[FlierPeriod1] [char](1) NULL,
	[FlierPeriod2] [char](1) NULL,
	[FlierPeriod3] [char](1) NULL,
	[FlierPeriod4] [char](1) NULL,
	[FlierPeriod5] [char](1) NULL,
	[FlierPeriod6] [char](1) NULL,
	[FlierPeriod7] [char](1) NULL,
	[FlierPeriod8] [char](1) NULL,
	[FlierPeriod9] [char](1) NULL,
	[FlierPeriod10] [char](1) NULL,
	[FlierPeriod11] [char](1) NULL,
	[FlierPeriod12] [char](1) NULL,
	[SalesBias0] [decimal](5, 3) NOT NULL,
	[SalesBias1] [decimal](5, 3) NOT NULL,
	[SalesBias2] [decimal](5, 3) NOT NULL,
	[SalesBias3] [decimal](5, 3) NOT NULL,
	[SalesBias4] [decimal](5, 3) NOT NULL,
	[SalesBias5] [decimal](5, 3) NOT NULL,
	[SalesBias6] [decimal](5, 3) NOT NULL,
	[DateLastSold] [date] NULL,
 CONSTRAINT [PK_STKMAS] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number - Valid are 000001 to 999999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short Description - Upper Case only' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PLUD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Filler - See New ALPH field' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W13 Department Number - Valid are 01 to 99
No Longer Used  *W13 *' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DEPT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W13 Group within Department - Valid are 001 to 999
No Longer Used  *W13 *' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'GROU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Code - See Retopt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'VATC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Buying Unit of Measure Code  (example = BOX )' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'BUYU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number of this item - Key to SUPMAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SUPP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Product Code - Suppliers Number of this item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PROD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of items per Supplier Pack  0001 to 9999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PACK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Normal Selling Price as xxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior Selling Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item Cost if Normal item (not Catch-all) See IMICAT
Catch all = Value of Item(s) at RETAIL
= Affected by Sales/Receipts/Adjustments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'COST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DSOL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Receipt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DREC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Purchase Order Placed for this item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DORD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Price Change Put into effect' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DPRC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the Item was Setup on this stores file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DSET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item set OBSOLETE - See IMIOBS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DOBS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item set DELETED  - See IMIDEL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock on Hand
W12 This no longer include items which are identified
W12 for mark down or write off - see :MDNQ & :WTFQ.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ONHA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On order from the Supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ONOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimum Stock to Maintain on the Shelf
Minimum also known as Impact level' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MINI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum Stock to Maintain on the Shelf (CAPACITY)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MAXI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Sold Today                - Not Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SOLD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Defined Item Status Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ISTA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Related Items Single ?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IRIS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of times item is a Related BULK 0 to 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IRIB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12 Mark-down item - On = yes
W12 NO LONGER USED - REMOVED FROM ALL CODING' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IMDN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dummy Item ? (Track Value only - not Stock)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ICAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Head Office Obsolete ? -- Stop Ordering item.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IOBS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Head Office Deleted  ? -- Ready for Physical Del.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Alternate Location Codes 0 to 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ILOC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of EANMAS records this item - 0 to 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IEAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of PRCCHG records this item - 0 to 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IPPC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tagged item, remove at till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ITAG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Non Stocked item indicator - Y = non stocked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'INON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALU1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALU2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALU3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALU4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALU5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALU6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALU7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days out of stock     (current period. ie PTD)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'CPDO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days out of stock     (current year.   ie YTD)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'CYDO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Average Weekly Sales  (Calc"d using at least 4 Wks)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'AWSF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Average weekly sales  (Calc"d using all 13 weeks)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'AW13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date item 1st had an on-hand Quantity to sell' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DATS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of weeks Updated - 00 to 13 - 13=Max' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'UWEK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Weekly Update Occured - Allows restart' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FLAG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Confident of Average Weekly Sales Calc.
Off = < 4 weeks Data or OOS for 1/2 of Calc Period' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'CFLG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US001'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US002'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US003'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US004'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US005'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US006'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US007'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US008'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US009'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US0010'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US0011'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US0012'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US0013'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Units Sold Current Week and 13 More' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'US0014'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO001'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO002'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO003'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO004'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO005'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO006'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO007'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO008'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO009'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO0010'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO0011'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO0012'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO0013'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which Days and Calculated how many curr+13
Mon=1  Tue=2  Wed=4  Thu=8  Fri=16  Sat=32  Sun=64' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DO0014'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Soq calculated using AWSF ( 4 week average)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SQ04'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Soq calculated using AW13 (13 week average)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SQ13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Soq calculated using SALU (last weeks sales)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SQ01'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Order placed this SOQ  Off=ok to add this line' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SQOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Todays receipts quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'TREQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Todays receipts value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'TREV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Todays activity flag - on = activity today' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'TACT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open Returns quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'RETQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open Returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'RETV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H/O Item check digit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'CHKD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Small  labels to print' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'LABN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'redundant - not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'LABS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Standing order number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'STON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Standing order quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'STOQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Standing order day 1 - valid = 1 - no. of days open' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SOD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Standing order day 2 - valid = 1 - no. of days open' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SOD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Standing order day 3 - valid = 1 - no. of days open' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SOD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Medium labels to print' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'LABM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Large  labels to print' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'LABL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item weight' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'WGHT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item volume' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'VOLU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Order Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'NOOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W08 Alternate Supplier - now used for internal depot
W08 supplier number if supplied from warehouse stock.
W09 If not equal to IM:SUPP then is an alternate supplier
W09 If equal to IM:SUPP then is a Primary Supplier
W09 Should always have a value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SUP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Inital Order Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IODT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Final Order Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FODT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Promotional Minimum' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PMIN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Promotional Minimum Start Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PMSD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Promotional Minimum End Date
W21:PMCP    A1       *W06 Promotional MCP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PMED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W21 Participating in MCP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PMCP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Spaceman Display Factor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SMAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Date of Last Change to Mimimum by Store' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DFLC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Participating in MCP - No Longer Used     *W21' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IMCP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 NOT Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FOLT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 NOT Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IDEA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 NOT Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'QADJ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Tagged Identifier
W06 Blank = Not Tagged
W06 S     = Supplier Tag
W06 C     = Concealed Tag
W06 L     = Local Tag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'TAGF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Alpha Lookup Key - Used by Enquiry Program,
Set from description of department product group
No Longer Used  *W13 *' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Hierarchy Category  (Level 5 Hierarchy Number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'CTGY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04           Group     (Level 4 Hierarchy Number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'GRUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04           Sub-Group (Level 3 Hierarchy Number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SGRP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04           Style     (Level 2 Hierarchy Number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'STYL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Product Equivalent Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'EQUV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Hierarchy Category & Group as a C12
W05 - Used for Fuzzy Matching Filter.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'HFIL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Retail Price (IM:PRIC) Event Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'REVT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Retail Price (IM:PRIC) Priority' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'RPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W07 Warranty Item?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IWAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W07 Offensive Weapon age restriction 16' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IOFF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W07 Solvent age restriction 18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ISOL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W08 Quarantine Flag
W08 Blank = Not Quarantined
W08     Q = Quarantined
W08     B = Batch Quarantined Only' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'QUAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W08 Y = Item has a Pricing Discrepancy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IPRD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Equivalent Price Multiplier
*W11 If zero then equivalent price does not exist' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'EQPM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Equivalent price unit
*W11 Foe Example - Litre.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'EQPU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12 Mark-down Stock
W12 This is independent of :ONHA, but is still part of
W12 overall stock valuation as the stock is not yet
W12 devalued.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MDNQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12 Write Off Stock
W12 This is independent of :ONHA, but is still part of
W12 overall stock valuation as the stock is not yet
W12 devalued.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'WTFQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12 Sale Type Attribute
W13       B = Basic Item
W13       P = Performance Item
W13       A = Aesthetic Item
W13       S = Showroom Item
W13       G = Good Pallet Item
W13       R = Reduced Pallet Item
W13       D = Delivery Item
W13       I ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SALT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12 Model Type Attribute
W14 No longer used   *W12       S = Standard Model
W14 No longer used   *W12       E = Express Model
W14 No longer used   *W12   blank = not a model item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MODT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12 Y = Auto Apply Price Changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'AAPC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W14 Y = Palleted Sku
Causes till prompt "Pallet Deposit required?"
W17:AADJ    I       *W15 Blank = Do not allow type 58/59 adjustments (Default)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'IPSK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W23 Blank = Do not allow type 57/58/59 adjustments (Default)
W15     1 = Allow type 58 adjustments
W15     2 = Allow type 58 adjustments
W15     3 = Allow type 58 & type 59 adjustments
W23     4 = Allow type 57 adjustments
W23     5 = Allow type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'AADJ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W20 Saved supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SUP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W22 "Y" = Fragile Item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FRAG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W22 % Managed Timber Content' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'TIMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W22 "Y" = Electrical Item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ELEC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W22 "Y" = Stock Held Off Sale
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SOFS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Web Order Allocated Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'WQTY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weight Rating System       *NOT USED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'WRAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weight Rating Sequence No. *NOT USED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'WSEQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Ballast Legislation Item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'BALI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Producers Recycling Fund Sku' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PRFSKU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start Price  - Was MSTP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSPR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start Price  - Was MSTP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSPR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start Price  - Was MSTP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSPR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start Price  - Was MSTP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSPR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSTQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSTQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSTQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSTQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MREQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MREQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MREQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MREQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MREV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MREV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MREV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MREV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MADQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MADQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MADQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MADQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MADV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MADV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MADV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MADV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price violations value Calculated as
Retail - Actual = violation value
ie Normal Price 8.99 sold for 6.99
gives Price violation of 2.00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MPVV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price violations value Calculated as
Retail - Actual = violation value
ie Normal Price 8.99 sold for 6.99
gives Price violation of 2.00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MPVV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price violations value Calculated as
Retail - Actual = violation value
ie Normal Price 8.99 sold for 6.99
gives Price violation of 2.00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MPVV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price violations value Calculated as
Retail - Actual = violation value
ie Normal Price 8.99 sold for 6.99
gives Price violation of 2.00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MPVV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/Out NET quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MIBQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/Out NET quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MIBQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/Out NET quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MIBQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/Out NET quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MIBQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/OUT NET value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MIBV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/OUT NET value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MIBV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/OUT NET value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MIBV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/OUT NET value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MIBV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MRTQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MRTQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MRTQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MRTQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MRTV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MRTV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MRTV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MRTV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cyclical count value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MCCV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cyclical count value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MCCV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cyclical count value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MCCV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cyclical count value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MCCV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL adjustment value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MDRV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL adjustment value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MDRV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL adjustment value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MDRV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL adjustment value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MDRV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MBSQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MBSQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MBSQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MBSQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MBSV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MBSV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MBSV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MBSV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NO LONGER USED - See MSPR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSTP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NO LONGER USED - See MSPR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSTP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NO LONGER USED - See MSPR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSTP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NO LONGER USED - See MSPR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'MSTP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Use with MSTP if needed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FIL11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Use with MSTP if needed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FIL12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Use with MSTP if needed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FIL13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Use with MSTP if needed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FIL14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Demand Pattern' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DemandPattern'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Demand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PeriodDemand'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Trend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'PeriodTrend'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Error Forecast' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ErrorForecast'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Error Smoothed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ErrorSmoothed'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Buffer Conversion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'BufferConversion'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Buffer Stock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'BufferStock'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Service Level Override' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ServiceLevelOverride'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Annual Usage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'ValueAnnualUsage'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Level' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'OrderLevel'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pattern Season' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SaleWeightSeason'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pattern Bank Holiday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SaleWeightBank'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pattern Promotion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SaleWeightPromo'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Last Forecast Initialized' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'DateLastSoqInit'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Per Week' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'StockLossPerWeek'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flier Period 12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'FlierPeriod12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Bias 0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SalesBias0'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Bias 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SalesBias1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Bias 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SalesBias2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Bias 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SalesBias3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Bias 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SalesBias4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Bias 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SalesBias5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Bias 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS', @level2type=N'COLUMN',@level2name=N'SalesBias6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S T K M A S  =  Stock Master File Definition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKMAS'
END
GO

