﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PICAUD]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PICAUD'
CREATE TABLE [dbo].[PICAUD](
	[DATE1] [date] NOT NULL,
	[SKUN] [char](6) NOT NULL,
	[IDEL] [bit] NOT NULL,
	[ORIG] [char](1) NULL,
 CONSTRAINT [PK_PICAUD] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICAUD', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICAUD', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Deleted exclude from count - Set from HOSTC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICAUD', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A = Audit entered (via HOSTC)
S = Store added' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICAUD', @level2type=N'COLUMN',@level2name=N'ORIG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' P I C A U D =   Additional PIC Count items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICAUD'
END
GO

