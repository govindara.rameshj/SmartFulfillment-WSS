﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionTotal]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table VisionTotal'
CREATE TABLE [dbo].[VisionTotal](
	[TranDate] [date] NOT NULL,
	[TillId] [int] NOT NULL,
	[TranNumber] [int] NOT NULL,
	[CashierId] [int] NULL,
	[TranDateTime] [char](8) NULL,
	[Type] [char](2) NULL,
	[ReasonCode] [char](2) NULL,
	[ReasonDescription] [varchar](50) NULL,
	[OrderNumber] [char](6) NULL,
	[Value] [decimal](9, 2) NOT NULL,
	[ValueMerchandising] [decimal](9, 2) NOT NULL,
	[ValueNonMerchandising] [decimal](9, 2) NOT NULL,
	[ValueTax] [decimal](9, 2) NOT NULL,
	[ValueDiscount] [decimal](9, 2) NOT NULL,
	[ValueColleagueDiscount] [decimal](9, 2) NOT NULL,
	[IsColleagueDiscount] [bit] NOT NULL,
	[IsPivotal] [bit] NOT NULL,
 CONSTRAINT [PK_VisionTotals] PRIMARY KEY CLUSTERED 
(
	[TranDate] ASC,
	[TillId] ASC,
	[TranNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

