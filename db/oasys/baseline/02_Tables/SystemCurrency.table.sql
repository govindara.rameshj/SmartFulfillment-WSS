﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemCurrency]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SystemCurrency'
CREATE TABLE [dbo].[SystemCurrency](
	[ID] [char](3) NOT NULL,
	[Description] [char](40) NOT NULL,
	[Symbol] [char](3) NOT NULL,
	[PrintSymbol] [char](3) NOT NULL,
	[ActiveDate] [date] NULL,
	[InactiveDate] [date] NULL,
	[LargestDenom] [int] NOT NULL,
	[MinCashMultiple] [decimal](9, 2) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[MaxAccepted] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_SystemCurrency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'Description'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'Symbol'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Symbol used when printing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'PrintSymbol'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date active' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'ActiveDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date inactive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'InactiveDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lagest Denomination (i.e. 50 pound note for serling)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'LargestDenom'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimum Cash Multiple (e.g. 1.00 for pound sterling)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'MinCashMultiple'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order to display in' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'DisplayOrder'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If the default currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'IsDefault'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum Amount In total that will be accepted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency', @level2type=N'COLUMN',@level2name=N'MaxAccepted'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S T E M C U R R E N C Y = System Currency File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrency'
END
GO

