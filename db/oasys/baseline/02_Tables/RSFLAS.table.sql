﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RSFLAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RSFLAS'
CREATE TABLE [dbo].[RSFLAS](
	[FKEY] [int] IDENTITY(1,1) NOT NULL,
	[FLOT] [decimal](9, 2) NULL,
	[PICK] [decimal](9, 2) NULL,
	[NUMB1] [decimal](5, 0) NULL,
	[NUMB2] [decimal](5, 0) NULL,
	[NUMB3] [decimal](5, 0) NULL,
	[NUMB4] [decimal](5, 0) NULL,
	[NUMB5] [decimal](5, 0) NULL,
	[NUMB6] [decimal](5, 0) NULL,
	[NUMB7] [decimal](5, 0) NULL,
	[NUMB8] [decimal](5, 0) NULL,
	[NUMB9] [decimal](5, 0) NULL,
	[NUMB10] [decimal](5, 0) NULL,
	[NUMB11] [decimal](5, 0) NULL,
	[NUMB12] [decimal](5, 0) NULL,
	[NUMB13] [decimal](5, 0) NULL,
	[TIME1] [char](4) NULL,
	[TIME2] [char](4) NULL,
	[TIME3] [char](4) NULL,
	[TIME4] [char](4) NULL,
	[TIME5] [char](4) NULL,
	[TIME6] [char](4) NULL,
	[TIME7] [char](4) NULL,
	[TIME8] [char](4) NULL,
	[TIME9] [char](4) NULL,
	[TIME10] [char](4) NULL,
	[TIME11] [char](4) NULL,
	[TIME12] [char](4) NULL,
	[TIME13] [char](4) NULL,
	[AMNT1] [decimal](9, 2) NULL,
	[AMNT2] [decimal](9, 2) NULL,
	[AMNT3] [decimal](9, 2) NULL,
	[AMNT4] [decimal](9, 2) NULL,
	[AMNT5] [decimal](9, 2) NULL,
	[AMNT6] [decimal](9, 2) NULL,
	[AMNT7] [decimal](9, 2) NULL,
	[AMNT8] [decimal](9, 2) NULL,
	[AMNT9] [decimal](9, 2) NULL,
	[AMNT10] [decimal](9, 2) NULL,
	[AMNT11] [decimal](9, 2) NULL,
	[AMNT12] [decimal](9, 2) NULL,
	[AMNT13] [decimal](9, 2) NULL,
	[TTCO1] [decimal](5, 0) NULL,
	[TTCO2] [decimal](5, 0) NULL,
	[TTCO3] [decimal](5, 0) NULL,
	[TTCO4] [decimal](5, 0) NULL,
	[TTCO5] [decimal](5, 0) NULL,
	[TTCO6] [decimal](5, 0) NULL,
	[TTCO7] [decimal](5, 0) NULL,
	[TTCO8] [decimal](5, 0) NULL,
	[TTCO9] [decimal](5, 0) NULL,
	[TTCO10] [decimal](5, 0) NULL,
	[TTCO11] [decimal](5, 0) NULL,
	[TTCO12] [decimal](5, 0) NULL,
	[TTCO13] [decimal](5, 0) NULL,
	[TTCO14] [decimal](5, 0) NULL,
	[TTCO15] [decimal](5, 0) NULL,
	[TTCO16] [decimal](5, 0) NULL,
	[TTCO17] [decimal](5, 0) NULL,
	[TTCO18] [decimal](5, 0) NULL,
	[TTCO19] [decimal](5, 0) NULL,
	[TTCO20] [decimal](5, 0) NULL,
	[TTAM1] [decimal](9, 2) NULL,
	[TTAM2] [decimal](9, 2) NULL,
	[TTAM3] [decimal](9, 2) NULL,
	[TTAM4] [decimal](9, 2) NULL,
	[TTAM5] [decimal](9, 2) NULL,
	[TTAM6] [decimal](9, 2) NULL,
	[TTAM7] [decimal](9, 2) NULL,
	[TTAM8] [decimal](9, 2) NULL,
	[TTAM9] [decimal](9, 2) NULL,
	[TTAM10] [decimal](9, 2) NULL,
	[TTAM11] [decimal](9, 2) NULL,
	[TTAM12] [decimal](9, 2) NULL,
	[TTAM13] [decimal](9, 2) NULL,
	[TTAM14] [decimal](9, 2) NULL,
	[TTAM15] [decimal](9, 2) NULL,
	[TTAM16] [decimal](9, 2) NULL,
	[TTAM17] [decimal](9, 2) NULL,
	[TTAM18] [decimal](9, 2) NULL,
	[TTAM19] [decimal](9, 2) NULL,
	[TTAM20] [decimal](9, 2) NULL,
 CONSTRAINT [PK_RSFLAS] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flash Totals Key assigned by Retail System' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting float Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'FLOT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup Amount so far today' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'PICK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of Number of This Tran Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'NUMB13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time as HHMM of last - 24 hour clock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TIME13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Value of this Transaction Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'AMNT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTCO20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS', @level2type=N'COLUMN',@level2name=N'TTAM20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R S F L A S  =   Daily Flash Totals by either Cashier or Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSFLAS'
END
GO

