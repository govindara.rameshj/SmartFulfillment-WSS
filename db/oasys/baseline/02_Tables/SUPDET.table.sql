﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SUPDET]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SUPDET'
CREATE TABLE [dbo].[SUPDET](
	[SUPN] [char](5) NOT NULL,
	[TYPE] [char](1) NOT NULL,
	[DEPO] [int] NOT NULL,
	[DELC] [bit] NOT NULL,
	[ADD1] [char](30) NULL,
	[ADD2] [char](30) NULL,
	[ADD3] [char](30) NULL,
	[ADD4] [char](30) NULL,
	[ADD5] [char](30) NULL,
	[POST] [char](8) NULL,
	[PHO1] [char](16) NULL,
	[PHO2] [char](16) NULL,
	[FAX1] [char](16) NULL,
	[FAX2] [char](16) NULL,
	[CON1] [char](30) NULL,
	[CON2] [char](30) NULL,
	[NOTE] [char](60) NULL,
	[TNET] [bit] NOT NULL,
	[BBCC] [char](1) NULL,
	[VLED1] [smallint] NOT NULL,
	[VLED2] [smallint] NOT NULL,
	[VLED3] [smallint] NOT NULL,
	[VLED4] [smallint] NOT NULL,
	[VLED5] [smallint] NOT NULL,
	[VLED6] [smallint] NOT NULL,
	[VLED7] [smallint] NOT NULL,
	[LEAD] [smallint] NOT NULL,
	[SOQF] [smallint] NOT NULL,
	[CHKM] [char](1) NULL,
	[OCSD] [date] NULL,
	[OCED] [date] NULL,
	[DCSD] [date] NULL,
	[DCED] [date] NULL,
	[RPNO] [char](3) NULL,
	[MCPT] [char](1) NULL,
	[MCPV] [int] NOT NULL,
	[MCPC] [int] NOT NULL,
	[MCPW] [int] NOT NULL,
	[TCPW] [int] NOT NULL,
	[TCPV] [int] NOT NULL,
	[TCPP] [int] NOT NULL,
	[RMES] [smallint] NOT NULL,
	[ReviewDay1] [bit] NOT NULL,
	[ReviewDay2] [bit] NOT NULL,
	[ReviewDay3] [bit] NOT NULL,
	[ReviewDay4] [bit] NOT NULL,
	[ReviewDay5] [bit] NOT NULL,
	[ReviewDay6] [bit] NOT NULL,
	[ReviewDay0] [bit] NOT NULL,
 CONSTRAINT [PK_SUPDET] PRIMARY KEY CLUSTERED 
(
	[SUPN] ASC,
	[TYPE] ASC,
	[DEPO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number - key to the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'SUPN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Depot Type - O = Ordering depot
        R = Returns depot
        S = Supplier H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Depot Number
 for type S - 000 = temporary changes
             from store maintenance
       999 = H/O specified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'DEPO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delete indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line one' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ADD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line two' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ADD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line three' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ADD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line four' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ADD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line five' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ADD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Post Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'POST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone Number 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'PHO1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone Number 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'PHO2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fax Number 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'FAX1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fax Number 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'FAX2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contact Name 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'CON1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contact Name 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'CON2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Depot Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'NOTE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TRADANET Supplier?  ON = Yes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'TNET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BBC code - space = direct
C = Consolidated
W02   D = Discreet
W03                W = Warehouse  
W03                A = Alternative Supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'BBCC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variable Lead Time - by day of week
O=1....Monday / O=7....Sunday
Old VM:LEAD field defaults to highest of
these new values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'VLED1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variable Lead Time - by day of week
O=1....Monday / O=7....Sunday
Old VM:LEAD field defaults to highest of
these new values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'VLED2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variable Lead Time - by day of week
O=1....Monday / O=7....Sunday
Old VM:LEAD field defaults to highest of
these new values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'VLED3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variable Lead Time - by day of week
O=1....Monday / O=7....Sunday
Old VM:LEAD field defaults to highest of
these new values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'VLED4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variable Lead Time - by day of week
O=1....Monday / O=7....Sunday
Old VM:LEAD field defaults to highest of
these new values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'VLED5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variable Lead Time - by day of week
O=1....Monday / O=7....Sunday
Old VM:LEAD field defaults to highest of
these new values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'VLED6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variable Lead Time - by day of week
O=1....Monday / O=7....Sunday
Old VM:LEAD field defaults to highest of
these new values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'VLED7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fixed Lead Time in Days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'LEAD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Soq Frequency Code - When to calc soq at store
Binary Hash -   1 = Monday
2 = Tuesday
4 = Wednesday
8 = Thursday
16 = Friday
32 = Saturday
64 = Sunday
Any combination of days is shown as a summ of the
relevant day numbers. ( ie. Mon + Fri = ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'SOQF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery checking method - eg 1 = cartons
2 = detail check' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'CHKM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Closedown Start Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'OCSD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Closedown End Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'OCED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Closedown Start Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'DCSD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Closedown End Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'DCED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns Policy Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'RPNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type  N=none M=money C=carton U=units E=either m/c
                       T=tonnage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'MCPT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'-- Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'MCPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'-- Cartons or Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'MCPC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MCP Value by WEIGHT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'MCPW'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Truck Capacity - weight' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'TCPW'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Truck Capacity - volume' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'TCPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Truck Capacity - pallets' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'TCPP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns Depot Message Reference  *W04' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'RMES'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True Is Review Day  1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ReviewDay1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True Is Review Day  2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ReviewDay2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True Is Review Day  3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ReviewDay3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True Is Review Day  4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ReviewDay4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True Is Review Day  5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ReviewDay5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True Is Review Day  6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ReviewDay6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True Is Review Day  0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET', @level2type=N'COLUMN',@level2name=N'ReviewDay0'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S U P D E T  =   Supplier Detail File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPDET'
END
GO

