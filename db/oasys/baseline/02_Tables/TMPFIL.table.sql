﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TMPFIL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TMPFIL'
CREATE TABLE [dbo].[TMPFIL](
	[TKEY] [int] IDENTITY(1,1) NOT NULL,
	[WSID] [char](2) NULL,
	[FKEY] [char](40) NULL,
	[DATE1] [date] NULL,
	[DATA] [char](200) NULL,
	[DAT1] [char](200) NULL,
	[DAT2] [char](200) NULL,
	[DAT3] [char](200) NULL,
	[DAT4] [char](200) NULL,
 CONSTRAINT [PK_TMPFIL] PRIMARY KEY CLUSTERED 
(
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Key - System Generated' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Workstation ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'WSID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Content Keys - Used to hold data necessary
   to sequence the records as desired.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Record Added to File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data Area' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'DATA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DAT1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'DAT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DAT2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'DAT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DAT3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'DAT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DAT4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL', @level2type=N'COLUMN',@level2name=N'DAT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'T M P F I L   =   Temporary Holding File to Emulate ANYDOS Type File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TMPFIL'
END
GO

