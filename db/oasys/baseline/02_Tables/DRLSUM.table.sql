﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DRLSUM]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DRLSUM'
CREATE TABLE [dbo].[DRLSUM](
	[NUMB] [char](6) NOT NULL,
	[TYPE] [char](1) NULL,
	[VALU] [decimal](9, 2) NOT NULL,
	[DATE1] [date] NULL,
	[CLAS] [char](2) NULL,
	[COMM] [bit] NOT NULL,
	[TKEY] [int] NOT NULL,
	[INIT] [char](5) NULL,
	[INFO] [char](20) NULL,
	[0BBC] [bit] NOT NULL,
	[0SUP] [char](5) NULL,
	[0CON] [int] NULL,
	[0PON] [int] NULL,
	[0DAT] [date] NULL,
	[0REL] [int] NOT NULL,
	[0SOQ] [int] NULL,
	[0DL1] [char](10) NULL,
	[0DL2] [char](10) NULL,
	[0DL3] [char](10) NULL,
	[0DL4] [char](10) NULL,
	[0DL5] [char](10) NULL,
	[0DL6] [char](10) NULL,
	[0DL7] [char](10) NULL,
	[0DL8] [char](10) NULL,
	[0DL9] [char](10) NULL,
	[1STR] [char](3) NULL,
	[1IBT] [int] NULL,
	[1PRT] [bit] NOT NULL,
	[3SUP] [char](5) NULL,
	[3DAT] [date] NULL,
	[3PON] [int] NULL,
	[1CON] [int] NULL,
	[IPSO] [bit] NOT NULL,
	[RTI] [char](1) NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_DRLSUM] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Number - assigned by the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=receipt 1=IBT in 2=IBT out 3=return' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total value of the DRL (all positive)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'VALU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date DRL Number was assigned' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Class number of 1st item in file - No longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'CLAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Already Commed to Head Office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'COMM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Direct key to PURHDR or RETHDR (DIG key)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entry Person initials' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'INIT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Free form comment/information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'INFO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Received from a BBC order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0BBC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number of the order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0SUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Consignment note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0CON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Purchase Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0PON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Purchase Order date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned P/O Release Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0REL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ Control Number of the purchase Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0SOQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 1
BBC = issue number - first 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 2
BBC = 1st character  Y=Matched N=Not matched' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 3
BBC = 1st 6 characters  "IMPORT"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 4
BBC = 1st 6 characters  "COMMED"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Delivery Note number 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'0DL9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Number (in=from  out=to)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'1STR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IN = from store DRL Number (keyed in)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'1IBT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OUT | ON = Needs to be printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'1PRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'3SUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Return Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'3DAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Was Original P/O No. - Now Return Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'3PON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Consignment note number for IBT IN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'1CON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = This is a Project Sales DC Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'IPSO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D R L S U M  =   DRL Summary (header) Record - 1 per DRL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLSUM'
END
GO

