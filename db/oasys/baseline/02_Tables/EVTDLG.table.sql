﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EVTDLG]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table EVTDLG'
CREATE TABLE [dbo].[EVTDLG](
	[NUMB] [char](6) NOT NULL,
	[DLGN] [char](6) NOT NULL,
	[TYPE] [char](1) NOT NULL,
	[KEY1] [char](6) NOT NULL,
	[IDEL] [bit] NOT NULL,
	[QUAN] [decimal](7, 0) NULL,
	[VERO] [decimal](9, 2) NULL,
	[PERO] [decimal](7, 3) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_EVTDLG] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[DLGN] ASC,
	[TYPE] ASC,
	[KEY1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Number (See EVTHDR)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deal Group Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'DLGN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deal Type - M = Mix & Match, S = Sku' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item Key
For M Type = Mix & Match Group Number
For S Type = Sku Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'KEY1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delete Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'QUAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Erosion Allocated to this Item
- Zero if Erosion Percentage Specified.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'VERO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentage of Erosion Allocated to this Item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'PERO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E V T D L G  =  Event Deal Group File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTDLG'
END
GO

