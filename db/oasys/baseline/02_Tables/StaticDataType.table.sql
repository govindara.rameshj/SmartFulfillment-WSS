﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StaticDataType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table StaticDataType'
CREATE TABLE [dbo].[StaticDataType](
	[ID] [int] NULL,
	[Description] [varchar](50) NULL,
	[Notes] [varchar](50) NULL
) ON [PRIMARY]
END
GO

