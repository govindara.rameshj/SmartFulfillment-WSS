﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ACTLOG]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ACTLOG'
CREATE TABLE [dbo].[ACTLOG](
	[TKEY] [int] IDENTITY(1,1) NOT NULL,
	[DATE1] [date] NULL,
	[EEID] [char](3) NULL,
	[WSID] [char](2) NULL,
	[AFGN] [char](2) NULL,
	[AFPN] [char](2) NULL,
	[LOGI] [bit] NOT NULL,
	[FORC] [bit] NOT NULL,
	[STIM] [char](6) NULL,
	[ETIM] [char](6) NULL,
	[EDAT] [date] NULL,
 CONSTRAINT [PK_ACTLOG] PRIMARY KEY CLUSTERED 
(
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System Assigned Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System Date (Part of When)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Public ID Number (who) Key to SYSPAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Workstation ID (where) Key to WSOCTL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'WSID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AFG Key to AFGCTL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'AFGN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Program : With above = Key to AFPCTL
 If both of the above are "00" (null), The record is a log-in
 or out of MSMENU - Which is determined by the below indicator.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'AFPN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Log-IN    OFF = Log-OUT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'LOGI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Log-OUT was done by system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'FORC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start time as HHMMSS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'STIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'End   time as HHMMSS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'ETIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending date - Just in case different' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG', @level2type=N'COLUMN',@level2name=N'EDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A C T L O G = Activity Log   (who/where/what/when)  Records in this file are created/updated by MSMENU           ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACTLOG'
END
GO

