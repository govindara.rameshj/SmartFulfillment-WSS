﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HHTDET]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HHTDET'
CREATE TABLE [dbo].[HHTDET](
	[DATE1] [date] NOT NULL,
	[SKUN] [char](6) NOT NULL,
	[INON] [bit] NOT NULL,
	[ONHA] [decimal](7, 0) NULL,
	[SCNT1] [decimal](7, 0) NULL,
	[SCNT2] [decimal](7, 0) NULL,
	[SCNT3] [decimal](7, 0) NULL,
	[SCNT4] [decimal](7, 0) NULL,
	[SCNT5] [decimal](7, 0) NULL,
	[SCNT6] [decimal](7, 0) NULL,
	[SCNT7] [decimal](7, 0) NULL,
	[SCNT8] [decimal](7, 0) NULL,
	[SCNT9] [decimal](7, 0) NULL,
	[SCNT10] [decimal](7, 0) NULL,
	[TSCN] [decimal](7, 0) NULL,
	[WCNT1] [decimal](7, 0) NULL,
	[WCNT2] [decimal](7, 0) NULL,
	[WCNT3] [decimal](7, 0) NULL,
	[WCNT4] [decimal](7, 0) NULL,
	[WCNT5] [decimal](7, 0) NULL,
	[WCNT6] [decimal](7, 0) NULL,
	[WCNT7] [decimal](7, 0) NULL,
	[WCNT8] [decimal](7, 0) NULL,
	[WCNT9] [decimal](7, 0) NULL,
	[WCNT10] [decimal](7, 0) NULL,
	[TWCN] [decimal](7, 0) NULL,
	[TPRE] [decimal](7, 0) NULL,
	[TCNT] [decimal](7, 0) NULL,
	[ICNT] [bit] NOT NULL,
	[IADJ] [bit] NOT NULL,
	[MDNQ] [decimal](7, 0) NULL,
	[MDNC1] [decimal](7, 0) NULL,
	[MDNC2] [decimal](7, 0) NULL,
	[MDNC3] [decimal](7, 0) NULL,
	[TMDC] [decimal](7, 0) NULL,
	[LBOK] [bit] NOT NULL,
	[ORIG] [char](1) NULL,
	[StockSold] [int] NOT NULL,
 CONSTRAINT [PK_HHTDET] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Non stocked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'INON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Normal On Hand stock at the start of day' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'ONHA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shop Floor counts(maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'SCNT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total shop floor counts (SCNT totalled)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'TSCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse counts (maximum of 10 entries)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'WCNT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Warehouse counts (WCNT totalled)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'TWCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Presold stock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'TPRE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Total count (TSCN +TWCN -TPRE + TMDC)*W05 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'TCNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An entry has been made for this SKU
- it may have been a zero entry ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'ICNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Adjustment applied' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'IADJ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Mark-down Stock at start of day
W04 :MDNQ N6(O=3) *W03 Mark-down Stock at start of day' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'MDNQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Mark-down count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'MDNC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Mark-down count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'MDNC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Mark-down count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'MDNC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Total Markdown Count (MDNC totalled)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'TMDC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Label Details Correct Y/N' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'LBOK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W08 Sku Origin.
"R" - Sku Originated from refund count
"A" - Sku Originated from audit count
"S" - Sku Originated from store selection
"H" - Sku Originated from historic adjustment checks
Any other value indicates normal PIC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'ORIG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Stock Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET', @level2type=N'COLUMN',@level2name=N'StockSold'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H H T D E T  =   HHT PIC Count detail file ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTDET'
END
GO

