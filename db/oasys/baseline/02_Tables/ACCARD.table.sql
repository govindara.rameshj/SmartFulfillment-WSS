﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ACCARD]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ACCARD'
CREATE TABLE [dbo].[ACCARD](
	[NUMB] [char](6) NOT NULL,
	[CARD] [char](2) NOT NULL,
	[NAME] [char](36) NULL,
	[ALPH] [char](10) NULL,
	[ADD1] [char](24) NULL,
	[ADD2] [char](20) NULL,
	[ADD3] [char](20) NULL,
	[ADD4] [char](20) NULL,
	[PHON] [char](15) NULL,
	[STAT] [char](1) NULL,
	[DSTP] [bit] NOT NULL,
	[DTST] [date] NULL,
	[URCS] [bit] NOT NULL,
	[DURS] [date] NULL,
	[DELC] [bit] NOT NULL,
	[DDEL] [date] NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_ACCARD] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[CARD] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Account Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Card Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'CARD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Holder Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alpha Lookup - First 10 Characters of the Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Holder Address Line One' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'ADD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Holder Address Line Two' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'ADD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Holder Address Line Three' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'ADD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Holder Address Line Three' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'ADD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Holder Phone Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'PHON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Holder Status - Blank = OK, R = Refer, S=Stop,
L = Legal Stop, X = Deleted A/C' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'STAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Placed on Credit Stop by the Depot' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'DSTP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Depot Placed Account on Stop' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'DTST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Depot Credit Stop Removed by Update' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'URCS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Depot Credit Stop Removed by Update' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'DURS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Card Holder Needs to be Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Customer Account Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'DDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A C C A R D  =   Customer Account Valid Cards File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ACCARD'
END
GO

