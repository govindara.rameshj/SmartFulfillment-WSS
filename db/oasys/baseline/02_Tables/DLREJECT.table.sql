﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLREJECT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLREJECT'
CREATE TABLE [dbo].[DLREJECT](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[SEQN] [decimal](5, 0) NOT NULL,
	[EEID] [char](3) NULL,
	[SKUN] [char](6) NULL,
	[REJECTTEXT] [char](82) NULL,
	[GROUPID] [char](6) NULL,
	[PROMPTID] [char](8) NULL,
	[RTI] [char](1) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_DLREJECT] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[SEQN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'SEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rejection Text 1 from PROMPTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'REJECTTEXT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item Group ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'GROUPID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item Prompt ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'PROMPTID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Was A39)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L R E J E C T  =  Group Prompts Reject File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLREJECT'
END
GO

