﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ActivityLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ActivityLog'
CREATE TABLE [dbo].[ActivityLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LogDate] [date] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[WorkstationID] [char](2) NOT NULL,
	[MenuOptionID] [int] NOT NULL,
	[LoggedIn] [bit] NOT NULL,
	[ForcedLogOut] [bit] NOT NULL,
	[StartTime] [char](6) NOT NULL,
	[EndTime] [char](6) NOT NULL,
	[AppStart] [datetime] NULL,
	[AppEnd] [datetime] NULL,
 CONSTRAINT [PK_ActivityLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of the Activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'LogDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Employee Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'EmployeeID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Work Station Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'WorkstationID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu Option Selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'MenuOptionID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether Logged In = True Or Not = False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'LoggedIn'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if forced to Log Out False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'ForcedLogOut'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Activity Started' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'StartTime'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'End Time of Activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog', @level2type=N'COLUMN',@level2name=N'EndTime'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A C T I V I T Y L O G = Activity Log   (who/where/what/when)  Records in this file are created/updated by MSMENU           ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog'
END
GO

