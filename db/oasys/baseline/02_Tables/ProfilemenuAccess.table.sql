﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ProfilemenuAccess]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ProfilemenuAccess'
CREATE TABLE [dbo].[ProfilemenuAccess](
	[ID] [int] NOT NULL,
	[MenuConfigID] [int] NOT NULL,
	[AccessAllowed] [bit] NOT NULL,
	[OverrideAllowed] [bit] NOT NULL,
	[SecurityLevel] [tinyint] NOT NULL,
	[IsManager] [bit] NOT NULL,
	[IsSupervisor] [bit] NOT NULL,
	[LastEdited] [datetime] NULL,
	[Parameters] [varchar](100) NULL,
 CONSTRAINT [PK_ProfilemenuAccess] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[MenuConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Linked to MenuConfig Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess', @level2type=N'COLUMN',@level2name=N'MenuConfigID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if access allowed False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess', @level2type=N'COLUMN',@level2name=N'AccessAllowed'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if override Allowed False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess', @level2type=N'COLUMN',@level2name=N'OverrideAllowed'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess', @level2type=N'COLUMN',@level2name=N'SecurityLevel'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True = Needs to be a manager False any user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess', @level2type=N'COLUMN',@level2name=N'IsManager'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True = Needs to be a supervisor. False = Does not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess', @level2type=N'COLUMN',@level2name=N'IsSupervisor'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last date and Time edited' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess', @level2type=N'COLUMN',@level2name=N'LastEdited'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P R O F I L E M E N U A C C E S S = Profile Menu Access File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfilemenuAccess'
END
GO

