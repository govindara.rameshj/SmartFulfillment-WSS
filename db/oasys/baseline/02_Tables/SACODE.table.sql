﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SACODE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SACODE'
CREATE TABLE [dbo].[SACODE](
	[NUMB] [char](2) NOT NULL,
	[DESCR] [char](25) NULL,
	[TYPE] [char](1) NULL,
	[SIGN] [char](1) NULL,
	[SECL] [char](3) NULL,
	[IMDN] [bit] NOT NULL,
	[IWTF] [bit] NOT NULL,
	[IsAuthRequired] [bit] NOT NULL,
	[IsPassRequired] [bit] NOT NULL,
	[IsReserved] [bit] NOT NULL,
	[IsCommentable] [bit] NOT NULL,
	[IsStockLoss] [bit] NOT NULL,
	[IsKnownTheft] [bit] NOT NULL,
	[IsIssueQuery] [bit] NOT NULL,
	[IsReceiptAdjustment] [bit] NOT NULL,
	[AllowCodes] [char](20) NULL,
 CONSTRAINT [PK_SACODE] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Adjustment Number 01 to 99 - file key' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 N=Normal
W04 T=Transfer.
W04 S=Sales Adjustment (Dept to SKU) is no longer
W04 used.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"+" = Add, "-" = Subtract, "S" =  + or -,
"R" = Replace, "C" = Calculate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'SIGN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security level required for this code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'SECL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 On = Mark-down adjustment
W03      Stock will be transferred from IM:ONHA to IM:MDNQ
W03      or vice versa dependant on sign of adjustment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IMDN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 On = Write Off adjustment
W03      Stock will be transferred from IM:ONHA to IM:WTFQ
W03      or vice versa dependant on sign of adjustment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IWTF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Authorisation is required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IsAuthRequired'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If pasword is required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IsPassRequired'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Reserved' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IsReserved'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Commentable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IsCommentable'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Stock Loss' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IsStockLoss'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Known Theft' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IsKnownTheft'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Issue Query' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'IsIssueQuery'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allow Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE', @level2type=N'COLUMN',@level2name=N'AllowCodes'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S A C O D E  =   Valid STOCK ADJUSTMENT Codes  
NOTE: If TYPE = S (sales Adj) then Sign is always set to "-"   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SACODE'
END
GO

