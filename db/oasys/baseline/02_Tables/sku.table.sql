﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sku]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table sku'
CREATE TABLE [dbo].[sku](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sku] [char](6) NULL,
	[descr] [varchar](200) NULL,
	[matches] [int] NULL,
	[SpacelessDescription] [varchar](200) NULL
) ON [PRIMARY]
END
GO

