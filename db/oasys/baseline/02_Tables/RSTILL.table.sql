﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RSTILL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RSTILL'
CREATE TABLE [dbo].[RSTILL](
	[TILL] [char](2) NOT NULL,
	[FTKY] [decimal](9, 0) NULL,
	[CASH] [char](3) NULL,
	[PCAS] [char](3) NULL,
	[PTIM] [char](4) NULL,
	[NUMB] [decimal](5, 0) NULL,
	[AMNT] [decimal](9, 2) NULL,
	[ZRED] [bit] NOT NULL,
	[TRAN] [char](4) NULL,
	[IEVT] [bit] NOT NULL,
 CONSTRAINT [PK_RSTILL] PRIMARY KEY CLUSTERED 
(
	[TILL] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number - 1sssnn   nn=Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flash Totals Key if Type = T - Assigned by System' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'FTKY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier on this till - if none = 999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'CASH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior Cashier Number on this till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'PCAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time last Cashier Signed off this Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'PTIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Transactions for this Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Transactions for this Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'AMNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Till Z-Read has been Done' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'ZRED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current Till Transaction Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03       Y = This Till is currently using EVT files for the
*W03       current "Sale" transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL', @level2type=N'COLUMN',@level2name=N'IEVT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R S T I L L  =   Till Flash Totals File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RSTILL'
END
GO

