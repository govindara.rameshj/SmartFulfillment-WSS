﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RELITM]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RELITM'
CREATE TABLE [dbo].[RELITM](
	[SPOS] [char](6) NOT NULL,
	[PPOS] [char](6) NULL,
	[DELC] [bit] NOT NULL,
	[NSPP] [decimal](5, 0) NULL,
	[QTYS] [decimal](7, 0) NULL,
	[PTDQ] [decimal](7, 0) NULL,
	[YTDQ] [decimal](9, 0) NULL,
	[PYRQ] [decimal](9, 0) NULL,
	[WTDS] [decimal](7, 0) NULL,
	[PWKS] [decimal](7, 0) NULL,
	[WTDM] [decimal](9, 2) NULL,
	[PWKM] [decimal](9, 2) NULL,
	[PTDM] [decimal](9, 2) NULL,
	[PPDM] [decimal](9, 2) NULL,
	[YTDM] [decimal](9, 2) NULL,
	[PYRM] [decimal](9, 2) NULL,
 CONSTRAINT [PK_RELITM] PRIMARY KEY CLUSTERED 
(
	[SPOS] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Single item Number - Normal key to the File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'SPOS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk/Package item number - Alt index to File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'PPOS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = record has been deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Singles per Pack' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'NSPP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Singles sold NOT updated yet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'QTYS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period to date Singles Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'PTDQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Year to date Singles Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'YTDQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior Year Singles Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'PYRQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This week units Transfered for markup calc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'WTDS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior week units Transfered for markup calc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'PWKS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This week markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'WTDM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior week markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'PWKM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period to date markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'PTDM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior period markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'PPDM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Year to date markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'YTDM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior year markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM', @level2type=N'COLUMN',@level2name=N'PYRM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R E L I T M  =  Related Items File - Singles and Packs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RELITM'
END
GO

