﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PicExternalAudit]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PicExternalAudit'
CREATE TABLE [dbo].[PicExternalAudit](
	[CountDate] [date] NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[Price] [decimal](8, 2) NOT NULL,
	[StockOnHand] [int] NOT NULL,
	[StockMarkdown] [int] NOT NULL,
	[RTI] [char](1) NOT NULL,
 CONSTRAINT [PK_PicExternalAudit] PRIMARY KEY CLUSTERED 
(
	[CountDate] ASC,
	[SkuNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Count took place' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PicExternalAudit', @level2type=N'COLUMN',@level2name=N'CountDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PicExternalAudit', @level2type=N'COLUMN',@level2name=N'SkuNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PicExternalAudit', @level2type=N'COLUMN',@level2name=N'Price'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity of Stock On Hand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PicExternalAudit', @level2type=N'COLUMN',@level2name=N'StockOnHand'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Stock Items Marked Down' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PicExternalAudit', @level2type=N'COLUMN',@level2name=N'StockMarkdown'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PicExternalAudit', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P I C E X T E R N A L A U D I T = Pic External Audit File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PicExternalAudit'
END
GO

