﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PICCTL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PICCTL'
CREATE TABLE [dbo].[PICCTL](
	[DAYN] [decimal](3, 0) NOT NULL,
	[CTGY] [char](6) NOT NULL,
	[GRUP] [char](6) NOT NULL,
	[SGRP] [char](6) NOT NULL,
	[STYL] [char](6) NOT NULL,
	[IDEL] [bit] NOT NULL,
 CONSTRAINT [PK_PICCTL] PRIMARY KEY CLUSTERED 
(
	[DAYN] ASC,
	[CTGY] ASC,
	[GRUP] ASC,
	[SGRP] ASC,
	[STYL] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Day Number within Cycle
Valid - 01 to (SD:CYNO 7)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICCTL', @level2type=N'COLUMN',@level2name=N'DAYN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Category Number
Valid - 000001 to 999999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICCTL', @level2type=N'COLUMN',@level2name=N'CTGY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Group Number
Valid - 000000 to 999999
000000 = Whole Category required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICCTL', @level2type=N'COLUMN',@level2name=N'GRUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Subgroup Number
Valid - 000000 to 999999
000000 = Whole Category or Group required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICCTL', @level2type=N'COLUMN',@level2name=N'SGRP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Style Number
Valid - 000000 to 999999
000000 = Whole Category, Group or Subgroup
required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICCTL', @level2type=N'COLUMN',@level2name=N'STYL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deleted Indicator - for use in LOAD process.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICCTL', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P I C C T L  =  PIC Control File                   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PICCTL'
END
GO

