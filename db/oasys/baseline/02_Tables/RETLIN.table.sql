﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RETLIN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RETLIN'
CREATE TABLE [dbo].[RETLIN](
	[TKEY] [int] IDENTITY(1,1) NOT NULL,
	[HKEY] [int] NULL,
	[SKUN] [char](6) NULL,
	[QUAN] [int] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[COST] [decimal](11, 4) NULL,
	[REAS] [char](2) NULL,
	[RTI] [char](1) NULL,
 CONSTRAINT [PK_RETLIN] PRIMARY KEY CLUSTERED 
(
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DIG compiler KEY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary KEY to RETHDR File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN', @level2type=N'COLUMN',@level2name=N'HKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number for this line' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity Returned' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN', @level2type=N'COLUMN',@level2name=N'QUAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price at time of Return' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cost at time of Return' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN', @level2type=N'COLUMN',@level2name=N'COST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'eturn Reason code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN', @level2type=N'COLUMN',@level2name=N'REAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R E T L I N  =   Supplier Returns Line Item Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETLIN'
END
GO

