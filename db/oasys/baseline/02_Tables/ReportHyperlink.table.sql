﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportHyperlink]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ReportHyperlink'
CREATE TABLE [dbo].[ReportHyperlink](
	[ReportId] [int] NOT NULL,
	[TableId] [int] NOT NULL,
	[RowId] [int] NOT NULL,
	[ColumnName] [varchar](50) NOT NULL,
	[Value] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ReportHyperlink_1] PRIMARY KEY CLUSTERED 
(
	[ReportId] ASC,
	[TableId] ASC,
	[RowId] ASC,
	[ColumnName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

