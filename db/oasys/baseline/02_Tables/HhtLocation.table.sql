﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HhtLocation]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HhtLocation'
CREATE TABLE [dbo].[HhtLocation](
	[DateCreated] [date] NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[Sequence] [int] NOT NULL,
	[IslandId] [int] NOT NULL,
	[IslandType] [char](1) NOT NULL,
	[IslandSegment] [int] NOT NULL,
	[PlanogramId] [int] NOT NULL,
	[StockCount] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_HhtLocation] PRIMARY KEY CLUSTERED 
(
	[DateCreated] ASC,
	[SkuNumber] ASC,
	[Sequence] ASC,
	[IslandId] ASC,
	[IslandSegment] ASC,
	[IslandType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creation Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'DateCreated'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'SkuNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'Sequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if island Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'IslandId'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Island type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'IslandType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Island Segment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'IslandSegment'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Planogram Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'PlanogramId'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'StockCount'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if Record is to be deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation', @level2type=N'COLUMN',@level2name=N'IsDeleted'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H H T L O C A T I O N = HHT Location File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HhtLocation'
END
GO

