﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TENCTL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TENCTL'
CREATE TABLE [dbo].[TENCTL](
	[TTID] [decimal](3, 0) NOT NULL,
	[TTDE] [char](15) NULL,
	[IUSE] [bit] NOT NULL,
 CONSTRAINT [PK_TENCTL] PRIMARY KEY CLUSTERED 
(
	[TTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENCTL', @level2type=N'COLUMN',@level2name=N'TTID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENCTL', @level2type=N'COLUMN',@level2name=N'TTDE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Tender is in Use False If not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENCTL', @level2type=N'COLUMN',@level2name=N'IUSE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'T E N C T L   =   Tender Control File                                               
                     Client''s do not have access to this file.                   
                     Contains list of potential Tenders.                          
                     This file is set up and maintained by CTS Only.      
                     Whether the Tender is available to the client is     
                     determined by the IUSE flag.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENCTL'
END
GO

