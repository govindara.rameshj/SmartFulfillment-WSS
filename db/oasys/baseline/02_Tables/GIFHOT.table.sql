﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GIFHOT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table GIFHOT'
CREATE TABLE [dbo].[GIFHOT](
	[SERI] [char](8) NOT NULL,
	[TEXT] [char](40) NULL,
	[HOTT] [char](40) NULL,
	[COMM] [bit] NOT NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_GIFHOT] PRIMARY KEY CLUSTERED 
(
	[SERI] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Serial Number of Gift Token' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GIFHOT', @level2type=N'COLUMN',@level2name=N'SERI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store,Date,Till,Tran,Type,Employee - Redemption Info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GIFHOT', @level2type=N'COLUMN',@level2name=N'TEXT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H/O Text relating to "Hot" Gift Token - Forged
stolen etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GIFHOT', @level2type=N'COLUMN',@level2name=N'HOTT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Commed Flag.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GIFHOT', @level2type=N'COLUMN',@level2name=N'COMM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GIFHOT', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'G I F H O T =   Gift Token Redemption / Hot File.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GIFHOT'
END
GO

