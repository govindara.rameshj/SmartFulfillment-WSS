﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MarkdownStock]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table MarkdownStock'
CREATE TABLE [dbo].[MarkdownStock](
	[SkuNumber] [char](6) NOT NULL,
	[Serial] [char](6) NOT NULL,
	[Price] [decimal](9, 2) NULL,
	[IsLabelled] [bit] NOT NULL,
	[IsReserved] [bit] NOT NULL,
	[DateCreated] [date] NULL,
	[DateWrittenOff] [date] NULL,
	[DateLastRollover] [date] NULL,
	[WeekNumber] [int] NULL,
	[ReductionWeek1] [decimal](9, 4) NULL,
	[ReductionWeek2] [decimal](9, 4) NULL,
	[ReductionWeek3] [decimal](9, 4) NULL,
	[ReductionWeek4] [decimal](9, 4) NULL,
	[ReductionWeek5] [decimal](9, 4) NULL,
	[ReductionWeek6] [decimal](9, 4) NULL,
	[ReductionWeek7] [decimal](9, 4) NULL,
	[ReductionWeek8] [decimal](9, 4) NULL,
	[ReductionWeek9] [decimal](9, 4) NULL,
	[ReductionWeek10] [decimal](9, 4) NULL,
	[ReasonCode] [char](2) NULL,
	[SoldDate] [date] NULL,
	[SoldTill] [char](2) NULL,
	[SoldTransaction] [char](4) NULL,
 CONSTRAINT [PK_MarkdownStock] PRIMARY KEY CLUSTERED 
(
	[SkuNumber] ASC,
	[Serial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'SkuNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Serial Number of the Mark down' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'Serial'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'Price'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if Item labeled False If Not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'IsLabelled'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if reserved false if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'IsReserved'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creation Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'DateCreated'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date to be Written Off' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'DateWrittenOff'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Last Rolled over' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'DateLastRollover'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current week were in in the cycle of markdown 1 - 10 weeks before write off' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'WeekNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 1 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction percentage off price in week 10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReductionWeek10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reason For Mark down Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'ReasonCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Item Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'SoldDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number Item was sold from' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'SoldTill'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number given when sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock', @level2type=N'COLUMN',@level2name=N'SoldTransaction'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'M A R K D O W N S T O C K = Markdown Stock File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarkdownStock'
END
GO

