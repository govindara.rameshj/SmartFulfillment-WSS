﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CARD_SCHEME]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CARD_SCHEME'
CREATE TABLE [dbo].[CARD_SCHEME](
	[ID] [char](4) NOT NULL,
	[SCHEME_NAME] [char](40) NOT NULL,
	[RECEIPT_TEXT] [char](40) NOT NULL,
	[CARD_NO_SIZE] [int] NOT NULL,
	[START_NO] [char](19) NOT NULL,
	[END_NO] [char](19) NOT NULL,
	[START_DATE] [date] NOT NULL,
	[END_DATE] [date] NOT NULL,
	[DELETED] [bit] NOT NULL,
	[KEYED] [char](1) NOT NULL,
	[CAPT_NAME] [char](1) NULL,
	[CAPT_ADD1] [char](1) NULL,
	[CAPT_ADD2] [char](1) NULL,
	[CAPT_ADD3] [char](1) NULL,
	[CAPT_ADD4] [char](1) NULL,
	[CAPT_POST] [char](1) NULL,
	[COPY_RECEIPT] [bit] NOT NULL,
	[EROSION_CODE] [char](6) NOT NULL,
	[DISCOUNT] [decimal](5, 2) NOT NULL,
	[CARD_PREF] [char](1) NULL,
	[SUPAUTHLEVEL] [decimal](7, 2) NULL,
	[MNGRAUTHLEVEL] [decimal](7, 2) NULL,
	[PRINT_SIG_SLIP] [bit] NOT NULL,
 CONSTRAINT [PK_CARD_SCHEME] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internal ID number for card scheme. 
Must always form the first 4 characters
of the card number. "*" may be used to signify a wildcard 
for insignificant digits in the 2nd, 3rd & 4th digits.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the discount scheme eg. Installer Card' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'SCHEME_NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text that will be printed on the receipt if this card discount scheme is used. eg. Installer Discount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'RECEIPT_TEXT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default will be the maximum of 19 digits
                                * but may be less.
                                * eg. Colleague Cards = 16' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'CARD_NO_SIZE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The first number that the scheme will use i.e 9800 0000 0000 0000 0000' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'START_NO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The last number in the range for the card scheme i.e. 9800 9999 9999 9999 9999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'END_NO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date the card scheme is valid from' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'START_DATE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date the card scheme is valid to.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'END_DATE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'DELETED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether we allow this card scheme to be keyed in by the cashier.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'KEYED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Capture customer details - Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'CAPT_NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Capture customer details - Address Line 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'CAPT_ADD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Capture customer details - Address Line 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'CAPT_ADD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Capture customer details - Address Line 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'CAPT_ADD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Capture customer details - Address Line 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'CAPT_ADD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Capture customer details - Post Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'CAPT_POST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Produces a copy receipt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'COPY_RECEIPT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Margin Erosion Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'EROSION_CODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentage Discount (Valid 0.00% to 100.00%)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'DISCOUNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Preference
      * I = Inclusive Scheme
      *   Only accept cards included in the card list
      * E = Exclusive Scheme
      *   Only accept cards absent from the card list
      * Else = Accept all in range' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'CARD_PREF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supervisor Auth required above this value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'SUPAUTHLEVEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Manager Auth required above this value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'MNGRAUTHLEVEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Print Signature Slip' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME', @level2type=N'COLUMN',@level2name=N'PRINT_SIG_SLIP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C A R D _ S C H E M E =  Discount Card Scheme File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_SCHEME'
END
GO

