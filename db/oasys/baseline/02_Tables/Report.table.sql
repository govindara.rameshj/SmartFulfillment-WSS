﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Report]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table Report'
CREATE TABLE [dbo].[Report](
	[Id] [int] NOT NULL,
	[Header] [varchar](50) NULL,
	[Title] [varchar](50) NULL,
	[ProcedureName] [varchar](50) NULL,
	[HideWhenNoData] [bit] NOT NULL,
	[PrintLandscape] [bit] NOT NULL,
	[MinWidth] [int] NULL,
	[MinHeight] [int] NULL,
	[ShowResetButton] [bit] NULL,
 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

