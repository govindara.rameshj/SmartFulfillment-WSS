﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CASMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CASMAS'
CREATE TABLE [dbo].[CASMAS](
	[CASH] [char](3) NOT NULL,
	[DELC] [bit] NOT NULL,
	[NAME] [char](22) NULL,
	[POSI] [char](15) NULL,
	[CODE] [char](5) NULL,
	[SUPV] [bit] NOT NULL,
	[FLOT] [decimal](9, 2) NULL,
	[SKUL1] [decimal](7, 0) NULL,
	[SKUL2] [decimal](7, 0) NULL,
	[SKUL3] [decimal](7, 0) NULL,
	[SKUL4] [decimal](7, 0) NULL,
	[SKUL5] [decimal](7, 0) NULL,
	[SKUL6] [decimal](7, 0) NULL,
	[SKUL7] [decimal](7, 0) NULL,
	[DPTL1] [decimal](7, 0) NULL,
	[DPTL2] [decimal](7, 0) NULL,
	[DPTL3] [decimal](7, 0) NULL,
	[DPTL4] [decimal](7, 0) NULL,
	[DPTL5] [decimal](7, 0) NULL,
	[DPTL6] [decimal](7, 0) NULL,
	[DPTL7] [decimal](7, 0) NULL,
	[SKUS1] [decimal](9, 2) NULL,
	[SKUS2] [decimal](9, 2) NULL,
	[SKUS3] [decimal](9, 2) NULL,
	[SKUS4] [decimal](9, 2) NULL,
	[SKUS5] [decimal](9, 2) NULL,
	[SKUS6] [decimal](9, 2) NULL,
	[SKUS7] [decimal](9, 2) NULL,
	[DPTS1] [decimal](9, 2) NULL,
	[DPTS2] [decimal](9, 2) NULL,
	[DPTS3] [decimal](9, 2) NULL,
	[DPTS4] [decimal](9, 2) NULL,
	[DPTS5] [decimal](9, 2) NULL,
	[DPTS6] [decimal](9, 2) NULL,
	[DPTS7] [decimal](9, 2) NULL,
	[SKUC1] [decimal](11, 3) NULL,
	[SKUC2] [decimal](11, 3) NULL,
	[SKUC3] [decimal](11, 3) NULL,
	[SKUC4] [decimal](11, 3) NULL,
	[SKUC5] [decimal](11, 3) NULL,
	[SKUC6] [decimal](11, 3) NULL,
	[SKUC7] [decimal](11, 3) NULL,
	[DPTC1] [decimal](11, 3) NULL,
	[DPTC2] [decimal](11, 3) NULL,
	[DPTC3] [decimal](11, 3) NULL,
	[DPTC4] [decimal](11, 3) NULL,
	[DPTC5] [decimal](11, 3) NULL,
	[DPTC6] [decimal](11, 3) NULL,
	[DPTC7] [decimal](11, 3) NULL,
	[PVNO1] [decimal](7, 0) NULL,
	[PVNO2] [decimal](7, 0) NULL,
	[PVNO3] [decimal](7, 0) NULL,
	[PVNO4] [decimal](7, 0) NULL,
	[PVNO5] [decimal](7, 0) NULL,
	[PVNO6] [decimal](7, 0) NULL,
	[PVNO7] [decimal](7, 0) NULL,
	[PVVA1] [decimal](9, 2) NULL,
	[PVVA2] [decimal](9, 2) NULL,
	[PVVA3] [decimal](9, 2) NULL,
	[PVVA4] [decimal](9, 2) NULL,
	[PVVA5] [decimal](9, 2) NULL,
	[PVVA6] [decimal](9, 2) NULL,
	[PVVA7] [decimal](9, 2) NULL,
	[PSSF] [char](1) NULL,
	[PSNQ1] [decimal](5, 0) NULL,
	[PSNQ2] [decimal](5, 0) NULL,
	[PSNQ3] [decimal](5, 0) NULL,
	[PSNQ4] [decimal](5, 0) NULL,
	[PSNQ5] [decimal](5, 0) NULL,
	[PSNQ6] [decimal](5, 0) NULL,
	[PSNC1] [decimal](5, 0) NULL,
	[PSNC2] [decimal](5, 0) NULL,
	[PSNC3] [decimal](5, 0) NULL,
	[PSNC4] [decimal](5, 0) NULL,
	[PSNC5] [decimal](5, 0) NULL,
	[PSNC6] [decimal](5, 0) NULL,
	[PSNS1] [decimal](5, 0) NULL,
	[PSNS2] [decimal](5, 0) NULL,
	[PSNS3] [decimal](5, 0) NULL,
	[PSNS4] [decimal](5, 0) NULL,
	[PSNS5] [decimal](5, 0) NULL,
	[PSNS6] [decimal](5, 0) NULL,
	[PSNF1] [decimal](5, 0) NULL,
	[PSNF2] [decimal](5, 0) NULL,
	[PSNF3] [decimal](5, 0) NULL,
	[PSNF4] [decimal](5, 0) NULL,
	[PSNF5] [decimal](5, 0) NULL,
	[PSNF6] [decimal](5, 0) NULL,
	[PSVS1] [decimal](9, 2) NULL,
	[PSVS2] [decimal](9, 2) NULL,
	[PSVS3] [decimal](9, 2) NULL,
	[PSVS4] [decimal](9, 2) NULL,
	[PSVS5] [decimal](9, 2) NULL,
	[PSVS6] [decimal](9, 2) NULL,
	[PSVF1] [decimal](9, 2) NULL,
	[PSVF2] [decimal](9, 2) NULL,
	[PSVF3] [decimal](9, 2) NULL,
	[PSVF4] [decimal](9, 2) NULL,
	[PSVF5] [decimal](9, 2) NULL,
	[PSVF6] [decimal](9, 2) NULL,
	[PSIV1] [decimal](9, 2) NULL,
	[PSIV2] [decimal](9, 2) NULL,
	[PSIV3] [decimal](9, 2) NULL,
	[PSIV4] [decimal](9, 2) NULL,
	[PSIV5] [decimal](9, 2) NULL,
	[PSIV6] [decimal](9, 2) NULL,
	[PSAV1] [decimal](9, 2) NULL,
	[PSAV2] [decimal](9, 2) NULL,
	[PSAV3] [decimal](9, 2) NULL,
	[PSAV4] [decimal](9, 2) NULL,
	[PSAV5] [decimal](9, 2) NULL,
	[PSAV6] [decimal](9, 2) NULL,
	[PSCV1] [decimal](9, 2) NULL,
	[PSCV2] [decimal](9, 2) NULL,
	[PSCV3] [decimal](9, 2) NULL,
	[PSCV4] [decimal](9, 2) NULL,
	[PSCV5] [decimal](9, 2) NULL,
	[PSCV6] [decimal](9, 2) NULL,
	[PSTV1] [decimal](9, 2) NULL,
	[PSTV2] [decimal](9, 2) NULL,
	[PSTV3] [decimal](9, 2) NULL,
	[PSTV4] [decimal](9, 2) NULL,
	[PSTV5] [decimal](9, 2) NULL,
	[PSTV6] [decimal](9, 2) NULL,
	[PSCA1] [decimal](9, 2) NULL,
	[PSCA2] [decimal](9, 2) NULL,
	[PSCA3] [decimal](9, 2) NULL,
	[PSCA4] [decimal](9, 2) NULL,
	[PSCA5] [decimal](9, 2) NULL,
	[PSCA6] [decimal](9, 2) NULL,
	[SPARE] [char](99) NULL,
 CONSTRAINT [PK_CASMAS] PRIMARY KEY CLUSTERED 
(
	[CASH] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Number ---- key = 001 to 998 valid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'CASH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Deleted by GSMCAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Position Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'POSI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sign on Code Entered at the till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'CODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier is a Supervisor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default Float Amount as xxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'FLOT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sku Lookup Lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUL1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sku Lookup Lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUL2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sku Lookup Lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUL3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sku Lookup Lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUL4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sku Lookup Lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUL5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sku Lookup Lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUL6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sku Lookup Lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUL7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Number of Department Lookup Lines - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTL1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Number of Department Lookup Lines - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTL2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Number of Department Lookup Lines - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTL3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Number of Department Lookup Lines - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTL4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Number of Department Lookup Lines - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTL5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Number of Department Lookup Lines - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTL6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Number of Department Lookup Lines - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTL7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Retail Sales as xxxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Retail Sales as xxxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Retail Sales as xxxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Retail Sales as xxxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Retail Sales as xxxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUS5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Retail Sales as xxxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUS6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Retail Sales as xxxxxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUS7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Retail Sales as xxxxxxx.xx -No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Retail Sales as xxxxxxx.xx -No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Retail Sales as xxxxxxx.xx -No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Retail Sales as xxxxxxx.xx -No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Retail Sales as xxxxxxx.xx -No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTS5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Retail Sales as xxxxxxx.xx -No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTS6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Retail Sales as xxxxxxx.xx -No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTS7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Cost of Sales as xxxxxxx.xxx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Cost of Sales as xxxxxxx.xxx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Cost of Sales as xxxxxxx.xxx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Cost of Sales as xxxxxxx.xxx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Cost of Sales as xxxxxxx.xxx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Cost of Sales as xxxxxxx.xxx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Cost of Sales as xxxxxxx.xxx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SKUC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Cost of sales from Factor - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Cost of sales from Factor - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Cost of sales from Factor - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Cost of sales from Factor - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Cost of sales from Factor - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Cost of sales from Factor - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Department Cost of sales from Factor - No longer used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'DPTC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVNO1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVNO2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVNO3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVNO4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVNO5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVNO6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVNO7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markup Value of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVVA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markup Value of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVVA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markup Value of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVVA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markup Value of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVVA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markup Value of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVVA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markup Value of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVVA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markup Value of Price Violations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PVVA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = Not valid to use the PSS System
S-Salaried C-Commission Only  N-No Comm.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSSF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNQ5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNQ6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Customers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Customers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Customers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Customers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Customers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Customers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes converted to a sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes converted to a sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes converted to a sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes converted to a sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes converted to a sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNS5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Quotes converted to a sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNS6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sales sold financing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNF1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sales sold financing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNF2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sales sold financing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNF3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sales sold financing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNF4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sales sold financing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNF5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sales sold financing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSNF6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Sales - Net coupons' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Sales - Net coupons' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Sales - Net coupons' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Sales - Net coupons' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Sales - Net coupons' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVS5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Sales - Net coupons' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVS6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Finanaced Sales' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVF1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Finanaced Sales' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVF2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Finanaced Sales' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVF3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Finanaced Sales' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVF4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Finanaced Sales' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVF5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Finanaced Sales' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSVF6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Installation Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSIV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Installation Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSIV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Installation Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSIV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Installation Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSIV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Installation Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSIV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Installation Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSIV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Accessories Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSAV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Accessories Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSAV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Accessories Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSAV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Accessories Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSAV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Accessories Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSAV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Accessories Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSAV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Commissionable Items Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Commissionable Items Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Commissionable Items Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Commissionable Items Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Commissionable Items Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Commissionable Items Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TARGET values -- DONT ROLL - Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSTV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TARGET values -- DONT ROLL - Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSTV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TARGET values -- DONT ROLL - Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSTV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TARGET values -- DONT ROLL - Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSTV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TARGET values -- DONT ROLL - Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSTV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TARGET values -- DONT ROLL - Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSTV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled Orders Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled Orders Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled Orders Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled Orders Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled Orders Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled Orders Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'PSCA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C A S M A S  =   Cashier Master File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASMAS'
END
GO

