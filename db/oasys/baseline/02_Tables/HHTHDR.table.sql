﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HHTHDR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HHTHDR'
CREATE TABLE [dbo].[HHTHDR](
	[DATE1] [date] NOT NULL,
	[NUMB] [int] NULL,
	[DONE] [int] NULL,
	[IADJ] [bit] NOT NULL,
	[AUTH] [char](3) NULL,
	[IsLocked] [bit] NOT NULL,
 CONSTRAINT [PK_HHTHDR] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTHDR', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total number of items to count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTHDR', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of items counted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTHDR', @level2type=N'COLUMN',@level2name=N'DONE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True = Today''s adjustments all applied' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTHDR', @level2type=N'COLUMN',@level2name=N'IADJ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee ID of authorising manager' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTHDR', @level2type=N'COLUMN',@level2name=N'AUTH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True = Locked record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTHDR', @level2type=N'COLUMN',@level2name=N'IsLocked'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H H T D E T  =   HHT PIC Count Header File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HHTHDR'
END
GO

