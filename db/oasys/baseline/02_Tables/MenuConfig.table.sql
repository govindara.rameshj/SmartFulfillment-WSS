﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MenuConfig]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table MenuConfig'
CREATE TABLE [dbo].[MenuConfig](
	[ID] [int] NOT NULL,
	[MasterID] [int] NOT NULL,
	[AppName] [varchar](100) NOT NULL,
	[AssemblyName] [varchar](100) NULL,
	[ClassName] [varchar](100) NULL,
	[MenuType] [int] NOT NULL,
	[Parameters] [varchar](250) NULL,
	[ImagePath] [char](50) NULL,
	[LoadMaximised] [bit] NOT NULL,
	[IsModal] [bit] NOT NULL,
	[DisplaySequence] [int] NOT NULL,
	[AllowMultiple] [bit] NOT NULL,
	[WaitForExit] [bit] NOT NULL,
	[TabName] [varchar](100) NULL,
	[Description] [varchar](100) NULL,
	[ImageKey] [char](20) NULL,
	[Timeout] [int] NOT NULL,
	[DoProcessingType] [int] NOT NULL,
 CONSTRAINT [PK_MenuConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Master Identifier. Which Menu you want to put this item under.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'MasterID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Application Name that will be seen on the menu.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'AppName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assembley Name of dll (i.e. BankingSafe.dll) ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'AssemblyName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the Class Used when calling a dll (i.e. Banking.BankingSafe)
You would need to get this from the developer' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'ClassName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu Type
  * 1 = Executable
  * 2 = Application Menu
  * 3 = Sub Menu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'MenuType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Command Line Parameters used to call the program (i.e. CONFIG=81)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'Parameters'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Path of the icon file you want to show next to the name of the application ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'ImagePath'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Display Sequence in the menu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'DisplaySequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if multiple launches of this program are allowed False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'AllowMultiple'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If the menu program should wait untill this application is finished. False it can carry on.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'WaitForExit'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tab Control Field Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'TabName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'Description'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'he Key used to display the image' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'ImageKey'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Timeout in seconds' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'Timeout'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Processing Type;
 * 0 = Menu (holding)
 * 1 = Launch a Application' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig', @level2type=N'COLUMN',@level2name=N'DoProcessingType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'M E N U C O N F I G = Menu Configuration File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuConfig'
END
GO

