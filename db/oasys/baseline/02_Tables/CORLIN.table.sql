﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CORLIN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CORLIN'
CREATE TABLE [dbo].[CORLIN](
	[NUMB] [char](6) NOT NULL,
	[LINE] [char](4) NOT NULL,
	[SKUN] [char](6) NOT NULL,
	[QTYO] [decimal](5, 0) NOT NULL,
	[QTYT] [decimal](5, 0) NOT NULL,
	[QTYR] [decimal](5, 0) NOT NULL,
	[WGHT] [decimal](7, 2) NOT NULL,
	[VOLU] [decimal](7, 2) NOT NULL,
	[PORC] [smallint] NOT NULL,
	[DeliveryStatus] [int] NOT NULL,
	[SellingStoreId] [int] NOT NULL,
	[SellingStoreOrderId] [int] NOT NULL,
	[QtyToBeDelivered] [int] NOT NULL,
	[DeliverySource] [char](4) NULL,
	[DeliverySourceIbtOut] [char](6) NULL,
	[SellingStoreIbtIn] [char](6) NULL,
	[Price] [decimal](7, 2) NOT NULL,
	[IsDeliveryChargeItem] [bit] NOT NULL,
	[QuantityScanned] [tinyint] NOT NULL,
	[RequiredFulfiller] [int] NULL,
 CONSTRAINT [PK_CORLIN] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[LINE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Customer Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line Number - 0001 to 9999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'LINE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'QTYO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity taken' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'QTYT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity to Refund' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'QTYR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total line weight - future' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'WGHT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total M3 volume of line - future
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'VOLU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Override Reason Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN', @level2type=N'COLUMN',@level2name=N'PORC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O R L I N  =  Customer Order Lines ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORLIN'
END
GO

