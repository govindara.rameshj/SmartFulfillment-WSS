﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeliveryChargeGroupValue]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    PRINT 'Creating table DeliveryChargeGroupValue'

    CREATE TABLE [dbo].DeliveryChargeGroupValue
    (
        Id int NOT NULL,
        [Group] char(1) NOT NULL,
        Value decimal(9,2) NOT NULL UNIQUE,
        CONSTRAINT FK_DeliveryChargeGroupValue_DeliveryChargeGroup FOREIGN KEY (Id, [Group]) REFERENCES DeliveryChargeGroup(Id, [Group])
    ) ON [PRIMARY]
END
GO
