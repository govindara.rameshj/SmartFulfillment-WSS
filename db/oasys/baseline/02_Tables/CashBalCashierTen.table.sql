﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CashBalCashierTen]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CashBalCashierTen'
CREATE TABLE [dbo].[CashBalCashierTen](
	[PeriodID] [int] NOT NULL,
	[CashierID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[ID] [int] NOT NULL,
	[Quantity] [decimal](5, 0) NOT NULL,
	[Amount] [decimal](9, 2) NOT NULL,
	[PickUp] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_CashBalCashierTen] PRIMARY KEY CLUSTERED 
(
	[PeriodID] ASC,
	[CashierID] ASC,
	[CurrencyID] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CashBalCashierTen', @level2type=N'COLUMN',@level2name=N'PeriodID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Identifier (Cashier who made the tender)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CashBalCashierTen', @level2type=N'COLUMN',@level2name=N'CashierID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Three characters to signify the currency being used for the tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CashBalCashierTen', @level2type=N'COLUMN',@level2name=N'CurrencyID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier for this tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CashBalCashierTen', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CashBalCashierTen', @level2type=N'COLUMN',@level2name=N'Quantity'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount (Value)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CashBalCashierTen', @level2type=N'COLUMN',@level2name=N'Amount'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pick Up (Value)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CashBalCashierTen', @level2type=N'COLUMN',@level2name=N'PickUp'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C A S H B A L C A S H I E R T E N = Cash Balancing by Cashier and Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CashBalCashierTen'
END
GO

