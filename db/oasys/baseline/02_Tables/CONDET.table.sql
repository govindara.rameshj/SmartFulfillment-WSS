﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CONDET]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CONDET'
CREATE TABLE [dbo].[CONDET](
	[ADEP] [char](3) NOT NULL,
	[CNUM] [char](9) NOT NULL,
	[PNUM] [char](6) NOT NULL,
	[LINE] [char](4) NOT NULL,
	[SKUN] [char](6) NULL,
	[QUAN] [int] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[REJI] [char](1) NULL,
 CONSTRAINT [PK_CONDET] PRIMARY KEY CLUSTERED 
(
	[ADEP] ASC,
	[CNUM] ASC,
	[PNUM] ASC,
	[LINE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assembly Depot Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET', @level2type=N'COLUMN',@level2name=N'ADEP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Container Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET', @level2type=N'COLUMN',@level2name=N'CNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store P/O Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET', @level2type=N'COLUMN',@level2name=N'PNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store P/O Line Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET', @level2type=N'COLUMN',@level2name=N'LINE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number      *W02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET', @level2type=N'COLUMN',@level2name=N'QUAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Used when record created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rejection Reason
Blank = not rejected
A = Invalid PURHDR record
B = PURHDR record not on file
C = PURHDR already received
D = PURHDR deleted
E = SOQ CTL number does not match
F = Invalid CONMAS record
G = CONMAS record not on file
H = New Is' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET', @level2type=N'COLUMN',@level2name=N'REJI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O N D E T  =   Container Detail Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONDET'
END
GO

