﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PCTTOS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PCTTOS'
CREATE TABLE [dbo].[PCTTOS](
	[FKEY] [char](2) NOT NULL,
	[TEND1] [bit] NOT NULL,
	[TEND2] [bit] NOT NULL,
	[TEND3] [bit] NOT NULL,
	[TEND4] [bit] NOT NULL,
	[TEND5] [bit] NOT NULL,
	[TEND6] [bit] NOT NULL,
	[TEND7] [bit] NOT NULL,
	[TEND8] [bit] NOT NULL,
	[TEND9] [bit] NOT NULL,
	[TEND10] [bit] NOT NULL,
	[TEND11] [bit] NOT NULL,
	[TEND12] [bit] NOT NULL,
	[TEND13] [bit] NOT NULL,
	[TEND14] [bit] NOT NULL,
	[TEND15] [bit] NOT NULL,
	[TEND16] [bit] NOT NULL,
	[TEND17] [bit] NOT NULL,
	[TEND18] [bit] NOT NULL,
	[TEND19] [bit] NOT NULL,
	[TEND20] [bit] NOT NULL,
	[SUPV] [bit] NOT NULL,
	[DOCN] [bit] NOT NULL,
	[SPRT] [bit] NOT NULL,
	[OPEN] [bit] NOT NULL,
	[SIGN] [bit] NOT NULL,
 CONSTRAINT [PK_PCTTOS] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'File Key
SA SC RF RC CC CO OD M+ M- +C -C XR ZR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Tender is valid for this Tran type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'TEND20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Supervisor required at start' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Ask for External Document Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'DOCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Special print - external Document' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'SPRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If zero tran, On = Open Drawer, Off = Do Not Open' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'OPEN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = positive    Off = Negative' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS', @level2type=N'COLUMN',@level2name=N'SIGN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P C T T O S    =  PC Till TOS Option Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PCTTOS'
END
GO

