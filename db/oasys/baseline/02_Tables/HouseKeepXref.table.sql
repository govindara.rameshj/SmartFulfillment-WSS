﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HouseKeepXref]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HouseKeepXref'
CREATE TABLE [dbo].[HouseKeepXref](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BOTableName] [char](30) NULL,
	[BOName] [char](40) NULL,
	[BOClass] [char](100) NULL,
 CONSTRAINT [PK_HouseKeepXref] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepXref', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Object Table Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepXref', @level2type=N'COLUMN',@level2name=N'BOTableName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Object Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepXref', @level2type=N'COLUMN',@level2name=N'BOName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Object Class' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepXref', @level2type=N'COLUMN',@level2name=N'BOClass'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H O U S E K E E P X R E F = House Keep Xref File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepXref'
END
GO

