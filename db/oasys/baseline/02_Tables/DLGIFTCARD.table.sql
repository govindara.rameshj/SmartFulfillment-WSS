﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLGIFTCARD]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLGIFTCARD'
CREATE TABLE [dbo].[DLGIFTCARD](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[CARDNUM] [char](19) NOT NULL,
	[EEID] [char](3) NULL,
	[TYPE] [char](2) NOT NULL,
	[AMNT] [decimal](9, 2) NULL,
	[AUTH] [char](150) NOT NULL,
	[MSGNUM] [char](150) NULL,
	[TRANID] [decimal](38, 0) NULL,
	[RTI] [char](1) NULL,
	[SEQN] [int] NOT NULL,
 CONSTRAINT [PK_DLGIFTCARD] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[AUTH] ASC,
	[SEQN] ASC,
	[TYPE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id - Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number from PC Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Serial Number of Gift Card' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'CARDNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee ID performing sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Type
	"SA" - Sale Of Voucher                - Allocation
	"TS" - Tender In Sale                 - Redemption
	"TR" - Tender In Refund               - Allocation
	"RR" - Refund Of Voucher              - Redemption
	"CS" - Cancel/line rever' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Amount - Opposite sign of Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'AMNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Authorisation Code assigned by Card Commerce' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'AUTH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Message number allocated to the transaction by Commidea' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'MSGNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction ID assigned by Commidea’s processing system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'TRANID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L G I F T C A R D  =   Daily Gift Card Transaction Lines.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLGIFTCARD'
END
GO

