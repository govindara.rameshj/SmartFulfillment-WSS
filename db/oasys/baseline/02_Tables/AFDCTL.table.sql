﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AFDCTL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table AFDCTL'
CREATE TABLE [dbo].[AFDCTL](
	[LANG] [char](3) NOT NULL,
	[AFGN] [char](2) NOT NULL,
	[AFGP] [char](2) NOT NULL,
	[DCOL] [decimal](3, 0) NULL,
	[TEXT1] [char](35) NULL,
	[TEXT2] [char](35) NULL,
	[TEXT3] [char](35) NULL,
	[TEXT4] [char](35) NULL,
	[TEXT5] [char](35) NULL,
	[TEXT6] [char](35) NULL,
	[TEXT7] [char](35) NULL,
	[TEXT8] [char](35) NULL,
	[TEXT9] [char](35) NULL,
	[TEXT10] [char](35) NULL,
	[TEXT11] [char](35) NULL,
	[TEXT12] [char](35) NULL,
	[TEXT13] [char](35) NULL,
	[TEXT14] [char](35) NULL,
	[TEXT15] [char](35) NULL,
	[LOCA] [bit] NOT NULL,
	[SPAR] [char](40) NULL,
 CONSTRAINT [PK_AFDCTL] PRIMARY KEY CLUSTERED 
(
	[LANG] ASC,
	[AFGN] ASC,
	[AFGP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Language Code - As used by Telephone Systems' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'LANG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Application Functional Group (AFG) Number
(See AFGCTL)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'AFGN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Program ID within AFG - Sequence Number
(See AFPCTL)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'AFGP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Defined Screen Color for the
documentation of the related menu selection.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'DCOL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'TEXT15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Yes=Allow this program to run on Stand Alone Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'LOCA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Spare for future use (Reduced from A70 - W02)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL', @level2type=N'COLUMN',@level2name=N'SPAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A F D C T L   =   AFG Program Documentation File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFDCTL'
END
GO

