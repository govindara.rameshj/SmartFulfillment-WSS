﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PlanNumbers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PlanNumbers'
CREATE TABLE [dbo].[PlanNumbers](
	[Number] [int] NOT NULL,
	[Name] [char](30) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Colour] [char](30) NULL,
 CONSTRAINT [PK_PlanNumbers] PRIMARY KEY CLUSTERED 
(
	[Number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Plan Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanNumbers', @level2type=N'COLUMN',@level2name=N'Number'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Plan Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanNumbers', @level2type=N'COLUMN',@level2name=N'Name'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Active False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanNumbers', @level2type=N'COLUMN',@level2name=N'IsActive'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Colour' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanNumbers', @level2type=N'COLUMN',@level2name=N'Colour'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P L A N N U M B E R S = Plan Number File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanNumbers'
END
GO

