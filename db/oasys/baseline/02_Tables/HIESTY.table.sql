﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HIESTY]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HIESTY'
CREATE TABLE [dbo].[HIESTY](
	[NUMB] [char](6) NOT NULL,
	[GROU] [char](6) NOT NULL,
	[SGRP] [char](6) NOT NULL,
	[STYL] [char](6) NOT NULL,
	[DESCR] [char](50) NULL,
	[ALPH] [char](50) NULL,
 CONSTRAINT [PK_HIESTY] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[GROU] ASC,
	[SGRP] ASC,
	[STYL] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Category Number - See HIECAT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIESTY', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Number    - See HIEGRP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIESTY', @level2type=N'COLUMN',@level2name=N'GROU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sub-Group Number within Category & Group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIESTY', @level2type=N'COLUMN',@level2name=N'SGRP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Style Number within Category, Group &
             Sub-Group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIESTY', @level2type=N'COLUMN',@level2name=N'STYL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIESTY', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Style Description as all CAPS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIESTY', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H I E S T Y  =  Product Hierarchy Style File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIESTY'
END
GO

