﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RMPMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RMPMAS'
CREATE TABLE [dbo].[RMPMAS](
	[PCOD] [char](8) NOT NULL,
	[PCODE] [char](15) NULL,
	[ADD1] [char](40) NULL,
	[ADD2] [char](40) NULL,
	[ADD3] [char](40) NULL,
	[ADD4] [char](40) NULL,
	[NUMB] [char](10) NOT NULL,
	[ALPH] [char](60) NOT NULL,
 CONSTRAINT [PK_RMPMAS] PRIMARY KEY CLUSTERED 
(
	[PCOD] ASC,
	[ALPH] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Post Code - key to the file (Blanks removed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS', @level2type=N'COLUMN',@level2name=N'PCOD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Post Code - As returned from CAPSCAN
There remains the possibility that the same Post
Code could be applicable to more than 1 street.
This PCOD will contain all elements of the Post Code
sent back after interrogating the Royal Mail database
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS', @level2type=N'COLUMN',@level2name=N'PCODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS', @level2type=N'COLUMN',@level2name=N'ADD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS', @level2type=N'COLUMN',@level2name=N'ADD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS', @level2type=N'COLUMN',@level2name=N'ADD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS', @level2type=N'COLUMN',@level2name=N'ADD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numbers in street to which the Post Code Applies' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alpha key' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R M P M A S  =  Royal Mail Post Code Master File  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMPMAS'
END
GO

