﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemLabels]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SystemLabels'
CREATE TABLE [dbo].[SystemLabels](
	[ID] [int] NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[Type] [int] NOT NULL,
	[Size] [int] NOT NULL,
	[Filename] [varchar](20) NOT NULL,
	[LabelsPerPage] [int] NOT NULL,
 CONSTRAINT [PK_SystemLabels] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemLabels', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemLabels', @level2type=N'COLUMN',@level2name=N'Description'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of label;
     0 =  None 
     1 =  Label
     2 =  Barcode
     3 =  Markdown
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemLabels', @level2type=N'COLUMN',@level2name=N'Type'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size of label;
    0 =  None
    1 =  Small
    2 =  Medium
    3 =  Large
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemLabels', @level2type=N'COLUMN',@level2name=N'Size'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filename of label' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemLabels', @level2type=N'COLUMN',@level2name=N'Filename'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Labels Per Page' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemLabels', @level2type=N'COLUMN',@level2name=N'LabelsPerPage'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S T E M L A B E L S  = System labels File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemLabels'
END
GO

