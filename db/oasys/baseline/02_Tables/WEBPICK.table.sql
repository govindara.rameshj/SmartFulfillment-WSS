﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WEBPICK]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table WEBPICK'
CREATE TABLE [dbo].[WEBPICK](
	[ORDN] [char](6) NULL,
	[SKUN] [char](6) NULL,
	[SEQN] [smallint] NULL,
	[QTY] [decimal](7, 0) NULL,
	[EEID] [char](3) NULL,
	[DATE1] [date] NULL,
	[TIME] [char](6) NULL
) ON [PRIMARY]
END
GO

