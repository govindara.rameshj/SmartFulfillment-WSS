﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[STKADJ]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table STKADJ'
CREATE TABLE [dbo].[STKADJ](
	[DATE1] [date] NOT NULL,
	[CODE] [char](2) NOT NULL,
	[SKUN] [char](6) NOT NULL,
	[SEQN] [char](2) NOT NULL,
	[AmendId] [int] NOT NULL,
	[INIT] [char](5) NULL,
	[DEPT] [char](2) NULL,
	[SSTK] [decimal](7, 0) NULL,
	[QUAN] [decimal](7, 0) NULL,
	[PRIC] [decimal](9, 2) NULL,
	[COST] [decimal](11, 4) NULL,
	[COMM] [bit] NOT NULL,
	[TYPE] [char](1) NULL,
	[TSKU] [char](6) NULL,
	[TVAL] [decimal](9, 2) NULL,
	[INFO] [char](20) NULL,
	[DRLN] [char](6) NULL,
	[RCOD] [char](1) NULL,
	[MOWT] [char](1) NULL,
	[WAUT] [char](3) NULL,
	[DAUT] [date] NULL,
	[RTI] [char](1) NULL,
	[PeriodID] [int] NOT NULL,
	[TransferStart] [decimal](7, 0) NULL,
	[TransferPrice] [decimal](9, 2) NULL,
	[IsReversed] [bit] NOT NULL,
	[ParentSeq] [char](2) NOT NULL,
	[AdjustmentCount] [int] NOT NULL,
 CONSTRAINT [PK_STKADJ] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[CODE] ASC,
	[SKUN] ASC,
	[SEQN] ASC,
	[AmendId] ASC,
	[AdjustmentCount] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of the adjustment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock adjustment code from SACODE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'CODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku number adjusted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Always 00 unless TRANSFER type - then 00 to 99
Added to allow more than 1 TRANSFER per SKU
W04TYPE 04 also allows multiple sequence numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'SEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amend Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'AmendId'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of who did the adjustment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'INIT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06From DEPT number (only if a SALES adjustment
No longer Used*W06*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'DEPT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting stock quantity - before adjustment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'SSTK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustment quantity
W05For :MOWT = M or W, the quantity specified here will
W05be the adjustment to the normal stock, whilst the
W05reverse sign of that quantity will be applied to the
W05mark-down or write off stock.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'QUAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price at time of adjustment
W05For :MOWT = M or W, the price of the item will be left
W05as zero to indicate a zero value adjustment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cost at time of adjustment
W05For :MOWT = M or W, the cost of the item will be left
W05as zero to indicate a zero cost adjustment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'COST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = has been comm''ed to head office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'COMM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustment type from SACODE file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NO LONGER USED - Transfer TO item (mark-down item)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'TSKU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NO LONGER USED - Transfer MARKDOWN value (qty*prc/dif)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'TVAL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustment Comment (keyed by operator)
                    * For code 10''s & 20''s = ppppppsdddsccccccccc,
                    *  pppppp    = P/O number
                    *  s         = space
                    *  ddd       = Assembly depot number
                    *  ccccccccc = Container number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'INFO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Number if CODE = 04 - H/O DRL Adjustment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'DRLN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reversal indicator
                    * R = Reversed Adjustment
                    * A = Reversal Adjustment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'RCOD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mark-down or Write Off Type - see :QUAN, :PRIC, :COST
                     M = Mark-down transfer
                     W = Write Off transfer
                blank = none of the above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'MOWT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D of person authorising Write Off (for :MOWT = W only) Added at authorisation time.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'WAUT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date write off was authorised.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'DAUT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'PeriodID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transfer Start' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'TransferStart'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transfer Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'TransferPrice'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Reversed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ', @level2type=N'COLUMN',@level2name=N'IsReversed'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S T K A D J  =   STocK ADJustment file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKADJ'
END
GO

