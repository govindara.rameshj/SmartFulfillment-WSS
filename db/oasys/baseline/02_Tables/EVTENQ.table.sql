﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EVTENQ]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table EVTENQ'
CREATE TABLE [dbo].[EVTENQ](
	[PRIO] [char](2) NULL,
	[NUMB] [char](6) NULL,
	[DLGN] [char](8) NULL,
	[ETYP] [char](2) NULL,
	[MMHS] [char](6) NULL,
	[DATC] [char](8) NULL,
	[SKUN] [char](6) NULL,
	[DAT1] [decimal](7, 0) NULL,
	[DAT2] [decimal](9, 2) NULL,
	[DTYP] [char](1) NULL,
	[DAT3] [decimal](9, 2) NULL,
	[IDEL] [bit] NULL,
	[BUYCPN] [char](7) NULL,
	[GETCPN] [char](7) NULL,
	[SPARE] [char](6) NULL
) ON [PRIMARY]
END
GO

