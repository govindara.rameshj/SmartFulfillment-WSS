﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[STKHIR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table STKHIR'
CREATE TABLE [dbo].[STKHIR](
	[SKUN] [char](6) NOT NULL,
	[HIER] [char](6) NOT NULL,
	[IDEL] [bit] NOT NULL,
	[SPARE] [varchar](50) NULL,
 CONSTRAINT [PK_STKHIR] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC,
	[HIER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number - Valid are 000001 to 999999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKHIR', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Entry Number
This may be any level of Hierarchy entry, as each
is unique within the Hierarchy structure.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKHIR', @level2type=N'COLUMN',@level2name=N'HIER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKHIR', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' S T K H I R  =  Stock Master Hierarchy Index Reference File Definition Each Sku should have 4 entries, one or each level of the hierarchy. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKHIR'
END
GO

