﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SYSDAT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SYSDAT'
CREATE TABLE [dbo].[SYSDAT](
	[FKEY] [char](2) NOT NULL,
	[DAYS] [decimal](3, 0) NULL,
	[WEND] [decimal](3, 0) NULL,
	[OPEN1] [decimal](3, 0) NULL,
	[OPEN2] [decimal](3, 0) NULL,
	[OPEN3] [decimal](3, 0) NULL,
	[OPEN4] [decimal](3, 0) NULL,
	[OPEN5] [decimal](3, 0) NULL,
	[OPEN6] [decimal](3, 0) NULL,
	[OPEN7] [decimal](3, 0) NULL,
	[OPEN8] [decimal](3, 0) NULL,
	[OPEN9] [decimal](3, 0) NULL,
	[OPEN10] [decimal](3, 0) NULL,
	[OPEN11] [decimal](3, 0) NULL,
	[OPEN12] [decimal](3, 0) NULL,
	[OPEN13] [decimal](3, 0) NULL,
	[OPEN14] [decimal](3, 0) NULL,
	[TODT] [date] NULL,
	[TODW] [decimal](3, 0) NULL,
	[TMDT] [date] NULL,
	[TMDW] [decimal](3, 0) NULL,
	[WKDT] [date] NULL,
	[WK131] [date] NULL,
	[WK132] [date] NULL,
	[WK133] [date] NULL,
	[WK134] [date] NULL,
	[WK135] [date] NULL,
	[WK136] [date] NULL,
	[WK137] [date] NULL,
	[WK138] [date] NULL,
	[WK139] [date] NULL,
	[WK1310] [date] NULL,
	[WK1311] [date] NULL,
	[WK1312] [date] NULL,
	[WK1313] [date] NULL,
	[LIVE] [date] NULL,
	[PSET] [decimal](3, 0) NULL,
	[S1DT1] [date] NULL,
	[S1DT2] [date] NULL,
	[S1DT3] [date] NULL,
	[S1DT4] [date] NULL,
	[S1DT5] [date] NULL,
	[S1DT6] [date] NULL,
	[S1DT7] [date] NULL,
	[S1DT8] [date] NULL,
	[S1DT9] [date] NULL,
	[S1DT10] [date] NULL,
	[S1DT11] [date] NULL,
	[S1DT12] [date] NULL,
	[S1DT13] [date] NULL,
	[S1DT14] [date] NULL,
	[S1YR1] [bit] NOT NULL,
	[S1YR2] [bit] NOT NULL,
	[S1YR3] [bit] NOT NULL,
	[S1YR4] [bit] NOT NULL,
	[S1YR5] [bit] NOT NULL,
	[S1YR6] [bit] NOT NULL,
	[S1YR7] [bit] NOT NULL,
	[S1YR8] [bit] NOT NULL,
	[S1YR9] [bit] NOT NULL,
	[S1YR10] [bit] NOT NULL,
	[S1YR11] [bit] NOT NULL,
	[S1YR12] [bit] NOT NULL,
	[S1YR13] [bit] NOT NULL,
	[S1YR14] [bit] NOT NULL,
	[S1PF1] [bit] NOT NULL,
	[S1PF2] [bit] NOT NULL,
	[S1PF3] [bit] NOT NULL,
	[S1PF4] [bit] NOT NULL,
	[S1PF5] [bit] NOT NULL,
	[S1PF6] [bit] NOT NULL,
	[S1PF7] [bit] NOT NULL,
	[S1PF8] [bit] NOT NULL,
	[S1PF9] [bit] NOT NULL,
	[S1PF10] [bit] NOT NULL,
	[S1PF11] [bit] NOT NULL,
	[S1PF12] [bit] NOT NULL,
	[S1PF13] [bit] NOT NULL,
	[S1PF14] [bit] NOT NULL,
	[S2DT1] [date] NULL,
	[S2DT2] [date] NULL,
	[S2DT3] [date] NULL,
	[S2DT4] [date] NULL,
	[S2DT5] [date] NULL,
	[S2DT6] [date] NULL,
	[S2DT7] [date] NULL,
	[S2DT8] [date] NULL,
	[S2DT9] [date] NULL,
	[S2DT10] [date] NULL,
	[S2DT11] [date] NULL,
	[S2DT12] [date] NULL,
	[S2DT13] [date] NULL,
	[S2DT14] [date] NULL,
	[S2YR1] [bit] NOT NULL,
	[S2YR2] [bit] NOT NULL,
	[S2YR3] [bit] NOT NULL,
	[S2YR4] [bit] NOT NULL,
	[S2YR5] [bit] NOT NULL,
	[S2YR6] [bit] NOT NULL,
	[S2YR7] [bit] NOT NULL,
	[S2YR8] [bit] NOT NULL,
	[S2YR9] [bit] NOT NULL,
	[S2YR10] [bit] NOT NULL,
	[S2YR11] [bit] NOT NULL,
	[S2YR12] [bit] NOT NULL,
	[S2YR13] [bit] NOT NULL,
	[S2YR14] [bit] NOT NULL,
	[S2PF1] [bit] NOT NULL,
	[S2PF2] [bit] NOT NULL,
	[S2PF3] [bit] NOT NULL,
	[S2PF4] [bit] NOT NULL,
	[S2PF5] [bit] NOT NULL,
	[S2PF6] [bit] NOT NULL,
	[S2PF7] [bit] NOT NULL,
	[S2PF8] [bit] NOT NULL,
	[S2PF9] [bit] NOT NULL,
	[S2PF10] [bit] NOT NULL,
	[S2PF11] [bit] NOT NULL,
	[S2PF12] [bit] NOT NULL,
	[S2PF13] [bit] NOT NULL,
	[S2PF14] [bit] NOT NULL,
	[TIDT] [date] NULL,
	[LADT] [date] NULL,
	[TIYR] [bit] NOT NULL,
	[PPER] [bit] NOT NULL,
	[PWEK] [bit] NOT NULL,
	[NSET] [char](1) NULL,
	[NITE] [char](3) NULL,
	[RETY] [bit] NOT NULL,
	[CYNO] [decimal](3, 0) NULL,
	[CYCW] [decimal](3, 0) NULL,
	[AUDT] [date] NULL,
	[DPCA] [decimal](3, 0) NULL,
	[LDAT] [date] NULL,
 CONSTRAINT [PK_SYSDAT] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Number - key to file - Always set to 01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of days per week store is open' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'DAYS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Week Ending Day of Week 1 to 7 = Mon to Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WEND'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days open last 14 Periods ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'OPEN14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Todays date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'TODT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Todays Day of Week Code 1 to 7 = Monday to Sunday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'TODW'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tomorrows date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'TMDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tomorrows Day of Week Code as above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'TMDW'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'One Week from today' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WKDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK131'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK132'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK133'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK134'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK135'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK136'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK137'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK138'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK139'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK1310'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK1311'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK1312'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last 13 Week Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'WK1313'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store live Date - Checked by NITMAS routine' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'LIVE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Use 1st set    2 = Use 2nd set' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'PSET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set one 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1DT14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1YR14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S1PF14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set two 14 Period Ending Dates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2DT14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Year End, Else Period End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2YR14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Processed, Else due to be processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'S2PF14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This Current Period/Year End Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'TIDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior Period/Year Ending Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'LADT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Current end is a Year end' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'TIYR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Period end detected has NOT been processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'PPER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Week end detected has NOT been processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'PWEK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net Set Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'NSET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JSNITE STEP NUBMER 000 = COMPLETED OK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'NITE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = IN RETRY MODE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'RETY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Weeks in the Cycle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'CYNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current Week Number in the Cycle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'CYCW'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'AUDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Days Prior to Price Change Effective Date when P/C
   can be applied.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'DPCA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Date upon which the last nightly close was started' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT', @level2type=N'COLUMN',@level2name=N'LDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S D A T  =   System Dates Record  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSDAT'
END
GO

