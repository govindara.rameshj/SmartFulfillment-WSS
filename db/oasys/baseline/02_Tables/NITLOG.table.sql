﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NITLOG]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table NITLOG'
CREATE TABLE [dbo].[NITLOG](
	[DATE1] [date] NOT NULL,
	[NSET] [char](1) NOT NULL,
	[TASK] [char](3) NOT NULL,
	[DESCR] [char](40) NULL,
	[PROG] [char](150) NULL,
	[SDAT] [date] NULL,
	[STIM] [char](6) NULL,
	[EDAT] [date] NULL,
	[ETIM] [char](6) NULL,
	[ADAT] [date] NULL,
	[ATIM] [char](6) NULL,
	[JERR] [char](1) NULL,
	[EMES] [char](60) NULL,
 CONSTRAINT [PK_NITLOG] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[NSET] ASC,
	[TASK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Nightly Log relates to - From SD:TODT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set Number                  - From NC:NSET' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'NSET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Task Number                 - From NC:TASK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'TASK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Program to RUN              - From NC:PROG' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'PROG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Task Started' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'SDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Task Started' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'STIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Task Ended' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'EDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Task Ended' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'ETIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Task Aborted (Set if Aborted)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'ADAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Task Aborted (Set if Aborted)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'ATIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Job Error         (Set if Aborted)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'JERR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Error Message     (Set if Aborted)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG', @level2type=N'COLUMN',@level2name=N'EMES'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'N I T L O G  =   Nightly Procedure Log ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITLOG'
END
GO

