﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SUPNOT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SUPNOT'
CREATE TABLE [dbo].[SUPNOT](
	[SUPN] [char](5) NOT NULL,
	[TYPE] [char](3) NOT NULL,
	[SEQN] [char](3) NOT NULL,
	[TEXT] [char](75) NULL,
 CONSTRAINT [PK_SUPNOT] PRIMARY KEY CLUSTERED 
(
	[SUPN] ASC,
	[TYPE] ASC,
	[SEQN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number - key to the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPNOT', @level2type=N'COLUMN',@level2name=N'SUPN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Narrative Type - 001 = Returns Policy
     002 = Damages Policy
     003 = Non-delivery Policy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPNOT', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Narrative Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPNOT', @level2type=N'COLUMN',@level2name=N'SEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Narrative Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPNOT', @level2type=N'COLUMN',@level2name=N'TEXT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S U P N O T  =   Supplier Notes File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPNOT'
END
GO

