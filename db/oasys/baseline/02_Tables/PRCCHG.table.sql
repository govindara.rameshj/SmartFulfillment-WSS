﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PRCCHG]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PRCCHG'
CREATE TABLE [dbo].[PRCCHG](
	[SKUN] [char](6) NOT NULL,
	[PDAT] [date] NOT NULL,
	[PRIC] [decimal](9, 2) NOT NULL,
	[PSTA] [char](1) NOT NULL,
	[SHEL] [bit] NOT NULL,
	[AUDT] [date] NULL,
	[AUAP] [date] NULL,
	[MARK] [decimal](9, 2) NULL,
	[MCOM] [bit] NOT NULL,
	[LABS] [bit] NOT NULL,
	[LABM] [bit] NOT NULL,
	[LABL] [bit] NOT NULL,
	[EVNT] [char](6) NOT NULL,
	[PRIO] [char](2) NULL,
	[EEID] [char](3) NULL,
	[MEID] [char](3) NULL,
 CONSTRAINT [PK_PRCCHG] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC,
	[PDAT] ASC,
	[EVNT] ASC,
	[PRIC] ASC,
	[PSTA] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku number for this price change' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Change Effective Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'PDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New retail Selling price - moved to IM:PRIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status  U=Unapplied  A=Applied  S=System Applied
  *W03 H= HHT applied, L= Applied before Label print
  *W03 W= Applied with warning(if set prior to system app.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'PSTA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Y/N - Shelf Edge Label of at least one size has
been printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'SHEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Auto Apply Date if RO:PCAA = Y' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'AUDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Price was auto applied if applicable
  *W03 Date Price was applied in all cases (inc.via HHT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'AUAP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Calculated Markup/down at time of apply' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'MARK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markup commed to H/O yet - Y = yes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'MCOM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Small label print required indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'LABS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Medium  "     "       "        "' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'LABM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Large   "     "       "  "' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'LABL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Price Change Event Number   - (See EVTHDR)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'EVNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Price Change Event Priority - (See EVTHDR)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'PRIO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Employee Id.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Manager  Id.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG', @level2type=N'COLUMN',@level2name=N'MEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P R C C H G  =  Waiting Price Changes File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRCCHG'
END
GO

