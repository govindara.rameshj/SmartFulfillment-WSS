﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TMP_MISSINGTRANS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TMP_MISSINGTRANS'
CREATE TABLE [dbo].[TMP_MISSINGTRANS](
	[TillNo] [int] NULL,
	[TranNo] [int] NULL
) ON [PRIMARY]
END
GO

