﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PlanStocks]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PlanStocks'
CREATE TABLE [dbo].[PlanStocks](
	[Number] [int] NOT NULL,
	[Segment] [tinyint] NOT NULL,
	[Fixture] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[Facings] [int] NOT NULL,
	[Capacity] [int] NOT NULL,
	[Pack] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_PlanStocks] PRIMARY KEY CLUSTERED 
(
	[Number] ASC,
	[Segment] ASC,
	[Fixture] ASC,
	[Sequence] ASC,
	[SkuNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'Number'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Segment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'Segment'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fixture Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'Fixture'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'Sequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'SkuNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Facings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'Facings'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Capacity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'Capacity'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pack Size' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'Pack'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If active False If Not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks', @level2type=N'COLUMN',@level2name=N'IsActive'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P L A N S T O C K S = Plan Stock File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlanStocks'
END
GO

