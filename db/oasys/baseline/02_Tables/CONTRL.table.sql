﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CONTRL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CONTRL'
CREATE TABLE [dbo].[CONTRL](
	[FKEY] [char](2) NOT NULL,
	[EFMA] [bit] NOT NULL,
	[IEVT] [bit] NOT NULL,
 CONSTRAINT [PK_CONTRL] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key ( always 01 )' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONTRL', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EFTPOS mode - Y = Manual , N = Automatic' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONTRL', @level2type=N'COLUMN',@level2name=N'EFMA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Y/N Till permitted to access (store) Event fil' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONTRL', @level2type=N'COLUMN',@level2name=N'IEVT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O N T R L  =   Retail control record ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONTRL'
END
GO

