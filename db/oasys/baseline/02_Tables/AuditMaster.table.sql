﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AuditMaster]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table AuditMaster'
CREATE TABLE [dbo].[AuditMaster](
	[AuditDate] [date] NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[NormalStock] [int] NOT NULL,
	[NormalCount] [int] NOT NULL,
	[MarkdownStock] [int] NOT NULL,
	[MarkdownCount] [int] NOT NULL,
	[SalesYearToDate] [decimal](9, 2) NOT NULL,
	[SalesPreviousYear] [decimal](9, 2) NOT NULL,
	[EntryMade] [bit] NOT NULL,
 CONSTRAINT [PK_AuditMaster] PRIMARY KEY CLUSTERED 
(
	[AuditDate] ASC,
	[SkuNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of the Audit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditMaster', @level2type=N'COLUMN',@level2name=N'AuditDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditMaster', @level2type=N'COLUMN',@level2name=N'SkuNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Total so far this year to date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditMaster', @level2type=N'COLUMN',@level2name=N'SalesYearToDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales total the previous year' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditMaster', @level2type=N'COLUMN',@level2name=N'SalesPreviousYear'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A U D I T M A S T ER  =  Sample stock audit File Definition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuditMaster'
END
GO

