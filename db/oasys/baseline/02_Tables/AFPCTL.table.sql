﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AFPCTL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table AFPCTL'
CREATE TABLE [dbo].[AFPCTL](
	[LANG] [char](3) NOT NULL,
	[AFGN] [char](2) NOT NULL,
	[NUMB] [char](2) NOT NULL,
	[DESCR] [char](35) NULL,
	[SECL] [char](3) NULL,
	[SDPW] [bit] NOT NULL,
	[DOSA] [bit] NOT NULL,
	[DOSE] [decimal](3, 0) NULL,
	[IDOC] [bit] NOT NULL,
	[SCOL] [decimal](3, 0) NULL,
	[PRUN] [char](70) NULL,
	[LOCA] [bit] NOT NULL,
	[SIGN] [bit] NOT NULL,
	[SPAR] [char](40) NULL,
 CONSTRAINT [PK_AFPCTL] PRIMARY KEY CLUSTERED 
(
	[LANG] ASC,
	[AFGN] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Language Code - As used by Telephone Systems' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'LANG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Application Functional Group (AFG) Number

Tilling, Customer Service, Cash Office, etc
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'AFGN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Program ID within AFG - Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level required to run this program.
Checked against SYSPAS SP:SECL(O=AFGN)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'SECL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Service Desk Password required to run
 this program' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'SDPW'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = This Program is a call to a DOS
 application. eg: Word Processor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'DOSA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DOS error level.  (00 - 99)

Used by the PC UPTRAC menu batch file to allow for
the selection of a DOS application from the PC
UPTRAC menu.

This will exit the PC UPTRAC menu and pass the error
level to the batch file, which in turn will make ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'DOSE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Documentation for this program option
exists in AFDCTL.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'IDOC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Defined Screen Color for this Menu Selection' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'SCOL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Program Run statement

If blank, this is a heading line, e.g.
reporting, maintenance, etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'PRUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Yes=Allow this program to run on Stand Alone Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'LOCA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sign (Plus or Minus)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'SIGN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Spare for future use (Reduced from A29)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL', @level2type=N'COLUMN',@level2name=N'SPAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A F P C T L   =   Application Functional Programs Control File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFPCTL'
END
GO

