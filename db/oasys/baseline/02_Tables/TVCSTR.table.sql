﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TVCSTR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TVCSTR'
CREATE TABLE [dbo].[TVCSTR](
	[FILE] [char](5) NOT NULL,
	[VRSN] [char](2) NOT NULL,
	[SEQN] [char](6) NULL,
	[AVER] [char](2) NULL,
	[ASEQ] [char](6) NULL,
	[PVER] [char](2) NULL,
	[PSEQ] [char](6) NULL,
	[DATE1] [date] NULL,
	[TIME] [char](8) NULL,
	[RTYP1] [char](2) NULL,
	[RTYP2] [char](2) NULL,
	[RTYP3] [char](2) NULL,
	[RTYP4] [char](2) NULL,
	[RTYP5] [char](2) NULL,
	[RTYP6] [char](2) NULL,
	[RTYP7] [char](2) NULL,
	[RTYP8] [char](2) NULL,
	[RTYP9] [char](2) NULL,
	[RTYP10] [char](2) NULL,
	[RTYP11] [char](2) NULL,
	[RTYP12] [char](2) NULL,
	[RTYP13] [char](2) NULL,
	[RTYP14] [char](2) NULL,
	[RTYP15] [char](2) NULL,
	[HREC1] [decimal](7, 0) NULL,
	[HREC2] [decimal](7, 0) NULL,
	[HREC3] [decimal](7, 0) NULL,
	[HREC4] [decimal](7, 0) NULL,
	[HREC5] [decimal](7, 0) NULL,
	[HREC6] [decimal](7, 0) NULL,
	[HREC7] [decimal](7, 0) NULL,
	[HREC8] [decimal](7, 0) NULL,
	[HREC9] [decimal](7, 0) NULL,
	[HREC10] [decimal](7, 0) NULL,
	[HREC11] [decimal](7, 0) NULL,
	[HREC12] [decimal](7, 0) NULL,
	[HREC13] [decimal](7, 0) NULL,
	[HREC14] [decimal](7, 0) NULL,
	[HREC15] [decimal](7, 0) NULL,
	[HVAL1] [decimal](11, 2) NULL,
	[HVAL2] [decimal](11, 2) NULL,
	[HVAL3] [decimal](11, 2) NULL,
	[HVAL4] [decimal](11, 2) NULL,
	[HVAL5] [decimal](11, 2) NULL,
	[HVAL6] [decimal](11, 2) NULL,
	[HVAL7] [decimal](11, 2) NULL,
	[HVAL8] [decimal](11, 2) NULL,
	[HVAL9] [decimal](11, 2) NULL,
	[HVAL10] [decimal](11, 2) NULL,
	[HVAL11] [decimal](11, 2) NULL,
	[HVAL12] [decimal](11, 2) NULL,
	[HVAL13] [decimal](11, 2) NULL,
	[HVAL14] [decimal](11, 2) NULL,
	[HVAL15] [decimal](11, 2) NULL,
	[FLAG] [bit] NOT NULL,
	[ECOD] [char](2) NULL,
 CONSTRAINT [PK_TVCSTR] PRIMARY KEY CLUSTERED 
(
	[FILE] ASC,
	[VRSN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'File name -  e.g.
 STHOA = from STore to Head Office Activities
       ^^       ^    ^      ^
 BBSTI = from BBc to STore Issues
       ^^     ^^    ^' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'FILE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If Header: 00
If Detail: 01 - 31' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'VRSN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If Header: 000000 Sequence number
If Detail: 000001 - 999999 Sequence number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'SEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Latest Available Version number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'AVER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Latest Available Sequence number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'ASEQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'PVER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'PSEQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of this version & sequence no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time of this version & sequence no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'TIME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'RTYP15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HREC15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Hash per Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'HVAL15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'FLAG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Error Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR', @level2type=N'COLUMN',@level2name=N'ECOD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'T V C S T R  =   Store transmission/validation control file ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TVCSTR'
END
GO

