﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GapWalk]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table GapWalk'
CREATE TABLE [dbo].[GapWalk](
	[ID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[DateCreated] [date] NOT NULL,
	[Location] [char](10) NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[QuantityRequired] [int] NOT NULL,
	[IsPrinted] [bit] NOT NULL,
 CONSTRAINT [PK_GapWalk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GapWalk', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GapWalk', @level2type=N'COLUMN',@level2name=N'Sequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creation Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GapWalk', @level2type=N'COLUMN',@level2name=N'DateCreated'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Location' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GapWalk', @level2type=N'COLUMN',@level2name=N'Location'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GapWalk', @level2type=N'COLUMN',@level2name=N'SkuNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GapWalk', @level2type=N'COLUMN',@level2name=N'QuantityRequired'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if printed False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GapWalk', @level2type=N'COLUMN',@level2name=N'IsPrinted'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'G A P W A L K = Gap Walk File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GapWalk'
END
GO

