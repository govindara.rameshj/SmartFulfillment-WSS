﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SecurityProfile]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SecurityProfile'
CREATE TABLE [dbo].[SecurityProfile](
	[ID] [int] NOT NULL,
	[ProfileType] [char](1) NOT NULL,
	[Description] [char](35) NOT NULL,
	[Position] [char](20) NULL,
	[IsSupervisor] [bit] NOT NULL,
	[IsManager] [bit] NOT NULL,
	[IsAreaManager] [bit] NOT NULL,
	[PasswordValidFor] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedBy] [char](20) NULL,
	[DeletedDate] [datetime] NULL,
	[DateLastEdited] [datetime] NULL,
	[IsSystemAdmin] [bit] NOT NULL,
	[IsHelpDeskUser] [bit] NOT NULL,
 CONSTRAINT [PK_SecurityProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Profile Type;
   * W = Workstation
   * U = User' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'ProfileType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'Description'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Position ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'Position'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If supervisor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'IsSupervisor'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Manager' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'IsManager'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if Area Manager' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'IsAreaManager'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Password Valid For Number of Days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'PasswordValidFor'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If it can be deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'IsDeleted'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deleted By Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'DeletedBy'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deleted Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'DeletedDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Last Edited' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'DateLastEdited'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If System Administrator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'IsSystemAdmin'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If is a Help Desk User' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile', @level2type=N'COLUMN',@level2name=N'IsHelpDeskUser'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S E C U R I T Y P R O F I L E = Security Profile File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SecurityProfile'
END
GO

