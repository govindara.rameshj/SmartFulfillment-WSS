﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VehicleCollectionHeader]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table VehicleCollectionHeader'
CREATE TABLE [dbo].[VehicleCollectionHeader](
	[CollectionID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NULL,
	[TotalWeight] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[CustomerAddress1] [varchar](50) NULL,
	[CustomerAddress2] [varchar](50) NULL,
	[CustomerAddress3] [varchar](50) NULL,
	[PostCode] [varchar](8) NULL,
	[VehicleTypeIndex] [varchar](10) NOT NULL,
	[DeliveryDate] [date] NOT NULL,
	[LastUpdateDateTime] [datetime] NULL,
 CONSTRAINT [PK_VehicleCollectionHeader] PRIMARY KEY CLUSTERED 
(
	[CollectionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

