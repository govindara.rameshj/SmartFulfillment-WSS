﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TMP_QOD]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TMP_QOD'
CREATE TABLE [dbo].[TMP_QOD](
	[Date] [date] NULL,
	[Day] [nvarchar](30) NULL,
	[Total] [int] NULL,
	[Income] [decimal](38, 2) NULL,
	[Weight] [decimal](38, 2) NULL,
	[Volume] [decimal](38, 2) NULL
) ON [PRIMARY]
END
GO

