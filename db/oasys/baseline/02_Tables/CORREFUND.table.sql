﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CORREFUND]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CORREFUND'
CREATE TABLE [dbo].[CORREFUND](
	[NUMB] [char](6) NOT NULL,
	[LINE] [char](4) NOT NULL,
	[RefundStoreId] [int] NOT NULL,
	[RefundDate] [date] NOT NULL,
	[RefundTill] [char](2) NOT NULL,
	[RefundTransaction] [char](4) NOT NULL,
	[QtyReturned] [int] NOT NULL,
	[QtyCancelled] [int] NOT NULL,
	[RefundStatus] [int] NOT NULL,
	[SellingStoreIbtOut] [char](6) NULL,
	[FulfillingStoreIbtIn] [char](6) NULL,
 CONSTRAINT [PK_CORRefund] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[LINE] ASC,
	[RefundStoreId] ASC,
	[RefundDate] ASC,
	[RefundTill] ASC,
	[RefundTransaction] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

