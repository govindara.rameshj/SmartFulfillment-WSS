﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CashBalCashierTenVar]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CashBalCashierTenVar'
CREATE TABLE [dbo].[CashBalCashierTenVar](
	[PeriodID] [int] NOT NULL,
	[TradingPeriodID] [int] NOT NULL,
	[CashierID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[ID] [int] NOT NULL,
	[Quantity] [decimal](5, 0) NOT NULL,
	[Amount] [decimal](9, 2) NOT NULL,
	[PickUp] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_CashBalCashierTenVar] PRIMARY KEY CLUSTERED 
(
	[PeriodID] ASC,
	[TradingPeriodID] ASC,
	[CashierID] ASC,
	[CurrencyID] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

