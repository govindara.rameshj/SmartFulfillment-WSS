﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HTRSUMold]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HTRSUMold'
CREATE TABLE [dbo].[HTRSUMold](
	[DATE1] [date] NOT NULL,
	[STIM] [char](6) NOT NULL,
	[ETIM] [char](6) NOT NULL,
	[NSAL] [decimal](7, 0) NOT NULL,
	[VSAL] [decimal](9, 2) NOT NULL,
	[NREF] [decimal](7, 0) NOT NULL,
	[VREF] [decimal](9, 2) NOT NULL,
	[SLOT] [decimal](3, 0) NOT NULL,
	[COMM] [bit] NOT NULL
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start Time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'STIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'End Time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'ETIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Sale Transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'NSAL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Of Sales Transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'VSAL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Refund Transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'NREF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Of Refund Transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'VREF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Slot Selected For This Record (Info Only)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'SLOT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True = Commed to H/O via STHOT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold', @level2type=N'COLUMN',@level2name=N'COMM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H T R S U M  =   Half Hourly TRADE SUMMARY record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HTRSUMold'
END
GO

