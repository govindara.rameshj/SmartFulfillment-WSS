﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CONSUM]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CONSUM'
CREATE TABLE [dbo].[CONSUM](
	[ADEP] [char](3) NOT NULL,
	[CNUM] [char](9) NOT NULL,
	[DDAT] [date] NULL,
	[DESCR] [char](36) NULL,
	[DDEP] [char](3) NULL,
	[VREF] [char](9) NULL,
	[TOTL] [int] NULL,
	[PNUM] [char](9) NULL,
	[IREC] [bit] NOT NULL,
	[IPRT] [bit] NOT NULL,
	[DELD] [date] NULL,
	[VALU] [decimal](11, 2) NULL,
	[APRT] [bit] NOT NULL,
	[REJI] [char](1) NULL,
 CONSTRAINT [PK_CONSUM] PRIMARY KEY CLUSTERED 
(
	[ADEP] ASC,
	[CNUM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assembly Depot Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'ADEP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Container Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'CNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dispatch Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'DDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dispatch Depot Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'DDEP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle Load Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'VREF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Number of Lines in Container' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'TOTL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent Container Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'PNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'IREC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Print Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'IPRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'DELD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Of The Contents of the Container' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'VALU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Automatically Printed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'APRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rejection Reason
Blank = not rejected
A = Invalid PURHDR record
B = PURHDR record not on file
C = PURHDR already received
D = PURHDR deleted
E = SOQ CTL number does not match
F = Invalid CONMAS record
G = CONMAS record not on file
H = New Is' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM', @level2type=N'COLUMN',@level2name=N'REJI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O N S U M  =   Container Summary Records ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONSUM'
END
GO

