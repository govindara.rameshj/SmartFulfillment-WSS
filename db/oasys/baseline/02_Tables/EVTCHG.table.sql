﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EVTCHG]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table EVTCHG'
CREATE TABLE [dbo].[EVTCHG](
	[SKUN] [char](6) NOT NULL,
	[SDAT] [date] NOT NULL,
	[PRIO] [char](2) NOT NULL,
	[NUMB] [char](6) NOT NULL,
	[EDAT] [date] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[IDEL] [bit] NOT NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_EVTCHG] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC,
	[SDAT] ASC,
	[PRIO] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number for this Price Change' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Start Date (Null Date NOT Allowed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG', @level2type=N'COLUMN',@level2name=N'SDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Priority from EVTHDR:PRIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG', @level2type=N'COLUMN',@level2name=N'PRIO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Number (See EVTHDR)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event Ending Date (Null Date Implies Permanent
Price)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG', @level2type=N'COLUMN',@level2name=N'EDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Event Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delete Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for Future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'E V T C H G  =  Price Event Change File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EVTCHG'
END
GO

