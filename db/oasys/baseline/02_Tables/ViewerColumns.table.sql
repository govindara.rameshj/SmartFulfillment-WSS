﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ViewerColumns]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ViewerColumns'
CREATE TABLE [dbo].[ViewerColumns](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ConfigID] [int] NOT NULL,
	[ColumnName] [varchar](50) NOT NULL,
	[ColumnText] [varchar](50) NULL,
	[ColumnHelp] [varchar](250) NULL,
	[HyperLink] [varchar](250) NULL,
	[HyperLinkType] [tinyint] NOT NULL,
	[SelectionType] [tinyint] NOT NULL,
	[LookupConfigID] [int] NOT NULL,
	[LookupColumnID] [int] NOT NULL,
	[DisplayColumn] [bit] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[TabIDs] [varchar](50) NULL,
	[IsEditable] [bit] NOT NULL,
 CONSTRAINT [PK_ViewerColumns] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to the Id for ViewerConfiguration Table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'ConfigID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Column Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'ColumnName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Column Text to display' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'ColumnText'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Column Help' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'ColumnHelp'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hyper Link' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'HyperLink'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hyper Link Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'HyperLinkType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Selection Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'SelectionType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lookup Configuration Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'LookupConfigID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lookup Column Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'LookupColumnID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Display Column' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'DisplayColumn'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Display Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'DisplayOrder'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to the table ViewerTabs Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'TabIDs'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Editable False If not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns', @level2type=N'COLUMN',@level2name=N'IsEditable'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'V I E W E R C O L U M N S = Report Viewing Columns File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerColumns'
END
GO

