﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ViewerConfig]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ViewerConfig'
CREATE TABLE [dbo].[ViewerConfig](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [char](30) NOT NULL,
	[BOName] [char](50) NOT NULL,
	[IsHorizontal] [bit] NOT NULL,
	[RecordLimit] [int] NOT NULL,
	[AllowAddition] [bit] NOT NULL,
	[AllowDeletion] [bit] NOT NULL,
	[AllowUpdate] [bit] NOT NULL,
 CONSTRAINT [PK_ViewerConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerConfig', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Title of report' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerConfig', @level2type=N'COLUMN',@level2name=N'Title'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Object Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerConfig', @level2type=N'COLUMN',@level2name=N'BOName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if layout is to be Horizontal, False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerConfig', @level2type=N'COLUMN',@level2name=N'IsHorizontal'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum Number of records to display' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerConfig', @level2type=N'COLUMN',@level2name=N'RecordLimit'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'V I E W E R C O N F I G = Report Viewer Configuration File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerConfig'
END
GO

