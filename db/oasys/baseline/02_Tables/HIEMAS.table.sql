﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HIEMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HIEMAS'
CREATE TABLE [dbo].[HIEMAS](
	[LEVL] [char](1) NOT NULL,
	[NUMB] [char](6) NOT NULL,
	[DESCR] [char](50) NULL,
	[IDEL] [bit] NOT NULL,
	[MaxOverride] [int] NULL,
	[ReductionWeek1] [decimal](9, 4) NULL,
	[ReductionWeek2] [decimal](9, 4) NULL,
	[ReductionWeek3] [decimal](9, 4) NULL,
	[ReductionWeek4] [decimal](9, 4) NULL,
	[ReductionWeek5] [decimal](9, 4) NULL,
	[ReductionWeek6] [decimal](9, 4) NULL,
	[ReductionWeek7] [decimal](9, 4) NULL,
	[ReductionWeek8] [decimal](9, 4) NULL,
	[ReductionWeek9] [decimal](9, 4) NULL,
	[ReductionWeek10] [decimal](9, 4) NULL,
 CONSTRAINT [PK_HIEMAS] PRIMARY KEY CLUSTERED 
(
	[LEVL] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Level
   *
   5 - Product Category
   4 -         Group
   3 -         Sub-Group
   2 -         Style
   *' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'LEVL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy Member Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = To Be Deleted as soon as there are NO Skus
        linked to this Hierarchy Level & Number.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum Override' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'MaxOverride'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reduction Week 10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS', @level2type=N'COLUMN',@level2name=N'ReductionWeek10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H I E M A S  =  Product Hierarchy Master File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HIEMAS'
END
GO

