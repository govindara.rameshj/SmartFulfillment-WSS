﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[STRMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table STRMAS'
CREATE TABLE [dbo].[STRMAS](
	[NUMB] [char](3) NOT NULL,
	[ADD1] [char](30) NULL,
	[ADD2] [char](30) NULL,
	[ADD3] [char](30) NULL,
	[ADD4] [char](30) NULL,
	[TILD] [char](12) NULL,
	[PHON] [char](12) NULL,
	[SFAX] [char](12) NULL,
	[MANG] [char](20) NULL,
	[REGC] [char](2) NULL,
	[DELC] [bit] NOT NULL,
	[CountryCode] [char](3) NULL,
 CONSTRAINT [PK_STRMAS] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Number - Key to the File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address 01 - Normally the Store Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'ADD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'ADD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address 03' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'ADD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address 04' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'ADD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Name on Till Receipt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'TILD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Phone Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'PHON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Fax Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'SFAX'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Managers Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'MANG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Region Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'REGC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Store has been Deleted (Closed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Country Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS', @level2type=N'COLUMN',@level2name=N'CountryCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S T R M A S  =   Other Stores Master File (used by IST etc)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STRMAS'
END
GO

