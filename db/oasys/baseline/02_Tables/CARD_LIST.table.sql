﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CARD_LIST]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CARD_LIST'
CREATE TABLE [dbo].[CARD_LIST](
	[CARD_NO] [char](19) NOT NULL,
	[CUST_NAME] [char](40) NOT NULL,
	[CUST_ADD1] [char](40) NULL,
	[CUST_ADD2] [char](40) NULL,
	[CUST_ADD3] [char](40) NULL,
	[CUST_ADD4] [char](40) NULL,
	[CUST_POST] [char](10) NULL,
	[HIDE_ADDR] [char](1) NOT NULL,
	[DELETED] [bit] NOT NULL,
 CONSTRAINT [PK_CARD_LIST] PRIMARY KEY CLUSTERED 
(
	[CARD_NO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Card Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'CARD_NO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'CUST_NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'CUST_ADD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'CUST_ADD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'CUST_ADD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'CUST_ADD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Post Code Line' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'CUST_POST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Do not show this address on screen, N or Blank = Allowed to show Address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'HIDE_ADDR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Has this record been marked for deletion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST', @level2type=N'COLUMN',@level2name=N'DELETED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C A R D L I S T =  Discount Card List File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CARD_LIST'
END
GO

