﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HouseKeepEffects]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HouseKeepEffects'
CREATE TABLE [dbo].[HouseKeepEffects](
	[GroupID] [int] NOT NULL,
	[BOTableName] [char](30) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Action] [char](10) NULL,
	[ActionValue] [char](30) NULL,
 CONSTRAINT [PK_HseKeepEffects] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC,
	[BOTableName] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepEffects', @level2type=N'COLUMN',@level2name=N'GroupID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Objects Table Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepEffects', @level2type=N'COLUMN',@level2name=N'BOTableName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepEffects', @level2type=N'COLUMN',@level2name=N'Sequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepEffects', @level2type=N'COLUMN',@level2name=N'Action'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Action Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepEffects', @level2type=N'COLUMN',@level2name=N'ActionValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H O U S E K E E P E F F E C T S = House Keep Effects File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepEffects'
END
GO

