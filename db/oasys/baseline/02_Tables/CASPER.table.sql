﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CASPER]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CASPER'
CREATE TABLE [dbo].[CASPER](
	[CASH] [char](3) NOT NULL,
	[DATE1] [date] NOT NULL,
	[NAME] [char](22) NULL,
	[TRNO] [decimal](7, 0) NULL,
	[CONO] [decimal](7, 0) NULL,
	[VONO] [decimal](7, 0) NULL,
	[LRNO] [decimal](7, 0) NULL,
	[ODNO] [decimal](7, 0) NULL,
	[PUVA] [decimal](9, 2) NULL,
	[OUVA] [decimal](9, 2) NULL,
	[LSNO] [decimal](7, 0) NULL,
	[SCNO] [decimal](7, 0) NULL,
	[SPARE] [char](93) NULL,
 CONSTRAINT [PK_CASPER] PRIMARY KEY CLUSTERED 
(
	[CASH] ASC,
	[DATE1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'CASH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of transactions.Includes types SA RF M+ M-' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'TRNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Correction transactions. Includes types
SC RC C+ C-' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'CONO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Voided transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'VONO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Line Reversals' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'LRNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Open Drawer transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'ODNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total value of Pickups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'PUVA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total value of Over/Unders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'OUVA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Number of lines sold by this cashier today' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'LSNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Number of lines sold which were scanned correctly.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'SCNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C A S P E R  =   Cashier Performance File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CASPER'
END
GO

