﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLLINE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLLINE'
CREATE TABLE [dbo].[DLLINE](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [smallint] NOT NULL,
	[SKUN] [char](6) NULL,
	[DEPT] [char](2) NULL,
	[IBAR] [bit] NOT NULL,
	[SUPV] [char](3) NULL,
	[QUAN] [decimal](7, 0) NULL,
	[SPRI] [decimal](9, 2) NULL,
	[PRIC] [decimal](9, 2) NULL,
	[PRVE] [decimal](9, 2) NULL,
	[EXTP] [decimal](9, 2) NULL,
	[EXTC] [decimal](9, 3) NULL,
	[RITM] [bit] NOT NULL,
	[PORC] [smallint] NOT NULL,
	[ITAG] [bit] NOT NULL,
	[CATA] [bit] NOT NULL,
	[VSYM] [char](1) NULL,
	[TPPD] [decimal](9, 2) NULL,
	[TPME] [char](6) NULL,
	[POPD] [decimal](9, 2) NULL,
	[POME] [char](6) NULL,
	[QBPD] [decimal](9, 2) NULL,
	[QBME] [char](6) NULL,
	[DGPD] [decimal](9, 2) NULL,
	[DGME] [char](6) NULL,
	[MBPD] [decimal](9, 2) NULL,
	[MBME] [char](6) NULL,
	[HSPD] [decimal](9, 2) NULL,
	[HSME] [char](6) NULL,
	[ESPD] [decimal](9, 2) NULL,
	[ESME] [char](6) NULL,
	[LREV] [bit] NOT NULL,
	[ESEQ] [char](6) NULL,
	[CTGY] [char](6) NULL,
	[GRUP] [char](6) NULL,
	[SGRP] [char](6) NULL,
	[STYL] [char](6) NULL,
	[QSUP] [char](3) NULL,
	[ESEV] [decimal](9, 2) NULL,
	[IMDN] [bit] NOT NULL,
	[SALT] [char](1) NULL,
	[VATN] [decimal](3, 0) NULL,
	[VATV] [decimal](9, 2) NULL,
	[BDCO] [char](1) NULL,
	[BDCOInd] [bit] NOT NULL,
	[RCOD] [char](2) NULL,
	[RTI] [char](1) NULL,
	[WEERATE] [char](2) NULL,
	[WEESEQN] [char](3) NULL,
	[WEECOST] [numeric](9, 2) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_DLLINE] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id - Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number from PC Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Sequence Number (ie, Line Number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number of this Item - 000000 = Department' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Department Number of this item - Dept Rec = Dept' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'DEPT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Input was Barcode - NOT SKU number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'IBAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supervisor Cashier Number
Price override or Line reversal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity Sold' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'QUAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System Lookup Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'SPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W08 Item Price - After TPPD and POPD applied' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item Price - VAT Exclusive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'PRVE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extended Value - QUAN times PRIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'EXTP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extended Cost - Calculated by QUAN times IM:COST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'EXTC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Related items single' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'RITM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Override Reason Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'PORC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Was a tagged Item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'ITAG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Was a Catch-All Item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'CATA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Vat Symbol used for this item (ie. Vat Rate)
  *W14 VAT rates (RETOPT are 1 to 9)
         *W14    related to letters a ---> j' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'VSYM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Temporary Price Change Price Difference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'TPPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02                        Margin Erosion Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'TPME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Price Override         Price Difference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'POPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02                        Margin Erosion Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'POME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Quantity Break         Erosion allocated to this Sku
W04 - No longer included in DL:PRIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'QBPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02                        Margin Erosion Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'QBME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Deal Group             Erosion allocated to this Sku
W05 - No longer included in DL:PRIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'DGPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02                        Margin Erosion Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'DGME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Multibuy               Erosion allocated to this Sku
W05 - No longer included in DL:PRIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'MBPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02                        Margin Erosion Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'MBME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Hiearchy               Erosion allocated to this Sku
W06 - Not included in DL:PRIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'HSPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06                        Margin Erosion Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'HSME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W10 Primary Emp. Discount  Price Difference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'ESPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W10                        Margin Erosion Code
W10 - NOTE: Primary Employee Discount (:ESPD), which is
W10   calculated using RO:VDIS, is mutually exclusive
W10   of all other discounts.
W10   However, Secondary Employee discount (:ESEV),
W10   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'ESME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 On = Line has been reversed OR is reversed line.
W03 (ie. one of pair)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'LREV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Last Event Sequence number used for this line.
W04 - See new file DLEVNT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'ESEQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Hierarchy Category - IM:CTGY - Level 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'CTGY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Hierarchy Group    - IM:GRUP -       4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'GRUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Hierarchy SubGroup - IM:SGRP -       3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'SGRP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Hierarchy Style    - IM:STYL -       2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'STYL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W07 Partial Quarantine (IM:QUAR=B) Supervisor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'QSUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W10 Secondary Employee Sale Erosion Value.
W10 - Note: See DL:ESME documentation above.
W10 - This Erosion value will be added to all other
W10   event erosion values for comparison with primary
W10   employee discount.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'ESEV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11    Y = Sold from Mark-down stock
W11    N = Sold from Normal On Hand stock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'IMDN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12 Sale Type Attribute
W12       B = Basic Item
W12       P = Performance Item
W12       A = Aesthetic Item
W12       S = Showroom Item
W12       G = Good Pallet Item
W12       R = Reduced Pallet Item
W12       D = Delivery Item
W12       I ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'SALT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W13 Vat code used for this line.Used for discs.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'VATN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W15    Actual Vat Value of this line' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'VATV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'* Y = Item to be collected at back door
* N = Sold from Normal On Hand stock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'BDCO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'* Y = Item to be collected at back door
* N = Sold from Normal On Hand stock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'BDCOInd'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line Reversal Reason Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'RCOD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'WEEE Rate - from STKMAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'WEERATE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'WEEE Sequence - from STKMAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'WEESEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'WEEE Value - from WEEECOS  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'WEECOST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W17    Filler for future use (Reduced from A18)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L L I N E  =   Daily Reformatted Till Line Item Records ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLLINE'
END
GO

