﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DAYPRICE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DAYPRICE'
CREATE TABLE [dbo].[DAYPRICE](
	[SKUN] [char](6) NOT NULL,
	[DATECREATED] [date] NOT NULL,
	[TIMECREATED] [char](4) NULL,
	[NEWPRICE] [numeric](9, 2) NULL,
	[AUTHID] [char](3) NULL,
	[COMMENT] [char](200) NULL,
 CONSTRAINT [PK_DAYPRICE] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC,
	[DATECREATED] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYPRICE', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYPRICE', @level2type=N'COLUMN',@level2name=N'DATECREATED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYPRICE', @level2type=N'COLUMN',@level2name=N'TIMECREATED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New Price (Value)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYPRICE', @level2type=N'COLUMN',@level2name=N'NEWPRICE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Authorised Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYPRICE', @level2type=N'COLUMN',@level2name=N'AUTHID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYPRICE', @level2type=N'COLUMN',@level2name=N'COMMENT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D A Y P R I C E = Temporary Price Overrides File  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DAYPRICE'
END
GO

