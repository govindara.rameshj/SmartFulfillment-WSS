﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[STKLOG]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table STKLOG'
CREATE TABLE [dbo].[STKLOG](
	[TKEY] [int] IDENTITY(1,1) NOT NULL,
	[SKUN] [char](6) NULL,
	[DAYN] [decimal](9, 0) NULL,
	[TYPE] [char](2) NULL,
	[DATE1] [date] NULL,
	[TIME] [char](6) NULL,
	[KEYS] [char](30) NULL,
	[EEID] [char](3) NULL,
	[ICOM] [bit] NOT NULL,
	[SSTK] [decimal](7, 0) NULL,
	[SRET] [decimal](7, 0) NULL,
	[SPRI] [decimal](9, 2) NULL,
	[ESTK] [decimal](7, 0) NULL,
	[ERET] [decimal](7, 0) NULL,
	[EPRI] [decimal](9, 2) NULL,
	[SMDN] [decimal](7, 0) NULL,
	[SWTF] [decimal](7, 0) NULL,
	[EMDN] [decimal](7, 0) NULL,
	[EWTF] [decimal](7, 0) NULL,
	[RTI] [char](1) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_STKLOG] PRIMARY KEY CLUSTERED 
(
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System Assigned Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Day Number - DATE field in N8 format
    (May be used in future for day numbers)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'DAYN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Movement that Created this Record
    (Please Note : The definitions listed below
 can be variable - see DATA\MOVDESCS for
 possible variations)
   01 = Normal Stock Sale Item
   02 = Normal Stock Refund Item
   03 = PSS In-store Sale I' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of the Movement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time of the Movement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'TIME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The fields that make up the key to the
   related detail movement file.
   *
   e.g. Movement of stock is a sale, the keys
        added to this field would be
   *
        DL:DATE, DL:TILL, DL:TRAN & DL:NUMB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'KEYS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee ID of Person that Made the Change' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Sent to H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'ICOM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting Stock On Hand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'SSTK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open Returns Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'SRET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'SPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending Stock On Hand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'ESTK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open Returns Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'ERET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'EPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Starting Mark-down Stock On Hand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'SMDN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04          Write Off Stock On Hand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'SWTF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Ending Mark-down Stock On Hand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'EMDN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04        Write Off Stock On Hand' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'EWTF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler For Future Use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S T K L O G   =   Stock Item On Hand Changes Logging Records  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKLOG'
END
GO

