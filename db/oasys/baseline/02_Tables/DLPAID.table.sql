﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLPAID]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLPAID'
CREATE TABLE [dbo].[DLPAID](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [smallint] NOT NULL,
	[TYPE] [decimal](3, 0) NULL,
	[AMNT] [decimal](9, 2) NULL,
	[CARD] [char](19) NULL,
	[EXDT] [char](4) NULL,
	[COPN] [char](6) NULL,
	[CLAS] [char](2) NULL,
	[AUTH] [char](9) NULL,
	[CKEY] [bit] NOT NULL,
	[SUPV] [char](3) NULL,
	[POST] [char](8) NULL,
	[CKAC] [char](10) NULL,
	[CKSC] [char](6) NULL,
	[CKNO] [char](6) NULL,
	[DBRF] [bit] NOT NULL,
	[SEQN] [char](4) NULL,
	[ISSU] [char](2) NULL,
	[ATYP] [char](1) NULL,
	[MERC] [char](15) NULL,
	[CBAM] [decimal](9, 2) NULL,
	[DIGC] [smallint] NULL,
	[ECOM] [bit] NOT NULL,
	[CTYP] [varchar](100) NULL,
	[EFID] [char](6) NULL,
	[EFTC] [char](1) NULL,
	[STDT] [char](4) NULL,
	[AUTHDESC] [char](30) NULL,
	[SECCODE] [char](3) NULL,
	[TENC] [char](3) NULL,
	[MPOW] [int] NULL,
	[MRAT] [decimal](7, 5) NULL,
	[TENV] [decimal](9, 2) NULL,
	[RTI] [char](1) NULL,
	[SPARE] [char](100) NULL,
 CONSTRAINT [PK_DLPAID] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id - Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number from PC Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type  - Retopt Occurrence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Amount - Opposite sign of Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'AMNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Number - Future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'CARD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Expire Date as MMYY - Future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'EXDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'COPN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Class of coupon if I can determine it - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'CLAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Auth Code if Credit Card and over floor limit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'AUTH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Credit Card was Keyed - not SWIPED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'CKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supervisor Number - (future use)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Post code - if coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'POST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cheque account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'CKAC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sort code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'CKSC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'CKNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = processed for DBR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'DBRF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EFTPOS Voucher Sequence number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'SEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Switch Card Issue number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'ISSU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Authorisation Type - L=local O=on-line R=Referral' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'ATYP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 EFTPOS merchant number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'MERC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Cash back amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'CBAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Digits count for Credit & Cheque cards etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'DIGC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 On = Eftpos Comms file Prepared.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'ECOM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Card Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'CTYP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Transaction ID given to EFTPOS for
              * authorisation, used to track request from initial
              * authorisation to final collection after all
              * receipts printed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'EFID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set to C once the payment has been acknowledged by
     * the PinPad as collected.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'EFTC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting date of the credit card' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'STDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Authorised Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'AUTHDESC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'SECCODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Status Code    *W15 NO LONGER USED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'TENC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Conversion Factor     *W15 NO LONGER USED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'MPOW'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Conversion Rate       *W15 NO LONGER USED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'MRAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value                 *W15 NO LONGER USED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'TENV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W09    Filler for future use (Reduced from A59)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L P A I D  =   Daily Reformatted Till Tender Type Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLPAID'
END
GO

