﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WEBLINE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table WEBLINE'
CREATE TABLE [dbo].[WEBLINE](
	[DATE1] [date] NULL,
	[TILL] [char](2) NULL,
	[TRAN] [char](4) NULL,
	[NUMB] [smallint] NULL,
	[SKUN] [char](6) NULL,
	[DEPT] [char](2) NULL,
	[IBAR] [bit] NULL,
	[SUPV] [char](3) NULL,
	[QUAN] [decimal](7, 0) NULL,
	[SPRI] [decimal](9, 2) NULL,
	[PRIC] [decimal](9, 2) NULL,
	[PRVE] [decimal](9, 2) NULL,
	[EXTP] [decimal](9, 2) NULL,
	[EXTC] [decimal](9, 3) NULL,
	[RITM] [bit] NULL,
	[PORC] [smallint] NULL,
	[ITAG] [bit] NULL,
	[CATA] [bit] NULL,
	[VSYM] [char](1) NULL,
	[TPPD] [decimal](9, 2) NULL,
	[TPME] [char](6) NULL,
	[POPD] [decimal](9, 2) NULL,
	[POME] [char](6) NULL,
	[QBPD] [decimal](9, 2) NULL,
	[QBME] [char](6) NULL,
	[DGPD] [decimal](9, 2) NULL,
	[DGME] [char](6) NULL,
	[MBPD] [decimal](9, 2) NULL,
	[MBME] [char](6) NULL,
	[HSPD] [decimal](9, 2) NULL,
	[HSME] [char](6) NULL,
	[ESPD] [decimal](9, 2) NULL,
	[ESME] [char](6) NULL,
	[LREV] [bit] NULL,
	[ESEQ] [char](6) NULL,
	[CTGY] [char](6) NULL,
	[GRUP] [char](6) NULL,
	[SGRP] [char](6) NULL,
	[STYL] [char](6) NULL,
	[QSUP] [char](3) NULL,
	[ESEV] [decimal](9, 2) NULL,
	[IMDN] [bit] NULL,
	[SALT] [char](1) NULL,
	[VATN] [decimal](1, 0) NULL,
	[VATV] [decimal](9, 2) NULL,
	[BDCO] [bit] NULL,
	[RTIHold] [bit] NULL,
	[RTIComp] [bit] NULL,
	[RTIProc0] [bit] NULL,
	[RTIProc1] [bit] NULL,
	[RTICount] [smallint] NULL,
	[SPARE] [char](15) NULL
) ON [PRIMARY]
END
GO

