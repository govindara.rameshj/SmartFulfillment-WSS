﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VisionLine]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table VisionLine'
CREATE TABLE [dbo].[VisionLine](
	[TranDate] [date] NOT NULL,
	[TillId] [int] NOT NULL,
	[TranNumber] [int] NOT NULL,
	[Number] [int] NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[Quantity] [int] NOT NULL,
	[PriceLookup] [decimal](9, 2) NOT NULL,
	[Price] [decimal](9, 2) NOT NULL,
	[PriceExVat] [decimal](9, 2) NOT NULL,
	[PriceExtended] [decimal](9, 2) NOT NULL,
	[IsRelatedItemSingle] [bit] NOT NULL,
	[VatSymbol] [char](1) NULL,
	[HieCategory] [char](6) NULL,
	[HieGroup] [char](6) NULL,
	[HieSubgroup] [char](6) NULL,
	[HieStyle] [char](6) NULL,
	[SaleType] [char](1) NULL,
	[IsInStoreStockItem] [bit] NOT NULL,
	[MovementTypeCode] [char](2) NULL,
	[IsPivotal] [bit] NOT NULL,
 CONSTRAINT [PK_VisionLine] PRIMARY KEY CLUSTERED 
(
	[TranDate] ASC,
	[TillId] ASC,
	[TranNumber] ASC,
	[Number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

