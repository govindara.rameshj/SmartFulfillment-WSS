﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SUPMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SUPMAS'
CREATE TABLE [dbo].[SUPMAS](
	[SUPN] [char](5) NOT NULL,
	[ALPH] [char](15) NULL,
	[NAME] [char](30) NULL,
	[DELC] [bit] NOT NULL,
	[HLIN] [char](16) NULL,
	[MERC] [char](30) NULL,
	[TYPE] [char](2) NULL,
	[ODNO] [int] NOT NULL,
	[BBCN] [char](3) NULL,
	[QCTL] [int] NULL,
	[QFLG] [bit] NOT NULL,
	[RDNO] [int] NULL,
	[DLPO] [date] NULL,
	[DLRE] [date] NULL,
	[OPON] [decimal](5, 0) NULL,
	[OPOV] [decimal](11, 2) NULL,
	[QTYO1] [decimal](11, 0) NULL,
	[QTYO2] [decimal](11, 0) NULL,
	[QTYO3] [decimal](11, 0) NULL,
	[QTYO4] [decimal](11, 0) NULL,
	[QTYO5] [decimal](11, 0) NULL,
	[QTYR1] [decimal](11, 0) NULL,
	[QTYR2] [decimal](11, 0) NULL,
	[QTYR3] [decimal](11, 0) NULL,
	[QTYR4] [decimal](11, 0) NULL,
	[QTYR5] [decimal](11, 0) NULL,
	[LREC1] [decimal](7, 0) NULL,
	[LREC2] [decimal](7, 0) NULL,
	[LREC3] [decimal](7, 0) NULL,
	[LREC4] [decimal](7, 0) NULL,
	[LREC5] [decimal](7, 0) NULL,
	[LNRE1] [decimal](7, 0) NULL,
	[LNRE2] [decimal](7, 0) NULL,
	[LNRE3] [decimal](7, 0) NULL,
	[LNRE4] [decimal](7, 0) NULL,
	[LNRE5] [decimal](7, 0) NULL,
	[LOVR1] [decimal](7, 0) NULL,
	[LOVR2] [decimal](7, 0) NULL,
	[LOVR3] [decimal](7, 0) NULL,
	[LOVR4] [decimal](7, 0) NULL,
	[LOVR5] [decimal](7, 0) NULL,
	[LUND1] [decimal](7, 0) NULL,
	[LUND2] [decimal](7, 0) NULL,
	[LUND3] [decimal](7, 0) NULL,
	[LUND4] [decimal](7, 0) NULL,
	[LUND5] [decimal](7, 0) NULL,
	[PREC1] [decimal](7, 0) NULL,
	[PREC2] [decimal](7, 0) NULL,
	[PREC3] [decimal](7, 0) NULL,
	[PREC4] [decimal](7, 0) NULL,
	[PREC5] [decimal](7, 0) NULL,
	[VREC1] [decimal](13, 2) NULL,
	[VREC2] [decimal](13, 2) NULL,
	[VREC3] [decimal](13, 2) NULL,
	[VREC4] [decimal](13, 2) NULL,
	[VREC5] [decimal](13, 2) NULL,
	[DYTR1] [decimal](5, 0) NULL,
	[DYTR2] [decimal](5, 0) NULL,
	[DYTR3] [decimal](5, 0) NULL,
	[DYTR4] [decimal](5, 0) NULL,
	[DYTR5] [decimal](5, 0) NULL,
	[DYSH1] [decimal](3, 0) NULL,
	[DYSH2] [decimal](3, 0) NULL,
	[DYSH3] [decimal](3, 0) NULL,
	[DYSH4] [decimal](3, 0) NULL,
	[DYSH5] [decimal](3, 0) NULL,
	[DYLO1] [decimal](3, 0) NULL,
	[DYLO2] [decimal](3, 0) NULL,
	[DYLO3] [decimal](3, 0) NULL,
	[DYLO4] [decimal](3, 0) NULL,
	[DYLO5] [decimal](3, 0) NULL,
	[VPCC] [char](2) NULL,
	[SODT] [date] NULL,
	[SOQDate] [date] NULL,
	[PalletCheck] [bit] NOT NULL,
 CONSTRAINT [PK_SUPMAS] PRIMARY KEY CLUSTERED 
(
	[SUPN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number - key to the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'SUPN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alpha Key' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Deleted by head office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Help Line Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'HLIN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Primary Merchant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'MERC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Supplier Type Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Order Depot Number ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'ODNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 BBC Site Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'BBCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ Control number - Assigned by the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QCTL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = OK to process SOQ
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QFLG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Returns Depot Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'RDNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of Last Purchase Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DLPO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of Last Receipt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DLRE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Current Outstanding Purchase Orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'OPON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of Current Outstanding Purchase Orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'OPOV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYO1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYO2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYO3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYO4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYO5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Quantities Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'QTYR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines on the PO Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LREC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines on the PO Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LREC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines on the PO Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LREC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines on the PO Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LREC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines on the PO Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LREC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of lines on the PO Not Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LNRE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of lines on the PO Not Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LNRE2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of lines on the PO Not Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LNRE3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of lines on the PO Not Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LNRE4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of lines on the PO Not Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LNRE5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - over qty ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LOVR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - over qty ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LOVR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - over qty ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LOVR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - over qty ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LOVR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - over qty ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LOVR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - Under qty Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LUND1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - Under qty Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LUND2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - Under qty Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LUND3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - Under qty Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LUND4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of Lines Received - Under qty Ordered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'LUND5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of PO''s Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'PREC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of PO''s Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'PREC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of PO''s Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'PREC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of PO''s Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'PREC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of PO''s Received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'PREC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the po''s Received (sum)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'VREC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the po''s Received (sum)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'VREC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the po''s Received (sum)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'VREC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the po''s Received (sum)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'VREC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the po''s Received (sum)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'VREC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days to received orders (sum of)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYTR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days to received orders (sum of)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYTR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days to received orders (sum of)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYTR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days to received orders (sum of)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYTR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Days to received orders (sum of)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYTR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shortest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYSH1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shortest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYSH2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shortest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYSH3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shortest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYSH4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shortest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYSH5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYLO1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYLO2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYLO3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYLO4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longest number of days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'DYLO5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vendor Primary Count Code  (day number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'VPCC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Not Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'SODT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of SOQ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'SOQDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Pallet Check Needed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS', @level2type=N'COLUMN',@level2name=N'PalletCheck'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S U P M A S  =   Supplier Master File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SUPMAS'
END
GO

