﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CONMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CONMAS'
CREATE TABLE [dbo].[CONMAS](
	[NUMB] [int] NOT NULL,
	[PONO] [int] NULL,
	[PREL] [smallint] NULL,
	[EDAT] [date] NULL,
	[SUPP] [char](5) NULL,
	[IBTI] [bit] NOT NULL,
	[IBTS] [char](3) NULL,
	[DONE] [bit] NOT NULL,
	[CNUM] [smallint] NULL,
	[DNOT1] [char](10) NULL,
	[DNOT2] [char](10) NULL,
	[DNOT3] [char](10) NULL,
	[DNOT4] [char](10) NULL,
	[DNOT5] [char](10) NULL,
	[DNOT6] [char](10) NULL,
	[DNOT7] [char](10) NULL,
	[DNOT8] [char](10) NULL,
	[DNOT9] [char](10) NULL,
	[BBCI] [char](6) NULL,
	[INIT1] [char](5) NULL,
	[INIT2] [char](5) NULL,
	[INIT3] [char](5) NULL,
	[INIT4] [char](5) NULL,
	[INIT5] [char](5) NULL,
	[INIT6] [char](5) NULL,
	[TIME1] [char](4) NULL,
	[TIME2] [char](4) NULL,
	[TIME3] [char](4) NULL,
	[TIME4] [char](4) NULL,
	[TIME5] [char](4) NULL,
	[TIME6] [char](4) NULL,
	[EEID] [int] NULL,
	[RCVD] [int] NULL,
	[RTND] [int] NULL,
 CONSTRAINT [PK_CONMAS] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Consignment number   (System assigned)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Purchase Order Number  (IBT = in store drl)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'PONO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Purchase Order Release number (IBT = 00)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'PREL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Consignment Entry Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'EDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number  (IBT = 00000)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'SUPP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT Indicator   (IBT = on)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'IBTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT Store number  (P/O = 000)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'IBTS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = P/O Received OR IBT In Entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DONE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of copies printed (1 thru 6) ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'CNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery note number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'DNOT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BBC issue note number (bbc p/o''s only)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'BBCI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of who printed copy (1 = 1st)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'INIT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of who printed copy (2 = Second printed copy)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'INIT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of who printed copy No 3 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'INIT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of who printed copy No 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'INIT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of who printed copy No 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'INIT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of who printed copy No 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'INIT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time (HHMM) of when printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'TIME1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time (HHMM) of when printed No 2 Copy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'TIME2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time (HHMM) of when printed No 3 Copy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'TIME3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time (HHMM) of when printed No 4 Copy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'TIME4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time (HHMM) of when printed No 5 Copy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'TIME5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time (HHMM) of when printed No 6 Copy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'TIME6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee ID of consignee			*W02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number Of Pallets Received			*W02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'RCVD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number Of Pallets Returned			*W02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS', @level2type=N'COLUMN',@level2name=N'RTND'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O N M A S  =  Consignment (Lorry tracking) File Definition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CONMAS'
END
GO

