﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CYHMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CYHMAS'
CREATE TABLE [dbo].[CYHMAS](
	[SKUN] [char](6) NOT NULL,
	[ADAT1] [date] NULL,
	[ADAT2] [date] NULL,
	[ADAT3] [date] NULL,
	[ADAT4] [date] NULL,
	[AQ021] [decimal](7, 0) NULL,
	[AQ022] [decimal](7, 0) NULL,
	[AQ023] [decimal](7, 0) NULL,
	[AQ024] [decimal](7, 0) NULL,
	[AV021] [decimal](9, 2) NULL,
	[AV022] [decimal](9, 2) NULL,
	[AV023] [decimal](9, 2) NULL,
	[AV024] [decimal](9, 2) NULL,
	[YTDS1] [decimal](9, 2) NULL,
	[YTDS2] [decimal](9, 2) NULL,
	[YTDS3] [decimal](9, 2) NULL,
	[YTDS4] [decimal](9, 2) NULL,
	[PYRS1] [decimal](9, 2) NULL,
	[PYRS2] [decimal](9, 2) NULL,
	[PYRS3] [decimal](9, 2) NULL,
	[PYRS4] [decimal](9, 2) NULL,
 CONSTRAINT [PK_CYHMAS] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number - Valid are 000001 to 999999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cycle date - end of cycle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'ADAT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cycle date - end of cycle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'ADAT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cycle date - end of cycle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'ADAT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cycle date - end of cycle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'ADAT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity code 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'AQ021'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity code 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'AQ022'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity code 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'AQ023'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity code 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'AQ024'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value     code 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'AV021'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value     code 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'AV022'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value     code 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'AV023'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value     code 02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'AV024'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'YTD Sales at ADAT above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'YTDS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'YTD Sales at ADAT above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'YTDS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'YTD Sales at ADAT above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'YTDS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'YTD Sales at ADAT above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'YTDS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior years sales at ADAT above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'PYRS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior years sales at ADAT above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'PYRS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior years sales at ADAT above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'PYRS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior years sales at ADAT above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS', @level2type=N'COLUMN',@level2name=N'PYRS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C Y H M A S  =  Cyclical Count History File Definition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CYHMAS'
END
GO

