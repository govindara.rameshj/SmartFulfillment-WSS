﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MODMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table MODMAS'
CREATE TABLE [dbo].[MODMAS](
	[NUMB] [char](6) NOT NULL,
	[DESCR] [char](40) NULL,
	[ALPH] [char](10) NULL,
	[GROU] [char](3) NULL,
	[PRIC] [decimal](9, 2) NULL,
	[PPRI] [decimal](9, 2) NULL,
	[DSOL] [date] NULL,
	[DPRC] [date] NULL,
	[DSET] [date] NULL,
	[DOBS] [date] NULL,
	[DDEL] [date] NULL,
	[ONOR] [decimal](7, 0) NULL,
	[SOLD] [decimal](7, 0) NULL,
	[ISTA] [char](1) NULL,
	[IOBS] [bit] NOT NULL,
	[IDEL] [bit] NOT NULL,
	[SALV1] [decimal](9, 2) NULL,
	[SALV2] [decimal](9, 2) NULL,
	[SALV3] [decimal](9, 2) NULL,
	[SALV4] [decimal](9, 2) NULL,
	[SALV5] [decimal](9, 2) NULL,
	[SALV6] [decimal](9, 2) NULL,
	[SALV7] [decimal](9, 2) NULL,
	[SALU1] [decimal](7, 0) NULL,
	[SALU2] [decimal](7, 0) NULL,
	[SALU3] [decimal](7, 0) NULL,
	[SALU4] [decimal](7, 0) NULL,
	[SALU5] [decimal](7, 0) NULL,
	[SALU6] [decimal](7, 0) NULL,
	[SALU7] [decimal](7, 0) NULL,
	[CHKD] [char](1) NULL,
	[SUPP] [char](5) NULL,
	[SUP1] [char](5) NULL,
	[MODT] [char](1) NULL,
 CONSTRAINT [PK_MODMAS] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Model Number - Second digit must be a 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01 Alpha Lookup Key - Used by Enquiry Program,
No longer Used*W01 Set from description of department product group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01 Group within Department - Valid are 001 to 999
No longer Used*W01 *' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'GROU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Normal Selling Price as xxxx.xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prior Selling Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'PPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'DSOL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Time Price Changed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'DPRC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the Item was Setup on this stores file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'DSET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item set OBSOLETE - See MMIOBS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'DOBS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item set DELETED  - See MMIDEL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'DDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On order from the Project Sales system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'ONOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sold Today - Updated by Project Sales system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SOLD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Defined Item Status Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'ISTA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Head Office Obsolete ?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'IOBS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Head Office Deleted  ? -- Ready for Physical Del.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'IDEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALU1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALU2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALU3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALU4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALU5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALU6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SALU7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H/O Item check digit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'CHKD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SUPP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alternate Supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'SUP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 Model Type Attribute
W02       S = Standard Model
W02       E = Express Model' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS', @level2type=N'COLUMN',@level2name=N'MODT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'M O D M A S  =  Model Master File Definition  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MODMAS'
END
GO

