﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StaticData]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table StaticData'
CREATE TABLE [dbo].[StaticData](
	[ID] [int] NULL,
	[StaticDataTypeID] [int] NULL,
	[Description] [varchar](250) NULL,
	[Value] [varchar](250) NULL,
	[Notes] [varchar](250) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
END
GO

