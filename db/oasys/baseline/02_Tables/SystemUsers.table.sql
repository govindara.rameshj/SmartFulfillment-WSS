﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemUsers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SystemUsers'
CREATE TABLE [dbo].[SystemUsers](
	[ID] [int] NOT NULL,
	[EmployeeCode] [char](3) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Initials] [varchar](5) NOT NULL,
	[Position] [varchar](20) NOT NULL,
	[PayrollID] [char](20) NOT NULL,
	[Password] [char](5) NOT NULL,
	[PasswordExpires] [date] NOT NULL,
	[IsManager] [bit] NOT NULL,
	[IsSupervisor] [bit] NOT NULL,
	[SupervisorPassword] [char](5) NULL,
	[SupervisorPwdExpires] [date] NULL,
	[Outlet] [char](2) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedDate] [date] NULL,
	[DeletedTime] [char](6) NULL,
	[DeletedBy] [char](3) NULL,
	[DeletedWhere] [char](2) NULL,
	[TillReceiptName] [char](35) NOT NULL,
	[DefaultAmount] [decimal](9, 2) NOT NULL,
	[LanguageCode] [char](3) NOT NULL,
	[SecurityProfileID] [int] NOT NULL,
 CONSTRAINT [PK_SystemUsers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee Code ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'EmployeeCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of Employee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'Name'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of Employee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'Initials'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Position within the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'Position'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payroll Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'PayrollID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Password to log into the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'Password'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Password expires' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'PasswordExpires'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If employee is a Manager' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'IsManager'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Employee is a supervisor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'IsSupervisor'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supervisor Password used for authority checks' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'SupervisorPassword'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Supervisor Password expires' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'SupervisorPwdExpires'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outlet currently being used by employee.
This is used in conjunction with (:MSON in SYSOPT)
 to determine if the employee can be signed onto more
 than one workstation (Outlet) at a time.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'Outlet'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TRUE  = Logically Deleted, can NOT be used but will
NOT be deleted until all history relating to
to this employee has been deleted.
FALSE = Valid for Use.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'IsDeleted'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Employee was Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'DeletedDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Employee deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'DeletedTime'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee ID of Person who Deleted this Employee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'DeletedBy'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outlet (Device) Used to Delete this Employee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'DeletedWhere'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name to be printed on the Till receipt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'TillReceiptName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default Float Amount for this Employee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'DefaultAmount'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Operator Preferred Language Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'LanguageCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Profile Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers', @level2type=N'COLUMN',@level2name=N'SecurityProfileID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SYSTEMUSERS = System Users File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemUsers'
END
GO

