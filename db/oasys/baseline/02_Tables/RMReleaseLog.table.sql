﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RMReleaseLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RMReleaseLog'
CREATE TABLE [dbo].[RMReleaseLog](
	[CompanyID] [int] NOT NULL,
	[ReleaseID] [int] NULL,
	[DateCreated] [date] NULL,
	[DestStore] [varchar](10) NULL,
	[PilotRelease] [bit] NULL,
	[DestArea] [varchar](5) NULL,
	[UserID] [int] NULL,
	[SubReleaseID] [int] NULL,
	[DateCompleted] [datetime] NULL
) ON [PRIMARY]
END
GO

