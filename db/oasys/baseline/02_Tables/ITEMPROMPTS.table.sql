﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ITEMPROMPTS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ITEMPROMPTS'
CREATE TABLE [dbo].[ITEMPROMPTS](
	[SKUN] [char](6) NOT NULL,
	[GROUPPROMPTID] [char](6) NOT NULL,
	[SEQUENCE] [char](4) NOT NULL,
	[SENT] [bit] NOT NULL,
	[DATEDELETED] [date] NULL,
 CONSTRAINT [PK_ITEMPROMPTS] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC,
	[GROUPPROMPTID] ASC,
	[SEQUENCE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEMPROMPTS', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Prompt Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEMPROMPTS', @level2type=N'COLUMN',@level2name=N'GROUPPROMPTID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEMPROMPTS', @level2type=N'COLUMN',@level2name=N'SEQUENCE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if Sent False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEMPROMPTS', @level2type=N'COLUMN',@level2name=N'SENT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEMPROMPTS', @level2type=N'COLUMN',@level2name=N'DATEDELETED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'I T E M P R O M P T S = Item Prompts File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ITEMPROMPTS'
END
GO

