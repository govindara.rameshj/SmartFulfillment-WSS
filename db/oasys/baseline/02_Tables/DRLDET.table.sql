﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DRLDET]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DRLDET'
CREATE TABLE [dbo].[DRLDET](
	[NUMB] [char](6) NOT NULL,
	[SEQN] [char](4) NOT NULL,
	[SKUN] [char](6) NOT NULL,
	[ORDQ] [int] NOT NULL,
	[RECQ] [int] NOT NULL,
	[ORDP] [decimal](9, 2) NOT NULL,
	[BDES] [char](20) NULL,
	[BSPC] [char](10) NULL,
	[PRIC] [decimal](9, 2) NOT NULL,
	[IBTQ] [int] NOT NULL,
	[RETQ] [int] NOT NULL,
	[POLN] [int] NOT NULL,
	[FOLQ] [int] NOT NULL,
	[RTI] [char](1) NULL,
	[ReasonCode] [char](2) NULL,
 CONSTRAINT [PK_DRLDET] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[SEQN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Number - assigned by the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Program Assigned Sequence    (0001 to 9999)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'SEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number of this Line item record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Quantity       (receipts only)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'ORDQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Received Quantity       (receipts only)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'RECQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Price        (receipts only)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'ORDP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blanket sku keyed desc.     (receipts only)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'BDES'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blanket sku Product code    (receipts only)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'BSPC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current Price at DRL creation Time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT in or out Quantity    (always positive)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'IBTQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Return Quantity     (always positive)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'RETQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P/O Line Number (Direct Link - TKEY)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'POLN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Back Order Quantity on this Receipt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'FOLQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reason Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET', @level2type=N'COLUMN',@level2name=N'ReasonCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L T O T S  =   Daily Reformatted Till Totals Records ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DRLDET'
END
GO

