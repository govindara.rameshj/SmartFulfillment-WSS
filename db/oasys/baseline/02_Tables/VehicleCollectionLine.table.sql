﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VehicleCollectionLine]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table VehicleCollectionLine'
CREATE TABLE [dbo].[VehicleCollectionLine](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CollectionID] [int] NOT NULL,
	[Description] [varchar](100) NULL,
	[Weight] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[TotalWeight] [int] NOT NULL,
	[DeliveryDate] [date] NOT NULL,
 CONSTRAINT [PK_VehicleCollectionLine] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[CollectionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

