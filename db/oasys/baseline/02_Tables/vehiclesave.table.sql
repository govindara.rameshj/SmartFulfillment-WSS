﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vehiclesave]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table vehiclesave'
CREATE TABLE [dbo].[vehiclesave](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DepotID] [varchar](50) NULL,
	[Registration] [varchar](50) NULL,
	[VehicleTypeID] [int] NULL,
	[StartTime] [time](7) NULL,
	[StopTime] [time](7) NULL,
	[MaximumWeight] [decimal](19, 2) NULL,
	[MaximumVolume] [decimal](19, 2) NULL,
	[LastUpdateTime] [datetime] NULL
) ON [PRIMARY]
END
GO

