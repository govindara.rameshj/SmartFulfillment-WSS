﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportSummary]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ReportSummary'
CREATE TABLE [dbo].[ReportSummary](
	[ReportId] [int] NOT NULL,
	[TableId] [int] NOT NULL,
	[ColumnId] [int] NOT NULL,
	[SummaryType] [int] NOT NULL,
	[ApplyToGroups] [bit] NOT NULL,
	[Format] [varchar](50) NULL,
	[DenominatorId] [int] NULL,
	[NumeratorId] [int] NULL,
 CONSTRAINT [PK_ReportSummaries_1] PRIMARY KEY CLUSTERED 
(
	[ReportId] ASC,
	[TableId] ASC,
	[ColumnId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

