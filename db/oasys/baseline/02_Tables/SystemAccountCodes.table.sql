﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemAccountCodes]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SystemAccountCodes'
CREATE TABLE [dbo].[SystemAccountCodes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MiscInAccount01] [char](12) NULL,
	[MiscInAccount02] [char](12) NULL,
	[MiscInAccount03] [char](12) NULL,
	[MiscInAccount04] [char](12) NULL,
	[MiscInAccount05] [char](12) NULL,
	[MiscInAccount06] [char](12) NULL,
	[MiscInAccount07] [char](12) NULL,
	[MiscInAccount08] [char](12) NULL,
	[MiscInAccount09] [char](12) NULL,
	[MiscInAccount10] [char](12) NULL,
	[MiscInAccount11] [char](12) NULL,
	[MiscInAccount12] [char](12) NULL,
	[MiscInAccount13] [char](12) NULL,
	[MiscInAccount14] [char](12) NULL,
	[MiscInAccount15] [char](12) NULL,
	[MiscInAccount16] [char](12) NULL,
	[MiscInAccount17] [char](12) NULL,
	[MiscInAccount18] [char](12) NULL,
	[MiscInAccount19] [char](12) NULL,
	[MiscInAccount20] [char](12) NULL,
	[MiscInAccountCode01] [char](4) NULL,
	[MiscInAccountCode02] [char](4) NULL,
	[MiscInAccountCode03] [char](4) NULL,
	[MiscInAccountCode04] [char](4) NULL,
	[MiscInAccountCode05] [char](4) NULL,
	[MiscInAccountCode06] [char](4) NULL,
	[MiscInAccountCode07] [char](4) NULL,
	[MiscInAccountCode08] [char](4) NULL,
	[MiscInAccountCode09] [char](4) NULL,
	[MiscInAccountCode10] [char](4) NULL,
	[MiscInAccountCode11] [char](4) NULL,
	[MiscInAccountCode12] [char](4) NULL,
	[MiscInAccountCode13] [char](4) NULL,
	[MiscInAccountCode14] [char](4) NULL,
	[MiscInAccountCode15] [char](4) NULL,
	[MiscInAccountCode16] [char](4) NULL,
	[MiscInAccountCode17] [char](4) NULL,
	[MiscInAccountCode18] [char](4) NULL,
	[MiscInAccountCode19] [char](4) NULL,
	[MiscInAccountCode20] [char](4) NULL,
	[MiscOutAccount01] [char](12) NULL,
	[MiscOutAccount02] [char](12) NULL,
	[MiscOutAccount03] [char](12) NULL,
	[MiscOutAccount04] [char](12) NULL,
	[MiscOutAccount05] [char](12) NULL,
	[MiscOutAccount06] [char](12) NULL,
	[MiscOutAccount07] [char](12) NULL,
	[MiscOutAccount08] [char](12) NULL,
	[MiscOutAccount09] [char](12) NULL,
	[MiscOutAccount10] [char](12) NULL,
	[MiscOutAccount11] [char](12) NULL,
	[MiscOutAccount12] [char](12) NULL,
	[MiscOutAccount13] [char](12) NULL,
	[MiscOutAccount14] [char](12) NULL,
	[MiscOutAccount15] [char](12) NULL,
	[MiscOutAccount16] [char](12) NULL,
	[MiscOutAccount17] [char](12) NULL,
	[MiscOutAccount18] [char](12) NULL,
	[MiscOutAccount19] [char](12) NULL,
	[MiscOutAccount20] [char](12) NULL,
	[MiscOutAccountCode01] [char](4) NULL,
	[MiscOutAccountCode02] [char](4) NULL,
	[MiscOutAccountCode03] [char](4) NULL,
	[MiscOutAccountCode04] [char](4) NULL,
	[MiscOutAccountCode05] [char](4) NULL,
	[MiscOutAccountCode06] [char](4) NULL,
	[MiscOutAccountCode07] [char](4) NULL,
	[MiscOutAccountCode08] [char](4) NULL,
	[MiscOutAccountCode09] [char](4) NULL,
	[MiscOutAccountCode10] [char](4) NULL,
	[MiscOutAccountCode11] [char](4) NULL,
	[MiscOutAccountCode12] [char](4) NULL,
	[MiscOutAccountCode13] [char](4) NULL,
	[MiscOutAccountCode14] [char](4) NULL,
	[MiscOutAccountCode15] [char](4) NULL,
	[MiscOutAccountCode16] [char](4) NULL,
	[MiscOutAccountCode17] [char](4) NULL,
	[MiscOutAccountCode18] [char](4) NULL,
	[MiscOutAccountCode19] [char](4) NULL,
	[MiscOutAccountCode20] [char](4) NULL,
 CONSTRAINT [PK_SystemAccountCodes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header 1 (e.g. Burger Van)  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount01'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount02'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount03'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount04'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount06'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount07'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount08'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount09'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Header ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccount20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code (e.g. 5604) ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode01'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode02'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode03'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode04'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode06'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode07'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode08'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode09'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscInAccountCode20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header (e.g Fork Lift Fuel)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount01'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount02'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount03'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount04'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount06'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount07'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount08'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount09'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccount20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code (e.g. 6504)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode01'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode02'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode03'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode04'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode06'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode07'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode08'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode09'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous out going Account Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes', @level2type=N'COLUMN',@level2name=N'MiscOutAccountCode20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S T E M A C C O U N T C O D E S = System Account Codes File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemAccountCodes'
END
GO

