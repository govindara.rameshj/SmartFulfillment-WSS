﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportRelation]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ReportRelation'
CREATE TABLE [dbo].[ReportRelation](
	[ReportId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[ParentTable] [varchar](50) NOT NULL,
	[ParentColumns] [varchar](200) NOT NULL,
	[ChildTable] [varchar](50) NOT NULL,
	[ChildColumns] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ReportRelation] PRIMARY KEY CLUSTERED 
(
	[ReportId] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

