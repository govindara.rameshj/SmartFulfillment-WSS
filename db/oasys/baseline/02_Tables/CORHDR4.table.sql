﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CORHDR4]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CORHDR4'
CREATE TABLE [dbo].[CORHDR4](
	[NUMB] [char](6) NOT NULL,
	[DATE1] [date] NOT NULL,
	[DELD] [date] NULL,
	[NAME] [varchar](50) NULL,
	[CUST] [char](10) NULL,
	[DDAT] [date] NULL,
	[OEID] [char](3) NULL,
	[FDID] [char](3) NULL,
	[DeliveryStatus] [int] NOT NULL,
	[RefundStatus] [int] NOT NULL,
	[SellingStoreId] [int] NOT NULL,
	[SellingStoreOrderId] [int] NOT NULL,
	[OMOrderNumber] [int] NULL,
	[CustomerAddress1] [varchar](30) NULL,
	[CustomerAddress2] [varchar](30) NULL,
	[CustomerAddress3] [varchar](30) NULL,
	[CustomerAddress4] [varchar](30) NULL,
	[CustomerPostcode] [varchar](8) NULL,
	[CustomerEmail] [varchar](100) NULL,
	[PhoneNumberWork] [varchar](15) NULL,
	[IsSuspended] [bit] NOT NULL,
 CONSTRAINT [PK_CORHDR4] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Customer Order Number (same as Quote number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date created (converted from quote to order)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Required delivery / collection date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4', @level2type=N'COLUMN',@level2name=N'DELD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Number - See CUSMAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4', @level2type=N'COLUMN',@level2name=N'CUST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date order despatched/collected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4', @level2type=N'COLUMN',@level2name=N'DDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id of Order Taker' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4', @level2type=N'COLUMN',@level2name=N'OEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID of manager who authorise free delivery' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4', @level2type=N'COLUMN',@level2name=N'FDID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O R H D R 4 = Store customer order header index and other details' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR4'
END
GO

