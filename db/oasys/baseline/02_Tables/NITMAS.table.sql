﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NITMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table NITMAS'
CREATE TABLE [dbo].[NITMAS](
	[NSET] [char](1) NOT NULL,
	[TASK] [char](3) NOT NULL,
	[DESCR] [char](40) NULL,
	[PROG] [char](150) NULL,
	[NITE] [bit] NOT NULL,
	[RETY] [bit] NOT NULL,
	[WEEK] [bit] NOT NULL,
	[PEND] [bit] NOT NULL,
	[LIVE] [bit] NOT NULL,
	[OPTN] [smallint] NULL,
	[ABOR] [bit] NOT NULL,
	[JRUN] [char](70) NULL,
	[DAYS1] [bit] NOT NULL,
	[DAYS2] [bit] NOT NULL,
	[DAYS3] [bit] NOT NULL,
	[DAYS4] [bit] NOT NULL,
	[DAYS5] [bit] NOT NULL,
	[DAYS6] [bit] NOT NULL,
	[DAYS7] [bit] NOT NULL,
	[BEGD] [date] NULL,
	[ENDD] [date] NULL,
	[SDAT] [date] NULL,
	[STIM] [char](6) NULL,
	[EDAT] [date] NULL,
	[ETIM] [char](6) NULL,
	[ADAT] [date] NULL,
	[ATIM] [char](6) NULL,
	[TTYPE] [int] NOT NULL,
 CONSTRAINT [PK_NITMAS] PRIMARY KEY CLUSTERED 
(
	[NSET] ASC,
	[TASK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set Number - See SYSDAT - 1 to 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'NSET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Task Number 001 to 999 valid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'TASK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Program to RUN - Full Statement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'PROG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run in Night' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'NITE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run in Retry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'RETY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run in Week ONLY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'WEEK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run in Period End ONLY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'PEND'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run BEFORE the store is Live' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'LIVE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Option number to MEMO-WRITE (L-programs)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'OPTN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Abort on JOB-ERROR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'ABOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RUN statement if JOB-ERROR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'JRUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run on DAYWK 1 to 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'DAYS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run on DAYWK 1 to 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'DAYS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run on DAYWK 1 to 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'DAYS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run on DAYWK 1 to 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'DAYS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run on DAYWK 1 to 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'DAYS5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run on DAYWK 1 to 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'DAYS6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Run on DAYWK 1 to 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'DAYS7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Beginning DATE valid to run' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'BEGD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending DATE valid to run' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'ENDD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last date Run Started' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'SDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Started time as hhmmss' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'STIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last date Run ended ok' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'EDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending time as hhmmss' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'ETIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last abort date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'ADAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last abort time as hhmmss' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'ATIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Application;
  * 0 = Executable
  * 1 = Application Dll
  * 2 = Housekeeping
  * 3 = Stream System
  * 4 = Batch File

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS', @level2type=N'COLUMN',@level2name=N'TTYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'N I T M A S  =   Nightly Procedure Record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NITMAS'
END
GO

