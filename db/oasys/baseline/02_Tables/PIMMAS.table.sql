﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PIMMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PIMMAS'
CREATE TABLE [dbo].[PIMMAS](
	[SKUN] [char](6) NOT NULL,
	[CategoryCode] [char](6) NOT NULL,
	[CategoryDescr] [char](50) NOT NULL,
	[GroupCode] [char](6) NOT NULL,
	[GroupDescr] [char](50) NOT NULL,
	[SubGroupCode] [char](6) NOT NULL,
	[SubGroupDescr] [char](50) NOT NULL,
	[StyleCode] [char](6) NOT NULL,
	[StyleDescr] [char](50) NOT NULL,
 CONSTRAINT [PK_PIMMAS] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'CategoryCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'CategoryDescr'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'GroupCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'GroupDescr'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sub-Group Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'SubGroupCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sub-Group Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'SubGroupDescr'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Style Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'StyleCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Style Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS', @level2type=N'COLUMN',@level2name=N'StyleDescr'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P I M M A S = Product Identification Manual Master File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PIMMAS'
END
GO

