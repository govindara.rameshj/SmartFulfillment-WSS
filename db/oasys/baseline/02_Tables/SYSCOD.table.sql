﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SYSCOD]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SYSCOD'
CREATE TABLE [dbo].[SYSCOD](
	[TYPE] [char](2) NOT NULL,
	[CODE] [char](2) NOT NULL,
	[DESCR] [char](35) NULL,
	[ATTR] [char](1) NULL,
	[LABL] [bit] NOT NULL,
	[IRPO] [bit] NOT NULL,
	[SUPV] [bit] NOT NULL,
	[MANA] [bit] NOT NULL,
	[IPER] [char](1) NULL,
	[ACTIVE] [char](1) NULL,
	[PMATCH] [bit] NOT NULL,
	[VALUE1] [decimal](9, 2) NULL,
	[VALUE2] [decimal](9, 2) NULL,
	[MFLEX] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SYSCOD] PRIMARY KEY CLUSTERED 
(
	[TYPE] ASC,
	[CODE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reason Code Type
   "IA" = Inventory Sales Type Attributes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'CODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02    Attribute (used for STKMAS IM:SALT update)
*W03    Used for "LR" Label request type (via HHT)
           = "P" for STKMAS update via Price Change.
           = "S" all other Label requests' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'ATTR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04    "Y" = Print Product Return Label.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'LABL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05    Refund Price Override' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'IRPO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Supervisor Authorisation Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Manager Authorisation Required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'MANA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to Decide if a discount is a Value or a fixed amount;
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'IPER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Active Flag True = On false = Off' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'ACTIVE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If a Price Match' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'PMATCH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'VALUE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'VALUE2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Manager Flexability Discount rate overide allowed if on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD', @level2type=N'COLUMN',@level2name=N'MFLEX'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S C O D  =  System Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCOD'
END
GO

