﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SYSNUM]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SYSNUM'
CREATE TABLE [dbo].[SYSNUM](
	[FKEY] [char](2) NOT NULL,
	[NEXT1] [char](6) NULL,
	[NEXT2] [char](6) NULL,
	[NEXT3] [char](6) NULL,
	[NEXT4] [char](6) NULL,
	[NEXT5] [char](6) NULL,
	[NEXT6] [char](6) NULL,
	[NEXT7] [char](6) NULL,
	[NEXT8] [char](6) NULL,
	[NEXT9] [char](6) NULL,
	[NEXT10] [char](6) NULL,
	[NEXT11] [char](6) NULL,
	[NEXT12] [char](6) NULL,
	[NEXT13] [char](6) NULL,
	[NEXT14] [char](6) NULL,
	[NEXT15] [char](6) NULL,
	[NEXT16] [char](6) NULL,
	[NEXT17] [char](6) NULL,
	[NEXT18] [char](6) NULL,
	[NEXT19] [char](6) NULL,
	[NEXT20] [char](6) NULL,
	[SIZE1] [char](1) NULL,
	[SIZE2] [char](1) NULL,
	[SIZE3] [char](1) NULL,
	[SIZE4] [char](1) NULL,
	[SIZE5] [char](1) NULL,
	[SIZE6] [char](1) NULL,
	[SIZE7] [char](1) NULL,
	[SIZE8] [char](1) NULL,
	[SIZE9] [char](1) NULL,
	[SIZE10] [char](1) NULL,
	[SIZE11] [char](1) NULL,
	[SIZE12] [char](1) NULL,
	[SIZE13] [char](1) NULL,
	[SIZE14] [char](1) NULL,
	[SIZE15] [char](1) NULL,
	[SIZE16] [char](1) NULL,
	[SIZE17] [char](1) NULL,
	[SIZE18] [char](1) NULL,
	[SIZE19] [char](1) NULL,
	[SIZE20] [char](1) NULL,
	[PBSN] [decimal](7, 2) NULL,
	[SPARE] [char](99) NULL,
 CONSTRAINT [PK_SYSNUM] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Number - key to file - Always set to 01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next System Number as Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'NEXT20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Size to roll related Number on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SIZE20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Book Sequence Number    - NOT USED' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'PBSN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S N U M  =   System Numbers Record ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSNUM'
END
GO

