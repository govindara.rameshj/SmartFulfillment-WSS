﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RMUsers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RMUsers'
CREATE TABLE [dbo].[RMUsers](
	[CompanyID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[UserName] [varchar](10) NOT NULL,
	[AuthLevel] [int] NOT NULL,
	[Pwd] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO

