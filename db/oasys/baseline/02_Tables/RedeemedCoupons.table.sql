﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RedeemedCoupons]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RedeemedCoupons'
CREATE TABLE [dbo].[RedeemedCoupons](
	[StoreID] [char](4) NOT NULL,
	[TranDate] [date] NOT NULL,
	[TranTillID] [char](2) NOT NULL,
	[TranNo] [char](4) NOT NULL,
	[CouponID] [char](7) NOT NULL,
	[Sequence] [char](4) NULL,
	[Status] [char](1) NULL,
	[ExcCoupon] [bit] NULL,
	[MarketRef] [char](4) NULL,
	[ReUse] [bit] NULL,
	[IssueStoreNo] [char](4) NULL,
	[SerialNo] [char](6) NULL,
	[RTIFlag] [char](1) NULL
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'StoreID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'TranDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Till Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'TranTillID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'TranNo'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'CouponID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'Sequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status of the coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'Status'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If the coupon excluded False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'ExcCoupon'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Marketing Reference Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'MarketRef'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if the coupon can be re-used false if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'ReUse'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Store Number of the store that issued the coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'IssueStoreNo'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon Serial Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'SerialNo'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons', @level2type=N'COLUMN',@level2name=N'RTIFlag'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R E D E E M E D C O U P O N S = Redeemed Coupons File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RedeemedCoupons'
END
GO

