﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CORHDR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CORHDR'
CREATE TABLE [dbo].[CORHDR](
	[NUMB] [char](6) NOT NULL,
	[CUST] [char](10) NULL,
	[DATE1] [date] NOT NULL,
	[DELD] [date] NULL,
	[CANC] [char](1) NULL,
	[DELI] [bit] NOT NULL,
	[DELC] [bit] NOT NULL,
	[AMDT] [char](3) NULL,
	[ADDR1] [varchar](60) NOT NULL,
	[ADDR2] [varchar](50) NULL,
	[ADDR3] [varchar](50) NOT NULL,
	[ADDR4] [varchar](50) NULL,
	[PHON] [varchar](20) NULL,
	[PRNT] [bit] NOT NULL,
	[RPRN] [decimal](3, 0) NOT NULL,
	[REVI] [decimal](3, 0) NOT NULL,
	[MVST] [decimal](9, 2) NOT NULL,
	[DCST] [decimal](9, 2) NOT NULL,
	[QTYO] [decimal](7, 0) NOT NULL,
	[QTYT] [decimal](7, 0) NOT NULL,
	[QTYR] [decimal](7, 0) NOT NULL,
	[WGHT] [decimal](7, 2) NOT NULL,
	[VOLU] [decimal](7, 2) NOT NULL,
	[SDAT] [date] NULL,
	[STIL] [char](2) NULL,
	[STRN] [char](4) NULL,
	[RDAT] [date] NULL,
	[RTIL] [char](2) NULL,
	[RTRN] [char](4) NULL,
	[MOBP] [varchar](20) NULL,
	[POST] [varchar](10) NULL,
	[NAME] [varchar](50) NULL,
	[HOMP] [varchar](20) NULL,
	[RequiredDeliverySlotID] [varchar](5) NULL,
	[RequiredDeliverySlotDescription] [varchar](50) NULL,
	[RequiredDeliverySlotStartTime] [time](0) NULL,
	[RequiredDeliverySlotEndTime] [time](0) NULL,
 CONSTRAINT [PK_CORHDR] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Customer Order Number (same as Quote number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Number - See CUSMAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'CUST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date created (converted from quote to order)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Required delivery / collection date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'DELD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Order is live
1 = Order is completely cancelled
2 = Order has been changed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'CANC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = is a delivery, Off = Collection' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'DELI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery despatched / confirmed (phase 1 use only)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of times order amended' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'AMDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'ADDR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'ADDR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'ADDR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'ADDR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contact telephone number - supercedes CUSMAS record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'PHON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Printed flag -pick / delivery document printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'PRNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No. times pick / delivery document reprinted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'RPRN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Revision of this order (incremented on each change)
*
V A L U E S
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'REVI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Merchandise Value - Store' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'MVST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Charge - Store - total of delivery SKU
included in MVST value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'DCST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total order units' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'QTYO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total units taken' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'QTYT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total units refunded (qtyo - qtyt - qtyr = qty to deliver)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'QTYR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total transaction weight - future' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'WGHT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total transaction M3 volume - future
*
 S A L E   I N F O           *
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'VOLU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sale date - DLTOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'SDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till number - DLTOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'STIL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number - DLTOTS
*
 R E F U N D   I N F O          *
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'STRN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refund date - DLTOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'RDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till number - DLTOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'RTIL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number - DLTOTS
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'RTRN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mobile Phone Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'MOBP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Post Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'POST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O R H D R  =  Store customer order header' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORHDR'
END
GO

