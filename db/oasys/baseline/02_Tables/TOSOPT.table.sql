﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TOSOPT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TOSOPT'
CREATE TABLE [dbo].[TOSOPT](
	[DSEQ] [char](2) NOT NULL,
	[TOSC] [char](2) NULL,
	[TOSD] [char](35) NOT NULL,
	[DOCN] [bit] NOT NULL,
	[SPRT] [bit] NOT NULL,
	[OPEN] [bit] NOT NULL,
	[SPEC] [bit] NOT NULL,
	[SUPV] [bit] NOT NULL,
 CONSTRAINT [PK_TOSOPT] PRIMARY KEY CLUSTERED 
(
	[DSEQ] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT', @level2type=N'COLUMN',@level2name=N'DSEQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of sale Code (e.g. SA = Sale)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT', @level2type=N'COLUMN',@level2name=N'TOSC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Sale Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT', @level2type=N'COLUMN',@level2name=N'TOSD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Ask for External Document Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT', @level2type=N'COLUMN',@level2name=N'DOCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Special print - external Document' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT', @level2type=N'COLUMN',@level2name=N'SPRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If zero tran, On = OPEN drawer OFF=no' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT', @level2type=N'COLUMN',@level2name=N'OPEN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Special Account Tender Allowed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT', @level2type=N'COLUMN',@level2name=N'SPEC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Supervisor required at start' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'T O S O P T    =  PC Till TOS Option Records ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSOPT'
END
GO

