﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HouseKeepCriteria]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HouseKeepCriteria'
CREATE TABLE [dbo].[HouseKeepCriteria](
	[GroupID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Connected] [char](15) NULL,
	[BOTableName] [char](30) NULL,
	[Field] [char](20) NULL,
	[TestType] [char](2) NULL,
	[TestValueOrTable] [char](30) NULL,
	[TestField] [char](20) NULL,
	[SelectGroup] [int] NULL,
	[WhereJoin] [char](10) NULL,
	[AccessLevel] [char](10) NULL,
	[PeriodType] [char](10) NULL,
	[PeriodValue] [int] NULL,
 CONSTRAINT [PK_HseKeepCriteria] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'GroupID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'Sequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Connected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'Connected'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Object Table Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'BOTableName'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Database Field ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'Field'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Test Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'TestType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Test Value or Table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'TestValueOrTable'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Test Field' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'TestField'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Select Group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'SelectGroup'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Where Join (SQL)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'WhereJoin'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Access Level' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'AccessLevel'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'PeriodType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria', @level2type=N'COLUMN',@level2name=N'PeriodValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H O U S E K E E P C R I T E R I A = House Keep Criteria File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HouseKeepCriteria'
END
GO

