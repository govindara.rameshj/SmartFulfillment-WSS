﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RETOPT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RETOPT'
CREATE TABLE [dbo].[RETOPT](
	[FKEY] [char](2) NOT NULL,
	[STOR] [char](3) NULL,
	[SNAM] [char](30) NULL,
	[SAD1] [char](30) NULL,
	[SAD2] [char](30) NULL,
	[SAD3] [char](30) NULL,
	[TYPE] [char](1) NULL,
	[HCAS] [char](3) NULL,
	[FLOT] [decimal](9, 2) NULL,
	[LAST] [decimal](3, 0) NULL,
	[NEXT] [decimal](3, 0) NULL,
	[KEEP] [decimal](3, 0) NULL,
	[TOSN1] [char](3) NULL,
	[TOSN2] [char](3) NULL,
	[TOSN3] [char](3) NULL,
	[TOSN4] [char](3) NULL,
	[TOSN5] [char](3) NULL,
	[TOSN6] [char](3) NULL,
	[TOSN7] [char](3) NULL,
	[TOSN8] [char](3) NULL,
	[TOSN9] [char](3) NULL,
	[TOSN10] [char](3) NULL,
	[TOSN11] [char](3) NULL,
	[TOSN12] [char](3) NULL,
	[TOSN13] [char](3) NULL,
	[TOSN14] [char](3) NULL,
	[TOSN15] [char](3) NULL,
	[TOSN16] [char](3) NULL,
	[TOSN17] [char](3) NULL,
	[TOSN18] [char](3) NULL,
	[TOSN19] [char](3) NULL,
	[TOSN20] [char](3) NULL,
	[TOSN21] [char](3) NULL,
	[TOSN22] [char](3) NULL,
	[TOSN23] [char](3) NULL,
	[TOSN24] [char](3) NULL,
	[TOSN25] [char](3) NULL,
	[TOSN26] [char](3) NULL,
	[TOSN27] [char](3) NULL,
	[TOSN28] [char](3) NULL,
	[TOSN29] [char](3) NULL,
	[TOSN30] [char](3) NULL,
	[TOSC1] [char](2) NULL,
	[TOSC2] [char](2) NULL,
	[TOSC3] [char](2) NULL,
	[TOSC4] [char](2) NULL,
	[TOSC5] [char](2) NULL,
	[TOSC6] [char](2) NULL,
	[TOSC7] [char](2) NULL,
	[TOSC8] [char](2) NULL,
	[TOSC9] [char](2) NULL,
	[TOSC10] [char](2) NULL,
	[TOSC11] [char](2) NULL,
	[TOSC12] [char](2) NULL,
	[TOSC13] [char](2) NULL,
	[TOSC14] [char](2) NULL,
	[TOSC15] [char](2) NULL,
	[TOSC16] [char](2) NULL,
	[TOSC17] [char](2) NULL,
	[TOSC18] [char](2) NULL,
	[TOSC19] [char](2) NULL,
	[TOSC20] [char](2) NULL,
	[TOSC21] [char](2) NULL,
	[TOSC22] [char](2) NULL,
	[TOSC23] [char](2) NULL,
	[TOSC24] [char](2) NULL,
	[TOSC25] [char](2) NULL,
	[TOSC26] [char](2) NULL,
	[TOSC27] [char](2) NULL,
	[TOSC28] [char](2) NULL,
	[TOSC29] [char](2) NULL,
	[TOSC30] [char](2) NULL,
	[TOSD1] [char](12) NULL,
	[TOSD2] [char](12) NULL,
	[TOSD3] [char](12) NULL,
	[TOSD4] [char](12) NULL,
	[TOSD5] [char](12) NULL,
	[TOSD6] [char](12) NULL,
	[TOSD7] [char](12) NULL,
	[TOSD8] [char](12) NULL,
	[TOSD9] [char](12) NULL,
	[TOSD10] [char](12) NULL,
	[TOSD11] [char](12) NULL,
	[TOSD12] [char](12) NULL,
	[TOSD13] [char](12) NULL,
	[TOSD14] [char](12) NULL,
	[TOSD15] [char](12) NULL,
	[TOSD16] [char](12) NULL,
	[TOSD17] [char](12) NULL,
	[TOSD18] [char](12) NULL,
	[TOSD19] [char](12) NULL,
	[TOSD20] [char](12) NULL,
	[TOSD21] [char](12) NULL,
	[TOSD22] [char](12) NULL,
	[TOSD23] [char](12) NULL,
	[TOSD24] [char](12) NULL,
	[TOSD25] [char](12) NULL,
	[TOSD26] [char](12) NULL,
	[TOSD27] [char](12) NULL,
	[TOSD28] [char](12) NULL,
	[TOSD29] [char](12) NULL,
	[TOSD30] [char](12) NULL,
	[TTOT1] [bit] NOT NULL,
	[TTOT2] [bit] NOT NULL,
	[TTOT3] [bit] NOT NULL,
	[TTOT4] [bit] NOT NULL,
	[TTOT5] [bit] NOT NULL,
	[TTOT6] [bit] NOT NULL,
	[TTOT7] [bit] NOT NULL,
	[TTOT8] [bit] NOT NULL,
	[TTOT9] [bit] NOT NULL,
	[TTOT10] [bit] NOT NULL,
	[TTCC1] [bit] NOT NULL,
	[TTCC2] [bit] NOT NULL,
	[TTCC3] [bit] NOT NULL,
	[TTCC4] [bit] NOT NULL,
	[TTCC5] [bit] NOT NULL,
	[TTCC6] [bit] NOT NULL,
	[TTCC7] [bit] NOT NULL,
	[TTCC8] [bit] NOT NULL,
	[TTCC9] [bit] NOT NULL,
	[TTCC10] [bit] NOT NULL,
	[TTDE1] [char](12) NULL,
	[TTDE2] [char](12) NULL,
	[TTDE3] [char](12) NULL,
	[TTDE4] [char](12) NULL,
	[TTDE5] [char](12) NULL,
	[TTDE6] [char](12) NULL,
	[TTDE7] [char](12) NULL,
	[TTDE8] [char](12) NULL,
	[TTDE9] [char](12) NULL,
	[TTDE10] [char](12) NULL,
	[ODRC1] [char](20) NULL,
	[ODRC2] [char](20) NULL,
	[ODRC3] [char](20) NULL,
	[ODRC4] [char](20) NULL,
	[ODRC5] [char](20) NULL,
	[ODRC6] [char](20) NULL,
	[ODRC7] [char](20) NULL,
	[ODRC8] [char](20) NULL,
	[ODRC9] [char](20) NULL,
	[ODRC10] [char](20) NULL,
	[DOLR] [char](8) NULL,
	[DOLU] [char](8) NULL,
	[PCAA] [bit] NOT NULL,
	[PCDA] [decimal](3, 0) NULL,
	[PSEL] [bit] NOT NULL,
	[BLKU] [bit] NOT NULL,
	[CNAM] [char](30) NULL,
	[CAD1] [char](30) NULL,
	[CAD2] [char](30) NULL,
	[CAD3] [char](30) NULL,
	[CTEL] [char](15) NULL,
	[CTLX] [char](15) NULL,
	[PATH] [char](3) NULL,
	[COID] [char](8) NULL,
	[CLOV] [decimal](5, 3) NULL,
	[CSAL1] [char](3) NULL,
	[CSAL2] [char](3) NULL,
	[CSAL3] [char](3) NULL,
	[CSAL4] [char](3) NULL,
	[CHON] [decimal](3, 0) NULL,
	[VION] [decimal](3, 0) NULL,
	[MCON] [decimal](3, 0) NULL,
	[AXON] [decimal](3, 0) NULL,
	[WION] [decimal](3, 0) NULL,
	[SVON] [decimal](3, 0) NULL,
	[SWON] [decimal](3, 0) NULL,
	[ACON] [decimal](3, 0) NULL,
	[MPRC1] [char](20) NULL,
	[MPRC2] [char](20) NULL,
	[MPRC3] [char](20) NULL,
	[MPRC4] [char](20) NULL,
	[MPRC5] [char](20) NULL,
	[MPRC6] [char](20) NULL,
	[MPRC7] [char](20) NULL,
	[MPRC8] [char](20) NULL,
	[MPRC9] [char](20) NULL,
	[MPRC10] [char](20) NULL,
	[MPRC11] [char](20) NULL,
	[MPRC12] [char](20) NULL,
	[MPRC13] [char](20) NULL,
	[MPRC14] [char](20) NULL,
	[MPRC15] [char](20) NULL,
	[MPRC16] [char](20) NULL,
	[MPRC17] [char](20) NULL,
	[MPRC18] [char](20) NULL,
	[MPRC19] [char](20) NULL,
	[MPRC20] [char](20) NULL,
	[SDIO] [decimal](3, 0) NULL,
	[MMRC1] [char](20) NULL,
	[MMRC2] [char](20) NULL,
	[MMRC3] [char](20) NULL,
	[MMRC4] [char](20) NULL,
	[MMRC5] [char](20) NULL,
	[MMRC6] [char](20) NULL,
	[MMRC7] [char](20) NULL,
	[MMRC8] [char](20) NULL,
	[MMRC9] [char](20) NULL,
	[MMRC10] [char](20) NULL,
	[MMRC11] [char](20) NULL,
	[MMRC12] [char](20) NULL,
	[MMRC13] [char](20) NULL,
	[MMRC14] [char](20) NULL,
	[MMRC15] [char](20) NULL,
	[MMRC16] [char](20) NULL,
	[MMRC17] [char](20) NULL,
	[MMRC18] [char](20) NULL,
	[MMRC19] [char](20) NULL,
	[MMRC20] [char](20) NULL,
	[SDOO] [decimal](3, 0) NULL,
	[PORC1] [char](20) NULL,
	[PORC2] [char](20) NULL,
	[PORC3] [char](20) NULL,
	[PORC4] [char](20) NULL,
	[PORC5] [char](20) NULL,
	[PORC6] [char](20) NULL,
	[PORC7] [char](20) NULL,
	[PORC8] [char](20) NULL,
	[PORC9] [char](20) NULL,
	[PORC10] [char](20) NULL,
	[CCRN] [char](16) NULL,
	[MAXC] [decimal](9, 2) NULL,
	[CCFL] [decimal](9, 2) NULL,
	[VATN] [decimal](3, 0) NULL,
	[VATI] [bit] NOT NULL,
	[VATR1] [decimal](5, 3) NULL,
	[VATR2] [decimal](5, 3) NULL,
	[VATR3] [decimal](5, 3) NULL,
	[VATR4] [decimal](5, 3) NULL,
	[VATR5] [decimal](5, 3) NULL,
	[VATR6] [decimal](5, 3) NULL,
	[VATR7] [decimal](5, 3) NULL,
	[VATR8] [decimal](5, 3) NULL,
	[VATR9] [decimal](5, 3) NULL,
	[VDIS] [decimal](5, 3) NULL,
	[TDES1] [char](12) NULL,
	[TDES2] [char](12) NULL,
	[TDES3] [char](12) NULL,
	[TDES4] [char](12) NULL,
	[TDES5] [char](12) NULL,
	[TDES6] [char](12) NULL,
	[TDES7] [char](12) NULL,
	[TDES8] [char](12) NULL,
	[TDES9] [char](12) NULL,
	[TDES10] [char](12) NULL,
	[TDES11] [char](12) NULL,
	[TDES12] [char](12) NULL,
	[TDES13] [char](12) NULL,
	[TDES14] [char](12) NULL,
	[TDES15] [char](12) NULL,
	[TDES16] [char](12) NULL,
	[TDES17] [char](12) NULL,
	[TDES18] [char](12) NULL,
	[TDES19] [char](12) NULL,
	[TDES20] [char](12) NULL,
	[TATC1] [decimal](3, 0) NULL,
	[TATC2] [decimal](3, 0) NULL,
	[TATC3] [decimal](3, 0) NULL,
	[TATC4] [decimal](3, 0) NULL,
	[TATC5] [decimal](3, 0) NULL,
	[TATC6] [decimal](3, 0) NULL,
	[TATC7] [decimal](3, 0) NULL,
	[TATC8] [decimal](3, 0) NULL,
	[TATC9] [decimal](3, 0) NULL,
	[TATC10] [decimal](3, 0) NULL,
	[TATC11] [decimal](3, 0) NULL,
	[TATC12] [decimal](3, 0) NULL,
	[TATC13] [decimal](3, 0) NULL,
	[TATC14] [decimal](3, 0) NULL,
	[TATC15] [decimal](3, 0) NULL,
	[TATC16] [decimal](3, 0) NULL,
	[TATC17] [decimal](3, 0) NULL,
	[TATC18] [decimal](3, 0) NULL,
	[TATC19] [decimal](3, 0) NULL,
	[TATC20] [decimal](3, 0) NULL,
	[TODR1] [bit] NOT NULL,
	[TODR2] [bit] NOT NULL,
	[TODR3] [bit] NOT NULL,
	[TODR4] [bit] NOT NULL,
	[TODR5] [bit] NOT NULL,
	[TODR6] [bit] NOT NULL,
	[TODR7] [bit] NOT NULL,
	[TODR8] [bit] NOT NULL,
	[TODR9] [bit] NOT NULL,
	[TODR10] [bit] NOT NULL,
	[TODR11] [bit] NOT NULL,
	[TODR12] [bit] NOT NULL,
	[TODR13] [bit] NOT NULL,
	[TODR14] [bit] NOT NULL,
	[TODR15] [bit] NOT NULL,
	[TODR16] [bit] NOT NULL,
	[TODR17] [bit] NOT NULL,
	[TODR18] [bit] NOT NULL,
	[TODR19] [bit] NOT NULL,
	[TODR20] [bit] NOT NULL,
	[TADA1] [bit] NOT NULL,
	[TADA2] [bit] NOT NULL,
	[TADA3] [bit] NOT NULL,
	[TADA4] [bit] NOT NULL,
	[TADA5] [bit] NOT NULL,
	[TADA6] [bit] NOT NULL,
	[TADA7] [bit] NOT NULL,
	[TADA8] [bit] NOT NULL,
	[TADA9] [bit] NOT NULL,
	[TADA10] [bit] NOT NULL,
	[TADA11] [bit] NOT NULL,
	[TADA12] [bit] NOT NULL,
	[TADA13] [bit] NOT NULL,
	[TADA14] [bit] NOT NULL,
	[TADA15] [bit] NOT NULL,
	[TADA16] [bit] NOT NULL,
	[TADA17] [bit] NOT NULL,
	[TADA18] [bit] NOT NULL,
	[TADA19] [bit] NOT NULL,
	[TADA20] [bit] NOT NULL,
	[MODR] [decimal](3, 0) NULL,
	[MAXF] [decimal](9, 2) NULL,
	[MSAV] [decimal](5, 2) NULL,
	[MRSA] [decimal](9, 2) NULL,
	[IEVT] [bit] NOT NULL,
	[SDIS] [decimal](5, 3) NULL,
	[VATP] [decimal](5, 2) NULL,
	[VATO] [bit] NOT NULL,
	[VATL] [bit] NOT NULL,
	[TSUP1] [bit] NOT NULL,
	[TSUP2] [bit] NOT NULL,
	[TSUP3] [bit] NOT NULL,
	[TSUP4] [bit] NOT NULL,
	[TSUP5] [bit] NOT NULL,
	[TSUP6] [bit] NOT NULL,
	[TSUP7] [bit] NOT NULL,
	[TSUP8] [bit] NOT NULL,
	[TSUP9] [bit] NOT NULL,
	[TSUP10] [bit] NOT NULL,
	[TSUP11] [bit] NOT NULL,
	[TSUP12] [bit] NOT NULL,
	[TSUP13] [bit] NOT NULL,
	[TSUP14] [bit] NOT NULL,
	[TSUP15] [bit] NOT NULL,
	[TSUP16] [bit] NOT NULL,
	[TSUP17] [bit] NOT NULL,
	[TSUP18] [bit] NOT NULL,
	[TSUP19] [bit] NOT NULL,
	[TSUP20] [bit] NOT NULL,
	[TADR1] [bit] NOT NULL,
	[TADR2] [bit] NOT NULL,
	[TADR3] [bit] NOT NULL,
	[TADR4] [bit] NOT NULL,
	[TADR5] [bit] NOT NULL,
	[TADR6] [bit] NOT NULL,
	[TADR7] [bit] NOT NULL,
	[TADR8] [bit] NOT NULL,
	[TADR9] [bit] NOT NULL,
	[TADR10] [bit] NOT NULL,
	[TADR11] [bit] NOT NULL,
	[TADR12] [bit] NOT NULL,
	[TADR13] [bit] NOT NULL,
	[TADR14] [bit] NOT NULL,
	[TADR15] [bit] NOT NULL,
	[TADR16] [bit] NOT NULL,
	[TADR17] [bit] NOT NULL,
	[TADR18] [bit] NOT NULL,
	[TADR19] [bit] NOT NULL,
	[TADR20] [bit] NOT NULL,
	[SAU1] [decimal](9, 2) NULL,
	[SAU2] [decimal](9, 2) NULL,
	[SAU3] [decimal](9, 2) NULL,
	[SAU4] [decimal](9, 2) NULL,
	[SAU5] [decimal](9, 2) NULL,
	[MINC] [decimal](9, 2) NULL,
	[SMAX] [decimal](5, 0) NULL,
	[PCAU] [bit] NOT NULL,
	[PCMA] [char](3) NULL,
	[LOAD] [char](1) NULL,
	[CountryCode] [char](3) NULL,
	[TTDE11] [char](12) NULL,
	[TTDE12] [char](12) NULL,
	[TTDE13] [char](12) NULL,
	[TTDE14] [char](12) NULL,
	[TTDE15] [char](12) NULL,
	[TTDE16] [char](12) NULL,
	[TTDE17] [char](12) NULL,
	[TTDE18] [char](12) NULL,
	[TTDE19] [char](12) NULL,
	[TTDE20] [char](12) NULL,
 CONSTRAINT [PK_RETOPT] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Number - Key to file - Always set to 01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store number for this store  - valid are 001 to 999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'STOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SNAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line one' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SAD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line two' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SAD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line three' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SAD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accountability type  C=Cashier  T=Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Highest Valid Cashier Number - Over = Training' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'HCAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default Float Amount for Accountability by Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'FLOT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Accounting Period Number Closed by Retail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'LAST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next Accounting Period Number to Open 01 to 31' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'NEXT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Accounting Periods to Keep on Disc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'KEEP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'30 Type of Sale Numbers - EX: J00 J13, ETC.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSN30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Code as in above box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSC30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TOS Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TOSD30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Over Tender Allowed Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTOT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Capture Indicator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTCC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TTDE10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 to 10 valid - Blank = invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ODRC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of last reformat (ERFT OR BRFT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'DOLR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of last Update (RSBUPD)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'DOLU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y/N - Auto Apply Price Changes on or after eff date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PCAA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If above Y, Number of days after effective date to
perform the auto apply of the price change.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PCDA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y/N -  Print Shelf edge Labels for price changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PSEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W07  ON = GSBLKU needed - set by SUBALL when run.
No longer Used *W07  Also set by GSMGRP if a description changes or add' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'BLKU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CNAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line one' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CAD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line two' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CAD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line three' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CAD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CTEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Telex Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CTLX'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Path Name - ie, WIX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PATH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company ID Number - For Company Credit Cards' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'COID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percent over credit limit allowed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CLOV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Charge Transaction type - Example = J03' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CSAL1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Charge Transaction type - Example = J03' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CSAL2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Charge Transaction type - Example = J03' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CSAL3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Charge Transaction type - Example = J03' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CSAL4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number for Cheques' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CHON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number for Visa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VION'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number for Access' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MCON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number for AMEX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'AXON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number for PROJECT LOAN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'WION'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number for Store Vouchers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SVON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number for Switch' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SWON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number of Account (Builders Mate)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'ACON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MPRC20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurance Number for Store Deposit Ins' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SDIO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 to 20 valid   Blank=Invalid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MMRC20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurance Number for Store Deposit Outs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SDOO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Up to 10 Price Override Reason Codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PORC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C/Card Retailer No. xxxx-xxx-xxxx-xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CCRN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Max Cash Change Given - 49.99' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MAXC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit Card Floor Limit - 100.00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CCFL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of VAT Rates being Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = VAT Rates are Inclusive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATR9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Highest Valid Discount Percent  ex: 20.000' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VDIS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Descriptions (allows twenty)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TDES20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RETOPT (10) Tender Number this Relates to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TATC20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Open Cash Drawer for this Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TODR20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Default Remaining Amount for Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADA20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurance number of MISC Open Drawer Reas.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MODR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum Till Drawer Float' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MAXF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Minimum Saving Value Before "You Have Saved"
Message will be Displayed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MSAV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Maximum Refund Self-Authorisation Amount - Used for
  *W05 non-supervisor cashiers ONLY.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MRSA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06??? Y/N Till permitted to access (store) Event ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'IEVT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 Secondary Staff Discount Percentage
W06 - Use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SDIS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W08 VAT Percentage Discount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W08 VAT Discount active? ON = apply discount
W09:' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W09 Not used. Fee Statement Must be incliuded on s' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'VATL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = supervisor required when tender selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TSUP20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = customer details required when tender select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'TADR20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Supervisor authorisation limit 1              ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SAU1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Supervisor authorisation limit 2              ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SAU2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Supervisor authorisation limit 3              ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SAU3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Supervisor authorisation limit 4              ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SAU4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Supervisor authorisation limit 5              ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SAU5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12 Minimum Change given via Gift Token           ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'MINC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W13  Sales Maximum Field' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'SMAX'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y/N  - Auto Apply Price Change Authorised' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PCAU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity of manager who authorised Auto Apply' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'PCMA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y" = Processing HOSTU LOAD file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'LOAD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internet country code in which store resides.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT', @level2type=N'COLUMN',@level2name=N'CountryCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R E T O P T  =   Retail Options Record ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETOPT'
END
GO

