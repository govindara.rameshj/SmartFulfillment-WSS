﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SYSCUR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SYSCUR'
CREATE TABLE [dbo].[SYSCUR](
	[CURR] [char](5) NOT NULL,
	[CURD] [char](35) NOT NULL,
	[MAXD] [decimal](9, 2) NOT NULL,
	[DELC] [bit] NOT NULL,
	[LDOM] [decimal](9, 0) NOT NULL,
	[MINT] [decimal](13, 2) NOT NULL,
	[SAFE] [decimal](13, 2) NOT NULL,
	[TEND1] [bit] NOT NULL,
	[TEND2] [bit] NOT NULL,
	[TEND3] [bit] NOT NULL,
	[TEND4] [bit] NOT NULL,
	[TEND5] [bit] NOT NULL,
	[TEND6] [bit] NOT NULL,
	[TEND7] [bit] NOT NULL,
	[TEND8] [bit] NOT NULL,
	[TEND9] [bit] NOT NULL,
	[TEND10] [bit] NOT NULL,
	[TEND11] [bit] NOT NULL,
	[TEND12] [bit] NOT NULL,
	[TEND13] [bit] NOT NULL,
	[TEND14] [bit] NOT NULL,
	[TEND15] [bit] NOT NULL,
	[TEND16] [bit] NOT NULL,
	[TEND17] [bit] NOT NULL,
	[TEND18] [bit] NOT NULL,
	[TEND19] [bit] NOT NULL,
	[TEND20] [bit] NOT NULL,
	[IEPC] [bit] NOT NULL,
	[CurrencySymbol] [char](3) NOT NULL,
	[PrintSymbol] [decimal](3, 0) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[InactiveDate] [date] NOT NULL,
	[SysDefaultCurr] [bit] NOT NULL,
	[MaxAccepted] [decimal](3, 0) NOT NULL,
	[Denom1] [decimal](7, 2) NOT NULL,
	[Denom2] [decimal](7, 2) NOT NULL,
	[Denom3] [decimal](7, 2) NOT NULL,
	[Denom4] [decimal](7, 2) NOT NULL,
	[Denom5] [decimal](7, 2) NOT NULL,
	[Denom6] [decimal](7, 2) NOT NULL,
	[Denom7] [decimal](7, 2) NOT NULL,
	[Denom8] [decimal](7, 2) NOT NULL,
	[Denom9] [decimal](7, 2) NOT NULL,
	[Denom10] [decimal](7, 2) NOT NULL,
	[Denom11] [decimal](7, 2) NOT NULL,
	[Denom12] [decimal](7, 2) NOT NULL,
	[Denom13] [decimal](7, 2) NOT NULL,
	[Denom14] [decimal](7, 2) NOT NULL,
	[Denom15] [decimal](7, 2) NOT NULL,
	[Denom16] [decimal](7, 2) NOT NULL,
	[Denom17] [decimal](7, 2) NOT NULL,
	[Denom18] [decimal](7, 2) NOT NULL,
	[Denom19] [decimal](7, 2) NOT NULL,
	[Denom20] [decimal](7, 2) NOT NULL,
	[DenTxt1] [char](20) NOT NULL,
	[DenTxt2] [char](20) NOT NULL,
	[DenTxt3] [char](20) NOT NULL,
	[DenTxt4] [char](20) NOT NULL,
	[DenTxt5] [char](20) NOT NULL,
	[DenTxt6] [char](20) NOT NULL,
	[DenTxt7] [char](20) NOT NULL,
	[DenTxt8] [char](20) NOT NULL,
	[DenTxt9] [char](20) NOT NULL,
	[DenTxt10] [char](20) NOT NULL,
	[DenTxt11] [char](20) NOT NULL,
	[DenTxt12] [char](20) NOT NULL,
	[DenTxt13] [char](20) NOT NULL,
	[DenTxt14] [char](20) NOT NULL,
	[DenTxt15] [char](20) NOT NULL,
	[DenTxt16] [char](20) NOT NULL,
	[DenTxt17] [char](20) NOT NULL,
	[DenTxt18] [char](20) NOT NULL,
	[DenTxt19] [char](20) NOT NULL,
	[DenTxt20] [char](20) NOT NULL,
	[DenSeq1] [decimal](3, 0) NOT NULL,
	[DenSeq2] [decimal](3, 0) NOT NULL,
	[DenSeq3] [decimal](3, 0) NOT NULL,
	[DenSeq4] [decimal](3, 0) NOT NULL,
	[DenSeq5] [decimal](3, 0) NOT NULL,
	[DenSeq6] [decimal](3, 0) NOT NULL,
	[DenSeq7] [decimal](3, 0) NOT NULL,
	[DenSeq8] [decimal](3, 0) NOT NULL,
	[DenSeq9] [decimal](3, 0) NOT NULL,
	[DenSeq10] [decimal](3, 0) NOT NULL,
	[DenSeq11] [decimal](3, 0) NOT NULL,
	[DenSeq12] [decimal](3, 0) NOT NULL,
	[DenSeq13] [decimal](3, 0) NOT NULL,
	[DenSeq14] [decimal](3, 0) NOT NULL,
	[DenSeq15] [decimal](3, 0) NOT NULL,
	[DenSeq16] [decimal](3, 0) NOT NULL,
	[DenSeq17] [decimal](3, 0) NOT NULL,
	[DenSeq18] [decimal](3, 0) NOT NULL,
	[DenSeq19] [decimal](3, 0) NOT NULL,
	[DenSeq20] [decimal](3, 0) NOT NULL,
	[SPARE] [char](100) NOT NULL
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Currency identifier - Left Justified - Eg. "GBP", "EUR"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'CURR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Currency Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'CURD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'MAXD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delete Flag True = Allow deletion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Largest accepted denomination value.
                        * This is used to calculate the maximum cash change.
                        * The calculated maximum will override the RETOPT
                        * maximum change field (RO:MAXC) only if it is
                        * greater.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'LDOM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimum Cash multiple.
Amount Tendered MUST be in multiples of MINT.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'MINT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of this currency in Store Safe.  Equivalent to BH:BANK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'SAFE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether this currency is available for the equivalent RETOPT tender.
Note - RETOPT:TTCC(O=10) (Credit card flag) will override setting here.  I.e. If TTCC is On, TEND will be set Off regardless.  This is because a Credit card will not be multi currency, it will always be local currency.As Tend 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'TEND20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = This is a EURO Participating Currency i.e. This currency is directly linked to the Euro currency (SYSOPT:EURO) by a fixed exchange rate. These currencies will expect their exchange rate to be defined as ?????? of this currency = 1 Euro. Conversion of any Euro participating currency to another must be perfomed via the Euro currency in a triangulated method.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'IEPC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Currency Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'CurrencySymbol'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Symbol to Print' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'PrintSymbol'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Active' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'ActiveDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Inactive ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'InactiveDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if the default currency for the store' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'SysDefaultCurr'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum Note Accepted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'MaxAccepted'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'Denom20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dnomination Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenTxt20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 14' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 16' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 19' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Denomination Sequence 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'DenSeq20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S C U R   =   System Currencies Description File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSCUR'
END
GO

