﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportGrouping]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ReportGrouping'
CREATE TABLE [dbo].[ReportGrouping](
	[ReportId] [int] NOT NULL,
	[TableId] [int] NOT NULL,
	[ColumnId] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[IsDescending] [bit] NOT NULL,
	[SummaryType] [int] NOT NULL,
	[ShowExpanded] [bit] NOT NULL,
 CONSTRAINT [PK_ReportGroupColumns] PRIMARY KEY CLUSTERED 
(
	[ReportId] ASC,
	[ColumnId] ASC,
	[TableId] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

