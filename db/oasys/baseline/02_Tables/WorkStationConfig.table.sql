﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WorkStationConfig]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table WorkStationConfig'
CREATE TABLE [dbo].[WorkStationConfig](
	[ID] [int] NOT NULL,
	[Description] [char](35) NOT NULL,
	[PrimaryFunction] [char](2) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[DateLastLoggedIn] [date] NULL,
	[IsBarcodeBroken] [bit] NOT NULL,
	[UseTouchScreen] [bit] NOT NULL,
	[SecurityProfileID] [int] NOT NULL,
 CONSTRAINT [PK_WorkStationConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig', @level2type=N'COLUMN',@level2name=N'Description'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Function' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig', @level2type=N'COLUMN',@level2name=N'PrimaryFunction'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if active' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig', @level2type=N'COLUMN',@level2name=N'IsActive'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Last Logged In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig', @level2type=N'COLUMN',@level2name=N'DateLastLoggedIn'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Barcode is Broken' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig', @level2type=N'COLUMN',@level2name=N'IsBarcodeBroken'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If using a touch screen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig', @level2type=N'COLUMN',@level2name=N'UseTouchScreen'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Profile Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig', @level2type=N'COLUMN',@level2name=N'SecurityProfileID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W O R K S T A T I O N C O N F I G = Work Station Configuration File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkStationConfig'
END
GO

