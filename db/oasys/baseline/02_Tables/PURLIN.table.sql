﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PURLIN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PURLIN'
CREATE TABLE [dbo].[PURLIN](
	[TKEY] [int] IDENTITY(1,1) NOT NULL,
	[HKEY] [int] NOT NULL,
	[SKUN] [char](6) NOT NULL,
	[KDES] [char](15) NULL,
	[PROD] [char](10) NULL,
	[QTYO] [int] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[COST] [decimal](11, 4) NULL,
	[DLOR] [date] NULL,
	[RQTY] [int] NULL,
	[RDAT] [date] NULL,
	[DELE] [bit] NOT NULL,
	[WRQT] [int] NULL,
	[WQTB] [decimal](7, 0) NULL,
	[WPRI] [decimal](9, 2) NULL,
	[WCOS] [decimal](11, 4) NULL,
	[WSHI] [char](10) NULL,
	[CONF] [char](1) NULL,
	[REAS] [smallint] NULL,
	[MESS] [char](60) NULL,
 CONSTRAINT [PK_PURLIN] PRIMARY KEY CLUSTERED 
(
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DIG compiler KEY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary KEY to PURHDR File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'HKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Keyed Description for Blanket items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'KDES'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Product Code - keyed for blanket else from the stkmas record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'PROD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity Ordered - Original' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'QTYO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price at time of order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cost at time of order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'COST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of last order - for DIG delete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'DLOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity Received so far' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'RQTY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of last receipt of this item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'RDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Received Complete - OK to delete line' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'DELE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity of this receipt (temporary work field) used during "Auto Issue of Receipts" to identify Issued lines (compared with non issued PO lines).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'WRQT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity Backordered (to follow) if allowed
*
*       THE ABOVE IS NOT A WORKING FIELD, IT CONTAINS THE AMOUNT TO FOLLOW
*       ON THE ORDER IF ANY. THIS IS THE DUE IN IF NOT ZERO.
*' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'WQTB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item Price at time of receipt (NOT USED)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'WPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cost at time of receipt (NOT USED)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'WCOS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shipper Number - from line by line entry
* THE ABOVE FIELD IS USED AS FOLLOWS:
*       R=(4+0) = Seqn number in DRLDET of receipt info for this line
*       R=(6+4) = Drl Number item received on.
*       Both of the redefines as used by PREISS only - not PREREC as
*       it does not require to-follows.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'WSHI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Confirmed Indicator;
 * " " = Unconfirmed
 * "A" = Amended
 * "C" = Confirmed 
 * "R" = Rejected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'CONF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reason code.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'REAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Free form Message (Record type 01 only) (NOT USED)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN', @level2type=N'COLUMN',@level2name=N'MESS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P U R L I N  =   Purchase Order Line Item Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURLIN'
END
GO

