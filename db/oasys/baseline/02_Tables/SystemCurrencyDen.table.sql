﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemCurrencyDen]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SystemCurrencyDen'
CREATE TABLE [dbo].[SystemCurrencyDen](
	[ID] [decimal](9, 2) NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[TenderID] [int] NOT NULL,
	[DisplayText] [char](20) NOT NULL,
	[BullionMultiple] [decimal](9, 2) NOT NULL,
	[BankingBagLimit] [decimal](9, 2) NOT NULL,
	[SafeMinimum] [decimal](9, 2) NOT NULL,
	[Amendable] [bit] NULL,
 CONSTRAINT [PK_SystemCurrencyDen] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC,
	[TenderID] ASC,
	[ID] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrencyDen', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Curency Identifier e.g. GBP = Sterling' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrencyDen', @level2type=N'COLUMN',@level2name=N'CurrencyID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Identifier;
 1 = Cash
 2 = Cheques
 3 = Debit/Credit Card' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrencyDen', @level2type=N'COLUMN',@level2name=N'TenderID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text to Display e.g. £5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrencyDen', @level2type=N'COLUMN',@level2name=N'DisplayText'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bullion Multiple' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrencyDen', @level2type=N'COLUMN',@level2name=N'BullionMultiple'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking Bag Limit amount (Value)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrencyDen', @level2type=N'COLUMN',@level2name=N'BankingBagLimit'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Safe Minimum Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrencyDen', @level2type=N'COLUMN',@level2name=N'SafeMinimum'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S T E M C U R R E N C Y D E N = System Currency Denomination File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SystemCurrencyDen'
END
GO

