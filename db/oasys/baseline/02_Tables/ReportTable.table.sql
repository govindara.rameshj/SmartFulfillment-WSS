﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportTable]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ReportTable'
CREATE TABLE [dbo].[ReportTable](
	[ReportId] [int] NOT NULL,
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[AllowGrouping] [bit] NOT NULL,
	[AllowSelection] [bit] NOT NULL,
	[ShowHeaders] [bit] NOT NULL,
	[ShowIndicator] [bit] NOT NULL,
	[ShowLinesVertical] [bit] NOT NULL,
	[ShowLinesHorizontal] [bit] NOT NULL,
	[ShowBorder] [bit] NOT NULL,
	[ShowInPrintOnly] [bit] NOT NULL,
	[AutoFitColumns] [bit] NOT NULL,
	[HeaderHeight] [int] NULL,
	[RowHeight] [int] NULL,
	[AnchorTop] [bit] NOT NULL,
	[AnchorBottom] [bit] NOT NULL,
 CONSTRAINT [PK_ReportTable] PRIMARY KEY CLUSTERED 
(
	[ReportId] ASC,
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

