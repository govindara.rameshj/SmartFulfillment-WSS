﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TOSCTL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TOSCTL'
CREATE TABLE [dbo].[TOSCTL](
	[TOSC] [char](2) NOT NULL,
	[TOSD] [char](15) NULL,
	[IUSE] [bit] NOT NULL,
	[UpdateZReads] [bit] NOT NULL,
 CONSTRAINT [PK_TOSCTL] PRIMARY KEY CLUSTERED 
(
	[TOSC] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of sale Code (e.g. SA = Sale)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSCTL', @level2type=N'COLUMN',@level2name=N'TOSC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Sale Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSCTL', @level2type=N'COLUMN',@level2name=N'TOSD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If In Use false If not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSCTL', @level2type=N'COLUMN',@level2name=N'IUSE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If need to update Z Reading False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSCTL', @level2type=N'COLUMN',@level2name=N'UpdateZReads'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'T O S C T L    =  PC Till TOS Contol file                               
                     Client''s do not have access to this file.           
                     Contains list of potential Types of Sale.             
                     This file is set up and maintained by CTS Only.       
                     Whether the Type of Sale is available to the client  
                     is determined by the IUSE flag. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSCTL'
END
GO

