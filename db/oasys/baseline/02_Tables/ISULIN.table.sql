﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ISULIN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ISULIN'
CREATE TABLE [dbo].[ISULIN](
	[NUMB] [char](6) NOT NULL,
	[LINE] [char](4) NOT NULL,
	[SKUN] [char](6) NULL,
	[QTYO] [int] NULL,
	[QTYI] [int] NULL,
	[QTYF] [int] NULL,
	[SRTF] [bit] NOT NULL,
	[ADDM] [bit] NOT NULL,
	[REJI] [char](1) NULL,
 CONSTRAINT [PK_ISULIN] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[LINE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Issue note number - KEY to ISUHDR File also' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line/Record number in store P/O
or BBC added line number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'LINE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number for this line' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity ordered from store - remaining' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'QTYO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity issued to store - this issue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'QTYI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Issue quantity to follow
only added by BBC maintenance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'QTYF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON=Supplier receipt to follow on BBC order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'SRTF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON=Added by maintenance at BBC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'ADDM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not rejected
I = Item not on STKMAS
W04 J = Item not in PURLIN      (Not Used)
W04 K = PURLIN record deleted  (Not Used)
W04 L = Issue QTYO not = PL:QTYO  (Not Used)
W04 M = Order Qty left not = to follow (Not Used)
N = Set Off' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN', @level2type=N'COLUMN',@level2name=N'REJI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'I S U L I N  =   Issue Note Line Item Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISULIN'
END
GO

