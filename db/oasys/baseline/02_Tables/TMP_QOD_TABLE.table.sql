﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TMP_QOD_TABLE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TMP_QOD_TABLE'
CREATE TABLE [dbo].[TMP_QOD_TABLE](
	[DelDate] [varchar](30) NULL,
	[DelDay] [nvarchar](30) NULL,
	[QuotesRaised] [int] NULL,
	[OrdersRaised] [int] NULL,
	[PercSold] [int] NULL,
	[QuotesDelivered] [int] NULL
) ON [PRIMARY]
END
GO

