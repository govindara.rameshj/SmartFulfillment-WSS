﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SOQSKU]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SOQSKU'
CREATE TABLE [dbo].[SOQSKU](
	[SKUN] [char](6) NOT NULL,
	[PATT] [char](1) NULL,
	[ELPD] [decimal](9, 2) NULL,
	[BTPD] [decimal](7, 2) NULL,
	[DEFE] [decimal](7, 2) NULL,
	[IESF] [decimal](7, 2) NULL,
	[VAUC] [char](2) NULL,
	[CONS] [decimal](5, 2) NULL,
	[SKUP] [decimal](3, 0) NULL,
	[SLOP] [decimal](5, 2) NULL,
	[BSIU] [decimal](7, 0) NULL,
	[OLIU] [decimal](7, 0) NULL,
	[FLAG1] [char](1) NULL,
	[FLAG2] [char](1) NULL,
	[FLAG3] [char](1) NULL,
	[FLAG4] [char](1) NULL,
	[FLAG5] [char](1) NULL,
	[FLAG6] [char](1) NULL,
	[FLAG7] [char](1) NULL,
	[FLAG8] [char](1) NULL,
	[FLAG9] [char](1) NULL,
	[FLAG10] [char](1) NULL,
	[FLAG11] [char](1) NULL,
	[FLAG12] [char](1) NULL,
	[CHIN] [char](1) NULL,
	[SPAT] [char](4) NULL,
	[AORD] [decimal](7, 0) NULL,
	[FCST] [char](1) NULL,
	[BPAT] [char](4) NULL,
	[PPAT] [char](4) NULL,
	[RDAT] [date] NULL,
	[BIAS1] [decimal](5, 3) NULL,
	[BIAS2] [decimal](5, 3) NULL,
	[BIAS3] [decimal](5, 3) NULL,
	[BIAS4] [decimal](5, 3) NULL,
	[BIAS5] [decimal](5, 3) NULL,
	[BIAS6] [decimal](5, 3) NULL,
	[BIAS7] [decimal](5, 3) NULL,
	[SPARE] [char](99) NULL,
 CONSTRAINT [PK_SOQSKU] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SKU number - Key to file.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Demand Pattern Classification' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'PATT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Level of Period Demand.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'ELPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Trend in Period Demand.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BTPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estimate of Forecast Error.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'DEFE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initial Estimate of Smoothed Forecast Error.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'IESF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of annual usage - A1 - C3.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'VAUC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Buffer Conversion Constant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'CONS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period adjusters pattern for this SKU' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'SKUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Service Level Override %.
   If 0 then use System Service Level for Class VAU.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'SLOP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Buffer stock in unit.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BSIU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Level in Units.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'OLIU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FLAG12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CHIN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'CHIN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Seasonality pattern - held in SOQPAR  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'SPAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual ordered quantity - from POESOQ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'AORD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Forecast indicator - Blank = Initialise
                R = Re-classify
         F = Forecast' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'FCST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bank Holiday pattern - held in SOQPAR  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BPAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Promotional pattern - held in SOQPAR  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'PPAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of last Forecast/initialisation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'RDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Sales Bias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BIAS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Sales Bias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BIAS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Sales Bias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BIAS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Sales Bias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BIAS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Sales Bias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BIAS5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Sales Bias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BIAS6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Sales Bias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'BIAS7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S O Q S K U  =   SOQ SKU Record ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQSKU'
END
GO

