﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AUDMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table AUDMAS'
CREATE TABLE [dbo].[AUDMAS](
	[SKUN] [char](6) NOT NULL,
	[SQTY] [decimal](7, 0) NULL,
	[EQTY] [decimal](7, 0) NULL,
	[FLAG] [bit] NOT NULL,
	[YTDS] [decimal](9, 2) NULL,
	[PYRS] [decimal](9, 2) NULL,
	[SMQT] [decimal](7, 0) NULL,
	[EMQT] [decimal](7, 0) NULL,
 CONSTRAINT [PK_AUDMAS] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number - Valid are 000001 to 999999' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AUDMAS', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 saved normal on-hand quantity (excludes markdown stock)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AUDMAS', @level2type=N'COLUMN',@level2name=N'SQTY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A U D M A S  =  Sample Stock Audit File Definition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AUDMAS'
END
GO

