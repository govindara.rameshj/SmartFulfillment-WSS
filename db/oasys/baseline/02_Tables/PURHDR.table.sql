﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PURHDR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PURHDR'
CREATE TABLE [dbo].[PURHDR](
	[TKEY] [int] IDENTITY(1,1) NOT NULL,
	[NUMB] [int] NULL,
	[SUPP] [char](5) NULL,
	[ODAT] [date] NULL,
	[DDAT] [date] NULL,
	[RELN] [int] NOT NULL,
	[RAIS] [char](5) NULL,
	[EmployeeId] [int] NULL,
	[SRCE] [char](1) NULL,
	[HONO] [char](6) NULL,
	[SOQN] [int] NULL,
	[NOCR] [int] NULL,
	[QTYO] [int] NULL,
	[VALU] [decimal](9, 2) NULL,
	[PRFL] [char](1) NULL,
	[NORE] [smallint] NULL,
	[RNUM] [int] NULL,
	[PNUM] [int] NULL,
	[RCOM] [bit] NOT NULL,
	[RPAR] [bit] NOT NULL,
	[DELM] [bit] NOT NULL,
	[BBCC] [char](1) NULL,
	[COMM] [bit] NOT NULL,
	[TNET] [bit] NOT NULL,
	[CONF] [char](1) NULL,
	[REAS] [smallint] NULL,
	[MES1] [char](60) NULL,
	[MES2] [char](60) NULL,
	[OKIS] [bit] NOT NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_PURHDR] PRIMARY KEY CLUSTERED 
(
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DIG Compiler KEY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Response Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number of this Purchase Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'SUPP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the Order was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'ODAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expected Delivery Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'DDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Release Number of this Purchase Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'RELN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials of who Raised it  - type S=SOQ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'RAIS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Source of Entry  S=soq E=entry M=master O=Standing Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'SRCE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Head Office Purchase Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'HONO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ Number used to create this - srce=S' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'SOQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ Number used to create this - srce=S' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'NOCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total of Quantity on order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'QTYO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of this Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'VALU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Print Flag Y=print R=reprint N=printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'PRFL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of re-prints of this P/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'NORE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Receipt Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'RNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Pricing Doc (lorry Tracking) Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'PNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON=Received Complete - OK to Delete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'RCOM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' ON=Received Partial - delete completed lines' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'RPAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON=Deleted by Maintenance - delete order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'DELM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BBC Supplier code - space = direct to store
 *                       C   = BBC consolidated
 *                       D   = BBC discreet
 *                       W  = Warehouse
 *                       A   = Alternative Supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'BBCC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prepared P/O for comms to BBC - only used for BBC types C & D, Y = done' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'COMM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Trader-Net Supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'TNET'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Confirmation Indicator
   * " " - Inconfirmed P/O
   * "A" - Amendment of a P/O
   * "C" - Confirmation of a P/O
   * "R" - Rejection of a P/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'CONF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amended / Rejected Reason Codes;
  * 01 - Destination location nnnn does not exist for company xx
  * 02 - Raised location nnnn does not exist for company xx
  * 03 - Supplier does not exist
  * 04 - Order date raised is future date
  * 05 - Order Date raised is an invalid date
  * 06 - Order expected delivery date is an invalid date
  * 07 - Sku does not exist for supplier
  * 08 - Sku is not unique within order
  * 09 - Company Code does not exist
  * 10 - Supplier SOQ day not valid for the date raised - Referred
  * 11 - Store SOQ day does not match Central maintenance -Referred
  * 12 - Supplier referral flag is set - Order Referred
  * 13 - Supplier SOQ day is not valid - Referred
  * 14 - Order contains Sku flagged as deleted - Referred
  * 15 - WARNING: LOC-SOQ-NR is not numeric
  * 16 - Line number is not unique
  * 17 - Line number is not numeric
  * 18 - No SOQ info for supplier & store on order date
  * 19 - Order confirms invalid central order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'REAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Free form Message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'MES1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Free form Message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'MES2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'"Y" = Issue file validated against order.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR', @level2type=N'COLUMN',@level2name=N'OKIS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P U R H D R  =   Purchase Order Header Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PURHDR'
END
GO

