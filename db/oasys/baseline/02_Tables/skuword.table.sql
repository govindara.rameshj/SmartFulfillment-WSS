﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[skuword]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table skuword'
CREATE TABLE [dbo].[skuword](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SkuId] [int] NULL,
	[Word] [varchar](200) NULL,
	[VowelsStripped] [bit] NULL,
	[Sku] [char](6) NULL,
	[Category] [int] NULL,
	[Group] [int] NULL,
	[SubGroup] [int] NULL,
	[Style] [int] NULL
) ON [PRIMARY]
END
GO

