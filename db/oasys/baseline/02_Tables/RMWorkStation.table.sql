﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RMWorkStation]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RMWorkStation'
CREATE TABLE [dbo].[RMWorkStation](
	[WorkstationID] [int] NOT NULL,
	[ReleaseID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateStarted] [datetime] NULL,
	[DateEnded] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO

