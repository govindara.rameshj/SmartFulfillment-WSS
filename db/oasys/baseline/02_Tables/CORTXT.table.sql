﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CORTXT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CORTXT'
CREATE TABLE [dbo].[CORTXT](
	[NUMB] [char](6) NOT NULL,
	[TYPE] [char](2) NOT NULL,
	[LINE] [char](2) NOT NULL,
	[TEXT] [nvarchar](200) NULL,
	[SellingStoreId] [int] NOT NULL,
	[SellingStoreOrderId] [int] NOT NULL,
 CONSTRAINT [PK_CORTXT] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC,
	[TYPE] ASC,
	[LINE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Customer Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORTXT', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORTXT', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORTXT', @level2type=N'COLUMN',@level2name=N'LINE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORTXT', @level2type=N'COLUMN',@level2name=N'TEXT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O R T X T  =  Customer Order Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CORTXT'
END
GO

