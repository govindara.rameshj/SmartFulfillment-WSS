﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WSOCTL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table WSOCTL'
CREATE TABLE [dbo].[WSOCTL](
	[WSID] [char](2) NOT NULL,
	[DESCR] [char](35) NULL,
	[NODE] [char](12) NULL,
	[PFCT] [char](2) NULL,
	[ACTV] [bit] NOT NULL,
	[TGRP1] [bit] NOT NULL,
	[TGRP2] [bit] NOT NULL,
	[TGRP3] [bit] NOT NULL,
	[TGRP4] [bit] NOT NULL,
	[TGRP5] [bit] NOT NULL,
	[TGRP6] [bit] NOT NULL,
	[TGRP7] [bit] NOT NULL,
	[TGRP8] [bit] NOT NULL,
	[TGRP9] [bit] NOT NULL,
	[IAFG1] [bit] NOT NULL,
	[IAFG2] [bit] NOT NULL,
	[IAFG3] [bit] NOT NULL,
	[IAFG4] [bit] NOT NULL,
	[IAFG5] [bit] NOT NULL,
	[IAFG6] [bit] NOT NULL,
	[IAFG7] [bit] NOT NULL,
	[IAFG8] [bit] NOT NULL,
	[IAFG9] [bit] NOT NULL,
	[IAFG10] [bit] NOT NULL,
	[IAFG11] [bit] NOT NULL,
	[IAFG12] [bit] NOT NULL,
	[IAFG13] [bit] NOT NULL,
	[IAFG14] [bit] NOT NULL,
	[IAFG15] [bit] NOT NULL,
	[IAFG16] [bit] NOT NULL,
	[IAFG17] [bit] NOT NULL,
	[IAFG18] [bit] NOT NULL,
	[IAFG19] [bit] NOT NULL,
	[IAFG20] [bit] NOT NULL,
	[IAFG21] [bit] NOT NULL,
	[IAFG22] [bit] NOT NULL,
	[IAFG23] [bit] NOT NULL,
	[IAFG24] [bit] NOT NULL,
	[IAFG25] [bit] NOT NULL,
	[IAFG26] [bit] NOT NULL,
	[IAFG27] [bit] NOT NULL,
	[IAFG28] [bit] NOT NULL,
	[IAFG29] [bit] NOT NULL,
	[IAFG30] [bit] NOT NULL,
	[IAFG31] [bit] NOT NULL,
	[IAFG32] [bit] NOT NULL,
	[IAFG33] [bit] NOT NULL,
	[IAFG34] [bit] NOT NULL,
	[IAFG35] [bit] NOT NULL,
	[IAFG36] [bit] NOT NULL,
	[IAFG37] [bit] NOT NULL,
	[IAFG38] [bit] NOT NULL,
	[IAFG39] [bit] NOT NULL,
	[IAFG40] [bit] NOT NULL,
	[IAFG41] [bit] NOT NULL,
	[IAFG42] [bit] NOT NULL,
	[IAFG43] [bit] NOT NULL,
	[IAFG44] [bit] NOT NULL,
	[IAFG45] [bit] NOT NULL,
	[IAFG46] [bit] NOT NULL,
	[IAFG47] [bit] NOT NULL,
	[IAFG48] [bit] NOT NULL,
	[IAFG49] [bit] NOT NULL,
	[IAFG50] [bit] NOT NULL,
	[IAFG51] [bit] NOT NULL,
	[IAFG52] [bit] NOT NULL,
	[IAFG53] [bit] NOT NULL,
	[IAFG54] [bit] NOT NULL,
	[IAFG55] [bit] NOT NULL,
	[IAFG56] [bit] NOT NULL,
	[IAFG57] [bit] NOT NULL,
	[IAFG58] [bit] NOT NULL,
	[IAFG59] [bit] NOT NULL,
	[IAFG60] [bit] NOT NULL,
	[IAFG61] [bit] NOT NULL,
	[IAFG62] [bit] NOT NULL,
	[IAFG63] [bit] NOT NULL,
	[IAFG64] [bit] NOT NULL,
	[IAFG65] [bit] NOT NULL,
	[IAFG66] [bit] NOT NULL,
	[IAFG67] [bit] NOT NULL,
	[IAFG68] [bit] NOT NULL,
	[IAFG69] [bit] NOT NULL,
	[IAFG70] [bit] NOT NULL,
	[IAFG71] [bit] NOT NULL,
	[IAFG72] [bit] NOT NULL,
	[IAFG73] [bit] NOT NULL,
	[IAFG74] [bit] NOT NULL,
	[IAFG75] [bit] NOT NULL,
	[IAFG76] [bit] NOT NULL,
	[IAFG77] [bit] NOT NULL,
	[IAFG78] [bit] NOT NULL,
	[IAFG79] [bit] NOT NULL,
	[IAFG80] [bit] NOT NULL,
	[IAFG81] [bit] NOT NULL,
	[IAFG82] [bit] NOT NULL,
	[IAFG83] [bit] NOT NULL,
	[IAFG84] [bit] NOT NULL,
	[IAFG85] [bit] NOT NULL,
	[IAFG86] [bit] NOT NULL,
	[IAFG87] [bit] NOT NULL,
	[IAFG88] [bit] NOT NULL,
	[IAFG89] [bit] NOT NULL,
	[IAFG90] [bit] NOT NULL,
	[IAFG91] [bit] NOT NULL,
	[IAFG92] [bit] NOT NULL,
	[IAFG93] [bit] NOT NULL,
	[IAFG94] [bit] NOT NULL,
	[IAFG95] [bit] NOT NULL,
	[IAFG96] [bit] NOT NULL,
	[IAFG97] [bit] NOT NULL,
	[IAFG98] [bit] NOT NULL,
	[IAFG99] [bit] NOT NULL,
	[DLog] [date] NULL,
	[BCOD] [char](1) NULL,
	[TOUCH] [char](1) NULL,
	[WACT] [bit] NOT NULL,
	[QUOTTILL] [char](1) NULL,
	[CSDRAWER] [char](1) NULL,
	[USEEFT] [char](1) NULL,
	[PRNTTYPE] [char](1) NULL,
 CONSTRAINT [PK_WSOCTL] PRIMARY KEY CLUSTERED 
(
	[WSID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Workstation Outlet Control Number
W03   WSID = "77" for (all) in use HHTs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'WSID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Network Node ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'NODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Function of the Outlet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'PFCT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Slave workstation is Opened
OFF = Slave workstation is Not Open
W02   ON  = This workstation is running an UPTRAC
W02         application program.
W02   OFF = This workstation has at least returned
W02         to the UPTRAC menu.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'ACTV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Groups that can be Accepted at this
Outlet.  See RETOPT for descriptions. - Not Used

Cash, Cheques, Credit Cards, etc would be an
example of tender groups.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TGRP9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG31'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG32'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG33'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG34'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG35'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG36'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG37'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG38'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG39'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG40'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG41'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG42'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG43'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG44'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG45'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG46'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG47'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG48'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG49'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG51'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG52'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG53'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG54'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG55'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG56'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG57'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG58'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG59'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG60'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG61'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG62'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG63'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG64'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG65'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG66'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG67'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG68'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG69'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG70'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG71'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG72'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG73'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG74'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG75'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG76'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG77'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG78'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG79'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG80'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG81'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG82'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG83'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG84'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG85'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG86'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG87'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG88'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG89'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG90'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG91'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG92'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG93'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG94'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG95'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG96'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG97'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG98'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Access to the Application Functional
Group is allowed. See AFGCTL.
Off = Access to the AFG is Not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'IAFG99'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log Date of last copied to local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'DLog'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Barcode Scanner flagged as broken' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'BCOD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y = Touch screen in use on this workstation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'TOUCH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Windows Menu Active?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'WACT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag if Quotes available on this workstation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'QUOTTILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag if workstation has a cash drawer for
Vouchers, Cheques and Cash payments.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'CSDRAWER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag if workstation has EFT equipment for Card Discount, Cheques and Credit Cards' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'USEEFT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of printer - A=A4, P=Pos Printer' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL', @level2type=N'COLUMN',@level2name=N'PRNTTYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W S O C T L   =   Workstation Outlet Control File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WSOCTL'
END
GO

