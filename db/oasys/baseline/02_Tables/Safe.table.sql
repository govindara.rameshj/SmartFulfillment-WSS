﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Safe]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table Safe'
CREATE TABLE [dbo].[Safe](
	[PeriodID] [int] NOT NULL,
	[PeriodDate] [date] NOT NULL,
	[UserID1] [int] NOT NULL,
	[UserID2] [int] NOT NULL,
	[LastAmended] [datetime] NOT NULL,
	[IsClosed] [bit] NULL,
	[SafeChecked] [bit] NULL,
	[EndOfDayCheckDone] [bit] NOT NULL,
	[EndOfDayCheckCompleted] [datetime] NULL,
	[EndOfDayCheckManagerID] [int] NULL,
	[EndOfDayCheckWitnessID] [int] NULL,
	[EndOfDayCheckLockedFrom] [datetime] NULL,
	[EndOfDayCheckLockedTo] [datetime] NULL,
 CONSTRAINT [PK_Safe] PRIMARY KEY CLUSTERED 
(
	[PeriodID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Safe', @level2type=N'COLUMN',@level2name=N'PeriodID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Safe', @level2type=N'COLUMN',@level2name=N'PeriodDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee Identifier 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Safe', @level2type=N'COLUMN',@level2name=N'UserID1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee Identifier 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Safe', @level2type=N'COLUMN',@level2name=N'UserID2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Date and time this record was amended' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Safe', @level2type=N'COLUMN',@level2name=N'LastAmended'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If Closed false if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Safe', @level2type=N'COLUMN',@level2name=N'IsClosed'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S A F E = Safe File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Safe'
END
GO

