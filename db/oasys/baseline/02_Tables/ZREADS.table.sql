﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ZREADS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ZREADS'
CREATE TABLE [dbo].[ZREADS](
	[TYPE] [char](2) NOT NULL,
	[STYP] [char](3) NOT NULL,
	[COUN] [decimal](5, 0) NOT NULL,
	[AMNT] [decimal](9, 2) NOT NULL,
	[WSID] [char](2) NOT NULL,
 CONSTRAINT [PK_ZREADS] PRIMARY KEY CLUSTERED 
(
	[WSID] ASC,
	[TYPE] ASC,
	[STYP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZREADS', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Sub-Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZREADS', @level2type=N'COLUMN',@level2name=N'STYP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZREADS', @level2type=N'COLUMN',@level2name=N'COUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZREADS', @level2type=N'COLUMN',@level2name=N'AMNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZREADS', @level2type=N'COLUMN',@level2name=N'WSID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Z R E A D S  =   PC Tills Z and X Reads   [path\ZREADS] ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZREADS'
END
GO

