﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ExternalRequests]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ExternalRequests'
CREATE TABLE [dbo].[ExternalRequests](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[RequestDetailsXml] [xml] NOT NULL,
	[Status] [int] NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[Source] [varchar](50) NOT NULL,
	[Error] [varchar](1000) NULL,
	[RetriesCount] [int] NOT NULL,
	[LastRetryDateTime] [datetime] NULL,
 CONSTRAINT [PK_ExternalRequests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'XML that contains request details, different for each type of request' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalRequests', @level2type=N'COLUMN',@level2name=N'RequestDetailsXml'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current status: 0 - New, 1 - Processed, 2 - Failed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalRequests', @level2type=N'COLUMN',@level2name=N'Status'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corresponds to operations supported by service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalRequests', @level2type=N'COLUMN',@level2name=N'Type'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes business action that led to request creation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalRequests', @level2type=N'COLUMN',@level2name=N'Source'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of error occured during request processing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalRequests', @level2type=N'COLUMN',@level2name=N'Error'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How much retries was made by ExternalRequestMonitor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalRequests', @level2type=N'COLUMN',@level2name=N'RetriesCount'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time of last retry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalRequests', @level2type=N'COLUMN',@level2name=N'LastRetryDateTime'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'REST requests to cloud services' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalRequests'
END
GO

