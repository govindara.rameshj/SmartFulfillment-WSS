﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WEBPAID]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table WEBPAID'
CREATE TABLE [dbo].[WEBPAID](
	[DATE1] [date] NULL,
	[TILL] [char](2) NULL,
	[TRAN] [char](4) NULL,
	[NUMB] [smallint] NULL,
	[TYPE] [decimal](3, 0) NULL,
	[AMNT] [decimal](9, 2) NULL,
	[CARD] [char](19) NULL,
	[EXDT] [char](4) NULL,
	[COPN] [char](6) NULL,
	[CLAS] [char](2) NULL,
	[AUTH] [char](9) NULL,
	[CKEY] [bit] NULL,
	[SUPV] [char](3) NULL,
	[POST] [char](8) NULL,
	[CKAC] [char](10) NULL,
	[CKSC] [char](6) NULL,
	[CKNO] [char](6) NULL,
	[DBRF] [bit] NULL,
	[SEQN] [char](4) NULL,
	[ISSU] [char](2) NULL,
	[ATYP] [char](1) NULL,
	[MERC] [char](15) NULL,
	[CBAM] [decimal](9, 2) NULL,
	[DIGC] [smallint] NULL,
	[ECOM] [bit] NULL,
	[CTYP] [char](30) NULL,
	[SPARE] [char](66) NULL
) ON [PRIMARY]
END
GO

