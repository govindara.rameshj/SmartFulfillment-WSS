﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SOQPAT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SOQPAT'
CREATE TABLE [dbo].[SOQPAT](
	[PATT] [char](4) NOT NULL,
	[YEAR] [date] NULL,
	[WPAR1] [decimal](3, 2) NULL,
	[WPAR2] [decimal](3, 2) NULL,
	[WPAR3] [decimal](3, 2) NULL,
	[WPAR4] [decimal](3, 2) NULL,
	[WPAR5] [decimal](3, 2) NULL,
	[WPAR6] [decimal](3, 2) NULL,
	[WPAR7] [decimal](3, 2) NULL,
	[WPAR8] [decimal](3, 2) NULL,
	[WPAR9] [decimal](3, 2) NULL,
	[WPAR10] [decimal](3, 2) NULL,
	[WPAR11] [decimal](3, 2) NULL,
	[WPAR12] [decimal](3, 2) NULL,
	[WPAR13] [decimal](3, 2) NULL,
	[WPAR14] [decimal](3, 2) NULL,
	[WPAR15] [decimal](3, 2) NULL,
	[WPAR16] [decimal](3, 2) NULL,
	[WPAR17] [decimal](3, 2) NULL,
	[WPAR18] [decimal](3, 2) NULL,
	[WPAR19] [decimal](3, 2) NULL,
	[WPAR20] [decimal](3, 2) NULL,
	[WPAR21] [decimal](3, 2) NULL,
	[WPAR22] [decimal](3, 2) NULL,
	[WPAR23] [decimal](3, 2) NULL,
	[WPAR24] [decimal](3, 2) NULL,
	[WPAR25] [decimal](3, 2) NULL,
	[WPAR26] [decimal](3, 2) NULL,
	[WPAR27] [decimal](3, 2) NULL,
	[WPAR28] [decimal](3, 2) NULL,
	[WPAR29] [decimal](3, 2) NULL,
	[WPAR30] [decimal](3, 2) NULL,
	[WPAR31] [decimal](3, 2) NULL,
	[WPAR32] [decimal](3, 2) NULL,
	[WPAR33] [decimal](3, 2) NULL,
	[WPAR34] [decimal](3, 2) NULL,
	[WPAR35] [decimal](3, 2) NULL,
	[WPAR36] [decimal](3, 2) NULL,
	[WPAR37] [decimal](3, 2) NULL,
	[WPAR38] [decimal](3, 2) NULL,
	[WPAR39] [decimal](3, 2) NULL,
	[WPAR40] [decimal](3, 2) NULL,
	[WPAR41] [decimal](3, 2) NULL,
	[WPAR42] [decimal](3, 2) NULL,
	[WPAR43] [decimal](3, 2) NULL,
	[WPAR44] [decimal](3, 2) NULL,
	[WPAR45] [decimal](3, 2) NULL,
	[WPAR46] [decimal](3, 2) NULL,
	[WPAR47] [decimal](3, 2) NULL,
	[WPAR48] [decimal](3, 2) NULL,
	[WPAR49] [decimal](3, 2) NULL,
	[WPAR50] [decimal](3, 2) NULL,
	[WPAR51] [decimal](3, 2) NULL,
	[WPAR52] [decimal](3, 2) NULL,
	[WPAR53] [decimal](3, 2) NULL,
	[SPARE] [char](99) NULL
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pattern Number - Key to file -
1000 to 1999 - seasonality. Ref from SOQSKU :SPAT
3000 to 3999 - Bank Holiday. Ref from SOQSKU :BPAT
5000 to 5999 - Promotional. Ref from SOQSKU :PPAT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'PATT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Year Pattern Applies to.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'YEAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR31'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR32'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR33'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR34'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR35'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR36'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR37'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR38'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR39'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR40'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR41'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR42'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR43'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR44'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR45'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR46'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR47'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR48'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR49'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR51'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR52'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Weekly parameters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'WPAR53'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S O Q P A T  =   SOQ Pattern File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SOQPAT'
END
GO

