﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TOSTEN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TOSTEN'
CREATE TABLE [dbo].[TOSTEN](
	[DSEQ] [char](2) NOT NULL,
	[TEND] [char](2) NOT NULL,
 CONSTRAINT [PK_TOSTEN] PRIMARY KEY CLUSTERED 
(
	[DSEQ] ASC,
	[TEND] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSTEN', @level2type=N'COLUMN',@level2name=N'DSEQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSTEN', @level2type=N'COLUMN',@level2name=N'TEND'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'T O S T E N    =  PC Till TOS Tenders allowed file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TOSTEN'
END
GO

