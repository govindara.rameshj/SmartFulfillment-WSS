﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLTOTS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLTOTS'
CREATE TABLE [dbo].[DLTOTS](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[CASH] [char](3) NULL,
	[TIME] [char](6) NULL,
	[SUPV] [char](3) NULL,
	[TCOD] [char](2) NULL,
	[OPEN] [smallint] NULL,
	[MISC] [smallint] NULL,
	[DESCR] [char](20) NULL,
	[ORDN] [char](6) NULL,
	[ACCT] [bit] NOT NULL,
	[VOID] [bit] NOT NULL,
	[VSUP] [char](3) NULL,
	[TMOD] [bit] NOT NULL,
	[PROC] [bit] NOT NULL,
	[DOCN] [char](8) NULL,
	[SUSE] [bit] NOT NULL,
	[STOR] [char](3) NULL,
	[MERC] [decimal](9, 2) NULL,
	[NMER] [decimal](9, 2) NULL,
	[TAXA] [decimal](9, 2) NULL,
	[DISC] [decimal](9, 2) NULL,
	[DSUP] [char](3) NULL,
	[TOTL] [decimal](9, 2) NULL,
	[ACCN] [char](6) NULL,
	[CARD] [char](6) NULL,
	[AUPD] [bit] NOT NULL,
	[BACK] [bit] NOT NULL,
	[ICOM] [bit] NOT NULL,
	[IEMP] [bit] NOT NULL,
	[RCAS] [char](3) NULL,
	[RSUP] [char](3) NULL,
	[VATR1] [decimal](5, 3) NULL,
	[VATR2] [decimal](5, 3) NULL,
	[VATR3] [decimal](5, 3) NULL,
	[VATR4] [decimal](5, 3) NULL,
	[VATR5] [decimal](5, 3) NULL,
	[VATR6] [decimal](5, 3) NULL,
	[VATR7] [decimal](5, 3) NULL,
	[VATR8] [decimal](5, 3) NULL,
	[VATR9] [decimal](5, 3) NULL,
	[VSYM1] [char](1) NULL,
	[VSYM2] [char](1) NULL,
	[VSYM3] [char](1) NULL,
	[VSYM4] [char](1) NULL,
	[VSYM5] [char](1) NULL,
	[VSYM6] [char](1) NULL,
	[VSYM7] [char](1) NULL,
	[VSYM8] [char](1) NULL,
	[VSYM9] [char](1) NULL,
	[XVAT1] [decimal](9, 2) NULL,
	[XVAT2] [decimal](9, 2) NULL,
	[XVAT3] [decimal](9, 2) NULL,
	[XVAT4] [decimal](9, 2) NULL,
	[XVAT5] [decimal](9, 2) NULL,
	[XVAT6] [decimal](9, 2) NULL,
	[XVAT7] [decimal](9, 2) NULL,
	[XVAT8] [decimal](9, 2) NULL,
	[XVAT9] [decimal](9, 2) NULL,
	[VATV1] [decimal](9, 2) NULL,
	[VATV2] [decimal](9, 2) NULL,
	[VATV3] [decimal](9, 2) NULL,
	[VATV4] [decimal](9, 2) NULL,
	[VATV5] [decimal](9, 2) NULL,
	[VATV6] [decimal](9, 2) NULL,
	[VATV7] [decimal](9, 2) NULL,
	[VATV8] [decimal](9, 2) NULL,
	[VATV9] [decimal](9, 2) NULL,
	[PARK] [bit] NOT NULL,
	[RMAN] [char](3) NULL,
	[TOCD] [char](2) NULL,
	[PKRC] [bit] NOT NULL,
	[REMO] [bit] NOT NULL,
	[GTPN] [decimal](3, 0) NULL,
	[CCRD] [char](9) NULL,
	[SSTA] [char](2) NULL,
	[SSEQ] [char](4) NULL,
	[CBBU] [char](8) NULL,
	[CARD_NO] [char](19) NULL,
	[RTI] [char](1) NOT NULL,
	[ReceivedDate] [datetime] NULL,
	[SourceTranId] [varchar](50) NULL,
	[SourceTranNumber] [varchar](20) NULL,
	[Source] [varchar](10) NULL,
	[ReceiptBarcode] [varchar](30) NULL,
 CONSTRAINT [PK_DLTOTS] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id - Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number from PC Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'CASH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time of Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'TIME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supervisor Cashier Number - Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W08 Transaction Code  SA SC RF RC CO CC M+ M- OD RL
C+ C- XR ZR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'TCOD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OD Reason code occurrence number in RETOPT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'OPEN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Reason code occurrence Number in RETOPT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'MISC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Number - If from Project Sales' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'ORDN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = This is an Account Sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'ACCT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = This Transaction was Voided' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VOID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Void Supervisor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Training Mode Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'TMOD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Updated by RSBUPD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'PROC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'External Document number - if required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'DOCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Supervisor used SOMEWHERE in this tran' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'SUSE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store/Dept number if Employee Discount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'STOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Merchandise Amount including VAT if inclusive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'MERC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Non-Merch - for M+ and M- Transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'NMER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tax Amount - Both inclusive and exclusive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'TAXA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount Amount for this Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'DISC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount Supervisor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'DSUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Sale Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'TOTL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Account Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'ACCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Account Card Holder Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'CARD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Account Update is Complete
W05   ON = Post Code Collection Active (Forget above)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'AUPD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02 ON = Sale or Refund Transaction from DDC/HDC Orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'BACK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 ON = Transaction Complete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'ICOM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W06 On = Transaction was Discounted using the
W06      Employee Discount ONLY (DL:ESPD)
W06 Off= Transaction was Discounted using All Discounts,
W06      including Secondary Employee Discount
W06      (DL:ESEV), if applicable, but excluding Prima' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'IEMP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Refund Cashier Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'RCAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Refund Authorisation Supervisor Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'RSUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Rates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATR9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vat Symbol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VSYM9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent Ex-VAT value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'XVAT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Equivalent VAT Value of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'VATV9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W09    "Y" = Transaction Parked(Suspended)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'PARK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Refund Authorisation Manager Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'RMAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W11 Tender Override Code used by manager' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'TOCD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W12    Indicate Parked Transaction has been recalled' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'PKRC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W13    Indicate Transaction was on/off line' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'REMO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W13    Number Of Gift Tokens Printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'GTPN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W16    Colleague Number
*W14    Zero if not a colleague discount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'CCRD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current save status of DLTOTS & sub records
   * 10 - Creating History File
   * 20 - Saving Line Items (DLLINE)
   * 30 - Saving Payments (DLPAID)
   * 40 - Saving Line Events (DLEVNT)
   * 50 - Saving Customer (DLCUST)
   * 60 - Saving Price Promise Lines
   * 70 - Saving Price Promise Customer
   * 80 - Saving Return Items Details
   * 90 - Saving Missing EANs
   * 99 - Save Completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'SSTA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used with SSTA to hold the sequence
   * being saved, ie. Line Number, Payment or
   * Event' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'SSEQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates updated by CBBUPD - uses RO:DOLR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'CBBU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount Card Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'CARD_NO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L T O T S  =   Daily Reformatted Till Totals Records ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLTOTS'
END
GO

