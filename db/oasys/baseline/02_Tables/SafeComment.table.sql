﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SafeComment]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SafeComment'
CREATE TABLE [dbo].[SafeComment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessProcessID] [int] NOT NULL,
	[SafePeriodID] [int] NOT NULL,
	[BagType] [nchar](1) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_SafeComment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

