﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CUSMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CUSMAS'
CREATE TABLE [dbo].[CUSMAS](
	[CUST] [char](10) NOT NULL,
	[ALPH] [char](10) NULL,
	[TITL] [char](4) NULL,
	[INIT] [char](6) NULL,
	[SNAM] [char](30) NULL,
	[ADDR1] [char](30) NULL,
	[ADDR2] [char](30) NULL,
	[ADDR3] [char](30) NULL,
	[ADDR4] [char](30) NULL,
	[POST] [char](8) NULL,
	[PHON1] [char](15) NULL,
	[PHON2] [char](15) NULL,
	[PHON3] [char](15) NULL,
	[ANSW1] [char](2) NULL,
	[ANSW2] [char](2) NULL,
	[ANSW3] [char](2) NULL,
	[ANSW4] [char](2) NULL,
	[ANSW5] [char](2) NULL,
	[ANSW6] [char](2) NULL,
	[ANSW7] [char](2) NULL,
	[ANSW8] [char](2) NULL,
	[ANSW9] [char](2) NULL,
	[ANSW10] [char](2) NULL,
	[DELI] [char](75) NULL,
 CONSTRAINT [PK_CUSMAS] PRIMARY KEY CLUSTERED 
(
	[CUST] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key - From PHONE number one' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'CUST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alpha Key (1st 10 of Surname)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ALPH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Title (Mr, Mrs, Dr. Etc.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'TITL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Initials' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'INIT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'SNAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ADDR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ADDR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ADDR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Address Line 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ADDR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Post Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'POST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Telephone 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'PHON1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Telephone 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'PHON2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Telephone 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'PHON3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Answers to Lifestyle questions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'ANSW10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Helpful delivery instructions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS', @level2type=N'COLUMN',@level2name=N'DELI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C U S M A S  =   Customer Master File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CUSMAS'
END
GO

