﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PVPAID]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PVPAID'
CREATE TABLE [dbo].[PVPAID](
	[DATE1] [char](8) NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [char](4) NOT NULL,
	[TYPE] [char](2) NULL,
	[AMNT] [char](10) NULL,
	[PIVT] [char](1) NULL,
 CONSTRAINT [PK_PVPAID] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVPAID', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Designated number of Pivotal till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVPAID', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVPAID', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVPAID', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type  - Retopt Occurrence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVPAID', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Amount - Opposite sign of Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVPAID', @level2type=N'COLUMN',@level2name=N'AMNT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''N'' from Pivotal. Updated to ''Y'' by CTS processing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVPAID', @level2type=N'COLUMN',@level2name=N'PIVT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P V P A I D  =   Daily Reformatted Till Tender Type Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVPAID'
END
GO

