﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CBSCTL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CBSCTL'
CREATE TABLE [dbo].[CBSCTL](
	[DATE1] [date] NOT NULL,
	[COMM] [bit] NOT NULL,
	[DONE] [bit] NOT NULL,
	[TEND1] [decimal](9, 2) NULL,
	[TEND2] [decimal](9, 2) NULL,
	[TEND3] [decimal](9, 2) NULL,
	[TEND4] [decimal](9, 2) NULL,
	[TEND5] [decimal](9, 2) NULL,
	[TEND6] [decimal](9, 2) NULL,
	[TEND7] [decimal](9, 2) NULL,
	[TEND8] [decimal](9, 2) NULL,
	[TEND9] [decimal](9, 2) NULL,
	[TEND10] [decimal](9, 2) NULL,
	[TEND11] [decimal](9, 2) NULL,
	[TEND12] [decimal](9, 2) NULL,
	[TEND13] [decimal](9, 2) NULL,
	[TEND14] [decimal](9, 2) NULL,
	[TEND15] [decimal](9, 2) NULL,
	[TEND16] [decimal](9, 2) NULL,
	[TEND17] [decimal](9, 2) NULL,
	[TEND18] [decimal](9, 2) NULL,
	[TEND19] [decimal](9, 2) NULL,
	[TEND20] [decimal](9, 2) NULL,
	[SLIP1] [char](6) NULL,
	[SLIP2] [char](6) NULL,
	[SLIP3] [char](6) NULL,
	[SLIP4] [char](6) NULL,
	[SLIP5] [char](6) NULL,
	[SLIP6] [char](6) NULL,
	[SLIP7] [char](6) NULL,
	[SLIP8] [char](6) NULL,
	[SLIP9] [char](6) NULL,
	[SLIP10] [char](6) NULL,
	[SLIP11] [char](6) NULL,
	[SLIP12] [char](6) NULL,
	[SLIP13] [char](6) NULL,
	[SLIP14] [char](6) NULL,
	[SLIP15] [char](6) NULL,
	[SLIP16] [char](6) NULL,
	[SLIP17] [char](6) NULL,
	[SLIP18] [char](6) NULL,
	[SLIP19] [char](6) NULL,
	[SLIP20] [char](6) NULL,
	[TB50] [decimal](9, 2) NULL,
	[TB20] [decimal](9, 2) NULL,
	[TB10] [decimal](9, 2) NULL,
	[TB05] [decimal](9, 2) NULL,
	[TB01] [decimal](9, 2) NULL,
	[TBCO] [decimal](9, 2) NULL,
	[ZTIL1] [char](6) NULL,
	[ZTIL2] [char](6) NULL,
	[ZTIL3] [char](6) NULL,
	[ZTIL4] [char](6) NULL,
	[ZTIL5] [char](6) NULL,
	[ZTIL6] [char](6) NULL,
	[ZTIL7] [char](6) NULL,
	[ZTIL8] [char](6) NULL,
	[ZTIL9] [char](6) NULL,
	[ZTIL10] [char](6) NULL,
	[ZTIL11] [char](6) NULL,
	[ZTIL12] [char](6) NULL,
	[ZTIL13] [char](6) NULL,
	[ZTIL14] [char](6) NULL,
	[ZTIL15] [char](6) NULL,
	[ZTIL16] [char](6) NULL,
	[ZTIL17] [char](6) NULL,
	[ZTIL18] [char](6) NULL,
	[ZTIL19] [char](6) NULL,
	[ZTIL20] [char](6) NULL,
	[ZTRN1] [char](4) NULL,
	[ZTRN2] [char](4) NULL,
	[ZTRN3] [char](4) NULL,
	[ZTRN4] [char](4) NULL,
	[ZTRN5] [char](4) NULL,
	[ZTRN6] [char](4) NULL,
	[ZTRN7] [char](4) NULL,
	[ZTRN8] [char](4) NULL,
	[ZTRN9] [char](4) NULL,
	[ZTRN10] [char](4) NULL,
	[ZTRN11] [char](4) NULL,
	[ZTRN12] [char](4) NULL,
	[ZTRN13] [char](4) NULL,
	[ZTRN14] [char](4) NULL,
	[ZTRN15] [char](4) NULL,
	[ZTRN16] [char](4) NULL,
	[ZTRN17] [char](4) NULL,
	[ZTRN18] [char](4) NULL,
	[ZTRN19] [char](4) NULL,
	[ZTRN20] [char](4) NULL,
	[ZNET1] [decimal](9, 2) NULL,
	[ZNET2] [decimal](9, 2) NULL,
	[ZNET3] [decimal](9, 2) NULL,
	[ZNET4] [decimal](9, 2) NULL,
	[ZNET5] [decimal](9, 2) NULL,
	[ZNET6] [decimal](9, 2) NULL,
	[ZNET7] [decimal](9, 2) NULL,
	[ZNET8] [decimal](9, 2) NULL,
	[ZNET9] [decimal](9, 2) NULL,
	[ZNET10] [decimal](9, 2) NULL,
	[ZNET11] [decimal](9, 2) NULL,
	[ZNET12] [decimal](9, 2) NULL,
	[ZNET13] [decimal](9, 2) NULL,
	[ZNET14] [decimal](9, 2) NULL,
	[ZNET15] [decimal](9, 2) NULL,
	[ZNET16] [decimal](9, 2) NULL,
	[ZNET17] [decimal](9, 2) NULL,
	[ZNET18] [decimal](9, 2) NULL,
	[ZNET19] [decimal](9, 2) NULL,
	[ZNET20] [decimal](9, 2) NULL,
	[SALV1] [decimal](9, 2) NULL,
	[SALV2] [decimal](9, 2) NULL,
	[SALV3] [decimal](9, 2) NULL,
	[SALV4] [decimal](9, 2) NULL,
	[SALV5] [decimal](9, 2) NULL,
	[SALV6] [decimal](9, 2) NULL,
	[SALV7] [decimal](9, 2) NULL,
	[SALV8] [decimal](9, 2) NULL,
	[SALV9] [decimal](9, 2) NULL,
	[SALV10] [decimal](9, 2) NULL,
	[SALV11] [decimal](9, 2) NULL,
	[SALV12] [decimal](9, 2) NULL,
	[SALV13] [decimal](9, 2) NULL,
	[SALV14] [decimal](9, 2) NULL,
	[SALV15] [decimal](9, 2) NULL,
	[SALV16] [decimal](9, 2) NULL,
	[SALV17] [decimal](9, 2) NULL,
	[SALV18] [decimal](9, 2) NULL,
	[SALV19] [decimal](9, 2) NULL,
	[SALV20] [decimal](9, 2) NULL,
	[SALV21] [decimal](9, 2) NULL,
	[SALV22] [decimal](9, 2) NULL,
	[SALV23] [decimal](9, 2) NULL,
	[SALV24] [decimal](9, 2) NULL,
	[SALV25] [decimal](9, 2) NULL,
	[SALV26] [decimal](9, 2) NULL,
	[SALV27] [decimal](9, 2) NULL,
	[SALV28] [decimal](9, 2) NULL,
	[SALV29] [decimal](9, 2) NULL,
	[SALV30] [decimal](9, 2) NULL,
	[SALV31] [decimal](9, 2) NULL,
	[SALV32] [decimal](9, 2) NULL,
	[SALV33] [decimal](9, 2) NULL,
	[SALV34] [decimal](9, 2) NULL,
	[SALV35] [decimal](9, 2) NULL,
	[SALV36] [decimal](9, 2) NULL,
	[SALV37] [decimal](9, 2) NULL,
	[SALV38] [decimal](9, 2) NULL,
	[SALV39] [decimal](9, 2) NULL,
	[SALV40] [decimal](9, 2) NULL,
	[SALV41] [decimal](9, 2) NULL,
	[SALV42] [decimal](9, 2) NULL,
	[SALV43] [decimal](9, 2) NULL,
	[SALV44] [decimal](9, 2) NULL,
	[SALV45] [decimal](9, 2) NULL,
	[SALV46] [decimal](9, 2) NULL,
	[SALV47] [decimal](9, 2) NULL,
	[SALV48] [decimal](9, 2) NULL,
	[SALV49] [decimal](9, 2) NULL,
	[SALV50] [decimal](9, 2) NULL,
	[SALV51] [decimal](9, 2) NULL,
	[SALV52] [decimal](9, 2) NULL,
	[SALV53] [decimal](9, 2) NULL,
	[SALV54] [decimal](9, 2) NULL,
	[SALV55] [decimal](9, 2) NULL,
	[SALV56] [decimal](9, 2) NULL,
	[SALV57] [decimal](9, 2) NULL,
	[SALV58] [decimal](9, 2) NULL,
	[SALV59] [decimal](9, 2) NULL,
	[SALV60] [decimal](9, 2) NULL,
	[SALV61] [decimal](9, 2) NULL,
	[SALV62] [decimal](9, 2) NULL,
	[SALV63] [decimal](9, 2) NULL,
	[SALV64] [decimal](9, 2) NULL,
	[SALV65] [decimal](9, 2) NULL,
	[SALV66] [decimal](9, 2) NULL,
	[SALV67] [decimal](9, 2) NULL,
	[SALV68] [decimal](9, 2) NULL,
	[SALV69] [decimal](9, 2) NULL,
	[SALV70] [decimal](9, 2) NULL,
	[SALV71] [decimal](9, 2) NULL,
	[SALV72] [decimal](9, 2) NULL,
	[SALV73] [decimal](9, 2) NULL,
	[SALV74] [decimal](9, 2) NULL,
	[SALV75] [decimal](9, 2) NULL,
	[SALV76] [decimal](9, 2) NULL,
	[SALV77] [decimal](9, 2) NULL,
	[SALV78] [decimal](9, 2) NULL,
	[SALV79] [decimal](9, 2) NULL,
	[SALV80] [decimal](9, 2) NULL,
	[SALV81] [decimal](9, 2) NULL,
	[SALV82] [decimal](9, 2) NULL,
	[SALV83] [decimal](9, 2) NULL,
	[SALV84] [decimal](9, 2) NULL,
	[SALV85] [decimal](9, 2) NULL,
	[SALV86] [decimal](9, 2) NULL,
	[SALV87] [decimal](9, 2) NULL,
	[SALV88] [decimal](9, 2) NULL,
	[SALV89] [decimal](9, 2) NULL,
	[SALV90] [decimal](9, 2) NULL,
	[SALV91] [decimal](9, 2) NULL,
	[SALV92] [decimal](9, 2) NULL,
	[SALV93] [decimal](9, 2) NULL,
	[SALV94] [decimal](9, 2) NULL,
	[SALV95] [decimal](9, 2) NULL,
	[SALV96] [decimal](9, 2) NULL,
	[SALV97] [decimal](9, 2) NULL,
	[SALV98] [decimal](9, 2) NULL,
	[SALV99] [decimal](9, 2) NULL,
	[COPV1] [decimal](9, 2) NULL,
	[COPV2] [decimal](9, 2) NULL,
	[COPV3] [decimal](9, 2) NULL,
	[COPV4] [decimal](9, 2) NULL,
	[COPV5] [decimal](9, 2) NULL,
	[COPV6] [decimal](9, 2) NULL,
	[COPV7] [decimal](9, 2) NULL,
	[COPV8] [decimal](9, 2) NULL,
	[COPV9] [decimal](9, 2) NULL,
	[COPV10] [decimal](9, 2) NULL,
	[COPV11] [decimal](9, 2) NULL,
	[COPV12] [decimal](9, 2) NULL,
	[COPV13] [decimal](9, 2) NULL,
	[COPV14] [decimal](9, 2) NULL,
	[COPV15] [decimal](9, 2) NULL,
	[COPV16] [decimal](9, 2) NULL,
	[COPV17] [decimal](9, 2) NULL,
	[COPV18] [decimal](9, 2) NULL,
	[COPV19] [decimal](9, 2) NULL,
	[COPV20] [decimal](9, 2) NULL,
	[COPV21] [decimal](9, 2) NULL,
	[COPV22] [decimal](9, 2) NULL,
	[COPV23] [decimal](9, 2) NULL,
	[COPV24] [decimal](9, 2) NULL,
	[COPV25] [decimal](9, 2) NULL,
	[COPV26] [decimal](9, 2) NULL,
	[COPV27] [decimal](9, 2) NULL,
	[COPV28] [decimal](9, 2) NULL,
	[COPV29] [decimal](9, 2) NULL,
	[COPV30] [decimal](9, 2) NULL,
	[COPV31] [decimal](9, 2) NULL,
	[COPV32] [decimal](9, 2) NULL,
	[COPV33] [decimal](9, 2) NULL,
	[COPV34] [decimal](9, 2) NULL,
	[COPV35] [decimal](9, 2) NULL,
	[COPV36] [decimal](9, 2) NULL,
	[COPV37] [decimal](9, 2) NULL,
	[COPV38] [decimal](9, 2) NULL,
	[COPV39] [decimal](9, 2) NULL,
	[COPV40] [decimal](9, 2) NULL,
	[COPV41] [decimal](9, 2) NULL,
	[COPV42] [decimal](9, 2) NULL,
	[COPV43] [decimal](9, 2) NULL,
	[COPV44] [decimal](9, 2) NULL,
	[COPV45] [decimal](9, 2) NULL,
	[COPV46] [decimal](9, 2) NULL,
	[COPV47] [decimal](9, 2) NULL,
	[COPV48] [decimal](9, 2) NULL,
	[COPV49] [decimal](9, 2) NULL,
	[COPV50] [decimal](9, 2) NULL,
	[COPV51] [decimal](9, 2) NULL,
	[COPV52] [decimal](9, 2) NULL,
	[COPV53] [decimal](9, 2) NULL,
	[COPV54] [decimal](9, 2) NULL,
	[COPV55] [decimal](9, 2) NULL,
	[COPV56] [decimal](9, 2) NULL,
	[COPV57] [decimal](9, 2) NULL,
	[COPV58] [decimal](9, 2) NULL,
	[COPV59] [decimal](9, 2) NULL,
	[COPV60] [decimal](9, 2) NULL,
	[COPV61] [decimal](9, 2) NULL,
	[COPV62] [decimal](9, 2) NULL,
	[COPV63] [decimal](9, 2) NULL,
	[COPV64] [decimal](9, 2) NULL,
	[COPV65] [decimal](9, 2) NULL,
	[COPV66] [decimal](9, 2) NULL,
	[COPV67] [decimal](9, 2) NULL,
	[COPV68] [decimal](9, 2) NULL,
	[COPV69] [decimal](9, 2) NULL,
	[COPV70] [decimal](9, 2) NULL,
	[COPV71] [decimal](9, 2) NULL,
	[COPV72] [decimal](9, 2) NULL,
	[COPV73] [decimal](9, 2) NULL,
	[COPV74] [decimal](9, 2) NULL,
	[COPV75] [decimal](9, 2) NULL,
	[COPV76] [decimal](9, 2) NULL,
	[COPV77] [decimal](9, 2) NULL,
	[COPV78] [decimal](9, 2) NULL,
	[COPV79] [decimal](9, 2) NULL,
	[COPV80] [decimal](9, 2) NULL,
	[COPV81] [decimal](9, 2) NULL,
	[COPV82] [decimal](9, 2) NULL,
	[COPV83] [decimal](9, 2) NULL,
	[COPV84] [decimal](9, 2) NULL,
	[COPV85] [decimal](9, 2) NULL,
	[COPV86] [decimal](9, 2) NULL,
	[COPV87] [decimal](9, 2) NULL,
	[COPV88] [decimal](9, 2) NULL,
	[COPV89] [decimal](9, 2) NULL,
	[COPV90] [decimal](9, 2) NULL,
	[COPV91] [decimal](9, 2) NULL,
	[COPV92] [decimal](9, 2) NULL,
	[COPV93] [decimal](9, 2) NULL,
	[COPV94] [decimal](9, 2) NULL,
	[COPV95] [decimal](9, 2) NULL,
	[COPV96] [decimal](9, 2) NULL,
	[COPV97] [decimal](9, 2) NULL,
	[COPV98] [decimal](9, 2) NULL,
	[COPV99] [decimal](9, 2) NULL,
	[XSLP1] [char](6) NULL,
	[XSLP2] [char](6) NULL,
	[XSLP3] [char](6) NULL,
	[CSLP1] [char](6) NULL,
	[CSLP2] [char](6) NULL,
	[CSLP3] [char](6) NULL,
	[CSLP4] [char](6) NULL,
	[CSLP5] [char](6) NULL,
	[CSLP6] [char](6) NULL,
	[CSLP7] [char](6) NULL,
	[CSLP8] [char](6) NULL,
	[CSLP9] [char](6) NULL,
	[CSLP10] [char](6) NULL,
	[EDSB1] [decimal](9, 2) NULL,
	[EDSB2] [decimal](9, 2) NULL,
	[EDSB3] [decimal](9, 2) NULL,
	[EDSB4] [decimal](9, 2) NULL,
	[EDSB5] [decimal](9, 2) NULL,
	[EDSB6] [decimal](9, 2) NULL,
	[EDSB7] [decimal](9, 2) NULL,
	[EDSB8] [decimal](9, 2) NULL,
	[EDSB9] [decimal](9, 2) NULL,
	[EDSB10] [decimal](9, 2) NULL,
	[EDSB11] [decimal](9, 2) NULL,
	[EDSB12] [decimal](9, 2) NULL,
	[EDSB13] [decimal](9, 2) NULL,
	[EDSB14] [decimal](9, 2) NULL,
	[EDSB15] [decimal](9, 2) NULL,
	[EDSB16] [decimal](9, 2) NULL,
	[EDSB17] [decimal](9, 2) NULL,
	[EDSB18] [decimal](9, 2) NULL,
	[EDSB19] [decimal](9, 2) NULL,
	[EDSB20] [decimal](9, 2) NULL,
	[TB100] [decimal](9, 2) NULL,
	[TB02] [decimal](9, 2) NULL,
	[PUOK] [bit] NOT NULL,
	[SPARE] [char](187) NULL,
 CONSTRAINT [PK_CBSCTL] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Prepared for comms      - Set by TOBADB *W04' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COMM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Ok to Prepare for comms - Set by CBEDBR *W04' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'DONE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Banking of Tenders   01 - 20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TEND20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number by tender type (deposit slip)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SLIP20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 50 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TB50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 20 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TB20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 10 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TB10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of  5 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TB05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of  1 pound Notes/Coins' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TB01'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of all coins except 1 pound coins' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TBCO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (000000 = not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTIL20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction number of the z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZTRN20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net transactions value from z-read' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'ZNET20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV31'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV32'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV33'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV34'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV35'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV36'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV37'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV38'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV39'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV40'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV41'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV42'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV43'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV44'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV45'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV46'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV47'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV48'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV49'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV51'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV52'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV53'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV54'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV55'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV56'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV57'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV58'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV59'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV60'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV61'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV62'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV63'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV64'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV65'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV66'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV67'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV68'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV69'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV70'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV71'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV72'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV73'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV74'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV75'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV76'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV77'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV78'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV79'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV80'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV81'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV82'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV83'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV84'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV85'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV86'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV87'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV88'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV89'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV90'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV91'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV92'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV93'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV94'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV95'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV96'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV97'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV98'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W01Net sales  value for Classes 01 - 99 - No Longer Used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SALV99'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV31'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV32'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV33'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV34'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV35'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV36'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV37'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV38'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV39'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV40'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV41'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV42'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV43'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV44'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV45'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV46'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV47'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV48'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV49'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV51'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV52'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV53'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV54'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV55'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV56'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV57'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV58'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV59'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV60'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV61'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV62'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV63'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV64'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV65'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV66'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV67'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV68'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV69'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV70'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV71'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV72'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV73'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV74'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV75'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV76'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV77'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV78'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV79'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV80'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV81'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV82'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV83'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV84'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV85'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV86'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV87'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV88'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV89'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV90'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV91'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV92'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV93'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV94'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV95'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV96'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV97'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV98'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W02(O=1) thru (O=20)  - End of Day Safe balances per
*W02         tender type
*W02(O=21) thru (O=79) - Not Used
*W02(O=99) - Total Coupon Value for all classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'COPV99'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number per cash tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'XSLP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number per cash tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'XSLP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip number per cash tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'XSLP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cancelled slip numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'CSLP10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03End of Day Safe balances per tender type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'EDSB20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 100 pound notes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TB100'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of 2 pound coins' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'TB02'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup Entry Allowed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'PUOK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Reduced from A192)   *W07' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C B S C T L  =  Cash Balancing Daily Control Record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCTL'
END
GO

