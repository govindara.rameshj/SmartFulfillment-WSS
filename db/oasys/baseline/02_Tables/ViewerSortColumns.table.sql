﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ViewerSortColumns]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ViewerSortColumns'
CREATE TABLE [dbo].[ViewerSortColumns](
	[ID] [int] NOT NULL,
	[SortID] [int] NOT NULL,
	[ConfigID] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Ascending] [bit] NOT NULL,
	[SumIDs] [varchar](50) NULL,
	[CountIDs] [varchar](50) NULL,
	[MaxIDs] [varchar](50) NULL,
	[MinIDs] [varchar](50) NULL,
	[AveIDs] [varchar](50) NULL,
 CONSTRAINT [PK_ViewerSortColumns] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[SortID] ASC,
	[ConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerSortColumns', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to Table ViewerSort  Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerSortColumns', @level2type=N'COLUMN',@level2name=N'SortID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to Table ViewerConfig Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerSortColumns', @level2type=N'COLUMN',@level2name=N'ConfigID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sort Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerSortColumns', @level2type=N'COLUMN',@level2name=N'SortOrder'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if Ascending False if desending' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerSortColumns', @level2type=N'COLUMN',@level2name=N'Ascending'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'V I E W E R S O R T C O L U M N S = Report Viewer Sort Columns File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ViewerSortColumns'
END
GO

