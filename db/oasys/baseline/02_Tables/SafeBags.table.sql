﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SafeBags]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SafeBags'
CREATE TABLE [dbo].[SafeBags](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SealNumber] [char](20) NOT NULL,
	[Type] [char](1) NOT NULL,
	[State] [char](1) NOT NULL,
	[Value] [decimal](9, 2) NOT NULL,
	[InPeriodID] [int] NOT NULL,
	[InDate] [datetime] NOT NULL,
	[InUserID1] [int] NOT NULL,
	[InUserID2] [int] NOT NULL,
	[OutPeriodID] [int] NULL,
	[OutDate] [datetime] NULL,
	[OutUserID1] [int] NULL,
	[OutUserID2] [int] NULL,
	[AccountabilityType] [char](1) NULL,
	[AccountabilityID] [int] NULL,
	[FloatValue] [decimal](9, 2) NOT NULL,
	[PickupPeriodID] [int] NOT NULL,
	[RelatedBagId] [int] NULL,
	[Comments] [char](255) NULL,
	[FloatChecked] [bit] NULL,
	[FloatCheckedUserID1] [int] NULL,
	[FloatCheckedUserID2] [int] NULL,
	[CashDrop] [bit] NULL,
	[BagCollectionSlipNo] [varchar](13) NULL,
	[BagCollectionComment] [varchar](100) NULL,
 CONSTRAINT [PK_SafeBags] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Seal Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'SealNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Bag;
  * F = Float
  * B = Banking
  * P = Pickup
  * G = Gift Token' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'Type'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'State of Safe Bag;
 * R = Released
 * C = Cancelled
 * S  = Sealed
 * B =  BackToSafe
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'State'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value (Amount)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'Value'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What period were in.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'InPeriodID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'In date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'InDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'In Employee Id of User 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'InUserID1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'In Employee Id of User 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'InUserID2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Out Period Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'OutPeriodID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Out Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'OutDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Out Employee Id of User 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'OutUserID1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Out Employee Id of User 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'OutUserID2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accountability Type;
  * C = Cashier
  * T = Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'AccountabilityType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accountability Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'AccountabilityID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Float Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'FloatValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup Period Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'PickupPeriodID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags', @level2type=N'COLUMN',@level2name=N'Comments'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S A F E B A G S = Safe Bags File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBags'
END
GO

