﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLRCUS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLRCUS'
CREATE TABLE [dbo].[DLRCUS](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NAME] [char](30) NULL,
	[HNUM] [char](4) NULL,
	[POST] [char](8) NULL,
	[OSTR] [char](3) NULL,
	[ODAT] [date] NULL,
	[OTIL] [char](2) NULL,
	[OTRN] [char](4) NULL,
	[HNAM] [char](15) NULL,
	[HAD1] [char](30) NULL,
	[HAD2] [char](30) NULL,
	[HAD3] [char](30) NULL,
	[PHON] [char](15) NULL,
	[NUMB] [smallint] NOT NULL,
	[OVAL] [bit] NOT NULL,
	[OTEN] [decimal](3, 0) NULL,
	[RCOD] [char](2) NULL,
	[LABL] [bit] NOT NULL,
	[MOBP] [char](15) NULL,
	[PhoneNumberWork] [varchar](20) NULL,
	[EmailAddress] [varchar](100) NULL,
	[RTI] [char](1) NULL,
	[SPARE] [char](40) NULL,
	[WEBONO] [char](20) NULL,
 CONSTRAINT [PK_DLRCUS] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

