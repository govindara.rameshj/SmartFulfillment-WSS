﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StockMovement]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table StockMovement'
CREATE TABLE [dbo].[StockMovement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ActivityId] [int] NOT NULL,
	[EnteredDate] [date] NOT NULL,
	[EnteredTime] [time](7) NOT NULL,
	[Quantity] [int] NULL,
	[Value] [decimal](10, 2) NULL,
	[DataDate] [date] NULL,
	[DataTime] [time](7) NULL,
 CONSTRAINT [PK_Dashboard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier System Created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement', @level2type=N'COLUMN',@level2name=N'Id'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Activity Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement', @level2type=N'COLUMN',@level2name=N'ActivityId'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of the entry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement', @level2type=N'COLUMN',@level2name=N'EnteredDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time of the entry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement', @level2type=N'COLUMN',@level2name=N'EnteredTime'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement', @level2type=N'COLUMN',@level2name=N'Quantity'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement', @level2type=N'COLUMN',@level2name=N'Value'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Data entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement', @level2type=N'COLUMN',@level2name=N'DataDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time Data entered' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement', @level2type=N'COLUMN',@level2name=N'DataTime'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D A S H B O A R D D A T A = Dash Board Data File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StockMovement'
END
GO

