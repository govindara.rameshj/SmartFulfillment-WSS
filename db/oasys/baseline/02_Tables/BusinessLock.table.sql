﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BusinessLock]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table BusinessLock'
CREATE TABLE [dbo].[BusinessLock](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WorkStationID] [int] NOT NULL,
	[AssemblyName] [varchar](100) NOT NULL,
	[Parameters] [varchar](50) NULL,
	[DateTimeStamp] [datetime] NOT NULL,
 CONSTRAINT [PK_BusinessLock] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

