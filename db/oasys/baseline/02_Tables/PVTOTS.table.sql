﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PVTOTS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PVTOTS'
CREATE TABLE [dbo].[PVTOTS](
	[DATE1] [char](8) NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[CASH] [char](3) NULL,
	[TIME] [char](6) NULL,
	[TCOD] [char](2) NULL,
	[MISC] [char](2) NULL,
	[DESCR] [char](20) NULL,
	[ORDN] [char](6) NULL,
	[MERC] [char](10) NULL,
	[NMER] [char](10) NULL,
	[TAXA] [char](10) NULL,
	[DISC] [char](10) NULL,
	[TOTL] [char](10) NULL,
	[IEMP] [char](1) NULL,
	[PVEM] [char](10) NULL,
	[PIVT] [char](1) NULL,
 CONSTRAINT [PK_PVTOTS] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id - Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number from PC Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'CASH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time of Transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'TIME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction type:
 "SA" - Sale
 "EX" - Amendment
 "RF" - Refund
 "M+" -
 "M-" -' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'TCOD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc Inc Reason code occurrence number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'MISC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'ORDN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Merchandising amount including VAT    N8.2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'MERC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Non-merchandising amount for M+ and M- transaction codes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'NMER'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of tax' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'TAXA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of discounts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'DISC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total sale amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'TOTL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 "Y" if colleague discount   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'IEMP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Colleague discount amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'PVEM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''N'' from Pivotal. Updated to ''Y'' by CTS processing.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS', @level2type=N'COLUMN',@level2name=N'PIVT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P V T O T S  =   Daily Reformatted Till Totals Records (PIVOTAL)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVTOTS'
END
GO

