﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SYSOPT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SYSOPT'
CREATE TABLE [dbo].[SYSOPT](
	[FKEY] [char](2) NOT NULL,
	[STOR] [char](3) NULL,
	[SNAM] [char](35) NULL,
	[HNAM] [char](35) NULL,
	[HAD1] [char](35) NULL,
	[HAD2] [char](35) NULL,
	[HAD3] [char](35) NULL,
	[HAD4] [char](35) NULL,
	[HAD5] [char](35) NULL,
	[HPST] [char](8) NULL,
	[HPHN] [char](15) NULL,
	[HFAX] [char](15) NULL,
	[CVAT] [char](11) NULL,
	[MAST] [char](2) NULL,
	[HVIS] [char](2) NULL,
	[MSON] [bit] NOT NULL,
	[PDAY] [decimal](3, 0) NULL,
	[ADAY] [decimal](3, 0) NULL,
	[USTK] [bit] NOT NULL,
	[IHOS] [bit] NOT NULL,
	[COSL] [char](1) NULL,
	[ADDS] [char](1) NULL,
	[DCLR] [char](2) NULL,
	[IHHT] [bit] NOT NULL,
	[NCOU] [decimal](7, 0) NULL,
	[SELC] [decimal](3, 0) NULL,
	[PCAA] [bit] NOT NULL,
	[PCDA] [decimal](3, 0) NULL,
	[PSEL] [bit] NOT NULL,
	[BLKU] [bit] NOT NULL,
	[LPFL] [char](1) NULL,
	[CTYP] [char](5) NULL,
	[CGRP] [char](2) NULL,
	[LOCD] [char](1) NULL,
	[REMD] [char](1) NULL,
	[PATH] [char](8) NULL,
	[PCOP] [decimal](3, 0) NULL,
	[MICI] [bit] NOT NULL,
	[MACI] [bit] NOT NULL,
	[MACM] [decimal](3, 0) NULL,
	[MREC] [bit] NOT NULL,
	[RDAY] [decimal](3, 0) NULL,
	[PHIE] [bit] NOT NULL,
 CONSTRAINT [PK_SYSOPT] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key to file - Always set to 01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store number for this store - See STRMAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'STOR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'SNAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company Name     - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HNAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line one    - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HAD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line two    - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HAD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line three    - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HAD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line four    - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HAD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Line five    - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HAD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Post Code     - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HPST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone Number     - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HPHN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fax Number     - H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HFAX'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company VAT Number - xxx xxxx xx' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'CVAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UPTRAC Master Outlet Number (see WSOCTL)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'MAST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Highest Valid In Store Primary Function Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'HVIS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Employees are able to sign on to multiple
 workstations (outlets) at one time.
OFF = Employees are only able to be signed on to a
 single workstation (outlet) at any one time.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'MSON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Days User Passwords are valid.
After this number of days, the next time the
password is used, the system will prompt the
user to change the Password.

(0 - Passwords never become invalid)
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'PDAY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Days an Authorisation Password is valid.
After this number of days, the next time the
authorisation password is used, the system will
prompt the user to change the Password.

(0 - Passwords never become invalid)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'ADAY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = System is tracking STOCK ON HAND' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'USTK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Use Store address in RETOPT for Account
Payment address on Till Invoices.
Off = Use Head Office address in SYSOPT instead.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'IHOS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level Required to Access Cost Figures, etc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'COSL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Style - A = Uk
    B = Germany' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'ADDS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Day code of last reformated PIC data' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'DCLR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = HHT''s used for PIC
Off= Paper driven PIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'IHHT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PIC - No. of items counted during last reformat' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'NCOU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Every ''n'' sku numbers is flagged in the PIC file
for a LIS verification check.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'SELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y/N - Auto Apply Price Changes on or after eff date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'PCAA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If above Y, Number of days after effective date to
perform the auto apply of the price change.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'PCDA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y/N -  Print Shelf edge Labels for price changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'PSEL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = GSBLKU needed - set by SUBALL when run' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'BLKU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Local Pricing allowed - 
       Blank = Not Allowed
            S	= Store Generated Only
            H	= H/O   Generated Only
            B	= Both Store & H/O
            Used by PPMSKU and TIBALL to determine if updating
            of Local (Promotional) Price changes allowed.
            See STKMAS E02 changes for details.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'LPFL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'CTYP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company Group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'CGRP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Local Drive Letter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'LOCD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remote (file server) drive letter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'REMD'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Path name (same on both drives)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'PATH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of copies for PRPPRI - See PRECON program' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'PCOP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = In-Store maintenance of Minimum shelf allowed,
      but updated from Head Office if IM:MINI = 0.
Off= Value will always be replaced by H/O update.
      Cannot maintain in store.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'MICI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = In-Store maintenance of Maximum shelf allowed,
         but updated from Head Office if IM:MAXI = 0.
Off= Value will be replaced by H/O update, or set
         to Default (See :MACM below) if MACM > 0.
         In store maintenance not allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'MACI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Maximum Shelf capacity multiplier.
 Value used in calculation of Default Maximum shelf
 on setup. Default = (SO:MACM x IM:PACK)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'MACM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On  = Allow Receipt Maintenance for RDAY
          days after Entry of Receipt
Off = Allow Maint. Until COMM Flag is on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'MREC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Days to Allow Maintenance of Rec.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'RDAY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' ON = Product Hierarchy Link Files Need to be re-created, i.e. run program TIBRSH.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT', @level2type=N'COLUMN',@level2name=N'PHIE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S O P T   =   System Options Record ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSOPT'
END
GO

