﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[EventDetailOverride]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table EventDetailOverride'
CREATE TABLE [dbo].[EventDetailOverride](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventHeaderOverrideID] [int] NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[Quantity] [decimal](9, 2) NOT NULL,
	[ErosionPercentage] [decimal](9, 3) NULL,
	[SkuNormalSellingPrice] [decimal](9, 2) NULL,
	[SkuActualSellingPrice] [decimal](9, 2) NULL,
 CONSTRAINT [PK_EventDetailOverride] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

