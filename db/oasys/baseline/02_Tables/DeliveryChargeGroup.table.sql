﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeliveryChargeGroup]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    PRINT 'Creating table DeliveryChargeGroup'

    CREATE TABLE [dbo].[DeliveryChargeGroup]
    (
        Id int NOT NULL IDENTITY(1,1),
        [Group] char(1) NOT NULL UNIQUE,
        SkuNumber char(6) NOT NULL,
        IsDefault bit DEFAULT 0,
        CONSTRAINT PK_DeliveryChargeGroup PRIMARY KEY (Id, [GROUP])) ON [PRIMARY]        
END
GO
