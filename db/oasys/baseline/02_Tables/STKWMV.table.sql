﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[STKWMV]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table STKWMV'
CREATE TABLE [dbo].[STKWMV](
	[SKUN] [char](6) NOT NULL,
	[WKNO] [decimal](3, 0) NOT NULL,
	[WEDT] [date] NULL,
	[SALV] [decimal](9, 2) NULL,
	[SALU] [decimal](7, 0) NULL,
	[MREV] [decimal](9, 2) NULL,
	[MREQ] [decimal](7, 0) NULL,
	[MADV] [decimal](9, 2) NULL,
	[MADQ] [decimal](7, 0) NULL,
	[MIBV] [decimal](9, 2) NULL,
	[MIBQ] [decimal](7, 0) NULL,
	[MRTV] [decimal](9, 2) NULL,
	[MRTQ] [decimal](7, 0) NULL,
	[MBSV] [decimal](9, 2) NULL,
	[MBSQ] [decimal](7, 0) NULL,
	[MPVV] [decimal](9, 2) NULL,
	[MPCV] [decimal](9, 2) NULL,
	[MCCV] [decimal](9, 2) NULL,
	[MDRV] [decimal](9, 2) NULL,
	[MEPR] [decimal](9, 2) NULL,
	[METQ] [decimal](7, 0) NULL,
	[MARK] [decimal](9, 2) NULL,
 CONSTRAINT [PK_STKWMV] PRIMARY KEY CLUSTERED 
(
	[SKUN] ASC,
	[WKNO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number   (STKMAS)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Week Number in the Calendar Year (1 to 53)
    *
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'WKNO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Week Ending Date for this Week
    (Calendar Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'WEDT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value at Retail  ^IM:SALV(O=3)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'SALV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Units   ^IM:SALU(O=3)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'SALU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts value   IM:MREV(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MREV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL/Receipts quantity   IM:MREQ(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MREQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments value   IM:MADV(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MADV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adjustments quantity   IM:MADQ(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MADQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/OUT NET value   IM:MIBV(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MIBV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IBT In/Out NET quantity  IM:MIBQ(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MIBQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns value    IM:MRTV(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MRTV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Returns quantity   IM:MRTQ(O=2)
    *' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MRTQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer Markup/Down Value
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MBSV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bulk to single transfer quantity
        IM:MBSQ(' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MBSQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price violations value Calculated as
    Retail -' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MPVV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Change Markup/Down Value (Not Used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MPCV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cyclical count value   IM:MCCV(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MCCV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL adjustment value   IM:MDRV(O=2)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MDRV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending Price    IM:MSPR(O=1)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MEPR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending Quantity   IM:MSTQ(O=1)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'METQ'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Single Markup Value            RI:WTDM' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV', @level2type=N'COLUMN',@level2name=N'MARK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S T K W M V  =  Stock Item Weekly Stock Movements File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'STKWMV'
END
GO

