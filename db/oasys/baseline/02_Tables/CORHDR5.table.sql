﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CORHDR5]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CORHDR5'
CREATE TABLE [dbo].[CORHDR5](
	[NUMB] [char](6) NOT NULL,
	[Source] [char](2) NULL,
	[SourceOrderNumber] [char](20) NULL,
	[ExtendedLeadTime] [bit] NOT NULL,
	[DeliveryContactName] [char](50) NULL,
	[DeliveryContactPhone] [char](20) NULL,
	[CustomerAccountNo] [char](20) NOT NULL,
	[IsClickAndCollect] [bit] NOT NULL,
 CONSTRAINT [PK_CORHDR5] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

