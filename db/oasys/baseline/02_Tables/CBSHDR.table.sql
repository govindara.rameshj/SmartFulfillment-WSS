﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CBSHDR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CBSHDR'
CREATE TABLE [dbo].[CBSHDR](
	[FKEY] [char](2) NOT NULL,
	[BANK] [decimal](9, 2) NULL,
	[BVAR] [decimal](9, 2) NULL,
	[BVAI] [bit] NOT NULL,
	[FLOT] [decimal](7, 2) NULL,
	[CB50] [decimal](9, 2) NULL,
	[CB20] [decimal](9, 2) NULL,
	[CB10] [decimal](9, 2) NULL,
	[CB05] [decimal](9, 2) NULL,
	[CB01] [decimal](9, 2) NULL,
	[CBCO] [decimal](9, 2) NULL,
	[INCD1] [char](12) NULL,
	[INCD2] [char](12) NULL,
	[INCD3] [char](12) NULL,
	[INCD4] [char](12) NULL,
	[INCD5] [char](12) NULL,
	[INCD6] [char](12) NULL,
	[INCD7] [char](12) NULL,
	[INCD8] [char](12) NULL,
	[INCD9] [char](12) NULL,
	[INCD10] [char](12) NULL,
	[INCD11] [char](12) NULL,
	[INCD12] [char](12) NULL,
	[INCD13] [char](12) NULL,
	[INCD14] [char](12) NULL,
	[INCD15] [char](12) NULL,
	[INCD16] [char](12) NULL,
	[INCD17] [char](12) NULL,
	[INCD18] [char](12) NULL,
	[INCD19] [char](12) NULL,
	[INCD20] [char](12) NULL,
	[INCA1] [char](4) NULL,
	[INCA2] [char](4) NULL,
	[INCA3] [char](4) NULL,
	[INCA4] [char](4) NULL,
	[INCA5] [char](4) NULL,
	[INCA6] [char](4) NULL,
	[INCA7] [char](4) NULL,
	[INCA8] [char](4) NULL,
	[INCA9] [char](4) NULL,
	[INCA10] [char](4) NULL,
	[INCA11] [char](4) NULL,
	[INCA12] [char](4) NULL,
	[INCA13] [char](4) NULL,
	[INCA14] [char](4) NULL,
	[INCA15] [char](4) NULL,
	[INCA16] [char](4) NULL,
	[INCA17] [char](4) NULL,
	[INCA18] [char](4) NULL,
	[INCA19] [char](4) NULL,
	[INCA20] [char](4) NULL,
	[OUTD1] [char](12) NULL,
	[OUTD2] [char](12) NULL,
	[OUTD3] [char](12) NULL,
	[OUTD4] [char](12) NULL,
	[OUTD5] [char](12) NULL,
	[OUTD6] [char](12) NULL,
	[OUTD7] [char](12) NULL,
	[OUTD8] [char](12) NULL,
	[OUTD9] [char](12) NULL,
	[OUTD10] [char](12) NULL,
	[OUTD11] [char](12) NULL,
	[OUTD12] [char](12) NULL,
	[OUTD13] [char](12) NULL,
	[OUTD14] [char](12) NULL,
	[OUTD15] [char](12) NULL,
	[OUTD16] [char](12) NULL,
	[OUTD17] [char](12) NULL,
	[OUTD18] [char](12) NULL,
	[OUTD19] [char](12) NULL,
	[OUTD20] [char](12) NULL,
	[OUTA1] [char](4) NULL,
	[OUTA2] [char](4) NULL,
	[OUTA3] [char](4) NULL,
	[OUTA4] [char](4) NULL,
	[OUTA5] [char](4) NULL,
	[OUTA6] [char](4) NULL,
	[OUTA7] [char](4) NULL,
	[OUTA8] [char](4) NULL,
	[OUTA9] [char](4) NULL,
	[OUTA10] [char](4) NULL,
	[OUTA11] [char](4) NULL,
	[OUTA12] [char](4) NULL,
	[OUTA13] [char](4) NULL,
	[OUTA14] [char](4) NULL,
	[OUTA15] [char](4) NULL,
	[OUTA16] [char](4) NULL,
	[OUTA17] [char](4) NULL,
	[OUTA18] [char](4) NULL,
	[OUTA19] [char](4) NULL,
	[OUTA20] [char](4) NULL,
	[MISA1] [char](4) NULL,
	[MISA2] [char](4) NULL,
	[MISA3] [char](4) NULL,
	[MISA4] [char](4) NULL,
	[MISA5] [char](4) NULL,
	[MISA6] [char](4) NULL,
	[MISA7] [char](4) NULL,
	[MISA8] [char](4) NULL,
	[MISA9] [char](4) NULL,
	[MISA10] [char](4) NULL,
	[MISA11] [char](4) NULL,
	[MISA12] [char](4) NULL,
	[MISA13] [char](4) NULL,
	[MISA14] [char](4) NULL,
	[MISA15] [char](4) NULL,
	[MISA16] [char](4) NULL,
	[MISA17] [char](4) NULL,
	[MISA18] [char](4) NULL,
	[MISA19] [char](4) NULL,
	[MISA20] [char](4) NULL,
	[TEND1] [char](12) NULL,
	[TEND2] [char](12) NULL,
	[TEND3] [char](12) NULL,
	[TEND4] [char](12) NULL,
	[TEND5] [char](12) NULL,
	[TEND6] [char](12) NULL,
	[TEND7] [char](12) NULL,
	[TEND8] [char](12) NULL,
	[TEND9] [char](12) NULL,
	[TEND10] [char](12) NULL,
	[TEND11] [char](12) NULL,
	[TEND12] [char](12) NULL,
	[TEND13] [char](12) NULL,
	[TEND14] [char](12) NULL,
	[TEND15] [char](12) NULL,
	[TEND16] [char](12) NULL,
	[TEND17] [char](12) NULL,
	[TEND18] [char](12) NULL,
	[TEND19] [char](12) NULL,
	[TEND20] [char](12) NULL,
	[TNUM1] [decimal](3, 0) NULL,
	[TNUM2] [decimal](3, 0) NULL,
	[TNUM3] [decimal](3, 0) NULL,
	[TNUM4] [decimal](3, 0) NULL,
	[TNUM5] [decimal](3, 0) NULL,
	[TNUM6] [decimal](3, 0) NULL,
	[TNUM7] [decimal](3, 0) NULL,
	[TNUM8] [decimal](3, 0) NULL,
	[TNUM9] [decimal](3, 0) NULL,
	[TNUM10] [decimal](3, 0) NULL,
	[TNUM11] [decimal](3, 0) NULL,
	[TNUM12] [decimal](3, 0) NULL,
	[TNUM13] [decimal](3, 0) NULL,
	[TNUM14] [decimal](3, 0) NULL,
	[TNUM15] [decimal](3, 0) NULL,
	[TNUM16] [decimal](3, 0) NULL,
	[TNUM17] [decimal](3, 0) NULL,
	[TNUM18] [decimal](3, 0) NULL,
	[TNUM19] [decimal](3, 0) NULL,
	[TNUM20] [decimal](3, 0) NULL,
	[TDEP1] [bit] NOT NULL,
	[TDEP2] [bit] NOT NULL,
	[TDEP3] [bit] NOT NULL,
	[TDEP4] [bit] NOT NULL,
	[TDEP5] [bit] NOT NULL,
	[TDEP6] [bit] NOT NULL,
	[TDEP7] [bit] NOT NULL,
	[TDEP8] [bit] NOT NULL,
	[TDEP9] [bit] NOT NULL,
	[TDEP10] [bit] NOT NULL,
	[TDEP11] [bit] NOT NULL,
	[TDEP12] [bit] NOT NULL,
	[TDEP13] [bit] NOT NULL,
	[TDEP14] [bit] NOT NULL,
	[TDEP15] [bit] NOT NULL,
	[TDEP16] [bit] NOT NULL,
	[TDEP17] [bit] NOT NULL,
	[TDEP18] [bit] NOT NULL,
	[TDEP19] [bit] NOT NULL,
	[TDEP20] [bit] NOT NULL,
	[LWED] [date] NULL,
	[LWPF] [bit] NOT NULL,
	[NOTE] [bit] NOT NULL,
	[BC50] [decimal](9, 2) NULL,
	[BC20] [decimal](9, 2) NULL,
	[BC10] [decimal](9, 2) NULL,
	[BC05] [decimal](9, 2) NULL,
	[TAMT1] [decimal](9, 2) NULL,
	[TAMT2] [decimal](9, 2) NULL,
	[TAMT3] [decimal](9, 2) NULL,
	[TAMT4] [decimal](9, 2) NULL,
	[TAMT5] [decimal](9, 2) NULL,
	[TAMT6] [decimal](9, 2) NULL,
	[TAMT7] [decimal](9, 2) NULL,
	[TAMT8] [decimal](9, 2) NULL,
	[TAMT9] [decimal](9, 2) NULL,
	[TAMT10] [decimal](9, 2) NULL,
	[TAMT11] [decimal](9, 2) NULL,
	[TAMT12] [decimal](9, 2) NULL,
	[TAMT13] [decimal](9, 2) NULL,
	[TAMT14] [decimal](9, 2) NULL,
	[TAMT15] [decimal](9, 2) NULL,
	[TAMT16] [decimal](9, 2) NULL,
	[TAMT17] [decimal](9, 2) NULL,
	[TAMT18] [decimal](9, 2) NULL,
	[TAMT19] [decimal](9, 2) NULL,
	[TFLT] [decimal](7, 2) NULL,
	[BKTE1] [decimal](9, 2) NULL,
	[BKTE2] [decimal](9, 2) NULL,
	[BKTE3] [decimal](9, 2) NULL,
	[BKTE4] [decimal](9, 2) NULL,
	[BKTE5] [decimal](9, 2) NULL,
	[BKTE6] [decimal](9, 2) NULL,
	[BKTE7] [decimal](9, 2) NULL,
	[BKTE8] [decimal](9, 2) NULL,
	[BKTE9] [decimal](9, 2) NULL,
	[BKTE10] [decimal](9, 2) NULL,
	[BKTE11] [decimal](9, 2) NULL,
	[BKTE12] [decimal](9, 2) NULL,
	[BKTE13] [decimal](9, 2) NULL,
	[BKTE14] [decimal](9, 2) NULL,
	[BKTE15] [decimal](9, 2) NULL,
	[BKTE16] [decimal](9, 2) NULL,
	[BKTE17] [decimal](9, 2) NULL,
	[BKTE18] [decimal](9, 2) NULL,
	[BKTE19] [decimal](9, 2) NULL,
	[BK50] [decimal](9, 2) NULL,
	[BK20] [decimal](9, 2) NULL,
	[BK10] [decimal](9, 2) NULL,
	[BK05] [decimal](9, 2) NULL,
	[MXSL] [decimal](9, 2) NULL,
	[CB100] [decimal](9, 2) NULL,
	[BK100] [decimal](9, 2) NULL,
	[BC100] [decimal](9, 2) NULL,
	[CB02] [decimal](9, 2) NULL,
	[BK02] [decimal](9, 2) NULL,
	[BC02] [decimal](9, 2) NULL,
	[SPARE] [char](69) NULL,
 CONSTRAINT [PK_CBSHDR] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Number - Key to file - Always set to 01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount currently in store bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BANK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store bank Variance (ie actual Versus BANK)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BVAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = BVAR changed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BVAI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initial Store Float Amount Inc. in BANK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'FLOT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 50 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'CB50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 20 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'CB20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 10 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'CB10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of  5 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'CB05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of  1 pound Notes/Coins' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'CB01'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of all coins except 1 pound coins' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'CBCO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCD20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Income Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'INCA20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTD20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. Out Account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'OUTA20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous account numbers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MISA20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TEND20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Occurrence number (1 to 10) of RETOPT tender
type this relates to (must not be blank)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TNUM20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Deposit this TT in the Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TDEP20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last day polled.         *W01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'LWED'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last week printed flag (N=print;Y=printed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'LWPF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Ask for Denominations of notes when
entering Cashier Information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'NOTE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bullion centre Minimum Multiple ?50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BC50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bullion centre Minimum Multiple ?20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BC20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bullion centre Minimum Multiple ?10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bullion centre Minimum Multiple ?05' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BC05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of tenders excluding cash in safe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TAMT19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till float amount (included in BHCBCO)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'TFLT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking tender types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BKTE19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking ?50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BK50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking ?20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BK20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking ?10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BK10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested banking ?5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BK05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum banking allowed per slip number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'MXSL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cash Denomination - ?100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'CB100'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested Banking - ?100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BK100'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bullion Centre minimum multiple - ?100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BC100'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Cash Denomination -£2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'CB02'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested Banking - £2  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BK02'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bullion Centre minimum multiple - £2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'BC02'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Reduced from A99)    *W02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C B S H D R  =   Cash Balancing Header Record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSHDR'
END
GO

