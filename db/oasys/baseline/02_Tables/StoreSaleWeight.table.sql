﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StoreSaleWeight]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table StoreSaleWeight'
CREATE TABLE [dbo].[StoreSaleWeight](
	[Id] [int] NOT NULL,
	[DateActive] [date] NOT NULL,
	[Week] [tinyint] NOT NULL,
	[Value] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_StoreSaleWeight] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[DateActive] ASC,
	[Week] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

