﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PROMPTS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PROMPTS'
CREATE TABLE [dbo].[PROMPTS](
	[PROMPTID] [char](8) NOT NULL,
	[PROMPTTYPE] [char](1) NOT NULL,
	[IMAGEPATH] [char](255) NOT NULL,
	[SCREENTEXT1] [char](40) NOT NULL,
	[SCREENTEXT2] [char](40) NOT NULL,
	[SCREENTEXT3] [char](40) NOT NULL,
	[SCREENTEXT4] [char](40) NOT NULL,
	[PRINTTEXT1] [char](40) NOT NULL,
	[PRINTTEXT2] [char](40) NOT NULL,
	[PRINTTEXT3] [char](40) NOT NULL,
	[PRINTTEXT4] [char](40) NOT NULL,
	[REJECTTEXT1] [char](40) NOT NULL,
	[REJECTTEXT2] [char](40) NOT NULL,
	[DATAITEM] [char](255) NOT NULL,
	[DATAGROUP] [char](255) NOT NULL,
	[CONDITION] [char](255) NOT NULL,
	[SUPV] [bit] NOT NULL,
	[MANA] [bit] NOT NULL,
	[COLLECT] [char](1) NOT NULL,
	[SALE] [bit] NOT NULL,
	[PROMPTGROUP] [tinyint] NOT NULL,
	[PROMPTVALUE] [char](20) NOT NULL,
	[RESPONSETYPE] [char](1) NOT NULL,
 CONSTRAINT [PK_PROMPTS] PRIMARY KEY CLUSTERED 
(
	[PROMPTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prompt ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'PROMPTID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prompt Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'PROMPTTYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Image Path' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'IMAGEPATH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Screen Text 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'SCREENTEXT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Screen Text 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'SCREENTEXT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Screen Text 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'SCREENTEXT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Screen Text 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'SCREENTEXT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Print Text 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'PRINTTEXT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Print Text 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'PRINTTEXT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Print Text 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'PRINTTEXT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Print Text 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'PRINTTEXT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reject Text 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'REJECTTEXT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reject Text 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'REJECTTEXT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data Item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'DATAITEM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data Group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'DATAGROUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Condition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'CONDITION'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supervisor?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Manager?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'MANA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Collect' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'COLLECT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sale?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'SALE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prompt Group?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'PROMPTGROUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prompt Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'PROMPTVALUE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Response Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS', @level2type=N'COLUMN',@level2name=N'RESPONSETYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P R O M P T S = Prompts File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROMPTS'
END
GO

