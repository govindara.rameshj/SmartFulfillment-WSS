﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Islands]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table Islands'
CREATE TABLE [dbo].[Islands](
	[IslandId] [int] NOT NULL,
	[Code] [char](1) NOT NULL,
	[Id] [int] NOT NULL,
	[PointX] [int] NOT NULL,
	[PointY] [int] NOT NULL,
	[PlanNumber] [int] NOT NULL,
	[PlanSegment] [tinyint] NOT NULL,
	[WalkSequence] [int] NOT NULL,
 CONSTRAINT [PK_Islands] PRIMARY KEY CLUSTERED 
(
	[IslandId] ASC,
	[Code] ASC,
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifier ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands', @level2type=N'COLUMN',@level2name=N'IslandId'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Island Code Number one character reference Link to IslandTypes table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands', @level2type=N'COLUMN',@level2name=N'Code'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands', @level2type=N'COLUMN',@level2name=N'Id'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'X Position' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands', @level2type=N'COLUMN',@level2name=N'PointX'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Y Position' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands', @level2type=N'COLUMN',@level2name=N'PointY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Plan Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands', @level2type=N'COLUMN',@level2name=N'PlanNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Plan Segment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands', @level2type=N'COLUMN',@level2name=N'PlanSegment'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Walk Sequence Number ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands', @level2type=N'COLUMN',@level2name=N'WalkSequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'I S L A N D S = Islands Master File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands'
END
GO

