﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RMFiles]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RMFiles'
CREATE TABLE [dbo].[RMFiles](
	[CompanyID] [int] NOT NULL,
	[FileID] [int] NOT NULL,
	[FileName] [varchar](30) NULL,
	[Version] [varchar](15) NULL,
	[FileDate] [datetime] NULL,
	[DateImported] [datetime] NULL,
	[FolderFlag] [int] NULL,
	[FileType] [int] NULL,
	[Folder] [varchar](50) NULL,
	[Register] [bit] NOT NULL,
	[PilotVersion] [varchar](15) NULL,
	[PilotFileDate] [datetime] NULL,
	[ServerFile] [bit] NULL,
	[IgnoreDuplicates] [bit] NULL,
	[UseTransactions] [bit] NULL
) ON [PRIMARY]
END
GO

