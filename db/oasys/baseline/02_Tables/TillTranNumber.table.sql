﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TillTranNumber]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TillTranNumber'
CREATE TABLE [dbo].[TillTranNumber](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateInserted] [datetime2](7) NOT NULL
) ON [PRIMARY]
END
GO

