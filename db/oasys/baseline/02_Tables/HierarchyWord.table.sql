﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[HierarchyWord]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table HierarchyWord'
CREATE TABLE [dbo].[HierarchyWord](
	[NUMB] [int] NULL,
	[GROU] [int] NULL,
	[SGRP] [int] NULL,
	[STYL] [int] NULL,
	[Word] [varchar](50) NULL,
	[VowelsStripped] [bit] NULL,
	[Source] [char](3) NULL
) ON [PRIMARY]
END
GO

