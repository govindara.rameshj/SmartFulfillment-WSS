﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Parameters]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table Parameters'
CREATE TABLE [dbo].[Parameters](
	[ParameterID] [int] NOT NULL,
	[Description] [varchar](50) NULL,
	[StringValue] [varchar](80) NULL,
	[LongValue] [int] NULL,
	[BooleanValue] [bit] NOT NULL,
	[DecimalValue] [numeric](10, 5) NULL,
	[ValueType] [tinyint] NOT NULL,
 CONSTRAINT [PK_Parameters] PRIMARY KEY CLUSTERED 
(
	[ParameterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parameter Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameters', @level2type=N'COLUMN',@level2name=N'ParameterID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameters', @level2type=N'COLUMN',@level2name=N'Description'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'String Value Upto 80 characters' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameters', @level2type=N'COLUMN',@level2name=N'StringValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Long Value (Number) ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameters', @level2type=N'COLUMN',@level2name=N'LongValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Boolean Value True or False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameters', @level2type=N'COLUMN',@level2name=N'BooleanValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Decimal Value ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameters', @level2type=N'COLUMN',@level2name=N'DecimalValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value Type;
   * 0 = String Value 
   * 1 = Integer Value
   * 2 = Decimal Value
   * 3 = Boolean Value
   * 4 = Long Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameters', @level2type=N'COLUMN',@level2name=N'ValueType'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P A R A M E T E R S = Parameters File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Parameters'
END
GO

