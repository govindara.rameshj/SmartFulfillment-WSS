﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TENOPT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table TENOPT'
CREATE TABLE [dbo].[TENOPT](
	[TEND] [char](2) NOT NULL,
	[TTID] [decimal](3, 0) NULL,
	[TTDE] [char](15) NULL,
	[TTDS] [decimal](3, 0) NULL,
	[TTOT] [bit] NOT NULL,
	[TTCC] [bit] NOT NULL,
	[TTEF] [bit] NOT NULL,
	[TODR] [bit] NOT NULL,
	[TADA] [bit] NOT NULL,
	[PFOC] [bit] NOT NULL,
	[PBOC] [bit] NOT NULL,
	[MINV] [decimal](5, 2) NULL,
	[MAXV] [decimal](13, 2) NULL,
	[FLIM] [decimal](9, 0) NULL,
	[InUse] [bit] NOT NULL,
 CONSTRAINT [PK_TENOPT] PRIMARY KEY CLUSTERED 
(
	[TEND] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Number - Defines Display sequence when listed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'TEND'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Identifier
- See TENCTL
- Defines Functionality of Tender.
- Tender ID must exist in TENCTL & TENCTL :IUSE
  must be On.
- It will be valid to define multiple TEND''s to a
  single Tender Identifier, eg. "Voucher A" and
			*   "Voucher B" can both be identified as "08" (See
			*   TENCTL) and will broadly perform in the same way.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'TTID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Type Description
 - Default description will be TENCTL :TTDE.
 - Client may change description but should be
   careful.  Changing Description of Cash to Cheque
   will not make the Tender function as a cheque, it
   will still function as Cash.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'TTDE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Over Tender Allowed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'TTOT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Capture Credit Card Details' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'TTCC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Use EFTPoS for this Tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'TTEF'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Open Cash Drawer for this tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'TODR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Default remaining amount to tender' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'TADA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Allow Option to Print Front of Cheque Usually only valid for Cheques' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'PFOC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'On = Force Print of Back of Cheque Usually only valid for Cheques' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'PBOC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimum value of Tender accepted
 Usually Lowest Cash Denomination Value
 Eg. Norway = 0.50
 If used for Cash, Cash Rounding Tender must also
 be valid.				- See TENCTL
 Also, if minimum is greater than amount due, and
 overtendering is not allowed, tender would be, in
 effect, an invalid tender.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'MINV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum value of Tender accepted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'MAXV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if in use False if not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT', @level2type=N'COLUMN',@level2name=N'InUse'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' T E N O P T   =   Tender Options file                               
                     Client Maintains this file.                           
                     Contains list of Valid Tenders.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TENOPT'
END
GO

