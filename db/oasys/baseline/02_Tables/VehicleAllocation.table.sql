﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[VehicleAllocation]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table VehicleAllocation'
CREATE TABLE [dbo].[VehicleAllocation](
	[OrderID] [int] NOT NULL,
	[VehicleTypeIndex] [varchar](10) NOT NULL,
	[DeliveryDate] [date] NOT NULL,
	[LastVehicleUpdateDateTime] [datetime] NULL,
 CONSTRAINT [PK_VehicleAllocation] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC,
	[DeliveryDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

