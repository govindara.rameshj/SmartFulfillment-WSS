﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SYSPAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SYSPAS'
CREATE TABLE [dbo].[SYSPAS](
	[EEID] [char](3) NOT NULL,
	[NAME] [char](35) NULL,
	[INIT] [char](5) NULL,
	[POSI] [char](20) NULL,
	[EPAY] [char](20) NULL,
	[PASS] [char](5) NULL,
	[DPAS] [date] NULL,
	[SUPV] [bit] NOT NULL,
	[AUTH] [char](5) NULL,
	[DAUT] [date] NULL,
	[OUTL] [char](2) NULL,
	[IAFG1] [bit] NOT NULL,
	[IAFG2] [bit] NOT NULL,
	[IAFG3] [bit] NOT NULL,
	[IAFG4] [bit] NOT NULL,
	[IAFG5] [bit] NOT NULL,
	[IAFG6] [bit] NOT NULL,
	[IAFG7] [bit] NOT NULL,
	[IAFG8] [bit] NOT NULL,
	[IAFG9] [bit] NOT NULL,
	[IAFG10] [bit] NOT NULL,
	[IAFG11] [bit] NOT NULL,
	[IAFG12] [bit] NOT NULL,
	[IAFG13] [bit] NOT NULL,
	[IAFG14] [bit] NOT NULL,
	[IAFG15] [bit] NOT NULL,
	[IAFG16] [bit] NOT NULL,
	[IAFG17] [bit] NOT NULL,
	[IAFG18] [bit] NOT NULL,
	[IAFG19] [bit] NOT NULL,
	[IAFG20] [bit] NOT NULL,
	[IAFG21] [bit] NOT NULL,
	[IAFG22] [bit] NOT NULL,
	[IAFG23] [bit] NOT NULL,
	[IAFG24] [bit] NOT NULL,
	[IAFG25] [bit] NOT NULL,
	[IAFG26] [bit] NOT NULL,
	[IAFG27] [bit] NOT NULL,
	[IAFG28] [bit] NOT NULL,
	[IAFG29] [bit] NOT NULL,
	[IAFG30] [bit] NOT NULL,
	[IAFG31] [bit] NOT NULL,
	[IAFG32] [bit] NOT NULL,
	[IAFG33] [bit] NOT NULL,
	[IAFG34] [bit] NOT NULL,
	[IAFG35] [bit] NOT NULL,
	[IAFG36] [bit] NOT NULL,
	[IAFG37] [bit] NOT NULL,
	[IAFG38] [bit] NOT NULL,
	[IAFG39] [bit] NOT NULL,
	[IAFG40] [bit] NOT NULL,
	[IAFG41] [bit] NOT NULL,
	[IAFG42] [bit] NOT NULL,
	[IAFG43] [bit] NOT NULL,
	[IAFG44] [bit] NOT NULL,
	[IAFG45] [bit] NOT NULL,
	[IAFG46] [bit] NOT NULL,
	[IAFG47] [bit] NOT NULL,
	[IAFG48] [bit] NOT NULL,
	[IAFG49] [bit] NOT NULL,
	[IAFG50] [bit] NOT NULL,
	[IAFG51] [bit] NOT NULL,
	[IAFG52] [bit] NOT NULL,
	[IAFG53] [bit] NOT NULL,
	[IAFG54] [bit] NOT NULL,
	[IAFG55] [bit] NOT NULL,
	[IAFG56] [bit] NOT NULL,
	[IAFG57] [bit] NOT NULL,
	[IAFG58] [bit] NOT NULL,
	[IAFG59] [bit] NOT NULL,
	[IAFG60] [bit] NOT NULL,
	[IAFG61] [bit] NOT NULL,
	[IAFG62] [bit] NOT NULL,
	[IAFG63] [bit] NOT NULL,
	[IAFG64] [bit] NOT NULL,
	[IAFG65] [bit] NOT NULL,
	[IAFG66] [bit] NOT NULL,
	[IAFG67] [bit] NOT NULL,
	[IAFG68] [bit] NOT NULL,
	[IAFG69] [bit] NOT NULL,
	[IAFG70] [bit] NOT NULL,
	[IAFG71] [bit] NOT NULL,
	[IAFG72] [bit] NOT NULL,
	[IAFG73] [bit] NOT NULL,
	[IAFG74] [bit] NOT NULL,
	[IAFG75] [bit] NOT NULL,
	[IAFG76] [bit] NOT NULL,
	[IAFG77] [bit] NOT NULL,
	[IAFG78] [bit] NOT NULL,
	[IAFG79] [bit] NOT NULL,
	[IAFG80] [bit] NOT NULL,
	[IAFG81] [bit] NOT NULL,
	[IAFG82] [bit] NOT NULL,
	[IAFG83] [bit] NOT NULL,
	[IAFG84] [bit] NOT NULL,
	[IAFG85] [bit] NOT NULL,
	[IAFG86] [bit] NOT NULL,
	[IAFG87] [bit] NOT NULL,
	[IAFG88] [bit] NOT NULL,
	[IAFG89] [bit] NOT NULL,
	[IAFG90] [bit] NOT NULL,
	[IAFG91] [bit] NOT NULL,
	[IAFG92] [bit] NOT NULL,
	[IAFG93] [bit] NOT NULL,
	[IAFG94] [bit] NOT NULL,
	[IAFG95] [bit] NOT NULL,
	[IAFG96] [bit] NOT NULL,
	[IAFG97] [bit] NOT NULL,
	[IAFG98] [bit] NOT NULL,
	[IAFG99] [bit] NOT NULL,
	[SECL1] [char](1) NULL,
	[SECL2] [char](1) NULL,
	[SECL3] [char](1) NULL,
	[SECL4] [char](1) NULL,
	[SECL5] [char](1) NULL,
	[SECL6] [char](1) NULL,
	[SECL7] [char](1) NULL,
	[SECL8] [char](1) NULL,
	[SECL9] [char](1) NULL,
	[SECL10] [char](1) NULL,
	[SECL11] [char](1) NULL,
	[SECL12] [char](1) NULL,
	[SECL13] [char](1) NULL,
	[SECL14] [char](1) NULL,
	[SECL15] [char](1) NULL,
	[SECL16] [char](1) NULL,
	[SECL17] [char](1) NULL,
	[SECL18] [char](1) NULL,
	[SECL19] [char](1) NULL,
	[SECL20] [char](1) NULL,
	[SECL21] [char](1) NULL,
	[SECL22] [char](1) NULL,
	[SECL23] [char](1) NULL,
	[SECL24] [char](1) NULL,
	[SECL25] [char](1) NULL,
	[SECL26] [char](1) NULL,
	[SECL27] [char](1) NULL,
	[SECL28] [char](1) NULL,
	[SECL29] [char](1) NULL,
	[SECL30] [char](1) NULL,
	[SECL31] [char](1) NULL,
	[SECL32] [char](1) NULL,
	[SECL33] [char](1) NULL,
	[SECL34] [char](1) NULL,
	[SECL35] [char](1) NULL,
	[SECL36] [char](1) NULL,
	[SECL37] [char](1) NULL,
	[SECL38] [char](1) NULL,
	[SECL39] [char](1) NULL,
	[SECL40] [char](1) NULL,
	[SECL41] [char](1) NULL,
	[SECL42] [char](1) NULL,
	[SECL43] [char](1) NULL,
	[SECL44] [char](1) NULL,
	[SECL45] [char](1) NULL,
	[SECL46] [char](1) NULL,
	[SECL47] [char](1) NULL,
	[SECL48] [char](1) NULL,
	[SECL49] [char](1) NULL,
	[SECL50] [char](1) NULL,
	[SECL51] [char](1) NULL,
	[SECL52] [char](1) NULL,
	[SECL53] [char](1) NULL,
	[SECL54] [char](1) NULL,
	[SECL55] [char](1) NULL,
	[SECL56] [char](1) NULL,
	[SECL57] [char](1) NULL,
	[SECL58] [char](1) NULL,
	[SECL59] [char](1) NULL,
	[SECL60] [char](1) NULL,
	[SECL61] [char](1) NULL,
	[SECL62] [char](1) NULL,
	[SECL63] [char](1) NULL,
	[SECL64] [char](1) NULL,
	[SECL65] [char](1) NULL,
	[SECL66] [char](1) NULL,
	[SECL67] [char](1) NULL,
	[SECL68] [char](1) NULL,
	[SECL69] [char](1) NULL,
	[SECL70] [char](1) NULL,
	[SECL71] [char](1) NULL,
	[SECL72] [char](1) NULL,
	[SECL73] [char](1) NULL,
	[SECL74] [char](1) NULL,
	[SECL75] [char](1) NULL,
	[SECL76] [char](1) NULL,
	[SECL77] [char](3) NULL,
	[SECL78] [char](1) NULL,
	[SECL79] [char](1) NULL,
	[SECL80] [char](1) NULL,
	[SECL81] [char](1) NULL,
	[SECL82] [char](1) NULL,
	[SECL83] [char](1) NULL,
	[SECL84] [char](1) NULL,
	[SECL85] [char](1) NULL,
	[SECL86] [char](1) NULL,
	[SECL87] [char](1) NULL,
	[SECL88] [char](1) NULL,
	[SECL89] [char](1) NULL,
	[SECL90] [char](1) NULL,
	[SECL91] [char](1) NULL,
	[SECL92] [char](1) NULL,
	[SECL93] [char](1) NULL,
	[SECL94] [char](1) NULL,
	[SECL95] [char](1) NULL,
	[SECL96] [char](1) NULL,
	[SECL97] [char](1) NULL,
	[SECL98] [char](1) NULL,
	[SECL99] [char](1) NULL,
	[DELC] [bit] NOT NULL,
	[DDAT] [date] NULL,
	[DTIM] [char](6) NULL,
	[DWHO] [char](3) NULL,
	[DOUT] [char](2) NULL,
	[TNAM] [char](35) NULL,
	[DFAM] [decimal](9, 2) NULL,
	[LANG] [char](3) NULL,
	[MANA] [bit] NOT NULL,
 CONSTRAINT [PK_SYSPAS] PRIMARY KEY CLUSTERED 
(
	[EEID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee Public ID number - eg. Cashier
Number (Key to the File)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of Person' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initials' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'INIT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Position' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'POSI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee Payroll Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'EPAY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee Password' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'PASS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Password Valid Until this Date
(Calculated by adding SO:PDAY days to today
when the password is changed)
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'DPAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Employee is a Supervisor, an
authorisation code must be entered.
OFF = No authorisation code required.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SUPV'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Authorisation code - Only if able to authorise
transactions (:SUPV = Y) - Else Blank.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'AUTH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Authorisation code Valid Until this Date
(Calculated by adding SO:ADAY days to today
when the password is changed)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'DAUT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outlet currently being used by employee.
This is used in conjunction with (:MSON in SYSOPT)
to determine if the employee can be signed onto more
than one workstation (Outlet) at a time.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'OUTL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG31'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG32'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG33'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG34'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG35'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG36'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG37'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG38'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG39'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG40'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG41'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG42'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG43'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG44'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG45'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG46'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG47'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG48'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG49'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG51'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG52'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG53'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG54'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG55'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG56'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG57'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG58'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG59'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG60'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG61'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG62'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG63'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG64'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG65'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG66'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG67'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG68'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG69'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG70'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG71'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG72'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG73'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG74'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG75'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG76'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG77'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG78'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG79'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG80'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG81'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG82'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG83'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG84'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG85'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG86'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG87'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG88'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG89'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG90'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG91'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG92'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG93'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG94'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG95'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG96'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG97'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG98'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Access to the Application Functional
Group is Allowed.
OFF = Access to the Application Functional
Group is NOT Allowed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'IAFG99'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL21'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL22'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL23'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL24'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL25'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL26'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL27'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL28'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL29'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL30'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL31'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL32'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL33'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL34'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL35'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL36'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL37'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL38'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL39'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL40'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL41'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL42'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL43'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL44'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL45'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL46'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL47'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL48'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL49'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL51'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL52'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL53'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL54'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL55'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL56'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL57'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL58'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL59'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL60'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL61'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL62'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL63'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL64'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL65'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL66'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL67'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL68'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL69'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL70'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL71'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL72'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL73'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL74'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL75'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL76'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL77'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL78'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL79'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL80'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL81'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL82'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL83'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL84'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL85'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL86'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL87'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL88'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL89'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL90'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL91'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL92'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL93'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL94'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL95'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL96'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL97'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL98'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Level of the person within this
Application Functional Group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'SECL99'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON  = Logically Deleted, can NOT be used but will
NOT be deleted until all history relating to
to this employee has been deleted.
OFF = Valid for Use.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Employee was Deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'DDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time of Day Employee Deleted (HHMMSS)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'DTIM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee ID of Person who Deleted this Employee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'DWHO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Outlet (Device) Used to Delete this Employee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'DOUT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name to be printed on the Till receipt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'TNAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default Float Amount for this Employee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'DFAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Operator Preferred Language Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'LANG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Indicates Management Authorisation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS', @level2type=N'COLUMN',@level2name=N'MANA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S Y S P A S  =   PC Uptrac Store System password record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYSPAS'
END
GO

