﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LogSOM_InvalidXml]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table LogSOM_InvalidXml'
CREATE TABLE [dbo].[LogSOM_InvalidXml](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VendaOrderNo] [nvarchar](20) NULL,
	[StoreOrderNo] [nvarchar](6) NOT NULL,
	[OrderManagerOrderNo] [int] NULL,
	[DateLogged] [datetime] NOT NULL,
	[RequestTypeID] [int] NOT NULL,
	[FailedXML] [xml] NULL,
 CONSTRAINT [PK_LogSOM_InvalidXml] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

