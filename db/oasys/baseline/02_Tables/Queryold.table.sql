﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Queryold]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table Queryold'
CREATE TABLE [dbo].[Queryold](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [smallint] NOT NULL,
	[SKUN] [char](6) NULL,
	[DEPT] [char](2) NULL,
	[IBAR] [bit] NOT NULL,
	[SUPV] [char](3) NULL,
	[QUAN] [decimal](7, 0) NULL,
	[SPRI] [decimal](9, 2) NULL,
	[PRIC] [decimal](9, 2) NULL,
	[PRVE] [decimal](9, 2) NULL,
	[EXTP] [decimal](9, 2) NULL,
	[EXTC] [decimal](9, 3) NULL,
	[RITM] [bit] NOT NULL,
	[PORC] [smallint] NOT NULL,
	[ITAG] [bit] NOT NULL,
	[CATA] [bit] NOT NULL,
	[VSYM] [char](1) NULL,
	[TPPD] [decimal](9, 2) NULL,
	[TPME] [char](6) NULL,
	[POPD] [decimal](9, 2) NULL,
	[POME] [char](6) NULL,
	[QBPD] [decimal](9, 2) NULL,
	[QBME] [char](6) NULL,
	[DGPD] [decimal](9, 2) NULL,
	[DGME] [char](6) NULL,
	[MBPD] [decimal](9, 2) NULL,
	[MBME] [char](6) NULL,
	[HSPD] [decimal](9, 2) NULL,
	[HSME] [char](6) NULL,
	[ESPD] [decimal](9, 2) NULL,
	[ESME] [char](6) NULL,
	[LREV] [bit] NOT NULL,
	[ESEQ] [char](6) NULL,
	[CTGY] [char](6) NULL,
	[GRUP] [char](6) NULL,
	[SGRP] [char](6) NULL,
	[STYL] [char](6) NULL,
	[QSUP] [char](3) NULL,
	[ESEV] [decimal](9, 2) NULL,
	[IMDN] [bit] NOT NULL,
	[SALT] [char](1) NULL,
	[VATN] [decimal](3, 0) NULL,
	[VATV] [decimal](9, 2) NULL,
	[BDCO] [char](1) NULL,
	[BDCOInd] [bit] NOT NULL,
	[RCOD] [char](2) NULL,
	[RTI] [char](1) NULL,
	[WEERATE] [char](2) NULL,
	[WEESEQN] [char](3) NULL,
	[WEECOST] [decimal](9, 2) NULL,
	[SPARE] [char](40) NULL
) ON [PRIMARY]
END
GO

