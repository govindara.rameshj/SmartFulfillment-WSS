﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReportColumn]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ReportColumn'
CREATE TABLE [dbo].[ReportColumn](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Caption] [varchar](50) NULL,
	[FormatType] [int] NOT NULL,
	[Format] [varchar](50) NULL,
	[IsImagePath] [bit] NOT NULL,
	[MinWidth] [int] NULL,
	[MaxWidth] [int] NULL,
	[Alignment] [int] NULL,
 CONSTRAINT [PK_ReportColumn] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

