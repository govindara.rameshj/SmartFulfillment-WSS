﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ISUHDRHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ISUHDRHistory'
CREATE TABLE [dbo].[ISUHDRHistory](
	[DATEADDED] [date] NOT NULL,
	[NUMB] [char](6) NOT NULL,
	[IDAT] [date] NOT NULL,
	[SUPP] [char](5) NULL,
	[SPON] [char](6) NULL,
	[SOQN] [char](6) NULL,
	[BBCC] [char](1) NULL,
	[VALU] [decimal](9, 2) NULL,
	[PNUM] [char](6) NULL,
	[IMPI] [bit] NOT NULL,
	[RECI] [bit] NOT NULL,
	[DELI] [bit] NOT NULL,
	[DRLN] [char](6) NULL,
	[REJI] [char](1) NULL,
 CONSTRAINT [PK_ISUHDRHistory] PRIMARY KEY CLUSTERED 
(
	[DATEADDED] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

