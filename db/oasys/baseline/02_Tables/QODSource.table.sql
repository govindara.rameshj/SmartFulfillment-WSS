﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[QODSource]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table QODSource'
CREATE TABLE [dbo].[QODSource](
	[SOURCE_CODE] [char](2) NOT NULL,
	[SHOW_IN_UI] [bit] NOT NULL,
	[SEND_UPDATES_TO_OM] [bit] NOT NULL,
	[ALLOW_REMOTE_CREATE] [bit] NOT NULL,
	[IS_CLICK_AND_COLLECT] [bit] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_QODSource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code of source' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QODSource', @level2type=N'COLUMN',@level2name=N'SOURCE_CODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag for showing orders from this source in UI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QODSource', @level2type=N'COLUMN',@level2name=N'SHOW_IN_UI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Specifies if order should be comunicated to the order manager' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QODSource', @level2type=N'COLUMN',@level2name=N'SEND_UPDATES_TO_OM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Specifies if order can be created by call to the QOD_Create web service method' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QODSource', @level2type=N'COLUMN',@level2name=N'ALLOW_REMOTE_CREATE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Click and collect order identificator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QODSource', @level2type=N'COLUMN',@level2name=N'IS_CLICK_AND_COLLECT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QODSource = Codes of sources for orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QODSource'
END
GO

