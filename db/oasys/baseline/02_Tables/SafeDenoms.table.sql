﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SafeDenoms]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SafeDenoms'
CREATE TABLE [dbo].[SafeDenoms](
	[PeriodID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[TenderID] [int] NOT NULL,
	[ID] [decimal](9, 2) NOT NULL,
	[SafeValue] [decimal](9, 2) NOT NULL,
	[ChangeValue] [decimal](9, 2) NOT NULL,
	[SystemValue] [decimal](9, 2) NOT NULL,
	[SuggestedValue] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK_SafeDenoms] PRIMARY KEY CLUSTERED 
(
	[PeriodID] ASC,
	[CurrencyID] ASC,
	[TenderID] ASC,
	[ID] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms', @level2type=N'COLUMN',@level2name=N'PeriodID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Currency Identifier (e.g. GBP = Sterling)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms', @level2type=N'COLUMN',@level2name=N'CurrencyID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms', @level2type=N'COLUMN',@level2name=N'TenderID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id for Type of denomination kept in the bag (e.g 50.00 = £50 pound notes )' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Safe Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms', @level2type=N'COLUMN',@level2name=N'SafeValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Change Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms', @level2type=N'COLUMN',@level2name=N'ChangeValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms', @level2type=N'COLUMN',@level2name=N'SystemValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suggested Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms', @level2type=N'COLUMN',@level2name=N'SuggestedValue'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S A F E D E N O M S = Safe Denominations File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeDenoms'
END
GO

