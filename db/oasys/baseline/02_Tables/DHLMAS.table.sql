﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DHLMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DHLMAS'
CREATE TABLE [dbo].[DHLMAS](
	[DATE1] [date] NOT NULL,
	[DESCR] [char](40) NOT NULL,
	[RNAM] [char](40) NOT NULL,
	[REXT] [char](3) NOT NULL,
	[RDES] [char](40) NOT NULL,
	[TDVU] [decimal](13, 2) NOT NULL,
	[TDVP] [decimal](13, 2) NOT NULL,
	[TDVN] [decimal](13, 2) NOT NULL,
	[WTVU] [decimal](13, 2) NOT NULL,
 CONSTRAINT [PK_DHLMAS] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[DESCR] ASC,
	[RNAM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Key - Transaction Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Key - Transaction Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'* Report name generating this field
* This may be blank if not relating directly to
* a specific report.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'RNAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Report Extension (If report is spooled to SPOOL\RRRGFT.355)
* then REXT will be "355". ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'REXT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Report description (as printed in report)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'RDES'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Absolute Value of specified entry for record date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'TDVU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Positive Value of specified entry for record date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'TDVP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Negative Value of specified entry for record date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'TDVN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Week-to-date value of specified entry for 
* record date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS', @level2type=N'COLUMN',@level2name=N'WTVU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D H L M A S  =   Daily Highlights Master Record ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DHLMAS'
END
GO

