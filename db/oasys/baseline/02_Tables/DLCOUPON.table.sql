﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLCOUPON]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLCOUPON'
CREATE TABLE [dbo].[DLCOUPON](
	[TranDate] [date] NOT NULL,
	[TranTillID] [char](2) NOT NULL,
	[TranNo] [char](4) NOT NULL,
	[Sequence] [char](4) NOT NULL,
	[CouponID] [char](7) NOT NULL,
	[Status] [char](1) NULL,
	[ExcCoupon] [bit] NULL,
	[MarketRef] [char](4) NULL,
	[ReUsable] [bit] NULL,
	[IssueStoreNo] [char](3) NULL,
	[SerialNo] [char](6) NULL,
	[ManagerID] [char](3) NULL,
	[RTI] [char](1) NULL,
 CONSTRAINT [PK_DLCOUPON] PRIMARY KEY CLUSTERED 
(
	[TranDate] ASC,
	[TranTillID] ASC,
	[TranNo] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'TranDate'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Till Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'TranTillID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'TranNo'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'Sequence'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'CouponID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status of the Coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'Status'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Exclude Coupon True (1) excluded fALSE (0) included' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'ExcCoupon'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Marketing Reference Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'MarketRef'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True (1) = re-usable False (0) = Not re-usable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'ReUsable'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Number that issued the coupon.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'IssueStoreNo'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon Serial Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'SerialNo'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Manage Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'ManagerID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L C O U P O N = Coupon Transaction File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOUPON'
END
GO

