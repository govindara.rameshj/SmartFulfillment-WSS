﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[COUPON]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table COUPON'
CREATE TABLE [dbo].[COUPON](
	[DATE1] [date] NOT NULL,
	[LINE] [char](3) NOT NULL,
	[CODE] [char](2) NULL,
	[CLAS] [char](2) NULL,
	[REFN] [char](2) NULL,
	[NAME] [char](30) NULL,
	[POST] [char](8) NULL,
	[TVAL] [decimal](9, 2) NULL,
	[CVAL] [decimal](9, 2) NULL,
	[FLAG] [bit] NOT NULL,
	[COMM] [bit] NOT NULL,
	[DONE] [bit] NOT NULL,
	[DELC] [bit] NOT NULL,
 CONSTRAINT [PK_COUPON] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[LINE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date COUPON entered at till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence number 001 - 999
Assigned by Entry Program COECOP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'LINE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon Code Non zero positive numeric' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'CODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon Class Number (Must be valid Class)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'CLAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference number within coupon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'REFN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'NAME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Postal code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'POST'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction value of sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'TVAL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coupon value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'CVAL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = First record for this sale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'FLAG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = Is ready to comm to head office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'COMM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = has been commed to head office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'DONE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON = ready to be deleted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON', @level2type=N'COLUMN',@level2name=N'DELC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C O U P O N  =   COUPON Entry File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COUPON'
END
GO

