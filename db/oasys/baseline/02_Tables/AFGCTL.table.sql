﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AFGCTL]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table AFGCTL'
CREATE TABLE [dbo].[AFGCTL](
	[LANG] [char](3) NOT NULL,
	[NUMB] [char](2) NOT NULL,
	[DESCR] [char](35) NULL,
	[DSCL] [char](1) NULL,
	[SCOL] [decimal](3, 0) NULL,
	[DCOL] [decimal](3, 0) NULL,
	[TEXT1] [char](35) NULL,
	[TEXT2] [char](35) NULL,
	[TEXT3] [char](35) NULL,
	[TEXT4] [char](35) NULL,
	[TEXT5] [char](35) NULL,
	[TEXT6] [char](35) NULL,
	[TEXT7] [char](35) NULL,
	[TEXT8] [char](35) NULL,
	[TEXT9] [char](35) NULL,
	[TEXT10] [char](35) NULL,
	[TEXT11] [char](35) NULL,
	[TEXT12] [char](35) NULL,
	[TEXT13] [char](35) NULL,
	[TEXT14] [char](35) NULL,
	[TEXT15] [char](35) NULL,
	[LOCA] [bit] NOT NULL,
	[SPAR] [char](40) NULL,
 CONSTRAINT [PK_AFGCTL] PRIMARY KEY CLUSTERED 
(
	[LANG] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Language Code - As used by Telephone Systems' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'LANG'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Application Functional Group (AFG) Number

Tilling, Customer Service, Cash Office, etc

00 - This record is used to hold general
information about the menu setup.

The field uses are as follows:

:TEXT Line 1 - Main Menu Title Line
Used whe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'DESCR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default Security Level for this AFG
Valid are 1 (lowest) to 9 (highest)

Used as Password security level default
in SYSPAS - SP:SECL(O=NUMB)

Minimum Security Level Allowed for this AFG
Password maintenance does not allow security
level to be' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'DSCL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Defined Screen Colour for this Menu Selection

For the 00 record, this field contains the
colour to be used for the menu sign on box.
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'SCOL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Defined Screen Colour for the
Documentation on this Menu Selection

For the 00 record, this field contains the colour
to be used for the menu title & help lines.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'DCOL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'TEXT15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Yes=Allow this program to run on Stand Alone Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'LOCA'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Spare for future use (Reduced from A70 - W02' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL', @level2type=N'COLUMN',@level2name=N'SPAR'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A F G C T L   =   Application Functional Group File ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AFGCTL'
END
GO

