﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GROUPPROMPTS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table GROUPPROMPTS'
CREATE TABLE [dbo].[GROUPPROMPTS](
	[GROUPID] [char](6) NOT NULL,
	[PROMPTID] [char](8) NOT NULL,
	[SEQUENCE] [char](8) NOT NULL,
	[OKGOTO] [char](8) NOT NULL,
	[YESGOTO] [char](8) NOT NULL,
	[NOGOTO] [char](8) NOT NULL,
	[LOGREJECT] [char](1) NOT NULL,
	[GROUPDESC] [char](20) NOT NULL,
 CONSTRAINT [PK_GROUPPROMPTS] PRIMARY KEY CLUSTERED 
(
	[GROUPID] ASC,
	[PROMPTID] ASC,
	[SEQUENCE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS', @level2type=N'COLUMN',@level2name=N'GROUPID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prompt Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS', @level2type=N'COLUMN',@level2name=N'PROMPTID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS', @level2type=N'COLUMN',@level2name=N'SEQUENCE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OK Goto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS', @level2type=N'COLUMN',@level2name=N'OKGOTO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Yes Goto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS', @level2type=N'COLUMN',@level2name=N'YESGOTO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Goto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS', @level2type=N'COLUMN',@level2name=N'NOGOTO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True = Rejection Log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS', @level2type=N'COLUMN',@level2name=N'LOGREJECT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS', @level2type=N'COLUMN',@level2name=N'GROUPDESC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'G R O U P P R O M P T S = Group Prompts File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GROUPPROMPTS'
END
GO

