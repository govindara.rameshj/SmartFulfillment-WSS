﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RETHDR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RETHDR'
CREATE TABLE [dbo].[RETHDR](
	[TKEY] [int] IDENTITY(1,1) NOT NULL,
	[NUMB] [char](6) NULL,
	[SUPP] [char](5) NULL,
	[EDAT] [date] NULL,
	[RDAT] [date] NULL,
	[PONO] [char](6) NULL,
	[DRLN] [char](6) NULL,
	[VALU] [decimal](9, 2) NULL,
	[EPRT] [bit] NOT NULL,
	[RPRT] [bit] NOT NULL,
	[CLAS] [char](2) NULL,
	[IsDeleted] [bit] NOT NULL,
	[RTI] [char](1) NULL,
 CONSTRAINT [PK_RETHDR] PRIMARY KEY CLUSTERED 
(
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DIG Compiler KEY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Supplier Return Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number for this Return' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'SUPP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the Return was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'EDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expected Return collection Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'RDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Was Original P/O No. - Now not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'PONO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL number (assign by system on release)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'DRLN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of this Return' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'VALU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Needs to be printed (Entry)	    N=Printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'EPRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Needs to be printed (Release)	    N=Printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'RPRT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Was Class no. of return - Now not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'CLAS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True If it can be deleted False if it can not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'IsDeleted'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'R E T H D R  =   Supplier Returns Header Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RETHDR'
END
GO

