﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLCOMM]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLCOMM'
CREATE TABLE [dbo].[DLCOMM](
	[DATE1] [date] NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [smallint] NOT NULL,
	[TOKENID] [varchar](50) NULL,
	[CNHASH] [char](28) NULL,
	[RTI] [char](1) NULL,
	[SPARE] [char](99) NULL,
 CONSTRAINT [PK_DLCOMM] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOMM', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id - Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOMM', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number from PC Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOMM', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Sequence Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOMM', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Commidea Account of File Registration ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOMM', @level2type=N'COLUMN',@level2name=N'TOKENID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Commidea unique PAN reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOMM', @level2type=N'COLUMN',@level2name=N'CNHASH'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Flag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOMM', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W09    Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLCOMM', @level2type=N'COLUMN',@level2name=N'SPARE'
END
GO

