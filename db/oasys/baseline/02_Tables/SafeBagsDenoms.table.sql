﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SafeBagsDenoms]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table SafeBagsDenoms'
CREATE TABLE [dbo].[SafeBagsDenoms](
	[BagID] [int] NOT NULL,
	[CurrencyID] [char](3) NOT NULL,
	[TenderID] [int] NOT NULL,
	[ID] [decimal](9, 2) NOT NULL,
	[Value] [decimal](9, 2) NOT NULL,
	[SlipNumber] [char](20) NULL,
 CONSTRAINT [PK_SafeBagsDenoms] PRIMARY KEY CLUSTERED 
(
	[BagID] ASC,
	[CurrencyID] ASC,
	[TenderID] ASC,
	[ID] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bag Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBagsDenoms', @level2type=N'COLUMN',@level2name=N'BagID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Currency Identifier (i.e. GBP = Sterling)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBagsDenoms', @level2type=N'COLUMN',@level2name=N'CurrencyID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBagsDenoms', @level2type=N'COLUMN',@level2name=N'TenderID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID Type of denomination used in bag (e.g. 50.00)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBagsDenoms', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value contained in bag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBagsDenoms', @level2type=N'COLUMN',@level2name=N'Value'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Slip Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBagsDenoms', @level2type=N'COLUMN',@level2name=N'SlipNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'S A F E B A G D E N O M S = Safe Bag Denominations File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SafeBagsDenoms'
END
GO

