﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RMFileVersions]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table RMFileVersions'
CREATE TABLE [dbo].[RMFileVersions](
	[CompanyID] [int] NOT NULL,
	[ReleaseID] [int] NULL,
	[FileID] [int] NULL,
	[Version] [varchar](15) NULL,
	[FileDate] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[PilotVersion] [varchar](15) NULL,
	[PilotFileDate] [datetime] NULL
) ON [PRIMARY]
END
GO

