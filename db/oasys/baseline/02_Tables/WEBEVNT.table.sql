﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[WEBEVNT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table WEBEVNT'
CREATE TABLE [dbo].[WEBEVNT](
	[DATE1] [date] NULL,
	[TILL] [char](2) NULL,
	[TRAN] [char](4) NULL,
	[NUMB] [smallint] NULL,
	[ESEQ] [char](6) NULL,
	[TYPE] [char](2) NULL,
	[AMNT] [decimal](9, 2) NULL,
	[SPARE] [char](32) NULL
) ON [PRIMARY]
END
GO

