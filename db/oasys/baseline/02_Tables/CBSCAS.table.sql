﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CBSCAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table CBSCAS'
CREATE TABLE [dbo].[CBSCAS](
	[DATE1] [date] NOT NULL,
	[NUMB] [char](3) NOT NULL,
	[GROS] [decimal](9, 2) NULL,
	[DISC] [decimal](9, 2) NULL,
	[SACN] [decimal](5, 0) NULL,
	[SAAM] [decimal](9, 2) NULL,
	[SADI] [decimal](9, 2) NULL,
	[SCCN] [decimal](5, 0) NULL,
	[SCAM] [decimal](9, 2) NULL,
	[SCDI] [decimal](9, 2) NULL,
	[RFCN] [decimal](5, 0) NULL,
	[RFAM] [decimal](9, 2) NULL,
	[RFDI] [decimal](9, 2) NULL,
	[RCCN] [decimal](5, 0) NULL,
	[RCAM] [decimal](9, 2) NULL,
	[RCDI] [decimal](9, 2) NULL,
	[MICT1] [decimal](5, 0) NULL,
	[MICT2] [decimal](5, 0) NULL,
	[MICT3] [decimal](5, 0) NULL,
	[MICT4] [decimal](5, 0) NULL,
	[MICT5] [decimal](5, 0) NULL,
	[MICT6] [decimal](5, 0) NULL,
	[MICT7] [decimal](5, 0) NULL,
	[MICT8] [decimal](5, 0) NULL,
	[MICT9] [decimal](5, 0) NULL,
	[MICT10] [decimal](5, 0) NULL,
	[MICT11] [decimal](5, 0) NULL,
	[MICT12] [decimal](5, 0) NULL,
	[MICT13] [decimal](5, 0) NULL,
	[MICT14] [decimal](5, 0) NULL,
	[MICT15] [decimal](5, 0) NULL,
	[MICT16] [decimal](5, 0) NULL,
	[MICT17] [decimal](5, 0) NULL,
	[MICT18] [decimal](5, 0) NULL,
	[MICT19] [decimal](5, 0) NULL,
	[MICT20] [decimal](5, 0) NULL,
	[MIVA1] [decimal](9, 2) NULL,
	[MIVA2] [decimal](9, 2) NULL,
	[MIVA3] [decimal](9, 2) NULL,
	[MIVA4] [decimal](9, 2) NULL,
	[MIVA5] [decimal](9, 2) NULL,
	[MIVA6] [decimal](9, 2) NULL,
	[MIVA7] [decimal](9, 2) NULL,
	[MIVA8] [decimal](9, 2) NULL,
	[MIVA9] [decimal](9, 2) NULL,
	[MIVA10] [decimal](9, 2) NULL,
	[MIVA11] [decimal](9, 2) NULL,
	[MIVA12] [decimal](9, 2) NULL,
	[MIVA13] [decimal](9, 2) NULL,
	[MIVA14] [decimal](9, 2) NULL,
	[MIVA15] [decimal](9, 2) NULL,
	[MIVA16] [decimal](9, 2) NULL,
	[MIVA17] [decimal](9, 2) NULL,
	[MIVA18] [decimal](9, 2) NULL,
	[MIVA19] [decimal](9, 2) NULL,
	[MIVA20] [decimal](9, 2) NULL,
	[MOCT1] [decimal](5, 0) NULL,
	[MOCT2] [decimal](5, 0) NULL,
	[MOCT3] [decimal](5, 0) NULL,
	[MOCT4] [decimal](5, 0) NULL,
	[MOCT5] [decimal](5, 0) NULL,
	[MOCT6] [decimal](5, 0) NULL,
	[MOCT7] [decimal](5, 0) NULL,
	[MOCT8] [decimal](5, 0) NULL,
	[MOCT9] [decimal](5, 0) NULL,
	[MOCT10] [decimal](5, 0) NULL,
	[MOCT11] [decimal](5, 0) NULL,
	[MOCT12] [decimal](5, 0) NULL,
	[MOCT13] [decimal](5, 0) NULL,
	[MOCT14] [decimal](5, 0) NULL,
	[MOCT15] [decimal](5, 0) NULL,
	[MOCT16] [decimal](5, 0) NULL,
	[MOCT17] [decimal](5, 0) NULL,
	[MOCT18] [decimal](5, 0) NULL,
	[MOCT19] [decimal](5, 0) NULL,
	[MOCT20] [decimal](5, 0) NULL,
	[MOVA1] [decimal](9, 2) NULL,
	[MOVA2] [decimal](9, 2) NULL,
	[MOVA3] [decimal](9, 2) NULL,
	[MOVA4] [decimal](9, 2) NULL,
	[MOVA5] [decimal](9, 2) NULL,
	[MOVA6] [decimal](9, 2) NULL,
	[MOVA7] [decimal](9, 2) NULL,
	[MOVA8] [decimal](9, 2) NULL,
	[MOVA9] [decimal](9, 2) NULL,
	[MOVA10] [decimal](9, 2) NULL,
	[MOVA11] [decimal](9, 2) NULL,
	[MOVA12] [decimal](9, 2) NULL,
	[MOVA13] [decimal](9, 2) NULL,
	[MOVA14] [decimal](9, 2) NULL,
	[MOVA15] [decimal](9, 2) NULL,
	[MOVA16] [decimal](9, 2) NULL,
	[MOVA17] [decimal](9, 2) NULL,
	[MOVA18] [decimal](9, 2) NULL,
	[MOVA19] [decimal](9, 2) NULL,
	[MOVA20] [decimal](9, 2) NULL,
	[STTC1] [decimal](5, 0) NULL,
	[STTC2] [decimal](5, 0) NULL,
	[STTC3] [decimal](5, 0) NULL,
	[STTC4] [decimal](5, 0) NULL,
	[STTC5] [decimal](5, 0) NULL,
	[STTC6] [decimal](5, 0) NULL,
	[STTC7] [decimal](5, 0) NULL,
	[STTC8] [decimal](5, 0) NULL,
	[STTC9] [decimal](5, 0) NULL,
	[STTC10] [decimal](5, 0) NULL,
	[STTA1] [decimal](9, 2) NULL,
	[STTA2] [decimal](9, 2) NULL,
	[STTA3] [decimal](9, 2) NULL,
	[STTA4] [decimal](9, 2) NULL,
	[STTA5] [decimal](9, 2) NULL,
	[STTA6] [decimal](9, 2) NULL,
	[STTA7] [decimal](9, 2) NULL,
	[STTA8] [decimal](9, 2) NULL,
	[STTA9] [decimal](9, 2) NULL,
	[STTA10] [decimal](9, 2) NULL,
	[PTOT] [decimal](9, 2) NULL,
	[PKUP1] [decimal](9, 2) NULL,
	[PKUP2] [decimal](9, 2) NULL,
	[PKUP3] [decimal](9, 2) NULL,
	[PKUP4] [decimal](9, 2) NULL,
	[PKUP5] [decimal](9, 2) NULL,
	[PKUP6] [decimal](9, 2) NULL,
	[PKUP7] [decimal](9, 2) NULL,
	[PKUP8] [decimal](9, 2) NULL,
	[PKUP9] [decimal](9, 2) NULL,
	[PKUP10] [decimal](9, 2) NULL,
	[PKUP11] [decimal](9, 2) NULL,
	[PKUP12] [decimal](9, 2) NULL,
	[PKUP13] [decimal](9, 2) NULL,
	[PKUP14] [decimal](9, 2) NULL,
	[PKUP15] [decimal](9, 2) NULL,
	[PKUP16] [decimal](9, 2) NULL,
	[PKUP17] [decimal](9, 2) NULL,
	[PKUP18] [decimal](9, 2) NULL,
	[PKUP19] [decimal](9, 2) NULL,
	[PKUP20] [decimal](9, 2) NULL,
	[PN50] [decimal](9, 2) NULL,
	[PN20] [decimal](9, 2) NULL,
	[PN10] [decimal](9, 2) NULL,
	[PN05] [decimal](9, 2) NULL,
	[PN01] [decimal](9, 2) NULL,
	[PNCO] [decimal](9, 2) NULL,
	[STXC1] [decimal](5, 0) NULL,
	[STXC2] [decimal](5, 0) NULL,
	[STXC3] [decimal](5, 0) NULL,
	[STXC4] [decimal](5, 0) NULL,
	[STXC5] [decimal](5, 0) NULL,
	[STXC6] [decimal](5, 0) NULL,
	[STXC7] [decimal](5, 0) NULL,
	[STXC8] [decimal](5, 0) NULL,
	[STXC9] [decimal](5, 0) NULL,
	[STXC10] [decimal](5, 0) NULL,
	[STXA1] [decimal](9, 2) NULL,
	[STXA2] [decimal](9, 2) NULL,
	[STXA3] [decimal](9, 2) NULL,
	[STXA4] [decimal](9, 2) NULL,
	[STXA5] [decimal](9, 2) NULL,
	[STXA6] [decimal](9, 2) NULL,
	[STXA7] [decimal](9, 2) NULL,
	[STXA8] [decimal](9, 2) NULL,
	[STXA9] [decimal](9, 2) NULL,
	[STXA10] [decimal](9, 2) NULL,
	[PN100] [decimal](9, 2) NULL,
	[TEXT] [char](40) NULL,
	[PN02] [decimal](9, 2) NULL,
	[SPARE] [char](47) NULL,
 CONSTRAINT [PK_CBSCAS] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of Cashier activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cashier Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gross Sales Amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'GROS'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount Amount ( nett = gros - disc )' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'DISC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'SACN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'SAAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales discount
W03 (Documentation change : SADI Field is not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'SADI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Correct count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'SCCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Correct amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'SCAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Correct discount
W03 (Documentation change : SCDI Field is not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'SCDI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refund count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'RFCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refund amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'RFAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refund discount
W03 (Documentation change : RFDI Field is not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'RFDI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refund Correct count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'RCCN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refund Correct amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'RCAM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refund Correct discount
W03 (Documentation change : RCDI Field is not used)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'RCDI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MICT20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Income 01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MIVA20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Count' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOCT20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Miscellaneous Out    01 - 20  Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'MOVA20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type count  occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender type amount occurrences (from RETOPT)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STTA10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup Total - Calculated' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PTOT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP11'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP12'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP13'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP14'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP15'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP16'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP17'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP18'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP19'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pickup of Tender Types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PKUP20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 50 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PN50'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 20 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PN20'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 10 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PN10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of  5 pound Notes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PN05'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of  1 pound Notes/Coins' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PN01'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of all coins except 1 pound coins' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PNCO'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXC10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA8'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA9'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No Comment Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'STXA10'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of 100 pound notes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'PN100'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05    Filler for future use (Reduced from A99) then A55 *' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'C B S C A S  =   Cash Balancing Cashier Record  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CBSCAS'
END
GO

