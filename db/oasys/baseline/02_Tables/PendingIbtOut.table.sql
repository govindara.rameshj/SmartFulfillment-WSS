﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PendingIbtOut]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PendingIbtOut'
CREATE TABLE [dbo].[PendingIbtOut](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StoreNumber] [char](4) NOT NULL,
	[DateCreated] [date] NOT NULL,
	[SkuNumber] [char](6) NOT NULL,
	[Quantity] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[DrlNumber] [char](6) NULL,
	[BatchID] [int] NOT NULL,
	[IsProcessed] [bit] NOT NULL,
 CONSTRAINT [PK_PendingIbtOut] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'ID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'StoreNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creation Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'DateCreated'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Item Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'SkuNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'Quantity'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee Identifier of who recorded this record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'UserID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'DrlNumber'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Batch Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'BatchID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True if it has been processed False if not.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut', @level2type=N'COLUMN',@level2name=N'IsProcessed'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P E N D I N G I B T O U T = Pending IBT Out File' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PendingIbtOut'
END
GO

