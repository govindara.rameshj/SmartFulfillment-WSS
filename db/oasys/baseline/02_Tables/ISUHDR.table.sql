﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ISUHDR]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table ISUHDR'
CREATE TABLE [dbo].[ISUHDR](
	[NUMB] [char](6) NOT NULL,
	[IDAT] [date] NULL,
	[SUPP] [char](5) NULL,
	[SPON] [char](6) NULL,
	[SOQN] [char](6) NULL,
	[BBCC] [char](1) NULL,
	[VALU] [decimal](9, 2) NULL,
	[PNUM] [char](6) NULL,
	[IMPI] [bit] NOT NULL,
	[RECI] [bit] NOT NULL,
	[DELI] [bit] NOT NULL,
	[DRLN] [char](6) NULL,
	[REJI] [char](1) NULL,
 CONSTRAINT [PK_ISUHDR] PRIMARY KEY CLUSTERED 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Issue Note Number (from BBC)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the Issue was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'IDAT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Supplier Number of this Issue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'SUPP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store purchase order number
Set to BBC order number if an import' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'SPON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store SOQ Ctl Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'SOQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BBC code -   C   = BBC consolidated
        D   = BBC discreet
W02        W   = Warehouse
W04        A   = Alternative Supplier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'BBCC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of this Issue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'VALU'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Pricing Doc (lorry Tracking) Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'PNUM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON=Imported issue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'IMPI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON=Received issue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'RECI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ON=Delete issue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'DELI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Number when received' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'DRLN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Blank = not rejected
A = Invalid PURHDR record
B = PURHDR record not on file
C = PURHDR already received
D = PURHDR deleted
E = SOQ CTL number does not match
F = Invalid CONMAS record
G = CONMAS record not on file
H = New Issue number less th' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR', @level2type=N'COLUMN',@level2name=N'REJI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'I S U H D R  =   Issue Master Header Records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISUHDR'
END
GO

