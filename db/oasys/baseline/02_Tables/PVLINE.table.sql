﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PVLINE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table PVLINE'
CREATE TABLE [dbo].[PVLINE](
	[DATE1] [char](8) NOT NULL,
	[TILL] [char](2) NOT NULL,
	[TRAN] [char](4) NOT NULL,
	[NUMB] [char](4) NOT NULL,
	[SKUN] [char](6) NULL,
	[QUAN] [char](7) NULL,
	[SPRI] [char](10) NULL,
	[PRIC] [char](10) NULL,
	[PRVE] [char](10) NULL,
	[EXTP] [char](10) NULL,
	[RITM] [char](1) NULL,
	[VSYM] [char](1) NULL,
	[CTGY] [char](6) NULL,
	[GRUP] [char](6) NULL,
	[SGRP] [char](6) NULL,
	[STYL] [char](6) NULL,
	[SALT] [char](1) NULL,
	[PIVI] [char](1) NULL,
	[PIVM] [char](2) NULL,
	[PIVT] [char](1) NULL,
 CONSTRAINT [PK_PVLINE] PRIMARY KEY CLUSTERED 
(
	[DATE1] ASC,
	[TILL] ASC,
	[TRAN] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Date - ie, AP Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PC Till Id - Network ID Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number from PC Till' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned Sequence Number (ie, Line Number)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'NUMB'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sku Number of this Item - 000000 = Department' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity sold   N6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'QUAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System retail price lookup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'SPRI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item price after discounts applied' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'PRIC'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT Exclusive PRIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'PRVE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extended value - QUAN PRIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'EXTP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03   "Y" - related items single' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'RITM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VAT symbol for this item
VAT rates (RETOPT are 1 to 9)
W14    related to letters a ---> i ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'VSYM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy level 5 - Category' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'CTGY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy level 4 - Group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'GRUP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy level 3 - Subgroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'SGRP'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hierarchy level 2 - Style' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'STYL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sale Type Attribute (from JDA):
"B" - Basic item
"P" - Performance item
"A" - Aesthetic item
"S" - Showroom item
"G" - Good pallet item
"R" - Reduced pallet item
"D" - Delivery item
"I" - Installation item
"O" - Other (default if not specifi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'SALT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W03 Y = in-store stock item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'PIVI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Movement Type code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'PIVM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''N'' from Pivotal. Updated to ''Y'' by CTS processing.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE', @level2type=N'COLUMN',@level2name=N'PIVT'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'P V L I N E  =   Daily Reformatted Till Line Item Records (PIVOTAL)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PVLINE'
END
GO

