﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DLEANCHK]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table DLEANCHK'
CREATE TABLE [dbo].[DLEANCHK](
	[TKEY] [int] IDENTITY(1,1) NOT NULL,
	[DATE1] [date] NULL,
	[TILL] [char](2) NULL,
	[TRAN] [char](4) NULL,
	[SEQN] [decimal](5, 0) NULL,
	[TIME] [char](4) NULL,
	[ENTRY] [char](10) NULL,
	[REASONCODE] [char](2) NULL,
	[KREASON] [char](35) NULL,
	[ITEM] [char](16) NULL,
	[TYPE] [char](10) NULL,
	[EEID] [char](3) NULL,
	[EAFOUND] [bit] NOT NULL,
	[EASKUN] [char](6) NULL,
	[EASKUFOUND] [bit] NOT NULL,
	[SKUN] [char](6) NULL,
	[RTI] [char](1) NULL,
	[SPARE] [char](40) NULL,
 CONSTRAINT [PK_DLEANCHK] PRIMARY KEY CLUSTERED 
(
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DIG Compiler KEY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'TKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of Check  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'DATE1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Till Number (If from Till, else zero)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'TILL'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Number (If from Till, else zero)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'TRAN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'SEQN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Time           ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'TIME'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entry Method, ie. ''Keyed'' or ''Scanned''' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'ENTRY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Keyed Reason Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'REASONCODE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reason for keying entry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'KREASON'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item number keyed or scanned' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'ITEM'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item type entered (see ITEM above)
*                               * ''SKU'' if 5 difits or less entered
*                               * ''In-house'' if 8 digits with a leading 2
*                               * ''EAN'' for all accept those above' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'TYPE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Employee signed on' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'EEID'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' EAN record found in EANMAS if TYPE N= ''SKU''' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'EAFOUND'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SKU related to EANMAS record
*                               * Shows EANMAS:SKUN if EAFOUND = Y else zero' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'EASKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EAN record found in EANMAS if TYPE = ''SKU''' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'EASKUFOUND'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Correct SKU as entered by operator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'SKUN'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RTI Control' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'RTI'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use (Was A39)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK', @level2type=N'COLUMN',@level2name=N'SPARE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D L E A N C H K  =  Barcode checking file ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DLEANCHK'
END
GO

