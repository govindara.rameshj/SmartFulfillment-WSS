﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MDHMAS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
PRINT 'Creating table MDHMAS'
CREATE TABLE [dbo].[MDHMAS](
	[FKEY] [char](2) NOT NULL,
	[SALV1] [decimal](9, 2) NULL,
	[SALV2] [decimal](9, 2) NULL,
	[SALV3] [decimal](9, 2) NULL,
	[SALV4] [decimal](9, 2) NULL,
	[SALV5] [decimal](9, 2) NULL,
	[SALV6] [decimal](9, 2) NULL,
	[SALV7] [decimal](9, 2) NULL,
	[DRLV1] [decimal](9, 2) NULL,
	[DRLV2] [decimal](9, 2) NULL,
	[DRLV3] [decimal](9, 2) NULL,
	[DRLV4] [decimal](9, 2) NULL,
	[DRLV5] [decimal](9, 2) NULL,
	[DRLV6] [decimal](9, 2) NULL,
	[DRLV7] [decimal](9, 2) NULL,
	[CLAS1] [decimal](9, 2) NULL,
	[CLAS2] [decimal](9, 2) NULL,
	[CLAS3] [decimal](9, 2) NULL,
	[CLAS4] [decimal](9, 2) NULL,
	[CLAS5] [decimal](9, 2) NULL,
	[CLAS6] [decimal](9, 2) NULL,
	[CLAS7] [decimal](9, 2) NULL,
	[PVNO1] [decimal](7, 0) NULL,
	[PVNO2] [decimal](7, 0) NULL,
	[PVNO3] [decimal](7, 0) NULL,
	[PVNO4] [decimal](7, 0) NULL,
	[PVNO5] [decimal](7, 0) NULL,
	[PVNO6] [decimal](7, 0) NULL,
	[PVNO7] [decimal](7, 0) NULL,
	[PVVA1] [decimal](9, 2) NULL,
	[PVVA2] [decimal](9, 2) NULL,
	[PVVA3] [decimal](9, 2) NULL,
	[PVVA4] [decimal](9, 2) NULL,
	[PVVA5] [decimal](9, 2) NULL,
	[PVVA6] [decimal](9, 2) NULL,
	[PVVA7] [decimal](9, 2) NULL,
	[STLQ1] [decimal](7, 0) NULL,
	[STLQ2] [decimal](7, 0) NULL,
	[STLQ3] [decimal](7, 0) NULL,
	[STLQ4] [decimal](7, 0) NULL,
	[STLQ5] [decimal](7, 0) NULL,
	[STLQ6] [decimal](7, 0) NULL,
	[STLQ7] [decimal](7, 0) NULL,
	[STLV1] [decimal](9, 2) NULL,
	[STLV2] [decimal](9, 2) NULL,
	[STLV3] [decimal](9, 2) NULL,
	[STLV4] [decimal](9, 2) NULL,
	[STLV5] [decimal](9, 2) NULL,
	[STLV6] [decimal](9, 2) NULL,
	[STLV7] [decimal](9, 2) NULL,
	[STKV1] [decimal](9, 2) NULL,
	[STKV2] [decimal](9, 2) NULL,
	[STKV3] [decimal](9, 2) NULL,
	[STKV4] [decimal](9, 2) NULL,
	[STKV5] [decimal](9, 2) NULL,
	[STKV6] [decimal](9, 2) NULL,
	[STKV7] [decimal](9, 2) NULL,
	[VARQ1] [decimal](7, 0) NULL,
	[VARQ2] [decimal](7, 0) NULL,
	[VARQ3] [decimal](7, 0) NULL,
	[VARQ4] [decimal](7, 0) NULL,
	[VARQ5] [decimal](7, 0) NULL,
	[VARQ6] [decimal](7, 0) NULL,
	[VARQ7] [decimal](7, 0) NULL,
	[DRLB1] [decimal](9, 2) NULL,
	[DRLB2] [decimal](9, 2) NULL,
	[DRLB3] [decimal](9, 2) NULL,
	[DRLB4] [decimal](9, 2) NULL,
	[DRLB5] [decimal](9, 2) NULL,
	[DRLB6] [decimal](9, 2) NULL,
	[DRLB7] [decimal](9, 2) NULL,
	[RNBI1] [decimal](3, 0) NULL,
	[RNBI2] [decimal](3, 0) NULL,
	[RNBI3] [decimal](3, 0) NULL,
	[RNBI4] [decimal](3, 0) NULL,
	[RNBI5] [decimal](3, 0) NULL,
	[RNBI6] [decimal](3, 0) NULL,
	[RNBI7] [decimal](3, 0) NULL,
	[NOOS1] [decimal](5, 0) NULL,
	[NOOS2] [decimal](5, 0) NULL,
	[NOOS3] [decimal](5, 0) NULL,
	[NOOS4] [decimal](5, 0) NULL,
	[NOOS5] [decimal](5, 0) NULL,
	[NOOS6] [decimal](5, 0) NULL,
	[NOOS7] [decimal](5, 0) NULL,
	[NIBI1] [decimal](5, 0) NULL,
	[NIBI2] [decimal](5, 0) NULL,
	[NIBI3] [decimal](5, 0) NULL,
	[NIBI4] [decimal](5, 0) NULL,
	[NIBI5] [decimal](5, 0) NULL,
	[NIBI6] [decimal](5, 0) NULL,
	[NIBI7] [decimal](5, 0) NULL,
	[DBRD1] [date] NULL,
	[DBRD2] [date] NULL,
	[DBRD3] [date] NULL,
	[DBRD4] [date] NULL,
	[DBRD5] [date] NULL,
	[DBRD6] [date] NULL,
	[DBRD7] [date] NULL,
	[NALP1] [decimal](5, 0) NULL,
	[NALP2] [decimal](5, 0) NULL,
	[NALP3] [decimal](5, 0) NULL,
	[NALP4] [decimal](5, 0) NULL,
	[NALP5] [decimal](5, 0) NULL,
	[NALP6] [decimal](5, 0) NULL,
	[NALP7] [decimal](5, 0) NULL,
	[NPRC1] [decimal](5, 0) NULL,
	[NPRC2] [decimal](5, 0) NULL,
	[NPRC3] [decimal](5, 0) NULL,
	[NPRC4] [decimal](5, 0) NULL,
	[NPRC5] [decimal](5, 0) NULL,
	[NPRC6] [decimal](5, 0) NULL,
	[NPRC7] [decimal](5, 0) NULL,
	[DSAQ1] [decimal](5, 2) NULL,
	[DSAQ2] [decimal](5, 2) NULL,
	[DSAQ3] [decimal](5, 2) NULL,
	[DSAQ4] [decimal](5, 2) NULL,
	[DSAQ5] [decimal](5, 2) NULL,
	[DSAQ6] [decimal](5, 2) NULL,
	[DSAQ7] [decimal](5, 2) NULL,
	[RSAQ1] [decimal](5, 2) NULL,
	[RSAQ2] [decimal](5, 2) NULL,
	[RSAQ3] [decimal](5, 2) NULL,
	[RSAQ4] [decimal](5, 2) NULL,
	[RSAQ5] [decimal](5, 2) NULL,
	[RSAQ6] [decimal](5, 2) NULL,
	[RSAQ7] [decimal](5, 2) NULL,
	[TSAQ1] [decimal](5, 2) NULL,
	[TSAQ2] [decimal](5, 2) NULL,
	[TSAQ3] [decimal](5, 2) NULL,
	[TSAQ4] [decimal](5, 2) NULL,
	[TSAQ5] [decimal](5, 2) NULL,
	[TSAQ6] [decimal](5, 2) NULL,
	[TSAQ7] [decimal](5, 2) NULL,
	[DSAV1] [decimal](5, 2) NULL,
	[DSAV2] [decimal](5, 2) NULL,
	[DSAV3] [decimal](5, 2) NULL,
	[DSAV4] [decimal](5, 2) NULL,
	[DSAV5] [decimal](5, 2) NULL,
	[DSAV6] [decimal](5, 2) NULL,
	[DSAV7] [decimal](5, 2) NULL,
	[RSAV1] [decimal](5, 2) NULL,
	[RSAV2] [decimal](5, 2) NULL,
	[RSAV3] [decimal](5, 2) NULL,
	[RSAV4] [decimal](5, 2) NULL,
	[RSAV5] [decimal](5, 2) NULL,
	[RSAV6] [decimal](5, 2) NULL,
	[RSAV7] [decimal](5, 2) NULL,
	[TSAV1] [decimal](5, 2) NULL,
	[TSAV2] [decimal](5, 2) NULL,
	[TSAV3] [decimal](5, 2) NULL,
	[TSAV4] [decimal](5, 2) NULL,
	[TSAV5] [decimal](5, 2) NULL,
	[TSAV6] [decimal](5, 2) NULL,
	[TSAV7] [decimal](5, 2) NULL,
	[SLPT1] [decimal](9, 2) NULL,
	[SLPT2] [decimal](9, 2) NULL,
	[SLPT3] [decimal](9, 2) NULL,
	[SLPT4] [decimal](9, 2) NULL,
	[SLPT5] [decimal](9, 2) NULL,
	[SLPT6] [decimal](9, 2) NULL,
	[SLPT7] [decimal](9, 2) NULL,
	[SQQT1] [decimal](7, 0) NULL,
	[SQQT2] [decimal](7, 0) NULL,
	[SQQT3] [decimal](7, 0) NULL,
	[SQQT4] [decimal](7, 0) NULL,
	[SQQT5] [decimal](7, 0) NULL,
	[SQQT6] [decimal](7, 0) NULL,
	[SQQT7] [decimal](7, 0) NULL,
	[SQVT1] [decimal](9, 2) NULL,
	[SQVT2] [decimal](9, 2) NULL,
	[SQVT3] [decimal](9, 2) NULL,
	[SQVT4] [decimal](9, 2) NULL,
	[SQVT5] [decimal](9, 2) NULL,
	[SQVT6] [decimal](9, 2) NULL,
	[SQVT7] [decimal](9, 2) NULL,
	[SQLT1] [decimal](7, 0) NULL,
	[SQLT2] [decimal](7, 0) NULL,
	[SQLT3] [decimal](7, 0) NULL,
	[SQLT4] [decimal](7, 0) NULL,
	[SQLT5] [decimal](7, 0) NULL,
	[SQLT6] [decimal](7, 0) NULL,
	[SQLT7] [decimal](7, 0) NULL,
	[SQQU1] [decimal](7, 0) NULL,
	[SQQU2] [decimal](7, 0) NULL,
	[SQQU3] [decimal](7, 0) NULL,
	[SQQU4] [decimal](7, 0) NULL,
	[SQQU5] [decimal](7, 0) NULL,
	[SQQU6] [decimal](7, 0) NULL,
	[SQQU7] [decimal](7, 0) NULL,
	[SQVU1] [decimal](9, 2) NULL,
	[SQVU2] [decimal](9, 2) NULL,
	[SQVU3] [decimal](9, 2) NULL,
	[SQVU4] [decimal](9, 2) NULL,
	[SQVU5] [decimal](9, 2) NULL,
	[SQVU6] [decimal](9, 2) NULL,
	[SQVU7] [decimal](9, 2) NULL,
	[SQLU1] [decimal](7, 0) NULL,
	[SQLU2] [decimal](7, 0) NULL,
	[SQLU3] [decimal](7, 0) NULL,
	[SQLU4] [decimal](7, 0) NULL,
	[SQLU5] [decimal](7, 0) NULL,
	[SQLU6] [decimal](7, 0) NULL,
	[SQLU7] [decimal](7, 0) NULL,
	[SQQD1] [decimal](7, 0) NULL,
	[SQQD2] [decimal](7, 0) NULL,
	[SQQD3] [decimal](7, 0) NULL,
	[SQQD4] [decimal](7, 0) NULL,
	[SQQD5] [decimal](7, 0) NULL,
	[SQQD6] [decimal](7, 0) NULL,
	[SQQD7] [decimal](7, 0) NULL,
	[SQVD1] [decimal](9, 2) NULL,
	[SQVD2] [decimal](9, 2) NULL,
	[SQVD3] [decimal](9, 2) NULL,
	[SQVD4] [decimal](9, 2) NULL,
	[SQVD5] [decimal](9, 2) NULL,
	[SQVD6] [decimal](9, 2) NULL,
	[SQVD7] [decimal](9, 2) NULL,
	[SQLD1] [decimal](7, 0) NULL,
	[SQLD2] [decimal](7, 0) NULL,
	[SQLD3] [decimal](7, 0) NULL,
	[SQLD4] [decimal](7, 0) NULL,
	[SQLD5] [decimal](7, 0) NULL,
	[SQLD6] [decimal](7, 0) NULL,
	[SQLD7] [decimal](7, 0) NULL,
	[ISSU1] [decimal](7, 0) NULL,
	[ISSU2] [decimal](7, 0) NULL,
	[ISSU3] [decimal](7, 0) NULL,
	[ISSU4] [decimal](7, 0) NULL,
	[ISSU5] [decimal](7, 0) NULL,
	[ISSU6] [decimal](7, 0) NULL,
	[ISSU7] [decimal](7, 0) NULL,
	[SLWK] [decimal](9, 2) NULL,
	[RETV1] [decimal](9, 2) NULL,
	[RETV2] [decimal](9, 2) NULL,
	[RETV3] [decimal](9, 2) NULL,
	[RETV4] [decimal](9, 2) NULL,
	[RETV5] [decimal](9, 2) NULL,
	[RETV6] [decimal](9, 2) NULL,
	[RETV7] [decimal](9, 2) NULL,
	[RADJ1] [decimal](9, 2) NULL,
	[RADJ2] [decimal](9, 2) NULL,
	[RADJ3] [decimal](9, 2) NULL,
	[RADJ4] [decimal](9, 2) NULL,
	[RADJ5] [decimal](9, 2) NULL,
	[RADJ6] [decimal](9, 2) NULL,
	[RADJ7] [decimal](9, 2) NULL,
	[VCHR1] [decimal](9, 2) NULL,
	[VCHR2] [decimal](9, 2) NULL,
	[VCHR3] [decimal](9, 2) NULL,
	[VCHR4] [decimal](9, 2) NULL,
	[VCHR5] [decimal](9, 2) NULL,
	[VCHR6] [decimal](9, 2) NULL,
	[VCHR7] [decimal](9, 2) NULL,
	[INSV1] [decimal](9, 2) NULL,
	[INSV2] [decimal](9, 2) NULL,
	[INSV3] [decimal](9, 2) NULL,
	[INSV4] [decimal](9, 2) NULL,
	[INSV5] [decimal](9, 2) NULL,
	[INSV6] [decimal](9, 2) NULL,
	[INSV7] [decimal](9, 2) NULL,
	[DELV1] [decimal](9, 2) NULL,
	[DELV2] [decimal](9, 2) NULL,
	[DELV3] [decimal](9, 2) NULL,
	[DELV4] [decimal](9, 2) NULL,
	[DELV5] [decimal](9, 2) NULL,
	[DELV6] [decimal](9, 2) NULL,
	[DELV7] [decimal](9, 2) NULL,
	[MKDV1] [decimal](9, 2) NULL,
	[MKDV2] [decimal](9, 2) NULL,
	[MKDV3] [decimal](9, 2) NULL,
	[MKDV4] [decimal](9, 2) NULL,
	[MKDV5] [decimal](9, 2) NULL,
	[MKDV6] [decimal](9, 2) NULL,
	[MKDV7] [decimal](9, 2) NULL,
	[MDNV1] [decimal](9, 2) NULL,
	[MDNV2] [decimal](9, 2) NULL,
	[MDNV3] [decimal](9, 2) NULL,
	[MDNV4] [decimal](9, 2) NULL,
	[MDNV5] [decimal](9, 2) NULL,
	[MDNV6] [decimal](9, 2) NULL,
	[MDNV7] [decimal](9, 2) NULL,
	[WTFV1] [decimal](9, 2) NULL,
	[WTFV2] [decimal](9, 2) NULL,
	[WTFV3] [decimal](9, 2) NULL,
	[WTFV4] [decimal](9, 2) NULL,
	[WTFV5] [decimal](9, 2) NULL,
	[WTFV6] [decimal](9, 2) NULL,
	[WTFV7] [decimal](9, 2) NULL,
	[FIL31] [decimal](9, 2) NULL,
	[FIL32] [decimal](9, 2) NULL,
	[FIL33] [decimal](9, 2) NULL,
	[FIL34] [decimal](9, 2) NULL,
	[FIL35] [decimal](9, 2) NULL,
	[FIL36] [decimal](9, 2) NULL,
	[FIL37] [decimal](9, 2) NULL,
 CONSTRAINT [PK_MDHMAS] PRIMARY KEY CLUSTERED 
(
	[FKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record Number - key to file - Always set to 01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'FKEY'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SALV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SALV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SALV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SALV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SALV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SALV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SALV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Class Entry Sales Value - No Longer Used
*W06 Now used to store overdiue price changes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'CLAS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Class Entry Sales Value - No Longer Used
*W06 Now used to store overdiue price changes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'CLAS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Class Entry Sales Value - No Longer Used
*W06 Now used to store overdiue price changes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'CLAS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Class Entry Sales Value - No Longer Used
*W06 Now used to store overdiue price changes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'CLAS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Class Entry Sales Value - No Longer Used
*W06 Now used to store overdiue price changes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'CLAS5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Class Entry Sales Value - No Longer Used
*W06 Now used to store overdiue price changes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'CLAS6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W05 Class Entry Sales Value - No Longer Used
*W06 Now used to store overdiue price changes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'CLAS7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violation No.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVNO1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violation No.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVNO2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violation No.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVNO3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violation No.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVNO4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violation No.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVNO5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violation No.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVNO6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violation No.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVNO7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violations Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVVA1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violations Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVVA2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violations Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVVA3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violations Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVVA4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violations Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVVA5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violations Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVVA6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price Violations Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'PVVA7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLQ5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLQ6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLQ7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Loss Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STLV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STKV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STKV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STKV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STKV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STKV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STKV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'STKV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variance Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VARQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variance Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VARQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variance Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VARQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variance Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VARQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variance Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VARQ5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variance Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VARQ6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Variance Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VARQ7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLB1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLB2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLB3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLB4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLB5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLB6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DRL Balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DRLB7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipts Not Booked In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RNBI1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipts Not Booked In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RNBI2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipts Not Booked In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RNBI3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipts Not Booked In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RNBI4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipts Not Booked In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RNBI5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipts Not Booked In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RNBI6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipts Not Booked In' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RNBI7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Out of Stock Items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NOOS1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Out of Stock Items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NOOS2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Out of Stock Items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NOOS3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Out of Stock Items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NOOS4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Out of Stock Items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NOOS5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Out of Stock Items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NOOS6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Out of Stock Items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NOOS7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Items Below Impact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NIBI1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Items Below Impact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NIBI2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Items Below Impact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NIBI3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Items Below Impact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NIBI4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Items Below Impact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NIBI5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Items Below Impact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NIBI6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Items Below Impact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NIBI7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date DBR Were Sent to H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DBRD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date DBR Were Sent to H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DBRD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date DBR Were Sent to H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DBRD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date DBR Were Sent to H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DBRD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date DBR Were Sent to H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DBRD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date DBR Were Sent to H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DBRD6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date DBR Were Sent to H/O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DBRD7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Alpha Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NALP1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Alpha Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NALP2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Alpha Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NALP3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Alpha Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NALP4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Alpha Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NALP5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Alpha Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NALP6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Alpha Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NALP7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Outstanding Price Changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NPRC1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Outstanding Price Changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NPRC2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Outstanding Price Changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NPRC3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Outstanding Price Changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NPRC4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Outstanding Price Changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NPRC5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Outstanding Price Changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NPRC6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Outstanding Price Changes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'NPRC7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAQ5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAQ6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAQ7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAQ5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAQ6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAQ7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAQ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAQ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAQ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAQ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAQ5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAQ6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Quantity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAQ7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DSAV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RSAV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Stock Accuracy Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'TSAV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales loss potential 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SLPT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales loss potential 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SLPT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales loss potential 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SLPT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales loss potential 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SLPT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales loss potential 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SLPT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales loss potential 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SLPT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales loss potential 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SLPT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total quantity 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total quantity 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total quantity 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total quantity 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total quantity 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total quantity 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total quantity 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total value 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total value 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total value 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total value 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total value 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total value 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total value 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total lines 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLT1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total lines 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLT2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total lines 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLT3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total lines 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLT4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total lines 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLT5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total lines 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLT6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - total lines 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLT7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity up 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQU1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity up 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQU2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity up 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQU3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity up 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQU4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity up 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQU5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity up 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQU6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity up 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQU7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value up 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVU1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value up 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVU2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value up 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVU3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value up 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVU4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value up 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVU5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value up 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVU6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value up 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVU7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines up 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLU1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines up 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLU2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines up 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLU3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines up 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLU4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines up 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLU5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines up 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLU6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines up 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLU7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity down 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity down 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity down 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity down 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity down 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity down 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQD6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - quantity down 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQQD7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value down 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value down 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value down 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value down 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value down 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value down 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVD6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - value down 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQVD7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines down 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLD1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines down 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLD2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines down 3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLD3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines down 4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLD4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines down 5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLD5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines down 6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLD6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOQ override - lines down 7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SQLD7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of pending issues' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'ISSU1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of pending issues' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'ISSU2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of pending issues' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'ISSU3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of pending issues' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'ISSU4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of pending issues' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'ISSU5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of pending issues' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'ISSU6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of pending issues' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'ISSU7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous weeks sales for today occ.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'SLWK'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RETV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RETV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RETV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RETV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RETV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RETV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Open returns value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RETV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipt Adjustment Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RADJ1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipt Adjustment Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RADJ2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipt Adjustment Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RADJ3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipt Adjustment Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RADJ4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipt Adjustment Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RADJ5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipt Adjustment Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RADJ6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Receipt Adjustment Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'RADJ7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Voucher Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VCHR1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Voucher Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VCHR2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Voucher Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VCHR3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Voucher Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VCHR4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Voucher Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VCHR5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Voucher Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VCHR6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Voucher Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'VCHR7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Installation Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'INSV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Installation Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'INSV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Installation Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'INSV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Installation Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'INSV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Installation Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'INSV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Installation Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'INSV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Installation Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'INSV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DELV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DELV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DELV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DELV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DELV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DELV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Delivery Sales Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'DELV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markdown Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MKDV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markdown Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MKDV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markdown Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MKDV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markdown Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MKDV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markdown Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MKDV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markdown Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MKDV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Markdown Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MKDV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Mark-Down Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MDNV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Mark-Down Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MDNV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Mark-Down Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MDNV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Mark-Down Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MDNV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Mark-Down Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MDNV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Mark-Down Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MDNV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Mark-Down Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'MDNV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Write-Off Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'WTFV1'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Write-Off Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'WTFV2'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Write-Off Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'WTFV3'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Write-Off Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'WTFV4'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Write-Off Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'WTFV5'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Write-Off Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'WTFV6'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'W04 Write-Off Stock Value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'WTFV7'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'FIL31'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'FIL32'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'FIL33'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'FIL34'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'FIL35'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'FIL36'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filler for future use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS', @level2type=N'COLUMN',@level2name=N'FIL37'
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'M D H M A S  =   Manger Daily Highlights Record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MDHMAS'
END
GO

