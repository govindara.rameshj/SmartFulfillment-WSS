﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgStrmasUpdate]'))
BEGIN
	PRINT 'Creating trigger tgStrmasUpdate on table STRMAS' 
	EXEC ('CREATE TRIGGER [dbo].[tgStrmasUpdate] ON [dbo].[STRMAS] AFTER INSERT AS BEGIN RAISERROR(''Not implemented NULL'', 16, 1) END')
END 
GO

PRINT 'Altering trigger tgStrmasUpdate on table STRMAS'
GO
ALTER TRIGGER [dbo].[tgStrmasUpdate]
    ON [dbo].[STRMAS]
    FOR UPDATE
    AS begin
	declare
		@Id				char(3),
		@Name			char(12),
		@Address1		char(30),
		@Address2		char(30),
		@Address3		char(30),
		@Address4		char(30),
		@PhoneNumber	char(12),
		@FaxNumber		char(12),
		@ManagerName	char(20),
		@RegionCode		char(2),
		@IsClosed		bit,
		@CountryCode	char(3);
	declare curs cursor for 
		select 
			i.numb, 
			i.tild, 
			i.add1,
			i.add2,
			i.add3,
			i.add4,
			i.phon,
			i.sfax,
			i.mang,
			i.regc,
			i.delc,
			i.CountryCode
		from inserted i;

	open curs
	fetch next from curs into 
		@Id,
		@Name,
		@Address1,
		@Address2,
		@Address3,
		@Address4,
		@PhoneNumber,
		@FaxNumber,
		@ManagerName,
		@RegionCode,
		@IsClosed,
		@CountryCode	

	while @@FETCH_STATUS=0
	begin
		update
			Store
		set
			Name		= @Name,
			Address1	= @Address1,
			Address2	= @Address2,
			Address3	= @Address3,
			Address4	= @Address4,
			PhoneNumber = @PhoneNumber,
			FaxNumber	= @FaxNumber,
			Manager		= @ManagerName,
			RegionCode	= @RegionCode,
			IsClosed	= @IsClosed,		
			CountryCode	= @CountryCode
		where
			id = @Id

	fetch next from curs into 
		@Id,
		@Name,
		@Address1,
		@Address2,
		@Address3,
		@Address4,
		@PhoneNumber,
		@FaxNumber,
		@ManagerName,
		@RegionCode,
		@IsClosed,
		@CountryCode
	end

	close curs
	deallocate curs
end
GO

