﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgStkmasMarkdownReturnsWriteOffs]'))
BEGIN
	PRINT 'Creating trigger tgStkmasMarkdownReturnsWriteOffs on table STKMAS' 
	EXEC ('CREATE TRIGGER [dbo].[tgStkmasMarkdownReturnsWriteOffs] ON [dbo].[STKMAS] AFTER INSERT AS BEGIN RAISERROR(''Not implemented NULL'', 16, 1) END')
END 
GO

PRINT 'Altering trigger tgStkmasMarkdownReturnsWriteOffs on table STKMAS'
GO
ALTER TRIGGER [dbo].[tgStkmasMarkdownReturnsWriteOffs]
    ON [dbo].[STKMAS]
    FOR UPDATE
    AS 
declare @MarkdownQty	int;
declare @MarkdownValue	dec(9,2);
declare @ReturnQty		int;
declare @ReturnValue	dec(9,2);
declare @WriteOffQty	int;
declare @WriteOffValue	dec(9,2);

set @MarkdownQty	= (select sum(i.MDNQ - d.MDNQ) from inserted i, deleted d where i.SKUN=d.SKUN);
set @MarkdownValue	= (select sum((i.MDNQ - d.MDNQ) * i.PRIC) from inserted i, deleted d where i.SKUN=d.SKUN);
set @ReturnQty		= (select sum(i.RETQ - d.RETQ) from inserted i, deleted d where i.SKUN=d.SKUN);
set @ReturnValue	= (select sum(i.RETV - d.RETV) from inserted i, deleted d where i.SKUN=d.SKUN);
set @WriteOffQty	= (select sum(i.WTFQ - d.WTFQ) from inserted i, deleted d where i.SKUN=d.SKUN);
set @WriteOffValue	= (select sum((i.WTFQ - d.WTFQ) * i.PRIC) from inserted i, deleted d where i.SKUN=d.SKUN);


--current markdown stock value
if @MarkdownQty <>0	
begin
	insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Quantity, Value, DataDate, DataTime)
	values (702, GETDATE(), GETDATE(), @MarkdownQty, @MarkdownValue, GETDATE(), GETDATE());
end

--open return value and quantity
if @ReturnQty<>0
begin
	insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Quantity, Value, DataDate, DataTime)
	values (705, GETDATE(), GETDATE(), @ReturnQty, @ReturnValue, GETDATE(), GETDATE());
end

--pending write offs
if @WriteOffQty <> 0 
begin
	insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Quantity, Value, DataDate, DataTime)
	values (908, GETDATE(), GETDATE(), @WriteOffQty, @WriteOffValue, GETDATE(), GETDATE());
end
GO

