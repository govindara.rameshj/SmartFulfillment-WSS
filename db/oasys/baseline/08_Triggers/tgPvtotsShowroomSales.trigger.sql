﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgPvtotsShowroomSales]'))
BEGIN
	PRINT 'Creating trigger tgPvtotsShowroomSales on table PVTOTS' 
	EXEC ('CREATE TRIGGER [dbo].[tgPvtotsShowroomSales] ON [dbo].[PVTOTS] AFTER INSERT AS BEGIN RAISERROR(''Not implemented NULL'', 16, 1) END')
END 
GO

PRINT 'Altering trigger tgPvtotsShowroomSales on table PVTOTS'
GO
ALTER TRIGGER [dbo].[tgPvtotsShowroomSales] on [dbo].[PVTOTS]  for insert
as
declare @insTotal	char(10);
declare @insDate	char(8);
declare @insTime	char(6);
declare curs cursor for select TOTL, DATE1, [TIME] from inserted;

open curs
fetch next from curs into @insTotal, @insDate, @insTime

while @@FETCH_STATUS = 0
begin
	insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Quantity, Value, DataDate, DataTime)
	values	(105, GETDATE(), GETDATE(),1, 
	(CONVERT(dec(9,2), substring(@insTotal,10,1) + substring(@insTotal,0,10) )), 
	(convert(date, @insDate, 3)), 
	(convert(time, substring(@insTime,1,2) + ':' + substring(@insTime,3,2) + ':' + substring(@insTime,5,2))))

	fetch next from curs into @insTotal, @insDate, @insTime

end

close curs
deallocate curs
GO

