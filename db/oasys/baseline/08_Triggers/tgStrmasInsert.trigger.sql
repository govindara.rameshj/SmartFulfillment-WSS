﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgStrmasInsert]'))
BEGIN
	PRINT 'Creating trigger tgStrmasInsert on table STRMAS' 
	EXEC ('CREATE TRIGGER [dbo].[tgStrmasInsert] ON [dbo].[STRMAS] AFTER INSERT AS BEGIN RAISERROR(''Not implemented NULL'', 16, 1) END')
END 
GO

PRINT 'Altering trigger tgStrmasInsert on table STRMAS'
GO
ALTER TRIGGER [dbo].[tgStrmasInsert]
    ON [dbo].[STRMAS]
    FOR INSERT
    AS begin
declare
		@Id				char(3),
		@Name			char(12),
		@Address1		char(30),
		@Address2		char(30),
		@Address3		char(30),
		@Address4		char(30),
		@PhoneNumber	char(12),
		@FaxNumber		char(12),
		@ManagerName	char(20),
		@RegionCode		char(2),
		@IsClosed		bit,
		@CountryCode	char(3);
		
	declare curs cursor for 
		select 
			i.numb, 
			i.tild, 
			i.add1,
			i.add2,
			i.add3,
			i.add4,
			i.phon,
			i.sfax,
			i.mang,
			i.regc,
			i.delc,
			i.CountryCode
		from inserted i;

	open curs
	fetch next from curs into 
		@Id,
		@Name,
		@Address1,
		@Address2,
		@Address3,
		@Address4,
		@PhoneNumber,
		@FaxNumber,
		@ManagerName,
		@RegionCode,
		@IsClosed,
		@CountryCode	

	while @@FETCH_STATUS=0
	begin
		insert into
			Store (
			id,
			Name,
			Address1,
			Address2,
			Address3,
			Address4,
			PhoneNumber,
			FaxNumber,
			Manager,
			RegionCode,
			IsClosed,		
			CountryCode
		) values ( 
			@Id,
			@Name,
			@Address1,
			@Address2,
			@Address3,
			@Address4,
			@PhoneNumber,
			@FaxNumber,
			@ManagerName,
			@RegionCode,
			@IsClosed,
			@CountryCode
		);

	fetch next from curs into 
		@Id,
		@Name,
		@Address1,
		@Address2,
		@Address3,
		@Address4,
		@PhoneNumber,
		@FaxNumber,
		@ManagerName,
		@RegionCode,
		@IsClosed,
		@CountryCode
	end

	close curs
	deallocate curs

end
GO

