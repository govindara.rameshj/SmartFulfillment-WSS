﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgStkmasStockHolding]'))
BEGIN
	PRINT 'Creating trigger tgStkmasStockHolding on table STKMAS' 
	EXEC ('CREATE TRIGGER [dbo].[tgStkmasStockHolding] ON [dbo].[STKMAS] AFTER INSERT AS BEGIN RAISERROR(''Not implemented NULL'', 16, 1) END')
END 
GO

PRINT 'Altering trigger tgStkmasStockHolding on table STKMAS'
GO
ALTER TRIGGER [dbo].[tgStkmasStockHolding]
    ON [dbo].[STKMAS]
    FOR UPDATE
AS 
declare @OnhandNew	int;
declare @OnhandOld	int;
declare @Price		dec(9,2);
declare @newStock	int;
declare @oldStock	int;
declare curs cursor for select i.ONHA, d.ONHA, i.PRIC from inserted i, deleted d where i.SKUN=d.SKUN and d.IOBS=0 and d.INON=0;

print 'tgStkmasStockHolding - start'

open curs
fetch next from curs into @OnhandNew, @OnhandOld, @Price

while @@FETCH_STATUS=0
begin
	if (@OnhandNew-@OnhandOld)<>0
	begin
		set @newStock=@OnhandNew;
		set @oldStock=@OnhandOld;
		if @newStock<0 set @newStock=0;
		if @oldStock<0 set @oldStock=0;
		if (@newStock - @oldStock)<>0
		begin
			insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Value, DataDate,DataTime)
			values (701, GETDATE(), GETDATE(), (@newStock - @oldStock) * @Price, GETDATE(), GETDATE());
		end
	end

	fetch next from curs into @OnhandNew, @OnhandOld, @Price

end

close curs
deallocate curs

print 'tgStkmasStockHolding - end'
GO

