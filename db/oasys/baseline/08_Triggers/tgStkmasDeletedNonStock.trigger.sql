﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgStkmasDeletedNonStock]'))
BEGIN
	PRINT 'Creating trigger tgStkmasDeletedNonStock on table STKMAS' 
	EXEC ('CREATE TRIGGER [dbo].[tgStkmasDeletedNonStock] ON [dbo].[STKMAS] AFTER INSERT AS BEGIN RAISERROR(''Not implemented NULL'', 16, 1) END')
END 
GO

PRINT 'Altering trigger tgStkmasDeletedNonStock on table STKMAS'
GO
ALTER TRIGGER [dbo].[tgStkmasDeletedNonStock]
    ON [dbo].[STKMAS]
    FOR UPDATE
   
AS
begin
	declare 
		@Deleted	dec(9,2),
		@NonStock	dec(9,2);

	print 'tgStkmasDeletedNonStock - start'

	set @Deleted	= (select sum((i.ONHA - d.ONHA) * i.PRIC) from inserted i, deleted d where i.SKUN=d.SKUN and i.IOBS=1);
	set @NonStock	= (select sum((i.ONHA - d.ONHA) * i.PRIC) from inserted i, deleted d where i.SKUN=d.SKUN and i.INON=1);


	--check whether deleted stock
	if @Deleted <> 0
	begin
		insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Value, DataDate,DataTime)
		values (703, GETDATE(), GETDATE(), @Deleted, GETDATE(), GETDATE());
	end

	--check whether non-stockholding stock
	if @NonStock <> 0
	begin
		insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Value, DataDate,DataTime)
		values (704, GETDATE(), GETDATE(), @NonStock, GETDATE(), GETDATE());
	end

	print 'tgStkmasDeletedNonStock - end'

end
GO

