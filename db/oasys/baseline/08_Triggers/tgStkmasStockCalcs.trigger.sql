﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgStkmasStockCalcs]'))
BEGIN
	PRINT 'Creating trigger tgStkmasStockCalcs on table STKMAS' 
	EXEC ('CREATE TRIGGER [dbo].[tgStkmasStockCalcs] ON [dbo].[STKMAS] AFTER INSERT AS BEGIN RAISERROR(''Not implemented NULL'', 16, 1) END')
END 
GO

PRINT 'Altering trigger tgStkmasStockCalcs on table STKMAS'
GO
ALTER TRIGGER [dbo].[tgStkmasStockCalcs]
    ON [dbo].[STKMAS]
    FOR UPDATE
    AS DECLARE @oldONHA	INT;
DECLARE @newONHA	INT;
DECLARE	@FODT		DATETIME;
DECLARE	@TODT		DATETIME;
DECLARE @MINI		INT;
DECLARE @SMAN		INT;
DECLARE @PMIN		INT;
DECLARE @PMSD		DATETIME;
DECLARE @PMED		DATETIME;

DECLARE @oldStock	INT;
DECLARE @newStock	INT;
DECLARE @Quantity	INT;
DECLARE @Value		DEC(9,2);

print 'tgStkmasStockCalcs - start'

declare curs cursor for 
	select	d.ONHA, i.ONHA, i.FODT, i.MINI, i.SMAN, i.PMIN, i.PMSD, i.PMED from inserted i, deleted d 
	where	i.SKUN=d.SKUN
	and		i.IOBS=0
	and		i.INON=0
	and		i.IDEL=0
	and		i.IRIS=0
	and		i.ICAT=0
	and		i.NOOR=0
	and		i.DATS is not null

--get todays date
SET @TODT		= (SELECT S.TODT FROM SYSDAT S WHERE S.FKEY='01');

open curs
fetch next from curs into @oldONHA, @newONHA, @FODT, @MINI, @SMAN, @PMIN, @PMSD, @PMED

while @@FETCH_STATUS=0
begin
	IF not (@FODT IS NOT NULL AND @FODT<@TODT)
	begin
		--check for out of stock item
		if @oldONHA>0 and @newONHA<=0
		begin
			insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Quantity,  DataDate,DataTime)
			values (710, GETDATE(), GETDATE(), 1,  GETDATE(), GETDATE())
		end
		
		if @oldONHA<=0 and @newONHA>0
		begin
			insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Quantity,  DataDate,DataTime)
			values (710, GETDATE(), GETDATE(), -1,  GETDATE(), GETDATE())
		end
		
		--check for below impact level
		SET @Value = @MINI;
		IF @SMAN>0	SET @Value=@SMAN;
		IF (@PMSD IS NOT NULL) AND (@PMED IS NOT NULL)	
		BEGIN
			IF @PMSD<@TODT AND @TODT<@PMED SET @Value=@PMIN;
		END

		if @oldONHA>@Value AND @newONHA<=@Value
		begin
			insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Quantity, DataDate,DataTime)
			values (711, GETDATE(), GETDATE(), 1, GETDATE(), GETDATE());
		end
		
		if @oldONHA<=@Value AND @newONHA>@Value	
		begin
			insert into StockMovement (ActivityID, EnteredDate, EnteredTime, Quantity, DataDate,DataTime)
			values (711, GETDATE(), GETDATE(), -1, GETDATE(), GETDATE());
		end
	end

	fetch next from curs into @oldONHA, @newONHA, @FODT, @MINI, @SMAN, @PMIN, @PMSD, @PMED
end

close curs
deallocate curs

print 'tgStkmasStockCalcs - end'
GO

