﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgCORHDR4StatusChanged]'))
BEGIN
	PRINT 'Creating trigger tgCORHDR4StatusChanged on table CORHDR4' 
	EXEC ('CREATE TRIGGER [dbo].[tgCORHDR4StatusChanged] ON [dbo].[CORHDR4] AFTER INSERT AS BEGIN RAISERROR(''Not implemented NULL'', 16, 1) END')
END 
GO

PRINT 'Altering trigger tgCORHDR4StatusChanged on table CORHDR4'
GO
ALTER TRIGGER [dbo].[tgCORHDR4StatusChanged] 
ON [dbo].[CORHDR4] AFTER UPDATE
AS
	IF UPDATE(DeliveryStatus)
		BEGIN
				insert into [dbo].CORHDR4Log (DateChanged, OldStatus, NewStatus, NUMB)
					SELECT GETDATE(), d.DeliveryStatus, i.DeliveryStatus, d.NUMB FROM inserted i INNER JOIN deleted d on i.NUMB = d.NUMB 
					Where i.DeliveryStatus <> d.DeliveryStatus AND (i.DeliveryStatus = 700 or i.DeliveryStatus = 800 or i.DeliveryStatus = 900)
		END
GO

