﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsClosed' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsClosed')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsClosed on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsClosed] DEFAULT ((0)) FOR [IsClosed]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsOpenMon' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsOpenMon')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsOpenMon on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsOpenMon] DEFAULT ((1)) FOR [IsOpenMon]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsOpenTue' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsOpenTue')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsOpenTue on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsOpenTue] DEFAULT ((1)) FOR [IsOpenTue]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsOpenWed' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsOpenWed')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsOpenWed on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsOpenWed] DEFAULT ((1)) FOR [IsOpenWed]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsOpenThu' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsOpenThu')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsOpenThu on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsOpenThu] DEFAULT ((1)) FOR [IsOpenThu]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsOpenFri' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsOpenFri')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsOpenFri on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsOpenFri] DEFAULT ((1)) FOR [IsOpenFri]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsOpenSat' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsOpenSat')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsOpenSat on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsOpenSat] DEFAULT ((1)) FOR [IsOpenSat]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsOpenSun' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsOpenSun')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsOpenSun on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsOpenSun] DEFAULT ((1)) FOR [IsOpenSun]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Store' AND COLUMN_NAME = 'IsHeadOffice' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Store_IsHeadOffice')
BEGIN 
    PRINT 'Creating default constraint DF_Store_IsHeadOffice on table Store' 
    ALTER TABLE [dbo].[Store]
    ADD CONSTRAINT [DF_Store_IsHeadOffice] DEFAULT ((0)) FOR [IsHeadOffice]
END 
GO

