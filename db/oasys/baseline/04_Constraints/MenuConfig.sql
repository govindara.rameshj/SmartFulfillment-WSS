﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MenuConfig' AND COLUMN_NAME = 'LoadMaximised' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_MenuConfig_LoadMaximised')
BEGIN 
    PRINT 'Creating default constraint DF_MenuConfig_LoadMaximised on table MenuConfig' 
    ALTER TABLE [dbo].[MenuConfig]
    ADD CONSTRAINT [DF_MenuConfig_LoadMaximised] DEFAULT ((0)) FOR [LoadMaximised]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MenuConfig' AND COLUMN_NAME = 'IsModal' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_MenuConfig_IsModal')
BEGIN 
    PRINT 'Creating default constraint DF_MenuConfig_IsModal on table MenuConfig' 
    ALTER TABLE [dbo].[MenuConfig]
    ADD CONSTRAINT [DF_MenuConfig_IsModal] DEFAULT ((1)) FOR [IsModal]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MenuConfig' AND COLUMN_NAME = 'DisplaySequence' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_MenuConfig_DisplaySequence')
BEGIN 
    PRINT 'Creating default constraint DF_MenuConfig_DisplaySequence on table MenuConfig' 
    ALTER TABLE [dbo].[MenuConfig]
    ADD CONSTRAINT [DF_MenuConfig_DisplaySequence] DEFAULT ((0)) FOR [DisplaySequence]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MenuConfig' AND COLUMN_NAME = 'AllowMultiple' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_MenuConfig_AllowMultiple')
BEGIN 
    PRINT 'Creating default constraint DF_MenuConfig_AllowMultiple on table MenuConfig' 
    ALTER TABLE [dbo].[MenuConfig]
    ADD CONSTRAINT [DF_MenuConfig_AllowMultiple] DEFAULT ((0)) FOR [AllowMultiple]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MenuConfig' AND COLUMN_NAME = 'WaitForExit' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_MenuConfig_WaitForExit')
BEGIN 
    PRINT 'Creating default constraint DF_MenuConfig_WaitForExit on table MenuConfig' 
    ALTER TABLE [dbo].[MenuConfig]
    ADD CONSTRAINT [DF_MenuConfig_WaitForExit] DEFAULT ((0)) FOR [WaitForExit]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MenuConfig' AND COLUMN_NAME = 'Timeout' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_MenuConfig_Timeout')
BEGIN 
    PRINT 'Creating default constraint DF_MenuConfig_Timeout on table MenuConfig' 
    ALTER TABLE [dbo].[MenuConfig]
    ADD CONSTRAINT [DF_MenuConfig_Timeout] DEFAULT ((0)) FOR [Timeout]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MenuConfig' AND COLUMN_NAME = 'DoProcessingType' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_MenuConfig_DoProcessingType')
BEGIN 
    PRINT 'Creating default constraint DF_MenuConfig_DoProcessingType on table MenuConfig' 
    ALTER TABLE [dbo].[MenuConfig]
    ADD CONSTRAINT [DF_MenuConfig_DoProcessingType] DEFAULT ((0)) FOR [DoProcessingType]
END 
GO

