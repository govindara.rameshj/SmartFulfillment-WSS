﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Alert]') AND name = N'IDX_Alert')
BEGIN 
PRINT 'Creating index IDX_Alert on table Alert'
CREATE NONCLUSTERED INDEX [IDX_Alert] ON [dbo].[Alert] 
(
	[Authorized] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Alert' AND COLUMN_NAME = 'Authorized' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Alert_Authorized')
BEGIN 
    PRINT 'Creating default constraint DF_Alert_Authorized on table Alert' 
    ALTER TABLE [dbo].[Alert]
    ADD CONSTRAINT [DF_Alert_Authorized] DEFAULT ((0)) FOR [Authorized]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Alert' AND COLUMN_NAME = 'AlertType' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Alert_AlertType')
BEGIN 
    PRINT 'Creating default constraint DF_Alert_AlertType on table Alert' 
    ALTER TABLE [dbo].[Alert]
    ADD CONSTRAINT [DF_Alert_AlertType] DEFAULT ((1)) FOR [AlertType]
END 
GO

