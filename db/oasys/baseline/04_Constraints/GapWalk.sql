﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'GapWalk' AND COLUMN_NAME = 'ID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_GapWalk_ID')
BEGIN 
    PRINT 'Creating default constraint DF_GapWalk_ID on table GapWalk' 
    ALTER TABLE [dbo].[GapWalk]
    ADD CONSTRAINT [DF_GapWalk_ID] DEFAULT ((0)) FOR [ID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'GapWalk' AND COLUMN_NAME = 'Sequence' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_GapWalk_Sequence')
BEGIN 
    PRINT 'Creating default constraint DF_GapWalk_Sequence on table GapWalk' 
    ALTER TABLE [dbo].[GapWalk]
    ADD CONSTRAINT [DF_GapWalk_Sequence] DEFAULT ((0)) FOR [Sequence]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'GapWalk' AND COLUMN_NAME = 'IsPrinted' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_GapWalk_IsPrinted')
BEGIN 
    PRINT 'Creating default constraint DF_GapWalk_IsPrinted on table GapWalk' 
    ALTER TABLE [dbo].[GapWalk]
    ADD CONSTRAINT [DF_GapWalk_IsPrinted] DEFAULT ((0)) FOR [IsPrinted]
END 
GO

