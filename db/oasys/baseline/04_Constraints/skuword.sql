﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skuword]') AND name = N'IX_Category')
BEGIN 
PRINT 'Creating index IX_Category on table skuword'
CREATE NONCLUSTERED INDEX [IX_Category] ON [dbo].[skuword] 
(
	[Category] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skuword]') AND name = N'IX_CategoryGroup')
BEGIN 
PRINT 'Creating index IX_CategoryGroup on table skuword'
CREATE NONCLUSTERED INDEX [IX_CategoryGroup] ON [dbo].[skuword] 
(
	[Category] ASC,
	[Group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skuword]') AND name = N'IX_CategoryGroupSubGroup')
BEGIN 
PRINT 'Creating index IX_CategoryGroupSubGroup on table skuword'
CREATE NONCLUSTERED INDEX [IX_CategoryGroupSubGroup] ON [dbo].[skuword] 
(
	[Category] ASC,
	[Group] ASC,
	[SubGroup] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skuword]') AND name = N'IX_CategoryGroupSubGroupStyle')
BEGIN 
PRINT 'Creating index IX_CategoryGroupSubGroupStyle on table skuword'
CREATE NONCLUSTERED INDEX [IX_CategoryGroupSubGroupStyle] ON [dbo].[skuword] 
(
	[Category] ASC,
	[Group] ASC,
	[SubGroup] ASC,
	[Style] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skuword]') AND name = N'IX_skuid')
BEGIN 
PRINT 'Creating index IX_skuid on table skuword'
CREATE NONCLUSTERED INDEX [IX_skuid] ON [dbo].[skuword] 
(
	[SkuId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skuword]') AND name = N'IX_skuVowelsStripped')
BEGIN 
PRINT 'Creating index IX_skuVowelsStripped on table skuword'
CREATE NONCLUSTERED INDEX [IX_skuVowelsStripped] ON [dbo].[skuword] 
(
	[VowelsStripped] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skuword]') AND name = N'IX_skuword')
BEGIN 
PRINT 'Creating index IX_skuword on table skuword'
CREATE NONCLUSTERED INDEX [IX_skuword] ON [dbo].[skuword] 
(
	[Word] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[skuword]') AND name = N'IX_SkuWord_Sku')
BEGIN 
PRINT 'Creating index IX_SkuWord_Sku on table skuword'
CREATE NONCLUSTERED INDEX [IX_SkuWord_Sku] ON [dbo].[skuword] 
(
	[Sku] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

