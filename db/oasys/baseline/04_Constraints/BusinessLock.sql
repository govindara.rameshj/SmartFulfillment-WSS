﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BusinessLock' AND COLUMN_NAME = 'DateTimeStamp' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_BusinessLock_DateTimeStamp')
BEGIN 
    PRINT 'Creating default constraint DF_BusinessLock_DateTimeStamp on table BusinessLock' 
    ALTER TABLE [dbo].[BusinessLock]
    ADD CONSTRAINT [DF_BusinessLock_DateTimeStamp] DEFAULT (getdate()) FOR [DateTimeStamp]
END 
GO

