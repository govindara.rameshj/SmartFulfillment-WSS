﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ViewerSortColumns]') AND name = N'IX_ViewerSortColumns')
BEGIN 
PRINT 'Creating index IX_ViewerSortColumns on table ViewerSortColumns'
ALTER TABLE [dbo].[ViewerSortColumns] ADD  CONSTRAINT [IX_ViewerSortColumns] UNIQUE NONCLUSTERED 
(
	[SortID] ASC,
	[ConfigID] ASC,
	[SortOrder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

