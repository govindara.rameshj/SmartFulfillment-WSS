﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DeliveryChargeGroup]') AND name = N'UQ__Delivery__D38B86590F4D3C5F')
BEGIN 
PRINT 'Creating index UQ__Delivery__D38B86590F4D3C5F on table DeliveryChargeGroup'
ALTER TABLE [dbo].[DeliveryChargeGroup] ADD UNIQUE NONCLUSTERED 
(
	[Group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DeliveryChargeGroup]') AND name = N'UQ__Delivery__D38B86595812160E')
BEGIN 
PRINT 'Creating index UQ__Delivery__D38B86595812160E on table DeliveryChargeGroup'
ALTER TABLE [dbo].[DeliveryChargeGroup] ADD UNIQUE NONCLUSTERED 
(
	[Group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DeliveryChargeGroup' AND COLUMN_NAME = 'IsDefault' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_DeliveryChargeGroup_IsDefault')
BEGIN 
    PRINT 'Creating default constraint DF_DeliveryChargeGroup_IsDefault on table DeliveryChargeGroup' 
    ALTER TABLE [dbo].[DeliveryChargeGroup]
    ADD CONSTRAINT [DF_DeliveryChargeGroup_IsDefault] DEFAULT ((0)) FOR [IsDefault]
END 
GO

