﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PlanStocks' AND COLUMN_NAME = 'Number')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'PlanStocks' AND CONSTRAINT_NAME = 'FK_PlanStocks_PlanSegments')
BEGIN 
PRINT 'Creating foreign key FK_PlanStocks_PlanSegments on table PlanStocks'
ALTER TABLE [dbo].[PlanStocks]  WITH CHECK ADD  CONSTRAINT [FK_PlanStocks_PlanSegments] FOREIGN KEY([Number], [Segment])
REFERENCES [PlanSegments] ([Number], [Segment])
ON DELETE CASCADE
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PlanStocks' AND COLUMN_NAME = 'Facings' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PlanStocks_Facings')
BEGIN 
    PRINT 'Creating default constraint DF_PlanStocks_Facings on table PlanStocks' 
    ALTER TABLE [dbo].[PlanStocks]
    ADD CONSTRAINT [DF_PlanStocks_Facings] DEFAULT ((0)) FOR [Facings]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PlanStocks' AND COLUMN_NAME = 'Capacity' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PlanStocks_Capacity')
BEGIN 
    PRINT 'Creating default constraint DF_PlanStocks_Capacity on table PlanStocks' 
    ALTER TABLE [dbo].[PlanStocks]
    ADD CONSTRAINT [DF_PlanStocks_Capacity] DEFAULT ((0)) FOR [Capacity]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PlanStocks' AND COLUMN_NAME = 'Pack' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PlanStocks_Pack')
BEGIN 
    PRINT 'Creating default constraint DF_PlanStocks_Pack on table PlanStocks' 
    ALTER TABLE [dbo].[PlanStocks]
    ADD CONSTRAINT [DF_PlanStocks_Pack] DEFAULT ((0)) FOR [Pack]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PlanStocks' AND COLUMN_NAME = 'IsActive' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PlanStocks_IsActive')
BEGIN 
    PRINT 'Creating default constraint DF_PlanStocks_IsActive on table PlanStocks' 
    ALTER TABLE [dbo].[PlanStocks]
    ADD CONSTRAINT [DF_PlanStocks_IsActive] DEFAULT ((1)) FOR [IsActive]
END 
GO

