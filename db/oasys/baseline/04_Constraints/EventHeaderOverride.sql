﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'EventHeaderOverride' AND COLUMN_NAME = 'AuthorisorSecurityID')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'EventHeaderOverride' AND CONSTRAINT_NAME = 'FK_EventHeaderOverride_AuthorisorSecurityID')
BEGIN 
PRINT 'Creating foreign key FK_EventHeaderOverride_AuthorisorSecurityID on table EventHeaderOverride'
ALTER TABLE [dbo].[EventHeaderOverride]  WITH CHECK ADD  CONSTRAINT [FK_EventHeaderOverride_AuthorisorSecurityID] FOREIGN KEY([AuthorisorSecurityID])
REFERENCES [SystemUsers] ([ID])
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'EventHeaderOverride' AND COLUMN_NAME = 'CashierSecurityID')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'EventHeaderOverride' AND CONSTRAINT_NAME = 'FK_EventHeaderOverride_CashierSecurityID')
BEGIN 
PRINT 'Creating foreign key FK_EventHeaderOverride_CashierSecurityID on table EventHeaderOverride'
ALTER TABLE [dbo].[EventHeaderOverride]  WITH CHECK ADD  CONSTRAINT [FK_EventHeaderOverride_CashierSecurityID] FOREIGN KEY([CashierSecurityID])
REFERENCES [SystemUsers] ([ID])
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'EventHeaderOverride' AND COLUMN_NAME = 'EventTypeID')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'EventHeaderOverride' AND CONSTRAINT_NAME = 'FK_EventHeaderOverride_EventTypeID')
BEGIN 
PRINT 'Creating foreign key FK_EventHeaderOverride_EventTypeID on table EventHeaderOverride'
ALTER TABLE [dbo].[EventHeaderOverride]  WITH CHECK ADD  CONSTRAINT [FK_EventHeaderOverride_EventTypeID] FOREIGN KEY([EventTypeID])
REFERENCES [EventType] ([ID])
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventHeaderOverride]') AND name = N'IDX_EventHeaderOverride')
BEGIN 
PRINT 'Creating index IDX_EventHeaderOverride on table EventHeaderOverride'
CREATE UNIQUE NONCLUSTERED INDEX [IDX_EventHeaderOverride] ON [dbo].[EventHeaderOverride] 
(
	[EventTypeID] ASC,
	[Description] ASC,
	[StartDate] ASC,
	[EndDate] ASC,
	[Revision] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'EventHeaderOverride' AND COLUMN_NAME = 'DateTimeStamp' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_EventHeaderOverride_DateTimeStamp')
BEGIN 
    PRINT 'Creating default constraint DF_EventHeaderOverride_DateTimeStamp on table EventHeaderOverride' 
    ALTER TABLE [dbo].[EventHeaderOverride]
    ADD CONSTRAINT [DF_EventHeaderOverride_DateTimeStamp] DEFAULT (getdate()) FOR [DateTimeStamp]
END 
GO

