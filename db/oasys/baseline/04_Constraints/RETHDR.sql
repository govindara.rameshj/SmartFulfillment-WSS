﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[RETHDR]') AND name = N'XRETHDR1')
BEGIN 
PRINT 'Creating index XRETHDR1 on table RETHDR'
CREATE NONCLUSTERED INDEX [XRETHDR1] ON [dbo].[RETHDR] 
(
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[RETHDR]') AND name = N'XRETHDR2')
BEGIN 
PRINT 'Creating index XRETHDR2 on table RETHDR'
CREATE NONCLUSTERED INDEX [XRETHDR2] ON [dbo].[RETHDR] 
(
	[SUPP] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RETHDR' AND COLUMN_NAME = 'EPRT' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_RETHDR_EPRT')
BEGIN 
    PRINT 'Creating default constraint DF_RETHDR_EPRT on table RETHDR' 
    ALTER TABLE [dbo].[RETHDR]
    ADD CONSTRAINT [DF_RETHDR_EPRT] DEFAULT ((0)) FOR [EPRT]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RETHDR' AND COLUMN_NAME = 'RPRT' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_RETHDR_RPRT')
BEGIN 
    PRINT 'Creating default constraint DF_RETHDR_RPRT on table RETHDR' 
    ALTER TABLE [dbo].[RETHDR]
    ADD CONSTRAINT [DF_RETHDR_RPRT] DEFAULT ((0)) FOR [RPRT]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RETHDR' AND COLUMN_NAME = 'CLAS' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_RETHDR_CLAS')
BEGIN 
    PRINT 'Creating default constraint DF_RETHDR_CLAS on table RETHDR' 
    ALTER TABLE [dbo].[RETHDR]
    ADD CONSTRAINT [DF_RETHDR_CLAS] DEFAULT ('00') FOR [CLAS]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RETHDR' AND COLUMN_NAME = 'IsDeleted' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_RETHDR_IsDeleted')
BEGIN 
    PRINT 'Creating default constraint DF_RETHDR_IsDeleted on table RETHDR' 
    ALTER TABLE [dbo].[RETHDR]
    ADD CONSTRAINT [DF_RETHDR_IsDeleted] DEFAULT ((0)) FOR [IsDeleted]
END 
GO

