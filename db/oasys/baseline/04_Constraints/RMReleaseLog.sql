﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[RMReleaseLog]') AND name = N'UK_RelLog')
BEGIN 
PRINT 'Creating index UK_RelLog on table RMReleaseLog'
CREATE UNIQUE NONCLUSTERED INDEX [UK_RelLog] ON [dbo].[RMReleaseLog] 
(
	[CompanyID] ASC,
	[ReleaseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RMReleaseLog' AND COLUMN_NAME = 'CompanyID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_RMReleaseLog_CompanyID')
BEGIN 
    PRINT 'Creating default constraint DF_RMReleaseLog_CompanyID on table RMReleaseLog' 
    ALTER TABLE [dbo].[RMReleaseLog]
    ADD CONSTRAINT [DF_RMReleaseLog_CompanyID] DEFAULT ((0)) FOR [CompanyID]
END 
GO

