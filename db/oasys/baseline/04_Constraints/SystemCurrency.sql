﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemCurrency' AND COLUMN_NAME = 'IsDefault' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemCurrency_IsDefault')
BEGIN 
    PRINT 'Creating default constraint DF_SystemCurrency_IsDefault on table SystemCurrency' 
    ALTER TABLE [dbo].[SystemCurrency]
    ADD CONSTRAINT [DF_SystemCurrency_IsDefault] DEFAULT ((0)) FOR [IsDefault]
END 
GO

