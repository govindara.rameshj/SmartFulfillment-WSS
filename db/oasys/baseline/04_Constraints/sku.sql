﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sku]') AND name = N'IX_C_descr')
BEGIN 
PRINT 'Creating index IX_C_descr on table sku'
CREATE CLUSTERED INDEX [IX_C_descr] ON [dbo].[sku] 
(
	[descr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sku]') AND name = N'IX_descr')
BEGIN 
PRINT 'Creating index IX_descr on table sku'
CREATE NONCLUSTERED INDEX [IX_descr] ON [dbo].[sku] 
(
	[descr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sku]') AND name = N'IX_idskudescr')
BEGIN 
PRINT 'Creating index IX_idskudescr on table sku'
CREATE NONCLUSTERED INDEX [IX_idskudescr] ON [dbo].[sku] 
(
	[id] ASC,
	[sku] ASC,
	[descr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sku]') AND name = N'IX_NC_descr')
BEGIN 
PRINT 'Creating index IX_NC_descr on table sku'
CREATE NONCLUSTERED INDEX [IX_NC_descr] ON [dbo].[sku] 
(
	[descr] ASC
)
INCLUDE ( [id],
[sku]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sku]') AND name = N'IX_sku')
BEGIN 
PRINT 'Creating index IX_sku on table sku'
CREATE NONCLUSTERED INDEX [IX_sku] ON [dbo].[sku] 
(
	[sku] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sku]') AND name = N'IX_SpacelessDescription')
BEGIN 
PRINT 'Creating index IX_SpacelessDescription on table sku'
CREATE NONCLUSTERED INDEX [IX_SpacelessDescription] ON [dbo].[sku] 
(
	[SpacelessDescription] ASC
)
INCLUDE ( [id],
[sku],
[descr]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

