﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportGrouping' AND COLUMN_NAME = 'TableId' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportGrouping_TableId')
BEGIN 
    PRINT 'Creating default constraint DF_ReportGrouping_TableId on table ReportGrouping' 
    ALTER TABLE [dbo].[ReportGrouping]
    ADD CONSTRAINT [DF_ReportGrouping_TableId] DEFAULT ((1)) FOR [TableId]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportGrouping' AND COLUMN_NAME = 'IsDescending' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportGrouping_IsDescending')
BEGIN 
    PRINT 'Creating default constraint DF_ReportGrouping_IsDescending on table ReportGrouping' 
    ALTER TABLE [dbo].[ReportGrouping]
    ADD CONSTRAINT [DF_ReportGrouping_IsDescending] DEFAULT ((0)) FOR [IsDescending]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportGrouping' AND COLUMN_NAME = 'SummaryType' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportGrouping_SummaryType')
BEGIN 
    PRINT 'Creating default constraint DF_ReportGrouping_SummaryType on table ReportGrouping' 
    ALTER TABLE [dbo].[ReportGrouping]
    ADD CONSTRAINT [DF_ReportGrouping_SummaryType] DEFAULT ((6)) FOR [SummaryType]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportGrouping' AND COLUMN_NAME = 'ShowExpanded' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportGrouping_ShowExpanded')
BEGIN 
    PRINT 'Creating default constraint DF_ReportGrouping_ShowExpanded on table ReportGrouping' 
    ALTER TABLE [dbo].[ReportGrouping]
    ADD CONSTRAINT [DF_ReportGrouping_ShowExpanded] DEFAULT ((0)) FOR [ShowExpanded]
END 
GO

