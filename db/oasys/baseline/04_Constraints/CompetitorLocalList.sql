﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CompetitorLocalList' AND COLUMN_NAME = 'DeleteFlag' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CompetitorLocalList_DeleteFlag')
BEGIN 
    PRINT 'Creating default constraint DF_CompetitorLocalList_DeleteFlag on table CompetitorLocalList' 
    ALTER TABLE [dbo].[CompetitorLocalList]
    ADD CONSTRAINT [DF_CompetitorLocalList_DeleteFlag] DEFAULT ((0)) FOR [DeleteFlag]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CompetitorLocalList' AND COLUMN_NAME = 'TPGroup' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CompetitorLocalList_TPGroup')
BEGIN 
    PRINT 'Creating default constraint DF_CompetitorLocalList_TPGroup on table CompetitorLocalList' 
    ALTER TABLE [dbo].[CompetitorLocalList]
    ADD CONSTRAINT [DF_CompetitorLocalList_TPGroup] DEFAULT ((0)) FOR [TPGroup]
END 
GO

