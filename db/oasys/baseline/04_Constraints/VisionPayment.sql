﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionPayment' AND COLUMN_NAME = 'TenderTypeId' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionPayment_TenderTypeId')
BEGIN 
    PRINT 'Creating default constraint DF_VisionPayment_TenderTypeId on table VisionPayment' 
    ALTER TABLE [dbo].[VisionPayment]
    ADD CONSTRAINT [DF_VisionPayment_TenderTypeId] DEFAULT ((0)) FOR [TenderTypeId]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionPayment' AND COLUMN_NAME = 'ValueTender' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionPayment_ValueTender')
BEGIN 
    PRINT 'Creating default constraint DF_VisionPayment_ValueTender on table VisionPayment' 
    ALTER TABLE [dbo].[VisionPayment]
    ADD CONSTRAINT [DF_VisionPayment_ValueTender] DEFAULT ((0)) FOR [ValueTender]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionPayment' AND COLUMN_NAME = 'IsPivotal' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionPayment_IsPivotal')
BEGIN 
    PRINT 'Creating default constraint DF_VisionPayment_IsPivotal on table VisionPayment' 
    ALTER TABLE [dbo].[VisionPayment]
    ADD CONSTRAINT [DF_VisionPayment_IsPivotal] DEFAULT ((0)) FOR [IsPivotal]
END 
GO

