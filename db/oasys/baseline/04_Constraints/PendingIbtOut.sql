﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PendingIbtOut' AND COLUMN_NAME = 'IsProcessed' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PendingIbtOut_IsProcessed')
BEGIN 
    PRINT 'Creating default constraint DF_PendingIbtOut_IsProcessed on table PendingIbtOut' 
    ALTER TABLE [dbo].[PendingIbtOut]
    ADD CONSTRAINT [DF_PendingIbtOut_IsProcessed] DEFAULT ((0)) FOR [IsProcessed]
END 
GO

