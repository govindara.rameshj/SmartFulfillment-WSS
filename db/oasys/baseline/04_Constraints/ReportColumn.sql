﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportColumn' AND COLUMN_NAME = 'FormatType' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportColumn_FormatType')
BEGIN 
    PRINT 'Creating default constraint DF_ReportColumn_FormatType on table ReportColumn' 
    ALTER TABLE [dbo].[ReportColumn]
    ADD CONSTRAINT [DF_ReportColumn_FormatType] DEFAULT ((0)) FOR [FormatType]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportColumn' AND COLUMN_NAME = 'IsImagePath' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportColumn_IsImagePath')
BEGIN 
    PRINT 'Creating default constraint DF_ReportColumn_IsImagePath on table ReportColumn' 
    ALTER TABLE [dbo].[ReportColumn]
    ADD CONSTRAINT [DF_ReportColumn_IsImagePath] DEFAULT ((0)) FOR [IsImagePath]
END 
GO

