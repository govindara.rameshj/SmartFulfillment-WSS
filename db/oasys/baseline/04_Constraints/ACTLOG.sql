﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ACTLOG]') AND name = N'XACTLOG1')
BEGIN 
PRINT 'Creating index XACTLOG1 on table ACTLOG'
CREATE NONCLUSTERED INDEX [XACTLOG1] ON [dbo].[ACTLOG] 
(
	[DATE1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ACTLOG]') AND name = N'XACTLOG2')
BEGIN 
PRINT 'Creating index XACTLOG2 on table ACTLOG'
CREATE NONCLUSTERED INDEX [XACTLOG2] ON [dbo].[ACTLOG] 
(
	[EEID] ASC,
	[DATE1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ACTLOG]') AND name = N'XACTLOG3')
BEGIN 
PRINT 'Creating index XACTLOG3 on table ACTLOG'
CREATE NONCLUSTERED INDEX [XACTLOG3] ON [dbo].[ACTLOG] 
(
	[WSID] ASC,
	[DATE1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ACTLOG' AND COLUMN_NAME = 'LOGI' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ACTLOG_LOGI')
BEGIN 
    PRINT 'Creating default constraint DF_ACTLOG_LOGI on table ACTLOG' 
    ALTER TABLE [dbo].[ACTLOG]
    ADD CONSTRAINT [DF_ACTLOG_LOGI] DEFAULT ((0)) FOR [LOGI]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ACTLOG' AND COLUMN_NAME = 'FORC' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ACTLOG_FORC')
BEGIN 
    PRINT 'Creating default constraint DF_ACTLOG_FORC on table ACTLOG' 
    ALTER TABLE [dbo].[ACTLOG]
    ADD CONSTRAINT [DF_ACTLOG_FORC] DEFAULT ((0)) FOR [FORC]
END 
GO

