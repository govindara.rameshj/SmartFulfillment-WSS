﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportHyperlink' AND COLUMN_NAME = 'TableId' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportHyperlink_TableId')
BEGIN 
    PRINT 'Creating default constraint DF_ReportHyperlink_TableId on table ReportHyperlink' 
    ALTER TABLE [dbo].[ReportHyperlink]
    ADD CONSTRAINT [DF_ReportHyperlink_TableId] DEFAULT ((1)) FOR [TableId]
END 
GO

