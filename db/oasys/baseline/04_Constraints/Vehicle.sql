﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Vehicle' AND COLUMN_NAME = 'StartTime' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Vehicle_StartTime')
BEGIN 
    PRINT 'Creating default constraint DF_Vehicle_StartTime on table Vehicle' 
    ALTER TABLE [dbo].[Vehicle]
    ADD CONSTRAINT [DF_Vehicle_StartTime] DEFAULT ('00:00:00') FOR [StartTime]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Vehicle' AND COLUMN_NAME = 'StopTime' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Vehicle_StopTime')
BEGIN 
    PRINT 'Creating default constraint DF_Vehicle_StopTime on table Vehicle' 
    ALTER TABLE [dbo].[Vehicle]
    ADD CONSTRAINT [DF_Vehicle_StopTime] DEFAULT ('00:00:00') FOR [StopTime]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Vehicle' AND COLUMN_NAME = 'MaximumWeight' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Vehicle_MaximumWeight')
BEGIN 
    PRINT 'Creating default constraint DF_Vehicle_MaximumWeight on table Vehicle' 
    ALTER TABLE [dbo].[Vehicle]
    ADD CONSTRAINT [DF_Vehicle_MaximumWeight] DEFAULT ((0)) FOR [MaximumWeight]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Vehicle' AND COLUMN_NAME = 'MaximumVolume' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Vehicle_MaximumVolume')
BEGIN 
    PRINT 'Creating default constraint DF_Vehicle_MaximumVolume on table Vehicle' 
    ALTER TABLE [dbo].[Vehicle]
    ADD CONSTRAINT [DF_Vehicle_MaximumVolume] DEFAULT ((0)) FOR [MaximumVolume]
END 
GO

