﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[RMFileVersions]') AND name = N'UK_ReleaseID')
BEGIN 
PRINT 'Creating index UK_ReleaseID on table RMFileVersions'
CREATE NONCLUSTERED INDEX [UK_ReleaseID] ON [dbo].[RMFileVersions] 
(
	[CompanyID] ASC,
	[ReleaseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[RMFileVersions]') AND name = N'UK_RelFileID')
BEGIN 
PRINT 'Creating index UK_RelFileID on table RMFileVersions'
CREATE UNIQUE NONCLUSTERED INDEX [UK_RelFileID] ON [dbo].[RMFileVersions] 
(
	[CompanyID] ASC,
	[ReleaseID] ASC,
	[FileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RMFileVersions' AND COLUMN_NAME = 'CompanyID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_RMFileVersions_CompanyID')
BEGIN 
    PRINT 'Creating default constraint DF_RMFileVersions_CompanyID on table RMFileVersions' 
    ALTER TABLE [dbo].[RMFileVersions]
    ADD CONSTRAINT [DF_RMFileVersions_CompanyID] DEFAULT ((0)) FOR [CompanyID]
END 
GO

