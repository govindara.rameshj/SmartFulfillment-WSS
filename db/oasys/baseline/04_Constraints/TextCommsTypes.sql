﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TextCommsTypes' AND COLUMN_NAME = 'Deleted' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_TextCommsTypes_Deleted')
BEGIN 
    PRINT 'Creating default constraint DF_TextCommsTypes_Deleted on table TextCommsTypes' 
    ALTER TABLE [dbo].[TextCommsTypes]
    ADD CONSTRAINT [DF_TextCommsTypes_Deleted] DEFAULT ((0)) FOR [Deleted]
END 
GO

