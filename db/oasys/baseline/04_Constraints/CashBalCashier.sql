﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'GrossSalesAmount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_GrossSalesAmount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_GrossSalesAmount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_GrossSalesAmount] DEFAULT ((0)) FOR [GrossSalesAmount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'DiscountAmount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_DiscountAmount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_DiscountAmount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_DiscountAmount] DEFAULT ((0)) FOR [DiscountAmount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'SalesCount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_SalesCount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_SalesCount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_SalesCount] DEFAULT ((0)) FOR [SalesCount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'SalesAmount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_SalesAmount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_SalesAmount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_SalesAmount] DEFAULT ((0)) FOR [SalesAmount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'SalesCorrectCount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_SalesCorrectCount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_SalesCorrectCount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_SalesCorrectCount] DEFAULT ((0)) FOR [SalesCorrectCount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'SalesCorrectAmount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_SalesCorrectAmount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_SalesCorrectAmount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_SalesCorrectAmount] DEFAULT ((0)) FOR [SalesCorrectAmount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'RefundCount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_RefundCount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_RefundCount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_RefundCount] DEFAULT ((0)) FOR [RefundCount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'RefundAmount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_RefundAmount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_RefundAmount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_RefundAmount] DEFAULT ((0)) FOR [RefundAmount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'RefundCorrectCount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_RefundCorrectCount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_RefundCorrectCount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_RefundCorrectCount] DEFAULT ((0)) FOR [RefundCorrectCount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'RefundCorrectAmount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_RefundCorrectAmount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_RefundCorrectAmount on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_RefundCorrectAmount] DEFAULT ((0)) FOR [RefundCorrectAmount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'FloatIssued' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_FloatIssued')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_FloatIssued on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_FloatIssued] DEFAULT ((0)) FOR [FloatIssued]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'FloatReturned' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_FloatReturned')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_FloatReturned on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_FloatReturned] DEFAULT ((0)) FOR [FloatReturned]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount01' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount01')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount01 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount01] DEFAULT ((0)) FOR [MiscIncomeCount01]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount02' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount02')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount02 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount02] DEFAULT ((0)) FOR [MiscIncomeCount02]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount03' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount03')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount03 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount03] DEFAULT ((0)) FOR [MiscIncomeCount03]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount04' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount04')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount04 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount04] DEFAULT ((0)) FOR [MiscIncomeCount04]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount05' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount05')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount05 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount05] DEFAULT ((0)) FOR [MiscIncomeCount05]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount06' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount06')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount06 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount06] DEFAULT ((0)) FOR [MiscIncomeCount06]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount07' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount07')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount07 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount07] DEFAULT ((0)) FOR [MiscIncomeCount07]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount08' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount08')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount08 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount08] DEFAULT ((0)) FOR [MiscIncomeCount08]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount09' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount09')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount09 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount09] DEFAULT ((0)) FOR [MiscIncomeCount09]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount10' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount10')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount10 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount10] DEFAULT ((0)) FOR [MiscIncomeCount10]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount11' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount11')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount11 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount11] DEFAULT ((0)) FOR [MiscIncomeCount11]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount12' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount12')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount12 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount12] DEFAULT ((0)) FOR [MiscIncomeCount12]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount13' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount13')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount13 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount13] DEFAULT ((0)) FOR [MiscIncomeCount13]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount14' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount14')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount14 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount14] DEFAULT ((0)) FOR [MiscIncomeCount14]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount15' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount15')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount15 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount15] DEFAULT ((0)) FOR [MiscIncomeCount15]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount16' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount16')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount16 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount16] DEFAULT ((0)) FOR [MiscIncomeCount16]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount17' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount17')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount17 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount17] DEFAULT ((0)) FOR [MiscIncomeCount17]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount18' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount18')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount18 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount18] DEFAULT ((0)) FOR [MiscIncomeCount18]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount19' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount19')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount19 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount19] DEFAULT ((0)) FOR [MiscIncomeCount19]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeCount20' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeCount20')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeCount20 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeCount20] DEFAULT ((0)) FOR [MiscIncomeCount20]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue01' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue01')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue01 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue01] DEFAULT ((0)) FOR [MiscIncomeValue01]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue02' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue02')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue02 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue02] DEFAULT ((0)) FOR [MiscIncomeValue02]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue03' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue03')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue03 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue03] DEFAULT ((0)) FOR [MiscIncomeValue03]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue04' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue04')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue04 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue04] DEFAULT ((0)) FOR [MiscIncomeValue04]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue05' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue05')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue05 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue05] DEFAULT ((0)) FOR [MiscIncomeValue05]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue06' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue06')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue06 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue06] DEFAULT ((0)) FOR [MiscIncomeValue06]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue07' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue07')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue07 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue07] DEFAULT ((0)) FOR [MiscIncomeValue07]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue08' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue08')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue08 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue08] DEFAULT ((0)) FOR [MiscIncomeValue08]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue09' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue09')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue09 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue09] DEFAULT ((0)) FOR [MiscIncomeValue09]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue10' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue10')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue10 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue10] DEFAULT ((0)) FOR [MiscIncomeValue10]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue11' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue11')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue11 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue11] DEFAULT ((0)) FOR [MiscIncomeValue11]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue12' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue12')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue12 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue12] DEFAULT ((0)) FOR [MiscIncomeValue12]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue13' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue13')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue13 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue13] DEFAULT ((0)) FOR [MiscIncomeValue13]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue14' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue14')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue14 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue14] DEFAULT ((0)) FOR [MiscIncomeValue14]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue15' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue15')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue15 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue15] DEFAULT ((0)) FOR [MiscIncomeValue15]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue16' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue16')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue16 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue16] DEFAULT ((0)) FOR [MiscIncomeValue16]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue17' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue17')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue17 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue17] DEFAULT ((0)) FOR [MiscIncomeValue17]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue18' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue18')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue18 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue18] DEFAULT ((0)) FOR [MiscIncomeValue18]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue19' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue19')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue19 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue19] DEFAULT ((0)) FOR [MiscIncomeValue19]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MiscIncomeValue20' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MiscIncomeValue20')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MiscIncomeValue20 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MiscIncomeValue20] DEFAULT ((0)) FOR [MiscIncomeValue20]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount01' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount01')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount01 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount01] DEFAULT ((0)) FOR [MisOutCount01]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount02' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount02')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount02 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount02] DEFAULT ((0)) FOR [MisOutCount02]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount03' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount03')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount03 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount03] DEFAULT ((0)) FOR [MisOutCount03]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount04' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount04')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount04 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount04] DEFAULT ((0)) FOR [MisOutCount04]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount05' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount05')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount05 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount05] DEFAULT ((0)) FOR [MisOutCount05]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount06' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount06')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount06 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount06] DEFAULT ((0)) FOR [MisOutCount06]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount07' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount07')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount07 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount07] DEFAULT ((0)) FOR [MisOutCount07]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount08' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount08')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount08 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount08] DEFAULT ((0)) FOR [MisOutCount08]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount09' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount09')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount09 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount09] DEFAULT ((0)) FOR [MisOutCount09]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount10' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount10')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount10 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount10] DEFAULT ((0)) FOR [MisOutCount10]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount11' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount11')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount11 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount11] DEFAULT ((0)) FOR [MisOutCount11]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount12' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount12')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount12 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount12] DEFAULT ((0)) FOR [MisOutCount12]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount13' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount13')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount13 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount13] DEFAULT ((0)) FOR [MisOutCount13]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount14' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount14')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount14 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount14] DEFAULT ((0)) FOR [MisOutCount14]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount15' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount15')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount15 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount15] DEFAULT ((0)) FOR [MisOutCount15]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount16' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount16')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount16 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount16] DEFAULT ((0)) FOR [MisOutCount16]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount17' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount17')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount17 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount17] DEFAULT ((0)) FOR [MisOutCount17]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount18' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount18')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount18 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount18] DEFAULT ((0)) FOR [MisOutCount18]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount19' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount19')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount19 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount19] DEFAULT ((0)) FOR [MisOutCount19]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutCount20' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutCount20')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutCount20 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutCount20] DEFAULT ((0)) FOR [MisOutCount20]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue01' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue01')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue01 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue01] DEFAULT ((0)) FOR [MisOutValue01]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue02' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue02')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue02 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue02] DEFAULT ((0)) FOR [MisOutValue02]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue03' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue03')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue03 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue03] DEFAULT ((0)) FOR [MisOutValue03]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue04' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue04')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue04 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue04] DEFAULT ((0)) FOR [MisOutValue04]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue05' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue05')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue05 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue05] DEFAULT ((0)) FOR [MisOutValue05]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue06' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue06')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue06 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue06] DEFAULT ((0)) FOR [MisOutValue06]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue07' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue07')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue07 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue07] DEFAULT ((0)) FOR [MisOutValue07]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue08' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue08')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue08 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue08] DEFAULT ((0)) FOR [MisOutValue08]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue09' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue09')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue09 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue09] DEFAULT ((0)) FOR [MisOutValue09]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue10' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue10')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue10 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue10] DEFAULT ((0)) FOR [MisOutValue10]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue11' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue11')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue11 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue11] DEFAULT ((0)) FOR [MisOutValue11]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue12' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue12')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue12 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue12] DEFAULT ((0)) FOR [MisOutValue12]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue13' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue13')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue13 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue13] DEFAULT ((0)) FOR [MisOutValue13]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue14' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue14')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue14 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue14] DEFAULT ((0)) FOR [MisOutValue14]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue15' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue15')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue15 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue15] DEFAULT ((0)) FOR [MisOutValue15]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue16' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue16')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue16 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue16] DEFAULT ((0)) FOR [MisOutValue16]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue17' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue17')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue17 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue17] DEFAULT ((0)) FOR [MisOutValue17]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue18' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue18')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue18 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue18] DEFAULT ((0)) FOR [MisOutValue18]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue19' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue19')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue19 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue19] DEFAULT ((0)) FOR [MisOutValue19]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'MisOutValue20' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_MisOutValue20')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_MisOutValue20 on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_MisOutValue20] DEFAULT ((0)) FOR [MisOutValue20]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'ExchangeRate' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_ExchangeRate')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_ExchangeRate on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_ExchangeRate] DEFAULT ((0)) FOR [ExchangeRate]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'ExchangePower' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_ExchangePower')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_ExchangePower on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_ExchangePower] DEFAULT ((0)) FOR [ExchangePower]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'NumTransactions' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_NumTransactions')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_NumTransactions on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_NumTransactions] DEFAULT ((0)) FOR [NumTransactions]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'NumCorrections' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_NumCorrections')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_NumCorrections on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_NumCorrections] DEFAULT ((0)) FOR [NumCorrections]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'NumVoids' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_NumVoids')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_NumVoids on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_NumVoids] DEFAULT ((0)) FOR [NumVoids]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'NumOpenDrawer' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_NumOpenDrawer')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_NumOpenDrawer on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_NumOpenDrawer] DEFAULT ((0)) FOR [NumOpenDrawer]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'NumLinesReversed' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_NumLinesReversed')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_NumLinesReversed on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_NumLinesReversed] DEFAULT ((0)) FOR [NumLinesReversed]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'NumLinesSold' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_NumLinesSold')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_NumLinesSold on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_NumLinesSold] DEFAULT ((0)) FOR [NumLinesSold]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashier' AND COLUMN_NAME = 'NumLinesScanned' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashier_NumLinesScanned')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashier_NumLinesScanned on table CashBalCashier' 
    ALTER TABLE [dbo].[CashBalCashier]
    ADD CONSTRAINT [DF_CashBalCashier_NumLinesScanned] DEFAULT ((0)) FOR [NumLinesScanned]
END 
GO

