﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[QUOHDR]') AND name = N'XQUOHDR1')
BEGIN 
PRINT 'Creating index XQUOHDR1 on table QUOHDR'
CREATE UNIQUE NONCLUSTERED INDEX [XQUOHDR1] ON [dbo].[QUOHDR] 
(
	[CUST] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[QUOHDR]') AND name = N'XQUOHDR2')
BEGIN 
PRINT 'Creating index XQUOHDR2 on table QUOHDR'
CREATE UNIQUE NONCLUSTERED INDEX [XQUOHDR2] ON [dbo].[QUOHDR] 
(
	[DATE1] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'QUOHDR' AND COLUMN_NAME = 'CANC' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_QUOHDR_CANC')
BEGIN 
    PRINT 'Creating default constraint DF_QUOHDR_CANC on table QUOHDR' 
    ALTER TABLE [dbo].[QUOHDR]
    ADD CONSTRAINT [DF_QUOHDR_CANC] DEFAULT ((0)) FOR [CANC]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'QUOHDR' AND COLUMN_NAME = 'SOLD' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_QUOHDR_SOLD')
BEGIN 
    PRINT 'Creating default constraint DF_QUOHDR_SOLD on table QUOHDR' 
    ALTER TABLE [dbo].[QUOHDR]
    ADD CONSTRAINT [DF_QUOHDR_SOLD] DEFAULT ((0)) FOR [SOLD]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'QUOHDR' AND COLUMN_NAME = 'DELI' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_QUOHDR_DELI')
BEGIN 
    PRINT 'Creating default constraint DF_QUOHDR_DELI on table QUOHDR' 
    ALTER TABLE [dbo].[QUOHDR]
    ADD CONSTRAINT [DF_QUOHDR_DELI] DEFAULT ((0)) FOR [DELI]
END 
GO

