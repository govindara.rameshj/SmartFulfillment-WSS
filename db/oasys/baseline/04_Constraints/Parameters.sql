﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Parameters' AND COLUMN_NAME = 'BooleanValue' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Parameters_BooleanValue')
BEGIN 
    PRINT 'Creating default constraint DF_Parameters_BooleanValue on table Parameters' 
    ALTER TABLE [dbo].[Parameters]
    ADD CONSTRAINT [DF_Parameters_BooleanValue] DEFAULT ((0)) FOR [BooleanValue]
END 
GO

