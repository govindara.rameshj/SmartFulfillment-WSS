﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionLine' AND COLUMN_NAME = 'Quantity' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionLine_Quantity')
BEGIN 
    PRINT 'Creating default constraint DF_VisionLine_Quantity on table VisionLine' 
    ALTER TABLE [dbo].[VisionLine]
    ADD CONSTRAINT [DF_VisionLine_Quantity] DEFAULT ((0)) FOR [Quantity]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionLine' AND COLUMN_NAME = 'PriceLookup' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionLine_PriceLookup')
BEGIN 
    PRINT 'Creating default constraint DF_VisionLine_PriceLookup on table VisionLine' 
    ALTER TABLE [dbo].[VisionLine]
    ADD CONSTRAINT [DF_VisionLine_PriceLookup] DEFAULT ((0)) FOR [PriceLookup]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionLine' AND COLUMN_NAME = 'Price' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionLine_Price')
BEGIN 
    PRINT 'Creating default constraint DF_VisionLine_Price on table VisionLine' 
    ALTER TABLE [dbo].[VisionLine]
    ADD CONSTRAINT [DF_VisionLine_Price] DEFAULT ((0)) FOR [Price]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionLine' AND COLUMN_NAME = 'PriceExVat' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionLine_PriceExVat')
BEGIN 
    PRINT 'Creating default constraint DF_VisionLine_PriceExVat on table VisionLine' 
    ALTER TABLE [dbo].[VisionLine]
    ADD CONSTRAINT [DF_VisionLine_PriceExVat] DEFAULT ((0)) FOR [PriceExVat]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionLine' AND COLUMN_NAME = 'PriceExtended' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionLine_PriceExtended')
BEGIN 
    PRINT 'Creating default constraint DF_VisionLine_PriceExtended on table VisionLine' 
    ALTER TABLE [dbo].[VisionLine]
    ADD CONSTRAINT [DF_VisionLine_PriceExtended] DEFAULT ((0)) FOR [PriceExtended]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionLine' AND COLUMN_NAME = 'IsRelatedItemSingle' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionLine_IsRelatedItemSingle')
BEGIN 
    PRINT 'Creating default constraint DF_VisionLine_IsRelatedItemSingle on table VisionLine' 
    ALTER TABLE [dbo].[VisionLine]
    ADD CONSTRAINT [DF_VisionLine_IsRelatedItemSingle] DEFAULT ((0)) FOR [IsRelatedItemSingle]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionLine' AND COLUMN_NAME = 'IsInStoreStockItem' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionLine_IsInStoreStockItem')
BEGIN 
    PRINT 'Creating default constraint DF_VisionLine_IsInStoreStockItem on table VisionLine' 
    ALTER TABLE [dbo].[VisionLine]
    ADD CONSTRAINT [DF_VisionLine_IsInStoreStockItem] DEFAULT ((0)) FOR [IsInStoreStockItem]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionLine' AND COLUMN_NAME = 'IsPivotal' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionLine_IsPivotal')
BEGIN 
    PRINT 'Creating default constraint DF_VisionLine_IsPivotal on table VisionLine' 
    ALTER TABLE [dbo].[VisionLine]
    ADD CONSTRAINT [DF_VisionLine_IsPivotal] DEFAULT ((0)) FOR [IsPivotal]
END 
GO

