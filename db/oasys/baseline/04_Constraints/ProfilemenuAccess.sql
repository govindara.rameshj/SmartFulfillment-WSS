﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ProfilemenuAccess' AND COLUMN_NAME = 'MenuConfigID')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'ProfilemenuAccess' AND CONSTRAINT_NAME = 'FK_ProfileMenuAccess_MenuConfig')
BEGIN 
PRINT 'Creating foreign key FK_ProfileMenuAccess_MenuConfig on table ProfilemenuAccess'
ALTER TABLE [dbo].[ProfilemenuAccess]  WITH CHECK ADD  CONSTRAINT [FK_ProfileMenuAccess_MenuConfig] FOREIGN KEY([MenuConfigID])
REFERENCES [MenuConfig] ([ID])
ON DELETE CASCADE
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ProfilemenuAccess' AND COLUMN_NAME = 'AccessAllowed' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ProfilemenuAccess_AccessAllowed')
BEGIN 
    PRINT 'Creating default constraint DF_ProfilemenuAccess_AccessAllowed on table ProfilemenuAccess' 
    ALTER TABLE [dbo].[ProfilemenuAccess]
    ADD CONSTRAINT [DF_ProfilemenuAccess_AccessAllowed] DEFAULT ((1)) FOR [AccessAllowed]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ProfilemenuAccess' AND COLUMN_NAME = 'OverrideAllowed' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ProfilemenuAccess_OverrideAllowed')
BEGIN 
    PRINT 'Creating default constraint DF_ProfilemenuAccess_OverrideAllowed on table ProfilemenuAccess' 
    ALTER TABLE [dbo].[ProfilemenuAccess]
    ADD CONSTRAINT [DF_ProfilemenuAccess_OverrideAllowed] DEFAULT ((0)) FOR [OverrideAllowed]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ProfilemenuAccess' AND COLUMN_NAME = 'IsManager' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ProfilemenuAccess_IsManager')
BEGIN 
    PRINT 'Creating default constraint DF_ProfilemenuAccess_IsManager on table ProfilemenuAccess' 
    ALTER TABLE [dbo].[ProfilemenuAccess]
    ADD CONSTRAINT [DF_ProfilemenuAccess_IsManager] DEFAULT ((0)) FOR [IsManager]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ProfilemenuAccess' AND COLUMN_NAME = 'IsSupervisor' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ProfilemenuAccess_IsSupervisor')
BEGIN 
    PRINT 'Creating default constraint DF_ProfilemenuAccess_IsSupervisor on table ProfilemenuAccess' 
    ALTER TABLE [dbo].[ProfilemenuAccess]
    ADD CONSTRAINT [DF_ProfilemenuAccess_IsSupervisor] DEFAULT ((0)) FOR [IsSupervisor]
END 
GO

