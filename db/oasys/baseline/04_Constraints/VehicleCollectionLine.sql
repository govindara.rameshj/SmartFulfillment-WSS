﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VehicleCollectionLine' AND COLUMN_NAME = 'CollectionID')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'VehicleCollectionLine' AND CONSTRAINT_NAME = 'FK_VehicleCollectionLine_VehicleCollectionHeader')
BEGIN 
PRINT 'Creating foreign key FK_VehicleCollectionLine_VehicleCollectionHeader on table VehicleCollectionLine'
ALTER TABLE [dbo].[VehicleCollectionLine]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCollectionLine_VehicleCollectionHeader] FOREIGN KEY([CollectionID])
REFERENCES [VehicleCollectionHeader] ([CollectionID])
ON DELETE CASCADE
END
GO

