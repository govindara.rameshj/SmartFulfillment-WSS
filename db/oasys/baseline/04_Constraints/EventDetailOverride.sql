﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'EventDetailOverride' AND COLUMN_NAME = 'EventHeaderOverrideID')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'EventDetailOverride' AND CONSTRAINT_NAME = 'FK_EventDetailOverride_EventHeaderOverrideID')
BEGIN 
PRINT 'Creating foreign key FK_EventDetailOverride_EventHeaderOverrideID on table EventDetailOverride'
ALTER TABLE [dbo].[EventDetailOverride]  WITH CHECK ADD  CONSTRAINT [FK_EventDetailOverride_EventHeaderOverrideID] FOREIGN KEY([EventHeaderOverrideID])
REFERENCES [EventHeaderOverride] ([ID])
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'EventDetailOverride' AND COLUMN_NAME = 'SkuNumber')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'EventDetailOverride' AND CONSTRAINT_NAME = 'FK_EventDetailOverride_SkuNumber')
BEGIN 
PRINT 'Creating foreign key FK_EventDetailOverride_SkuNumber on table EventDetailOverride'
ALTER TABLE [dbo].[EventDetailOverride]  WITH CHECK ADD  CONSTRAINT [FK_EventDetailOverride_SkuNumber] FOREIGN KEY([SkuNumber])
REFERENCES [STKMAS] ([SKUN])
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventDetailOverride]') AND name = N'IDX_EventDetailOverride')
BEGIN 
PRINT 'Creating index IDX_EventDetailOverride on table EventDetailOverride'
CREATE UNIQUE NONCLUSTERED INDEX [IDX_EventDetailOverride] ON [dbo].[EventDetailOverride] 
(
	[EventHeaderOverrideID] ASC,
	[SkuNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

