﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CompetitorFixedList' AND COLUMN_NAME = 'DeleteFlag' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CompetitorFixedList_DeleteFlag')
BEGIN 
    PRINT 'Creating default constraint DF_CompetitorFixedList_DeleteFlag on table CompetitorFixedList' 
    ALTER TABLE [dbo].[CompetitorFixedList]
    ADD CONSTRAINT [DF_CompetitorFixedList_DeleteFlag] DEFAULT ((0)) FOR [DeleteFlag]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CompetitorFixedList' AND COLUMN_NAME = 'TPGroup' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CompetitorFixedList_TPGroup')
BEGIN 
    PRINT 'Creating default constraint DF_CompetitorFixedList_TPGroup on table CompetitorFixedList' 
    ALTER TABLE [dbo].[CompetitorFixedList]
    ADD CONSTRAINT [DF_CompetitorFixedList_TPGroup] DEFAULT ((0)) FOR [TPGroup]
END 
GO

