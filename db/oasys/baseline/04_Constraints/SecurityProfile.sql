﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SecurityProfile' AND COLUMN_NAME = 'IsSupervisor' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SecurityProfile_IsSupervisor')
BEGIN 
    PRINT 'Creating default constraint DF_SecurityProfile_IsSupervisor on table SecurityProfile' 
    ALTER TABLE [dbo].[SecurityProfile]
    ADD CONSTRAINT [DF_SecurityProfile_IsSupervisor] DEFAULT ((0)) FOR [IsSupervisor]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SecurityProfile' AND COLUMN_NAME = 'IsManager' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SecurityProfile_IsManager')
BEGIN 
    PRINT 'Creating default constraint DF_SecurityProfile_IsManager on table SecurityProfile' 
    ALTER TABLE [dbo].[SecurityProfile]
    ADD CONSTRAINT [DF_SecurityProfile_IsManager] DEFAULT ((0)) FOR [IsManager]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SecurityProfile' AND COLUMN_NAME = 'IsAreaManager' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SecurityProfile_IsAreaManager')
BEGIN 
    PRINT 'Creating default constraint DF_SecurityProfile_IsAreaManager on table SecurityProfile' 
    ALTER TABLE [dbo].[SecurityProfile]
    ADD CONSTRAINT [DF_SecurityProfile_IsAreaManager] DEFAULT ((0)) FOR [IsAreaManager]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SecurityProfile' AND COLUMN_NAME = 'PasswordValidFor' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SecurityProfile_PasswordValidFor')
BEGIN 
    PRINT 'Creating default constraint DF_SecurityProfile_PasswordValidFor on table SecurityProfile' 
    ALTER TABLE [dbo].[SecurityProfile]
    ADD CONSTRAINT [DF_SecurityProfile_PasswordValidFor] DEFAULT ((30)) FOR [PasswordValidFor]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SecurityProfile' AND COLUMN_NAME = 'IsDeleted' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SecurityProfile_IsDeleted')
BEGIN 
    PRINT 'Creating default constraint DF_SecurityProfile_IsDeleted on table SecurityProfile' 
    ALTER TABLE [dbo].[SecurityProfile]
    ADD CONSTRAINT [DF_SecurityProfile_IsDeleted] DEFAULT ((0)) FOR [IsDeleted]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SecurityProfile' AND COLUMN_NAME = 'IsSystemAdmin' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SecurityProfile_IsSystemAdmin')
BEGIN 
    PRINT 'Creating default constraint DF_SecurityProfile_IsSystemAdmin on table SecurityProfile' 
    ALTER TABLE [dbo].[SecurityProfile]
    ADD CONSTRAINT [DF_SecurityProfile_IsSystemAdmin] DEFAULT ((0)) FOR [IsSystemAdmin]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SecurityProfile' AND COLUMN_NAME = 'IsHelpDeskUser' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SecurityProfile_IsHelpDeskUser')
BEGIN 
    PRINT 'Creating default constraint DF_SecurityProfile_IsHelpDeskUser on table SecurityProfile' 
    ALTER TABLE [dbo].[SecurityProfile]
    ADD CONSTRAINT [DF_SecurityProfile_IsHelpDeskUser] DEFAULT ((0)) FOR [IsHelpDeskUser]
END 
GO

