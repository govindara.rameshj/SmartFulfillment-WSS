﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AUDMAS' AND COLUMN_NAME = 'FLAG' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_AUDMAS_FLAG')
BEGIN 
    PRINT 'Creating default constraint DF_AUDMAS_FLAG on table AUDMAS' 
    ALTER TABLE [dbo].[AUDMAS]
    ADD CONSTRAINT [DF_AUDMAS_FLAG] DEFAULT ((0)) FOR [FLAG]
END 
GO

