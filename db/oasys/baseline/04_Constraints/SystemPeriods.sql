﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemPeriods' AND COLUMN_NAME = 'IsClosed' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemPeriods_IsClosed')
BEGIN 
    PRINT 'Creating default constraint DF_SystemPeriods_IsClosed on table SystemPeriods' 
    ALTER TABLE [dbo].[SystemPeriods]
    ADD CONSTRAINT [DF_SystemPeriods_IsClosed] DEFAULT ((0)) FOR [IsClosed]
END 
GO

