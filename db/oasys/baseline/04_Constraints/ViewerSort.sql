﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ViewerSort]') AND name = N'IX_ViewerSort')
BEGIN 
PRINT 'Creating index IX_ViewerSort on table ViewerSort'
ALTER TABLE [dbo].[ViewerSort] ADD  CONSTRAINT [IX_ViewerSort] UNIQUE NONCLUSTERED 
(
	[ConfigID] ASC,
	[DisplayOrder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

