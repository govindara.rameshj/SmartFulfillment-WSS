﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemCurrencyDen' AND COLUMN_NAME = 'BullionMultiple' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemCurrencyDen_BullionMultiple')
BEGIN 
    PRINT 'Creating default constraint DF_SystemCurrencyDen_BullionMultiple on table SystemCurrencyDen' 
    ALTER TABLE [dbo].[SystemCurrencyDen]
    ADD CONSTRAINT [DF_SystemCurrencyDen_BullionMultiple] DEFAULT ((0)) FOR [BullionMultiple]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemCurrencyDen' AND COLUMN_NAME = 'BankingBagLimit' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemCurrencyDen_BankingBagLimit')
BEGIN 
    PRINT 'Creating default constraint DF_SystemCurrencyDen_BankingBagLimit on table SystemCurrencyDen' 
    ALTER TABLE [dbo].[SystemCurrencyDen]
    ADD CONSTRAINT [DF_SystemCurrencyDen_BankingBagLimit] DEFAULT ((0)) FOR [BankingBagLimit]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemCurrencyDen' AND COLUMN_NAME = 'SafeMinimum' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemCurrencyDen_SafeMinimum')
BEGIN 
    PRINT 'Creating default constraint DF_SystemCurrencyDen_SafeMinimum on table SystemCurrencyDen' 
    ALTER TABLE [dbo].[SystemCurrencyDen]
    ADD CONSTRAINT [DF_SystemCurrencyDen_SafeMinimum] DEFAULT ((0)) FOR [SafeMinimum]
END 
GO

