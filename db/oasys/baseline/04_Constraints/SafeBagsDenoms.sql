﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeBagsDenoms' AND COLUMN_NAME = 'TenderID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeBagsDenoms_TenderID')
BEGIN 
    PRINT 'Creating default constraint DF_SafeBagsDenoms_TenderID on table SafeBagsDenoms' 
    ALTER TABLE [dbo].[SafeBagsDenoms]
    ADD CONSTRAINT [DF_SafeBagsDenoms_TenderID] DEFAULT ((1)) FOR [TenderID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeBagsDenoms' AND COLUMN_NAME = 'ID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeBagsDenoms_ID')
BEGIN 
    PRINT 'Creating default constraint DF_SafeBagsDenoms_ID on table SafeBagsDenoms' 
    ALTER TABLE [dbo].[SafeBagsDenoms]
    ADD CONSTRAINT [DF_SafeBagsDenoms_ID] DEFAULT ((0.01)) FOR [ID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeBagsDenoms' AND COLUMN_NAME = 'Value' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeBagsDenoms_Value')
BEGIN 
    PRINT 'Creating default constraint DF_SafeBagsDenoms_Value on table SafeBagsDenoms' 
    ALTER TABLE [dbo].[SafeBagsDenoms]
    ADD CONSTRAINT [DF_SafeBagsDenoms_Value] DEFAULT ((0)) FOR [Value]
END 
GO

