﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PRCCHG]') AND name = N'XPRCCHG1')
BEGIN 
PRINT 'Creating index XPRCCHG1 on table PRCCHG'
CREATE UNIQUE NONCLUSTERED INDEX [XPRCCHG1] ON [dbo].[PRCCHG] 
(
	[PDAT] ASC,
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PRCCHG' AND COLUMN_NAME = 'SHEL' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PRCCHG_SHEL')
BEGIN 
    PRINT 'Creating default constraint DF_PRCCHG_SHEL on table PRCCHG' 
    ALTER TABLE [dbo].[PRCCHG]
    ADD CONSTRAINT [DF_PRCCHG_SHEL] DEFAULT ((0)) FOR [SHEL]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PRCCHG' AND COLUMN_NAME = 'MCOM' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PRCCHG_MCOM')
BEGIN 
    PRINT 'Creating default constraint DF_PRCCHG_MCOM on table PRCCHG' 
    ALTER TABLE [dbo].[PRCCHG]
    ADD CONSTRAINT [DF_PRCCHG_MCOM] DEFAULT ((0)) FOR [MCOM]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PRCCHG' AND COLUMN_NAME = 'LABS' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PRCCHG_LABS')
BEGIN 
    PRINT 'Creating default constraint DF_PRCCHG_LABS on table PRCCHG' 
    ALTER TABLE [dbo].[PRCCHG]
    ADD CONSTRAINT [DF_PRCCHG_LABS] DEFAULT ((0)) FOR [LABS]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PRCCHG' AND COLUMN_NAME = 'LABM' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PRCCHG_LABM')
BEGIN 
    PRINT 'Creating default constraint DF_PRCCHG_LABM on table PRCCHG' 
    ALTER TABLE [dbo].[PRCCHG]
    ADD CONSTRAINT [DF_PRCCHG_LABM] DEFAULT ((0)) FOR [LABM]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PRCCHG' AND COLUMN_NAME = 'LABL' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PRCCHG_LABL')
BEGIN 
    PRINT 'Creating default constraint DF_PRCCHG_LABL on table PRCCHG' 
    ALTER TABLE [dbo].[PRCCHG]
    ADD CONSTRAINT [DF_PRCCHG_LABL] DEFAULT ((0)) FOR [LABL]
END 
GO

