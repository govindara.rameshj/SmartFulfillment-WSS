﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RMCompany' AND COLUMN_NAME = 'CompanyID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_RMCompany_CompanyID')
BEGIN 
    PRINT 'Creating default constraint DF_RMCompany_CompanyID on table RMCompany' 
    ALTER TABLE [dbo].[RMCompany]
    ADD CONSTRAINT [DF_RMCompany_CompanyID] DEFAULT ((0)) FOR [CompanyID]
END 
GO

