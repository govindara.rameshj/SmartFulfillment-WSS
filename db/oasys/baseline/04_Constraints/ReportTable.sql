﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'AllowGrouping' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_AllowGrouping')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_AllowGrouping on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_AllowGrouping] DEFAULT ((0)) FOR [AllowGrouping]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'AllowSelection' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_AllowSelection')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_AllowSelection on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_AllowSelection] DEFAULT ((0)) FOR [AllowSelection]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'ShowHeaders' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_ShowHeaders')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_ShowHeaders on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_ShowHeaders] DEFAULT ((1)) FOR [ShowHeaders]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'ShowIndicator' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_ShowIndicator')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_ShowIndicator on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_ShowIndicator] DEFAULT ((1)) FOR [ShowIndicator]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'ShowLinesVertical' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_ShowLinesVertical')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_ShowLinesVertical on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_ShowLinesVertical] DEFAULT ((1)) FOR [ShowLinesVertical]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'ShowLinesHorizontal' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_ShowLinesHorizontal')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_ShowLinesHorizontal on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_ShowLinesHorizontal] DEFAULT ((1)) FOR [ShowLinesHorizontal]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'ShowBorder' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_ShowBorder')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_ShowBorder on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_ShowBorder] DEFAULT ((1)) FOR [ShowBorder]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'ShowInPrintOnly' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_ShowInPrintOnly')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_ShowInPrintOnly on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_ShowInPrintOnly] DEFAULT ((0)) FOR [ShowInPrintOnly]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'AutoFitColumns' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_AutoFitColumns')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_AutoFitColumns on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_AutoFitColumns] DEFAULT ((1)) FOR [AutoFitColumns]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'AnchorTop' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_AnchorTop')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_AnchorTop on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_AnchorTop] DEFAULT ((1)) FOR [AnchorTop]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportTable' AND COLUMN_NAME = 'AnchorBottom' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportTable_AnchorBottom')
BEGIN 
    PRINT 'Creating default constraint DF_ReportTable_AnchorBottom on table ReportTable' 
    ALTER TABLE [dbo].[ReportTable]
    ADD CONSTRAINT [DF_ReportTable_AnchorBottom] DEFAULT ((1)) FOR [AnchorBottom]
END 
GO

