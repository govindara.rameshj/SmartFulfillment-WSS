﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportSummary' AND COLUMN_NAME = 'TableId' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportSummary_TableId')
BEGIN 
    PRINT 'Creating default constraint DF_ReportSummary_TableId on table ReportSummary' 
    ALTER TABLE [dbo].[ReportSummary]
    ADD CONSTRAINT [DF_ReportSummary_TableId] DEFAULT ((1)) FOR [TableId]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportSummary' AND COLUMN_NAME = 'ApplyToGroups' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportSummary_ApplyToGroups')
BEGIN 
    PRINT 'Creating default constraint DF_ReportSummary_ApplyToGroups on table ReportSummary' 
    ALTER TABLE [dbo].[ReportSummary]
    ADD CONSTRAINT [DF_ReportSummary_ApplyToGroups] DEFAULT ((0)) FOR [ApplyToGroups]
END 
GO

