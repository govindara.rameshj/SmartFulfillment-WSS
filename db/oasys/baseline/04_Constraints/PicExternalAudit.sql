﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PicExternalAudit' AND COLUMN_NAME = 'RTI' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PicExternalAudit_RTI')
BEGIN 
    PRINT 'Creating default constraint DF_PicExternalAudit_RTI on table PicExternalAudit' 
    ALTER TABLE [dbo].[PicExternalAudit]
    ADD CONSTRAINT [DF_PicExternalAudit_RTI] DEFAULT ('S') FOR [RTI]
END 
GO

