﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AuditMaster' AND COLUMN_NAME = 'NormalStock' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_AuditMaster_NormalStock')
BEGIN 
    PRINT 'Creating default constraint DF_AuditMaster_NormalStock on table AuditMaster' 
    ALTER TABLE [dbo].[AuditMaster]
    ADD CONSTRAINT [DF_AuditMaster_NormalStock] DEFAULT ((0)) FOR [NormalStock]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AuditMaster' AND COLUMN_NAME = 'NormalCount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_AuditMaster_NormalCount')
BEGIN 
    PRINT 'Creating default constraint DF_AuditMaster_NormalCount on table AuditMaster' 
    ALTER TABLE [dbo].[AuditMaster]
    ADD CONSTRAINT [DF_AuditMaster_NormalCount] DEFAULT ((0)) FOR [NormalCount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AuditMaster' AND COLUMN_NAME = 'MarkdownStock' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_AuditMaster_MarkdownStock')
BEGIN 
    PRINT 'Creating default constraint DF_AuditMaster_MarkdownStock on table AuditMaster' 
    ALTER TABLE [dbo].[AuditMaster]
    ADD CONSTRAINT [DF_AuditMaster_MarkdownStock] DEFAULT ((0)) FOR [MarkdownStock]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AuditMaster' AND COLUMN_NAME = 'MarkdownCount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_AuditMaster_MarkdownCount')
BEGIN 
    PRINT 'Creating default constraint DF_AuditMaster_MarkdownCount on table AuditMaster' 
    ALTER TABLE [dbo].[AuditMaster]
    ADD CONSTRAINT [DF_AuditMaster_MarkdownCount] DEFAULT ((0)) FOR [MarkdownCount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AuditMaster' AND COLUMN_NAME = 'SalesYearToDate' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_AuditMaster_SalesYearToDate')
BEGIN 
    PRINT 'Creating default constraint DF_AuditMaster_SalesYearToDate on table AuditMaster' 
    ALTER TABLE [dbo].[AuditMaster]
    ADD CONSTRAINT [DF_AuditMaster_SalesYearToDate] DEFAULT ((0)) FOR [SalesYearToDate]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AuditMaster' AND COLUMN_NAME = 'SalesPreviousYear' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_AuditMaster_SalesPreviousYear')
BEGIN 
    PRINT 'Creating default constraint DF_AuditMaster_SalesPreviousYear on table AuditMaster' 
    ALTER TABLE [dbo].[AuditMaster]
    ADD CONSTRAINT [DF_AuditMaster_SalesPreviousYear] DEFAULT ((0)) FOR [SalesPreviousYear]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AuditMaster' AND COLUMN_NAME = 'EntryMade' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_AuditMaster_EntryMade')
BEGIN 
    PRINT 'Creating default constraint DF_AuditMaster_EntryMade on table AuditMaster' 
    ALTER TABLE [dbo].[AuditMaster]
    ADD CONSTRAINT [DF_AuditMaster_EntryMade] DEFAULT ((0)) FOR [EntryMade]
END 
GO

