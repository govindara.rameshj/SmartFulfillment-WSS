﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DeliveryChargeGroupValue' AND COLUMN_NAME = 'Id')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'DeliveryChargeGroupValue' AND CONSTRAINT_NAME = 'FK_DeliveryChargeGroupValue_DeliveryChargeGroup')
BEGIN 
PRINT 'Creating foreign key FK_DeliveryChargeGroupValue_DeliveryChargeGroup on table DeliveryChargeGroupValue'
ALTER TABLE [dbo].[DeliveryChargeGroupValue]  WITH CHECK ADD  CONSTRAINT [FK_DeliveryChargeGroupValue_DeliveryChargeGroup] FOREIGN KEY([Id], [Group])
REFERENCES [DeliveryChargeGroup] ([Id], [Group])
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DeliveryChargeGroupValue]') AND name = N'UQ__Delivery__07D9BBC21229A90A')
BEGIN 
PRINT 'Creating index UQ__Delivery__07D9BBC21229A90A on table DeliveryChargeGroupValue'
ALTER TABLE [dbo].[DeliveryChargeGroupValue] ADD UNIQUE NONCLUSTERED 
(
	[Value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DeliveryChargeGroupValue]') AND name = N'UQ__Delivery__07D9BBC25CD6CB2B')
BEGIN 
PRINT 'Creating index UQ__Delivery__07D9BBC25CD6CB2B on table DeliveryChargeGroupValue'
ALTER TABLE [dbo].[DeliveryChargeGroupValue] ADD UNIQUE NONCLUSTERED 
(
	[Value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

