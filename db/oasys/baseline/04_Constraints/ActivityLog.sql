﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ActivityLog' AND COLUMN_NAME = 'LoggedIn' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ActivityLog_LoggedIn')
BEGIN 
    PRINT 'Creating default constraint DF_ActivityLog_LoggedIn on table ActivityLog' 
    ALTER TABLE [dbo].[ActivityLog]
    ADD CONSTRAINT [DF_ActivityLog_LoggedIn] DEFAULT ((0)) FOR [LoggedIn]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ActivityLog' AND COLUMN_NAME = 'ForcedLogOut' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ActivityLog_ForcedLogOut')
BEGIN 
    PRINT 'Creating default constraint DF_ActivityLog_ForcedLogOut on table ActivityLog' 
    ALTER TABLE [dbo].[ActivityLog]
    ADD CONSTRAINT [DF_ActivityLog_ForcedLogOut] DEFAULT ((0)) FOR [ForcedLogOut]
END 
GO

