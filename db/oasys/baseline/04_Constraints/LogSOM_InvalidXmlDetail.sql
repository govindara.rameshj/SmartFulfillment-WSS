﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'LogSOM_InvalidXmlDetail' AND COLUMN_NAME = 'LogID')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'LogSOM_InvalidXmlDetail' AND CONSTRAINT_NAME = 'FK_LogSOM_InvalidXml')
BEGIN 
PRINT 'Creating foreign key FK_LogSOM_InvalidXml on table LogSOM_InvalidXmlDetail'
ALTER TABLE [dbo].[LogSOM_InvalidXmlDetail]  WITH CHECK ADD  CONSTRAINT [FK_LogSOM_InvalidXml] FOREIGN KEY([LogID])
REFERENCES [LogSOM_InvalidXml] ([ID])
END
GO

