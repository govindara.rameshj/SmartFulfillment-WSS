﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportParameters' AND COLUMN_NAME = 'AllowMultiple' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportParameters_AllowMultiple')
BEGIN 
    PRINT 'Creating default constraint DF_ReportParameters_AllowMultiple on table ReportParameters' 
    ALTER TABLE [dbo].[ReportParameters]
    ADD CONSTRAINT [DF_ReportParameters_AllowMultiple] DEFAULT ((0)) FOR [AllowMultiple]
END 
GO

