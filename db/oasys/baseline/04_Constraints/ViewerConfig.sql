﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerConfig' AND COLUMN_NAME = 'IsHorizontal' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerConfig_IsHorizontal')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerConfig_IsHorizontal on table ViewerConfig' 
    ALTER TABLE [dbo].[ViewerConfig]
    ADD CONSTRAINT [DF_ViewerConfig_IsHorizontal] DEFAULT ((0)) FOR [IsHorizontal]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerConfig' AND COLUMN_NAME = 'RecordLimit' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerConfig_RecordLimit')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerConfig_RecordLimit on table ViewerConfig' 
    ALTER TABLE [dbo].[ViewerConfig]
    ADD CONSTRAINT [DF_ViewerConfig_RecordLimit] DEFAULT ((0)) FOR [RecordLimit]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerConfig' AND COLUMN_NAME = 'AllowAddition' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerConfig_AllowAddition')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerConfig_AllowAddition on table ViewerConfig' 
    ALTER TABLE [dbo].[ViewerConfig]
    ADD CONSTRAINT [DF_ViewerConfig_AllowAddition] DEFAULT ((0)) FOR [AllowAddition]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerConfig' AND COLUMN_NAME = 'AllowDeletion' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerConfig_AllowDeletion')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerConfig_AllowDeletion on table ViewerConfig' 
    ALTER TABLE [dbo].[ViewerConfig]
    ADD CONSTRAINT [DF_ViewerConfig_AllowDeletion] DEFAULT ((0)) FOR [AllowDeletion]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerConfig' AND COLUMN_NAME = 'AllowUpdate' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerConfig_AllowUpdate')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerConfig_AllowUpdate on table ViewerConfig' 
    ALTER TABLE [dbo].[ViewerConfig]
    ADD CONSTRAINT [DF_ViewerConfig_AllowUpdate] DEFAULT ((0)) FOR [AllowUpdate]
END 
GO

