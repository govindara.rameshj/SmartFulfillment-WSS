﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND1' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND1')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND1 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND1] DEFAULT ((0)) FOR [TEND1]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND2' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND2')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND2 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND2] DEFAULT ((0)) FOR [TEND2]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND3' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND3')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND3 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND3] DEFAULT ((0)) FOR [TEND3]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND4' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND4')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND4 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND4] DEFAULT ((0)) FOR [TEND4]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND5' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND5')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND5 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND5] DEFAULT ((0)) FOR [TEND5]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND6' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND6')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND6 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND6] DEFAULT ((0)) FOR [TEND6]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND7' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND7')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND7 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND7] DEFAULT ((0)) FOR [TEND7]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND8' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND8')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND8 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND8] DEFAULT ((0)) FOR [TEND8]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND9' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND9')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND9 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND9] DEFAULT ((0)) FOR [TEND9]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND10' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND10')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND10 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND10] DEFAULT ((0)) FOR [TEND10]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND11' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND11')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND11 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND11] DEFAULT ((0)) FOR [TEND11]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND12' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND12')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND12 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND12] DEFAULT ((0)) FOR [TEND12]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND13' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND13')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND13 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND13] DEFAULT ((0)) FOR [TEND13]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND14' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND14')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND14 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND14] DEFAULT ((0)) FOR [TEND14]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND15' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND15')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND15 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND15] DEFAULT ((0)) FOR [TEND15]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND16' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND16')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND16 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND16] DEFAULT ((0)) FOR [TEND16]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND17' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND17')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND17 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND17] DEFAULT ((0)) FOR [TEND17]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND18' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND18')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND18 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND18] DEFAULT ((0)) FOR [TEND18]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND19' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND19')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND19 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND19] DEFAULT ((0)) FOR [TEND19]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'TEND20' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_TEND20')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_TEND20 on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_TEND20] DEFAULT ((0)) FOR [TEND20]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'SUPV' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_SUPV')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_SUPV on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_SUPV] DEFAULT ((0)) FOR [SUPV]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'DOCN' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_DOCN')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_DOCN on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_DOCN] DEFAULT ((0)) FOR [DOCN]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'SPRT' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_SPRT')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_SPRT on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_SPRT] DEFAULT ((0)) FOR [SPRT]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'OPEN' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_OPEN')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_OPEN on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_OPEN] DEFAULT ((0)) FOR [OPEN]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCTTOS' AND COLUMN_NAME = 'SIGN' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PCTTOS_SIGN')
BEGIN 
    PRINT 'Creating default constraint DF_PCTTOS_SIGN on table PCTTOS' 
    ALTER TABLE [dbo].[PCTTOS]
    ADD CONSTRAINT [DF_PCTTOS_SIGN] DEFAULT ((0)) FOR [SIGN]
END 
GO

