﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ExternalRequests' AND COLUMN_NAME = 'CreateTime' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ExternalRequests_CreateTime')
BEGIN 
    PRINT 'Creating default constraint DF_ExternalRequests_CreateTime on table ExternalRequests' 
    ALTER TABLE [dbo].[ExternalRequests]
    ADD CONSTRAINT [DF_ExternalRequests_CreateTime] DEFAULT (getdate()) FOR [CreateTime]
END 
GO

