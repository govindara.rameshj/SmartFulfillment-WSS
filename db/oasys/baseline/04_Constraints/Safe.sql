﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Safe' AND COLUMN_NAME = 'EndOfDayCheckDone' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Safe_EndOfDayCheckDone')
BEGIN 
    PRINT 'Creating default constraint DF_Safe_EndOfDayCheckDone on table Safe' 
    ALTER TABLE [dbo].[Safe]
    ADD CONSTRAINT [DF_Safe_EndOfDayCheckDone] DEFAULT ((0)) FOR [EndOfDayCheckDone]
END 
GO

