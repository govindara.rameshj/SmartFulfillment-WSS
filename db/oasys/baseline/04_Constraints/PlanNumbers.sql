﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PlanNumbers' AND COLUMN_NAME = 'IsActive' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PlanNumbers_IsActive')
BEGIN 
    PRINT 'Creating default constraint DF_PlanNumbers_IsActive on table PlanNumbers' 
    ALTER TABLE [dbo].[PlanNumbers]
    ADD CONSTRAINT [DF_PlanNumbers_IsActive] DEFAULT ((1)) FOR [IsActive]
END 
GO

