﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportColumns' AND COLUMN_NAME = 'TableId' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportColumns_TableId')
BEGIN 
    PRINT 'Creating default constraint DF_ReportColumns_TableId on table ReportColumns' 
    ALTER TABLE [dbo].[ReportColumns]
    ADD CONSTRAINT [DF_ReportColumns_TableId] DEFAULT ((1)) FOR [TableId]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportColumns' AND COLUMN_NAME = 'Sequence' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportColumns_Sequence')
BEGIN 
    PRINT 'Creating default constraint DF_ReportColumns_Sequence on table ReportColumns' 
    ALTER TABLE [dbo].[ReportColumns]
    ADD CONSTRAINT [DF_ReportColumns_Sequence] DEFAULT ((0)) FOR [Sequence]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportColumns' AND COLUMN_NAME = 'IsVisible' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportColumns_IsVisible')
BEGIN 
    PRINT 'Creating default constraint DF_ReportColumns_IsVisible on table ReportColumns' 
    ALTER TABLE [dbo].[ReportColumns]
    ADD CONSTRAINT [DF_ReportColumns_IsVisible] DEFAULT ((1)) FOR [IsVisible]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportColumns' AND COLUMN_NAME = 'IsBold' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportColumns_IsBold')
BEGIN 
    PRINT 'Creating default constraint DF_ReportColumns_IsBold on table ReportColumns' 
    ALTER TABLE [dbo].[ReportColumns]
    ADD CONSTRAINT [DF_ReportColumns_IsBold] DEFAULT ((0)) FOR [IsBold]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ReportColumns' AND COLUMN_NAME = 'IsHighlight' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ReportColumns_IsHighlight')
BEGIN 
    PRINT 'Creating default constraint DF_ReportColumns_IsHighlight on table ReportColumns' 
    ALTER TABLE [dbo].[ReportColumns]
    ADD CONSTRAINT [DF_ReportColumns_IsHighlight] DEFAULT ((0)) FOR [IsHighlight]
END 
GO

