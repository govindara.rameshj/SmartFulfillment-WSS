﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'QUOLIN' AND COLUMN_NAME = 'NUMB')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'QUOLIN' AND CONSTRAINT_NAME = 'FK_QUOLIN_QUOHDR')
BEGIN 
PRINT 'Creating foreign key FK_QUOLIN_QUOHDR on table QUOLIN'
ALTER TABLE [dbo].[QUOLIN]  WITH CHECK ADD  CONSTRAINT [FK_QUOLIN_QUOHDR] FOREIGN KEY([NUMB])
REFERENCES [QUOHDR] ([NUMB])
ON DELETE CASCADE
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[QUOLIN]') AND name = N'XQUOLIN1')
BEGIN 
PRINT 'Creating index XQUOLIN1 on table QUOLIN'
CREATE UNIQUE NONCLUSTERED INDEX [XQUOLIN1] ON [dbo].[QUOLIN] 
(
	[SKUN] ASC,
	[NUMB] ASC,
	[LINE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

