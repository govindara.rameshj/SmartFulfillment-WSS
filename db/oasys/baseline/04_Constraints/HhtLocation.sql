﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'HhtLocation' AND COLUMN_NAME = 'DateCreated')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'HhtLocation' AND CONSTRAINT_NAME = 'FK_HhtLocation_HHTDET')
BEGIN 
PRINT 'Creating foreign key FK_HhtLocation_HHTDET on table HhtLocation'
ALTER TABLE [dbo].[HhtLocation]  WITH CHECK ADD  CONSTRAINT [FK_HhtLocation_HHTDET] FOREIGN KEY([DateCreated], [SkuNumber])
REFERENCES [HHTDET] ([DATE1], [SKUN])
ON UPDATE CASCADE
ON DELETE CASCADE
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'HhtLocation' AND COLUMN_NAME = 'Sequence' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_HhtLocation_Sequence')
BEGIN 
    PRINT 'Creating default constraint DF_HhtLocation_Sequence on table HhtLocation' 
    ALTER TABLE [dbo].[HhtLocation]
    ADD CONSTRAINT [DF_HhtLocation_Sequence] DEFAULT ((1)) FOR [Sequence]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'HhtLocation' AND COLUMN_NAME = 'IsDeleted' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_HhtLocation_IsDeleted')
BEGIN 
    PRINT 'Creating default constraint DF_HhtLocation_IsDeleted on table HhtLocation' 
    ALTER TABLE [dbo].[HhtLocation]
    ADD CONSTRAINT [DF_HhtLocation_IsDeleted] DEFAULT ((0)) FOR [IsDeleted]
END 
GO

