﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'StaticData' AND COLUMN_NAME = 'Enabled' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_StaticData_Enabled')
BEGIN 
    PRINT 'Creating default constraint DF_StaticData_Enabled on table StaticData' 
    ALTER TABLE [dbo].[StaticData]
    ADD CONSTRAINT [DF_StaticData_Enabled] DEFAULT ((1)) FOR [Enabled]
END 
GO

