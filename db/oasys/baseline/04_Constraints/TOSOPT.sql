﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TOSOPT' AND COLUMN_NAME = 'DOCN' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_TOSOPT_DOCN')
BEGIN 
    PRINT 'Creating default constraint DF_TOSOPT_DOCN on table TOSOPT' 
    ALTER TABLE [dbo].[TOSOPT]
    ADD CONSTRAINT [DF_TOSOPT_DOCN] DEFAULT ((0)) FOR [DOCN]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TOSOPT' AND COLUMN_NAME = 'SPRT' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_TOSOPT_SPRT')
BEGIN 
    PRINT 'Creating default constraint DF_TOSOPT_SPRT on table TOSOPT' 
    ALTER TABLE [dbo].[TOSOPT]
    ADD CONSTRAINT [DF_TOSOPT_SPRT] DEFAULT ((0)) FOR [SPRT]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TOSOPT' AND COLUMN_NAME = 'OPEN' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_TOSOPT_OPEN')
BEGIN 
    PRINT 'Creating default constraint DF_TOSOPT_OPEN on table TOSOPT' 
    ALTER TABLE [dbo].[TOSOPT]
    ADD CONSTRAINT [DF_TOSOPT_OPEN] DEFAULT ((0)) FOR [OPEN]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TOSOPT' AND COLUMN_NAME = 'SPEC' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_TOSOPT_SPEC')
BEGIN 
    PRINT 'Creating default constraint DF_TOSOPT_SPEC on table TOSOPT' 
    ALTER TABLE [dbo].[TOSOPT]
    ADD CONSTRAINT [DF_TOSOPT_SPEC] DEFAULT ((0)) FOR [SPEC]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TOSOPT' AND COLUMN_NAME = 'SUPV' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_TOSOPT_SUPV')
BEGIN 
    PRINT 'Creating default constraint DF_TOSOPT_SUPV on table TOSOPT' 
    ALTER TABLE [dbo].[TOSOPT]
    ADD CONSTRAINT [DF_TOSOPT_SUPV] DEFAULT ((0)) FOR [SUPV]
END 
GO

