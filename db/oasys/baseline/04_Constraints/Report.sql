﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report' AND COLUMN_NAME = 'HideWhenNoData' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Report_HideWhenNoData')
BEGIN 
    PRINT 'Creating default constraint DF_Report_HideWhenNoData on table Report' 
    ALTER TABLE [dbo].[Report]
    ADD CONSTRAINT [DF_Report_HideWhenNoData] DEFAULT ((0)) FOR [HideWhenNoData]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report' AND COLUMN_NAME = 'PrintLandscape' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Report_PrintLandscape')
BEGIN 
    PRINT 'Creating default constraint DF_Report_PrintLandscape on table Report' 
    ALTER TABLE [dbo].[Report]
    ADD CONSTRAINT [DF_Report_PrintLandscape] DEFAULT ((0)) FOR [PrintLandscape]
END 
GO

