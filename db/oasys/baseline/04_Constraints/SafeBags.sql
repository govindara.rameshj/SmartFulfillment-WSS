﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeBags' AND COLUMN_NAME = 'Value' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeBags_Value')
BEGIN 
    PRINT 'Creating default constraint DF_SafeBags_Value on table SafeBags' 
    ALTER TABLE [dbo].[SafeBags]
    ADD CONSTRAINT [DF_SafeBags_Value] DEFAULT ((0)) FOR [Value]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeBags' AND COLUMN_NAME = 'FloatValue' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeBags_FloatValue')
BEGIN 
    PRINT 'Creating default constraint DF_SafeBags_FloatValue on table SafeBags' 
    ALTER TABLE [dbo].[SafeBags]
    ADD CONSTRAINT [DF_SafeBags_FloatValue] DEFAULT ((0)) FOR [FloatValue]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeBags' AND COLUMN_NAME = 'PickupPeriodID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeBags_PickupPeriodID')
BEGIN 
    PRINT 'Creating default constraint DF_SafeBags_PickupPeriodID on table SafeBags' 
    ALTER TABLE [dbo].[SafeBags]
    ADD CONSTRAINT [DF_SafeBags_PickupPeriodID] DEFAULT ((0)) FOR [PickupPeriodID]
END 
GO

