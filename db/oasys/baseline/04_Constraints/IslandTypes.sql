﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IslandTypes' AND COLUMN_NAME = 'DisplayOrder' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_IslandTypes_DisplayOrder')
BEGIN 
    PRINT 'Creating default constraint DF_IslandTypes_DisplayOrder on table IslandTypes' 
    ALTER TABLE [dbo].[IslandTypes]
    ADD CONSTRAINT [DF_IslandTypes_DisplayOrder] DEFAULT ((0)) FOR [DisplayOrder]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IslandTypes' AND COLUMN_NAME = 'Zorder' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_IslandTypes_Zorder')
BEGIN 
    PRINT 'Creating default constraint DF_IslandTypes_Zorder on table IslandTypes' 
    ALTER TABLE [dbo].[IslandTypes]
    ADD CONSTRAINT [DF_IslandTypes_Zorder] DEFAULT ((0)) FOR [Zorder]
END 
GO

