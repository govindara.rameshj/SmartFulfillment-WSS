﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Islands' AND COLUMN_NAME = 'Code')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'Islands' AND CONSTRAINT_NAME = 'FK_Islands_IslandTypes')
BEGIN 
PRINT 'Creating foreign key FK_Islands_IslandTypes on table Islands'
ALTER TABLE [dbo].[Islands]  WITH CHECK ADD  CONSTRAINT [FK_Islands_IslandTypes] FOREIGN KEY([Code])
REFERENCES [IslandTypes] ([Code])
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Islands' AND COLUMN_NAME = 'PointX' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Islands_PointX')
BEGIN 
    PRINT 'Creating default constraint DF_Islands_PointX on table Islands' 
    ALTER TABLE [dbo].[Islands]
    ADD CONSTRAINT [DF_Islands_PointX] DEFAULT ((0)) FOR [PointX]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Islands' AND COLUMN_NAME = 'PointY' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Islands_PointY')
BEGIN 
    PRINT 'Creating default constraint DF_Islands_PointY on table Islands' 
    ALTER TABLE [dbo].[Islands]
    ADD CONSTRAINT [DF_Islands_PointY] DEFAULT ((0)) FOR [PointY]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Islands' AND COLUMN_NAME = 'PlanNumber' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Islands_PlanNumber')
BEGIN 
    PRINT 'Creating default constraint DF_Islands_PlanNumber on table Islands' 
    ALTER TABLE [dbo].[Islands]
    ADD CONSTRAINT [DF_Islands_PlanNumber] DEFAULT ((0)) FOR [PlanNumber]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Islands' AND COLUMN_NAME = 'PlanSegment' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Islands_PlanSegment')
BEGIN 
    PRINT 'Creating default constraint DF_Islands_PlanSegment on table Islands' 
    ALTER TABLE [dbo].[Islands]
    ADD CONSTRAINT [DF_Islands_PlanSegment] DEFAULT ((0)) FOR [PlanSegment]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Islands' AND COLUMN_NAME = 'WalkSequence' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_Islands_WalkSequence')
BEGIN 
    PRINT 'Creating default constraint DF_Islands_WalkSequence on table Islands' 
    ALTER TABLE [dbo].[Islands]
    ADD CONSTRAINT [DF_Islands_WalkSequence] DEFAULT ((0)) FOR [WalkSequence]
END 
GO

