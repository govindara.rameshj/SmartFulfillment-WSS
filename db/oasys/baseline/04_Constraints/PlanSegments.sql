﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PlanSegments' AND COLUMN_NAME = 'Number')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE TABLE_NAME = 'PlanSegments' AND CONSTRAINT_NAME = 'FK_PlanSegments_PlanNumbers')
BEGIN 
PRINT 'Creating foreign key FK_PlanSegments_PlanNumbers on table PlanSegments'
ALTER TABLE [dbo].[PlanSegments]  WITH CHECK ADD  CONSTRAINT [FK_PlanSegments_PlanNumbers] FOREIGN KEY([Number])
REFERENCES [PlanNumbers] ([Number])
ON DELETE CASCADE
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PlanSegments' AND COLUMN_NAME = 'IsActive' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_PlanSegments_IsActive')
BEGIN 
    PRINT 'Creating default constraint DF_PlanSegments_IsActive on table PlanSegments' 
    ALTER TABLE [dbo].[PlanSegments]
    ADD CONSTRAINT [DF_PlanSegments_IsActive] DEFAULT ((1)) FOR [IsActive]
END 
GO

