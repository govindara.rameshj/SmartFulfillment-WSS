﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerColumns' AND COLUMN_NAME = 'HyperLinkType' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerColumns_HyperLinkType')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerColumns_HyperLinkType on table ViewerColumns' 
    ALTER TABLE [dbo].[ViewerColumns]
    ADD CONSTRAINT [DF_ViewerColumns_HyperLinkType] DEFAULT ((0)) FOR [HyperLinkType]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerColumns' AND COLUMN_NAME = 'SelectionType' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerColumns_SelectionType')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerColumns_SelectionType on table ViewerColumns' 
    ALTER TABLE [dbo].[ViewerColumns]
    ADD CONSTRAINT [DF_ViewerColumns_SelectionType] DEFAULT ((0)) FOR [SelectionType]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerColumns' AND COLUMN_NAME = 'LookupConfigID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerColumns_LookupConfigID')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerColumns_LookupConfigID on table ViewerColumns' 
    ALTER TABLE [dbo].[ViewerColumns]
    ADD CONSTRAINT [DF_ViewerColumns_LookupConfigID] DEFAULT ((0)) FOR [LookupConfigID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerColumns' AND COLUMN_NAME = 'LookupColumnID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerColumns_LookupColumnID')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerColumns_LookupColumnID on table ViewerColumns' 
    ALTER TABLE [dbo].[ViewerColumns]
    ADD CONSTRAINT [DF_ViewerColumns_LookupColumnID] DEFAULT ((0)) FOR [LookupColumnID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerColumns' AND COLUMN_NAME = 'DisplayColumn' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerColumns_DisplayColumn')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerColumns_DisplayColumn on table ViewerColumns' 
    ALTER TABLE [dbo].[ViewerColumns]
    ADD CONSTRAINT [DF_ViewerColumns_DisplayColumn] DEFAULT ((0)) FOR [DisplayColumn]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerColumns' AND COLUMN_NAME = 'DisplayOrder' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerColumns_DisplayOrder')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerColumns_DisplayOrder on table ViewerColumns' 
    ALTER TABLE [dbo].[ViewerColumns]
    ADD CONSTRAINT [DF_ViewerColumns_DisplayOrder] DEFAULT ((0)) FOR [DisplayOrder]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ViewerColumns' AND COLUMN_NAME = 'IsEditable' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_ViewerColumns_IsEditable')
BEGIN 
    PRINT 'Creating default constraint DF_ViewerColumns_IsEditable on table ViewerColumns' 
    ALTER TABLE [dbo].[ViewerColumns]
    ADD CONSTRAINT [DF_ViewerColumns_IsEditable] DEFAULT ((0)) FOR [IsEditable]
END 
GO

