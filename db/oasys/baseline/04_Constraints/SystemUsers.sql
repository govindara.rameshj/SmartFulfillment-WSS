﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemUsers' AND COLUMN_NAME = 'IsManager' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemUsers_IsManager')
BEGIN 
    PRINT 'Creating default constraint DF_SystemUsers_IsManager on table SystemUsers' 
    ALTER TABLE [dbo].[SystemUsers]
    ADD CONSTRAINT [DF_SystemUsers_IsManager] DEFAULT ((0)) FOR [IsManager]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemUsers' AND COLUMN_NAME = 'IsSupervisor' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemUsers_IsSupervisor')
BEGIN 
    PRINT 'Creating default constraint DF_SystemUsers_IsSupervisor on table SystemUsers' 
    ALTER TABLE [dbo].[SystemUsers]
    ADD CONSTRAINT [DF_SystemUsers_IsSupervisor] DEFAULT ((0)) FOR [IsSupervisor]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemUsers' AND COLUMN_NAME = 'IsDeleted' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemUsers_IsDeleted')
BEGIN 
    PRINT 'Creating default constraint DF_SystemUsers_IsDeleted on table SystemUsers' 
    ALTER TABLE [dbo].[SystemUsers]
    ADD CONSTRAINT [DF_SystemUsers_IsDeleted] DEFAULT ((0)) FOR [IsDeleted]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SystemUsers' AND COLUMN_NAME = 'DefaultAmount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SystemUsers_DefaultAmount')
BEGIN 
    PRINT 'Creating default constraint DF_SystemUsers_DefaultAmount on table SystemUsers' 
    ALTER TABLE [dbo].[SystemUsers]
    ADD CONSTRAINT [DF_SystemUsers_DefaultAmount] DEFAULT ((0)) FOR [DefaultAmount]
END 
GO

