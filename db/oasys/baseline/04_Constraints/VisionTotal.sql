﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionTotal' AND COLUMN_NAME = 'Value' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionTotal_Value')
BEGIN 
    PRINT 'Creating default constraint DF_VisionTotal_Value on table VisionTotal' 
    ALTER TABLE [dbo].[VisionTotal]
    ADD CONSTRAINT [DF_VisionTotal_Value] DEFAULT ((0)) FOR [Value]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionTotal' AND COLUMN_NAME = 'ValueMerchandising' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionTotal_ValueMerchandising')
BEGIN 
    PRINT 'Creating default constraint DF_VisionTotal_ValueMerchandising on table VisionTotal' 
    ALTER TABLE [dbo].[VisionTotal]
    ADD CONSTRAINT [DF_VisionTotal_ValueMerchandising] DEFAULT ((0)) FOR [ValueMerchandising]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionTotal' AND COLUMN_NAME = 'ValueNonMerchandising' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionTotal_ValueNonMerchandising')
BEGIN 
    PRINT 'Creating default constraint DF_VisionTotal_ValueNonMerchandising on table VisionTotal' 
    ALTER TABLE [dbo].[VisionTotal]
    ADD CONSTRAINT [DF_VisionTotal_ValueNonMerchandising] DEFAULT ((0)) FOR [ValueNonMerchandising]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionTotal' AND COLUMN_NAME = 'ValueTax' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionTotal_ValueTax')
BEGIN 
    PRINT 'Creating default constraint DF_VisionTotal_ValueTax on table VisionTotal' 
    ALTER TABLE [dbo].[VisionTotal]
    ADD CONSTRAINT [DF_VisionTotal_ValueTax] DEFAULT ((0)) FOR [ValueTax]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionTotal' AND COLUMN_NAME = 'ValueDiscount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionTotal_ValueDiscount')
BEGIN 
    PRINT 'Creating default constraint DF_VisionTotal_ValueDiscount on table VisionTotal' 
    ALTER TABLE [dbo].[VisionTotal]
    ADD CONSTRAINT [DF_VisionTotal_ValueDiscount] DEFAULT ((0)) FOR [ValueDiscount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionTotal' AND COLUMN_NAME = 'ValueColleagueDiscount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionTotal_ValueColleagueDiscount')
BEGIN 
    PRINT 'Creating default constraint DF_VisionTotal_ValueColleagueDiscount on table VisionTotal' 
    ALTER TABLE [dbo].[VisionTotal]
    ADD CONSTRAINT [DF_VisionTotal_ValueColleagueDiscount] DEFAULT ((0)) FOR [ValueColleagueDiscount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionTotal' AND COLUMN_NAME = 'IsColleagueDiscount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionTotal_IsColleagueDiscount')
BEGIN 
    PRINT 'Creating default constraint DF_VisionTotal_IsColleagueDiscount on table VisionTotal' 
    ALTER TABLE [dbo].[VisionTotal]
    ADD CONSTRAINT [DF_VisionTotal_IsColleagueDiscount] DEFAULT ((0)) FOR [IsColleagueDiscount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'VisionTotal' AND COLUMN_NAME = 'IsPivotal' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_VisionTotal_IsPivotal')
BEGIN 
    PRINT 'Creating default constraint DF_VisionTotal_IsPivotal on table VisionTotal' 
    ALTER TABLE [dbo].[VisionTotal]
    ADD CONSTRAINT [DF_VisionTotal_IsPivotal] DEFAULT ((0)) FOR [IsPivotal]
END 
GO

