﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TextCommunications' AND COLUMN_NAME = 'Deleted' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_TextCommunications_Deleted')
BEGIN 
    PRINT 'Creating default constraint DF_TextCommunications_Deleted on table TextCommunications' 
    ALTER TABLE [dbo].[TextCommunications]
    ADD CONSTRAINT [DF_TextCommunications_Deleted] DEFAULT ((0)) FOR [Deleted]
END 
GO

