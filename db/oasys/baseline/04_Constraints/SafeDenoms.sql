﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeDenoms' AND COLUMN_NAME = 'TenderID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeDenoms_TenderID')
BEGIN 
    PRINT 'Creating default constraint DF_SafeDenoms_TenderID on table SafeDenoms' 
    ALTER TABLE [dbo].[SafeDenoms]
    ADD CONSTRAINT [DF_SafeDenoms_TenderID] DEFAULT ((1)) FOR [TenderID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeDenoms' AND COLUMN_NAME = 'ID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeDenoms_ID')
BEGIN 
    PRINT 'Creating default constraint DF_SafeDenoms_ID on table SafeDenoms' 
    ALTER TABLE [dbo].[SafeDenoms]
    ADD CONSTRAINT [DF_SafeDenoms_ID] DEFAULT ((0.01)) FOR [ID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeDenoms' AND COLUMN_NAME = 'SafeValue' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeDenoms_SafeValue')
BEGIN 
    PRINT 'Creating default constraint DF_SafeDenoms_SafeValue on table SafeDenoms' 
    ALTER TABLE [dbo].[SafeDenoms]
    ADD CONSTRAINT [DF_SafeDenoms_SafeValue] DEFAULT ((0)) FOR [SafeValue]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeDenoms' AND COLUMN_NAME = 'ChangeValue' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeDenoms_ChangeValue')
BEGIN 
    PRINT 'Creating default constraint DF_SafeDenoms_ChangeValue on table SafeDenoms' 
    ALTER TABLE [dbo].[SafeDenoms]
    ADD CONSTRAINT [DF_SafeDenoms_ChangeValue] DEFAULT ((0)) FOR [ChangeValue]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeDenoms' AND COLUMN_NAME = 'SystemValue' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeDenoms_SystemValue')
BEGIN 
    PRINT 'Creating default constraint DF_SafeDenoms_SystemValue on table SafeDenoms' 
    ALTER TABLE [dbo].[SafeDenoms]
    ADD CONSTRAINT [DF_SafeDenoms_SystemValue] DEFAULT ((0)) FOR [SystemValue]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SafeDenoms' AND COLUMN_NAME = 'SuggestedValue' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_SafeDenoms_SuggestedValue')
BEGIN 
    PRINT 'Creating default constraint DF_SafeDenoms_SuggestedValue on table SafeDenoms' 
    ALTER TABLE [dbo].[SafeDenoms]
    ADD CONSTRAINT [DF_SafeDenoms_SuggestedValue] DEFAULT ((0)) FOR [SuggestedValue]
END 
GO

