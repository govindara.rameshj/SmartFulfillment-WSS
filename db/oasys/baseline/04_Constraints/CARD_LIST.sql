﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CARD_LIST' AND COLUMN_NAME = 'DELETED' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CARD_LIST_DELETED')
BEGIN 
    PRINT 'Creating default constraint DF_CARD_LIST_DELETED on table CARD_LIST' 
    ALTER TABLE [dbo].[CARD_LIST]
    ADD CONSTRAINT [DF_CARD_LIST_DELETED] DEFAULT ((0)) FOR [DELETED]
END 
GO

