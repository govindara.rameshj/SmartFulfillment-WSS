﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CashBalTillTenVar]') AND name = N'key0')
BEGIN 
PRINT 'Creating index key0 on table CashBalTillTenVar'
CREATE UNIQUE NONCLUSTERED INDEX [key0] ON [dbo].[CashBalTillTenVar] 
(
	[PeriodID] ASC,
	[TradingPeriodID] ASC,
	[TillID] ASC,
	[CurrencyID] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalTillTenVar' AND COLUMN_NAME = 'PeriodID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalTillTenVar_PeriodID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalTillTenVar_PeriodID on table CashBalTillTenVar' 
    ALTER TABLE [dbo].[CashBalTillTenVar]
    ADD CONSTRAINT [DF_CashBalTillTenVar_PeriodID] DEFAULT ((0)) FOR [PeriodID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalTillTenVar' AND COLUMN_NAME = 'TradingPeriodID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalTillTenVar_TradingPeriodID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalTillTenVar_TradingPeriodID on table CashBalTillTenVar' 
    ALTER TABLE [dbo].[CashBalTillTenVar]
    ADD CONSTRAINT [DF_CashBalTillTenVar_TradingPeriodID] DEFAULT ((0)) FOR [TradingPeriodID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalTillTenVar' AND COLUMN_NAME = 'TillID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalTillTenVar_TillID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalTillTenVar_TillID on table CashBalTillTenVar' 
    ALTER TABLE [dbo].[CashBalTillTenVar]
    ADD CONSTRAINT [DF_CashBalTillTenVar_TillID] DEFAULT ('') FOR [TillID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalTillTenVar' AND COLUMN_NAME = 'CurrencyID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalTillTenVar_CurrencyID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalTillTenVar_CurrencyID on table CashBalTillTenVar' 
    ALTER TABLE [dbo].[CashBalTillTenVar]
    ADD CONSTRAINT [DF_CashBalTillTenVar_CurrencyID] DEFAULT ('') FOR [CurrencyID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalTillTenVar' AND COLUMN_NAME = 'ID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalTillTenVar_ID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalTillTenVar_ID on table CashBalTillTenVar' 
    ALTER TABLE [dbo].[CashBalTillTenVar]
    ADD CONSTRAINT [DF_CashBalTillTenVar_ID] DEFAULT ((0)) FOR [ID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalTillTenVar' AND COLUMN_NAME = 'Quantity' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalTillTenVar_Quantity')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalTillTenVar_Quantity on table CashBalTillTenVar' 
    ALTER TABLE [dbo].[CashBalTillTenVar]
    ADD CONSTRAINT [DF_CashBalTillTenVar_Quantity] DEFAULT ((0)) FOR [Quantity]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalTillTenVar' AND COLUMN_NAME = 'Amount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalTillTenVar_Amount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalTillTenVar_Amount on table CashBalTillTenVar' 
    ALTER TABLE [dbo].[CashBalTillTenVar]
    ADD CONSTRAINT [DF_CashBalTillTenVar_Amount] DEFAULT ((0)) FOR [Amount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalTillTenVar' AND COLUMN_NAME = 'PickUp' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalTillTenVar_PickUp')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalTillTenVar_PickUp on table CashBalTillTenVar' 
    ALTER TABLE [dbo].[CashBalTillTenVar]
    ADD CONSTRAINT [DF_CashBalTillTenVar_PickUp] DEFAULT ((0)) FOR [PickUp]
END 
GO

