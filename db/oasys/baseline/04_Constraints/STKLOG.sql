﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[STKLOG]') AND name = N'key1')
BEGIN 
PRINT 'Creating index key1 on table STKLOG'
CREATE NONCLUSTERED INDEX [key1] ON [dbo].[STKLOG] 
(
	[SKUN] ASC,
	[DAYN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[STKLOG]') AND name = N'key2')
BEGIN 
PRINT 'Creating index key2 on table STKLOG'
CREATE NONCLUSTERED INDEX [key2] ON [dbo].[STKLOG] 
(
	[DAYN] ASC,
	[TYPE] ASC,
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[STKLOG]') AND name = N'key3')
BEGIN 
PRINT 'Creating index key3 on table STKLOG'
CREATE NONCLUSTERED INDEX [key3] ON [dbo].[STKLOG] 
(
	[DAYN] ASC,
	[SKUN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[STKLOG]') AND name = N'key4')
BEGIN 
PRINT 'Creating index key4 on table STKLOG'
CREATE UNIQUE NONCLUSTERED INDEX [key4] ON [dbo].[STKLOG] 
(
	[RTI] ASC,
	[TKEY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'STKLOG' AND COLUMN_NAME = 'ICOM' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_STKLOG_ICOM')
BEGIN 
    PRINT 'Creating default constraint DF_STKLOG_ICOM on table STKLOG' 
    ALTER TABLE [dbo].[STKLOG]
    ADD CONSTRAINT [DF_STKLOG_ICOM] DEFAULT ((0)) FOR [ICOM]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'STKLOG' AND COLUMN_NAME = 'RTI' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_STKLOG_RTI')
BEGIN 
    PRINT 'Creating default constraint DF_STKLOG_RTI on table STKLOG' 
    ALTER TABLE [dbo].[STKLOG]
    ADD CONSTRAINT [DF_STKLOG_RTI] DEFAULT ('S') FOR [RTI]
END 
GO

