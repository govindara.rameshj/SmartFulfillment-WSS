﻿IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CashBalCashierTenVar]') AND name = N'key0')
BEGIN 
PRINT 'Creating index key0 on table CashBalCashierTenVar'
CREATE UNIQUE NONCLUSTERED INDEX [key0] ON [dbo].[CashBalCashierTenVar] 
(
	[PeriodID] ASC,
	[TradingPeriodID] ASC,
	[CashierID] ASC,
	[CurrencyID] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashierTenVar' AND COLUMN_NAME = 'PeriodID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashierTenVar_PeriodID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashierTenVar_PeriodID on table CashBalCashierTenVar' 
    ALTER TABLE [dbo].[CashBalCashierTenVar]
    ADD CONSTRAINT [DF_CashBalCashierTenVar_PeriodID] DEFAULT ((0)) FOR [PeriodID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashierTenVar' AND COLUMN_NAME = 'TradingPeriodID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashierTenVar_TradingPeriodID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashierTenVar_TradingPeriodID on table CashBalCashierTenVar' 
    ALTER TABLE [dbo].[CashBalCashierTenVar]
    ADD CONSTRAINT [DF_CashBalCashierTenVar_TradingPeriodID] DEFAULT ((0)) FOR [TradingPeriodID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashierTenVar' AND COLUMN_NAME = 'CashierID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashierTenVar_CashierID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashierTenVar_CashierID on table CashBalCashierTenVar' 
    ALTER TABLE [dbo].[CashBalCashierTenVar]
    ADD CONSTRAINT [DF_CashBalCashierTenVar_CashierID] DEFAULT ('') FOR [CashierID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashierTenVar' AND COLUMN_NAME = 'CurrencyID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashierTenVar_CurrencyID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashierTenVar_CurrencyID on table CashBalCashierTenVar' 
    ALTER TABLE [dbo].[CashBalCashierTenVar]
    ADD CONSTRAINT [DF_CashBalCashierTenVar_CurrencyID] DEFAULT ('') FOR [CurrencyID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashierTenVar' AND COLUMN_NAME = 'ID' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashierTenVar_ID')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashierTenVar_ID on table CashBalCashierTenVar' 
    ALTER TABLE [dbo].[CashBalCashierTenVar]
    ADD CONSTRAINT [DF_CashBalCashierTenVar_ID] DEFAULT ((0)) FOR [ID]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashierTenVar' AND COLUMN_NAME = 'Quantity' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashierTenVar_Quantity')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashierTenVar_Quantity on table CashBalCashierTenVar' 
    ALTER TABLE [dbo].[CashBalCashierTenVar]
    ADD CONSTRAINT [DF_CashBalCashierTenVar_Quantity] DEFAULT ((0)) FOR [Quantity]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashierTenVar' AND COLUMN_NAME = 'Amount' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashierTenVar_Amount')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashierTenVar_Amount on table CashBalCashierTenVar' 
    ALTER TABLE [dbo].[CashBalCashierTenVar]
    ADD CONSTRAINT [DF_CashBalCashierTenVar_Amount] DEFAULT ((0)) FOR [Amount]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CashBalCashierTenVar' AND COLUMN_NAME = 'PickUp' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_CashBalCashierTenVar_PickUp')
BEGIN 
    PRINT 'Creating default constraint DF_CashBalCashierTenVar_PickUp on table CashBalCashierTenVar' 
    ALTER TABLE [dbo].[CashBalCashierTenVar]
    ADD CONSTRAINT [DF_CashBalCashierTenVar_PickUp] DEFAULT ((0)) FOR [PickUp]
END 
GO

