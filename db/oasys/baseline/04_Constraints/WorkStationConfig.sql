﻿IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'WorkStationConfig' AND COLUMN_NAME = 'IsActive' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_WorkStationConfig_IsActive')
BEGIN 
    PRINT 'Creating default constraint DF_WorkStationConfig_IsActive on table WorkStationConfig' 
    ALTER TABLE [dbo].[WorkStationConfig]
    ADD CONSTRAINT [DF_WorkStationConfig_IsActive] DEFAULT ((0)) FOR [IsActive]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'WorkStationConfig' AND COLUMN_NAME = 'IsBarcodeBroken' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_WorkStationConfig_IsBarcodeBroken')
BEGIN 
    PRINT 'Creating default constraint DF_WorkStationConfig_IsBarcodeBroken on table WorkStationConfig' 
    ALTER TABLE [dbo].[WorkStationConfig]
    ADD CONSTRAINT [DF_WorkStationConfig_IsBarcodeBroken] DEFAULT ((0)) FOR [IsBarcodeBroken]
END 
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'WorkStationConfig' AND COLUMN_NAME = 'UseTouchScreen' AND COLUMN_DEFAULT IS NULL)
	AND NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE NAME = 'DF_WorkStationConfig_UseTouchScreen')
BEGIN 
    PRINT 'Creating default constraint DF_WorkStationConfig_UseTouchScreen on table WorkStationConfig' 
    ALTER TABLE [dbo].[WorkStationConfig]
    ADD CONSTRAINT [DF_WorkStationConfig_UseTouchScreen] DEFAULT ((0)) FOR [UseTouchScreen]
END 
GO

