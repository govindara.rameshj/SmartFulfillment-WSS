﻿INSERT [dbo].[SystemCurrency] ([ID], [Description], [Symbol], [PrintSymbol], [ActiveDate], [InactiveDate], [LargestDenom], [MinCashMultiple], [DisplayOrder], [IsDefault], [MaxAccepted]) VALUES (N'GBP', N'Pound Sterling                          ', N'£  ', N'£  ', CAST(0x29340B00 AS Date), NULL, 100, CAST(1.00 AS Decimal(9, 2)), 1, 1, CAST(1000000.00 AS Decimal(9, 2)))
GO
