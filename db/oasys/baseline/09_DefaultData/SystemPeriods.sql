﻿declare @todayStart datetime
set @todayStart = CONVERT(date, getdate())
declare @todayEnd datetime
set @todayEnd = dateadd(s, (23 * 60 + 59) * 60 + 59, @todayStart)

SET IDENTITY_INSERT dbo.SystemPeriods ON

;with periods as
(
    -- Generate range for periods, from 0 to 27.
    -- It is 1 week before, and 3 weeks from today. Total 7 * 4 = 28.
    SELECT distinct number as periodNumber FROM master..[spt_values] WHERE number BETWEEN 0 AND 27
)
INSERT INTO dbo.SystemPeriods ( [ID],[StartDate],[EndDate],[IsClosed])
    select
            1503 + periodNumber,
            DATEADD(d, periodNumber - 7, @todayStart),
            DATEADD(d, periodNumber - 7, @todayEnd),
            0 -- all are closed
        from periods

SET IDENTITY_INSERT dbo.SystemPeriods OFF
GO