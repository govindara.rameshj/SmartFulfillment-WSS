﻿SET IDENTITY_INSERT [dbo].[ViewerConfig] ON 

INSERT [dbo].[ViewerConfig] ([ID], [Title], [BOName], [IsHorizontal], [RecordLimit], [AllowAddition], [AllowDeletion], [AllowUpdate]) VALUES (1, N'Customer Order Headers        ', N'BOSalesOrders.cOrderHeaderShownInUI               ', 1, 300, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[ViewerConfig] OFF
GO
