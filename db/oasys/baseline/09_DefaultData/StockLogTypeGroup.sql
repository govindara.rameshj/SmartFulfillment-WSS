﻿INSERT [dbo].[StockLogTypeGroup] ([Id], [Description]) VALUES (1, N'Transaction')
INSERT [dbo].[StockLogTypeGroup] ([Id], [Description]) VALUES (2, N'DRLS')
INSERT [dbo].[StockLogTypeGroup] ([Id], [Description]) VALUES (3, N'Stock Adjustments')
INSERT [dbo].[StockLogTypeGroup] ([Id], [Description]) VALUES (4, N'Price Changes')
INSERT [dbo].[StockLogTypeGroup] ([Id], [Description]) VALUES (5, N'System')
GO
