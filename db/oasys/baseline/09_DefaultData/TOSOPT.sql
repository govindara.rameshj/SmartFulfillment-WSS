﻿INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'01', N'SA', N'Sale                               ', 0, 0, 1, 1, 0)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'02', N'SC', N'Sale Correction                    ', 0, 0, 1, 1, 0)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'03', N'RF', N'Refund                             ', 0, 0, 0, 1, 0)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'04', N'RC', N'Refund Correction                  ', 0, 0, 0, 1, 0)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'05', N'OD', N'Open Drawer                        ', 0, 0, 1, 0, 1)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'06', N'M+', N'Misc. Income                       ', 1, 0, 1, 0, 1)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'07', N'M-', N'Paid Out                           ', 1, 0, 1, 0, 1)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'08', N'C+', N'Misc Inc Correct                   ', 1, 0, 1, 0, 1)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'09', N'C-', N'Paid Out Correct                   ', 1, 0, 1, 0, 1)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'11', N'RP', N'Recall Transaction                 ', 0, 0, 1, 0, 0)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'12', N'RL', N'Reprint Logo                       ', 0, 0, 0, 0, 0)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'13', N'EC', N'Barcode Check                      ', 0, 0, 0, 0, 0)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'14', N'XR', N'X-READ                             ', 0, 0, 0, 0, 1)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'15', N'ZR', N'Z-READ                             ', 0, 0, 0, 0, 1)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'98', N'CO', N'Sign On                            ', 0, 0, 0, 0, 0)
INSERT [dbo].[TOSOPT] ([DSEQ], [TOSC], [TOSD], [DOCN], [SPRT], [OPEN], [SPEC], [SUPV]) VALUES (N'99', N'CC', N'Sign Off                           ', 0, 0, 0, 0, 0)
GO
