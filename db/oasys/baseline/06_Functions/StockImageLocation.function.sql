﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[StockImageLocation]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function StockImageLocation'
	EXEC ('CREATE FUNCTION [dbo].[StockImageLocation]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function StockImageLocation'
GO
ALTER FUNCTION StockImageLocation(@SkuNumber nvarchar(6))
   returns nvarchar(255)
as
begin
   return (select StringValue + '\' + @SkuNumber +'.jpg' from Parameters where ParameterID = 915)
end
GO

