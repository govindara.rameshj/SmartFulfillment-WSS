﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_PickupBagsInSafeOnDate]', N'IF') IS NULL
BEGIN
	PRINT 'Creating function udf_PickupBagsInSafeOnDate'
	EXEC ('CREATE FUNCTION [dbo].[udf_PickupBagsInSafeOnDate]() RETURNS TABLE AS RETURN ( SELECT 1 AS [Column] )')
END
GO

PRINT 'Altering function udf_PickupBagsInSafeOnDate'
GO
ALTER FUNCTION udf_PickupBagsInSafeOnDate(@PeriodID int)
   returns table
as
   return
   (
       select * from dbo.udf_BagsInSafeOnDate(@PeriodID, 'P')
   )
GO

