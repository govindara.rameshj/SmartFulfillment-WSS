﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportClickAndCollectSalesQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportClickAndCollectSalesQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportClickAndCollectSalesQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportClickAndCollectSalesQtyDAY'
GO
ALTER FUNCTION [dbo].[svf_ReportClickAndCollectSalesQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare	@StartDate		date,
			@CCDayQty		numeric(9,2)
	
	Set		@StartDate		=	@InputDate;	
	
	Select @CCDayQty = COUNT(DT.TOTL)
	From DLTOTS DT 
	Inner Join vwCORHDRFull COR	on	DT.ORDN		= COR.NUMB
	Where	TCOD			= 'SA'
			and COR.IS_CLICK_AND_COLLECT = 1
			and DT.DATE1	=	@StartDate;
	
	Set		@CCDayQty		=	isnull(@CCDayQty, 0)
		
	RETURN	@CCDayQty

END
GO

