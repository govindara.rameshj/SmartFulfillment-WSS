﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportGiftVoucherAllocationQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportGiftVoucherAllocationQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportGiftVoucherAllocationQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportGiftVoucherAllocationQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Gift Voucher Allocation Qty for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportGiftVoucherAllocationQtyDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate			date,
					@GiftQty			int
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Gift Voucher Allocation Qty - DAY
	----------------------------------------------------------------------------------
	Select			@GiftQty				=		count(AMNT)
	From			DLGIFT
	Where			[TYPE]					in		('SA', 'TR', 'CR', 'CC' ,'VR', 'RC')
					and Date1				=		@StartDate;

	Set				@GiftQty				=		isnull(@GiftQty, 0);
		
	RETURN			@GiftQty

END
GO

