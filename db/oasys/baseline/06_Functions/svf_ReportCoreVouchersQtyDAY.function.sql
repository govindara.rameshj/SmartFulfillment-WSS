﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportCoreVouchersQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportCoreVouchersQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportCoreVouchersQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportCoreVouchersQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Core Refunds Value for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportCoreVouchersQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate				date,
					@CoreVouchersQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Core VOUCHERS Qty - DAY
	----------------------------------------------------------------------------------
	Select			@CoreVouchersQty	=		count(DP.AMNT)
	From			DLTOTS as DT
	Inner Join		DLPAID as DP        on		DP.DATE1	= DT.DATE1
										and		DP.TILL		= DT.TILL
										and		DP.[TRAN]	= DT.[TRAN]
										and		DP.[TYPE]	= 6
	Where			DT.CASH				<>		'000'
					and DT.VOID			=		0
					and DT.PARK			=		0
					and DT.TMOD			=		0  
					and DT.DATE1		=		@StartDate;

	Set				@CoreVouchersQty	=		isnull(@CoreVouchersQty, 0);
		
	RETURN			@CoreVouchersQty

END
GO

