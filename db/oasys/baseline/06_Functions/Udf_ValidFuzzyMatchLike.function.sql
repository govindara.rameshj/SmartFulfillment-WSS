﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[Udf_ValidFuzzyMatchLike]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function Udf_ValidFuzzyMatchLike'
	EXEC ('CREATE FUNCTION [dbo].[Udf_ValidFuzzyMatchLike](@SearchCriteria nvarchar(50), @Phrase nvarchar(1000)) RETURNS bit AS BEGIN RETURN 0 END')
END
GO

PRINT 'Altering function Udf_ValidFuzzyMatchLike'
GO
ALTER FUNCTION Udf_ValidFuzzyMatchLike(@SearchCriteria nvarchar(50), @Phrase nvarchar(1000))
   returns bit
as
begin
   declare @Temp   nvarchar(1000)
   declare @Index  int
   declare @OffSet int

   set @Temp  = rtrim(ltrim(isnull(@SearchCriteria, '')))
   set @Index = patindex('% %', @Temp) 

   if @SearchCriteria is null return 0
   if len(@Temp) = 0 return 0
   
   if @Index = 0
      set @OffSet = 1000
   else
      set @OffSet = @Index - 1

   if exists (select a.Phase
              from (select Phase = @Phrase) a
              where a.Phase like '%' + substring(@Temp, 1, @OffSet) + '%') 
      return 1
   else
      if @Index <> 0 return dbo.Udf_ValidFuzzyMatchLike(substring(@Temp, @Index + 1, 1000), @Phrase)

   return 0
end
GO

