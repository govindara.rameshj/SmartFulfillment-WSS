﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportMiscIncomeQtyWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportMiscIncomeQtyWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportMiscIncomeQtyWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportMiscIncomeQtyWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Misc Income for WTD - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportMiscIncomeQtyWTD]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@MIQty				int
	
	Set				@StartDate			=		@InputDate;	
	Set				@DateEnd			=		@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Misc Income Qty - WTD
	----------------------------------------------------------------------------------
	Select			@MIQty			=		count(TOTL)
	From			DLTOTS 
	Where			TCOD			in		('M+', 'C+')
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		>=		@StartDate
					and Date1		<=		@DateEnd;

	Set				@MIQty			=		isnull(@MIQty, 0);
		
	RETURN			@MIQty

END
GO

