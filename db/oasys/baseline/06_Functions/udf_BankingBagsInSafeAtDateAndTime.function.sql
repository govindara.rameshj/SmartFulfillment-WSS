﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_BankingBagsInSafeAtDateAndTime]', N'IF') IS NULL
BEGIN
	PRINT 'Creating function udf_BankingBagsInSafeAtDateAndTime'
	EXEC ('CREATE FUNCTION [dbo].[udf_BankingBagsInSafeAtDateAndTime]() RETURNS TABLE AS RETURN ( SELECT 1 AS [Column] )')
END
GO

PRINT 'Altering function udf_BankingBagsInSafeAtDateAndTime'
GO
ALTER FUNCTION udf_BankingBagsInSafeAtDateAndTime(@Now datetime)
   returns table
as
   return
   (
       select * from dbo.udf_BagsInSafeAtDateAndTime(@Now, 'B')
   )
GO

