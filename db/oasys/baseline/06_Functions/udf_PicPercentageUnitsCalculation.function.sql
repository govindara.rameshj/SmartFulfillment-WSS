﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_PicPercentageUnitsCalculation]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_PicPercentageUnitsCalculation'
	EXEC ('CREATE FUNCTION [dbo].[udf_PicPercentageUnitsCalculation]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_PicPercentageUnitsCalculation'
GO
ALTER FUNCTION [dbo].[udf_PicPercentageUnitsCalculation]
 (@QtyVariance AS DEC(9 , 2), 
  @StartOnHand AS DEC(9 , 2)) 
RETURNS DEC(9, 2) 
AS 
  BEGIN 
      DECLARE @PercentageUnit DEC(9, 2) 

      SET @PercentageUnit = Abs(( @QtyVariance ) / Nullif(@StartOnHand, 0)) * 
                            100 

      RETURN @PercentageUnit 
  END
GO

