﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_PriceChangeReportOverdueQuantity]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_PriceChangeReportOverdueQuantity'
	EXEC ('CREATE FUNCTION [dbo].[udf_PriceChangeReportOverdueQuantity]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_PriceChangeReportOverdueQuantity'
GO
ALTER FUNCTION dbo.udf_PriceChangeReportOverdueQuantity(
                                                        )
RETURNS int
AS
BEGIN

    DECLARE
       @Quantity int;

    SELECT @Quantity = count (*)
      FROM
           PRCCHG prc INNER JOIN STKMAS stk
           ON prc.SKUN
              = 
              stk.SKUN
                      INNER JOIN evtchg ec
           ON ec.SKUN
              = 
              prc.SKUN
          AND ec.NUMB
              = 
              prc.EVNT
          AND ec.PRIO
              = 
              prc.PRIO
      WHERE prc.PSTA = 'U'
        AND ec.SDAT
            < 
            ( 
              SELECT tmdt
                FROM sysdat
            );

    SET @Quantity = ISNULL( @Quantity , 0
                          );

    RETURN @Quantity;

END;
GO

