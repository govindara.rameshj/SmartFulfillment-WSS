﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_CashierHasTakenSaleOnDay]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_CashierHasTakenSaleOnDay'
	EXEC ('CREATE FUNCTION [dbo].[udf_CashierHasTakenSaleOnDay]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_CashierHasTakenSaleOnDay'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 08/11/2012
-- User Story	 : 8924
-- Project		 : P022-ITN019-I: RF1031 - Cash Drop Cashiers
--				 : Dropdown List
-- Task Id		 : 8985
-- Description   : Create udf to determine whether a given
--				 : cashier has taken any sales for a given
--				 : banking period.
-- =============================================
ALTER FUNCTION [dbo].[udf_CashierHasTakenSaleOnDay]
(
	@CashierID As Int,
	@PeriodID  As Int = Null
)
Returns Bit
Begin
	Declare @CashierHasTakenSaleToday As Bit
	
	Set @CashierHasTakenSaleToday =
		CAST
			(
				(
					Case IsNull
						(
							(
								Select 
									NumTransactions + NumCorrections
								From 
									CashBalCashier
								Where 
									(
										PeriodID    = @PeriodID
									Or
										(
											PeriodID    <> @PeriodID
										And
											@PeriodID Is Null
										)
									)
								 And   
									CurrencyID  = 
										(
											Select 
												ID 
											From 
												SystemCurrency 
											Where 
												IsDefault = 1
										)
							 And   
								CashierID   = @CashierID
							 And  
								(
									GrossSalesAmount <> 0 
								Or 
									(
										Select 
											Count(*)
										From 
											CashBalCashierTen
										Where 
											(
												PeriodID    = @PeriodID
											Or
												(
													PeriodID    <> @PeriodID
												And
													@PeriodID Is Null
												)
											)
										And   
											CurrencyID = 
												(
													Select 
														ID 
													From 
														SystemCurrency 
													Where 
														IsDefault = 1
												)
										And
											CashierID  = @CashierID
									) > 0
								)
							), 0)
						When 0 
							Then 0
						Else 
							1
						End
				) As Bit
			)
		
	Return @CashierHasTakenSaleToday
End
GO

