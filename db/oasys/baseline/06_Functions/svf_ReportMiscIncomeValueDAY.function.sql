﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportMiscIncomeValueDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportMiscIncomeValueDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportMiscIncomeValueDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportMiscIncomeValueDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Misc Income Value for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 7th July 2011
-- Description	: Changed to return a sum of total rather than count
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportMiscIncomeValueDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate			date,
					@MIValue			int
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Misc Income Value - DAY
	----------------------------------------------------------------------------------
	Select			@MIValue		=		sum(TOTL)
	From			DLTOTS 
	Where			TCOD			in		('M+', 'C+')
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		=		@StartDate;

	Set				@MIValue		=		isnull(@MIValue, 0);
		
	RETURN			@MIValue

END
GO

