﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportPriceChangesQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportPriceChangesQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportPriceChangesQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportPriceChangesQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 8th June 2011
-- Description	: Returns the figure to use for Price Changes Qty for TODAY - Scalar Valued Function
-- Notes		: This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION dbo.svf_ReportPriceChangesQtyDAY(
                                               )
RETURNS int
AS
BEGIN

    DECLARE
       @PCToday int;
    SELECT @PCToday = COUNT( *
                           )
      FROM
           PRCCHG prc INNER JOIN STKMAS stk
           ON prc.SKUN
              = 
              stk.SKUN
                      INNER JOIN evtchg ec
           ON ec.SKUN
              = 
              prc.SKUN
          AND ec.NUMB
              = 
              prc.EVNT
          AND ec.PRIO
              = 
              prc.PRIO
      WHERE prc.PSTA = 'U'
        AND ec.SDAT
            = 
            ( 
              SELECT tmdt
                FROM sysdat
            );

    RETURN @PCToday;

END;
GO

