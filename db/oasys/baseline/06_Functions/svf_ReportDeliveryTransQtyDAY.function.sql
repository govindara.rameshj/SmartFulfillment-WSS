﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportDeliveryTransQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportDeliveryTransQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportDeliveryTransQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportDeliveryTransQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Delivery Trans Value for DAY - Scalar Valued Function
-- Notes		: This uses CORHDR to draw data to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 11th July 2011
-- Description	: Removed filter on CORHDR.DCST <> 0
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportDeliveryTransQtyDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate				date,
					@DeliveryTransQ_WD		int
	
	Set				@StartDate				=		@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Retrieve Delivery Trans - Qty(DAY)
	----------------------------------------------------------------------------------
	Select			@DeliveryTransQ_WD		=		count(ch.MVST)
	
	From			CORHDR					as		ch
	Inner Join		CORHDR4					as		c4		
											on		(c4.NUMB = ch.NUMB) and (c4.DATE1 = ch.DATE1)
	
	Where			ch.DATE1				=		(@StartDate)
					and ch.CANC				<		'1' 
					and	ch.DELI				=		'1'
					and ch.STIL				<		'13'
					and c4.SellingStoreId	=		(Select Oasys.dbo.svf_SystemStoreGroupID())

	Set				@DeliveryTransQ_WD		=		isnull(@DeliveryTransQ_WD, 0);
		
	RETURN			@DeliveryTransQ_WD

END
GO

