﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportKBSalesQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportKBSalesQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportKBSalesQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportKBSalesQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 6th June 2011
-- Description	: Returns the figure to use for K&B Sales Qty for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportKBSalesQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@KBWeekQty			numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get K&B SALES Count - DAY
	----------------------------------------------------------------------------------
	Select			@KBWeekQty		=	count(ValueTender)
	From			VisionPayment 
	Where			TranDate		=	@StartDate;

	Set			@KBWeekQty	=		isnull(@KBWeekQty, 0);
		
	RETURN		@KBWeekQty

END
GO

