﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportMiscOutgoingValueWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportMiscOutgoingValueWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportMiscOutgoingValueWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportMiscOutgoingValueWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Misc Outgoing Value for WTD - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportMiscOutgoingValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@MOValue			numeric(9,2)
	
	Set				@StartDate			=		@InputDate;
	Set				@DateEnd			=		@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End		

					
	----------------------------------------------------------------------------------
	-- Get Misc Outgoing Value - WTD
	----------------------------------------------------------------------------------
	Select			@MOValue		=		sum(TOTL)
	From			DLTOTS 
	Where			TCOD			in		('M-', 'C-')
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		>=		@StartDate
					and Date1		<=		@DateEnd;

	Set				@MOValue		=		isnull(@MOValue, 0);
		
	RETURN			@MOValue

END
GO

