﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportDeliveryTransValueDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportDeliveryTransValueDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportDeliveryTransValueDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportDeliveryTransValueDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Delivery Trans Value for DAY - Scalar Valued Function
-- Notes		: This uses CORHDR to draw data to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 11th July 2011
-- Description	: Removed filter on CORHDR.DCST <> 0
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportDeliveryTransValueDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate				date,
					@DeliveryTransV_WD		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Retrieve Delivery Transaction - Value(DAY)
	----------------------------------------------------------------------------------
	Select			@DeliveryTransV_WD		=		SUM(ch.MVST)
	
	From			CORHDR					as		ch
	Inner Join		CORHDR4					as		c4		
											on		(c4.NUMB = ch.NUMB) and (c4.DATE1 = ch.DATE1)
	
	Where			ch.DATE1				=		(@StartDate)
					and ch.CANC				<		'1' 
					and	ch.DELI				=		'1'
					and ch.STIL				<		'13'
					and c4.SellingStoreId	=		(Select Oasys.dbo.svf_SystemStoreGroupID())

	Set				@DeliveryTransV_WD		=		isnull(@DeliveryTransV_WD, 0);
		
	RETURN			@DeliveryTransV_WD

END
GO

