﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_GetThisDayLastYear]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_GetThisDayLastYear'
	EXEC ('CREATE FUNCTION [dbo].[udf_GetThisDayLastYear]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_GetThisDayLastYear'
GO
ALTER FUNCTION udf_GetThisDayLastYear 
(
	@DateIn datetime
)
RETURNS datetime
AS
BEGIN
	RETURN DATEADD(week, -52, @DateIn)
END
GO

