﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportConsignmentsNotReceived]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportConsignmentsNotReceived'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportConsignmentsNotReceived]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportConsignmentsNotReceived'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Consignments Not Received - Scalar Valued Function
-- Notes		: This uses STKADJ to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportConsignmentsNotReceived]
(
)
RETURNS int
AS
BEGIN

	Declare			@ConsignNR			int

					
	----------------------------------------------------------------------------------
	-- Get Consignments Not Received
	----------------------------------------------------------------------------------
	Select			@ConsignNR				=		count(DONE)
	From			CONMAS 
	Where			DONE					=		0;

	Set				@ConsignNR				=		isnull(@ConsignNR, 0);
		
	RETURN			@ConsignNR
END
GO

