﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_EventGetTypeDescription]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_EventGetTypeDescription'
	EXEC ('CREATE FUNCTION [dbo].[udf_EventGetTypeDescription]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_EventGetTypeDescription'
GO
ALTER FUNCTION [dbo].[udf_EventGetTypeDescription]
(
	@Type		CHAR(2)
)
RETURNS CHAR(40)
BEGIN
	DECLARE @Description	CHAR(30)
	
	SET @Description =
		CASE
			WHEN
				@Type = 'TS'
			THEN
				'TS - TLP SKU Event'
			WHEN
				@Type = 'TM'
			THEN
				'TM - TLP Mix & Match Event'
			WHEN
				@Type = 'QS'
			THEN
				'QS - SKU Bulk Saving Event'
			WHEN
				@Type = 'QM'
			THEN
				'QM - Mix & Match Bulk Saving Event'
			WHEN
				@Type = 'QD'
			THEN
				'QD - Deal Bulk Saving Event'
			WHEN
				@Type = 'MS'
			THEN
				'MS - Multi-buy SKU Event'
			WHEN
				@Type = 'MM'
			THEN
				'MM - Multi-buy Mix & Match Event'
			WHEN
				@Type = 'DG'
			THEN
				'DG - Deal Group'
			WHEN
				@Type = 'DM'
			THEN
				'DM - Mix & Match Deal Group'
			WHEN
				@Type = 'DS'
			THEN
				'DS - SKU Deal Group'
			WHEN
				@Type = 'HS'
			THEN
				'HS - Spend Level Saving'
			WHEN
				@Type IS NULL 
			THEN
				'No Event Type Given'
			ELSE
				@Type + ' - Unknown Event Type'
		END
	RETURN @Description
END
GO

