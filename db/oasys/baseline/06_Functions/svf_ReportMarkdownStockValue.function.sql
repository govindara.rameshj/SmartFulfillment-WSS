﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportMarkdownStockValue]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportMarkdownStockValue'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportMarkdownStockValue]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportMarkdownStockValue'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Markdown Stock Value - Scalar Valued Function
-- Notes		: This uses STKMAS:MDNQ value to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 8th July 2011
-- Description	: Changed return data type to decimal(9, 2) from int
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportMarkdownStockValue]()
RETURNS decimal(9,2) 
AS
BEGIN

	Declare		@MarkdownValue		decimal(9,2)
		
	Set			@MarkdownValue	=	
									(
									Select	SUM(MDNQ * PRIC)
									From	STKMAS
									Where	MDNQ	<>	0
									)
	
	RETURN		@MarkdownValue

END
GO

