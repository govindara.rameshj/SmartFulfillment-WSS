﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_StripVowels]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_StripVowels'
	EXEC ('CREATE FUNCTION [dbo].[udf_StripVowels]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_StripVowels'
GO
ALTER FUNCTION [dbo].[udf_StripVowels] (@StringArray varchar(max))
RETURNS varchar(max) 
AS
BEGIN
	DECLARE @Return varchar(max) 
	
	SELECT @Return = 
		REPLACE(
			REPLACE(
				REPLACE(
					REPLACE(
						REPLACE(@StringArray, 'a', '')
						, 'e', '')
					, 'i', '')
				, 'o', '')
			, 'u', '')
	
	RETURN @return
END
GO

