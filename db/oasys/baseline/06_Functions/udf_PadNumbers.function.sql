﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_PadNumbers]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_PadNumbers'
	EXEC ('CREATE FUNCTION [dbo].[udf_PadNumbers]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_PadNumbers'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 22/08/2012
-- User Story	 : 6219
-- Project		 : P022-007 - Fuzzy Logic Search.
-- Task Id		 : 7262
-- Description   : Remove any spaces added around a decimal point
--				 : during the padding process.
-- =============================================
ALTER FUNCTION udf_PadNumbers
(
	@original varchar(max),
	@delimiter char(1)
)
RETURNS varchar(max)
AS
BEGIN
	DECLARE @output varchar(max) = ''

	declare @Pointer int = 1, @MaxPointer int

	set @MaxPointer = LEN(@original)

	declare @char char(1), @isNumericChar bit = 0, @wasNumericChar bit = 0

	-- pad numbers with spaces
	while @Pointer <= @MaxPointer 
	begin
		set @char = SUBSTRING(@original, @pointer, 1)

		set @isNumericChar = case when CHARINDEX(@char, '0123456789') > 0 then 1 else 0 end
		
		-- if the status of isNumeric has changed, add a space
		--	this signifies the boundary of a numeric word
		set @output = @output +
			case when (not @isNumericChar = @wasNumericChar) OR @char = @delimiter
				then ' ' + @char
				else @char
			end

		set @wasNumericChar = @isNumericChar 
		
		set @Pointer = @Pointer + 1
	end

	return replace(@output, ' . ', '.')

END
GO

