﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable]', N'IF') IS NULL
BEGIN
	PRINT 'Creating function udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable'
	EXEC ('CREATE FUNCTION [dbo].[udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable]() RETURNS TABLE AS RETURN ( SELECT 1 AS [Column] )')
END
GO

PRINT 'Altering function udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable'
GO
ALTER FUNCTION dbo.udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable
(	
	@SkuNumbers varchar(max), 
	@Quantities varchar(max)
)
RETURNS TABLE 
AS
RETURN 
(
	select sku.Item 'SkuNumber', qty.item 'Quantity'
	from dbo.udf_SplitVarcharToTable(@SkuNumbers, ',') sku
		inner join dbo.udf_SplitVarcharToTable(@Quantities, ',') qty
			on sku.ID = qty.ID 
)
GO

