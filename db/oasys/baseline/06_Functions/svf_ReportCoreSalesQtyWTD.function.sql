﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportCoreSalesQtyWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportCoreSalesQtyWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportCoreSalesQtyWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportCoreSalesQtyWTD'
GO
ALTER FUNCTION [dbo].[svf_ReportCoreSalesQtyWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@CoreWeekQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Core SALES Qty - WDT
	----------------------------------------------------------------------------------
	Select			@CoreWeekQty	=		count(TOTL)
	From			DLTOTS DT left join vwCORHDRFull COR on DT.ORDN = COR.NUMB 
	Where			TCOD			=		'SA'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and DT.Date1	>=		@StartDate
					and DT.Date1	<=		@DateEnd
					and (cor.Source IS NULL or cor.IS_CLICK_AND_COLLECT = 0)

	Set				@CoreWeekQty	=		isnull(@CoreWeekQty, 0);
		
	RETURN			@CoreWeekQty

END
GO

