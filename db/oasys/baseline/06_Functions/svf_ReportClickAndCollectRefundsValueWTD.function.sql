﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportClickAndCollectRefundsValueWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportClickAndCollectRefundsValueWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportClickAndCollectRefundsValueWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportClickAndCollectRefundsValueWTD'
GO
ALTER FUNCTION [dbo].[svf_ReportClickAndCollectRefundsValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN
	Declare		@StartDate		date,
				@DateEnd		date,
				@CCValue			numeric(9,2)
	
	Set			@StartDate	=	@InputDate;			
	Set			@DateEnd	=	@InputDate;
	
	While		Datepart(Weekday, @StartDate) <> 1
					
				Begin
					Set	@StartDate = DateAdd(Day, -1, @StartDate)
				End	

	Select @CCValue = SUM(DT.TOTL)
	From DLTOTS DT 
	Inner Join vwCORHDRFull COR	on	DT.ORDN		= COR.NUMB
	Where	TCOD 			= 'RF'
			and COR.IS_CLICK_AND_COLLECT = 1
			and DT.DATE1	>=	@StartDate
			and DT.DATE1	<=	@DateEnd;

	Set			@CCValue	=	isnull(@CCValue, 0);
		
	RETURN		@CCValue

END
GO

