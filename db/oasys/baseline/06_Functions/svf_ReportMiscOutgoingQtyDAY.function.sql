﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportMiscOutgoingQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportMiscOutgoingQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportMiscOutgoingQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportMiscOutgoingQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Misc Outgoing Qty for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportMiscOutgoingQtyDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate			date,
					@MOQty				int
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Misc Outgoing Qty - DAY
	----------------------------------------------------------------------------------
	Select			@MOQty			=		count(TOTL)
	From			DLTOTS 
	Where			TCOD			in		('M-', 'C-')
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		=		@StartDate;

	Set				@MOQty			=		isnull(@MOQty, 0);
		
	RETURN			@MOQty

END
GO

