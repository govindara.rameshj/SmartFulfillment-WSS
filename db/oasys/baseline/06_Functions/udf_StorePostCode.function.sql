﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_StorePostCode]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_StorePostCode'
	EXEC ('CREATE FUNCTION [dbo].[udf_StorePostCode]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_StorePostCode'
GO
-- =============================================
-- Author:		Michael O'Cain
-- Create date: 17th May, 2011
-- Description:	Return the Store Post Code from RETOPT:SAD3
-- =============================================
ALTER FUNCTION [dbo].[udf_StorePostCode] 
(	
	 @StoreId as Int 
)
RETURNS VARCHAR(8) 
AS
begin
	declare @Store varchar(8)
	
	-- Add the SELECT statement with parameter references here
	SELECT @Store = PostCode FROM Store WHERE Id = @StoreId
	
	return @Store
end
GO

