﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_PicPercentageValueCalculation]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_PicPercentageValueCalculation'
	EXEC ('CREATE FUNCTION [dbo].[udf_PicPercentageValueCalculation]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_PicPercentageValueCalculation'
GO
ALTER FUNCTION [dbo].[udf_PicPercentageValueCalculation] 
(@Value       AS DEC(9 , 2), 
 @ValueOnhand AS DEC(9 , 2)) 
RETURNS DEC(9, 2) 
AS 
  BEGIN 
      DECLARE @PercentageValue DEC(9, 2) 

      SET @PercentageValue = Abs(( @Value ) / Nullif(@ValueOnhand, 0)) * 100 

      RETURN @PercentageValue 
  END
GO

