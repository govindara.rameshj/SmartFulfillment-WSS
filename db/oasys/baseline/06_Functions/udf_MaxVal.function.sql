﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_MaxVal]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_MaxVal'
	EXEC ('CREATE FUNCTION [dbo].[udf_MaxVal]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_MaxVal'
GO
ALTER FUNCTION udf_MaxVal(@val1 int, @val2 int)
	returns int
	as
	begin
	  if @val1 > @val2
	    return @val1
	  return isnull(@val2,@val1)		

	end
GO

