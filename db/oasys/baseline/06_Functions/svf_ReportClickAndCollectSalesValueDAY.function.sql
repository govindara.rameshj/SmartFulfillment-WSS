﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportClickAndCollectSalesValueDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportClickAndCollectSalesValueDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportClickAndCollectSalesValueDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportClickAndCollectSalesValueDAY'
GO
ALTER FUNCTION [dbo].[svf_ReportClickAndCollectSalesValueDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CCWeekValue		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

	
	Select @CCWeekValue = SUM(DT.TOTL)
	From DLTOTS DT 
	Inner Join vwCORHDRFull COR	on	DT.ORDN		= COR.NUMB
	Where	TCOD 			= 'SA'
			and COR.IS_CLICK_AND_COLLECT = 1
			and DT.DATE1	=	@StartDate;
	
	Set		@CCWeekValue		=	isnull(@CCWeekValue, 0)
		
	RETURN	@CCWeekValue

END
GO

