﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_SystemStoreGroupID]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_SystemStoreGroupID'
	EXEC ('CREATE FUNCTION [dbo].[svf_SystemStoreGroupID]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_SystemStoreGroupID'
GO
-- =============================================
-- Author:		Kevan Madelin
-- Version:		1.0
-- Create date: 14th March 2011
-- Description:	Returns the Local Store ID Number (I.e. 8052) Scalar Valued Function
-- =============================================
ALTER FUNCTION [dbo].[svf_SystemStoreGroupID]()
RETURNS int
AS
BEGIN
	DECLARE		@StoreID int
	Set			@StoreID = 
	(
	Select		('8' + STOR)
	From		RETOPT 
	where		FKEY = '01'
	)
	RETURN		@StoreID

END
GO

