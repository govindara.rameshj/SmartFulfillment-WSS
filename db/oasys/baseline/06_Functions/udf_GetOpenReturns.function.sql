﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_GetOpenReturns]', N'TF') IS NULL
BEGIN
	PRINT 'Creating function udf_GetOpenReturns'
	EXEC ('CREATE FUNCTION [dbo].[udf_GetOpenReturns]() RETURNS @t TABLE ([Col] int) AS BEGIN RETURN END')
END
GO

PRINT 'Altering function udf_GetOpenReturns'
GO
ALTER FUNCTION dbo.udf_GetOpenReturns
(
@SupplierNumber		VARCHAR(5)	=Null,
@Date				DATETIME	=Null
)
	
RETURNS @OutputTable TABLE(
                            Number char(6) NULL ,                             
                            SupplierNumber char(5)NULL , 
                            SupplierName char(30)NULL , 
                            DateCreated date NULL , 
                            DateCollect date NULL , 
                            DrlNumber char(6) NULL,
                            Value decimal(9,2)NULL                             
                          )
AS
BEGIN

    INSERT INTO @OutputTable
SELECT NUMB , 
       SUPP , 
       sm.NAME , 
       EDAT , 
       RDAT , 
       DRLN , 
       VALU
  FROM
       RETHDR rh INNER JOIN SUPMAS sm
       ON rh.SUPP
          = 
          sm.SUPN
  WHERE rh.DRLN
        = 
        '000000'
    AND rh.isdeleted = 0
    AND (@SupplierNumber IS NULL
      OR SUPP
         = 
         @SupplierNumber)
  ORDER BY sm.NAME , rh.NUMB;
RETURN;

END;
GO

