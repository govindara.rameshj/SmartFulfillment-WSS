﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportOutOfStocks]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportOutOfStocks'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportOutOfStocks]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportOutOfStocks'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Out of Stock count - Scalar Valued Function
-- Notes		: This uses STKMAS:ONHA value to calculate the items out of stock.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportOutOfStocks]()
RETURNS int 
AS
BEGIN

	Declare		@Quantity	int
	Declare		@TODT		date
	
	Set			@TODT	=	(Select TMDT From SYSDAT Where FKEY = '01')	
	Set			@Quantity	=	
							(
							Select	COUNT(SKUN) 
							From	STKMAS
							Where	IDEL		=	0 
									and IOBS	=	0
									and INON	=	0
									and IRIS	=	0
									and ICAT	=	0
									and NOOR	=	0
									and DATS	IS NOT NULL
									and (FODT    IS NULL or FODT	<	@TODT)
									and ONHA	<=	0
							)
	
	If @Quantity IS NULL Select @Quantity = 0;
	
	RETURN		@Quantity

END
GO

