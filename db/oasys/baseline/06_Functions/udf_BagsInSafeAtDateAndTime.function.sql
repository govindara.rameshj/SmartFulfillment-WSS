﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_BagsInSafeAtDateAndTime]', N'TF') IS NULL
BEGIN
	PRINT 'Creating function udf_BagsInSafeAtDateAndTime'
	EXEC ('CREATE FUNCTION [dbo].[udf_BagsInSafeAtDateAndTime]() RETURNS @t TABLE ([Col] int) AS BEGIN RETURN END')
END
GO

PRINT 'Altering function udf_BagsInSafeAtDateAndTime'
GO
ALTER FUNCTION udf_BagsInSafeAtDateAndTime(@Now datetime, @BagType char(1))
   returns @OutputTable table (
                               ID                   int not null,
                               SealNumber           char(20) not null,
                               [Type]               char(1) not null,
                               [State]              char(1) not null,
                               Value                decimal(9, 2) not null,
                               InPeriodID           int not null,
                               InDate               datetime not null,
                               InUserID1            int not null,
                               InUserID2            int not null,
                               OutPeriodID          int null,
                               OutDate              datetime null,
                               OutUserID1           int null,
                               OutUserID2           int null,
                               AccountabilityType   char(1) null,
                               AccountabilityID     int null,
                               FloatValue           decimal(9, 2) not null,
                               PickupPeriodID       int not null,
                               RelatedBagId         int null,
                               Comments             char(255) null,
                               FloatChecked         bit null,
                               FloatCheckedUserID1  int null,
                               FloatCheckedUserID2  int null,
                               CashDrop             bit null,
                               BagCollectionSlipNo  varchar(13) null,
                               BagCollectionComment varchar(100) null
                              )
as
begin

   insert @OutputTable select ID,
                              SealNumber,
                              [Type],
                              [State],
                              Value,
                              InPeriodID,
                              InDate,
                              InUserID1,
                              InUserID2,
                              OutPeriodID,
                              OutDate,
                              OutUserID1,
                              OutUserID2,
                              AccountabilityType,
                              AccountabilityID,
                              FloatValue,
                              PickupPeriodID,
                              RelatedBagId,
                              Comments,
                              FloatChecked,
                              FloatCheckedUserID1,
                              FloatCheckedUserID2,
                              CashDrop,
                              BagCollectionSlipNo,
                              BagCollectionComment  
                       from SafeBags
                       where [Type]   = @BagType
                       and   [State] <> 'C'
                       and   datediff(second, InDate,  @Now) >= 0
                       and  (datediff(second, OutDate, @Now) <= 0 or OutDate is null)
                       and  (@BagType = 'P' or (select count(*)
                                                from SafeBagsDenoms
                                                where BagID     = SafeBags.ID
                                                and   TenderID in (1, 2))> 0)

   return 

end
GO

