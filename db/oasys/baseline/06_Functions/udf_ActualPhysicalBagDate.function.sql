﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_ActualPhysicalBagDate]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_ActualPhysicalBagDate'
	EXEC ('CREATE FUNCTION [dbo].[udf_ActualPhysicalBagDate]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_ActualPhysicalBagDate'
GO
ALTER FUNCTION udf_ActualPhysicalBagDate(@Now datetime, @SelectedPeriodID int)
   returns datetime
as
begin

   --Scenerios considered
   --
   --1. Return datetime passed in by @Now
   --
   --2. Return date at midnight of selected @SelectedPeriodID
   --
   --3. Use the datetime stamp that is created on "end of day" check process for selected @SelectedPeriodID
   --
   --
   --
   --
   --Implementing point 2 ONLY!

   return (select EndDate from SystemPeriods where ID = @SelectedPeriodID) 

end
GO

