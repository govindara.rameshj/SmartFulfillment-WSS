﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportKBDepositsQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportKBDepositsQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportKBDepositsQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportKBDepositsQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 6th June 2011
-- Description	: Returns the figure to use for K&B Deposits Qty for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportKBDepositsQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@KBWeekQty			numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get K&B DEPOSISTS Count - DAY
	----------------------------------------------------------------------------------
	Select			@KBWeekQty		=	count(TOTL)
	From			DLTOTS 
	Where			(TCOD		=		'M+'	or TCOD		=		'M-')
					and MISC	=		'20'
					and VOID	=		0
					and PARK	=		0
					and TMOD	=		0  
					and Date1	=		@StartDate

	Set			@KBWeekQty	=		isnull(@KBWeekQty, 0);
		
	RETURN		@KBWeekQty

END
GO

