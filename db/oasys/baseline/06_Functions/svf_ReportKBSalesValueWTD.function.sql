﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportKBSalesValueWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportKBSalesValueWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportKBSalesValueWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportKBSalesValueWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 6th June 2011
-- Description	: Returns the figure to use for K&B Sales Value for WTD - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportKBSalesValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@KBWeekValue		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get K&B SALES Value - WTD
	----------------------------------------------------------------------------------
	Select			@KBWeekValue		=		sum(ValueTender * -1)
	From			VisionPayment 
	Where			TranDate			>=		@StartDate
					and TranDate		<=		@DateEnd;

	Set				@KBWeekValue		=		isnull(@KBWeekValue, 0);
		
	RETURN			@KBWeekValue

END
GO

