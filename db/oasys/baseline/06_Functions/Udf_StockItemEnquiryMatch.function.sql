﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[Udf_StockItemEnquiryMatch]', N'TF') IS NULL
BEGIN
	PRINT 'Creating function Udf_StockItemEnquiryMatch'
	EXEC ('CREATE FUNCTION [dbo].[Udf_StockItemEnquiryMatch]() RETURNS @t TABLE ([Col] int) AS BEGIN RETURN END')
END
GO

PRINT 'Altering function Udf_StockItemEnquiryMatch'
GO
ALTER FUNCTION Udf_StockItemEnquiryMatch(@Search varchar(max), @MinSize int, @MaxResults int, @Method int)
   returns @OutputTable table(ID      int         not null,
                              SKUN    nvarchar(6) not null,
                              Pointer int         not null)
begin
   declare @Length     int
   declare @Pointer    int
   declare @SKUN       table (ID int identity, SKUN char(6), MatchedCharacters int)

   --method one: perform a wildcard search on stkmas:descr & stkmas:equv fields
   --            use complete search citeria knocking one off until the min size has been reached
   if (@Method = 1)
   begin
      set @Length  = len(@Search)
      set @Pointer = len(@Search)

      while @Pointer >= @MinSize and (select count(*) from @SKUN) < @MaxResults 
      begin
            insert into @SKUN (SKUN, MatchedCharacters) select SKUN, @Pointer
                                                        from STKMAS
                                                        where DESCR like '%' + left(@Search, @Pointer) + '%'
                                                        or    EQUV  like '%' + left(@Search, @Pointer) + '%'
         set @Pointer = @Pointer - 1
      end
   end

   --method two: perform a wildcard search on stkmas:descr & stkmas:equv fields
   --            use individual words within search citeria
   --            could be easily modified to search for 'blk' if search citeria is 'black'
   if (@Method = 2)
   begin
      insert into @SKUN (SKUN, MatchedCharacters) select SKUN, 0
                                                  from STKMAS
                                                  where dbo.Udf_ValidFuzzyMatchLike('%' + @Search + '%', DESCR) = 1
                                                  or    dbo.Udf_ValidFuzzyMatchLike('%' + @Search + '%', EQUV)  = 1
                                                  order by SKUN
   end
   --method three: refinement of method two but uses "soundex" instead of "like"
   if (@Method = 3)
   begin
      insert into @SKUN (SKUN, MatchedCharacters) select SKUN, 0
                                                  from STKMAS
                                                  where dbo.Udf_ValidFuzzyMatchSoundex('%' + @Search + '%', DESCR) = 1
                                                  or    dbo.Udf_ValidFuzzyMatchSoundex('%' + @Search + '%', EQUV)  = 1
                                                  order by SKUN   
   end

   insert @OutputTable select * from @SKUN where ID <= @MaxResults
   return
end
GO

