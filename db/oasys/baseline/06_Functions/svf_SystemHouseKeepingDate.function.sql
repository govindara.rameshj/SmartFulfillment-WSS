﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_SystemHouseKeepingDate]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_SystemHouseKeepingDate'
	EXEC ('CREATE FUNCTION [dbo].[svf_SystemHouseKeepingDate]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_SystemHouseKeepingDate'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 20th April 2011
-- Description	: Returns the Date to use for Housekeeping (I.e. 2010-09-01) Scalar Valued Function
-- Notes		: This uses RETOPT:KEEP value to calculate the date used for removing old records.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_SystemHouseKeepingDate]()
RETURNS date 
AS
BEGIN

	Declare		@TMDT	date,
				@HKDT	date,
				@ROKP	int 
	
	Set			@TMDT	=	(Select TMDT From SYSDAT Where FKEY = '01')
	Set			@ROKP	=	(Select KEEP From RETOPT Where FKEY = '01')
	Set			@HKDT	=	DATEADD(Day,@ROKP*-1,@TMDT)
	
	RETURN		@HKDT

END
GO

