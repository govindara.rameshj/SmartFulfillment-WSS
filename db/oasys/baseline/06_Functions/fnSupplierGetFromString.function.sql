﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[fnSupplierGetFromString]', N'TF') IS NULL
BEGIN
	PRINT 'Creating function fnSupplierGetFromString'
	EXEC ('CREATE FUNCTION [dbo].[fnSupplierGetFromString]() RETURNS @t TABLE ([Col] int) AS BEGIN RETURN END')
END
GO

PRINT 'Altering function fnSupplierGetFromString'
GO
ALTER FUNCTION [dbo].[fnSupplierGetFromString] ( @StringInput VARCHAR(8000) = NULL )
RETURNS @OutputTable TABLE ( Id INT )
AS
BEGIN
    DECLARE @String    VARCHAR(10)

	IF (@StringInput IS NULL)
		BEGIN
			INSERT INTO @OutputTable (id) SELECT supn FROM supmas
		END
	ELSE
		BEGIN
			WHILE LEN(@StringInput) > 0
			BEGIN
				SET @StringInput	=	REPLACE(@StringInput, '(', '');
				SET @StringInput	=	REPLACE(@StringInput, ')', '');
			
				SET @String     = LEFT(@StringInput, 
										ISNULL(NULLIF(CHARINDEX(',', @StringInput) - 1, -1),
										LEN(@StringInput)))
				SET @StringInput= SUBSTRING(@StringInput,
											 ISNULL(NULLIF(CHARINDEX(',', @StringInput), 0),
											 LEN(@StringInput)) + 1, LEN(@StringInput))

				INSERT INTO @OutputTable ( Id ) VALUES ( @String )
			END		
		END
    
    RETURN
END
GO

