﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportStockHoldingValue]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportStockHoldingValue'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportStockHoldingValue]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportStockHoldingValue'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Stock Holding Value - Scalar Valued Function
-- Notes		: This uses STKMAS:ONHA+MDNQ value to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportStockHoldingValue]()
RETURNS numeric(9,2) 
AS
BEGIN

	Declare		@StockHoldingValue		numeric(9,2)
		
	Set			@StockHoldingValue		=	
										(
										Select	SUM((ONHA + MDNQ) * PRIC)
										From	STKMAS
										Where	(ONHA + MDNQ)	>	0
										)
	
	RETURN		@StockHoldingValue

END
GO

