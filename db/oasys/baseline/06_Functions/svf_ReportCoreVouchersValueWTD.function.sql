﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportCoreVouchersValueWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportCoreVouchersValueWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportCoreVouchersValueWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportCoreVouchersValueWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Core Refunds Value for WTD - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportCoreVouchersValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate				date,
					@DateEnd				date,
					@CoreVouchersValue		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Core VOUCHERS Value - WTD
	----------------------------------------------------------------------------------
	Select			@CoreVouchersValue	=		sum(DP.AMNT * -1)
	From			DLTOTS as DT
	Inner Join		DLPAID as DP        on		DP.DATE1	= DT.DATE1
										and		DP.TILL		= DT.TILL
										and		DP.[TRAN]	= DT.[TRAN]
										and		DP.[TYPE]	= 6
	Where			DT.CASH				<>		'000'
					and DT.VOID			=		0
					and DT.PARK			=		0
					and DT.TMOD			=		0  
					and DT.DATE1		>=		@StartDate
					and DT.DATE1		<=		@DateEnd;

	Set				@CoreVouchersValue	=		isnull(@CoreVouchersValue, 0);
		
	RETURN			@CoreVouchersValue

END
GO

