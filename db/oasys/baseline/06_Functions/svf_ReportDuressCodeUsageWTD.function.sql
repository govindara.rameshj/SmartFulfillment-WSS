﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportDuressCodeUsageWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportDuressCodeUsageWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportDuressCodeUsageWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportDuressCodeUsageWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Duress Code Usage for WTD - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportDuressCodeUsageWTD]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@DuressQty			int
	
	Set				@StartDate			=		@InputDate;	
	Set				@DateEnd			=		@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin	
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get Duress Code Usage - WTD
	----------------------------------------------------------------------------------
	Select			@DuressQty		=		count([TRAN])
	From			DLTOTS 
	Where			[OPEN]			=		'9'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		>=		@StartDate
					and Date1		<=		@DateEnd;

	Set				@DuressQty		=		isnull(@DuressQty, 0);
		
	RETURN			@DuressQty

END
GO

