﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[Udf_StockItemEnquiryFuzzy]', N'TF') IS NULL
BEGIN
	PRINT 'Creating function Udf_StockItemEnquiryFuzzy'
	EXEC ('CREATE FUNCTION [dbo].[Udf_StockItemEnquiryFuzzy]() RETURNS @t TABLE ([Col] int) AS BEGIN RETURN END')
END
GO

PRINT 'Altering function Udf_StockItemEnquiryFuzzy'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 22/08/2012
-- User Story	 : 6219
-- Project		 : P022-007 - Fuzzy Logic Search.
-- Task Id		 : 7262
-- Description   : Create new fuzzy logic search function based on
--				 : old UdfStockItemEnquiryFuzzy function.  Allow for new
--				 : requirements (in no particular order)...
--				 : 1. Hard code around spelling issues with Celotex and Fascia
--				 : 2. Search for concatenated find words;-
--				 :		e.g. shower head, look for showerhead too
--				 : 3. Search for find words in concatenated description;-
--				 :		e.g. look for deckboard, find in '...deck board...' 
--				 :			 as well as '...deckboard...'
--				 : 4. Ensure 'carpet gripper rods' finds 'carpet gripper' (rods)?
--				 : 5. Matching items that also have associated hierarchy category
--				 :	  description with matching word(s) to appear higher up the
--				 :	  results;-
--				 :		e.g. Oak Flooring - Oak matches that appear in the Flooring
--				 :		hierarchy category to appear higher in list than Oak matches
--				 :		that do not.
--				 : 6. Ensure numeric searching works correctly;-
--				 :		e.g. 47x100 sawn
--				 : 7. Search for 'Shower hed' brings back shower head and showerhead results
-- =============================================
ALTER FUNCTION [dbo].[Udf_StockItemEnquiryFuzzy](@Find VarChar(Max), @MaxResults Int, 	@AllowMisSpelling As Bit = 0, @ElevateForHierarchy As Bit = 0)
	Returns @OutputTable Table
		(
			ID                 Int           Not Null,
			SKU                Char(6)       Not Null,
			FinalWeightedScore Decimal(8, 2) Not Null
		)
As
Begin
	Declare @FindTable Table (ID int Identity, Find VarChar(Max), VowelsStripped Bit)
	Declare @TempTable Table (LocalID Int Identity, ID Int Not Null, SKU Char(6) Not Null, FinalWeightedScore Decimal(8, 2) Not Null)
	Declare @SearchStringWithoutVowelsOrSpacesExistsInDescriptionWithoutVowelsOrSpacesTable Table (ID Int Not Null, SKU Char(6) Not Null, Score Decimal(8, 2) Not Null)
	Declare @ExactSearchStringExistsInDescriptionTable Table (ID Int Not Null, SKU Char(6) Not Null, Score Decimal(8, 2) Not Null)
	Declare @SearchStringWithoutSpacesExistsInDescriptionTable Table (ID Int Not Null, SKU Char(6) Not Null, Score Decimal(8, 2) Not Null)
	Declare @SearchStringWithoutSpacesExistsInDescriptionWithoutSpacesTable Table (ID Int Not Null, SKU Char(6) Not Null, Score Decimal(8, 2) Not Null)
	Declare @ExactWordTable Table (ID Int Not Null, SKU Char(6) Not Null, Score Decimal(8, 2) Not Null)
	Declare @ExactCategoryWordTable Table (ID Int Not Null, SKU Char(6) Not Null, Score Decimal(8, 2) Not Null)

	Declare @ExactSearchStringExistsInDescription decimal(8,2) = 10000
	Declare @SearchStringWithoutSpacesExistsInDescription decimal(8,2) = 9900
	Declare @SearchStringWithoutSpacesExistsInDescriptionWithoutSpaces decimal(8,2) = 9800
	Declare @SearchStringWithoutVowelsOrSpacesExistsInDescriptionWithoutVowelsOrSpaces decimal(8,2) = 9700


	Declare @ExactWordWeighting Decimal(8,2) = 100
	Declare @ExactWordWithoutVowelsWeighting Decimal(8,2) = 90
	Declare @ExactCategoryWordWeighting Decimal(8,2) = 2
	Declare @ExactCategoryWordWithoutVowelsWeighting Decimal(8,2) = 1

	Declare @SpacelessFind VarChar(Max)

	If @Find Is Null Return  --performance on Null value is bad

	If @AllowMisSpelling = 1
		Begin
			Set @Find = REPLACE(@Find, 'selotex', 'celotex')
			Set @Find = REPLACE(@Find, 'selotecks', 'celotex')
			Set @Find = REPLACE(@Find, 'celotecks', 'celotex')
			-- Fascia is currently spelt two ways in stock descriptions, so look for both (convert 3rd way to one of other 2 before starting)
			Set @Find = REPLACE(@Find, 'fasia', 'fascia')	
			Set @Find = @Find + ' '
			If (PATINDEX('% facia %', @Find) > 0 Or @Find = 'facia' ) And (PATINDEX('% fascia %', @Find) = 0 And @Find <> 'fascia')
				Begin
					Set @Find = @Find + 'fascia'
				End
			Else
				Begin
					If (PATINDEX('% fascia %', @Find) > 0 Or @Find = 'fascia')
						Begin
							Set @Find = @Find + ' facia'
						End
				End
			Set @Find = RTRIM(@Find)
		End
	Set @SpacelessFind = REPLACE(@Find, ' ', '')

	Insert Into 
		@FindTable
			(
				Find, VowelsStripped
			) 
		Select 
			item, 0 
		From 
 			dbo.udf_SplitVarcharToTable(dbo.udf_PadNumbers(@Find, ' '), ' ')
		Where 
			Len(item) > 0 
		And 
			Item <> 'x'

	Insert Into 
		@FindTable 
			(
				Find, VowelsStripped
			) 
		Select 
			item, 1 
		From 
			dbo.udf_SplitVarcharToTable(dbo.udf_StripVowels(@Find), ' ')
		Where
			Len(item) > 0 
		And 
			Item <> 'x'

	--Like comparison on description
	Insert Into
		@ExactSearchStringExistsInDescriptionTable
		Select
			sk.id, 
			sk.sku, 
			Score = @ExactSearchStringExistsInDescription
		From 
			sku sk
		Where
			sk.descr Like '%' + @Find + '%'
		Group By 
			sk.id, sk.sku, sk.descr
		Order By
			Score Desc, 
			sku Asc
			
	--Like comparison on description
	Insert Into
		@SearchStringWithoutSpacesExistsInDescriptionTable
		Select
			sk.id, 
			sk.sku, 
			WeightedScore = @SearchStringWithoutSpacesExistsInDescription
		From sku sk
		Where
			sk.descr Like '%' + @SpacelessFind + '%'
		Group By 
			sk.id, sk.sku, sk.descr
		Order By
			WeightedScore Desc, 
			sku Asc
			
	--Like comparison without spaces on description without spaces
	Insert Into
		@SearchStringWithoutSpacesExistsInDescriptionWithoutSpacesTable
		Select 
			sk.id, 
			sk.sku, 
			Score = @SearchStringWithoutSpacesExistsInDescriptionWithoutSpaces
		From 
			sku sk
		Where
			sk.SpacelessDescription Like '%' + @SpacelessFind + '%'
		Group By 
			sk.id, sk.sku, sk.descr
		Order By
			Score Desc, 
			sku Asc
	
	-- Like comparison without vowels or spaces on description without vowels or spaces 
	-- Only populated if @AllowMisSpelling parameter is set to 1
	Insert Into
		@SearchStringWithoutVowelsOrSpacesExistsInDescriptionWithoutVowelsOrSpacesTable
		Select
			sk.id, 
			sk.sku, 
			Score = @SearchStringWithoutVowelsOrSpacesExistsInDescriptionWithoutVowelsOrSpaces
		From 
			sku sk
		Where
			@AllowMisSpelling = 1
		And
			dbo.udf_StripVowels(sk.SpacelessDescription) Like '%' + dbo.udf_StripVowels(@SpacelessFind) + '%'
		Group By 
			sk.id, 
			sk.sku, 
			sk.descr
		Order By
			Score Desc, 
			sku Asc

	--check for existence of matching words
	Insert Into
		@ExactWordTable
		Select 
			sk.id, 
			sk.sku, 
			Score = Count(*) *	Case
									When ft.VowelsStripped = 0 Then @ExactWordWeighting
									Else @ExactWordWithoutVowelsWeighting
								End
		From
			sku sk
				Inner Join 
					skuword skwrd
				On 
					sk.id = skwrd.skuid
				Inner Join 
					@FindTable ft 
				On 
					skwrd.word = ft.find
		Group By 
			sk.id, sk.sku, sk.descr, ft.VowelsStripped
		Order By
			Score Desc, 
			sku Asc
	
	-- search for word which exist in a category
	-- Only populated in @ElevateForHierarchy parameter is set to 1
	Insert Into
		@ExactCategoryWordTable		
		Select 
			skwrd.SkuId, 
			skwrd.sku, 
			Score = Sum(
							Case
								When ft.VowelsStripped = 0 Then @ExactCategoryWordWeighting
								Else @ExactCategoryWordWithoutVowelsWeighting
							End
						)
		From
			@FindTable ft
				inner join 
					HierarchyWord hwd
				On 
					ft.Find = hwd.Word
						Inner Join 
							skuword skwrd
						On 
							skwrd.Word = hwd.Word
						And
							(
								hwd.NUMB = skwrd.Category
							Or
								hwd.GROU = skwrd.[Group]
							Or
								hwd.SGRP = skwrd.SubGroup
							Or
								hwd.STYL = skwrd.Style
							)									
		Where
			@ElevateForHierarchy = 1
		Group By 
			skwrd.SkuId, skwrd.sku, ft.VowelsStripped
		Order By
			Score Desc, 
			skwrd.sku Asc

	
	Insert 
		@TempTable 
			(
				ID, SKU, FinalWeightedScore
			) 
		Select 
			id, sku, FinalWeightedScore = Sum(Score)
		From 
			(
				--Like comparison on description
				Select * From @ExactSearchStringExistsInDescriptionTable
			Union All
				--Like comparison without spaces on description
				Select * From @SearchStringWithoutSpacesExistsInDescriptionTable
			Union All
				--Like comparison without spaces on description without spaces
				Select * From @SearchStringWithoutSpacesExistsInDescriptionWithoutSpacesTable
			Union All
				---Like comparison without vowels or spaces on description without vowels or spaces (only if @AllowMisSpelling is set to 1; see above)
				Select * From @SearchStringWithoutVowelsOrSpacesExistsInDescriptionWithoutVowelsOrSpacesTable
			Union All
				--check for existence of matching words
				Select * From @ExactWordTable
			Union All
				-- search for word which exist in a category (only if @ElevateForHierarchy is set to 1; see above)
				Select * From @ExactCategoryWordTable
			) a
		Group By 
			a.id, a.sku
		Order By 
			Sum(A.Score) Desc, 
			a.sku Asc
		
	Insert Into
		@OutputTable 
	Select 
		ID, SKU, FinalWeightedScore
	From 
		@TempTable lst
	Where 
		LocalID <= @MaxResults
	Order By 
		lst.FinalWeightedScore desc, 
		lst.SKU

   Return
End
GO

