﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportGiftVoucherAllocationValueDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportGiftVoucherAllocationValueDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportGiftVoucherAllocationValueDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportGiftVoucherAllocationValueDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Gift Voucher Allocation Value for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportGiftVoucherAllocationValueDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@GiftValue			numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Gift Voucher Allocation Value - DAY
	----------------------------------------------------------------------------------
	Select			@GiftValue				=		sum(AMNT)
	From			DLGIFT
	Where			[TYPE]					in		('SA', 'TR', 'CR', 'CC' ,'VR', 'RC')
					and Date1				=		@StartDate;

	Set				@GiftValue				=		isnull(@GiftValue, 0);
		
	RETURN			@GiftValue

END
GO

