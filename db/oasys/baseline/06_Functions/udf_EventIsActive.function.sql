﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_EventIsActive]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_EventIsActive'
	EXEC ('CREATE FUNCTION [dbo].[udf_EventIsActive]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_EventIsActive'
GO
ALTER FUNCTION [dbo].[udf_EventIsActive]
(
	@Start		DATETIME,
	@End		DATETIME,
	@StartTime	CHAR(4),
	@EndTime	CHAR(4),
	@ActiveDay1	BIT,
	@ActiveDay2	BIT,
	@ActiveDay3	BIT,
	@ActiveDay4	BIT,
	@ActiveDay5	BIT,
	@ActiveDay6	BIT,
	@ActiveDay7	BIT,
	@Deleted	BIT
)
RETURNS BIT
BEGIN
	-- Lots of variables for intermediate stage values, so not recalling functions and can see
	-- values in sql debug window (function call result not shown directly in debugger)
	DECLARE
		@Today			  DATETIME,
		@StartToToday	  INT,
		@EndToToday		  INT,
		@STime			  DATETIME,
		@StartTimeIsNull  INT,
		@EndIsNull		  INT,
		@ETime			  DATETIME,
		@EndTimeIsNull	  INT,
		@TodayHours		  INT,
		@TodayMinutes	  INT,
		@TodaySeconds	  INT,
		@StartHours		  INT,
		@StartMinutes	  INT,
		@StartSeconds	  INT,
		@EndHours		  INT,
		@EndMinutes		  INT,
		@EndSeconds		  INT,
		@CurrentWeekDay   INT,
		-- store the result, again mostly for debugging, could select it directly
		@IsActive		  BIT;

	-- Set up function result values for use later on	
	SET @Today		      = GETDATE()
	SET @StartToToday	  = DATEDIFF(D, @Start, @Today)
	SET @EndToToday		  = DATEDIFF(D, @End, @Today)
	SET @StartTimeIsNull  = 
		CASE
			WHEN
				@StartTime = '0000'
			THEN
				1
			ELSE
				0
		END
	SET @STime			  = CONVERT(DATETIME, SUBSTRING(@StartTime, 1, 2) + ':' + SUBSTRING(@StartTime, 3, 2) + ':00', 108) 
	SET @EndIsNull        = DATEDIFF(DD, ISNULL(@End, @Today),  ISNULL(@End, DATEADD(DD, 1, @Today)))
	SET @EndTimeIsNull    = 
		CASE
			WHEN
				@EndTime = '0000'
			THEN
				1
			ELSE
				0
		END
	SET @ETime			  = CONVERT(DATETIME, SUBSTRING(@EndTime, 1, 2) + ':' + SUBSTRING(@EndTime, 3, 2) + ':00', 108) 
	SET @TodayHours		  = DATEPART(HH, @Today)
	SET @TodayMinutes	  = DATEPART(MI, @Today)
	SET @TodaySeconds	  = DATEPART(SS, @Today)
	SET @StartHours		  = 
		CASE
			WHEN
				@StartTimeIsNull = 1
			THEN
				0
		ELSE
			DATEPART(HH, @STime)
		END
	SET @StartMinutes =	
		CASE
			WHEN
				@StartTimeIsNull = 1
			THEN
				0
		ELSE
			DATEPART(MI, @STime)
		END
	SET @StartSeconds =
		CASE
			WHEN
				@StartTimeIsNull = 1
			THEN
				0
		ELSE
			DATEPART(SS, @STime)
		END
	SET @EndHours = 
		CASE
			WHEN
				@EndTimeIsNull = 1
			THEN
				0
		ELSE
			DATEPART(HH, @ETime)
		END
	SET @EndMinutes	=
		CASE
			WHEN
				@EndTimeIsNull = 1
			THEN
				0
		ELSE
			DATEPART(MI, @ETime)
		END
	SET @EndSeconds	=
		CASE
			WHEN
				@EndTimeIsNull = 1
			THEN
				0
		ELSE
			DATEPART(SS, @ETime)
		END
	-- Calculate consistent number based on current DateFirst and today's
	-- day of the week; Monday = 2, Tues=3, Wed=4...Fri=6, Sat=1, Sun=1
	-- so for e.g.  if datefirst is 7 (default) and today is Wed then
	-- (7 + 4) % 7 => 11 % 7 => 4;
	-- but if datefirst is Monday and today is Wed then
	-- (1 + 3) % 7 => 4  % 7 => 4;
	-- i.e. getting 4 for Wed no matter what datefirst is set at.
	SET @CurrentWeekDay = (@@DATEFIRST + DATEPART(DW, @Today)) % 7

	-- Use all parameters and variables to calculate whether the event is active
	-- or not and return an appropriate string
	SET
		@IsActive =
			CASE
				-- No time part, so today falls between start and end (or treat as unending end if end is null)
				-- and today is an ActiveDay
				WHEN 
					 @StartToToday >= 0
				AND
					(
						@EndToToday <= 0
					OR
						@EndIsNull = 1
					)
				AND
					@StartTimeIsNull = 1
				AND
					(	
						-- @CurrentWeekDay: Monday = 2, Tues=3, Wed=4...Fri=6, Sat=1, Sun=1
						-- @ActiveDay1 = Mon... ActiveDay7=Sun
						CASE
							WHEN
								@CurrentWeekDay = 2
							AND
								@ActiveDay1 = 1
							THEN
								1
							WHEN
								@CurrentWeekDay = 3
							AND
								@ActiveDay2 = 1
							THEN
								1
							WHEN
								@CurrentWeekDay = 4
							AND
								@ActiveDay3 = 1
							THEN
								1
							WHEN
								@CurrentWeekDay = 5
							AND
								@ActiveDay4 = 1
							THEN
								1
							WHEN
								@CurrentWeekDay = 6
							AND
								@ActiveDay5 = 1
							THEN
								1
							WHEN
								@CurrentWeekDay = 0
							AND
								@ActiveDay6 = 1
							THEN
								1
							WHEN
								@CurrentWeekDay = 1
							AND
								@ActiveDay7 = 1
							THEN
								1
							ELSE
								0
						END = 1
					)
				THEN
					1
				-- Have time part, so still check if today falls between start and end (or treat as unending end if end is null)
				-- but also that the now falls between the start and end times (or to end of day if no end time)
				-- and that the day of the week for today falls on the corresponding ActiveDay having a value of 1.
				--  NB ActiveDay1 is for Monday through to ActiveDay7 that is Sunday
				WHEN 
					@StartToToday >= 0
				AND 
					(
						@EndToToday <= 0
					OR
						@EndIsNull = 1
					)
				AND
					(
						@StartTimeIsNull = 0
					AND
						(
							CASE
								WHEN
									@TodayHours > @StartHours
								OR
									(	
										@TodayHours = @StartHours
									AND
										(
											@TodayMinutes > @StartMinutes
										OR
											(
												@TodayHours = @StartHours
											AND
												@TodayMinutes = @StartMinutes
											AND
												@TodaySeconds >= @StartSeconds
											)
										)
									)
								THEN
									1
							ELSE
								0
							END = 1
						)
					AND
						(
							@EndTimeIsNull = 1
						OR
							(
								CASE
									WHEN
										@TodayHours < @EndHours
									OR
										(	
											@TodayHours = @EndHours
										AND
											(
												@TodayMinutes < @EndMinutes
											OR
												(
													@TodayHours = @EndHours
												AND
													@TodayMinutes = @EndMinutes
												AND
													@TodaySeconds <= @EndSeconds
												)
											)
										)
									THEN
										1
								ELSE
									0
								END = 1
							)
						)
					AND
						-- Today is an active day
						(	
							-- @CurrentWeekDay: Monday = 2, Tues=3, Wed=4...Fri=6, Sat=1, Sun=1
							-- @ActiveDay1 = Mon... ActiveDay7=Sun
							CASE
								WHEN
									@CurrentWeekDay = 2
								AND
									@ActiveDay1 = 1
								THEN
									1
								WHEN
									@CurrentWeekDay = 3
								AND
									@ActiveDay2 = 1
								THEN
									1
								WHEN
									@CurrentWeekDay = 4
								AND
									@ActiveDay3 = 1
								THEN
									1
								WHEN
									@CurrentWeekDay = 5
								AND
									@ActiveDay4 = 1
								THEN
									1
								WHEN
									@CurrentWeekDay = 6
								AND
									@ActiveDay5 = 1
								THEN
									1
								WHEN
									@CurrentWeekDay = 0
								AND
									@ActiveDay6 = 1
								THEN
									1
								WHEN
									@CurrentWeekDay = 1
								AND
									@ActiveDay7 = 1
								THEN
									1
								ELSE
									0
							END = 1
						)
					)
				AND
					@Deleted = 0
				THEN
					1
			ELSE
				0
			END
	RETURN @IsActive
END
GO

