﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportCoreRefundsQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportCoreRefundsQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportCoreRefundsQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportCoreRefundsQtyDAY'
GO
ALTER FUNCTION [dbo].[svf_ReportCoreRefundsQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@CoreRefundQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

	----------------------------------------------------------------------------------
	-- Get Core REFUND Qty - DAY
	----------------------------------------------------------------------------------
	Select			@CoreRefundQty	=		count(TOTL)
	From			DLTOTS DT left join vwCORHDRFull COR on DT.ORDN = COR.NUMB 
	Where			TCOD			=		'RF'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and DT.Date1	=		@StartDate
					and (cor.Source IS NULL or cor.IS_CLICK_AND_COLLECT = 0)

	Set				@CoreRefundQty	=		isnull(@CoreRefundQty, 0);
		
	RETURN			@CoreRefundQty

END
GO

