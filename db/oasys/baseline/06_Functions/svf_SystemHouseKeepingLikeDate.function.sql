﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_SystemHouseKeepingLikeDate]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_SystemHouseKeepingLikeDate'
	EXEC ('CREATE FUNCTION [dbo].[svf_SystemHouseKeepingLikeDate]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_SystemHouseKeepingLikeDate'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date		: 20th April 2011
-- Description		: Returns the Date to use for Housekeeping (I.e. dd/mm/yy) Scalar Valued Function
-- Notes		: This uses RETOPT:KEEP value to calculate the date used for removing old records.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_SystemHouseKeepingLikeDate]()
RETURNS char(8) 
AS
BEGIN

	Declare		@TMDT	date,
				@ROKP	int,
				@GDAT	date 
	
	Set			@TMDT	=	(Select TMDT From SYSDAT Where FKEY = '01')
	Set			@ROKP	=	(Select KEEP From RETOPT Where FKEY = '01')
	Set			@GDAT	=	DATEADD(Day,@ROKP*-1,@TMDT)
	
	Declare 	@SYear 		char (2)
	Declare 	@SMonth 	char (2)
	Declare 	@SDay 		char (2)
	Declare 	@FullDate 	char (8)

	Set 		@SYear 		= RIGHT(DATENAME(YY, @GDAT),2)
	Set 		@SMonth 	= RIGHT('0'+CONVERT(VARCHAR,DATEPART(MM, @GDAT)),2)
	Set 		@SDay 		= RIGHT('0'+DATENAME(DD, @GDAT),2)
	Set 		@FullDate 	= @SDay + '/' + @SMonth + '/' + @SYear

	RETURN		@FullDate

END
GO

