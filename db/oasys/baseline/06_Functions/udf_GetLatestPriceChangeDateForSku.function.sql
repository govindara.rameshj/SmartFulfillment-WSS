﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_GetLatestPriceChangeDateForSku]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_GetLatestPriceChangeDateForSku'
	EXEC ('CREATE FUNCTION [dbo].[udf_GetLatestPriceChangeDateForSku]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_GetLatestPriceChangeDateForSku'
GO
ALTER FUNCTION [dbo].[udf_GetLatestPriceChangeDateForSku] 
(
	@SKU char(6)
)

RETURNS date
AS
BEGIN
	DECLARE @ReturnDate date
	DECLARE @Status char(1)
	DECLARE	@CurrentDate date
	SET @CurrentDate = DATEADD(dd, DATEDIFF(dd,0,GETDATE()), 0)
	
/*
** Get the minimum unapplied start date for a record with an end date on or after today 
** and a start date on or before today.
*/

	SELECT @ReturnDate = MIN(e.SDAT)
	FROM PRCCHG p
	JOIN EVTCHG e ON p.SKUN = e.SKUN
				AND p.EVNT = e.NUMB
				AND p.PRIO = e.PRIO
	WHERE e.SKUN = @sku
	AND e.EDAT >= @CurrentDate
	AND p.PSTA = 'U'
	
/*
** Get the maximum record with a start date before today and an end date grouped by status.
** The effective date will be the end date + 1 day.
*/

	IF @ReturnDate IS NULL
	BEGIN
		SELECT TOP 1 @ReturnDate = DATEADD(day,1,MAX(e.EDAT)),@Status = CASE p.PSTA WHEN 'U' THEN 'U' ELSE 'S' END
		FROM PRCCHG p
		JOIN EVTCHG e ON p.SKUN = e.SKUN
					AND p.EVNT = e.NUMB
					AND p.PRIO = e.PRIO
		WHERE e.SKUN = @sku
		AND e.EDAT IS NOT NULL
		AND e.SDAT < @CurrentDate
		GROUP BY p.PSTA
		ORDER BY 2 DESC
		
		IF @ReturnDate IS NOT NULL
		BEGIN
			SELECT TOP 1 @ReturnDate = MIN(e.SDAT),@Status = CASE p.PSTA WHEN 'U' THEN 'U' ELSE 'S' END
			FROM PRCCHG p
			JOIN EVTCHG e ON p.SKUN = e.SKUN
						AND p.EVNT = e.NUMB
						AND p.PRIO = e.PRIO
			WHERE e.SKUN = @sku
			AND e.EDAT IS NULL
			AND e.SDAT > @ReturnDate
			GROUP BY p.PSTA
			ORDER BY 2 DESC
		END
	END
	
/*
** Get the minimum start date, ordered by status
*/

	IF @ReturnDate IS NULL
	BEGIN
		SELECT TOP 1 @ReturnDate = MAX(e.SDAT),@Status = CASE p.PSTA WHEN 'U' THEN 'U' ELSE 'S' END
		FROM PRCCHG p
		JOIN EVTCHG e ON p.SKUN = e.SKUN
					AND p.EVNT = e.NUMB
					AND p.PRIO = e.PRIO
		WHERE e.SKUN = @sku
		GROUP BY p.PSTA
		ORDER BY 2 DESC
	END

	RETURN @ReturnDate

END
GO

