﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportNETSalesValueWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportNETSalesValueWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportNETSalesValueWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportNETSalesValueWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Total Sales Value for WTD - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportNETSalesValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@WeekValue			numeric(9,2),
					@CoreWeekValue		numeric(9,2),
					@KbWeekValue		numeric(9,2),
					@VoucherWeekValue	numeric(9,2),
					@TotalWeekValue		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	
					
	----------------------------------------------------------------------------------				
	-- Get CORE SALES + REFUNDS - WTD
	----------------------------------------------------------------------------------
	Select		@CoreWeekValue	=	sum(TOTL)
	From		dltots dl
	Where		(dl.TCOD		=		'SA'		or		dl.TCOD		=	'RF') 
				and (dl.DATE1	<=		@DateEnd	and		dl.date1	>=		@StartDate)
				and	dl.CASH		<>		'000'
				and	dl.VOID		=		0
				and	dl.PARK		=		0
				and	dl.TMOD		=		0;	
	
	----------------------------------------------------------------------------------
	-- Get K&B DEPOSISTS - WTD
	----------------------------------------------------------------------------------
	Select			@KbWeekValue		=	sum(TOTL)
	From			DLTOTS 
	Where			(TCOD		=		'M+'	or TCOD		=		'M-')
					and MISC	=		'20'
					and VOID	=		0
					and PARK	=		0
					and TMOD	=		0  
					and Date1	>=		@StartDate
					and Date1	<=		@DateEnd;

	----------------------------------------------------------------------------------
	-- Get VOUCHERS - WTD
	----------------------------------------------------------------------------------
	Select			@VoucherWeekValue	=	sum(dp.amnt * -1)
	From			dltots dl
	inner join		dlpaid dp			on	dp.date1 = dl.date1 
					and	dp.till			=	dl.till
					and	dp.[tran]		=	dl.[tran]
					and dp.[type]		=	6
	Where			dl.DATE1			<=	@DateEnd
					and dl.date1		>=	@StartDate
					and	dl.CASH			<> '000'
					and	dl.VOID			=	0
					and	dl.PARK			=	0
					and	dl.TMOD			=	0
					
	----------------------------------------------------------------------------------
	-- Calculate Final Figure - WTD Sales (NET)
	----------------------------------------------------------------------------------					
	If @CoreWeekValue is NULL set @CoreWeekValue = 0;
	If @KbWeekValue is NULL set @KbWeekValue = 0;
	If @VoucherWeekValue is NULL set @VoucherWeekValue = 0;
	Set @TotalWeekValue	= (@CoreWeekValue +@KbWeekValue -@VoucherWeekValue);
	If @TotalWeekValue is NULL set @TotalWeekValue = 0;
					
	RETURN		@TotalWeekValue

END
GO

