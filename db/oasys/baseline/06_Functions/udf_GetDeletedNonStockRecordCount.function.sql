﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_GetDeletedNonStockRecordCount]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_GetDeletedNonStockRecordCount'
	EXEC ('CREATE FUNCTION [dbo].[udf_GetDeletedNonStockRecordCount]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_GetDeletedNonStockRecordCount'
GO
ALTER FUNCTION [dbo].[udf_GetDeletedNonStockRecordCount]
(
)
RETURNS int 
AS
BEGIN

	Declare		@NonStockQty		int
		
	Set			@NonStockQty		=	
									(
									Select COUNT(*)
									From	STKMAS st
									inner join HIEMAS hm 
									on st.CTGY=hm.NUMB and hm.LEVL=5
									Where										
									 st.ONHA <> 0									 
									 AND (st.IDEL=1 or st.IOBS=1 and st.INON = 0)                                  
                                                  
									 
									)
	
	RETURN		@NonStockQty

END
GO

