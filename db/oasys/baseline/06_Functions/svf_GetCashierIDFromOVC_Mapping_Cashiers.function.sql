﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_GetCashierIDFromOVC_Mapping_Cashiers]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_GetCashierIDFromOVC_Mapping_Cashiers'
	EXEC ('CREATE FUNCTION [dbo].[svf_GetCashierIDFromOVC_Mapping_Cashiers]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_GetCashierIDFromOVC_Mapping_Cashiers'
GO
ALTER FUNCTION [dbo].[svf_GetCashierIDFromOVC_Mapping_Cashiers]
(
@InputTillID int
)
RETURNS int
AS
BEGIN
    DECLARE @CashierID int
    
    SELECT @CashierID = [Mapping_CashierID]
    FROM [dbo].[OVC_Mapping_Cashiers]  
    WHERE   [OVC_TillID] =  @InputTillID
        
    RETURN @CashierID
END
GO

