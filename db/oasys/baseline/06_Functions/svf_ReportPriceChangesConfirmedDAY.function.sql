﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportPriceChangesConfirmedDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportPriceChangesConfirmedDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportPriceChangesConfirmedDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportPriceChangesConfirmedDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 14th June 2011
-- Description	: Returns the figure to use for Price Changes Confirmed - Scalar Valued Function
-- Notes		: This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 7th July 2011
-- Description	: Excluded non stock items
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportPriceChangesConfirmedDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate				date,
					@Confirms				int
	
	Set				@StartDate			=		@InputDate;		

					
	----------------------------------------------------------------------------------
	-- Get Price Changes Confirmed
	----------------------------------------------------------------------------------
	Select			@Confirms			=		count(PC.SKUN)
	From			PRCCHG as PC
	Inner Join		STKMAS as SM		on		SM.SKUN = PC.SKUN
	Where			SM.DPRC				=		@StartDate
					and sm.INON = 0;

	Set				@Confirms			=		isnull(@Confirms, 0);
		
	RETURN			@Confirms

END
GO

