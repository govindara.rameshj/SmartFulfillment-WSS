﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_SystemHouseKeepingPeriod]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_SystemHouseKeepingPeriod'
	EXEC ('CREATE FUNCTION [dbo].[svf_SystemHouseKeepingPeriod]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_SystemHouseKeepingPeriod'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date		: 20th April 2011
-- Description		: Returns the Date to use for Housekeeping (I.e. 2010-09-01) Scalar Valued Function
-- Notes		: This uses RETOPT:KEEP value to calculate the date used for removing old records.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_SystemHouseKeepingPeriod]()
RETURNS int 
AS
BEGIN

	Declare			@TMDT	date,
				@HKPD	int,
				@ROKP	int,
				@GDAT	date 
	
	Set			@TMDT	=	(Select TMDT From SYSDAT Where FKEY = '01')
	Set			@ROKP	=	(Select KEEP From RETOPT Where FKEY = '01')
	Set			@GDAT	=	DATEADD(Day,@ROKP*-1,@TMDT)
	Set			@HKPD	=	(Select Id From SystemPeriods Where StartDate = @GDAT) 
	
	RETURN			@HKPD

END
GO

