﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportCoreScanningQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportCoreScanningQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportCoreScanningQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportCoreScanningQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Core Scanning Qty for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportCoreScanningQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate				date,
					@CoreScanQty			numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Core Scanning Qty - WTD
	----------------------------------------------------------------------------------
	Select			@CoreScanQty		=		count(DL.IBAR)
	From			DLTOTS as DT
	Inner Join		DLLINE as DL        on		DL.DATE1	= DT.DATE1
										and		DL.TILL		= DT.TILL
										and		DL.[TRAN]	= DT.[TRAN]
	Where			DL.IBAR				=		1
					and DL.LREV			=		0
					and DT.CASH			<>		'000'
					and DT.VOID			=		0
					and DT.PARK			=		0
					and DT.TMOD			=		0  
					and DT.DATE1		=		@StartDate

	Set				@CoreScanQty		=		isnull(@CoreScanQty, 0);
		
	RETURN			@CoreScanQty

END
GO

