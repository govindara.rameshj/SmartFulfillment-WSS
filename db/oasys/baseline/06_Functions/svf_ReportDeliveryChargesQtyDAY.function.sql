﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportDeliveryChargesQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportDeliveryChargesQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportDeliveryChargesQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportDeliveryChargesQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Delivery Charges Value for DAY - Scalar Valued Function
-- Notes		: This uses CORHDR to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportDeliveryChargesQtyDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate				date,
					@DeliveryChargeQ_WD		int
	
	Set				@StartDate				=		@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Retrieve Delivery Charges - Qty(DAY)
	----------------------------------------------------------------------------------
	Select			@DeliveryChargeQ_WD		=		count(ch.DCST)
	
	From			CORHDR					as		ch
	Inner Join		CORHDR4					as		c4		
											on		(c4.NUMB = ch.NUMB) and (c4.DATE1 = ch.DATE1)
	
	Where			ch.DATE1				=		(@StartDate)
					and ch.CANC				<		'1' 
					and	ch.DELI				=		'1'
					and ch.DCST				<>		'0.00'
					and ch.STIL				<		'13'
					and c4.SellingStoreId	=		(Select Oasys.dbo.svf_SystemStoreGroupID())

	Set				@DeliveryChargeQ_WD		=		isnull(@DeliveryChargeQ_WD, 0);
		
	RETURN			@DeliveryChargeQ_WD

END
GO

