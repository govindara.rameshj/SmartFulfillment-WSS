﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_GetEventHeaderOverrideNextRevision]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_GetEventHeaderOverrideNextRevision'
	EXEC ('CREATE FUNCTION [dbo].[udf_GetEventHeaderOverrideNextRevision]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_GetEventHeaderOverrideNextRevision'
GO
ALTER FUNCTION [dbo].[udf_GetEventHeaderOverrideNextRevision](@EventTypeID int, @StartDate date, @EndDate date, @SkuNumbers varchar(max), @Quantities varchar(max))
   returns int
as
begin
	declare @Value int, @NumberOfSKUsToMatch int 
	select @NumberOfSKUsToMatch = count(*) from dbo.udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable(@SkuNumbers, @Quantities)

	declare @HeaderIDsContainingMatch table(HeaderID int)
	declare @HeaderIDsExactMatch table(HeaderID int)

	-- find deal groups which contain the matching sku/quantity pairs
	insert into @HeaderIDsContainingMatch 
	select det.EventHeaderOverrideID 
	from EventHeaderOverride hdr
		inner join EventDetailOverride det
			on det.EventHeaderOverrideID = hdr.ID
		inner join dbo.udf_ConvertDelimitedSkusAndQuantitiesToDealGroupTable(@SkuNumbers, @Quantities) dg
			on dg.SkuNumber = det.SkuNumber
				and dg.Quantity = det.Quantity
	where hdr.EventTypeID = @EventTypeID
		and hdr.StartDate <= @StartDate
		and hdr.EndDate >= @EndDate
	group by det.EventHeaderOverrideID
	having COUNT(*) = @NumberOfSKUsToMatch

	-- check that matching skus found account for all skus in the deal group
	insert into @HeaderIDsExactMatch
	select det.EventHeaderOverrideID from EventDetailOverride det
		inner join @HeaderIDsContainingMatch hdr
			on hdr.HeaderID = det.EventHeaderOverrideID 
	group by det.EventHeaderOverrideID
	having COUNT(*) = @NumberOfSKUsToMatch

	select @Value = Revision + 1 
		from EventHeaderOverride 
		where ID = (select MAX(ex.HeaderID) 'Matching Header ID' 
					from @HeaderIDsExactMatch ex)

	return ISNULL(@Value, 1)

end
GO

