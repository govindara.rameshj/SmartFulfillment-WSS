﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportDuressCodeUsageDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportDuressCodeUsageDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportDuressCodeUsageDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportDuressCodeUsageDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Duress Code Usage for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportDuressCodeUsageDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate			date,
					@DuressQty			int
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Duress Code Usage - DAY
	----------------------------------------------------------------------------------
	Select			@DuressQty		=		count([TRAN])
	From			DLTOTS 
	Where			[OPEN]			=		'9'
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		=		@StartDate;

	Set				@DuressQty		=		isnull(@DuressQty, 0);
		
	RETURN			@DuressQty

END
GO

