﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportPendingWriteOffValueDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportPendingWriteOffValueDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportPendingWriteOffValueDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportPendingWriteOffValueDAY'
GO
ALTER FUNCTION [dbo].[svf_ReportPendingWriteOffValueDAY]()
	Returns Numeric(9,2)
As
	Begin
		Declare @WOValue Numeric(9,2)
	Select
		@WOValue = Abs(Sum(SA.QUAN * SA.PRIC))
	From
		STKADJ As SA
			Inner Join
				STKMAS As SM
			On
				SA.SKUN = SM.SKUN
	Where
		SA.MOWT = 'W'
	And
		SA.WAUT = 0;

	Set @WOValue = IsNull(@WOValue, 0);
		
	Return @WOValue
End
GO

