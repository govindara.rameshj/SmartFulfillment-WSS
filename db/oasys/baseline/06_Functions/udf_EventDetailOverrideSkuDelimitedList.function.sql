﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_EventDetailOverrideSkuDelimitedList]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_EventDetailOverrideSkuDelimitedList'
	EXEC ('CREATE FUNCTION [dbo].[udf_EventDetailOverrideSkuDelimitedList]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_EventDetailOverrideSkuDelimitedList'
GO
ALTER FUNCTION dbo.udf_EventDetailOverrideSkuDelimitedList
(
   @EventHeaderOverrideID int
)
returns varchar(max)
as
begin
   declare @List varchar(max)

   select @List = coalesce(@List + ',', '') + SkuNumber
   from EventDetailOverride
   where EventHeaderOverrideID = @EventHeaderOverrideID

   return @List

end
GO

