﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_EventGetCategory]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_EventGetCategory'
	EXEC ('CREATE FUNCTION [dbo].[udf_EventGetCategory]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_EventGetCategory'
GO
ALTER FUNCTION [dbo].[udf_EventGetCategory]
(
	@CategoryNumber	CHAR(6)
)
RETURNS CHAR(50)
BEGIN
	DECLARE @MyCount	INT,
			@Category	CHAR(50)

	SET @MyCount =
		(
			SELECT 
				COUNT(*)
			FROM
				HIESTY
			WHERE
				HIESTY.STYL = @CategoryNumber
		)
	IF @MyCount = 0
		BEGIN
			SET @MyCount =
				(
					SELECT 
						COUNT(*)
					FROM
						HIESGP
					WHERE
						HIESGP.SGRP = @CategoryNumber
				)
			IF @MyCount = 0 
				BEGIN
					SET @MyCount =
						(
							SELECT 
								COUNT(*)
							FROM
								HIEGRP
							WHERE
								HIEGRP.GROU = @CategoryNumber
						)
					IF @MyCount = 0 
						BEGIN
							SET @MyCount =
								(
									SELECT 
										COUNT(*)
									FROM
										HIECAT
									WHERE
										HIECAT.NUMB = @CategoryNumber
								)
							IF @MyCount = 0 
								BEGIN
									SET @Category = 'Category Unknown'
								END
							ELSE
								BEGIN
									SET @Category = 
										(
											SELECT TOP(1)
												DESCR
											FROM
												HIECAT
											WHERE
												HIECAT.NUMB = @CategoryNumber
										)
								END
						END
					ELSE
						BEGIN
							SET @Category = 
								(
									SELECT TOP(1)
										DESCR
									FROM
										HIEGRP
									WHERE
										HIEGRP.GROU = @CategoryNumber
								)
						END
				END
			ELSE
				BEGIN
					SET @Category = 
						(
							SELECT TOP(1)
								DESCR
							FROM
								HIESGP
							WHERE
								HIESGP.SGRP = @CategoryNumber
						)
				END
				
		END
	ELSE
		BEGIN
			SET @Category = 
				(
					SELECT TOP(1)
						DESCR
					FROM
						HIESTY
					WHERE
						HIESTY.STYL = @CategoryNumber
				)
		END
	RETURN @Category
END
GO

