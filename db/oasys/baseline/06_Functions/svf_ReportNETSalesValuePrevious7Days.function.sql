﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportNETSalesValuePrevious7Days]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportNETSalesValuePrevious7Days'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportNETSalesValuePrevious7Days]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportNETSalesValuePrevious7Days'
GO
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.0
-- Create date	: 8th July 2011
-- Description	: Returns the figure to use for Total Sales Value for last 7 days - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportNETSalesValuePrevious7Days]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@CoreWeekValue		numeric(9,2)
	
	Set				@StartDate		=	DATEADD(day, -7, @InputDate);			
	Set				@DateEnd		=	@InputDate;
						
	----------------------------------------------------------------------------------				
	-- Get CORE SALES + REFUNDS - Previous 7 days
	----------------------------------------------------------------------------------
	Select		@CoreWeekValue	=	isnull(sum(TOTL), 0)
	From		dltots dl
	Where		dl.TCOD IN ('SA', 'RF') 
				and (dl.DATE1 < @DateEnd and dl.date1 >= @StartDate)
				and	dl.CASH		<>		'000'
				and	dl.VOID = 0
				and	dl.PARK = 0
				and	dl.TMOD = 0	
					
	RETURN		@CoreWeekValue

END
GO

