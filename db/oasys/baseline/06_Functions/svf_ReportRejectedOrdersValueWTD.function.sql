﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportRejectedOrdersValueWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportRejectedOrdersValueWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportRejectedOrdersValueWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportRejectedOrdersValueWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 14th June 2011
-- Description	: Returns the figure to use for Rejected Orders for WTD - Scalar Valued Function
-- Notes		: This uses PURHDR to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportRejectedOrdersValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate				date,
					@DateEnd				date,
					@Rejected				numeric(9,2)
	
	Set				@StartDate			=		@InputDate;			
	Set				@DateEnd			=		@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End		

					
	----------------------------------------------------------------------------------
	-- Get Rejected Orders Value - WTD
	----------------------------------------------------------------------------------
	Select			@Rejected			=		sum(VALU)
	From			PURHDR
	Where			CONF				=		'R'
					and ODAT			>=		@StartDate
					and ODAT			<=		@DateEnd;

	Set				@Rejected			=		isnull(@Rejected, 0);
		
	RETURN			@Rejected

END
GO

