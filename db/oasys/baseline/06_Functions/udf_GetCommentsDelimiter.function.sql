﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_GetCommentsDelimiter]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_GetCommentsDelimiter'
	EXEC ('CREATE FUNCTION [dbo].[udf_GetCommentsDelimiter]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_GetCommentsDelimiter'
GO
-- =============================================
-- Author        : Alan Lewis
-- Create date   : 06/11/2012
-- User Story	 : 9019: Iteration 18 Rework for pilot
-- Project		 : P022-017: RF0620 - Ability to add comments
--				 : when rechecking floats, performing pickups,
--				 : performing cash drops, rechecking pickups.
-- Task Id		 : 9020
-- Description   : Create udf to read parameter setting for
--				 : comments delimiter.
-- =============================================
ALTER FUNCTION [dbo].[udf_GetCommentsDelimiter]
(
)
Returns VarChar(80)
Begin
	Declare @CommentsDelimiter As VarChar(80) = Char(13) + Char(10);
	
	If Exists(Select ParameterID From [Parameters] Where ParameterID = 2210)
		Begin
			Select 
				@CommentsDelimiter = StringValue
			From
				[Parameters]
			Where
				ParameterID = 2210;
		End

	Return @CommentsDelimiter
End
GO

