﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportColleagueDiscountQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportColleagueDiscountQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportColleagueDiscountQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportColleagueDiscountQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Colleague Discount Count for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportColleagueDiscountQtyDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@ColleagueQty		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Colleague Discount Qty - DAY
	----------------------------------------------------------------------------------
	Select			@ColleagueQty	=		count([TRAN])
	From			DLTOTS 
	Where			IEMP			=		1
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		=		@StartDate;

	Set				@ColleagueQty	=		isnull(@ColleagueQty, 0);
		
	RETURN			@ColleagueQty

END
GO

