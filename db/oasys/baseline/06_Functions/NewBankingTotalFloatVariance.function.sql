﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[NewBankingTotalFloatVariance]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function NewBankingTotalFloatVariance'
	EXEC ('CREATE FUNCTION [dbo].[NewBankingTotalFloatVariance]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function NewBankingTotalFloatVariance'
GO
-- =============================================
-- Author       : Partha Dutta
-- Create date  : 01/06/2011
-- Referral No  : 811
-- Description  : Baseline version
--                Calculate total variance for all floated cashiers
--
-- Author       : Partha Dutta
-- Create date  : 26/06/2011
-- Referral No  : 811
-- Description  : Post QA testing of Release 3.7.2 has indicated that cashier float variance for days in the future of the banking day
--                is the correct way to resolve float variances
-- =============================================

ALTER FUNCTION dbo.NewBankingTotalFloatVariance(@PeriodID int, @CurrencyID nvarchar(3))
   returns decimal(8, 2)
as
begin
   declare @Total decimal(8, 2)

   set @Total = (select sum(isnull(FloatVariance, 0))
                 from CashBalCashier 
               --where PeriodID   = @PeriodID
                 where PeriodID   > @PeriodID
                 and   CurrencyID = @CurrencyID)


   return @Total
end
GO

