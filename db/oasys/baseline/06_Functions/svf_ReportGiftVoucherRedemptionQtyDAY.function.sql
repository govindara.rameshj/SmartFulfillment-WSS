﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportGiftVoucherRedemptionQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportGiftVoucherRedemptionQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportGiftVoucherRedemptionQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportGiftVoucherRedemptionQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Gift Voucher Redemption Qty for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportGiftVoucherRedemptionQtyDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate			date,
					@GiftQty			int
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Gift Voucher Redemption Qty - DAY
	----------------------------------------------------------------------------------
	Select			@GiftQty				=		count(AMNT)
	From			DLGIFT
	Where			[TYPE]					in		('TS', 'RR', 'CS', 'VS')
					and Date1				=		@StartDate;

	Set				@GiftQty				=		isnull(@GiftQty, 0);
		
	RETURN			@GiftQty

END
GO

