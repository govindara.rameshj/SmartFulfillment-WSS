﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_EventWhenActive]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_EventWhenActive'
	EXEC ('CREATE FUNCTION [dbo].[udf_EventWhenActive]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_EventWhenActive'
GO
ALTER FUNCTION [dbo].[udf_EventWhenActive]
(
	@Start		DATETIME,
	@End		DATETIME,
	@StartTime	CHAR(4),
	@EndTime	CHAR(4),
	@ActiveDay1	BIT,
	@ActiveDay2	BIT,
	@ActiveDay3	BIT,
	@ActiveDay4	BIT,
	@ActiveDay5	BIT,
	@ActiveDay6	BIT,
	@ActiveDay7	BIT,
	@Deleted	BIT
)
RETURNS VARCHAR(90)
BEGIN
	-- Lots of variables for intermediate stage values, so not recalling functions and can see
	-- values in sql debug window (function call result not shown directly in debugger)
	DECLARE
		@StartIsNull	  INT,
		@STime			  CHAR(5),
		@StartTimeIsNull  INT,
		@EndIsNull		  INT,
		@ETime			  CHAR(5),
		@EndTimeIsNull	  INT,
		@WhatDays		  VARCHAR(37),
		-- store the result, again mostly for debugging, could select it directly
		@WhenActive		  VARCHAR(90);

	-- Set up function result values for use later on	
	SET @StartIsNull        = DATEDIFF(DD, ISNULL(@Start, GETDATE()),  ISNULL(@Start, DATEADD(DD, 1, GETDATE())))
	SET @StartTimeIsNull  = 
		CASE
			WHEN
				@StartTime = '0000'
			THEN
				1
			ELSE
				0
		END
	SET @STime			  = SUBSTRING(@StartTime, 1, 2) + ':' + SUBSTRING(@StartTime, 3, 2)
	SET @EndIsNull        = DATEDIFF(DD, ISNULL(@End, GETDATE()),  ISNULL(@End, DATEADD(DD, 1, GETDATE())))
	SET @EndTimeIsNull    = 
		CASE
			WHEN
				@EndTime = '0000'
			THEN
				1
			ELSE
				0
		END
	SET @ETime			  = SUBSTRING(@EndTime, 1, 2) + ':' + SUBSTRING(@EndTime, 3, 2)

	SET @WhatDays = 
		CASE
			WHEN
				@ActiveDay1 = 1
			THEN
				'On Mon'
			ELSE
				''
		END
	SET @WhatDays = @WhatDays +
		CASE
			WHEN
				LEN(@WhatDays) = 0 AND @ActiveDay2 = 1
			THEN
				'On Tue'
			WHEN
				LEN(@WhatDays) > 0 AND @ActiveDay2 = 1
			THEN
				', Tue'
			ELSE
				''
		END
	SET @WhatDays = @WhatDays +
		CASE
			WHEN
				LEN(@WhatDays) = 0 AND @ActiveDay3 = 1
			THEN
				'On Wed'
			WHEN
				LEN(@WhatDays) > 0 AND @ActiveDay3 = 1
			THEN
				', Wed'
			ELSE
				''
		END
	SET @WhatDays = @WhatDays +
		CASE
			WHEN
				LEN(@WhatDays) = 0 AND @ActiveDay4 = 1
			THEN
				'On Thu'
			WHEN
				LEN(@WhatDays) > 0 AND @ActiveDay4 = 1
			THEN
				', Thu'
			ELSE
				''
		END
	SET @WhatDays = @WhatDays +
		CASE
			WHEN
				LEN(@WhatDays) = 0 AND @ActiveDay5 = 1
			THEN
				'On Fri'
			WHEN
				LEN(@WhatDays) > 0 AND @ActiveDay5 = 1
			THEN
				', Fri'
			ELSE
				''
		END
	SET @WhatDays = @WhatDays +
		CASE
			WHEN
				LEN(@WhatDays) = 0 AND @ActiveDay6 = 1
			THEN
				'On Sat'
			WHEN
				LEN(@WhatDays) > 0 AND @ActiveDay6 = 1
			THEN
				', Sat'
			ELSE
				''
		END
	SET @WhatDays = @WhatDays +
		CASE
			WHEN
				LEN(@WhatDays) = 0 AND @ActiveDay7 = 1
			THEN
				'On Sun'
			WHEN
				LEN(@WhatDays) > 0 AND @ActiveDay7 = 1
			THEN
				', Sun'
			ELSE
				''
		END
	SET @WhatDays = 
		CASE
			WHEN
				SUBSTRING(@WhatDays, LEN(@WhatDays) - 4, 1) = ','
			THEN
				LEFT(@WhatDays, LEN(@WhatDays) - 5) + ' &' + RIGHT(@WhatDays, 4)
			ELSE
				@WhatDays
		END

	SET @WhenActive = 
		CASE
			WHEN
				@StartIsNull = 0
			THEN
				' From ' + CONVERT(NVARCHAR(10), @Start, 103) 
			ELSE
				''
		END 				
	SET @WhenActive = @WhenActive + 
		CASE
			WHEN
				LEN(@WhenActive) > 0 AND @EndIsNull = 0
			THEN
				' To '  + CONVERT(NVARCHAR(10), @End, 103)
			ELSE
				''
		END
	SET @WhenActive =  @WhenActive + 
		CASE
			WHEN
				LEN(@WhenActive) = 0
			THEN
				''
			WHEN
				@StartTime <> '0000' AND @EndTime = '0000'
			THEN
				' After ' + @STime
			WHEN
				@StartTime <> '0000' AND @EndTime <> '0000'
			THEN
				' Between ' + @STime + ' & ' +  @ETime
			ELSE
				''
		END 

	SET @WhenActive =
		CASE
			WHEN
				@Deleted <> 0
			THEN
				''
			ELSE
			  LTRIM(RTRIM(@WhenActive + ' ' + @WhatDays))
		END

	RETURN @WhenActive
END
GO

