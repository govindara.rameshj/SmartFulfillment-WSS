﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportPendingWriteOffQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportPendingWriteOffQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportPendingWriteOffQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportPendingWriteOffQtyDAY'
GO
ALTER FUNCTION [dbo].[svf_ReportPendingWriteOffQtyDAY]()
	Returns Int
As
	Begin
		Declare @WOQty Int
		
		Select
			@WOQty =Abs(Sum(SA.QUAN))
		From
			STKADJ As SA
				Inner Join
					STKMAS As SM
				On
					SA.SKUN = SM.SKUN
		Where
			SA.MOWT = 'W'
		And
			SA.WAUT = 0;
					
	Set @WOQty = IsNull(@WOQty, 0);
		
	Return @WOQty
End
GO

