﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportCardSummaryValueDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportCardSummaryValueDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportCardSummaryValueDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportCardSummaryValueDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Credit Card Summary Value for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION dbo.svf_ReportCardSummaryValueDAY( @InputDate date
                                                )
RETURNS numeric( 9 , 2
               )
AS
BEGIN

    DECLARE
       @StartDate date , 
       @CardValue numeric( 9 , 2
                         );

    SET @StartDate = @InputDate;           

    SELECT @CardValue = ISNULL(SUM( CreditCardSummary.Value
                           ),0)
      FROM(( 
             SELECT SUM( DP.AMNT * -1
                       )AS Value
               FROM
                    DLTOTS AS DT INNER JOIN DLPAID AS DP
                    ON DP.DATE1
                       = 
                       DT.DATE1
                   AND DP.TILL
                       = 
                       DT.TILL
                   AND DP.[TRAN]
                       = 
                       DT.[TRAN]
               WHERE DP.TYPE IN( 3 , 8 , 9
                               )
                 AND DP.CARD
                     <> 
                     '0000000000000000000'
                 AND DT.CASH <> '000'
                 AND DT.VOID = 0
                 AND DT.PARK = 0
                 AND DT.TMOD = 0
                 AND DT.Date1
                     = 
                     @StartDate
           )
           UNION ALL
           ( 
             SELECT SUM( VP.ValueTender * -1
                       )AS Value
               FROM VisionPayment AS VP
               WHERE VP.TranDate
                     = 
                     @StartDate
                 AND VP.TenderTypeId IN( '03' , '08' , '09'
                                       )
           )
          )AS CreditCardSummary;

    RETURN @CardValue;

END;
GO

