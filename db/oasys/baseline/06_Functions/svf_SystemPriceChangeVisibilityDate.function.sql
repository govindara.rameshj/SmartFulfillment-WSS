﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_SystemPriceChangeVisibilityDate]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_SystemPriceChangeVisibilityDate'
	EXEC ('CREATE FUNCTION [dbo].[svf_SystemPriceChangeVisibilityDate]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_SystemPriceChangeVisibilityDate'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 8th June 2011
-- Description	: Returns the Date to use for Price Changes Visibility (I.e. Today +7 +SYSDAT:DPCA) Scalar Valued Function
-- Notes		: This uses SYSDAT:DPCA value to calculate the date used for removing old records.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_SystemPriceChangeVisibilityDate]()
RETURNS date 
AS
BEGIN

	Declare		@Days	int,
				@PCDate	date
	
	Set			@Days		= 7 + (Select DPCA From SYSDAT Where FKEY = '01')
	Set			@PCDate		= (Convert (date, DATEADD(Day,@Days,GETDATE())))
	
	RETURN		@PCDate

END
GO

