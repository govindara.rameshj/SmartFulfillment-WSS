﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportMiscOutgoingValueDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportMiscOutgoingValueDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportMiscOutgoingValueDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportMiscOutgoingValueDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Misc Outgoing Value for DAY - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportMiscOutgoingValueDAY]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@MOValue			numeric(9,2)
	
	Set				@StartDate		=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Misc Outgoing Value - DAY
	----------------------------------------------------------------------------------
	Select			@MOValue		=		sum(TOTL)
	From			DLTOTS 
	Where			TCOD			in		('M-', 'C-')
					and CASH		<>		'000'
					and VOID		=		0
					and PARK		=		0
					and TMOD		=		0  
					and Date1		=		@StartDate;

	Set				@MOValue		=		isnull(@MOValue, 0);
		
	RETURN			@MOValue

END
GO

