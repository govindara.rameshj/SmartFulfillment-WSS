﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_SkuIncorrectDayPrice]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_SkuIncorrectDayPrice'
	EXEC ('CREATE FUNCTION [dbo].[udf_SkuIncorrectDayPrice]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_SkuIncorrectDayPrice'
GO
ALTER FUNCTION udf_SkuIncorrectDayPrice
(
    @SkuNumber char(6),
    @StartDate date,
    @EndDate   date
)
   returns decimal(9, 2)
as
begin
   return (
           select top 1 a.Price
           from EventHeaderOverride a
           inner join EventDetailOverride b
                 on b.EventHeaderOverrideID = a.ID
           where a.EventTypeID = 1
           and   a.StartDate  <= @StartDate
           and   a.EndDate    >= @EndDate
           and   b.SkuNumber   = @SkuNumber
           order by a.Revision desc
          )
end
GO

