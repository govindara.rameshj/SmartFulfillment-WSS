﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_EventGetDiscount]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_EventGetDiscount'
	EXEC ('CREATE FUNCTION [dbo].[udf_EventGetDiscount]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_EventGetDiscount'
GO
ALTER FUNCTION [dbo].[udf_EventGetDiscount]
(
	@ValueDiscount		DECIMAL(9,2),
	@PercentDiscount	DECIMAL(7,3)
)
RETURNS CHAR(9)
BEGIN
	DECLARE @Discount	CHAR(9)

	SET @Discount =
		(
			CASE
				WHEN
					(@ValueDiscount IS NULL AND @PercentDiscount IS NULL) OR (@ValueDiscount = 0 AND @PercentDiscount = 0 )
				THEN
					'None'
				WHEN
					@ValueDiscount IS NULL OR @ValueDiscount = 0
				THEN
					LTRIM(RTRIM(CONVERT(CHAR(9) , CAST(@PercentDiscount AS MONEY), 126))) + '%'
				ELSE
					LTRIM(RTRIM(CONVERT(CHAR(9) , CAST(@ValueDiscount AS MONEY), 0)))
			END
		)
	RETURN @Discount
END
GO

