﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_SkuNormalSellingPrice]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function udf_SkuNormalSellingPrice'
	EXEC ('CREATE FUNCTION [dbo].[udf_SkuNormalSellingPrice]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function udf_SkuNormalSellingPrice'
GO
ALTER FUNCTION udf_SkuNormalSellingPrice
(
    @SkuNumber char(6)
)
   returns decimal(9, 2)
as
begin

   return (select PRIC from STKMAS where SKUN = @SkuNumber)

end
GO

