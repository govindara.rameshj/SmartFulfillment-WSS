﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_EffectiveEVTCHGRecordsForDate]', N'TF') IS NULL
BEGIN
	PRINT 'Creating function udf_EffectiveEVTCHGRecordsForDate'
	EXEC ('CREATE FUNCTION [dbo].[udf_EffectiveEVTCHGRecordsForDate]() RETURNS @t TABLE ([Col] int) AS BEGIN RETURN END')
END
GO

PRINT 'Altering function udf_EffectiveEVTCHGRecordsForDate'
GO
ALTER FUNCTION [dbo].[udf_EffectiveEVTCHGRecordsForDate] 
	(
	@date date
	)
RETURNS @Results TABLE
(
	[SKUN] [char](6) NOT NULL,
	[SDAT] [date] NOT NULL,
	[PRIO] [char](2) NOT NULL,
	[NUMB] [char](6) NOT NULL,
	[EDAT] [date] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[IDEL] [bit] NOT NULL,
	[SPARE] [char](40) NULL
)
AS

BEGIN
	insert into @Results 
	select EVTCHGFullRow.* from Oasys.dbo.EVTCHG EVTCHGFullRow
		inner join (
			(select evt.skun, max(evt.numb) 'numb' 					
			from Oasys.dbo.evtchg evt					
			inner join (					
						select evt.skun, max(evt.prio) 'prio' 		
						from Oasys.dbo.evtchg evt		
							inner join Oasys.dbo.stkmas stk	
								on stk.SKUN = evt.SKUN 
						where sdat <= @date		
								and isnull(edat, @date) >= @date
								and evt.idel = 0
								and stk.IDEL = 0
						group by evt.skun) MaximumPriorityEVTCHGForSkuOnDate		
				on evt.skun = MaximumPriorityEVTCHGForSkuOnDate.skun				
					and evt.prio = MaximumPriorityEVTCHGForSkuOnDate.prio			
			inner join Oasys.dbo.stkmas stk					
				on stk.SKUN = evt.SKUN 				
			where sdat <= @date					
					and isnull(edat, @date) >= @date
					and evt.idel = 0			
					and stk.IDEL = 0
			group by evt.skun)
			) EffectiveEVTCHGRecordKeyValues
			on EffectiveEVTCHGRecordKeyValues.Skun = EVTCHGFullRow.SKUN
				and EffectiveEVTCHGRecordKeyValues.Numb = EVTCHGFullRow.NUMB 
				
	return
END
GO

