﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportRejectedOrdersQtyDAY]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportRejectedOrdersQtyDAY'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportRejectedOrdersQtyDAY]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportRejectedOrdersQtyDAY'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Rejected Orders for DAY - Scalar Valued Function
-- Notes		: This uses PURHDR to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportRejectedOrdersQtyDAY]
(
@InputDate date
)
RETURNS int
AS
BEGIN

	Declare			@StartDate				date,
					@Rejected				int
	
	Set				@StartDate			=	@InputDate;	

					
	----------------------------------------------------------------------------------
	-- Get Rejected Orders Qty - DAY
	----------------------------------------------------------------------------------
	Select			@Rejected			=		count(VALU)
	From			PURHDR
	Where			CONF				=		'R'
					and ODAT			=		@StartDate;

	Set				@Rejected			=		isnull(@Rejected, 0);
		
	RETURN			@Rejected

END
GO

