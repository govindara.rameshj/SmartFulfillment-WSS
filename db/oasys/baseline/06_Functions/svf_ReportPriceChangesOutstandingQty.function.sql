﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportPriceChangesOutstandingQty]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportPriceChangesOutstandingQty'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportPriceChangesOutstandingQty]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportPriceChangesOutstandingQty'
GO
ALTER FUNCTION [dbo].[svf_ReportPriceChangesOutstandingQty]
(
)
RETURNS int
AS
BEGIN

	DECLARE @OutstandingQty	INT
	
	SELECT @OutstandingQty = count(sm.SKUN)
	FROM PRCCHG AS Pc
	INNER JOIN STKMAS AS sm ON sm.SKUN = pc.SKUN
	WHERE PSTA = 'U'
	
	SET @OutstandingQty = ISNULL(@OutstandingQty, 0);
		
	RETURN			@OutstandingQty

END
GO

