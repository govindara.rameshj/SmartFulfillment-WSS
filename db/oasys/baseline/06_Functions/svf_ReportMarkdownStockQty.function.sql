﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportMarkdownStockQty]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportMarkdownStockQty'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportMarkdownStockQty]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportMarkdownStockQty'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Markdown Stock Qty - Scalar Valued Function
-- Notes		: This uses STKMAS:MDNQ value to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportMarkdownStockQty]()
RETURNS int 
AS
BEGIN

	Declare		@MarkdownQty		int
		
	Set			@MarkdownQty	=	
									(
									Select	SUM(MDNQ)
									From	STKMAS
									Where	MDNQ	<>	0
									)
	
	RETURN		@MarkdownQty

END
GO

