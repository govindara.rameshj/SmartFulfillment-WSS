﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[udf_PickupBagsInSafeAtDateAndTime]', N'IF') IS NULL
BEGIN
	PRINT 'Creating function udf_PickupBagsInSafeAtDateAndTime'
	EXEC ('CREATE FUNCTION [dbo].[udf_PickupBagsInSafeAtDateAndTime]() RETURNS TABLE AS RETURN ( SELECT 1 AS [Column] )')
END
GO

PRINT 'Altering function udf_PickupBagsInSafeAtDateAndTime'
GO
ALTER FUNCTION udf_PickupBagsInSafeAtDateAndTime(@Now datetime)
   returns table
as
   return
   (
       select * from dbo.udf_BagsInSafeAtDateAndTime(@Now, 'P')
   )
GO

