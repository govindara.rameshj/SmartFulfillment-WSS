﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportBelowImpacts]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportBelowImpacts'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportBelowImpacts]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportBelowImpacts'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 16th May 2011
-- Description	: Returns the figure to use for Below Impact Level count - Scalar Valued Function
-- Notes		: This uses STKMAS:MINI value to calculate the items below impact.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportBelowImpacts]()
RETURNS int 
AS
BEGIN

	Declare		@BelowImpacts		int,
				@TODT				date
	
	Set			@TODT			=	(Select TMDT From SYSDAT Where FKEY = '01')	
	Set			@BelowImpacts	=	
							(
							Select	CAST(COUNT(SKUN) as int)
							From	STKMAS
							Where	IDEL		=	0 
									and IOBS	=	0
									and INON	=	0
									and IRIS	=	0
									and ICAT	=	0
									and NOOR	=	0 
									and IODT	<	@TODT
									and ONHA	<	MINI
							)
	
	RETURN		@BelowImpacts

END
GO

