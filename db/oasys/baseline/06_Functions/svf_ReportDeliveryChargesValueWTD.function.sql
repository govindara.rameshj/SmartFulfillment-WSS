﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportDeliveryChargesValueWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportDeliveryChargesValueWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportDeliveryChargesValueWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportDeliveryChargesValueWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 7th June 2011
-- Description	: Returns the figure to use for Delivery Charges Value for WTD - Scalar Valued Function
-- Notes		: This uses CORHDR to draw data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportDeliveryChargesValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate				date,
					@DateEnd				date,
					@DeliveryChargeV_WD		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Retrieve Delivery Charges - Value(WTD)
	----------------------------------------------------------------------------------
	Select			@DeliveryChargeV_WD		=		SUM(ch.DCST)
	
	From			CORHDR					as		ch
	Inner Join		CORHDR4					as		c4		
											on		(c4.NUMB = ch.NUMB) and (c4.DATE1 = ch.DATE1)
	
	Where			ch.DATE1				<=		(@DateEnd)
					and ch.DATE1			>=		(@StartDate)
					and ch.CANC				<		'1' 
					and	ch.DELI				=		'1'
					and ch.DCST				<>		'0.00'
					and ch.STIL				<		'13'
					and c4.SellingStoreId	=		(Select Oasys.dbo.svf_SystemStoreGroupID())

	Set				@DeliveryChargeV_WD		=		isnull(@DeliveryChargeV_WD, 0);
		
	RETURN			@DeliveryChargeV_WD

END
GO

