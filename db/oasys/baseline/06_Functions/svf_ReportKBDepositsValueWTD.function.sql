﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportKBDepositsValueWTD]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportKBDepositsValueWTD'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportKBDepositsValueWTD]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportKBDepositsValueWTD'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 6th June 2011
-- Description	: Returns the figure to use for K&B Deposits Value for WTD - Scalar Valued Function
-- Notes		: This uses DLTOTS to draw sales data to calculate the items.
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportKBDepositsValueWTD]
(
@InputDate date
)
RETURNS numeric(9,2)
AS
BEGIN

	Declare			@StartDate			date,
					@DateEnd			date,
					@KBWeekValue		numeric(9,2)
	
	Set				@StartDate		=	@InputDate;			
	Set				@DateEnd		=	@InputDate;
	
	While			Datepart(Weekday, @StartDate) <> 1
					
					Begin
						Set	@StartDate = DateAdd(Day, -1, @StartDate)
					End	

					
	----------------------------------------------------------------------------------
	-- Get K&B DEPOSISTS - WTD
	----------------------------------------------------------------------------------
	Select			@KBWeekValue		=	sum(TOTL)
	From			DLTOTS 
	Where			(TCOD		=		'M+'	or TCOD		=		'M-')
					and MISC	=		'20'
					and VOID	=		0
					and PARK	=		0
					and TMOD	=		0  
					and Date1	>=		@StartDate
					and Date1	<=		@DateEnd;

	Set			@KBWeekValue	=		isnull(@KBWeekValue, 0);
		
	RETURN		@KBWeekValue

END
GO

