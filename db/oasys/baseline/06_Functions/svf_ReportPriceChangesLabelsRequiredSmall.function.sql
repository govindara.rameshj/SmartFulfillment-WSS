﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF OBJECT_ID (N'[dbo].[svf_ReportPriceChangesLabelsRequiredSmall]', N'FN') IS NULL
BEGIN
	PRINT 'Creating function svf_ReportPriceChangesLabelsRequiredSmall'
	EXEC ('CREATE FUNCTION [dbo].[svf_ReportPriceChangesLabelsRequiredSmall]() RETURNS INT AS BEGIN RETURN NULL END')
END
GO

PRINT 'Altering function svf_ReportPriceChangesLabelsRequiredSmall'
GO
-- =================================================================================================
-- Author		: Kevan Madelin
-- Version		: 1.0
-- Create date	: 8th June 2011
-- Description	: Returns the figure to use for Price Changes - Labels Required SMALL - Scalar Valued Function
-- Notes		: This uses PRCCHG to draw data to calculate the items.
-- =================================================================================================
-- Author		: Sean Moir
-- Version		: 1.1
-- Create date	: 7th July 2011
-- Description	: Changed to return sum of STKMAS.LABM
-- =================================================================================================
ALTER FUNCTION [dbo].[svf_ReportPriceChangesLabelsRequiredSmall]
(
)
RETURNS int
AS
BEGIN

	Declare			@Small			int,
					@PCDate			date
					
	Set				@PCDate			=		Oasys.dbo.svf_SystemPriceChangeVisibilityDate()
					
	----------------------------------------------------------------------------------
	-- Retrieve Count for Price Change Labels Required - SMALL
	----------------------------------------------------------------------------------
	Select			@Small			=		sum(stk.LABM)
	From			PRCCHG prc
		inner join STKMAS stk
			on prc.SKUN = stk.SKUN
	Where			PSTA			=		'U'
					and prc.LABM	=		1
					and PDAT		<=		@PCDate

	Set				@Small			=		isnull(@Small, 0);
		
	RETURN			@Small

END
GO

