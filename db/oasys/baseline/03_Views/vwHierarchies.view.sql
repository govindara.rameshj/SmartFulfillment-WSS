﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vwHierarchies]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	PRINT 'Creating view vwHierarchies'
	EXEC ('CREATE VIEW [dbo].[vwHierarchies] AS SELECT 1 [Column]')
END
GO

PRINT 'Altering view vwHierarchies'
GO
ALTER VIEW dbo.vwHierarchies
AS
SELECT     TOP (100) PERCENT dbo.HIECAT.NUMB AS CategoryNumber, RTRIM(dbo.HIECAT.DESCR) AS CategoryName, dbo.HIECAT.ALPH AS CategoryAlpha, 
                      dbo.HIEGRP.GROU AS GroupNumber, RTRIM(dbo.HIEGRP.DESCR) AS GroupName, dbo.HIEGRP.ALPH AS GroupAlpha, dbo.HIESGP.SGRP AS SubgroupNumber, 
                      RTRIM(dbo.HIESGP.DESCR) AS SubgroupName, dbo.HIESGP.ALPH AS SubgroupAlpha, dbo.HIESTY.STYL AS StyleNumber, RTRIM(dbo.HIESTY.DESCR) AS StyleName, 
                      dbo.HIESTY.ALPH AS StyleAlpha
FROM         dbo.HIECAT INNER JOIN
                      dbo.HIEGRP ON dbo.HIECAT.NUMB = dbo.HIEGRP.NUMB INNER JOIN
                      dbo.HIESGP ON dbo.HIEGRP.NUMB = dbo.HIESGP.NUMB AND dbo.HIEGRP.GROU = dbo.HIESGP.GROU INNER JOIN
                      dbo.HIESTY ON dbo.HIESGP.NUMB = dbo.HIESTY.NUMB AND dbo.HIESGP.GROU = dbo.HIESTY.GROU AND dbo.HIESGP.SGRP = dbo.HIESTY.SGRP
GO
                      
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[36] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "HIECAT"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 110
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "HIEGRP"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "HIESGP"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 252
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "HIESTY"
            Begin Extent = 
               Top = 126
               Left = 236
               Bottom = 276
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2925
         Alias = 2715
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwHierarchies'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwHierarchies'
GO

