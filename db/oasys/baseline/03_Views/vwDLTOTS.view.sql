﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vwDLTOTS]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	PRINT 'Creating view vwDLTOTS'
	EXEC ('CREATE VIEW [dbo].[vwDLTOTS] AS SELECT 1 [Column]')
END
GO

PRINT 'Altering view vwDLTOTS'
GO
ALTER VIEW dbo.[vwDLTOTS] AS
SELECT
	   [DATE1]
      ,[TILL]
      ,[TRAN]
      ,[CASH]
      ,[TIME]
      ,[SUPV]
      ,[TCOD]
      ,[OPEN]
      ,[MISC]
      ,[DESCR]
      ,[ORDN]
      ,[ACCT]
      ,[VOID]
      ,[VSUP]
      ,[TMOD]
      ,[PROC]
      ,[DOCN]
      ,[SUSE]
      ,[STOR]
      ,[MERC]
      ,[NMER]
      ,[TAXA]
      ,[DISC]
      ,[DSUP]
      ,[TOTL]
      ,[ACCN]
      ,[CARD]
      ,[AUPD]
      ,[BACK]
      ,[ICOM]
      ,[IEMP]
      ,[RCAS]
      ,[RSUP]
      ,[VATR1]
      ,[VATR2]
      ,[VATR3]
      ,[VATR4]
      ,[VATR5]
      ,[VATR6]
      ,[VATR7]
      ,[VATR8]
      ,[VATR9]
      ,[VSYM1]
      ,[VSYM2]
      ,[VSYM3]
      ,[VSYM4]
      ,[VSYM5]
      ,[VSYM6]
      ,[VSYM7]
      ,[VSYM8]
      ,[VSYM9]
      ,[XVAT1]
      ,[XVAT2]
      ,[XVAT3]
      ,[XVAT4]
      ,[XVAT5]
      ,[XVAT6]
      ,[XVAT7]
      ,[XVAT8]
      ,[XVAT9]
      ,[VATV1]
      ,[VATV2]
      ,[VATV3]
      ,[VATV4]
      ,[VATV5]
      ,[VATV6]
      ,[VATV7]
      ,[VATV8]
      ,[VATV9]
      ,[PARK]
      ,[RMAN]
      ,[TOCD]
      ,[PKRC]
      ,[REMO]
      ,[GTPN]
      ,[CCRD]
      ,[SSTA]
      ,[SSEQ]
      ,[CBBU]
      ,[CARD_NO]
      ,[RTI]
      ,[ReceivedDate]
      ,case when [Source]='OVC' then [ReceiptBarcode] end As 'OVCTranNumber'
FROM dbo.DLTOTS
GO

