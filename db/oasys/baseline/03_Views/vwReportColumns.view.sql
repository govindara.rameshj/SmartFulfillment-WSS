﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vwReportColumns]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	PRINT 'Creating view vwReportColumns'
	EXEC ('CREATE VIEW [dbo].[vwReportColumns] AS SELECT 1 [Column]')
END
GO

PRINT 'Altering view vwReportColumns'
GO
ALTER VIEW dbo.vwReportColumns
AS
SELECT     TOP (100) PERCENT dbo.Report.Id AS ReportId, dbo.Report.ProcedureName, dbo.ReportTable.Id AS TableId, dbo.ReportColumns.Sequence, 
                      dbo.ReportColumns.ColumnId, dbo.ReportColumn.Name, dbo.ReportColumn.Caption, dbo.ReportColumn.FormatType, dbo.ReportColumn.Format, 
                      dbo.ReportColumn.IsImagePath, dbo.ReportColumn.MinWidth, dbo.ReportColumn.MaxWidth, dbo.ReportColumn.Alignment, dbo.ReportColumns.IsVisible, 
                      dbo.ReportColumns.IsBold
FROM         dbo.ReportTable INNER JOIN
                      dbo.ReportColumn INNER JOIN
                      dbo.ReportColumns ON dbo.ReportColumn.Id = dbo.ReportColumns.ColumnId ON dbo.ReportTable.ReportId = dbo.ReportColumns.ReportId AND 
                      dbo.ReportTable.Id = dbo.ReportColumns.TableId INNER JOIN
                      dbo.Report ON dbo.ReportTable.ReportId = dbo.Report.Id
ORDER BY ReportId, TableId, dbo.ReportColumns.Sequence
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[30] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ReportTable"
            Begin Extent = 
               Top = 10
               Left = 228
               Bottom = 237
               Right = 392
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ReportColumn"
            Begin Extent = 
               Top = 40
               Left = 643
               Bottom = 240
               Right = 803
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ReportColumns"
            Begin Extent = 
               Top = 10
               Left = 439
               Bottom = 239
               Right = 599
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Report"
            Begin Extent = 
               Top = 9
               Left = 15
               Bottom = 235
               Right = 184
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1065
         Width = 1170
         Width = 1020
         Width = 1275
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1110
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1305
         Table = 1815
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwReportColumns'
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'= 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwReportColumns'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwReportColumns'
GO

