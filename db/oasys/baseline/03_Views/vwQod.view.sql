﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vwQod]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	PRINT 'Creating view vwQod'
	EXEC ('CREATE VIEW [dbo].[vwQod] AS SELECT 1 [Column]')
END
GO

PRINT 'Altering view vwQod'
GO
ALTER VIEW [dbo].[vwQod]
AS
SELECT     
	vc.NUMB AS Number, 
	vc.DeliveryStatus, 
	vc.OMOrderNumber, 
	vc.IsSuspended, 
    SUM(c.QTYO * c.Price) AS Value, 
    vc.DELD AS DateDelivery
--Tables CORHDR and CORHDR4 have been replaced with the view vwCORHDRFull
FROM vwCORHDRFull as vc 
INNER JOIN dbo.CORLIN as c ON vc.NUMB = c.NUMB
WHERE vc.SHOW_IN_UI = 1
GROUP BY 
	vc.NUMB, 
	vc.DeliveryStatus, 
	vc.OMOrderNumber, 
	vc.IsSuspended, 
	vc.CANC, 
    vc.DELD
HAVING (vc.CANC <> '1')
GO

