﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vwSuppliersWithStockItems]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	PRINT 'Creating view vwSuppliersWithStockItems'
	EXEC ('CREATE VIEW [dbo].[vwSuppliersWithStockItems] AS SELECT 1 [Column]')
END
GO

PRINT 'Altering view vwSuppliersWithStockItems'
GO
ALTER VIEW [dbo].[vwSuppliersWithStockItems]
AS
SELECT DISTINCT TOP (100) PERCENT 	SUPMAS.SUPN AS Number, 
					RTRIM(SUPMAS.NAME) AS Name, 
					SUPMAS.SUPN + ' ' + RTRIM(SUPMAS.NAME) AS Display

FROM         	STKMAS LEFT OUTER JOIN 
		SUPMAS ON STKMAS.SUPP = SUPMAS.SUPN;
GO

