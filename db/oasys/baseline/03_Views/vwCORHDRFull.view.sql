﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vwCORHDRFull]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	PRINT 'Creating view vwCORHDRFull'
	EXEC ('CREATE VIEW [dbo].[vwCORHDRFull] AS SELECT 1 [Column]')
END
GO

PRINT 'Altering view vwCORHDRFull'
GO
ALTER VIEW dbo.[vwCORHDRFull] AS
SELECT     
CH.NUMB, 
CH.CUST, 
CH.DATE1, 
CH.DELD, 
CH.CANC, 
CH.DELI, 
CH.DELC, 
CH.AMDT, 
CH.ADDR1, 
CH.ADDR2, 
CH.ADDR3, 
CH.ADDR4, 
CH.PHON, 
CH.PRNT, 
CH.RPRN, 
CH.REVI, 
CH.MVST, 
CH.DCST, 
CH.QTYO, 
CH.QTYT, 
CH.QTYR, 
CH.WGHT, 
CH.VOLU, 
CH.SDAT, 
CH.STIL, 
CH.STRN, 
CH.RDAT, 
CH.RTIL, 
CH.RTRN, 
CH.MOBP, 
CH.POST, 
CH.NAME,
CH.HOMP, 
CH4.DDAT, 
CH4.OEID, 
CH4.FDID, 
CH4.DeliveryStatus, 
CH4.RefundStatus, 
CH4.SellingStoreId, 
CH4.SellingStoreOrderId, 
CH4.OMOrderNumber, 
CH4.CustomerAddress1, 
CH4.CustomerAddress2, 
CH4.CustomerAddress3, 
CH4.CustomerAddress4, 
CH4.CustomerPostcode, 
CH4.CustomerEmail, 
CH4.PhoneNumberWork, 
CH4.IsSuspended,
CH5.Source, 
CH5.SourceOrderNumber, 
CH5.ExtendedLeadTime, 
CH5.DeliveryContactName, 
CH5.DeliveryContactPhone, 
CH5.CustomerAccountNo, 
ISNULL(CH5.Source, 'NU') AS SOURCE_CODE, 
CAST(1 - ISNULL(CH5.IsClickAndCollect, 0) AS BIT) AS SHOW_IN_UI, 
CAST(1 - ISNULL(CH5.IsClickAndCollect, 0) AS BIT) AS SEND_UPDATES_TO_OM, 
ISNULL(CH5.IsClickAndCollect, 0) AS ALLOW_REMOTE_CREATE,
ISNULL(CH5.IsClickAndCollect, 0) AS IS_CLICK_AND_COLLECT,
CH.RequiredDeliverySlotID,
CH.RequiredDeliverySlotDescription,
CAST(CH.RequiredDeliverySlotStartTime AS VARCHAR) AS RequiredDeliverySlotStartTime,
CAST(CH.RequiredDeliverySlotEndTime AS VARCHAR) AS RequiredDeliverySlotEndTime
FROM dbo.CORHDR AS CH 
    INNER JOIN dbo.CORHDR4 AS CH4 ON CH.NUMB = CH4.NUMB 
    LEFT OUTER JOIN dbo.CORHDR5 AS CH5 ON CH5.NUMB = CH.NUMB
GO

