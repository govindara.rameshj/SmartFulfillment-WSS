﻿--[Section to remove in merged SQL]
USE [Oasys]
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--~[Section to remove in merged SQL]

IF NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vwQodNonZeroStockOrOutstandingIbt]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	PRINT 'Creating view vwQodNonZeroStockOrOutstandingIbt'
	EXEC ('CREATE VIEW [dbo].[vwQodNonZeroStockOrOutstandingIbt] AS SELECT 1 [Column]')
END
GO

PRINT 'Altering view vwQodNonZeroStockOrOutstandingIbt'
GO
ALTER VIEW [dbo].[vwQodNonZeroStockOrOutstandingIbt]
AS
SELECT     sm.SKUN AS SkuNumber, sm.DESCR AS Description, sm.ONHA AS QtyOnHand, COALESCE (SUM(cl.QTYO - cl.QTYR - cl.QTYT), 0) AS QtyOutstanding, 
                      sm.ONHA - COALESCE (SUM(cl.QTYO - cl.QTYR - cl.QTYT), 0) AS QtyVariance
FROM         dbo.STKMAS AS sm LEFT OUTER JOIN
                      dbo.CORLIN AS cl ON cl.SKUN = sm.SKUN AND cl.DeliveryStatus <= 399
GROUP BY sm.SKUN, sm.DESCR, sm.ONHA
GO

