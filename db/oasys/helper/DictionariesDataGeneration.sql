﻿------------------------------------------------------
set nocount on

create table #config
(
    [ID] int identity(1, 1) primary key,
    [path] nvarchar(255) collate database_default not null,
    [table] nvarchar(100) collate database_default not null,
    [update] bit not null
)
/*
- table should be filled in [schemaName].[tableName] format
- path should exist
*/
insert into #config ([path], [table], [update])
select
    'D:\trav-perk\ScriptRunner\scripts\scripts\08_DefaultData',
    [table],
    [update]
from (
    select '[dbo].[BusinessProcess]' [table], CAST(0 as bit) [update]
--  union select '[dbo].[EventType]', 0
) t
------------------------------------------------------
-- Enable required options
exec sp_configure 'show advanced options' , 1
go
reconfigure;
go
exec sp_configure 'Ole Automation Procedures', 1
go
reconfigure;
go
------------------------------------------------------
set quoted_identifier on
go
------------------------------------------------------
create procedure #spWriteVarToFile
    @string nvarchar(max),
    @path nvarchar(255),
    @filename nvarchar(100)
as
begin

set nocount on

declare
    @objFileSystem int,
    @objTextStream int,
    @objErrorObject int,
    @strErrorMessage varchar(1000),
    @command varchar(1000),
    @hr int,
    @fileAndPath varchar(80)

select @strErrorMessage = 'opening the File System Object'
execute @hr = sp_OACreate 'Scripting.FileSystemObject', @objFileSystem out

select @fileAndPath = @path + '\' + @filename
if @hr = 0
    select @objErrorObject = @objFileSystem, @strErrorMessage = 'Creating file " ' + @fileAndPath collate database_default + '"'
if @hr = 0
    execute @hr = sp_OAMethod @objFileSystem, 'CreateTextFile', @objTextStream out, @fileAndPath, 2, True

if @hr = 0
    select @objErrorObject = @objTextStream, @strErrorMessage = 'writing to the file "' + @fileAndPath collate database_default + '"'
if @hr = 0
    execute @hr = sp_OAMethod @objTextStream, 'Write', null, @string

if @hr = 0
    select @objErrorObject = @objTextStream, @strErrorMessage = 'closing the file "' + @fileAndPath collate database_default + '"'
if @hr = 0
    execute @hr = sp_OAMethod @objTextStream, 'Close'

if @hr <> 0
begin
    declare 
        @Source varchar(255),
        @Description Varchar(255),
        @Helpfile Varchar(255),
        @HelpID int
    
    execute sp_OAGetErrorInfo @objErrorObject, @source out, @Description out, @Helpfile out, @HelpID out
    select @strErrorMessage = 'Error whilst ' + coalesce(@strErrorMessage collate database_default, 'doing something') + ', ' + coalesce(@Description collate database_default,'')
    raiserror (@strErrorMessage, 16, 1)
end

execute sp_OADestroy @objTextStream
execute sp_OADestroy @objTextStream

set nocount off
end
go
------------------------------------------------------
create procedure #unloadDataScript
    @path nvarchar(255),
    @table nvarchar(100),
    @update bit
as
begin

set nocount on

declare @sql_main nvarchar(max)
declare @sql_temp nvarchar(max)
declare @identity_insert bit
declare @x nvarchar(max)

-- Define if table contains identity column
select top(1) @identity_insert = col.[is_identity]
from sys.columns col
where col.[object_id] = object_id(@table)
order by col.[is_identity] desc

-- Create column list, replace spaces if any
set @sql_temp = N''
select @sql_temp = @sql_temp + N'
, ' + case when typ.[name] in (N'char', N'nchar') then N'rtrim(replace([' + col.[name] + N'], '''''''', ''''''''''''))'
when typ.[name] in (N'nvarchar', N'varchar') then N'replace([' + col.[name] + N'], '''''''', '''''''''''')'
else N'[' + col.[name] + N']' end + N' [' + replace(col.[name], N' ', N'_') + N']'
from sys.columns col
inner join sys.types typ on typ.[system_type_id] = col.[system_type_id] and typ.[user_type_id] = col.[user_type_id]
where col.[object_id] = object_id(@table) and typ.[is_user_defined] = 0
order by col.[column_id]

-- Prepare select statement
set @sql_temp = N'set @x = (select ' + substring(@sql_temp, 4, len(@sql_temp) - 3) + N'
from ' + @table + N'[row]
for xml auto, binary base64, root(''root''))'

-- Save xml with data to variable
exec sp_executesql @sql_temp, N'@x nvarchar(max) output', @x = @x output

-- Prepare xml columns definition
set @sql_temp = N''
select @sql_temp = @sql_temp + N'
, r.value(''./@' + replace(col.[name], N' ', N'_') + N''', ''' + typ.[name] +
case when typ.[name] in (N'binary', N'char', N'nchar', N'nvarchar', N'varbinary', N'varchar') then N'(' + isnull(nullif(cast(col.[max_length] as nvarchar), -1), 'max') + N')'
when typ.[name] in (N'decimal', N'numeric') then N'(' + cast(col.[precision] as nvarchar) + N', ' + cast(col.[scale] as nvarchar) + N')' else N'' end +
N''') as [' + col.[name] + N']'
from sys.columns col
inner join sys.types typ on typ.[system_type_id] = col.[system_type_id] and typ.[user_type_id] = col.[user_type_id]
where col.[object_id] = object_id(@table) and typ.[is_user_defined] = 0
order by col.[column_id]

-- Complete select statement
set @sql_temp = N'select ' + substring(@sql_temp, 4, len(@sql_temp) - 3) + N'
from @xml.nodes(''/root/row'') as t(r)'

-- Start main batch
set @sql_main = 'declare @xml xml
set @xml = N''' + @x + N''''

-- Enable identity insert if needed
if @identity_insert = 1
set @sql_main = @sql_main + N'

set identity_insert ' + @table + N' on'

-- Start merge statement
set @sql_main = @sql_main + N'

merge ' + @table + N' as [target]
using (' + @sql_temp + N') as [source]'

-- Prepare join condition
set @sql_temp = N''
select @sql_temp = @sql_temp + N'and [target].[' + kc.[column_name] + N'] = [source].[' + kc.[column_name] + N']'
from [information_schema].[key_column_usage] kc
inner join sys.objects obj on obj.[type] = N'PK' and obj.[name] = kc.[constraint_name]
    and object_id([table_name]) = object_id(@table)

-- Check PK existence
if len(isnull(@sql_temp, '')) = 0
begin
declare @msg varchar(100)
set @msg = 'Table ' + @table + ' has no PK'
raiserror (@msg, 16, 1)
return
end

-- Add join condition
set @sql_main = @sql_main + N'
on ' + substring(@sql_temp, 5, len(@sql_temp) - 4)

-- Create column list for insert statement
set @sql_temp = N''
select @sql_temp = @sql_temp + N'
,[' + col.[name] + N']'
from sys.columns col
where col.[object_id] = object_id(@table)
order by col.[column_id]

-- Add insert statement
set @sql_main = @sql_main + N'
when not matched then
insert (
 ' + substring(@sql_temp, 4, len(@sql_temp) - 3) + N'
)
values ('

-- Create values column list
set @sql_temp = N''
select @sql_temp = @sql_temp + N'
,[source].[' + col.[name] + N']'
from sys.columns col
where col.[object_id] = object_id(@table)
order by col.[column_id]

-- Add values
set @sql_main = @sql_main + N'
 ' + substring(@sql_temp, 4, len(@sql_temp) - 3) + N'
)'

-- Select column list for update
set @sql_temp = N''
select @sql_temp = @sql_temp + N'
,[target].[' + col.[name] + N'] = [source].[' + col.[name] + N']'
from sys.columns col
where col.[object_id] = object_id(@table)
    and not exists (
        select 1 from information_schema.key_column_usage kc
        inner join sys.objects obj on obj.[type] = N'PK' and obj.[name] = kc.[constraint_name]
            and object_id([table_name]) = object_id(@table) and kc.[ordinal_position] = col.[column_id]
)
order by col.[column_id]

-- Check if there are non PK columns
if @update = 1 and len(isnull(@sql_temp, '')) > 0
begin
-- Add update statement
set @sql_main = @sql_main + N'
when matched then
update set
 ' + substring(@sql_temp, 4, len(@sql_temp) - 3)
end

-- Add semicolon at the end of merge statement
set @sql_main = @sql_main + N';'

-- Disable identity insert if needed
if @identity_insert = 1

set @sql_main = @sql_main + N'

set identity_insert ' + @table + N' off'

set @sql_main = @sql_main + N'
GO'

set @table = substring(@table, charindex(N'.', @table collate database_default) + 1, len(@table))
set @table = replace(replace(@table collate database_default, N'[', N''), N']', N'') + N'.sql'

exec #spWriteVarToFile @sql_main, @path, @table
set nocount off
end
go
------------------------------------------------------
declare @id int
declare @path nvarchar(255)
declare @table nvarchar(100)
declare @update bit

set @id = 0
while 1 = 1
begin

    select top 1 @id = [ID], @path = [path], @table = [table], @update = [update]
    from #config
    where [ID] > @id
    order by [ID]

    if @@rowcount = 0
        break
        
    if object_id(@table) is null
    begin
        declare @msg varchar(100)
        set @msg = 'Table ' + @table + ' doesn''t exist'
        raiserror (@msg, 16, 1)
        continue
    end
    
    exec #unloadDataScript @path, @table, @update
end
print 'Completed'
------------------------------------------------------
drop table #config
drop procedure #unloadDataScript
drop procedure #spWriteVarToFile
go
------------------------------------------------------