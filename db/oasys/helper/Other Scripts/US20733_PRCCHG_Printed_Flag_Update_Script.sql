DECLARE @dateDifference INT = 30

UPDATE dbo.PRCCHG
SET 
    SHEL = 1,
    LABS = 0,
    LABM = 0,
    LABL = 0
WHERE PDAT < DATEADD(day, -@dateDifference, GETDATE())
GO
    
If @@Error = 0
   Print 'Success: The Update of table dbo.PRCCHG was completed successfully'
Else
   Print 'Failure: The Update of table dbo.PRCCHG was not completed successfully'
GO
