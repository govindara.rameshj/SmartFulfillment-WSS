Declare @Store Char(3) = '853'
Declare @StoreNum Int = 853
Declare @StoreNumber Int = 8000 + @StoreNum

Insert Into Store Values(@StoreNum, 'Dev Store ' + @Store, 'WSS Dev Team', 'Mallard Court', 'Express Park', 'Bridgwater', 'Somerset', 'TA6 4RN', '01278411309', '', '', '6', 'UK', 0, 1, 1, 1, 1, 1, 1, 1, 0)
Update CORHDR4 Set SellingStoreId = @StoreNumber Where SellingStoreId = Cast('8' + (Select STOR From RETOPT) AS Int)
Update CORLIN Set DeliverySource = @StoreNumber Where DeliverySource = Cast('8' + (Select STOR From RETOPT) AS Int)
Update CORTXT Set SellingStoreId = @StoreNumber Where SellingStoreId = Cast('8' + (Select STOR From RETOPT) AS Int)
Update CORRefund Set RefundStoreId = @StoreNumber Where RefundStoreId = Cast('8' + (Select STOR From RETOPT) AS Int)
Update DLCOUPON Set IssueStoreNo = @Store Where IssueStoreNo = (Select STOR From RETOPT)
Update RETOPT Set STOR = @Store, SNAM = 'Dev Store ' + @Store
Update SYSOPT Set STOR = @Store, SNAM = 'Dev Store ' + @Store, HNAM = 'WSS Dev Team', HAD1 = 'Mallard Court', HAD2 = 'Express Park', HAD3 = '', HAD4 = 'Bridgwater', HAD5 = 'Somerset', HPST = 'TA6 4RN', HPHN = '01278411325', HFAX = ''
Update Parameters Set LongValue = 8902 Where ParameterID = 3020
Update Parameters Set LongValue = 8903 Where ParameterID = 3030
Update Parameters Set StringValue = 'Dev Store ' + @Store, LongValue = 853 Where ParameterID = 100
Update Parameters Set StringValue = 'C:\Shared Backups' Where ParameterID = 190
Update Parameters Set BooleanValue = 0 Where ParameterID = 920
Update Parameters Set LongValue = 2 Where ParameterID = 954
Update Parameters Set StringValue = 'C:\Saved Routes' Where ParameterID = 194
Update Parameters Set StringValue = '\\dev-b\data_tmp\paragon\wickes\' Where ParameterID = 199
Update Vehicle Set DepotID = @StoreNumber
/*
Select * From RETOPT
Select * From SYSOPT
Select * From Parameters
*/
