-- ===============================================================================
-- Author         : Alan Lewis
-- Date	          : 01/11/2011
-- Description    : Updates an Oasys database Commidea related parameters with 
--					values that are specific to the development/test environment.
--					For use, for example, after a live database has been restored
--					on a dev machine
-- ===============================================================================

If Exists
	(
		Select
			[Oasys].[dbo].[Parameters].[ParameterID] 
		From 
			[Oasys].[dbo].[Parameters]
		Where 
			[Oasys].[dbo].[Parameters].[ParameterID] = 970001
	)
		Begin
			Update
				[Oasys].[dbo].[Parameters]
			Set
				[Oasys].[dbo].[Parameters].StringValue = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20'
			Where
				[Oasys].[dbo].[Parameters].[ParameterID] = 970001;
			If @@ERROR = 0
				Print 'Success: The update of the ''Commidea Tills'' parameter (ID = 970001) to a dev/test value has been successful'
			Else
				Print 'Failure: The update of the ''Commidea Tills'' parameter (ID = 970001) to a dev/test value has NOT been successful'
		End
Else
	Begin
		Insert Into 
			[Oasys].[dbo].[Parameters]
				(
					ParameterID,
					[Description],
					StringValue,
					LongValue,
					BooleanValue,
					DecimalValue,
					ValueType
				)
			Values
				(
					970001,
					' Commidea Tills',
					'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20',
					NULL,
					0,
					NULL,
					0
				)
		If @@ERROR = 0
			Print 'Success: The insert of the ''Commidea Tills'' parameter (ID = 970001) dev/test value has been successful'
		Else
			Print 'Failure: The insert of the ''Commidea Tills'' parameter (ID = 970001) dev/test value has NOT been successful'
	End
				     
If EXISTS 
	(
		Select 
			[Oasys].[dbo].[Parameters].[ParameterID] 
		From 
			[Oasys].[dbo].[Parameters]
		Where 
			[Oasys].[dbo].[Parameters].[ParameterID] = 970003
	)
		Begin
			Update
				[Oasys].[dbo].[Parameters]
			Set
				[Oasys].[dbo].[Parameters].StringValue = '1234,1234'
			Where 
				[Oasys].[dbo].[Parameters].[ParameterID] = 970003
			If @@ERROR = 0
				Print 'Success: The update of the ''Ocius Sentinel Login'' parameter (ID = 970003) to a dev/test value has been successful'
			Else
				Print 'Failure: The update of the ''Ocius Sentinel Login'' parameter (ID = 970003) to a dev/test value has NOT been successful'
		End
Else
	Begin
		Insert Into 
			[Oasys].[dbo].[Parameters]
				(
					ParameterID,
					[Description],
					StringValue,
					LongValue,
					BooleanValue,
					DecimalValue,
					ValueType
				)
			Values
				(
					970003,
					'Ocius Sentinel Login',
					'1234,1234',
					NULL,
					0,
					NULL,
					0
			    )
		If @@ERROR = 0
			Print 'Success: The insert of the ''Ocius Sentinel Login'' parameter (ID = 970003) dev/test value has been successful'
		Else
			Print 'Failure: The insert of the ''Ocius Sentinel Login'' parameter (ID = 970003) dev/test value has NOT been successful'
	End
	
If EXISTS 
	(
		Select 
			[Oasys].[dbo].[Parameters].[ParameterID] 
		From 
			[Oasys].[dbo].[Parameters]
		Where 
			[Oasys].[dbo].[Parameters].[ParameterID] = 970017
	)
		Begin
			Update
				[Oasys].[dbo].[Parameters]
			Set
				[Oasys].[dbo].[Parameters].StringValue = '2'
			Where 
				[Oasys].[dbo].[Parameters].[ParameterID] = 970017
			If @@ERROR = 0
				Print 'Success: The update of the ''Commidea Receipt Header Last Line'' parameter (ID = 970017) to a dev/test value has been successful'
			Else
				Print 'Failure: The update of the ''Commidea Receipt Header Last Line'' parameter (ID = 970017) to a dev/test value has NOT been successful'
		End
Else
	Begin
		Insert Into 
			[Oasys].[dbo].[Parameters]
				(
					ParameterID,
					[Description],
					StringValue,
					LongValue,
					BooleanValue,
					DecimalValue,
					ValueType
				)
			Values
				(
					970017,
					'Commidea Receipt Header Last Line',
					'2',
					NULL,
					0,
					NULL,
					0
				)
		If @@ERROR = 0
			Print 'Success: The insert of the ''Commidea Receipt Header Last Line'' parameter (ID = 970017) dev/test value has been successful'
		Else
			Print 'Failure: The insert of the ''Commidea Receipt Header Last Line'' parameter (ID = 970017) dev/test value has NOT been successful'
	End

If EXISTS 
	(
		Select 
			[Oasys].[dbo].[Parameters].[ParameterID] 
		From 
			[Oasys].[dbo].[Parameters]
		Where 
			[Oasys].[dbo].[Parameters].[ParameterID] = 970018
	)
		Begin
			Update
				[Oasys].[dbo].[Parameters]
			Set
				[Oasys].[dbo].[Parameters].StringValue = '1'
			Where 
				[Oasys].[dbo].[Parameters].[ParameterID] = 970018
			If @@ERROR = 0
				Print 'Success: The update of the ''Commidea Receipt Footer First Line'' parameter (ID = 970018) to a dev/test value has been successful'
			Else
				Print 'Failure: The update of the ''Commidea Receipt Footer First Line'' parameter (ID = 970018) to a dev/test value has NOT been successful'
		End
Else
	Begin
		Insert Into 
			[Oasys].[dbo].[Parameters]
				(
					ParameterID,
					[Description],
					StringValue,
					LongValue,
					BooleanValue,
					DecimalValue,
					ValueType
				)
			Values
				(
					970018,
					'Commidea Receipt Footer First Line',
					'1',
					NULL,
					0,
					NULL,
					0
				)
		If @@ERROR = 0
			Print 'Success: The insert of the ''Commidea Receipt Footer First Line'' parameter (ID = 970018) dev/test value has been successful'
		Else
			Print 'Failure: The insert of the ''Commidea Receipt Footer First Line'' parameter (ID = 970018) dev/test value has NOT been successful'				
	End


If EXISTS 
	(
		Select 
			[Oasys].[dbo].[Parameters].[ParameterID] 
		From 
			[Oasys].[dbo].[Parameters]
		Where 
			[Oasys].[dbo].[Parameters].[ParameterID] = 970019
	)
		Begin
			Update
				[Oasys].[dbo].[Parameters]
			Set
				[Oasys].[dbo].[Parameters].StringValue = 'Travis Perkins'
			Where 
				[Oasys].[dbo].[Parameters].[ParameterID] = 970019
			If @@ERROR = 0
				Print 'Success: The update of the ''Commidea Licence Merchant Name'' parameter (ID = 970019) to a dev/test value has been successful'
			Else
				Print 'Failure: The update of the ''Commidea Licence Merchant Name'' parameter (ID = 970019) to a dev/test value has NOT been successful'
		End
Else
	Begin
		Insert Into 
			[Oasys].[dbo].[Parameters]
				(
					ParameterID,
					[Description],
					StringValue,
					LongValue,
					BooleanValue,
					DecimalValue,
					ValueType
				)
			Values
				(
					970019,
					'Commidea Licence Merchant Name',
					'Travis Perkins',
					NULL,
					0,
					NULL,
					0
				)
		If @@ERROR = 0
			Print 'Success: The insert of the ''Commidea Licence Merchant Name'' parameter (ID = 970019) dev/test value has been successful'
		Else
			Print 'Failure: The insert of the ''Commidea Licence Merchant Name'' parameter (ID = 970019) dev/test value has NOT been successful'				
	End
