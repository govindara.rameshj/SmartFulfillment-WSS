--DLRCUS
Update
	DLRCUS
Set
	NAME = (Select Case ISNULL(NAME, '') When '' Then ''  Else UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))) End),
	HNAM = (Select Case ISNULL(HNAM, '') When '' Then ''  Else UPPER(dbo.udf_GetHouseNumberOrName(((ABS(CHECKSUM(NewId())) % 16669) + 1))) End),
	HAD1 = (Select Case ISNULL(HAD1, '') When '' Then ''  Else UPPER(dbo.udf_GetStreetName(((ABS(CHECKSUM(NewId())) % 59) + 1))) End),
	HAD2 = (Select Case ISNULL(HAD2, '') When '' Then ''  Else UPPER(dbo.udf_GetLocalArea(((ABS(CHECKSUM(NewId())) % 383) + 1))) End),
	HAD3 = (Select Case ISNULL(HAD3, '') When '' Then ''  Else UPPER(dbo.udf_GetTown(((ABS(CHECKSUM(NewId())) % 83) + 1))) End),
	PHON = (Select Case ISNULL(PHON, '') When '' Then ''  Else dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	MOBP = (Select Case ISNULL(MOBP, '') When '' Then ''  Else '07' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 5) + 4)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	PhoneNumberWork = '',
	EmailAddress = ''
Where
	NUMB = 0
And
	[TRAN] % 11 >= 0 And [TRAN] % 11 <= 4

Update
	DLRCUS
Set
	NAME = (Select Case ISNULL(NAME, '') When '' Then ''  Else UPPER(dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1))) + ' ' + UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))) End),
	HNAM = (Select Case ISNULL(HNAM, '') When '' Then ''  Else UPPER(dbo.udf_GetHouseNumberOrName(((ABS(CHECKSUM(NewId())) % 16669) + 1))) End),
	HAD1 = (Select Case ISNULL(HAD1, '') When '' Then ''  Else UPPER(dbo.udf_GetStreetName(((ABS(CHECKSUM(NewId())) % 59) + 1))) End),
	HAD2 = (Select Case ISNULL(HAD2, '') When '' Then ''  Else UPPER(dbo.udf_GetLocalArea(((ABS(CHECKSUM(NewId())) % 383) + 1))) End),
	HAD3 = (Select Case ISNULL(HAD3, '') When '' Then ''  Else UPPER(dbo.udf_GetTown(((ABS(CHECKSUM(NewId())) % 83) + 1))) End),
	PHON = (Select Case ISNULL(PHON, '') When '' Then ''  Else dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	MOBP = (Select Case ISNULL(MOBP, '') When '' Then ''  Else '07' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 5) + 4)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	PhoneNumberWork = '',
	EmailAddress = ''
Where
	NUMB = 0
And
	[TRAN] % 11 >= 5 And [TRAN] % 11 <= 6

Update
	DLRCUS
Set
	NAME = (Select Case ISNULL(NAME, '') When '' Then ''  Else UPPER(dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1))) + ' ' + UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))) End),
	HNAM = (Select Case ISNULL(HNAM, '') When '' Then ''  Else UPPER(dbo.udf_GetHouseNumberOrName(((ABS(CHECKSUM(NewId())) % 16669) + 1))) End),
	HAD1 = (Select Case ISNULL(HAD1, '') When '' Then ''  Else UPPER(dbo.udf_GetStreetName(((ABS(CHECKSUM(NewId())) % 59) + 1))) End),
	HAD2 = (Select Case ISNULL(HAD2, '') When '' Then ''  Else UPPER(dbo.udf_GetLocalArea(((ABS(CHECKSUM(NewId())) % 383) + 1))) End),
	HAD3 = (Select Case ISNULL(HAD3, '') When '' Then ''  Else UPPER(dbo.udf_GetTown(((ABS(CHECKSUM(NewId())) % 83) + 1))) End),
	PHON = (Select Case ISNULL(PHON, '') When '' Then ''  Else dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	MOBP = (Select Case ISNULL(MOBP, '') When '' Then ''  Else '07' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 5) + 4)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	PhoneNumberWork = '',
	EmailAddress = ''
Where
	NUMB = 0
And
	[TRAN] % 11 >= 7 And [TRAN] % 11 <= 8

Update
	DLRCUS
Set
	NAME = (Select Case ISNULL(NAME, '') When '' Then ''  Else UPPER(dbo.udf_GetFemaleTitle(((ABS(CHECKSUM(NewId())) % 51) + 1))) + ' ' + UPPER(dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1))) + ' ' + UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))) End),
	HNAM = (Select Case ISNULL(HNAM, '') When '' Then ''  Else UPPER(dbo.udf_GetHouseNumberOrName(((ABS(CHECKSUM(NewId())) % 16669) + 1))) End),
	HAD1 = (Select Case ISNULL(HAD1, '') When '' Then ''  Else UPPER(dbo.udf_GetStreetName(((ABS(CHECKSUM(NewId())) % 59) + 1))) End),
	HAD2 = (Select Case ISNULL(HAD2, '') When '' Then ''  Else UPPER(dbo.udf_GetLocalArea(((ABS(CHECKSUM(NewId())) % 383) + 1))) End),
	HAD3 = (Select Case ISNULL(HAD3, '') When '' Then ''  Else UPPER(dbo.udf_GetTown(((ABS(CHECKSUM(NewId())) % 83) + 1))) End),
	PHON = (Select Case ISNULL(PHON, '') When '' Then ''  Else dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	MOBP = (Select Case ISNULL(MOBP, '') When '' Then ''  Else '07' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 5) + 4)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	PhoneNumberWork = '',
	EmailAddress = ''
Where
	NUMB = 0
And
	[TRAN] % 11 = 9

Update
	DLRCUS
Set
	NAME = (Select Case ISNULL(NAME, '') When '' Then ''  Else UPPER(dbo.udf_GetMaleTitle(((ABS(CHECKSUM(NewId())) % 51) + 1))) + ' ' + UPPER(dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1))) + ' ' + UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))) End),
	HNAM = (Select Case ISNULL(HNAM, '') When '' Then ''  Else UPPER(dbo.udf_GetHouseNumberOrName(((ABS(CHECKSUM(NewId())) % 16669) + 1))) End),
	HAD1 = (Select Case ISNULL(HAD1, '') When '' Then ''  Else UPPER(dbo.udf_GetStreetName(((ABS(CHECKSUM(NewId())) % 59) + 1))) End),
	HAD2 = (Select Case ISNULL(HAD2, '') When '' Then ''  Else UPPER(dbo.udf_GetLocalArea(((ABS(CHECKSUM(NewId())) % 383) + 1))) End),
	HAD3 = (Select Case ISNULL(HAD3, '') When '' Then ''  Else UPPER(dbo.udf_GetTown(((ABS(CHECKSUM(NewId())) % 83) + 1))) End),
	PHON = (Select Case ISNULL(PHON, '') When '' Then ''  Else dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	MOBP = (Select Case ISNULL(MOBP, '') When '' Then ''  Else '07' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 5) + 4)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	PhoneNumberWork = '',
	EmailAddress = ''
Where
	NUMB = 0
And
	[TRAN] % 11 = 10


Update
	DLRCUS
Set
	NAME = '',
	HNAM = (Select RC.HNAM From DLRCUS RC Where RC.DATE1 = DLRCUS.DATE1 And RC.TILL = DLRCUS.TILL And RC.[TRAN] = DLRCUS.[TRAN] And NUMB = 0),
	HAD1 = '',
	HAD2 = '',
	HAD3 = '',
	PHON = '',
	MOBP = '',
	PhoneNumberWork = '',
	EmailAddress = ''
Where
	NUMB > 0


-- COR tables
Declare @NewCorHdrValues Table
(
  Name		VarChar(50),
  HouseName	VarChar(15),
  Addr1		VarChar(60),
  Addr2		VarChar(50),
  Addr3		VarChar(50),
  Phone		VarChar(20),
  Mobile	VarChar(20),
  WorkPhone	VarChar(20),
  Email		VarChar(100),
  Numb      Char(6)
)

Insert 
	@NewCorHdrValues 
Select
	RC.NAME, 
	RC.HNAM,
	RC.HAD1, 
	RC.HAD2, 
	RC.HAD3, 
	RC.PHON, 
	RC.MOBP,
	RC.PhoneNumberWork,
	RC.EmailAddress,
	T.ORDN
From 
	DLTOTS T
Inner Join
	DLRCUS RC
On 
	RC.DATE1 = T.DATE1 
And 
	RC.TILL = T.TILL 
And 
	RC.[TRAN] = T.[TRAN] 
And
	RC.NUMB = 0
And
	T.VOID = 0
And
	T.PARK = 0
And
	T.TMOD = 0
Order By
	T.DATE1

-- CORHDR
Update
	CORHDR
Set
	NAME = '?'

Declare @Numb	Char(6)
Declare 
	TempCursor 
Cursor For
	Select
		NUMB
	From
		CORHDR
		
Open
	TempCursor
Fetch Next From 
	TempCursor 
Into 
	@Numb
	
While @@fetch_status = 0
	Begin
				
		If (Select COUNT(*) From @NewCorHdrValues Where Numb = @Numb) > 0
		Begin
			Update
				CORHDR
			Set
				NAME = (Select Top(1) Name From @NewCorHdrValues Where Numb = @Numb),
				ADDR1 = LTrim(RTrim((Select Top(1) HouseName From @NewCorHdrValues Where Numb = @Numb))) + ', ' + LTrim(RTrim((Select Top(1) Addr1 From @NewCorHdrValues Where Numb = @Numb))),
				ADDR2 = (Select Top(1) Addr2 From @NewCorHdrValues Where Numb = @Numb),
				ADDR3 = (Select Top(1) Addr3 From @NewCorHdrValues Where Numb = @Numb),
				ADDR4 = (Select Case ISNULL(ADDR4, '') When '' Then ''  Else UPPER(dbo.udf_GetCounty(((ABS(CHECKSUM(NewId())) % 40) + 1))) End),
				PHON = (Select Top(1) Phone From @NewCorHdrValues Where Numb = @Numb),
				MOBP = (Select Top(1) Mobile From @NewCorHdrValues Where Numb = @Numb)
			Where
				NUMB = @Numb
		End
		
		Fetch Next From
			TempCursor 
		Into
			@Numb
	End
	
Close TempCursor
Deallocate TempCursor

Update
	CORHDR
Set
	ADDR1 = CONVERT(VARCHAR(3), ((ABS(CHECKSUM(NewId())) % 150) + 1)) + ', ' + UPPER(dbo.udf_GetStreetName(((ABS(CHECKSUM(NewId())) % 59) + 1))),
	ADDR2 = (Select Case ISNULL(ADDR2, '') When '' Then ''  Else UPPER(dbo.udf_GetLocalArea(((ABS(CHECKSUM(NewId())) % 383) + 1))) End),
	ADDR3 = (Select Case ISNULL(ADDR3, '') When '' Then ''  Else UPPER(dbo.udf_GetTown(((ABS(CHECKSUM(NewId())) % 83) + 1))) End),
	ADDR4 = (Select Case ISNULL(ADDR4, '') When '' Then ''  Else UPPER(dbo.udf_GetCounty(((ABS(CHECKSUM(NewId())) % 40) + 1))) End),
	PHON = (Select Case ISNULL(PHON, '') When '' Then ''  Else dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End),
	MOBP =	(Select Case ISNULL(MOBP, '') When '' Then ''  Else '07' +  
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 5) + 4)) + 
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + 
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + ' ' + 
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + 
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + 
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + 
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + 
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + 
			CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End)
Where
	NAME = '?'

Update
	CORHDR
Set
	NAME = UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1)))
Where
	NAME = '?'
And
	NUMB % 8 >= 0 And NUMB % 8 <= 3

Update
	CORHDR
Set
	NAME = UPPER(dbo.udf_GetMaleTitle(((ABS(CHECKSUM(NewId())) % 51) + 1))) + ' ' + UPPER(dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1))) + ' ' + UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1)))
Where
	NAME = '?'
And
	NUMB % 8 = 4

Update
	CORHDR
Set
	NAME = UPPER(dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1))) + ' ' + UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1)))
Where
	NAME = '?'
And
	NUMB % 8 = 5

Update
	CORHDR
Set
	NAME = UPPER(dbo.udf_GetFemaleTitle(((ABS(CHECKSUM(NewId())) % 51) + 1))) + ' ' + UPPER(dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1))) + ' ' + UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1)))
Where
	NAME = '?'
And
	NUMB % 8 = 6

Update
	CORHDR
Set
	NAME = UPPER(dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1))) + ' ' + UPPER(dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1)))
Where
	NAME = '?'
And
	NUMB % 8 = 7

-- CORHDR4
Update
	CORHDR4
Set
	CORHDR4.NAME = (Select NAME From CORHDR Where CORHDR.NUMB = CORHDR4.NUMB)

Update
	CORHDR4
Set
	CustomerAddress1 = 
		(
			Select Case 
				When
					(
						(Select LEN(LTrim(RTrim(ADDR1))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR2))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR3))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR4))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And
						CORHDR4.OMOrderNumber % 15 > 0
					)
				Then 
					(Select ADDR1 From CORHDR H Where CORHDR4.NUMB = H.NUMB)
				Else
					(Select Case ISNULL(CustomerAddress1, '') When '' Then ''  Else CONVERT(VARCHAR(3), ((ABS(CHECKSUM(NewId())) % 150) + 1)) + ' ' + UPPER(dbo.udf_GetStreetName(((ABS(CHECKSUM(NewId())) % 59) + 1))) End)
				End
		),
	CustomerAddress2 = 
		(
			Select Case 
				When
					(
						(Select LEN(LTrim(RTrim(ADDR1))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR2))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR3))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR4))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And
						CORHDR4.OMOrderNumber % 15 > 0
					)
				Then 
					(Select ADDR2 From CORHDR H Where CORHDR4.NUMB = H.NUMB)
				Else 
					(Select Case ISNULL(CustomerAddress2, '') When '' Then ''  Else UPPER(dbo.udf_GetLocalArea(((ABS(CHECKSUM(NewId())) % 383) + 1))) End)
				End
		),
	CustomerAddress3 = 
		(
			Select Case 
				When
					(
						(Select LEN(LTrim(RTrim(ADDR1))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR2))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR3))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR4))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And
						CORHDR4.OMOrderNumber % 15 > 0
					)
				Then 
					(Select ADDR3 From CORHDR H Where CORHDR4.NUMB = H.NUMB)
				Else 
					(Select Case ISNULL(CustomerAddress3, '') When '' Then ''  Else UPPER(dbo.udf_GetTown(((ABS(CHECKSUM(NewId())) % 83) + 1))) End)
				End
		),
	CustomerAddress4 = 
		(
			Select Case 
				When
					(
						(Select LEN(LTrim(RTrim(ADDR1))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR2))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR3))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR4))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And
						CORHDR4.OMOrderNumber % 15 > 0
					)
				Then 
					(Select ADDR4 From CORHDR H Where CORHDR4.NUMB = H.NUMB)
				Else 
					(Select Case ISNULL(CustomerAddress4, '') When '' Then ''  Else UPPER(dbo.udf_GetCounty(((ABS(CHECKSUM(NewId())) % 40) + 1))) End)
				End
		),
	PhoneNumberWork = 
		(
			Select Case 
				When
					(
						(Select LEN(LTrim(RTrim(ADDR1))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR2))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR3))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR4))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And
						CORHDR4.OMOrderNumber % 15 > 0
					)
				Then 
					(Select Case ISNULL(PhoneNumberWork, '') When '' Then ''  Else (Select Top(1) WorkPhone From @NewCorHdrValues Where Numb = CORHDR4.NUMB) End)
				Else 
					dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1))
				End
		),
	CustomerEmail = 
		(
			Select Case 
				When
					(
						(Select LEN(LTrim(RTrim(ADDR1))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR2))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR3))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And	
						(Select LEN(LTrim(RTrim(ADDR4))) From CORHDR H Where CORHDR4.NUMB = H.NUMB) <= 30
					And
						CORHDR4.OMOrderNumber % 15 > 0
					)
				Then 
					(Select Case ISNULL(CustomerEmail, '') When '' Then ''  Else (Select Top(1) Email From @NewCorHdrValues Where Numb = CORHDR4.NUMB) End)
				Else 
					(Select Case ISNULL(CustomerEmail, '') When '' Then ''  Else 'no.one@nowhere.co.uk' End)
				End
		)

-- CORHDR5
Update
	CORHDR5
Set
	DeliveryContactName = (Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactName  Else (Select Name From CORHDR Where CORHDR.NUMB = CORHDR5.NUMB) End),
	DeliveryContactPhone = (Case ISNULL(DeliveryContactPhone, '') When '' Then DeliveryContactPhone  Else (Select MOBP From CORHDR Where CORHDR.NUMB = CORHDR5.NUMB) End)
Where
	NUMB % 14 < 7

Update
	CORHDR5
Set
	DeliveryContactName = (Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactName  Else (Select Name From CORHDR Where CORHDR.NUMB = CORHDR5.NUMB) End),
	DeliveryContactPhone = (Case ISNULL(DeliveryContactPhone, '') When '' Then DeliveryContactPhone  Else (Select PHON From CORHDR Where CORHDR.NUMB = CORHDR5.NUMB) End)
Where
	NUMB % 14 = 7

Update
	CORHDR5
Set
	DeliveryContactName = (Select Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactName  Else UPPER((dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + (dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))))) End),
	DeliveryContactPhone = (Select Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactPhone  Else dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End)
Where
	NUMB % 14 = 8

Update
	CORHDR5
Set
	DeliveryContactName = (Select Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactName  Else UPPER((dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + (dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))))) End),
	DeliveryContactPhone = (Case ISNULL(DeliveryContactPhone, '') When '' Then DeliveryContactPhone  Else (Select PHON From CORHDR Where CORHDR.NUMB = CORHDR5.NUMB) End)
Where
	NUMB % 14 = 9

Update
	CORHDR5
Set
	DeliveryContactName = (Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactName  Else UPPER((dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + (dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))))) End),
	DeliveryContactPhone = (Case ISNULL(DeliveryContactPhone, '') When '' Then DeliveryContactPhone  Else (Select MOBP From CORHDR Where CORHDR.NUMB = CORHDR5.NUMB) End)
Where
	NUMB % 14 = 10

Update
	CORHDR5
Set
	DeliveryContactName = (Select Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactName  Else UPPER((dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + (dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))))) End),
	DeliveryContactPhone = (Select Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactPhone  Else dbo.udf_GetSTDCode(((ABS(CHECKSUM(NewId())) % 40) + 1)) + ' ' + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) + CONVERT(CHAR(1), ((ABS(CHECKSUM(NewId())) % 9) + 1)) End)
Where
	NUMB % 14 = 11

Update
	CORHDR5
Set
	DeliveryContactName = (Select Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactName  Else UPPER((dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + (dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))))) End),
	DeliveryContactPhone = (Case ISNULL(DeliveryContactPhone, '') When '' Then DeliveryContactPhone  Else (Select PHON From CORHDR Where CORHDR.NUMB = CORHDR5.NUMB) End)
Where
	NUMB % 14 > 12

Update
	CORHDR5
Set
	DeliveryContactName = (Case ISNULL(DeliveryContactName, '') When '' Then DeliveryContactName  Else UPPER((dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + (dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))))) End),
	DeliveryContactPhone = (Case ISNULL(DeliveryContactPhone, '') When '' Then DeliveryContactPhone  Else (Select MOBP From CORHDR Where CORHDR.NUMB = CORHDR5.NUMB) End)
Where
	NUMB % 14 = 13


-- VehicleCollectionHeader

Update
	VehicleCollectionHeader
Set
	Name = 
		(
			Case
				When 
					(
						(
							Select Case 
								When (CHARINDEX(' ', CustomerAddress1)) > 0 Then ISNUMERIC(LEFT(CustomerAddress1, CHARINDEX(' ', CustomerAddress1) - 1))
								Else 0
							End
						)
					) = 1 Then dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))
				Else
					Name
				End
		),
	CustomerAddress2 =
		(
			Case
				When ISNULL(CustomerAddress2, '') = '' Then CustomerAddress2
				Else dbo.udf_GetLocalArea(((ABS(CHECKSUM(NewId())) % 383) + 1))
			End
		),
	CustomerAddress3 =
		(
			Case
				When ISNULL(CustomerAddress3, '') = '' Then CustomerAddress3
				Else dbo.udf_GetTown(((ABS(CHECKSUM(NewId())) % 83) + 1))
			End
		)
Update
	VehicleCollectionHeader
Set
	CustomerAddress1 =
		(
			Case
				When 
					(
						(
							Select Case 
								When (CHARINDEX(' ', CustomerAddress1)) > 0 Then ISNUMERIC(LEFT(CustomerAddress1, CHARINDEX(' ', CustomerAddress1) - 1))
								Else 0
							End
						)
					) = 1 Then CONVERT(VARCHAR(3), ((ABS(CHECKSUM(NewId())) % 150) + 1)) + ' ' + UPPER(dbo.udf_GetStreetName(((ABS(CHECKSUM(NewId())) % 59) + 1)))
				Else CustomerAddress1
			End 
		)

/****** Object:  Trigger [tgSystemUsersUpdate]    Script Date: 05/08/2013 10:15:09 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tgSystemUsersUpdate]'))
DROP TRIGGER [dbo].[tgSystemUsersUpdate]




Declare @CursorID	Int
Declare 
	TempCursor 
Cursor For
	Select
		ID
	From
		SystemUsers
		
Open
	TempCursor
Fetch Next From 
	TempCursor 
Into 
	@CursorID
	
While @@fetch_status = 0
	Begin
		Update
			SystemUsers
		Set
			[Password] = '11111',
			[PasswordExpires] = DateAdd(y, 10, GetDate()),
			[SupervisorPassword] = Null,
			[SupervisorPwdExpires] = Null,
			[Name] = 
			(
				Select
					Case
						When Lower([Name]) In ('trade express till', 'refund till', 'loss prevention', 'temp vision user', 'tp support desk', 'it support', 'cts', 'all') Then [Name] 
						When ID % 2 =0 Then dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))
						Else dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))
					End
			)
		Where
			ID = @CursorID 
		And
			[IsSupervisor] = 0
		And
			[IsManager] = 0
		Update
			SystemUsers
		Set
			[Password] = '11111',
			[PasswordExpires] = DateAdd(y, 10, GetDate()),
			[SupervisorPassword] = '22222',
			[SupervisorPwdExpires] = DateAdd(yy, 10, GetDate()),
			[Name] = 
			(
				Select
					Case
						When Lower([Name]) In ('trade express till', 'refund till', 'loss prevention', 'temp vision user', 'tp support desk', 'it support', 'cts', 'all') Then [Name] 
						When ID % 2 =0 Then dbo.udf_GetMaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))
						Else dbo.udf_GetFemaleGivenName(((ABS(CHECKSUM(NewId())) % 100) + 1)) + ' ' + dbo.udf_GetFamilyName(((ABS(CHECKSUM(NewId())) % 500) + 1))
					End
			)
		Fetch Next From
			TempCursor 
		Into
			@CursorID
	End
	
Close TempCursor
Deallocate TempCursor

DECLARE	
		@EmployeeCode			char(3),
		@Name					char(35),
		@Initials				char(5),
		@Position				char(20),
		@PayrollId				char(20),
		@Password				char(5),
		@PasswordExpires		date,
		@IsSupervisor			bit,
		@SuperPassword			char(5),
		@SuperPasswordExpires	date,
		@Outlet					char(2),
		@IsDeleted				bit,
		@DeletedDate			date,
		@DeletedTime			char(6),
		@DeletedBy				char(3),
		@DeletedWhere			char(2),
		@TillReceiptName		char(35),
		@DefaultAmount			dec(9,2),
		@LanguageCode			char(3),
		@IsManager				bit,
		@SecurityProfileId		int;

Declare 
	TempCursor 
Cursor For
	Select
		ID
	From
		SystemUsers
		
Open
	TempCursor
Fetch Next From 
	TempCursor 
Into 
	@CursorID
	
While @@fetch_status = 0
	Begin

	Update
		SystemUsers
	Set
		[TillReceiptName] = 
		(
			Select
				Case
					When Lower([Name]) In ('trade express till', 'refund till', 'loss prevention', 'temp vision user', 'tp support desk', 'it support', 'cts', 'all') Then [TillReceiptName]
					Else LEFT([Name], CHARINDEX(' ', [Name]) - 1) + ' ' + SUBSTRING([Name], CHARINDEX(' ', [Name]) + 1, 1)
				End
		),
		[Initials] = 
		(
			Select
				Case
					When Lower([Name]) In ('trade express till', 'refund till', 'loss prevention', 'temp vision user', 'tp support desk', 'it support', 'cts', 'all') Then [Initials]
					Else LEFT([Name], 1) + ' ' + SUBSTRING([Name], CHARINDEX(' ', [Name]) + 1, 1)
				End
		)
	Where
		ID = @CursorID 

	Select
		@EmployeeCode			= i.EmployeeCode,
		@Name					= i.Name,
		@Initials				= i.Initials,
		@Position				= i.Position,
		@PayrollId				= i.PayrollId,
		@Password				= i.Password,
		@PasswordExpires		= i.PasswordExpires,
		@IsSupervisor			= i.IsSupervisor,
		@SuperPassword			= i.SupervisorPassword,
		@SuperPasswordExpires	= i.SupervisorPwdExpires,
		@Outlet					= i.Outlet,
		@IsDeleted				= i.IsDeleted,
		@DeletedDate			= i.DeletedDate,
		@DeletedTime			= i.DeletedTime,
		@DeletedBy				= i.DeletedBy,
		@DeletedWhere			= i.DeletedWhere,
		@TillReceiptName		= i.TillReceiptName,
		@DefaultAmount			= i.DefaultAmount,
		@LanguageCode			= i.LanguageCode,
		@IsManager				= i.IsManager,
		@SecurityProfileId		= i.SecurityProfileID
	From
		SystemUsers i
	Where
		i.ID = @CursorID

	--------------------------------------------------------------------------------------------------------------------------
	-- Call SYSPAS Update / Insert (usp_SystemUsersUpdateSYSPAS)
	--------------------------------------------------------------------------------------------------------------------------
	Begin
		EXEC	[dbo].[usp_SystemUsersUpdateSYSPAS]
						
						@EmployeeCode,
						@Name,
						@Initials,
						@Position,
						@PayrollId,
						@Password,
						@PasswordExpires,
						@IsSupervisor,
						@SuperPassword,
						@SuperPasswordExpires,
						@Outlet,
						@IsDeleted,
						@DeletedDate,
						@DeletedTime,
						@DeletedBy,
						@DeletedWhere,
						@TillReceiptName,
						@DefaultAmount,
						@LanguageCode,
						@IsManager,
						@SecurityProfileId		
	End

			
	--------------------------------------------------------------------------------------------------------------------------
	-- Call CASMAS Update / Insert (usp_SystemUsersUpdateCASMAS)
	--------------------------------------------------------------------------------------------------------------------------
	Begin
		EXEC	[dbo].[usp_SystemUsersUpdateCASMAS]
						
						@EmployeeCode,
						@Position,
						@Password,
						@IsSupervisor,
						@IsDeleted,
						@TillReceiptName,
						@DefaultAmount
	End
	Fetch Next From
		TempCursor 
	Into
		@CursorID
		
	Begin
		Update
			RSCASH
		Set
			NAME = @Name,
			SECC = @Password
		Where
			CASH = @EmployeeCode
	End
End
	
Close TempCursor
Deallocate TempCursor
