﻿Declare @Today Date = GETDATE()
Declare @Yesterday Date = DateAdd(Day, -1, @Today)
Declare @W_Year char(4) = CAST(DATEPART(Year, @Today) as Char(4))
Declare @W_Month Char(2) = RIGHT('0' + Convert(VARCHAR,DATEPART(MM, @Today)),2)
Declare @W_Day Char(2) = RIGHT('0' + DateName(DD, @Today),2)
Declare @L_Day Char(2) = RIGHT('0' + DateName(DD, @Today),2)
Declare @LDAT Char(10) = @W_Year + '-' + @W_Month + '-' + @L_Day
Declare @RDAT Char(10) = @W_Year + '-' + @W_Month + '-' + @W_Day
If Exists (Select TASK From NITLOG Where DATE1 = @Today and TASK = '410')
	Begin
		Print('System Task Already Set')
	End
Else
	Begin
		Declare @NSET char(1) = (Select LTRIM(RTRIM(NSET)) From SYSDAT Where FKEY = '01')
		Insert into NITLOG Values (@RDAT, @NSET, '410', 'System Log', 'NA', @RDAT, '010000', @RDAT, '010100', NULL, NULL, NULL, NULL)
	End