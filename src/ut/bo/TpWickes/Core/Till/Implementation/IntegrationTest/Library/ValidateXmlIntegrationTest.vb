﻿<TestClass()> Public Class ValidateXmlIntegrationTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Constants"

    Private GoodResponsePopulated As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                                 <OMCheckFulfilmentResponse>
                                                     <DateTimeStamp>2012-01-16T09:00:00.000</DateTimeStamp>
                                                     <SuccessFlag>true</SuccessFlag>
                                                     <EstoreFulfiller>Central Warehouse</EstoreFulfiller>
                                                     <HubFulfiller>Salisbury</HubFulfiller>
                                                 </OMCheckFulfilmentResponse>

    Private GoodResponseEmpty As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                             <OMCheckFulfilmentResponse>
                                                 <DateTimeStamp>2012-01-16T09:00:00.000</DateTimeStamp>
                                                 <SuccessFlag>true</SuccessFlag>
                                                 <EstoreFulfiller/>
                                                 <HubFulfiller/>
                                             </OMCheckFulfilmentResponse>

    Private BadResponse As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                       <OMCheckFulfilmentResponse>
                                           <DateTimeStamp>2012-01-16T09:00:00.000</DateTimeStamp>
                                           <SuccessFlag></SuccessFlag>
                                           <EstoreFulfiller/>
                                           <HubFulfiller/>
                                       </OMCheckFulfilmentResponse>

    Private GoodRequest As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                       <OMCheckFulfilmentRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                           <DateTimeStamp>0001-01-01T00:00:00</DateTimeStamp>
                                           <OrderHeader>
                                               <SellingStoreCode>8080</SellingStoreCode>
                                               <SellingStoreOrderNumber>18580</SellingStoreOrderNumber>
                                               <RequiredDeliveryDate>2012-01-30</RequiredDeliveryDate>
                                               <DeliveryCharge>40</DeliveryCharge>
                                               <TotalOrderValue>279.5</TotalOrderValue>
                                               <SaleDate>2012-01-20</SaleDate>
                                               <DeliveryPostcode>GL15AU</DeliveryPostcode>
                                               <ToBeDelivered>true</ToBeDelivered>
                                           </OrderHeader>
                                           <OrderLines>
                                               <OrderLine>
                                                   <SellingStoreLineNo>1</SellingStoreLineNo>
                                                   <ProductCode>100803</ProductCode>
                                                   <ProductDescription>Planed Softwood 18x28mmx1.8m PK10</ProductDescription>
                                                   <TotalOrderQuantity>10</TotalOrderQuantity>
                                                   <QuantityTaken>0</QuantityTaken>
                                                   <UOM>EACH</UOM>
                                                   <LineValue>109.9</LineValue>
                                                   <DeliveryChargeItem>false</DeliveryChargeItem>
                                                   <SellingPrice>10.99</SellingPrice>
                                               </OrderLine>
                                               <OrderLine>
                                                   <SellingStoreLineNo>2</SellingStoreLineNo>
                                                   <ProductCode>805000</ProductCode>
                                                   <ProductDescription>Delivery Charge</ProductDescription>
                                                   <TotalOrderQuantity>30</TotalOrderQuantity>
                                                   <QuantityTaken>0</QuantityTaken>
                                                   <UOM>EACH</UOM>
                                                   <LineValue>30</LineValue>
                                                   <DeliveryChargeItem>true</DeliveryChargeItem>
                                                   <SellingPrice>1</SellingPrice>
                                               </OrderLine>
                                               <OrderLine>
                                                   <SellingStoreLineNo>3</SellingStoreLineNo>
                                                   <ProductCode>101000</ProductCode>
                                                   <ProductDescription>Deck Board 28 x 140mm x 2.4m</ProductDescription>
                                                   <TotalOrderQuantity>20</TotalOrderQuantity>
                                                   <QuantityTaken>0</QuantityTaken>
                                                   <UOM>EACH</UOM>
                                                   <LineValue>129.6</LineValue>
                                                   <DeliveryChargeItem>false</DeliveryChargeItem>
                                                   <SellingPrice>6.48</SellingPrice>
                                               </OrderLine>
                                               <OrderLine>
                                                   <SellingStoreLineNo>4</SellingStoreLineNo>
                                                   <ProductCode>805000</ProductCode>
                                                   <ProductDescription>Delivery Charge</ProductDescription>
                                                   <TotalOrderQuantity>10</TotalOrderQuantity>
                                                   <QuantityTaken>0</QuantityTaken>
                                                   <UOM>EACH</UOM>
                                                   <LineValue>10</LineValue>
                                                   <DeliveryChargeItem>true</DeliveryChargeItem>
                                                   <SellingPrice>1</SellingPrice>
                                               </OrderLine>
                                           </OrderLines>
                                       </OMCheckFulfilmentRequest>

    Private BadRequest As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                      <OMCheckFulfilmentRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                          <DateTimeStamp></DateTimeStamp>
                                          <OrderHeader>
                                              <SellingStoreCode>8080</SellingStoreCode>
                                              <SellingStoreOrderNumber>18580</SellingStoreOrderNumber>
                                              <RequiredDeliveryDate>2012-01-30</RequiredDeliveryDate>
                                              <DeliveryCharge>40</DeliveryCharge>
                                              <TotalOrderValue>279.5</TotalOrderValue>
                                              <SaleDate>2012-01-20</SaleDate>
                                              <DeliveryPostcode>GL15AU</DeliveryPostcode>
                                              <ToBeDelivered>true</ToBeDelivered>
                                          </OrderHeader>
                                          <OrderLines>
                                              <OrderLine>
                                                  <SellingStoreLineNo>1</SellingStoreLineNo>
                                                  <ProductCode>100803</ProductCode>
                                                  <ProductDescription>Planed Softwood 18x28mmx1.8m PK10</ProductDescription>
                                                  <TotalOrderQuantity>10</TotalOrderQuantity>
                                                  <QuantityTaken>0</QuantityTaken>
                                                  <UOM>EACH</UOM>
                                                  <LineValue>109.9</LineValue>
                                                  <DeliveryChargeItem>false</DeliveryChargeItem>
                                                  <SellingPrice>10.99</SellingPrice>
                                              </OrderLine>
                                              <OrderLine>
                                                  <SellingStoreLineNo>2</SellingStoreLineNo>
                                                  <ProductCode>805000</ProductCode>
                                                  <ProductDescription>Delivery Charge</ProductDescription>
                                                  <TotalOrderQuantity>30</TotalOrderQuantity>
                                                  <QuantityTaken>0</QuantityTaken>
                                                  <UOM>EACH</UOM>
                                                  <LineValue>30</LineValue>
                                                  <DeliveryChargeItem>true</DeliveryChargeItem>
                                                  <SellingPrice>1</SellingPrice>
                                              </OrderLine>
                                              <OrderLine>
                                                  <SellingStoreLineNo>3</SellingStoreLineNo>
                                                  <ProductCode>101000</ProductCode>
                                                  <ProductDescription>Deck Board 28 x 140mm x 2.4m</ProductDescription>
                                                  <TotalOrderQuantity>20</TotalOrderQuantity>
                                                  <QuantityTaken>0</QuantityTaken>
                                                  <UOM>EACH</UOM>
                                                  <LineValue>129.6</LineValue>
                                                  <DeliveryChargeItem>false</DeliveryChargeItem>
                                                  <SellingPrice>6.48</SellingPrice>
                                              </OrderLine>
                                              <OrderLine>
                                                  <SellingStoreLineNo>4</SellingStoreLineNo>
                                                  <ProductCode>805000</ProductCode>
                                                  <ProductDescription>Delivery Charge</ProductDescription>
                                                  <TotalOrderQuantity>10</TotalOrderQuantity>
                                                  <QuantityTaken>0</QuantityTaken>
                                                  <UOM>EACH</UOM>
                                                  <LineValue>10</LineValue>
                                                  <DeliveryChargeItem>true</DeliveryChargeItem>
                                                  <SellingPrice>1</SellingPrice>
                                              </OrderLine>
                                          </OrderLines>
                                      </OMCheckFulfilmentRequest>

#End Region

#Region "Unit Tests"

    <TestMethod()> Public Sub FunctionValideAgainstXSD_InputScenerioTests()

        Dim V As ValidateXml
        Dim ReturnValue As Boolean
        Dim XmlError As String

        V = New ValidateXml
        XmlError = String.Empty
        ReturnValue = V.ValideXML(XsdType.OrderManagerCheckHowOrderIsFulfilled, XsdTypeDirection.Input, BadRequest, XmlError)
        Assert.IsFalse(ReturnValue, "ValideAgainstXSD (Input) - Bad Request Test A : Return Value")
        Assert.AreEqual("The 'DateTimeStamp' element is invalid - The value '' is invalid according to its datatype 'DateTime' - The string '' is not a valid XsdDateTime value.", _
                        XmlError, "ValideAgainstXSD (Input) - Bad Request Test A : Return Message")

        '''''''''''''''''''''''''''''''''''''''''''

        V = New ValidateXml
        XmlError = String.Empty
        ReturnValue = V.ValideXML(XsdType.OrderManagerCheckHowOrderIsFulfilled, XsdTypeDirection.Input, GoodRequest, XmlError)
        Assert.IsTrue(ReturnValue, "ValideAgainstXSD (Input) - Good Request Test A : Return Value")
        Assert.AreEqual("", XmlError, "ValideAgainstXSD (Input) - Good Request Test A : Return Message")


    End Sub

    <TestMethod()> Public Sub FunctionValideAgainstXSD_OuputScenerioTests()

        Dim V As ValidateXml
        Dim ReturnValue As Boolean
        Dim XmlError As String

        V = New ValidateXml
        XmlError = String.Empty
        ReturnValue = V.ValideXML(XsdType.OrderManagerCheckHowOrderIsFulfilled, XsdTypeDirection.Ouptut, BadResponse, XmlError)

        Assert.IsFalse(ReturnValue, "ValideAgainstXSD (Output) - Bad Response Test A : Return Value")
        Assert.AreEqual("The 'SuccessFlag' element is invalid - The value '' is invalid according to its datatype 'http://www.w3.org/2001/XMLSchema:boolean' - The string '' is not a valid Boolean value.", _
                        XmlError, "ValideAgainstXSD (Output) - Bad Response Test A : Return Message")

        '''''''''''''''''''''''''''''''''''''''''''

        V = New ValidateXml
        XmlError = String.Empty
        ReturnValue = V.ValideXML(XsdType.OrderManagerCheckHowOrderIsFulfilled, XsdTypeDirection.Ouptut, GoodResponsePopulated, XmlError)
        Assert.IsTrue(ReturnValue, "ValideAgainstXSD (Output) - Good Response Test A : Return Value")
        Assert.AreEqual("", XmlError, "ValideAgainstXSD (Output) - Good Response Test A : Return Message")

        '''''''''''''''''''''''''''''''''''''''''''

        V = New ValidateXml
        XmlError = String.Empty
        ReturnValue = V.ValideXML(XsdType.OrderManagerCheckHowOrderIsFulfilled, XsdTypeDirection.Ouptut, GoodResponseEmpty, XmlError)
        Assert.IsTrue(ReturnValue, "ValideAgainstXSD (Output) - Good Response Test B : Return Value")
        Assert.AreEqual("", XmlError, "ValideAgainstXSD (Output) - Good Response Test B : Return Message")

    End Sub

#End Region

End Class