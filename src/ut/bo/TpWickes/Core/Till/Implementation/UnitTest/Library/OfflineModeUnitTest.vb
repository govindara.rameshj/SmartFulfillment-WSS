﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class OfflineModeUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> _
    Public Sub FunctionIsInOfflineMode_RepositoryIsInOnlineModeIsFalse_ReturnsFalse()
        Dim X As New OfflineMode

        ConfigureIOfflineModeRepository(X, False)
        Assert.AreEqual(X.IsInOfflineMode, False)
    End Sub

    <TestMethod()> _
    Public Sub FunctionIsInOfflineMode_RepositoryIsInOnlineModeIsTrue_ReturnsTrue()
        Dim X As New OfflineMode

        ConfigureIOfflineModeRepository(X, True)
        Assert.AreEqual(X.IsInOfflineMode, True)
    End Sub

    Private Sub ConfigureIOfflineModeRepository(ByRef X As OfflineMode, ByVal IsInOfflineMode As Boolean)
        Dim Mock As New MockRepository
        Dim Repository As IOfflineModeRepository

        'arrange
        Repository = Mock.Stub(Of IOfflineModeRepository)()
        SetupResult.On(Repository).Call(Repository.IsInOfflineMode()).Return(IsInOfflineMode).IgnoreArguments()
        OfflineModeRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()
    End Sub
End Class
