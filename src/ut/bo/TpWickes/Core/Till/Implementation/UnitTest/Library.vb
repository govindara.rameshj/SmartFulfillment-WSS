﻿<HideModuleName()> Module Library


#Region "Private Procedures And Functions"

    Friend Function DTCouponTextConfigureStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns
            .Add("COUPONID", System.Type.GetType("System.String"))
            .Add("SEQUENCENO", System.Type.GetType("System.String"))
            .Add("PRINTSIZE", System.Type.GetType("System.String"))
            .Add("TEXTALIGN", System.Type.GetType("System.String"))
            .Add("PRINTTEXT", System.Type.GetType("System.String"))
            .Add("DELETED", System.Type.GetType("System.Boolean"))
        End With

        Return DT

    End Function

    Friend Sub DTCouponTextAddData(ByRef DT As DataTable, _
                                          ByVal CouponID As String, ByVal SequenceNo As String, ByVal Printsize As String, _
                                          ByVal TextAlign As String, ByVal PrintText As String, ByVal Deleted As Boolean)

        DT.Rows.Add(CouponID, SequenceNo, Printsize, TextAlign, PrintText, Deleted)

    End Sub

#End Region

End Module
