﻿<TestClass()> Public Class TextCommunicationUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : TextCommunicationLine | Public Property (ReadOnly) : GetFullText"


    <TestMethod()> Public Sub PropertyGetFullText_InitialValues_FullTextNull_LineDelimiterNull_ParagraphDelimiterNull_ReturnEmptyString()

        Dim X As New TextCommunicationLine

        X._FullText = String.Empty
        X._LineDelimiter = Nothing
        X._ParagraphDelimiter = Nothing

        Assert.AreEqual(String.Empty, X.GetFullText())

    End Sub

    <TestMethod()> Public Sub PropertyGetFullText_InitialValues_FullTextNotNull_LineDelimiterNull_ParagraphDelimiterNull_ReturnFullText()

        Dim X As New TextCommunicationLine

        X._FullText = "abcdefghijklmnopqrstuvwxyz"
        X._LineDelimiter = Nothing
        X._ParagraphDelimiter = Nothing

        Assert.AreEqual("abcdefghijklmnopqrstuvwxyz", X.GetFullText())

    End Sub

    <TestMethod()> Public Sub PropertyGetFullText_InitialValues_FullTextNotNull_LineDelimiterNotNull_ParagraphDelimiterNotNull_ReturnFullText()

        Dim X As New TextCommunicationLine

        X._FullText = "abcdefghijklmnopqrstuvwxyz"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("abcdefghijklmnopqrstuvwxyz", X.GetFullText())

    End Sub

    <TestMethod()> Public Sub PropertyGetFullText_InitialValues_FullTextNotNull_LineDelimiterNotNull_ParagraphDelimiterNotNull_ReturnFullTextLineDelimiterReplaced()

        Dim X As New TextCommunicationLine

        X._FullText = "abcde!fghijklmnopqrstuvwxyz"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("abcde  fghijklmnopqrstuvwxyz", X.GetFullText())

    End Sub

    <TestMethod()> Public Sub PropertyGetFullText_InitialValues_FullTextNotNull_LineDelimiterNotNull_ParagraphDelimiterNotNull_ReturnFullTextParagraphDelimiterReplaced()

        Dim X As New TextCommunicationLine

        X._FullText = "abcde|fghijklmnopqrstuvwxyz"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("abcde" & vbNewLine & "fghijklmnopqrstuvwxyz", X.GetFullText())

    End Sub

    <TestMethod()> Public Sub PropertyGetFullText_InitialValues_FullTextNotNull_LineDelimiterNotNull_ParagraphDelimiterNotNull_ReturnFullTextLineAndParagraphDelimiterReplaced()

        Dim X As New TextCommunicationLine

        X._FullText = "abcde!fghijk|lmnopqrstuvwxyz"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("abcde  fghijk" & vbNewLine & "lmnopqrstuvwxyz", X.GetFullText())

    End Sub

    <TestMethod()> Public Sub PropertyGetFullText_ComplexInitialValues_ReturnCorrectOutput()

        Dim X As New TextCommunicationLine

        X._FullText = "ParagraphOne LineOne!ParagraphOne LineTwo|ParagraphTwo LineOne!ParagraphTwo LineTwo"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("ParagraphOne LineOne  ParagraphOne LineTwo" & vbNewLine & "ParagraphTwo LineOne  ParagraphTwo LineTwo", X.GetFullText())

    End Sub

#End Region

#Region "Class : TextCommunicationLine | Public Property (ReadOnly) : GetSummaryText"

    <TestMethod()> Public Sub PropertyGetSummaryText_InitialValues_SummaryTextNull_LineDelimiterNull_ParagraphDelimiterNull_ReturnEmptyString()

        Dim X As New TextCommunicationLine

        X._SummaryText = Nothing
        X._LineDelimiter = Nothing
        X._ParagraphDelimiter = Nothing

        Assert.AreEqual(String.Empty, X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub PropertyGetSummaryText_InitialValues_SummaryTextNotNull_LineDelimiterNull_ParagraphDelimiterNull_ReturnSummaryText()

        Dim X As New TextCommunicationLine

        X._SummaryText = "abcdefghijklmnopqrstuvwxyz"
        X._LineDelimiter = Nothing
        X._ParagraphDelimiter = Nothing

        Assert.AreEqual("abcdefghijklmnopqrstuvwxyz", X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub PropertyGetSummaryText_InitialValues_SummaryTextNotNull_LineDelimiterNotNull_ParagraphDelimiterNotNull_ReturnSummaryText()

        Dim X As New TextCommunicationLine

        X._SummaryText = "abcdefghijklmnopqrstuvwxyz"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("abcdefghijklmnopqrstuvwxyz", X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub PropertyGetSummaryText_InitialValues_SummaryTextNotNull_LineDelimiterNotNull_ParagraphDelimiterNotNull_ReturnSummaryTextLineDelimiterReplaced()

        Dim X As New TextCommunicationLine

        X._SummaryText = "abcde!fghijklmnopqrstuvwxyz"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("abcde  fghijklmnopqrstuvwxyz", X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub PropertyGetSummaryText_InitialValues_SummaryTextNotNull_LineDelimiterNotNull_ParagraphDelimiterNotNull_ReturnSummaryTextParagraphDelimiterReplaced()

        Dim X As New TextCommunicationLine

        X._SummaryText = "abcde|fghijklmnopqrstuvwxyz"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("abcde" & vbNewLine & "fghijklmnopqrstuvwxyz", X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub PropertyGetSummaryText_InitialValues_SummaryTextNotNull_LineDelimiterNotNull_ParagraphDelimiterNotNull_ReturnSummaryTextLineAndParagraphDelimiterReplaced()

        Dim X As New TextCommunicationLine

        X._SummaryText = "abcde!fghijk|lmnopqrstuvwxyz"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("abcde  fghijk" & vbNewLine & "lmnopqrstuvwxyz", X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub PropertyGetSummaryText_ComplexInitialValues_ReturnCorrectOutput()

        Dim X As New TextCommunicationLine

        X._SummaryText = "ParagraphOne LineOne!ParagraphOne LineTwo|ParagraphTwo LineOne!ParagraphTwo LineTwo"
        X._LineDelimiter = "!"
        X._ParagraphDelimiter = "|"

        Assert.AreEqual("ParagraphOne LineOne  ParagraphOne LineTwo" & vbNewLine & "ParagraphTwo LineOne  ParagraphTwo LineTwo", X.GetSummaryText())

    End Sub

#End Region

#Region "Class : TextCommunicationLine(s) | Public Function : Initialise"

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDNothing_TypeIDNothing_ExpectInternalFunctionalIDValueZero()

        Dim X As New TextCommunicationLines

        X.Initialise(Nothing, Nothing)

        Assert.AreEqual(0, X._FunctionalAreaID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDNothing_TypeIDNothing_ExpectInternalTypeIDValueZero()

        Dim X As New TextCommunicationLines

        X.Initialise(Nothing, Nothing)

        Assert.AreEqual(0, X._TypeID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDNothing_TypeIDNothing_ReturnValueFalse()

        Dim X As New TextCommunicationLines

        Assert.IsFalse(X.Initialise(Nothing, Nothing))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDTen_TypeIDNothing_ExpectInternalFunctionalIDValueZero()

        Dim X As New TextCommunicationLines

        X.Initialise(10, Nothing)

        Assert.AreEqual(0, X._FunctionalAreaID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDTen_TypeIDNothing_ReturnValueFalse()

        Dim X As New TextCommunicationLines

        Assert.IsFalse(X.Initialise(10, Nothing))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDNothing_TypeIDTen_ExpectInternalTypeIDValueZero()

        Dim X As New TextCommunicationLines

        X.Initialise(Nothing, 10)

        Assert.AreEqual(0, X._TypeID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDNothing_TypeIDTen_ReturnValueFalse()

        Dim X As New TextCommunicationLines

        Assert.IsFalse(X.Initialise(Nothing, 10))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDTen_TypeIDTwenty_ExpectInternalFunctionalIDValueTen()

        Dim Mock As New MockRepository

        Dim X As New TextCommunicationLines
        Dim Repository As IGetTextCommunicationRepository
        Dim DT As DataTable

        Repository = Mock.Stub(Of IGetTextCommunicationRepository)()

        DT = Nothing
        SetupResult.On(Repository).Call(Repository.GetTextCommunications(Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetTextCommunicationRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        X.Initialise(10, 20)

        Assert.AreEqual(10, X._FunctionalAreaID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDTen_TypeIDTwenty_ExpectInternalTypeIDValueTwenty()

        Dim Mock As New MockRepository

        Dim X As New TextCommunicationLines
        Dim Repository As IGetTextCommunicationRepository
        Dim DT As DataTable

        Repository = Mock.Stub(Of IGetTextCommunicationRepository)()

        DT = Nothing
        SetupResult.On(Repository).Call(Repository.GetTextCommunications(Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetTextCommunicationRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        X.Initialise(10, 20)

        Assert.AreEqual(20, X._TypeID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_FunctionalIDTen_TypeIDTwenty_ReturnValueTrue()

        Dim Mock As New MockRepository

        Dim X As New TextCommunicationLines
        Dim Repository As IGetTextCommunicationRepository
        Dim DT As DataTable

        Repository = Mock.Stub(Of IGetTextCommunicationRepository)()

        DT = Nothing
        SetupResult.On(Repository).Call(Repository.GetTextCommunications(Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetTextCommunicationRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        Assert.IsTrue(X.Initialise(10, 20))

    End Sub

#End Region

#Region "Class : TextCommunicationLine(s) | Public Function GetFullText"

    <TestMethod()> Public Sub FunctionGetFullText_InitialValue_NoTextLine_ReturnEmptyString()

        Dim X As New TextCommunicationLines

        Assert.AreEqual(String.Empty, X.GetFullText())

    End Sub

    <TestMethod()> Public Sub FunctionGetFullText_InitialValues_OneTextLine_ReturnOneTextLine()

        Dim X As New TextCommunicationLines

        X.Add(ConfigureTextCommunicationLine("a", Nothing, Nothing, Nothing))

        Assert.AreEqual("a", X.GetFullText())

    End Sub

    <TestMethod()> Public Sub FunctionGetFullText_InitialValues_TwoTextLine_ReturnTwoTextLineVersionOne()

        Dim X As New TextCommunicationLines

        X.Add(ConfigureTextCommunicationLine("a", Nothing, Nothing, Nothing))
        X.Add(ConfigureTextCommunicationLine("b", Nothing, Nothing, Nothing))

        Assert.AreEqual("a" & vbNewLine & vbNewLine & "b", X.GetFullText())

    End Sub

    <TestMethod()> Public Sub FunctionGetFullText_InitialValues_TwoTextLine_ReturnTwoTextLineVersionTwo()

        Dim X As New TextCommunicationLines

        X.Add(ConfigureTextCommunicationLine("", Nothing, Nothing, Nothing))
        X.Add(ConfigureTextCommunicationLine("b", Nothing, Nothing, Nothing))

        Assert.AreEqual("b", X.GetFullText())

    End Sub

    <TestMethod()> Public Sub FunctionGetFullText_InitialValues_TwoTextLine_ReturnTwoTextLineVersionThree()

        Dim X As New TextCommunicationLines

        X.Add(ConfigureTextCommunicationLine("a", Nothing, Nothing, Nothing))
        X.Add(ConfigureTextCommunicationLine("", Nothing, Nothing, Nothing))

        Assert.AreEqual("a", X.GetFullText())

    End Sub

#End Region

#Region "Class : TextCommunicationLine(s) | Public Function GetSummaryText"

    <TestMethod()> Public Sub FunctionGetSummaryText_InitialValues_NoTextLines_ReturnEmptyString()

        Dim X As New TextCommunicationLines

        Assert.AreEqual(String.Empty, X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub FunctionGetSummaryText_InitialValues_OneTextLine_ReturnOneTextLine()

        Dim X As New TextCommunicationLines

        X.Add(ConfigureTextCommunicationLine(Nothing, "a", Nothing, Nothing))

        Assert.AreEqual("a", X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub FunctionGetSummaryText_InitialValues_TwoTextLine_ReturnTwoTextLineVersionOne()

        Dim X As New TextCommunicationLines

        X.Add(ConfigureTextCommunicationLine(Nothing, "a", Nothing, Nothing))
        X.Add(ConfigureTextCommunicationLine(Nothing, "b", Nothing, Nothing))

        Assert.AreEqual("a" & vbNewLine & vbNewLine & "b", X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub FunctionGetSummaryText_InitialValues_TwoTextLine_ReturnTwoTextLineVersionTwo()

        Dim X As New TextCommunicationLines

        X.Add(ConfigureTextCommunicationLine(Nothing, "", Nothing, Nothing))
        X.Add(ConfigureTextCommunicationLine(Nothing, "b", Nothing, Nothing))

        Assert.AreEqual("b", X.GetSummaryText())

    End Sub

    <TestMethod()> Public Sub FunctionGetSummaryText_InitialValues_TwoTextLine_ReturnTwoTextLineVersionThree()

        Dim X As New TextCommunicationLines

        X.Add(ConfigureTextCommunicationLine(Nothing, "a", Nothing, Nothing))
        X.Add(ConfigureTextCommunicationLine(Nothing, "", Nothing, Nothing))

        Assert.AreEqual("a", X.GetSummaryText())

    End Sub

#End Region

#Region "Class : TextCommunicationLine(s) | Private Procedure : Load - FunctionalAreaID, TypeID"

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DataTableNothing_ReturnValue_CollectionCountZero()

        Dim X As New TextCommunicationLines

        ConfigureTextCommunicationRepository(X, Nothing)

        X.Load(Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DataTableNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New TextCommunicationLines

        ConfigureTextCommunicationRepository(X, Nothing)

        X.Load(Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DataTableEmpty_ReturnValue_CollectionCountZero()

        Dim X As New TextCommunicationLines

        ConfigureTextCommunicationRepository(X, TextCommunicationDataTableConfigureStructure)

        X.Load(Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DataTableEmpty_ReturnValue_ExistInDatabaseFalse()

        Dim X As New TextCommunicationLines

        ConfigureTextCommunicationRepository(X, TextCommunicationDataTableConfigureStructure)

        X.Load(Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DataTableOneRecord_ReturnValue_CollectionCountOne()

        Dim X As New TextCommunicationLines
        Dim DT As DataTable

        DT = TextCommunicationDataTableConfigureStructure()
        TextCommunicationDataTableAddData(DT, 1, 1, 1, 1, "Description", "SummaryText", "FullText", "LineDelimiter", "ParagraphDelimiter", "FieldStartDelimiter", "FieldEndDelimiter", False)
        ConfigureTextCommunicationRepository(X, DT)

        X.Load(Nothing, Nothing)

        Assert.AreEqual(1, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DataTableOneRecord_ReturnValue_ExistInDatabaseTrue()

        Dim X As New TextCommunicationLines
        Dim DT As DataTable

        DT = TextCommunicationDataTableConfigureStructure()
        TextCommunicationDataTableAddData(DT, 1, 1, 1, 1, "Description", "SummaryText", "FullText", "LineDelimiter", "ParagraphDelimiter", "FieldStartDelimiter", "FieldEndDelimiter", False)
        ConfigureTextCommunicationRepository(X, DT)

        X.Load(Nothing, Nothing)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_BusinessObjectLoadsDataTableCorrectly()

        Dim X As New TextCommunicationLines
        Dim DT As DataTable

        DT = TextCommunicationDataTableConfigureStructure()
        TextCommunicationDataTableAddData(DT, 1, 2, 3, 4, "Description A", "SummaryText B", "FullText C", "LineDelimiter D", "ParagraphDelimiter E", "FieldStartDelimiter F", "FieldEndDelimiter G", False)
        TextCommunicationDataTableAddData(DT, 5, 6, 7, 8, "Description H", "SummaryText I", "FullText J", "LineDelimiter K", "ParagraphDelimiter L", "FieldStartDelimiter M", "FieldEndDelimiter N", True)
        ConfigureTextCommunicationRepository(X, DT)

        X.Load(Nothing, Nothing)

        Assert.IsTrue(MatchBusinessObjectAgainstDataTable(X, DT))

    End Sub

#End Region

#Region "Class : TextCommunicationLine(s) | Private Procedure : Load - FunctionalAreaID, TypeID, SequenceNumber"

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableNothing_ReturnValue_CollectionCountZero()

        Dim X As New TextCommunicationLines

        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, Nothing)

        X.Load(Nothing, Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New TextCommunicationLines

        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, Nothing)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreParameter_DataTableEmpty_ReturnValue_CollectionCountZero()

        Dim X As New TextCommunicationLines

        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, TextCommunicationDataTableConfigureStructure)

        X.Load(Nothing, Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableEmpty_ReturnValue_ExistInDatabaseFalse()

        Dim X As New TextCommunicationLines

        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, TextCommunicationDataTableConfigureStructure)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableOneRecord_ReturnValue_CollectionCountOne()

        Dim X As New TextCommunicationLines
        Dim DT As DataTable

        DT = TextCommunicationDataTableConfigureStructure()
        TextCommunicationDataTableAddData(DT, 1, 1, 1, 1, "Description", "SummaryText", "FullText", "LineDelimiter", "ParagraphDelimiter", "FieldStartDelimiter", "FieldEndDelimiter", False)
        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.AreEqual(1, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableOneRecord_ReturnValue_ExistInDatabaseTrue()

        Dim X As New TextCommunicationLines
        Dim DT As DataTable

        DT = TextCommunicationDataTableConfigureStructure()
        TextCommunicationDataTableAddData(DT, 1, 1, 1, 1, "Description", "SummaryText", "FullText", "LineDelimiter", "ParagraphDelimiter", "FieldStartDelimiter", "FieldEndDelimiter", False)
        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableTwoRecord_ReturnValue_CollectionCountZero()

        Dim X As New TextCommunicationLines
        Dim DT As DataTable

        DT = TextCommunicationDataTableConfigureStructure()
        TextCommunicationDataTableAddData(DT, 1, 2, 3, 4, "Description A", "SummaryText B", "FullText C", "LineDelimiter D", "ParagraphDelimiter E", "FieldStartDelimiter F", "FieldEndDelimiter G", False)
        TextCommunicationDataTableAddData(DT, 5, 6, 7, 8, "Description H", "SummaryText I", "FullText J", "LineDelimiter K", "ParagraphDelimiter L", "FieldStartDelimiter M", "FieldEndDelimiter N", True)
        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableToRecord_ReturnValue_ExistInDatabaseFalse()

        Dim X As New TextCommunicationLines
        Dim DT As DataTable

        DT = TextCommunicationDataTableConfigureStructure()
        TextCommunicationDataTableAddData(DT, 1, 2, 3, 4, "Description A", "SummaryText B", "FullText C", "LineDelimiter D", "ParagraphDelimiter E", "FieldStartDelimiter F", "FieldEndDelimiter G", False)
        TextCommunicationDataTableAddData(DT, 5, 6, 7, 8, "Description H", "SummaryText I", "FullText J", "LineDelimiter K", "ParagraphDelimiter L", "FieldStartDelimiter M", "FieldEndDelimiter N", True)
        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_BusinessObjectLoadsDataTableCorrectly()

        Dim X As New TextCommunicationLines
        Dim DT As DataTable

        DT = TextCommunicationDataTableConfigureStructure()
        TextCommunicationDataTableAddData(DT, 1, 2, 3, 4, "Description A", "SummaryText B", "FullText C", "LineDelimiter D", "ParagraphDelimiter E", "FieldStartDelimiter F", "FieldEndDelimiter G", False)
        ConfigureTextCommunicationRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsTrue(MatchBusinessObjectAgainstDataTable(X, DT))

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Private Sub ConfigureTextCommunicationRepository(ByRef X As TextCommunicationLines, ByRef DT As DataTable)

        Dim Mock As New MockRepository
        Dim Repository As IGetTextCommunicationRepository

        'arrange
        Repository = Mock.Stub(Of IGetTextCommunicationRepository)()
        SetupResult.On(Repository).Call(Repository.GetTextCommunications(Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetTextCommunicationRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Sub ConfigureTextCommunicationRepositoryIncludeSequenceNumber(ByRef X As TextCommunicationLines, ByRef DT As DataTable)

        Dim Mock As New MockRepository
        Dim Repository As IGetTextCommunicationRepository

        'arrange
        Repository = Mock.Stub(Of IGetTextCommunicationRepository)()
        SetupResult.On(Repository).Call(Repository.GetTextCommunications(Nothing, Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetTextCommunicationRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Friend Function TextCommunicationDataTableConfigureStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns
            .Add("Id", System.Type.GetType("System.Int16"))
            .Add("FunctionalAreaId", System.Type.GetType("System.Int16"))
            .Add("TypeId", System.Type.GetType("System.Int16"))
            .Add("Sequence", System.Type.GetType("System.Int16"))
            .Add("Description", System.Type.GetType("System.String"))
            .Add("SummaryText", System.Type.GetType("System.String"))
            .Add("FullText", System.Type.GetType("System.String"))
            .Add("LineDelimiter", System.Type.GetType("System.String"))
            .Add("ParagraphDelimiter", System.Type.GetType("System.String"))
            .Add("FieldStartDelimiter", System.Type.GetType("System.String"))
            .Add("FieldEndDelimiter", System.Type.GetType("System.String"))
            .Add("Deleted", System.Type.GetType("System.Boolean"))
        End With

        Return DT

    End Function

    Friend Sub TextCommunicationDataTableAddData(ByRef DT As DataTable, _
                                                 ByVal ID As Integer, ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer, _
                                                 ByVal Sequence As Integer, ByVal Description As String, ByVal SummaryText As String, _
                                                 ByVal FullText As String, ByVal LineDelimiter As String, ByVal ParagraphDelimiter As String, _
                                                 ByVal FieldStartDelimiter As String, ByVal FieldEndDelimiter As String, _
                                                 ByVal Deleted As Boolean)

        DT.Rows.Add(ID, FunctionalAreaID, TypeID, Sequence, Description, SummaryText, FullText, LineDelimiter, _
                    ParagraphDelimiter, FieldStartDelimiter, FieldEndDelimiter, Deleted)

    End Sub

    Private Function MatchBusinessObjectAgainstDataTable(ByRef BO As TextCommunicationLines, ByRef DT As DataTable) As Boolean

        'assume that business object & datatable sorting order are consistent

        Dim RowIndex As Integer

        Try
            RowIndex = 0
            For Each X As TextCommunicationLine In BO

                If X.ID <> CType(DT.Rows.Item(RowIndex).Item(0), Integer) Then Return False
                If X.FunctionalAreaID <> CType(DT.Rows.Item(RowIndex).Item(1), Integer) Then Return False
                If X.TypeID <> CType(DT.Rows.Item(RowIndex).Item(2), Integer) Then Return False
                If X.SequenceNumber <> CType(DT.Rows.Item(RowIndex).Item(3), Integer) Then Return False
                If X.Description <> CType(DT.Rows.Item(RowIndex).Item(4), String) Then Return False
                If X.SummaryText <> CType(DT.Rows.Item(RowIndex).Item(5), String) Then Return False
                If X.FullText <> CType(DT.Rows.Item(RowIndex).Item(6), String) Then Return False
                If X.LineDelimiter <> CType(DT.Rows.Item(RowIndex).Item(7), String) Then Return False
                If X.ParagraphDelimiter <> CType(DT.Rows.Item(RowIndex).Item(8), String) Then Return False
                If X.FieldStartDelimiter <> CType(DT.Rows.Item(RowIndex).Item(9), String) Then Return False
                If X.FieldEndDelimiter <> CType(DT.Rows.Item(RowIndex).Item(10), String) Then Return False
                If X.Deleted <> CType(DT.Rows.Item(RowIndex).Item(11), Boolean) Then Return False

                RowIndex += 1
            Next

        Catch ex As Exception

            Return False

        End Try

        Return True

    End Function

    Private Function ConfigureTextCommunicationLine(ByVal FullText As String, ByVal SummaryText As String, ByVal LineDelimiter As String, ByVal ParagraphDelimiter As String) As TextCommunicationLine

        Dim X As New TextCommunicationLine

        With X

            X._FullText = FullText
            X._SummaryText = SummaryText
            X._LineDelimiter = LineDelimiter
            X._ParagraphDelimiter = ParagraphDelimiter

        End With

        Return X

    End Function

#End Region

End Class