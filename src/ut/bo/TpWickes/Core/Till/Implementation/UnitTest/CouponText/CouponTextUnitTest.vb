﻿<TestClass()> Public Class CouponTextUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : CouponTextLine | Public Property : SequenceNo"

    <TestMethod()> Public Sub PropertySequenceNo_InitialValue_Ten_ReturnValue_Ten()

        Dim X As New CouponTextLine

        X.SequenceNumber = "10"

        Assert.AreEqual("10", X.SequenceNumber)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateSequenceNo_InitialValue_Nothing_ReturnValue_MinusOne()

        Dim X As New CouponTextLine

        Assert.AreEqual("-1", X.SequenceNumber)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateSequenceNo_InitialValue_Empty_ReturnValue_MinusOne()

        Dim X As New CouponTextLine

        X._SEQUENCENO = String.Empty

        Assert.AreEqual("-1", X.SequenceNumber)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateSequenceNo_InitialValue_Blank_ReturnValue_MinusOne()

        Dim X As New CouponTextLine

        X._SEQUENCENO = Space(1)

        Assert.AreEqual("-1", X.SequenceNumber)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateSequenceNo_InitialValue_MultipleBlank_ReturnValue_MinusOne()

        Dim X As New CouponTextLine

        X._SEQUENCENO = Space(5)

        Assert.AreEqual("-1", X.SequenceNumber)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateSequenceNo_InitialValue_NonInteger_ReturnValue_MinusOne()

        Dim X As New CouponTextLine

        X._SEQUENCENO = "XXX"

        Assert.AreEqual("-1", X.SequenceNumber)

    End Sub

#End Region

#Region "Class : CouponTextLine | Public Property : PrintSize"

    <TestMethod()> Public Sub PropertyPrintSize_InitialValue_Normal_ReturnValue_CouponTextPrintSizeNormal()

        Dim X As New CouponTextLine

        X.PrintSize = CouponTextPrintSize.Normal

        Assert.AreEqual(CouponTextPrintSize.Normal, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub PropertyPrintSize_InitialValue_Small_ReturnValue_CouponTextPrintSizeSmall()

        Dim X As New CouponTextLine

        X.PrintSize = CouponTextPrintSize.Small

        Assert.AreEqual(CouponTextPrintSize.Small, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub PropertyPrintSize_InitialValue_Medium_ReturnValue_CouponTextPrintSizeMedium()

        Dim X As New CouponTextLine

        X.PrintSize = CouponTextPrintSize.Medium

        Assert.AreEqual(CouponTextPrintSize.Medium, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub PropertyPrintSize_InitialValue_Large_ReturnValue_CouponTextPrintSizeLarge()

        Dim X As New CouponTextLine

        X.PrintSize = CouponTextPrintSize.Large

        Assert.AreEqual(CouponTextPrintSize.Large, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub PropertyPrintSize_InitialValue_Huge_ReturnValue_CouponTextPrintSizeHuge()

        Dim X As New CouponTextLine

        X.PrintSize = CouponTextPrintSize.Huge

        Assert.AreEqual(CouponTextPrintSize.Huge, X.PrintSize)

    End Sub




    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_Nothing_ReturnValue_CouponTextPrintSizeNormal()

        Dim X As New CouponTextLine

        Assert.AreEqual(CouponTextPrintSize.Normal, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_Empty_ReturnValue_CouponTextPrintSizeNormal()

        Dim X As New CouponTextLine

        X._PRINTSIZE = String.Empty

        Assert.AreEqual(CouponTextPrintSize.Normal, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_Blank_ReturnValue_CouponTextPrintSizeNormal()

        Dim X As New CouponTextLine

        X._PRINTSIZE = Space(1)

        Assert.AreEqual(CouponTextPrintSize.Normal, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_MultipleBlank_ReturnValue_CouponTextPrintSizeNormal()

        Dim X As New CouponTextLine

        X._PRINTSIZE = Space(5)

        Assert.AreEqual(CouponTextPrintSize.Normal, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_IncorrectLength_ReturnValue_CouponTextPrintSizeNormal()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "XXXXX"

        Assert.AreEqual(CouponTextPrintSize.Normal, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_LowerCaseNormal_ReturnValue_CouponTextPrintSizeNormal()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "n"

        Assert.AreEqual(CouponTextPrintSize.Normal, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_UpperCaseNormal_ReturnValue_CouponTextPrintSizeNormal()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "N"

        Assert.AreEqual(CouponTextPrintSize.Normal, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_LowerCaseSmall_ReturnValue_CouponTextPrintSizeSmall()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "s"

        Assert.AreEqual(CouponTextPrintSize.Small, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_UpperCaseSmall_ReturnValue_CouponTextPrintSizeSmall()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "S"

        Assert.AreEqual(CouponTextPrintSize.Small, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_LowerCaseMedium_ReturnValue_CouponTextPrintSizeMedium()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "m"

        Assert.AreEqual(CouponTextPrintSize.Medium, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_UpperCaseMedium_ReturnValue_CouponTextPrintSizeMedium()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "M"

        Assert.AreEqual(CouponTextPrintSize.Medium, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_LowerCaseLarge_ReturnValue_CouponTextPrintSizeLarge()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "l"

        Assert.AreEqual(CouponTextPrintSize.Large, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_UpperCaseLarge_ReturnValue_CouponTextPrintSizeLarge()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "L"

        Assert.AreEqual(CouponTextPrintSize.Large, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_LowerCaseHuge_ReturnValue_CouponTextPrintSizeHuge()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "h"

        Assert.AreEqual(CouponTextPrintSize.Huge, X.PrintSize)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintSize_InitialValue_UpperCaseHuge_ReturnValue_CouponTextPrintSizeHuge()

        Dim X As New CouponTextLine

        X._PRINTSIZE = "H"

        Assert.AreEqual(CouponTextPrintSize.Huge, X.PrintSize)

    End Sub

#End Region

#Region "Class : CouponTextLine | Public Property : TextAlign"

    <TestMethod()> Public Sub PropertyTextAlign_InitialValue_Left_ReturnValue_CouponTextAlignmentLeft()

        Dim X As New CouponTextLine

        X.TextAlignment = CouponTextAlignment.Left

        Assert.AreEqual(CouponTextAlignment.Left, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub PropertyTextAlign_InitialValue_Centre_ReturnValue_CouponTextAlignmentCentre()

        Dim X As New CouponTextLine

        X.TextAlignment = CouponTextAlignment.Centre

        Assert.AreEqual(CouponTextAlignment.Centre, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub PropertyTextAlign_InitialValue_Right_ReturnValue_CouponTextAlignmentRight()

        Dim X As New CouponTextLine

        X.TextAlignment = CouponTextAlignment.Right

        Assert.AreEqual(CouponTextAlignment.Right, X.TextAlignment)

    End Sub




    <TestMethod()> Public Sub PropertyTextAlign_InitialValue_Nothing_ReturnValue_CouponTextAlignmentLeft()

        Dim X As New CouponTextLine

        Assert.AreEqual(CouponTextAlignment.Left, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub PropertyTextAlign_InitialValue_Empty_ReturnValue_CouponTextAlignmentLeft()

        Dim X As New CouponTextLine

        X._TEXTALIGN = String.Empty

        Assert.AreEqual(CouponTextAlignment.Left, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub PropertyTextAlign_InitialValue_Blank_ReturnValue_CouponTextAlignmentLeft()

        Dim X As New CouponTextLine

        X._TEXTALIGN = Space(1)

        Assert.AreEqual(CouponTextAlignment.Left, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub PropertyTextAlign_InitialValue_MultipleBlank_ReturnValue_CouponTextAlignmentLeft()

        Dim X As New CouponTextLine

        X._TEXTALIGN = Space(5)

        Assert.AreEqual(CouponTextAlignment.Left, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub PropertyTextAlign_InitialValue_IncorrectLength_ReturnValue_CouponTextAlignmentLeft()

        Dim X As New CouponTextLine

        X._TEXTALIGN = "XXXXX"

        Assert.AreEqual(CouponTextAlignment.Left, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateTextAlign_InitialValue_LowerCaseLeft_ReturnValue_CouponTextAlignmentLeft()

        Dim X As New CouponTextLine

        X._TEXTALIGN = "l"

        Assert.AreEqual(CouponTextAlignment.Left, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateTextAlign_InitialValue_UpperCaseLeft_ReturnValue_CouponTextAlignmentLeft()

        Dim X As New CouponTextLine

        X._TEXTALIGN = "L"

        Assert.AreEqual(CouponTextAlignment.Left, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateTextAlign_InitialValue_LowerCaseCentre_ReturnValue_CouponTextAlignmentCentre()

        Dim X As New CouponTextLine

        X._TEXTALIGN = "c"

        Assert.AreEqual(CouponTextAlignment.Centre, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateTextAlign_InitialValue_UpperCaseCentre_ReturnValue_CouponTextAlignmentCentre()

        Dim X As New CouponTextLine

        X._TEXTALIGN = "C"

        Assert.AreEqual(CouponTextAlignment.Centre, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateTextAlign_InitialValue_LowerCaseRight_ReturnValue_CouponTextAlignmentRight()

        Dim X As New CouponTextLine

        X._TEXTALIGN = "r"

        Assert.AreEqual(CouponTextAlignment.Right, X.TextAlignment)

    End Sub

    <TestMethod()> Public Sub DirectlyPopulateTextAlign_InitialValue_UpperCaseRight_ReturnValue_CouponTextAlignmentRight()

        Dim X As New CouponTextLine

        X._TEXTALIGN = "R"

        Assert.AreEqual(CouponTextAlignment.Right, X.TextAlignment)

    End Sub

#End Region

#Region "Class : CouponTextLine | Public Property (ReadOnly) : PrintTextSubstring"

    <TestMethod()> Public Sub DirectlyPopulatePrintTextSubstring_InitialValue_ABCDE_StringLengthNothing_ReturnValue_ABCDE()

        Dim X As New CouponTextLine

        X._PRINTTEXT = "ABCDE"

        Assert.AreEqual("ABCDE", X.PrintTextSubstring())

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintTextSubstring_InitialValue_ABCDE_StringLengthMinusTwo_ReturnValue_ABCDE()

        Dim X As New CouponTextLine

        X._PRINTTEXT = "ABCDE"

        Assert.AreEqual("ABCDE", X.PrintTextSubstring(-2))

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintTextSubstring_InitialValue_ABCDE_StrngLengthMinusOne_ReturnValue_ABCDE()

        Dim X As New CouponTextLine

        X._PRINTTEXT = "ABCDE"

        Assert.AreEqual("ABCDE", X.PrintTextSubstring(-1))

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintTextSubstring_InitialValue_ABCDE_StringLengthZero_ReturnValue_Empty()

        Dim X As New CouponTextLine

        X._PRINTTEXT = "ABCDE"

        Assert.AreEqual("", X.PrintTextSubstring(0))

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintTextSubstring_InitialValue_ABCDE_StringLengthOne_ReturnValue_A()

        Dim X As New CouponTextLine

        X._PRINTTEXT = "ABCDE"

        Assert.AreEqual("A", X.PrintTextSubstring(1))

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintTextSubstring_InitialValue_ABCDE_StringLengthFive_ReturnValue_ABCDE()

        Dim X As New CouponTextLine

        X._PRINTTEXT = "ABCDE"

        Assert.AreEqual("ABCDE", X.PrintTextSubstring(5))

    End Sub

    <TestMethod()> Public Sub DirectlyPopulatePrintTextSubstring_InitialValue_ABCDE_StringLengthOutOfRangeTen_ReturnValue_ABCDE()

        Dim X As New CouponTextLine

        X._PRINTTEXT = "ABCDE"

        Assert.AreEqual("ABCDE", X.PrintTextSubstring(10))

    End Sub

#End Region

#Region "Class : CouponTextLine(s) | Public Function : Initialise"

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdNothing_DeletedTrue_ExpectInternalCouponIdValueNothing()

        Dim X As New CouponTextLines

        X.Initialise(Nothing, True)

        Assert.IsNull(X._CouponId)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdNothing_DeletedTrue_ReturnValueFalse()

        Dim X As New CouponTextLines

        Assert.IsFalse(X.Initialise(Nothing, True))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdEmpty_DeletedTrue_ExpectInternalCouponIdValueNothing()

        Dim X As New CouponTextLines

        X.Initialise(String.Empty, True)

        Assert.IsNull(X._CouponId)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdEmpty_DeletedTrue_ReturnValueFalse()

        Dim X As New CouponTextLines

        Assert.IsFalse(X.Initialise(String.Empty, True))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdXYZ_DeletedTrue_ExpectInternalCouponIdValueXYZ()

        Dim Mock As New MockRepository

        Dim X As New CouponTextLines
        Dim Repository As IGetCouponTextRepository
        Dim DT As DataTable

        'arrange; stubbed out call to database
        Repository = Mock.Stub(Of IGetCouponTextRepository)()

        DT = Nothing
        SetupResult.On(Repository).Call(Repository.GetCouponTextIncludeDeletedFalse("XYZ")).Return(DT).IgnoreArguments()
        GetCouponTextRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        X.Initialise("XYZ", True)

        Assert.AreEqual("XYZ", X._CouponId)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdXYZ_DeletedTrue_ReturnValueTrue()

        Dim Mock As New MockRepository

        Dim X As New CouponTextLines
        Dim Repository As IGetCouponTextRepository
        Dim DT As DataTable

        'arrange; stubbed out call to database
        Repository = Mock.Stub(Of IGetCouponTextRepository)()

        DT = Nothing
        SetupResult.On(Repository).Call(Repository.GetCouponTextIncludeDeletedFalse("XYZ")).Return(DT).IgnoreArguments()
        GetCouponTextRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        Assert.IsTrue(X.Initialise("XYZ", True))

    End Sub

#End Region

#Region "Class : CouponTextLine(s) | Public Function : FirstLine"

#End Region

#Region "Class : CouponTextLine(s) | Public Function : NextLine"

#End Region

#Region "Class: CouponTextLine(s) | Private Procedure : Load - CouponID, Deleted"

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedTrue_DataTableNothing_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, Nothing, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, True)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedTrue_DataTableNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, Nothing, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, True)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedTrue_DataTableEmpty_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        'arrange
        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructure, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, True)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedTrue_DataTableEmpty_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructure, DTCouponTextConfigureStructureAddSingleRecord)


        X.Load(Nothing, True)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedTrue_DataTableOneRecord_ReturnValue_CollectionCountOne()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, True)

        Assert.AreEqual(1, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedTrue_DataTableOneRecord_ReturnValue_ExistInDatabaseTrue()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, True)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedTrue_DataTableTwelveRecords_ReturnValue_CollectionCountTwelve()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructureAddTwelveRecords, Nothing)

        X.Load(Nothing, True)

        Assert.AreEqual(12, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedTrue_DataTableTwelveRecords_BusinessObjectLoadsDataTableCorrectly()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructureAddTwelveRecords, Nothing)

        X.Load(Nothing, True)

        Assert.IsTrue(MatchBusinessObjectAgainstDataTable(X, DTCouponTextConfigureStructureAddTwelveRecords))

    End Sub




    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedFalse_DataTableNothing_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, False)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedFalse_DataTableNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, False)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedFalse_DataTableEmpty_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructureAddSingleRecord, DTCouponTextConfigureStructure)

        X.Load(Nothing, False)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedFalse_DataTableEmpty_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, DTCouponTextConfigureStructureAddSingleRecord, DTCouponTextConfigureStructure)

        X.Load(Nothing, False)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedFalse_DataTableOneRecord_ReturnValue_CollectionCountOne()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, Nothing, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, False)

        Assert.AreEqual(1, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadTwoParameter_DeletedFalse_DataTableOneRecord_ReturnValue_ExistInDatabaseTrue()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepository(X, Nothing, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, False)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

#End Region

#Region "Class: CouponTextLine(s) | Private Procedure : Load - CouponID, SequenceNumber, Deleted"

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedTrue_DataTableNothing_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, Nothing, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, Nothing, True)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedTrue_DataTableNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, Nothing, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, Nothing, True)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedTrue_DataTableEmpty_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        'arrange
        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructure, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, Nothing, True)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedTrue_DataTableEmpty_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructure, DTCouponTextConfigureStructureAddSingleRecord)


        X.Load(Nothing, Nothing, True)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedTrue_DataTableOneRecord_ReturnValue_CollectionCountOne()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, Nothing, True)

        Assert.AreEqual(1, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedTrue_DataTableOneRecord_ReturnValue_ExistInDatabaseTrue()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, Nothing, True)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedTrue_DataTableTwoRecord_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddTwoRecords, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, Nothing, True)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedTrue_DataTableTwoRecord_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddTwoRecords, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, Nothing, True)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreParameter_DeletedTrue_DataTableOneRecord_BusinessObjectLoadsDataTableCorrectly()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, Nothing, True)

        Assert.IsTrue(MatchBusinessObjectAgainstDataTable(X, DTCouponTextConfigureStructureAddSingleRecord))

    End Sub




    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedFalse_DataTableNothing_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, Nothing, False)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedFalse_DataTableNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, Nothing)

        X.Load(Nothing, Nothing, False)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedFalse_DataTableEmpty_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        'arrange
        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, DTCouponTextConfigureStructure)

        X.Load(Nothing, Nothing, False)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedFalse_DataTableEmpty_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, DTCouponTextConfigureStructure)


        X.Load(Nothing, Nothing, False)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedFalse_DataTableOneRecord_ReturnValue_CollectionCountOne()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, Nothing, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, Nothing, False)

        Assert.AreEqual(1, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedFalse_DataTableOneRecord_ReturnValue_ExistInDatabaseTrue()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, Nothing, DTCouponTextConfigureStructureAddSingleRecord)

        X.Load(Nothing, Nothing, False)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedFalse_DataTableTwoRecord_ReturnValue_CollectionCountZero()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, DTCouponTextConfigureStructureAddTwoRecords)

        X.Load(Nothing, Nothing, False)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DeletedFalse_DataTableTwoRecord_ReturnValue_ExistInDatabaseFalse()

        Dim X As New CouponTextLines

        ConfigureCouponTextRepositoryIncludeSequenceNumber(X, DTCouponTextConfigureStructureAddSingleRecord, DTCouponTextConfigureStructureAddTwoRecords)

        X.Load(Nothing, Nothing, False)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Private Sub ConfigureCouponTextRepository(ByRef X As CouponTextLines, ByRef TrueDT As DataTable, ByRef FalseDT As DataTable)

        Dim Mock As New MockRepository
        Dim Repository As IGetCouponTextRepository

        Repository = Mock.Stub(Of IGetCouponTextRepository)()

        SetupResult.On(Repository).Call(Repository.GetCouponTextIncludeDeletedTrue(Nothing)).Return(TrueDT).IgnoreArguments()
        SetupResult.On(Repository).Call(Repository.GetCouponTextIncludeDeletedFalse(Nothing)).Return(FalseDT).IgnoreArguments()

        GetCouponTextRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Sub ConfigureCouponTextRepositoryIncludeSequenceNumber(ByRef X As CouponTextLines, ByRef TrueDT As DataTable, ByRef FalseDT As DataTable)

        Dim Mock As New MockRepository
        Dim Repository As IGetCouponTextRepository

        Repository = Mock.Stub(Of IGetCouponTextRepository)()

        SetupResult.On(Repository).Call(Repository.GetCouponTextIncludeDeletedTrue(Nothing, Nothing)).Return(TrueDT).IgnoreArguments()
        SetupResult.On(Repository).Call(Repository.GetCouponTextIncludeDeletedFalse(Nothing, Nothing)).Return(FalseDT).IgnoreArguments()

        GetCouponTextRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Function DTCouponTextConfigureStructureAddSingleRecord() As DataTable

        Dim DT As DataTable

        DT = DTCouponTextConfigureStructure()
        DT.Rows.Add("0000001", "1", "S", "L", "Dummy Entry", False)

        Return DT

    End Function

    Private Function DTCouponTextConfigureStructureAddTwoRecords() As DataTable

        Dim DT As DataTable

        DT = DTCouponTextConfigureStructure()
        DT.Rows.Add("0000001", "1", "S", "L", "Dummy Entry", False)
        DT.Rows.Add("0000001", "1", "S", "L", "Dummy Entry", False)

        Return DT

    End Function

    Private Function DTCouponTextConfigureStructureAddTwelveRecords() As DataTable

        Dim DT As DataTable

        DT = DTCouponTextConfigureStructure()
        DTCouponTextAddData(DT, "0000001", "1", "S", "L", "Dummy Entry", False)
        DTCouponTextAddData(DT, "0000001", "2", "S", "C", "Dummy Entry", False)
        DTCouponTextAddData(DT, "0000001", "3", "S", "R", "Dummy Entry", False)

        DTCouponTextAddData(DT, "0000001", "4", "M", "L", "Dummy Entry", False)
        DTCouponTextAddData(DT, "0000001", "5", "M", "C", "Dummy Entry", False)
        DTCouponTextAddData(DT, "0000001", "6", "M", "R", "Dummy Entry", False)

        DTCouponTextAddData(DT, "0000001", "7", "L", "L", "Dummy Entry", False)
        DTCouponTextAddData(DT, "0000001", "8", "L", "C", "Dummy Entry", False)
        DTCouponTextAddData(DT, "0000001", "9", "L", "R", "Dummy Entry", False)

        DTCouponTextAddData(DT, "0000001", "10", "N", "L", "Dummy Entry", False)
        DTCouponTextAddData(DT, "0000001", "11", "N", "C", "Dummy Entry", False)
        DTCouponTextAddData(DT, "0000001", "12", "N", "R", "Dummy Entry", False)

        Return DT

    End Function

    Private Function MatchBusinessObjectAgainstDataTable(ByRef BO As CouponTextLines, ByRef DT As DataTable) As Boolean

        'assume that business object & datatable sorting order are consistent

        Dim RowIndex As Integer
        Dim Temp As String

        Try
            RowIndex = 0
            For Each X As CouponTextLine In BO

                If X.CouponID <> CType(DT.Rows.Item(RowIndex).Item(0), String) Then Return False
                If X.SequenceNumber <> CType(DT.Rows.Item(RowIndex).Item(1), String) Then Return False

                Temp = String.Empty
                Select Case X.PrintSize
                    Case CouponTextPrintSize.Normal
                        Temp = "N"
                    Case CouponTextPrintSize.Small
                        Temp = "S"
                    Case CouponTextPrintSize.Medium
                        Temp = "M"
                    Case CouponTextPrintSize.Large
                        Temp = "L"
                End Select
                If Temp <> CType(DT.Rows.Item(RowIndex).Item(2), String) Then Return False

                Temp = String.Empty
                Select Case X.TextAlignment
                    Case CouponTextAlignment.Left
                        Temp = "L"
                    Case CouponTextAlignment.Centre
                        Temp = "C"
                    Case CouponTextAlignment.Right
                        Temp = "R"
                End Select
                If Temp <> CType(DT.Rows.Item(RowIndex).Item(3), String) Then Return False

                If X.PrintText <> CType(DT.Rows.Item(RowIndex).Item(4), String) Then Return False
                If X.Deleted <> CType(DT.Rows.Item(RowIndex).Item(5), Boolean) Then Return False

                RowIndex += 1
            Next

        Catch ex As Exception

            Return False

        End Try

        If BO.Count > 0 Then Return True

    End Function

#End Region

End Class