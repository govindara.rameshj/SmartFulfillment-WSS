﻿<TestClass()> Public Class CouponUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : Coupon | Public Function : Initialise"

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdNothing_DeletedTrue_ReturnValueFalse()

        Dim X As New Coupon

        Assert.IsFalse(X.Initialise(Nothing, True))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdEmpty_DeletedTrue_ReturnValueFalse()

        Dim X As New Coupon

        Assert.IsFalse(X.Initialise(String.Empty, True))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_CouponIdXYZ_DeletedTrue_ReturnValueTrue()

        Dim Mock As New MockRepository

        Dim X As New Coupon
        Dim Repository As IGetCouponRepository
        Dim DR As DataRow

        'arrange; stubbed out call to database
        Repository = Mock.Stub(Of IGetCouponRepository)()

        DR = Nothing
        SetupResult.On(Repository).Call(Repository.GetCouponDeletedTrue("XYZ")).Return(DR).IgnoreArguments()
        GetCouponRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        Assert.IsTrue(X.Initialise("XYZ", True))

    End Sub

#End Region

#Region "Class : Coupon | Private Procedure : Load"

    <TestMethod()> Public Sub ProcedureLoad_DeletedTrue_DataRowNothing_ReturnValue_EmptyBusinessObject()

        Dim X As New Coupon

        ConfigureCouponRepository(X, Nothing, CouponMasterDataRowConfigureStructureAndPopulate("XXXXX", "Description", False, False, False, False, False, False))

        X.Load(Nothing, True)

        Assert.IsNull(X._COUPONID)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_DeletedTrue_DataRowNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New Coupon

        ConfigureCouponRepository(X, Nothing, CouponMasterDataRowConfigureStructureAndPopulate("XXXXX", "Description", False, False, False, False, False, False))

        X.Load(Nothing, True)

        Assert.IsFalse(X.ExistsInDatabase)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_DeletedTrue_DataRowExist_ReturnValue_NonEmptyBusinessObject()

        Dim X As New Coupon

        ConfigureCouponRepository(X, CouponMasterDataRowConfigureStructureAndPopulate("XXXXX", "Description", False, False, False, False, False, False), Nothing)

        X.Load(Nothing, True)

        Assert.IsNotNull(X._COUPONID)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_DeletedTrue_DataRowExist_ReturnValue_ExistInDatabaseTrue()

        Dim X As New Coupon

        ConfigureCouponRepository(X, CouponMasterDataRowConfigureStructureAndPopulate("XXXXX", "Description", False, False, False, False, False, False), Nothing)

        X.Load(Nothing, True)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_DeletedTrue_BusinessObjectLoadsDataTableCorrectly()

        Dim X As New Coupon
        Dim DR As DataRow

        DR = CouponMasterDataRowConfigureStructureAndPopulate("0000001", "Test Description", True, False, True, False, True, False)
        ConfigureCouponRepository(X, DR, Nothing)

        X.Load(Nothing, True)

        Assert.IsTrue(MatchBusinessObjectAgainstDataRow(X, DR))

    End Sub




    <TestMethod()> Public Sub ProcedureLoad_DeletedFalse_DataRowNothing_ReturnValue_EmptyBusinessObject()

        Dim X As New Coupon

        ConfigureCouponRepository(X, CouponMasterDataRowConfigureStructureAndPopulate("XXXXX", "Description", False, False, False, False, False, False), Nothing)

        X.Load(Nothing, False)

        Assert.IsNull(X._COUPONID)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_DeletedFalse_DataRowNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New Coupon

        ConfigureCouponRepository(X, CouponMasterDataRowConfigureStructureAndPopulate("XXXXX", "Description", False, False, False, False, False, False), Nothing)

        X.Load(Nothing, False)

        Assert.IsFalse(X.ExistsInDatabase)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_DeletedFalse_DataRowExist_ReturnValue_NonEmptyBusinessObject()

        Dim X As New Coupon

        ConfigureCouponRepository(X, Nothing, CouponMasterDataRowConfigureStructureAndPopulate("XXXXX", "Description", False, False, False, False, False, False))

        X.Load(Nothing, False)

        Assert.IsNotNull(X._COUPONID)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_DeletedFalse_DataRowExist_ReturnValue_ExistInDatabaseTrue()

        Dim X As New Coupon

        ConfigureCouponRepository(X, Nothing, CouponMasterDataRowConfigureStructureAndPopulate("XXXXX", "Description", False, False, False, False, False, False))

        X.Load(Nothing, False)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_DeletedFalse_BusinessObjectLoadsDataTableCorrectly()

        Dim X As New Coupon
        Dim DR As DataRow

        DR = CouponMasterDataRowConfigureStructureAndPopulate("0000001", "Test Description", True, False, True, False, True, False)
        ConfigureCouponRepository(X, Nothing, DR)

        X.Load(Nothing, False)

        Assert.IsTrue(MatchBusinessObjectAgainstDataRow(X, DR))

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub ConfigureCouponRepository(ByRef X As Coupon, ByRef TrueDR As DataRow, ByRef FalseDR As DataRow)

        Dim Mock As New MockRepository
        Dim Repository As IGetCouponRepository

        Repository = Mock.Stub(Of IGetCouponRepository)()
        SetupResult.On(Repository).Call(Repository.GetCouponDeletedTrue(Nothing)).Return(TrueDR).IgnoreArguments()
        SetupResult.On(Repository).Call(Repository.GetCouponDeletedFalse(Nothing)).Return(FalseDR).IgnoreArguments()
        GetCouponRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Function CouponMasterDataRowConfigureStructureAndPopulate(ByVal CouponID As String, ByVal Description As String, _
                                                                      ByVal StoreGenerated As Boolean, ByVal SerialNumber As Boolean, _
                                                                      ByVal Exclusive As Boolean, ByVal Reusable As Boolean, _
                                                                      ByVal RequiresCustomerInfo As Boolean, ByVal Deleted As Boolean) As DataRow

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns
            .Add("COUPONID", System.Type.GetType("System.String"))
            .Add("DESCRIPTION", System.Type.GetType("System.String"))
            .Add("STOREGEN", System.Type.GetType("System.Boolean"))
            .Add("SERIALNO", System.Type.GetType("System.Boolean"))
            .Add("EXCCOUPON", System.Type.GetType("System.Boolean"))
            .Add("REUSABLE", System.Type.GetType("System.Boolean"))
            .Add("COLLECTINFO", System.Type.GetType("System.Boolean"))
            .Add("DELETED", System.Type.GetType("System.Boolean"))
        End With

        DT.Rows.Add(CouponID, Description, StoreGenerated, SerialNumber, Exclusive, Reusable, RequiresCustomerInfo, Deleted)

        Return DT.Rows.Item(0)

    End Function

    Private Function MatchBusinessObjectAgainstDataRow(ByRef BO As Coupon, ByRef DR As DataRow) As Boolean

        Try

            If BO.CouponID <> CType(DR.Item(0), String) Then Return False
            If BO.Description <> CType(DR.Item(1), String) Then Return False
            If BO.StoreGenerated <> CType(DR.Item(2), Boolean) Then Return False
            If BO.SerialNumber <> CType(DR.Item(3), Boolean) Then Return False
            If BO.Exclusive <> CType(DR.Item(4), Boolean) Then Return False
            If BO.Reusable <> CType(DR.Item(5), Boolean) Then Return False
            If BO.RequiresCustomerInfo <> CType(DR.Item(6), Boolean) Then Return False
            If BO.Deleted <> CType(DR.Item(7), Boolean) Then Return False

        Catch ex As Exception

            Return False

        End Try

        Return True

    End Function

#End Region

End Class