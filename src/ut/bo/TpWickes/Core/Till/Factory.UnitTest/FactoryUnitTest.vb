<TestClass()> Public Class FactoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory - TpWickes.Core.Till.Interface"

    <TestMethod()> Public Sub CouponFactory_ReturnCorrectObject()

        Dim X As ICoupon

        X = CouponFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.Coupon.Coupon", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub CouponHeaderFactory_ReturnCorrectObject()

        Dim X As ICouponHeader

        X = CouponHeaderFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.Coupon.CouponHeader", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub CouponTextLineFactory_ReturnCorrectObject()

        Dim X As ICouponTextLine

        X = CouponTextLineFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.CouponText.CouponTextLine", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub CouponTextLinesFactory_ReturnCorrectObject()

        Dim X As ICouponTextLines

        X = CouponTextLinesFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.CouponText.CouponTextLines", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub OfflineModeFactory_ReturnCorrectObject()
        Dim X As IOfflineMode

        X = OfflineModeFactory.FactoryGet
        Assert.AreEqual("TpWickes.Core.Till.Implementation.Library.OfflineMode", X.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub TextCommunicationLineFactory_ReturnCorrectObject()

        Dim X As ITextCommunicationLine

        X = TextCommunicationLineFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.Library.TextCommunicationLine", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub TextCommunicationLinesFactory_ReturnCorrectObject()

        Dim X As ITextCommunicationLines

        X = TextCommunicationLinesFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.Library.TextCommunicationLines", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub TextLineFactory_ReturnCorrectObject()

        Dim X As ICouponTextLine

        X = CouponTextLineFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.CouponText.CouponTextLine", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub TextLinesFactory_ReturnCorrectObject()

        Dim X As ICouponTextLines

        X = CouponTextLinesFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.CouponText.CouponTextLines", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub ValidateXmlFactory_ReturnCorrectObject()

        Dim X As IValidateXml

        X = ValidateXmlFactory.FactoryGet

        Assert.AreEqual("TpWickes.Core.Till.Implementation.Library.ValidateXml", X.GetType.FullName)

    End Sub

#End Region

End Class