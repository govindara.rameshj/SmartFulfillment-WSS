﻿<TestClass()> _
Public Class OfflineModeRepositoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : OfflineModeRepository | Public Function : SetOfflineMode"

    '<TestMethod()> _
    'Public Sub FunctionSetOfflineMode_CallsExecuteNonQuery()
    '    Dim Com As ICommand = MockRepository.GenerateStub(Of ICommand)()
    '    Dim X As IOfflineModeRepository = New OfflineModeRepository

    '    Rhino.Mocks.Expect.Call(Com.ExecuteNonQuery()).Return(1).IgnoreArguments()
    '    X.SetOfflineMode(False)
    '    Com.AssertWasCalled(Function(y) y.ExecuteNonQuery)
    'End Sub

    '<TestMethod()> _
    'Public Sub FunctionSetOfflineMode_CallsAddSetStoredProcedure()
    '    Dim Com As ICommand = MockRepository.GenerateStub(Of ICommand)()
    '    Dim X As IOfflineModeRepository = New OfflineModeRepository

    '    X.SetOfflineMode(False)
    '    Com.AssertWasCalled(Function(y) y.ExecuteNonQuery)
    'End Sub

    '<TestMethod()> _
    'Public Sub FunctionSetOfflineMode_CallsAddInOfflineModeParameter()
    '    Dim Com As ICommand = MockRepository.GenerateStub(Of ICommand)()
    '    Dim X As IOfflineModeRepository = New OfflineModeRepository

    '    X.SetOfflineMode(False)
    '    Com.AssertWasCalled(Function(y) y.ExecuteNonQuery)
    'End Sub
#End Region

#Region "Class : OfflineModeRepository | Public Function : IsInOfflineMode"

    '<TestMethod()> _
    'Public Sub FunctionIsInOfflineMode_CallsExecuteNonQuery()
    '    Dim Com As ICommand = MockRepository.GenerateStub(Of ICommand)()
    '    Dim X As IOfflineModeRepository = New OfflineModeRepository

    '    X.SetOfflineMode(False)
    '    Com.AssertWasCalled(Function(y) y.ExecuteNonQuery)
    'End Sub

#End Region

#Region "Class : OfflineModeRepository | Private Function : LocalConnection"

    <TestMethod()> _
    Public Sub FunctionLocalConnection_ReturnsOfflineModeConnection()
        Dim X As New OfflineModeRepository

        Assert.IsTrue(TypeOf X.LocalConnection Is OfflineModeConnection)
    End Sub
#End Region

#Region "Class : OfflineModeRepository | Private Function : LocalConnectionCommand"

    '<TestMethod()> _
    'Public Sub FunctionLocalConnection_ReturnsICommand()
    '    Dim X As New OfflineModeRepository
    '    Dim Con As IConnection = New OfflineModeConnection

    '    Assert.IsTrue(X.LocalConnectionCommand(Con).GetType.Name, New Command(Con))
    'End Sub
#End Region

#Region "Class : OfflineModeRepository | Private Function : AddSetStoredProcedure"

    <TestMethod()> _
    Public Sub ProcedureAddSetStoredProcedure_CorrectNameReturned()
        Dim X As New OfflineModeRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)
        X.AddSetStoredProcedure(Com)
        Assert.AreEqual("usp_OfflineModeSet", Com.StoredProcedureName)
    End Sub
#End Region

#Region "Class : OfflineModeRepository | Private Procedure : AddGetStoredProcedure"

    <TestMethod()> Public Sub ProcedureAddGetStoredProcedure_CorrectNameReturned()
        Dim X As New OfflineModeRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)
        X.AddGetStoredProcedure(Com)
        Assert.AreEqual("usp_OfflineModeGet", Com.StoredProcedureName)
    End Sub
#End Region

#Region "Class : OfflineModeRepository | Private Procedure : AddInOfflineModeParameter"

    <TestMethod()> _
    Public Sub ProcedureInOfflineModeParameter_InitialValue_InOfflineModeAnyValue_ParameterCollectionItemZeroCorrectNameReturned()
        Dim X As New OfflineModeRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)
        X.AddInOfflineModeParameter(Com, False)
        Assert.AreEqual("@InOfflineMode", Com.ParameterName(0))
    End Sub

    <TestMethod()> _
    Public Sub ProcedureInOfflineModeParameter_InitialValue_InOfflineModeAnyValue_ParameterCollectionItemZeroCorrectTypeReturned()
        Dim X As New OfflineModeRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)
        X.AddInOfflineModeParameter(Com, True)
        Assert.AreEqual(Com.ParameterTypeProperty(0), SqlDbType.Bit)
    End Sub

    <TestMethod()> _
    Public Sub ProcedureInOfflineModeParameter_InitialValue_InOfflineModeFalse_ParameterInOfflineMode_CorrectValueReturned()
        Dim X As New OfflineModeRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)
        X.AddInOfflineModeParameter(Com, False)
        Assert.AreEqual(Com.GetParameterValue("@InOfflineMode"), 0)
    End Sub

    <TestMethod()> _
    Public Sub ProcedureInOfflineModeParameter_InitialValue_InOfflineModeTrue_ParameterInOfflineMode_CorrectValueReturned()
        Dim X As New OfflineModeRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)
        X.AddInOfflineModeParameter(Com, True)
        Assert.AreEqual(Com.GetParameterValue("@InOfflineMode"), 1)
    End Sub
#End Region

#Region "Class : OfflineModeRepository | Private Property : RequirementSwitchID"

    <TestMethod()> _
    Public Sub PropertyRequirementSwitchID_CorrectValueReturned()
        Dim X As New OfflineModeRepository

        Assert.AreEqual(X.RequirementSwitchID, 984151)
    End Sub
#End Region

#Region "Private Procedures And Functions"

    Private Function CouponMasterDataTableConfigureStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns
            .Add("COUPONID", System.Type.GetType("System.String"))
            .Add("DESCRIPTION", System.Type.GetType("System.String"))
            .Add("STOREGEN", System.Type.GetType("System.Boolean"))
            .Add("SERIALNO", System.Type.GetType("System.Boolean"))
            .Add("EXCCOUPON", System.Type.GetType("System.Boolean"))
            .Add("REUSABLE", System.Type.GetType("System.Boolean"))
            .Add("COLLECTINFO", System.Type.GetType("System.Boolean"))
            .Add("DELETED", System.Type.GetType("System.Boolean"))
        End With

        Return DT

    End Function

    Friend Sub CouponMasterTableAddData(ByRef DT As DataTable, _
                                        ByVal CouponID As String, ByVal Description As String, _
                                        ByVal StoreGenerated As Boolean, ByVal SerialNumber As Boolean, _
                                        ByVal Exclusive As Boolean, ByVal Reusable As Boolean, _
                                        ByVal RequiresCustomerInfo As Boolean, ByVal Deleted As Boolean)

        DT.Rows.Add(CouponID, Description, StoreGenerated, SerialNumber, Exclusive, Reusable, RequiresCustomerInfo, Deleted)

    End Sub

#End Region

End Class