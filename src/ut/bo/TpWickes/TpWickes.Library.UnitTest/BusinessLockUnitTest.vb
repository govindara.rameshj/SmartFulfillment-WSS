﻿<TestClass()> Public Class BusinessLockUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestBusinessLock
        Inherits BusinessLock

#Region "Private Variables"

        Private _DT As DataTable
        Private _LockDuration As System.Nullable(Of Integer)
        Private _LockExistValue As System.Nullable(Of Boolean)
        Private _NewRecordLockIdValue As System.Nullable(Of Integer)

        Private _ExecuteLockExistCalled As Boolean
        Private _ExecuteAddLockCalled As Boolean
        Private _ExecuteRemoveLockCalled As Boolean

#End Region

#Region "Local Properties"

        Public ReadOnly Property ExecuteLockExistCalled() As Boolean
            Get
                Return _ExecuteLockExistCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteAddLockCalled() As Boolean
            Get
                Return _ExecuteAddLockCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteRemoveLockCalled() As Boolean
            Get
                Return _ExecuteRemoveLockCalled
            End Get
        End Property

#End Region

#Region "Constructor"

        Public Sub New(ByVal DT As DataTable, _
                       ByVal LockDuration As System.Nullable(Of Integer), _
                       ByVal LockExist As System.Nullable(Of Boolean), _
                       ByVal NewRecordLockID As System.Nullable(Of Integer))

            _DT = DT
            _LockDuration = LockDuration

            _LockExistValue = LockExist
            _NewRecordLockIdValue = NewRecordLockID

        End Sub

#End Region

#Region "Overrides"

        Friend Overrides Function GetLockData() As System.Data.DataTable

            Return _DT

        End Function

        Friend Overrides Function GetLockDuration() As System.Nullable(Of Integer)

            Return _LockDuration

        End Function

        Friend Overrides Function ExecuteLockExist(ByVal WorkStationID As Integer, _
                                                   ByVal CurrentDateAndTime As Date, _
                                                   ByVal AssemblyName As String, _
                                                   ByVal ParameterValues As String) As Boolean

            _ExecuteLockExistCalled = True

            If _LockExistValue.HasValue = False Then

                Return MyBase.ExecuteLockExist(WorkStationID, CurrentDateAndTime, AssemblyName, ParameterValues)

            Else

                Return _LockExistValue.Value

            End If

        End Function

        Friend Overrides Function ExecuteAddLock(ByVal WorkStationID As Integer, _
                                                 ByVal AssemblyName As String, _
                                                 ByVal ParameterValues As String) As Integer

            _ExecuteAddLockCalled = True

            If _NewRecordLockIdValue.HasValue = False Then

                Return MyBase.ExecuteAddLock(WorkStationID, AssemblyName, ParameterValues)

            Else

                Return _NewRecordLockIdValue.Value

            End If

        End Function

        Friend Overrides Sub ExecuteRemoveLock(ByVal RecordID As Integer)

            _ExecuteRemoveLockCalled = True

        End Sub

#End Region

    End Class

#End Region


#Region "Unit Test | Public Interface Function CreateLock"

    <TestMethod()> Public Sub FunctionCreateLock_LockExistsFalse_DoesCallInternalFunctionExecuteLockExist()

        Dim Test As New TestBusinessLock(Nothing, Nothing, False, 7)

        Test.CreateLock(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(Test.ExecuteLockExistCalled)

    End Sub

    <TestMethod()> Public Sub FunctionCreateLock_LockExistsTrue_ReturnMinusOne()

        Dim Test As New TestBusinessLock(Nothing, Nothing, True, 7)

        Assert.AreEqual(-1, Test.CreateLock(Nothing, Nothing, Nothing, Nothing))

    End Sub

    <TestMethod()> Public Sub FunctionCreateLock_LockExistsFalse_DoesCallInternalFunctionExecuteAddLock()

        Dim Test As New TestBusinessLock(Nothing, Nothing, False, 7)

        Test.CreateLock(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(Test.ExecuteAddLockCalled)

    End Sub

    <TestMethod()> Public Sub FunctionCreateLock_LockExistsFalse_ReturnSeven()

        Dim Test As New TestBusinessLock(Nothing, Nothing, False, 7)

        Assert.AreEqual(7, Test.CreateLock(Nothing, Nothing, Nothing, Nothing))

    End Sub

#End Region

#Region "Unit Test | Public Interface Procedure RemoveLock"

    <TestMethod()> Public Sub FunctionRemoveLock_DoesCallInternalProcedureExecuteRemoveLock()

        Dim Test As New TestBusinessLock(Nothing, Nothing, Nothing, Nothing)

        Test.RemoveLock(Nothing)

        Assert.IsTrue(Test.ExecuteRemoveLockCalled)

    End Sub

#End Region

#Region "Unit Test | Function ExecuteLockExist"

    <TestMethod()> Public Sub FunctionExecuteLockExist_TestScenarios()

        Dim Lock As TestBusinessLock
        Dim DT As DataTable
        Dim CurrentDateTime As DateTime

        CurrentDateTime = Now

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Data
        Lock = New TestBusinessLock(Nothing, Nothing, Nothing, Nothing)
        Assert.IsFalse(Lock.ExecuteLockExist(20, CurrentDateTime, Nothing, Nothing), "Data Exist: No (Data Table Missing) | Return: False")

        Lock = New TestBusinessLock(New DataTable, Nothing, Nothing, Nothing)
        Assert.IsFalse(Lock.ExecuteLockExist(20, CurrentDateTime, Nothing, Nothing), "Data Exist: No (Data Table Empty)   | Return: False")


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Assembly Name / Parameters
        DT = CreateAndPopulateBusinessLockDataTable(1, 20, Nothing, Nothing, Nothing)
        Lock = New TestBusinessLock(DT, Nothing, Nothing, Nothing)

        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, Nothing, Nothing), "              Data Exists | Assembly Name: Nothing  | Parameters: Nothing  | Return: False")
        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", Nothing), "      Data Exists | Assembly Name: MisMatch | Parameters: Nothing  | Return: False")
        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersA"), "Data Exists | Assembly Name: MisMatch | Parameters: MisMatch | Return: False")


        DT = CreateAndPopulateBusinessLockDataTable(1, 20, "AssemblyNameA", Nothing, Nothing)
        Lock = New TestBusinessLock(DT, Nothing, Nothing, Nothing)

        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, Nothing, Nothing), "              Data Exists | Assembly Name: Nothing  | Parameters: Nothing  | Return: False")
        Assert.IsTrue(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", Nothing), "       Data Exists | Assembly Name: Match    | Parameters: Nothing  | Return: True")
        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameB", Nothing), "      Data Exists | Assembly Name: MisMatch | Parameters: Nothing  | Return: False")
        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersA"), "Data Exists | Assembly Name: Match    | Parameters: MisMatch | Return: False")


        DT = CreateAndPopulateBusinessLockDataTable(1, 20, "AssemblyNameA", "ParametersA", Nothing)
        Lock = New TestBusinessLock(DT, Nothing, Nothing, Nothing)

        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, Nothing, Nothing), "              Data Exists | Assembly Name: Nothing | Parameters: Nothing  | Return: False")
        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", Nothing), "      Data Exists | Assembly Name: Match   | Parameters: Nothing  | Return: False")
        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersB"), "Data Exists | Assembly Name: Match   | Parameters: MisMatch | Return: False")
        Assert.IsTrue(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersA"), " Data Exists | Assembly Name: Match   | Parameters: Match    | Return: True")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Lock Duration
        DT = CreateAndPopulateBusinessLockDataTable(1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime)
        Lock = New TestBusinessLock(DT, Nothing, Nothing, Nothing)
        Assert.IsTrue(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersA"), " Data Exists | Assembly Name & Parameters Match | Lock Duration: Nothing                      | Return: True")

        Lock = New TestBusinessLock(DT, 0, Nothing, Nothing)
        Assert.IsTrue(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersA"), " Data Exists | Assembly Name & Parameters Match | Lock Duration: Zero (Lock always valid)     | Return: True")

        DT = CreateAndPopulateBusinessLockDataTable(1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddSeconds(-3601))
        Lock = New TestBusinessLock(DT, 3600, Nothing, Nothing)
        Assert.IsFalse(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersA"), "Data Exists | Assembly Name & Parameters Match | Lock Duration: 3600 (Lock 3601 Seconds Old) | Return: False")

        DT = CreateAndPopulateBusinessLockDataTable(1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddSeconds(-3600))
        Lock = New TestBusinessLock(DT, 3600, Nothing, Nothing)
        Assert.IsTrue(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersA"), " Data Exists | Assembly Name & Parameters Match | Lock Duration: 3600 (Lock 3600 Seconds Old  | Return: True")

        DT = CreateAndPopulateBusinessLockDataTable(1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddSeconds(-3599))
        Lock = New TestBusinessLock(DT, 3600, Nothing, Nothing)
        Assert.IsTrue(Lock.ExecuteLockExist(21, CurrentDateTime, "AssemblyNameA", "ParametersA"), " Data Exists | Assembly Name & Parameters Match | Lock Duration: 3600 (Lock 3599 Seconds Old  | Return: True")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Workstation ID
        DT = CreateAndPopulateBusinessLockDataTable(1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime)
        Lock = New TestBusinessLock(DT, 3600, Nothing, Nothing)
        Assert.IsFalse(Lock.ExecuteLockExist(20, CurrentDateTime, "AssemblyNameA", "ParametersA"), "Data Exists | Assembly Name & Parameters Match | Lock Vaid: True  | WorkStation ID: Match    | Return: False")
        Assert.IsTrue(Lock.ExecuteLockExist(19, CurrentDateTime, "AssemblyNameA", "ParametersA"), " Data Exists | Assembly Name & Parameters Match | Lock Vaid: True  | WorkStation ID: MisMatch | Return: True")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Multiple records

        DT = CreateBusinessLockDataTable()
        PopulateBusinessLockDataTable(DT, 1, 20, Nothing, Nothing, Nothing)
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime)

        Lock = New TestBusinessLock(DT, 3600, Nothing, Nothing)
        Assert.IsFalse(Lock.ExecuteLockExist(20, CurrentDateTime, "AssemblyNameA", "ParametersA"), "Data Exists | Assembly Name & Parameters Match | Lock Vaid: True  | WorkStation ID: Match    | Multiple Records | Return: False")
        Assert.IsTrue(Lock.ExecuteLockExist(19, CurrentDateTime, "AssemblyNameA", "ParametersA"), " Data Exists | Assembly Name & Parameters Match | Lock Vaid: True  | WorkStation ID: MisMatch | Multiple Records | Return: True")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Multiple records: Expired 

        DT = CreateBusinessLockDataTable()
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-5))
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-4))
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-3))
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-2))

        Lock = New TestBusinessLock(DT, 3600, Nothing, Nothing)
        Assert.IsFalse(Lock.ExecuteLockExist(19, CurrentDateTime, "AssemblyNameA", "ParametersA"), "Data Exists | Assembly Name & Parameters Match | Lock Vaid: True  | WorkStation ID: Match    | Multiple Records: Expired Duration | Return: False")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Multiple records: Expired And Unexpired 

        DT = CreateBusinessLockDataTable()
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-5))
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-4))
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-3))
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-2))
        PopulateBusinessLockDataTable(DT, 1, 20, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-1))

        Lock = New TestBusinessLock(DT, 3600, Nothing, Nothing)
        Assert.IsTrue(Lock.ExecuteLockExist(19, CurrentDateTime, "AssemblyNameA", "ParametersA"), "Data Exists | Assembly Name & Parameters Match | Lock Vaid: True  | WorkStation ID: Match    | Multiple Records: Expired And Unexpired  Duration | Return: True")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Function CreateBusinessLockDataTable() As DataTable

        Dim DT As DataTable

        DT = New DataTable
        DT.Columns.Add("ID", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("WorkStationID", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("AssemblyName", System.Type.GetType("System.String"))
        DT.Columns.Add("Parameters", System.Type.GetType("System.String"))
        DT.Columns.Add("DateTimeStamp", System.Type.GetType("System.DateTime"))

        Return DT

    End Function

    Private Sub EmptyBusinessLockDataTable(ByRef DT As DataTable)

        DT.Rows.Clear()

    End Sub

    Private Sub PopulateBusinessLockDataTable(ByRef DT As DataTable, _
                                              ByVal ID As Integer, ByVal WorkStationID As Integer, _
                                              ByVal AssemblyName As String, ByVal Parameters As String, ByVal DateTimeStamp As Date)

        DT.Rows.Add(ID, WorkStationID, AssemblyName, Parameters, DateTimeStamp)

    End Sub

    Private Function CreateAndPopulateBusinessLockDataTable(ByVal ID As Integer, ByVal WorkStationID As Integer, _
                                                            ByVal AssemblyName As String, ByVal Parameters As String, ByVal DateTimeStamp As Date) As DataTable

        Dim DT As DataTable

        DT = New DataTable
        DT.Columns.Add("ID", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("WorkStationID", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("AssemblyName", System.Type.GetType("System.String"))
        DT.Columns.Add("Parameters", System.Type.GetType("System.String"))
        DT.Columns.Add("DateTimeStamp", System.Type.GetType("System.DateTime"))

        DT.Rows.Add(ID, WorkStationID, AssemblyName, Parameters, DateTimeStamp)

        Return DT

    End Function

#End Region

End Class
