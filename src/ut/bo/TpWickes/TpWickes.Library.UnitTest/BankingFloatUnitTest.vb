﻿<TestClass()> Public Class BankingFloatUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Requirement Test"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassEventHeaderDateNew()

        Dim NewVersionOnly As IBankingFloat
        Dim Factory As IBaseFactory(Of IBankingFloat)

        Factory = New BankingFloatFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("TpWickes.Library.BankingFloatRevised", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - Revised"

    <TestMethod()> Public Sub BankingFloatRevised_FunctionFloatIssued_ParameterNothing_ReturnZero()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(0, Decimal), V.FloatIssues(ArrangeDataTableFloatIssues(Nothing)))

    End Sub

    <TestMethod()> Public Sub BankingFloatRevised_ParameterZero_ReturnZero()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(0, Decimal), V.FloatIssues(ArrangeDataTableFloatIssues("0")))

    End Sub

    <TestMethod()> Public Sub BankingFloatRevised_ParameterTen_ReturnTen()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(10, Decimal), V.FloatIssues(ArrangeDataTableFloatIssues("10")))

    End Sub

    <TestMethod()> Public Sub BankingFloatRevised_FunctionFloatReturned_ParameterNothing_ReturnZero()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(0, Decimal), V.FloatReturned(ArrangeDataTableFloatReturned(Nothing)))

    End Sub

    <TestMethod()> Public Sub BankingFloatRevised_FunctionFloatReturned_ParameterZero_ReturnZero()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(0, Decimal), V.FloatReturned(ArrangeDataTableFloatReturned("0")))

    End Sub

    <TestMethod()> Public Sub BankingFloatRevised_FunctionFloatReturned_ParameterTen_ReturnTen()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(10, Decimal), V.FloatReturned(ArrangeDataTableFloatReturned("10")))

    End Sub

    <TestMethod()> Public Sub BankingFloatRevised_FunctionVariance__ParameterNothing_ReturnZero()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(0, Decimal), V.FloatVariance(ArrangeDataSetFloatVariance(Nothing)))

    End Sub

    <TestMethod()> Public Sub BankingFloatRevised_FunctionVariance__ParameterZero_ReturnZero()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(0, Decimal), V.FloatVariance(ArrangeDataSetFloatVariance("0")))

    End Sub

    <TestMethod()> Public Sub BankingFloatRevised_FunctionVariance__ParameterTen_ReturnTen()

        Dim V As IBankingFloat = (New BankingFloatFactory).GetImplementation

        Assert.AreEqual(CType(10, Decimal), V.FloatVariance(ArrangeDataSetFloatVariance("10")))

    End Sub

#End Region

#Region "Private Test Functions"

    Private Function ArrangeDataTableFloatIssues(ByVal Value As String) As DataTable

        Dim DT As DataTable

        DT = New DataTable

        DT.Columns.Add("COLUMN1", System.Type.GetType("System.String"))
        DT.Columns.Add("COLUMN2", System.Type.GetType("System.String"))
        DT.Rows.Add(Value, Nothing)

        Return DT

    End Function

    Private Function ArrangeDataTableFloatReturned(ByVal Value As String) As DataTable

        Dim DT As DataTable

        DT = New DataTable

        DT.Columns.Add("COLUMN1", System.Type.GetType("System.String"))
        DT.Columns.Add("COLUMN2", System.Type.GetType("System.String"))
        DT.Rows.Add(Nothing, Value)

        Return DT

    End Function

    Private Function ArrangeDataSetFloatVariance(ByVal Value As String) As DataSet

        Dim DS As DataSet
        Dim DT As DataTable

        DS = New DataSet
        DT = New DataTable

        DT.Columns.Add("FloatVariance", System.Type.GetType("System.String"))
        DT.Rows.Add(Value)

        DS.Tables.Add(DT)

        Return DS

    End Function

#End Region

End Class
