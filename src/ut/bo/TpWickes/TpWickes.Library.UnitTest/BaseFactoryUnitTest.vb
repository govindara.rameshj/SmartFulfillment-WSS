﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports TpWickes.Library



'''<summary>
'''This is a test class for BaseFactoryUnitTest and is intended
'''to contain all BaseFactoryUnitTest Unit Tests
'''</summary>
<TestClass()> _
Public Class BaseFactoryUnitTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '<TestMethod()> _
    'Public Sub ImplementationTest()

    '    ImplementationTestHelper(Of GenericParameterHelper)()
    'End Sub

    ''''<summary>
    ''''A test for Implementation
    ''''</summary>
    'Public Sub ImplementationTestHelper(Of ITestImplementation)()
    '    Dim target As IBaseFactory(Of ITestImplementation) = CreateBaseFactory(Of ITestImplementation)() ' TODO: Initialize to an appropriate value
    '    Dim expected As ITestImplementation = CType(New TestImplementation, ITestImplementation) ' TODO: Initialize to an appropriate value
    '    Dim actual As ITestImplementation

    '    actual = target.Implementation
    '    Assert.AreEqual(expected, actual)
    '    Assert.Inconclusive("Verify the correctness of this test method.")
    'End Sub

    '<TestMethod()> _
    'Public Sub GetOnlyImplementationTest()

    '    GetOnlyImplementationTestHelper(Of GenericParameterHelper)()
    'End Sub

    ''''<summary>
    ''''A test for GetOnlyImplementation
    ''''</summary>
    'Public Sub GetOnlyImplementationTestHelper(Of T)()
    '    Dim target As IBaseFactory(Of T) = CreateBaseFactory(Of T)() ' TODO: Initialize to an appropriate value
    '    Dim expected As T = CType(Nothing, T) ' TODO: Initialize to an appropriate value
    '    Dim actual As T

    '    actual = target.GetOnlyImplementation
    '    Assert.AreEqual(expected, actual)
    '    Assert.Inconclusive("Verify the correctness of this test method.")
    'End Sub

    'Friend Overridable Function CreateBaseFactory(Of T)() As BaseFactory(Of T)
    '    'TODO: Instantiate an appropriate concrete class.
    '    Dim target As BaseFactory(Of T) = New TestFactory(Of T)

    '    Return target
    'End Function
End Class

Friend Interface ITestImplementation

    ReadOnly Property Name() As String
End Interface

Friend Class TestImplementation
    Implements ITestImplementation

    Public ReadOnly Property Name() As String Implements ITestImplementation.Name
        Get
            Return "TestImplementation"
        End Get
    End Property
End Class

'Friend Class TestFactory(Of ITestImplementation)
'    Inherits BaseFactory(Of ITestImplementation)

'    Public Overrides Function Implementation() As ITestImplementation

'        Return New TestImplementation
'    End Function
'End Class
