﻿<TestClass()> Public Class BusinessLockIntegrationTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Integration Test - Business Lock Class"

    <TestMethod()> Public Sub FunctionCreateLock_TestScenarios()

        Dim Lock As New BusinessLock
        Dim CurrentDateTime As DateTime

        CurrentDateTime = Now


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Multiple records: Expired 

        DynamicDeleteByWorkStationID(9999)
        DynamicDeleteByWorkStationID(19)

        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-5))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-4))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-3))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-2))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-1).AddSeconds(-1))

        Lock = New BusinessLock()
        Assert.AreNotEqual(-1, Lock.CreateLock(19, CurrentDateTime, "AssemblyNameA", "ParametersA"), "WorkStation ID: Match | Assembly Name & Parameters Match | Multiple Records: Expired Duration | Return: Non Zero Value")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Multiple records: Expired And Unexpired 

        DynamicDeleteByWorkStationID(9999)
        DynamicDeleteByWorkStationID(19)

        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-5))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-4))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-3))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-2))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime.AddHours(-1).AddSeconds(1))
        DynamicAdd(9999, "AssemblyNameA", "ParametersA", CurrentDateTime)

        Lock = New BusinessLock()
        Assert.AreEqual(-1, Lock.CreateLock(19, CurrentDateTime, "AssemblyNameA", "ParametersA"), "WorkStation ID: Match | Assembly Name & Parameters Match | Multiple Records: Expired And Unexpired  Duration | Return: Minus One")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        DynamicDeleteByWorkStationID(9999)
        DynamicDeleteByWorkStationID(19)

    End Sub

    <TestMethod()> Public Sub ProcedureRemoveLock_TestScenarios()

        Dim Lock As New BusinessLock
        Dim RecordID As Integer
        Dim DT As DataTable

        RecordID = Lock.CreateLock(20, Now, "AssemblyA", "ParametersA")

        Lock.RemoveLock(RecordID)

        DT = DynamicSelect(RecordID)

        Assert.AreEqual(0, DT.Rows.Count)

    End Sub

    <TestMethod()> Public Sub Function_AddLock_TestScenarios()

        Dim Lock As New BusinessLock
        Dim RecordID As Integer
        Dim DT As DataTable

        RecordID = Lock.ExecuteAddLock(20, "AssemblyA", "ParametersA")

        DT = DynamicSelect(RecordID)

        Assert.IsNotNull(RecordID, "Returned value not null")
        Assert.AreEqual(RecordID, CType(DT.Rows.Item(0).Item("ID"), Integer), "BusinessLock:ID Column")
        Assert.AreEqual(20, CType(DT.Rows.Item(0).Item("WorkStationID"), Integer), "BusinessLock:WorkStationID Column")
        Assert.AreEqual("AssemblyA", CType(DT.Rows.Item(0).Item("AssemblyName"), String), "BusinessLock:AssemblyName Column")
        Assert.AreEqual("ParametersA", CType(DT.Rows.Item(0).Item("Parameters"), String), "BusinessLock:Parameters Column")


        DynamicDeleteByRecordID(RecordID)

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Function DynamicSelect(ByVal RecordID As Integer) As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "select * from BusinessLock where ID  = " & RecordID.ToString
                Return com.ExecuteDataTable

            End Using
        End Using

    End Function

    Private Sub DynamicDeleteByRecordID(ByVal Value As Integer)

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "delete BusinessLock where ID = " & Value.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub DynamicDeleteByWorkStationID(ByVal Value As Integer)

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "delete BusinessLock where WorkStationID = " & Value.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub DynamicAdd(ByVal WorkStationID As Integer, _
                           ByVal AssemblyName As String, _
                           ByVal ParameterValues As String, _
                           ByVal CurrentDateAndTime As DateTime)

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("insert BusinessLock (WorkStationID, AssemblyName, Parameters, DateTimeStamp) ")
            .Append("             values (" & WorkStationID.ToString & ", '" & AssemblyName & "', '" & ParameterValues & "', '" & CurrentDateAndTime.ToString("yyyy-MM-dd HH:mm:ss") & "')")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

    End Sub

#End Region

End Class