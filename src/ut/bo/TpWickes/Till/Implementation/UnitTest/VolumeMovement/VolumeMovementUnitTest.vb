﻿<TestClass()> Public Class VolumeMovementUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Constants"

    Private TextCommunicationWarehouse As String = "The order will be delivered by our Central Warehouse and the delivery timeslot will be advised by text/email"
    Private TextCommunicationHubs As String = "The order will be delivered by our Salisbury Delivery Centre who will contact you by phone the evening before with a timeslot"
    Private TextCommunicationSplit As String = "Part of the order will be delivered by our Salisbury Delivery Centre who will contact the customer by phone the evening before with a timeslot, the remainer will be delivered by our Central Warehouse and the delivery timeslot will be advised by text/email"
    Private TextCommunicationCannotDetermine As String = "There is a problem with the order, once we are in a position to confirm the detail the customer will be contacted with delivery instructions"

    Private ResponseWarehouse As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                             <OMCheckFulfilmentResponse>
                                                 <DateTimeStamp>2012-01-16T09:00:00.000</DateTimeStamp>
                                                 <SuccessFlag>true</SuccessFlag>
                                                 <EstoreFulfiller>Central Warehouse</EstoreFulfiller>
                                                 <HubFulfiller/>
                                             </OMCheckFulfilmentResponse>

    Private ResponseHubs As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                        <OMCheckFulfilmentResponse>
                                            <DateTimeStamp>2012-01-16T09:00:00.000</DateTimeStamp>
                                            <SuccessFlag>true</SuccessFlag>
                                            <EstoreFulfiller/>
                                            <HubFulfiller>Salisbury</HubFulfiller>
                                        </OMCheckFulfilmentResponse>

    Private ResponseSplit As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                         <OMCheckFulfilmentResponse>
                                             <DateTimeStamp>2012-01-16T09:00:00.000</DateTimeStamp>
                                             <SuccessFlag>true</SuccessFlag>
                                             <EstoreFulfiller>Central Warehouse</EstoreFulfiller>
                                             <HubFulfiller>Salisbury</HubFulfiller>
                                         </OMCheckFulfilmentResponse>

    Private ResponseCannotDetermineSuccessFlagTrue As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                                                  <OMCheckFulfilmentResponse>
                                                                      <DateTimeStamp>2012-01-16T09:00:00.000</DateTimeStamp>
                                                                      <SuccessFlag>true</SuccessFlag>
                                                                      <EstoreFulfiller/>
                                                                      <HubFulfiller/>
                                                                  </OMCheckFulfilmentResponse>

    Private ResponseCannotDetermineSuccessFlagFalse As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                                                   <OMCheckFulfilmentResponse>
                                                                       <DateTimeStamp>2012-01-16T09:00:00.000</DateTimeStamp>
                                                                       <SuccessFlag>false</SuccessFlag>
                                                                       <EstoreFulfiller>Central Warehouse</EstoreFulfiller>
                                                                       <HubFulfiller>Salisbury</HubFulfiller>
                                                                   </OMCheckFulfilmentResponse>

    Private ResponseBad As XDocument = <?xml version="1.0" encoding="utf-8" standalone="no"?>
                                       <OMCheckFulfilmentResponse/>

#End Region

#Region "Unit Tests"

    <TestMethod()> <Ignore()> Public Sub FunctionOMCheckFulfilment_ScenerioTests()

        Dim VM As IVolumeMovement

        VM = VolumeMovementFactory.FactoryGet
        MockTextCommunicationRepository()

        MockOrderManagerCall(ResponseWarehouse.ToString)
        Assert.AreEqual(TextCommunicationWarehouse, VM.OMCheckFulfilment(GoodOrder), "Response: Warehouse")

        MockOrderManagerCall(ResponseHubs.ToString)
        Assert.AreEqual(TextCommunicationHubs, VM.OMCheckFulfilment(GoodOrder), "Response: Hubs")

        MockOrderManagerCall(ResponseSplit.ToString)
        Assert.AreEqual(TextCommunicationSplit, VM.OMCheckFulfilment(GoodOrder), "Response: Split")

        MockOrderManagerCall(ResponseCannotDetermineSuccessFlagTrue.ToString)
        Assert.AreEqual(TextCommunicationCannotDetermine, VM.OMCheckFulfilment(GoodOrder), "(SuccessFlag: True) Response: Cannot Determine")

        MockOrderManagerCall(ResponseCannotDetermineSuccessFlagFalse.ToString)
        Assert.AreEqual(TextCommunicationCannotDetermine, VM.OMCheckFulfilment(GoodOrder), "(SuccessFlag: False) Response: Cannot Determine")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        MockOrderManagerCall(ResponseBad.ToString)
        Assert.AreEqual(TextCommunicationCannotDetermine, VM.OMCheckFulfilment(GoodOrder), "Bad Response: Cannot Determine")

        MockOrderManagerCall(Nothing)
        Assert.AreEqual(TextCommunicationCannotDetermine, VM.OMCheckFulfilment(Nothing), "Bad Request: Cannot Determine")

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Private Sub MockOrderManagerCall(ByRef Response As String)

        Dim Mock As New MockRepository
        Dim OrderManager As IBaseService

        OrderManager = Mock.Stub(Of IBaseService)()
        SetupResult.On(OrderManager).Call(OrderManager.GetRequest).Return(New OMCheckFulfilmentRequest).IgnoreArguments()
        SetupResult.On(OrderManager).Call(OrderManager.GetResponseXml(Nothing)).Return(Response).IgnoreArguments()

        BaseServiceFactory.FactorySet(OrderManager)
        Mock.ReplayAll()

    End Sub

    Private Sub MockTextCommunicationRepository()

        Dim Mock As New MockRepository
        Dim Repository As IGetTextCommunicationRepository
        Dim ResonseWarehouseDT As DataTable
        Dim ResponseHubsDT As DataTable
        Dim ResponseSplitDT As DataTable
        Dim ResponseFailureDT As DataTable

        Repository = Mock.Stub(Of IGetTextCommunicationRepository)()

        ResonseWarehouseDT = ConfigureDataTabled()
        ResponseHubsDT = ConfigureDataTabled()
        ResponseSplitDT = ConfigureDataTabled()
        ResponseFailureDT = ConfigureDataTabled()

        AddData(ResonseWarehouseDT, 5, 2, 4, 1, "", Nothing, TextCommunicationWarehouse, Nothing, Nothing, Nothing, Nothing, False)
        AddData(ResponseHubsDT, 5, 2, 5, 1, "", Nothing, TextCommunicationHubs, Nothing, Nothing, Nothing, Nothing, False)
        AddData(ResponseSplitDT, 5, 2, 6, 1, "", Nothing, TextCommunicationSplit, Nothing, Nothing, Nothing, Nothing, False)
        AddData(ResponseFailureDT, 5, 2, 7, 1, "", Nothing, TextCommunicationCannotDetermine, Nothing, Nothing, Nothing, Nothing, False)

        SetupResult.On(Repository).Call(Repository.GetTextCommunications(2, 4)).Return(ResonseWarehouseDT)
        SetupResult.On(Repository).Call(Repository.GetTextCommunications(2, 5)).Return(ResponseHubsDT)
        SetupResult.On(Repository).Call(Repository.GetTextCommunications(2, 6)).Return(ResponseSplitDT)
        SetupResult.On(Repository).Call(Repository.GetTextCommunications(2, 7)).Return(ResponseFailureDT)

        GetTextCommunicationRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Function ConfigureDataTabled() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns

            .Add("Id", System.Type.GetType("System.Int16"))
            .Add("FunctionalAreaId", System.Type.GetType("System.Int16"))
            .Add("TypeId", System.Type.GetType("System.Int16"))
            .Add("Sequence", System.Type.GetType("System.Int16"))
            .Add("Description", System.Type.GetType("System.String"))
            .Add("SummaryText", System.Type.GetType("System.String"))
            .Add("FullText", System.Type.GetType("System.String"))
            .Add("LineDelimiter", System.Type.GetType("System.String"))
            .Add("ParagraphDelimiter", System.Type.GetType("System.String"))
            .Add("FieldStartDelimiter", System.Type.GetType("System.String"))
            .Add("FieldEndDelimiter", System.Type.GetType("System.String"))
            .Add("Deleted", System.Type.GetType("System.Boolean"))

        End With

        Return DT

    End Function

    Friend Sub AddData(ByRef DT As DataTable, _
                       ByVal ID As Integer, ByVal FunctionalAreaID As Integer, ByVal TypeID As Integer, ByVal Sequence As Integer, _
                       ByVal Description As String, ByVal SummaryText As String, ByVal FullText As String, ByVal LineDelimiter As String, _
                       ByVal ParagraphDelimiter As String, ByVal FieldStartDelimiter As String, ByVal FieldEndDelimiter As String, _
                       ByVal Deleted As Boolean)

        DT.Rows.Add(ID, FunctionalAreaID, TypeID, Sequence, Description, SummaryText, FullText, LineDelimiter, _
                    ParagraphDelimiter, FieldStartDelimiter, FieldEndDelimiter, Deleted)

    End Sub

    Friend Function GoodOrder() As IOrderHeader

        Dim OrderHeader As IOrderHeader

        'Wimbledon
        OrderHeader = OrderHeaderFactory.FactoryGet
        With OrderHeader
            .SellingStoreCode = 59
            .SellingStoreOrderNumber = 999
            .RequiredDeliveryDate = Now
            .DeliveryCharge = 10.0
            .TotalOrderValue = 624.94
            .SaleDate = Now
            .DeliveryPostcode = "SW6 4NF"
            .ToBeDelivered = True
        End With

        OrderHeader.AddByValue(1, 166159, "Spacers Box 3000", 2, 0, "EACH", 14.98, False, 7.49)
        OrderHeader.AddByValue(2, 191204, "Loft Ladder Timber", 4, 0, "EACH", 599.96, False, 149.99)

        Return OrderHeader

    End Function

#End Region

End Class