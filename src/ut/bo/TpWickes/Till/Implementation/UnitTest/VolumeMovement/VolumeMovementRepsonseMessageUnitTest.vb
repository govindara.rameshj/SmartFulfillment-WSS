﻿<TestClass()> Public Class VolumeMovementRepsonseMessageUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : VolumeMovementRepsonseMessage"

    <TestMethod()> Public Sub VolumeMovementRepsonseMessageFactory_ReturnCorrectObject()

        Dim X As IVolumeMovementResponseMessage

        X = VolumeMovementResponseMessageFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.VolumeMvmt.VolumeMovementResponseMessage", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub PropertyScenerioTests()

        Dim X As VolumeMovementResponseMessage

        X = New VolumeMovementResponseMessage

        'initial
        Assert.AreEqual(String.Empty, X._EstoreFulfiller, "Property Test - Initial State: Internal _EstoreFulfiller variable is String.Empty")
        Assert.AreEqual(String.Empty, X._HubFulfiller, "Property Test - Initial State: Internal _HubFulfiller variable is String.Empty")
        Assert.AreEqual(False, X._SuccessFlag, "Property Test - Initial State: Internal _SuccessFlag variable is False")

        'set
        X.EstoreFulfiller = "Set EstoreFulfiller"
        Assert.AreEqual("Set EstoreFulfiller", X._EstoreFulfiller, "Property Test - Set EstoreFulfiller")
        X.HubFulfiller = "Set HubFulfiller"
        Assert.AreEqual("Set HubFulfiller", X._HubFulfiller, "Property Test - Set HubFulfiller")
        X.SuccessFlag = True
        Assert.AreEqual(True, X._SuccessFlag, "Property Test - Set SuccessFlag")

        'get
        X._EstoreFulfiller = "Get EstoreFulfiller"
        Assert.AreEqual("Get EstoreFulfiller", X.EstoreFulfiller, "Property Test - Get EstoreFulfiller")
        X._HubFulfiller = "Get HubFulfiller"
        Assert.AreEqual("Get HubFulfiller", X.HubFulfiller, "Property Test - Get HubFulfiller")
        X._SuccessFlag = False
        Assert.AreEqual(False, X.SuccessFlag, "Property Test - Get SuccessFlag")

    End Sub

    <TestMethod()> Public Sub FunctionWorkOutResponseType_ScenerioTests()

        Dim X As VolumeMovementResponseMessage

        X = New VolumeMovementResponseMessage

        ConfigureVolumeMovementRepsonseMessage(X, String.Empty, String.Empty, False)
        Assert.AreEqual(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageFailure, X.WorkOutResponseType(), "WorkOutResponseType Test A")

        ConfigureVolumeMovementRepsonseMessage(X, String.Empty, String.Empty, True)
        Assert.AreEqual(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageFailure, X.WorkOutResponseType(), "WorkOutResponseType Test B")


        ConfigureVolumeMovementRepsonseMessage(X, "076", "080", True)
        Assert.AreEqual(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageSplit, X.WorkOutResponseType(), "WorkOutResponseType Test C")

        ConfigureVolumeMovementRepsonseMessage(X, "076", String.Empty, True)
        Assert.AreEqual(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageEstore, X.WorkOutResponseType(), "WorkOutResponseType Test D")

        ConfigureVolumeMovementRepsonseMessage(X, String.Empty, "080", True)
        Assert.AreEqual(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageHub, X.WorkOutResponseType(), "WorkOutResponseType Test E")

    End Sub

    <TestMethod()> Public Sub FunctionFormatResponseString_ScenerioTests()

        Dim X As VolumeMovementResponseMessage

        X = New VolumeMovementResponseMessage

        ConfigureVolumeMovementRepsonseMessage(X, "076", "080", True)

        Assert.AreEqual("abc<Store>def", _
                        X.FormatResponseString(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageEstore, "abc<Store>def"), _
                        "FormatResponseString - Test A")

        Assert.AreEqual("abc<Store>def", _
                        X.FormatResponseString(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageFailure, "abc<Store>def"), _
                        "FormatResponseString - Test B")

        Assert.AreEqual("abc080def", _
                        X.FormatResponseString(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageHub, "abc<Store>def"), _
                        "FormatResponseString - Test C")

        Assert.AreEqual("abc<store>def", _
                        X.FormatResponseString(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageHub, "abc<store>def"), _
                        "FormatResponseString - Test C Lowercase")

        Assert.AreEqual("abc080def", _
                        X.FormatResponseString(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageSplit, "abc<Store>def"), _
                        "FormatResponseString - Test D")

        Assert.AreEqual("abc<store>def", _
                        X.FormatResponseString(TextCommunicationVolumeMovementTypes.OrderFulfilledByMessageSplit, "abc<store>def"), _
                        "FormatResponseString - Test D Lowercase")

    End Sub

    <TestMethod()> Public Sub FunctionResponseMessage_ScenerioTests()

        'N/A

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Private Sub ConfigureVolumeMovementRepsonseMessage(ByRef X As VolumeMovementResponseMessage, _
                                                       ByVal EstoreFulfiller As String, ByVal HubFulfiller As String, ByVal SuccessFlag As Boolean)

        With X
            .EstoreFulfiller = EstoreFulfiller
            .HubFulfiller = HubFulfiller
            .SuccessFlag = SuccessFlag
        End With

    End Sub

#End Region

End Class