﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Public Class PriceMatchInstanceForTest
    Public Function GetPriceMatch(ByVal currentPrice As Double, ByVal competitorPrice As Double) As Double
        Throw New NotImplementedException()
    End Function
    Public Shared Function PriceDifference(ByVal currentPrice As Double, ByVal competitorPrice As Double) As Double
        Return currentPrice - competitorPrice
    End Function

    Public Function ValidateCompetitorName(ByVal competitorPrice As String) As Boolean
        If competitorPrice = String.Empty Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub New()

    End Sub
End Class
<TestClass()> Public Class PriceMatchUnitTests

    Private testContextInstance As TestContext

    Private priceMatchInstance As New PriceMatch.PriceMatchFormDetails
    Private priceMatchFormInstance As New PriceMatch.frmPriceMatchCaptureDetails

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub PriceMatchSetCurrentPriceReturnsCurrentPrice()
        priceMatchInstance.CurrentPrice = 9.28
        Assert.AreEqual(9.28, priceMatchInstance.CurrentPrice)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetPriceReturnsPrice()
        priceMatchInstance.Price = "9.28"
        Assert.AreEqual("9.28", priceMatchInstance.Price)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetOverridePriceReturnsOverridePrice()
        priceMatchInstance.Overrideprice = "9.28"
        Assert.AreEqual("9.28", priceMatchInstance.Overrideprice)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetTillIdReturnsTillId()
        priceMatchInstance.TillId = 55
        Assert.AreEqual(55, priceMatchInstance.TillId)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetTransactionNumberReturnsTransactionNumber()
        priceMatchInstance.TransactionNumber = 2233
        Assert.AreEqual(2233, priceMatchInstance.TransactionNumber)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetStoreIdReturnsStoreId()
        priceMatchInstance.StoreId = 8080
        Assert.AreEqual(8080, priceMatchInstance.StoreId)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetTransactionDateReturnsTransactionDate()
        priceMatchInstance.TransactionDate = CDate("2012-04-20")
        Assert.AreEqual(CDate("2012-04-20"), priceMatchInstance.TransactionDate)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetOriginalPriceReturnsOriginalPrice()
        priceMatchInstance.OriginalPrice = 15.4
        Assert.AreEqual(15.4, priceMatchInstance.OriginalPrice)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetSkuNumberReturnsSkuNumber()
        priceMatchInstance.SkuNumber = "100803"
        Assert.AreEqual("100803", priceMatchInstance.SkuNumber)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetProductDescriptionReturnsProductDescription()
        priceMatchInstance.ProductDescription = "Planed Softwood 18x28mmx1.8m PK10"
        Assert.AreEqual("Planed Softwood 18x28mmx1.8m PK10", priceMatchInstance.ProductDescription)
    End Sub
    <TestMethod()> Public Sub PriceMatchSetVatInclusiveReturnsVatInclusive()
        priceMatchInstance.VatInclusive = True
        Assert.AreEqual(True, priceMatchInstance.VatInclusive)
    End Sub

    '<TestMethod()> Public Sub PriceMatchFormSetCallsForm()
    '    Dim chkovverride As New PriceMatch.PriceMatchOverride
    '    chkovverride.CalculateOverridePrice("100803", "Planed Softwood 18x28mmx1.8m PK1", 8080, CDate("2012-04-24"), 5, 1233, 12.33, 20000, 20)
    '    'priceMatchFormInstance.CalculateOverridePrice("100803", "Planed Softwood 18x28mmx1.8m PK1", 8080, CDate("2012-04-24"), 55, 1233, 12.33, 20000, 20)
    'End Sub
    <TestMethod()> Public Sub PriceAfterPercentDiscount_Price100DiscountPercent10_Returns90()
        Dim Percentage As Double = 10
        Dim Price As String = "100"
        Assert.AreEqual(CDbl(90), priceMatchInstance.PriceAfterPercentDiscount(Price, Percentage))
    End Sub
    <TestMethod()> Public Sub PriceAfterPercentDiscount_Price200DiscountPercent10_Returns180()
        Dim Percentage As Double = 10
        Dim Price As String = "100"
        Assert.AreEqual(CDbl(90), priceMatchInstance.PriceAfterPercentDiscount(Price, Percentage))
    End Sub
    <TestMethod()> Public Sub PriceAfterPercentDiscount_Price100DiscountPercent20_Returns80()
        Dim Percentage As Double = 20
        Dim Price As String = "100"
        Assert.AreEqual(CDbl(80), priceMatchInstance.PriceAfterPercentDiscount(Price, Percentage))
    End Sub

    <TestMethod()> Public Sub Discount_Price100DiscountPercent20_Returns20()
        Dim Percentage As Double = 20
        Dim Price As String = "100"
        Assert.AreEqual(CDbl(20), priceMatchInstance.Discount(Price, Percentage))
    End Sub
    <TestMethod()> Public Sub Discount_Price100DiscountPercentMinus20_Returns120()
        Dim Percentage As Double = -20
        Dim Price As String = "100"
        Assert.AreEqual(CDbl(-20), priceMatchInstance.Discount(Price, Percentage))
    End Sub
    <TestMethod()> Public Sub Discount_Price100DiscountPercent100_Returns100()
        Dim Percentage As Double = 100
        Dim Price As String = "100"
        Assert.AreEqual(CDbl(100), priceMatchInstance.Discount(Price, Percentage))
    End Sub
    <TestMethod()> Public Sub Discount_Price100DiscountPercent0_Returns0()
        Dim Percentage As Double = 0
        Dim Price As String = "100"
        Assert.AreEqual(CDbl(0), priceMatchInstance.Discount(Price, Percentage))
    End Sub

    <TestMethod()> Public Sub PriceDifference_CurrentPrice100CompetitorPrice95_Returns5()
        Dim _temo As New PriceMatchInstanceForTest

        Dim CurrentPrice As Double = 100
        Dim CompetitorPrice As Double = 95
        Assert.AreEqual(CDbl(5), PriceMatchInstanceForTest.PriceDifference(CurrentPrice, CompetitorPrice))
    End Sub
    <TestMethod()> Public Sub PriceDifference_CurrentPrice100CompetitorPrice105_ReturnsMinus5()
        Dim _temo As New PriceMatchInstanceForTest

        Dim CurrentPrice As Double = 100
        Dim CompetitorPrice As Double = 105
        Assert.AreEqual(CDbl(-5), PriceMatchInstanceForTest.PriceDifference(CurrentPrice, CompetitorPrice))
    End Sub

    <TestMethod()> Public Sub CompetitorPriceIncludingVat_CompetitorPriceIncludesVat_ReturnsSameValue()
        Dim CompetitorPriceIncudingVat As String = "19.98"
        Assert.AreEqual(CDbl(19.98), priceMatchInstance.CompetitorPriceIncludingVat(CompetitorPriceIncudingVat))
    End Sub
    <TestMethod()> Public Sub CompetitorPriceIncludingVat_CompetitorPrice20_ReturnsHigherValue()

        Dim pricematchVatAdjustedInstance As New PriceMatchInstance_VatAdjustedCompetitorPriceReturns24

        Dim CompetitorPriceIncudingVat As String = "20"
        pricematchVatAdjustedInstance.VatInclusive = False
        pricematchVatAdjustedInstance.VatRate = 20
        Assert.IsTrue(pricematchVatAdjustedInstance.CompetitorPriceIncludingVat(CompetitorPriceIncudingVat) > CDbl(CompetitorPriceIncudingVat))
    End Sub

    <TestMethod()> Public Sub VatAdjustedCompetitorPrice_CompetitorPrice20VatRate20_Returns24()
        Dim CompetitorPriceIncudingVat As Double = 20
        priceMatchInstance.VatRate = 20
        Assert.AreEqual(CDbl(24), priceMatchInstance.VatAdjustedCompetitorPrice(CStr(CompetitorPriceIncudingVat)))
    End Sub

    '<TestMethod()> Public Sub ValidateCompetitorPrice_CompetitorPriceEmptyPassed_ReturnsTrue()
    '    ' Dim _chk As New _priceMatchInstance
    '    Dim CompetitorPrice As String = String.Empty
    '    Assert.AreEqual(True, priceMatchInstance.ValidateData(CompetitorPrice))
    'End Sub

    <TestMethod()> Public Sub ValidateCompetitorPrice_CompetitorPriceZeroPassed_ReturnsTrue()
        ' Dim _chk As New _priceMatchInstance
        Dim CompetitorPrice As String = "0"
        Assert.AreEqual(True, priceMatchInstance.ValidateData(CompetitorPrice))
    End Sub


    <TestMethod()> Public Sub ValidateCompetitorPrice_CompetitorPriceNonZeroPassed_ReturnsFalse()
        ' Dim _chk As New _priceMatchInstancet
        Dim CompetitorPrice As String = "2"
        Assert.AreEqual(False, priceMatchInstance.ValidateData(CompetitorPrice))
    End Sub

    <TestMethod()> Public Sub ValidateCompetitorName_CompetitorNameZeroPassed_ReturnsTrue()
        'Dim _chk As New _priceMatchInstance
        Dim CompetitorName As String = "0"
        Assert.AreEqual(True, priceMatchInstance.ValidateData(CompetitorName))
    End Sub

    '<TestMethod()> Public Sub ValidateCompetitorName_CompetitorNameNonEmptyPassed_ReturnsFalse()
    '    ' Dim _chk As New _priceMatchInstance
    '    Dim CompetitorName As String = "sdfsdfsdfsdf"
    '    Assert.AreEqual(False, priceMatchInstance.ValidateData(CompetitorName))
    'End Sub

    '<TestMethod()> Public Sub ValidateCompetitorPrice_CompetitorPriceNothingPassed_ReturnsTrue()
    '    ' Dim _chk As New _priceMatchInstance
    '    Dim CompetitorPrice As String = Nothing
    '    Assert.AreEqual(True, priceMatchInstance.ValidateData(CompetitorPrice))
    'End Sub

    '<TestMethod()> Public Sub ValidateCompetitorName_CompetitorNameNothingPassed_ReturnsTrue()
    '    'Dim _chk As New _priceMatchInstance
    '    Dim CompetitorName As String = Nothing
    '    Assert.AreEqual(True, priceMatchInstance.ValidateData(CompetitorName))
    'End Sub

    <TestMethod()> Public Sub ValidateOverridePrice_OverridePriceZeroPassed_ReturnsTrue()
        'Dim _chk As New _priceMatchInstance
        Dim OverridePrice As String = "0"
        Assert.AreEqual(True, priceMatchInstance.ValidateData(OverridePrice))
    End Sub

    <TestMethod()> Public Sub ValidateOverridePrice_OverridePriceNonZeroPassed_ReturnsFalse()
        'Dim _chk As New _priceMatchInstance
        Dim OverridePrice As String = "22.34"
        Assert.AreEqual(False, priceMatchInstance.ValidateData(OverridePrice))
    End Sub
    '<TestMethod()> Public Sub ValidateOverridePrice_OverridePriceNothingPassed_ReturnsTrue()
    '    'Dim _chk As New _priceMatchInstance
    '    Dim OverridePrice As String = Nothing
    '    Assert.AreEqual(True, priceMatchInstance.ValidateData(OverridePrice))
    'End Sub

    <TestMethod()> Public Sub ValidateOverrideAndCurrentPrice_OverridePriceHigherAndCurrentPriceLowerPassed_ReturnsTrue()
        'Dim _chk As New _priceMatchInstance
        Dim OverridePrice As Double = 25.89
        Dim CurrentPrice As Double = 24.99
        Assert.AreEqual(True, priceMatchInstance.ValidateOverrideAndCurrentPrice(OverridePrice, CurrentPrice))
    End Sub

    <TestMethod()> Public Sub ValidateOverrideAndCurrentPrice_OverridePriceLowerAndCurrentPriceHigherPassed_ReturnsFalse()
        'Dim _chk As New _priceMatchInstance
        Dim CurrentPrice As Double = 25.89
        Dim OverridePrice As Double = 24.99
        Assert.AreEqual(False, priceMatchInstance.ValidateOverrideAndCurrentPrice(OverridePrice, CurrentPrice))
    End Sub
    <TestMethod()> Public Sub ValidateOverrideAndCurrentPriceForSuspiciousPrice_CurrentPriceAndOverridePriceDifferenceHigherThanOrEqualTo1000_ReturnsTrue()
        'Dim _chk As New _priceMatchInstance
        Dim CurrentPrice As Double = 2000
        Dim OverridePrice As Double = 900
        Assert.AreEqual(True, priceMatchInstance.ValidateOverrideAndCurrentPriceForSuspiciousPrice(OverridePrice, CurrentPrice))
    End Sub
    <TestMethod()> Public Sub ValidateOverrideAndCurrentPriceForSuspiciousPrice_CurrentPriceAndOverridePriceDifferenceLessThan1000_ReturnsFalse()
        'Dim _chk As New _priceMatchInstance
        Dim CurrentPrice As Double = 400
        Dim OverridePrice As Double = 360
        Assert.AreEqual(False, priceMatchInstance.ValidateOverrideAndCurrentPriceForSuspiciousPrice(OverridePrice, CurrentPrice))
    End Sub

    <TestMethod()> Public Sub SelectedCompetitorName_ReturnsSelectedCompetitorName()
        priceMatchInstance.SelectedCompetitorName = "TileGaint"
        Assert.AreEqual("TileGaint", priceMatchInstance.SelectedCompetitorName)
    End Sub

    <TestMethod()> Public Sub FormatPencePrice_Provide0_Returns0Point00()
        Dim _price As Integer = 0
        Assert.AreEqual("0.00", priceMatchInstance.FormatPencePrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPencePrice_Provide2_ReturnsPoint02()
        Dim _price As Integer = 2
        Assert.AreEqual("0.02", priceMatchInstance.FormatPencePrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPencePrice_Provide20_ReturnsPoint20()
        Dim _price As Integer = 20
        Assert.AreEqual("0.20", priceMatchInstance.FormatPencePrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPencePrice_Provide111_Returns1Point11()
        Dim _price As Integer = 111
        Assert.AreEqual("1.11", priceMatchInstance.FormatPencePrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPrice_Provide0_Returns0Point00()
        Dim _price As String = "0"
        Assert.AreEqual("0.00", priceMatchInstance.FormatPrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPrice_Provide1Point23_Returns1Point23()
        Dim _price As String = "1.23"
        Assert.AreEqual("1.23", priceMatchInstance.FormatPrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPrice_Provide2PointPoint34_Returns2Point34()
        Dim _price As String = "2..34"
        Assert.AreEqual("2.34", priceMatchInstance.FormatPrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPrice_Provide2Point3Point4_Returns2Point34()
        Dim _price As String = "2.3.4"
        Assert.AreEqual("2.34", priceMatchInstance.FormatPrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPrice_ProvideEmptyString_ReturnsEmptyString()
        Dim _price As String = ""
        Assert.AreEqual("", priceMatchInstance.FormatPrice(_price))
    End Sub

    <TestMethod()> Public Sub FormatPrice_ProvideNothing_ReturnsEmptyString()
        Dim _price As String = Nothing
        Assert.AreEqual("", priceMatchInstance.FormatPrice(_price))
    End Sub


End Class

Public Class PriceMatchInstance_VatAdjustedCompetitorPriceReturns24
    Inherits PriceMatch.PriceMatchFormDetails

    Friend Overrides Function VatAdjustedCompetitorPrice(ByVal competitorPriceIncudingVat As String) As Double
        Return 24
    End Function

End Class
