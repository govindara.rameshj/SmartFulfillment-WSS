﻿<TestClass()> Public Class SaleCouponUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : SaleCoupon | Public Property : Status"

    <TestMethod()> Public Sub PropertyStatus_InitialValue_Active_ReturnValue_Active()

        Dim X As New Sale.SaleCoupon

        X.Status = CouponStatus.Active

        Assert.AreEqual(CouponStatus.Active, X.Status)

    End Sub

    <TestMethod()> Public Sub PropertyStatus_InitialValue_CouponReturnedAsNotUsed_ReturnValue_CouponReturnedAsNotUsed()

        Dim X As New Sale.SaleCoupon

        X.Status = CouponStatus.CouponReturnedAsNotUsed

        Assert.AreEqual(CouponStatus.CouponReturnedAsNotUsed, X.Status)

    End Sub

    <TestMethod()> Public Sub PropertyStatus_InitialValue_EventNotActive_ReturnValue_EventNotActive()

        Dim X As New Sale.SaleCoupon

        X.Status = CouponStatus.EventNotActive

        Assert.AreEqual(CouponStatus.EventNotActive, X.Status)

    End Sub

    <TestMethod()> Public Sub PropertyStatus_InitialValue_NoEventExists_ReturnValue_NoEventExists()

        Dim X As New Sale.SaleCoupon

        X.Status = CouponStatus.NoEventExists

        Assert.AreEqual(CouponStatus.NoEventExists, X.Status)

    End Sub

    <TestMethod()> Public Sub PropertyStatus_InitialValue_ReversedAtTill_ReturnValue_ReversedAtTill()

        Dim X As New Sale.SaleCoupon

        X.Status = CouponStatus.ReversedAtTill

        Assert.AreEqual(CouponStatus.ReversedAtTill, X.Status)

    End Sub

    <TestMethod()> Public Sub PropertyStatus_InitialValue_StoreIssuedAsReward_ReturnValue_StoreIssuedAsReward()

        Dim X As New Sale.SaleCoupon

        X.Status = CouponStatus.StoreIssuedAsReward

        Assert.AreEqual(CouponStatus.StoreIssuedAsReward, X.Status)

    End Sub

    <TestMethod()> Public Sub PropertyStatus_InitialValue_UnAppliedAndReturnedToCustomer_ReturnValue_UnAppliedAndReturnedToCustomer()

        Dim X As New Sale.SaleCoupon

        X.Status = CouponStatus.UnAppliedAndReturnedToCustomer

        Assert.AreEqual(CouponStatus.UnAppliedAndReturnedToCustomer, X.Status)

    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentNullException), "Value cannot be null")> Public Sub PropertyStatus_InitialValue_Null_ReturnValue_ArgumentNullException()

        Dim X As New Sale.SaleCoupon
        Dim Y As CouponStatus

        Y = X.Status

    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentException), "Value cannot be empty")> Public Sub PropertyStatus_InitialValue_Empty_ReturnValue_ArgumentArgumentException()

        Dim X As New Sale.SaleCoupon
        Dim Y As CouponStatus

        X._Status = String.Empty

        Y = X.Status

    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentException), "Value cannot be blank")> Public Sub PropertyStatus_InitialValue_Blank_ReturnValue_ArgumentArgumentException()

        Dim X As New Sale.SaleCoupon
        Dim Y As CouponStatus

        X._Status = "     "

        Y = X.Status

    End Sub

    <TestMethod()> <ExpectedException(GetType(System.InvalidOperationException), "Value not in range")> Public Sub PropertyStatus_InitialValue_OutOfRange_ReturnValue_ArgumentInvalidOperationException()

        Dim X As New Sale.SaleCoupon
        Dim Y As CouponStatus

        X.Status = CType("999", CouponStatus)

        Y = X.Status

    End Sub

#End Region

#Region "Class : SaleCoupon(s) | Public Function : Initialise"

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNothing_TillIdNothing_TransactionNoNothing_ExpectInternalValue_TransactionDateMinValue()

        Dim X As New SaleCoupons

        X.Initialise(Nothing, Nothing, Nothing)

        Assert.AreEqual(DateTime.MinValue, X._TransactionDate)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNothing_TillIdNothing_TransactionNoNothing_ExpectInternalValue_TillIdNothing()

        Dim X As New SaleCoupons

        X.Initialise(Nothing, Nothing, Nothing)

        Assert.IsNull(X._TillID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNothing_TillIdNothing_TransactionNoNothing_ExpectInternalValue_TransactionNoNothing()

        Dim X As New SaleCoupons

        X.Initialise(Nothing, Nothing, Nothing)

        Assert.IsNull(X._TransactionNo)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNothing_TillIdNothing_TransactionNoNothing_ReturnValueFalse()

        Dim X As New SaleCoupons

        Assert.IsFalse(X.Initialise(Nothing, Nothing, Nothing))

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNothing_TillIdOne_TransactionNoOne_ExpectInternalValue_TransactionDateMinValue()

        Dim X As New SaleCoupons

        X.Initialise(Nothing, "1", "1")

        Assert.AreEqual(DateTime.MinValue, X._TransactionDate)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNothing_TillIdOne_TransactionNoOne_ReturnValueFalse()

        Dim X As New SaleCoupons

        Assert.IsFalse(X.Initialise(Nothing, "1", "1"))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdNothing_TransactionNoOne_ExpectInternalValue_TillIdNothing()

        Dim X As New SaleCoupons

        X.Initialise(Now, Nothing, "1")

        Assert.IsNull(X._TillID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdNothing_TransactionNoOne_ReturnValueFalse()

        Dim X As New SaleCoupons

        Assert.IsFalse(X.Initialise(Now, Nothing, "1"))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdOne_TransactionNoNothing_ExpectInternalValue_TransactionNoNothing()

        Dim X As New SaleCoupons

        X.Initialise(Now, "1", Nothing)

        Assert.IsNull(X._TransactionNo)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdOne_TransactionNoNothing_ReturnValueFalse()

        Dim X As New SaleCoupons

        Assert.IsFalse(X.Initialise(Now, "1", Nothing))

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateDefaultValue_TillIdEmpty_TransactionNoEmpty_ExpectInternalValue_TransactionDateMinValue()

        Dim X As New SaleCoupons

        X.Initialise(DateTime.MinValue, String.Empty, String.Empty)

        Assert.AreEqual(DateTime.MinValue, X._TransactionDate)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateDefaultValue_TillIdEmpty_TransactionNoEmpty_ExpectInternalValue_TillIdNothing()

        Dim X As New SaleCoupons

        X.Initialise(DateTime.MinValue, String.Empty, String.Empty)

        Assert.IsNull(X._TillID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateDefaultValue_TillIdEmpty_TransactionNoEmpty_ExpectInternalValue_TransactionNoNothing()

        Dim X As New SaleCoupons

        X.Initialise(DateTime.MinValue, String.Empty, String.Empty)

        Assert.IsNull(X._TransactionNo)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateDefaultValue_TillIdEmpty_TransactionNoEmpty_ReturnvalueFalse()

        Dim X As New SaleCoupons

        Assert.IsFalse(X.Initialise(DateTime.MinValue, String.Empty, String.Empty))

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateDefaultValue_TillIdOne_TransactionNoOne_ExpectInternalValue_TransactionDateMinValue()

        Dim X As New SaleCoupons

        X.Initialise(DateTime.MinValue, "1", "1")

        Assert.AreEqual(DateTime.MinValue, X._TransactionDate)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateDefaultValue_TillIdOne_TransactionNoOne_ReturnValueFalse()

        Dim X As New SaleCoupons

        Assert.IsFalse(X.Initialise(DateTime.MinValue, "1", "1"))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdEmpty_TransactionNoOne_ExpectInternalValue_TillIdNothing()

        Dim X As New SaleCoupons

        X.Initialise(Now, String.Empty, "1")

        Assert.IsNull(X._TillID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdEmpty_TransactionNoOne_ReturnValueFalse()

        Dim X As New SaleCoupons

        Assert.IsFalse(X.Initialise(Now, String.Empty, "1"))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdOne_TransactionNoEmpty_ExpectInternalValue_TransactionNoNothing()

        Dim X As New SaleCoupons

        X.Initialise(Now, "1", String.Empty)

        Assert.IsNull(X._TransactionNo)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdOne_TransactionNoEmpty_ReturnValueFalse()

        Dim X As New SaleCoupons

        Assert.IsFalse(X.Initialise(Now, "1", String.Empty))

    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdOne_TransactionNoOne_ExpectInternalValue_TransactionDateNow()

        Dim Mock As New MockRepository
        Dim Repository As IGetSaleCouponRepository
        Dim DT As DataTable

        Dim X As New SaleCoupons
        Dim Y As DateTime

        'arrange; stubbed out call to database
        Repository = Mock.Stub(Of IGetSaleCouponRepository)()
        DT = Nothing

        SetupResult.On(Repository).Call(Repository.GetSaleCoupon(Nothing, Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetSaleCouponRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        Y = Now
        'act
        X.Initialise(Y, "1", "1")

        Assert.AreEqual(Y, X._TransactionDate)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdOne_TransactionNoOne_ExpectInternalValue_TillIdOne()

        Dim Mock As New MockRepository
        Dim Repository As IGetSaleCouponRepository
        Dim DT As DataTable

        Dim X As New SaleCoupons

        'arrange; stubbed out call to database
        Repository = Mock.Stub(Of IGetSaleCouponRepository)()
        DT = Nothing

        SetupResult.On(Repository).Call(Repository.GetSaleCoupon(Nothing, Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetSaleCouponRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        'act
        X.Initialise(Now, "1", "1")

        Assert.AreEqual("1", X._TillID)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdOne_TransactionNoOne_ExpectInternalValue_TransactionNoOne()

        Dim Mock As New MockRepository
        Dim Repository As IGetSaleCouponRepository
        Dim DT As DataTable

        Dim X As New SaleCoupons

        'arrange; stubbed out call to database
        Repository = Mock.Stub(Of IGetSaleCouponRepository)()
        DT = Nothing

        SetupResult.On(Repository).Call(Repository.GetSaleCoupon(Nothing, Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetSaleCouponRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        'act
        X.Initialise(Now, "1", "1")

        Assert.AreEqual("1", X._TransactionNo)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_TransactionDateNow_TillIdOne_TransactionNoOne_ValueTrue()

        Dim Mock As New MockRepository
        Dim Repository As IGetSaleCouponRepository
        Dim DT As DataTable

        Dim X As New SaleCoupons

        'arrange; stubbed out call to database
        Repository = Mock.Stub(Of IGetSaleCouponRepository)()
        DT = Nothing

        SetupResult.On(Repository).Call(Repository.GetSaleCoupon(Nothing, Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetSaleCouponRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

        Assert.IsTrue(X.Initialise(Now, "1", "1"))

    End Sub

#End Region

#Region "Class : SaleCoupon(s) | Public Function : FirstLine"

#End Region

#Region "Class : SaleCoupon(s) | Public Function : NextLine"

#End Region

#Region "Class : SaleCoupon(s) | Private Procedure : Load - TransactionDate, TillID, TransactionNumber"

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableNothing_ReturnValue_CollectionCountZero()

        Dim X As New SaleCoupons

        ConfigureSaleCouponRepository(X, Nothing)

        X.Load(Nothing, Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New SaleCoupons

        ConfigureSaleCouponRepository(X, Nothing)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableEmpty_ReturnValue_CollectionCountZero()

        Dim X As New SaleCoupons

        ConfigureSaleCouponRepository(X, SaleCouponDataTableConfigureStructure)

        X.Load(Nothing, Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableEmpty_ReturnValue_ExistInDatabaseFalse()

        Dim X As New SaleCoupons

        ConfigureSaleCouponRepository(X, SaleCouponDataTableConfigureStructure)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableOneRecord_ReturnValue_CollectionCountOne()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "9", False, "", False, "", "", "", "")
        ConfigureSaleCouponRepository(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.AreEqual(1, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableOneRecord_ReturnValue_ExistInDatabaseTrue()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "9", False, "", False, "", "", "", "")
        ConfigureSaleCouponRepository(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableSevenRecords_ReturnValue_CollectionCountSeven()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        'arrange
        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "0", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "1", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "2", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "3", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "7", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "8", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "9", False, "", False, "", "", "", "")
        ConfigureSaleCouponRepository(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.AreEqual(7, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_DataTableSevenRecords_ReturnValue_ExistInDatabaseTrue()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        'arrange
        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "0", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "1", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "2", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "3", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "7", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "8", False, "", False, "", "", "", "")
        SaleCouponDataTableAddData(DT, "080", Now, "1", "1", "1", "1", "9", False, "", False, "", "", "", "")
        ConfigureSaleCouponRepository(X, DT)

        X.Load(Nothing, Nothing, Nothing)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadThreeParameter_BusinessObjectLoadsDataTableCorrectly()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        'arrange
        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(1), "1", "10", "21", "31", "0", False, "A", False, "90", "101", "201", "Z")
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(2), "2", "11", "22", "32", "1", False, "B", False, "91", "102", "202", "Y")
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(3), "3", "12", "23", "33", "2", False, "C", False, "92", "103", "203", "X")
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(4), "4", "13", "24", "34", "3", False, "D", False, "93", "104", "204", "W")
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(5), "5", "14", "25", "35", "7", False, "E", False, "94", "105", "205", "V")
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(6), "6", "15", "26", "36", "8", False, "F", False, "95", "106", "206", "U")
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(7), "7", "16", "27", "37", "9", False, "G", False, "96", "107", "207", "T")
        ConfigureSaleCouponRepository(X, DT)

        'act
        X.Load(Nothing, Nothing, Nothing)

        Assert.IsTrue(MatchBusinessObjectAgainstDataTable(X, DT))

    End Sub

#End Region

#Region "Class : SaleCoupon(s) | Private Procedure : Load - TransactionDate, TillID, TransactionNumber, SequenceNumber"

    <TestMethod()> Public Sub ProcedureLoadFourParameter_DataTableNothing_ReturnValue_CollectionCountZero()

        Dim X As New SaleCoupons

        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadFourParameter_DataTableNothing_ReturnValue_ExistInDatabaseFalse()

        Dim X As New SaleCoupons

        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadFourParameter_DataTableEmpty_ReturnValue_CollectionCountZero()

        Dim X As New SaleCoupons

        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, SaleCouponDataTableConfigureStructure)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadFourParameter_DataTableEmpty_ReturnValue_ExistInDatabaseFalse()

        Dim X As New SaleCoupons

        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, SaleCouponDataTableConfigureStructure)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadFourParameter_DataTableOneRecord_ReturnValue_CollectionCountOne()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        'arrange
        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(1), "1", "10", "21", "31", "0", False, "A", False, "90", "101", "201", "Z")
        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(1, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadFourParameter_DataTableOneRecord_ReturnValue_ExistInDatabaseTrue()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        'arrange
        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(1), "1", "10", "21", "31", "0", False, "A", False, "90", "101", "201", "Z")
        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadFourParameter_DataTableTwoRecord_ReturnValue_CollectionCountZero()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        'arrange
        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(1), "1", "10", "21", "31", "0", False, "A", False, "90", "101", "201", "Z")
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(2), "2", "11", "22", "32", "1", False, "B", False, "91", "102", "202", "Y")
        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(0, X.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadFourParameter_DataTableTwoRecord_ReturnValue_ExistInDatabaseFalse()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        'arrange
        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(1), "1", "10", "21", "31", "0", False, "A", False, "90", "101", "201", "Z")
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(2), "2", "11", "22", "32", "1", False, "B", False, "91", "102", "202", "Y")
        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, DT)

        'act
        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsFalse(X._ExistsInDB)

    End Sub

    <TestMethod()> Public Sub ProcedureLoadFourParameter_BusinessObjectLoadsDataTableCorrectly()

        Dim X As New SaleCoupons
        Dim DT As New DataTable

        'arrange
        DT = SaleCouponDataTableConfigureStructure()
        SaleCouponDataTableAddData(DT, "080", Now.AddSeconds(1), "1", "10", "21", "31", "0", False, "A", False, "90", "101", "201", "Z")
        ConfigureSaleCouponRepositoryIncludeSequenceNumber(X, DT)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(MatchBusinessObjectAgainstDataTable(X, DT))

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub ConfigureSaleCouponRepository(ByRef X As SaleCoupons, ByRef DT As DataTable)

        Dim Mock As New MockRepository
        Dim Repository As IGetSaleCouponRepository

        'arrange
        Repository = Mock.Stub(Of IGetSaleCouponRepository)()
        SetupResult.On(Repository).Call(Repository.GetSaleCoupon(Nothing, Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetSaleCouponRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Sub ConfigureSaleCouponRepositoryIncludeSequenceNumber(ByRef X As SaleCoupons, ByRef DT As DataTable)

        Dim Mock As New MockRepository
        Dim Repository As IGetSaleCouponRepository

        'arrange
        Repository = Mock.Stub(Of IGetSaleCouponRepository)()
        SetupResult.On(Repository).Call(Repository.GetSaleCoupon(Nothing, Nothing, Nothing, Nothing)).Return(DT).IgnoreArguments()
        GetSaleCouponRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Function SaleCouponDataTableConfigureStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns
            .Add("StoreID", System.Type.GetType("System.String"))
            .Add("TranDate", System.Type.GetType("System.DateTime"))
            .Add("TranTillID", System.Type.GetType("System.String"))
            .Add("TranNo", System.Type.GetType("System.String"))
            .Add("Sequence", System.Type.GetType("System.String"))
            .Add("CouponID", System.Type.GetType("System.String"))
            .Add("Status", System.Type.GetType("System.String"))
            .Add("ExcCoupon", System.Type.GetType("System.Boolean"))
            .Add("MarketRef", System.Type.GetType("System.String"))
            .Add("ReUsable", System.Type.GetType("System.Boolean"))
            .Add("IssueStoreNo", System.Type.GetType("System.String"))
            .Add("SerialNo", System.Type.GetType("System.String"))
            .Add("ManagerID", System.Type.GetType("System.String"))
            .Add("RTIFlag", System.Type.GetType("System.String"))
        End With

        Return DT

    End Function

    Friend Sub SaleCouponDataTableAddData(ByRef DT As DataTable, _
                                          ByVal StoreID As String, ByVal TransactionDate As Date, _
                                          ByVal TransactionTillID As String, ByVal TransactionNumber As String, _
                                          ByVal SequenceNo As String, ByVal CouponID As String, _
                                          ByVal Status As String, ByVal Exclusive As Boolean, _
                                          ByVal MarketReference As String, ByVal Reusable As Boolean, _
                                          ByVal IssuingStoreNumber As String, ByVal SerialNumber As String, _
                                          ByVal ManagerID As String, ByVal RtiFlag As String)

        DT.Rows.Add(StoreID, TransactionDate, TransactionTillID, TransactionNumber, SequenceNo, CouponID, _
                    Status, Exclusive, MarketReference, Reusable, IssuingStoreNumber, SerialNumber, ManagerID, RtiFlag)

    End Sub

    Private Function MatchBusinessObjectAgainstDataTable(ByRef BO As SaleCoupons, ByRef DT As DataTable) As Boolean

        'assume that business object & datatable sorting order are consistent

        Dim RowIndex As Integer
        Dim Temp As String

        Try
            RowIndex = 0
            For Each X As SaleCoupon In BO

                If X.StoreID <> CType(DT.Rows.Item(RowIndex).Item(0), String) Then Return False
                If X.TransactionDate <> CType(DT.Rows.Item(RowIndex).Item(1), Date) Then Return False
                If X.TransactionTillID <> CType(DT.Rows.Item(RowIndex).Item(2), String) Then Return False
                If X.TransactionNumber <> CType(DT.Rows.Item(RowIndex).Item(3), String) Then Return False
                If X.SequenceNumber <> CType(DT.Rows.Item(RowIndex).Item(4), String) Then Return False
                If X.CouponID <> CType(DT.Rows.Item(RowIndex).Item(5), String) Then Return False

                Temp = String.Empty
                Select Case X.Status
                    Case CouponStatus.Active
                        Temp = "0"
                    Case CouponStatus.NoEventExists
                        Temp = "1"
                    Case CouponStatus.EventNotActive
                        Temp = "2"
                    Case CouponStatus.CouponReturnedAsNotUsed
                        Temp = "3"
                    Case CouponStatus.ReversedAtTill
                        Temp = "7"
                    Case CouponStatus.UnAppliedAndReturnedToCustomer
                        Temp = "8"
                    Case CouponStatus.StoreIssuedAsReward
                        Temp = "9"
                End Select
                If Temp <> CType(DT.Rows.Item(RowIndex).Item(6), String) Then Return False

                If X.Exclusive <> CType(DT.Rows.Item(RowIndex).Item(7), Boolean) Then Return False
                If X.MarketReference <> CType(DT.Rows.Item(RowIndex).Item(8), String) Then Return False
                If X.Reusable <> CType(DT.Rows.Item(RowIndex).Item(9), Boolean) Then Return False
                If X.IssuingStoreNumber <> CType(DT.Rows.Item(RowIndex).Item(10), String) Then Return False
                If X.SerialNumber <> CType(DT.Rows.Item(RowIndex).Item(11), String) Then Return False
                If X.ManagerID <> CType(DT.Rows.Item(RowIndex).Item(12), String) Then Return False
                If X.RtiFlag <> CType(DT.Rows.Item(RowIndex).Item(13), String) Then Return False

                RowIndex += 1
            Next

        Catch ex As Exception

            Return False

        End Try

        Return True

    End Function

#End Region

End Class
