﻿<TestClass()> Public Class EventFactoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory - TpWickes.Till.Implementation.Event"

    <TestMethod()> Public Sub EventOverrideHeaderFactory_ReturnCorrectObject()

        Dim X As IEventOverrideHeader

        X = EventOverrideHeaderFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.EventOverrideHeader", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EventOverrideHeadersFactory_ReturnCorrectObject()

        Dim X As IEventOverrideHeaders

        X = EventOverrideHeadersFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.EventOverrideHeaders", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EventOverrideMasterFactory_ReturnCorrectObject()

        Dim X As IEventOverrideMaster

        X = EventOverrideMasterFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.EventOverrideMaster", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EventOverrideMastersFactory_ReturnCorrectObject()

        Dim X As IEventOverrideMasters

        X = EventOverrideMastersFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.EventOverrideMasters", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EventExistingHeaderFactory_ReturnCorrectObject()

        Dim X As IEventExistingHeader

        X = EventExistingHeaderFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.EventExistingHeader", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EventExistingHeadersFactory_ReturnCorrectObject()

        Dim X As IEventExistingHeaders

        X = EventExistingHeadersFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.EventExistingHeaders", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EventExistingMasterFactory_ReturnCorrectObject()

        Dim X As IEventExistingMaster

        X = EventExistingMasterFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.EventExistingMaster", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EventExistingMastersFactory_ReturnCorrectObject()

        Dim X As IEventExistingMasters

        X = EventExistingMastersFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.EventExistingMasters", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub ErosionPercentageFactory_ReturnCorrectObject()

        Dim X As IErosionPercentage

        X = ErosionPercentageFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.ErosionPercentage", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub ErosionPercentagesFactory_ReturnCorrectObject()

        Dim X As IErosionPercentages

        X = ErosionPercentagesFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.ErosionPercentages", X.GetType.FullName)

    End Sub

#End Region

End Class