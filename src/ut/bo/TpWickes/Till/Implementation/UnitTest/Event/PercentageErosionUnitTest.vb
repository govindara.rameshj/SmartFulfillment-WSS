﻿<TestClass()> Public Class PercentageErosionUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : PercentageErosion | N/A"

#End Region

#Region "Class : PercentageErosions"

#Region "Class : PercentageErosions | Public Procedure : Add"

    <TestMethod()> Public Sub ProcedureAdd_ZeroObjectAdded_ReturnCollectionZero()

        Dim X As New ErosionPercentages

        Assert.AreEqual(0, X._ErosionPercentageList.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureAdd_OneObjectAdded_ReturnCollectionOne()

        Dim X As New ErosionPercentages

        X.Add("100803", 1, 10.99)

        Assert.AreEqual(1, X._ErosionPercentageList.Count)

    End Sub

    <TestMethod()> Public Sub ProcedureAdd_ThreeObjectAdded_ReturnCollectionThree()

        Dim X As New ErosionPercentages

        X.Add("100803", 1, 10.99)
        X.Add("100803", 1, 10.99)
        X.Add("100803", 1, 10.99)

        Assert.AreEqual(3, X._ErosionPercentageList.Count)

    End Sub

#End Region

#Region "Class : PercentageErosions | Public Procedure : GenerateErosionPrice"

    <TestMethod()> Public Sub ProcedureGenerateErosionPrice()

        Dim X As New ErosionPercentages

        'empty list
        X.GenerateErosionPrice()

        Assert.AreEqual(0, X._ErosionPercentageList.Count, "GenerateErosionPrice - Test 3")                     'erosion percentage calculation not performed

        'invalid quantity
        X._ErosionPercentageList.Clear()

        X.Add("160293", 1, 40)
        X.Add("223115", Nothing, 40)
        X.Add("223116", 1, 80)
        X.GenerateErosionPrice()

        Assert.IsFalse(ErosionPriceCalculated(X._ErosionPercentageList), "GenerateErosionPrice - Test 4")       'erosion percentage calculation not performed

        'invalid price
        X._ErosionPercentageList.Clear()

        X.Add("160293", 1, 40)
        X.Add("223115", 1, Nothing)
        X.Add("223116", 1, 80)
        X.GenerateErosionPrice()

        Assert.IsFalse(ErosionPriceCalculated(X._ErosionPercentageList), "GenerateErosionPrice - Test 5")       'erosion percentage calculation not performed

        'erosion price calculation performed
        X._ErosionPercentageList.Clear()

        X.Add("160293", 1, 40)
        X.Add("223115", 1, 40)
        X.Add("223116", 1, 80)
        X.GenerateErosionPrice()

        Assert.IsTrue(ErosionPriceCalculated(X._ErosionPercentageList), "GenerateErosionPrice - Test 6")       'erosion percentage calculation performed

    End Sub

#End Region

#Region "Class : PercentageErosions | Private Function : SkuQuantitiesValid"

    <TestMethod()> Public Sub FunctionSkuQuantitiesValid_ValidQuantitiesExist_ReturnTrue()

        Dim X As New ErosionPercentages

        X.Add("160293", 1, 48)
        X.Add("223115", 1, 49)
        X.Add("223116", 1, 49)

        Assert.IsTrue(X.SkuQuantitiesValid(X._ErosionPercentageList))

    End Sub

    <TestMethod()> Public Sub FunctionSkuQuantitiesValid_ValidQuantitiesNotExist_ReturnFalse()

        Dim X As New ErosionPercentages

        X.Add("160293", 1, 48)
        X.Add("223115", Nothing, 49)
        X.Add("223116", 1, 49)

        Assert.IsFalse(X.SkuQuantitiesValid(X._ErosionPercentageList))

    End Sub

#End Region

#Region "Class : PercentageErosions | Private Function : SkuSellingPricesValid"

    <TestMethod()> Public Sub FunctionSkuSellingPricesValid_ValidPricesExist_ReturnTrue()

        Dim X As New ErosionPercentages

        X.Add("160293", 1, 48)
        X.Add("223115", 1, 49)
        X.Add("223116", 1, 49)

        Assert.IsTrue(X.SkuSellingPricesValid(X._ErosionPercentageList))

    End Sub

    <TestMethod()> Public Sub FunctionSkuSellingPricesValid_ValidPricesNotExist_ReturnFalse()

        Dim X As New ErosionPercentages

        X.Add("160293", 1, 48)
        X.Add("223115", 1, Nothing)
        X.Add("223116", 1, 49)

        Assert.IsFalse(X.SkuSellingPricesValid(X._ErosionPercentageList))

    End Sub

#End Region

#Region "Class : PercentageErosions | Private Procedure : CalculateSkuErosionPercentages"

    <TestMethod()> Public Sub ProcedureCalculateSkuErosionPercentagesScenerioOne_ReturnCorrectValues()

        Dim X As New ErosionPercentages

        'total price : 255.36
        X.Add("160293", 1, 48)
        X.Add("223115", 1, 49)
        X.Add("223116", 1, 49)
        X.Add("404021", 1, 88.32)
        X.Add("407810", 1, 21.04)

        X.CalculateSkuErosionPercentages(X._ErosionPercentageList)

        Assert.AreEqual(18.797, X._ErosionPercentageList.Item(0).ErosionPercentage, "SkuErosionPercentage - Test 1")
        Assert.AreEqual(19.189, X._ErosionPercentageList.Item(1).ErosionPercentage, "SkuErosionPercentage - Test 2")
        Assert.AreEqual(19.189, X._ErosionPercentageList.Item(2).ErosionPercentage, "SkuErosionPercentage - Test 3")
        Assert.AreEqual(34.586, X._ErosionPercentageList.Item(3).ErosionPercentage, "SkuErosionPercentage - Test 4")
        Assert.AreEqual(8.239, X._ErosionPercentageList.Item(4).ErosionPercentage, "SkuErosionPercentage - Test 5")

    End Sub

    <TestMethod()> Public Sub ProcedureCalculateSkuErosionPercentagesScenerioTwo_ReturnCorrectValues()

        Dim X As New ErosionPercentages

        'total price : 401.36
        X.Add("160293", 2, 48)
        X.Add("223115", 1, 49)
        X.Add("223116", 3, 49)
        X.Add("404021", 1, 88.32)
        X.Add("407810", 1, 21.04)
        X.CalculateSkuErosionPercentages(X._ErosionPercentageList)

        Assert.AreEqual(23.919, X._ErosionPercentageList.Item(0).ErosionPercentage, "SkuErosionPercentage - Test 1")
        Assert.AreEqual(12.208, X._ErosionPercentageList.Item(1).ErosionPercentage, "SkuErosionPercentage - Test 2")
        Assert.AreEqual(36.625, X._ErosionPercentageList.Item(2).ErosionPercentage, "SkuErosionPercentage - Test 3")
        Assert.AreEqual(22.005, X._ErosionPercentageList.Item(3).ErosionPercentage, "SkuErosionPercentage - Test 4")
        Assert.AreEqual(5.243, X._ErosionPercentageList.Item(4).ErosionPercentage, "SkuErosionPercentage - Test 5")

    End Sub

    <TestMethod()> Public Sub ProcedureCalculateSkuErosionPercentagesScenerioThree_ReturnCorrectValues()

        Dim X As New ErosionPercentages

        'total price : 99.99
        X.Add("160293", 1, 33.33)
        X.Add("223115", 1, 33.33)
        X.Add("223116", 1, 33.33)
        X.CalculateSkuErosionPercentages(X._ErosionPercentageList)

        Assert.AreEqual(33.333, X._ErosionPercentageList.Item(0).ErosionPercentage, "SkuErosionPercentage - Test 1")
        Assert.AreEqual(33.333, X._ErosionPercentageList.Item(1).ErosionPercentage, "SkuErosionPercentage - Test 2")
        Assert.AreEqual(33.334, X._ErosionPercentageList.Item(2).ErosionPercentage, "SkuErosionPercentage - Test 3")

    End Sub

#End Region

#Region "Class : PercentageErosions | Private Function : ErosionPercentageDescrepency"

    <TestMethod()> Public Sub FunctionErosionPercentageDescrepency_ReturnCorrectValues()

        Dim X As New ErosionPercentages

        X.Add("160293", Nothing, Nothing)
        X.Add("223115", Nothing, Nothing)
        X.Add("223116", Nothing, Nothing)

        X._ErosionPercentageList.Item(0).ErosionPercentage = 50.0
        X._ErosionPercentageList.Item(1).ErosionPercentage = 30.0
        X._ErosionPercentageList.Item(2).ErosionPercentage = 20.0

        Assert.AreEqual(0.0, X.ErosionPercentageDescrepency(X._ErosionPercentageList), "ErosionPercentageDescrepency - Test 1")

        X._ErosionPercentageList.Item(0).ErosionPercentage = 10.0
        X._ErosionPercentageList.Item(1).ErosionPercentage = 10.0
        X._ErosionPercentageList.Item(2).ErosionPercentage = 10.0

        Assert.AreEqual(70.0, X.ErosionPercentageDescrepency(X._ErosionPercentageList), "ErosionPercentageDescrepency - Test 2")

    End Sub

#End Region

#Region "Class : PercentageErosions | Private Function : SkuErosionPercentage"

    <TestMethod()> Public Sub FunctionSkuErosionPercentage_ReturnCorrectValues()

        Dim X As New ErosionPercentages

        Assert.AreEqual(0.0, X.SkuErosionPercentage(0, 10, 100), "SkuErosionPercentage - Test 1: Zero Quantity")
        Assert.AreEqual(0.0, X.SkuErosionPercentage(1, 0, 100), "SkuErosionPercentage - Test 1: Zero Price")
        Assert.AreEqual(0.1, X.SkuErosionPercentage(1, 10, 100), "SkuErosionPercentage - Test 2")
        Assert.AreEqual(0.33333, X.SkuErosionPercentage(1, 33.33, 99.99), "SkuErosionPercentage - Test 3")

    End Sub

#End Region

#Region "Class : PercentageErosions | Private Function : RoundValue"

    <TestMethod()> Public Sub FunctionRoundValue_ReturnCorrectValues()

        Dim X As New ErosionPercentages

        Assert.AreEqual(0.0, X.RoundValue(0.0, 5), "RoundValue - Test 1")
        Assert.AreEqual(0.1, X.RoundValue(0.1, 5), "RoundValue - Test 2")
        Assert.AreEqual(0.33333, X.RoundValue(0.333334, 5), "RoundValue - Test 3")
        Assert.AreEqual(0.33334, X.RoundValue(0.333335, 5), "RoundValue - Test 4")

    End Sub

#End Region

#Region "Class : PercentageErosions | Class : PercentageErosion | Public Property : Sku"

    <TestMethod()> Public Sub PropertySku_ReturnCorrectValue()

        Dim X As New ErosionPercentage
        Dim Y As New ErosionPercentages

        X.Sku = "100803"
        Assert.AreEqual("100803", X._Sku, "Property : Sku - Test 1")

        X._Sku = "100805"
        Assert.AreEqual("100805", X.Sku, "Property : Sku - Test 2")

        Y.Add("100803", 25, 100.345)
        Assert.AreEqual("100803", Y._ErosionPercentageList.Item(0).Sku, "Property : Sku - Test 3")

    End Sub

#End Region

#Region "Class : PercentageErosions | Class : PercentageErosion | Public Property : Quantity"

    <TestMethod()> Public Sub PropertyQuantity_ReturnCorrectValue()

        Dim X As New ErosionPercentage
        Dim Y As New ErosionPercentages

        X.Quantity = 15
        Assert.AreEqual(15, X._Quantity, "Property : Quantity - Test 1")

        X._Quantity = 15
        Assert.AreEqual(15, X.Quantity, "Property : Quantity - Test 2")

        Y.Add("100803", 25, 100.345)
        Assert.AreEqual(25, Y._ErosionPercentageList.Item(0).Quantity, "Property : Quantity - Test 3")

    End Sub

#End Region

#Region "Class : PercentageErosions | Class : PercentageErosion | Public Property : SellingPrice"

    <TestMethod()> Public Sub PropertySellingPrice_ReturnCorrectValue()

        Dim X As New ErosionPercentage
        Dim Y As New ErosionPercentages

        X.SellingPrice = 10.234
        Assert.AreEqual(10.234, X._SellingPrice, "Property : SellingPrice - Test 1")

        X._SellingPrice = 29.567
        Assert.AreEqual(29.567, X.SellingPrice, "Property : SellingPrice - Test 2")

        Y.Add("100803", 25, 100.345)
        Assert.AreEqual(100.345, Y._ErosionPercentageList.Item(0).SellingPrice, "Property : SellingPrice - Test 3")

    End Sub

#End Region

#Region "Class : PercentageErosions | Class : PercentageErosion | Public Property : ErosionPercentage"

    <TestMethod()> Public Sub PropertySetErosionPercentage_ReturnCorrectValue()

        Dim X As New ErosionPercentage

        X.ErosionPercentage = 12.456
        Assert.AreEqual(12.456, X._ErosionPercentage, "Property : ErosionPercentage - Test 1")

        X._ErosionPercentage = 45.789
        Assert.AreEqual(45.789, X.ErosionPercentage, "Property : ErosionPercentage - Test 2")

    End Sub

#End Region

#End Region

#Region "Private Procedures And Functions"

    Private Function ErosionPriceCalculated(ByRef List As List(Of IErosionPercentage)) As Boolean

        ErosionPriceCalculated = True

        For Each X As IErosionPercentage In List

            If X.ErosionPercentage.HasValue = False Then ErosionPriceCalculated = False

        Next

    End Function

#End Region

End Class