﻿<TestClass()> Public Class EventExistingHeaderUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region





#Region "Class : EventExistingHeaders | Private Function : ActiveDay"

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayMonday_StateActive_ReturnTrue()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsTrue(HeaderCollection.ActiveDay(DayOfWeek.Monday, ConfigureHeader(True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayMonday_StateInActive_ReturnFalse()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsFalse(HeaderCollection.ActiveDay(DayOfWeek.Monday, ConfigureHeader(False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayTuesday_StateActive_ReturnTrue()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsTrue(HeaderCollection.ActiveDay(DayOfWeek.Tuesday, ConfigureHeader(Nothing, True, Nothing, Nothing, Nothing, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayTuesday_StateInActive_ReturnFalse()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsFalse(HeaderCollection.ActiveDay(DayOfWeek.Tuesday, ConfigureHeader(Nothing, False, Nothing, Nothing, Nothing, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayWednesday_StateActive_ReturnTrue()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsTrue(HeaderCollection.ActiveDay(DayOfWeek.Wednesday, ConfigureHeader(Nothing, Nothing, True, Nothing, Nothing, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayWednesdayy_StateInActive_ReturnFalse()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsFalse(HeaderCollection.ActiveDay(DayOfWeek.Wednesday, ConfigureHeader(Nothing, Nothing, False, Nothing, Nothing, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayThursday_StateActive_ReturnTrue()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsTrue(HeaderCollection.ActiveDay(DayOfWeek.Thursday, ConfigureHeader(Nothing, Nothing, Nothing, True, Nothing, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayThursday_StateInActive_ReturnFalse()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsFalse(HeaderCollection.ActiveDay(DayOfWeek.Thursday, ConfigureHeader(Nothing, Nothing, Nothing, False, Nothing, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayFriday_StateActive_ReturnTrue()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsTrue(HeaderCollection.ActiveDay(DayOfWeek.Friday, ConfigureHeader(Nothing, Nothing, Nothing, Nothing, True, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDayFriday_StateInActive_ReturnFalse()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsFalse(HeaderCollection.ActiveDay(DayOfWeek.Friday, ConfigureHeader(Nothing, Nothing, Nothing, Nothing, False, Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDaySaturday_StateActive_ReturnTrue()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsTrue(HeaderCollection.ActiveDay(DayOfWeek.Saturday, ConfigureHeader(Nothing, Nothing, Nothing, Nothing, Nothing, True, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDaySaturday_StateInActive_ReturnFalse()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsFalse(HeaderCollection.ActiveDay(DayOfWeek.Saturday, ConfigureHeader(Nothing, Nothing, Nothing, Nothing, Nothing, False, Nothing)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDaySunday_StateActive_ReturnTrue()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsTrue(HeaderCollection.ActiveDay(DayOfWeek.Sunday, ConfigureHeader(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, True)))

    End Sub

    <TestMethod()> Public Sub FunctionActiveDay_InitialValues_ActiveDaySunday_StateInActive_ReturnFalse()

        Dim HeaderCollection As New EventExistingHeaders

        Assert.IsFalse(HeaderCollection.ActiveDay(DayOfWeek.Sunday, ConfigureHeader(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, False)))

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Function ConfigureHeader(ByVal Monday As Boolean, ByVal Tuesday As Boolean, ByVal Wednesday As Boolean, ByVal Thursday As Boolean, _
                                     ByVal Friday As Boolean, ByVal Saturday As Boolean, ByVal Sunday As Boolean) As IEventExistingHeader

        ConfigureHeader = EventExistingHeaderFactory.FactoryGet

        ConfigureHeader.Monday = Monday
        ConfigureHeader.Tuesday = Tuesday
        ConfigureHeader.Wednesday = Wednesday
        ConfigureHeader.Thursday = Thursday
        ConfigureHeader.Friday = Friday
        ConfigureHeader.Saturday = Saturday
        ConfigureHeader.Sunday = Sunday

    End Function

#End Region

End Class