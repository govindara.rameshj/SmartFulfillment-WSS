﻿<TestClass()> Public Class TemporaryPriceChangeEventUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : TemporaryPriceChangeEvent"

#Region "Class : TemporaryPriceChangeEvent | Public Function : GetCurrentPrice"

    <TestMethod()> Public Sub FunctionGetCurrentPrice_TestScenerios()

        Dim Actual As New TemporaryPriceChangeEvent
        Dim OverrideEvent As Boolean
        Dim ExistingEventPrice As Double
        Dim ExistingEventNumber As String
        Dim ReturnValue As Double

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio One
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(Nothing, Nothing)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.AreEqual(CType(0, System.Double), ReturnValue, "GetCurrentPrice (Scenerio One): Parameters - OverridePriceNull ExistingPriceNull | ReturnZero")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Two
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(Nothing, 10)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.AreEqual(CType(10, System.Double), ReturnValue, "GetCurrentPrice (Scenerio Two): Parameters - OverridePriceNull ExistingPriceTen | ReturnTen")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Three
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(5, 10)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.AreEqual(CType(5, System.Double), ReturnValue, "GetCurrentPrice (Scenerio Three): Parameters - OverridePriceFive ExistingPriceTen | ReturnFive")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Four
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(10, 10)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.IsTrue(OverrideEvent, "GetCurrentPrice (Scenerio Four): Parameters - OverrideEventExist ExistingEventExist | ReturnOverrideEventTrue")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Five
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(Nothing, 10)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.IsFalse(OverrideEvent, "GetCurrentPrice (Scenerio Five): Parameters - OverrideEventDoesNotNull ExistingEventExist | ReturnOverrideEventFalse")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Six
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(Nothing, Nothing)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.AreEqual(CType(0, System.Double), ExistingEventPrice, "GetCurrentPrice (Scenerio Six): Parameters - OverridePriceNull ExistingPriceNull | ReturnExistingEventPriceZero")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Seven
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(Nothing, 0)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.AreEqual(CType(0, System.Double), ExistingEventPrice, "GetCurrentPrice (Scenerio Seven): Parameters - OverridePriceNull ExistingPriceZero | ReturnExistingEventPriceZero")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Eight
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(Nothing, 10)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.AreEqual(CType(10, System.Double), ExistingEventPrice, "GetCurrentPrice (Scenerio Eight): Parameters - OverridePriceNull ExistingPriceNonZero | ReturnExistingEventPriceNonZero")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Nine
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(Nothing, Nothing, String.Empty)
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.AreEqual(String.Empty, ExistingEventNumber, "GetCurrentPrice (Scenerio Nine): Parameters - OverridePriceNull ExistingPriceNull | ReturnExistingEventNumberStringEmpty")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Ten
        OverrideEvent = Nothing
        ExistingEventPrice = Nothing
        ExistingEventNumber = String.Empty
        ReturnValue = Nothing

        ArrangeHelperRepository(Nothing, 25, "123456")
        ReturnValue = Actual.GetCurrentPrice(Nothing, Nothing, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

        Assert.AreEqual("123456", ExistingEventNumber, "GetCurrentPrice (Scenerio Ten): Parameters - OverridePriceNull ExistingPriceNotNull | ReturnExistingEventNumber")

    End Sub

#End Region

#Region "Class : TemporaryPriceChangeEvent | Public Function : SetNewPrice"

    <TestMethod()> Public Sub FunctionSetNewPrice_ReturnTrue()

        Dim Actual As New TemporaryPriceChangeEvent
        Dim NullValue As New System.Nullable(Of Decimal)

        ArrangeNewPriceRepository(True)

        Assert.IsTrue(Actual.SetNewPrice(Nothing, Nothing, Nothing, Nothing, Nothing))

    End Sub

    <TestMethod()> Public Sub FunctionSetNewPrice_ReturnFalse()

        Dim Actual As New TemporaryPriceChangeEvent
        Dim NullValue As New System.Nullable(Of Decimal)

        ArrangeNewPriceRepository(False)

        Assert.IsFalse(Actual.SetNewPrice(Nothing, Nothing, Nothing, Nothing, Nothing))

    End Sub

#End Region

#End Region

#Region "Class : TemporaryPriceChangeEventHelper"

#Region "Class : TemporaryPriceChangeEventHelper | Public Function : GetEventExistingPrice"

    <TestMethod()> Public Sub FunctionGetEventExistingPrice_ExistingCollectionEmpty_ReturnNulPrice()

        Dim X As New TemporaryPriceChangeEventHelper

        Assert.IsFalse(X.GetEventExistingPrice().HasValue)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventExistingPrice_OneEvent_CommonValues_ReturnCorrectPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Initialise("100803", Today)

        Assert.AreEqual(CType(123.45, Decimal), X.GetEventExistingPrice().Value)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventExistingPrice_OneEvent_DayOrWeekTrue_ActiveDayFalse_ReturnNullPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable
        Dim DayOfWeekEvent As Boolean

        DayOfWeekEvent = True

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, DayOfWeekEvent, Now, Now, CType(123.45, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Initialise("100803", Today)

        'Assert.AreEqual(CType(123.45, Decimal), X.GetEventExistingPrice().Value)
        Assert.IsFalse(X.GetEventExistingPrice().HasValue)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventExistingPrice_OneEvent_DayOrWeekTrue_ActiveDayTrue_ReturnCorrectPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable
        Dim DayOfWeekEvent As Boolean

        DayOfWeekEvent = True
        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", True, True, True, True, True, True, True, False)
        'ExistingEventHeaderAddRow(HeaderDT, "055555", "50", ActiveDay(Today, DayOfWeek.Monday), _
        '                                                    ActiveDay(Today, DayOfWeek.Tuesday), _
        '                                                    ActiveDay(Today, DayOfWeek.Wednesday), _
        '                                                    ActiveDay(Today, DayOfWeek.Thursday), _
        '                                                    ActiveDay(Today, DayOfWeek.Friday), _
        '                                                    ActiveDay(Today, DayOfWeek.Saturday), _
        '                                                    ActiveDay(Today, DayOfWeek.Sunday), _
        '                                                    False)
        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, DayOfWeekEvent, Now, Now, CType(123.45, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Initialise("100803", Today)

        Assert.AreEqual(CType(123.45, Decimal), X.GetEventExistingPrice().Value)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventExistingPrice_OneEvent_MultiplePriority_ReturnCorrectPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "066666", "40", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "077777", "30", False, False, False, False, False, False, False, False)
        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "066666", "40", False, False, Now, Now, CType(456.78, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "077777", "30", False, False, Now, Now, CType(789.12, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Initialise("100803", Today)

        Assert.AreEqual(CType(123.45, Decimal), X.GetEventExistingPrice().Value)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventExistingPrice_OneEvent_MultiplePriority_MultipleEventID_ReturnCorrectPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "077777", "30", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "066666", "40", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055553", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055554", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)


        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "077777", "30", False, False, Now, Now, CType(789.12, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "066666", "40", False, False, Now, Now, CType(456.78, Decimal))

        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055553", "50", False, False, Now, Now, CType(222.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055554", "50", False, False, Now, Now, CType(111.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Initialise("100803", Today)

        Assert.AreEqual(CType(123.45, Decimal), X.GetEventExistingPrice().Value)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventExistingPrice_OneEvent_MultiplePriority_MultipleEventID_OneEvent_DayOrWeekTrue_ActiveDayFalse_ReturnCorrectPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "077777", "30", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "066666", "40", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055553", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055554", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055556", "50", False, False, False, False, False, False, False, False)


        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "077777", "30", False, False, Now, Now, CType(789.12, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "066666", "40", False, False, Now, Now, CType(456.78, Decimal))

        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055553", "50", False, False, Now, Now, CType(222.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055554", "50", False, False, Now, Now, CType(111.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, True, Now, Now, CType(999.999, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Initialise("100803", Today)

        Assert.AreEqual(CType(123.45, Decimal), X.GetEventExistingPrice().Value)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventExistingPrice_OneEvent_MultiplePriority_MultipleEventID_OneEvent_DayOrWeekTrue_ActiveDayTrue_ReturnCorrectPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "077777", "30", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "066666", "40", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055553", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055554", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055556", "50", True, True, True, True, True, True, True, False)

        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "077777", "30", False, False, Now, Now, CType(789.12, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "066666", "40", False, False, Now, Now, CType(456.78, Decimal))

        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055553", "50", False, False, Now, Now, CType(222.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055554", "50", False, False, Now, Now, CType(111.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, True, Now, Now, CType(999.999, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Initialise("100803", Today)

        Assert.AreEqual(CType(123.45, Decimal), X.GetEventExistingPrice().Value)

    End Sub

#End Region

#Region "Class : TemporaryPriceChangeEventHelper | Public Function : GetEventExistingEventNumber"

    <TestMethod()> Public Sub FunctionGetEventExistingEventNumber_TestScenerios()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable
        Dim DayOfWeekEvent As Boolean

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio One
        Assert.AreEqual(Nothing, X.GetEventExistingEventNumber(), "GetEventExistingEventNumber (Scenerio One): Parameters - ExistingCollectionEmpty | ReturnNullValue")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Two
        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))
        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)
        X.Initialise("100803", Today)

        Assert.AreEqual("055555", X.GetEventExistingEventNumber(), "GetEventExistingEventNumber (Scenerio Two): Parameters - OneEvent, CommonValues | ReturnCorrectEventNumber")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Three
        HeaderDT.Rows.Clear()
        DetailDT.Rows.Clear()
        DayOfWeekEvent = True
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, DayOfWeekEvent, Now, Now, CType(123.45, Decimal))
        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)
        X.Initialise("100803", Today)

        Assert.AreEqual(String.Empty, X.GetEventExistingEventNumber(), "GetEventExistingEventNumber (Scenerio Three): Parameters - OneEvent DayOrWeekTrue ActiveDayFalse | ReturnStringEmpty")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Four
        HeaderDT.Rows.Clear()
        DetailDT.Rows.Clear()
        DayOfWeekEvent = True
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", True, True, True, True, True, True, True, False)
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, DayOfWeekEvent, Now, Now, CType(123.45, Decimal))
        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)
        X.Initialise("100803", Today)

        Assert.AreEqual("055555", X.GetEventExistingEventNumber(), "GetEventExistingEventNumber (Scenerio Four): Parameters - OneEvent DayOrWeekTrue ActiveDayTrue | ReturnEventNumber")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Five
        HeaderDT.Rows.Clear()
        DetailDT.Rows.Clear()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "066666", "40", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "077777", "30", False, False, False, False, False, False, False, False)
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "066666", "40", False, False, Now, Now, CType(456.78, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "077777", "30", False, False, Now, Now, CType(789.12, Decimal))
        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)
        X.Initialise("100803", Today)

        Assert.AreEqual("055555", X.GetEventExistingEventNumber(), "GetEventExistingEventNumber (Scenerio Five): Parameters - OneEvent MultiplePriority | ReturnCorrectEventNumber")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Six
        HeaderDT.Rows.Clear()
        DetailDT.Rows.Clear()
        ExistingEventHeaderAddRow(HeaderDT, "077777", "30", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "066666", "40", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055553", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055554", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "077777", "30", False, False, Now, Now, CType(789.12, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "066666", "40", False, False, Now, Now, CType(456.78, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055553", "50", False, False, Now, Now, CType(222.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055554", "50", False, False, Now, Now, CType(111.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))
        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)
        X.Initialise("100803", Today)

        Assert.AreEqual("055555", X.GetEventExistingEventNumber(), "GetEventExistingEventNumber (Scenerio Six): Parameters - OneEvent MultiplePriority MultipleEventID | ReturnCorrectEventNumber")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Seven
        HeaderDT.Rows.Clear()
        DetailDT.Rows.Clear()
        ExistingEventHeaderAddRow(HeaderDT, "077777", "30", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "066666", "40", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055553", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055554", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055556", "50", False, False, False, False, False, False, False, False)
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "077777", "30", False, False, Now, Now, CType(789.12, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "066666", "40", False, False, Now, Now, CType(456.78, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055553", "50", False, False, Now, Now, CType(222.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055554", "50", False, False, Now, Now, CType(111.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, True, Now, Now, CType(999.999, Decimal))
        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)
        X.Initialise("100803", Today)


        Assert.AreEqual("055555", X.GetEventExistingEventNumber(), "GetEventExistingEventNumber (Scenerio Seven): Parameters - OneEvent MultiplePriority MultipleEventID OneEvent DayOrWeekTrue ActiveDayFalse | ReturnCorrectEventNumber")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Scenerio Eight
        HeaderDT.Rows.Clear()
        DetailDT.Rows.Clear()
        ExistingEventHeaderAddRow(HeaderDT, "077777", "30", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "066666", "40", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055553", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055554", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "055556", "50", True, True, True, True, True, True, True, False)
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "077777", "30", False, False, Now, Now, CType(789.12, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "066666", "40", False, False, Now, Now, CType(456.78, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055553", "50", False, False, Now, Now, CType(222.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055554", "50", False, False, Now, Now, CType(111.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, True, Now, Now, CType(999.999, Decimal))
        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)
        X.Initialise("100803", Today)

        Assert.AreEqual("055555", X.GetEventExistingEventNumber(), "GetEventExistingEventNumber (Scenerio Eight): Parameters - OneEvent MultiplePriority MultipleEventID OneEvent DayOrWeekTrue ActiveDayTrue | ReturnCorrectEventNumber")

    End Sub

#End Region

#Region "Class : TemporaryPriceChangeEventHelper | Public Function : GetEventOverridePrice"

    <TestMethod()> Public Sub FunctionGetEventOverridePrice_OverrideCollectionEmpty_ReturnNullValue()

        Dim X As New TemporaryPriceChangeEventHelper

        Assert.IsFalse(X.GetEventOverridePrice().HasValue)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventOverridePrice_OverrideCollectionOneObject_ReturnCorrectPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = OverrideEventHeaderCreateStructure()
        OverrideEventHeaderAddRow(HeaderDT, 1, 1, "Dummy Entry", Now, Now, 1, 999, 999)

        DetailDT = OverrideEventDetailCreateStructure()
        OverrideEventDetailAddRow(DetailDT, 1, 1, "123456", CType(999.24, Decimal))

        ArrangeEventRepository(Nothing, Nothing, HeaderDT, DetailDT)
        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(CType(999.24, Decimal), X.GetEventOverridePrice().Value)

    End Sub

    <TestMethod()> Public Sub FunctionGetEventOverridePrice_OverrideCollectionThreeObjects_ReturnCorrectPrice()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = OverrideEventHeaderCreateStructure()
        OverrideEventHeaderAddRow(HeaderDT, 1, 1, "Dummy Entry One", Now, Now, 1, 101, 102)
        OverrideEventHeaderAddRow(HeaderDT, 2, 1, "Dummy Entry Two", Now, Now, 3, 103, 104)
        OverrideEventHeaderAddRow(HeaderDT, 3, 1, "Dummy Entry Three", Now, Now, 2, 105, 106)

        DetailDT = OverrideEventDetailCreateStructure()
        OverrideEventDetailAddRow(DetailDT, 7, 1, "111111", CType(111.22, Decimal))
        OverrideEventDetailAddRow(DetailDT, 8, 2, "222222", CType(888.44, Decimal))
        OverrideEventDetailAddRow(DetailDT, 9, 3, "333333", CType(555.66, Decimal))

        ArrangeEventRepository(Nothing, Nothing, HeaderDT, DetailDT)
        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(CType(888.44, Decimal), X.GetEventOverridePrice().Value)

    End Sub

#End Region

#Region "Class : TemporaryPriceChangeEventHelper | Public Function : Initialise"

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_SkuNull_SelectedDateNull_ReturnFalse()

        Dim X As New TemporaryPriceChangeEventHelper

        Assert.IsFalse(X.Initialise(Nothing, Nothing))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_SkuEmpty_SelectedDateNull_ReturnFalse()

        Dim X As New TemporaryPriceChangeEventHelper

        Assert.IsFalse(X.Initialise(String.Empty, Nothing))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_SkuNotEmpty_SelectedDateNull_ReturnFalse()

        Dim X As New TemporaryPriceChangeEventHelper

        Assert.IsFalse(X.Initialise("12345", Nothing))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_SkuNotEmpty_SelectedDateNotNull_ReturnTrue()

        Dim X As New TemporaryPriceChangeEventHelper

        ArrangeEventRepository(Nothing, Nothing, Nothing, Nothing)
        Assert.IsTrue(X.Initialise("12345", Now))

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_SkuNotEmpty_SelectedDateNotNull_ReturnCorrectInternalSKUValue()

        Dim X As New TemporaryPriceChangeEventHelper

        ArrangeEventRepository(Nothing, Nothing, Nothing, Nothing)
        X.Initialise("12345", Now)

        Assert.AreEqual("12345", X._SKU)

    End Sub

    <TestMethod()> Public Sub FunctionInitialise_InitialValue_SkuNotEmpty_SelectedDateNotNull_ReturnCorrectInternalSelectedDateValue()

        Dim X As New TemporaryPriceChangeEventHelper

        ArrangeEventRepository(Nothing, Nothing, Nothing, Nothing)
        X.Initialise("12345", Now.AddDays(5))

        Assert.AreEqual(Now.AddDays(5).Date, X._SelectedDate.Date)

    End Sub

#End Region

#Region "Class: TemporaryPriceChangeEventHelper | Private Procedure : Load"

    <TestMethod()> Public Sub ProcedureLoad_ExistingEventsNoData_OverrideEventsNoData_ReturnExistInDatabaseFalse()

        Dim X As New TemporaryPriceChangeEventHelper

        ArrangeEventRepository(Nothing, Nothing, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsFalse(X.ExistsInDatabase)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_ExistingEventsData_OverrideEventsNoData_ReturnExistInDatabaseTrue()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)

        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(X.ExistsInDatabase)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_ExistingEventsNoData_OverrideEventsData_ReturnExistInDatabaseTrue()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = OverrideEventHeaderCreateStructure()
        OverrideEventHeaderAddRow(HeaderDT, 1, 1, "Dummy Entry", Now, Now, 1, 999, 999)

        DetailDT = OverrideEventDetailCreateStructure()
        OverrideEventDetailAddRow(DetailDT, 1, 1, "123456", CType(999.24, Decimal))

        ArrangeEventRepository(Nothing, Nothing, HeaderDT, DetailDT)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(X.ExistsInDatabase)

    End Sub

    <TestMethod()> Public Sub ProcedureLoad_ExistingEventsData_OverrideEventsData_ReturnExistInDatabaseTrue()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim ExistingHeaderDT As DataTable
        Dim ExistingDetailDT As DataTable
        Dim OverrideHeaderDT As DataTable
        Dim OverrideDetailDT As DataTable

        ExistingHeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(ExistingHeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        ExistingDetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(ExistingDetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))

        OverrideHeaderDT = OverrideEventHeaderCreateStructure()
        OverrideEventHeaderAddRow(OverrideHeaderDT, 1, 1, "Dummy Entry", Now, Now, 1, 999, 999)
        OverrideDetailDT = OverrideEventDetailCreateStructure()
        OverrideEventDetailAddRow(OverrideDetailDT, 1, 1, "123456", CType(999.24, Decimal))

        ArrangeEventRepository(ExistingHeaderDT, ExistingDetailDT, OverrideHeaderDT, OverrideDetailDT)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(X.ExistsInDatabase)

    End Sub

#End Region

#Region "Class: TemporaryPriceChangeEventHelper | Private Procedure : ExistingEventsLoadData"

    <TestMethod()> Public Sub FunctionExistingEventsLoadData_HeaderNothing_DetailNothing_ReturnCollectionNothing()

        Dim X As New TemporaryPriceChangeEventHelper

        ArrangeEventRepository(Nothing, Nothing, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(Nothing, X.ExistingEventCollection)

    End Sub

    <TestMethod()> Public Sub FunctionExistingEventsLoadData_HeaderEmpty_DetailNothing_ReturnCollectionNothing()

        Dim X As New TemporaryPriceChangeEventHelper

        ArrangeEventRepository(ExistingEventHeaderCreateStructure, Nothing, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(Nothing, X.ExistingEventCollection)

    End Sub

    <TestMethod()> Public Sub FunctionExistingEventsLoadData_HeaderNotEmpty_DetailNothing_ReturnCollectionNothing()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)

        ArrangeEventRepository(HeaderDT, Nothing, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(Nothing, X.ExistingEventCollection)

    End Sub

    <TestMethod()> Public Sub FunctionExistingEventsLoadData_HeaderNotEmpty_DetailEmpty_ReturnCollectionNothing()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)

        ArrangeEventRepository(HeaderDT, ExistingEventDetailCreateStructure, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(Nothing, X.ExistingEventCollection)

    End Sub

    <TestMethod()> Public Sub FunctionExistingEventsLoadData_HeaderNotEmpty_DetailNotEmpty_ReturnCollectionOneObject()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, False, False, False, False, False, False, False)
        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, False, Now, Now, CType(123.45, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(1, X.ExistingEventCollection.CollectionCount)

    End Sub

    <TestMethod()> Public Sub FunctionExistingEventsLoadData_HeaderNotEmpty_DetailNotEmpty_BusinessObjectLoadedCorrectly()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = ExistingEventHeaderCreateStructure()
        ExistingEventHeaderAddRow(HeaderDT, "055555", "50", False, True, False, True, False, True, False, False)
        ExistingEventHeaderAddRow(HeaderDT, "123456", "40", True, False, True, False, True, False, True, False)
        ExistingEventHeaderAddRow(HeaderDT, "999999", "10", True, True, True, True, True, True, True, True)

        DetailDT = ExistingEventDetailCreateStructure()
        ExistingEventDetailAddRow(DetailDT, "TS", "100803", "055555", "50", False, True, Now, Now, CType(123.45, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "100500", "123456", "40", True, False, Now.AddDays(1), Now.AddDays(1), CType(456.78, Decimal))
        ExistingEventDetailAddRow(DetailDT, "TS", "765432", "999999", "10", False, True, Now.AddDays(2), Now.AddDays(2), CType(789.01, Decimal))

        ArrangeEventRepository(HeaderDT, DetailDT, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(ExistingEventComplexMatch(X.ExistingEventCollection, HeaderDT, DetailDT))

    End Sub

#End Region

#Region "Class: TemporaryPriceChangeEventHelper | Private Procedure : OverrideEventsLoadData"

    <TestMethod()> Public Sub FunctionOverrideEventsLoadData_HeaderNothing_DetailNothing_ReturnCollectionNothing()

        Dim X As New TemporaryPriceChangeEventHelper

        ArrangeEventRepository(Nothing, Nothing, Nothing, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(Nothing, X.OverrideEventCollection)

    End Sub

    <TestMethod()> Public Sub FunctionOverrideEventsLoadData_HeaderEmpty_DetailNothing_ReturnCollectionNothing()

        Dim X As New TemporaryPriceChangeEventHelper

        ArrangeEventRepository(Nothing, Nothing, OverrideEventHeaderCreateStructure, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(Nothing, X.OverrideEventCollection)

    End Sub

    <TestMethod()> Public Sub FunctionOverrideEventsLoadData_HeaderNotEmpty_DetailNothing_ReturnCollectionNothing()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable

        HeaderDT = OverrideEventHeaderCreateStructure()
        OverrideEventHeaderAddRow(HeaderDT, 1, 1, "Dummy Entry", Now, Now, 1, 999, 999)

        ArrangeEventRepository(Nothing, Nothing, HeaderDT, Nothing)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(Nothing, X.OverrideEventCollection)

    End Sub

    <TestMethod()> Public Sub FunctionOverrideEventsLoadData_HeaderNotEmpty_DetailEmpty_ReturnCollectionNothing()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable

        HeaderDT = OverrideEventHeaderCreateStructure()
        OverrideEventHeaderAddRow(HeaderDT, 1, 1, "Dummy Entry", Now, Now, 1, 999, 999)

        ArrangeEventRepository(Nothing, Nothing, HeaderDT, OverrideEventDetailCreateStructure)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(Nothing, X.OverrideEventCollection)

    End Sub

    <TestMethod()> Public Sub FunctionOverrideEventsLoadData_HeaderNotEmpty_DetailNotEmpty_ReturnCollectionOneObject()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = OverrideEventHeaderCreateStructure()
        OverrideEventHeaderAddRow(HeaderDT, 1, 1, "Dummy Entry", Now, Now, 1, 999, 999)

        DetailDT = OverrideEventDetailCreateStructure()
        OverrideEventDetailAddRow(DetailDT, 1, 1, "123456", CType(999.24, Decimal))

        ArrangeEventRepository(Nothing, Nothing, HeaderDT, DetailDT)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual(1, X.OverrideEventCollection.CollectionCount)

    End Sub

    <TestMethod()> Public Sub FunctionOverrideEventsLoadData_HeaderNotEmpty_DetailNotEmpty_BusinessObjectLoadedCorrectly()

        Dim X As New TemporaryPriceChangeEventHelper
        Dim HeaderDT As DataTable
        Dim DetailDT As DataTable

        HeaderDT = OverrideEventHeaderCreateStructure()
        OverrideEventHeaderAddRow(HeaderDT, 1, 1, "Dummy Entry One", Now, Now, 1, 101, 102)
        OverrideEventHeaderAddRow(HeaderDT, 2, 1, "Dummy Entry Two", Now, Now, 2, 103, 104)
        OverrideEventHeaderAddRow(HeaderDT, 3, 1, "Dummy Entry Three", Now, Now, 3, 105, 106)

        DetailDT = OverrideEventDetailCreateStructure()
        OverrideEventDetailAddRow(DetailDT, 7, 1, "111111", CType(111.22, Decimal))
        OverrideEventDetailAddRow(DetailDT, 8, 2, "222222", CType(333.44, Decimal))
        OverrideEventDetailAddRow(DetailDT, 9, 3, "333333", CType(555.66, Decimal))

        ArrangeEventRepository(Nothing, Nothing, HeaderDT, DetailDT)

        X.Load(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(OverrideEventComplexMatch(X.OverrideEventCollection, HeaderDT, DetailDT))

    End Sub

#End Region

#End Region

#Region "Private Procedures And Functions"

#Region "Common"

    Private Sub ArrangeEventRepository(ByRef ExistingHeaderDT As DataTable, ByVal ExistingDetailDT As DataTable, _
                                       ByRef OverrideHeaderDT As DataTable, ByVal OverrideDetailDT As DataTable)

        Dim Mock As New MockRepository
        Dim Repository As IEventRepository

        Repository = Mock.Stub(Of IEventRepository)()

        SetupResult.On(Repository).Call(Repository.GetExistingTemporaryPriceChangeForSkuHeader(Nothing, Nothing, Nothing, Nothing)).Return(ExistingHeaderDT).IgnoreArguments()
        SetupResult.On(Repository).Call(Repository.GetExistingTemporaryPriceChangeForSkuDetail(Nothing, Nothing, Nothing, Nothing)).Return(ExistingDetailDT).IgnoreArguments()

        SetupResult.On(Repository).Call(Repository.GetOverrideTemporaryPriceChangeForSkuHeader(Nothing, Nothing, Nothing)).Return(OverrideHeaderDT).IgnoreArguments()
        SetupResult.On(Repository).Call(Repository.GetOverrideTemporaryPriceChangeForSkuDetail(Nothing, Nothing, Nothing)).Return(OverrideDetailDT).IgnoreArguments()

        EventRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Sub ArrangeHelperRepository(ByVal OverridePrice As System.Nullable(Of Decimal), _
                                        ByVal ExistingPrice As System.Nullable(Of Decimal), _
                                        Optional ByVal ExistingEventNumber As String = Nothing)

        Dim Mock As New MockRepository
        Dim HelperClass As ITemporaryPriceChangeEventHelper

        HelperClass = Mock.Stub(Of ITemporaryPriceChangeEventHelper)()
        SetupResult.On(HelperClass).Call(HelperClass.GetEventOverridePrice()).Return(OverridePrice).IgnoreArguments()
        SetupResult.On(HelperClass).Call(HelperClass.GetEventExistingPrice()).Return(ExistingPrice).IgnoreArguments()
        SetupResult.On(HelperClass).Call(HelperClass.GetEventExistingEventNumber()).Return(ExistingEventNumber).IgnoreArguments()

        TemporaryPriceChangeEventHelperFactory.FactorySet(HelperClass)
        Mock.ReplayAll()

    End Sub

    Private Sub ArrangeNewPriceRepository(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Repository As IEventRepository

        Repository = Mock.Stub(Of IEventRepository)()

        SetupResult.On(Repository).Call(Repository.SetOverrideTemporaryPriceChange(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)).Return(ReturnValue).IgnoreArguments()
        EventRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Function DataTableFilter(ByVal FilterDT As DataTable, ByVal FilterCitreria As String) As DataTable

        Dim DT As DataTable
        Dim DR As DataRow()

        DT = FilterDT.Clone
        DR = FilterDT.Select(FilterCitreria)

        For Each Row As DataRow In DR

            DT.ImportRow(Row)

        Next

        Return DT

    End Function

    Private Function ActiveDay(ByVal SelectedDate As Date, ByVal Day As System.DayOfWeek) As Boolean

        Select Case SelectedDate.DayOfWeek

            Case DayOfWeek.Monday
                If Day = DayOfWeek.Monday Then Return True

            Case DayOfWeek.Tuesday
                If Day = DayOfWeek.Tuesday Then Return True

            Case DayOfWeek.Wednesday
                If Day = DayOfWeek.Wednesday Then Return True

            Case DayOfWeek.Thursday
                If Day = DayOfWeek.Thursday Then Return True

            Case DayOfWeek.Friday
                If Day = DayOfWeek.Friday Then Return True

            Case DayOfWeek.Saturday
                If Day = DayOfWeek.Saturday Then Return True

            Case DayOfWeek.Sunday
                If Day = DayOfWeek.Sunday Then Return True

        End Select

        Return False

    End Function

#End Region

#Region "Existing Events"

    Private Function ExistingEventHeaderCreateStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns

            .Add("NUMB", System.Type.GetType("System.String"))
            .Add("PRIO", System.Type.GetType("System.String"))
            .Add("DACT1", System.Type.GetType("System.Boolean"))
            .Add("DACT2", System.Type.GetType("System.Boolean"))
            .Add("DACT3", System.Type.GetType("System.Boolean"))
            .Add("DACT4", System.Type.GetType("System.Boolean"))
            .Add("DACT5", System.Type.GetType("System.Boolean"))
            .Add("DACT6", System.Type.GetType("System.Boolean"))
            .Add("DACT7", System.Type.GetType("System.Boolean"))
            .Add("IDEL", System.Type.GetType("System.Boolean"))

        End With

        Return DT

    End Function

    Private Sub ExistingEventHeaderAddRow(ByRef DT As DataTable, _
                                          ByVal EventID As String, ByVal Priority As String, ByVal Monday As Boolean, _
                                          ByVal Tuesday As Boolean, ByVal Wednesday As Boolean, ByVal Thursday As Boolean, _
                                          ByVal Friday As Boolean, ByVal Saturday As Boolean, ByVal Sunday As Boolean, ByVal Deleted As Boolean)

        DT.Rows.Add(EventID, Priority, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, Deleted)

    End Sub

    Private Function ExistingEventDetailCreateStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns

            .Add("TYPE", System.Type.GetType("System.String"))
            .Add("KEY1", System.Type.GetType("System.String"))
            .Add("NUMB", System.Type.GetType("System.String"))
            .Add("PRIO", System.Type.GetType("System.String"))
            .Add("IDEL", System.Type.GetType("System.Boolean"))
            .Add("IDOW", System.Type.GetType("System.Boolean"))
            .Add("SDAT", System.Type.GetType("System.DateTime"))
            .Add("EDAT", System.Type.GetType("System.DateTime"))
            .Add("PRIC", System.Type.GetType("System.Decimal"))

        End With

        Return DT

    End Function

    Friend Sub ExistingEventDetailAddRow(ByRef DT As DataTable, _
                                         ByVal EventType As String, ByVal EventKeyOne As String, ByVal EventID As String, _
                                         ByVal Priority As String, ByVal Deleted As Boolean, ByVal DayOfWeekEvent As Boolean, _
                                         ByVal StartDate As System.Nullable(Of Date), ByVal EndDate As System.Nullable(Of Date), _
                                         ByVal Price As System.Nullable(Of Decimal))

        DT.Rows.Add(EventType, EventKeyOne, EventID, Priority, Deleted, DayOfWeekEvent, StartDate, EndDate, Price)

    End Sub

    Private Function ExistingEventComplexMatch(ByRef BO As IEventExistingHeaders, ByRef HeaderDT As DataTable, ByRef DetailDT As DataTable) As Boolean

        'assume that business object & datatable sorting order are consistent

        Dim Header As IEventExistingHeader
        Dim Detail As IEventExistingMaster

        Dim FilteredDetailDT As DataTable

        Dim HeaderDTIndex As Integer
        Dim DetailDTIndex As Integer

        Try
            'loop around header
            HeaderDTIndex = 0

            For HeaderIndex As Integer = 0 To BO.CollectionCount - 1 Step 1

                Header = BO.ItemObject(HeaderIndex)

                If Header.EventID <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(0), String) Then Return False
                If Header.Priority <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(1), String) Then Return False
                If Header.Monday <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(2), Boolean) Then Return False
                If Header.Tuesday <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(3), Boolean) Then Return False
                If Header.Wednesday <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(4), Boolean) Then Return False
                If Header.Thursday <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(5), Boolean) Then Return False
                If Header.Friday <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(6), Boolean) Then Return False
                If Header.Saturday <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(7), Boolean) Then Return False
                If Header.Sunday <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(8), Boolean) Then Return False
                If Header.Deleted <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(9), Boolean) Then Return False

                HeaderDTIndex += 1

                'loop around line
                DetailDTIndex = 0

                For DetailIndex As Integer = 0 To Header.MasterCollection.CollectionCount - 1 Step 1

                    Detail = Header.MasterCollection.ItemObject(DetailIndex)

                    FilteredDetailDT = DataTableFilter(DetailDT, "NUMB = '" & Header.EventID & "'")

                    If Detail.EventType <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(0), String) Then Return False
                    If Detail.EventKeyOne <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(1), String) Then Return False
                    If Detail.EventID <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(2), String) Then Return False
                    If Detail.Priority <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(3), String) Then Return False
                    If Detail.Deleted <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(4), Boolean) Then Return False
                    If Detail.DayOfWeekEvent <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(5), Boolean) Then Return False
                    If Detail.StartDate <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(6), System.Nullable(Of Date)).Value Then Return False
                    If Detail.EndDate <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(7), System.Nullable(Of Date)).Value Then Return False
                    If Detail.Price <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(8), System.Nullable(Of Decimal)).Value Then Return False

                Next

            Next

        Catch ex As Exception

            Return False

        End Try

        If BO.CollectionCount > 0 Then Return True

    End Function

#End Region

#Region "Override Events"

    Private Function OverrideEventHeaderCreateStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns

            .Add("ID", System.Type.GetType("System.Int16"))
            .Add("EventTypeID", System.Type.GetType("System.Int16"))
            .Add("Description", System.Type.GetType("System.String"))
            .Add("StartDate", System.Type.GetType("System.DateTime"))
            .Add("EndDate", System.Type.GetType("System.DateTime"))
            .Add("Revision", System.Type.GetType("System.Int16"))
            .Add("CashierSecurityID", System.Type.GetType("System.Int16"))
            .Add("AuthorisorSecurityID", System.Type.GetType("System.Int16"))

        End With

        Return DT

    End Function

    Private Sub OverrideEventHeaderAddRow(ByRef DT As DataTable, ByVal ID As Integer, ByVal EventTypeID As Integer, _
                                          ByVal Description As String, ByVal StartDate As Date, ByVal EndDate As Date, _
                                          ByVal SequenceNo As Integer, ByVal UserOneID As Integer, ByVal UserTwoID As Integer)

        DT.Rows.Add(ID, EventTypeID, Description, StartDate, EndDate, SequenceNo, UserOneID, UserTwoID)

    End Sub

    Private Function OverrideEventDetailCreateStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns

            .Add("ID", System.Type.GetType("System.Int16"))
            .Add("EventHeaderOverrideID", System.Type.GetType("System.Int16"))
            .Add("SkuNumber", System.Type.GetType("System.String"))
            .Add("Price", System.Type.GetType("System.Decimal"))

        End With

        Return DT

    End Function

    Friend Sub OverrideEventDetailAddRow(ByRef DT As DataTable, ByVal ID As Integer, _
                                         ByVal EventHeaderOverrideID As Integer, ByVal SkuNumber As String, ByVal Price As Decimal)

        DT.Rows.Add(ID, EventHeaderOverrideID, SkuNumber, Price)

    End Sub

    Private Function OverrideEventComplexMatch(ByRef BO As IEventOverrideHeaders, ByRef HeaderDT As DataTable, ByRef DetailDT As DataTable) As Boolean

        'assume that business object & datatable sorting order are consistent

        Dim Header As IEventOverrideHeader
        Dim Detail As IEventOverrideMaster

        Dim FilteredDetailDT As DataTable

        Dim HeaderDTIndex As Integer
        Dim DetailDTIndex As Integer

        Try
            'loop around header
            HeaderDTIndex = 0

            For HeaderIndex As Integer = 0 To BO.CollectionCount - 1 Step 1

                Header = BO.ItemObject(HeaderIndex)

                If Header.ID <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(0), Integer) Then Return False
                If Header.EventTypeID <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(1), Integer) Then Return False
                If Header.Description <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(2), String) Then Return False
                If Header.StartDate <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(3), Date) Then Return False
                If Header.EndDate <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(4), Date) Then Return False
                If Header.RevisionNumber <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(5), Integer) Then Return False
                If Header.CashierSecurityID <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(6), Integer) Then Return False
                If Header.AuthorisorSecurityID <> CType(HeaderDT.Rows.Item(HeaderDTIndex).Item(7), Integer) Then Return False

                HeaderDTIndex += 1

                'loop around line
                DetailDTIndex = 0

                For DetailIndex As Integer = 0 To Header.MasterCollection.CollectionCount - 1 Step 1

                    Detail = Header.MasterCollection.ItemObject(DetailIndex)
                    FilteredDetailDT = DataTableFilter(DetailDT, "EventHeaderOverrideID = " & Header.ID.ToString)

                    If Detail.ID <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(0), Integer) Then Return False
                    If Detail.HeaderID <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(1), Integer) Then Return False
                    If Detail.Sku <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(2), String) Then Return False
                    If Detail.Price <> CType(FilteredDetailDT.Rows.Item(DetailDTIndex).Item(3), Decimal) Then Return False

                Next

            Next

        Catch ex As Exception

            Return False

        End Try

        If BO.CollectionCount > 0 Then Return True

    End Function

#End Region

#End Region

End Class