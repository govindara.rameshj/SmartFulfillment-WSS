﻿<TestClass()> Public Class MultiplePriceOverrideUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : MultiplePriceOverride"

#Region "Class : MultiplePriceOverride | Public Function : GetCurrentPriceXXXXXXXXXXXXXXx"

#End Region

#Region "Class : MultiplePriceOverride | Public Function : SetNewPrice"

    <TestMethod()> Public Sub FunctionSetNewPrice_ReturnTrue()

        Dim Actual As New MultiplePriceOverride
        Dim NullValue As New System.Nullable(Of Decimal)

        ArrangeNewPriceRepository(True)

        Assert.IsTrue(Actual.SetNewPrice(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing))

    End Sub

    <TestMethod()> Public Sub FunctionSetNewPrice_ReturnFalse()

        Dim Actual As New MultiplePriceOverride
        Dim NullValue As New System.Nullable(Of Decimal)

        ArrangeNewPriceRepository(False)

        Assert.IsFalse(Actual.SetNewPrice(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing))

    End Sub

#End Region

#End Region

#Region "Private Procedures And Functions"

#Region "Common"

    Private Sub ArrangeNewPriceRepository(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Repository As IEventRepository

        Repository = Mock.Stub(Of IEventRepository)()

        SetupResult.On(Repository).Call(Repository.SetOverrideTemporaryPriceChange(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)).Return(ReturnValue).IgnoreArguments()
        EventRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

#End Region

#End Region

End Class