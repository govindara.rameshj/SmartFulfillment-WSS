﻿<TestClass()> Public Class MultiplePriceOverrideIntegrationTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    '<TestMethod()> Public Sub TestMethod1()

    '    Dim x As New MultiplePriceOverride
    '    Dim RS1 As Recordset
    '    Dim RS2 As Recordset
    '    Dim RS3 As Recordset

    '    RS1 = x.GetDealGroup(Now, "100803,100804")
    '    RS2 = x.GetEventMaster(Now, "100803,100804")
    '    RS3 = x.GetValidEvent("400000", "00")

    'End Sub

End Class