﻿<TestClass()> Public Class TemporaryPriceChangeEventIntegrationTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : TemporaryPriceChangeEvent | Public Function : GetCurrentPrice - NOT IMPLEMENTED"

#End Region

#Region "Class : TemporaryPriceChangeEvent | Public Function : SetNewPrice"

    <TestMethod()> Public Sub ClassTemporaryPriceChangeEvent_FunctionSetNewPrice_WriteSuccess()

        'use concrete implementation instaed of interface so that i could access internal class details not define in interface

        Dim X As New TemporaryPriceChangeEvent

        Assert.IsTrue(X.SetNewPrice(SkuID, Now, CType(123.45, Decimal), UserID, UserID))

        DeleteTestData(X._EventHeaderOverideID)

    End Sub

#End Region

#Region "Class : MultiplePriceOverride | Public Function : GetDealGroup - NOT IMPLEMENTED"

#End Region

#Region "Class : MultiplePriceOverride | Public Function : GetEventMaster - NOT IMPLEMENTED"

#End Region

#Region "Class : MultiplePriceOverride | Public Function : GetValidEvent - NOT IMPLEMENTED"

#End Region

#Region "Class : MultiplePriceOverride | Public Function : SetNewPrice"

    <TestMethod()> Public Sub ClassMultiplePriceOverride_FunctionSetNewPrice_WriteSuccess()

        'use concrete implementation instaed of interface so that i could access internal class details not define in interface

        Dim X As New MultiplePriceOverride

        Assert.IsTrue(X.SetNewPrice(SkuListID(5), QuantityListID(5), Now, CType(123.45, Decimal), UserID, UserID))

        DeleteTestData(X._EventHeaderOverideID)

    End Sub

#End Region

#Region "Private Procedure(s) & Function(s)"

    Private Function SkuID() As String

        Dim DT As DataTable

        Using con As New Cts.Oasys.Data.Connection

            Using com As New Cts.Oasys.Data.Command(con)

                com.CommandText = "select top 1 SKUN from STKMAS order by SKUN"
                DT = com.ExecuteDataTable

            End Using

        End Using

        Return CType(DT.Rows(0).Item(0), String)

    End Function

    Private Function SkuListID(ByVal NumberOfSkuElements As Integer) As String

        Dim DT As DataTable

        Using con As New Cts.Oasys.Data.Connection

            Using com As New Cts.Oasys.Data.Command(con)

                com.CommandText = "select top " & NumberOfSkuElements.ToString & " SKUN from STKMAS order by SKUN"
                DT = com.ExecuteDataTable

            End Using

        End Using

        SkuListID = String.Empty
        For Each Row As DataRow In DT.Rows

            If SkuListID.Length <> 0 Then SkuListID &= ","

            SkuListID &= CType(Row.Item(0), String)

        Next

    End Function

    Private Function QuantityListID(ByVal NumberOfSkuElements As Integer) As String

        QuantityListID = String.Empty
        For Index As Integer = 1 To NumberOfSkuElements Step 1

            If QuantityListID.Length <> 0 Then QuantityListID &= ","

            QuantityListID &= Index.ToString

        Next

    End Function

    Private Function UserID() As Integer

        Dim DT As DataTable

        Using con As New Cts.Oasys.Data.Connection

            Using com As New Cts.Oasys.Data.Command(con)

                com.CommandText = "select top 1 ID from SystemUsers order by ID"
                DT = com.ExecuteDataTable

            End Using

        End Using

        Return CType(DT.Rows(0).Item(0), Integer)

    End Function

    Private Sub DeleteTestData(ByVal EventHeaderID As Integer)

        Using con As New Cts.Oasys.Data.Connection

            Using com As New Cts.Oasys.Data.Command(con)

                com.CommandText = "delete EventDetailOverride where EventHeaderOverrideID = " & EventHeaderID.ToString

                com.ExecuteNonQuery()

                com.CommandText = "delete EventHeaderOverride where ID = " & EventHeaderID.ToString

                com.ExecuteNonQuery()

            End Using

        End Using

    End Sub

#End Region

End Class