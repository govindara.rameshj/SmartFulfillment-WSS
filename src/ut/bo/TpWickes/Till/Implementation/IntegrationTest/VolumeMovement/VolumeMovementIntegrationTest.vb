﻿<TestClass()> Public Class VolumeMovementIntegrationTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Constants"

    Private TextCommunicationWarehouse As String = "The order will be delivered by our Central Warehouse and the delivery timeslot will be advised by text or email."
    Private TextCommunicationHubs As String = "The order will be delivered by our WICKES WIMBLEDON Delivery Centre who will contact you by phone the evening before with a timeslot."
    Private TextCommunicationSplit As String = "Part of the order will be delivered by our WICKES WIMBLEDON Delivery Centre who will contact the customer by phone the evening before with a timeslot, the remainder will be delivered by our Central Warehouse and the delivery timeslot will be advised by text or email."
    Private TextCommunicationCannotDetermine As String = "There is a problem with the order, once we are in a position to confirm the detail the customer will be contacted with delivery instructions."

#End Region

#Region "Integration Tests"

    <TestMethod()> Public Sub IntegrationTestPlan_ScenerioHubFulfilled()

        Dim VM As IVolumeMovement

        VM = VolumeMovementFactory.FactoryGet

        Assert.AreEqual(TextCommunicationHubs, VM.OMCheckFulfilment(HubOrder()))

    End Sub

    <TestMethod()> Public Sub IntegrationTestPlan_ScenerioEStoreFulfilled()

        Dim VM As IVolumeMovement

        VM = VolumeMovementFactory.FactoryGet

        Assert.AreEqual(TextCommunicationWarehouse, VM.OMCheckFulfilment(EStoreOrder()))

    End Sub

    <TestMethod()> Public Sub IntegrationTestPlan_ScenerioSplitFulfilled()

        Dim VM As IVolumeMovement

        VM = VolumeMovementFactory.FactoryGet

        Assert.AreEqual(TextCommunicationSplit, VM.OMCheckFulfilment(SplitOrder()))

    End Sub

    <TestMethod()> Public Sub IntegrationTestPlan_ScenerioCannotDetermine()

        Dim VM As IVolumeMovement

        VM = VolumeMovementFactory.FactoryGet

        Assert.AreEqual(TextCommunicationCannotDetermine, VM.OMCheckFulfilment(CannotDetermineOrder()))

    End Sub

    <TestMethod()> Public Sub IntegrationTestPlan_ScenerioWebServiceUnavailable()

        'N/A - covered by unit test

    End Sub

    <TestMethod()> Public Sub IntegrationTestPlan_ScenerioBadRequestXML()

        'N/A - covered by unit test

    End Sub

    <TestMethod()> Public Sub IntegrationTestPlan_ScenerioPerformanceTest()

        'Not implemented, time pressures!!!

    End Sub

#End Region

#Region "Private Procedures & Functions"

    Public Function HubOrder() As IOrderHeader

        Dim OrderHeader As IOrderHeader
        'Dim OrderLine As IOrderLine

        'Wimbledon
        OrderHeader = OrderHeaderFactory.FactoryGet
        With OrderHeader
            .SellingStoreCode = 59
            .SellingStoreOrderNumber = 997
            .RequiredDeliveryDate = Now
            .DeliveryCharge = 10.0
            .TotalOrderValue = 56.94
            .SaleDate = Now
            .DeliveryPostcode = "SW6 4NF"
            .ToBeDelivered = True
        End With

        'OrderLine = OrderLineFactory.FactoryGet
        'With OrderLine
        '    .SellingStoreLineNo = 1
        '    .ProductCode = 166159
        '    .ProductDescription = "Spacers Box 3000"
        '    .TotalOrderQuantity = 2
        '    .QuantityTaken = 0
        '    .UOM = "EACH"
        '    .LineValue = CType(14.98, Decimal)
        '    .DeliveryChargeItem = False
        '    .SellingPrice = CType(7.49, Decimal)
        'End With
        'OrderHeader.Add(OrderLine)

        'OrderLine = OrderLineFactory.FactoryGet
        'With OrderLine
        '    .SellingStoreLineNo = 2
        '    .ProductCode = 218813
        '    .ProductDescription = "Fixed Downlight Conversion Ring B/Chrome"
        '    .TotalOrderQuantity = 4
        '    .QuantityTaken = 0
        '    .UOM = "EACH"
        '    .LineValue = CType(31.96, Decimal)
        '    .DeliveryChargeItem = False
        '    .SellingPrice = CType(7.99, Decimal)
        'End With
        'OrderHeader.Add(OrderLine)

        OrderHeader.AddByValue(1, 166159, "Spacers Box 3000", 2, 0, "EACH", 14.98, False, 7.49)
        OrderHeader.AddByValue(2, 218813, "Fixed Downlight Conversion Ring B/Chrome", 4, 0, "EACH", 31.96, False, 7.99)
        Return OrderHeader

    End Function

    Public Function EStoreOrder() As IOrderHeader

        Dim OrderHeader As IOrderHeader
        'Dim OrderLine As IOrderLine

        'kettering extra
        OrderHeader = OrderHeaderFactory.FactoryGet
        With OrderHeader
            .SellingStoreCode = 326
            .SellingStoreOrderNumber = 998
            .RequiredDeliveryDate = Now
            .DeliveryCharge = 10.0
            .TotalOrderValue = 870.74
            .SaleDate = Now
            .DeliveryPostcode = "NN16 0QH"
            .ToBeDelivered = True
        End With

        'OrderLine = OrderLineFactory.FactoryGet
        'With OrderLine
        '    .SellingStoreLineNo = 1
        '    .ProductCode = 189571
        '    .ProductDescription = "Chopin Arch"
        '    .TotalOrderQuantity = 2
        '    .QuantityTaken = 0
        '    .UOM = "EACH"
        '    .LineValue = 260.78
        '    .DeliveryChargeItem = False
        '    .SellingPrice = 130.39
        'End With
        'OrderHeader.Add(OrderLine)

        'OrderLine = OrderLineFactory.FactoryGet
        'With OrderLine
        '    .SellingStoreLineNo = 2
        '    .ProductCode = 191204
        '    .ProductDescription = "Loft Ladder Timber"
        '    .TotalOrderQuantity = 4
        '    .QuantityTaken = 0
        '    .UOM = "EACH"
        '    .LineValue = 599.96
        '    .DeliveryChargeItem = False
        '    .SellingPrice = 149.99
        'End With
        'OrderHeader.Add(OrderLine)

        OrderHeader.AddByValue(1, 189571, "Chopin Arc", 2, 0, "EACH", 260.78, False, 130.39)
        OrderHeader.AddByValue(2, 191204, "Loft Ladder Timber", 4, 0, "EACH", 599.96, False, 149.99)
        Return OrderHeader

    End Function

    Public Function SplitOrder() As IOrderHeader

        Dim OrderHeader As IOrderHeader
        'Dim OrderLine As IOrderLine

        'Wimbledon
        OrderHeader = OrderHeaderFactory.FactoryGet
        With OrderHeader
            .SellingStoreCode = 59
            .SellingStoreOrderNumber = 999
            .RequiredDeliveryDate = Now
            .DeliveryCharge = 10.0
            .TotalOrderValue = 624.94
            .SaleDate = Now
            .DeliveryPostcode = "SW6 4NF"
            .ToBeDelivered = True
        End With

        'OrderLine = OrderLineFactory.FactoryGet
        'With OrderLine
        '    .SellingStoreLineNo = 1
        '    .ProductCode = 166159
        '    .ProductDescription = "Spacers Box 3000"
        '    .TotalOrderQuantity = 2
        '    .QuantityTaken = 0
        '    .UOM = "EACH"
        '    .LineValue = 14.98
        '    .DeliveryChargeItem = False
        '    .SellingPrice = 7.49
        'End With
        'OrderHeader.Add(OrderLine)

        'OrderLine = OrderLineFactory.FactoryGet
        'With OrderLine
        '    .SellingStoreLineNo = 2
        '    .ProductCode = 191204
        '    .ProductDescription = "Loft Ladder Timber"
        '    .TotalOrderQuantity = 4
        '    .QuantityTaken = 0
        '    .UOM = "EACH"
        '    .LineValue = 599.96
        '    .DeliveryChargeItem = False
        '    .SellingPrice = 149.99
        'End With
        'OrderHeader.Add(OrderLine)

        OrderHeader.AddByValue(1, 166159, "Spacers Box 3000", 2, 0, "EACH", 14.98, False, 7.49)
        OrderHeader.AddByValue(2, 191204, "Loft Ladder Timber", 4, 0, "EACH", 599.96, False, 149.99)
        Return OrderHeader

    End Function

    Public Function CannotDetermineOrder() As IOrderHeader

        Dim OrderHeader As IOrderHeader
        'Dim OrderLine As IOrderLine

        'Woking
        OrderHeader = OrderHeaderFactory.FactoryGet
        With OrderHeader
            .SellingStoreCode = 80
            .SellingStoreOrderNumber = 1000
            .RequiredDeliveryDate = Now
            .DeliveryCharge = 10.0
            .TotalOrderValue = 17.49
            .SaleDate = Now
            .DeliveryPostcode = "GU21 2WB"
            .ToBeDelivered = True
        End With

        'OrderLine = OrderLineFactory.FactoryGet
        'With OrderLine
        '    .SellingStoreLineNo = 1
        '    .ProductCode = 999999
        '    .ProductDescription = "XXXXXX"
        '    .TotalOrderQuantity = 1
        '    .QuantityTaken = 0
        '    .UOM = "EACH"
        '    .LineValue = 10.99
        '    .DeliveryChargeItem = False
        '    .SellingPrice = 10.99
        'End With
        'OrderHeader.Add(OrderLine)

        OrderHeader.AddByValue(1, 999999, "XXXXXX", 1, 0, "EACH", 10.99, False, 10.99)
        Return OrderHeader

    End Function

#End Region

End Class