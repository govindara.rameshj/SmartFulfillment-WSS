﻿<TestClass()> Public Class FactoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory - TpWickes.Interop.Interface"

#Region "Factory - TpWickes.Interop.Interface | Coupon"

    <TestMethod()> Public Sub HeaderFactory_ReturnCorrectObject()

        Dim X As IHeader

        X = HeaderFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Coupon.Header", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub PrintCouponFactory_ReturnCorrectObject()

        Dim X As IPrintCoupon

        X = PrintCouponFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Coupon.PrintCoupon", X.GetType.FullName)

    End Sub

#End Region

#Region "Factory - TpWickes.Interop.Interface | Event"

    <TestMethod()> Public Sub MultiplePriceOverrideFactory_ReturnCorrectObject()

        Dim X As IMultiplePriceOverride

        X = MultiplePriceOverrideFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.MultiplePriceOverride", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub TemporaryPriceChangeEventFactory_ReturnCorrectObject()

        Dim X As ITemporaryPriceChangeEvent

        X = TemporaryPriceChangeEventFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Event.TemporaryPriceChangeEvent", X.GetType.FullName)

    End Sub

#End Region

#Region "Factory - TpWickes.Interop.Interface | Sale"

    <TestMethod()> Public Sub SaleCouponFactory_ReturnCorrectObject()

        Dim X As ISaleCoupon

        X = SaleCouponFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Sale.SaleCoupon", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SaleCouponsFactory_ReturnCorrectObject()

        Dim X As ISaleCoupons

        X = SaleCouponsFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.Sale.SaleCoupons", X.GetType.FullName)

    End Sub

#End Region

#Region "Factory - TpWickes.Interop.Interface | Volume Movement"

    <TestMethod()> Public Sub OrderHeaderFactory_ReturnCorrectObject()

        Dim X As IOrderHeader

        X = OrderHeaderFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.VolumeMvmt.OrderHeader", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub OrderLineFactory_ReturnCorrectObject()

        Dim X As IOrderLine

        X = OrderLineFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.VolumeMvmt.OrderLine", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub VolumeMovementFactory_ReturnCorrectObject()

        Dim X As IVolumeMovement

        X = VolumeMovementFactory.FactoryGet

        Assert.AreEqual("TpWickes.Till.Implementation.VolumeMvmt.WebService", X.GetType.FullName)

    End Sub

#End Region

#End Region

End Class