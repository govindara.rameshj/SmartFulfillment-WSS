﻿<TestClass()> Public Class VisionValidationUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub HeaderFactory_ReturnCorrectObject()

        Dim X As IVisionValidation

        X = VisionValidationFactory.FactoryGet

        Assert.AreEqual("TpWickes.Library.VisionSale.VisionValidation", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub FunctionValid_TestScenerios()

        Dim X As IVisionValidation

        X = VisionValidationFactory.FactoryGet

        Assert.IsFalse(X.Valid(0, 0), "Function Valid: Total = 0, TenderCount = 0, Return = False")
        Assert.IsTrue(X.Valid(0, 5), "Function Valid: Total = 0, TenderCount = 5, Return = True")
        Assert.IsTrue(X.Valid(5, 0), "Function Valid: Total = 5, TenderCount = 0, Return = True")
        Assert.IsTrue(X.Valid(5, 0), "Function Valid: Total = 5, TenderCount = 2, Return = True")

    End Sub

End Class