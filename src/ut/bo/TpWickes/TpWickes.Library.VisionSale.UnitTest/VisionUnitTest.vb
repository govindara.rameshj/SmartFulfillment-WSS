﻿<TestClass()> Public Class VisionUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test XML Data"

    Private KB_DepositIn_SingleTender_StockNotPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                                     <CTSVISIONDEPOSIT>
                                                                         <PVTOTS>
                                                                             <DATE>23/10/11</DATE>
                                                                             <TILL>55</TILL>
                                                                             <TRAN>6610</TRAN>
                                                                             <CASH>012</CASH>
                                                                             <TIME>124607</TIME>
                                                                             <TCOD>M+</TCOD>
                                                                             <MISC/>
                                                                             <DESC/>
                                                                             <ORDN>373964</ORDN>
                                                                             <MERC>000000.00</MERC>
                                                                             <NMER>000249.00</NMER>
                                                                             <TAXA>000041.50</TAXA>
                                                                             <DISC>000000.00</DISC>
                                                                             <TOTL>000249.00</TOTL>
                                                                             <IEMP>N</IEMP>
                                                                             <PVEM>000000.00</PVEM>
                                                                             <PIVT>N</PIVT>
                                                                         </PVTOTS>
                                                                         <PVPAID>
                                                                             <TENDER>
                                                                                 <DATE>23/10/11</DATE>
                                                                                 <TILL>55</TILL>
                                                                                 <TRAN>6610</TRAN>
                                                                                 <NUMB>0001</NUMB>
                                                                                 <TYPE>03</TYPE>
                                                                                 <AMNT>000249.00-</AMNT>
                                                                                 <CARD>492181******9646</CARD>
                                                                                 <EXDT>1410</EXDT>
                                                                                 <PIVT>N</PIVT>
                                                                             </TENDER>
                                                                         </PVPAID>
                                                                     </CTSVISIONDEPOSIT>

    Private KB_DepositIn_SingleTender_StockPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                                  <CTSVISIONDEPOSIT>
                                                                      <PVTOTS>
                                                                          <DATE>23/10/11</DATE>
                                                                          <TILL>55</TILL>
                                                                          <TRAN>6610</TRAN>
                                                                          <CASH>012</CASH>
                                                                          <TIME>124607</TIME>
                                                                          <TCOD>M+</TCOD>
                                                                          <MISC/>
                                                                          <DESC/>
                                                                          <ORDN>373964</ORDN>
                                                                          <MERC>000000.00</MERC>
                                                                          <NMER>000249.00</NMER>
                                                                          <TAXA>000041.50</TAXA>
                                                                          <DISC>000000.00</DISC>
                                                                          <TOTL>000249.00</TOTL>
                                                                          <IEMP>N</IEMP>
                                                                          <PVEM>000000.00</PVEM>
                                                                          <PIVT>N</PIVT>
                                                                      </PVTOTS>
                                                                      <PVPAID>
                                                                          <TENDER>
                                                                              <DATE>23/10/11</DATE>
                                                                              <TILL>55</TILL>
                                                                              <TRAN>6610</TRAN>
                                                                              <NUMB>0001</NUMB>
                                                                              <TYPE>03</TYPE>
                                                                              <AMNT>000249.00-</AMNT>
                                                                              <CARD>492181******9646</CARD>
                                                                              <EXDT>1410</EXDT>
                                                                              <PIVT>N</PIVT>
                                                                          </TENDER>
                                                                      </PVPAID>
                                                                      <STOCK>
                                                                          <LINE>
                                                                              <SKUN>100803</SKUN>
                                                                              <QUAN>000005</QUAN>
                                                                          </LINE>
                                                                          <LINE>
                                                                              <SKUN>100804</SKUN>
                                                                              <QUAN>000014</QUAN>
                                                                          </LINE>
                                                                      </STOCK>
                                                                  </CTSVISIONDEPOSIT>

    Private NonKB_SingleTender_StockNotPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                              <CTSINTEGRATION>
                                                                  <PVTOTS>
                                                                      <DATE>03/03/12</DATE>
                                                                      <TILL>55</TILL>
                                                                      <TRAN>6773</TRAN>
                                                                      <CASH>003</CASH>
                                                                      <TIME>143335</TIME>
                                                                      <TCOD>SA</TCOD>
                                                                      <MISC/>
                                                                      <DESC/>
                                                                      <ORDN>374376</ORDN>
                                                                      <MERC>000105.25</MERC>
                                                                      <NMER>000000.00</NMER>
                                                                      <TAXA>000017.54</TAXA>
                                                                      <DISC>000097.75</DISC>
                                                                      <TOTL>000105.25</TOTL>
                                                                      <IEMP>N</IEMP>
                                                                      <PVEM>000000.00</PVEM>
                                                                      <PIVT>N</PIVT>
                                                                  </PVTOTS>
                                                                  <PVLINE>
                                                                      <LINE>
                                                                          <DATE>03/03/12</DATE>
                                                                          <TILL>55</TILL>
                                                                          <TRAN>6773</TRAN>
                                                                          <NUMB>0001</NUMB>
                                                                          <SKUN>156855</SKUN>
                                                                          <QUAN>000001</QUAN>
                                                                          <SPRI>000000.00</SPRI>
                                                                          <PRIC>000105.25</PRIC>
                                                                          <PRVE>000087.71</PRVE>
                                                                          <EXTP>000105.25</EXTP>
                                                                          <RITM>N</RITM>
                                                                          <VSYM>a</VSYM>
                                                                          <PIVI>N</PIVI>
                                                                          <PIVM/>
                                                                          <CTGY>125424</CTGY>
                                                                          <GRUP>184494</GRUP>
                                                                          <SGRP>184496</SGRP>
                                                                          <STYL>184504</STYL>
                                                                          <SALT>S</SALT>
                                                                          <PIVT>N</PIVT>
                                                                      </LINE>
                                                                  </PVLINE>
                                                                  <PVPAID>
                                                                      <TENDER>
                                                                          <DATE>03/03/12</DATE>
                                                                          <TILL>55</TILL>
                                                                          <TRAN>6773</TRAN>
                                                                          <NUMB>0001</NUMB>
                                                                          <TYPE>03</TYPE>
                                                                          <AMNT>000105.25-</AMNT>
                                                                          <CARD>************7796</CARD>
                                                                          <EXDT>1213</EXDT>
                                                                          <PIVT>N</PIVT>
                                                                      </TENDER>
                                                                  </PVPAID>
                                                              </CTSINTEGRATION>

    Private NonKB_SingleTender_StockPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                           <CTSINTEGRATION>
                                                               <PVTOTS>
                                                                   <DATE>03/03/12</DATE>
                                                                   <TILL>55</TILL>
                                                                   <TRAN>6773</TRAN>
                                                                   <CASH>003</CASH>
                                                                   <TIME>143335</TIME>
                                                                   <TCOD>SA</TCOD>
                                                                   <MISC/>
                                                                   <DESC/>
                                                                   <ORDN>374376</ORDN>
                                                                   <MERC>000105.25</MERC>
                                                                   <NMER>000000.00</NMER>
                                                                   <TAXA>000017.54</TAXA>
                                                                   <DISC>000097.75</DISC>
                                                                   <TOTL>000105.25</TOTL>
                                                                   <IEMP>N</IEMP>
                                                                   <PVEM>000000.00</PVEM>
                                                                   <PIVT>N</PIVT>
                                                               </PVTOTS>
                                                               <PVLINE>
                                                                   <LINE>
                                                                       <DATE>03/03/12</DATE>
                                                                       <TILL>55</TILL>
                                                                       <TRAN>6773</TRAN>
                                                                       <NUMB>0001</NUMB>
                                                                       <SKUN>156855</SKUN>
                                                                       <QUAN>000001</QUAN>
                                                                       <SPRI>000000.00</SPRI>
                                                                       <PRIC>000105.25</PRIC>
                                                                       <PRVE>000087.71</PRVE>
                                                                       <EXTP>000105.25</EXTP>
                                                                       <RITM>N</RITM>
                                                                       <VSYM>a</VSYM>
                                                                       <PIVI>N</PIVI>
                                                                       <PIVM/>
                                                                       <CTGY>125424</CTGY>
                                                                       <GRUP>184494</GRUP>
                                                                       <SGRP>184496</SGRP>
                                                                       <STYL>184504</STYL>
                                                                       <SALT>S</SALT>
                                                                       <PIVT>N</PIVT>
                                                                   </LINE>
                                                               </PVLINE>
                                                               <PVPAID>
                                                                   <TENDER>
                                                                       <DATE>03/03/12</DATE>
                                                                       <TILL>55</TILL>
                                                                       <TRAN>6773</TRAN>
                                                                       <NUMB>0001</NUMB>
                                                                       <TYPE>03</TYPE>
                                                                       <AMNT>000105.25-</AMNT>
                                                                       <CARD>************7796</CARD>
                                                                       <EXDT>1213</EXDT>
                                                                       <PIVT>N</PIVT>
                                                                   </TENDER>
                                                               </PVPAID>
                                                               <STOCK>
                                                                   <LINE>
                                                                       <SKUN>100805</SKUN>
                                                                       <QUAN>000015</QUAN>
                                                                   </LINE>
                                                                   <LINE>
                                                                       <SKUN>100806</SKUN>
                                                                       <QUAN>000114</QUAN>
                                                                   </LINE>
                                                               </STOCK>
                                                           </CTSINTEGRATION>

#End Region

#Region "Unit Test - VisionSaleOld"

    <TestMethod()> Public Sub VisionSaleOld_TestScenarios()

        Dim KB_NoStock As XDocument
        Dim KB_Stock As XDocument
        Dim NonKB_NoStock As XDocument
        Dim NonKB_Stock As XDocument

        Dim Sale As New VisionSaleWithoutStockProcessing
        Dim StockList As StockCollection
        Dim Stock As Vision.Stock

        KB_NoStock = XDocument.Parse(KB_DepositIn_SingleTender_StockNotPresent.ToString)
        KB_Stock = XDocument.Parse(KB_DepositIn_SingleTender_StockPresent.ToString)

        NonKB_NoStock = XDocument.Parse(NonKB_SingleTender_StockNotPresent.ToString)
        NonKB_Stock = XDocument.Parse(NonKB_SingleTender_StockPresent.ToString)

        Assert.AreEqual(KB_NoStock.ToString, Sale.RemoveStockNodeFromXmlDocument(KB_NoStock.ToString), "      Test: KB Sale | No stock present")
        Assert.AreEqual(KB_NoStock.ToString, Sale.RemoveStockNodeFromXmlDocument(KB_Stock.ToString), "        Test: KB Sale | Stock present")
        Assert.AreEqual(NonKB_NoStock.ToString, Sale.RemoveStockNodeFromXmlDocument(NonKB_NoStock.ToString), "Test: Non KB Sale | No stock present")
        Assert.AreEqual(NonKB_NoStock.ToString, Sale.RemoveStockNodeFromXmlDocument(NonKB_Stock.ToString), "  Test: Non KB Sale | Stock present")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsFalse(Sale.SaveStock(Nothing), "Test: Stock collection - Nothing")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        StockList = New StockCollection

        Assert.IsFalse(Sale.SaveStock(StockList), "Test: Stock collection - Created")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Stock = New Vision.Stock
        Stock.StockCode = "100803"
        Stock.Quantity = 5
        StockList.Add(Stock)

        Assert.IsFalse(Sale.SaveStock(StockList), "Test: Stock collection - Not empty")

    End Sub

#End Region

#Region "Unit Test - VisionSaleNew"

    <TestMethod()> Public Sub VisionSaleNew_TestScenarios()

        Dim KB_NoStock As XDocument
        Dim KB_Stock As XDocument
        Dim NonKB_NoStock As XDocument
        Dim NonKB_Stock As XDocument

        Dim Sale As New VisionSaleWithStockProcessing
        Dim StockList As StockCollection
        Dim Stock As Vision.Stock

        KB_NoStock = XDocument.Parse(KB_DepositIn_SingleTender_StockNotPresent.ToString)
        KB_Stock = XDocument.Parse(KB_DepositIn_SingleTender_StockPresent.ToString)

        NonKB_NoStock = XDocument.Parse(NonKB_SingleTender_StockNotPresent.ToString)
        NonKB_Stock = XDocument.Parse(NonKB_SingleTender_StockPresent.ToString)

        Assert.AreEqual(KB_NoStock.ToString, Sale.RemoveStockNodeFromXmlDocument(KB_NoStock.ToString), "      Test: KB Sale | No stock present")
        Assert.AreEqual(KB_Stock.ToString, Sale.RemoveStockNodeFromXmlDocument(KB_Stock.ToString), "          Test: KB Sale | Stock present")
        Assert.AreEqual(NonKB_NoStock.ToString, Sale.RemoveStockNodeFromXmlDocument(NonKB_NoStock.ToString), "Test: Non KB Sale | No stock present")
        Assert.AreEqual(NonKB_Stock.ToString, Sale.RemoveStockNodeFromXmlDocument(NonKB_Stock.ToString), "    Test: Non KB Sale | Stock present")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsFalse(Sale.SaveStock(Nothing), "Test: Stock collection - Nothing")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        StockList = New StockCollection

        Assert.IsFalse(Sale.SaveStock(StockList), "Test: Stock collection - Created")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Stock = New Vision.Stock
        Stock.StockCode = "100803"
        Stock.Quantity = 5
        StockList.Add(Stock)

        Assert.IsTrue(Sale.SaveStock(StockList), "Test: Stock collection - Not empty")

    End Sub

#End Region

#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitch_TestScenerios()

        Dim Sale As IVisionSaleStockProcessing
        Dim Factory As New VisionSaleStockProcessingFactory

        ArrangeRequirementSwitchDependency(True)
        Sale = Factory.GetImplementation
        Assert.AreEqual("TpWickes.Library.VisionSale.VisionSaleWithStockProcessing", Sale.GetType.FullName, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        Sale = Factory.GetImplementation
        Assert.AreEqual("TpWickes.Library.VisionSale.VisionSaleWithoutStockProcessing", Sale.GetType.FullName, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        Sale = Factory.GetImplementation
        Assert.AreEqual("TpWickes.Library.VisionSale.VisionSaleWithoutStockProcessing", Sale.GetType.FullName, "Test Three: Return existing implementation")

    End Sub

#End Region

#Region "Unit Test - Stock Collection"

    <TestMethod()> Public Sub PopulateStockCollectionDirectly_TestScenarios()

        Dim StockCollection As New StockCollection
        Dim Stock As Vision.Stock

        Assert.AreEqual(0, StockCollection.Count, "Test: Stock collection empty")

        'add stock item
        Stock = New Vision.Stock
        Stock.StockCode = "100803"
        Stock.Quantity = 5
        StockCollection.Add(Stock)

        Assert.AreEqual("100803", StockCollection.Item(0).StockCode, "Test: Stock Property")
        Assert.AreEqual(5, StockCollection.Item(0).Quantity, "Test: Quantity Property")

    End Sub

#End Region

#Region "Unit Test - Non Kitchen And Bathroom / Kitchen And Bathroom"

    <TestMethod()> Public Sub PopulateStockCollectionViaXml_TestScenarios()

        Dim SaleKB As Vision.KitchenAndBathroom
        Dim SaleNonKB As Vision.NonKitchenAndBathroom

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SaleKB = New KitchenAndBathroom
        SaleNonKB = New NonKitchenAndBathroom

        Assert.IsTrue(SaleKB.Stocks Is Nothing, "   Test: KB Sale    | Unpopulated | Stock collection - empty")
        Assert.IsTrue(SaleNonKB.Stocks Is Nothing, "Test: NonKB Sale | Unpopulated | Stock collection - empty")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SaleKB = New KitchenAndBathroom
        SaleNonKB = New NonKitchenAndBathroom

        ArrangeRequirementSwitchDependency(False)

        SaleKB = Vision.KitchenAndBathroom.Deserialise(KB_DepositIn_SingleTender_StockNotPresent.ToString)
        SaleNonKB = Vision.NonKitchenAndBathroom.Deserialise(NonKB_SingleTender_StockNotPresent.ToString)

        Assert.IsTrue(SaleKB.Stocks.Count = 0, "   Test: KB Sale     | Populate via XML | Requirement Switch Off | No stock elements present | Stock collection - zero count")
        Assert.IsTrue(SaleNonKB.Stocks.Count = 0, "Test: Non KB Sale | Populate via XML | Requirement Switch Off | No stock elements present | Stock collection - zero count")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SaleKB = New KitchenAndBathroom
        SaleNonKB = New NonKitchenAndBathroom

        ArrangeRequirementSwitchDependency(False)

        SaleKB = Vision.KitchenAndBathroom.Deserialise(KB_DepositIn_SingleTender_StockPresent.ToString)
        SaleNonKB = Vision.NonKitchenAndBathroom.Deserialise(NonKB_SingleTender_StockPresent.ToString)

        Assert.IsTrue(SaleKB.Stocks.Count = 0, "   Test: KB Sale     | Populate via XML | Requirement Switch Off | Stock elements present | Stock collection - zero count")
        Assert.IsTrue(SaleNonKB.Stocks.Count = 0, "Test: Non KB Sale | Populate via XML | Requirement Switch Off | Stock elements present | Stock collection - zero count")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SaleKB = New KitchenAndBathroom
        SaleNonKB = New NonKitchenAndBathroom

        ArrangeRequirementSwitchDependency(True)

        SaleKB = Vision.KitchenAndBathroom.Deserialise(KB_DepositIn_SingleTender_StockNotPresent.ToString)
        SaleNonKB = Vision.NonKitchenAndBathroom.Deserialise(NonKB_SingleTender_StockNotPresent.ToString)

        Assert.IsTrue(SaleKB.Stocks.Count = 0, "    Test: KB Sale     | Populate via XML | Requirement Switch On | No stock elements present | Stock collection - zero count")
        Assert.IsTrue(SaleNonKB.Stocks.Count = 0, "Test: Non KB Sale | Populate via XML | Requirement Switch On | No stock elements present | Stock collection - zero count")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        SaleKB = New KitchenAndBathroom
        SaleNonKB = New NonKitchenAndBathroom

        ArrangeRequirementSwitchDependency(True)

        SaleKB = Vision.KitchenAndBathroom.Deserialise(KB_DepositIn_SingleTender_StockPresent.ToString)
        SaleNonKB = Vision.NonKitchenAndBathroom.Deserialise(NonKB_SingleTender_StockPresent.ToString)

        Assert.IsTrue(SaleKB.Stocks.Count = 2, "   Test: KB Sale     | Populate via XML | Requirement Switch On | Stock elements present | Stock collection - non zero count")
        Assert.IsTrue(SaleNonKB.Stocks.Count = 2, "Test: Non KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection - non zero count")

        Assert.AreEqual("100803", SaleKB.Stocks.Item(0).StockCode, "Test: KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection | Item Zero | Valid Skun")
        Assert.AreEqual("100804", SaleKB.Stocks.Item(1).StockCode, "Test: KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection | Item One  | Valid Skun")
        Assert.AreEqual(5, SaleKB.Stocks.Item(0).Quantity, "        Test: KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection | Item Zero | Valid Quantity")
        Assert.AreEqual(14, SaleKB.Stocks.Item(1).Quantity, "       Test: KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection | Item One  | Valid Quantity")

        Assert.AreEqual("100805", SaleNonKB.Stocks.Item(0).StockCode, "Test: KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection | Item Zero | Valid Skun")
        Assert.AreEqual("100806", SaleNonKB.Stocks.Item(1).StockCode, "Test: KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection | Item One  | Valid Skun")
        Assert.AreEqual(15, SaleNonKB.Stocks.Item(0).Quantity, "       Test: KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection | Item Zero | Valid Quantity")
        Assert.AreEqual(114, SaleNonKB.Stocks.Item(1).Quantity, "      Test: KB Sale | Populate via XML | Requirement Switch On | Stock elements present | Stock collection | Item One  | Valid Quantity")

    End Sub

    <TestMethod()> Public Sub PersistStock_TestScenerios()

        Dim SaleKB As Vision.KitchenAndBathroom
        Dim SaleNonKB As Vision.NonKitchenAndBathroom
        Dim Sale As IVisionSaleStockProcessing
        Dim Factory As VisionSaleStockProcessingFactory

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Factory = New VisionSaleStockProcessingFactory
        SaleKB = New KitchenAndBathroom
        SaleNonKB = New NonKitchenAndBathroom

        ArrangeRequirementSwitchDependency(False)

        Sale = Factory.GetImplementation
        SaleKB = Vision.KitchenAndBathroom.Deserialise(KB_DepositIn_SingleTender_StockNotPresent.ToString)
        SaleNonKB = Vision.NonKitchenAndBathroom.Deserialise(NonKB_SingleTender_StockNotPresent.ToString)




        'Assert.IsTrue(SaleKB.Stocks.Count = 0, "   Test: KB Sale     | Populate via XML | Requirement Switch Off | No stock elements present | Stock collection - zero count")
        'Assert.IsTrue(SaleNonKB.Stocks.Count = 0, "Test: Non KB Sale | Populate via XML | Requirement Switch Off | No stock elements present | Stock collection - zero count")



    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class