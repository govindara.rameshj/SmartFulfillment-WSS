﻿<TestClass()> Public Class EventRepositoryIntegrationTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : EventRepository | Public Function : GetExistingTemporaryPriceChangeForSkuHeader | Not Done"

#End Region

#Region "Class : EventRepository | Public Function : GetExistingTemporaryPriceChangeForSkuDetail | Not Done"

#End Region

#Region "Class : EventRepository | Public Function : GetOverrideTemporaryPriceChangeForSkuHeader"

    <TestMethod()> Public Sub FunctionGetOverrideTemporaryPriceChangeForSkuHeader_ReadSuccess()

        Dim X As New EventRepository
        Dim DT As DataTable

        DT = X.GetOverrideTemporaryPriceChangeForSkuHeader(String.Empty, Now, Now)

        Assert.IsTrue(DataTableFieldMatch(DT, OverrideEventHeaderCreateStructure))

    End Sub

#End Region

#Region "Class : EventRepository | Public Function : GetOverrideTemporaryPriceChangeForSkuDetail"

    <TestMethod()> Public Sub FunctionGetOverrideTemporaryPriceChangeForSkuDetail_StoredProcedureExistReadSuccess()

        Dim X As New EventRepository
        Dim DT As DataTable

        DT = X.GetOverrideTemporaryPriceChangeForSkuDetail(String.Empty, Now, Now)

        Assert.IsTrue(DataTableFieldMatch(DT, OverrideEventDetailCreateStructure))

    End Sub

#End Region

#Region "Class : EventRepository | Public Function : SetOverrideTemporaryPriceChange"

    <TestMethod()> Public Sub FunctionSetOverrideTemporaryPriceChange_TemporaryPriceOverrideSKU_WriteSuccess()

        Dim X As New EventRepository
        Dim EventHeaderOverrideID As Integer

        Assert.IsTrue(X.SetOverrideTemporaryPriceChange(EventTypeEnumerator.TemporaryPriceOverrideSKU, "Test Entry", Now, Now, SkuID, "1", CType(123.45, Decimal), UserID, UserID, EventHeaderOverrideID))

        DeleteTestData(EventHeaderOverrideID)

    End Sub

    <TestMethod()> Public Sub FunctionSetOverrideTemporaryPriceChange_TemporaryPriceOverrideSKU_ReturnEventHeaderOverrideID()

        Dim X As New EventRepository
        Dim EventHeaderOverrideID As Integer

        X.SetOverrideTemporaryPriceChange(EventTypeEnumerator.TemporaryPriceOverrideSKU, "Test Entry", Now, Now, SkuID, "1", CType(123.45, Decimal), UserID, UserID, EventHeaderOverrideID)

        Assert.IsTrue(EventHeaderOverrideID > 0)

        DeleteTestData(EventHeaderOverrideID)

    End Sub

    <TestMethod()> Public Sub FunctionSetOverrideTemporaryPriceChange_TemporaryPriceOverrideDealGroup_WriteSuccess()

        Dim X As New EventRepository
        Dim EventHeaderOverrideID As Integer

        Assert.IsTrue(X.SetOverrideTemporaryPriceChange(EventTypeEnumerator.TemporaryPriceOverrideDealGroup, "Test Entry", Now, Now, SkuID, "1", CType(123.45, Decimal), UserID, UserID, EventHeaderOverrideID))

        DeleteTestData(EventHeaderOverrideID)

    End Sub

    <TestMethod()> Public Sub FunctionSetOverrideTemporaryPriceChange_TemporaryPriceOverrideDealGroup_ReturnEventHeaderOverrideID()

        Dim X As New EventRepository
        Dim EventHeaderOverrideID As Integer

        X.SetOverrideTemporaryPriceChange(EventTypeEnumerator.TemporaryPriceOverrideDealGroup, "Test Entry", Now, Now, SkuID, "1", CType(123.45, Decimal), UserID, UserID, EventHeaderOverrideID)

        Assert.IsTrue(EventHeaderOverrideID > 0)

        DeleteTestData(EventHeaderOverrideID)

    End Sub

#End Region

#Region "Private Procedure(s) & Function(s)"

    Private Function SkuID() As String

        Dim DT As DataTable

        Using con As New Connection

            Using com As New Command(con)

                com.CommandText = "select top 1 SKUN from STKMAS order by SKUN"
                DT = com.ExecuteDataTable

            End Using

        End Using

        Return CType(DT.Rows(0).Item(0), String)

    End Function

    Private Function UserID() As Integer

        Dim DT As DataTable

        Using con As New Connection

            Using com As New Command(con)

                com.CommandText = "select top 1 ID from SystemUsers order by ID"
                DT = com.ExecuteDataTable

            End Using

        End Using

        Return CType(DT.Rows(0).Item(0), Integer)

    End Function

    Private Sub DeleteTestData(ByVal EventHeaderID As Integer)

        Using con As New Connection

            Using com As New Command(con)

                com.CommandText = "delete EventDetailOverride where EventHeaderOverrideID = " & EventHeaderID.ToString

                com.ExecuteNonQuery()

                com.CommandText = "delete EventHeaderOverride where ID = " & EventHeaderID.ToString

                com.ExecuteNonQuery()

            End Using

        End Using

    End Sub

    Private Function DataTableFieldMatch(ByRef DT1 As DataTable, ByRef DT2 As DataTable) As Boolean

        Try
            'compare DT1 against DT2
            For ColumnIndex As Integer = 0 To DT1.Columns.Count - 1 Step 1

                If DT1.Columns.Item(ColumnIndex).ColumnName.ToUpper <> DT2.Columns.Item(ColumnIndex).ColumnName.ToUpper Then Return False

            Next
            'compare DT2 against DT1
            For ColumnIndex As Integer = 0 To DT2.Columns.Count - 1 Step 1

                If DT2.Columns.Item(ColumnIndex).ColumnName.ToUpper <> DT1.Columns.Item(ColumnIndex).ColumnName.ToUpper Then Return False

            Next

        Catch ex As Exception
            'most likely error is missing column
            Return False

        End Try

        Return True

    End Function

    Private Function OverrideEventHeaderCreateStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns

            .Add("ID", System.Type.GetType("System.Int16"))
            .Add("EventTypeID", System.Type.GetType("System.Int16"))
            .Add("Description", System.Type.GetType("System.String"))
            .Add("StartDate", System.Type.GetType("System.DateTime"))
            .Add("EndDate", System.Type.GetType("System.DateTime"))
            .Add("Revision", System.Type.GetType("System.Int16"))
            .Add("CashierSecurityID", System.Type.GetType("System.Int16"))
            .Add("AuthorisorSecurityID", System.Type.GetType("System.Int16"))

        End With

        Return DT

    End Function

    Private Function OverrideEventDetailCreateStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns

            .Add("ID", System.Type.GetType("System.Int16"))
            .Add("EventHeaderOverrideID", System.Type.GetType("System.Int16"))
            .Add("SkuNumber", System.Type.GetType("System.String"))
            .Add("Price", System.Type.GetType("System.Decimal"))

        End With

        Return DT

    End Function

#End Region

End Class