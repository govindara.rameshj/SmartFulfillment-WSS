﻿<TestClass()> Public Class EventRepositoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : EventRepository | Public Function : GetExistingTemporaryPriceChangeForSkuHeader | N/A"

#End Region

#Region "Class : EventRepository | Public Function : GetExistingTemporaryPriceChangeForSkuDetail | N/A"

#End Region

#Region "Class : EventRepository | Public Function : GetOverrideTemporaryPriceChangeForSkuHeader | N/A"

#End Region

#Region "Class : EventRepository | Public Function : GetOverrideTemporaryPriceChangeForSkuDetail | N/A"

#End Region

#Region "Class : EventRepository | Public Function : GetExistingDealGroup | N/A"

#End Region

#Region "Class : EventRepository | Public Function : GetOverrideDealGroup | N/A"

#End Region

#Region "Class : EventRepository | Public Function : GetEventMaster | N/A"

#End Region

#Region "Class : EventRepository | Public Function : GetValidEvent | N/A"

#End Region

#Region "Class : EventRepository | Public Function : SetOverrideTemporaryPriceChangeForSku | N/A"

#End Region

#Region "Class : EventRepository | Private Procedures"

    <TestMethod()> Public Sub ProcedureSingleSkuPriceOverrideGetExistingHeader_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        Repository.SingleSkuPriceOverrideGetExisting(Com, SqlServer.My.Resources.Procedures.GetExistingTemporaryPriceChangeForSkuHeader, Nothing, Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual("usp_GetEventExistingTemporaryPriceChangeForSkuHeader", Com.StoredProcedureName)

        Assert.AreEqual(5, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@EventTypeID", Com.ParameterName(0), "Parameter Name - Item Zero")
        Assert.AreEqual("@SkuNumber", Com.ParameterName(1), "Parameter Name - Item One")
        Assert.AreEqual("@StartDate", Com.ParameterName(2), "Parameter Name - Item Two")
        Assert.AreEqual("@EndDate", Com.ParameterName(3), "Parameter Name - Item Three")
        Assert.AreEqual("@Deleted", Com.ParameterName(4), "Parameter Name - Item Four")

        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")
        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(1), "Parameter Type - Item One")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(2), "Parameter Type - Item Two")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(3), "Parameter Type - Item Three")
        Assert.AreEqual(SqlDbType.Bit, Com.ParameterTypeProperty(4), "Parameter Type - Item Four")

    End Sub

    <TestMethod()> Public Sub ProcedureSingleSkuPriceOverrideGetExistingDetail_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        Repository.SingleSkuPriceOverrideGetExisting(Com, SqlServer.My.Resources.Procedures.GetExistingTemporaryPriceChangeForSkuDetail, Nothing, Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual("usp_GetEventExistingTemporaryPriceChangeForSkuDetail", Com.StoredProcedureName)

        Assert.AreEqual(5, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@EventTypeID", Com.ParameterName(0), "Parameter Name - Item Zero")
        Assert.AreEqual("@SkuNumber", Com.ParameterName(1), "Parameter Name - Item One")
        Assert.AreEqual("@StartDate", Com.ParameterName(2), "Parameter Name - Item Two")
        Assert.AreEqual("@EndDate", Com.ParameterName(3), "Parameter Name - Item Three")
        Assert.AreEqual("@Deleted", Com.ParameterName(4), "Parameter Name - Item Four")

        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")
        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(1), "Parameter Type - Item One")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(2), "Parameter Type - Item Two")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(3), "Parameter Type - Item Three")
        Assert.AreEqual(SqlDbType.Bit, Com.ParameterTypeProperty(4), "Parameter Type - Item Four")

    End Sub

    <TestMethod()> Public Sub ProcedureSingleSkuPriceOverrideGetOverrideHeader_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)
        Repository.SingleSkuPriceOverrideGetOverride(Com, SqlServer.My.Resources.Procedures.GetOverrideTemporaryPriceChangeForSkuHeader, Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual("usp_GetEventOverrideTemporaryPriceChangeForSkuHeader", Com.StoredProcedureName, "Stored Procedure - Header")

        Assert.AreEqual(4, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@EventTypeID", Com.ParameterName(0), "Parameter Name - Item Zero")
        Assert.AreEqual("@SkuNumber", Com.ParameterName(1), "Parameter Name - Item One")
        Assert.AreEqual("@StartDate", Com.ParameterName(2), "Parameter Name - Item Two")
        Assert.AreEqual("@EndDate", Com.ParameterName(3), "Parameter Name - Item Three")

        Assert.AreEqual(SqlDbType.Int, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")
        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(1), "Parameter Type - Item One")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(2), "Parameter Type - Item Two")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(3), "Parameter Type - Item Three")

    End Sub

    <TestMethod()> Public Sub ProcedureSingleSkuPriceOverrideGetOverrideDetail_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)
        Repository.SingleSkuPriceOverrideGetOverride(Com, SqlServer.My.Resources.Procedures.GetOverrideTemporaryPriceChangeForSkuDetail, Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual("usp_GetEventOverrideTemporaryPriceChangeForSkuDetail", Com.StoredProcedureName, "Stored Procedure - Header")

        Assert.AreEqual(4, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@EventTypeID", Com.ParameterName(0), "Parameter Name - Item Zero")
        Assert.AreEqual("@SkuNumber", Com.ParameterName(1), "Parameter Name - Item One")
        Assert.AreEqual("@StartDate", Com.ParameterName(2), "Parameter Name - Item Two")
        Assert.AreEqual("@EndDate", Com.ParameterName(3), "Parameter Name - Item Three")

        Assert.AreEqual(SqlDbType.Int, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")
        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(1), "Parameter Type - Item One")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(2), "Parameter Type - Item Two")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(3), "Parameter Type - Item Three")

    End Sub

    <TestMethod()> Public Sub ProcedureMultipleSkuPriceOverrideGetExistingDealGroup_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)
        Repository.MultipleSkuPriceOverrideGetExistingDealGroup(Com, SqlServer.My.Resources.Procedures.MultipleSkuPriceOverrideGetExistingDealGroup, Nothing)

        Assert.AreEqual("usp_GetExistingDealGroups", Com.StoredProcedureName, "Stored Procedure")

        Assert.AreEqual(1, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@SkuNumbers", Com.ParameterName(0), "Parameter Name - Item Zero")

        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")

    End Sub

    <TestMethod()> Public Sub ProcedureMultipleSkuPriceOverrideGetOverrideDealGroup_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)
        Repository.MultipleSkuPriceOverrideGetOverrideDealGroup(Com, SqlServer.My.Resources.Procedures.MultipleSkuPriceOverrideGetOverrideDealGroup, Nothing, Nothing)

        Assert.AreEqual("usp_GetOverrideDealGroups", Com.StoredProcedureName, "Stored Procedure")

        Assert.AreEqual(2, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@SelectedDate", Com.ParameterName(0), "Parameter Name - Item Zero")
        Assert.AreEqual("@SkuNumbers", Com.ParameterName(1), "Parameter Name - Item One")

        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")
        Assert.AreEqual(SqlDbType.VarChar, Com.ParameterTypeProperty(1), "Parameter Type - Item One")

    End Sub

    <TestMethod()> Public Sub ProcedureMultipleSkuPriceOverrideGetEventMaster_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        Repository.MultipleSkuPriceOverrideGetEventMaster(Com, SqlServer.My.Resources.Procedures.MultipleSkuPriceOverrideGetEventMaster, Nothing, Nothing)

        Assert.AreEqual("usp_GetEventMasterDealGroup", Com.StoredProcedureName, "Stored Procedure")

        Assert.AreEqual(2, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@SelectedDate", Com.ParameterName(0), "Parameter Name - Item Zero")
        Assert.AreEqual("@SkuNumbers", Com.ParameterName(1), "Parameter Name - Item One")

        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")
        Assert.AreEqual(SqlDbType.VarChar, Com.ParameterTypeProperty(1), "Parameter Type - Item One")

    End Sub

    <TestMethod()> Public Sub ProcedureMultipleSkuPriceOverrideGetValidEvent_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        Repository.MultipleSkuPriceOverrideGetValidEvent(Com, SqlServer.My.Resources.Procedures.MultipleSkuPriceOverrideGetValidEvent, Nothing, Nothing)

        Assert.AreEqual("usp_GetEventHeaderDealGroup", Com.StoredProcedureName, "Stored Procedure")

        Assert.AreEqual(2, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@EventNumber", Com.ParameterName(0), "Parameter Name - Item Zero")
        Assert.AreEqual("@Priority", Com.ParameterName(1), "Parameter Name - Item One")

        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")
        Assert.AreEqual(SqlDbType.Char, Com.ParameterTypeProperty(1), "Parameter Type - Item One")

    End Sub

    <TestMethod()> Public Sub ProcedureTemporaryPriceChangeSet_CorrectRepository()

        Dim Repository As New EventRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        Repository.TemporaryPriceChangeSet(Com, Nothing, Nothing, SqlServer.My.Resources.Procedures.SetTemporaryPriceChanges, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

        Assert.AreEqual("usp_SetTemporaryPriceChanges", Com.StoredProcedureName, "Stored Procedure")

        Assert.AreEqual(11, Com.ParameterCollectionCount, "Parameter Count")

        Assert.AreEqual("@EventTypeID", Com.ParameterName(0), "Parameter Name - Item Zero")
        Assert.AreEqual("@Description", Com.ParameterName(1), "Parameter Name - Item One")
        Assert.AreEqual("@StartDate", Com.ParameterName(2), "Parameter Name - Item Two")
        Assert.AreEqual("@EndDate", Com.ParameterName(3), "Parameter Name - Item Three")
        Assert.AreEqual("@SkuNumbers", Com.ParameterName(4), "Parameter Name - Item Four")
        Assert.AreEqual("@Quantities", Com.ParameterName(5), "Parameter Name - Item Five")
        Assert.AreEqual("@Price", Com.ParameterName(6), "Parameter Name - Item Six")
        Assert.AreEqual("@CashierSecurityID", Com.ParameterName(7), "Parameter Name - Item Seven")
        Assert.AreEqual("@AuthorisorSecurityID", Com.ParameterName(8), "Parameter Name - Item Eight")
        Assert.AreEqual("@EventHeaderID", Com.ParameterName(9), "Parameter Name - Item Nine")
        Assert.AreEqual("@Success", Com.ParameterName(10), "Parameter Name - Item Ten")

        Assert.AreEqual(SqlDbType.Int, Com.ParameterTypeProperty(0), "Parameter Type - Item Zero")
        Assert.AreEqual(SqlDbType.NVarChar, Com.ParameterTypeProperty(1), "Parameter Type - Item One")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(2), "Parameter Type - Item Two")
        Assert.AreEqual(SqlDbType.Date, Com.ParameterTypeProperty(3), "Parameter Type - Item Three")
        Assert.AreEqual(SqlDbType.VarChar, Com.ParameterTypeProperty(4), "Parameter Type - Item Four")
        Assert.AreEqual(SqlDbType.VarChar, Com.ParameterTypeProperty(5), "Parameter Type - Item Five")
        Assert.AreEqual(SqlDbType.Decimal, Com.ParameterTypeProperty(6), "Parameter Type - Item Six")
        Assert.AreEqual(SqlDbType.Int, Com.ParameterTypeProperty(7), "Parameter Type - Item Seven")
        Assert.AreEqual(SqlDbType.Int, Com.ParameterTypeProperty(8), "Parameter Type - Item Eight")
        Assert.AreEqual(SqlDbType.Int, Com.ParameterTypeProperty(9), "Parameter Type - Item Nine")
        Assert.AreEqual(SqlDbType.Bit, Com.ParameterTypeProperty(10), "Parameter Type - Item Ten")

    End Sub

#End Region

End Class
