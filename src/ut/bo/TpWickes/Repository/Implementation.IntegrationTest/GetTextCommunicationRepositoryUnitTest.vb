﻿<TestClass()> Public Class GetTextCommunicationRepositoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : GetTextCommunicationRepository | Public Function : GetTextCommunications"

#End Region

#Region "Class : GetTextCommunicationRepository | Private Function : GetTextCommunications"

#End Region

#Region "Class : GetTextCommunicationRepository | Private Procedure : AddStoredProcedure"

    <TestMethod()> Public Sub ProcedureAddStoredProcedure_CorrectNameReturned()

        Dim X As New GetTextCommunicationRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        Com = New Command(Con)

        X.AddStoredProcedure(Com)

        Assert.AreEqual("usp_TextCommunicationsGet", Com.StoredProcedureName)

    End Sub

#End Region

#Region "Class : GetTextCommunicationRepository | Private Procedure : AddParameters"

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_FunctionalAreaIDOne_TypeIDOne_ParameterCollectionCountTwo()

        Dim X As New GetTextCommunicationRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, 1, 1)

        Assert.AreEqual(2, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_FunctionalAreaIDOne_TypeIDOne_ParameterCollectionItemZeroCorrectNameReturned()

        Dim X As New GetTextCommunicationRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, 1, 1)

        Assert.AreEqual("@FunctionalAreaId", Com.ParameterName(0))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_FunctionalAreaIDOne_TypeIDOne_ParameterCollectionItemOneCorrectNameReturned()

        Dim X As New GetTextCommunicationRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, 1, 1)

        Assert.AreEqual("@TypeId", Com.ParameterName(1))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_FunctionalAreaIDOne_TypeIDOne_SequenceNumberOne_ParameterCollectionCountThree()

        Dim X As New GetTextCommunicationRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, 1, 1, 1)

        Assert.AreEqual(3, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_FunctionalAreaIDOne_TypeIDOne_SequenceNumberOne_ParameterCollectionItemZeroCorrectNameReturned()

        Dim X As New GetTextCommunicationRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, 1, 1, 1)

        Assert.AreEqual("@FunctionalAreaId", Com.ParameterName(0))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_FunctionalAreaIDOne_TypeIDOne_SequenceNumberOne_ParameterCollectionItemOneCorrectNameReturned()

        Dim X As New GetTextCommunicationRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, 1, 1, 1)

        Assert.AreEqual("@TypeId", Com.ParameterName(1))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_FunctionalAreaIDOne_TypeIDOne_SequenceNumberOne_ParameterCollectionItemTwoCorrectNameReturned()

        Dim X As New GetTextCommunicationRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, 1, 1, 1)

        Assert.AreEqual("@Sequence", Com.ParameterName(2))

    End Sub

#End Region

End Class
