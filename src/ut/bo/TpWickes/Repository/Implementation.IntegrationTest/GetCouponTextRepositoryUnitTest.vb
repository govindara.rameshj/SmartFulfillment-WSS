﻿<TestClass()> Public Class GetCouponTextRepositoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : GetCouponTextRepository | Public Function : GetCouponTextIncludeDeletedTrue"

#End Region

#Region "Class : GetCouponTextRepository | Public Function : GetCouponTextIncludeDeletedFalse"

#End Region

#Region "Class : GetCouponTextRepository | Private Function : GetCouponText"

#End Region

#Region "Class : GetCouponTextRepository | Private Procedure : AddStoredProcedure"

    <TestMethod()> Public Sub ProcedureAddStoredProcedure_CorrectNameReturned()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddStoredProcedure(Com)

        Assert.AreEqual("usp_CouponTextGet", Com.StoredProcedureName)

    End Sub

#End Region

#Region "Class : GetCouponTextRepository | Private Procedure : AddParameters"

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_DeletedFalse_ParameterCollectionCountOne()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", False)

        Assert.AreEqual(1, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_DeletedTrue_ParameterCollectionCountTwo()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", True)

        Assert.AreEqual(2, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_DeletedTrue_ParameterCollectionItemZeroCorrectNameReturned()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", True)

        Assert.AreEqual("@CouponId", Com.ParameterName(0))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_DeletedTrue_ParameterCollectionItemOneCorrectNameReturned()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", True)

        Assert.AreEqual("@IncludeDeleted", Com.ParameterName(1))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_SequenceNoOne_DeletedFalse_ParameterCollectionCountTwo()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", "1", False)

        Assert.AreEqual(2, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_SequenceNoOne_DeletedTrue_ParameterCollectionCountThree()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", "1", True)

        Assert.AreEqual(3, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_SequenceNoOne_DeletedTrue_ParameterCollectionItemZeroCorrectNameReturned()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", "1", True)

        Assert.AreEqual("@CouponId", Com.ParameterName(0))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_SequenceNoOne_DeletedTrue_ParameterCollectionItemOneCorrectNameReturned()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", "1", True)

        Assert.AreEqual("@SequenceNumber", Com.ParameterName(1))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_SequenceNoOne_DeletedTrue_ParameterCollectionItemTwoCorrectNameReturned()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddParameters(Com, "1", "1", True)

        Assert.AreEqual("@IncludeDeleted", Com.ParameterName(2))

    End Sub

#End Region

#Region "Class : GetCouponTextRepository | Private Procedure : DeletedParameter"

    <TestMethod()> Public Sub ProcedureAddDeletedParameter_ParameterCollectionCountOne()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddDeletedParameter(Com)

        Assert.AreEqual(1, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddDeletedParameter_ParameterCollectionItemZeroCorrectNameReturned()

        Dim X As New GetCouponTextRepository
        Dim Con As IConnection
        Dim Com As Command

        Con = New Connection
        Com = New Command(Con)

        X.AddDeletedParameter(Com)

        Assert.AreEqual("@IncludeDeleted", Com.ParameterName(0))

    End Sub

#End Region

End Class