﻿<TestClass()> Public Class GetCouponRepositoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Class : GetCouponRepository | Public Function : GetCouponDeletedTrue"

#End Region

#Region "Class : GetCouponRepository | Public Function : GetCouponDeletedFalse"

#End Region

#Region "Class : GetCouponRepository | Private Function : GetCoupon"

    <TestMethod()> Public Sub FunctionGetCoupon_InitialValue_DataTableNothing_ReturnNothing()

        Dim Mock As New MockRepository

        Dim X As New GetCouponRepository
        Dim Command As ICommand
        Dim DT As DataTable

        'arrange
        Command = Mock.Stub(Of ICommand)()

        DT = Nothing
        SetupResult.On(Command).Call(Command.ExecuteDataTable).Return(DT).IgnoreArguments()
        CommandFactory.FactorySet(Command)
        Mock.ReplayAll()

        Assert.AreEqual(Nothing, X.GetCoupon("999", True))

    End Sub

    <TestMethod()> Public Sub FunctionGetCoupon_InitialValue_DataTableZeroRecords_ReturnNothing()

        Dim Mock As New MockRepository

        Dim X As New GetCouponRepository
        Dim Command As ICommand
        Dim DT As DataTable

        'arrange
        Command = Mock.Stub(Of ICommand)()

        DT = CouponMasterDataTableConfigureStructure()
        SetupResult.On(Command).Call(Command.ExecuteDataTable).Return(DT).IgnoreArguments()
        CommandFactory.FactorySet(Command)
        Mock.ReplayAll()

        Assert.AreEqual(Nothing, X.GetCoupon("999", True))

    End Sub

    <TestMethod()> Public Sub FunctionGetCoupon_InitialValue_DataTableOneRecords_ReturnOneRecord()

        Dim Mock As New MockRepository

        Dim X As New GetCouponRepository
        Dim Offline As IOfflineModeRepository
        Dim Command As ICommand
        Dim DT As DataTable

        'arrange
        Command = Mock.Stub(Of ICommand)()
        DT = CouponMasterDataTableConfigureStructure()
        CouponMasterTableAddData(DT, "XXXXX", "Description", False, False, False, False, False, False)
        SetupResult.On(Command).Call(Command.ExecuteDataTable).Return(DT).IgnoreArguments()
        CommandFactory.FactorySet(Command)

        Offline = Mock.Stub(Of IOfflineModeRepository)()
        SetupResult.On(Offline).Call(Offline.RequirementIsEnabled).Return(False).IgnoreArguments()
        OfflineModeRepositoryFactory.FactorySet(Offline)

        Mock.ReplayAll()

        Assert.AreNotEqual(Nothing, X.GetCoupon("999", True))

    End Sub

#End Region

#Region "Class : GetCouponRepository | Private Procedure : AddStoredProcedure"

    <TestMethod()> Public Sub ProcedureAddStoredProcedure_CorrectNameReturned()

        Dim X As New GetCouponRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)

        X.AddStoredProcedure(Com)

        Assert.AreEqual("usp_CouponMasterGet", Com.StoredProcedureName)

    End Sub

#End Region

#Region "Class : GetCouponRepository | Private Procedure : AddParameters"

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_DeletedFalse_ParameterCollectionCountOne()

        Dim X As New GetCouponRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)

        X.AddParameters(Com, "1", False)

        Assert.AreEqual(1, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_DeletedTrue_ParameterCollectionCountTwo()

        Dim X As New GetCouponRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)

        X.AddParameters(Com, "1", True)

        Assert.AreEqual(2, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_DeletedTrue_ParameterCollectionItemZeroCorrectNameReturned()

        Dim X As New GetCouponRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)

        X.AddParameters(Com, "1", True)

        Assert.AreEqual("@CouponId", Com.ParameterName(0))

    End Sub

    <TestMethod()> Public Sub ProcedureAddParameters_InitialValue_CouponIdOne_DeletedTrue_ParameterCollectionItemOneCorrectNameReturned()

        Dim X As New GetCouponRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)

        X.AddParameters(Com, "1", True)

        Assert.AreEqual("@IncludeDeleted", Com.ParameterName(1))

    End Sub

#End Region

#Region "Class : GetCouponRepository | Private Procedure : DeletedParameter"

    <TestMethod()> Public Sub ProcedureAddDeletedParameter_ParameterCollectionCountOne()

        Dim X As New GetCouponRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)

        X.AddDeletedParameter(Com)

        Assert.AreEqual(1, Com.ParameterCollectionCount)

    End Sub

    <TestMethod()> Public Sub ProcedureAddDeletedParameter_ParameterCollectionItemZeroCorrectNameReturned()

        Dim X As New GetCouponRepository
        Dim Con As IConnection
        Dim Com As ICommand

        Con = New Connection
        CommandFactory.FactorySet(Nothing)
        Com = CommandFactory.FactoryGet(Con)

        X.AddDeletedParameter(Com)

        Assert.AreEqual("@IncludeDeleted", Com.ParameterName(0))

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Function CouponMasterDataTableConfigureStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns
            .Add("COUPONID", System.Type.GetType("System.String"))
            .Add("DESCRIPTION", System.Type.GetType("System.String"))
            .Add("STOREGEN", System.Type.GetType("System.Boolean"))
            .Add("SERIALNO", System.Type.GetType("System.Boolean"))
            .Add("EXCCOUPON", System.Type.GetType("System.Boolean"))
            .Add("REUSABLE", System.Type.GetType("System.Boolean"))
            .Add("COLLECTINFO", System.Type.GetType("System.Boolean"))
            .Add("DELETED", System.Type.GetType("System.Boolean"))
        End With

        Return DT

    End Function

    Friend Sub CouponMasterTableAddData(ByRef DT As DataTable, _
                                        ByVal CouponID As String, ByVal Description As String, _
                                        ByVal StoreGenerated As Boolean, ByVal SerialNumber As Boolean, _
                                        ByVal Exclusive As Boolean, ByVal Reusable As Boolean, _
                                        ByVal RequiresCustomerInfo As Boolean, ByVal Deleted As Boolean)

        DT.Rows.Add(CouponID, Description, StoreGenerated, SerialNumber, Exclusive, Reusable, RequiresCustomerInfo, Deleted)

    End Sub

#End Region

End Class