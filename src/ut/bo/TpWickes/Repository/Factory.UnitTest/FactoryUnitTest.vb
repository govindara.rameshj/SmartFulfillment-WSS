﻿<TestClass()> Public Class FactoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory"

    <TestMethod()> Public Sub CouponRepositoryFactory_ReturnCorrectObject()

        Dim X As IGetCouponRepository

        X = GetCouponRepositoryFactory.FactoryGet

        Assert.AreEqual("TpWickes.Repository.Implementation.SqlServer.GetCouponRepository", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub CouponTextLineRepositoryFactory_ReturnCorrectObject()

        Dim X As IGetCouponTextRepository

        X = GetCouponTextRepositoryFactory.FactoryGet

        Assert.AreEqual("TpWickes.Repository.Implementation.SqlServer.GetCouponTextRepository", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SaleCouponRepositoryFactory_ReturnCorrectObject()

        Dim X As IGetSaleCouponRepository

        X = GetSaleCouponRepositoryFactory.FactoryGet

        Assert.AreEqual("TpWickes.Repository.Implementation.SqlServer.GetSaleCouponRepository", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub TextCommunicationRepositoryFactory_ReturnCorrectObject()

        Dim X As IGetTextCommunicationRepository

        X = GetTextCommunicationRepositoryFactory.FactoryGet

        Assert.AreEqual("TpWickes.Repository.Implementation.SqlServer.GetTextCommunicationRepository", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EventRepositoryFactory_ReturnCorrectObject()

        Dim X As IEventRepository

        X = EventRepositoryFactory.FactoryGet

        Assert.AreEqual("TpWickes.Repository.Implementation.SqlServer.EventRepository", X.GetType.FullName)

    End Sub

#End Region

End Class