﻿<TestClass()> Public Class EnquirySelectionUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Factory"

    <TestMethod()> Public Sub RequirementSwitchOn_ReturnClassSelectionColumnDisplay()

        Dim X As IEnquirySelection

        ArrangeRequirementSwitchDependency(True)
        X = (New EnquirySelectionFactory).GetImplementation

        Assert.AreEqual("CtsControls.SelectionColumnDisplay", X.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementSwitchOff_ReturnClassSelectionColumnID()

        Dim X As IEnquirySelection

        ArrangeRequirementSwitchDependency(False)
        X = (New EnquirySelectionFactory).GetImplementation
        Assert.AreEqual("CtsControls.SelectionColumnID", X.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - Return correct column"

    <TestMethod()> Public Sub ClassSelectionColumnDisplay_SortOnDisplayColumn()

        Dim X As New SelectionColumnDisplay

        Assert.AreEqual("display column", X.SelectionColumn("id column", "display column"))

    End Sub

    <TestMethod()> Public Sub ClassSelectionColumnID_SortOnIDColumn()

        Dim X As New SelectionColumnID

        Assert.AreEqual("id column", X.SelectionColumn("id column", "display column"))

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class