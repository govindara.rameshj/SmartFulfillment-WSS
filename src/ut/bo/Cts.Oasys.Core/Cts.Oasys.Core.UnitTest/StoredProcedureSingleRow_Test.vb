﻿Imports System
Imports System.Data
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public MustInherit Class StoredProcedureSingleRow_Test
    Inherits StoredProcedure_Test


#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    MustOverride Function GetExistingMultipleDataAndKey(ByRef ExistingMultipleData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean
    MustOverride Overrides Function GetExistingDataAndKey(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean
    MustOverride Overrides Function GetExistingSQL() As String
    MustOverride Overrides Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Microsoft.VisualBasic.Collection) As Boolean
    MustOverride Overrides Function GetNonExistingKey(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean
    MustOverride Overrides Function GetNonExistingSQL() As String
    MustOverride Overrides ReadOnly Property StoredProcedureName() As String

    <TestMethod()> _
    Public Overridable Sub WhenDataExists_StoredProcedureDoesNotGetMultipleRecords()

        If InitialiseTests() Then
            Dim TrySPExisting As Object = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))

            If TrySPExisting IsNot Nothing AndAlso TypeOf TrySPExisting Is DataTable Then
                Dim SPExisting As DataTable = CType(TrySPExisting, DataTable)

                If SPExisting.Rows.Count > 1 Then
                    Assert.Fail(StoredProcedureName & " retrieved multiple records")
                End If
            Else
                Assert.Inconclusive("Intialisation failed as did not get a datatable using " & StoredProcedureName & " so cannot perform this test")
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Overridable Sub WhenDataExists_StoredProcedureGetsRecordByKey()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Dim KeyFields As New Collection

            If GetKeyFieldsNoValueRequired(KeyFields) Then
                For Each SPKey As Key In KeyFields
                    Assert.IsTrue(FieldsMatch(SPKey.DynamicSQLName, Message, SPKey.Type), Message)
                Next
            End If
        End If
    End Sub
End Class
