﻿'Imports Rhino.Mocks


<TestClass()> Public Class FunctionUnitTestClass

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Implementation"

    <TestMethod()> _
    Public Sub ReturnNewValidatedImplementation()
        Dim OM As IConvert = Nothing

        OM = (New ConvertFactory).GetImplementation
        Assert.IsTrue(OM.GetType.FullName = "Cts.Oasys.Core.Stock.TpWickes.ConvertNew")
    End Sub
#End Region

#Region "Class: Function New"

    <TestMethod()> _
    Public Sub Class_FunctionNew_Function_NullToEmptyString_Parameter_Nothing_ReturnNothing()
        Dim OM As IConvert = Nothing

        OM = (New ConvertFactory).GetImplementation
        Assert.AreEqual(String.Empty, OM.NullToEmptyString(Nothing))
    End Sub

    <TestMethod()> _
    Public Sub Class_FunctionNew_Function_NullToEmptyString_Parameter_StringEmpty_ReturnStringEmpty()
        Dim OM As IConvert = Nothing

        OM = (New ConvertFactory).GetImplementation
        Assert.AreEqual(String.Empty, OM.NullToEmptyString(String.Empty))
    End Sub

    <TestMethod()> _
    Public Sub Class_FunctionNew_Function_NullToEmptyString_Parameter_NonNullString_ReturnNonNullString()
        Dim OM As IConvert = Nothing

        OM = (New ConvertFactory).GetImplementation
        Assert.AreEqual("TEST", OM.NullToEmptyString("TEST"))
    End Sub
#End Region
End Class
