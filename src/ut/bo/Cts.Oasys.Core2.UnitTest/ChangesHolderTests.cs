using System;
using System.Collections.Generic;
using System.Reflection;
using Cts.Oasys.Core.Helpers;
using Cts.Oasys.Hubs.Core.Order.Qod;
using NUnit.Framework;

namespace Cts.Oasys.Core2.NUnit.UnitTest
{
    [TestFixture]
    public class ChangesHolderTests
    {
        private const string FakeValue = "12345";
        private  QodHeader previous;
        private  QodHeader current;
        private  List<PropertyInfo> trackingProperties;

        [SetUp]
        public void InitTest()
        {
            DateTime now = DateTime.Now;

            previous = new QodHeader(true)
            {
                DateDelivery = now,
                PhoneNumberMobile = FakeValue,
                PhoneNumber = FakeValue,
                PhoneNumberHome = FakeValue,
                PhoneNumberWork = FakeValue,
                CustomerName = FakeValue,
                CustomerPostcode = FakeValue,
                DeliveryAddress1 = FakeValue,
                DeliveryAddress2 = FakeValue,
                DeliveryAddress3 = FakeValue,
                DeliveryAddress4 = FakeValue
            };

            current = new QodHeader(true)
            {
                DateDelivery = now,
                PhoneNumberMobile = FakeValue,
                PhoneNumber = FakeValue,
                PhoneNumberHome = FakeValue,
                PhoneNumberWork = FakeValue,
                CustomerName = FakeValue,
                CustomerPostcode = FakeValue,
                DeliveryAddress1 = FakeValue,
                DeliveryAddress2 = FakeValue,
                DeliveryAddress3 = FakeValue,
                DeliveryAddress4 = FakeValue
            };

            trackingProperties = new List<PropertyInfo> {
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.DateDelivery),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.PhoneNumberMobile),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.PhoneNumber),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.PhoneNumberHome),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.PhoneNumberWork),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.CustomerName),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.CustomerPostcode),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.DeliveryAddress1),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.DeliveryAddress2),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.DeliveryAddress3),
                PropertyUtils.GetPropertyInfo((QodHeader f) => f.DeliveryAddress4)
               };
        }

        [Test]
        public void HasChangesReturnsFalse()
        {
            ChangesHolder<QodHeader> comparer = CreateComparer();
            Assert.AreEqual(false, comparer.HasChanges());
        }

        [Test]
        public void HasChangesReturnsTrueStringProperty()
        {
            current.DeliveryAddress1 = "new address";
            Assert.AreEqual(true, CreateComparer().HasChanges());
        }


        [Test]
        public void HasChangesReturnsTrueDateProperty()
        {
            current.DateDelivery = DateTime.Now.AddDays(1);
            Assert.AreEqual(true, CreateComparer().HasChanges());
        }

        [Test]
        public void HasChangeForPropertyReturnsTrue()
        {
            current.DateDelivery = DateTime.Now.AddDays(1);
            Assert.AreEqual(true, CreateComparer().HasChange(PropertyUtils.GetPropertyInfo((QodHeader f) => f.DateDelivery)));
        }

        private ChangesHolder<QodHeader> CreateComparer()
        {
            return new ChangesHolder<QodHeader>(previous, current, trackingProperties);
        }
    }
}
