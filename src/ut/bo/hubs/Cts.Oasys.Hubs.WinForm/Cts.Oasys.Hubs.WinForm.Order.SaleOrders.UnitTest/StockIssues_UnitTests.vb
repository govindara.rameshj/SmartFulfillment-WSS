﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports NSubstitute
Imports TpWickes.Hubs.Library

<TestClass()> Public Class StockIssues_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "RequirmentSwitch Test"
    <TestMethod()> Public Sub ReturnsMaintain_RequirementSwitch_ReturnsStockIssuesNegativeOnHand()
        Dim V As IStockIssues


        ArrangeRequirementSwitchDependency(True)

        V = (New StockIssuesFactory).GetImplementation

        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.StockIssuesNegativeOnHand", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub ReturnsMaintain_RequirementSwitch_ReturnsStockIssuesOldWay()
        Dim V As IStockIssues

        ArrangeRequirementSwitchDependency(False)

        V = (New StockIssuesFactory).GetImplementation

        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.StockIssuesOldWay", V.GetType.FullName)

    End Sub
#End Region

#Region "Test Code"

    <TestMethod()> Public Sub StockIssues_DeliveryStatusLessThan399AndStockOnHandNegative_ReturnsFalse()
        Dim s As New StockIssues
        With s
            s.QtyOnHand = -1
            s.DeliveryStatus = 100
            s.Skun = "101000"
            s.QtyToDeliver = 3
        End With
        Dim x As New StockIssuesNegativeOnHand
        Assert.IsFalse(x.IsStockAvailable(s))
    End Sub

    <TestMethod()> Public Sub StockIssues_DeliveryStatusEqual399AndStockOnHandPositive_ReturnsFalse()
        Dim s As New StockIssues
        With s
            s.QtyOnHand = 10
            s.DeliveryStatus = 399
            s.Skun = "101000"
            s.QtyToDeliver = 1
        End With
        Dim x As New StockIssuesNegativeOnHand
        Assert.IsFalse(x.IsStockAvailable(s))
    End Sub

    <TestMethod()> Public Sub StockIssues_DeliveryStatusEqual399AndStockOnHandZero_ReturnsFalse()
        Dim s As New StockIssues
        With s
            s.QtyOnHand = 0
            s.DeliveryStatus = 399
            s.Skun = "101000"
            s.QtyToDeliver = 1
        End With
        Dim x As New StockIssuesNegativeOnHand
        Assert.IsFalse(x.IsStockAvailable(s))
    End Sub

    <TestMethod()> Public Sub StockIssues_DeliveryStatusEqual399AndStockOnHandZero_ReturnsTrue()
        Dim s As New StockIssues
        With s
            s.QtyOnHand = -2
            s.DeliveryStatus = 399
            s.Skun = "101000"
            s.QtyToDeliver = 1
        End With
        Dim x As New StockIssuesNegativeOnHand
        Assert.IsTrue(x.IsStockAvailable(s))
    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)
        Dim requirement = Substitute.For(Of IRequirementRepository)()

        requirement.RequirementEnabled(Arg.Any(Of Integer)).Returns(returnValue)
        RequirementRepositoryFactory.FactorySet(requirement)

    End Sub

#End Region

End Class
