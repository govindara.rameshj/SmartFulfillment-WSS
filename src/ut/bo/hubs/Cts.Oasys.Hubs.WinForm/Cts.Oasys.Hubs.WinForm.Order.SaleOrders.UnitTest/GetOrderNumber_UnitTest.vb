﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class GetOrderNumber_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region
    Private Class TestGetOrderNumberFactory

        Inherits GetOrderNumberFactory

        Private _RequirementEnabled As Nullable(Of Boolean)

        Public Sub New(ByVal RequirementEnabled As Nullable(Of Boolean))

            _RequirementEnabled = RequirementEnabled

        End Sub

        Friend Overrides Function IsEnabled() As Boolean
            Return CBool(_RequirementEnabled)
        End Function

    End Class

    <TestMethod()> Public Sub GetOrderNumber_RequirementSwitch_ReturnsGetOrderNumber()
        Dim Repository As IGetOrderNumber
        Dim Factory As New TestGetOrderNumberFactory(True)

        Repository = Factory.FactoryGet
        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.GetOrderNumber", Repository.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub GetOrderNumber_RequirementSwitch_ReturnsGetOrderNumberDoesNothing()
        Dim Repository As IGetOrderNumber
        Dim Factory As New TestGetOrderNumberFactory(False)

        Repository = Factory.FactoryGet
        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.GetOrderNumberDoesNothing", Repository.GetType.FullName)

    End Sub

End Class
