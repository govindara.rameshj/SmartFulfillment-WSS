﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class PickNote_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    ''' 


    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub SetDescriptionAsWeight_CallPickNoteSetWeightDescription_Returns_UnitWeight()
        Dim pickNoteInstance As IPickNote = (New PickNoteFactory).Implementation
        Assert.AreEqual("Unit Weight", pickNoteInstance.SetWeightDescription())
    End Sub


    '<TestMethod()> Public Sub SetDescriptionAsWeight_CallPickNoteOriginalSetWeightDescription_Returns_Weight()
    '    Dim pickNoteInstance As IPickNote = (New PickNoteFactory).ImplementationB
    '    Assert.AreEqual("Weight", pickNoteInstance.SetWeightDescription())
    'End Sub

    <TestMethod()> Public Sub SetDescriptionAsVolume_CallPickNoteSetVolumeDescription_Returns_LineWeight()
        Dim pickNoteInstance As IPickNote = (New PickNoteFactory).Implementation
        Assert.AreEqual("Line Weight", pickNoteInstance.SetVolumeDescription())
    End Sub

    '<TestMethod()> Public Sub SetDescriptionAsVolume_CallPickNoteOriginalSetVolumeDescription_Returns_Volume()
    '    Dim pickNoteInstance As IPickNote = (New PickNoteFactory).ImplementationB
    '    Assert.AreEqual("Volume", pickNoteInstance.SetVolumeDescription())
    'End Sub

    <TestMethod()> Public Sub SetDescriptionAsQuantity_CallPickNoteSetQuantityDescription_Returns_QuantityOrdered()
        Dim pickNoteInstance As IPickNote = (New PickNoteFactory).Implementation
        Assert.AreEqual("Quantity Ordered", pickNoteInstance.SetQuantityDescription())
    End Sub

    '<TestMethod()> Public Sub SetDescriptionAsQuantity_CallPickNoteOriginalSetQuantityDescription_Returns_Quantity()
    '    Dim pickNoteInstance As IPickNote = (New PickNoteFactory).ImplementationB
    '    Assert.AreEqual("Quantity", pickNoteInstance.SetQuantityDescription())
    'End Sub

    <TestMethod()> Public Sub SetDescriptionAsPicked_CallPickNoteSetPickedDescription_Returns_QuantityPicked()
        Dim pickNoteInstance As IPickNote = (New PickNoteFactory).Implementation
        Assert.AreEqual("Quantity Picked", pickNoteInstance.SetPickedDescription())
    End Sub

    '<TestMethod()> Public Sub SetDescriptionAsPicked_CallPickNoteOriginalSetPickedDescription_Returns_Picked()
    '    Dim pickNoteInstance As IPickNote = (New PickNoteFactory).ImplementationB
    '    Assert.AreEqual("Picked", pickNoteInstance.SetPickedDescription())
    'End Sub

    <TestMethod()> Public Sub SetQuantity10AndWeight20_CallPickNoteUpdateVolume_Returns_200()
        Dim pickNoteInstance As IPickNote = (New PickNoteFactory).Implementation
        Assert.AreEqual(CDec(200), pickNoteInstance.UpdateVolume(10, 20))
    End Sub
    <TestMethod()> Public Sub SetQuantity0AndWeight10_CallPickNoteUpdateVolume_Returns_0()
        Dim pickNoteInstance As IPickNote = (New PickNoteFactory).Implementation
        Assert.AreEqual(CDec(0), pickNoteInstance.UpdateVolume(0, 10))
    End Sub
    ''<TestMethod()> Public Sub SetQuantity10AndWeight20_CallPickNoteOriginalUpdateVolume_Returns_0()
    ''    Dim pickNoteInstance As IPickNote = (New PickNoteFactory).ImplementationB
    ''    Assert.AreEqual(CDec(0), pickNoteInstance.UpdateVolume(10, 20))
    ''End Sub
    '<TestMethod()> Public Sub SetQuantity0AndWeight10_CallPickNoteOriginalUpdateVolume_Returns_0()
    '    Dim pickNoteInstance As IPickNote = (New PickNoteFactory).ImplementationB
    '    Assert.AreEqual(CDec(0), pickNoteInstance.UpdateVolume(0, 10))
    'End Sub

    <TestMethod()> Public Sub SetWaterMarkTransparency_CallPickNoteSetWatermarkTrasparency_Returns_170()
        Dim pickNoteInstance As IPickNote = (New PickNoteFactory).Implementation
        Assert.AreEqual(170, pickNoteInstance.SetWaterMarkTransparency)
    End Sub

    '<TestMethod()> Public Sub SetWaterMarkTransparency_CallPickNoteOriginalSetWatermarkTrasparency_Returns_230()
    '    Dim pickNoteInstance As IPickNote = (New PickNoteFactory).ImplementationB
    '    Assert.AreEqual(230, pickNoteInstance.SetWaterMarkTransparency)
    'End Sub

    '<TestMethod()> Public Sub CallPickNoteOriginalRedrawForm_Returns_False()
    '    Dim pickNoteInstance As IPickNote = (New PickNoteFactory).ImplementationB
    '    Assert.AreEqual(False, pickNoteInstance.ReDrawForm)
    'End Sub

    <TestMethod()> Public Sub CallPickNoteRedrawForm_Returns_True()
        Dim pickNoteInstance As IPickNote = (New PickNoteFactory).Implementation
        Assert.AreEqual(True, pickNoteInstance.ReDrawForm)
    End Sub

End Class
