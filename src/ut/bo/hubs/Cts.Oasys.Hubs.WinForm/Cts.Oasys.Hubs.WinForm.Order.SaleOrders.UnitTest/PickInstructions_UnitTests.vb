﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Cts.Oasys.Hubs.Core.Order.Qod

Public Class SetupQodHeader

    Public Shared Function SetDeliveryInstructions() As QodHeader
        Dim QodHeader As New QodHeader
        Dim Texts As New QodTextCollection
        Dim Text As New QodText

        Text.Text = "This is a delivery line instruction."
        Text.Type = "DI"
        Text.SellingStoreId = 8851
        Text.SellingStoreOrderId = 9900
        Text.OrderNumber = "111111"
        Text.Number = "01"
        QodHeader.Texts.Add(Text)

        Dim Text1 As New QodText
        Text1.Text = "Customer requires morning delivery."
        Text1.Type = "DI"
        Text1.SellingStoreId = 8851
        Text1.SellingStoreOrderId = 9900
        Text1.OrderNumber = "111111"
        Text1.Number = "02"
        QodHeader.Texts.Add(Text1)


        Dim Text3 As New QodText
        Text3.Text = "Stock of 1 x 207546 (Tyne Aluminium Door 1981x762mm LH) is available at alternate location 8259 (WICKES DARLINGTON) as at 10:35 on 24/10/12"
        Text3.Type = "PI"
        Text3.SellingStoreId = 8851
        Text3.SellingStoreOrderId = 9900
        Text3.OrderNumber = "111111"
        Text3.Number = "03"
        QodHeader.Texts.Add(Text3)

        Dim Text4 As New QodText
        Text4.Text = "If required, please make arrangements with the alternate location specified"
        Text4.Type = "PI"
        Text4.SellingStoreId = 8851
        Text4.SellingStoreOrderId = 9900
        Text4.OrderNumber = "111111"
        Text4.Number = "04"
        QodHeader.Texts.Add(Text4)

        Return QodHeader
    End Function

End Class

<TestClass()> Public Class PickInstructions_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub SetControlsVisibleProperty_ReturnsTrue()
        Dim x As New PickInstructions

        Assert.IsTrue(x.SetControlsVisibleProperty)
    End Sub

    <TestMethod()> Public Sub SetControlsVisibleProperty_ReturnsFalse()
        Dim x As New PickInstructionsOldWay

        Assert.IsFalse(x.SetControlsVisibleProperty)
    End Sub

    <TestMethod()> Public Sub GetDeliveryInstruction_ReturnsOnlyDeliveryInstructions()
        Dim x As New PickInstructions
        Dim qod As QodHeader = SetupQodHeader.SetDeliveryInstructions

        Assert.AreEqual("This is a delivery line instruction. Customer requires morning delivery. ", x.GetDeliveryInstructions(qod))
    End Sub

    <TestMethod()> Public Sub GetDeliveryInstruction_ReturnsOnlyPickInstructions()
        Dim x As New PickInstructions
        Dim qod As QodHeader = SetupQodHeader.SetDeliveryInstructions
        Dim result = "Stock of 1 x 207546 (Tyne Aluminium Door 1981x762mm LH) is available at alternate location 8259 (WICKES DARLINGTON) as at 10:35 on 24/10/12 " & vbCrLf & _
                     "If required, please make arrangements with the alternate location specified " & vbCrLf
        Assert.AreEqual(result, x.GetPickingInstructions(qod))
    End Sub


End Class
