﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports NSubstitute
Imports TpWickes.Hubs.Library

<TestClass()> Public Class PickInstructionsFactory_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub GetImplementation_RequirementsSwitchOn_Returns_PickInstructions()
        Dim V As IPickInstructions

        ArrangeRequirementSwitchDependency(True)

        V = (New PickInstructionsFactory).GetImplementation

        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.PickInstructions", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub GetImplementation_RequirementsSwitchOff_Returns_PickInstructionsOldWay()
        Dim V As IPickInstructions

        ArrangeRequirementSwitchDependency(False)

        V = (New PickInstructionsFactory).GetImplementation

        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.PickInstructionsOldWay", V.GetType.FullName)

    End Sub

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)
        Dim requirement = Substitute.For(Of IRequirementRepository)()

        requirement.RequirementEnabled(Arg.Any(Of Integer)).Returns(returnValue)
        RequirementRepositoryFactory.FactorySet(requirement)

    End Sub

End Class
