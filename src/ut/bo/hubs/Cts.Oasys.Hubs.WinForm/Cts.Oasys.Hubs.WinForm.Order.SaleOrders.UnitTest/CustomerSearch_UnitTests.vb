﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TpWickes.Hubs.Library
Imports NSubstitute

<TestClass()> Public Class CustomerSearch_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Customer Search Tests"

    <TestMethod()> Public Sub CustomerSearch_PostCode_ReturnsTest()
        Dim V As New CustomerSearch

        Call V.SearchCriteriaParameters(0, "Test", "", "", "")
        Assert.AreSame(V._SearchCriteria.PostCode, "Test")

    End Sub

    <TestMethod()> Public Sub CustomerSearch_TelephoneNumber_Returns01249882233()
        Dim V As New CustomerSearch

        Call V.SearchCriteriaParameters(0, "", "01249882233", "", "")
        Assert.AreSame(V._SearchCriteria.TelephoneNumber, "01249882233")

    End Sub

    <TestMethod()> Public Sub CustomerSearch_CustomerName_ReturnsMr_Test()
        Dim V As New CustomerSearch

        Call V.SearchCriteriaParameters(0, "", "", "Mr Test", "")
        Assert.AreSame(V._SearchCriteria.CustomerName, "Mr Test")

    End Sub

    <TestMethod()> Public Sub CustomerSearch_OMOrderNumber_Returns123456()
        Dim V As New CustomerSearch

        Call V.SearchCriteriaParameters(123456, "", "", "", "")
        Assert.AreEqual(V._SearchCriteria.OrderManagerOrderNumber, 123456)

    End Sub

    <TestMethod()> Public Sub CustomerSearch_OvcOrderNumber_Returns123456789101112()
        Dim V As New CustomerSearch

        Call V.SearchCriteriaParameters(0, "", "", "", "123456789101112")
        Assert.AreEqual(V._SearchCriteria.OvcOrderNumber, "123456789101112")

    End Sub

#End Region


End Class
