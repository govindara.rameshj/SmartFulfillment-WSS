﻿Imports Cts.Oasys.Core.Tests
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()>
Public Class DISetup

    <AssemblyInitializeAttribute()>
    Public Shared Sub Setup(context As TestContext)
        TestEnvironmentSetup.SetupConnectionToAatDb()
    End Sub

End Class
