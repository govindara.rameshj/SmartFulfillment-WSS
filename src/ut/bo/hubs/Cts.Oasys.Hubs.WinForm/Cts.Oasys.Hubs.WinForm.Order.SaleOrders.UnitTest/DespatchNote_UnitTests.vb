﻿Imports System.Collections.ObjectModel
Imports Cts.Oasys.Hubs.Core.Order.Qod
Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Implementations
Imports Cts.Oasys.Hubs.WinForm.Order.SaleOrders.Interfaces
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class DespatchNote_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "ConvertQodLineToQodLine2109 Tests"

    <TestMethod()> _
    Public Sub ConvertQodLineToQodLine2109_CheckOutputValues_IsDeliveryItem_ValuesMatch()
        Dim qodLine2109 As IQodLine2109
        Dim despatchNote As New DespatchNote2109
        Dim inputQodLine As QodLine

        inputQodLine = GetCheckOutputQodLine()

        qodLine2109 = despatchNote.ConvertQodLineToQodLine2109(inputQodLine)
        Assert.AreEqual(inputQodLine.IsDeliveryChargeItem, qodLine2109.IsDeliveryChargeItem)
    End Sub

    <TestMethod()> _
    Public Sub ConvertQodLineToQodLine2109_CheckOutputValues_QtyTaken_ValuesMatch()
        Dim qodLine2109 As Interfaces.IQodLine2109
        Dim despatchNote As New DespatchNote2109
        Dim inputQodLine As QodLine

        inputQodLine = GetCheckOutputQodLine()

        qodLine2109 = despatchNote.ConvertQodLineToQodLine2109(inputQodLine)
        Assert.AreEqual(inputQodLine.QtyTaken, qodLine2109.QtyTaken)
    End Sub

    <TestMethod()> _
    Public Sub ConvertQodLineToQodLine2109_CheckOutputValues_Price_ValuesMatch()
        Dim qodLine2109 As Interfaces.IQodLine2109
        Dim despatchNote As New DespatchNote2109
        Dim inputQodLine As QodLine

        inputQodLine = GetCheckOutputQodLine()

        qodLine2109 = despatchNote.ConvertQodLineToQodLine2109(inputQodLine)
        Assert.AreEqual(inputQodLine.Price, qodLine2109.Price)
    End Sub

    <TestMethod()> _
    Public Sub ConvertQodLineToQodLine2109_CheckOutputValues_QtyOrdered_ValuesMatch()
        Dim qodLine2109 As Interfaces.IQodLine2109
        Dim despatchNote As New DespatchNote2109
        Dim inputQodLine As QodLine

        inputQodLine = GetCheckOutputQodLine()

        qodLine2109 = despatchNote.ConvertQodLineToQodLine2109(inputQodLine)
        Assert.AreEqual(inputQodLine.QtyOrdered, qodLine2109.QtyOrdered)
    End Sub

    <TestMethod()> _
    Public Sub ConvertQodLineToQodLine2109_CheckOutputValues_QtyRefunded_ValuesMatch()
        Dim qodLine2109 As Interfaces.IQodLine2109
        Dim despatchNote As New DespatchNote2109
        Dim inputQodLine As QodLine

        inputQodLine = GetCheckOutputQodLine()

        qodLine2109 = despatchNote.ConvertQodLineToQodLine2109(inputQodLine)
        Assert.AreEqual(inputQodLine.QtyRefunded, qodLine2109.QtyRefunded)
    End Sub

    Private Function GetCheckOutputQodLine() As QodLine
        GetCheckOutputQodLine = New QodLine

        With GetCheckOutputQodLine
            .IsDeliveryChargeItem = False
            .Price = 1.5@
            .QtyOrdered = 10
            .QtyRefunded = -2
            .QtyTaken = 5
        End With
    End Function
#End Region

#Region "ConvertQodLineCollectionToCollectionOfQodLine2109 Tests"

    <TestMethod()> _
    Public Sub ConvertQodLineCollectionToCollectionOfQodLine2109_PassEmptyQodLineCollection_ReturnsCollectionWithZeroLineCount()
        Dim inputQodLineCollection As New QodLineCollection
        Dim despatchNote As New DespatchNote2109
        Dim qodLine2109Lines As Collection(Of QodLine2109)

        qodLine2109Lines = despatchNote.ConvertQodLineCollectionToCollectionOfQodLine2109(inputQodLineCollection)
        Assert.IsTrue(qodLine2109Lines.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub ConvertQodLineCollectionToCollectionOfQodLine2109_PassNothing_ReturnsCollectionWithZeroLineCount()
        Dim inputQodLineCollection As QodLineCollection = Nothing
        Dim despatchNote As New DespatchNote2109
        Dim qodLine2109Lines As Collection(Of QodLine2109)

        qodLine2109Lines = despatchNote.ConvertQodLineCollectionToCollectionOfQodLine2109(inputQodLineCollection)
        Assert.IsTrue(qodLine2109Lines.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub ConvertQodLineCollectionToCollectionOfQodLine2109_Pass3LineQodLineCollection_ReturnsCollectionWith3LineCount()
        Dim inputQodLineCollection As New QodLineCollection
        Dim despatchNote As New DespatchNote2109
        Dim qodLine2109Lines As Collection(Of QodLine2109)

        inputQodLineCollection.Add(New QodLine())
        inputQodLineCollection.Add(New QodLine())
        inputQodLineCollection.Add(New QodLine())
        qodLine2109Lines = despatchNote.ConvertQodLineCollectionToCollectionOfQodLine2109(inputQodLineCollection)
        Assert.IsTrue(qodLine2109Lines.Count = 3)
    End Sub
#End Region

#Region "NetQuantity Tests"

    <TestMethod()> _
    Public Sub NetQuantity_ZeroTakenNegativeRefunded_SumValueCorrect()
        Dim qodLine2109 As New QodLine2109
        Dim despatchNote As New DespatchNote2109

        With qodLine2109
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        Assert.AreEqual(5, despatchNote.NetQuantity(qodLine2109))
    End Sub

    <TestMethod()> _
    Public Sub NetQuantity_PositiveTakenNegativeRefunded_SumValueCorrect()
        Dim qodLine2109 As New QodLine2109
        Dim despatchNote As New DespatchNote2109

        With qodLine2109
            .QtyOrdered = 10
            .QtyTaken = 5
            .QtyRefunded = -5
        End With
        Assert.AreEqual(0, despatchNote.NetQuantity(qodLine2109))
    End Sub

    <TestMethod()> _
    Public Sub NetQuantity_ZeroTakenZeroRefunded_SumValueCorrect()
        Dim qodLine2109 As New QodLine2109
        Dim despatchNote As New DespatchNote2109

        With qodLine2109
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        Assert.AreEqual(10, despatchNote.NetQuantity(qodLine2109))
    End Sub

    <TestMethod()> _
    Public Sub NetQuantity_PositiveTakenZeroRefunded_SumValueCorrect()
        Dim qodLine2109 As New QodLine2109
        Dim despatchNote As New DespatchNote2109

        With qodLine2109
            .QtyOrdered = 10
            .QtyTaken = 5
            .QtyRefunded = 0
        End With
        Assert.AreEqual(5, despatchNote.NetQuantity(qodLine2109))
    End Sub
#End Region

#Region "NetValue Tests"

    <TestMethod()> _
    Public Sub NetValue_WhenNetQuantityZero_NetValueIsZero()
        Dim qodLine2109 As New QodLine2109
        Dim despatchNote As New DespatchNote2109

        With qodLine2109
            .Price = 2
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 10
            .QtyTaken = 5
            .QtyRefunded = -5
        End With
        ' Should be product of price and net quantity
        Assert.AreEqual(0D, despatchNote.NetValue(qodLine2109))
    End Sub

    <TestMethod()> _
    Public Sub NetValue_WhenNetQuantity6AndPrice4_NetValueIs24()
        Dim qodLine2109 As New QodLine2109
        Dim despatchNote As New DespatchNote2109

        With qodLine2109
            .Price = 4
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = -4
        End With
        ' Should be product of price and net quantity
        Assert.AreEqual(24D, despatchNote.NetValue(qodLine2109))
    End Sub
#End Region

#Region "GetDeliveryCharge Tests"

    <TestMethod()> _
    Public Sub GetDeliveryCharge_NoDeliveryChargeLine_IsZero()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 4
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = -4
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 5
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 10
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -1
        End With
        testOrder.Lines.Add(testLine)
        Assert.AreEqual(0D, despatchNote.GetDeliveryCharge(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetDeliveryCharge_SingleDeliveryChargeLineValue20_Is20()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 4
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = -4
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 5
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            ' Single delivery charge item
            .IsDeliveryChargeItem = True
            .Price = 1
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 20
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        Assert.AreEqual(20D, despatchNote.GetDeliveryCharge(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetDeliveryCharge_MultipleDeliveryChargeLinesNoRefunds_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 4
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = -4
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 6
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 1
            .QtyOrdered = 20
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            .Price = 1
            ' Net Value should be 5
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Total net value (of delivery charge items) should be 25
        Assert.AreEqual(25D, despatchNote.GetDeliveryCharge(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetDeliveryCharge_MultipleDeliveryChargeLinesSingleRefund_IsSumOfLinesNetValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 4
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = -4
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 6
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            ' Net value 15
            .Price = 1
            .QtyOrdered = 15
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            ' Net value 5
            .Price = 1
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            ' Net value 0
            .Price = 1
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        ' Total net value (of delivery charge items) should be 20
        Assert.AreEqual(20D, despatchNote.GetDeliveryCharge(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetDeliveryCharge_MultipleDeliveryChargeLinesMultipleRefund_IsSumOfLinesNetValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 4
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = -4
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            .Price = 6
            ' Net Quantity calculated from the sum of these
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            ' Net value 15
            .Price = 1
            .QtyOrdered = 15
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            ' Net value 5
            .Price = 1
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            ' Net value 0
            .Price = 1
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine()
        With testLine
            .IsDeliveryChargeItem = True
            ' Net value 5
            .Price = 1
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        ' Total net value (of delivery charge items) should be 25
        Assert.AreEqual(25D, despatchNote.GetDeliveryCharge(testOrder))
    End Sub
#End Region

#Region "GetOrderValue Tests"

    <TestMethod()> _
    Public Sub GetOrderValue_NoLines_IsZero()
        Dim testOrder As New QodHeader
        Dim despatchNote As New DespatchNote2109

        ' QodHeader will default to a new QodLineCollection with no lines if the
        ' collection is not intialised before it is used and there is no QodHeader
        ' (Order) Number (property value set)
        Assert.AreEqual(0D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_SingleLine_IsValueOfLine()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        Assert.AreEqual(40D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLines_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(60D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesOneRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(80D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesMultipleRefunds_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 9
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 9
            .Price = 3
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(109D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesSingleDeliveryCharge_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(100D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesMultipleDeliveryCharge_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(116D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesSingleRefundMultipleDeliveryCharge_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 13
            .Price = 13
            .QtyOrdered = 3
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(129D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesMultipleRefundMultipleDeliveryCharge_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 13
            .Price = 13
            .QtyOrdered = 3
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 0
            .Price = 97.49@
            .QtyOrdered = 24
            .QtyTaken = 0
            .QtyRefunded = -24
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(129D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesMultipleDeliveryChargeOneWithRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 2
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 24
            .Price = 4
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(104D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesMultipleDeliveryChargeSeveralWithRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 2
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 21
            .Price = 7
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 39
            .Price = 13
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(156D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesWithOneRefundMultipleDeliveryChargeSeveralWithRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 6
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 2
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 21
            .Price = 7
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 39
            .Price = 13
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(176D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesSeveralWithRefundMultipleDeliveryChargeSeveralWithRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 6
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 45
            .Price = 15
            .QtyOrdered = 6
            .QtyTaken = 0
            .QtyRefunded = -3
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 6
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 2
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 21
            .Price = 7
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 39
            .Price = 13
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(241D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesOneTakenOneRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 30
            .Price = 5
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 7
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(110D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesMultipleTakenMultipleRefunds_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 30
            .Price = 5
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 10
            .Price = 10
            .QtyOrdered = 7
            .QtyTaken = 1
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 9
            .Price = 3
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(109D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesWithOneTakenAndASingleDeliveryCharge_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 30
            .Price = 5
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(130D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesWithSeveralTakenAndMultipleDeliveryCharge_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 12
            .Price = 12
            .QtyOrdered = 3
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 28
            .Price = 14
            .QtyOrdered = 3
            .QtyTaken = 1
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(156D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesSingleTakenSingleRefundMultipleDeliveryCharge_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 30
            .Price = 5
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 13
            .Price = 13
            .QtyOrdered = 3
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(159D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesMultipleTakenMultipleRefundMultipleDeliveryCharge_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 30
            .Price = 5
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 13
            .Price = 13
            .QtyOrdered = 5
            .QtyTaken = 2
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 0
            .Price = 97.49@
            .QtyOrdered = 24
            .QtyTaken = 0
            .QtyRefunded = -24
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 40
            .Price = 10
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(159D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesWithSingleTakenMultipleDeliveryChargeOneWithRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 30
            .Price = 5
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 2
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 24
            .Price = 4
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(134D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesMultipleTakenMultipleDeliveryChargeSeveralWithRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 30
            .Price = 5
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 94.15
            .Price = 13.45@
            .QtyOrdered = 12
            .QtyTaken = 5
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 2
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 21
            .Price = 7
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 39
            .Price = 13
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(280.15D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesWithOneTakenOneRefundMultipleDeliveryChargeSeveralWithRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 10
            .Price = 5
            .QtyOrdered = 6
            .QtyTaken = 2
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 2
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 21
            .Price = 7
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 39
            .Price = 13
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(166D, despatchNote.GetOrderValue(testOrder))
    End Sub

    <TestMethod()> _
    Public Sub GetOrderValue_MultipleLinesSeveralWithTakenAndRefundMultipleDeliveryChargeSeveralWithRefund_IsSumOfLineValues()
        Dim testOrder As New QodHeader
        Dim testLine As New QodLine
        Dim despatchNote As New DespatchNote2109

        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 40
            .Price = 4
            .QtyOrdered = 10
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 4
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 30
            .Price = 5
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 66.54
            .Price = 11.09@
            .QtyOrdered = 8
            .QtyTaken = 2
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 29.49
            .Price = 9.83@
            .QtyOrdered = 6
            .QtyTaken = 1
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 6
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 45
            .Price = 15
            .QtyOrdered = 6
            .QtyTaken = 0
            .QtyRefunded = -3
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            ' Should default to false but to be clear that this is the desired test data
            .IsDeliveryChargeItem = False
            ' Net Value should be 20
            .Price = 5
            .QtyOrdered = 6
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 20
            .Price = 10
            .QtyOrdered = 2
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 16
            .Price = 2
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = 0
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 21
            .Price = 7
            .QtyOrdered = 8
            .QtyTaken = 0
            .QtyRefunded = -5
        End With
        testOrder.Lines.Add(testLine)
        testLine = New QodLine
        With testLine
            .IsDeliveryChargeItem = True
            ' Net Value should be 39
            .Price = 13
            .QtyOrdered = 5
            .QtyTaken = 0
            .QtyRefunded = -2
        End With
        testOrder.Lines.Add(testLine)
        ' Order value should be sum of line net values
        Assert.AreEqual(367.03D, despatchNote.GetOrderValue(testOrder))
    End Sub
#End Region
End Class
