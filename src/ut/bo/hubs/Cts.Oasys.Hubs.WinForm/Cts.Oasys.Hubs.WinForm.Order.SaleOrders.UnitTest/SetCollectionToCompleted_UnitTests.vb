﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class SetCollectionToCompleted_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region
    Private Class TestSupportFactory

        Inherits SetCollectionToCompleteFactory

        Private _RequirementEnabled As Nullable(Of Boolean)

        Public Sub New(ByVal RequirementEnabled As Nullable(Of Boolean))

            _RequirementEnabled = RequirementEnabled

        End Sub

        Public Overrides Function IsEnabled() As Boolean
            Return CBool(_RequirementEnabled)
        End Function

        Public Function SetupQODHeaderLines(ByVal storeId As Integer, ByVal IsForDelivery As Boolean) As Core.Order.Qod.QodHeaderCollection
            Dim qods As New Core.Order.Qod.QodHeaderCollection
            Dim QodHeader As New Core.Order.Qod.QodHeader
            Dim QodLine As New Core.Order.Qod.QodLine
            QodHeader.IsForDelivery = IsForDelivery
            QodHeader.DeliveryStatus = 599
            QodLine.DeliveryStatus = 599
            QodLine.DeliverySource = storeId.ToString
            QodHeader.Lines.Add(QodLine)
            qods.Add(QodHeader)

            Return qods
        End Function

    End Class

    <TestMethod()> Public Sub SetCollectionToCompleteFactory_RequirementSwitch_ReturnsSetCollectionToComplete()
        Dim Repository As ISetCollectionToComplete
        Dim Factory As New TestSupportFactory(True)

        Repository = Factory.FactoryGet
        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.SetCollectionToComplete", Repository.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub SetCollectionToCompleteFactory_RequirementSwitch_ReturnsSetCollectionToCompleteDoesNothing()
        Dim Repository As ISetCollectionToComplete
        Dim Factory As New TestSupportFactory(False)

        Repository = Factory.FactoryGet
        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.SetCollectionToCompleteDoesNothing", Repository.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub SetCollectionToCompleteFactory_CallsSetCollectionToCompleteIsCollection_ReturnsDeliveryStatus700()
        Dim Repository As ISetCollectionToComplete
        Dim Factory As New TestSupportFactory(False)
        Repository = Factory.FactoryGet
        Dim qods As Core.Order.Qod.QodHeaderCollection = Factory.SetupQODHeaderLines(8851, False)
        Dim storeId As Integer = 8851
        Dim QodHeaderActionFilter As New Core.Order.Qod.QodHeaderActionFilter(qods, Core.Order.Qod.State.Delivery.PickingListCreated, Core.Order.Qod.State.Delivery.PickingStatusOk, storeId.ToString)
        Repository.SetCollectionToComplete(QodHeaderActionFilter, 8851)
        Assert.AreEqual(qods(0).DeliveryStatus, 700)
    End Sub

    <TestMethod()> Public Sub SetCollectionToCompleteFactory_CallsSetCollectionToCompleteIsDelivery_ReturnsDeliveryStatus700()
        Dim Repository As ISetCollectionToComplete
        Dim Factory As New TestSupportFactory(True)
        Repository = Factory.FactoryGet
        Dim qods As Core.Order.Qod.QodHeaderCollection = Factory.SetupQODHeaderLines(8851, True)
        Dim storeId As Integer = 8851
        Dim QodHeaderActionFilter As New Core.Order.Qod.QodHeaderActionFilter(qods, Core.Order.Qod.State.Delivery.PickingListCreated, Core.Order.Qod.State.Delivery.PickingStatusOk, storeId.ToString)
        Repository.SetCollectionToComplete(QodHeaderActionFilter, 8851)
        Assert.AreEqual(qods(0).DeliveryStatus, 700)
    End Sub

    <TestMethod()> Public Sub SetCollectionToCompleteFactory_CallsSetCollectionToCompleteIsCollection_ReturnsDeliveryStatus900()
        Dim Repository As ISetCollectionToComplete
        Dim Factory As New TestSupportFactory(True)
        Repository = Factory.FactoryGet
        Dim qods As Core.Order.Qod.QodHeaderCollection = Factory.SetupQODHeaderLines(8851, False)
        Dim storeId As Integer = 8851
        Dim QodHeaderActionFilter As New Core.Order.Qod.QodHeaderActionFilter(qods, Core.Order.Qod.State.Delivery.PickingListCreated, Core.Order.Qod.State.Delivery.PickingStatusOk, storeId.ToString)
        Repository.SetCollectionToComplete(QodHeaderActionFilter, 8851)
        Assert.AreEqual(qods(0).DeliveryStatus, 900)
    End Sub

End Class
