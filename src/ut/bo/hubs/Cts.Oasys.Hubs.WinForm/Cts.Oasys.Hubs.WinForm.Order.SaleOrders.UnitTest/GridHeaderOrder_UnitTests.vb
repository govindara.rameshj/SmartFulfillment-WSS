﻿Imports System
Imports System.Data
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class GridHeaderOrder_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region
#Region "Test Classes"

    Private Class TestGetHeaderOrderFactory

        Inherits GetHeaderOrderFactory

        Private _RequirementEnabled As Nullable(Of Boolean)

        Public Sub New(ByVal RequirementEnabled As Nullable(Of Boolean))

            _RequirementEnabled = RequirementEnabled

        End Sub

        Public Overrides Function ImplementationA_IsActive() As Boolean
            Return CBool(_RequirementEnabled)
        End Function

    End Class

#End Region


    <TestMethod()> Public Sub GridHeaderOrder_RequirementSwitch_ReturnsGridHeaderOrder()
        Dim Repository As IGridHeaderOrder
        Dim Factory As New TestGetHeaderOrderFactory(True)

        Repository = Factory.GetImplementation

        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.GridHeaderOrder", Repository.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub GridHeaderOrder_RequirementSwitch_ReturnsGridHeaderOrderOriginal()
        Dim Repository As IGridHeaderOrder
        Dim Factory As New TestGetHeaderOrderFactory(False)

        Repository = Factory.GetImplementation

        Assert.AreEqual("Cts.Oasys.Hubs.WinForm.Order.SaleOrders.GridHeaderOrderOriginal", Repository.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub GridHeaderOrder_UseExtendedColours_ReturnsTrue()
        Dim Repository As IGridHeaderOrder
        Dim Factory As New TestGetHeaderOrderFactory(True)

        Repository = Factory.GetImplementation

        Assert.AreEqual(True, Repository.UseExtendedColours())

    End Sub

    <TestMethod()> Public Sub GridHeaderOrder_GridHeaderOrder_UseExtendedColours_ReturnsFalse()
        Dim Repository As IGridHeaderOrder
        Dim Factory As New TestGetHeaderOrderFactory(False)

        Repository = Factory.GetImplementation

        Assert.AreEqual(False, Repository.UseExtendedColours())

    End Sub


End Class
