﻿<TestClass()> Public Class StockGetStock

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"

    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        _comparingAgainstDynamicSQL = True
                        Assert.IsTrue(SetupForTests, "Failed initialising for the Stock Stored Procedures tests.")
                    Case Else
                        Assert.Fail("Cannot perform Stock Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub

    <ClassCleanup()> Public Shared Sub MyClassCleanup()

        _existingStock = Nothing
        _storedProcedureStock = Nothing
        _storedProcedureNonExistingStock = Nothing
        _initialised = Nothing
        _nonExistingSkuNumber = Nothing
        _comparingAgainstDynamicSQL = Nothing
    End Sub

#End Region

#Region "Tests"

    <TestMethod()> Public Sub WhenExists_GetsStockItemData()

        Assert.IsNotNull(StoredProcedureStock, "GetStock did not retrieve any stock item data")
    End Sub

    <TestMethod()> Public Sub WhenExists_GetsAtLeastOneStockItem()

        If StoredProcedureStock IsNot Nothing Then
            If StoredProcedureStock.Rows.Count = 0 Then
                Assert.Fail("GetStock did not retrieve any stock items when should have retrieved the existing stock item (Sku Number = " & _existingStock.Item("SkuNumber").ToString.PadLeft(6, "0"c) & ")")
            End If
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_GetsAtLeastOneStockItem test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_GetsOnlyOneStockItem()

        If StoredProcedureStock IsNot Nothing Then
            If StoredProcedureStock.Rows.Count > 1 Then
                Assert.Fail("GetStock retrieved multiple stock items instead of just the existing stock item (Sku Number = " & _existingStock.Item("SkuNumber").ToString.PadLeft(6, "0"c) & ")")
            End If
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_GetsOnlyOneStockItem test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_GetsSpecifiedStockItemByNumber()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("SkuNumber", Message, FieldType.ftString), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_GetsSpecifiedStockItemNumber test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDescriptionMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("Description", Message, FieldType.ftString), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDescriptionMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockProductCodeMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("ProductCode", Message, FieldType.ftString), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockProductCodeMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockPackSizeMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("PackSize", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockPackSizeMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockPriceMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("Price", Message, FieldType.ftDecimal), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockPriceMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockCostMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("Cost", Message, FieldType.ftDecimal), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockCostMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockWeightMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("Weight", Message, FieldType.ftDecimal), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockWeightMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockOnHandQtyMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("OnHandQty", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockOnHandQtyMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockOnOrderQtyMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("OnOrderQty", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockOnOrderQtyMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockMinimumQtyMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("MinimumQty", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockMinimumQtyMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockMaximumQtyMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("MaximumQty", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockMaximumQtyMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockOpenReturnsQtyMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("OpenReturnsQty", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockOpenReturnsQtyMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockMarkdownQtyMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("MarkdownQty", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockMarkdownQtyMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockWriteOffQtyMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("WriteOffQty", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockWriteOffQtyMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockIsNonStockMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("IsNonStock", Message, FieldType.ftBoolean), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockIsNonStockMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockIsObsoleteMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("IsObsolete", Message, FieldType.ftBoolean), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockIsObsoleteMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockIsDeletedMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("IsDeleted", Message, FieldType.ftBoolean), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockIsDeletedMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockIsItemSingleMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("IsItemSingle", Message, FieldType.ftBoolean), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockIsItemSingleMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockIsNonOrderMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("IsNonOrder", Message, FieldType.ftBoolean), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockIsNonOrderMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockReceivedTodayQtyMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("ReceivedTodayQty", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockReceivedTodayQtyMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockReceivedTodayValueMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("ReceivedTodayValue", Message, FieldType.ftDecimal), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockReceivedTodayValueMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDateFirstStockedMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DateFirstStocked", Message, FieldType.ftDate), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDateFirstStockedMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDateFirstOrderMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DateFirstOrder", Message, FieldType.ftDate), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDateFirstOrderMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDateFinalOrderMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DateFinalOrder", Message, FieldType.ftDate), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDateFinalOrderMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDateLastReceivedMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DateLastReceived", Message, FieldType.ftDate), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDateLastReceivedMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockHieCategoryMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("HieCategory", Message, FieldType.ftString), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockHieCategoryMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockHieGroupMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("HieGroup", Message, FieldType.ftString), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockHieGroupMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockHieSubGroupMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("HieSubGroup", Message, FieldType.ftString), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockHieSubGroupMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockHieStyleMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("HieStyle", Message, FieldType.ftString), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockHieStyleMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockActivityTodayMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("ActivityToday", Message, FieldType.ftBoolean), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockActivityTodayMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockVatCodeMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("VatCode", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockVatCodeMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockSalesTypeMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("SalesType", Message, FieldType.ftString), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockSalesTypeMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockIsTaggedItemMatches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("IsTaggedItem", Message, FieldType.ftBoolean), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockIsTaggedItemMatches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales1Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales1", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales1Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales2Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales2", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales2Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales3Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales3", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales3Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales4Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales4", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales4Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales5Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales5", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales5Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales6Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales6", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales6Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales7Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales7", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales7Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales8Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales8", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales8Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales9Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales9", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales9Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales10Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales10", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales10Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales11Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales11", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales11Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales12Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales12", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales12Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales13Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales13", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales13Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockUnitSales14Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("UnitSales14", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockUnitSales14Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock1Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock1", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock1Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock2Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock2", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock2Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock3Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock3", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock3Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock4Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock4", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock4Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock5Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock5", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock5Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock6Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock6", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock6Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock7Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock7", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock7Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock8Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock8", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock8Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock9Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock9", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock9Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock10Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock10", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock10Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock11Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock11", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock11Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock12Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock12", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock12Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock13Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock13", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock13Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedStockDaysOutStock14Matches()
        Dim Message As String = ""

        If ExistingStock IsNot Nothing And StoredProcedureStock IsNot Nothing AndAlso StoredProcedureStock.Rows.Count = 1 Then
            Assert.IsTrue(FieldsMatch("DaysOutStock14", Message, FieldType.ftInteger), Message)
        Else
            Assert.Inconclusive("StockGetStock test failed a preceding test so WhenExists_RetrievedStockDaysOutStock14Matches test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenNotExists_GetsStockItemData()

        If StoredProcedureNonExistingStock IsNot Nothing AndAlso StoredProcedureNonExistingStock.Rows.Count > 0 Then
            Assert.Fail("GetStock retrieved a stock item for a non existent Sku Number (" & NonExistingSkuNumber & ")")
        End If
    End Sub
#End Region

#Region "Local properties"
    Private Shared _existingStock As DataRow
    Private Shared _storedProcedureStock As DataTable
    Private Shared _storedProcedureNonExistingStock As DataTable
    Private Shared _initialised As Boolean
    Private Shared _nonExistingSkuNumber As String
    Private Shared _comparingAgainstDynamicSQL As Boolean

    Private ReadOnly Property ExistingStock() As DataRow
        Get
            ExistingStock = _existingStock
        End Get
    End Property

    Private ReadOnly Property Initialised() As Boolean
        Get
            Initialised = _initialised
        End Get
    End Property

    Private ReadOnly Property NonExistingSkuNumber() As String
        Get
            NonExistingSkuNumber = _nonExistingSkuNumber
        End Get
    End Property

    Private ReadOnly Property StoredProcedureStock() As DataTable
        Get
            StoredProcedureStock = _storedProcedureStock
        End Get
    End Property

    Private ReadOnly Property StoredProcedureNonExistingStock() As DataTable
        Get
            StoredProcedureNonExistingStock = _storedProcedureNonExistingStock
        End Get
    End Property


#End Region

#Region "Local methods"

    Private Shared Function GetExistingStock() As Boolean
        Dim SelectSQL As String = GetStockSQL()
        Dim Min As Integer
        Dim Max As Integer
        Dim ExistingStock As DataTable = Nothing

        Using con As New Connection
            Using com As New Command(con)

                Min = 100000
                Do While Min < 999999
                    Max = Min + 99999
                    com.CommandText = SelectSQL
                    com.AddParameter("@SkuMin", Min.ToString.PadLeft(6, "0"c))
                    com.AddParameter("@SkuMax", Max.ToString.PadLeft(6, "0"c))
                    ExistingStock = com.ExecuteDataTable
                    If ExistingStock IsNot Nothing Then
                        If ExistingStock.Rows.Count > 0 Then
                            Exit Do
                        End If
                    End If
                    Min += 100000
                Loop
                If ExistingStock IsNot Nothing Then
                    _existingStock = ExistingStock.Rows(0)
                    GetExistingStock = True
                End If
            End Using
        End Using
    End Function

    Private Shared Function GetNonExistingStockNumber() As Boolean
        Dim SelectSQL As String = GetNonStockSQL()
        Dim Values() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        Dim ExistingStock As DataTable = Nothing
        Dim Sku As Integer = 100000


        Using con As New Connection
            Using com As New Command(con)

                Do While Sku < 999990
                    For i As Integer = 0 To 9
                        Values(i) += i
                    Next
                    com.CommandText = SelectSQL
                    com.AddParameter("@Sku1", (Sku + Values(0)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku2", (Sku + Values(1)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku3", (Sku + Values(2)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku4", (Sku + Values(3)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku5", (Sku + Values(4)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku6", (Sku + Values(5)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku7", (Sku + Values(6)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku8", (Sku + Values(7)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku9", (Sku + Values(8)).ToString.PadLeft(6, "0"c))
                    com.AddParameter("@Sku10", (Sku + Values(9)).ToString.PadLeft(6, "0"c))
                    ExistingStock = com.ExecuteDataTable
                    If ExistingStock Is Nothing OrElse ExistingStock.Rows.Count = 0 Then
                        _nonExistingSkuNumber = (Sku + Values(0)).ToString.PadLeft(6, "0"c)
                        GetNonExistingStockNumber = True
                        Exit Do
                    Else
                        If ExistingStock.Rows.Count < 10 Then
                            For s As Integer = 0 To 9
                                Dim SkuNumber As String = (Sku + Values(s)).ToString.PadLeft(6, "0"c)
                                Dim i As Integer
                                Dim Found As Boolean

                                Found = False
                                For i = 0 To ExistingStock.Rows.Count - 1
                                    If String.Equals(ExistingStock.Rows(i)("SkuNumber"), SkuNumber) Then
                                        Found = True
                                        Exit For
                                    End If
                                Next
                                If Not Found Then
                                    _nonExistingSkuNumber = SkuNumber
                                    GetNonExistingStockNumber = True
                                    Exit Do
                                End If
                            Next
                        End If
                    End If
                Loop
            End Using
        End Using
    End Function

    Private Shared Function GetStockUsingGetStockStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "StockGetStock"
                com.AddParameter("SkuNumber", _existingStock.Item("SkuNumber").ToString.PadLeft(6, "0"c))
                _storedProcedureStock = com.ExecuteDataTable
                GetStockUsingGetStockStoredProcedure = True
            End Using
        End Using
    End Function

    Private Shared Function GetNonExistingStockUsingGetStockStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "StockGetStock"
                com.AddParameter("SkuNumber", _nonExistingSkuNumber.PadLeft(6, "0"c))
                _storedProcedureNonExistingStock = com.ExecuteDataTable
                GetNonExistingStockUsingGetStockStoredProcedure = True
            End Using
        End Using
    End Function

    Private Shared Function GetNonStockSQL() As String
        Dim sb As New StringBuilder

        sb.Append("Select top(1) ")
        sb.Append("st.skun as SkuNumber ")
        sb.Append("from stkmas st where st.skun in (@Sku1, @Sku2, @Sku3, @Sku4, @Sku5, @Sku6, @Sku7, @Sku8, @Sku9, @Sku10)")

        Return sb.ToString
    End Function

    Private Shared Function GetStockSQL() As String
        Dim sb As New StringBuilder

        If _comparingAgainstDynamicSQL Then
            sb.Append("Select top(1) ")
            sb.Append("st.skun	as SkuNumber,")
            sb.Append("st.descr	as Description,")
            sb.Append("st.prod	as ProductCode,")
            sb.Append("st.pack	as PackSize,")
            sb.Append("st.pric	as Price,")
            sb.Append("st.COST	as Cost,")
            sb.Append("st.WGHT	as Weight,")
            sb.Append("st.onha	as OnHandQty,")
            sb.Append("st.onor  as OnOrderQty,")
            sb.Append("st.mini  as MinimumQty,")
            sb.Append("st.maxi  as MaximumQty,")
            sb.Append("st.retq  as OpenReturnsQty,")
            sb.Append("st.mdnq  as MarkdownQty,")
            sb.Append("st.wtfq  as WriteOffQty,")
            sb.Append("st.INON  as IsNonStock,")
            sb.Append("st.IOBS  as IsObsolete,")
            sb.Append("st.IDEL  as IsDeleted,")
            sb.Append("st.IRIS  as IsItemSingle,")
            sb.Append("st.NOOR  as IsNonOrder,")
            sb.Append("st.treq  as ReceivedTodayQty,")
            sb.Append("st.trev  as ReceivedTodayValue,")
            sb.Append("st.dats  as DateFirstStocked,")
            sb.Append("st.IODT  as DateFirstOrder,")
            sb.Append("st.FODT  as DateFinalOrder,")
            sb.Append("st.drec  as DateLastReceived,")
            sb.Append("st.CTGY  as HieCategory,")
            sb.Append("st.GRUP  as HieGroup,")
            sb.Append("st.SGRP  as HieSubgroup,")
            sb.Append("st.STYL  as HieStyle,")
            sb.Append("st.tact  as ActivityToday,")
            sb.Append("st.VATC  as VatCode,")
            sb.Append("st.SALT  as SalesType,")
            sb.Append("st.ITAG  as IsTaggedItem,")
            sb.Append("st.us001 as UnitSales1,")
            sb.Append("st.us002 as UnitSales2,")
            sb.Append("st.us003 as UnitSales3,")
            sb.Append("st.us004 as UnitSales4,")
            sb.Append("st.us005 as UnitSales5,")
            sb.Append("st.us006 as UnitSales6,")
            sb.Append("st.us007 as UnitSales7,")
            sb.Append("st.us008 as UnitSales8,")
            sb.Append("st.us009 as UnitSales9,")
            sb.Append("st.us0010 as UnitSales10,")
            sb.Append("st.us0011 as UnitSales11,")
            sb.Append("st.us0012 as UnitSales12,")
            sb.Append("st.us0013 as UnitSales13,")
            sb.Append("st.us0014 as UnitSales14,")
            sb.Append("st.do001 as DaysOutStock1,")
            sb.Append("st.do002 as DaysOutStock2,")
            sb.Append("st.do003 as DaysOutStock3,")
            sb.Append("st.do004 as DaysOutStock4,")
            sb.Append("st.do005 as DaysOutStock5,")
            sb.Append("st.do006 as DaysOutStock6,")
            sb.Append("st.do007 as DaysOutStock7,")
            sb.Append("st.do008 as DaysOutStock8,")
            sb.Append("st.do009 as DaysOutStock9,")
            sb.Append("st.do0010 as DaysOutStock10,")
            sb.Append("st.do0011 as DaysOutStock11,")
            sb.Append("st.do0012 as DaysOutStock12,")
            sb.Append("st.do0013 as DaysOutStock13,")
            sb.Append("st.do0014 as DaysOutStock14 ")
            sb.Append("from stkmas st where st.skun > @SkuMin and st.skun < @SkuMax")
        Else
        End If

        Return sb.ToString
    End Function

    Private Shared Function SetupForTests() As Boolean

        If Not _initialised Then
            If GetExistingStock() Then
                If GetNonExistingStockNumber() Then
                    If GetStockUsingGetStockStoredProcedure() Then
                        If GetNonExistingStockUsingGetStockStoredProcedure() Then
                            _initialised = True
                        End If
                    End If
                End If
            End If
        End If
        SetupForTests = _initialised
    End Function
#End Region

#Region "Field Comparisons"

    Private Enum FieldType
        ftString = 0
        ftInteger
        ftDecimal
        ftBoolean
        ftDate
    End Enum

    Private Function FieldsMatch(ByVal FieldName As String, ByRef Message As String, ByVal Type As FieldType) As Boolean
        Dim Marker As String = "Start"

        Try
            Marker = "Existing"
            If ExistingStock.Item(FieldName) IsNot Nothing Then
                Marker = "Retreived"
                If StoredProcedureStock.Rows(0).Item(FieldName) IsNot Nothing Then
                    Marker = "Select"
                    Select Case Type
                        Case FieldType.ftString
                            Marker = "String"
                            FieldsMatch = StringsMatch(ExistingStock.Item(FieldName), StoredProcedureStock.Rows(0).Item(FieldName), Message)
                        Case FieldType.ftInteger
                            Marker = "Integer"
                            FieldsMatch = IntegersMatch(ExistingStock.Item(FieldName), StoredProcedureStock.Rows(0).Item(FieldName), Message)
                        Case FieldType.ftBoolean
                            Marker = "Boolean"
                            FieldsMatch = BooleansMatch(ExistingStock.Item(FieldName), StoredProcedureStock.Rows(0).Item(FieldName), Message)
                        Case FieldType.ftDecimal
                            Marker = "Decimal"
                            FieldsMatch = DecimalsMatch(ExistingStock.Item(FieldName), StoredProcedureStock.Rows(0).Item(FieldName), Message)
                        Case FieldType.ftDate
                            Marker = "Date"
                            FieldsMatch = DatesMatch(ExistingStock.Item(FieldName), StoredProcedureStock.Rows(0).Item(FieldName), Message)
                        Case Else
                            Marker = "Not recognised"
                            Assert.Inconclusive("Cannot check field " & FieldName & " as field type specified is no recognised")
                    End Select
                Else
                    If _comparingAgainstDynamicSQL Then
                        Assert.Fail("StockGet Stored Procedure field name " & FieldName & " is not a field name in the original dynamic sql")
                    Else
                        Assert.Inconclusive("Cannot check field " & FieldName & ", it is not a field in the row returned by the stored procedure.")
                    End If
                End If
            Else
                Assert.Inconclusive("Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row.")
            End If
        Catch ex As Exception
            Select Case Marker
                Case "Start"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem at start of FieldsMatch function. " & ex.Message)
                Case "Existing"
                    Assert.Inconclusive("Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row.")
                Case "Retreived"
                    If _comparingAgainstDynamicSQL Then
                        Assert.Fail("StockGet Stored Procedure field name " & FieldName & " is not a field name in the original dynamic sql")
                    Else
                        Assert.Inconclusive("Cannot check field " & FieldName & ", it is not a field in the row returned by the stored procedure.")
                    End If
                Case "Select"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing field type. " & ex.Message)
                Case "String"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing string field type. " & ex.Message)
                Case "Integer"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing integer field type. " & ex.Message)
                Case "Boolean"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing boolean field type. " & ex.Message)
                Case "Decimal"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing decimal field type. " & ex.Message)
                Case "Date"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing date field type. " & ex.Message)
                Case "Not recognised"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing unrecognised field type. " & ex.Message)
                Case Else
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem with FieldsMatch function. " & ex.Message)
            End Select
        End Try
    End Function

    Private Function BooleansMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Booleans Match fail (" & FailReason & ")"
        Else
            If TypeOf Against Is Boolean Then
                Dim AgainstAsBoolean As Boolean = CBool(Against)

                If TypeOf Compare Is Boolean Then
                    Dim CompareAsBoolean As Boolean = CBool(Compare)

                    If AgainstAsBoolean = CompareAsBoolean Then
                        BooleansMatch = True
                    Else
                        FailReason = "Booleans Match fail as 'Against' (" & AgainstAsBoolean.ToString & ") and 'Compare' (" & CompareAsBoolean.ToString & ") are not the same"
                    End If
                Else
                    FailReason = "Booleans Match fail as 'Compare' is not a Boolean"
                End If
            Else
                FailReason = "Booleans Match fail as 'Against' is not a Boolean"
            End If
        End If
        Message = FailReason
    End Function

    Private Function DatesMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal DateInterval As DateInterval = DateInterval.Second, Optional ByVal AreNullable As Boolean = False) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Dates Match fail (" & FailReason & ")"
        Else
            Dim AgainstDate As Date
            Dim CompareDate As Date

            If AreNullable Then
                If TypeOf Against Is Date? Then
                    Dim AgainstAsNullableDate As Date?

                    If TypeOf Compare Is Date? Then
                        Dim CompareAsNullableDate As Date?

                        If AgainstAsNullableDate.HasValue Then
                            AgainstDate = AgainstAsNullableDate.Value
                            If CompareAsNullableDate.HasValue Then
                                CompareDate = CompareAsNullableDate.Value
                            Else
                                FailReason = "Dates Match fail as 'Against' is not Null but 'Compare' is Null"
                            End If
                        Else
                            If CompareAsNullableDate.HasValue Then
                                FailReason = "Dates Match fail as 'Against' is Null but 'Compare' is not Null"
                            Else
                                Message = FailReason
                                DatesMatch = True
                                Exit Function
                            End If
                        End If
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                    End If
                Else
                    FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                End If
            Else
                If TypeOf Against Is Date Then
                    AgainstDate = CDate(Against)
                    If TypeOf Compare Is Date Then
                        CompareDate = CDate(Compare)
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Date"
                    End If
                Else
                    FailReason = "Dates Match fail as 'Compare' is not a Date"
                End If
            End If
            Dim Diff As Boolean = DateDiff(DateInterval, AgainstDate, CompareDate) <> 0

            If Diff Then
                Dim Interval As String

                Select Case DateInterval
                    Case Microsoft.VisualBasic.DateInterval.Day
                        Interval = "days"
                    Case Microsoft.VisualBasic.DateInterval.DayOfYear
                        Interval = "days (of year)"
                    Case Microsoft.VisualBasic.DateInterval.Hour
                        Interval = "hours"
                    Case Microsoft.VisualBasic.DateInterval.Month
                        Interval = "months"
                    Case Microsoft.VisualBasic.DateInterval.Quarter
                        Interval = "quarters"
                    Case Microsoft.VisualBasic.DateInterval.Second
                        Interval = "seconds"
                    Case Microsoft.VisualBasic.DateInterval.Weekday
                        Interval = "week days"
                    Case Microsoft.VisualBasic.DateInterval.WeekOfYear
                        Interval = "weeks (of year)"
                    Case Microsoft.VisualBasic.DateInterval.Year
                        Interval = "years"
                    Case Else
                        Interval = "unknown units"
                End Select
                FailReason = "Dates Match fail as 'Against' (" & AgainstDate.ToString("F") & ") is " & CInt(Diff).ToString & " " & Interval & " different to 'Compare' (" & AgainstDate.ToString("F") & ")"
            Else
                DatesMatch = True
            End If
        End If
        Message = FailReason
    End Function

    Private Function DecimalsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Integers Match fail (" & FailReason & ")"
        Else
            Dim AgainstAsDecimal As Decimal

            If Decimal.TryParse(Against.ToString, AgainstAsDecimal) Then
                Dim CompareAsDecimal As Decimal

                If Decimal.TryParse(Compare.ToString, CompareAsDecimal) Then
                    If AgainstAsDecimal <> CompareAsDecimal Then
                        FailReason = "Decimals Match fail as 'Against' (= " & AgainstAsDecimal & ") and 'Compare' (= " & CompareAsDecimal & ") are not the same"
                    Else
                        DecimalsMatch = True
                    End If
                Else
                    FailReason = "Decimals Match fail as 'Compare' is not a Decimal"
                End If
            Else
                FailReason = "Decimals Match fail as 'Against' is not a Decimal"
            End If
        End If
        Message = FailReason
    End Function

    Private Function IntegersMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Integers Match fail (" & FailReason & ")"
        Else
            Dim AgainstAsInteger As Integer

            If Integer.TryParse(Against.ToString, AgainstAsInteger) Then
                Dim CompareAsInteger As Integer

                If Integer.TryParse(Compare.ToString, CompareAsInteger) Then
                    If AgainstAsInteger <> CompareAsInteger Then
                        FailReason = "Integers Match fail as 'Against' (= " & AgainstAsInteger & ") and 'Compare' (= " & CompareAsInteger & ") are not the same"
                    Else
                        IntegersMatch = True
                    End If
                Else
                    FailReason = "Integers Match fail as 'Compare' is not an Integer"
                End If
            Else
                FailReason = "Integers Match fail as 'Against' is not an Integer"
            End If
        End If
        Message = FailReason
    End Function

    Private Function StringsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal StringComparisonType As System.StringComparison = StringComparison.CurrentCulture) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Strings Match fail (" & FailReason & ")"
        Else
            If TypeOf Against Is String Then
                Dim AgainstAsString As String = Against.ToString

                If TypeOf Compare Is String Then
                    Dim CompareAsString As String = Compare.ToString

                    If AgainstAsString = String.Empty Then
                        If CompareAsString = String.Empty Then
                            StringsMatch = True
                        Else
                            FailReason = "Strings Match fail as 'Against' is Empty but 'Compare' is not Empty"
                        End If
                    Else
                        If CompareAsString = String.Empty Then
                            FailReason = "Strings Match fail as 'Against' is not Empty but 'Compare' is Empty"
                        Else
                            If String.Equals(AgainstAsString, CompareAsString, StringComparisonType) Then
                                StringsMatch = True
                            Else
                                FailReason = "Strings Match fail as 'Against' (= " & AgainstAsString & ") and 'Compare' (= " & CompareAsString & ") are not the same"
                            End If
                        End If
                    End If
                Else
                    FailReason = "Strings Match fail as 'Compare' is not a string"
                End If
            Else
                FailReason = "Strings Match fail as 'Against' is not a string"
            End If
        End If
        Message = FailReason
    End Function

    Private Function ObjectsBothNotNothingOrBothSomething(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String

        If Against Is Nothing Then
            If Compare IsNot Nothing Then
                FailReason = "Against is 'Nothing', Compare is not 'Nothing'"
            Else
                ObjectsBothNotNothingOrBothSomething = True
            End If
        Else
            If Compare Is Nothing Then
                FailReason = "Against is not 'Nothing', Compare is 'Nothing'"
            Else
                ObjectsBothNotNothingOrBothSomething = True
            End If
        End If
    End Function
#End Region
End Class
