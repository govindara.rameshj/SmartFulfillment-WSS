<TestClass()> Public Class StockGetStock_Tests

    Private testContextInstance As TestContext

    Private Const skuToCreate As String = "899889"
    Private Const _INSERT_SQL As String = "INSERT INTO STKMAS SELECT " & skuToCreate & ", [PLUD] ,[DESCR] ,[FILL] ,[DEPT] ,[GROU] ,[VATC] ,[BUYU] ,[SUPP] ,[PROD] ,[PACK] ,[PRIC] ,[PPRI] ,[COST] ,[DSOL] ,[DREC] ,[DORD] ,[DPRC] ,[DSET] ,[DOBS] ,[DDEL] ,[ONHA] ,[ONOR] ,[MINI] ,[MAXI] ,[SOLD] ,[ISTA] ,[IRIS] ,[IRIB] ,[IMDN] ,[ICAT] ,[IOBS] ,[IDEL] ,[ILOC] ,[IEAN] ,[IPPC] ,[ITAG] ,[INON] ,[SALV1] ,[SALV2] ,[SALV3] ,[SALV4] ,[SALV5] ,[SALV6] ,[SALV7] ,[SALU1] ,[SALU2] ,[SALU3] ,[SALU4] ,[SALU5] ,[SALU6] ,[SALU7] ,[CPDO] ,[CYDO] ,[AWSF] ,[AW13] ,[DATS] ,[UWEK] ,[FLAG] ,[CFLG] ,[US001] ,[US002] ,[US003] ,[US004] ,[US005] ,[US006] ,[US007] ,[US008] ,[US009] ,[US0010] ,[US0011] ,[US0012] ,[US0013] ,[US0014] ,[DO001] ,[DO002] ,[DO003] ,[DO004] ,[DO005] ,[DO006] ,[DO007] ,[DO008] ,[DO009] ,[DO0010] ,[DO0011] ,[DO0012] ,[DO0013] ,[DO0014] ,[SQ04] ,[SQ13] ,[SQ01] ,[SQOR] ,[TREQ] ,[TREV] ,[TACT] ,[RETQ] ,[RETV] ,[CHKD] ,[LABN] ,[LABS] ,[STON] ,[STOQ] ,[SOD1] ,[SOD2] ,[SOD3] ,[LABM] ,[LABL] ,[WGHT] ,[VOLU] ,[NOOR] ,[SUP1] ,[IODT] ,[FODT] ,[PMIN] ,[PMSD] ,[PMED] ,[PMCP] ,[SMAN] ,[DFLC] ,[IMCP] ,[FOLT] ,[IDEA] ,[QADJ] ,[TAGF] ,[ALPH] ,[CTGY] ,[GRUP] ,[SGRP] ,[STYL] ,[EQUV] ,[HFIL] ,[REVT] ,[RPRI] ,[IWAR] ,[IOFF] ,[ISOL] ,[QUAR] ,[IPRD] ,[EQPM] ,[EQPU] ,[MDNQ] ,[WTFQ] ,[SALT] ,[MODT] ,[AAPC] ,[IPSK] ,[AADJ] ,[SUP2] ,[FRAG] ,[TIMB] ,[ELEC] ,[SOFS] ,[WQTY] ,[WRAT] ,[WSEQ] ,[BALI] ,[PRFSKU] ,[MSPR1] ,[MSPR2] ,[MSPR3] ,[MSPR4] ,[MSTQ1] ,[MSTQ2] ,[MSTQ3] ,[MSTQ4] ,[MREQ1] ,[MREQ2] ,[MREQ3] ,[MREQ4] ,[MREV1] ,[MREV2] ,[MREV3] ,[MREV4] ,[MADQ1] ,[MADQ2] ,[MADQ3] ,[MADQ4] ,[MADV1] ,[MADV2] ,[MADV3] ,[MADV4] ,[MPVV1] ,[MPVV2] ,[MPVV3] ,[MPVV4] ,[MIBQ1] ,[MIBQ2] ,[MIBQ3] ,[MIBQ4] ,[MIBV1] ,[MIBV2] ,[MIBV3] ,[MIBV4] ,[MRTQ1] ,[MRTQ2] ,[MRTQ3] ,[MRTQ4] ,[MRTV1] ,[MRTV2] ,[MRTV3] ,[MRTV4] ,[MCCV1] ,[MCCV2] ,[MCCV3] ,[MCCV4] ,[MDRV1] ,[MDRV2] ,[MDRV3] ,[MDRV4] ,[MBSQ1] ,[MBSQ2] ,[MBSQ3] ,[MBSQ4] ,[MBSV1] ,[MBSV2] ,[MBSV3] ,[MBSV4] ,[MSTP1] ,[MSTP2] ,[MSTP3] ,[MSTP4] ,[FIL11] ,[FIL12] ,[FIL13] ,[FIL14] ,[ToForecast] ,[IsInitialised] ,[DemandPattern] ,[PeriodDemand] ,[PeriodTrend] ,[ErrorForecast] ,[ErrorSmoothed] ,[BufferConversion] ,[BufferStock] ,[ServiceLevelOverride] ,[ValueAnnualUsage] ,[OrderLevel] ,[SaleWeightSeason] ,[SaleWeightBank] ,[SaleWeightPromo] ,[DateLastSoqInit] ,[StockLossPerWeek] ,[FlierPeriod1] ,[FlierPeriod2] ,[FlierPeriod3] ,[FlierPeriod4] ,[FlierPeriod5] ,[FlierPeriod6] ,[FlierPeriod7] ,[FlierPeriod8] ,[FlierPeriod9] ,[FlierPeriod10] ,[FlierPeriod11] ,[FlierPeriod12] ,[SalesBias0] ,[SalesBias1] ,[SalesBias2] ,[SalesBias3] ,[SalesBias4] ,[SalesBias5] ,[SalesBias6] ,[DateLastSold] FROM STKMAS WHERE SKUN = '424995'"
    Private Const _DELETE_SQL As String = "DELETE FROM STKMAS WHERE SKUN = '" & skuToCreate & "'"
    Private Const _SELECT_SQL As String = "SELECT **FIELD_TOKEN** FROM STKMAS WHERE SKUN  = '" & skuToCreate & "'"

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"

    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection

            Using com As New Command(con)

                'delete test record if exists
                com.CommandText = _DELETE_SQL
                com.ExecuteNonQuery()

                'insert test record to STKMAS
                com.CommandText = _INSERT_SQL
                com.ExecuteNonQuery()

            End Using

        End Using

    End Sub

    ' Use ClassCleanup to run code after all tests in a class have run
    <ClassCleanup()> Public Shared Sub MyClassCleanup()

        Using con As New Connection

            Using com As New Command(con)

                com.CommandText = _DELETE_SQL
                com.ExecuteNonQuery()

            End Using

        End Using

    End Sub

    Private Function ValuesAreEqual(ByVal fieldName As String, ByVal returnedFieldName As String) As Boolean
        Dim returnedList As List(Of Object) = GetValuesToCompare(fieldName, returnedFieldName)

        If IsDBNull(returnedList.Item(0)) Or IsDBNull(returnedList.Item(1)) Then
            If IsDBNull(returnedList.Item(0)) Then returnedList.Item(0) = "NULL"
            If IsDBNull(returnedList.Item(1)) Then returnedList.Item(1) = "NULL"
        End If

        Return returnedList.Item(0).Equals(returnedList.Item(1))

    End Function

    Private Function GetValuesToCompare(ByVal physicalFieldName As String, ByVal returnedFieldName As String) As List(Of Object)

        Dim sql As String = _SELECT_SQL.Replace("**FIELD_TOKEN**", physicalFieldName)
        Dim results As New List(Of Object)

        Dim DynamicDT As DataTable
        Dim StoreProcedureDT As DataTable

        Using con As New Connection

            'dynamic
            Using com As New Command(con)
                com.CommandText = sql
                DynamicDT = com.ExecuteDataTable()
            End Using

            For Each Row As DataRow In DynamicDT.Rows
                results.Add(Row.Item(physicalFieldName))
            Next

            'stored procedure
            Using com As New Command(con)
                com.StoredProcedureName = "[StockGetStock]"
                com.AddParameter("@SkuNumber", skuToCreate, SqlDbType.Char, 6, ParameterDirection.Input)

                StoreProcedureDT = com.ExecuteDataTable()
            End Using

            For Each Row As DataRow In StoreProcedureDT.Rows
                results.Add(Row.Item(returnedFieldName))
            Next

        End Using

        Return results

    End Function
#End Region

    <TestMethod()> Public Sub StockGetStock_CompareSPtoDESCRValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DESCR", "Description"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoPRODValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("PROD", "ProductCode"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoPACKValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("PACK", "PackSize"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoPRICValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("PRIC", "Price"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoCOSTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("COST", "Cost"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoWGHTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("WGHT", "Weight"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoONHAValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("ONHA", "OnHandQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoONORValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("ONOR", "OnOrderQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoMINIValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("MINI", "MinimumQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoMAXIValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("MAXI", "MaximumQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoRETQValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("RETQ", "OpenReturnsQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoMDNQValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("MDNQ", "MarkdownQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoWTFQValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("WTFQ", "WriteOffQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoINONValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("INON", "IsNonStock"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoIOBSValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("IOBS", "IsObsolete"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoIDELValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("IDEL", "IsDeleted"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoIRISValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("IRIS", "IsItemSingle"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoNOORValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("NOOR", "IsNonOrder"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoTREQValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("TREQ", "ReceivedTodayQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoTREVValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("TREV", "ReceivedTodayValue"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDATSValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DATS", "DateFirstStocked"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoIODTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("IODT", "DateFirstOrder"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoFODTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("FODT", "DateFinalOrder"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDRECValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DREC", "DateLastReceived"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoCTGYValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("CTGY", "HieCategory"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoGRUPValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("GRUP", "HieGroup"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoSGRPValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("SGRP", "HieSubgroup"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoSTYLValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("STYL", "HieStyle"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoTACTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("TACT", "ActivityToday"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoVATCValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("VATC", "VatCode"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoSALT_Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("SALT", "SalesType"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoITAG_Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("ITAG", "IsTaggedItem"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS001Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US001", "UnitSales1"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS002Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US002", "UnitSales2"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS003Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US003", "UnitSales3"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS004Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US004", "UnitSales4"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS005Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US005", "UnitSales5"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS006Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US006", "UnitSales6"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS007Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US007", "UnitSales7"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS008Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US008", "UnitSales8"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS009Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US009", "UnitSales9"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0010Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0010", "UnitSales10"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0011Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0011", "UnitSales11"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0012Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0012", "UnitSales12"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0013Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0013", "UnitSales13"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0014Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0014", "UnitSales14"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO001Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO001", "DaysOutStock1"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO002Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO002", "DaysOutStock2"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO003Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO003", "DaysOutStock3"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO004Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO004", "DaysOutStock4"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO005Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO005", "DaysOutStock5"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO006Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO006", "DaysOutStock6"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO007Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO007", "DaysOutStock7"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO008Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO008", "DaysOutStock8"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO009Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO009", "DaysOutStock9"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0010Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0010", "DaysOutStock10"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0011Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0011", "DaysOutStock11"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0012Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0012", "DaysOutStock12"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0013Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0013", "DaysOutStock13"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0014Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0014", "DaysOutStock14"))
    End Sub

End Class