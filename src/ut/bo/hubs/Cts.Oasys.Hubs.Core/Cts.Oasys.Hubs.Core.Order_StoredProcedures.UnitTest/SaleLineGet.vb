﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class SaleLineGet
    Inherits Cts.Oasys.Hubs.Core.Database.StoredProcedure_MultipleRows_Test

#Region "StoredProcedure_Test Overridable Tests"

    ' Seems it is necessary to override these tests (and just call the base test with the override),
    ' just to get base test to run.

    '<TestMethod()> _
    'Public Overrides Sub StoredProcedureExists()

    '    MyBase.StoredProcedureExists()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsDataTable()

    '    MyBase.WhenDataExists_StoredProcedureGetsDataTable()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

    '    MyBase.WhenDataExists_StoredProcedureGetsAtLeastOneRecord()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureCanGetMultipleRecords()

    '    MyBase.WhenDataExists_StoredProcedureCanGetMultipleRecords()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsRecordsByKey()

    '    MyBase.WhenDataExists_StoredProcedureGetsRecordsByKey()
    'End Sub

#End Region

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Field Tests"

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureDATE1()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DATE1", Message, FieldType.ftDate), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureTILL()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TILL", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureTRAN()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TRAN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureNUMB()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("NUMB", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureSKUN()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SKUN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureDEPT()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DEPT", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureIBAR()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("IBAR", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureSUPV()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SUPV", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureQUAN()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("QUAN", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureSPRI()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SPRI", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedurePRIC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("PRIC", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedurePRVE()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("PRVE", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureEXTP()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("EXTP", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureEXTC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("EXTC", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRITM()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RITM", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedurePORC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("PORC", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureITAG()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ITAG", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureCATA()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CATA", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureVSYM()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureTPPD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TPPD", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureTPME()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TPME", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedurePOPD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("POPD", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedurePOME()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("POME", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureQBPD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("QBPD", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureQBME()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("QBME", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureDGPD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DGPD", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureDGME()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DGME", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureMBPD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("MBPD", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureMBME()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("MBME", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureHSPD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("HSPD", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureHSME()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("HSME", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureESPD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ESPD", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureESME()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ESME", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureLREV()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("LREV", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureESEQ()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ESEQ", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureCTGY()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CTGY", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureGRUP()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("GRUP", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureSGRP()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SGRP", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureSTYL()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("STYL", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureQSUP()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("QSUP", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureESEV()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ESEV", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureIMDN()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("IMDN", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureSALT()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SALT", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureVATN()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATN", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureVATV()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureBDCO()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("BDCO", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureBDCOInd()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("BDCOInd", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRCOD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RCOD", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureWSEQ()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("WSEQ", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRTI()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RTI", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureWRAT()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("WRAT", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureWEECOST()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("WEECOST", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

#End Region

#Region "StoredProcedure_Test MustOverrides"

    Public Overrides Function GetExistingDataAndKeyWithDynamicSQL(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim TranDate As Date = CDate("30/06/2011") ' Now
                    Dim TranNo As Integer = 4200
                    Dim TillNo As Integer
                    Dim Found As Boolean

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ExistingData = New DataTable
                                ExistingData.Load(reader)
                                reader.Close()

                                If ExistingData.Rows.Count = 1 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranNo = 1
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetExistingDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Overrides Function GetExistingSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("dl.DATE1,dl.TILL,dl.[TRAN],dl.NUMB,dl.SKUN,dl.DEPT,")
        sb.Append("dl.IBAR,dl.SUPV,dl.QUAN,dl.SPRI,dl.PRIC,dl.PRVE,")
        sb.Append("dl.EXTP,dl.EXTC,dl.RITM,dl.PORC,dl.ITAG,dl.CATA,")
        sb.Append("dl.VSYM,dl.TPPD,dl.TPME,dl.POPD,dl.POME,dl.QBPD,")
        sb.Append("dl.QBME,dl.DGPD,dl.DGME,dl.MBPD,dl.MBME,dl.HSPD, ")
        sb.Append("dl.HSME,dl.ESPD,dl.ESME,dl.LREV,dl.ESEQ,dl.CTGY,")
        sb.Append("dl.GRUP,dl.SGRP,dl.STYL,dl.QSUP,dl.ESEV,dl.IMDN,")
        sb.Append("dl.SALT,dl.VATN,dl.VATV,dl.BDCO,dl.BDCOInd,dl.RCOD,")
        sb.Append("st.WSEQ,dl.RTI ,st.WRAT,dl.WEECOST ")
        sb.Append("FROM DLLINE  dl ")
        sb.Append("INNER JOIN STKMAS st on st.SKUN=dl.SKUN ")
        sb.Append("WHERE dl.DATE1=@TranDate ")
        sb.Append("and dl.[TRAN]=@TranNumber ")
        sb.Append("and dl.TILL=@TillId ")

        GetExistingSQL = sb.ToString
    End Function

    Public Overrides Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Microsoft.VisualBasic.Collection) As Boolean
        Dim KeyField As Key

        KeyFields = New Collection
        KeyField = New Key("TranDate", "DATE1", "Transaction Date", Nothing, "DATE1", FieldType.ftDate)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TranNumber", "TRAN", "Transaction Number", Nothing, "TRAN", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TillId", "TILL", "Till Number", Nothing, "TILL", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("", "NUMB", "Sequence Number", Nothing, "NUMB", FieldType.ftInteger)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        GetKeyFieldsNoValueRequired = True
    End Function

    Public Overrides Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetNonExistingSQL(), con)
                    Dim TranDate As Date = Now
                    Dim TranNo As Integer
                    Dim TillNo As Integer
                    Dim Found As Boolean
                    Dim ReturnedData As DataTable

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        TranNo = 1
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ReturnedData = New DataTable
                                ReturnedData.Load(reader)
                                reader.Close()

                                If ReturnedData.Rows.Count = 0 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetNonExistingKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record.")
        End Try
    End Function

    Public Overrides Function GetNonExistingSQL() As String

        GetNonExistingSQL = GetExistingSQL()
    End Function

    Public Overrides ReadOnly Property StoredProcedureName() As String
        Get
            StoredProcedureName = "SaleLineGet"
        End Get
    End Property

    Public Overrides Function GetExistingMultipleDataAndKeyWithDynamicSQL(ByRef ExistingMultipleData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim TranDate As Date = CDate("10/06/2011") ' Now
                    Dim TranNo As Integer = 4130
                    Dim TillNo As Integer
                    Dim Found As Boolean

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ExistingMultipleData = New DataTable
                                ExistingMultipleData.Load(reader)
                                reader.Close()

                                If ExistingMultipleData.Rows.Count > 1 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranNo = 1
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetExistingMultipleDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a multiple records.")
        End Try
    End Function
#End Region

End Class