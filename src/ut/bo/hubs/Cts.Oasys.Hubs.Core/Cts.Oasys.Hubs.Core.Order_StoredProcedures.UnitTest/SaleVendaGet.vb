﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class SaleVendaGet
    Inherits StoredProcedureSingleRow_Test

#Region "StoredProcedure_Test Overridable Tests"
    ' Seems it is necessary to override these tests (and just call the base test with the override),
    ' just to get base test to run.

    '<TestMethod()> _
    'Public Overrides Sub StoredProcedureExists()

    '    MyBase.StoredProcedureExists()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsDataTable()

    '    MyBase.WhenDataExists_StoredProcedureGetsDataTable()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

    '    MyBase.WhenDataExists_StoredProcedureGetsAtLeastOneRecord()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureDoesNotGetMultipleRecords()

    '    MyBase.WhenDataExists_StoredProcedureDoesNotGetMultipleRecords()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsRecordByKey()

    '    MyBase.WhenDataExists_StoredProcedureGetsRecordByKey()
    'End Sub

#End Region

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Tests"

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordDATE1Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DATE1", Message, FieldType.ftDate), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordTILLMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TILL", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordTRANMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TRAN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordCASHMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CASH", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordTIMEMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TIME", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordSUPVMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SUPV", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordTCODMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TCOD", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordOPENMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("OPEN", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordMISCMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("MISC", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordDESCRMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DESCR", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordORDNMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ORDN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordACCTMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ACCT", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVOIDMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VOID", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedVSUPMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSUP", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordTMODMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TMOD", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordPROCMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("PROC", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordDOCNMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DOCN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordSUSEMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SUSE", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordSTORMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("STOR", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordMERCMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("MERC", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordNMERMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("NMER", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordTAXAMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TAXA", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordDISCMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DISC", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordDSUPMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DSUP", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordTOTLMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TOTL", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordACCNMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ACCN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordCARDMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CARD", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordAUPDMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("AUPD", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordBACKMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("BACK", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordICOMMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ICOM", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordIEMPMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("IEMP", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordRCASMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RCAS", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordRSUPMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RSUP", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordPARKMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("PARK", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordRMANMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RMAN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordTOCDMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TOCD", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordPKRCMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("PKRC", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordREMOMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("REMO", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordGTPNMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("GTPN", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordCCRDMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CCRD", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordSSTAMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SSTA", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordSSEQMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SSEQ", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordCBBUMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CBBU", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordCARD_NOMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CARD_NO", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordRTIMatches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RTI", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR1Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR1", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR2Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR2", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR3Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR3", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR4Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR4", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR5Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR5", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR6Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR6", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR7Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR7", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR8Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR8", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATR9Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATR9", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM1Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM1", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM2Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM2", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM3Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM3", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM4Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM4", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM5Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM5", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM6Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM6", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM7Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM7", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM8Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM8", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVSYM9Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VSYM9", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT1Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT1", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT2Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT2", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT3Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT3", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT4Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT4", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT5Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT5", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT6Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT6", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT7Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT7", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT8Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT8", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordXVAT9Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("XVAT9", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV1Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV1", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV2Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV2", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV3Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV3", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV4Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV4", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV5Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV5", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV6Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV6", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV7Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV7", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV8Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV8", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRecordVATV9Matches()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("VATV9", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

#End Region

#Region "StoredProcedure_Test MustOverrides"

    Public Overrides Function GetExistingDataAndKeyWithDynamicSQL(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim VendaNumber As Integer = 11450
                    Dim Found As Boolean

                    com.Parameters.Add("@VendaNumber", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While VendaNumber < 12000 And Not Found
                        com.Parameters.Item("@VendaNumber").Value = VendaNumber.ToString.PadLeft(8, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 1 Then
                            Dim OMONKey As New Key

                            With OMONKey
                                .SPParamName = "DocumentNumber"
                                .DynamicSQLName = "DOCN"
                                .Caption = "Document Number"
                                .Value = VendaNumber.ToString.PadLeft(8, "0"c)
                                .Type = FieldType.ftString
                            End With
                            Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                            Found = True
                        End If
                        VendaNumber += 1
                    Loop
                    GetExistingDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Overrides Function GetExistingSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("DATE1,TILL,[TRAN],CASH,[TIME],SUPV,")
        sb.Append("TCOD,[OPEN],MISC,DESCR,ORDN,ACCT,")
        sb.Append("VOID,VSUP,TMOD,[PROC],DOCN,SUSE,")
        sb.Append("STOR,MERC,NMER,TAXA,DISC,DSUP,")
        sb.Append("TOTL,ACCN,[CARD],AUPD,BACK,ICOM,")
        sb.Append("IEMP,RCAS,RSUP,PARK,RMAN,TOCD,")
        sb.Append("PKRC,REMO,GTPN,CCRD,SSTA,SSEQ,")
        sb.Append("CBBU,CARD_NO,RTI,")
        sb.Append("VATR1,VATR2,VATR3,VATR4,VATR5,VATR6,VATR7,VATR8,VATR9,")
        sb.Append("VSYM1,VSYM2,VSYM3,VSYM4,VSYM5,VSYM6,VSYM7,VSYM8,VSYM9,")
        sb.Append("XVAT1,XVAT2,XVAT3,XVAT4,XVAT5,XVAT6,XVAT7,XVAT8,XVAT9,")
        sb.Append("VATV1,VATV2,VATV3,VATV4,VATV5,VATV6,VATV7,VATV8,VATV9 ")

        sb.Append("from DLTOTS ")
        sb.Append("where DOCN = @VendaNumber ")
        sb.Append("and   TCOD = 'SA' ")

        GetExistingSQL = sb.ToString
    End Function

    Public Overrides Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Microsoft.VisualBasic.Collection) As Boolean
        Dim KeyField As Key

        KeyFields = New Collection
        KeyField = New Key("TranDate", "DATE1", "Transaction Date", Nothing, "DATE1", FieldType.ftDate)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TranNumber", "TRAN", "Transaction Number", Nothing, "TRAN", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TillId", "TILL", "Till Number", Nothing, "TILL", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        GetKeyFieldsNoValueRequired = True
    End Function

    Public Overrides Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim ReturnedData As DataTable
                    Dim VendaNumber As Integer = 1000
                    Dim Found As Boolean

                    com.Parameters.Add("@VendaNumber", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While VendaNumber < 1999 And Not Found
                        com.Parameters.Item("@VendaNumber").Value = VendaNumber.ToString.PadLeft(8, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ReturnedData = New DataTable
                        ReturnedData.Load(reader)
                        reader.Close()

                        If ReturnedData.Rows.Count = 0 Then
                            Dim OMONKey As New Key

                            With OMONKey
                                .SPParamName = "DocumentNumber"
                                .DynamicSQLName = "DOCN"
                                .Caption = "Document Number"
                                .Value = VendaNumber.ToString.PadLeft(8, "0"c)
                                .Type = FieldType.ftString
                            End With
                            Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                        End If
                        VendaNumber += 1
                        Found = True
                    Loop
                    GetNonExistingKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Overrides Function GetNonExistingSQL() As String

        GetNonExistingSQL = GetExistingSQL()
    End Function

    Public Overrides ReadOnly Property StoredProcedureName() As String
        Get
            StoredProcedureName = "SaleVendaGet"
        End Get
    End Property

#End Region

End Class