﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class usp_CashBalCashierUpdate

    Protected Const TC_KEY_FOUNDSTOREDPROCEDURE As String = "FoundStoredProcedure"
    Protected Const TC_KEY_INITIALISED As String = "Initialised"
    Protected Const TC_KEY_EXISTING As String = "Existing"
    Protected Const TC_KEY_NONEXISTINGKEY As String = "NonExistingKey"
    Protected Const TC_KEY_EXISTINGKEY As String = "ExistingKey"
    Protected Const TC_KEY_STOREDPROCEDUREEXISTING As String = "StoredProcedureExisting"
    Protected Const TC_KEY_STOREDPROCEDURENONEXISTING As String = "StoredProcedureNonExisting"

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                    Case Else
                        Assert.Fail("Cannot perform Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    '<TestMethod()> _
    'Public Sub TestMethod1()

    '    If InitialiseTests() Then

    '    End If
    'End Sub

#Region "Initialise Test Context methods"

    Protected Overridable Function CheckForStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim spCheck As DataTable

                    com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & StoredProcedureName & "'"
                    spCheck = com.ExecuteDataTable
                    If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                        SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, True)
                    Else
                        SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, False)
                    End If
                    CheckForStoredProcedure = True
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & StoredProcedureName & ".")
        End Try
    End Function

    Protected Overridable Function GetExisting() As Boolean

        Try
            Dim ExistingData As New DataTable
            Dim Keys As Collection = New Collection

            If GetExistingCashBalTenDataAndKey(ExistingData, Keys) Then
                ' Keep the key for use in retrieval by stored procedure
                SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                GetExisting = True
            End If
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Protected Overridable Function GetNonExistingKey() As Boolean

        Try
            Dim Keys As Collection = New Collection

            If GetNonExistingKeyWithDynamicSQL(Keys) Then
                SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                GetNonExistingKey = True
            End If
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record key.")
        End Try
    End Function

    Protected Overridable Function GetExistingUsingStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                Dim TrySPKey As Object

                com.StoredProcedureName = StoredProcedureName
                TrySPKey = SafeGetTestContextProperty(TC_KEY_EXISTINGKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey As StoredProcedureKey = CType(TrySPKey, usp_CashBalCashierUpdate.StoredProcedureKey)

                    If SPKey.AddParameters(com) Then
                        Dim SPExisting As DataTable

                        SPExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, SPExisting)
                        GetExistingUsingStoredProcedure = True
                    End If
                End If
            End Using
        End Using
    End Function

    Protected Overridable Function GetNonExistingUsingStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                Dim TrySPKey As Object

                com.StoredProcedureName = StoredProcedureName
                TrySPKey = SafeGetTestContextProperty(TC_KEY_NONEXISTINGKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey As StoredProcedureKey = CType(TrySPKey, usp_CashBalCashierUpdate.StoredProcedureKey)

                    If SPKey.AddParameters(com) Then
                        Dim SPNonExisting As DataTable

                        SPNonExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENONEXISTING, SPNonExisting)
                        GetNonExistingUsingStoredProcedure = True
                    End If
                End If
            End Using
        End Using
    End Function

    Protected Overridable Function InitialiseTests() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            If CheckForStoredProcedure() Then
                If GetExisting() Then
                    If GetNonExistingKey() Then
                        If GetExistingUsingStoredProcedure() Then
                            If GetNonExistingUsingStoredProcedure() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & StoredProcedureName & ")")
            End If
        End If
        InitialiseTests = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Public Sub SafeAddTestContextProperty(ByVal PropertyKey As String, ByVal PropertyValue As Object)

        If TestContext.Properties(PropertyKey) IsNot Nothing Then
            TestContext.Properties.Remove(PropertyKey)
        End If
        TestContext.Properties.Add(PropertyKey, PropertyValue)
    End Sub

    Public Function SafeGetTestContextProperty(ByVal PropertyKey As String, ByVal PropertyType As Type) As Object

        SafeGetTestContextProperty = Nothing
        If TestContext IsNot Nothing AndAlso TestContext.Properties(PropertyKey) IsNot Nothing Then
            If PropertyType IsNot Nothing Then
                Select Case PropertyType.Name
                    Case GetType(String).Name
                        SafeGetTestContextProperty = TestContext.Properties(PropertyKey).ToString()
                    Case GetType(Integer).Name
                        Dim Prop As Integer

                        If Integer.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(Boolean).Name
                        Dim Prop As Boolean

                        If Boolean.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(Date).Name
                        Dim Prop As Date

                        If Date.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(DataTable).Name
                        Dim TryDataTable As Object = TestContext.Properties(PropertyKey)
                        Dim Prop As DataTable

                        If TypeOf TryDataTable Is DataTable Then
                            Prop = CType(TryDataTable, DataTable)
                            SafeGetTestContextProperty = Prop
                        End If
                    Case Else
                        SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
                End Select
            Else
                SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
            End If
        End If
    End Function
#End Region

#Region "StoredProcedure_Test MustOverrides"

    Public Function GetExistingCashBalTenDataAndKey(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingCashBalCashierTenSQL(), con)
                    Dim TranDate As Date = CDate("30/06/2011") ' Now
                    Dim TranNo As Integer = 4200
                    Dim TillNo As Integer
                    Dim Found As Boolean

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ExistingData = New DataTable
                                ExistingData.Load(reader)
                                reader.Close()

                                If ExistingData.Rows.Count = 1 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranNo = 1
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetExistingCashBalTenDataAndKey = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Function GetExistingCashBalCashierTenSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("PeriodID, ")
        sb.Append("CashierID, ")
        sb.Append("CurrencyID, ")
        sb.Append("ID, ")
        sb.Append("Quantity, ")
        sb.Append("Amount, ")
        sb.Append("PickUp ")
        sb.Append("FROM CashBalCashierTen ")
        sb.Append("WHERE PeriodID = @PeriodId ")
        sb.Append("AND CashierID = @CashierId")
        sb.Append("AND CurrencyID = @CurrencyId")
        sb.Append("AND ID = @Id")

        GetExistingCashBalCashierTenSQL = sb.ToString
    End Function

    Public Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Microsoft.VisualBasic.Collection) As Boolean
        Dim KeyField As Key

        KeyFields = New Collection
        KeyField = New Key("TranDate", "DATE1", "Transaction Date", Nothing, "DATE1", FieldType.ftDate)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TranNumber", "TRAN", "Transaction Number", Nothing, "TRAN", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TillId", "TILL", "Till Number", Nothing, "TILL", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("", "NUMB", "Sequence Number", Nothing, "NUMB", FieldType.ftInteger)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        GetKeyFieldsNoValueRequired = True
    End Function

    Public Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetNonExistingSQL(), con)
                    Dim TranDate As Date = Now
                    Dim TranNo As Integer
                    Dim TillNo As Integer
                    Dim Found As Boolean
                    Dim ReturnedData As DataTable

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        TranNo = 1
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ReturnedData = New DataTable
                                ReturnedData.Load(reader)
                                reader.Close()

                                If ReturnedData.Rows.Count = 0 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetNonExistingKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record.")
        End Try
    End Function

    Public Function GetNonExistingSQL() As String

        GetNonExistingSQL = GetExistingCashBalCashierTenSQL()
    End Function

    Public ReadOnly Property StoredProcedureName() As String
        Get
            StoredProcedureName = "usp_CashBalCashTenUpdate"
        End Get
    End Property

    Public Function GetExistingMultipleDataAndKeyWithDynamicSQL(ByRef ExistingMultipleData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingCashBalCashierTenSQL(), con)
                    Dim TranDate As Date = CDate("10/06/2011") ' Now
                    Dim TranNo As Integer = 4130
                    Dim TillNo As Integer
                    Dim Found As Boolean

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ExistingMultipleData = New DataTable
                                ExistingMultipleData.Load(reader)
                                reader.Close()

                                If ExistingMultipleData.Rows.Count > 1 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranNo = 1
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetExistingMultipleDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a multiple records.")
        End Try
    End Function
#End Region


    Protected Enum FieldType
        ftString = 0
        ftInteger
        ftDecimal
        ftBoolean
        ftDate
    End Enum

    Protected Structure Key

        Private storedProcedureName As String
        Private storedProcedureParamName As String
        Private dynSQLName As String
        Private friendlyName As String
        Private keyValue As Object
        Private keyfieldType As FieldType

        Public Property SPName() As String
            Get
                SPName = storedProcedureName
            End Get
            Set(ByVal value As String)
                storedProcedureName = value
            End Set
        End Property

        Public Property SPParamName() As String
            Get
                SPParamName = storedProcedureParamName
            End Get
            Set(ByVal value As String)
                storedProcedureParamName = value
            End Set
        End Property

        Public Property DynamicSQLName() As String
            Get
                DynamicSQLName = dynSQLName
            End Get
            Set(ByVal value As String)
                dynSQLName = value
            End Set
        End Property

        Public Property Caption() As String
            Get
                Caption = friendlyName
            End Get
            Set(ByVal value As String)
                friendlyName = value
            End Set
        End Property

        Public Property Value() As Object
            Get
                Value = keyValue
            End Get
            Set(ByVal value As Object)
                keyValue = value
            End Set
        End Property

        Public Property Type() As FieldType
            Get
                Type = keyfieldType
            End Get
            Set(ByVal value As FieldType)
                keyfieldType = value
            End Set
        End Property

        Public Sub New(ByVal KeySPParamName As String, ByVal KeyDynamicSQLName As String, ByVal KeyCaption As String, ByRef KeyValue As Object, ByVal KeyType As FieldType)

            SPParamName = KeySPParamName
            DynamicSQLName = KeyDynamicSQLName
            Caption = KeyCaption
            Value = KeyValue
            Type = KeyType
        End Sub

        Public Sub New(ByVal KeySPParamName As String, ByVal KeyDynamicSQLName As String, ByVal KeyCaption As String, ByRef KeyValue As Object, ByVal SPName As String, ByVal KeyType As FieldType)

            SPParamName = KeySPParamName
            DynamicSQLName = KeyDynamicSQLName
            Caption = KeyCaption
            Value = KeyValue
            SPName = SPName
            Type = KeyType
        End Sub
    End Structure

    Protected Class StoredProcedureKey
        Private Keys As Collection

        Public Sub New(ByVal SPKeys As Collection)

            Keys = New Collection
            If SPKeys IsNot Nothing Then
                For Each SPKey As Key In SPKeys
                    Keys.Add(SPKey, SPKey.DynamicSQLName)
                Next
            End If
        End Sub

        Public Overrides Function ToString() As String

            ToString = ""
            If Keys IsNot Nothing Then
                For Each SPKey As Key In Keys
                    If ToString.Length > 0 Then
                        ToString &= ";"
                    End If
                    ToString &= SPKey.Caption & " = '" & SPKey.Value.ToString & "'"
                Next
            Else
                ToString = "Empty StoredProcedureKey"
            End If
        End Function

        Public Function GetKeyString(ByRef From As DataRow) As String

            GetKeyString = ""
            If Keys IsNot Nothing Then
                For Each SPKey As Key In Keys
                    If ToString.Length > 0 Then
                        GetKeyString &= ";"
                    End If
                    GetKeyString &= SPKey.Caption & " = '" & From.Item(SPKey.DynamicSQLName).ToString & "'"
                Next
            Else
                GetKeyString = "Empty StoredProcedureKey"
            End If
        End Function

        Public Function AddParameters(ByRef ComToAddTo As Command) As Boolean

            If Keys IsNot Nothing And ComToAddTo IsNot Nothing Then
                For Each SPKey As Key In Keys
                    ComToAddTo.AddParameter("@" & SPKey.SPParamName, SPKey.Value)
                Next
                AddParameters = True
            End If
        End Function

        Public Function AddParameters(ByRef ComToAddTo As SqlCommand) As Boolean

            If Keys IsNot Nothing And ComToAddTo IsNot Nothing Then
                For Each SPKey As Key In Keys
                    ComToAddTo.Parameters.Add(New SqlParameter("@" & SPKey.SPParamName, SPKey.Value))
                Next
                AddParameters = True
            End If
        End Function
    End Class
End Class
