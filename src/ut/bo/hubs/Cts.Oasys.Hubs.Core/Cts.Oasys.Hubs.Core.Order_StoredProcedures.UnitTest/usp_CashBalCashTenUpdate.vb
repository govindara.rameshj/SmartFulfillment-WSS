﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class usp_CashBalCashTenUpdate

    Protected Const TC_KEY_FOUNDSTOREDPROCEDURE As String = "FoundStoredProcedure"
    Protected Const TC_KEY_INITIALISED As String = "Initialised"
    Protected Const TC_KEY_TRANSACTIONDATE As String = "TransactionDate"
    Protected Const TC_KEY_CASHIERID As String = "CashierId"
    Protected Const TC_KEY_TILLID As String = "TillId"
    Protected Const TC_KEY_TRANSACTIONDATEPERIODID As String = "TransactionDateSystemPeriodId"
    Protected Const TC_KEY_CURRENCYID As String = "Currency Id"
    Protected Const TC_KEY_STOREDPROCEDURERESULT As String = "StoredProcedureResult"
    Protected Const TC_KEY_TRANSACTIONNUMBER As String = "TransactionNumber"
    Protected Const TC_KEY_TRANSACTIONAMOUNT As String = "TransactionAmount"
    Protected Const TC_KEY_STOREDPROCEDUREKEY As String = "StoredProcedureKey"
    Protected Const TC_KEY_CASHBALCASHIERTENENTRY As String = "CashBalCashierEntry"
    Protected Const TC_KEY_CASHBALCASHIERTENCURREENTENTRY As String = "CashBalCashierCurrentEntry"

    Protected Const TC_KEY_EXISTING As String = "Existing"
    Protected Const TC_KEY_NONEXISTINGKEY As String = "NonExistingKey"
    Protected Const TC_KEY_STOREDPROCEDURENONEXISTING As String = "StoredProcedureNonExisting"

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        Assert.IsTrue(SetupDLTOTSCashBalCashierTen(com, testContext), "Cannot perform Stored Procedures tests as could not set up DLTOTS and CashBalCashierTen tables.")
                    Case Else
                        Assert.Fail("Cannot perform Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> _
    Public Sub StoredProcedureExists()

        If BasicInitialise() Then
            Assert.IsTrue(CBool(SafeGetTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, GetType(Boolean))), "Stored procedure '" & StoredProcedureName & "' does not exist.")
        End If
    End Sub

    '<TestMethod()> _
    'Public Sub WhenNoPreviousEntry_StoredProcedureInsertsNewCorrectCashBalCashierTen()

    '    If InitialiseForFirstTests() Then
    '        ' If is a new entry then the Quantity field should be 1 and the Amount equal to the amount for the transaction
    '        '  NB Check both here as cannot redo the intialisation process in current format
    '        Dim TryCashBalCashierTen As DataTable = SafeGetTestContextProperty(TC_KEY_CASHBALCASHIERTENENTRY, GetType(DataTable))

    '        If TryCashBalCashierTen IsNot Nothing AndAlso TypeOf TryCashBalCashierTen Is DataTable Then
    '            Dim CashBalCashierTen As DataTable = TryCashBalCashierTen

    '            Select Case CashBalCashierTen.Rows.Count
    '                Case 0
    '                    Assert.Fail("The stored procedure did not create a new CashBalCashierTen entry for the new transaction data.")
    '                Case 1
    '                    ' sales are -ve in system, so stored procedure should subtract the amount (from zero if new CashBalCashierTen entry)
    '                    Dim Amount As Single = SafeGetTestContextProperty(TC_KEY_TRANSACTIONAMOUNT, GetType(Single)) * -1
    '                    Dim QuantityField As Integer
    '                    Dim AmountField As Single

    '                    ' Check the row has the correct data
    '                    With CashBalCashierTen.Rows(0)
    '                        If Integer.TryParse(.Item("Quantity").ToString, QuantityField) Then
    '                            If QuantityField = 1 Then
    '                                If Single.TryParse(.Item("Amount").ToString, AmountField) Then
    '                                    If Not Single.Equals(Amount, AmountField) Then
    '                                        Assert.Fail("The CashBalCashierTen Amount (" & AmountField.ToString & ") is not as expected (" & Amount.ToString & ").")
    '                                    End If
    '                                Else
    '                                    Assert.Inconclusive("Problem retrieving Amount value from the new CashBalCashierTen entry.")
    '                                End If
    '                            Else
    '                                Assert.Fail("The CashBalCashierTen Quantity field is not 1 (is " & QuantityField.ToString & ") implying that the stored procedure has updated an existing entry rather than inserting a new one.")
    '                            End If
    '                        Else
    '                            Assert.Inconclusive("Problem retrieving Quantity value from the new CashBalCashierTen entry.")
    '                        End If
    '                    End With
    '                Case Else
    '                    Assert.Inconclusive("Problem retrieving CashBalCashierTen entry, multiple entries found for unique key?!")
    '            End Select
    '        End If
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenPreviousEntryExists_StoredProcedureCorrectlyUpdatesExistingCashBalCashierTen()

    '    If InitialiseForSecondTests() Then
    '        Dim TryPreCashBalCashierTen As DataTable = SafeGetTestContextProperty(TC_KEY_CASHBALCASHIERTENCURREENTENTRY, GetType(DataTable))
    '        Dim TryCashBalCashierTen As DataTable = SafeGetTestContextProperty(TC_KEY_CASHBALCASHIERTENENTRY, GetType(DataTable))

    '        ' This should be the CashBalCashierTen entry, prior ro being updated
    '        If TryPreCashBalCashierTen IsNot Nothing AndAlso TypeOf TryCashBalCashierTen Is DataTable Then
    '            Dim PreCashBalCashierTen As DataTable = TryPreCashBalCashierTen

    '            Select Case PreCashBalCashierTen.Rows.Count
    '                Case 0
    '                    Assert.Inconclusive("There is no CashBalCashierTen entry for this test to have updated.")
    '                Case 1
    '                    Dim PreQuantity As Integer
    '                    Dim PreAmount As Single

    '                    ' Get pre-updated values
    '                    With PreCashBalCashierTen.Rows(0)
    '                        PreQuantity = .Item("Quantity")
    '                        PreAmount = .Item("Amount")
    '                    End With
    '                    ' This should be the CashBalCashierTen entry AFTER the update
    '                    If TryCashBalCashierTen IsNot Nothing AndAlso TypeOf TryCashBalCashierTen Is DataTable Then
    '                        Dim CashBalCashierTen As DataTable = TryCashBalCashierTen

    '                        Select Case CashBalCashierTen.Rows.Count
    '                            Case 0
    '                                Assert.Inconclusive("There is no CashBalCashierTen entry after the stored procedure was called.")
    '                            Case 1
    '                                ' sales are -ve in system, so stored procedure should subtract the amount (from zero if new CashBalCashierTen entry)
    '                                Dim Amount As Single = (SafeGetTestContextProperty(TC_KEY_TRANSACTIONAMOUNT, GetType(Single)) * -1)
    '                                Dim QuantityField As Integer
    '                                Dim AmountField As Single

    '                                ' Check the row has the correct data
    '                                With CashBalCashierTen.Rows(0)
    '                                    If Integer.TryParse(.Item("Quantity").ToString, QuantityField) Then
    '                                        If QuantityField = PreQuantity + 1 Then
    '                                            If Single.TryParse(.Item("Amount").ToString, AmountField) Then
    '                                                ' Make sure the pre-updated amount has been increased by the update amount
    '                                                If Not Single.Equals(PreAmount + Amount, AmountField) Then
    '                                                    Assert.Fail("The CashBalCashierTen Amount (" & AmountField.ToString & ") is not as expected (" & (PreAmount + Amount).ToString & ").  The Amount in the pre-updated CashBalCashierTen record should have been changed during this update by the stored procedure.")
    '                                                End If
    '                                            Else
    '                                                Assert.Inconclusive("Problem retrieving Amount value from the new CashBalCashierTen entry.")
    '                                            End If
    '                                        Else
    '                                            Assert.Fail("The CashBalCashierTen Quantity field is not " & (PreQuantity + 1).ToString & " (is " & QuantityField.ToString & ").  The Quantity should have gone up by 1 during this update by the stored procedure.")
    '                                        End If
    '                                    Else
    '                                        Assert.Inconclusive("Problem retrieving Quantity value from the new CashBalCashierTen entry.")
    '                                    End If
    '                                End With
    '                            Case Else
    '                                Assert.Inconclusive("Problem retrieving the updated CashBalCashierTen entry, multiple entries found for unique key?!")
    '                        End Select
    '                    End If
    '                Case Else
    '                    Assert.Inconclusive("Problem retrieving the pre-update CashBalCashierTen entry, multiple entries found for unique key?!")
    '            End Select
    '        End If
    '    End If
    'End Sub

#Region "Initialise Database"

    Protected Shared Function SetupDLTOTSCashBalCashierTen(ByRef com As Command, ByRef testContext As TestContext) As Boolean

        If PrimeTestContext(testContext) Then
            Dim TranDate As Date

            ' Should be tomorrows date as less likely to have any existing dltots entries for that day
            If testContext.Properties.Contains(TC_KEY_TRANSACTIONDATE) AndAlso Date.TryParse(testContext.Properties(TC_KEY_TRANSACTIONDATE).ToString(), TranDate) Then
                If testContext.Properties.Contains(TC_KEY_CASHIERID) Then
                    Dim FormattedTranDate As String
                    Dim CashierID As String = testContext.Properties(TC_KEY_CASHIERID).ToString

                    If testContext.Properties.Contains(TC_KEY_TILLID) Then
                        Dim TillId As String = testContext.Properties(TC_KEY_TILLID).ToString
                        Dim SystemPeriodId As Integer
                        Dim CurrencyId As String

                        With TranDate
                            FormattedTranDate = .Year.ToString & "-" & .Month.ToString & "-" & .Day.ToString
                        End With
                        With com
                            ' Start by removing any existing DLTOTS and DLPAID - there shouldn't be any for tomorrow, but just to be sure,
                            ' so we know exzactly what should altering the balancing tables
                            .CommandText = "DELETE FROM DLTOTS WHERE DATE1 = '" & FormattedTranDate & "' AND CASH = '" & CashierID & "' AND TILL = '" & TillId & "'"
                            .ExecuteNonQuery()
                            .CommandText = "DELETE FROM DLPAID WHERE DATE1 = '" & FormattedTranDate & "' AND TILL = '" & TillId & "'"
                            .ExecuteNonQuery()
                            ' Now delete any existing CashBalCashierTen entries for same as need to force 
                            ' the stored procedure to insert for the 1st call to stored procedure then update 
                            ' for the second, so test both pathways.
                            '  For this need the system period id for the given date (tomorrow) and the current Currency Id
                            .CommandText = "SELECT ID FROM SystemPeriods WHERE StartDate = '" & FormattedTranDate & "'"

                            Dim SystemPeriod As DataTable = .ExecuteDataTable

                            If SystemPeriod.Rows.Count = 1 Then
                                SystemPeriodId = Convert.ToInt32(SystemPeriod.Rows(0).Item(0))
                                .CommandText = "SELECT ID FROM SystemCurrency"

                                Dim SystemCurrency As DataTable = .ExecuteDataTable

                                If SystemCurrency.Rows.Count = 1 Then
                                    CurrencyId = CStr(SystemCurrency.Rows(0).Item(0))
                                    .CommandText = "DELETE FROM CashBalCashierTen WHERE PeriodID = " & SystemPeriodId.ToString & " AND CashierID = '" & CashierID & "' AND CurrencyID = '" & CurrencyId & "'"
                                    .ExecuteNonQuery()
                                    SetupDLTOTSCashBalCashierTen = True
                                End If
                            End If
                        End With
                    End If
                End If
            End If
        End If
    End Function

    Private Shared Function PrimeTestContext(ByRef testContext As TestContext) As Boolean

        If testContext IsNot Nothing Then
            With testContext
                If .Properties(TC_KEY_TRANSACTIONDATE) IsNot Nothing Then
                    .Properties.Remove(TC_KEY_TRANSACTIONDATE)
                End If
                ' Set as tomorrow as less likely to have existing data for then
                .Properties.Add(TC_KEY_TRANSACTIONDATE, DateAdd(DateInterval.Day, 1, Now))
            End With
            With testContext
                If .Properties(TC_KEY_CASHIERID) IsNot Nothing Then
                    .Properties.Remove(TC_KEY_CASHIERID)
                End If
                ' Store 120 always uses cashier id of 800
                .Properties.Add(TC_KEY_CASHIERID, "800")
            End With
            With testContext
                If .Properties(TC_KEY_TILLID) IsNot Nothing Then
                    .Properties.Remove(TC_KEY_TILLID)
                End If
                ' Store 120 always uses till id of 1
                .Properties.Add(TC_KEY_TILLID, "01")
            End With
            PrimeTestContext = True
        End If
    End Function
#End Region

#Region "Initialise Test Context methods"

    Protected Function AddFirstNewDLTOTSEntry() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                With com
                    Dim sb As New StringBuilder
                    Dim TranDate As Object = SafeGetTestContextProperty(TC_KEY_TRANSACTIONDATE, GetType(Date))
                    Dim FormattedTranDate As String
                    Dim TillId As Object = SafeGetTestContextProperty(TC_KEY_TILLID, GetType(String))
                    Dim CashierId As String = CStr(SafeGetTestContextProperty(TC_KEY_CASHIERID, GetType(String)))
                    Dim TranNo As Object = "0001"
                    Dim Amount As Single = -123.56  ' A sale is recorded as a -ve amuont in dlpaid

                    With CDate(TranDate)
                        FormattedTranDate = .Year.ToString & "-" & .Month.ToString & "-" & .Day.ToString
                    End With
                    ' Keep a log of the transaction number and amount used, for checking results later
                    SafeAddTestContextProperty(TC_KEY_TRANSACTIONNUMBER, TranNo)
                    SafeAddTestContextProperty(TC_KEY_TRANSACTIONAMOUNT, Amount)
                    ' include PARK, PKRC, REMO, RTI as not nullable
                    sb.Append("INSERT INTO DLTOTS (")
                    sb.Append("DATE1, TILL, [TRAN], CASH, ")
                    sb.Append("PARK, PKRC, REMO, RTI) ")
                    sb.Append("VALUES (")
                    sb.Append("'" & FormattedTranDate & "', ")
                    sb.Append("'" & CStr(TillId) & "', ")
                    sb.Append("'" & CStr(TranNo) & "', ")
                    sb.Append("'" & CashierId & "', ")
                    sb.Append("0, 0, 1, 'S')")
                    .CommandText = sb.ToString
                    .ExecuteNonQuery()
                    ' Now the corresponding DLPAID entry, required for the actual amount used by the stored procedure to update the cash bal tables
                    sb = New StringBuilder
                    ' include CKEY, DBRF, ECOM as not nullable
                    sb.Append("INSERT INTO DLPAID (")
                    sb.Append("DATE1, TILL, [TRAN], NUMB, TYPE, AMNT, ")
                    sb.Append("CKEY, DBRF, ECOM) ")
                    sb.Append("VALUES (")
                    sb.Append("'" & FormattedTranDate & "', ")
                    sb.Append("'" & CStr(TillId) & "', ")
                    sb.Append("'" & CStr(TranNo) & "', ")
                    ' Use Numb 1 - just the one payment record for this tots.  
                    ' Also store 120 uses Type = 1 (cash) for all transactions
                    sb.Append("1, 1, " & Amount.ToString() & ", ")
                    sb.Append("0, 0, 1)")
                    .CommandText = sb.ToString
                    .ExecuteNonQuery()
                    ' Generate a (parameters) key to use when calling the stored procedure
                    Dim Keys As New Collection
                    Dim FieldKey As New Key("TransactionDate", "DATE1", "Transaction Date", TranDate, FieldType.ftDate)

                    Keys.Add(FieldKey)
                    FieldKey = New Key("TillNumber", "TILL", "Till Number", TillId, FieldType.ftString)
                    Keys.Add(FieldKey)
                    FieldKey = New Key("TransactionNumber", "TRAN", "Transaction Number", TranNo, FieldType.ftString)
                    Keys.Add(FieldKey)
                    ' Keep it in the context for use later
                    SafeAddTestContextProperty(TC_KEY_STOREDPROCEDUREKEY, New StoredProcedureKey(Keys))

                    AddFirstNewDLTOTSEntry = True
                End With
            End Using
        End Using
    End Function

    Protected Function AddSecondNewDLTOTSEntry() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                With com
                    Dim sb As New StringBuilder
                    Dim TranDate As Object = SafeGetTestContextProperty(TC_KEY_TRANSACTIONDATE, GetType(Date))
                    Dim FormattedTranDate As String
                    Dim TillId As Object = SafeGetTestContextProperty(TC_KEY_TILLID, GetType(String))
                    Dim CashierId As String = CStr(SafeGetTestContextProperty(TC_KEY_CASHIERID, GetType(String)))
                    Dim TranNo As Object = "0002"
                    Dim Amount As Single = -78.91   ' A sale is recorded as -ve amount in DLPAID

                    With CDate(TranDate)
                        FormattedTranDate = .Year.ToString & "-" & .Month.ToString & "-" & .Day.ToString
                    End With
                    ' Keep a log of the transaction number and amount used, for checking results later
                    SafeAddTestContextProperty(TC_KEY_TRANSACTIONNUMBER, TranNo)
                    SafeAddTestContextProperty(TC_KEY_TRANSACTIONAMOUNT, Amount)
                    ' include PARK, PKRC, REMO, RTI as not nullable
                    sb.Append("INSERT INTO DLTOTS (")
                    sb.Append("DATE1, TILL, [TRAN], CASH, ")
                    sb.Append("PARK, PKRC, REMO, RTI) ")
                    sb.Append("VALUES ('")
                    sb.Append(FormattedTranDate & "', ")
                    sb.Append("'" & CStr(TillId) & "', ")
                    sb.Append("'" & CStr(TranNo) & "', ")
                    sb.Append("'" & CashierId & "', ")
                    sb.Append("0, 0, 1, 'S')")
                    .CommandText = sb.ToString
                    .ExecuteNonQuery()
                    ' Now the corresponding DLPAID entry, required for the actual amount used by the stored procedure to update the cash bal tables
                    sb = New StringBuilder
                    ' include CKEY, DBRF, ECOM as not nullable
                    sb.Append("INSERT INTO DLPAID (")
                    sb.Append("DATE1, TILL, [TRAN], NUMB, TYPE, AMNT, ")
                    sb.Append("CKEY, DBRF, ECOM) ")
                    sb.Append("VALUES ('")
                    sb.Append(FormattedTranDate & "', ")
                    sb.Append("'" & CStr(TillId) & "', ")
                    sb.Append("'" & CStr(TranNo) & "', ")
                    ' Use Numb 1 - just the one payment record for this tots.  
                    ' Also store 120 uses Type = 1 (cash) for all transactions
                    sb.Append("1, 1, " & Amount.ToString() & ", ")
                    sb.Append("0, 0, 1)")
                    .CommandText = sb.ToString
                    .ExecuteNonQuery()

                    ' Generate a (parameters) key to use when calling the stored procedure
                    Dim Keys As New Collection
                    Dim FieldKey As New Key("TransactionDate", "DATE1", "Transaction Date", TranDate, FieldType.ftDate)

                    Keys.Add(FieldKey)
                    FieldKey = New Key("TillNumber", "TILL", "Till Number", TillId, FieldType.ftString)
                    Keys.Add(FieldKey)
                    FieldKey = New Key("TransactionNumber", "TRAN", "Transaction Number", TranNo, FieldType.ftString)
                    Keys.Add(FieldKey)
                    ' Keep it in the context for use later
                    SafeAddTestContextProperty(TC_KEY_STOREDPROCEDUREKEY, New StoredProcedureKey(Keys))
                    AddSecondNewDLTOTSEntry = True
                End With
            End Using
        End Using
    End Function

    Protected Function BasicInitialise() As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            If GetStartUpProperties() Then
                If CheckForStoredProcedure() Then
                    SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & StoredProcedureName & ").")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying set up the TestContext with the start up values.")
            End If
        End If
        BasicInitialise = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    Protected Overridable Function CallStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim TrySPKey As Object

                    com.StoredProcedureName = StoredProcedureName
                    TrySPKey = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREKEY, GetType(StoredProcedureKey))
                    If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                        Dim SPKey As StoredProcedureKey = CType(TrySPKey, usp_CashBalCashTenUpdate.StoredProcedureKey)

                        If SPKey.AddParameters(com) Then
                            Dim LinesUpdated As Integer

                            ' 
                            LinesUpdated = CInt(com.ExecuteValue)
                            SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURERESULT, LinesUpdated)
                            CallStoredProcedure = True
                        End If
                    End If
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Problem calling the stored procedure.")
        End Try
    End Function

    Protected Function CheckForStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim spCheck As DataTable

                    com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & StoredProcedureName & "'"
                    spCheck = com.ExecuteDataTable
                    If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                        SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, True)
                    Else
                        SafeAddTestContextProperty(TC_KEY_FOUNDSTOREDPROCEDURE, False)
                    End If
                    CheckForStoredProcedure = True
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & StoredProcedureName & ".")
        End Try
    End Function

    Protected Function GetCashBalCashierTenEntry() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim CurrencyId As String = CStr(SafeGetTestContextProperty(TC_KEY_CURRENCYID, GetType(String)))
                    Dim Type As Integer = 1   ' Store 120 always uses tender type 1, cash, for all transactions
                    Dim SystemPeriodId As Integer = CInt(SafeGetTestContextProperty(TC_KEY_TRANSACTIONDATEPERIODID, GetType(Integer)))
                    Dim CashierId As String = CStr(SafeGetTestContextProperty(TC_KEY_CASHIERID, GetType(String)))
                    Dim sb As New StringBuilder
                    Dim CashBalCashierTen As New DataTable

                    sb.Append("Select PeriodID, CashierID, CurrencyID, ID, Quantity, Amount, Pickup ")
                    sb.Append("From CashBalCashierTen where ")
                    sb.Append("PeriodID = " & SystemPeriodId.ToString & " And ")
                    sb.Append("CashierID = '" & CashierId & "' And ")
                    sb.Append("CurrencyID = '" & CurrencyId & "' And ")
                    sb.Append("ID = " & Type.ToString)
                    com.CommandText = sb.ToString
                    CashBalCashierTen = com.ExecuteDataTable
                    ' Store result for use in tests
                    SafeAddTestContextProperty(TC_KEY_CASHBALCASHIERTENENTRY, CashBalCashierTen)
                    GetCashBalCashierTenEntry = True
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a CashBalCashierTen record.")
        End Try
    End Function

    Protected Function GetNonExistingKey() As Boolean

        Try
            Dim Keys As Collection = New Collection

            If GetNonExistingKeyWithDynamicSQL(Keys) Then
                SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                GetNonExistingKey = True
            End If
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record key.")
        End Try
    End Function

    Protected Function GetStartUpProperties() As Boolean

        Try
            Dim TranDate As Date
            Dim FormattedTranDate As String

            ' Set as tomorrow as less likely to have existing data for then
            TranDate = DateAdd(DateInterval.Day, 1, Now)
            With TranDate
                FormattedTranDate = .Year.ToString & "-" & .Month.ToString & "-" & .Day.ToString
            End With
            SafeAddTestContextProperty(TC_KEY_TRANSACTIONDATE, TranDate)
            ' Store 120 always uses cashier id of 800
            SafeAddTestContextProperty(TC_KEY_CASHIERID, "800")
            ' Store 120 always uses till id of 1
            SafeAddTestContextProperty(TC_KEY_TILLID, "01")
            ' Get the System Period Id for tomorrow and the Currency Id
            Using con As New Connection()
                Using com As New Command(con)
                    With com
                        .CommandText = "SELECT ID FROM SystemPeriods WHERE StartDate = '" & FormattedTranDate & "'"

                        Dim SystemPeriod As DataTable = .ExecuteDataTable

                        If SystemPeriod.Rows.Count = 1 Then
                            Dim SystemPeriodId As Integer = Convert.ToInt32(SystemPeriod.Rows(0).Item(0))

                            SafeAddTestContextProperty(TC_KEY_TRANSACTIONDATEPERIODID, SystemPeriodId)

                            .CommandText = "SELECT ID FROM SystemCurrency"

                            Dim SystemCurrency As DataTable = .ExecuteDataTable

                            If SystemCurrency.Rows.Count = 1 Then
                                Dim CurrencyId As String

                                CurrencyId = CStr(SystemCurrency.Rows(0).Item(0))
                                SafeAddTestContextProperty(TC_KEY_CURRENCYID, CurrencyId)
                                GetStartUpProperties = True
                            End If
                        End If
                    End With
                End Using
            End Using
        Catch
        End Try
    End Function

    Protected Function InitialiseForFirstTests() As Boolean
        Dim AlreadyInitialised As Boolean

        If TestContext.Properties(TC_KEY_INITIALISED) IsNot Nothing Then
            If Not Boolean.TryParse(CStr(TestContext.Properties(TC_KEY_INITIALISED)), AlreadyInitialised) Then
                AlreadyInitialised = False
            End If
        End If
        If Not AlreadyInitialised Then
            If BasicInitialise() Then
                ' might have already run the 1st tests by virtue of the 2nd tests being run first (as not ordered tests here), so
                ' do not run a second time
                If Not Run1stTests() Then
                    ' Add the 1st entry in DLTOTS to be picked up by the stored procedure call (resulting in an insert into CashBalCashierTen table)
                    If AddFirstNewDLTOTSEntry() Then
                        ' Do insert before update, then will be something to update because of the insert
                        If CallStoredProcedure() Then
                            ' Get the new entry made in the CashBalCashierTen by the call to the stored procedure
                            If GetCashBalCashierTenEntry() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve the new CashBalCashierTen record created by calling the stored procedure.")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying call the stored procedure to update the CashBalCashierTen table.")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to add a new DLTOTS record to be picked up by the stored procedure when called.")
                    End If
                End If
            End If
        End If
        InitialiseForFirstTests = CBool(SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean)))
    End Function

    'Protected Function InitialiseForSecondTests() As Boolean

    '    Dim AlreadyInitialised As Boolean

    '    If TestContext.Properties(TC_KEY_INITIALISED) IsNot Nothing Then
    '        If Boolean.TryParse(TestContext.Properties(TC_KEY_INITIALISED), AlreadyInitialised) Then
    '        End If
    '    End If
    '    If Not AlreadyInitialised Then
    '        If BasicInitialise() Then
    '            ' Need the data from 1st test to be run before can test the 2nd test, so might as well run the whole 1st test
    '            If Not Run1stTests() Then
    '                ' Switch off previous intialisation (2nd tests) setting to force throough new (1st tests) intialisation
    '                SafeAddTestContextProperty(TC_KEY_INITIALISED, False)
    '                WhenNoPreviousEntry_StoredProcedureInsertsNewCorrectCashBalCashierTen()
    '            End If
    '            ' Add the 2nd entry in DLTOTS to be picked up by the stored procedure call (resulting in an insert into CashBalCashierTen table)
    '            If AddSecondNewDLTOTSEntry() Then
    '                ' Get the current entry made in the CashBalCashierTen by the call to the 1st test call to stored procedure
    '                If GetCashBalCashierTenEntry() Then
    '                    SafeAddTestContextProperty(TC_KEY_CASHBALCASHIERTENCURREENTENTRY, SafeGetTestContextProperty(TC_KEY_CASHBALCASHIERTENENTRY, GetType(DataTable)))
    '                    If CallStoredProcedure() Then
    '                        ' Get the new entry made in the CashBalCashierTen by the call to the stored procedure
    '                        If GetCashBalCashierTenEntry() Then
    '                            SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
    '                        Else
    '                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve the updated CashBalCashierTen record created by calling the stored procedure.")
    '                        End If
    '                    Else
    '                        Assert.Inconclusive("Test initialisation failed whilst trying call the stored procedure to update the CashBalCashierTen table.")
    '                    End If
    '                Else
    '                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve the current CashBalCashierTen entry.")
    '                End If
    '            Else
    '                Assert.Inconclusive("Test initialisation failed whilst trying to add the second new DLTOTS record to be picked up by the stored procedure when called.")
    '            End If
    '        End If
    '    End If
    '    InitialiseForSecondTests = SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean))
    'End Function

    Protected Function Run1stTests() As Boolean

        ' Just check for the 1st DLTOTS entry
        Using con As New Connection
            Using com As New Command(con)
                With com
                    Dim sb As New StringBuilder
                    Dim TranDate As Date = CDate(SafeGetTestContextProperty(TC_KEY_TRANSACTIONDATE, GetType(Date)))
                    Dim FormattedTranDate As String
                    Dim TillId As String = CStr(SafeGetTestContextProperty(TC_KEY_TILLID, GetType(String)))
                    Dim TranNo As String = "0001"
                    Dim FoundData As DataTable

                    With TranDate
                        FormattedTranDate = .Year.ToString & "-" & .Month.ToString & "-" & .Day.ToString
                    End With
                    sb.Append("SELECT COUNT(*) FROM DLTOTS WHERE ")
                    sb.Append("DATE1 = '" & FormattedTranDate & "' AND ")
                    sb.Append("TILL = '" & TillId & "' AND ")
                    sb.Append("[TRAN] = '" & TranNo & "'")
                    .CommandText = sb.ToString
                    FoundData = .ExecuteDataTable()
                    If FoundData IsNot Nothing AndAlso CBool(FoundData.Rows.Count) AndAlso CInt(FoundData.Rows(0).Item(0)) > 0 Then
                        Run1stTests = True
                    End If
                End With
            End Using
        End Using
    End Function

    Public Sub SafeAddTestContextProperty(ByVal PropertyKey As String, ByVal PropertyValue As Object)

        If TestContext.Properties(PropertyKey) IsNot Nothing Then
            TestContext.Properties.Remove(PropertyKey)
        End If
        TestContext.Properties.Add(PropertyKey, PropertyValue)
    End Sub

    Public Function SafeGetTestContextProperty(ByVal PropertyKey As String, ByVal PropertyType As Type) As Object

        SafeGetTestContextProperty = Nothing
        If TestContext IsNot Nothing AndAlso TestContext.Properties(PropertyKey) IsNot Nothing Then
            If PropertyType IsNot Nothing Then
                Select Case PropertyType.Name
                    Case GetType(String).Name
                        SafeGetTestContextProperty = TestContext.Properties(PropertyKey).ToString()
                    Case GetType(Integer).Name
                        Dim Prop As Integer

                        If Integer.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(Boolean).Name
                        Dim Prop As Boolean

                        If Boolean.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(Date).Name
                        Dim Prop As Date

                        If Date.TryParse(TestContext.Properties(PropertyKey).ToString(), Prop) Then
                            SafeGetTestContextProperty = Prop
                        End If
                    Case GetType(DataTable).Name
                        Dim TryDataTable As Object = TestContext.Properties(PropertyKey)
                        Dim Prop As DataTable

                        If TypeOf TryDataTable Is DataTable Then
                            Prop = CType(TryDataTable, DataTable)
                            SafeGetTestContextProperty = Prop
                        End If
                    Case Else
                        SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
                End Select
            Else
                SafeGetTestContextProperty = TestContext.Properties(PropertyKey)
            End If
        End If
    End Function
#End Region

#Region "StoredProcedure_Test MustOverrides"

    Public Function GetExistingCashBalCashierTenSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("PeriodID, ")
        sb.Append("CashierID, ")
        sb.Append("CurrencyID, ")
        sb.Append("ID, ")
        sb.Append("Quantity, ")
        sb.Append("Amount, ")
        sb.Append("PickUp ")
        sb.Append("FROM CashBalCashierTen ")
        sb.Append("WHERE PeriodID = @PeriodId ")
        sb.Append("AND CashierID = @CashierId")
        sb.Append("AND CurrencyID = @CurrencyId")
        sb.Append("AND ID = @Id")

        GetExistingCashBalCashierTenSQL = sb.ToString
    End Function

    Public Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Microsoft.VisualBasic.Collection) As Boolean
        Dim KeyField As Key

        KeyFields = New Collection
        KeyField = New Key("TranDate", "DATE1", "Transaction Date", Nothing, "DATE1", FieldType.ftDate)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TranNumber", "TRAN", "Transaction Number", Nothing, "TRAN", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TillId", "TILL", "Till Number", Nothing, "TILL", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("", "NUMB", "Sequence Number", Nothing, "NUMB", FieldType.ftInteger)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        GetKeyFieldsNoValueRequired = True
    End Function

    Public Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetNonExistingSQL(), con)
                    Dim TranDate As Date = Now
                    Dim TranNo As Integer
                    Dim TillNo As Integer
                    Dim Found As Boolean
                    Dim ReturnedData As DataTable

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        TranNo = 1
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ReturnedData = New DataTable
                                ReturnedData.Load(reader)
                                reader.Close()

                                If ReturnedData.Rows.Count = 0 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetNonExistingKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record.")
        End Try
    End Function

    Public Function GetNonExistingSQL() As String

        GetNonExistingSQL = GetExistingCashBalCashierTenSQL()
    End Function

    Public ReadOnly Property StoredProcedureName() As String
        Get
            StoredProcedureName = "usp_CashBalCashTenUpdate"
        End Get
    End Property

    Public Function GetExistingMultipleDataAndKeyWithDynamicSQL(ByRef ExistingMultipleData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingCashBalCashierTenSQL(), con)
                    Dim TranDate As Date = CDate("10/06/2011") ' Now
                    Dim TranNo As Integer = 4130
                    Dim TillNo As Integer
                    Dim Found As Boolean

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ExistingMultipleData = New DataTable
                                ExistingMultipleData.Load(reader)
                                reader.Close()

                                If ExistingMultipleData.Rows.Count > 1 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranNo = 1
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetExistingMultipleDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a multiple records.")
        End Try
    End Function
#End Region

    Protected Enum FieldType
        ftString = 0
        ftInteger
        ftDecimal
        ftBoolean
        ftDate
    End Enum

    Protected Structure Key

        Private storedProcedureName As String
        Private storedProcedureParamName As String
        Private dynSQLName As String
        Private friendlyName As String
        Private keyValue As Object
        Private keyfieldType As FieldType

        Public Property SPName() As String
            Get
                SPName = storedProcedureName
            End Get
            Set(ByVal value As String)
                storedProcedureName = value
            End Set
        End Property

        Public Property SPParamName() As String
            Get
                SPParamName = storedProcedureParamName
            End Get
            Set(ByVal value As String)
                storedProcedureParamName = value
            End Set
        End Property

        Public Property DynamicSQLName() As String
            Get
                DynamicSQLName = dynSQLName
            End Get
            Set(ByVal value As String)
                dynSQLName = value
            End Set
        End Property

        Public Property Caption() As String
            Get
                Caption = friendlyName
            End Get
            Set(ByVal value As String)
                friendlyName = value
            End Set
        End Property

        Public Property Value() As Object
            Get
                Value = keyValue
            End Get
            Set(ByVal value As Object)
                keyValue = value
            End Set
        End Property

        Public Property Type() As FieldType
            Get
                Type = keyfieldType
            End Get
            Set(ByVal value As FieldType)
                keyfieldType = value
            End Set
        End Property

        Public Sub New(ByVal KeySPParamName As String, ByVal KeyDynamicSQLName As String, ByVal KeyCaption As String, ByRef KeyValue As Object, ByVal KeyType As FieldType)

            SPParamName = KeySPParamName
            DynamicSQLName = KeyDynamicSQLName
            Caption = KeyCaption
            Value = KeyValue
            Type = KeyType
        End Sub

        Public Sub New(ByVal KeySPParamName As String, ByVal KeyDynamicSQLName As String, ByVal KeyCaption As String, ByRef KeyValue As Object, ByVal SPName As String, ByVal KeyType As FieldType)

            SPParamName = KeySPParamName
            DynamicSQLName = KeyDynamicSQLName
            Caption = KeyCaption
            Value = KeyValue
            SPName = SPName
            Type = KeyType
        End Sub
    End Structure

    Protected Class StoredProcedureKey
        Private Keys As Collection

        Public Sub New(ByVal SPKeys As Collection)

            Keys = New Collection
            If SPKeys IsNot Nothing Then
                For Each SPKey As Key In SPKeys
                    Keys.Add(SPKey, SPKey.DynamicSQLName)
                Next
            End If
        End Sub

        Public Overrides Function ToString() As String

            ToString = ""
            If Keys IsNot Nothing Then
                For Each SPKey As Key In Keys
                    If ToString.Length > 0 Then
                        ToString &= ";"
                    End If
                    ToString &= SPKey.Caption & " = '" & SPKey.Value.ToString & "'"
                Next
            Else
                ToString = "Empty StoredProcedureKey"
            End If
        End Function

        Public Function GetKeyString(ByRef From As DataRow) As String

            GetKeyString = ""
            If Keys IsNot Nothing Then
                For Each SPKey As Key In Keys
                    If ToString.Length > 0 Then
                        GetKeyString &= ";"
                    End If
                    GetKeyString &= SPKey.Caption & " = '" & From.Item(SPKey.DynamicSQLName).ToString & "'"
                Next
            Else
                GetKeyString = "Empty StoredProcedureKey"
            End If
        End Function

        Public Function AddParameters(ByRef ComToAddTo As Command) As Boolean

            If Keys IsNot Nothing And ComToAddTo IsNot Nothing Then
                For Each SPKey As Key In Keys
                    ComToAddTo.AddParameter("@" & SPKey.SPParamName, SPKey.Value)
                Next
                AddParameters = True
            End If
        End Function

        Public Function AddParameters(ByRef ComToAddTo As SqlCommand) As Boolean

            If Keys IsNot Nothing And ComToAddTo IsNot Nothing Then
                For Each SPKey As Key In Keys
                    ComToAddTo.Parameters.Add(New SqlParameter("@" & SPKey.SPParamName, SPKey.Value))
                Next
                AddParameters = True
            End If
        End Function
    End Class

End Class