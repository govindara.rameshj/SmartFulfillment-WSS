﻿Imports System
'Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class SalePaidGet
    Inherits Cts.Oasys.Hubs.Core.Database.StoredProcedure_MultipleRows_Test

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "StoredProcedure_Test Overridable Tests"

    ' Seems it is necessary to override these tests (and just call the base test with the override),
    ' just to get base test to run.

    '<TestMethod()> _
    'Public Overrides Sub StoredProcedureExists()

    '    MyBase.StoredProcedureExists()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsDataTable()

    '    MyBase.WhenDataExists_StoredProcedureGetsDataTable()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

    '    MyBase.WhenDataExists_StoredProcedureGetsAtLeastOneRecord()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureCanGetMultipleRecords()

    '    MyBase.WhenDataExists_StoredProcedureCanGetMultipleRecords()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsRecordsByKey()

    '    MyBase.WhenDataExists_StoredProcedureGetsRecordsByKey()
    'End Sub

#End Region

#Region "Field Tests"
    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedDATE1()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DATE1", Message, FieldType.ftDate), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedTILL()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TILL", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedTRAN()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TRAN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedNUMB()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("NUMB", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedTYPE()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TYPE", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedAMNT()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("AMNT", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCARD()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CARD", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedEXDT()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("EXDT", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCOPN()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("COPN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCLAS()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CLAS", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedAUTH()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("AUTH", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCKEY()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CKEY", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedSUPV()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SUPV", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedPOST()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("POST", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCKAC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CKAC", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCKSC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CKSC", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCKNO()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CKNO", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedDBRF()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DBRF", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedSEQN()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SEQN", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedISSU()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ISSU", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedATYP()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ATYP", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedMERC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("MERC", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCBAM()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CBAM", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedDIGC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DIGC", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedECOM()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("ECOM", Message, FieldType.ftBoolean), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedCTYP()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("CTYP", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedEFID()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("EFID", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedEFTC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("EFTC", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedSTDT()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("STDT", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedAUTHDESC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("AUTHDESC", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedSECCODE()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SECCODE", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedTENC()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TENC", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedMPOW()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("MPOW", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedMRAT()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("MRAT", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedTENV()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("TENV", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRTI()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RTI", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

#End Region

#Region "StoredProcedure_Test MustOverrides"
    Public Overrides Function GetExistingDataAndKeyWithDynamicSQL(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim TranDate As Date = CDate("30/06/2011") ' Now
                    Dim TranNo As Integer = 4200
                    Dim TillNo As Integer
                    Dim Found As Boolean

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ExistingData = New DataTable
                                ExistingData.Load(reader)
                                reader.Close()

                                If ExistingData.Rows.Count = 1 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranNo = 1
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetExistingDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Overrides Function GetExistingSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("dP.DATE1,dP.TILL,dP.[TRAN],dP.NUMB,dP.[TYPE],dP.AMNT,")
        sb.Append("dP.[CARD],dP.EXDT,dP.COPN,dP.CLAS,dP.AUTH,dP.CKEY,")
        sb.Append("dP.SUPV,dP.POST,dP.CKAC,dP.CKSC,dP.CKNO,dP.DBRF,")
        sb.Append("dP.SEQN,dP.ISSU,dP.ATYP,dP.MERC,dP.CBAM,dP.DIGC, ")
        sb.Append("dP.ECOM,dP.CTYP,dP.EFID,dP.EFTC,dP.STDT,dP.AUTHDESC, ")
        sb.Append("dP.SECCODE,dP.TENC,dP.MPOW,dP.MRAT,dP.TENV,dP.RTI ")
        sb.Append("FROM DLPAID   dP ")
        sb.Append("WHERE dP.DATE1=@TranDate ")
        sb.Append("and dP.[TRAN]=@TranNumber ")
        sb.Append("and dP.TILL=@TillId ")

        GetExistingSQL = sb.ToString
    End Function

    Public Overrides Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetNonExistingSQL(), con)
                    Dim TranDate As Date = Now
                    Dim TranNo As Integer
                    Dim TillNo As Integer
                    Dim Found As Boolean
                    Dim ReturnedData As DataTable

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        TranNo = 1
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ReturnedData = New DataTable
                                ReturnedData.Load(reader)
                                reader.Close()

                                If ReturnedData.Rows.Count = 0 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetNonExistingKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record.")
        End Try
    End Function

    Public Overrides Function GetNonExistingSQL() As String

        GetNonExistingSQL = GetExistingSQL()
    End Function

    Public Overrides ReadOnly Property StoredProcedureName() As String
        Get
            StoredProcedureName = "SalePaidGet"
        End Get
    End Property

    Public Overrides Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Collection) As Boolean
        Dim KeyField As Key

        KeyFields = New Collection
        KeyField = New Key("TranDate", "DATE1", "Transaction Date", Nothing, "DATE1", FieldType.ftDate)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TranNumber", "TRAN", "Transaction Number", Nothing, "TRAN", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("TillId", "TILL", "Till Number", Nothing, "TILL", FieldType.ftString)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        KeyField = New Key("", "NUMB", "Sequence Number", Nothing, "NUMB", FieldType.ftInteger)
        KeyFields.Add(KeyField, KeyField.DynamicSQLName)
        GetKeyFieldsNoValueRequired = True
    End Function

    Public Overrides Function GetExistingMultipleDataAndKeyWithDynamicSQL(ByRef ExistingMultipleData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim TranDate As Date = CDate("30/06/2011") ' Now
                    Dim TranNo As Integer = 4200
                    Dim TillNo As Integer
                    Dim Found As Boolean

                    com.Parameters.Add("@TranDate", SqlDbType.Date)
                    com.Parameters.Add("@TranNumber", SqlDbType.NChar)
                    com.Parameters.Add("@TillId", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While TranDate > Date.MinValue And Not Found
                        com.Parameters.Item("@TranDate").Value = TranDate
                        Do While TranNo < 9999 And Not Found
                            com.Parameters.Item("@TranNumber").Value = TranNo.ToString.PadLeft(4, "0"c)
                            TillNo = 1
                            Do While TillNo <= 20 And Not Found
                                com.Parameters.Item("@TillId").Value = TillNo.ToString.PadLeft(2, "0"c)

                                Dim reader As SqlDataReader = com.ExecuteReader

                                ExistingMultipleData = New DataTable
                                ExistingMultipleData.Load(reader)
                                reader.Close()

                                If ExistingMultipleData.Rows.Count > 1 Then
                                    Dim OMONKey As New Key

                                    With OMONKey
                                        .SPParamName = "TranDate"
                                        .DynamicSQLName = "DATE1"
                                        .Caption = "Transaction Date"
                                        .Value = TranDate
                                        .Type = FieldType.ftDate
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TranNumber"
                                        .DynamicSQLName = "TRAN"
                                        .Caption = "Transaction Number"
                                        .Value = TranNo.ToString.PadLeft(4, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    With OMONKey
                                        .SPParamName = "TillId"
                                        .DynamicSQLName = "TILL"
                                        .Caption = "Till Number"
                                        .Value = TillNo.ToString.PadLeft(2, "0"c)
                                        .Type = FieldType.ftString
                                    End With
                                    Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                    Found = True
                                End If
                                TillNo += 1
                            Loop
                            TranNo += 1
                        Loop
                        TranNo = 1
                        TranDate = DateAdd(DateInterval.Day, -1, TranDate)
                    Loop
                    GetExistingMultipleDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a multiple records.")
        End Try
    End Function
#End Region
End Class
