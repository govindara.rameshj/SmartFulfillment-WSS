﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class SaleOrderRefundGet
    Inherits StoredProcedure_MultipleRows_Test

#Region "StoredProcedure_Test Overridable Tests"

    ' Seems it is necessary to override these tests (and just call the base test with the override),
    ' just to get base test to run.

    '<TestMethod()> _
    'Public Overrides Sub StoredProcedureExists()

    '    MyBase.StoredProcedureExists()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsDataTable()

    '    MyBase.WhenDataExists_StoredProcedureGetsDataTable()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsAtLeastOneRecord()

    '    MyBase.WhenDataExists_StoredProcedureGetsAtLeastOneRecord()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureCanGetMultipleRecords()

    '    MyBase.WhenDataExists_StoredProcedureCanGetMultipleRecords()
    'End Sub

    '<TestMethod()> _
    'Public Overrides Sub WhenDataExists_StoredProcedureGetsRecordsByKey()

    '    MyBase.WhenDataExists_StoredProcedureGetsRecordsByKey()
    'End Sub

#End Region

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Field Tests"

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedNumber()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("Number", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedSkuNumber()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SkuNumber", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedSkuDescription()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SkuDescription", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedSkuUnitMeasure()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SkuUnitMeasure", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRefundStoreId()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RefundStoreId", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRefundDate()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RefundDate", Message, FieldType.ftDate), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRefundTill()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RefundTill", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRefundTransaction()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RefundTransaction", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedQtyReturned()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("QtyReturned", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedQtyCancelled()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("QtyCancelled", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedQtyOnHand()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("QtyOnHand", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedQtyOnOrder()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("QtyOnOrder", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedRefundStatus()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("RefundStatus", Message, FieldType.ftInteger), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedSellingStoreIbtOut()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("SellingStoreIbtOut", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedDeliverySource()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("DeliverySource", Message, FieldType.ftString), Message)
    '    End If
    'End Sub

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureRetrievedPrice()
    '    Dim Message As String = ""

    '    If HaveDataToCompare() Then
    '        Assert.IsTrue(FieldsMatch("Price", Message, FieldType.ftDecimal), Message)
    '    End If
    'End Sub

#End Region

#Region "Additonal tests for optional parameters"

    '<TestMethod()> _
    'Public Sub WhenDataExists_StoredProcedureGetsRecordByRefundStatus()
    '    Dim Message As String = ""

    '    If IntialiseForRefundStatus() Then
    '        Dim Existing As DataTable = SafeGetTestContextProperty(TC_KEY_EXISTING, GetType(DataTable))

    '        If Existing IsNot Nothing AndAlso TypeOf Existing Is DataTable Then
    '            Dim SPExisting As DataTable = SafeGetTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, GetType(DataTable))

    '            If SPExisting IsNot Nothing AndAlso TypeOf SPExisting Is DataTable Then
    '                If Existing.Rows.Count = SPExisting.Rows.Count Then
    '                    Dim ExistingRow As Integer

    '                    For ExistingRow = 0 To Existing.Rows.Count - 1
    '                        Dim Found As Boolean
    '                        Dim KeyFields As New Collection

    '                        For SPExistingRow = 0 To SPExisting.Rows.Count - 1
    '                            If GetKeyFieldsNoValueRequired(KeyFields) Then
    '                                Found = True
    '                                For Each SPKey As Key In KeyFields
    '                                    If Existing.Rows(ExistingRow).Item(SPKey.DynamicSQLName) <> SPExisting.Rows(SPExistingRow).Item(SPKey.DynamicSQLName) Then
    '                                        Found = False
    '                                        Exit For
    '                                    End If
    '                                Next
    '                            End If
    '                            If Found Then
    '                                Exit For
    '                            End If
    '                        Next
    '                        If Not Found Then
    '                            Assert.Fail(StoredProcedureName & " (using Refund Status parameters) failed to bring back a matching row for one which was brought back the the existing dynamic sql")
    '                            Exit For
    '                        End If
    '                    Next
    '                Else
    '                    Assert.Fail(StoredProcedureName & " (using Refund Status Range parameters) did not return the same number of rows (" & SPExisting.Rows.Count.ToString & " as the existing pervasive sql (" & Existing.Rows.Count.ToString & ")")
    '                End If
    '            End If
    '        End If
    '    End If
    'End Sub

#End Region

#Region "StoredProcedure_Test MustOverrides"

    Public Overrides Function GetExistingDataAndKeyWithDynamicSQL(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim OrderNumber As Integer = 13365
                    Dim Found As Boolean

                    com.Parameters.Add("@OrderNumber", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While OrderNumber < 999999 And Not Found
                        com.Parameters.Item("@Ordernumber").Value = OrderNumber.ToString.PadLeft(6, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingData = New DataTable
                        ExistingData.Load(reader)
                        reader.Close()

                        If ExistingData.Rows.Count = 1 Then
                            Dim OMONKey As New Key

                            With OMONKey
                                .SPParamName = "OrderNumber"
                                .DynamicSQLName = "NUMB"
                                .Caption = "Order number"
                                .Value = OrderNumber.ToString.PadLeft(6, "0"c)
                                .Type = FieldType.ftString
                            End With
                            Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                            Found = True
                        End If
                        OrderNumber += 1
                    Loop
                    GetExistingDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Overrides Function GetExistingSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("cr.NUMB	as OrderNumber,")
        sb.Append("cr.LINE	as Number,")
        sb.Append("cl.SKUN  as SkuNumber, ")
        sb.Append("st.DESCR	as SkuDescription,")
        sb.Append("st.BUYU	as SkuUnitMeasure,")
        sb.Append("cr.RefundStoreId,")
        sb.Append("cr.RefundDate,")
        sb.Append("cr.RefundTill,")
        sb.Append("cr.RefundTransaction,")
        sb.Append("cr.QtyReturned,")
        sb.Append("cr.QtyCancelled,")
        sb.Append("st.ONHA	as QtyOnHand,")
        sb.Append("st.ONOR	as QtyOnOrder,")
        sb.Append("cr.RefundStatus,")
        sb.Append("cr.SellingStoreIbtOut, ")
        sb.Append("cl.DeliverySource, ")
        sb.Append("cl.Price ")
        sb.Append("FROM CORREFUND cr ")
        sb.Append("INNER JOIN CORLIN cl on cr.NUMB=cl.NUMB and cr.LINE=cl.LINE ")
        sb.Append("INNER JOIN STKMAS st ON st.SKUN=cl.SKUN ")
        sb.Append("WHERE cr.NUMB=@OrderNumber ")
        sb.Append("ORDER BY cr.RefundStoreId, cr.RefundTill, cr.RefundTransaction, cr.LINE")

        GetExistingSQL = sb.ToString
    End Function

    Public Overrides Function GetKeyFieldsNoValueRequired(ByRef KeyFields As Microsoft.VisualBasic.Collection) As Boolean
        Dim OMONKey As New Key

        KeyFields = New Collection
        With OMONKey
            .SPParamName = "OrderNumber"
            .DynamicSQLName = "OrderNumber"
            .SPName = "OrderNumber"
            .Caption = "Order Number"
            .Type = FieldType.ftString
        End With
        KeyFields.Add(OMONKey, OMONKey.DynamicSQLName)
        With OMONKey
            .SPParamName = ""
            .DynamicSQLName = "Number"
            .SPName = "Number"
            .Caption = "Line Number"
            .Type = FieldType.ftString
        End With
        KeyFields.Add(OMONKey, OMONKey.DynamicSQLName)
        With OMONKey
            .SPParamName = ""
            .DynamicSQLName = "RefundStoreId"
            .SPName = "RefundStoreId"
            .Caption = "Refund Store Id"
            .Type = FieldType.ftInteger
        End With
        KeyFields.Add(OMONKey, OMONKey.DynamicSQLName)
        With OMONKey
            .SPParamName = ""
            .DynamicSQLName = "RefundDate"
            .SPName = "RefundDate"
            .Caption = "Refund Date"
            .Type = FieldType.ftDate
        End With
        KeyFields.Add(OMONKey, OMONKey.DynamicSQLName)
        With OMONKey
            .SPParamName = ""
            .DynamicSQLName = "RefundTill"
            .SPName = "RefundTill"
            .Caption = "Refund Till"
            .Type = FieldType.ftString
        End With
        KeyFields.Add(OMONKey, OMONKey.DynamicSQLName)
        With OMONKey
            .SPParamName = ""
            .DynamicSQLName = "RefundTransaction"
            .SPName = "RefundTransaction"
            .Caption = "Refund Transaction"
            .Type = FieldType.ftString
        End With
        KeyFields.Add(OMONKey, OMONKey.DynamicSQLName)
    End Function

    Public Overrides Function GetNonExistingKeyWithDynamicSQL(ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim OrderNumber As Integer = 1000
                    Dim Found As Boolean
                    Dim ReturnedData As DataTable

                    com.Parameters.Add("@OrderNumber", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While OrderNumber < 999999 And Not Found
                        com.Parameters.Item("@Ordernumber").Value = OrderNumber.ToString.PadLeft(6, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ReturnedData = New DataTable
                        ReturnedData.Load(reader)
                        reader.Close()

                        If ReturnedData.Rows.Count = 0 Then
                            Dim OMONKey As New Key

                            With OMONKey
                                .SPParamName = "OrderNumber"
                                .DynamicSQLName = "NUMB"
                                .Caption = "Order number"
                                .Value = OrderNumber.ToString.PadLeft(6, "0"c)
                                .Type = FieldType.ftString
                            End With
                            Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                            Found = True
                        End If
                        OrderNumber += 1
                    Loop
                    GetNonExistingKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Overrides Function GetNonExistingSQL() As String

        GetNonExistingSQL = GetExistingSQL()
    End Function

    Public Overrides ReadOnly Property StoredProcedureName() As String
        Get
            StoredProcedureName = "SaleOrderRefundGet"
        End Get
    End Property

    Public Overrides Function GetExistingMultipleDataAndKeyWithDynamicSQL(ByRef ExistingMultipleData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingSQL(), con)
                    Dim OrderNumber As Integer = 13365
                    Dim Found As Boolean

                    com.Parameters.Add("@OrderNumber", SqlDbType.NChar)
                    ' Find a date, tran and till which brings back any existing sales transactions
                    Do While OrderNumber < 999999 And Not Found
                        com.Parameters.Item("@Ordernumber").Value = OrderNumber.ToString.PadLeft(6, "0"c)

                        Dim reader As SqlDataReader = com.ExecuteReader

                        ExistingMultipleData = New DataTable
                        ExistingMultipleData.Load(reader)
                        reader.Close()

                        If ExistingMultipleData.Rows.Count > 1 Then
                            Dim OMONKey As New Key

                            With OMONKey
                                .SPParamName = "OrderNumber"
                                .DynamicSQLName = "NUMB"
                                .Caption = "Order Number"
                                .Value = OrderNumber.ToString.PadLeft(6, "0"c)
                                .Type = FieldType.ftString
                            End With
                            Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                            Found = True
                        End If
                        OrderNumber += 1
                    Loop
                    GetExistingMultipleDataAndKeyWithDynamicSQL = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get multiple existing records.")
        End Try
    End Function

#End Region

#Region "initialisation For Extra Tests on Stored Procedure Optional Parameters"

    Private Function IntialiseForRefundStatus() As Object

        If TestContext.Properties(TC_KEY_INITIALISED) Is Nothing Then
            If CheckForStoredProcedure() Then
                If GetExistingByRefundStatus() Then
                    If GetNonExistingKeyByRefundStatus() Then
                        If GetExistingUsingStoredProcedure() Then
                            If GetNonExistingUsingStoredProcedure() Then
                                SafeAddTestContextProperty(TC_KEY_INITIALISED, True)
                            Else
                                Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an non existing record using stored procedure")
                            End If
                        Else
                            Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using stored procedure")
                        End If
                    Else
                        Assert.Inconclusive("Test initialisation failed whilst trying to retrieve a key for a non existing record using dynamic sql")
                    End If
                Else
                    Assert.Inconclusive("Test initialisation failed whilst trying to retrieve an existing record using dynamic sql")
                End If
            Else
                Assert.Inconclusive("Test initialisation failed whilst trying to check for the stored procedure (" & StoredProcedureName & ")")
            End If
        End If
        IntialiseForRefundStatus = SafeGetTestContextProperty(TC_KEY_INITIALISED, GetType(Boolean))
    End Function

    Protected Function GetExistingByRefundStatus() As Boolean

        Try
            Dim ExistingData As New DataTable
            Dim Keys As Collection = New Collection

            If GetExistingDataAndKeyWithDynamicSQLByRefundStatus(ExistingData, Keys) Then
                ' Keep the key for use in retrieval by stored procedure
                SafeAddTestContextProperty(TC_KEY_EXISTINGKEY, New StoredProcedureKey(Keys))
                SafeAddTestContextProperty(TC_KEY_EXISTING, ExistingData)
                GetExistingByRefundStatus = True
            End If
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Protected Function GetNonExistingKeyByRefundStatus() As Boolean

        Try
            Dim Keys As Collection = New Collection

            If GetNonExistingKeyWithDynamicSQL(Keys) Then
                SafeAddTestContextProperty(TC_KEY_NONEXISTINGKEY, New StoredProcedureKey(Keys))
                GetNonExistingKeyByRefundStatus = True
            End If
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a non existing record key.")
        End Try
    End Function

    Protected Overridable Function GetExistingByRefundStatusUsingStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                Dim TrySPKey As Object

                com.StoredProcedureName = StoredProcedureName
                TrySPKey = SafeGetTestContextProperty(TC_KEY_EXISTINGKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey = CType(TrySPKey, StoredProcedureKey)

                    If SPKey.AddParameters(com) Then
                        Dim SPExisting As DataTable

                        SPExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDUREEXISTING, SPExisting)
                        GetExistingByRefundStatusUsingStoredProcedure = True
                    End If
                End If
            End Using
        End Using
    End Function

    Protected Overridable Function GetNonExistingByRefundStatusUsingStoredProcedure() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                Dim TrySPKey As Object

                com.StoredProcedureName = StoredProcedureName
                TrySPKey = SafeGetTestContextProperty(TC_KEY_NONEXISTINGKEY, GetType(StoredProcedureKey))
                If TrySPKey IsNot Nothing AndAlso TypeOf TrySPKey Is StoredProcedureKey Then
                    Dim SPKey = CType(TrySPKey, StoredProcedureKey)

                    If SPKey.AddParameters(com) Then
                        Dim SPNonExisting As DataTable

                        SPNonExisting = com.ExecuteDataTable
                        SafeAddTestContextProperty(TC_KEY_STOREDPROCEDURENONEXISTING, SPNonExisting)
                        GetNonExistingByRefundStatusUsingStoredProcedure = True
                    End If
                End If
            End Using
        End Using
    End Function

    Public Function GetExistingDataAndKeyWithDynamicSQLByRefundStatus(ByRef ExistingData As System.Data.DataTable, ByRef Keys As Microsoft.VisualBasic.Collection) As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetExistingWithRefundStatusParameterSQL, con)
                    Dim OrderNumber As Integer = 13365
                    Dim Found As Boolean

                    com.Parameters.Add("@OrderNumber", SqlDbType.NChar)
                    com.Parameters.Add("@RefundStatus", SqlDbType.Int)
                    ' Find a ordernumber and refund status which brings back any existing refunds
                    Do While OrderNumber < 999999 And Not Found
                        Dim RefundStatus As Integer

                        RefundStatus = 100
                        com.Parameters.Item("@OrderNumber").Value = OrderNumber.ToString.PadLeft(6, "0"c)

                        Do While RefundStatus < 999 And Not Found

                            com.Parameters.Item("@RefundStatus").Value = RefundStatus

                            Dim reader As SqlDataReader = com.ExecuteReader

                            ExistingData = New DataTable
                            ExistingData.Load(reader)
                            reader.Close()

                            If ExistingData.Rows.Count = 1 Then
                                Dim OMONKey As New Key

                                With OMONKey
                                    .SPParamName = "OrderNumber"
                                    .DynamicSQLName = "NUMB"
                                    .Caption = "Order Number"
                                    .Value = OrderNumber.ToString.PadLeft(6, "0"c)
                                    .Type = FieldType.ftString
                                End With
                                Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                With OMONKey
                                    .SPParamName = "RefundStatus"
                                    .DynamicSQLName = "RefundStatus"
                                    .Caption = "Refund Status"
                                    .Value = RefundStatus
                                    .Type = FieldType.ftInteger
                                End With
                                Keys.Add(OMONKey, OMONKey.DynamicSQLName)
                                Found = True
                            End If
                            If RefundStatus Mod 100 = 90 Then
                                RefundStatus += 9
                            Else
                                RefundStatus += 10
                            End If
                        Loop
                    Loop
                    GetExistingDataAndKeyWithDynamicSQLByRefundStatus = Found
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get an existing record.")
        End Try
    End Function

    Public Function GetExistingWithRefundStatusParameterSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT ")
        sb.Append("cr.NUMB	as OrderNumber,")
        sb.Append("cr.LINE	as Number,")
        sb.Append("cl.SKUN  as SkuNumber, ")
        sb.Append("st.DESCR	as SkuDescription,")
        sb.Append("st.BUYU	as SkuUnitMeasure,")
        sb.Append("cr.RefundStoreId,")
        sb.Append("cr.RefundDate,")
        sb.Append("cr.RefundTill,")
        sb.Append("cr.RefundTransaction,")
        sb.Append("cr.QtyReturned,")
        sb.Append("cr.QtyCancelled,")
        sb.Append("st.ONHA	as QtyOnHand,")
        sb.Append("st.ONOR	as QtyOnOrder,")
        sb.Append("cr.RefundStatus,")
        sb.Append("cr.SellingStoreIbtOut, ")
        sb.Append("cl.DeliverySource, ")
        sb.Append("cl.Price ")
        sb.Append("FROM CORREFUND cr ")
        sb.Append("INNER JOIN CORLIN cl on cr.NUMB=cl.NUMB and cr.LINE=cl.LINE ")
        sb.Append("INNER JOIN STKMAS st ON st.SKUN=cl.SKUN ")
        sb.Append("WHERE cr.NUMB=@OrderNumber and cr.RefundStatus = @RefundStatus")
        sb.Append("ORDER BY cr.RefundStoreId, cr.RefundTill, cr.RefundTransaction, cr.LINE")

        GetExistingWithRefundStatusParameterSQL = sb.ToString
    End Function

    Public Function GetNonExistingWithRefundStatusParameterSQL() As String

        GetNonExistingWithRefundStatusParameterSQL = GetExistingWithRefundStatusParameterSQL()
    End Function

#End Region

End Class