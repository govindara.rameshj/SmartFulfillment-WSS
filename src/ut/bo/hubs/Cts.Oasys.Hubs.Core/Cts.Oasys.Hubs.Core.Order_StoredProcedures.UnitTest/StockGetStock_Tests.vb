﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data
Imports System.Data.SqlClient

<TestClass()> Public Class StockGetStock_Tests

    Private testContextInstance As TestContext

    Private Const skuToCopyFrom As String = "154385"
    Private Const skuToCreate As String = "899889"

    Private Const CONNECTION_STRING As String = "Server=S130372\SQLEXPRESS;Database=Oasys;User ID=sa;Password=gandalf;Trusted_Connection=False;"
    Private Const _INSERT_SQL As String = "INSERT INTO STKMAS SELECT " & skuToCreate & ", [PLUD], [DESCR], [FILL], [DEPT], [GROU], [VATC], [BUYU], [SUPP], [PROD], [PACK], [PRIC], [PPRI], [COST], [DSOL], [DREC], [DORD], [DPRC], [DSET], [DOBS], [DDEL], [ONHA], [ONOR], [MINI], [MAXI], [SOLD], [ISTA], [IRIS], [IRIB], [IMDN], [ICAT], [IOBS], [IDEL], [ILOC], [IEAN], [IPPC], [ITAG], [INON], [SALV1], [SALV2], [SALV3], [SALV4], [SALV5], [SALV6], [SALV7], [SALU1], [SALU2], [SALU3], [SALU4], [SALU5], [SALU6], [SALU7], [CPDO], [CYDO], [AWSF], [AW13], [DATS], [UWEK], [FLAG], [CFLG], [US001], [US002], [US003], [US004], [US005], [US006], [US007], [US008], [US009], [US0010], [US0011], [US0012], [US0013], [US0014], [DO001], [DO002], [DO003], [DO004], [DO005], [DO006], [DO007], [DO008], [DO009], [DO0010], [DO0011], [DO0012], [DO0013], [DO0014], [SQ04], [SQ13], [SQ01], [SQOR], [TREQ], [TREV], [TACT], [RETQ], [RETV], [CHKD], [LABN], [LABS], [STON], [STOQ], [SOD1], [SOD2], [SOD3], [LABM], [LABL], [WGHT], [VOLU], [NOOR], [SUP1], [IODT], [FODT], [PMIN], [PMSD], [PMED], [PMCP], [SMAN], [DFLC], [IMCP], [FOLT], [IDEA], [QADJ], [TAGF], [ALPH], [CTGY], [GRUP], [SGRP], [STYL], [EQUV], [HFIL], [REVT], [RPRI], [IWAR], [IOFF], [ISOL], [QUAR], [IPRD], [EQPM], [EQPU], [MDNQ], [WTFQ], [SALT], [MODT], [AAPC], [IPSK], [AADJ], [SUP2], [FRAG], [TIMB], [ELEC], [SOFS], [WQTY], [WRAT], [WSEQ], [BALI], [PRFSKU], [MSPR1], [MSPR2], [MSPR3], [MSPR4], [MSTQ1], [MSTQ2], [MSTQ3], [MSTQ4], [MREQ1], [MREQ2], [MREQ3], [MREQ4], [MREV1], [MREV2], [MREV3], [MREV4], [MADQ1], [MADQ2], [MADQ3], [MADQ4], [MADV1], [MADV2], [MADV3], [MADV4], [MPVV1], [MPVV2], [MPVV3], [MPVV4], [MIBQ1], [MIBQ2], [MIBQ3], [MIBQ4], [MIBV1], [MIBV2], [MIBV3], [MIBV4], [MRTQ1], [MRTQ2], [MRTQ3], [MRTQ4], [MRTV1], [MRTV2], [MRTV3], [MRTV4], [MCCV1], [MCCV2], [MCCV3], [MCCV4], [MDRV1], [MDRV2], [MDRV3], [MDRV4], [MBSQ1], [MBSQ2], [MBSQ3], [MBSQ4], [MBSV1], [MBSV2], [MBSV3], [MBSV4], [MSTP1], [MSTP2], [MSTP3], [MSTP4], [FIL11], [FIL12], [FIL13], [FIL14], [ToForecast], [IsInitialised], [DemandPattern], [PeriodDemand], [PeriodTrend], [ErrorForecast], [ErrorSmoothed], [BufferConversion], [BufferStock], [ServiceLevelOverride], [ValueAnnualUsage], [OrderLevel], [SaleWeightSeason], [SaleWeightBank], [SaleWeightPromo], [DateLastSoqInit], [StockLossPerWeek], [FlierPeriod1], [FlierPeriod2], [FlierPeriod3], [FlierPeriod4], [FlierPeriod5], [FlierPeriod6], [FlierPeriod7], [FlierPeriod8], [FlierPeriod9], [FlierPeriod10], [FlierPeriod11], [FlierPeriod12], [SalesBias0], [SalesBias1], [SalesBias2], [SalesBias3], [SalesBias4], [SalesBias5], [SalesBias6] FROM STKMAS WHERE SKUN = '" & skuToCopyFrom & "'"
    Private Const _DELETE_SQL As String = "DELETE FROM STKMAS WHERE SKUN = '" & skuToCreate & "'"

    Private Const _SELECT_SQL As String = "SELECT **FIELD_TOKEN** FROM STKMAS WHERE SKUN  = '" & skuToCreate & "'"

    Private Const SP_NAME As String = "[StockGetStock]"

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        ' run SQL command to populate the STKMAS table
        Using con As New SqlConnection(CONNECTION_STRING)
            con.Open()
            ' delete test record if exists
            Using delCom As New SqlCommand(_DELETE_SQL, con)
                delCom.CommandType = CommandType.Text
                delCom.ExecuteNonQuery()
            End Using

            ' insert test record to STKMAS
            Using com As New SqlCommand(_INSERT_SQL, con)
                com.CommandType = CommandType.Text
                com.ExecuteNonQuery()
            End Using
        End Using

    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    <ClassCleanup()> Public Shared Sub MyClassCleanup()
        ' remove record from STKMAS
        ' run SQL command to remove STKMAS record inserted by test initialise
        Using con As New SqlConnection(CONNECTION_STRING)
            con.Open()
            Using com As New SqlCommand(_DELETE_SQL, con)
                com.CommandType = CommandType.Text
                com.ExecuteNonQuery()
            End Using
        End Using

    End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '

    'Private Function ValuesAreEqual(ByVal fieldName As String) As Boolean
    '    Dim returnedList As List(Of Object) = GetValuesToCompare(fieldName)

    '    If IsDBNull(returnedList.Item(0)) or IsDBNull(returnedList.Item(1)) Then
    '        If IsDBNull(returnedList.Item(0)) Then returnedList.Item(0) = "NULL"
    '        If IsDBNull(returnedList.Item(1)) Then returnedList.Item(1) = "NULL"
    '    End If

    '    Return returnedList.Item(0) = returnedList.Item(1)

    'End Function

    Private Function ValuesAreEqual(ByVal fieldName As String, ByVal returnedFieldName As String) As Boolean
        Dim returnedList As List(Of Object) = GetValuesToCompare(fieldName, returnedFieldName)

        If IsDBNull(returnedList.Item(0)) Or IsDBNull(returnedList.Item(1)) Then
            If IsDBNull(returnedList.Item(0)) Then returnedList.Item(0) = "NULL"
            If IsDBNull(returnedList.Item(1)) Then returnedList.Item(1) = "NULL"
        End If

        Return returnedList.Item(0) = returnedList.Item(1)

    End Function
    'Private Function GetValuesToCompare(ByVal physicalFieldName As String) As List(Of Object)

    '    Dim sql As String = _SELECT_SQL.Replace("**FIELD_TOKEN**", physicalFieldName)
    '    Dim results As New List(Of Object)

    '    Using con As New SqlConnection(CONNECTION_STRING)
    '        con.Open()
    '        Using com As New SqlCommand(sql, con)
    '            com.CommandType = CommandType.Text
    '            Dim reader As SqlDataReader = com.ExecuteReader

    '            While reader.Read
    '                results.Add(reader(physicalFieldName))
    '            End While

    '            Return results
    '        End Using
    '    End Using
    'End Function

    Private Function GetValuesToCompare(ByVal physicalFieldName As String, ByVal returnedFieldName As String) As List(Of Object)

        Dim sql As String = _SELECT_SQL.Replace("**FIELD_TOKEN**", physicalFieldName)
        Dim results As New List(Of Object)

        Using con As New SqlConnection(CONNECTION_STRING)
            con.Open()
            Using com As New SqlCommand(sql, con)
                com.CommandType = CommandType.Text
                Dim reader As SqlDataReader = com.ExecuteReader

                While reader.Read
                    results.Add(reader(physicalFieldName))
                End While
                reader.Close()
            End Using

            Using com As New SqlCommand(sql, con)
                com.CommandType = CommandType.StoredProcedure
                com.CommandText = SP_NAME

                Dim param As New SqlParameter
                With param
                    .ParameterName = "@SkuNumber"
                    .SqlDbType = SqlDbType.Char
                    .Size = 6
                    .Value = skuToCreate
                End With

                com.Parameters.Add(param)

                Dim reader As SqlDataReader = com.ExecuteReader

                While reader.Read
                    results.Add(reader(returnedFieldName))
                End While

            End Using

        End Using

        Return results

    End Function
#End Region

    '<TestMethod()> Public Sub StockGetStock_CompareSPtoDESCRValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DESCR", "Description"))
    'End Sub

    <TestMethod()> Public Sub StockGetStock_CompareSPtoDESCRValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DESCR", "Description"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoPRODValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("PROD", "ProductCode"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoPACKValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("PACK", "PackSize"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoPRICValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("PRIC", "Price"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoCOSTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("COST", "Cost"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoWGHTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("WGHT", "Weight"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoONHAValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("ONHA", "OnHandQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoONORValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("ONOR", "OnOrderQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoMINIValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("MINI", "MinimumQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoMAXIValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("MAXI", "MaximumQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoRETQValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("RETQ", "OpenReturnsQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoMDNQValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("MDNQ", "MarkdownQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoWTFQValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("WTFQ", "WriteOffQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoINONValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("INON", "IsNonStock"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoIOBSValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("IOBS", "IsObsolete"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoIDELValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("IDEL", "IsDeleted"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoIRISValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("IRIS", "IsItemSingle"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoNOORValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("NOOR", "IsNonOrder"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoTREQValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("TREQ", "ReceivedTodayQty"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoTREVValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("TREV", "ReceivedTodayValue"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDATSValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DATS", "DateFirstStocked"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoIODTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("IODT", "DateFirstOrder"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoFODTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("FODT", "DateFinalOrder"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDRECValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DREC", "DateLastReceived"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoCTGYValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("CTGY", "HieCategory"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoGRUPValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("GRUP", "HieGroup"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoSGRPValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("SGRP", "HieSubgroup"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoSTYLValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("STYL", "HieStyle"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoTACTValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("TACT", "ActivityToday"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoVATCValues_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("VATC", "VatCode"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoSALT_Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("SALT", "SalesType"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoITAG_Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("ITAG", "IsTaggedItem"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS001Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US001", "UnitSales1"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS002Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US002", "UnitSales2"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS003Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US003", "UnitSales3"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS004Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US004", "UnitSales4"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS005Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US005", "UnitSales5"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS006Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US006", "UnitSales6"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS007Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US007", "UnitSales7"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS008Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US008", "UnitSales8"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS009Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US009", "UnitSales9"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0010Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0010", "UnitSales10"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0011Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0011", "UnitSales11"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0012Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0012", "UnitSales12"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0013Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0013", "UnitSales13"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoUS0014Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("US0014", "UnitSales14"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO001Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO001", "DaysOutStock1"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO002Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO002", "DaysOutStock2"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO003Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO003", "DaysOutStock3"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO004Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO004", "DaysOutStock4"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO005Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO005", "DaysOutStock5"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO006Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO006", "DaysOutStock6"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO007Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO007", "DaysOutStock7"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO008Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO008", "DaysOutStock8"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO009Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO009", "DaysOutStock9"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0010Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0010", "DaysOutStock10"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0011Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0011", "DaysOutStock11"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0012Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0012", "DaysOutStock12"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0013Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0013", "DaysOutStock13"))
    End Sub
    <TestMethod()> Public Sub StockGetStock_CompareSPtoDO0014Values_AreEqual()
        ' assert
        Assert.IsTrue(ValuesAreEqual("DO0014", "DaysOutStock14"))
    End Sub


    '<TestMethod()> Public Sub StockGetStock_ComparePLUDValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PLUD"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDESCRValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DESCR"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFILLValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FILL"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDEPTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DEPT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareGROUValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("GROU"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareVATCValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("VATC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareBUYUValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("BUYU"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSUPPValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SUPP"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePRODValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PROD"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePACKValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PACK"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePRICValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PRIC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePPRIValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PPRI"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareCOSTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("COST"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDSOLValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DSOL"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDRECValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DREC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDORDValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DORD"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDPRCValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DPRC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDSETValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DSET"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDOBSValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DOBS"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDDELValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DDEL"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareONHAValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ONHA"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareONORValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ONOR"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMINIValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MINI"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMAXIValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MAXI"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSOLDValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SOLD"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareISTAValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ISTA"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIRISValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IRIS"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIRIBValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IRIB"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIMDNValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IMDN"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareICATValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ICAT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIOBSValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IOBS"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIDELValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IDEL"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareILOCValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ILOC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIEANValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IEAN"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIPPCValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IPPC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareITAGValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ITAG"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareINONValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("INON"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALV5Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALV5"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALV6Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALV6"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALV7Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALV7"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALU1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALU1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALU2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALU2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALU3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALU3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALU4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALU4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALU5Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALU5"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALU6Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALU6"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALU7Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALU7"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareCPDOValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("CPDO"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareCYDOValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("CYDO"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareAWSFValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("AWSF"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareAW13Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("AW13"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDATSValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DATS"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUWEKValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("UWEK"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFLAGValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FLAG"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareCFLGValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("CFLG"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS001Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US001"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS002Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US002"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS003Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US003"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS004Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US004"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS005Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US005"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS006Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US006"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS007Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US007"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS008Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US008"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS009Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US009"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS0010Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US0010"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS0011Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US0011"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS0012Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US0012"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS0013Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US0013"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareUS0014Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("US0014"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO001Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO001"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO002Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO002"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO003Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO003"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO004Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO004"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO005Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO005"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO006Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO006"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO007Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO007"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO008Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO008"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO009Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO009"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO0010Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO0010"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO0011Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO0011"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO0012Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO0012"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO0013Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO0013"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDO0014Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DO0014"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSQ04Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SQ04"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSQ13Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SQ13"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSQ01Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SQ01"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSQORValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SQOR"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareTREQValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("TREQ"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareTREVValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("TREV"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareTACTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("TACT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareRETQValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("RETQ"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareRETVValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("RETV"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareCHKDValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("CHKD"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareLABNValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("LABN"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareLABSValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("LABS"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSTONValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("STON"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSTOQValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("STOQ"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSOD1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SOD1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSOD2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SOD2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSOD3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SOD3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareLABMValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("LABM"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareLABLValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("LABL"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareWGHTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("WGHT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareVOLUValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("VOLU"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareNOORValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("NOOR"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSUP1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SUP1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIODTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IODT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFODTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FODT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePMINValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PMIN"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePMSDValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PMSD"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePMEDValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PMED"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePMCPValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PMCP"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSMANValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SMAN"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDFLCValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DFLC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIMCPValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IMCP"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFOLTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FOLT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIDEAValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IDEA"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareQADJValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("QADJ"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareTAGFValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("TAGF"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareALPHValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ALPH"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareCTGYValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("CTGY"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareGRUPValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("GRUP"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSGRPValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SGRP"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSTYLValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("STYL"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareEQUVValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("EQUV"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareHFILValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("HFIL"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareREVTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("REVT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareRPRIValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("RPRI"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIWARValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IWAR"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIOFFValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IOFF"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareISOLValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ISOL"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareQUARValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("QUAR"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIPRDValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IPRD"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareEQPMValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("EQPM"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareEQPUValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("EQPU"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMDNQValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MDNQ"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareWTFQValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("WTFQ"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSALTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SALT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMODTValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MODT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareAAPCValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("AAPC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIPSKValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IPSK"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareAADJValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("AADJ"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSUP2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SUP2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFRAGValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FRAG"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareTIMBValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("TIMB"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareELECValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ELEC"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSOFSValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SOFS"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareWQTYValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("WQTY"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareWRATValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("WRAT"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareWSEQValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("WSEQ"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareBALIValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("BALI"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePRFSKUValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PRFSKU"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSPR1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSPR1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSPR2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSPR2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSPR3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSPR3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSPR4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSPR4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSTQ1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSTQ1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSTQ2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSTQ2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSTQ3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSTQ3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSTQ4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSTQ4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMREQ1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MREQ1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMREQ2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MREQ2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMREQ3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MREQ3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMREQ4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MREQ4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMREV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MREV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMREV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MREV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMREV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MREV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMREV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MREV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMADQ1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MADQ1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMADQ2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MADQ2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMADQ3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MADQ3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMADQ4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MADQ4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMADV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MADV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMADV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MADV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMADV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MADV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMADV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MADV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMPVV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MPVV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMPVV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MPVV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMPVV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MPVV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMPVV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MPVV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMIBQ1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MIBQ1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMIBQ2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MIBQ2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMIBQ3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MIBQ3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMIBQ4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MIBQ4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMIBV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MIBV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMIBV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MIBV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMIBV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MIBV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMIBV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MIBV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMRTQ1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MRTQ1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMRTQ2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MRTQ2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMRTQ3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MRTQ3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMRTQ4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MRTQ4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMRTV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MRTV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMRTV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MRTV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMRTV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MRTV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMRTV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MRTV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMCCV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MCCV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMCCV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MCCV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMCCV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MCCV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMCCV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MCCV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMDRV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MDRV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMDRV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MDRV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMDRV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MDRV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMDRV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MDRV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMBSQ1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MBSQ1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMBSQ2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MBSQ2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMBSQ3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MBSQ3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMBSQ4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MBSQ4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMBSV1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MBSV1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMBSV2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MBSV2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMBSV3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MBSV3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMBSV4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MBSV4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSTP1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSTP1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSTP2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSTP2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSTP3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSTP3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareMSTP4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("MSTP4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFIL11Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FIL11"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFIL12Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FIL12"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFIL13Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FIL13"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFIL14Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FIL14"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareToForecastValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ToForecast"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareIsInitialisedValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("IsInitialised"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDemandPatternValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DemandPattern"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePeriodDemandValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PeriodDemand"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_ComparePeriodTrendValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("PeriodTrend"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareErrorForecastValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ErrorForecast"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareErrorSmoothedValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ErrorSmoothed"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareBufferConversionValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("BufferConversion"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareBufferStockValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("BufferStock"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareServiceLevelOverrideValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ServiceLevelOverride"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareValueAnnualUsageValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("ValueAnnualUsage"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareOrderLevelValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("OrderLevel"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSaleWeightSeasonValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SaleWeightSeason"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSaleWeightBankValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SaleWeightBank"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSaleWeightPromoValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SaleWeightPromo"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareDateLastSoqInitValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("DateLastSoqInit"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareStockLossPerWeekValues_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("StockLossPerWeek"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod5Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod5"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod6Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod6"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod7Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod7"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod8Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod8"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod9Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod9"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod10Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod10"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod11Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod11"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareFlierPeriod12Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("FlierPeriod12"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSalesBias0Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SalesBias0"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSalesBias1Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SalesBias1"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSalesBias2Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SalesBias2"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSalesBias3Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SalesBias3"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSalesBias4Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SalesBias4"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSalesBias5Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SalesBias5"))
    'End Sub
    '<TestMethod()> Public Sub StockGetStock_CompareSalesBias6Values_AreEqual()
    '    ' assert
    '    Assert.IsTrue(ValuesAreEqual("SalesBias6"))
    'End Sub

End Class
