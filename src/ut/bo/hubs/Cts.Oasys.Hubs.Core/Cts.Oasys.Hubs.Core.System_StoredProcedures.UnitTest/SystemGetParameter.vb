<TestClass()> _
Public Class SystemGetParameter

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Static members"

    Private Const StoredProcedureName As String = "SystemGetParameter"
    Private Shared FoundStoredProcedure As Boolean
    Private Shared Initialised As Boolean
    Private Shared ExistingParameter As DataTable
    Private Shared NonExistingParameterId As Integer
    Private Shared StoredProcedureExistingParameter As DataTable
    Private Shared StoredProcedureNonExistingParameter As DataTable

#End Region

#Region "Additional test attributes"

    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        Assert.IsTrue(SetupForTests, "Failed initialising for the Cts.Oasys.Hubs.Core.System Stored Procedures tests.")
                    Case Else
                        Assert.Fail("Cannot perform Cts.Oasys.Hubs.Core.System Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub

    <TestInitialize()> Public Sub MyTestInitialize()
    End Sub

    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    <ClassCleanup()> _
    Public Shared Sub MyClassCleanup()

        FoundStoredProcedure = Nothing
        Initialised = Nothing
        ExistingParameter = Nothing
        NonExistingParameterId = Nothing
        StoredProcedureExistingParameter = Nothing
        StoredProcedureNonExistingParameter = Nothing
    End Sub

#End Region

#Region "Local Static methods"

    Private Shared Function CheckForStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim spCheck As DataTable

                    com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & StoredProcedureName & "'"
                    spCheck = com.ExecuteDataTable
                    If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                        FoundStoredProcedure = True
                    End If
                    CheckForStoredProcedure = True
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & StoredProcedureName & ".")
        End Try
    End Function

    Private Shared Function GetExistingParameter() As Boolean
        Dim Id As Integer

        Using con As New Connection
            Using com As New Command(con)
                Id = 100
                Do While Id < 999999
                    com.ClearParamters()
                    com.CommandText = GetParameterSQL()
                    com.AddParameter("@Id", Id)
                    ExistingParameter = com.ExecuteDataTable
                    If ExistingParameter IsNot Nothing Then
                        If ExistingParameter.Rows.Count > 0 Then
                            GetExistingParameter = True
                            Exit Do
                        End If
                    End If
                    Id += 1
                Loop
            End Using
        End Using
    End Function

    Private Shared Function GetNonExistingParameterId() As Boolean
        Dim Id As Integer

        Using con As New Connection
            Using com As New Command(con)
                Dim NonExistingParameter As DataTable

                Id = 1
                Do While Id < 999999
                    com.ClearParamters()
                    com.CommandText = GetParameterSQL()
                    com.AddParameter("@Id", Id)
                    NonExistingParameter = com.ExecuteDataTable
                    If NonExistingParameter IsNot Nothing Then
                        If NonExistingParameter.Rows.Count = 0 Then
                            NonExistingParameterId = Id
                            GetNonExistingParameterId = True
                            Exit Do
                        End If
                    End If
                    Id += 1
                Loop
            End Using
        End Using
    End Function

    Private Shared Function GetStoredProcedureExistingParameter() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                com.ClearParamters()
                com.StoredProcedureName = StoredProcedureName
                com.AddParameter("@Id", Convert.ToInt32(ExistingParameter.Rows(0).Item("ParameterId")))
                StoredProcedureExistingParameter = com.ExecuteDataTable
                GetStoredProcedureExistingParameter = True
            End Using
        End Using
    End Function

    Private Shared Function GetStoredProcedureNonExistingParameter() As Boolean

        Using con As New Connection
            Using com As New Command(con)
                com.ClearParamters()
                com.StoredProcedureName = StoredProcedureName
                com.AddParameter("@Id", NonExistingParameterId)
                StoredProcedureNonExistingParameter = com.ExecuteDataTable
                GetStoredProcedureNonExistingParameter = True
            End Using
        End Using
    End Function

    Private Shared Function GetParameterSQL() As String

        GetParameterSQL = "Select * from Parameters where ParameterId = @Id"
    End Function

    Private Shared Function SetupForTests() As Boolean

        If Not Initialised Then
            If CheckForStoredProcedure() Then
                If GetExistingParameter() Then
                    If GetNonExistingParameterId() Then
                        If GetStoredProcedureExistingParameter() Then
                            If GetStoredProcedureNonExistingParameter() Then
                                Initialised = True
                            End If
                        End If
                    End If
                End If
            End If
        End If
        SetupForTests = Initialised
    End Function

#End Region

#Region "Tests"

    <TestMethod()> _
    Public Overridable Sub StoredProcedureExists()

        Assert.IsTrue(FoundStoredProcedure, "Stored procedure '" & StoredProcedureName & "' does not exist.")
    End Sub

    <TestMethod()> Public Sub WhenExists_GetsParameterData()

        Assert.IsNotNull(StoredProcedureExistingParameter, StoredProcedureName & " did not retrieve any parameter data")
    End Sub

    <TestMethod()> Public Sub WhenExists_GetsAtLeastOneParameter()

        If StoredProcedureExistingParameter IsNot Nothing Then
            If StoredProcedureExistingParameter.Rows.Count = 0 Then
                Assert.Fail(StoredProcedureName & " did not retrieve any parameters when should have retrieved the existing parameter (ParameterId = " & ExistingParameter.Rows(0).Item("ParameterId").ToString & ")")
            End If
        Else
            Assert.Inconclusive(StoredProcedureName & " test failed a preceding test so WhenExists_GetsAtLeastOneParameter test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_GetsOnlyOneParameter()

        If StoredProcedureExistingParameter IsNot Nothing Then
            If StoredProcedureExistingParameter.Rows.Count > 1 Then
                Assert.Fail(StoredProcedureName & " retrieved multiple parameters instead of just the existing parameter (ParameterId = " & ExistingParameter.Rows(0).Item("ParameterId").ToString & ")")
            End If
        Else
            Assert.Inconclusive(StoredProcedureName & " test failed a preceding test so WhenExists_GetsOnlyOneParameter test not performed")
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_GetsSpecifiedParameterById()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("ParameterId", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedParameterDescriptionMatches()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("Description", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedParameterStringValueMatches()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("StringValue", Message, FieldType.ftString), Message)
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedParameterLongValueMatches()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("LongValue", Message, FieldType.ftInteger), Message)
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedParameterBooleanValueMatches()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("BooleanValue", Message, FieldType.ftBoolean), Message)
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedParameterDecimalValueMatches()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("DecimalValue", Message, FieldType.ftDecimal), Message)
        End If
    End Sub

    <TestMethod()> Public Sub WhenExists_RetrievedParameterValueTypeMatches()
        Dim Message As String = ""

        If HaveDataToCompare() Then
            Assert.IsTrue(FieldsMatch("ValueType", Message, FieldType.ftInteger), Message)
        End If
    End Sub

#End Region

#Region "Common Tests"

    Private Function HaveDataToCompare() As Boolean

        If ExistingParameter IsNot Nothing Then
            If ExistingParameter.Rows.Count = 1 Then
                If StoredProcedureExistingParameter IsNot Nothing Then
                    If StoredProcedureExistingParameter.Rows.Count = 1 Then
                        HaveDataToCompare = True
                    Else
                        Assert.Inconclusive(StoredProcedureName & " failed to return an existing parameter so this test has nothing to compare")
                    End If
                Else
                    Assert.Inconclusive(StoredProcedureName & " failed to return a parameter datatable so this test has nothing to compare")
                End If
            Else
                Assert.Inconclusive("Dynamic sql failed to return an existing parameter so this test has nothing to compare against")
            End If
        Else
            Assert.Inconclusive("Dynamic sql failed to return a parameter datatable so this test has nothing to compare against")
        End If
    End Function

#End Region

#Region "Field Comparisons"

    Private Enum FieldType
        ftString = 0
        ftInteger
        ftDecimal
        ftBoolean
        ftDate
    End Enum

    Private Function FieldsMatch(ByVal FieldName As String, ByRef Message As String, ByVal Type As FieldType) As Boolean
        Dim Marker As String = "Start"

        Try
            Marker = "Existing"
            If ExistingParameter.Rows(0).Item(FieldName) IsNot Nothing Then
                Marker = "Retreived"
                If StoredProcedureExistingParameter.Rows(0).Item(FieldName) IsNot Nothing Then
                    Marker = "Select"
                    Select Case Type
                        Case FieldType.ftString
                            Marker = "String"
                            FieldsMatch = StringsMatch(ExistingParameter.Rows(0).Item(FieldName), StoredProcedureExistingParameter.Rows(0).Item(FieldName), Message)
                        Case FieldType.ftInteger
                            Marker = "Integer"
                            FieldsMatch = IntegersMatch(ExistingParameter.Rows(0).Item(FieldName), StoredProcedureExistingParameter.Rows(0).Item(FieldName), Message)
                        Case FieldType.ftBoolean
                            Marker = "Boolean"
                            FieldsMatch = BooleansMatch(ExistingParameter.Rows(0).Item(FieldName), StoredProcedureExistingParameter.Rows(0).Item(FieldName), Message)
                        Case FieldType.ftDecimal
                            Marker = "Decimal"
                            FieldsMatch = DecimalsMatch(ExistingParameter.Rows(0).Item(FieldName), StoredProcedureExistingParameter.Rows(0).Item(FieldName), Message)
                        Case FieldType.ftDate
                            Marker = "Date"
                            FieldsMatch = DatesMatch(ExistingParameter.Rows(0).Item(FieldName), StoredProcedureExistingParameter.Rows(0).Item(FieldName), Message)
                        Case Else
                            Marker = "Not recognised"
                            Assert.Inconclusive("Cannot check field " & FieldName & " as field type specified is no recognised")
                    End Select
                Else
                    Assert.Fail(StoredProcedureName & " Stored Procedure field name " & FieldName & " is not a field name in the original dynamic sql")
                End If
            Else
                Assert.Inconclusive("Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row.")
            End If
        Catch ex As Exception
            Select Case Marker
                Case "Start"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem at start of FieldsMatch function. " & ex.Message)
                Case "Existing"
                    Assert.Inconclusive("Cannot check field " & FieldName & ", it is not a field in the original dynamic sql row.")
                Case "Retreived"
                    Assert.Fail("Stored Procedure field name " & FieldName & " is not a field name in the original dynamic sql")
                Case "Select"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing field type. " & ex.Message)
                Case "String"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing string field type. " & ex.Message)
                Case "Integer"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing integer field type. " & ex.Message)
                Case "Boolean"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing boolean field type. " & ex.Message)
                Case "Decimal"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing decimal field type. " & ex.Message)
                Case "Date"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing date field type. " & ex.Message)
                Case "Not recognised"
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem processing unrecognised field type. " & ex.Message)
                Case Else
                    Assert.Inconclusive("Cannot check field " & FieldName & " problem with FieldsMatch function. " & ex.Message)
            End Select
        End Try
    End Function

    Private Function BooleansMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Booleans Match fail (" & FailReason & ")"
        Else
            If TypeOf Against Is Boolean Then
                Dim AgainstAsBoolean As Boolean = CBool(Against)

                If TypeOf Compare Is Boolean Then
                    Dim CompareAsBoolean As Boolean = CBool(Compare)

                    If AgainstAsBoolean = CompareAsBoolean Then
                        BooleansMatch = True
                    Else
                        FailReason = "Booleans Match fail as 'Against' (" & AgainstAsBoolean.ToString & ") and 'Compare' (" & CompareAsBoolean.ToString & ") are not the same"
                    End If
                Else
                    FailReason = "Booleans Match fail as 'Compare' is not a Boolean"
                End If
            Else
                FailReason = "Booleans Match fail as 'Against' is not a Boolean"
            End If
        End If
        Message = FailReason
    End Function

    Private Function DatesMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal DateInterval As DateInterval = DateInterval.Second, Optional ByVal AreNullable As Boolean = False) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Dates Match fail (" & FailReason & ")"
        Else
            Dim AgainstDate As Date
            Dim CompareDate As Date

            If AreNullable Then
                If TypeOf Against Is Date? Then
                    Dim AgainstAsNullableDate As Date?

                    If TypeOf Compare Is Date? Then
                        Dim CompareAsNullableDate As Date?

                        If AgainstAsNullableDate.HasValue Then
                            AgainstDate = AgainstAsNullableDate.Value
                            If CompareAsNullableDate.HasValue Then
                                CompareDate = CompareAsNullableDate.Value
                            Else
                                FailReason = "Dates Match fail as 'Against' is not Null but 'Compare' is Null"
                            End If
                        Else
                            If CompareAsNullableDate.HasValue Then
                                FailReason = "Dates Match fail as 'Against' is Null but 'Compare' is not Null"
                            Else
                                Message = FailReason
                                DatesMatch = True
                                Exit Function
                            End If
                        End If
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                    End If
                Else
                    FailReason = "Dates Match fail as 'Compare' is not a Nullable Date"
                End If
            Else
                If TypeOf Against Is Date Then
                    AgainstDate = CDate(Against)
                    If TypeOf Compare Is Date Then
                        CompareDate = CDate(Compare)
                    Else
                        FailReason = "Dates Match fail as 'Compare' is not a Date"
                    End If
                Else
                    FailReason = "Dates Match fail as 'Compare' is not a Date"
                End If
            End If
            Dim Diff As Boolean = DateDiff(DateInterval, AgainstDate, CompareDate) <> 0

            If Diff Then
                Dim Interval As String

                Select Case DateInterval
                    Case Microsoft.VisualBasic.DateInterval.Day
                        Interval = "days"
                    Case Microsoft.VisualBasic.DateInterval.DayOfYear
                        Interval = "days (of year)"
                    Case Microsoft.VisualBasic.DateInterval.Hour
                        Interval = "hours"
                    Case Microsoft.VisualBasic.DateInterval.Month
                        Interval = "months"
                    Case Microsoft.VisualBasic.DateInterval.Quarter
                        Interval = "quarters"
                    Case Microsoft.VisualBasic.DateInterval.Second
                        Interval = "seconds"
                    Case Microsoft.VisualBasic.DateInterval.Weekday
                        Interval = "week days"
                    Case Microsoft.VisualBasic.DateInterval.WeekOfYear
                        Interval = "weeks (of year)"
                    Case Microsoft.VisualBasic.DateInterval.Year
                        Interval = "years"
                    Case Else
                        Interval = "unknown units"
                End Select
                FailReason = "Dates Match fail as 'Against' (" & AgainstDate.ToString("F") & ") is " & CInt(Diff).ToString & " " & Interval & " different to 'Compare' (" & AgainstDate.ToString("F") & ")"
            Else
                DatesMatch = True
            End If
        End If
        Message = FailReason
    End Function

    Private Function DecimalsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Integers Match fail (" & FailReason & ")"
        Else
            Dim AgainstAsDecimal As Decimal

            If Decimal.TryParse(Against.ToString, AgainstAsDecimal) Then
                Dim CompareAsDecimal As Decimal

                If Decimal.TryParse(Compare.ToString, CompareAsDecimal) Then
                    If AgainstAsDecimal <> CompareAsDecimal Then
                        FailReason = "Decimals Match fail as 'Against' (= " & AgainstAsDecimal & ") and 'Compare' (= " & CompareAsDecimal & ") are not the same"
                    Else
                        DecimalsMatch = True
                    End If
                Else
                    FailReason = "Decimals Match fail as 'Compare' is not a Decimal"
                End If
            Else
                FailReason = "Decimals Match fail as 'Against' is not a Decimal"
            End If
        End If
        Message = FailReason
    End Function

    Private Function IntegersMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Integers Match fail (" & FailReason & ")"
        Else
            Dim AgainstAsInteger As Integer

            If Integer.TryParse(Against.ToString, AgainstAsInteger) Then
                Dim CompareAsInteger As Integer

                If Integer.TryParse(Compare.ToString, CompareAsInteger) Then
                    If AgainstAsInteger <> CompareAsInteger Then
                        FailReason = "Integers Match fail as 'Against' (= " & AgainstAsInteger & ") and 'Compare' (= " & CompareAsInteger & ") are not the same"
                    Else
                        IntegersMatch = True
                    End If
                Else
                    FailReason = "Integers Match fail as 'Compare' is not an Integer"
                End If
            Else
                FailReason = "Integers Match fail as 'Against' is not an Integer"
            End If
        End If
        Message = FailReason
    End Function

    Private Function StringsMatch(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String, Optional ByVal StringComparisonType As System.StringComparison = StringComparison.CurrentCulture) As Boolean
        Dim FailReason As String = ""

        If Not ObjectsBothNotNothingOrBothSomething(Against, Compare, FailReason) Then
            FailReason = "Strings Match fail (" & FailReason & ")"
        Else
            If TypeOf Against Is String Then
                Dim AgainstAsString As String = Against.ToString

                If TypeOf Compare Is String Then
                    Dim CompareAsString As String = Compare.ToString

                    If AgainstAsString = String.Empty Then
                        If CompareAsString = String.Empty Then
                            StringsMatch = True
                        Else
                            FailReason = "Strings Match fail as 'Against' is Empty but 'Compare' is not Empty"
                        End If
                    Else
                        If CompareAsString = String.Empty Then
                            FailReason = "Strings Match fail as 'Against' is not Empty but 'Compare' is Empty"
                        Else
                            If String.Equals(AgainstAsString, CompareAsString, StringComparisonType) Then
                                StringsMatch = True
                            Else
                                FailReason = "Strings Match fail as 'Against' (= " & AgainstAsString & ") and 'Compare' (= " & CompareAsString & ") are not the same"
                            End If
                        End If
                    End If
                Else
                    FailReason = "Strings Match fail as 'Compare' is not a string"
                End If
            Else
                FailReason = "Strings Match fail as 'Against' is not a string"
            End If
        End If
        Message = FailReason
    End Function

    Private Function ObjectsBothNotNothingOrBothSomething(ByRef Against As Object, ByRef Compare As Object, ByRef Message As String) As Boolean
        Dim FailReason As String

        If Against Is Nothing Then
            If Compare IsNot Nothing Then
                FailReason = "Against is 'Nothing', Compare is not 'Nothing'"
            Else
                ObjectsBothNotNothingOrBothSomething = True
            End If
        Else
            If Compare Is Nothing Then
                FailReason = "Against is not 'Nothing', Compare is 'Nothing'"
            Else
                ObjectsBothNotNothingOrBothSomething = True
            End If
        End If
    End Function
#End Region

End Class
