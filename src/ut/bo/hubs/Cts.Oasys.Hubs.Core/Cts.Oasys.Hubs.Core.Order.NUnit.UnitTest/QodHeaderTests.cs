using System;
using Cts.Oasys.Hubs.Core.Order.Qod;
using Cts.Oasys.Hubs.Core.System.Store;
using NUnit.Framework;
using Rhino.Mocks;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails;
using WSS.BO.DataLayer.Model.Repositories;

namespace Cts.Oasys.Hubs.Core.Order.NUnit.UnitTest
{
    [TestFixture]
    public class QodHeaderTests
    {
        private IExternalRequestRepository _dalMock;
        private QodHeader _order;

        [SetUp]
        public void CreateObjects()
        {
            _dalMock = MockRepository.GenerateMock<IExternalRequestRepository>();

            _order = new QodHeader(true) {
                SellingStoreId = 8000,
                DateDelivery = DateTime.Today
            };

            var store = new Store();
            Common.ThisStore = store;
            Common.AllStores = new StoreCollection(store); 
        }

        [Test]
        public void QodCreatesNewConsignmentRequest()
        {
            _order.GenerateNewConsignmentRequest(_dalMock);

            _dalMock.AssertWasCalled(x => x.CreateRequest(
                Arg<string>.Is.Equal(ExternalRequestType.NewConsignment),
                Arg<ConsignmentRequestDetailsExtended>.Is.Anything));
        }

        [Test]
        public void QodCreatesUpdateConsignmentRequest()
        {
            _order.GenerateUpdateConsignmentRequest(_dalMock);

            _dalMock.AssertWasCalled(x => x.CreateRequest(
                Arg<string>.Is.Equal(ExternalRequestType.UpdateConsignment),
                Arg<ConsignmentRequestDetailsExtended>.Is.Anything));
        }

        [Test]
        public void QodCreatesCancelConsignmentRequest()
        {
            _order.GenerateCancelConsignmentRequest(_dalMock);

            _dalMock.AssertWasCalled(x => x.CreateRequest(
                Arg<string>.Is.Equal(ExternalRequestType.CancelConsignment),
                Arg<ConsignmentRequestDetails>.Is.Anything));
        }
    }
}
