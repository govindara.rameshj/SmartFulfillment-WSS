﻿<TestClass()> Public Class RequestUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"

    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    End Sub

#End Region

#Region "Unit Test - Function: PopulateRefundSequenceNo"

    <TestMethod()> Public Sub FunctionPopulateRefundSequenceNo_RequirementOn_NullRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim Request As New OMOrderRefundRequest

        ArrangeRequirementCheck(True)

        Assert.IsFalse(Request.PopulateRefundSequenceNo(Nothing).HasValue)

    End Sub

    <TestMethod()> Public Sub FunctionPopulateRefundSequenceNo_RequirementOn_NonNullRefundSequenceNo_ExpectNonNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim Request As New OMOrderRefundRequest

        ArrangeRequirementCheck(True)

        Assert.AreEqual(10, Request.PopulateRefundSequenceNo(10).Value)

    End Sub

    <TestMethod()> Public Sub FunctionPopulateRefundSequenceNo_RequirementOff_NullRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim Request As New OMOrderRefundRequest

        ArrangeRequirementCheck(False)

        Assert.IsFalse(Request.PopulateRefundSequenceNo(Nothing).HasValue)

    End Sub

    <TestMethod()> Public Sub FunctionPopulateRefundSequenceNo_RequirementOff_NonNullRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim Request As New OMOrderRefundRequest

        ArrangeRequirementCheck(False)

        Assert.IsFalse(Request.PopulateRefundSequenceNo(10).HasValue)

    End Sub

#End Region

#Region "Unit Test - Function: PopulateRefundSequenceNo"

    <TestMethod()> Public Sub ClassRequest_RequirementOn_NullRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim Request As New OMOrderRefundRequest

        ArrangeRequirementCheck(True)
        ArrangeRequestObject(Request, Nothing)
        Assert.AreEqual(Nothing, Request.OrderRefunds(0).PreProcessorRefundSeq)

    End Sub

    <TestMethod()> Public Sub ClassRequest_RequirementOn_NonNullRefundSequenceNo_ExpectNonNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim Request As New OMOrderRefundRequest

        ArrangeRequirementCheck(True)
        ArrangeRequestObject(Request, 10)
        Assert.AreEqual("10", Request.OrderRefunds(0).PreProcessorRefundSeq)

    End Sub

    <TestMethod()> Public Sub ClassRequest_RequirementOff_NullRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim Request As New OMOrderRefundRequest

        ArrangeRequirementCheck(False)
        ArrangeRequestObject(Request, Nothing)
        Assert.AreEqual(Nothing, Request.OrderRefunds(0).PreProcessorRefundSeq)

    End Sub

    <TestMethod()> Public Sub ClassRequest_RequirementOff_NonNullRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim Request As New OMOrderRefundRequest

        ArrangeRequirementCheck(False)
        ArrangeRequestObject(Request, 10)
        Assert.AreEqual(Nothing, Request.OrderRefunds(0).PreProcessorRefundSeq)

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementCheck(ByVal RequirementEnabled As Nullable(Of Boolean))

        Dim Stub As New RequirementRepositoryStub

        Stub.ConfigureStub(RequirementEnabled)
        RequirementRepositoryFactory.FactorySet(Stub)

    End Sub

    Private Sub ArrangeRequestObject(ByRef Request As OMOrderRefundRequest, ByVal RefundSeqNo As Nullable(Of Integer))

        Dim Qod As New QodHeader
        Dim Refund As New QodRefund
        Dim LineCollection As New QodLineCollection
        Dim Line As New QodLine

        With Refund
            .Number = "123"
            .RefundStatus = 100
            .RefundStoreId = 80
            .RefundDate = Now
            .RefundTill = "01"
            .RefundTransaction = "0001"
            .RefundSequenceNo = RefundSeqNo
        End With

        With Line
            .Number = "0123"
            .DeliverySource = "0"
        End With
        LineCollection.Add(Line)

        With Qod
            .Number = "123"
            '.OmOrderNumber = 1
            .Refunds.Add(Refund)
            .UnitTestPopulateQodLinesCollection(LineCollection)
        End With

        Request.SetFieldsFromQod(Qod)

    End Sub

#End Region


End Class