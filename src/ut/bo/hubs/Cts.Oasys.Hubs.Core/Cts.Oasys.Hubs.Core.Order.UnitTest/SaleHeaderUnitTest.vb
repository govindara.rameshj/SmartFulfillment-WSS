﻿<TestClass()> Public Class SaleHeaderUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Constructor"

    <TestMethod()> Public Sub Function_PopulateRefundSequenceNo_RequirementDeactivated_NonNullRefundSequenceNo_ExpectNulValueReturned()

        Dim VendaRefundRequest As New WebService.CTSEnableRefundCreateRequest
        Dim VendaRefundReponse As New WebService.CTSEnableRefundCreateResponse
        Dim SH As New Qod.SaleHeader
        Dim Output As Nullable(Of Integer)

        'arrange
        ArrangeRequirementCheck(False)
        VendaRefundRequest = CType(VendaRefundRequest.Deserialise(VendaRefund.ToString), WebService.CTSEnableRefundCreateRequest)
        VendaRefundReponse.SetFieldsFromRequest(VendaRefundRequest, DateTime.Now)
        'act
        Output = SH.PopulateRefundSequenceNo(VendaRefundReponse)

        Assert.IsFalse(Output.HasValue)

    End Sub

    <TestMethod()> Public Sub Function_PopulateRefundSequenceNo_RequirementActivate_NonNullRefundSequenceNo_ExpectNonNullValueReturned()

        Dim VendaRefundRequest As New WebService.CTSEnableRefundCreateRequest
        Dim VendaRefundReponse As New WebService.CTSEnableRefundCreateResponse
        Dim SH As New Qod.SaleHeader
        Dim Output As Nullable(Of Integer)

        'arrange
        ArrangeRequirementCheck(True)
        VendaRefundRequest = CType(VendaRefundRequest.Deserialise(VendaRefund.ToString), WebService.CTSEnableRefundCreateRequest)
        VendaRefundReponse.SetFieldsFromRequest(VendaRefundRequest)
        'act
        Output = SH.PopulateRefundSequenceNo(VendaRefundReponse)

        Assert.AreEqual(1, Output.Value)

    End Sub

#End Region

#Region "Unit Test - Persist"

    <TestMethod()> Public Sub Function_AddRefundSequenceNoSqlParameterObject_RequirementActivate_NullRefundSequenceNo_ExpectSqlParameterNotCreated()

        Dim SH As New Qod.SaleHeader
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(True)
        SH.AddRefundSequenceNoSqlParameterObject(CtsCmd, Nothing)

        Assert.AreEqual(0, CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Count)

    End Sub

    <TestMethod()> Public Sub Function_AddRefundSequenceNoSqlParameterObject_RequirementActivate_NonNullRefundSequenceNo_ExpectSqlParameterCreated()

        Dim SH As New Qod.SaleHeader
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(True)
        SH.AddRefundSequenceNoSqlParameterObject(CtsCmd, 10)

        Assert.AreEqual(1, CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Count)

    End Sub

    <TestMethod()> Public Sub Function_AddRefundSequenceNoSqlParameterObject_RequirementActivate_NonNullRefundSequenceNo_ExpectSqlParameterCreatedCalledRefundSequenceNo()

        Dim SH As New Qod.SaleHeader
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(True)
        SH.AddRefundSequenceNoSqlParameterObject(CtsCmd, 10)

        Assert.AreEqual("RefundSequenceNo", CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Item(0).ParameterName)

    End Sub

    <TestMethod()> Public Sub Function_AddRefundSequenceNoSqlParameterObject_RequirementDeactivate_NullRefundSequenceNo_ExpectSqlParameterNotCreated()

        Dim SH As New Qod.SaleHeader
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(False)
        SH.AddRefundSequenceNoSqlParameterObject(CtsCmd, Nothing)

        Assert.AreEqual(0, CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Count)

    End Sub

    <TestMethod()> Public Sub Function_AddRefundSequenceNoSqlParameterObject_RequirementDeactivate_NonNullRefundSequenceNo_ExpectSqlParameterNotCreated()

        Dim SH As New Qod.SaleHeader
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(False)
        SH.AddRefundSequenceNoSqlParameterObject(CtsCmd, 10)

        Assert.AreEqual(0, CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Count)

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementCheck(ByVal RequirementEnabled As Nullable(Of Boolean))

        Dim Stub As New RequirementRepositoryStub

        Stub.ConfigureStub(RequirementEnabled)
        RequirementRepositoryFactory.FactorySet(Stub)

    End Sub

#End Region

End Class