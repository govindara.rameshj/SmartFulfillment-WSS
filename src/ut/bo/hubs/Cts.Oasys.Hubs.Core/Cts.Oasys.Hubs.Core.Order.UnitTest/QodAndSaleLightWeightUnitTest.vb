﻿<TestClass()> Public Class QodAndSaleLightWeightUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Requirement Activated"

    <TestMethod()> Public Sub RequirementCheck_ParameterMissing_ReturnExistingBusinessObject()

        Dim V As IQodAndSaleLightWeight = Nothing

        ArrangeRequirementCheck(New Nullable(Of Boolean))
        ArrangeSaleHeaderLightWeight(V, Nothing)

        Assert.AreEqual("Cts.Oasys.Hubs.Core.Order.TpWickes.QodAndSaleLightWeight", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementCheck_ParameterFalse_ReturnExistingBusinessObject()

        Dim V As IQodAndSaleLightWeight = Nothing

        ArrangeRequirementCheck(False)
        ArrangeSaleHeaderLightWeight(V, Nothing)

        Assert.AreEqual("Cts.Oasys.Hubs.Core.Order.TpWickes.QodAndSaleLightWeight", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementECheck_ParameterTrue_ReturnNewBusinessObject()

        Dim V As IQodAndSaleLightWeight = Nothing

        ArrangeRequirementCheck(True)
        ArrangeSaleHeaderLightWeight(V, Nothing)

        Assert.AreEqual("Cts.Oasys.Hubs.Core.Order.TpWickes.QodAndSaleLightWeightNew", V.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - Sale Header Light Weight New"

    <TestMethod()> Public Sub New_RefundSequenceNo_NullRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing

        ArrangeRequirementCheck(True)
        ArrangeSaleHeaderLightWeight(V, Nothing)

        Assert.IsFalse(V.RefundSequenceNo.HasValue)

    End Sub

    <TestMethod()> Public Sub New_RefundSequenceNo_NonNullRefundSequenceNo_ExpectNonNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing

        ArrangeRequirementCheck(True)
        ArrangeSaleHeaderLightWeight(V, 10)

        Assert.AreEqual(10, V.RefundSequenceNo.Value)

    End Sub

    <TestMethod()> Public Sub New_AddSqlParameterObject_NullRefundSequenceNo_ExpectSqlParameterNotCreated()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(True)
        ArrangeSaleHeaderLightWeight(V, Nothing)
        AddSqlParameter(V, CtsCmd)

        Assert.AreEqual(0, CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Count)

    End Sub

    <TestMethod()> Public Sub New_AddSqlParameterObject_NonNullRefundSequenceNo_ExpectSqlParameterCreated()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(True)
        ArrangeSaleHeaderLightWeight(V, 10)
        AddSqlParameter(V, CtsCmd)

        Assert.AreEqual(1, CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Count)

    End Sub

    <TestMethod()> Public Sub New_AddSqlParameterObject_NonNullRefundSequenceNo_ExpectSqlParameterCreatedCalledRefundSequenceNo()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(True)
        ArrangeSaleHeaderLightWeight(V, 10)
        AddSqlParameter(V, CtsCmd)

        Assert.AreEqual("RefundSequenceNo", CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Item(0).ParameterName)

    End Sub

#End Region

#Region "Unit Test - Sale Header Light Weight Existing"

    <TestMethod()> Public Sub Existing_NulRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing

        ArrangeRequirementCheck(False)
        ArrangeSaleHeaderLightWeight(V, Nothing)

        Assert.IsFalse(V.RefundSequenceNo.HasValue)

    End Sub

    <TestMethod()> Public Sub Existing_NonNullRefundSequenceNo_ExpectNullValueReturned()

        Dim V As IQodAndSaleLightWeight = Nothing

        ArrangeRequirementCheck(False)
        ArrangeSaleHeaderLightWeight(V, 10)

        Assert.IsFalse(V.RefundSequenceNo.HasValue)

    End Sub

    <TestMethod()> Public Sub Existing_AddSqlParameterObject_NullRefundSequenceNo_ExpectSqlParameterNotCreated()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(False)
        ArrangeSaleHeaderLightWeight(V, Nothing)
        AddSqlParameter(V, CtsCmd)

        Assert.AreEqual(0, CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Count)

    End Sub

    <TestMethod()> Public Sub Existing_AddSqlParameterObject_NonNullRefundSequenceNo_ExpectSqlParameterNotCreated()

        Dim V As IQodAndSaleLightWeight = Nothing
        Dim CtsCmd As New Cts.Oasys.Hubs.Data.Command(New Cts.Oasys.Hubs.Data.Connection)

        ArrangeRequirementCheck(False)
        ArrangeSaleHeaderLightWeight(V, 10)
        AddSqlParameter(V, CtsCmd)

        Assert.AreEqual(0, CtsCmd.UnitTestExposeSqlCommandObject.Parameters.Count)

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementCheck(ByVal RequirementEnabled As Nullable(Of Boolean))

        Dim Stub As New RequirementRepositoryStub

        Stub.ConfigureStub(RequirementEnabled)
        RequirementRepositoryFactory.FactorySet(Stub)

    End Sub

    Private Sub ArrangeSaleHeaderLightWeight(ByRef V As IQodAndSaleLightWeight, ByVal Value As Nullable(Of Integer))

        QodAndSaleLightWeightFactory.FactorySet(Nothing)

        V = QodAndSaleLightWeightFactory.FactoryGet
        V.RefundSequenceNo = Value

    End Sub

    Private Sub AddSqlParameter(ByRef V As IQodAndSaleLightWeight, ByRef CtsCmd As Cts.Oasys.Hubs.Data.Command)

        V.AddRefundSequenceNoSqlParameterObject(CtsCmd)

    End Sub

#End Region

End Class