﻿Module GlobalStuff

#Region "Venda Refund"

    Friend VendaRefund As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                      <CTSEnableRefundCreateRequest>
                                          <DateTimeStamp>2011-08-05T07:03:28.469</DateTimeStamp>
                                          <RefundHeader>
                                              <Source>WO</Source>
                                              <SourceOrderNumber>111112</SourceOrderNumber>
                                              <StoreOrderNumber>017847</StoreOrderNumber>
                                              <RefundDate>2011-08-05T00:00:25.387</RefundDate>
                                              <RefundSeq>0001</RefundSeq>
                                              <LoyaltyCardNumber></LoyaltyCardNumber>
                                              <DeliveryChargeRefundValue>0.00</DeliveryChargeRefundValue>
                                              <RefundTotal>162.99</RefundTotal>
                                          </RefundHeader>
                                          <RefundLines>
                                              <RefundLine>
                                                  <StoreLineNo>1</StoreLineNo>
                                                  <ProductCode>190234</ProductCode>
                                                  <QuantityCancelled>1</QuantityCancelled>
                                                  <RefundLineValue>14.99</RefundLineValue>
                                              </RefundLine>
                                          </RefundLines>
                                      </CTSEnableRefundCreateRequest>

#End Region

End Module