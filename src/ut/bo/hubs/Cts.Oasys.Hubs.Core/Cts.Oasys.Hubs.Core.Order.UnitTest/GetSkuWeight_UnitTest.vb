﻿<TestClass()> Public Class UnitTestReferral922

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Scenario classes"

    Private Class FactoryGetSkuWeight_ParameterEnabledScenario
        Inherits FactoryGetSkuWeight

        Friend Overrides Function ParameterIsEnabled() As Boolean
            Return True
        End Function

    End Class

    Private Class FactoryGetSkuWeight_ParameterDisabledScenario
        Inherits FactoryGetSkuWeight

        Friend Overrides Function ParameterIsEnabled() As Boolean
            Return False
        End Function

    End Class

    Private Class GetSkuWeight123Point56Scenario
        Inherits GetSkuWeight

        Friend Overrides Function GetSkuWeightRepository(ByVal SkuNumber As String) As Decimal
            Return CType(123.56, Decimal)
        End Function

    End Class

#End Region

    <TestMethod()> Public Sub FactoryGetSkuWeight_RequirementOn_ReturnsGetSkuWeight()
        Dim F As New FactoryGetSkuWeight_ParameterEnabledScenario
        Dim X As IGetSkuWeight = F.FactoryGet

        Assert.AreEqual(True, IIf(X.GetType.FullName = "Cts.Oasys.Hubs.Core.Order.GetSkuWeight", True, False))
    End Sub

    <TestMethod()> Public Sub FactoryGetSkuWeight_RequirementOff_ReturnsGetSkuWeightReturnZero()
        Dim F As New FactoryGetSkuWeight_ParameterDisabledScenario
        Dim X As IGetSkuWeight = F.FactoryGet

        Assert.AreEqual(True, IIf(X.GetType.FullName = "Cts.Oasys.Hubs.Core.Order.GetSkuWeightReturnZero", True, False))
    End Sub

    <TestMethod()> Public Sub GetSkuWeight_InstantiateGetSkuWeight_ReturnsKnownWeight()
        Dim X As IGetSkuWeight = New GetSkuWeightReturnZero
        Assert.AreEqual(CDec(0), X.GetSkuWeight(""))
    End Sub

    <TestMethod()> Public Sub GetSkuWeight_InstantiateGetSkuWeightReturnsZero_ReturnsZero()
        Dim X As IGetSkuWeight = New GetSkuWeight123Point56Scenario
        Assert.AreEqual(CDec(123.56), X.GetSkuWeight(""))
    End Sub

End Class
