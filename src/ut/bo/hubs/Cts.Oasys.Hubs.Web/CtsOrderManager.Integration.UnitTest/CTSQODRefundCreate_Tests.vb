﻿<TestClass()> Public Class CTSQODRefundCreate_Tests

    Private testContextInstance As TestContext

    Private Const NonExistentOrderNumber As String = "1"
    Private Const CtsqodRefundCreateRequestXml As String = "<CTSRefundCreateRequest><DateTimeStamp>2011-08-09T00:02:50.590</DateTimeStamp><RefundHeader><Source>WO</Source><SourceOrderNumber>726982</SourceOrderNumber><StoreOrderNumber>012711</StoreOrderNumber><RefundDate>2011-08-08T00:00:49.778</RefundDate><RefundSeq>0001</RefundSeq><LoyaltyCardNumber></LoyaltyCardNumber><DeliveryChargeRefundValue>0</DeliveryChargeRefundValue><RefundTotal>1.96</RefundTotal></RefundHeader><RefundLines><RefundLine><StoreLineNo>4</StoreLineNo><ProductCode>153089</ProductCode><QuantityCancelled>4</QuantityCancelled><RefundLineValue>1.96</RefundLineValue></RefundLine></RefundLines></CTSRefundCreateRequest>"
    Private Const CtsRefundCreateOneLineRequestXml As String = "<CTSRefundCreateRequest><DateTimeStamp>2011-08-11T07:03:28.469</DateTimeStamp><RefundHeader><Source>WO</Source><SourceOrderNumber>726982</SourceOrderNumber><StoreOrderNumber>012711</StoreOrderNumber><RefundDate>2011-08-11T00:00:25.387</RefundDate><RefundSeq>0001</RefundSeq><LoyaltyCardNumber></LoyaltyCardNumber><DeliveryChargeRefundValue>0.00</DeliveryChargeRefundValue><RefundTotal>2.21</RefundTotal></RefundHeader><RefundLines><RefundLine><StoreLineNo>3</StoreLineNo><ProductCode>107177</ProductCode><QuantityCancelled>1</QuantityCancelled><RefundLineValue>2.21</RefundLineValue></RefundLine></RefundLines></CTSRefundCreateRequest>"
    Private Const CtsqodRefundCreateRequestComplexOrderXml As String = ""
    Private Const ExistingOrderNumber As Long = 123333
    Private Const ExistingStoreorderNumber As Long = 14014

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"

    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    End Sub

    <TestInitialize()> Public Sub MyTestInitialize()
    End Sub

    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    <ClassCleanup()> Public Shared Sub MyClassCleanup()
    End Sub

#End Region

    'Olga Shaforost - Moved to Fitnesse
    <Ignore()>
    <TestMethod()> Public Sub CTSQODRefundCreate_ProductCode_DoesNotExist()

        ' Arrange
        Dim requestXML As String = _
            CtsqodRefundCreateRequestXml.Replace("<ProductCode>240693</ProductCode>", _
                                                         "<ProductCode>" & NonExistentOrderNumber & "</ProductCode>")
        Dim xmlDoc As New XmlDocument

        Dim chk As String

        Dim ws As New Cts.Oasys.Hubs.Webservices.CtsOrderManager

        Dim log As New Cts.Oasys.Hubs.Webservices.Log
        With log
            .Header = "C:\temp\"
        End With

        Dim dtbase As New Cts.Oasys.Hubs.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
        Cts.Oasys.Hubs.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dtbase)

        Cts.Oasys.Hubs.Webservices.LogFactory.FactorySet(log)

        '  Act

        Dim str As String = ws.CTS_Enable_Refund_Create(requestXML)
        xmlDoc.LoadXml(str)
        chk = xmlDoc.DocumentElement.ChildNodes(1).ChildNodes(0).InnerText


        ' Assert
        Assert.IsTrue(chk.Equals("false"))

    End Sub

End Class