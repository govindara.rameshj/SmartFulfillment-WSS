﻿<TestClass()> Public Class CTSQODCreate_Tests

    Private testContextInstance As TestContext

    Private Const FileSystemLocal As String = "c:\"
    Private Const NonExistentOrderNumber As String = "1"
    Private Const CtsqodCreateRequestXml As String = "<CTSQODCreateRequest><DateTimeStamp>2011-08-05T00:29:47.618</DateTimeStamp><OrderHeader><Source>HY</Source><SourceOrderNumber>726378</SourceOrderNumber>    <RequiredDeliveryDate>2011-08-08</RequiredDeliveryDate><DeliveryCharge>0</DeliveryCharge><TotalOrderValue>333.02</TotalOrderValue><SaleDate>2011-08-09</SaleDate><CustomerAccountNo>2572315</CustomerAccountNo><CustomerName>Mr Tomasz Frontczak</CustomerName> <CustomerAddressLine1>44 Gibbins Road</CustomerAddressLine1><CustomerAddressLine2></CustomerAddressLine2><CustomerAddressTown>London</CustomerAddressTown><CustomerAddressLine4></CustomerAddressLine4><CustomerPostcode>E15 2HU</CustomerPostcode><DeliveryAddressLine1>110 Westbury Avenue</DeliveryAddressLine1><DeliveryAddressLine2></DeliveryAddressLine2><DeliveryAddressTown>LONDON</DeliveryAddressTown><DeliveryAddressLine4></DeliveryAddressLine4><DeliveryPostcode>N22 6RT</DeliveryPostcode><ContactPhoneHome>078 77497598</ContactPhoneHome><ContactPhoneMobile></ContactPhoneMobile><ContactPhoneWork></ContactPhoneWork><ContactEmail>tomaszfrontczak@hotmail.com</ContactEmail><DeliveryInstructions></DeliveryInstructions><ToBeDelivered>true</ToBeDelivered><CreateDeliveryChargeItem>false</CreateDeliveryChargeItem><LoyaltyCardNumber></LoyaltyCardNumber><ExtendedLeadTime>false</ExtendedLeadTime><DeliveryContactName>Mr Tomasz Frontczak</DeliveryContactName><DeliveryContactPhone>078 77497598</DeliveryContactPhone><RecordSaleOnly>false</RecordSaleOnly></OrderHeader><OrderLines><OrderLine><SourceOrderLineNo>16</SourceOrderLineNo><ProductCode>240693</ProductCode><ProductDescription>Decorators Caulk White 310ml</ProductDescription><TotalOrderQuantity>4</TotalOrderQuantity><LineValue>7.12</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>1.78</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine></OrderLines></CTSQODCreateRequest>"
    Private Const CtsqodCreateRequestComplexOrderXml As String = "<CTSQODCreateRequest><DateTimeStamp>2011-08-09T01:03:11.887</DateTimeStamp> <OrderHeader><Source>HY</Source><SourceOrderNumber>729551</SourceOrderNumber><RequiredDeliveryDate>2011-08-10</RequiredDeliveryDate><DeliveryCharge>0</DeliveryCharge><TotalOrderValue>325.31</TotalOrderValue><SaleDate>2011-08-09</SaleDate><CustomerAccountNo>203918</CustomerAccountNo><CustomerName>Happy Holidays</CustomerName><CustomerAddressLine1>48 Duke Road</CustomerAddressLine1><CustomerAddressLine2></CustomerAddressLine2><CustomerAddressTown>London</CustomerAddressTown><CustomerAddressLine4></CustomerAddressLine4><CustomerPostcode>W4 2DE</CustomerPostcode><DeliveryAddressLine1>48 Duke Road</DeliveryAddressLine1><DeliveryAddressLine2></DeliveryAddressLine2><DeliveryAddressTown>London</DeliveryAddressTown><DeliveryAddressLine4></DeliveryAddressLine4><DeliveryPostcode>W4 2DE</DeliveryPostcode><ContactPhoneHome>07855 323723</ContactPhoneHome><ContactPhoneMobile></ContactPhoneMobile><ContactPhoneWork></ContactPhoneWork><ContactEmail>sortitout20@hotmail.com</ContactEmail><DeliveryInstructions></DeliveryInstructions><ToBeDelivered>true</ToBeDelivered><CreateDeliveryChargeItem>false</CreateDeliveryChargeItem><LoyaltyCardNumber></LoyaltyCardNumber><ExtendedLeadTime>false</ExtendedLeadTime><DeliveryContactName>Mr Nick Wallsworth</DeliveryContactName><DeliveryContactPhone>07855 323723</DeliveryContactPhone><RecordSaleOnly>false</RecordSaleOnly></OrderHeader><OrderLines><OrderLine><SourceOrderLineNo>1</SourceOrderLineNo><ProductCode>164516</ProductCode><ProductDescription>Grade P5 Chipboard MR Flr 2400x600x18mm</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity><LineValue>7.68</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>7.68</SellingPrice> <RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>2</SourceOrderLineNo><ProductCode>107136</ProductCode><ProductDescription>Sawn Treated 25x38mmx2.4m PK8</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity><LineValue>17.85</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>17.85</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>3</SourceOrderLineNo><ProductCode>107177</ProductCode><ProductDescription>Studwork Timber 38x63mmx2.4m</ProductDescription><TotalOrderQuantity>25</TotalOrderQuantity><LineValue>55.25</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>2.21</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>4</SourceOrderLineNo><ProductCode>153089</ProductCode><ProductDescription>Multi-Purpose Tarpaulin 2x3m Approx</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity> <LineValue>5.49</LineValue><DeliveryChargeItem>false</DeliveryChargeItem> <SellingPrice>5.49</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>5</SourceOrderLineNo><ProductCode>214491</ProductCode><ProductDescription>Easy Drive Screws 4.0 x 30mm Pk 1000</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity><LineValue>16.5</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>16.5</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>6</SourceOrderLineNo><ProductCode>163653</ProductCode><ProductDescription>Plasterboard Square Edge 1800x900x12.5mm</ProductDescription><TotalOrderQuantity>5</TotalOrderQuantity><LineValue>19.25</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>3.85</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>7</SourceOrderLineNo><ProductCode>190544</ProductCode><ProductDescription>Celotex 50mm Board 2400x1200mm</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity><LineValue>27.98</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>27.98</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>8</SourceOrderLineNo><ProductCode>107542</ProductCode><ProductDescription>Kiln Dried Sawn Softwood 47x100mmx3.0m</ProductDescription><TotalOrderQuantity>14</TotalOrderQuantity><LineValue>92.26</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>6.59</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>9</SourceOrderLineNo><ProductCode>110111</ProductCode><ProductDescription>Exterior Plywood 2440x1220x9mm</ProductDescription><TotalOrderQuantity>5</TotalOrderQuantity><LineValue>80.5</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>16.1</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>10</SourceOrderLineNo><ProductCode>510990</ProductCode><ProductDescription>Drywall Screws 38mm PK100</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity><LineValue>2.55</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>2.55</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine></OrderLines></CTSQODCreateRequest>"
    Private Const CtsqodCreateRequestFiveLineOrderXml As String = "<CTSQODCreateRequest><DateTimeStamp>2011-08-09T01:03:11.887</DateTimeStamp> <OrderHeader><Source>HY</Source><SourceOrderNumber>729551</SourceOrderNumber><RequiredDeliveryDate>2011-08-10</RequiredDeliveryDate><DeliveryCharge>0</DeliveryCharge><TotalOrderValue>325.31</TotalOrderValue><SaleDate>2011-08-09</SaleDate><CustomerAccountNo>203918</CustomerAccountNo><CustomerName>Mr Five Liner</CustomerName><CustomerAddressLine1>48 Duke Road</CustomerAddressLine1><CustomerAddressLine2></CustomerAddressLine2><CustomerAddressTown>London</CustomerAddressTown><CustomerAddressLine4></CustomerAddressLine4><CustomerPostcode>W4 2DE</CustomerPostcode><DeliveryAddressLine1>48 Duke Road</DeliveryAddressLine1><DeliveryAddressLine2></DeliveryAddressLine2><DeliveryAddressTown>London</DeliveryAddressTown><DeliveryAddressLine4></DeliveryAddressLine4><DeliveryPostcode>W4 2DE</DeliveryPostcode><ContactPhoneHome>07855 323723</ContactPhoneHome><ContactPhoneMobile></ContactPhoneMobile><ContactPhoneWork></ContactPhoneWork><ContactEmail>sortitout20@hotmail.com</ContactEmail><DeliveryInstructions></DeliveryInstructions><ToBeDelivered>true</ToBeDelivered><CreateDeliveryChargeItem>false</CreateDeliveryChargeItem><LoyaltyCardNumber></LoyaltyCardNumber><ExtendedLeadTime>false</ExtendedLeadTime><DeliveryContactName>Mr Nick Wallsworth</DeliveryContactName><DeliveryContactPhone>07855 323723</DeliveryContactPhone><RecordSaleOnly>false</RecordSaleOnly></OrderHeader><OrderLines><OrderLine><SourceOrderLineNo>1</SourceOrderLineNo><ProductCode>164516</ProductCode><ProductDescription>Grade P5 Chipboard MR Flr 2400x600x18mm</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity><LineValue>7.68</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>7.68</SellingPrice> <RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>2</SourceOrderLineNo><ProductCode>107136</ProductCode><ProductDescription>Sawn Treated 25x38mmx2.4m PK8</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity><LineValue>17.85</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>17.85</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>3</SourceOrderLineNo><ProductCode>107177</ProductCode><ProductDescription>Studwork Timber 38x63mmx2.4m</ProductDescription><TotalOrderQuantity>25</TotalOrderQuantity><LineValue>55.25</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>2.21</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>4</SourceOrderLineNo><ProductCode>153089</ProductCode><ProductDescription>Multi-Purpose Tarpaulin 2x3m Approx</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity> <LineValue>5.49</LineValue><DeliveryChargeItem>false</DeliveryChargeItem> <SellingPrice>5.49</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine><OrderLine><SourceOrderLineNo>5</SourceOrderLineNo><ProductCode>214491</ProductCode><ProductDescription>Easy Drive Screws 4.0 x 30mm Pk 1000</ProductDescription><TotalOrderQuantity>1</TotalOrderQuantity><LineValue>16.5</LineValue><DeliveryChargeItem>false</DeliveryChargeItem><SellingPrice>16.5</SellingPrice><RecordSaleOnly>false</RecordSaleOnly></OrderLine></OrderLines></CTSQODCreateRequest>"
    Private Const MaximumRequests As Integer = 10

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"

    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    End Sub

    <TestInitialize()> Public Sub MyTestInitialize()
    End Sub

    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    <ClassCleanup()> Public Shared Sub MyClassCleanup()
    End Sub

#End Region

    'Olga Shaforost: Moved to Fitnesse
    <Ignore()>
    <TestMethod()> Public Sub CTSQODCreate_StoredProcedure_CashBalUpdateDataCheckOK()

        ' Arrange
        Trace.WriteLine("Started: " & Now.ToString)
        Dim ramCounter As PerformanceCounter
        Dim requestXML As String
        ramCounter = New PerformanceCounter("Memory", "Available MBytes")
        Dim CashbalCashval As DataTable
        Dim CashbalCashTenVal As DataTable
        Dim GrossAmnt As Decimal
        Dim Quantity As Integer
        Dim Amount As Decimal
        Dim SaleAmnt As Decimal
        Dim SaleCount As Integer
        Dim NumnTran As Integer
        Dim FailedUpdate As Boolean
        Dim FailedUpdateTen As Boolean
        Dim NonExisting As Boolean
        Dim NonExistingTen As Boolean
        Dim totValue As String
        Dim xmlDoc As New XmlDocument
        'Dim GrossAmnt As Integer


        Dim initialMemoryFree As Single = 0, finalMemoryFree As Single = 0
        initialMemoryFree = ramCounter.NextValue

        ' configure log for test
        Dim log As New Cts.Oasys.Hubs.Webservices.LogTest(FileSystemLocal)
        With log
            .Header = "CTSQODCreate_StoredProcedure_CashBalUpdateDataCheckOK"
        End With

        Dim dbase As New Cts.Oasys.Hubs.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
        Cts.Oasys.Hubs.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dbase)
        Cts.Oasys.Hubs.Webservices.LogFactory.FactorySet(log)

        xmlDoc.LoadXml(CtsqodCreateRequestFiveLineOrderXml)
        totValue = xmlDoc.DocumentElement.ChildNodes(1).ChildNodes(4).InnerText

        Dim NewWebOrderNumber As Long = 0
        Dim ws As New Cts.Oasys.Hubs.Webservices.CtsOrderManager
        Using con As New Connection
            Using com As New Command(con)
                com.CommandText = "Select MAX(Cast(SourceOrderNumber as int)) from corhdr5"
                NewWebOrderNumber = CInt(com.ExecuteValue)
                com.CommandText = "select * from CashBalCashier where PeriodID in (select distinct(id) from SystemPeriods where StartDate = '" & Format(DateTime.Today, "yyyy-MM-dd") & "') and CashierID = '800'"
                CashbalCashval = com.ExecuteDataTable
                com.CommandText = "select * from CashBalCashierTen where PeriodID in (select distinct(id) from SystemPeriods where StartDate = '" & Format(DateTime.Today, "yyyy-MM-dd") & "') and CashierID = '800'"
                CashbalCashTenVal = com.ExecuteDataTable
            End Using
        End Using
        If (CashbalCashval.Rows.Count > 0) Then
            GrossAmnt = CDec(CashbalCashval.Rows(0).Item("GrossSalesAmount"))
            SaleCount = CInt(CashbalCashval.Rows(0).Item("SalesCount"))
            SaleAmnt = CDec(CashbalCashval.Rows(0).Item("SalesAmount"))
            NumnTran = CInt(CashbalCashval.Rows(0).Item("NumTransactions"))
        Else
            NonExisting = True
        End If


        If (CashbalCashTenVal.Rows.Count > 0) Then
            Quantity = CInt(CashbalCashTenVal.Rows(0).Item("Quantity"))
            Amount = CDec(CashbalCashTenVal.Rows(0).Item("Amount"))
        Else
            NonExistingTen = True
        End If



        ' Act
        NewWebOrderNumber += 1
        requestXML = CtsqodCreateRequestFiveLineOrderXml _
            .Replace("<SourceOrderNumber>729551</SourceOrderNumber>", "<SourceOrderNumber>" & NewWebOrderNumber.ToString & "</SourceOrderNumber>") _
            .Replace("<SaleDate>2011-08-09</SaleDate>", "<SaleDate>" & Format(DateTime.Today, "yyyy-MM-dd") & "</SaleDate>")
        ws.CTS_QOD_Create(requestXML)

        CashbalCashval.Clear()
        CashbalCashTenVal.Clear()
        Using con As New Connection
            Using com As New Command(con)
                com.CommandText = "select * from CashBalCashier where PeriodID in (select distinct(id) from SystemPeriods where StartDate = '" & Format(DateTime.Today, "yyyy-MM-dd") & "') and CashierID = '800'"
                NewWebOrderNumber = CInt(com.ExecuteValue)
                CashbalCashval = com.ExecuteDataTable
                com.CommandText = "select * from CashBalCashierTen where PeriodID in (select distinct(id) from SystemPeriods where StartDate = '" & Format(DateTime.Today, "yyyy-MM-dd") & "') and CashierID = '800'"
                CashbalCashTenVal = com.ExecuteDataTable
            End Using
        End Using

        If (CashbalCashval.Rows.Count > 0) Then
            If NonExisting And ((CDec(CashbalCashval.Rows(0).Item("GrossSalesAmount")).Equals(totValue)) _
                                And (CDec(CashbalCashval.Rows(0).Item("SalesCount")).Equals(1)) _
                                And (CDec(CashbalCashval.Rows(0).Item("SalesAmount")).Equals(totValue)) _
                                And (CDec(CashbalCashval.Rows(0).Item("NumTransactions")).Equals(1))) Then
                FailedUpdate = False
            ElseIf (CDec(CashbalCashval.Rows(0).Item("GrossSalesAmount")) - GrossAmnt).Equals(CDec(totValue)) And _
            (CDec(CashbalCashval.Rows(0).Item("SalesCount")) - SaleCount).Equals(1) And _
            (CDec(CashbalCashval.Rows(0).Item("SalesAmount")) - SaleAmnt).Equals(CDec(totValue)) And _
            (CDec(CashbalCashval.Rows(0).Item("NumTransactions")) - NumnTran).Equals(1) Then
                FailedUpdate = False
            End If
        Else
            FailedUpdate = True
        End If


        If (CashbalCashTenVal.Rows.Count > 0) Then
            If NonExistingTen And ((CDec(CashbalCashTenVal.Rows(0).Item("Amount")).Equals(CDec(totValue))) _
                                And (CDec(CashbalCashTenVal.Rows(0).Item("Quantity")).Equals(1))) Then
                FailedUpdateTen = False

            ElseIf (CDec(CashbalCashTenVal.Rows(0).Item("Amount")) - GrossAmnt).Equals(CDec(totValue)) And _
            (CDec(CashbalCashTenVal.Rows(0).Item("Quantity")) - SaleCount).Equals(1) Then
                FailedUpdateTen = False
            End If
        Else
            FailedUpdateTen = True
        End If

        If FailedUpdate = False And FailedUpdateTen = False Then
            FailedUpdate = False
        End If
        ' Assert
        Assert.IsFalse(FailedUpdate)


    End Sub

    'Olga Shaforost: Moved to Fitnesse
    <Ignore()>
    <TestMethod()> Public Sub CTSQODCreate_ProductCode_DoesNotExist()

        ' Arrange
        Dim requestXML As String = _
            CtsqodCreateRequestXml.Replace("<ProductCode>240693</ProductCode>", _
                                                         "<ProductCode>" & NonExistentOrderNumber & "</ProductCode>")
        Dim xmlDoc As New XmlDocument

        Dim chk As String

        Dim ws As New Cts.Oasys.Hubs.Webservices.CtsOrderManager

        Dim log As New Cts.Oasys.Hubs.Webservices.LogTest(FileSystemLocal)
        With log
            .Header = "CTSQODCreate_ProductCode_DoesNotExist"
        End With

        Dim dtbase As New Cts.Oasys.Hubs.Webservices.TpWickes.RequestDecisionEngineDatabaseSQLTest
        Cts.Oasys.Hubs.Webservices.TpWickes.RequestDecisionEngineDatabaseFactory.SetRequestDecisionEngineDatabase(dtbase)

        Cts.Oasys.Hubs.Webservices.LogFactory.FactorySet(log)

        '  Act

        Dim str As String = ws.CTS_QOD_Create(requestXML)
        xmlDoc.LoadXml(str)
        chk = xmlDoc.DocumentElement.ChildNodes(1).ChildNodes(0).InnerText


        ' Assert
        Assert.IsTrue(chk.Equals("false"))

    End Sub

End Class