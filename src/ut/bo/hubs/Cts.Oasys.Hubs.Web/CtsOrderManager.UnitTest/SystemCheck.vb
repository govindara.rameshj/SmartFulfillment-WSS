﻿<TestClass()> Public Class SystemCheck

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Database Permission Check"

#End Region

    <TestMethod()> Public Sub DatabasePermissionCheck_EmptyDataSet()

        Dim DT As DataTable

        'arrange
        DT = New DataTable
        DT.Columns.Add("Schema Name")
        DT.Columns.Add("Type")
        DT.Columns.Add("Name")
        DT.Columns.Add("Permission")
        DT.Columns.Add("Access Rights")
        DT.Columns.Add("Assigned To")
        DT.Columns.Add("Assigned By")

        'stub version
        Dim Stub = New SystemCheckDatabaseStubVersion
        Stub.CheckStubVersion = DT

        SystemCheckDatabaseFactory.FactorySet(Stub)     'use stub version


        'create instant of webpage
        'call "database permission check" button event
        'code will use stub instead of making a call to the database

    End Sub

    <TestMethod()> Public Sub DatabasePermissionCheck_ThrowError()

    End Sub

#Region "Log File Check"

    <TestMethod()> Public Sub LogFileCheck_ThrowError()

    End Sub

    <TestMethod()> Public Sub LogFileCheck_FolderExist_True()

    End Sub

    <TestMethod()> Public Sub LogFileCheck_FolderExist_False()

    End Sub

    <TestMethod()> Public Sub LogFileCheck_CreateNewFile_True()

    End Sub

    <TestMethod()> Public Sub LogFileCheck_CreateNewFile_False()

    End Sub

    <TestMethod()> Public Sub LogFileCheck_WriteNewFile_True()

    End Sub

    <TestMethod()> Public Sub LogFileCheck_WriteNewFile_False()

    End Sub

    <TestMethod()> Public Sub LogFileCheck_DeleteFile_True()

    End Sub

    <TestMethod()> Public Sub LogFileCheck_DeleteFile_False()

    End Sub

#End Region

End Class