﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Tests
Imports Cts.Oasys.Hubs.Core.System.EndOfDayLock

<TestClass()> Public Class RequestDecisionEngineTests

#Region "Complex Scenerios"

    <TestMethod()> Public Sub AcceptRequest_ComplexScenerio()
        MakeRequestAndAssertStatusResult(NowPlusMinutes(5), True)
    End Sub

#End Region

#Region "Current Day"

    <TestMethod()> Public Sub AcceptRequest_SubmittedDuring_DatabaseMinimisedMode_ReturnsTrue()
        'nitmas pending: 5 minutes from now
        MakeRequestAndAssertStatusResult(NowPlusMinutes(5), True)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_SubmittedAt_PendingMode_ReturnsTrue()
        'nitmas pending: Now
        MakeRequestAndAssertStatusResult(Now(), True)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_SubmittedDuring_PendingMode_ReturnsTrue()
        'nitmas pending: 5 minutes ago
        MakeRequestAndAssertStatusResult(NowPlusMinutes(-5), True)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_SubmittedDuring_RunningMode_ReturnsFalse()
        'nitmas pending: 5 minutes ago, nitmas started
        MakeRequestAndAssertStatusResult(
            NowPlusMinutes(-5),
            False,
            Sub(setup) setup.IsNitmasStarted = True)
    End Sub

#End Region

#Region "Next Day"

    <TestMethod()> Public Sub AcceptRequest_SubmittedDuring_DatabaseMinimisedMode_OnFollowingDay_ReturnsTrue()
        'nitmas pending: 5 minutes from now
        MakeRequestAndAssertStatusResultForNextDay(NowPlusMinutes(5), True)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_SubmittedAt_PendingMode_OnFollowingDay_ReturnsTrue()
        'nitmas pending: now
        MakeRequestAndAssertStatusResultForNextDay(Tomorrow(), True)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_SubmittedDuring_PendingMode_OnFollowingDay_ReturnsTrue()
        MakeRequestAndAssertStatusResultForNextDay(NowPlusMinutes(-5), True)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_SubmittedDuring_RunningMode_OnFollowingDay_ReturnsFalse()
        MakeRequestAndAssertStatusResultForNextDay(NowPlusMinutes(-5), False, Sub(setup) setup.IsNitmasStarted = True)
    End Sub

#End Region

#Region "State Change Testing"

#Region "Initial State - MinimisedMode"

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_MinimisedMode_To_PendingMode_PendingDateTimeNotChanged()
        'nitmas pending: 5 minutes from now
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(5),
            True,
            Sub(setup) setup.StubNow = NowPlusMinutes(10))
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_MinimisedMode_To_RunningMode_PendingDateTimeNotChanged()
        'nitmas pending: 5 minutes from now
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(5),
            True,
            Sub(setup) setup.StubNow = NowPlusMinutes(10))
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_MinimisedMode_To_MinimisedMode_FollowingDay_PendingDateTimeChanged()
        'nitmas pending: 10 minutes from now
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(10),
            False,
            Sub(setup) setup.StubNow = Tomorrow())
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_MinimisedMode_To_PendingMode_FollowingDay_PendingDateTimeChanged()
        'nitmas pending: 5 minutes from now
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(10),
            False,
            Sub(setup)
                setup.StubNow = TomorrowPlusMinutes(10)
            End Sub)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_MinimisedMode_To_RunningMode_FollowingDay_PendingDateTimeChanged()
        'nitmas pending: 5 minutes from now
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(5),
            False,
            Sub(setup)
                setup.StubNow = TomorrowPlusMinutes(10)
                setup.IsNitmasStarted = True
            End Sub)
    End Sub

#End Region

#Region "Initial State - PendingMode"

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_PendingMode_To_RunningMode_PendingDateTimeNotChanged()
        'nitmas pending: 5 minutes ago
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(-5),
            True,
            Sub(setup)
                setup.IsNitmasStarted = True
            End Sub)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_PendingMode_To_MinimisedMode_PendingDateTimeChanged()
        'nitmas pending: 5 minutes ago
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(-5),
            False,
            Sub(setup)
                setup.IsNitmasStarted = True
                setup.IsNitmasStopped = True
            End Sub)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_PendingMode_To_PendingMode_FollowingDay_PendingDateTimeChanged()
        'nitmas pending: 5 minutes ago
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(-5),
            False,
            Sub(setup)
                setup.StubNow = TomorrowPlusMinutes(-10)
            End Sub)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_PendingMode_To_RunningMode_FollowingDay_PendingDateTimeChanged()
        'nitmas pending: 5 minutes ago
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(-5),
            False,
            Sub(setup)
                setup.StubNow = Tomorrow()
                setup.IsNitmasStarted = True
            End Sub)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_PendingMode_To_MinimisedMode_FollowingDay_PendingDateTimeChanged()
        'nitmas pending: 5 minutes ago
        MakeTwoRequestsAndCompareNitmasPending(
            NowPlusMinutes(-5),
            False,
            Sub(setup)
                setup.StubNow = TomorrowPlusMinutes(-10)
            End Sub)
    End Sub

#End Region

#Region "Initial State - RunningMode"

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_RunningMode_To_MinimisedMode_PendingDateTimeChanged()
        MakeTwoRequestsWhileNitmasRunningAndCompareNitmasPending(
            NowPlusMinutes(-5),
            False,
            Sub(setup) setup.IsNitmasStopped = True)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_RunningMode_To_RunningMode_FollowingDay_PendingDateTimeChanged()
        MakeTwoRequestsWhileNitmasRunningAndCompareNitmasPending(
            NowPlusMinutes(-5),
            False,
            Sub(setup)
                setup.StubNow = Tomorrow()
            End Sub)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_RunningMode_To_MinimisedMode_FollowingDay_PendingDateTimeChanged()
        MakeTwoRequestsWhileNitmasRunningAndCompareNitmasPending(
            NowPlusMinutes(-5),
            False,
            Sub(setup)
                setup.StubNow = Tomorrow()
                setup.IsNitmasStopped = True
            End Sub)
    End Sub

    <TestMethod()> Public Sub AcceptRequest_StateMovesFrom_RunningMode_To_PendingMode_FollowingDay_PendingDateTimeChanged()
        MakeTwoRequestsWhileNitmasRunningAndCompareNitmasPending(
            NowPlusMinutes(-5),
            False,
            Sub(setup)
                setup.StubNow = Tomorrow()
                setup.IsNitmasStopped = True
            End Sub)
    End Sub

#End Region

#End Region

#Region "Private Test Procedures And Functions"

    Private Shared Function NowPlusMinutes(minutes As Integer) As DateTime
        Return DateAdd(DateInterval.Minute, minutes, Now())
    End Function

    Private Shared Function Tomorrow() As DateTime
        Return DateAdd(DateInterval.Day, 1, Now())
    End Function

    Private Shared Function TomorrowPlusMinutes(minutes As Integer) As DateTime
        Return DateAdd(DateInterval.Minute, minutes, Tomorrow())
    End Function

    Private Sub ConfigureDatabaseStub(ByRef oStubDb As StubRequestDecisionEngineDatabase, ByVal PendingTime As DateTime, _
                                      ByVal NitmasStarted As Boolean, ByVal NitmasStopped As Boolean)

        oStubDb = New StubRequestDecisionEngineDatabase
        With oStubDb
            .StubPendingTime = PendingTime
            .StubStarted = NitmasStarted
            .StubStopped = NitmasStopped
        End With

    End Sub

    Class RequestDecisionEngineSetup
        Public PendingTime As Date
        Public IsNitmasStarted As Boolean = False
        Public IsNitmasStopped As Boolean = False
        Public StubNow As Date = Now()
    End Class


    Private Function CreateRequestDecisionEngine(setup As RequestDecisionEngineSetup) As RequestDecisionEngine
        Dim oStubDb As StubRequestDecisionEngineDatabase = Nothing 'declare stub version of database interface to allow properties to be set
        ConfigureDatabaseStub(oStubDb, setup.PendingTime, setup.IsNitmasStarted, setup.IsNitmasStopped)     'nitmas pending: 5 minutes from now

        Dim testEnvironment = CType(GlobalVars.SystemEnvironment, TestSystemEnvironment)
        testEnvironment.GetDateTimeNowMethod = Function() setup.StubNow

        Return New RequestDecisionEngine(oStubDb, testEnvironment)                                       'initialise, will use stub run-time environment
    End Function

    Private Sub MakeRequestAndAssertStatusResult(pendingTime As DateTime, expected As Boolean, Optional changeDefaultSetup As Action(Of RequestDecisionEngineSetup) = Nothing)

        Dim setup As New RequestDecisionEngineSetup
        setup.PendingTime = pendingTime

        If (Not changeDefaultSetup Is Nothing) Then
            changeDefaultSetup(setup)
        End If

        Dim request = CreateRequestDecisionEngine(setup)
        Dim actual = request.AcceptRequest()

        Assert.AreEqual(expected, actual)
    End Sub

    Private Sub MakeRequestAndAssertStatusResultForNextDay(pendingTime As DateTime, expected As Boolean, Optional changeDefaultSetup As Action(Of RequestDecisionEngineSetup) = Nothing)
        MakeRequestAndAssertStatusResult(
            pendingTime,
            expected,
            Sub(setup)
                If (Not changeDefaultSetup Is Nothing) Then
                    changeDefaultSetup(setup)
                End If
                setup.StubNow = DateAdd(DateInterval.Day, 1, Now)
            End Sub
        )
    End Sub

    Private Sub MakeTwoRequestsAndCompareNitmasPendingWithPreparedSetup(ByVal expectedEquality As Boolean, ByVal changeSecondRequestSetup As Action(Of RequestDecisionEngineSetup), ByVal setup As RequestDecisionEngineSetup)
        Dim request1 = CreateRequestDecisionEngine(setup)
        request1.AcceptRequest()
        Dim originalPendingDateTime = request1.NitmasPending

        changeSecondRequestSetup(setup)
        Dim request2 = CreateRequestDecisionEngine(setup)
        request2.AcceptRequest()
        Dim CurrentPendingDateTime = request2.NitmasPending

        If (expectedEquality) Then
            Assert.AreEqual(originalPendingDateTime, CurrentPendingDateTime)
        Else
            Assert.AreNotEqual(originalPendingDateTime, CurrentPendingDateTime)
        End If
    End Sub

    Private Sub MakeTwoRequestsAndCompareNitmasPending(pendingTime As DateTime, expectedEquality As Boolean, Optional changeSecondRequestSetup As Action(Of RequestDecisionEngineSetup) = Nothing)
        Dim setup As New RequestDecisionEngineSetup
        setup.PendingTime = pendingTime

        MakeTwoRequestsAndCompareNitmasPendingWithPreparedSetup(expectedEquality, changeSecondRequestSetup, setup)
    End Sub

    Private Sub MakeTwoRequestsWhileNitmasRunningAndCompareNitmasPending(pendingTime As DateTime, expectedEquality As Boolean, Optional changeSecondRequestSetup As Action(Of RequestDecisionEngineSetup) = Nothing)
        Dim setup As New RequestDecisionEngineSetup
        setup.PendingTime = pendingTime
        setup.IsNitmasStarted = True

        MakeTwoRequestsAndCompareNitmasPendingWithPreparedSetup(expectedEquality, changeSecondRequestSetup, setup)
    End Sub


#End Region

End Class