﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports NSubstitute
Imports TpWickes.Hubs.Library
Imports Cts.Oasys.Hubs.Core.Order.Qod
Imports Cts.Oasys.Hubs.Core.Order.WebService
Imports Cts.Oasys.Hubs.Core.Order

<TestClass()> Public Class XMLToQODTextsConvertor_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region
#Region "Requirement Switch Tests"

    <TestMethod()> Public Sub GetImplementation_RequirementsSwitchOn_Returns_XMLToQodTextsConvertor()
        Dim V As IXMLToQodTextsConvertor

        ArrangeRequirementSwitchDependency(True)
        V = (New XMLToQodTextsConvertorFactory).GetImplementation
        Assert.AreEqual("Cts.Oasys.Hubs.Webservices.XMLToQodTextsConvertor", V.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub GetImplementation_RequirementsSwitchOff_Returns_XMLToQodTextsConvertorOldWay()
        Dim V As IXMLToQodTextsConvertor

        ArrangeRequirementSwitchDependency(False)
        V = (New XMLToQodTextsConvertorFactory).GetImplementation
        Assert.AreEqual("Cts.Oasys.Hubs.Webservices.XMLToQodTextsConvertorOldWay", V.GetType.FullName)
    End Sub

#End Region

#Region "Test Methods"

    <TestMethod()> Public Sub CheckToSeeIfOneDeliveryInstruction_ReturnsCountOfOne()
        Dim x As New XMLToQodTextsConvertor
        Dim response As CTSFulfilmentResponse = SetupQodHeader.GetQodFromXMLWith1DelAnd0PickIns
        Dim qod As New QodHeader
        Dim Texts As Qod.QodTextCollection = x.ConvertToQodTexts(response.OrderHeader.DeliveryInstructions.InstructionLine, qod)
        Assert.AreEqual(1, Texts.Count)
    End Sub

    <TestMethod()> Public Sub CheckToSeeIfInstructionLineCount_ReturnsCountOfFive()
        Dim x As New XMLToQodTextsConvertor
        Dim response As CTSFulfilmentResponse = SetupQodHeader.GetQodFromXMLWith2DelAndPickIns
        Dim qod As New QodHeader
        Dim Texts As Qod.QodTextCollection = x.ConvertToQodTexts(response.OrderHeader.DeliveryInstructions.InstructionLine, qod)
        Assert.AreEqual(5, Texts.Count)
    End Sub

    <TestMethod()> Public Sub CheckToSeeIfInstructionLineCount_ReturnsCountOfThree()
        Dim x As New XMLToQodTextsConvertor
        Dim response As CTSFulfilmentResponse = SetupQodHeader.GetQodFromXMLWith0DelAnd2PickIns
        Dim qod As New QodHeader
        Dim Texts As Qod.QodTextCollection = x.ConvertToQodTexts(response.OrderHeader.DeliveryInstructions.InstructionLine, qod)
        Assert.AreEqual(3, Texts.Count)
    End Sub

    <TestMethod()> Public Sub CheckToSeeIfNoDeliveryPickInstructionLineCount_ReturnsCountOfTwo()
        Dim x As New XMLToQodTextsConvertor
        Dim response As CTSFulfilmentResponse = SetupQodHeader.GetQodFromXMLWith0DelAnd2PickIns
        Dim qod As New QodHeader
        Dim count As Integer = 0
        Dim Texts As Qod.QodTextCollection = x.ConvertToQodTexts(response.OrderHeader.DeliveryInstructions.InstructionLine, qod)

        For Each txt As QodText In Texts.GetTexts(QodTextType.Type.PickInstruction)
            count += 1
        Next
        Assert.AreEqual(2, count)
    End Sub

    <TestMethod()> Public Sub CheckToSeeIfDeliveryEqualTwoPickInstructionLineCount_ReturnsCountOfTwo()
        Dim x As New XMLToQodTextsConvertor
        Dim response As CTSFulfilmentResponse = SetupQodHeader.GetQodFromXMLWith2DelAndPickIns
        Dim qod As New QodHeader
        Dim count As Integer = 0
        Dim Texts As Qod.QodTextCollection = x.ConvertToQodTexts(response.OrderHeader.DeliveryInstructions.InstructionLine, qod)

        For Each txt As QodText In Texts.GetTexts(QodTextType.Type.PickInstruction)
            count += 1
        Next
        Assert.AreEqual(2, count)
    End Sub

    <TestMethod()> Public Sub CheckToSeeIfDeliveryInstructionLineCountWithTwoPickInstructions_ReturnsCountOfThree()
        Dim x As New XMLToQodTextsConvertor
        Dim response As CTSFulfilmentResponse = SetupQodHeader.GetQodFromXMLWith2DelAndPickIns
        Dim qod As New QodHeader
        Dim count As Integer = 0
        Dim Texts As Qod.QodTextCollection = x.ConvertToQodTexts(response.OrderHeader.DeliveryInstructions.InstructionLine, qod)

        For Each txt As QodText In Texts.GetTexts(QodTextType.Type.DeliveryInstruction)
            count += 1
        Next
        Assert.AreEqual(3, count)
    End Sub

    <TestMethod()> Public Sub CheckToSeeIfDeliveryInstructionLineCountWithNoPickInstructions_ReturnsCountOfOne()
        Dim x As New XMLToQodTextsConvertor
        Dim response As CTSFulfilmentResponse = SetupQodHeader.GetQodFromXMLWith1DelAnd0PickIns
        Dim qod As New QodHeader
        Dim count As Integer = 0
        Dim Texts As Qod.QodTextCollection = x.ConvertToQodTexts(response.OrderHeader.DeliveryInstructions.InstructionLine, qod)

        For Each txt As QodText In Texts.GetTexts(QodTextType.Type.DeliveryInstruction)
            count += 1
        Next
        Assert.AreEqual(1, count)
    End Sub

#End Region

#Region "Support Methods"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)
        Dim requirement = Substitute.For(Of IRequirementRepository)()

        requirement.RequirementEnabled(Arg.Any(Of Integer)).Returns(returnValue)
        RequirementRepositoryFactory.FactorySet(requirement)

    End Sub
#End Region

End Class

Public Class SetupQodHeader
    Public Const XML2DeliveryInsAnd2PickIns As String = "<CTSFulfilmentRequest>" & _
"  <DateTimeStamp>2012-11-05T15:40:07.342</DateTimeStamp>" & _
"  <TargetFulfilmentSite>8851</TargetFulfilmentSite>" & _
"  <OrderHeader>" & _
"    <SellingStoreCode>8902</SellingStoreCode>" & _
"    <SellingStoreOrderNumber>353202</SellingStoreOrderNumber>" & _
"    <RequiredDeliveryDate>2012-10-25</RequiredDeliveryDate>" & _
"    <DeliveryCharge>29.95</DeliveryCharge>" & _
"    <TotalOrderValue>261.74</TotalOrderValue>" & _
"    <SaleDate>2012-10-24</SaleDate>" & _
"    <OMOrderNumber>090909060</OMOrderNumber>" & _
"    <OrderStatus>200</OrderStatus>" & _
"    <CustomerAccountNo>151199</CustomerAccountNo>" & _
"    <CustomerName>Mr Philip Weston</CustomerName>" & _
"    <CustomerAddressLine1>954 Windsor Road East</CustomerAddressLine1>" & _
"    <CustomerAddressLine2>Ponceytown</CustomerAddressLine2>" & _
"    <CustomerAddressTown>Birmingham</CustomerAddressTown>" & _
"    <CustomerAddressLine4>West Midlands</CustomerAddressLine4>" & _
"    <CustomerPostcode>B25 8RQ</CustomerPostcode>" & _
"    <DeliveryAddressLine1>19 Evenwood Close</DeliveryAddressLine1>" & _
"    <DeliveryAddressLine2/>" & _
"    <DeliveryAddressTown>Stockton-on-Tees</DeliveryAddressTown>" & _
"    <DeliveryAddressLine4/>" & _
"    <DeliveryPostcode>TS19 9PB</DeliveryPostcode>" & _
"    <ContactPhoneHome>01604 351247</ContactPhoneHome>" & _
"    <ContactPhoneWork>01327 351122</ContactPhoneWork>" & _
"    <ContactPhoneMobile/>" & _
"    <ContactEmail>philip.weston@travisperkins.co.uk</ContactEmail>" & _
"    <DeliveryInstructions>" & _
"      <InstructionLine>" & _
"        <LineNo>01</LineNo>" & _
"        <LineText>Customer requires a Morning delivery.</LineText>" & _
"      </InstructionLine>" & _
"      <InstructionLine>" & _
"        <LineNo>02</LineNo>" & _
"        <LineText>Also, please ring before setting off.</LineText>" & _
"      </InstructionLine>" & _
"      <InstructionLine>" & _
"        <LineNo>03</LineNo>" & _
"        <LineText>S#OM.PI#F</LineText>" & _
"      </InstructionLine>" & _
"      <InstructionLine>" & _
"        <LineNo>04</LineNo>" & _
"        <LineText>Stock of 1 x 207546 (Tyne Aluminium Door 1981x762mm LH) is available at alternate location 8259 (WICKES DARLINGTON) as at 10:35 on 24/10/12</LineText>" & _
"      </InstructionLine>" & _
"      <InstructionLine>" & _
"        <LineNo>05</LineNo>" & _
"        <LineText>If required, please make arrangements with the alternate location specified</LineText>" & _
"      </InstructionLine>" & _
"    </DeliveryInstructions>" & _
"    <ToBeDelivered>1</ToBeDelivered>" & _
"    <Source>WO</Source>" & _
"    <SourceOrderNumber>494128</SourceOrderNumber>" & _
"    <SellingStoreTill>01</SellingStoreTill>" & _
"    <SellingStoreTransaction>0051</SellingStoreTransaction>" & _
"    <ExtendedLeadTime>0</ExtendedLeadTime>" & _
"    <DeliveryContactName>Paul Weston</DeliveryContactName>" & _
"    <DeliveryContactPhone>0114 581269</DeliveryContactPhone>" & _
"  </OrderHeader>" & _
"  <OrderLines>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>1</SellingStoreLineNo>" & _
"      <OMOrderLineNo>1</OMOrderLineNo>" & _
"      <ProductCode>224661</ProductCode>" & _
"      <ProductDescription>Blue Circle Cement 25kg</ProductDescription>" & _
"      <TotalOrderQuantity>5</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>18.80</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>3.76</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>2</SellingStoreLineNo>" & _
"      <OMOrderLineNo>2</OMOrderLineNo>" & _
"      <ProductCode>600569</ProductCode>" & _
"      <ProductDescription>Hardboard Paste Table</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>13.99</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>13.99</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>3</SellingStoreLineNo>" & _
"      <OMOrderLineNo>3</OMOrderLineNo>" & _
"      <ProductCode>207546</ProductCode>" & _
"      <ProductDescription>Tyne Aluminium Door 1981x762mm LH</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>199.00</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>199.00</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>4</SellingStoreLineNo>" & _
"      <OMOrderLineNo>4</OMOrderLineNo>" & _
"      <ProductCode>805111</ProductCode>" & _
"      <ProductDescription>Distant Shopping Delivery Charge</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>29.95</LineValue>" & _
"      <LineStatus>950</LineStatus>" & _
"      <DeliveryChargeItem>1</DeliveryChargeItem>" & _
"      <SellingPrice>29.95</SellingPrice>" & _
"      <FulfilmentSite>0</FulfilmentSite>" & _
"    </OrderLine>" & _
"  </OrderLines>" & _
"</CTSFulfilmentRequest>"

    Public Const XML1DeliveryInsAnd0PickIns As String = "<CTSFulfilmentRequest>" & _
"  <DateTimeStamp>2012-11-05T15:40:07.342</DateTimeStamp>" & _
"  <TargetFulfilmentSite>8851</TargetFulfilmentSite>" & _
"  <OrderHeader>" & _
"    <SellingStoreCode>8902</SellingStoreCode>" & _
"    <SellingStoreOrderNumber>353202</SellingStoreOrderNumber>" & _
"    <RequiredDeliveryDate>2012-10-25</RequiredDeliveryDate>" & _
"    <DeliveryCharge>29.95</DeliveryCharge>" & _
"    <TotalOrderValue>261.74</TotalOrderValue>" & _
"    <SaleDate>2012-10-24</SaleDate>" & _
"    <OMOrderNumber>090909060</OMOrderNumber>" & _
"    <OrderStatus>200</OrderStatus>" & _
"    <CustomerAccountNo>151199</CustomerAccountNo>" & _
"    <CustomerName>Mr Philip Weston</CustomerName>" & _
"    <CustomerAddressLine1>954 Windsor Road East</CustomerAddressLine1>" & _
"    <CustomerAddressLine2>Ponceytown</CustomerAddressLine2>" & _
"    <CustomerAddressTown>Birmingham</CustomerAddressTown>" & _
"    <CustomerAddressLine4>West Midlands</CustomerAddressLine4>" & _
"    <CustomerPostcode>B25 8RQ</CustomerPostcode>" & _
"    <DeliveryAddressLine1>19 Evenwood Close</DeliveryAddressLine1>" & _
"    <DeliveryAddressLine2/>" & _
"    <DeliveryAddressTown>Stockton-on-Tees</DeliveryAddressTown>" & _
"    <DeliveryAddressLine4/>" & _
"    <DeliveryPostcode>TS19 9PB</DeliveryPostcode>" & _
"    <ContactPhoneHome>01604 351247</ContactPhoneHome>" & _
"    <ContactPhoneWork>01327 351122</ContactPhoneWork>" & _
"    <ContactPhoneMobile/>" & _
"    <ContactEmail>philip.weston@travisperkins.co.uk</ContactEmail>" & _
"    <DeliveryInstructions>" & _
"      <InstructionLine>" & _
"        <LineNo>01</LineNo>" & _
"        <LineText>Customer requires a Morning delivery.</LineText>" & _
"      </InstructionLine>" & _
"    </DeliveryInstructions>" & _
"    <ToBeDelivered>1</ToBeDelivered>" & _
"    <Source>WO</Source>" & _
"    <SourceOrderNumber>494128</SourceOrderNumber>" & _
"    <SellingStoreTill>01</SellingStoreTill>" & _
"    <SellingStoreTransaction>0051</SellingStoreTransaction>" & _
"    <ExtendedLeadTime>0</ExtendedLeadTime>" & _
"    <DeliveryContactName>Paul Weston</DeliveryContactName>" & _
"    <DeliveryContactPhone>0114 581269</DeliveryContactPhone>" & _
"  </OrderHeader>" & _
"  <OrderLines>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>1</SellingStoreLineNo>" & _
"      <OMOrderLineNo>1</OMOrderLineNo>" & _
"      <ProductCode>224661</ProductCode>" & _
"      <ProductDescription>Blue Circle Cement 25kg</ProductDescription>" & _
"      <TotalOrderQuantity>5</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>18.80</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>3.76</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>2</SellingStoreLineNo>" & _
"      <OMOrderLineNo>2</OMOrderLineNo>" & _
"      <ProductCode>600569</ProductCode>" & _
"      <ProductDescription>Hardboard Paste Table</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>13.99</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>13.99</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>3</SellingStoreLineNo>" & _
"      <OMOrderLineNo>3</OMOrderLineNo>" & _
"      <ProductCode>207546</ProductCode>" & _
"      <ProductDescription>Tyne Aluminium Door 1981x762mm LH</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>199.00</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>199.00</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>4</SellingStoreLineNo>" & _
"      <OMOrderLineNo>4</OMOrderLineNo>" & _
"      <ProductCode>805111</ProductCode>" & _
"      <ProductDescription>Distant Shopping Delivery Charge</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>29.95</LineValue>" & _
"      <LineStatus>950</LineStatus>" & _
"      <DeliveryChargeItem>1</DeliveryChargeItem>" & _
"      <SellingPrice>29.95</SellingPrice>" & _
"      <FulfilmentSite>0</FulfilmentSite>" & _
"    </OrderLine>" & _
"  </OrderLines>" & _
"</CTSFulfilmentRequest>"

    Public Const XML0DeliveryInsAnd2PickIns As String = "<CTSFulfilmentRequest>" & _
"  <DateTimeStamp>2012-11-05T15:40:07.342</DateTimeStamp>" & _
"  <TargetFulfilmentSite>8851</TargetFulfilmentSite>" & _
"  <OrderHeader>" & _
"    <SellingStoreCode>8902</SellingStoreCode>" & _
"    <SellingStoreOrderNumber>353202</SellingStoreOrderNumber>" & _
"    <RequiredDeliveryDate>2012-10-25</RequiredDeliveryDate>" & _
"    <DeliveryCharge>29.95</DeliveryCharge>" & _
"    <TotalOrderValue>261.74</TotalOrderValue>" & _
"    <SaleDate>2012-10-24</SaleDate>" & _
"    <OMOrderNumber>090909060</OMOrderNumber>" & _
"    <OrderStatus>200</OrderStatus>" & _
"    <CustomerAccountNo>151199</CustomerAccountNo>" & _
"    <CustomerName>Mr Philip Weston</CustomerName>" & _
"    <CustomerAddressLine1>954 Windsor Road East</CustomerAddressLine1>" & _
"    <CustomerAddressLine2>Ponceytown</CustomerAddressLine2>" & _
"    <CustomerAddressTown>Birmingham</CustomerAddressTown>" & _
"    <CustomerAddressLine4>West Midlands</CustomerAddressLine4>" & _
"    <CustomerPostcode>B25 8RQ</CustomerPostcode>" & _
"    <DeliveryAddressLine1>19 Evenwood Close</DeliveryAddressLine1>" & _
"    <DeliveryAddressLine2/>" & _
"    <DeliveryAddressTown>Stockton-on-Tees</DeliveryAddressTown>" & _
"    <DeliveryAddressLine4/>" & _
"    <DeliveryPostcode>TS19 9PB</DeliveryPostcode>" & _
"    <ContactPhoneHome>01604 351247</ContactPhoneHome>" & _
"    <ContactPhoneWork>01327 351122</ContactPhoneWork>" & _
"    <ContactPhoneMobile/>" & _
"    <ContactEmail>philip.weston@travisperkins.co.uk</ContactEmail>" & _
"    <DeliveryInstructions>" & _
"      <InstructionLine>" & _
"        <LineNo>01</LineNo>" & _
"        <LineText>S#OM.PI#F</LineText>" & _
"      </InstructionLine>" & _
"      <InstructionLine>" & _
"        <LineNo>02</LineNo>" & _
"        <LineText>Stock of 1 x 207546 (Tyne Aluminium Door 1981x762mm LH) is available at alternate location 8259 (WICKES DARLINGTON) as at 10:35 on 24/10/12</LineText>" & _
"      </InstructionLine>" & _
"      <InstructionLine>" & _
"        <LineNo>03</LineNo>" & _
"        <LineText>If required, please make arrangements with the alternate location specified</LineText>" & _
"      </InstructionLine>" & _
"    </DeliveryInstructions>" & _
"    <ToBeDelivered>1</ToBeDelivered>" & _
"    <Source>WO</Source>" & _
"    <SourceOrderNumber>494128</SourceOrderNumber>" & _
"    <SellingStoreTill>01</SellingStoreTill>" & _
"    <SellingStoreTransaction>0051</SellingStoreTransaction>" & _
"    <ExtendedLeadTime>0</ExtendedLeadTime>" & _
"    <DeliveryContactName>Paul Weston</DeliveryContactName>" & _
"    <DeliveryContactPhone>0114 581269</DeliveryContactPhone>" & _
"  </OrderHeader>" & _
"  <OrderLines>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>1</SellingStoreLineNo>" & _
"      <OMOrderLineNo>1</OMOrderLineNo>" & _
"      <ProductCode>224661</ProductCode>" & _
"      <ProductDescription>Blue Circle Cement 25kg</ProductDescription>" & _
"      <TotalOrderQuantity>5</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>18.80</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>3.76</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>2</SellingStoreLineNo>" & _
"      <OMOrderLineNo>2</OMOrderLineNo>" & _
"      <ProductCode>600569</ProductCode>" & _
"      <ProductDescription>Hardboard Paste Table</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>13.99</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>13.99</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>3</SellingStoreLineNo>" & _
"      <OMOrderLineNo>3</OMOrderLineNo>" & _
"      <ProductCode>207546</ProductCode>" & _
"      <ProductDescription>Tyne Aluminium Door 1981x762mm LH</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>199.00</LineValue>" & _
"      <LineStatus>200</LineStatus>" & _
"      <DeliveryChargeItem>0</DeliveryChargeItem>" & _
"      <SellingPrice>199.00</SellingPrice>" & _
"      <FulfilmentSite>8851</FulfilmentSite>" & _
"    </OrderLine>" & _
"    <OrderLine>" & _
"      <SellingStoreLineNo>4</SellingStoreLineNo>" & _
"      <OMOrderLineNo>4</OMOrderLineNo>" & _
"      <ProductCode>805111</ProductCode>" & _
"      <ProductDescription>Distant Shopping Delivery Charge</ProductDescription>" & _
"      <TotalOrderQuantity>1</TotalOrderQuantity>" & _
"      <QuantityTaken>0</QuantityTaken>" & _
"      <UOM>EACH</UOM>" & _
"      <LineValue>29.95</LineValue>" & _
"      <LineStatus>950</LineStatus>" & _
"      <DeliveryChargeItem>1</DeliveryChargeItem>" & _
"      <SellingPrice>29.95</SellingPrice>" & _
"      <FulfilmentSite>0</FulfilmentSite>" & _
"    </OrderLine>" & _
"  </OrderLines>" & _
"</CTSFulfilmentRequest>"

    Public Shared Function GetQodFromXMLWith2DelAndPickIns() As CTSFulfilmentResponse
        Dim request As New CTSFulfilmentRequest
        Dim response As New CTSFulfilmentResponse

        request = DirectCast(request.Deserialise(XML2DeliveryInsAnd2PickIns), CTSFulfilmentRequest)
        response.SetFieldsFromRequest(request)
        Return response
    End Function

    Public Shared Function GetQodFromXMLWith1DelAnd0PickIns() As CTSFulfilmentResponse
        Dim request As New CTSFulfilmentRequest
        Dim response As New CTSFulfilmentResponse

        request = DirectCast(request.Deserialise(XML1DeliveryInsAnd0PickIns), CTSFulfilmentRequest)
        response.SetFieldsFromRequest(request)
        Return response
    End Function

    Public Shared Function GetQodFromXMLWith0DelAnd2PickIns() As CTSFulfilmentResponse
        Dim request As New CTSFulfilmentRequest
        Dim response As New CTSFulfilmentResponse

        request = DirectCast(request.Deserialise(XML0DeliveryInsAnd2PickIns), CTSFulfilmentRequest)
        response.SetFieldsFromRequest(request)
        Return response
    End Function

    Public Shared Function SetDeliveryInstructions() As QodHeader

        Dim QodHeader As New QodHeader
        Dim Texts As New QodTextCollection
        Dim Text As New QodText

        Text.Text = "This is a delivery line instruction."
        Text.Type = "DI"
        Text.SellingStoreId = 8851
        Text.SellingStoreOrderId = 9900
        Text.OrderNumber = "111111"
        Text.Number = "01"
        QodHeader.Texts.Add(Text)

        Dim Text1 As New QodText
        Text1.Text = "Customer requires morning delivery."
        Text1.Type = "DI"
        Text1.SellingStoreId = 8851
        Text1.SellingStoreOrderId = 9900
        Text1.OrderNumber = "111111"
        Text1.Number = "02"
        QodHeader.Texts.Add(Text1)

        'Dim Text2 As New QodText
        ''Text2.Text = "S#OM.PI#F"
        'Text2.Text = ""
        'Text2.Type = "DI"
        'Text2.SellingStoreId = 8851
        'Text2.SellingStoreOrderId = 9900
        'Text2.OrderNumber = "111111"
        'Text2.Number = "03"
        'QodHeader.Texts.Add(Text2)

        Dim Text3 As New QodText
        Text3.Text = "Stock of 1 x 207546 (Tyne Aluminium Door 1981x762mm LH) is available at alternate location 8259 (WICKES DARLINGTON) as at 10:35 on 24/10/12"
        Text3.Type = "PI"
        Text3.SellingStoreId = 8851
        Text3.SellingStoreOrderId = 9900
        Text3.OrderNumber = "111111"
        Text3.Number = "04"
        QodHeader.Texts.Add(Text3)

        Dim Text4 As New QodText
        Text4.Text = "If required, please make arrangements with the alternate location specified"
        Text4.Type = "PI"
        Text4.SellingStoreId = 8851
        Text4.SellingStoreOrderId = 9900
        Text4.OrderNumber = "111111"
        Text4.Number = "05"
        QodHeader.Texts.Add(Text4)

        Return QodHeader
    End Function
End Class

