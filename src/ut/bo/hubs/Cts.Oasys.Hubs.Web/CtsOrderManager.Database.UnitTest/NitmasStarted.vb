﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class NitmasStarted

#Region "Properties"
    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property
#End Region

#Region "Static members"

    Private Const StoredProcedureName As String = "NitmasStarted"
    Private Shared initialised As Boolean
    Private Shared nitmasPendingTime As String
    Private Shared NitLogCount As DataTable
    Private Shared NonZeroNitLogDate As Date
    Private Shared ZeroNitLogDate As Date
    Private Shared FoundStoredProcedure As Boolean
    Private Shared StoredProcedureNitLogNonZeroCountNitmasStarted As Boolean
    Private Shared StoredProcedureNitLogZeroCountNitmasStarted As Boolean
#End Region

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        Assert.IsTrue(SetupForTests, "Failed initialising for the CTSOrderManager Stored Procedures tests.")
                    Case Else
                        Assert.Fail("Cannot perform Stock Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Tests"

    '<TestMethod()> _
    'Public Sub StoredProcedureExists()

    '    Assert.IsTrue(FoundStoredProcedure, "Stored procedure '" & StoredProcedureName & "' does not exist.")
    'End Sub

    '<TestMethod()> _
    'Public Sub NitmasStartedReturnsTrueForANitmasStartedDate()

    '    Assert.IsTrue(StoredProcedureNitLogNonZeroCountNitmasStarted, "Stored procedure '" & StoredProcedureName & "' did not return true when given a datetime for which the NitLog table has entries.")
    'End Sub

    '<TestMethod()> _
    'Public Sub NitmasStartedReturnsFalseForANitmasNotStartedDate()

    '    Assert.IsFalse(StoredProcedureNitLogZeroCountNitmasStarted, "Stored procedure '" & StoredProcedureName & "' did not return false when given a datetime for which the NitLog table does not have entries.")
    'End Sub

#End Region

#Region "Local Static Methods"

    Private Shared Function CheckForStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim spCheck As DataTable

                    com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & StoredProcedureName & "'"
                    spCheck = com.ExecuteDataTable
                    If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                        FoundStoredProcedure = True
                    End If
                    CheckForStoredProcedure = True
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & StoredProcedureName & ".")
        End Try
    End Function

    Private Shared Function GetNitmasPendingTime() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim Parameter6000 As DataTable

                    nitmasPendingTime = "01:00:00"
                    ' Attempt to get a pending time from database, otherwise use default as above
                    com.CommandText = "Select StringValue from Parameters where ParameterId = 6000"
                    Parameter6000 = com.ExecuteDataTable
                    If Parameter6000 IsNot Nothing AndAlso Parameter6000.Rows.Count > 0 Then
                        nitmasPendingTime = Parameter6000.Rows(0).Item("StringValue").ToString
                    End If
                End Using
            End Using
            GetNitmasPendingTime = True
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get/set a time for the Nitmas Pending date.")
        End Try
    End Function

    Private Shared Function GetDateForNonZeroNitlogCount() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetNitmasStartedSQL(), con)
                    Dim Start As Date
                    Dim StartDate As String
                    Dim StartTime As String
                    Dim strSplit() As String = {"01", "00", "00"}

                    If Not String.Equals(nitmasPendingTime, String.Empty) Then
                        strSplit = nitmasPendingTime.Split(":"c)
                    End If
                    Start = New System.DateTime(Now.Year, Now.Month, Now.Day, CType(strSplit(0), Integer), CType(strSplit(1), Integer), 0)

                    com.Parameters.Add("@StartDate", SqlDbType.NChar)
                    com.Parameters.Add("@StartTime", SqlDbType.NChar)

                    ' Find a date which brings back a non zero nitlog entry count
                    Do While Start > Date.MinValue
                        With Start
                            StartDate = .Year.ToString & "-" & _
                                        .Month.ToString.PadLeft(2, CType("0", Char)) & "-" & _
                                        .Day.ToString.PadLeft(2, CType("0", Char))
                            StartTime = .Hour.ToString.PadLeft(2, CType("0", Char)) & _
                                        .Minute.ToString.PadLeft(2, CType("0", Char)) & _
                                        .Second.ToString.PadLeft(2, CType("0", Char))
                        End With
                        com.Parameters.Item("@StartDate").Value = StartDate
                        com.Parameters.Item("@StartTime").Value = StartTime

                        Dim reader As SqlDataReader = com.ExecuteReader

                        NitLogCount = New DataTable
                        NitLogCount.Load(reader)
                        reader.Close()

                        If NitLogCount.Rows.Count = 1 Then
                            If CInt(NitLogCount.Rows(0).Item(0)) > 0 Then
                                NonZeroNitLogDate = Start
                                Exit Do
                            End If
                        End If
                        Start = DateAdd(DateInterval.Day, -1, Start)
                    Loop
                End Using
                con.Close()
            End Using
            GetDateForNonZeroNitlogCount = True
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a Nitmas Pending date that provided a non zero Nitlog count.")
        End Try
    End Function

    Private Shared Function GetDateForZeroNitlogCount() As Boolean

        Try
            Using con As New SqlConnection(GetConnectionString())
                con.Open()
                Using com As New SqlCommand(GetNitmasStartedSQL(), con)
                    Dim Start As Date
                    Dim StartDate As String
                    Dim StartTime As String
                    Dim strSplit() As String = {"01", "00", "00"}

                    If Not String.Equals(nitmasPendingTime, String.Empty) Then
                        strSplit = nitmasPendingTime.Split(":"c)
                    End If
                    Start = New System.DateTime(Now.Year, Now.Month, Now.Day, CType(strSplit(0), Integer), CType(strSplit(1), Integer), 0)

                    com.Parameters.Add("@StartDate", SqlDbType.NChar)
                    com.Parameters.Add("@StartTime", SqlDbType.NChar)

                    ' Find a date which brings back a non zero nitlog entry count
                    Do While Start > Date.MinValue
                        With Start
                            StartDate = .Year.ToString & "-" & _
                                        .Month.ToString.PadLeft(2, CType("0", Char)) & "-" & _
                                        .Day.ToString.PadLeft(2, CType("0", Char))
                            StartTime = .Hour.ToString.PadLeft(2, CType("0", Char)) & _
                                        .Minute.ToString.PadLeft(2, CType("0", Char)) & _
                                        .Second.ToString.PadLeft(2, CType("0", Char))
                        End With
                        com.Parameters.Item("@StartDate").Value = StartDate
                        com.Parameters.Item("@StartTime").Value = StartTime

                        Dim reader As SqlDataReader = com.ExecuteReader

                        NitLogCount = New DataTable
                        NitLogCount.Load(reader)
                        reader.Close()

                        If NitLogCount.Rows.Count = 1 Then
                            If CInt(NitLogCount.Rows(0).Item(0)) = 0 Then
                                ZeroNitLogDate = Start
                                Exit Do
                            End If
                        End If
                        Start = DateAdd(DateInterval.Day, -1, Start)
                    Loop
                End Using
                con.Close()
            End Using
            GetDateForZeroNitlogCount = True
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Could not get a Nitmas Pending date that provided a zero Nitlog count.")
        End Try
    End Function

    Private Shared Function GetNitmasStartedSQL() As String
        Dim sb As New StringBuilder

        sb.Append("SELECT COUNT(*) ")
        sb.Append("FROM NITLOG ")
        sb.Append("WHERE SDAT >= @StartDate ")
        sb.Append("AND   STIM >= @StartTime")
        GetNitmasStartedSQL = sb.ToString
    End Function

    Private Shared Function GetStoredProcedureNonZeroNitlogCount() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .StoredProcedureName = StoredProcedureName
                        .AddParameter("@NitmasPending", NonZeroNitLogDate)
                        .AddOutputParameter("@Started", SqlDbType.Bit)
                        .ExecuteNonQuery()
                        StoredProcedureNitLogNonZeroCountNitmasStarted = Convert.ToBoolean(.GetParameterValue("@Started"))
                    End With
                End Using
            End Using
            GetStoredProcedureNonZeroNitlogCount = True
        Catch ex As SqlException
            Assert.Inconclusive("Initialisation for tests failed.  Could not use the stored procedure (" & StoredProcedureName & ") to provide a non-zero Nitlog count.  " & ex.Message & ".")
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in attempt to use the stored procedure (" & StoredProcedureName & ") to provide a zero Nitlog count.  " & ex.Message & ".")
        End Try
    End Function

    Private Shared Function GetStoredProcedureZeroNitlogCount() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    With com
                        .StoredProcedureName = StoredProcedureName
                        .AddParameter("@NitmasPending", ZeroNitLogDate)
                        .AddOutputParameter("@Started", SqlDbType.Bit)
                        .ExecuteNonQuery()
                        StoredProcedureNitLogZeroCountNitmasStarted = Convert.ToBoolean(.GetParameterValue("@Started"))
                    End With
                    GetStoredProcedureZeroNitlogCount = True
                End Using
            End Using
        Catch ex As SqlException
            Assert.Inconclusive("Initialisation for tests failed.  Could not use the stored procedure (" & StoredProcedureName & ") to provide a zero Nitlog count.  " & ex.Message & ".")
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in attempt to use the stored procedure (" & StoredProcedureName & ") to provide a zero Nitlog count.  " & ex.Message & ".")
        End Try
    End Function

    Private Shared Function SetupForTests() As Boolean

        If Not initialised Then
            If CheckForStoredProcedure() Then
                If GetNitmasPendingTime() Then
                    If GetDateForNonZeroNitlogCount() Then
                        If GetDateForZeroNitlogCount() Then
                            If GetStoredProcedureNonZeroNitlogCount() Then
                                If GetStoredProcedureZeroNitlogCount() Then
                                    initialised = True
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
        SetupForTests = initialised
    End Function
#End Region
End Class
