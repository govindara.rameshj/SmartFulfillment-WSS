﻿
Module GlobalStuff

#Region "XSD Definitions - Good"

    Friend OM_CreateOrderRequest_CorrectXSD As XElement = <xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                                                              <xs:element name="OMOrderCreateRequest">
                                                                  <xs:complexType>
                                                                      <xs:all>
                                                                          <xs:element name="DateTimeStamp">
                                                                              <xs:complexType>
                                                                                  <xs:simpleContent>
                                                                                      <xs:extension base="xs:dateTime"/>
                                                                                  </xs:simpleContent>
                                                                              </xs:complexType>
                                                                          </xs:element>
                                                                          <xs:element name="OrderHeader">
                                                                              <xs:complexType>
                                                                                  <xs:all>
                                                                                      <xs:element name="Source" type="xs:string" minOccurs="0"/>
                                                                                      <xs:element name="SourceOrderNumber" type="xs:string" minOccurs="0"/>
                                                                                      <xs:element name="SellingStoreCode">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:integer"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="SellingStoreOrderNumber">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:integer"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="SellingStoreTill" type="xs:string" minOccurs="0"/>
                                                                                      <xs:element name="SellingStoreTransaction" type="xs:string" minOccurs="0"/>
                                                                                      <xs:element name="RequiredDeliveryDate">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:date"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="DeliveryCharge">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:decimal"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="TotalOrderValue">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:decimal"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="SaleDate">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:date"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="CustomerAccountNo" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="CustomerName">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="CustomerAddressLine1">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="CustomerAddressLine2" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="CustomerAddressTown">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="CustomerAddressLine4" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="CustomerPostcode" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="DeliveryAddressLine1">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="DeliveryAddressLine2" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="DeliveryAddressTown">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="DeliveryAddressLine4" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="DeliveryPostcode" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="ContactPhoneHome" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="ContactPhoneMobile" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="ContactPhoneWork" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="ContactEmail" nillable="true">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:string"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="DeliveryInstructions">
                                                                                          <xs:complexType>
                                                                                              <xs:sequence minOccurs="0" maxOccurs="unbounded">
                                                                                                  <xs:element name="InstructionLine">
                                                                                                      <xs:complexType>
                                                                                                          <xs:all>
                                                                                                              <xs:element name="LineNo">
                                                                                                                  <xs:complexType>
                                                                                                                      <xs:simpleContent>
                                                                                                                          <xs:extension base="xs:integer"/>
                                                                                                                      </xs:simpleContent>
                                                                                                                  </xs:complexType>
                                                                                                              </xs:element>
                                                                                                              <xs:element name="LineText">
                                                                                                                  <xs:complexType>
                                                                                                                      <xs:simpleContent>
                                                                                                                          <xs:extension base="xs:string"/>
                                                                                                                      </xs:simpleContent>
                                                                                                                  </xs:complexType>
                                                                                                              </xs:element>
                                                                                                          </xs:all>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                              </xs:sequence>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="ToBeDelivered">
                                                                                          <xs:complexType>
                                                                                              <xs:simpleContent>
                                                                                                  <xs:extension base="xs:boolean"/>
                                                                                              </xs:simpleContent>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                      <xs:element name="ExtendedLeadTime" type="xs:boolean" minOccurs="0"/>
                                                                                      <xs:element name="DeliveryContactName" type="xs:string" minOccurs="0"/>
                                                                                      <xs:element name="DeliveryContactPhone" type="xs:string" minOccurs="0"/>
                                                                                  </xs:all>
                                                                              </xs:complexType>
                                                                          </xs:element>
                                                                          <xs:element name="OrderLines">
                                                                              <xs:complexType>
                                                                                  <xs:sequence>
                                                                                      <xs:element name="OrderLine" maxOccurs="unbounded">
                                                                                          <xs:complexType>
                                                                                              <xs:all>
                                                                                                  <xs:element name="SourceOrderLineNo" type="xs:integer" minOccurs="0"/>
                                                                                                  <xs:element name="SellingStoreLineNo">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:integer"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="ProductCode">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:integer"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="ProductDescription">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:string"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="TotalOrderQuantity">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:decimal"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="QuantityTaken">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:integer"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="UOM">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:string"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="LineValue">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:decimal"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="DeliveryChargeItem">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:boolean"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="SellingPrice">
                                                                                                      <xs:complexType>
                                                                                                          <xs:simpleContent>
                                                                                                              <xs:extension base="xs:decimal"/>
                                                                                                          </xs:simpleContent>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                              </xs:all>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                  </xs:sequence>
                                                                              </xs:complexType>
                                                                          </xs:element>
                                                                      </xs:all>
                                                                  </xs:complexType>
                                                              </xs:element>
                                                          </xs:schema>

    Friend OM_RefundOrderRequest_CorrectXSD As XElement = <xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                                                              <xs:element name="OMOrderRefundRequest">
                                                                  <xs:complexType>
                                                                      <xs:all>
                                                                          <xs:element name="DateTimeStamp" type="xs:dateTime"/>
                                                                          <xs:element name="OMOrderNumber" type="xs:string"/>
                                                                          <xs:element name="OrderRefunds">
                                                                              <xs:complexType>
                                                                                  <xs:sequence>
                                                                                      <xs:element maxOccurs="unbounded" name="OrderRefund">
                                                                                          <xs:complexType>
                                                                                              <xs:all>
                                                                                                  <xs:element name="RefundDate" type="xs:dateTime"/>
                                                                                                  <xs:element name="RefundStoreCode" type="xs:integer"/>
                                                                                                  <xs:element name="RefundTransaction" type="xs:string"/>
                                                                                                  <xs:element name="RefundTill" type="xs:string"/>
                                                                                                  <xs:element minOccurs="0" name="FulfilmentSites">
                                                                                                      <xs:complexType>
                                                                                                          <xs:sequence>
                                                                                                              <xs:element maxOccurs="unbounded" name="FulfilmentSite">
                                                                                                                  <xs:complexType>
                                                                                                                      <xs:all>
                                                                                                                          <xs:element name="FulfilmentSiteCode" type="xs:integer"/>
                                                                                                                          <xs:element name="SellingStoreIBTOutNumber" type="xs:integer"/>
                                                                                                                      </xs:all>
                                                                                                                  </xs:complexType>
                                                                                                              </xs:element>
                                                                                                          </xs:sequence>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                                  <xs:element name="RefundLines">
                                                                                                      <xs:complexType>
                                                                                                          <xs:sequence>
                                                                                                              <xs:element maxOccurs="unbounded" name="RefundLine">
                                                                                                                  <xs:complexType>
                                                                                                                      <xs:all>
                                                                                                                          <xs:element name="SellingStoreLineNo" type="xs:integer"/>
                                                                                                                          <xs:element name="OMOrderLineNo" type="xs:integer"/>
                                                                                                                          <xs:element name="ProductCode" type="xs:integer"/>
                                                                                                                          <xs:element name="FulfilmentSiteCode" type="xs:integer"/>
                                                                                                                          <xs:element name="QuantityReturned" type="xs:integer"/>
                                                                                                                          <xs:element name="QuantityCancelled" type="xs:integer"/>
                                                                                                                          <xs:element name="RefundLineValue" type="xs:decimal"/>
                                                                                                                          <xs:element name="RefundLineStatus" type="xs:integer"/>
                                                                                                                      </xs:all>
                                                                                                                  </xs:complexType>
                                                                                                              </xs:element>
                                                                                                          </xs:sequence>
                                                                                                      </xs:complexType>
                                                                                                  </xs:element>
                                                                                              </xs:all>
                                                                                          </xs:complexType>
                                                                                      </xs:element>
                                                                                  </xs:sequence>
                                                                              </xs:complexType>
                                                                          </xs:element>
                                                                      </xs:all>
                                                                  </xs:complexType>
                                                              </xs:element>
                                                          </xs:schema>

    Friend OM_StatusUpdateRequest_CorrectXSD As XElement = <xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                                                               <xs:element name="OMOrderReceiveStatusUpdateRequest">
                                                                   <xs:complexType>
                                                                       <xs:all>
                                                                           <xs:element name="DateTimeStamp">
                                                                               <xs:complexType>
                                                                                   <xs:simpleContent>
                                                                                       <xs:extension base="xs:dateTime">
                                                                                           <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                       </xs:extension>
                                                                                   </xs:simpleContent>
                                                                               </xs:complexType>
                                                                           </xs:element>
                                                                           <xs:element name="OMOrderNumber">
                                                                               <xs:complexType>
                                                                                   <xs:simpleContent>
                                                                                       <xs:extension base="xs:string">
                                                                                           <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                       </xs:extension>
                                                                                   </xs:simpleContent>
                                                                               </xs:complexType>
                                                                           </xs:element>
                                                                           <xs:element name="FulfilmentSite">
                                                                               <xs:complexType>
                                                                                   <xs:simpleContent>
                                                                                       <xs:extension base="xs:integer">
                                                                                           <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                       </xs:extension>
                                                                                   </xs:simpleContent>
                                                                               </xs:complexType>
                                                                           </xs:element>
                                                                           <xs:element name="OrderStatus">
                                                                               <xs:complexType>
                                                                                   <xs:sequence>
                                                                                       <xs:element name="OrderLines">
                                                                                           <xs:complexType>
                                                                                               <xs:sequence>
                                                                                                   <xs:element maxOccurs="unbounded" name="OrderLine">
                                                                                                       <xs:complexType>
                                                                                                           <xs:all>
                                                                                                               <xs:element name="OMOrderLineNo">
                                                                                                                   <xs:complexType>
                                                                                                                       <xs:simpleContent>
                                                                                                                           <xs:extension base="xs:integer">
                                                                                                                               <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                                                           </xs:extension>
                                                                                                                       </xs:simpleContent>
                                                                                                                   </xs:complexType>
                                                                                                               </xs:element>
                                                                                                               <xs:element name="LineStatus">
                                                                                                                   <xs:annotation>
                                                                                                                       <xs:documentation>"Input" - can only contain:
300	All Stock IBT'd Out from fulfilment site
330	IBT Out notification contained invalid data
550	Picking notification recorded at OM
580	Picking status update contained invalid data
750	Despatch notification recorded at OM
780	Despatch status update contained invalid data

"Output" - can only contain:
330	IBT Out notification contained invalid data
400	All stock IBT'd In from fulfilment site
580	Picking status update contained invalid data
600	Picking status update successful
780	Despatch status update contained invalid data
800	Despatch status update successful
</xs:documentation>
                                                                                                                   </xs:annotation>
                                                                                                                   <xs:complexType>
                                                                                                                       <xs:simpleContent>
                                                                                                                           <xs:extension base="xs:integer">
                                                                                                                               <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                                                           </xs:extension>
                                                                                                                       </xs:simpleContent>
                                                                                                                   </xs:complexType>
                                                                                                               </xs:element>
                                                                                                           </xs:all>
                                                                                                       </xs:complexType>
                                                                                                   </xs:element>
                                                                                               </xs:sequence>
                                                                                           </xs:complexType>
                                                                                       </xs:element>
                                                                                   </xs:sequence>
                                                                               </xs:complexType>
                                                                           </xs:element>
                                                                       </xs:all>
                                                                   </xs:complexType>
                                                               </xs:element>
                                                           </xs:schema>

#End Region

#Region "XSD Definitions - Bad"

    Friend OM_CreateOrderRequest_InCorrectXSD As XElement = <xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                                                                <xs:element name="OMOrderCreateRequest">
                                                                    <xs:complexType>
                                                                        <xs:all>
                                                                            <xs:element name="DateTimeStampXXXXXXXXXXXXXXXXXXXXXXX">
                                                                                <xs:complexType>
                                                                                    <xs:simpleContent>
                                                                                        <xs:extension base="xs:dateTime"/>
                                                                                    </xs:simpleContent>
                                                                                </xs:complexType>
                                                                            </xs:element>
                                                                            <xs:element name="OrderHeader">
                                                                                <xs:complexType>
                                                                                    <xs:all>
                                                                                        <xs:element name="Source" type="xs:string" minOccurs="0"/>
                                                                                        <xs:element name="SourceOrderNumber" type="xs:string" minOccurs="0"/>
                                                                                        <xs:element name="SellingStoreCode">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:integer"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="SellingStoreOrderNumber">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:integer"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="SellingStoreTill" type="xs:string" minOccurs="0"/>
                                                                                        <xs:element name="SellingStoreTransaction" type="xs:string" minOccurs="0"/>
                                                                                        <xs:element name="RequiredDeliveryDate">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:date"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="DeliveryCharge">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:decimal"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="TotalOrderValue">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:decimal"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="SaleDate">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:date"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="CustomerAccountNo" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="CustomerName">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="CustomerAddressLine1">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="CustomerAddressLine2" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="CustomerAddressTown">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="CustomerAddressLine4" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="CustomerPostcode" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="DeliveryAddressLine1">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="DeliveryAddressLine2" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="DeliveryAddressTown">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="DeliveryAddressLine4" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="DeliveryPostcode" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="ContactPhoneHome" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="ContactPhoneMobile" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="ContactPhoneWork" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="ContactEmail" nillable="true">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:string"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="DeliveryInstructions">
                                                                                            <xs:complexType>
                                                                                                <xs:sequence minOccurs="0" maxOccurs="unbounded">
                                                                                                    <xs:element name="InstructionLine">
                                                                                                        <xs:complexType>
                                                                                                            <xs:all>
                                                                                                                <xs:element name="LineNo">
                                                                                                                    <xs:complexType>
                                                                                                                        <xs:simpleContent>
                                                                                                                            <xs:extension base="xs:integer"/>
                                                                                                                        </xs:simpleContent>
                                                                                                                    </xs:complexType>
                                                                                                                </xs:element>
                                                                                                                <xs:element name="LineText">
                                                                                                                    <xs:complexType>
                                                                                                                        <xs:simpleContent>
                                                                                                                            <xs:extension base="xs:string"/>
                                                                                                                        </xs:simpleContent>
                                                                                                                    </xs:complexType>
                                                                                                                </xs:element>
                                                                                                            </xs:all>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                </xs:sequence>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="ToBeDelivered">
                                                                                            <xs:complexType>
                                                                                                <xs:simpleContent>
                                                                                                    <xs:extension base="xs:boolean"/>
                                                                                                </xs:simpleContent>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                        <xs:element name="ExtendedLeadTime" type="xs:boolean" minOccurs="0"/>
                                                                                        <xs:element name="DeliveryContactName" type="xs:string" minOccurs="0"/>
                                                                                        <xs:element name="DeliveryContactPhone" type="xs:string" minOccurs="0"/>
                                                                                    </xs:all>
                                                                                </xs:complexType>
                                                                            </xs:element>
                                                                            <xs:element name="OrderLines">
                                                                                <xs:complexType>
                                                                                    <xs:sequence>
                                                                                        <xs:element name="OrderLine" maxOccurs="unbounded">
                                                                                            <xs:complexType>
                                                                                                <xs:all>
                                                                                                    <xs:element name="SourceOrderLineNo" type="xs:integer" minOccurs="0"/>
                                                                                                    <xs:element name="SellingStoreLineNo">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:integer"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="ProductCode">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:integer"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="ProductDescription">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:string"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="TotalOrderQuantity">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:decimal"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="QuantityTaken">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:integer"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="UOM">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:string"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="LineValue">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:decimal"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="DeliveryChargeItem">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:boolean"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="SellingPrice">
                                                                                                        <xs:complexType>
                                                                                                            <xs:simpleContent>
                                                                                                                <xs:extension base="xs:decimal"/>
                                                                                                            </xs:simpleContent>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                </xs:all>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                    </xs:sequence>
                                                                                </xs:complexType>
                                                                            </xs:element>
                                                                        </xs:all>
                                                                    </xs:complexType>
                                                                </xs:element>
                                                            </xs:schema>

    Friend OM_RefundOrderRequest_InCorrectXSD As XElement = <xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                                                                <xs:element name="OMOrderRefundRequest">
                                                                    <xs:complexType>
                                                                        <xs:all>
                                                                            <xs:element name="DateTimeStampXXXXXXXXXXXXXXXXXXXXXXX" type="xs:dateTime"/>
                                                                            <xs:element name="OMOrderNumber" type="xs:string"/>
                                                                            <xs:element name="OrderRefunds">
                                                                                <xs:complexType>
                                                                                    <xs:sequence>
                                                                                        <xs:element maxOccurs="unbounded" name="OrderRefund">
                                                                                            <xs:complexType>
                                                                                                <xs:all>
                                                                                                    <xs:element name="RefundDate" type="xs:dateTime"/>
                                                                                                    <xs:element name="RefundStoreCode" type="xs:integer"/>
                                                                                                    <xs:element name="RefundTransaction" type="xs:string"/>
                                                                                                    <xs:element name="RefundTill" type="xs:string"/>
                                                                                                    <xs:element minOccurs="0" name="FulfilmentSites">
                                                                                                        <xs:complexType>
                                                                                                            <xs:sequence>
                                                                                                                <xs:element maxOccurs="unbounded" name="FulfilmentSite">
                                                                                                                    <xs:complexType>
                                                                                                                        <xs:all>
                                                                                                                            <xs:element name="FulfilmentSiteCode" type="xs:integer"/>
                                                                                                                            <xs:element name="SellingStoreIBTOutNumber" type="xs:integer"/>
                                                                                                                        </xs:all>
                                                                                                                    </xs:complexType>
                                                                                                                </xs:element>
                                                                                                            </xs:sequence>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                    <xs:element name="RefundLines">
                                                                                                        <xs:complexType>
                                                                                                            <xs:sequence>
                                                                                                                <xs:element maxOccurs="unbounded" name="RefundLine">
                                                                                                                    <xs:complexType>
                                                                                                                        <xs:all>
                                                                                                                            <xs:element name="SellingStoreLineNo" type="xs:integer"/>
                                                                                                                            <xs:element name="OMOrderLineNo" type="xs:integer"/>
                                                                                                                            <xs:element name="ProductCode" type="xs:integer"/>
                                                                                                                            <xs:element name="FulfilmentSiteCode" type="xs:integer"/>
                                                                                                                            <xs:element name="QuantityReturned" type="xs:integer"/>
                                                                                                                            <xs:element name="QuantityCancelled" type="xs:integer"/>
                                                                                                                            <xs:element name="RefundLineValue" type="xs:decimal"/>
                                                                                                                            <xs:element name="RefundLineStatus" type="xs:integer"/>
                                                                                                                        </xs:all>
                                                                                                                    </xs:complexType>
                                                                                                                </xs:element>
                                                                                                            </xs:sequence>
                                                                                                        </xs:complexType>
                                                                                                    </xs:element>
                                                                                                </xs:all>
                                                                                            </xs:complexType>
                                                                                        </xs:element>
                                                                                    </xs:sequence>
                                                                                </xs:complexType>
                                                                            </xs:element>
                                                                        </xs:all>
                                                                    </xs:complexType>
                                                                </xs:element>
                                                            </xs:schema>

    Friend OM_StatusUpdateRequest_InCorrectXSD As XElement = <xs:schema elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
                                                                 <xs:element name="OMOrderReceiveStatusUpdateRequest">
                                                                     <xs:complexType>
                                                                         <xs:all>
                                                                             <xs:element name="DateTimeStampXXXXXXXXXXXXXXXXXXXXXXX">
                                                                                 <xs:complexType>
                                                                                     <xs:simpleContent>
                                                                                         <xs:extension base="xs:dateTime">
                                                                                             <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                         </xs:extension>
                                                                                     </xs:simpleContent>
                                                                                 </xs:complexType>
                                                                             </xs:element>
                                                                             <xs:element name="OMOrderNumber">
                                                                                 <xs:complexType>
                                                                                     <xs:simpleContent>
                                                                                         <xs:extension base="xs:string">
                                                                                             <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                         </xs:extension>
                                                                                     </xs:simpleContent>
                                                                                 </xs:complexType>
                                                                             </xs:element>
                                                                             <xs:element name="FulfilmentSite">
                                                                                 <xs:complexType>
                                                                                     <xs:simpleContent>
                                                                                         <xs:extension base="xs:integer">
                                                                                             <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                         </xs:extension>
                                                                                     </xs:simpleContent>
                                                                                 </xs:complexType>
                                                                             </xs:element>
                                                                             <xs:element name="OrderStatus">
                                                                                 <xs:complexType>
                                                                                     <xs:sequence>
                                                                                         <xs:element name="OrderLines">
                                                                                             <xs:complexType>
                                                                                                 <xs:sequence>
                                                                                                     <xs:element maxOccurs="unbounded" name="OrderLine">
                                                                                                         <xs:complexType>
                                                                                                             <xs:all>
                                                                                                                 <xs:element name="OMOrderLineNo">
                                                                                                                     <xs:complexType>
                                                                                                                         <xs:simpleContent>
                                                                                                                             <xs:extension base="xs:integer">
                                                                                                                                 <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                                                             </xs:extension>
                                                                                                                         </xs:simpleContent>
                                                                                                                     </xs:complexType>
                                                                                                                 </xs:element>
                                                                                                                 <xs:element name="LineStatus">
                                                                                                                     <xs:annotation>
                                                                                                                         <xs:documentation>"Input" - can only contain:
300	All Stock IBT'd Out from fulfilment site
330	IBT Out notification contained invalid data
550	Picking notification recorded at OM
580	Picking status update contained invalid data
750	Despatch notification recorded at OM
780	Despatch status update contained invalid data

"Output" - can only contain:
330	IBT Out notification contained invalid data
400	All stock IBT'd In from fulfilment site
580	Picking status update contained invalid data
600	Picking status update successful
780	Despatch status update contained invalid data
800	Despatch status update successful
</xs:documentation>
                                                                                                                     </xs:annotation>
                                                                                                                     <xs:complexType>
                                                                                                                         <xs:simpleContent>
                                                                                                                             <xs:extension base="xs:integer">
                                                                                                                                 <xs:attribute name="ValidationStatus" type="xs:string" use="optional"/>
                                                                                                                             </xs:extension>
                                                                                                                         </xs:simpleContent>
                                                                                                                     </xs:complexType>
                                                                                                                 </xs:element>
                                                                                                             </xs:all>
                                                                                                         </xs:complexType>
                                                                                                     </xs:element>
                                                                                                 </xs:sequence>
                                                                                             </xs:complexType>
                                                                                         </xs:element>
                                                                                     </xs:sequence>
                                                                                 </xs:complexType>
                                                                             </xs:element>
                                                                         </xs:all>
                                                                     </xs:complexType>
                                                                 </xs:element>
                                                             </xs:schema>

#End Region

#Region "Order Manager Xml Request Examples - Good"

    Friend OM_CreateOrderCorrectRequest As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                       <OMOrderCreateRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                                           <DateTimeStamp>2011-09-13T15:48:07</DateTimeStamp>
                                                           <OrderHeader>
                                                               <Source>WO</Source>
                                                               <SourceOrderNumber>111114</SourceOrderNumber>
                                                               <SellingStoreCode>8080</SellingStoreCode>
                                                               <SellingStoreOrderNumber>17847</SellingStoreOrderNumber>
                                                               <SellingStoreTill>01</SellingStoreTill>
                                                               <SellingStoreTransaction>0021</SellingStoreTransaction>
                                                               <RequiredDeliveryDate>2011-08-09</RequiredDeliveryDate>
                                                               <DeliveryCharge>0.00</DeliveryCharge>
                                                               <TotalOrderValue>192.97</TotalOrderValue>
                                                               <SaleDate>2011-09-13</SaleDate>
                                                               <CustomerAccountNo>69481</CustomerAccountNo>
                                                               <CustomerName>The Doomed</CustomerName>
                                                               <CustomerAddressLine1>102 Inkerman Road</CustomerAddressLine1>
                                                               <CustomerAddressLine2>Knaphill</CustomerAddressLine2>
                                                               <CustomerAddressTown>Woking</CustomerAddressTown>
                                                               <CustomerAddressLine4>Surrey</CustomerAddressLine4>
                                                               <CustomerPostcode>GU21 2WB</CustomerPostcode>
                                                               <DeliveryAddressLine1>102 Inkerman Road</DeliveryAddressLine1>
                                                               <DeliveryAddressLine2>Knaphill</DeliveryAddressLine2>
                                                               <DeliveryAddressTown>Woking</DeliveryAddressTown>
                                                               <DeliveryAddressLine4>Surrey</DeliveryAddressLine4>
                                                               <DeliveryPostcode>GU21 2WB</DeliveryPostcode>
                                                               <ContactPhoneHome>01483 489801</ContactPhoneHome>
                                                               <ContactPhoneMobile>07584 491292</ContactPhoneMobile>
                                                               <ContactPhoneWork>01483 489801</ContactPhoneWork>
                                                               <ContactEmail>kevan.madelin@wickes.co.uk</ContactEmail>
                                                               <DeliveryInstructions>
                                                                   <InstructionLine>
                                                                       <LineNo>1</LineNo>
                                                                       <LineText>Deliver before riots</LineText>
                                                                   </InstructionLine>
                                                               </DeliveryInstructions>
                                                               <ToBeDelivered>true</ToBeDelivered>
                                                               <ExtendedLeadTime>true</ExtendedLeadTime>
                                                               <DeliveryContactName>The Forky Driver</DeliveryContactName>
                                                               <DeliveryContactPhone>01483 489801</DeliveryContactPhone>
                                                           </OrderHeader>
                                                           <OrderLines>
                                                               <OrderLine>
                                                                   <SourceOrderLineNo>1</SourceOrderLineNo>
                                                                   <SellingStoreLineNo>0001</SellingStoreLineNo>
                                                                   <ProductCode>190234</ProductCode>
                                                                   <ProductDescription>Nitrile Safety Wellington 10 Black</ProductDescription>
                                                                   <TotalOrderQuantity>2</TotalOrderQuantity>
                                                                   <QuantityTaken>0</QuantityTaken>
                                                                   <UOM>EACH</UOM>
                                                                   <LineValue>29.98</LineValue>
                                                                   <DeliveryChargeItem>false</DeliveryChargeItem>
                                                                   <SellingPrice>14.99</SellingPrice>
                                                               </OrderLine>
                                                               <OrderLine>
                                                                   <SourceOrderLineNo>2</SourceOrderLineNo>
                                                                   <SellingStoreLineNo>0002</SellingStoreLineNo>
                                                                   <ProductCode>189571</ProductCode>
                                                                   <ProductDescription>Chopin Arch</ProductDescription>
                                                                   <TotalOrderQuantity>1</TotalOrderQuantity>
                                                                   <QuantityTaken>0</QuantityTaken>
                                                                   <UOM>EACH</UOM>
                                                                   <LineValue>162.99</LineValue>
                                                                   <DeliveryChargeItem>false</DeliveryChargeItem>
                                                                   <SellingPrice>162.99</SellingPrice>
                                                               </OrderLine>
                                                           </OrderLines>
                                                       </OMOrderCreateRequest>

    Friend OM_CreateRefundCorrectRequest As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                        <OMOrderRefundRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                                            <DateTimeStamp>2011-09-13T16:37:37</DateTimeStamp>
                                                            <OMOrderNumber>0</OMOrderNumber>
                                                            <OrderRefunds>
                                                                <OrderRefund>
                                                                    <RefundDate>2011-09-13T00:00:00</RefundDate>
                                                                    <RefundStoreCode>8080</RefundStoreCode>
                                                                    <RefundTransaction>0022</RefundTransaction>
                                                                    <RefundTill>01</RefundTill>
                                                                    <RefundLines>
                                                                        <RefundLine>
                                                                            <SellingStoreLineNo>0001</SellingStoreLineNo>
                                                                            <OMOrderLineNo>0001</OMOrderLineNo>
                                                                            <ProductCode>190234</ProductCode>
                                                                            <FulfilmentSiteCode>0</FulfilmentSiteCode>
                                                                            <QuantityReturned>0</QuantityReturned>
                                                                            <QuantityCancelled>1</QuantityCancelled>
                                                                            <RefundLineValue>14.99</RefundLineValue>
                                                                            <RefundLineStatus>110</RefundLineStatus>
                                                                        </RefundLine>
                                                                    </RefundLines>
                                                                </OrderRefund>
                                                            </OrderRefunds>
                                                        </OMOrderRefundRequest>

    Friend OM_StatusUpdateCorrectRequest As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                        <OMOrderReceiveStatusUpdateRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                                            <DateTimeStamp>2011-09-14T08:59:50</DateTimeStamp>
                                                            <OMOrderNumber>0</OMOrderNumber>
                                                            <FulfilmentSite>8080</FulfilmentSite>
                                                            <OrderStatus>
                                                                <OrderLines>
                                                                    <OrderLine>
                                                                        <OMOrderLineNo>0001</OMOrderLineNo>
                                                                        <LineStatus>530</LineStatus>
                                                                    </OrderLine>
                                                                    <OrderLine>
                                                                        <OMOrderLineNo>0002</OMOrderLineNo>
                                                                        <LineStatus>530</LineStatus>
                                                                    </OrderLine>
                                                                </OrderLines>
                                                            </OrderStatus>
                                                        </OMOrderReceiveStatusUpdateRequest>

#End Region

#Region "Order Manager Xml Request Examples - Bad"

    Friend OM_CreateOrderInCorrectRequest As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                         <OMOrderCreateRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                                             <DateTimeStampZZZZ>2011-09-13T15:48:07</DateTimeStampZZZZ>
                                                             <OrderHeaderYYYY>
                                                                 <Source>WO</Source>
                                                                 <SourceOrderNumber>111114</SourceOrderNumber>
                                                                 <SellingStoreCode>8080</SellingStoreCode>
                                                                 <SellingStoreOrderNumber>17847</SellingStoreOrderNumber>
                                                                 <SellingStoreTill>01</SellingStoreTill>
                                                                 <SellingStoreTransaction>0021</SellingStoreTransaction>
                                                                 <RequiredDeliveryDate>2011-08-09</RequiredDeliveryDate>
                                                                 <DeliveryCharge>0.00</DeliveryCharge>
                                                                 <TotalOrderValue>192.97</TotalOrderValue>
                                                                 <SaleDate>2011-09-13</SaleDate>
                                                                 <CustomerAccountNo>69481</CustomerAccountNo>
                                                                 <CustomerName>The Doomed</CustomerName>
                                                                 <CustomerAddressLine1>102 Inkerman Road</CustomerAddressLine1>
                                                                 <CustomerAddressLine2>Knaphill</CustomerAddressLine2>
                                                                 <CustomerAddressTown>Woking</CustomerAddressTown>
                                                                 <CustomerAddressLine4>Surrey</CustomerAddressLine4>
                                                                 <CustomerPostcode>GU21 2WB</CustomerPostcode>
                                                                 <DeliveryAddressLine1>102 Inkerman Road</DeliveryAddressLine1>
                                                                 <DeliveryAddressLine2>Knaphill</DeliveryAddressLine2>
                                                                 <DeliveryAddressTown>Woking</DeliveryAddressTown>
                                                                 <DeliveryAddressLine4>Surrey</DeliveryAddressLine4>
                                                                 <DeliveryPostcode>GU21 2WB</DeliveryPostcode>
                                                                 <ContactPhoneHome>01483 489801</ContactPhoneHome>
                                                                 <ContactPhoneMobile>07584 491292</ContactPhoneMobile>
                                                                 <ContactPhoneWork>01483 489801</ContactPhoneWork>
                                                                 <ContactEmail>kevan.madelin@wickes.co.uk</ContactEmail>
                                                                 <DeliveryInstructions>
                                                                     <InstructionLine>
                                                                         <LineNo>1</LineNo>
                                                                         <LineText>Deliver before riots</LineText>
                                                                     </InstructionLine>
                                                                 </DeliveryInstructions>
                                                                 <ToBeDelivered>true</ToBeDelivered>
                                                                 <ExtendedLeadTime>true</ExtendedLeadTime>
                                                                 <DeliveryContactName>The Forky Driver</DeliveryContactName>
                                                                 <DeliveryContactPhone>01483 489801</DeliveryContactPhone>
                                                             </OrderHeaderYYYY>
                                                             <OrderLines>
                                                                 <OrderLine>
                                                                     <SourceOrderLineNo>1</SourceOrderLineNo>
                                                                     <SellingStoreLineNo>0001</SellingStoreLineNo>
                                                                     <ProductCode>190234</ProductCode>
                                                                     <ProductDescription>Nitrile Safety Wellington 10 Black</ProductDescription>
                                                                     <TotalOrderQuantity>2</TotalOrderQuantity>
                                                                     <QuantityTaken>0</QuantityTaken>
                                                                     <UOM>EACH</UOM>
                                                                     <LineValue>29.98</LineValue>
                                                                     <DeliveryChargeItem>false</DeliveryChargeItem>
                                                                     <SellingPrice>14.99</SellingPrice>
                                                                 </OrderLine>
                                                                 <OrderLine>
                                                                     <SourceOrderLineNo>2</SourceOrderLineNo>
                                                                     <SellingStoreLineNo>0002</SellingStoreLineNo>
                                                                     <ProductCode>189571</ProductCode>
                                                                     <ProductDescription>Chopin Arch</ProductDescription>
                                                                     <TotalOrderQuantity>1</TotalOrderQuantity>
                                                                     <QuantityTaken>0</QuantityTaken>
                                                                     <UOM>EACH</UOM>
                                                                     <LineValue>162.99</LineValue>
                                                                     <DeliveryChargeItem>false</DeliveryChargeItem>
                                                                     <SellingPrice>162.99</SellingPrice>
                                                                 </OrderLine>
                                                             </OrderLines>
                                                         </OMOrderCreateRequest>

    Friend OM_CreateRefundInCorrectRequest As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                          <OMOrderRefundRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                                              <DateTimeStampXXXXXXXXXXXXXXXXXXXXXXX>2011-09-13T16:37:37</DateTimeStampXXXXXXXXXXXXXXXXXXXXXXX>
                                                              <OMOrderNumberYYYY>0</OMOrderNumberYYYY>
                                                              <OrderRefunds>
                                                                  <OrderRefund>
                                                                      <RefundDate>2011-09-13T00:00:00</RefundDate>
                                                                      <RefundStoreCode>8080</RefundStoreCode>
                                                                      <RefundTransaction>0022</RefundTransaction>
                                                                      <RefundTill>01</RefundTill>
                                                                      <RefundLines>
                                                                          <RefundLine>
                                                                              <SellingStoreLineNo>0001</SellingStoreLineNo>
                                                                              <OMOrderLineNo>0001</OMOrderLineNo>
                                                                              <ProductCode>190234</ProductCode>
                                                                              <FulfilmentSiteCode>0</FulfilmentSiteCode>
                                                                              <QuantityReturned>0</QuantityReturned>
                                                                              <QuantityCancelled>1</QuantityCancelled>
                                                                              <RefundLineValue>14.99</RefundLineValue>
                                                                              <RefundLineStatus>110</RefundLineStatus>
                                                                          </RefundLine>
                                                                      </RefundLines>
                                                                  </OrderRefund>
                                                              </OrderRefunds>
                                                          </OMOrderRefundRequest>

    Friend OM_StatusUpdateInCorrectRequest As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                          <OMOrderReceiveStatusUpdateRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                                              <DateTimeStampXXXXXXXXXXXXXXXXXXXXXXX>2011-09-14T08:59:50</DateTimeStampXXXXXXXXXXXXXXXXXXXXXXX>
                                                              <OMOrderNumberYYYY>0</OMOrderNumberYYYY>
                                                              <FulfilmentSite>8080</FulfilmentSite>
                                                              <OrderStatus>
                                                                  <OrderLines>
                                                                      <OrderLine>
                                                                          <OMOrderLineNo>0001</OMOrderLineNo>
                                                                          <LineStatus>530</LineStatus>
                                                                      </OrderLine>
                                                                      <OrderLine>
                                                                          <OMOrderLineNo>0002</OMOrderLineNo>
                                                                          <LineStatus>530</LineStatus>
                                                                      </OrderLine>
                                                                  </OrderLines>
                                                              </OrderStatus>
                                                          </OMOrderReceiveStatusUpdateRequest>

#End Region

End Module
