﻿<TestClass()> Public Class GlobalStuffUnitTestModule

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test: Module"

    <TestMethod()> Public Sub GlobalStuff_Function_OrderNumberCollection_Parameter_Ignore_CheckVendaOrderNoKeyExist()

        Dim Value As New Dictionary(Of String, String)

        Value = SaleOrderMonitor.TpWickes.GlobalStuff.OrderNumberCollection("", "", 0)

        Assert.IsTrue(Value.ContainsKey("Venda Order No"))

    End Sub

    <TestMethod()> Public Sub GlobalStuff_Function_OrderNumberCollection_Parameter_Ignore_CheckStoreOrderNoKeyExist()

        Dim Value As New Dictionary(Of String, String)

        Value = SaleOrderMonitor.TpWickes.GlobalStuff.OrderNumberCollection("", "", 0)

        Assert.IsTrue(Value.ContainsKey("Store Order No"))

    End Sub

    <TestMethod()> Public Sub GlobalStuff_Function_OrderNumberCollection_Parameter_Ignore_CheckOrderManagerOrderNoKeyExist()

        Dim Value As New Dictionary(Of String, String)

        Value = SaleOrderMonitor.TpWickes.GlobalStuff.OrderNumberCollection("", "", 0)

        Assert.IsTrue(Value.ContainsKey("Order Manager Order No"))

    End Sub

#End Region

End Class
