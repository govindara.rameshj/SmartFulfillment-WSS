﻿<TestClass()> Public Class LogSaleOrderMonitorInvalidXmlUnitTestClass

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test: Class"

#Region "Requirement"

    <TestMethod()> Public Sub RequirementEnabled_Parameter_Missing_ReturnExistingImplementation()

        Dim Log As ILogSaleOrderMonitorInvalidXml = Nothing

        Arrange(Log, New System.Nullable(Of Boolean), Nothing, Nothing)
        Assert.IsTrue(Log.GetType.FullName = "SaleOrderMonitor.TpWickes.LogSaleOrderMonitorInvalidXmlEmpty")

    End Sub

    <TestMethod()> Public Sub RequirementEnabled_Parameter_False_ReturnExistingImplementation()

        Dim Log As ILogSaleOrderMonitorInvalidXml = Nothing

        Arrange(Log, False, Nothing, Nothing)
        Assert.IsTrue(Log.GetType.FullName = "SaleOrderMonitor.TpWickes.LogSaleOrderMonitorInvalidXmlEmpty")

    End Sub

    <TestMethod()> Public Sub RequirementEnabled_Parameter_True_ReturnNewImplementation()

        Dim Log As ILogSaleOrderMonitorInvalidXml = Nothing

        Arrange(Log, True, Nothing, Nothing)
        Assert.IsTrue(Log.GetType.FullName = "SaleOrderMonitor.TpWickes.LogSaleOrderMonitorInvalidXml")

    End Sub

#End Region

#Region "Save XML Data"

    <TestMethod()> Public Sub LogSaleOrderMonitorInvalidXml_Function_SaveXmlData_Parameter_Missing_ReturnFalse()

        Dim NewVersion As New LogSaleOrderMonitorInvalidXml

        ConfigureStub(True, New System.Nullable(Of Boolean), Nothing)
        Assert.AreEqual(False, NewVersion.SaveXmlData)

    End Sub

    <TestMethod()> Public Sub LogSaleOrderMonitorInvalidXml_Function_SaveXmlData_Parameter_False_ReturnFalse()

        Dim NewVersion As New LogSaleOrderMonitorInvalidXml

        ConfigureStub(True, False, Nothing)
        Assert.AreEqual(False, NewVersion.SaveXmlData)

    End Sub

    <TestMethod()> Public Sub LogSaleOrderMonitorInvalidXml_Function_SaveXmlData_Parameter_True_ReturnTue()

        Dim NewVersion As New LogSaleOrderMonitorInvalidXml

        ConfigureStub(True, True, Nothing)
        Assert.AreEqual(True, NewVersion.SaveXmlData)

    End Sub

#End Region

#End Region

#Region "Private Procedures And Functions"

    Private Sub Arrange(ByRef OM As ILogSaleOrderMonitorInvalidXml, ByVal RequirementEnabled As System.Nullable(Of Boolean), _
                        ByVal LogXmlData As System.Nullable(Of Boolean), ByVal ID As System.Nullable(Of Integer))

        Dim Stub As New LogSaleOrderMonitorInvalidXmlRepositoryStub

        Stub.ConfigureStub(RequirementEnabled, LogXmlData, ID)

        LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactorySet(Stub)

        OM = LogSaleOrderMonitorInvalidXmlFactory.FactoryGet

    End Sub

    Private Sub ConfigureStub(ByVal RequirementEnabled As System.Nullable(Of Boolean), _
                              ByVal LogXmlData As System.Nullable(Of Boolean), ByVal ID As System.Nullable(Of Integer))

        Dim Stub As New LogSaleOrderMonitorInvalidXmlRepositoryStub

        Stub.ConfigureStub(RequirementEnabled, LogXmlData, ID)

        LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactorySet(Stub)

    End Sub

#End Region

End Class
