﻿<TestClass()> Public Class OrderManagerUnitTestClass

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test: All Order Manager Classes"

#Region "Requirement: All Order Manager Classes"

    <TestMethod()> Public Sub Requirement_Parameter_ValidationMissing_LoggingMissing_ReturnExistingImplementation()

        Dim OM As IOrderManager = Nothing

        Arrange(OM, Nothing, Nothing)

        Assert.IsTrue(OM.GetType.FullName = "SaleOrderMonitor.TpWickes.OrderManagerCurrent")

    End Sub

    <TestMethod()> Public Sub Requirement_Parameter_ValidationFalse_LoggingMissing_ReturnExistingImplementation()

        Dim OM As IOrderManager = Nothing

        Arrange(OM, False, Nothing)
        Assert.IsTrue(OM.GetType.FullName = "SaleOrderMonitor.TpWickes.OrderManagerCurrent")

    End Sub

    <TestMethod()> Public Sub Requirement_Parameter_ValidationTrue_LoggingMissing_ReturnNewValidatedImplementation()

        Dim OM As IOrderManager = Nothing

        Arrange(OM, True, Nothing)
        Assert.IsTrue(OM.GetType.FullName = "SaleOrderMonitor.TpWickes.OrderManagerValidatedVersion")

    End Sub

    <TestMethod()> Public Sub Requirement_Parameter_ValidationTrue_LoggingFalse_ReturnNewValidatedImplementation()

        Dim OM As IOrderManager = Nothing

        Arrange(OM, True, False)
        Assert.IsTrue(OM.GetType.FullName = "SaleOrderMonitor.TpWickes.OrderManagerValidatedVersion")

    End Sub

    <TestMethod()> Public Sub Requirement_Parameter_ValidationTrue_LoggingTrue_ReturnNewValidatedAndLoggedImplementation()

        Dim OM As IOrderManager = Nothing

        Arrange(OM, True, True)
        Assert.IsTrue(OM.GetType.FullName = "SaleOrderMonitor.TpWickes.OrderManagerValidatedLoggingVersion")

    End Sub

    <TestMethod()> Public Sub Requirement_Parameter_ValidationFalse_LoggingTrue_ReturnExistingImplementation()

        Dim OM As IOrderManager = Nothing

        Arrange(OM, False, True)
        Assert.IsTrue(OM.GetType.FullName = "SaleOrderMonitor.TpWickes.OrderManagerCurrent")

    End Sub

#End Region

#Region "Class: OrderManagerValidatedVersion"

#Region "Function: OrderManagerServiceClassName"

    <TestMethod()> Public Sub Function_OrderManagerServiceClassName_Parameter_CreateService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New CreateService

        Assert.AreEqual(NewVersion.OrderManagerServiceClassName(OM), "Cts.Oasys.Hubs.Core.Order.WebService.CreateService")

    End Sub

    <TestMethod()> Public Sub Function_OrderManagerServiceClassName_Parameter_RefundService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New RefundService

        Assert.AreEqual(NewVersion.OrderManagerServiceClassName(OM), "Cts.Oasys.Hubs.Core.Order.WebService.RefundService")

    End Sub

    <TestMethod()> Public Sub Function_OrderManagerServiceClassName_Parameter_UpdateService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New UpdateService

        Assert.AreEqual(NewVersion.OrderManagerServiceClassName(OM), "Cts.Oasys.Hubs.Core.Order.WebService.UpdateService")

    End Sub

#End Region

#Region "Function: OrderManagerRequestReturnErrorValue"

    <TestMethod()> Public Sub Function_OrderManagerRequestReturnErrorValue_Parameter_CreateService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New CreateService

        Assert.AreEqual(NewVersion.OrderManagerRequestReturnErrorValue(OM), "Failed Validation - Order")

    End Sub

    <TestMethod()> Public Sub Function_OrderManagerRequestReturnErrorValue_Parameter_RefundService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New RefundService

        Assert.AreEqual(NewVersion.OrderManagerRequestReturnErrorValue(OM), "Failed Validation - Refund")

    End Sub

    <TestMethod()> Public Sub Function_OrderManagerRequestReturnErrorValue_Parameter_UpdateService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New UpdateService

        Assert.AreEqual(NewVersion.OrderManagerRequestReturnErrorValue(OM), "Failed Validation - Update")

    End Sub

#End Region

#Region "Function: OrderManagerServiceType"

    <TestMethod()> Public Sub Function_OrderManagerServiceType_Parameter_CreateService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New CreateService

        Assert.AreEqual(NewVersion.OrderManagerServiceType(OM), WebServiceType.CreateOrder)

    End Sub

    <TestMethod()> Public Sub Function_OrderManagerServiceType_Parameter_RefundService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New RefundService

        Assert.AreEqual(NewVersion.OrderManagerServiceType(OM), WebServiceType.RefundOrder)

    End Sub

    <TestMethod()> Public Sub Function_OrderManagerServiceType_Parameter_UpdateService_ValidMatch()

        Dim NewVersion As New OrderManagerValidatedVersion

        Dim OM As BaseService = New UpdateService

        Assert.AreEqual(NewVersion.OrderManagerServiceType(OM), WebServiceType.StatusUpdate)

    End Sub

#End Region

#End Region

#Region "Class: OrderManagerValidatedLoggingVersion"

#Region "Function: ExtractStoreOrderNo"

    <TestMethod()> Public Sub Function_ExtractStoreOrderNo_Parameter_Nothing_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Assert.AreEqual(String.Empty, NewVersion.ExtractStoreOrderNo(Nothing))

    End Sub

    <TestMethod()> Public Sub Function_ExtractStoreOrderNo_Parameter_MissingKey_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("XXXX", "")

        Assert.AreEqual(String.Empty, NewVersion.ExtractStoreOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractStoreOrderNo_Parameter_ValidKey_Value_Nothing_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Store Order No", Nothing)

        Assert.AreEqual(String.Empty, NewVersion.ExtractStoreOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractStoreOrderNo_Parameter_ValidKey_Value_EmptyString_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Store Order No", "")

        Assert.AreEqual(String.Empty, NewVersion.ExtractStoreOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractStoreOrderNo_Parameter_ValidKey_Value_NotEmptyString_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Store Order No", "A")

        Assert.AreEqual("A", NewVersion.ExtractStoreOrderNo(Value))

    End Sub

#End Region

#Region "Function: ExtractOrderManagerOrderNo"

    <TestMethod()> Public Sub Function_ExtractOrderManagerOrderNo_Parameter_Nothing_ReturnNothing()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Assert.AreEqual(Nothing, NewVersion.ExtractOrderManagerOrderNo(Nothing))

    End Sub

    <TestMethod()> Public Sub Function_ExtractOrderManagerOrderNo_Parameter_MissingKey_ReturnNothing()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("XXXX", "")

        Assert.AreEqual(Nothing, NewVersion.ExtractOrderManagerOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractOrderManagerOrderNo_Parameter_ValidKey_Value_Nothing_ReturnNothing()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Order Manager Order No", Nothing)

        Assert.AreEqual(Nothing, NewVersion.ExtractOrderManagerOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractOrderManagerOrderNo_Parameter_ValidKey_Value_EmptyString_ReturnNothing()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Order Manager Order No", "")

        Assert.AreEqual(Nothing, NewVersion.ExtractOrderManagerOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractOrderManagerOrderNo_Parameter_ValidKey_Value_NotEmptyString_NonNumeric_ReturnNothing()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Order Manager Order No", "A")

        Assert.AreEqual(Nothing, NewVersion.ExtractOrderManagerOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractOrderManagerOrderNo_Parameter_ValidKey_Value_NotEmptyString_Numeric_ReturnValue()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Order Manager Order No", "123456")

        Assert.AreEqual(123456, NewVersion.ExtractOrderManagerOrderNo(Value))

    End Sub

#End Region

#Region "Function: ExtractVendaOrderNo"

    <TestMethod()> Public Sub Function_ExtractVendaOrderNo_Parameter_Nothing_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Assert.AreEqual(String.Empty, NewVersion.ExtractVendaOrderNo(Nothing))

    End Sub

    <TestMethod()> Public Sub Function_ExtractVendaOrderNo_Parameter_MissingKey_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("XXXX", "")

        Assert.AreEqual(String.Empty, NewVersion.ExtractVendaOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractVendaOrderNo_Parameter_ValidKey_Value_Nothing_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Venda Order No", Nothing)

        Assert.AreEqual(String.Empty, NewVersion.ExtractVendaOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractVendaOrderNo_Parameter_ValidKey_Value_EmptyString_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Venda Order No", "")

        Assert.AreEqual(String.Empty, NewVersion.ExtractVendaOrderNo(Value))

    End Sub

    <TestMethod()> Public Sub Function_ExtractVendaOrderNo_Parameter_ValidKey_Value_NotEmptyString_ReturnEmptyString()

        Dim NewVersion As New OrderManagerValidatedLoggingVersion

        Dim Value As New Dictionary(Of String, String)

        Value.Add("Venda Order No", "A")

        Assert.AreEqual("A", NewVersion.ExtractVendaOrderNo(Value))

    End Sub

#End Region

#Region "Function: OrderManagerRequest"

    'Using test harnes to check the "logging of bad xml requests" to the database without running SOMonitor

    'Imports TpWickes.Hubs.Library

    '<TestMethod()> Public Sub OrderManager_Function_OrderManagerRequest_CreateService()

    '    Dim OrderManager As IOrderManager
    '    Dim Om As BaseService = New CreateService
    '    Dim KeyValue As New Dictionary(Of String, String)

    '    'arrange
    '    StubOutDependencyToConnectionXmlFile(System.AppDomain.CurrentDomain.BaseDirectory)
    '    ConfigureOrderManagerRepositoryStub(True)                        ' run new functionality
    '    OrderManager = OrderManagerFactory.FactoryGet

    '    'act
    '    KeyValue.Add("Venda Order No", Nothing)
    '    KeyValue.Add("Store Order No", "123456")
    '    KeyValue.Add("Order Manager Order No", Nothing)

    '    OrderManager.OrderManagerRequest(Om, OM_CreateOrderInCorrectRequest.ToString, KeyValue)

    'End Sub

#End Region

#End Region

#End Region

#Region "Private Procedures And Functions"

    Private Sub Arrange(ByRef OM As IOrderManager, _
                        ByVal RequirementEnabled As System.Nullable(Of Boolean), ByVal XmlErrorLoggingRequirementEnabled As System.Nullable(Of Boolean))

        Dim OrderManagerStub As New OrderManagerRepositoryStub
        Dim LoggingStub As New LogSaleOrderMonitorInvalidXmlRepositoryStub

        OrderManagerStub.ConfigureStub(RequirementEnabled)
        OrderManagerRepositoryFactory.FactorySet(OrderManagerStub)

        LoggingStub.ConfigureStub(XmlErrorLoggingRequirementEnabled, Nothing, Nothing)
        LogSaleOrderMonitorInvalidXmlRepositoryFactory.FactorySet(LoggingStub)

        OM = OrderManagerFactory.FactoryGet

    End Sub

    Private Sub ConfigureXmlValidationStub(ByVal ValidateXML As Boolean, ByVal XmlFailureReason As List(Of String))

        Dim Stub As New XmlValidationStub

        Stub.ConfigureStub(ValidateXML, XmlFailureReason)

        XmlValidationFactory.FactorySet(Stub)

    End Sub

    Private Sub ConfigureOrderManagerRepositoryStub(ByVal RequirementEnabled As Boolean)

        Dim Stub As New OrderManagerRepositoryStub

        Stub.ConfigureStub(RequirementEnabled)

        OrderManagerRepositoryFactory.FactorySet(Stub)

    End Sub

#End Region

End Class
