﻿<TestClass()> Public Class XmlValidationUnitTestClass

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test: All Xml Validation Classes"

#Region "Requirement: "


#End Region

    <TestMethod()> Public Sub XmlValidation_Function_XsdPathLocation_InValidPath_InvalidMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, "D:\Resources\XSDs\", Nothing)

        Assert.AreNotEqual(NewVersion.XsdPathLocation(), "C:\Resources\XSDs\")

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_XsdPathLocation_XSDFilesTag_ValidPath_ValidMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, "C:\Resources\XSDs\", Nothing)

        Assert.AreEqual(NewVersion.XsdPathLocation(), "C:\Resources\XSDs\")

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_XSDFileName_Parameter_CreateService_ValidMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, "Test.xsd", String.Empty, String.Empty, String.Empty, Nothing)

        Assert.AreEqual(NewVersion.XSDFileName(WebServiceType.CreateOrder), "Test.xsd")

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_XSDFileName_Parameter_RefundService_ValidMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, "Test.xsd", String.Empty, String.Empty, Nothing)

        Assert.AreEqual(NewVersion.XSDFileName(WebServiceType.RefundOrder), "Test.xsd")

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_XSDFileName_Parameter_UpdateService_ValidMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, "Test.xsd", String.Empty, Nothing)

        Assert.AreEqual(NewVersion.XSDFileName(WebServiceType.StatusUpdate), "Test.xsd")

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_GetXSD_Parameter_Ignore_TestCreateOrder_XSDMisMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_CreateOrderRequest_CorrectXSD)

        Assert.AreNotEqual(NewVersion.GetXSD(String.Empty, String.Empty), OM_CreateOrderRequest_InCorrectXSD)

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_GetXSD_Parameter_Ignore_TestCreateOrder_XSDMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_CreateOrderRequest_CorrectXSD)

        Assert.AreEqual(NewVersion.GetXSD(String.Empty, String.Empty), OM_CreateOrderRequest_CorrectXSD)

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_GetXSD_Parameter_Ignore_TestCreateRefund_XSDMisMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_RefundOrderRequest_CorrectXSD)

        Assert.AreNotEqual(NewVersion.GetXSD(String.Empty, String.Empty), OM_RefundOrderRequest_InCorrectXSD)

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_GetXSD_Parameter_Ignore_TestCreateRefund_XSDMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_RefundOrderRequest_CorrectXSD)

        Assert.AreEqual(NewVersion.GetXSD(String.Empty, String.Empty), OM_RefundOrderRequest_CorrectXSD)

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_GetXSD_Parameter_Ignore_TestStatusUpdate_XSDMisMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_StatusUpdateRequest_CorrectXSD)

        Assert.AreNotEqual(NewVersion.GetXSD(String.Empty, String.Empty), OM_StatusUpdateRequest_InCorrectXSD)

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_GetXSD_Parameter_Ignore_TestStatusUpdate_XSDMatch()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_StatusUpdateRequest_CorrectXSD)

        Assert.AreEqual(NewVersion.GetXSD(String.Empty, String.Empty), OM_StatusUpdateRequest_CorrectXSD)

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_Validate_CreateOrder_XmlFailure()

        Dim NewVersion As New XmlValidation

        Assert.IsFalse(NewVersion.Validate(OM_CreateOrderRequest_CorrectXSD, OM_CreateOrderInCorrectRequest))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_Validate_CreateOrder_XmlSucess()

        Dim NewVersion As New XmlValidation

        Assert.IsTrue(NewVersion.Validate(OM_CreateOrderRequest_CorrectXSD, OM_CreateOrderCorrectRequest))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_Validate_CreateRefund_XmlFailure()

        Dim NewVersion As New XmlValidation

        Assert.IsFalse(NewVersion.Validate(OM_RefundOrderRequest_CorrectXSD, OM_CreateRefundInCorrectRequest))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_Validate_CrateRefund_XmlSucess()

        Dim NewVersion As New XmlValidation

        Assert.IsTrue(NewVersion.Validate(OM_RefundOrderRequest_CorrectXSD, OM_CreateRefundCorrectRequest))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_Validate_StatusUpdate_XmlFailure()

        Dim NewVersion As New XmlValidation

        Assert.IsFalse(NewVersion.Validate(OM_StatusUpdateRequest_CorrectXSD, OM_StatusUpdateInCorrectRequest))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_Validate_StatusUpdate_XmlSucess()

        Dim NewVersion As New XmlValidation

        Assert.IsTrue(NewVersion.Validate(OM_StatusUpdateRequest_CorrectXSD, OM_StatusUpdateCorrectRequest))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_ValidateXML_Parameters_BadXml_CreateOrder_XmlFailure()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_CreateOrderRequest_CorrectXSD)

        Assert.IsFalse(NewVersion.ValidateXML(OM_CreateOrderInCorrectRequest.ToString, WebServiceType.CreateOrder))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_ValidateXML_Parameters_GoodXml_CreateOrder_XmlSucess()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_CreateOrderRequest_CorrectXSD)

        Assert.IsTrue(NewVersion.ValidateXML(OM_CreateOrderCorrectRequest.ToString, WebServiceType.CreateOrder))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_ValidateXML_Parameters_BadXml_RefundCreate_XmlFailure()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_RefundOrderRequest_CorrectXSD)

        Assert.IsFalse(NewVersion.ValidateXML(OM_CreateRefundInCorrectRequest.ToString, WebServiceType.RefundOrder))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_ValidateXML_Parameters_GoodXml_RefundCreate_XmlSucess()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_RefundOrderRequest_CorrectXSD)

        Assert.IsTrue(NewVersion.ValidateXML(OM_CreateRefundCorrectRequest.ToString, WebServiceType.RefundOrder))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_ValidateXML_Parameters_BadXml_StatusUpdate_XmlFailure()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_StatusUpdateRequest_CorrectXSD)

        Assert.IsFalse(NewVersion.ValidateXML(OM_StatusUpdateInCorrectRequest.ToString, WebServiceType.StatusUpdate))

    End Sub

    <TestMethod()> Public Sub XmlValidation_Function_ValidateXML_Parameters_GoodXml_StatusUpdate_XmlSucess()

        Dim XmlConfig As IXmlConfig = Nothing
        Dim NewVersion As New XmlValidation

        Arrange(XmlConfig, String.Empty, String.Empty, String.Empty, String.Empty, OM_StatusUpdateRequest_CorrectXSD)

        Assert.IsTrue(NewVersion.ValidateXML(OM_StatusUpdateCorrectRequest.ToString, WebServiceType.StatusUpdate))

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub Arrange(ByRef XmlConfig As IXmlConfig, _
                        ByVal CreateOrder As String, ByVal CreateRefund As String, _
                        ByVal StatusUpdate As String, ByVal Path As String, ByVal XSD As XElement)

        Dim Stub As New XmlConfigStub

        Stub.ConfigureStub(CreateOrder, CreateRefund, StatusUpdate, Path, XSD)

        XmlConfigFactory.FactorySet(Stub)
        XmlConfig = XmlConfigFactory.FactoryGet

    End Sub

#End Region

End Class
