using System;
using NUnit.Framework;
using Rhino.Mocks;
using System.Net;
using RestSharp;
using WSS.BO.RemoteServices.Common.Client;
using WSS.BO.RemoteServices.ProxyService.Contract.Capacity;
using WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification;

namespace WSS.BO.RemoteServices.ProxyService.Client.NUnit.UnitTest
{
    [TestFixture]
    public class ProxyServiceClientTests
    {
        private RestClient _clientMock;
        private ProxyServiceClient _apiFacade;

        [SetUp]
        public void CreateObjects()
        {
            _clientMock = MockRepository.GenerateMock<RestClient>();
            _clientMock.Stub(x => x.BaseUrl).Return(new Uri("http://example.com"));
            _apiFacade = new ProxyServiceClient(string.Empty, _clientMock);
        }

        [Test]
        public void SuccessCapacityRequestThrowsNoError()
        {
            StubExecuteMethod(HttpStatusCode.OK);

            Assert.DoesNotThrow(() => _apiFacade.AllocateCapacity(CreateAllocationDetails()));
        }

        [Test]
        public void FailedCapacityRequestThrowsError()
        {
            StubExecuteMethod(HttpStatusCode.BadRequest);

            Assert.Throws(typeof(BaseServiceClientException), () => _apiFacade.AllocateCapacity(CreateAllocationDetails()));
        }

        [Test]
        public void SuccessNewConsignmentRequestThrowsNoError()
        {
            StubExecuteMethod(HttpStatusCode.Created);

            Assert.DoesNotThrow(() => _apiFacade.NewConsignment(CreateConsignmentDetails()));
        }

        [Test]
        public void SuccessUpdateConsignmentRequestThrowsNoError()
        {
            StubExecuteMethod(HttpStatusCode.OK);

            Assert.DoesNotThrow(() => _apiFacade.UpdateConsignment(CreateConsignmentDetails()));
        }

        [Test]
        public void SuccessCancelConsignmentRequestThrowsNoError()
        {
            StubExecuteMethod(HttpStatusCode.OK);

            Assert.DoesNotThrow(() => _apiFacade.CancelConsignment(CreateConsignmentDetails()));
        }

        [Test]
        public void FailedConsignmentRequestThrowsError()
        {
            StubExecuteMethod(HttpStatusCode.ExpectationFailed);

            var ex = (BaseServiceClientException)Assert.Throws(typeof(BaseServiceClientException), () => _apiFacade.NewConsignment(CreateConsignmentDetails()));
            Assert.That(ex.Severity, Is.EqualTo(BaseServiceClientException.ErrorSeverity.Normal));
        }

        [Test]
        public void BadRequestFailedConsignmentRequestThrowsError()
        {
            SevereFailedConsignmentRequestThrowsError(HttpStatusCode.BadRequest);
        }

        [Test]
        public void NotFoundFailedConsignmentRequestThrowsError()
        {
            SevereFailedConsignmentRequestThrowsError(HttpStatusCode.NotFound);
        }

        [Test]
        public void ConflictFailedConsignmentRequestThrowsError()
        {
            SevereFailedConsignmentRequestThrowsError(HttpStatusCode.Conflict);
        }

        public void SevereFailedConsignmentRequestThrowsError(HttpStatusCode statusCode)
        {
            StubExecuteMethod(statusCode);

            var ex = (BaseServiceClientException)Assert.Throws(typeof(BaseServiceClientException), () => _apiFacade.NewConsignment(CreateConsignmentDetails()));
            Assert.That(ex.Severity, Is.EqualTo(BaseServiceClientException.ErrorSeverity.Fatal));
        }

        private AllocationDetails CreateAllocationDetails()
        {
            return new AllocationDetails()
            {
                FulfillerId = "8010"
            };
        }

        private ConsignmentDetails CreateConsignmentDetails()
        {
            return new ConsignmentDetails()
            {
                ConsignmentNumber = "00257"
            };
        }

        private void StubExecuteMethod(HttpStatusCode statusCode)
        {
            _clientMock.Stub(x => x.Execute(Arg<IRestRequest>.Is.Anything)).Return(new RestResponse() { StatusCode = statusCode });
        }
    }
}
