﻿using NUnit.Framework;
using System.Configuration;
using WSS.BO.RemoteServices.ProxyService.Client;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses;

namespace WSS.BO.RemoteServices.ProxyService.IntegrationTest
{
    [TestFixture]
    public class AddressesClearingTests
    {
        private readonly ProxyServiceClient client;

        public AddressesClearingTests()
        {
            client = new ProxyServiceClient(ConfigurationManager.AppSettings["ProxyUrl"]);
        }

        [Test]
        public void AddressesClearingReturnsTheSameAddress()
        {
            var address = new StructuredAddress()
            {
                BuildingName = string.Empty,
                BuildingNumber = "2",
                District = string.Empty,
                SubBuildingName = string.Empty,
                Thoroughfare = "Briar Rd",
                Town = "LEICESTER",
                County = "Leicestershire",
                PostCode = "LE5 2HA",
                Organization = string.Empty
            };

            var responseAddress = client.ClearUpAddress<StructuredAddress>(address);

            Assert.IsNotNull(responseAddress.Address);
            Assert.AreEqual(address.BuildingName, responseAddress.Address.BuildingName);
            Assert.AreEqual(address.BuildingNumber, responseAddress.Address.BuildingNumber);
            Assert.AreEqual(address.County, responseAddress.Address.County);
            Assert.AreEqual(address.District, responseAddress.Address.District);
            Assert.AreEqual(address.Organization, responseAddress.Address.Organization);
            Assert.AreEqual(address.PostCode, responseAddress.Address.PostCode);
            Assert.AreEqual(address.SubBuildingName, responseAddress.Address.SubBuildingName);
            Assert.AreEqual(address.Thoroughfare, responseAddress.Address.Thoroughfare);
            Assert.AreEqual(address.Town, responseAddress.Address.Town);
            Assert.IsNull(responseAddress.PickupItems);
            Assert.AreEqual(AddressLookupStatus.ExactMatchFound, responseAddress.Status);
        }

        [Test]
        public void PostCodeRefreshingTest()
        {
            var address = new StructuredAddress() 
            { 
                BuildingName = "Mount Pleasant Mail Centr", 
                Organization = "Royal Mail", 
                Thoroughfare = "Farringdon Rd", 
                Town = "LONDON", 
                PostCode = "QQ11QQ" 
            };

            var responseAddress = client.ClearUpAddress<StructuredAddress>(address);

            Assert.IsNotNull(responseAddress.Address);
            Assert.AreEqual("EC1A 1BB", responseAddress.Address.PostCode);
            Assert.IsNull(responseAddress.PickupItems);
            Assert.AreEqual(AddressLookupStatus.ExactMatchFound, responseAddress.Status);
        }
    }
}
