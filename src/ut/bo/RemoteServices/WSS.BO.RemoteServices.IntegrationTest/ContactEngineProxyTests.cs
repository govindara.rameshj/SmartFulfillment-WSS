using System;
using System.Configuration;
using NUnit.Framework;
using WSS.BO.RemoteServices.Common.Client;
using WSS.BO.RemoteServices.ProxyService.Client;
using WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification;

namespace WSS.BO.RemoteServices.ProxyService.IntegrationTest
{
    [TestFixture]
    public class ContactEngineProxyTests
    {
        private int consignmentNumber = 000875;
        private const int FulfillerNumber = 1287;

        private readonly ProxyServiceClient client;

        public ContactEngineProxyTests()
        {
            client = new ProxyServiceClient(ConfigurationManager.AppSettings["ProxyUrl"]);
        }

        [TestFixtureSetUp]
        public void InitClass()
        {
            ++consignmentNumber;
        }

        [Test]
        public void NewConsignmentExecuteSuccessfully()
        {
            // the test may create a new consignment without an exception or raise "Consignment already exists". Both are valid.
            try
            {
                client.NewConsignment(CreateConsignmentDetails(consignmentNumber));
            }
            catch (BaseServiceClientException ex)
            {
                Assert.That(ex.Message, Is.StringContaining("Consignment already exists"));
            }
        }

        [Test]
        public void UpdateConsignmentExecuteSuccessfully()
        {
            client.UpdateConsignment(CreateConsignmentDetails(consignmentNumber));
        }

        [Test]
        public void CancelConsignmentExecuteSuccessfully()
        {
            // the test may cancel an existing consignment without an exception or raise "Not found". Both are valid.
            try
            {
                client.CancelConsignment(CreateConsignmentDetails(consignmentNumber));
            }
            catch (BaseServiceClientException ex)
            {
                Assert.That(ex.Message, Is.StringContaining("The specified consignment was already deleted"));
            }
        }

        private ConsignmentDetails CreateConsignmentDetails(int consignmentNumber)
        {
            return new ConsignmentDetails
            {
                ConsignmentNumber = consignmentNumber.ToString(),
                FulfillerNumber = FulfillerNumber,
                FulfillerName = "TUNBRIDGE WE",
                DeliveryDate = new DateTime(2015, 06, 22),
                CustomerMobilePhone = "07555 555555",
                CustomerContactPhone = "07555 555555",
                CustomerHomePhone = "07555 555555",
                CustomerWorkPhone = "07555 555555",
                CustomerName = "John Smith",
                PostCode = "NP25 5FE",
                AddressLine1 = "93, THE GROVE",
                AddressLine2 = "RATFORD",
                AddressLine3 = "NR BRISTOL",
                AddressLine4 = "NORTH SOMERSET",
                CustomerOrderNumber = 1803601,
                OriginatingStoreNumber = 5034,
                OriginatingStoreName = "HIGH WYCOMBE",
            };
        }
    }
}
