﻿using System;
using System.Configuration;
using System.Linq;
using NUnit.Framework;
using WSS.BO.RemoteServices.ProxyService.Client;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup;
using WSS.BO.RemoteServices.ProxyService.Contract.AddressLookup.Addresses;

namespace WSS.BO.RemoteServices.ProxyService.IntegrationTest
{
    [TestFixture]
    public class AddressLookupTests
    {
        private readonly ProxyServiceClient client;

        public AddressLookupTests()
        {
            client = new ProxyServiceClient(ConfigurationManager.AppSettings["ProxyUrl"]);
        }

        #region CommonTests       
        
        [Test]
        public void ResolvePostCodeForFourLinesAddress()
        {
            var result = client.ResolvePostCode<FourLineAddress>("EC1A 1BB");

            Assert.AreEqual(typeof(FourLineAddress), result.Address.GetType());
        }

        [Test]
        public void ResolvePostCodeForStructuredAddress()
        {
            var result = client.ResolvePostCode<StructuredAddress>("EC1A 1BB");

            Assert.AreEqual(typeof(StructuredAddress), result.Address.GetType());
        }

        [Test]
        public void ResolvePostCodeForNonStructuredAddress()
        {
            var result = client.ResolvePostCode<NonStructuredAddress>("EC1A 1BB");

            Assert.AreEqual(typeof(NonStructuredAddress), result.Address.GetType());
        }

        #endregion

        #region FourLineAddress
        [Test]
        public void ResolvePostCodeForTooManyResults1ForFourLineAddress()
        {
            var result = client.ResolvePostCode<FourLineAddress>("EC");

            Assert.IsNull(result.Address);
            Assert.IsNull(result.PickupItems);
            Assert.AreEqual(AddressLookupStatus.TooManyMatches, result.Status);
        }

        [Test]
        public void ResolvePostCodeForTooManyResults2ForFourLineAddress()
        {
            var result = client.ResolvePostCode<FourLineAddress>("EC1");

            Assert.IsNull(result.Address);
            Assert.IsNull(result.PickupItems);
            Assert.AreEqual(AddressLookupStatus.TooManyMatches, result.Status);
        }

        [Test]
        public void ResolvePostCodeForTooManyResults3ForFourLineAddress()
        {
            var result = client.ResolvePostCode<FourLineAddress>("EC1A");

            Assert.IsNull(result.Address);
            Assert.IsNull(result.PickupItems);
            Assert.AreEqual(AddressLookupStatus.TooManyMatches, result.Status);
        }

        [Test]
        public void ResolvePostCodeExactMatchForFourLineAddress()
        {
            var result = client.ResolvePostCode<FourLineAddress>("EC1A 1BB");

            Assert.IsNotNull(result.Address);
            Assert.AreEqual("Royal Mail", result.Address.AddressLine1);
            Assert.AreEqual("Mount Pleasant Mail Centre, Fa", result.Address.AddressLine2);
            Assert.AreEqual("EC1A 1BB", result.Address.PostCode);
            Assert.AreEqual("LONDON", result.Address.Town);
            Assert.IsNull(result.PickupItems);
            Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
        }

        [Test]
        public void ResolvePostCodeSeveralResultsForFourLineAddress()
        {
            var result = client.ResolvePostCode<FourLineAddress>("M11AE");

            Assert.IsNull(result.Address);
            Assert.IsNotNull(result.PickupItems);
            Assert.AreNotEqual(1, result.PickupItems.Count);
            Assert.AreEqual(AddressLookupStatus.SeveralMatchesFound, result.Status);
        }

        [Test]
        public void ResolvePostCodeSeveralResultsWithRequestingOneOfThem1ForFourLineAddress()
        {
            string pickupItemId;

            {
                var result = client.ResolvePostCode<FourLineAddress>("CF14 5EL");

                Assert.IsNull(result.Address);
                Assert.IsNotNull(result.PickupItems);
                Assert.AreNotEqual(1, result.PickupItems.Count);
                Assert.AreEqual(AddressLookupStatus.SeveralMatchesFound, result.Status);
                pickupItemId = result.PickupItems.First().Id;
            }

            {
                var result = client.RefinePickListItem<FourLineAddress>(pickupItemId);

                Assert.IsNotNull(result.Address);
                Assert.AreEqual("67 Coed Glas Rd", result.Address.AddressLine1);
                Assert.AreEqual("Llanishen", result.Address.AddressLine2);
                Assert.AreEqual("CARDIFF", result.Address.Town);
                Assert.AreEqual("CF14 5EL", result.Address.PostCode);
                Assert.IsNull(result.PickupItems);
                Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
            }
        }

        [Test]
        public void ResolvePostCodeSeveralResultsWithRequestingOneOfThem2ForFourLineAddress()
        {
            string pickupItemId;

            {
                var result = client.ResolvePostCode<FourLineAddress>("s87bw");

                Assert.IsNull(result.Address);
                Assert.IsNotNull(result.PickupItems);
                Assert.AreNotEqual(1, result.PickupItems.Count);
                Assert.AreEqual(AddressLookupStatus.SeveralMatchesFound, result.Status);
                pickupItemId = result.PickupItems.First().Id;
            }

            {
                var result = client.RefinePickListItem<FourLineAddress>(pickupItemId);
                Assert.IsNotNull(result.Address);
                Assert.AreEqual("121 Westwick Rd", result.Address.AddressLine1);
                Assert.AreEqual(String.Empty, result.Address.AddressLine2);
                Assert.AreEqual("SHEFFIELD", result.Address.Town);
                Assert.AreEqual("S8 7BW", result.Address.PostCode);
                Assert.IsNull(result.PickupItems);
                Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
            }
        }

        [Test]
        public void ResolvePostCodeSeveralResultsWithRequestingOneOfThem3ForFourLineAddress()
        {
            string pickupItemId;

            {
                var result = client.ResolvePostCode<FourLineAddress>("LE5 2HA");

                Assert.IsNull(result.Address);
                Assert.IsNotNull(result.PickupItems);
                Assert.AreNotEqual(1, result.PickupItems.Count);
                Assert.AreEqual(AddressLookupStatus.SeveralMatchesFound, result.Status);
                pickupItemId = result.PickupItems.First().Id;
            }

            {
                var result = client.RefinePickListItem<FourLineAddress>(pickupItemId);
                Assert.IsNotNull(result.Address);
                Assert.AreEqual("2 Briar Rd", result.Address.AddressLine1);
                Assert.AreEqual(String.Empty, result.Address.AddressLine2);
                Assert.AreEqual("LEICESTER", result.Address.Town);
                Assert.AreEqual("LE5 2HA", result.Address.PostCode);
                Assert.IsNull(result.PickupItems);
                Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
            }
        }

        [Test]
        public void ResolvePostCodeForNotExistingAddressForFourLineAddress()
        {
            var result = client.ResolvePostCode<FourLineAddress>("1");

            Assert.IsNull(result.Address);
            Assert.IsNull(result.PickupItems);
            Assert.AreEqual(AddressLookupStatus.NotFound, result.Status);
        }

        #endregion

        #region StructuredAddress
        [Test]
        public void ResolvePostcodeWithFulfilledOrganizationFieldForStructuredAddress()
        {
            var result = client.ResolvePostCode<StructuredAddress>("EC1A 1BB");
            Assert.IsNotNull(result.Address);
            Assert.AreEqual("Royal Mail", result.Address.Organization);
            Assert.IsNull(result.PickupItems);
            Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
        }

        [Test]
        public void ResolvePostcodeWithFulfilledDistrictFieldForStructuredAddress()
        {
            string pickupItemId;

            {
                var result = client.ResolvePostCode<StructuredAddress>("CF14 5EL");

                Assert.IsNull(result.Address);
                Assert.IsNotNull(result.PickupItems);
                Assert.AreNotEqual(1, result.PickupItems.Count);
                Assert.AreEqual(AddressLookupStatus.SeveralMatchesFound, result.Status);
                pickupItemId = result.PickupItems.First().Id;
            }

            {
                var result = client.RefinePickListItem<StructuredAddress>(pickupItemId);

                Assert.IsNotNull(result.Address);
                Assert.AreEqual("Llanishen", result.Address.District);
                Assert.IsNull(result.PickupItems);
                Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
            }
        }

        [Test]
        public void ResolvePostcodeWithFulfilledCountyFieldForStructuredAddress()
        {
            string pickupItemId;

            {
                var result = client.ResolvePostCode<StructuredAddress>("LE5 2HA");

                Assert.IsNull(result.Address);
                Assert.IsNotNull(result.PickupItems);
                Assert.AreNotEqual(1, result.PickupItems.Count);
                Assert.AreEqual(AddressLookupStatus.SeveralMatchesFound, result.Status);
                pickupItemId = result.PickupItems.First().Id;
            }

            {
                var result = client.RefinePickListItem<StructuredAddress>(pickupItemId);
                Assert.IsNotNull(result.Address);
                Assert.AreEqual("Leicestershire", result.Address.County);
                Assert.IsNull(result.PickupItems);
                Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
            }
        }
           
        #endregion

        #region NonStructuredAddress
        [Test]
        public void ResolvePostcodeWithFulfilledDistrictFieldForNonStructuredAddress()
        {
            string pickupItemId;

            {
                var result = client.ResolvePostCode<NonStructuredAddress>("CF14 5EL");

                Assert.IsNull(result.Address);
                Assert.IsNotNull(result.PickupItems);
                Assert.AreNotEqual(1, result.PickupItems.Count);
                Assert.AreEqual(AddressLookupStatus.SeveralMatchesFound, result.Status);
                pickupItemId = result.PickupItems.First().Id;
            }

            {
                var result = client.RefinePickListItem<NonStructuredAddress>(pickupItemId);

                Assert.IsNotNull(result.Address);
                Assert.AreEqual("Llanishen", result.Address.Address2);
                Assert.IsNull(result.PickupItems);
                Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
            }
        }

        [Test]
        public void ResolvePostcodeWithFulfilledCountyFieldForNonStructuredAddress()
        {
            string pickupItemId;

            {
                var result = client.ResolvePostCode<NonStructuredAddress>("LE5 2HA");

                Assert.IsNull(result.Address);
                Assert.IsNotNull(result.PickupItems);
                Assert.AreNotEqual(1, result.PickupItems.Count);
                Assert.AreEqual(AddressLookupStatus.SeveralMatchesFound, result.Status);
                pickupItemId = result.PickupItems.First().Id;
            }

            {
                var result = client.RefinePickListItem<NonStructuredAddress>(pickupItemId);
                Assert.IsNotNull(result.Address);
                Assert.AreEqual("Leicestershire", result.Address.County);
                Assert.IsNull(result.PickupItems);
                Assert.AreEqual(AddressLookupStatus.ExactMatchFound, result.Status);
            }
        }
        #endregion
    }
}
