using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using NUnit.Framework;
using Rhino.Mocks;
using WSS.BO.RemoteServices.ProxyService.Host.Adapters;
using WSS.BO.RemoteServices.ProxyService.Host.Executers;
using WSS.BO.RemoteServices.ProxyService.Host.Factories;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.TokenManagers;

namespace WSS.BO.RemoteServices.ProxyService.NUnit.UnitTest
{
    [TestFixture]
    public class ShipmentServiceAccessorTests
    {
        private static HttpWebRequest _httpWebRequest;
        private static HttpResponseMessage _expectedResponse;
        private static ShipmentExecuter _executer;

        private static readonly string Parameters = "8385_2015-03-25_1/deallocatefromstore";
        private static readonly string HttpMethod = System.Net.Http.HttpMethod.Post.ToString();

        [SetUp]
        public void InitTest()
        {
            _expectedResponse = MockRepository.GenerateMock<HttpResponseMessage>();

            _httpWebRequest = MockRepository.GenerateMock<HttpWebRequest>();
            _httpWebRequest.Expect(item => item.Headers).Return(new WebHeaderCollection());
            _httpWebRequest.Expect(item => item.GetRequestStream()).Return(new MemoryStream());

            RequestEngineFactory requestFactory = MockRepository.GenerateMock<RequestEngineFactory>();
            requestFactory.Expect(item => item.CreateWebRequestEngine(Arg<string>.Is.Anything)).Return(_httpWebRequest);

            GoogleOAuth2TokenManager tokenManager = MockRepository.GenerateMock<GoogleOAuth2TokenManager>(new X509Certificate2(), string.Empty);
            BaseTokenManager tokenManagerInterface = tokenManager;
            tokenManagerInterface.Expect(item => item.CheckToken());
            tokenManagerInterface.Expect(item => item.IsTokenExpired(Arg<HttpWebResponse>.Is.Anything)).Return(true);
            tokenManagerInterface.Expect(item => item.TryToReAquireToken(Arg<string>.Is.Anything));

            ResponseConverter responseConverter = MockRepository.GenerateMock<ResponseConverter>();
            responseConverter.Expect(item => item.GetHttpResponse(Arg<WebResponseAdapter>.Is.Anything)).Return(_expectedResponse);

            ResponseAdaptersFactory adaptersFactory = MockRepository.GenerateMock<ResponseAdaptersFactory>();

            _executer = new ShipmentExecuter(string.Empty, requestFactory, responseConverter, tokenManager, adaptersFactory);
        }

        [Test]
        public void ExecuteRequestReturnsResponseSuccessfully()
        {
            HttpResponseMessage resultResponse = _executer.ExecuteRequest(Parameters, HttpMethod);

            Assert.AreEqual(_expectedResponse, resultResponse);
        }

        [Test]
        [ExpectedException(typeof(WebException))]
        public void ExecuteRequestThrowsWebException()
        {
            _httpWebRequest.Expect(item => item.GetResponse()).Throw(new WebException());

            _executer.ExecuteRequest(Parameters, HttpMethod);
        }

        [Test]
        public void ExecuteRequestReturnsResponseFromSecondWebException()
        {
            _httpWebRequest.Expect(item => item.GetResponse()).Throw(new WebException(string.Empty, null, WebExceptionStatus.SendFailure, MockRepository.GenerateMock<HttpWebResponse>())).Repeat.Once();
            _httpWebRequest.Expect(item => item.GetResponse()).Throw(new WebException(string.Empty, null, WebExceptionStatus.SendFailure, MockRepository.GenerateMock<HttpWebResponse>())).Repeat.Once();

            HttpResponseMessage resultResponse = _executer.ExecuteRequest(Parameters, HttpMethod);

            Assert.AreEqual(_expectedResponse, resultResponse);
        }

        [Test]
        public void ExecuteRequestReturnsResponseAfterResend()
        {
            _httpWebRequest.Expect(item => item.GetResponse()).Throw(new WebException(string.Empty, null, WebExceptionStatus.SendFailure, MockRepository.GenerateMock<HttpWebResponse>())).Repeat.Once();
            _httpWebRequest.Expect(item => item.GetResponse()).Return(MockRepository.GenerateMock<HttpWebResponse>());

            HttpResponseMessage resultResponse = _executer.ExecuteRequest(Parameters, HttpMethod);

            Assert.AreEqual(_expectedResponse, resultResponse);
        }
    }
}
