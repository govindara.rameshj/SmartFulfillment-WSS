using System;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using RestSharp;
using Rhino.Mocks;
using WSS.BO.RemoteServices.ProxyService.Host.Adapters;
using WSS.BO.RemoteServices.ProxyService.Host.Executers;
using WSS.BO.RemoteServices.ProxyService.Host.Factories;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.TokenManagers;

namespace WSS.BO.RemoteServices.ProxyService.NUnit.UnitTest
{
    [TestFixture]
    public class ContactEngineServiceAccessorTests
    {
        private  ContactEngineExecuter _executer;
        private  IRestResponse _restResponse;
        private  HttpResponseMessage _expectedResponse;

        
        private static readonly string PATH = "fake_path";
        private static readonly string METHOD = "POST";
        private static readonly byte[] BODY = { 1, 2, 3 };
        private static readonly string CONTENT_TYPE = "application/json";

        [SetUp]
        public void InitTest()
        {
            _expectedResponse = MockRepository.GenerateMock<HttpResponseMessage>();

            _restResponse = MockRepository.GenerateMock<IRestResponse>();
            IRestClient restClient = MockRepository.GenerateMock<IRestClient>();
            restClient.Expect(item => item.Execute(Arg<IRestRequest>.Is.Anything)).Return(_restResponse);

            RequestEngineFactory executeFactory = MockRepository.GenerateMock<RequestEngineFactory>();
            executeFactory.Expect(item => item.CreateRestRequestEngine(Arg<string>.Is.Anything)).Return(restClient);

            ResponseConverter responseConverter = MockRepository.GenerateMock<ResponseConverter>();
            responseConverter.Expect(item => item.GetHttpResponse(Arg<RestResponseAdapter>.Is.Anything)).Return(_expectedResponse);

            GenericOAuth2TokenManager tokenManager = MockRepository.GenerateMock<GenericOAuth2TokenManager>(string.Empty, string.Empty, string.Empty);

            ResponseAdaptersFactory adaptersFactory = MockRepository.GenerateMock<ResponseAdaptersFactory>();

            _executer = new ContactEngineExecuter(string.Empty, executeFactory, responseConverter, tokenManager, adaptersFactory);
        }

        [Test]
        public void ExecuteRequestSuccessfully()
        {
            HttpResponseMessage response = _executer.ExecuteRequest(PATH, METHOD, BODY, CONTENT_TYPE);
            
            Assert.AreEqual(_expectedResponse, response);
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void ExecuteRequestThrowsApplicatonException()
        {
            _restResponse.Expect(item => item.ErrorException).Return(new ApplicationException());

            HttpResponseMessage response = _executer.ExecuteRequest(PATH, METHOD, BODY, CONTENT_TYPE);
        }

        [Test]
        public void ExecuteRequestReturnsResponseFromSecondWebException()
        {
            _restResponse.Expect(item => item.ErrorException).Return(new WebException(string.Empty, null, WebExceptionStatus.SendFailure, MockRepository.GenerateMock<HttpWebResponse>()));

            HttpResponseMessage response = _executer.ExecuteRequest(PATH, METHOD, BODY, CONTENT_TYPE);

            Assert.AreEqual(_expectedResponse, response);
        }

        [Test]
        public void ExecuteRequestReturnsResponseAfterResend()
        {
            _restResponse.Expect(item => item.ErrorException).Return(null).Repeat.Once();
            _restResponse.Expect(item => item.ErrorException).Return(new WebException(string.Empty, null, WebExceptionStatus.SendFailure, MockRepository.GenerateMock<HttpWebResponse>())).Repeat.Once();
 
            HttpResponseMessage resultResponse = _executer.ExecuteRequest(PATH, METHOD, BODY, CONTENT_TYPE);

            Assert.AreEqual(_expectedResponse, resultResponse);
        }
    }
}
