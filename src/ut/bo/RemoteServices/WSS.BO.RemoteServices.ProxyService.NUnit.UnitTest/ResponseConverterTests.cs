using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Rhino.Mocks;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.Interfaces;

namespace WSS.BO.RemoteServices.ProxyService.NUnit.UnitTest
{
    [TestFixture]
    public class ResponseConverterTests
    {        
        private static ResponseConverter _responseConverter;
        private IResponseAdapter _inputData;

        [TestFixtureSetUp]
        public void InitClass()
        {
            _responseConverter = new ResponseConverter();
        }

        [SetUp]
        public void InitTest()
        {
            _inputData = MockRepository.GenerateMock<IResponseAdapter>();
        }

        [Test]
        public void GetHttpResponseReturnsResponseWithExpectedStatusSuccessfully()
        {
            HttpStatusCode expectedStatus = HttpStatusCode.OK;
            SetUpData(expectedStatus);

            HttpResponseMessage responseMessage = _responseConverter.GetHttpResponse(_inputData);

            Assert.AreEqual(expectedStatus, responseMessage.StatusCode);
        }

        [Test]
        public void GetHttpResponseReturnsResponseWithExpectedBodySuccessfully()
        {
            byte[] expectedBody = {1, 2, 2, 3};
            SetUpData(expectedBody);

            HttpResponseMessage responseMessage = _responseConverter.GetHttpResponse(_inputData);

            Assert.AreEqual(expectedBody, responseMessage.Content.ReadAsByteArrayAsync().Result);
        }

        [Test]
        public void GetHttpResponseReturnsResponseWithExpectedBodyHeaderSuccessfully()
        {
            string expectedBodyHeaderName = "content-type";
            string expectedBodyHeaderValue = "application/json";
            SetUpData(expectedBodyHeaderName, expectedBodyHeaderValue);

            HttpResponseMessage responseMessage = _responseConverter.GetHttpResponse(_inputData);

            Assert.AreEqual(expectedBodyHeaderValue, responseMessage.Content.Headers.GetValues(expectedBodyHeaderName).First());
        }

        [Test]
        public void GetHttpResponseReturnsResponseWithExpectedHeaderSuccessfully()
        {
            var expectedHeaderName = "User-Agent";
            var expectedHeaderValue = "test";
            SetUpData(expectedHeaderName, expectedHeaderValue);

            HttpResponseMessage responseMessage = _responseConverter.GetHttpResponse(_inputData);

            Assert.AreEqual(expectedHeaderValue, responseMessage.Headers.GetValues(expectedHeaderName).First());
        }

        private void SetUpData(byte[] content, string headerName, string headerValue)
        {
            SetUpContent(content);
            SetUpHeader(headerName, headerValue);
        }

        private void SetUpData(HttpStatusCode statusCode)
        {
            SetUpData(new byte[0], string.Empty, string.Empty);
            SetUpStatusCode(statusCode);
        }

        private void SetUpData(byte[] content)
        {
            SetUpData(content, string.Empty, string.Empty);
        }

        private void SetUpData(string headerName, string headerValue)
        {
            SetUpData(new byte[0], headerName, headerValue);
        }

        private void SetUpContent(byte[] content)
        {
            _inputData.Expect(item => item.Content).Return(new ByteArrayContent(content));
        }

        private void SetUpHeader(string headerName, string headerValue)
        {
            var headers = new NameValueCollection();
            if (!string.IsNullOrWhiteSpace(headerName))
            {
                headers.Add(headerName, headerValue);
            }
            _inputData.Expect(item => item.Headers).Return(headers);
        }

        private void SetUpStatusCode(HttpStatusCode statusCode)
        {
            _inputData.Expect(item => item.StatusCode).Return(statusCode);
        }
    }
}
