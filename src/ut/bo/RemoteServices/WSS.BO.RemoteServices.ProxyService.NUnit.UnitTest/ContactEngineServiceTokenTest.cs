using System;
using NUnit.Framework;
using WSS.BO.RemoteServices.ProxyService.Host.Tokens;

namespace WSS.BO.RemoteServices.ProxyService.NUnit.UnitTest
{
    [TestFixture]
    public class ContactEngineServiceTokenTests
    {
        private const string AccessToken = "accessToken";

        [Test]
        public void TokenIsExpired()
        {
            GenericOAuth2Token token = new GenericOAuth2Token(AccessToken, 5, GetTimeInPast(10));
            Assert.IsTrue(token.IsExpired(DateTime.Now));
        }

        [Test]
        public void TokenNotIsExpired()
        {
            GenericOAuth2Token token = new GenericOAuth2Token(AccessToken, 15, DateTime.Now);
            Assert.IsFalse(token.IsExpired(DateTime.Now));
        }

        private DateTime GetTimeInPast(long secondsAgo)
        {
            return DateTime.Now.AddSeconds((-1) * secondsAgo);
        }
    }
}
