using System.Security.Authentication;
using System.Security.Principal;
using NUnit.Framework;
using Rhino.Mocks;
using WSS.BO.RemoteServices.ProxyService.Host.Helpers;
using WSS.BO.RemoteServices.ProxyService.Host.Settings;

namespace WSS.BO.RemoteServices.ProxyService.NUnit.UnitTest
{
    [TestFixture]
    public class FulfillerValidatorTests
    {
        private static FulfillerValidator _contactEngineValidator;
        private static FulfillerValidator _shipmentValidator;
        private static IPrincipal _user;
        private static IIdentity _identity;

        [TestFixtureSetUp]
        public void InitClass()
        {
            ServiceEnvironmentSettings settings = new ServiceEnvironmentSettings();
            settings.FulfillerIdPattern = @"^.+?\\srvc(?'fulfillerId'\d+)\$$";
            _contactEngineValidator = new FulfillerValidator(settings, @"fulfillers/(?<fulfillerId>\d+)/", true);
            _shipmentValidator = new FulfillerValidator(settings, @"^(?<fulfillerId>\d+)_", true);
        }

        [SetUp]
        public void InitTest()
        {
            _user = MockRepository.GenerateMock<IPrincipal>();
            _identity = MockRepository.GenerateMock<IIdentity>();
            _user.Stub(item => item.Identity).Return(_identity);           
        }

        [Test]
        public void ValidateFulfillerFromContactEngineUrlSuccessfully()
        {            
            _identity.Stub(item => item.IsAuthenticated).Return(true);
            _identity.Stub(item => item.Name).Return("domain\\srvc1287$");

            _contactEngineValidator.Validate(_user, "fulfillers/1287/consignments");

            _user.VerifyAllExpectations();
            _identity.VerifyAllExpectations();
        }

        [Test]
        public void ValidateFulfillerFromShipmentUrlSuccessfully()
        {
            _identity.Stub(item => item.IsAuthenticated).Return(true);
            _identity.Stub(item => item.Name).Return("domain\\srvc1287$");

            _shipmentValidator.Validate(_user, "1287_2015-03-24_1/deallocatefromstore");

            _user.VerifyAllExpectations();
            _identity.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(AuthenticationException), ExpectedMessage = "User is not authenticated")]
        public void ValidateFulfillerThrowsAuthenticationExceptionBecauseUserIsNotAuthenticated()
        {
            _identity.Stub(item => item.IsAuthenticated).Return(false);

            _contactEngineValidator.Validate(_user, "");

            _user.VerifyAllExpectations();
            _identity.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(AuthenticationException), ExpectedMessage = "User domain\\srvc1287$ is not authorized to make this request as fulfillerId 1286")]
        public void ValidateFulfillerThrowsAuthenticationExceptionBecauseUserIsNotAuthorizedToMakeThisRequest()
        {
            _identity.Stub(item => item.IsAuthenticated).Return(true);
            _identity.Stub(item => item.Name).Return("domain\\srvc1287$");

            _contactEngineValidator.Validate(_user, "fulfillers/1286/consignments");

            _user.VerifyAllExpectations();
            _identity.VerifyAllExpectations();
        }

        [Test]
        [ExpectedException(typeof(AuthenticationException), ExpectedMessage = "User John_Doe is not authorized to make this request")]
        public void ValidateFulfillerThrowsAuthenticationExceptionBecauseJohnDoeIsNotAuthorizedToMakeThisRequest()
        {
            _identity.Stub(item => item.IsAuthenticated).Return(true);
            _identity.Stub(item => item.Name).Return("John_Doe");

            _contactEngineValidator.Validate(_user, "fulfillers/1286/consignments");

            _user.VerifyAllExpectations();
            _identity.VerifyAllExpectations();
        }
    }
}
