using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using ExtRequestMonitor = ExternalRequestMonitor.ExternalRequestMonitor;
using NUnit.Framework;
using Rhino.Mocks;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.RemoteServices.Common.Client;
using WSS.BO.RemoteServices.ProxyService.Contract.Capacity;
using WSS.BO.RemoteServices.ProxyService.Contract.DeliveryNotification;
using WSS.BO.RemoteServices.UserManagementService.Client;

namespace ExternalRequestMonitor.UnitTest
{
    [TestFixture]
    public class ExternalRequestMonitorTests
    {
        private readonly int _retriesNumber = 3;
        
        private IDataLayerFactory _dalMock;
        private IExternalRequestRepository _extRequestsRepoMock;
        private IDictionariesRepository _dictRepoMock;
        private ICapacityService _capacityServiceMock;
        private IDeliveryNotificationService _deliveryNotificationServiceMock;
        private IUserManagementService _userManagementServiceMock;
        private IUserRepository _userRepositoryMock;
        private ExtRequestMonitor _monitor;
        
        [SetUp]
        public void CreateObjects()
        {
            _extRequestsRepoMock = MockRepository.GenerateMock<IExternalRequestRepository>();
            _extRequestsRepoMock.Stub(x => x.GetRequestsForProcessing(Arg<int>.Is.Anything)).Return(new List<ExternalRequest>());

            _dictRepoMock = MockRepository.GenerateMock<IDictionariesRepository>();
            _dictRepoMock.Stub(x => x.GetSettings()).Return(new Settings() { MaxRetriesNumber = _retriesNumber });

            _userRepositoryMock = MockRepository.GenerateMock<IUserRepository>();

            _dalMock = MockRepository.GenerateMock<IDataLayerFactory>();

            _dalMock.Stub(x => x.Create<IExternalRequestRepository>()).Return(_extRequestsRepoMock);
            _dalMock.Stub(x => x.Create<IDictionariesRepository>()).Return(_dictRepoMock);
            _dalMock.Stub(x => x.Create<IUserRepository>()).Return(_userRepositoryMock);

            var remoteServicesFactoryMock = MockRepository.GenerateMock<IRemoteServicesFactory>();
            _capacityServiceMock = MockRepository.GenerateMock<ICapacityService>();
            _deliveryNotificationServiceMock = MockRepository.GenerateMock<IDeliveryNotificationService>();
            _userManagementServiceMock = MockRepository.GenerateMock<IUserManagementService>();
            remoteServicesFactoryMock.Stub(x => x.GetCapacityService()).Return(_capacityServiceMock);
            remoteServicesFactoryMock.Stub(x => x.GetDeliveryNotificationService()).Return(_deliveryNotificationServiceMock);
            remoteServicesFactoryMock.Stub(x => x.GetUserManagementService()).Return(_userManagementServiceMock);

            _monitor = new ExtRequestMonitor(_dalMock, remoteServicesFactoryMock);
        }

        [Test]
        public void MonitorRetrievesExternalRequests()
        {
            _monitor.GetPendingRequests();
            _extRequestsRepoMock.AssertWasCalled(x => x.GetRequestsForProcessing(Arg<int>.Is.Anything));
        }

        [Test]
        public void SuccessfulProcessingUpdatesExternalRequest()
        {
            _monitor.ProcessRequest(CreateExternalRequest<UpdateCapacityRequestDetails>(ExternalRequestType.Allocate));
            AssertRequestWasUpdated(ExternalRequestStatus.Processed);
        }

        [Test]
        public void FailedProcessingUpdatesExternalRequest()
        {
            _capacityServiceMock.Stub(x => x.AllocateCapacity(Arg<AllocationDetails>.Is.Anything)).Throw(new BaseServiceClientException(BaseServiceClientException.ErrorSeverity.Normal, "ERROR"));

            _monitor.ProcessRequest(CreateExternalRequest<UpdateCapacityRequestDetails>(ExternalRequestType.Allocate));
            AssertRequestWasUpdated(ExternalRequestStatus.Retry);
        }

        [Test]
        public void UnderMaxRetriesProcessingUpdatesExternalRequest()
        {
            _capacityServiceMock.Stub(x => x.AllocateCapacity(Arg<AllocationDetails>.Is.Anything)).Throw(new BaseServiceClientException(BaseServiceClientException.ErrorSeverity.Normal, "ERROR"));

            _extRequestsRepoMock.Expect(x => x.UpdateRequest(Arg<long>.Is.Anything, Arg<int>.Is.Equal(ExternalRequestStatus.Retry), Arg<string>.Is.Anything)).Repeat.Times(_retriesNumber - 1);
            var request = CreateExternalRequest<UpdateCapacityRequestDetails>(ExternalRequestType.Allocate);
            for (var i = 0; i < _retriesNumber - 1; i++)
            {
                _monitor.ProcessRequest(request);
                request.RetriesCount++;
            }
            _dalMock.VerifyAllExpectations();
       }

        [Test]
        public void MaxRetriesProcessingUpdatesExternalRequest()
        {
            _capacityServiceMock.Stub(x => x.AllocateCapacity(Arg<AllocationDetails>.Is.Anything)).Throw(new BaseServiceClientException(BaseServiceClientException.ErrorSeverity.Normal, "ERROR"));

            var request = CreateExternalRequest<UpdateCapacityRequestDetails>(ExternalRequestType.Allocate);
            for (var i = 0; i < _retriesNumber; i++)
            {
                _monitor.ProcessRequest(request);
                request.RetriesCount++;
            }
            AssertRequestWasUpdated(ExternalRequestStatus.Failed);
        }

        [Test]
        public void SevereFailedProcessingUpdatesExternalRequest()
        {
            _capacityServiceMock.Stub(x => x.AllocateCapacity(Arg<AllocationDetails>.Is.Anything)).Throw(new BaseServiceClientException(BaseServiceClientException.ErrorSeverity.Fatal, ""));

            _monitor.ProcessRequest(CreateExternalRequest<UpdateCapacityRequestDetails>(ExternalRequestType.Allocate));
            AssertRequestWasUpdated(ExternalRequestStatus.Failed);
        }

        [Test]
        public void UnexpectedFailedProcessingUpdatesExternalRequest()
        {
            _capacityServiceMock.Stub(x => x.AllocateCapacity(Arg<AllocationDetails>.Is.Anything)).Throw(new Exception());

            _monitor.ProcessRequest(CreateExternalRequest<UpdateCapacityRequestDetails>(ExternalRequestType.Allocate));
            AssertRequestWasUpdated(ExternalRequestStatus.Failed);
        }

        [Test]
        public void AllocateSendsCorrectRequestType()
        {
            _monitor.ProcessRequest(CreateExternalRequest<UpdateCapacityRequestDetails>(ExternalRequestType.Allocate));
            _capacityServiceMock.AssertWasCalled(x => x.AllocateCapacity(Arg<AllocationDetails>.Is.Anything));
        }

        [Test]
        public void DeallocateSendsCorrectRequestType()
        {
            _monitor.ProcessRequest(CreateExternalRequest<UpdateCapacityRequestDetails>(ExternalRequestType.Deallocate));
            _capacityServiceMock.AssertWasCalled(x => x.DeallocateCapacity(Arg<AllocationDetails>.Is.Anything));
        }

        [Test]
        public void AllocateDeallocateSendsCorrectRequestType()
        {
            _monitor.ProcessRequest(CreateExternalRequest<UpdateCapacityRequestDetailsExtended>(ExternalRequestType.AllocateDeallocate));
            _capacityServiceMock.AssertWasCalled(x => x.AllocateCapacity(Arg<AllocationDetails>.Is.Anything));
            _capacityServiceMock.AssertWasCalled(x => x.DeallocateCapacity(Arg<AllocationDetails>.Is.Anything));
        }

        [Test]
        public void NewConsignmentSendsCorrectRequestType()
        {
            _monitor.ProcessRequest(CreateExternalRequest<ConsignmentRequestDetailsExtended>(ExternalRequestType.NewConsignment));
            _deliveryNotificationServiceMock.AssertWasCalled(x => x.NewConsignment(Arg<ConsignmentDetails>.Is.Anything));
        }

        [Test]
        public void UpdateConsignmentSendsCorrectRequestType()
        {
            _monitor.ProcessRequest(CreateExternalRequest<ConsignmentRequestDetailsExtended>(ExternalRequestType.UpdateConsignment));
            _deliveryNotificationServiceMock.AssertWasCalled(x => x.UpdateConsignment(Arg<ConsignmentDetails>.Is.Anything));
        }

        [Test]
        public void CancelConsignmentSendsCorrectRequestType()
        {
            _monitor.ProcessRequest(CreateExternalRequest<ConsignmentRequestDetails>(ExternalRequestType.CancelConsignment));
            _deliveryNotificationServiceMock.AssertWasCalled(x => x.CancelConsignment(Arg<ConsignmentDetails>.Is.Anything));
        }

        [Test]
        public void ProcessRequestForInsertUserCallsCorrectMethod()
        {
            _monitor.ProcessRequest(CreateExternalRequest<UserManagmentRequestDetails>(ExternalRequestType.InsertUser));
            _userManagementServiceMock.AssertWasCalled(x => x.NewUser(Arg<UserManagmentDetails>.Is.Anything));
        }

        [Test]
        public void ProcessRequestForModifiedUserCallsCorrectMethod([Values(ExternalRequestType.UpdateUser, ExternalRequestType.DeleteUser)]string requestType)
        {
            _monitor.ProcessRequest(CreateExternalRequest<UserManagmentRequestDetails>(requestType));
            _userManagementServiceMock.AssertWasCalled(x => x.UpdateUser(Arg<UserManagmentDetails>.Is.Anything));
        }

        [Test]
        public void ProcessRequestForUserUpdatesUser([Values(ExternalRequestType.InsertUser, ExternalRequestType.UpdateUser, ExternalRequestType.DeleteUser)]string requestType)
        {
            var detail = CreateUserManagmentRequestDetails();
            _monitor.ProcessRequest(CreateExternalRequest(requestType, detail));
            _userRepositoryMock.AssertWasCalled(x => x.UpdateSynchronizedWhen(Arg<int>.Is.Equal(detail.Id)));
        }

        [Test]
        public void ProcessRequestFailedFatalyForNewUserUpdatesUser()
        {
            var detail = CreateUserManagmentRequestDetails();
            _userManagementServiceMock.Stub(x => x.NewUser(Arg<UserManagmentDetails>.Is.Anything))
                .Throw(new BaseServiceClientException(BaseServiceClientException.ErrorSeverity.Fatal, "ERROR"));

            _monitor.ProcessRequest(CreateExternalRequest(ExternalRequestType.InsertUser, detail));
            _userRepositoryMock.AssertWasCalled(x => x.UpdateSynchronizationFailedWhen(Arg<int>.Is.Equal(detail.Id)));
        }

        [Test]
        public void ProcessRequestFailedFatalyForModifiedUserUpdatesUser([Values(ExternalRequestType.UpdateUser, ExternalRequestType.DeleteUser)]string requestType)
        {
            var detail = CreateUserManagmentRequestDetails();
            _userManagementServiceMock.Stub(x => x.UpdateUser(Arg<UserManagmentDetails>.Is.Anything))
                .Throw(new BaseServiceClientException(BaseServiceClientException.ErrorSeverity.Fatal, "ERROR"));

            _monitor.ProcessRequest(CreateExternalRequest(requestType, detail));
            _userRepositoryMock.AssertWasCalled(x => x.UpdateSynchronizationFailedWhen(Arg<int>.Is.Equal(detail.Id)));
        }

        [Test]
        public void ProcessRequestFailedUnexpectedlyForNewUserUpdateUser()
        {
            var detail = CreateUserManagmentRequestDetails();
            _userManagementServiceMock.Stub(x => x.NewUser(Arg<UserManagmentDetails>.Is.Anything))
                .Throw(new Exception("ERROR"));

            _monitor.ProcessRequest(CreateExternalRequest(ExternalRequestType.InsertUser, detail));
            _userRepositoryMock.AssertWasCalled(x => x.UpdateSynchronizationFailedWhen(Arg<int>.Is.Equal(detail.Id)));
        }

        [Test]
        public void ProcessRequestFailedUnexpectedlyForModifiedUserUpdateUser([Values(ExternalRequestType.UpdateUser, ExternalRequestType.DeleteUser)]string requestType)
        {
            var detail = CreateUserManagmentRequestDetails();
            _userManagementServiceMock.Stub(x => x.UpdateUser(Arg<UserManagmentDetails>.Is.Anything))
                .Throw(new Exception("ERROR"));

            _monitor.ProcessRequest(CreateExternalRequest(requestType, detail));
            _userRepositoryMock.AssertWasCalled(x => x.UpdateSynchronizationFailedWhen(Arg<int>.Is.Equal(detail.Id)));
        }

        [Test]
        public void ProcessRequestFailedNormalyForNewUserNotUpdateUser()
        {
            var detail = CreateUserManagmentRequestDetails();
            _userManagementServiceMock.Stub(x => x.NewUser(Arg<UserManagmentDetails>.Is.Anything))
                .Throw(new BaseServiceClientException(BaseServiceClientException.ErrorSeverity.Normal, "ERROR"));

            _monitor.ProcessRequest(CreateExternalRequest(ExternalRequestType.InsertUser, detail));
            _userRepositoryMock.AssertWasNotCalled(x => x.UpdateSynchronizationFailedWhen(Arg<int>.Is.Anything));
            _userRepositoryMock.AssertWasNotCalled(x => x.UpdateSynchronizedWhen(Arg<int>.Is.Anything));
        }

        [Test]
        public void ProcessRequestFailedNormalyForModifiedUserNotUpdateUser([Values(ExternalRequestType.UpdateUser, ExternalRequestType.DeleteUser)]string requestType)
        {
            var detail = CreateUserManagmentRequestDetails();
            _userManagementServiceMock.Stub(x => x.UpdateUser(Arg<UserManagmentDetails>.Is.Anything))
                .Throw(new BaseServiceClientException(BaseServiceClientException.ErrorSeverity.Normal, "ERROR"));

            _monitor.ProcessRequest(CreateExternalRequest(requestType, detail));
            _userRepositoryMock.AssertWasNotCalled(x => x.UpdateSynchronizationFailedWhen(Arg<int>.Is.Anything));
            _userRepositoryMock.AssertWasNotCalled(x => x.UpdateSynchronizedWhen(Arg<int>.Is.Anything));
        }

        private static UserManagmentRequestDetails CreateUserManagmentRequestDetails()
        {
            return new UserManagmentRequestDetails(){ Id = 18.ToString() };
        }

        private void AssertRequestWasUpdated(int status)
        {
            _extRequestsRepoMock.AssertWasCalled(x => x.UpdateRequest(Arg<long>.Is.Anything, Arg<int>.Is.Equal(status), Arg<string>.Is.Anything));
        }

        private ExternalRequest CreateExternalRequest<T>(string type, T details) where T : IExternalRequestDetails
        {
            var request = new ExternalRequest()
            {
                Status = ExternalRequestStatus.New,
                Source = "",
                Type = type,
                RetriesCount = 0
            };
            request.SetDetails<T>(details);
            return request;
        }

        private ExternalRequest CreateExternalRequest<T>(string type) where T : IExternalRequestDetails, new()
        {
            return CreateExternalRequest(type, new T());
        }

    }
}
