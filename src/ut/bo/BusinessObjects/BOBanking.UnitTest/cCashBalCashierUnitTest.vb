﻿<TestClass()> Public Class cCashBalCashierUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub RequirementOn_FloatIssuedValue_Null_FloatReturnedValue_Null_ReturnZero()

        Dim X As New cCashBalCashier
        Dim Value As System.Nullable(Of Decimal) = 0

        Assert.AreEqual(Value, X.FloatIssuedMinusReturned(ArrangeDataTableFloatIssuesAndReturned(Nothing, Nothing)))

    End Sub

    <TestMethod()> Public Sub RequirementOn_FloatIssuedValue_Null_FloatReturnedValue_Ten_ReturnMinusTen()

        Dim X As New cCashBalCashier
        Dim Value As System.Nullable(Of Decimal) = 10

        Assert.AreEqual(Value * -1, X.FloatIssuedMinusReturned(ArrangeDataTableFloatIssuesAndReturned(Nothing, Value)))

    End Sub

    <TestMethod()> Public Sub RequirementOn_FloatIssuedValue_Ten_FloatReturnedValue_Null_ReturnTen()

        Dim X As New cCashBalCashier
        Dim Value As System.Nullable(Of Decimal) = 10

        Assert.AreEqual(Value, X.FloatIssuedMinusReturned(ArrangeDataTableFloatIssuesAndReturned(Value, Nothing)))

    End Sub

#End Region

#Region "Private Test Functions"

    Private Function ArrangeDataTableFloatIssuesAndReturned(ByVal Issued As System.Nullable(Of Decimal), _
                                                            ByVal Returned As System.Nullable(Of Decimal)) As DataTable

        Dim DT As DataTable

        DT = New DataTable

        DT.Columns.Add("COLUMN1", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("COLUMN2", System.Type.GetType("System.Decimal"))
        DT.Rows.Add(Issued, Returned)

        Return DT

    End Function

#End Region

End Class