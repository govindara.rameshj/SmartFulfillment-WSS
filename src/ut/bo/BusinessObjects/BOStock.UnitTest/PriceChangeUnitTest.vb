﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class PriceChangeUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub PriceChange_ZeroStoreID_ReturnExistingBusinessObject()
        Dim V As IPriceChange = Nothing

        Arrange(V, 0, 8120, 8076, New DataTable)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStock.cPriceChange", True, False))
    End Sub

    <TestMethod()> Public Sub PriceChange_EmptyEStoreShopID_And_EmptyEStoreWarehouseID_ReturnExistingBusinessObject()
        Dim V As IPriceChange = Nothing

        Arrange(V, 120, 0, 0, New DataTable)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStock.cPriceChange", True, False))
    End Sub

    <TestMethod()> Public Sub PriceChange_StoreID_DoesNotMatch_EStoreShopID_Or_EStoreWarehouseID_ReturnExistingBusinessObject()
        Dim V As IPriceChange = Nothing

        Arrange(V, 100, 8120, 8076, New DataTable)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStock.cPriceChange", True, False))
    End Sub

    <TestMethod()> Public Sub PriceChange_StoreID_Match_EStoreShopID_ReturnNewBusinessObject()
        Dim V As IPriceChange = Nothing

        Arrange(V, 120, 8120, 0, New DataTable)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStock.PriceChange", True, False))
    End Sub

    <TestMethod()> Public Sub PriceChange_StoreID_Match_EStoreWarehouseID_ReturnNewBusinessObject()
        Dim V As IPriceChange = Nothing

        Arrange(V, 76, 0, 8076, New DataTable)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStock.PriceChange", True, False))
    End Sub

#End Region

#Region "Private Test Procedures And Functions"

    Private Sub Arrange(ByRef V As IPriceChange, ByVal StoreID As Integer, ByVal eStoreShopID As Integer, ByVal eStoreWarehouseID As Integer, ByRef DT As DataTable)

        Dim Stub As New PriceChangeRepositoryStub

        Stub.ConfigureStub(StoreID, eStoreShopID, eStoreWarehouseID, DT)
        PriceChangeRepositoryFactory.FactorySet(Stub)
        V = PriceChangeFactory.FactoryGet

    End Sub

#End Region

End Class
