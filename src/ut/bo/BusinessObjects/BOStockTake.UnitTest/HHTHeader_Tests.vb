﻿<TestClass()> Public Class HHTHeader

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub HHTHeader_RequirementParameterMissing_ReturnExistingBusinessObject()
        Dim V As IHHTHeader = Nothing

        Arrange(V, New System.Nullable(Of Boolean))
        Assert.AreEqual("BOStockTake.cHhtHeader", V.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub HHTHeader_SetStubWithRequirementEnabled_ReturnsEnabledStub()
        Dim V As IHHTHeader = Nothing

        V = SetStubWithRequirementDisabled()

        Assert.AreEqual(true, V.GetType..)
    End Sub

    <TestMethod()> Public Sub HHTHeader_RequirementParameterTrue_ReturnNewBusinessObject()
        Dim V As IHHTHeader = Nothing

        V = SetStubWithRequirementEnabled()

        Assert.AreEqual("BOStockTake.HHTHeader", V.GetType.FullName)
    End Sub

#End Region

#Region "Private Test Procedures And Functions"

    Private Function SetStubWithRequirementEnabled() As IHHTHeader

        Dim Stub As New HHTHeaderRepositoryStub

        Stub.ConfigureStub(True)
        HHTHeaderRepositoryFactory.FactorySet(Stub)

        Return HHTHeaderFactory.FactoryGet

    End Function

    Private Function SetStubWithRequirementDisabled() As IHHTHeader

        Dim Stub As New HHTHeaderRepositoryStub

        Stub.ConfigureStub(False)
        HHTHeaderRepositoryFactory.FactorySet(Stub)

        Return HHTHeaderFactory.FactoryGet

    End Function
#End Region

End Class
