﻿Imports System.Data
Imports System.Text
Imports Cts.Oasys.Data

<TestClass()> Public Class RefundUnitTest

    Private testContextInstance As TestContext
    Private dtable As DataTable
    Private DT As DataTable
    Private skuItems As StringBuilder
    Private MinPrice As Decimal = 25
    Private RandPercent As Integer = 10
    Private SelectedDate As Date = CDate("2011-07-05")


    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>

    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"

    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '

#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub PicRefund_RequirementParameterTrue_Return_BOStockTakeRefundNew_BusinessObject()

        Dim V As IRefund = Nothing

        ArrangePicRefund(V, True)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStockTake.RefundNew", True, False))

    End Sub
    <TestMethod()> Public Sub PicRefund_RequirementParameterFalse_Return_BOStockTakeRefundNew_BusinessObject()

        Dim V As IRefund = Nothing

        ArrangePicRefund(V, False)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStockTake.RefundNew", True, False))

    End Sub


    <TestMethod()> Public Sub PicRefundRepository_RequirementParameterTrue_Return_RefundNewRepository()

        Dim V As IRefundRepository = Nothing

        ArrangePicRefundRepository(V, True)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStockTake.RefundRepositoryNew", True, False))

    End Sub

    <TestMethod()> Public Sub PicRefundRepository_RequirementParameterFalse_Return_RefundNewRepository()

        Dim V As IRefundRepository = Nothing

        ArrangePicRefundRepository(V, False)
        Assert.AreEqual(True, IIf(V.GetType.FullName = "BOStockTake.RefundRepositoryNew", True, False))

    End Sub

    <TestMethod()> Public Sub RefundNew_RecordCountCheck_ExactMatch()

        Dim RefundNew As IRefund = Nothing
        Dim RefundNewDT As DataTable
        Dim DynamicSequelDT As DataTable
        Dim SelectedDate As Date

        'arrange & act: add data
        SelectedDate = Now.AddDays(-1)
        InsertHHTHDR(SelectedDate, False, False)
        InsertHHTDET(SelectedDate, "111111", False, False, False, "R")

        'arrange & act: get data
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(SelectedDate, 0)

        ArrangePicRefund(RefundNew, True)
        RefundNewDT = RefundNew.GetRefundedLinesNotPreviouslyCounted(SelectedDate)

        'arrange & act: delete data
        DeleteHHTDET(SelectedDate, "111111")
        DeleteHHTHDR(SelectedDate)

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsTrue(DataTableRecordCountMatch(DynamicSequelDT, RefundNewDT))

    End Sub

    <TestMethod()> Public Sub RefundNew_FieldNameCheck_ExactMatch()

        Dim RefundNew As IRefund = Nothing
        Dim RefundNewDT As DataTable
        Dim DynamicSequelDT As DataTable
        Dim SelectedDate As Date

        'arrange & act: add data
        SelectedDate = Now.AddDays(-1)
        InsertHHTHDR(SelectedDate, False, False)
        InsertHHTDET(SelectedDate, "111111", False, False, False, "R")

        'arrange & act: get data
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(SelectedDate, 0)

        ArrangePicRefund(RefundNew, True)
        RefundNewDT = RefundNew.GetRefundedLinesNotPreviouslyCounted(SelectedDate)

        'arrange & act: delete data
        DeleteHHTDET(SelectedDate, "111111")
        DeleteHHTHDR(SelectedDate)

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsTrue(DataTableFieldMatch(DynamicSequelDT, RefundNewDT))

    End Sub

    <TestMethod()> Public Sub RefundNew_DataCheck_ExactMatch()

        Dim RefundNew As IRefund = Nothing
        Dim RefundNewDT As DataTable
        Dim DynamicSequelDT As DataTable
        Dim SelectedDate As Date

        'arrange & act: add data
        SelectedDate = Now.AddDays(-1)
        InsertHHTHDR(SelectedDate, False, False)
        InsertHHTDET(SelectedDate, "111111", False, False, False, "R")

        'arrange & act: get data
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(SelectedDate, 0)

        ArrangePicRefund(RefundNew, True)
        RefundNewDT = RefundNew.GetRefundedLinesNotPreviouslyCounted(SelectedDate)

        'arrange & act: delete data
        DeleteHHTDET(SelectedDate, "111111")
        DeleteHHTHDR(SelectedDate)

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsTrue(DataTableDataMatch(DynamicSequelDT, RefundNewDT, "SKUN asc"))

    End Sub


    'database tests for stored procedures are held in project StockAdjustment.Database->HandHeldTerminalDetailRepository.vb
    'following test are invalid because if no data is returned then test passes!



    'too time comsuming; need to generate data for DLTOTS, DLPAID, DLLINE

    '<TestMethod()> Public Sub GetRefundedLinesOverThresholdValue_StoredProcedure_RecordCountCheck()

    '    DT = PrepareDataTable(PrepareDynamicQueryGetRefundedLinesOverThresholdValue())

    '    RefundFactory.FactorySet(New RefundNew())
    '    RefundRepositoryFactory.FactorySet(New RefundRepositoryNew())
    '    dtable = RefundFactory.FactoryGet().GetRefundedLinesOverThresholdValue(SelectedDate, MinPrice)

    '    If DT.Rows.Count > 0 And Not dtable Is Nothing Then
    '        Assert.AreEqual(DT.Rows.Count, dtable.Rows.Count)
    '    End If
    'End Sub



    'refactored above

    '<TestMethod()> Public Sub GetRefundedLinesNotPreviouslyCounted_StoredProcedure_RecordCountCheck()

    '    DT = PrepareDataTable(PrepareDynamicQueryGetRefundedLinesNotPreviouslyCounted())
    '    RefundFactory.FactorySet(New RefundNew())
    '    RefundRepositoryFactory.FactorySet(New RefundRepositoryNew())
    '    dtable = RefundFactory.FactoryGet().GetRefundedLinesNotPreviouslyCounted(Today.AddDays(-7))

    '    If DT.Rows.Count > 0 And Not dtable Is Nothing Then
    '        Assert.AreEqual(DT.Rows.Count, dtable.Rows.Count)
    '    End If
    'End Sub



    'Cannot unit test a process that returns a random result set

    '<TestMethod()> Public Sub GetRefundedLinesPseudoRandomSample_StoredProcedure_RecordCountCheck()

    '    DT = PrepareDataTable(PrepareDynamicQueryGetRefundedLinesPseudoRandomSample())
    '    RefundFactory.FactorySet(New RefundNew())
    '    RefundRepositoryFactory.FactorySet(New RefundRepositoryNew())
    '    dtable = RefundFactory.FactoryGet().GetRefundedLinesPseudoRandomSample(SelectedDate, MinPrice, RandPercent)

    '    If DT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

    '    If DT.Rows.Count > 0 And Not dtable Is Nothing Then
    '        Assert.AreEqual(DT.Rows.Count, dtable.Rows.Count)
    '    End If

    'End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangePicRefund(ByRef V As IRefund, ByVal RequirementEnabled As System.Nullable(Of Boolean))

        Dim Stub As New TpWickes.RequirementRepositoryStub

        Stub.ConfigureStub(RequirementEnabled)
        TpWickes.RequirementRepositoryFactory.FactorySet(Stub)

        V = (New RefundFactory).GetImplementation

    End Sub

    Private Sub ArrangePicRefundRepository(ByRef V As IRefundRepository, ByVal RequirementEnabled As System.Nullable(Of Boolean))

        Dim Stub As New TpWickes.RequirementRepositoryStub

        AppDomain.CurrentDomain.SetData("APPBASE", "..\..\..\..\Staging Area\")

        Stub.ConfigureStub(RequirementEnabled)
        TpWickes.RequirementRepositoryFactory.FactorySet(Stub)

        V = (New RefundRepositoryFactory).GetImplementation

    End Sub

#Region "HHTDET: Create and Remove"

    Private Sub InsertHHTHDR(ByVal DATE1 As Date, ByVal IADJ As Boolean, ByVal IsLocked As Boolean)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                NotExist.Append("if not exists(select * from HHTHDR where DATE1 = '" & DATE1.ToString("yyyy-MM-dd") & "')")

                SB.Append(NotExist.ToString)
                SB.Append("begin")
                SB.Append("   insert HHTHDR (DATE1, IADJ, IsLocked) ")
                SB.Append("          values ('" & DATE1.ToString("yyyy-MM-dd") & "', ")
                SB.Append("                  '" & IADJ.ToString & "', ")
                SB.Append("                  '" & IsLocked.ToString & "' ")
                SB.Append("                 )")
                SB.Append("end")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub InsertHHTDET(ByVal DATE1 As Date, ByVal SKUN As String, ByVal ICNT As Boolean, ByVal IADJ As Boolean, _
                             ByVal LBOK As Boolean, ByVal ORIG As String)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                NotExist.Append("if not exists(select * from HHTDET where DATE1 = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                NotExist.Append("                                   and   ORIG  = 'R' ")
                NotExist.Append("                                   and   ICNT  = '0' ")
                NotExist.Append("             )")

                SB.Append(NotExist.ToString)
                SB.Append("begin")
                SB.Append("   insert HHTDET (DATE1, SKUN, ICNT, IADJ, LBOK, ORIG) ")
                SB.Append("          values ('" & DATE1.ToString("yyyy-MM-dd") & "', ")
                SB.Append("                  '" & SKUN & "', ")
                SB.Append("                  '" & ICNT.ToString & "', ")
                SB.Append("                  '" & IADJ.ToString & "', ")
                SB.Append("                  '" & LBOK.ToString & "', ")
                SB.Append("                  '" & ORIG & "' ")
                SB.Append("                 )")
                SB.Append("end")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub DeleteHHTHDR(ByVal DATE1 As Date)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                SB.Append("delete HHTHDR where DATE1 = '" & DATE1.ToString("yyyy-MM-dd") & "' ")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub DeleteHHTDET(ByVal DATE1 As Date, ByVal SKUN As String)

        Dim SB As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                SB.Append("delete HHTDET ")
                SB.Append("where DATE1  = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                SB.Append("and   SKUN   = '" & SKUN & "' ")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

#End Region

    Private Function LoadRefundedItemsExistingDynamicSequel(ByVal DateCreated As Date, ByVal Offset As Integer) As DataTable

        Dim SB As New StringBuilder
        Dim DT As DataTable

        SB.Append("Select Distinct SKUN from HHTDET where ORIG = 'R' and ICNT = '0' and DATE1 >= ")
        SB.Append("'" & Format(DateCreated.AddDays(Offset), "yyyy-MM-dd") & "'")

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using
        Return DT

    End Function

#Region "DataTable"

    Private Function DataTableRecordCountMatch(ByRef DT1 As DataTable, ByRef DT2 As DataTable) As Boolean

        If DT1.Rows.Count = DT2.Rows.Count Then

            Return True

        Else

            Return False

        End If

    End Function

    Private Function DataTableFieldMatch(ByRef DT1 As DataTable, ByRef DT2 As DataTable) As Boolean

        Try
            'compare DT1 against DT2
            For ColumnIndex As Integer = 0 To DT1.Columns.Count - 1 Step 1

                If DT1.Columns.Item(ColumnIndex).ColumnName.ToUpper <> DT2.Columns.Item(ColumnIndex).ColumnName.ToUpper Then Return False

            Next
            'compare DT2 against DT1
            For ColumnIndex As Integer = 0 To DT2.Columns.Count - 1 Step 1

                If DT2.Columns.Item(ColumnIndex).ColumnName.ToUpper <> DT1.Columns.Item(ColumnIndex).ColumnName.ToUpper Then Return False

            Next

        Catch ex As Exception
            'most likely error is missing column
            Return False

        End Try

        Return True

    End Function

    Private Function DataTableDataMatch(ByRef DT1 As DataTable, ByRef DT2 As DataTable, Optional ByVal OrderBy As String = "") As Boolean

        Dim RowCount As Integer
        Dim ColumnCount As Integer

        Try
            'compare DT1 against DT2
            RowCount = DT1.Rows.Count - 1
            ColumnCount = DT1.Columns.Count - 1

            If OrderBy.Length > 0 Then

                DT1.DefaultView.Sort = OrderBy
                DT2.DefaultView.Sort = OrderBy

            End If

            For RowIndex As Integer = 0 To RowCount Step 1
                For ColumnIndex As Integer = 0 To ColumnCount Step 1

                    Select Case DT1.Columns.Item(ColumnIndex).DataType.FullName
                        Case "System.Int32"
                            If CType(DT1.Rows.Item(RowIndex).Item(ColumnIndex), Integer) <> CType(DT2.Rows.Item(RowIndex).Item(ColumnIndex), Integer) Then Return False

                        Case "System.String"
                            If CType(DT1.Rows.Item(RowIndex).Item(ColumnIndex), String) <> CType(DT2.Rows.Item(RowIndex).Item(ColumnIndex), String) Then Return False

                    End Select

                Next
            Next
            'compare DT2 against DT1
            RowCount = DT2.Rows.Count - 1
            ColumnCount = DT2.Columns.Count - 1

            For RowIndex As Integer = 0 To RowCount Step 1
                For ColumnIndex As Integer = 0 To ColumnCount Step 1

                    Select Case DT2.Columns.Item(ColumnIndex).DataType.FullName
                        Case "System.Int32"
                            If CType(DT2.Rows.Item(RowIndex).Item(ColumnIndex), Integer) <> CType(DT1.Rows.Item(RowIndex).Item(ColumnIndex), Integer) Then Return False

                        Case "System.String"
                            If CType(DT2.Rows.Item(RowIndex).Item(ColumnIndex), String) <> CType(DT1.Rows.Item(RowIndex).Item(ColumnIndex), String) Then Return False

                    End Select

                Next
            Next

        Catch ex As Exception

            'most likely error is missing column, or mismatched data types
            Return False

        End Try

        Return True

    End Function

#End Region

    'Private Function PrepareDataTable(ByVal query As String) As DataTable
    '    DT = Nothing
    '    Using con As New Connection
    '        Using com As New Command(con)
    '            Select Case con.DataProvider
    '                Case DataProvider.Sql
    '                    com.CommandText = query
    '                    DT = com.ExecuteDataTable
    '            End Select
    '        End Using
    '    End Using
    '    Return DT
    'End Function

    'Private Function PrepareDynamicQueryGetRefundedLinesOverThresholdValue() As String
    '    skuItems = New StringBuilder()
    '    skuItems.Append("Select dl.SKUN From DLLINE as dl ")
    '    skuItems.Append("inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
    '    skuItems.Append("dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN] ")
    '    skuItems.Append("inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and ")
    '    skuItems.Append("dp.TILL = dl.TILL and dp.[TRAN] = dl.[TRAN] ")
    '    skuItems.Append("where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
    '    skuItems.Append("and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' ")
    '    skuItems.Append("and dl.DATE1 = ")
    '    skuItems.Append("'" & Format(SelectedDate, "yyyy-MM-dd") & "'")
    '    skuItems.Append(" group by dl.skun, dl.[tran] having sum(dl.extp) <= -")
    '    skuItems.Append(MinPrice)
    '    Return skuItems.ToString
    'End Function

    'Private Function PrepareDynamicQueryGetRefundedLinesNotPreviouslyCounted() As String
    '    skuItems = New StringBuilder()
    '    skuItems.Append("Select  Distinct SKUN  from HHTDET where ORIG = 'R' and ICNT = '0' and DATE1 >= ")
    '    skuItems.Append("'" & Format(Today.AddDays(-7), "yyyy-MM-dd") & "'")
    '    Return skuItems.ToString
    'End Function

    'Private Function PrepareDynamicQueryGetRefundedLinesPseudoRandomSample() As String
    '    skuItems = New StringBuilder()
    '    skuItems.Append(" SELECT TOP " & RandPercent)
    '    skuItems.Append(" PERCENT SKUN  From DLLINE as dl")
    '    skuItems.Append(" inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
    '    skuItems.Append(" dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN]  ")
    '    skuItems.Append(" inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and  ")
    '    skuItems.Append(" dp.TILL = dl.TILL and dp.[TRAN] = dl.[TRAN]  ")
    '    skuItems.Append(" where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
    '    skuItems.Append(" and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' and dl.DATE1 = ")
    '    skuItems.Append("'" & Format(SelectedDate, "yyyy-MM-dd") & "'")
    '    skuItems.Append(" and dl.SKUN not in (Select SKUN  From DLLINE as dl ")
    '    skuItems.Append(" inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
    '    skuItems.Append(" dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN] ")
    '    skuItems.Append(" inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and ")
    '    skuItems.Append(" dp.TILL = dl.TILL and dp.[TRAN] = dl.[TRAN] ")
    '    skuItems.Append(" where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
    '    skuItems.Append(" and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' ")
    '    skuItems.Append(" and dl.DATE1 = ")
    '    skuItems.Append("'" & Format(SelectedDate, "yyyy-MM-dd") & "'")
    '    skuItems.Append(" group by dl.skun, dl.[tran] having sum(dl.extp) <= -")
    '    skuItems.Append(MinPrice)
    '    skuItems.Append(" )group by dl.SKUN, dl.[TRAN]  ORDER BY NEWID() ")
    '    Return skuItems.ToString
    'End Function

#End Region

End Class
