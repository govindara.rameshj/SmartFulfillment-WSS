﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports OasysDBBO
Imports System.Data

<TestClass()> Public Class StkmasOnorUpdate_Tests

    Private testContextInstance As TestContext
    Private isulin As BOPurchases.cIssueLine
    Private oasys3dbConnection As OasysDBBO.Oasys3.DB.clsOasys3DB

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub GetOnOrderValue_StkmasOnorUpdateExisting_PassedDecimalValue_ReturnsExactValue()

        isulin = New BOPurchases.cIssueLine()
        oasys3dbConnection = New OasysDBBO.Oasys3.DB.clsOasys3DB
        isulin = PopulateIsuLineDate(isulin, CDec(100.4))
        Assert.AreEqual(CDec(100.4), New StkmasOnorUpdateFactory().ImplementationB.GetOnOrderValue(oasys3dbConnection, isulin))

    End Sub
    <TestMethod()> Public Sub GetOnOrderValue_StkmasOnorUpdateExisting_PassedValue0_Return0()

        isulin = New BOPurchases.cIssueLine()
        oasys3dbConnection = New OasysDBBO.Oasys3.DB.clsOasys3DB
        isulin = PopulateIsuLineDate(isulin, 0)
        Assert.AreEqual(CDec(0), New StkmasOnorUpdateFactory().ImplementationB.GetOnOrderValue(oasys3dbConnection, isulin))

    End Sub

    <TestMethod()> Public Sub GetOnOrderValue_StkmasOnorUpdateExisting_PassedValueNothing_Return0()

        isulin = New BOPurchases.cIssueLine()
        oasys3dbConnection = New OasysDBBO.Oasys3.DB.clsOasys3DB
        isulin = PopulateIsuLineDate(isulin, Nothing)
        Assert.AreEqual(CDec(0), New StkmasOnorUpdateFactory().ImplementationB.GetOnOrderValue(oasys3dbConnection, isulin))

    End Sub


    <TestMethod()> Public Sub GetOnOrderValue_StkmasOnorUpdateNew_PassAnyValue_ReturnsPopulatedDataTableValue10()
        Dim stkmasUpdateInstance As New TestStkmasOnorUpdate
        isulin = New BOPurchases.cIssueLine()
        oasys3dbConnection = New OasysDBBO.Oasys3.DB.clsOasys3DB
        isulin = PopulateIsuLineDate(isulin, Nothing)
        Assert.AreEqual(CDec(10), stkmasUpdateInstance.GetOnOrderValue(oasys3dbConnection, isulin))

    End Sub


    <TestMethod()> Public Sub GetPurlinRecord_StkmasOnorRepository_PassSkuNumberAndPoNumber_ReturnsPopulatedDataTableValue10point2()
        Dim stkmasUpdateRepositoryInstance As New TestStkmasOnorUpdateRepository
        isulin = New BOPurchases.cIssueLine()
        oasys3dbConnection = New OasysDBBO.Oasys3.DB.clsOasys3DB
        isulin = PopulateIsuLineDate(isulin, Nothing)
        Assert.AreEqual(CDec(10.2), stkmasUpdateRepositoryInstance.GetPurlinRecord(oasys3dbConnection, "100803", "001897").Rows(0)(0))

    End Sub

    <TestMethod()> Public Sub GetPurlinRecord_StkmasOnorRepository_PassSkuNumberAndPoNumber_ReturnsPopulatedDataTableWithOneRow()
        Dim stkmasUpdateRepositoryInstance As New TestStkmasOnorUpdateRepository
        isulin = New BOPurchases.cIssueLine()
        oasys3dbConnection = New OasysDBBO.Oasys3.DB.clsOasys3DB
        isulin = PopulateIsuLineDate(isulin, Nothing)
        Assert.AreEqual(1, stkmasUpdateRepositoryInstance.GetPurlinRecord(oasys3dbConnection, "100803", "001897").Rows.Count)

    End Sub

    Private Function PopulateIsuLineDate(ByVal isulin As BOPurchases.cIssueLine, ByVal onOrder As Decimal) As BOPurchases.cIssueLine
        Dim _IssueNumber As New ColField(Of String)("NUMB", "086772", "Issue Number", True, False)
        Dim _LineNumber As New ColField(Of String)("LINE", "002", "Line Number", True, False)
        Dim _SkuNumber As New ColField(Of String)("SKUN", "100803", "SKU Number", False, False)
        Dim _QtyOrdered As New ColField(Of Decimal)("QTYO", onOrder, "Quantity Ordered", False, False)

        isulin.IssueNumber = _IssueNumber
        isulin.LineNumber = _LineNumber
        isulin.SkuNumber = _SkuNumber
        isulin.QtyOrdered = _QtyOrdered
        Return isulin
    End Function



    Public Class TestStkmasOnorUpdate
        Inherits StkmasOnorUpdateNew

        Private dt As New DataTable

        Public Overrides Function GetOnOrderValue(ByRef conn As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal isulin As BOPurchases.cIssueLine) As Decimal
            populateDataReader()
            Return CDec(dt.Rows(0)(0))
        End Function

        Private Sub populateDataReader()
            dt.Columns.Add("QTYO", System.Type.GetType("System.Decimal"))
            dt.Rows.Add(10)
        End Sub

    End Class

    Public Class TestStkmasOnorUpdateRepository
        Inherits StkmasOnorRepository

        Public Overrides Function GetPurlinRecord(ByRef oasysConnection As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal skuNumber As String, ByVal purchaseOrderNumber As String) As DataTable
            Return populateDataTable()
        End Function

        Private Function populateDataTable() As DataTable

            Dim dt As New DataTable
            dt.Columns.Add("QTYO", System.Type.GetType("System.Decimal"))
            dt.Rows.Add(10.2)
            Return dt

        End Function

    End Class


End Class
