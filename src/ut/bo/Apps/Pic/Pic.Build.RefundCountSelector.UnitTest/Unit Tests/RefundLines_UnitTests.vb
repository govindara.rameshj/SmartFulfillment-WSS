﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class RefundLines_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub TestMethod1()
        ' TODO: Add test logic here
    End Sub


#Region "Method Tests"

#Region "TotalIsOverLimit Tests"

    <TestMethod()> _
    Public Sub TotalIsOverLimit_Total5_01AndLimit5_ReturnTrue()
        Dim testRefundTran As New RefundLines
        Dim testLine As New TestRefundLineTotalIs5_01

        testRefundTran.Lines.Add(testLine)
        Assert.IsTrue(testRefundTran.TotalIsOverLimit(5D))
    End Sub

    <TestMethod()> _
    Public Sub TotalIsOverLimit_Total4_99AndLimit5_ReturnFalse()
        Dim testRefundTran As New RefundLines
        Dim testLine As New TestRefundLineTotalIs4_99

        testRefundTran.Lines.Add(testLine)
        Assert.IsFalse(testRefundTran.TotalIsOverLimit(5D))
    End Sub

    <TestMethod()> _
    Public Sub TotalIsOverLimit_Total5AndLimit5_ReturnFalse()
        Dim testRefundTran As New RefundLines
        Dim testLine As New TestRefundLineTotalIs5Exactly

        testRefundTran.Lines.Add(testLine)
        Assert.IsFalse(testRefundTran.TotalIsOverLimit(5D))
    End Sub
#End Region

#Region "TotalIsOnOrOverLimit Tests"

    <TestMethod()> _
    Public Sub TotalIsOnOrOverLimit_Total5_01AndLimit5_ReturnTrue()
        Dim testRefundTran As New RefundLines
        Dim testLine As New TestRefundLineTotalIs5_01

        testRefundTran.Lines.Add(testLine)
        Assert.IsTrue(testRefundTran.TotalIsOnOrOverLimit(5D))
    End Sub

    <TestMethod()> _
    Public Sub TotalIsOnOrOverLimit_Total4_99AndLimit5_ReturnFalse()
        Dim testRefundTran As New RefundLines
        Dim testLine As New TestRefundLineTotalIs4_99

        testRefundTran.Lines.Add(testLine)
        Assert.IsFalse(testRefundTran.TotalIsOnOrOverLimit(5D))
    End Sub

    <TestMethod()> _
    Public Sub TotalIsOnOrOverLimit_Total5AndLimit5_ReturnTrue()
        Dim testRefundTran As New RefundLines
        Dim testLine As New TestRefundLineTotalIs5Exactly

        testRefundTran.Lines.Add(testLine)
        Assert.IsTrue(testRefundTran.TotalIsOnOrOverLimit(5D))
    End Sub
#End Region
#End Region
End Class
