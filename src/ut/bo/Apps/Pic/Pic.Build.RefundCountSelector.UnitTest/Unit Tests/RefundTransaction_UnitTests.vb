﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class RefundTransaction_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Constructor Tests"

    '''<summary>
    '''A test for RefundTransaction constructor
    '''</summary>
    <TestMethod()> _
    Public Sub RefundTransaction_Constructor_CreatesTypeOfCorrectFullName()
        Dim target As RefundTransaction = New RefundTransaction

        Assert.AreEqual("Pic.Build.RefundCountSelector.RefundTransaction", target.GetType.FullName)
    End Sub

    '''<summary>
    '''A test for RefundTransaction constructor initialising Lines collection
    '''</summary>
    <TestMethod()> _
    Public Sub RefundTransaction_Constructor_IntialisesLinesCollection()
        Dim target As RefundTransaction = New RefundTransaction
        Dim actual As List(Of RefundLine)

        actual = target.TransactionLines.Lines
        Assert.IsTrue(actual.Count = 0)
    End Sub

#End Region

#Region "Property Tests"

    '''<summary>
    '''A test for TransactionNumber
    '''</summary>
    <TestMethod()> _
    Public Sub TransactionNumber_PropertySetGetTest()
        Dim target As RefundTransaction = New RefundTransaction
        Dim expected As String = "1234"
        Dim actual As String

        target.TransactionNumber = expected
        actual = target.TransactionNumber
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for TransactionDate
    '''</summary>
    <TestMethod()> _
    Public Sub TransactionDate_PropertySetGetTest()
        Dim target As RefundTransaction = New RefundTransaction
        Dim expected As DateTime = #4/1/2012#
        Dim actual As DateTime

        target.TransactionDate = expected
        actual = target.TransactionDate
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for Till
    '''</summary>
    <TestMethod()> _
    Public Sub Till_PropertySetGetTest()
        Dim target As RefundTransaction = New RefundTransaction
        Dim expected As String = "11"
        Dim actual As String

        target.Till = expected
        actual = target.Till
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for Lines
    '''</summary>
    <TestMethod()> _
    Public Sub Lines_Property_PropertyGetTest()
        Dim target As RefundTransaction = New RefundTransaction
        Dim actual As List(Of RefundLine)

        actual = target.TransactionLines.Lines
        Assert.IsTrue(actual IsNot Nothing)
    End Sub
#End Region

#Region "Internals Tests"

#Region "GetTotal Tests"

    '''<summary>
    '''A test for GetTotal with no lines
    '''</summary>
    <TestMethod()> _
    Public Sub GetTotal_NoLines_Returns0()
        Dim target As RefundTransaction = New RefundTransaction
        Dim actual As [Decimal]

        actual = target.TransactionLines.GetTotal
        Assert.AreEqual(actual, 0D)
    End Sub

    '''<summary>
    '''A test for GetTotal with 1 line
    '''</summary>
    <TestMethod()> _
    Public Sub GetTotal_1Line_ReturnsLineTotalValue()
        Dim target As RefundTransaction = New RefundTransaction
        Dim refLine As New TestRefundLine(CDec(5.67))
        Dim actual As [Decimal]

        target.TransactionLines.Lines.Add(refLine)
        actual = target.TransactionLines.GetTotal
        Assert.AreEqual(actual, 5.67D)
    End Sub


    '''<summary>
    '''A test for GetTotal with 3 lines
    '''</summary>
    <TestMethod()> _
    Public Sub GetTotal_3Lines_ReturnsSumOfAllLines_TotalValues()
        Dim target As RefundTransaction = New RefundTransaction
        Dim actual As [Decimal]

        target.TransactionLines.Lines.Add(New TestRefundLine(CDec(1.23)))
        target.TransactionLines.Lines.Add(New TestRefundLine(CDec(4.56)))
        target.TransactionLines.Lines.Add(New TestRefundLine(CDec(7.89)))
        actual = target.TransactionLines.GetTotal
        Assert.AreEqual(actual, 13.68D)
    End Sub
#End Region
#End Region
End Class
