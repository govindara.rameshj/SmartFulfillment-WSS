﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.ComponentModel

<TestClass()> _
Public Class RefundLine_UnitTests
    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Constructor Tests"

    '''<summary>
    '''A test for RefundLine Constructor
    '''</summary>
    <TestMethod()> _
    Public Sub RefundLine_Constructor_CreatesTypeOfCorrectFullName()
        Dim target As RefundLine = New RefundLine

        Assert.AreEqual("Pic.Build.RefundCountSelector.RefundLine", target.GetType.FullName)
    End Sub
#End Region

#Region "Property Tests"

    '''<summary>
    '''A test for SkuNumber property get/set
    '''</summary>
    <TestMethod()> _
    Public Sub SkuNumber_PropertyGetSetTest()
        Dim target As RefundLine = New RefundLine
        Dim expected As String = "500300"
        Dim actual As String

        target.SkuNumber = expected
        actual = target.SkuNumber
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for Quantity property get/set
    '''</summary>
    <TestMethod()> _
    Public Sub Quantity_PropertyGetSetTest()
        Dim target As RefundLine = New RefundLine
        Dim expected As Integer = 1234
        Dim actual As Integer

        target.Quantity = expected
        actual = target.Quantity
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for OriginalTransactionNumber property get/set
    '''</summary>
    <TestMethod()> _
    Public Sub OriginalTransactionNumber_PropertyGetSetTest()
        Dim target As RefundLine = New RefundLine
        Dim expected As String = "1234"
        Dim actual As String

        target.OriginalTransactionNumber = expected
        actual = target.OriginalTransactionNumber
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for LineNumber property get/set
    '''</summary>
    <TestMethod()> _
    Public Sub LineNumber_PropertyGetSetTest()
        Dim target As RefundLine = New RefundLine
        Dim expected As Integer = 3
        Dim actual As Integer

        target.LineNumber = CShort(expected)
        actual = target.LineNumber
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for ItemPrice property get/set
    '''</summary>
    <TestMethod()> _
    Public Sub ItemPrice_PropertyGetSetTest()
        Dim target As RefundLine = New RefundLine
        Dim expected As Decimal = CDec(123.45)
        Dim actual As Decimal

        target.ItemPrice = expected
        actual = target.ItemPrice
        Assert.AreEqual(expected, actual)
    End Sub
#End Region

#Region "NoOriginalReceiptProvided Tests"

    <TestMethod()> _
    Public Sub NoOriginalReceiptProvided_OriginalTransactionNumber0000_ReturnTrue()
        Dim testLine As RefundLine = New RefundLine

        testLine.OriginalTransactionNumber = "0000"
        Assert.IsTrue(testLine.NoOriginalReceiptProvided())
    End Sub

    <TestMethod()> _
    Public Sub NoOriginalReceiptProvided_OriginalTransactionNumberNot0000_ReturnFalse()
        Dim testLine As RefundLine = New RefundLine

        testLine.OriginalTransactionNumber = "1111"
        Assert.IsFalse(testLine.NoOriginalReceiptProvided())
    End Sub

    <TestMethod()> _
    Public Sub NoOriginalReceiptProvided_OriginalTransactionNumberEmptyString_ReturnFalse()
        Dim testLine As RefundLine = New RefundLine

        testLine.OriginalTransactionNumber = String.Empty
        Assert.IsFalse(testLine.NoOriginalReceiptProvided())
    End Sub

    <TestMethod()> _
    Public Sub NoOriginalReceiptProvided_OriginalTransactionNumberBlankString_ReturnFalse()
        Dim testLine As RefundLine = New RefundLine

        testLine.OriginalTransactionNumber = ""
        Assert.IsFalse(testLine.NoOriginalReceiptProvided())
    End Sub
#End Region

#Region "TotalValue Tests"

    <TestMethod()> _
    Public Sub TotalValue_Quantity2AndItemPrice1_87_Returns3_74()
        Dim testLine As RefundLine = New RefundLine

        testLine.Quantity = 2
        testLine.ItemPrice = CDec(1.87)
        Assert.AreEqual(testLine.TotalValue, 3.74D)
    End Sub

    <TestMethod()> _
    Public Sub TotalValue_Quantity0AndItemPrice1_87_Returns0()
        Dim testLine As RefundLine = New RefundLine

        testLine.Quantity = 0
        testLine.ItemPrice = CDec(1.87)
        Assert.AreEqual(testLine.TotalValue, 0D)
    End Sub

    <TestMethod()> _
    Public Sub TotalValue_Quantity2AndItemPrice0_Returns0()
        Dim testLine As RefundLine = New RefundLine

        testLine.Quantity = 2
        testLine.ItemPrice = 0D
        Assert.AreEqual(testLine.TotalValue, 0D)
    End Sub

    <TestMethod()> _
    Public Sub TotalValue_Quantity2_veAndItemPrice1_87_Returns3_74_ve()
        Dim testLine As RefundLine = New RefundLine

        testLine.Quantity = -2
        testLine.ItemPrice = CDec(1.87)
        Assert.AreEqual(testLine.TotalValue, -3.74D)
    End Sub

    <TestMethod()> _
    Public Sub TotalValue_Quantity2AndItemPrice1_87_ve_Returns3_74_ve()
        Dim testLine As RefundLine = New RefundLine

        testLine.Quantity = 2
        testLine.ItemPrice = CDec(-1.87)
        Assert.AreEqual(testLine.TotalValue, -3.74D)
    End Sub
#End Region

#Region "TotalIsOverLimit Tests"

    <TestMethod()> _
    Public Sub TotalIsOverLimit_Total5_01AndLimit5_ReturnTrue()
        Dim testLine As New TestRefundLineTotalIs5_01

        Assert.IsTrue(testLine.TotalIsOverLimit(5D))
    End Sub

    <TestMethod()> _
    Public Sub TotalIsOverLimit_Total5AndLimit5_ReturnFalse()
        Dim testLine As New TestRefundLineTotalIs5Exactly

        Assert.IsFalse(testLine.TotalIsOverLimit(5D))
    End Sub

    <TestMethod()> _
    Public Sub TotalIsOverLimit_Total4_99AndLimit5_ReturnFalse()
        Dim testLine As New TestRefundLineTotalIs4_99

        Assert.IsFalse(testLine.TotalIsOverLimit(5D))
    End Sub
#End Region
End Class
