﻿Imports System
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Pic.Build.RefundCountSelector



'''<summary>
'''This is a test class for RefundSku_UnitTests and is intended
'''to contain all RefundSku_UnitTests Unit Tests
'''</summary>
<TestClass()> _
Public Class RefundSku_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

#Region "Constructor Tests"

    '''<summary>
    '''A test for RefundSku Constructor no parameters
    '''</summary>
    <TestMethod()> _
    Public Sub RefundSku_Constructor_WithNoParameters_CreatesRightType()
        Dim target As RefundSku = New RefundSku

        Assert.AreEqual("Pic.Build.RefundCountSelector.RefundSku", target.GetType.FullName)
    End Sub

    '''<summary>
    '''A test for RefundSku Constructor from RefundLine
    '''</summary>
    <TestMethod()> _
    Public Sub RefundSku_Constructor_FromRefundLine_CreatesRightType()
        Dim IntialiseFrom As RefundLine = Nothing
        Dim target As RefundSku = New RefundSku(IntialiseFrom)

        Assert.AreEqual("Pic.Build.RefundCountSelector.RefundSku", target.GetType.FullName)
    End Sub

    '''<summary>
    '''A test for RefundSku Constructor from RefundLine
    '''</summary>
    <TestMethod()> _
    Public Sub RefundSku_Constructor_FromRefundLine_InitialisesSkuNumber()
        Dim target As RefundSku
        Dim IntialiseFrom As RefundLine = New RefundLine()
        Dim expected As String = "123456"
        Dim actual As String

        With IntialiseFrom
            .SkuNumber = expected
        End With
        target = New RefundSku(IntialiseFrom)
        actual = target.Number

        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for RefundSku Constructor from RefundLine
    '''</summary>
    <TestMethod()> _
    Public Sub RefundSku_Constructor_FromRefundLine_InitialisesValue()
        Dim target As RefundSku
        Dim expected As Decimal = CDec(65.43)
        Dim IntialiseFrom As TestRefundLine = New TestRefundLine(expected)
        Dim actual As Decimal

        target = New RefundSku(IntialiseFrom)
        actual = target.Value

        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for RefundSku Constructor from SkuNumber and Value
    '''</summary>
    <TestMethod()> _
    Public Sub RefundSku_Constructor_FromSkuNumberAndValue_InitialisesSkuNumber()
        Dim expected As String = "246801"
        Dim SkuNumber As String = expected
        Dim Value As Decimal = CDec(75.31)
        Dim target As RefundSku = New RefundSku(SkuNumber, Value)
        Dim actual As String = target.Number

        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for RefundSku Constructor from SkuNumber and Value
    '''</summary>
    <TestMethod()> _
    Public Sub RefundSku_Constructor_FromSkuNumberAndValue_InitialisesValue()
        Dim expected As Decimal = CDec(75.31)
        Dim SkuNumber As String = "246801"
        Dim Value As Decimal = expected
        Dim target As RefundSku = New RefundSku(SkuNumber, Value)
        Dim actual As Decimal = target.Value

        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for RefundSku Constructor from SkuNumber and Value
    '''</summary>
    <TestMethod()> _
    Public Sub RefundSku_Constructor_FromSkuNumberAndValueAndRelatedItemSkuNumber_InitialisesSkuNumber()
        Dim expected As String = "246801"
        Dim SkuNumber As String = expected
        Dim Value As Decimal = CDec(75.31)
        Dim target As RefundSku = New RefundSku(SkuNumber, Value)
        Dim actual As String = target.Number

        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for RefundSku Constructor from SkuNumber and Value
    '''</summary>
    <TestMethod()> _
    Public Sub RefundSku_Constructor_FromSkuNumberAndValueAndRelatedItemSkuNumber_InitialisesValue()
        Dim expected As Decimal = CDec(75.31)
        Dim SkuNumber As String = "246801"
        Dim Value As Decimal = expected
        Dim target As RefundSku = New RefundSku(SkuNumber, Value)
        Dim actual As Decimal = target.Value

        Assert.AreEqual(expected, actual)
    End Sub
#End Region

#Region "Property Tests"

    '''<summary>
    '''A test for Number get/set
    '''</summary>
    <TestMethod()> _
    Public Sub Number_Test()
        Dim target As RefundSku = New RefundSku
        Dim expected As String = "500300"
        Dim actual As String

        target.Number = expected
        actual = target.Number
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for Value get/set
    '''</summary>
    <TestMethod()> _
    Public Sub Value_Test()
        Dim target As RefundSku = New RefundSku
        Dim expected As Decimal = CDec(36.22)
        Dim actual As Decimal

        target.Value = expected
        actual = target.Value

        Assert.AreEqual(expected, actual)
    End Sub
#End Region

#Region "Method Tests"

    '''<summary>
    '''A test for AddToValue method
    '''</summary>
    <TestMethod()> _
    Public Sub AddToValue_InitialiseTo66_66AndCallWith33_33_SetsValueTo99_99()
        Dim target As RefundSku = New RefundSku("753086", CDec(66.66))
        Dim ValueToAdd As Decimal = CDec(33.33)
        Dim expected As Decimal = CDec(99.99)
        Dim actual As Decimal

        target.AddToValue(ValueToAdd)
        actual = target.Value
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for AddToValue method
    '''</summary>
    <TestMethod()> _
    Public Sub AddToValue_InitialiseTo66_66AndCallWithNegative33_33_SetsValueTo33_33()
        Dim target As RefundSku = New RefundSku("753086", CDec(66.66))
        Dim ValueToAdd As Decimal = CDec(-33.33)
        Dim expected As Decimal = CDec(33.33)
        Dim actual As Decimal

        target.AddToValue(ValueToAdd)
        actual = target.Value
        Assert.AreEqual(expected, actual)
    End Sub
#End Region
End Class
