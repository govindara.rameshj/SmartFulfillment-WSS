﻿Imports System
Imports System.Data
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class RefundRepository_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Constructor Tests"

    '''<summary>
    '''A test for RefundRepository constructor
    '''</summary>
    <TestMethod()> _
    Public Sub RefundRepository_Constructor_CreatesTypeOfCorrectFullName()
        Dim target As RefundRepository = New RefundRepository

        Assert.AreEqual("Pic.Build.RefundCountSelector.RefundRepository", target.GetType.FullName)
    End Sub

    '''<summary>
    '''A test for RefundRepository constructor initialising Lines collection
    '''</summary>
    <TestMethod()> _
    Public Sub RefundRepository_Constructor_IntialisesTransactionsCollection()
        Dim target As RefundRepository = New RefundRepository
        Dim actual As List(Of RefundTransaction)

        actual = target.RefundTransactions
        Assert.IsTrue(actual.Count = 0)
    End Sub

    '''<summary>
    '''A test for RefundRepository constructor initialising Line Limit
    '''</summary>
    <TestMethod()> _
    Public Sub RefundRepository_Constructor_IntialisesLineLimit()
        Dim target As TestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77 = New TestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77
        Dim actual As Decimal

        actual = target.LineLimit
        Assert.IsTrue(actual = 33.66D)
    End Sub

    '''<summary>
    '''A test for RefundRepository constructor initialising SkuQuantity Limit
    '''</summary>
    <TestMethod()> _
    Public Sub RefundRepository_Constructor_IntialisesSkuQuantityLimit()
        Dim target As TestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77 = New TestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77
        Dim actual As Decimal

        actual = target.SkuQuantityLimit
        Assert.IsTrue(actual = 22)
    End Sub

    '''<summary>
    '''A test for RefundRepository constructor initialising Transaction Limit
    '''</summary>
    <TestMethod()> _
    Public Sub RefundRepository_Constructor_IntialisesTransactionLimit()
        Dim target As TestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77 = New TestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77
        Dim actual As Decimal

        actual = target.TransactionLimit
        Assert.IsTrue(actual = 44.77D)
    End Sub
#End Region

#Region "Method Tests"

#Region "GetRefundTransactions Tests"

    '''<summary>
    '''A test for GetRefundTransactions
    '''</summary>
    <TestMethod()> _
    Public Sub GetRefundTransactions_ReturnsSomething()
        Dim testRefundRep As New TestRefundRepository

        Assert.IsTrue(testRefundRep.GetRefundTransactions(Now, Now) IsNot Nothing)
    End Sub

    '''<summary>
    '''A test for GetRefundTransactions
    '''</summary>
    <TestMethod()> _
    Public Sub GetRefundTransactions_UsingSQLServerMinDates_ReturnsListOfRefundTransaction()
        Dim testRefundRep As New TestRefundRepository

        Dim Transactions As List(Of RefundTransaction) = testRefundRep.GetRefundTransactions(#1/1/1753#, #1/1/1753#)

        Assert.IsTrue(Transactions.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_3RefundTransactionLinesFromSameTransaction_Returns1Transaction()
        Dim testRefundRep As New TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToSupply3RefundTransactionLinesFromSameTransaction
        Dim Transactions As List(Of RefundTransaction) = testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(Transactions.Count = 1)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_3RefundTransactionLinesFromSameTransaction_ReturnsTransactionWith3Lines()
        Dim testRefundRep As New TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToSupply3RefundTransactionLinesFromSameTransaction
        Dim Transactions As List(Of RefundTransaction) = testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(Transactions(0).TransactionLines.Lines.Count = 3)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_2RefundTransactionLinesEachWith3Lines_Returns2Transactions()
        Dim testRefundRep As New TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToSupply2RefundTransactionEachWith3Lines
        Dim Transactions As List(Of RefundTransaction) = testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(Transactions.Count = 2)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_2RefundTransactionLinesEachWith3Lines_Returns1stTransactionWith3Lines()
        Dim testRefundRep As New TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToSupply2RefundTransactionEachWith3Lines
        Dim Transactions As List(Of RefundTransaction) = testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(Transactions(0).TransactionLines.Lines.Count = 3)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_2RefundTransactionLinesEachWith3Lines_Returns2ndTransactionWith3Lines()
        Dim testRefundRep As New TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToSupply2RefundTransactionEachWith3Lines
        Dim Transactions As List(Of RefundTransaction) = testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(Transactions(1).TransactionLines.Lines.Count = 3)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_CallsInitialiseSortedRefundRepository()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToLogWhetherItHasBeenCalled

        testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testRefundRep.InitialiseSortedRefundRepositoryWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_CallsInitialiseRefundTransactionsList()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseRefundTransactionsListToLogWhetherItHasBeenCalled

        testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testRefundRep.InitialiseRefundTransactionsListWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_CallsSortedRefundRepositoryHasData()
        Dim testRefundRep As New TestRefundRepositoryOverridesSortedRefundRepositoryHasDataToLogWhetherItHasBeenCalled

        testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testRefundRep.SortedRefundRepositoryHasDataWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_WithoutSortedRefundRepository_DoesNotCallProcessSortedAndFilteredRefundRepository()
        Dim testRefundRep As New TestRefundRepositoryOverridesProcessSortedAndFilteredRefundRepositoryToLogWhetherItHasBeenCalledAndOverridesSortedRefundRepositoryHasDataToReturnFalse

        testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsFalse(testRefundRep.ProcessSortedAndFilteredRefundRepositoryWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_WithSortedRefundRepository_CallsProcessSortedAndFilteredRefundRepository()
        Dim testRefundRep As New TestRefundRepositoryOverridesProcessSortedAndFilteredRefundRepositoryToLogWhetherItHasBeenCalledAndOverridesSortedAndFilteredRefundRepositoryHasDataToReturnTrueAndOverridesSortedRefundRepositoryHasDataToReturnTrue

        testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testRefundRep.ProcessSortedAndFilteredRefundRepositoryWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactions_CallsInitialiseFilteredRefundRepositoryFromSortedRefundRepository()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToLogWhetherItHasBeenCalled()

        testRefundRep.GetRefundTransactions(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testRefundRep.InitialiseFilteredRefundRepositoryFromSortedRefundRepositoryWasCalled)
    End Sub
#End Region

#Region "InitialiseSortedRefundRepository Tests"

    <TestMethod()> _
    Public Sub InitialiseSortedRefundRepository_CallsGetSortedRefundTransactionLines()
        Dim testRefundRep As New TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToLogWhetherItHasBeenCalled()

        testRefundRep.InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testRefundRep.GetSortedRefundTransactionLinesWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub InitialiseSortedRefundRepository_CallsGetSortedRefundTransactionLinesPassingOnStartDate()
        Dim testRefundRep As New TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToLogWhetherItHasBeenCalled()
        Dim expected As DateTime = #4/1/2012#

        testRefundRep.InitialiseSortedRefundRepository(expected, #4/2/2012#)

        Assert.AreEqual(expected, testRefundRep.StartDate)
    End Sub

    <TestMethod()> _
    Public Sub InitialiseSortedRefundRepository_CallsGetSortedRefundTransactionLinesPassingOnEndDate()
        Dim testRefundRep As New TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToLogWhetherItHasBeenCalled()
        Dim expected As DateTime = #4/1/2012#

        testRefundRep.InitialiseSortedRefundRepository(#3/31/2012#, expected)

        Assert.AreEqual(expected, testRefundRep.EndDate)
    End Sub
#End Region

#Region "GetFilteredRefundRepositoryFromSortedRefundRepository Tests"

    <TestMethod()> _
    Public Sub GetFilteredRefundRepositoryFromSortedRefundRepository_WhenSortedRefundRepositoryHasData_CallsRowsIsNoProofOfPurchaseRefund()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowIsNoProofOfPurchaseRefundtoFlagWhetherItWasCalledAndReturnTrue

        With testRefundRep
            .InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
            .GetFilteredRefundRepositoryFromSortedRefundRepository()
        End With

        Assert.IsTrue(testRefundRep.RowIsNoProofOfPurchaseRefundWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub GetFilteredRefundRepositoryFromSortedRefundRepository_WhenSortedRefundRepositoryHas1DataRowAndRowsIsNoProofOfPurchaseRefundReturnsTrue_ReturnsDatatableWithARow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowIsNoProofOfPurchaseRefundtoFlagWhetherItWasCalledAndReturnTrueAndExposeTheInitialisedRow
        Dim actual As DataTable = Nothing

        With testRefundRep
            .InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
            actual = .GetFilteredRefundRepositoryFromSortedRefundRepository()
        End With

        Assert.IsTrue(actual IsNot Nothing AndAlso actual.Rows.Count = 1)
    End Sub

    <TestMethod()> _
    Public Sub GetFilteredRefundRepositoryFromSortedRefundRepository_WhenSortedRefundRepositoryHas1DataRowAndRowsIsNoProofOfPurchaseRefundReturnsTrue_ReturnsDatatableWithTheInitialisedRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowIsNoProofOfPurchaseRefundtoFlagWhetherItWasCalledAndReturnTrueAndExposeTheInitialisedRow
        Dim actual As DataTable = Nothing

        With testRefundRep
            .InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
            actual = .GetFilteredRefundRepositoryFromSortedRefundRepository()
        End With

        If actual IsNot Nothing AndAlso actual.Rows.Count = 1 Then
            For Each ColumnName As String In testRefundRep.GetColumnNames
                Assert.AreEqual(testRefundRep.InitialisedRow.Item(ColumnName), actual.Rows(0).Item(ColumnName), "GetFilteredRefundRepositoryFromSortedRefundRepository returns datatable with incorrectly assigned data in '" & ColumnName & "' column.")
            Next
        Else
            Assert.Inconclusive("Cannot test the returned row as no row is being returned.")
        End If
    End Sub

    <TestMethod()> _
    Public Sub GetFilteredRefundRepositoryFromSortedRefundRepository_WhenSortedRefundRepositoryHas1DataRowAndRowsIsNoProofOfPurchaseRefundReturnsFalse_CallsRefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens()
        Dim testRefundRep As New TestRefundRepositoryOverridesRefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensToFlagWhetherItWasCalledAndOverridesRowIsNoProofOfPurchaseRefundToReturnFalse

        With testRefundRep
            .InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
            .GetFilteredRefundRepositoryFromSortedRefundRepository()
        End With

        Assert.IsTrue(testRefundRep.RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub GetFilteredRefundRepositoryFromSortedRefundRepository_WhenSortedRefundRepositoryHas1DataRowAndRowsIsNoProofOfPurchaseRefundReturnsFalseAndRefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensReturnsTrue_ReturnsDatatableWithARow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensToFlagWhetherItWasCalledAndReturnFalseAndOverridesRowIsNoProofOfPurchaseRefundToReturnTrueAndExposeTheInitialisedRow
        Dim actual As DataTable = Nothing

        With testRefundRep
            .InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
            actual = .GetFilteredRefundRepositoryFromSortedRefundRepository()
        End With

        Assert.IsTrue(actual IsNot Nothing AndAlso actual.Rows.Count = 1)
    End Sub

    <TestMethod()> _
    Public Sub GetFilteredRefundRepositoryFromSortedRefundRepository_WhenSortedRefundRepositoryHas1DataRowAndRowsIsNoProofOfPurchaseRefundReturnsFalse_ReturnsDatatableWithTheInitialisedRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensToFlagWhetherItWasCalledAndReturnFalseAndOverridesRowIsNoProofOfPurchaseRefundToReturnTrueAndExposeTheInitialisedRow
        Dim actual As DataTable = Nothing

        With testRefundRep
            .InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
            actual = .GetFilteredRefundRepositoryFromSortedRefundRepository()
        End With

        If actual IsNot Nothing AndAlso actual.Rows.Count = 1 Then
            For Each ColumnName As String In testRefundRep.GetColumnNames
                Assert.AreEqual(testRefundRep.InitialisedRow.Item(ColumnName), actual.Rows(0).Item(ColumnName), "GetFilteredRefundRepositoryFromSortedRefundRepository returns datatable with incorrectly assigned data in '" & ColumnName & "' column.")
            Next
        Else
            Assert.Inconclusive("Cannot test the returned row as no row is being returned.")
        End If
    End Sub
#End Region

#Region "RowIsNoProofOfPurchaseRefund Tests"

    <TestMethod()> _
    Public Sub RowIsNoProofOfPurchaseRefund_NothingPassedAsRefundRow_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository

        Assert.IsFalse(testRefundRep.RowIsNoProofOfPurchaseRefund(Nothing))
    End Sub

    <TestMethod()> _
    Public Sub RowIsNoProofOfPurchaseRefund_EmptyDataRowPassedAsRefundRow_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim CreateFrom As DataTable = testRefundRep.GetInitialisedDataTable
        Dim TestRefundRow As DataRow = CreateFrom.NewRow

        Assert.IsFalse(testRefundRep.RowIsNoProofOfPurchaseRefund(TestRefundRow))
    End Sub

    <TestMethod()> _
    Public Sub RowIsNoProofOfPurchaseRefund_PassedRefundRowHasNoOriginalTranNumberField_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim CreateFrom As DataTable = testRefundRep.GetInitialisedDataTable
        Dim TestRefundRow As DataRow = Nothing

        CreateFrom.Columns.Remove(testRefundRep._fieldName_OriginalTranNumber)
        TestRefundRow = CreateFrom.NewRow
        With TestRefundRow
            .Item(testRefundRep._fieldName_TranDate) = #4/2/2012#
            .Item(testRefundRep._fieldName_TranTill) = "02"
            .Item(testRefundRep._fieldName_TranNumber) = "0002"
            .Item(testRefundRep._fieldName_TranLineNumber) = 1
            .Item(testRefundRep._fieldName_SkuNumber) = "123456"
            .Item(testRefundRep._fieldName_SkuPrice) = 14.44
            .Item(testRefundRep._fieldName_SkuQuantity) = -1
            .Item(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
        Assert.IsFalse(testRefundRep.RowIsNoProofOfPurchaseRefund(TestRefundRow))
    End Sub

    <TestMethod()> _
    Public Sub RowIsNoProofOfPurchaseRefund_PassedRefundRowHasOriginalTranNumberEqualTo0000_ReturnsTrue()
        Dim testRefundRep As New TestRefundRepository
        Dim CreateFrom As DataTable = testRefundRep.GetInitialisedDataTable
        Dim TestRefundRow As DataRow = CreateFrom.NewRow

        With TestRefundRow
            .Item(testRefundRep._fieldName_TranDate) = #4/2/2012#
            .Item(testRefundRep._fieldName_TranTill) = "02"
            .Item(testRefundRep._fieldName_TranNumber) = "0002"
            .Item(testRefundRep._fieldName_TranLineNumber) = 1
            .Item(testRefundRep._fieldName_SkuNumber) = "123456"
            .Item(testRefundRep._fieldName_SkuPrice) = 14.44
            .Item(testRefundRep._fieldName_SkuQuantity) = -1
            .Item(testRefundRep._fieldName_OriginalTranNumber) = "0000"
            .Item(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
        Assert.IsTrue(testRefundRep.RowIsNoProofOfPurchaseRefund(TestRefundRow))
    End Sub

    <TestMethod()> _
    Public Sub RowIsNoProofOfPurchaseRefund_PassedRefundRowHasOriginalTranNumberNotEqualTo0000_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim CreateFrom As DataTable = testRefundRep.GetInitialisedDataTable
        Dim TestRefundRow As DataRow = CreateFrom.NewRow

        With TestRefundRow
            .Item(testRefundRep._fieldName_TranDate) = #4/2/2012#
            .Item(testRefundRep._fieldName_TranTill) = "02"
            .Item(testRefundRep._fieldName_TranNumber) = "0002"
            .Item(testRefundRep._fieldName_TranLineNumber) = 1
            .Item(testRefundRep._fieldName_SkuNumber) = "123456"
            .Item(testRefundRep._fieldName_SkuPrice) = 14.44
            .Item(testRefundRep._fieldName_SkuQuantity) = -1
            .Item(testRefundRep._fieldName_OriginalTranNumber) = "0000"
            .Item(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
        For OrigTranNumber = 1 To 9999
            TestRefundRow.Item(testRefundRep._fieldName_OriginalTranNumber) = OrigTranNumber.ToString("0000")
            Assert.IsFalse(testRefundRep.RowIsNoProofOfPurchaseRefund(TestRefundRow))
        Next
    End Sub
#End Region

#Region "RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens Tests"

    <TestMethod()> _
    Public Sub RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens_NothingPassedAsRefundRow_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository

        Assert.IsFalse(testRefundRep.RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens(Nothing))
    End Sub

    <TestMethod()> _
    Public Sub RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens_EmptyDataRowPassedAsRefundRow_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim CreateFrom As DataTable = testRefundRep.GetInitialisedDataTable
        Dim TestRefundRow As DataRow = CreateFrom.NewRow

        Assert.IsFalse(testRefundRep.RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens(TestRefundRow))
    End Sub

    <TestMethod()> _
    Public Sub RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens_PassedRefundRowHasNoNumberCashAndGiftTokenRefundPaymentsField_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim CreateFrom As DataTable = testRefundRep.GetInitialisedDataTable
        Dim TestRefundRow As DataRow = Nothing

        CreateFrom.Columns.Remove(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments)
        TestRefundRow = CreateFrom.NewRow
        With TestRefundRow
            .Item(testRefundRep._fieldName_TranDate) = #4/2/2012#
            .Item(testRefundRep._fieldName_TranTill) = "02"
            .Item(testRefundRep._fieldName_TranNumber) = "0002"
            .Item(testRefundRep._fieldName_TranLineNumber) = 1
            .Item(testRefundRep._fieldName_SkuNumber) = "123456"
            .Item(testRefundRep._fieldName_SkuPrice) = 14.44
            .Item(testRefundRep._fieldName_SkuQuantity) = -1
            .Item(testRefundRep._fieldName_OriginalTranNumber) = "0000"
        End With
        Assert.IsFalse(testRefundRep.RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens(TestRefundRow))
    End Sub

    <TestMethod()> _
    Public Sub RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens_PassedRefundRowHasNumberCashAndGiftTokenRefundPaymentsLessThanOrEqualTo0_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim CreateFrom As DataTable = testRefundRep.GetInitialisedDataTable
        Dim TestRefundRow As DataRow = CreateFrom.NewRow

        With TestRefundRow
            .Item(testRefundRep._fieldName_TranDate) = #4/2/2012#
            .Item(testRefundRep._fieldName_TranTill) = "02"
            .Item(testRefundRep._fieldName_TranNumber) = "0002"
            .Item(testRefundRep._fieldName_TranLineNumber) = 1
            .Item(testRefundRep._fieldName_SkuNumber) = "123456"
            .Item(testRefundRep._fieldName_SkuPrice) = 14.44
            .Item(testRefundRep._fieldName_SkuQuantity) = -1
            .Item(testRefundRep._fieldName_OriginalTranNumber) = "0000"
            .Item(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
        For NumberCashAndGiftTokenRefundPayments = 0 To -9 Step -1
            TestRefundRow.Item(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = NumberCashAndGiftTokenRefundPayments
            Assert.IsFalse(testRefundRep.RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens(TestRefundRow))
        Next
    End Sub

    <TestMethod()> _
    Public Sub RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens_PassedRefundRowHasNumberCashAndGiftTokenRefundPaymentsGreaterThan0_ReturnsTrue()
        Dim testRefundRep As New TestRefundRepository
        Dim CreateFrom As DataTable = testRefundRep.GetInitialisedDataTable
        Dim TestRefundRow As DataRow = CreateFrom.NewRow

        With TestRefundRow
            .Item(testRefundRep._fieldName_TranDate) = #4/2/2012#
            .Item(testRefundRep._fieldName_TranTill) = "02"
            .Item(testRefundRep._fieldName_TranNumber) = "0002"
            .Item(testRefundRep._fieldName_TranLineNumber) = 1
            .Item(testRefundRep._fieldName_SkuNumber) = "123456"
            .Item(testRefundRep._fieldName_SkuPrice) = 14.44
            .Item(testRefundRep._fieldName_SkuQuantity) = -1
            .Item(testRefundRep._fieldName_OriginalTranNumber) = "0000"
            .Item(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
        For NumberCashAndGiftTokenRefundPayments = 1 To 10
            TestRefundRow.Item(testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = NumberCashAndGiftTokenRefundPayments
            Assert.IsTrue(testRefundRep.RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens(TestRefundRow))
        Next
    End Sub
#End Region

#Region "InitialiseRefundTransactionsList Tests"

    <TestMethod()> _
    Public Sub InitialiseRefundTransactionsList_Initialises_refundTransactionsToSomething()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewToCallMyBaseNewAndThenReset_refundTransactions_BackToNothing()

        testRefundRep.InitialiseRefundTransactionsList()

        Assert.IsTrue(testRefundRep.RefundTransactions IsNot Nothing)
    End Sub

    <TestMethod()> _
    Public Sub InitialiseRefundTransactionsList_Initialises_refundTransactionsToNewListOfRefundTransactions()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewToCallMyBaseNewAndThenReset_refundTransactions_BackToNothing()

        testRefundRep.InitialiseRefundTransactionsList()

        Assert.IsTrue(testRefundRep.RefundTransactions.Count = 0)
    End Sub
#End Region

#Region "SortedRefundRepositoryHasData Tests"

    <TestMethod()> _
    Public Sub SortedRefundRepositoryHasData_SortedRefundRepositoryIsNothing_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToNothing

        testRefundRep.InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
        Assert.IsFalse(testRefundRep.SortedRefundRepositoryHasData())
    End Sub

    <TestMethod()> _
    Public Sub SortedRefundRepositoryHasData_SortedRefundRepositoryHasCountOfZero_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToEmptyDataTable

        testRefundRep.InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
        Assert.IsFalse(testRefundRep.SortedRefundRepositoryHasData())
    End Sub

    <TestMethod()> _
    Public Sub SortedRefundRepositoryHasData_SortedRefundRepositoryHasCountOfOne_ReturnsTrue()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToDataTableWith1Row

        testRefundRep.InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
        Assert.IsTrue(testRefundRep.SortedRefundRepositoryHasData())
    End Sub
#End Region

#Region "SortedAndFilteredRefundRepositoryHasData Tests"

    <TestMethod()> _
    Public Sub SortedAndFilteredRefundRepositoryHasData_SortedAndFilteredRefundRepositoryIsNothing_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedAndFilteredDataToNothing

        testRefundRep.InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
        Assert.IsFalse(testRefundRep.SortedAndFilteredRefundRepositoryHasData())
    End Sub

    <TestMethod()> _
    Public Sub SortedAndFilteredRefundRepositoryHasData_SortedAndFilteredRefundRepositoryHasCountOfZero_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToEmptyDataTable

        testRefundRep.InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
        Assert.IsFalse(testRefundRep.SortedAndFilteredRefundRepositoryHasData())
    End Sub

    <TestMethod()> _
    Public Sub SortedAndFilteredRefundRepositoryHasData_SortedAndFilteredRefundRepositoryHasCountOfOne_ReturnsTrue()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1Row

        testRefundRep.InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
        Assert.IsTrue(testRefundRep.SortedAndFilteredRefundRepositoryHasData())
    End Sub
#End Region

#Region "ProcessSortedAndFilteredRefundRepository Tests"

    <TestMethod()> _
    Public Sub ProcessSortedAndFilteredRefundRepository_CallsInitialiseRefundTranAndRefundLine()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseRefundTranToLogWhetherItHasBeenCalledUsingEmptySortedRefundRepository

        testRefundRep.ProcessSortedAndFilteredRefundRepository()

        Assert.IsTrue(testRefundRep.InitialiseRefundTranWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub ProcessSortedAndFilteredRefundRepository_CallsProcessSortedAndFilteredRefundRepositoryRows()
        Dim testRefundRep As New TestRefundRepositoryOverridesProcessSortedAndFilteredRefundRepositoryToLogWhetherItHasBeenCalled

        testRefundRep.ProcessSortedAndFilteredRefundRepository()

        Assert.IsTrue(testRefundRep.GetProcessSortedAndFilteredRefundRepositoryRowsWasCalled())
    End Sub
#End Region

#Region "ProcessSortedAndFilteredRefundRepositoryRows"

    <TestMethod()> _
    Public Sub ProcessSortedAndFilteredRefundRepositoryRows_SortedRefundRepositoryHas1RowAndRefundTransactionBeingProcessedIsNotNothing_DoesNotCallSetRefundTransactionBeingProcessedToNewRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnFalseAndOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalled

        With testRefundRep
            .InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
            .ProcessSortedAndFilteredRefundRepositoryRows()
        End With
        Assert.IsFalse(testRefundRep.SetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled())
    End Sub

    <TestMethod()> _
    Public Sub ProcessSortedAndFilteredRefundRepositoryRows_SortedRefundRepositoryHas1RowAndRefundTransactionBeingProcessedIsNothing_CallsSetRefundTransactionBeingProcessedToNewRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnTrueAndOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalled

        With testRefundRep
            .InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
            .ProcessSortedAndFilteredRefundRepositoryRows()
        End With

        Assert.IsTrue(testRefundRep.SetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled())
    End Sub

    <TestMethod()> _
    Public Sub ProcessSortedAndFilteredRefundRepositoryRows_SortedRefundRepositoryHas1RowAndRefundTransactionBeingProcessedIsNotNothing_CallsProcessRefundRepositoryRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnFalseAndOverridesProcessRefundRepositoryRowToLogWhetherItHasBeenCalled

        With testRefundRep
            .InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
            .ProcessSortedAndFilteredRefundRepositoryRows()
        End With

        Assert.IsTrue(testRefundRep.ProcessRefundRepositoryRowWasCalled())
    End Sub

    <TestMethod()> _
    Public Sub ProcessSortedAndFilteredRefundRepositoryRows_SortedRefundRepositoryHas1RowAndRefundTransactionBeingProcessedIsNothing_DoesNotCallProcessRefundRepositoryRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnTrueAndOverridesProcessRefundRepositoryRowToLogWhetherItHasBeenCalled

        With testRefundRep
            .InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
            .ProcessSortedAndFilteredRefundRepositoryRows()
        End With

        Assert.IsFalse(testRefundRep.ProcessRefundRepositoryRowWasCalled())
    End Sub

    <TestMethod()> _
    Public Sub ProcessSortedAndFilteredRefundRepositoryRows_SortedRefundRepositoryIsEmptyAndRefundTransactionBeingProcessedIsNothing_DoesNotCallAddRefundTransactionBeingProcessedToRefundTransactionsList()
        Dim testRefundRep As New TestRefundRepositoryUsingEmptySortedRefundRepositoryOverridesRefundTransactionBeingProcessedIsNothingToReturnTrueAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListToLogWhetherItHasBeenCalled

        With testRefundRep
            .InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
            .ProcessSortedAndFilteredRefundRepositoryRows()
        End With

        Assert.IsFalse(testRefundRep.AddRefundTransactionBeingProcessedToRefundTransactionsListWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub ProcessSortedAndFilteredRefundRepositoryRows_SortedRefundRepositoryIsEmptyAndRefundTransactionBeingProcessedIsNotNothing_CallsAddRefundTransactionBeingProcessedToRefundTransactionsList()
        Dim testRefundRep As New TestRefundRepositoryUsingEmptySortedRefundRepositoryOverridesRefundTransactionBeingProcessedIsNothingToReturnFalseAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListToLogWhetherItHasBeenCalled

        With testRefundRep
            .InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
            .ProcessSortedAndFilteredRefundRepositoryRows()
        End With

        Assert.IsTrue(testRefundRep.AddRefundTransactionBeingProcessedToRefundTransactionsListWasCalled)
    End Sub
#End Region

#Region "RefundTransactionBeingProcessedIsNothing Tests"

    <TestMethod()> _
    Public Sub RefundTransactionBeingProcessedIsNothing_RefundTransactionBeingProcessedIsNothing_ReturnsTrue()
        Dim testRefundRep As New TestRefundRepositoryUsingRefundTransactionBeingProcessedSetToNothing

        Assert.IsTrue(testRefundRep.RefundTransactionBeingProcessedIsNothing)
    End Sub

    <TestMethod()> _
    Public Sub RefundTransactionBeingProcessedIsNothing_RefundTransactionBeingProcessedIsSomething_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryUsingRefundTransactionBeingProcessedSetToNewRefundTransaction

        Assert.IsFalse(testRefundRep.RefundTransactionBeingProcessedIsNothing)
    End Sub
#End Region

#Region "ProcessRefundRepositoryRow Tests"

    <TestMethod()> _
    Public Sub ProcessRefundRepositoryRow_RowSameAsTransactionIsTrue_CallsAddRefundLineToRefundTransactionBeingProcessedsLines()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherItHasBeenCalledAndProvidesTestDataRows

        With testRefundRep
            .ProcessRefundRepositoryRow(.TestRefundRow)
            Assert.IsTrue(.AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ProcessRefundRepositoryRow_RowSameAsTransactionIsFalse_CallsAddRefundLineToRefundTransactionBeingProcessedsLinesPassingInRefundRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowSameAsTransactionToReturnFalseAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherItHasBeenCalledAndProvidesTestDataRows

        With testRefundRep
            .ProcessRefundRepositoryRow(.TestRefundRow)
            Assert.IsTrue(.AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ProcessRefundRepositoryRow_RowSameAsTransactionIsTrue_CallsAddRefundLineToRefundTransactionBeingProcessedsLinesPassingInRefundRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherRefundRowIsPassedIn
        Dim expected As DataRow
        Dim actual As DataRow

        With testRefundRep
            expected = .TestRefundRow
            .ProcessRefundRepositoryRow(.TestRefundRow)
            actual = .AddRefundLineToRefundTransactionBeingProcessedsLinesPassedInRefundRow
            Assert.AreEqual(expected, actual)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ProcessRefundRepositoryRow_RowSameAsTransactionIsTrue_DoesNotCallAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalledAndProvidesTestDataRows

        With testRefundRep
            .ProcessRefundRepositoryRow(.TestRefundRow)
            Assert.IsFalse(.AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ProcessRefundRepositoryRow_RowSameAsTransactionIsFalse_CallsAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowSameAsTransactionToReturnFalseAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalledAndProvidesTestDataRows

        With testRefundRep
            .ProcessRefundRepositoryRow(.TestRefundRow)
            Assert.IsTrue(.AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ProcessRefundRepositoryRow_RowSameAsTransactionIsFalse_CallsAddRefundLineToRefundTransactionBeingProcessedsLinesAndSetRefundTransactionBeingProcessedToNewRefundTransactionPassingInRefundRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRowSameAsTransactionToReturnFalseAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherRefundRowIsPassedIn
        Dim expected As DataRow
        Dim actual As DataRow

        With testRefundRep
            expected = .TestRefundRow
            .ProcessRefundRepositoryRow(.TestRefundRow)
            actual = .AddRefundLineToRefundTransactionBeingProcessedsLinesAndSetRefundTransactionBeingProcessedToNewRefundTransactionPassedInRefundRow
            Assert.AreEqual(expected, actual)
        End With
    End Sub
#End Region

#Region "AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction Tests"

    <TestMethod()> _
    Public Sub AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction_CallsAddRefundTransactionBeingProcessedToRefundTransactionsList()
        Dim testRefundRep As New TestRefundRepositoryOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListToLogWhetherItWasCalledAndProvidesTestDataRow

        With testRefundRep
            .AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction(.TestRefundRow)
            Assert.IsTrue(.AddRefundTransactionBeingProcessedToRefundTransactionsListWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction_CallsSetRefundTransactionBeingProcessedToNewRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItWasCalledAndExposeTheRefundRowParameterUsedAndProvidesTestDataRow

        With testRefundRep
            .AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction(.TestRefundRow)
            Assert.IsTrue(.SetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction_CallsSetRefundTransactionBeingProcessedToNewRefundTransactionPassingInRefundRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItWasCalledAndExposeTheRefundRowParameterUsedAndProvidesTestDataRow
        Dim expected As DataRow
        Dim actual As DataRow

        With testRefundRep
            expected = .TestRefundRow
            .AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction(.TestRefundRow)
            actual = .SetRefundTransactionBeingProcessedToNewRefundTransactionRefundRowParameter
            Assert.AreEqual(expected, actual)
        End With
    End Sub
#End Region

#Region "SetRefundTransactionBeingProcessedToNewRefundTransaction Tests"

    <TestMethod()> _
    Public Sub SetRefundTransactionBeingProcessedToNewRefundTransaction_CallsNewRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewRefundTransactionToLogWhetehrItWasCalledAndExposeTheRefundRowParameterUsedAndTheNewRefundTransactionAndProvideTestDataRow

        With testRefundRep
            .SetRefundTransactionBeingProcessedToNewRefundTransaction(.TestRefundRow)
            Assert.IsTrue(.NewRefundTransactionWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub SetRefundTransactionBeingProcessedToNewRefundTransaction_CallsNewRefundTransactionPassingInRefundRowParameter()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewRefundTransactionToLogWhetehrItWasCalledAndExposeTheRefundRowParameterUsedAndTheNewRefundTransactionAndProvideTestDataRow
        Dim expected As DataRow
        Dim actual As DataRow

        With testRefundRep
            expected = .TestRefundRow
            .SetRefundTransactionBeingProcessedToNewRefundTransaction(.TestRefundRow)
            actual = .NewRefundTransactionRefundRowParameter
            Assert.AreEqual(expected, actual)
        End With
    End Sub

    <TestMethod()> _
    Public Sub SetRefundTransactionBeingProcessedToNewRefundTransaction_AssignsReturnValueOfNewRefundTransactionCallToRefundTransactionBeingProcessed()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewRefundTransactionToLogWhetehrItWasCalledAndExposeTheRefundRowParameterUsedAndTheNewRefundTransactionAndProvideTestDataRow
        Dim expected As RefundTransaction
        Dim actual As RefundTransaction

        With testRefundRep
            .SetRefundTransactionBeingProcessedToNewRefundTransaction(.TestRefundRow)
            expected = .CreatedRefundTransaction
            actual = ._refundTransactionBeingProcessed
            Assert.AreEqual(expected, actual)
        End With
    End Sub
#End Region

#Region "AddRefundTransactionBeingProcessedToRefundTransactionsList Tests"

    <TestMethod()> _
    Public Sub AddRefundTransactionBeingProcessedToRefundTransactionsList_RefundTransactionBeingProcessedIsNothing_DoesNotAddRefundTransactionBeingProcessedToRefundTransactionList()
        Dim testRefundRep As New TestRefundRepository
        Dim expected As RefundTransaction = Nothing

        With testRefundRep
            ._refundTransactions = New List(Of RefundTransaction)
            ._refundTransactionBeingProcessed = expected
            .AddRefundTransactionBeingProcessedToRefundTransactionsList()
            Assert.IsTrue(._refundTransactions.Count = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub AddRefundTransactionBeingProcessedToRefundTransactionsList_RefundTransactionBeingProcessedIsSomething_AddsRefundTransactionBeingProcessedToRefundTransactionList()
        Dim testRefundRep As New TestRefundRepository
        Dim expected As RefundTransaction

        expected = New RefundTransaction
        With expected
            .TransactionDate = #4/1/2012#
            .Till = "03"
            .TransactionNumber = "0001"
        End With
        With testRefundRep
            ._refundTransactions = New List(Of RefundTransaction)
            ._refundTransactionBeingProcessed = expected
            .AddRefundTransactionBeingProcessedToRefundTransactionsList()
            Assert.AreEqual(expected, ._refundTransactions(0))
        End With
    End Sub
#End Region

#Region "AddRefundLineToRefundTransactionBeingProcessedsLines Tests"

    <TestMethod()> _
    Public Sub AddRefundLineToRefundTransactionBeingProcessedsLines_CallsNewRefundLine()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewRefundLineToLogWhetherItHasBeenCalledAndExposeTheRefundRwoParameterAndExposeTheCreatedRefundLineAndProvideTestRefundRow

        With testRefundRep
            .AddRefundLineToRefundTransactionBeingProcessedsLines(.TestRefundRow)
            Assert.IsTrue(.NewRefundLineWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub AddRefundLineToRefundTransactionBeingProcessedsLines_CallsNewRefundLinePassingInRefundRowParameter()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewRefundLineToLogWhetherItHasBeenCalledAndExposeTheRefundRwoParameterAndExposeTheCreatedRefundLineAndProvideTestRefundRow
        Dim expected As DataRow
        Dim actual As DataRow

        With testRefundRep
            expected = .TestRefundRow
            .AddRefundLineToRefundTransactionBeingProcessedsLines(.TestRefundRow)
            actual = .NewRefundLineRefundRowParameter
            Assert.AreEqual(expected, actual)
        End With
    End Sub

    <TestMethod()> _
    Public Sub AddRefundLineToRefundTransactionBeingProcessedsLines_ReturnValueOfNewRefundLineIsNothing_DoesNotAddRefundLineToRefundTransactionBeingProcessedsLines()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewRefundLineToReturnNothingAndProvideTestRefundRow

        With testRefundRep
            .AddRefundLineToRefundTransactionBeingProcessedsLines(.TestRefundRow)
            Assert.IsTrue(._refundTransactionBeingProcessed.TransactionLines.Lines.Count = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub AddRefundLineToRefundTransactionBeingProcessedsLines_ReturnValueOfNewRefundLineIsSomething_AddsNewRefundLineRefundLineToRefundTransactionBeingProcessedsLines()
        Dim testRefundRep As New TestRefundRepositoryOverridesNewRefundLineToReturnTestRefundLine
        Dim expected As RefundLine

        With testRefundRep
            expected = .TestRefundLine
            .AddRefundLineToRefundTransactionBeingProcessedsLines(Nothing)
            Assert.AreEqual(expected, ._refundTransactionBeingProcessed.TransactionLines.Lines(0))
        End With
    End Sub
#End Region

#Region "GetSortColumn Tests"

    <TestMethod()> _
    Public Sub GetSortColumn_SortColumnIndex0_ReturnsEmptyString()
        Dim testRefundRep As New TestRefundRepository

        Assert.IsTrue(testRefundRep.GetSortColumn(0) = "")
    End Sub

    <TestMethod()> _
    Public Sub GetSortColumn_SortColumnIndex1_ReturnsTransactionDateFieldName()
        Dim testRefundRep As New TestRefundRepository

        With testRefundRep
            Assert.IsTrue(String.Compare(.GetSortColumn(1), ._fieldName_TranDate) = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetSortColumn_SortColumnIndex2_ReturnsTransactionTillFieldName()
        Dim testRefundRep As New TestRefundRepository

        With testRefundRep
            Assert.IsTrue(String.Compare(.GetSortColumn(2), ._fieldName_TranTill) = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetSortColumn_SortColumnIndex3_ReturnsTransactionNumberFieldName()
        Dim testRefundRep As New TestRefundRepository

        With testRefundRep
            Assert.IsTrue(String.Compare(.GetSortColumn(3), ._fieldName_TranNumber) = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetSortColumn_SortColumnIndex4_ReturnsEmptyString()
        Dim testRefundRep As New TestRefundRepository

        Assert.IsTrue(testRefundRep.GetSortColumn(4) = "")
    End Sub
#End Region

#Region "GetInitialisedDataTable Tests"

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsSomething()
        Dim testRefundRep As New TestRefundRepository

        Assert.IsTrue(testRefundRep.GetInitialisedDataTable IsNot Nothing)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith9Columns()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns.Count = 9)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith1stColumnNamedAfterTransactionDateField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(0).ColumnName, testRefundRep._fieldName_TranDate) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith1stColumnOfTypeDateTime()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(0).DataType Is GetType(DateTime))
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith2ndColumnNamedAfterTransactionTillField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(1).ColumnName, testRefundRep._fieldName_TranTill) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith2ndColumnOfTypeString()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(1).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith3rdColumnNamedAfterTransactionNumberField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(2).ColumnName, testRefundRep._fieldName_TranNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith3rdColumnOfTypeString()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(2).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith4thColumnNamedAfterTransactionLineNumber()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(3).ColumnName, testRefundRep._fieldName_TranLineNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith4thColumnOfTypeInt16()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(3).DataType Is GetType(Int16))
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith5thColumnNamedAfterSkuNumberField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(4).ColumnName, testRefundRep._fieldName_SkuNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith5thColumnOfTypeString()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(4).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith6thColumnNamedAfterSkuPriceField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(5).ColumnName, testRefundRep._fieldName_SkuPrice) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith6thColumnOfTypeDecimal()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(5).DataType Is GetType(Decimal))
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith7thColumnNamedAfterSkuQuantityField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(6).ColumnName, testRefundRep._fieldName_SkuQuantity) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith7thColumnOfTypeDecimal()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(6).DataType Is GetType(Decimal))
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith8thColumnNamedAfterOriginalTransactionNumber()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(7).ColumnName, testRefundRep._fieldName_OriginalTranNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith8thColumnOfTypeString()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(7).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith9thColumnNamedAfterNumberCashAndGiftTokenRefundPayments()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(String.Compare(testDataTable.Columns(8).ColumnName, testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetInitialisedDataTable_ReturnsDataTableWith9thColumnOfTypeInteger()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetInitialisedDataTable

        Assert.IsTrue(testDataTable.Columns(8).DataType Is GetType(Integer))
    End Sub
#End Region

#Region "GetRefundTransactionLines Tests"

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsSomething()
        Dim testRefundRep As New TestRefundRepository

        Assert.IsTrue(testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#) IsNot Nothing)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTable()
        Dim testRefundRep As New TestRefundRepository

        Assert.AreEqual("System.Data.DataTable", testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#).GetType.FullName)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsCorrectlyNamedDataTable()
        Dim testRefundRep As New TestRefundRepository
        Dim testData As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.AreEqual(testRefundRep.TableName, testData.TableName)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith9Columns()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns.Count = 9)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWithCorrectColumns()
        Dim testRefundRep As New RefundRepositoryProvidesColumnNamesArray
        Dim testData As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        For Each ColumnName As String In testRefundRep.GetColumnNames
            Assert.IsTrue(testData.Columns.Contains(ColumnName), "GetRefundTransactionLines returns datatable without '" & ColumnName & "' column.")
        Next
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith1stColumnNamedAfterTransactionDateField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(0).ColumnName, testRefundRep._fieldName_TranDate) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith1stColumnOfTypeDateTime()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(0).DataType Is GetType(DateTime))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith2ndColumnNamedAfterTransactionTillField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(1).ColumnName, testRefundRep._fieldName_TranTill) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith2ndColumnOfTypeString()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(1).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith3rdColumnNamedAfterTransactionNumberField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(2).ColumnName, testRefundRep._fieldName_TranNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith3rdColumnOfTypeString()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(2).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith4thColumnNamedAfterTransactionLineNumber()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(3).ColumnName, testRefundRep._fieldName_TranLineNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith4thColumnOfTypeInt16()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(3).DataType Is GetType(Int16))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith5thColumnNamedAfterSkuNumberField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(4).ColumnName, testRefundRep._fieldName_SkuNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith5thColumnOfTypeString()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(4).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith6thColumnNamedAfterSkuPriceField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(5).ColumnName, testRefundRep._fieldName_SkuPrice) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith6thColumnOfTypeDecimal()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(5).DataType Is GetType(Decimal))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith7thColumnNamedAfterSkuQuantityField()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(6).ColumnName, testRefundRep._fieldName_SkuQuantity) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith7thColumnOfTypeDecimal()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(6).DataType Is GetType(Decimal))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith8thColumnNamedAfterOriginalTransactionNumber()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(7).ColumnName, testRefundRep._fieldName_OriginalTranNumber) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith8thColumnOfTypeString()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(7).DataType Is GetType(String))
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith9thColumnNamedAfterNumberCashAndGiftTokenRefundPayments()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(String.Compare(testDataTable.Columns(8).ColumnName, testRefundRep._fieldName_NumberCashAndGiftTokenRefundPayments) = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundTransactionLines_ReturnsDataTableWith8thColumnOfTypeInteger()
        Dim testRefundRep As New TestRefundRepository
        Dim testDataTable As DataTable = testRefundRep.GetRefundTransactionLines(#4/1/2012#, #4/1/2012#)

        Assert.IsTrue(testDataTable.Columns(8).DataType Is GetType(Integer))
    End Sub
#End Region

#Region "GetSortedRefundTransactionLines Tests"

    <TestMethod()> _
    Public Sub GetSortedRefundTransactionLines_ReturnsDataTable()
        Dim testRefundRep As New TestRefundRepository

        Assert.AreEqual("System.Data.DataTable", testRefundRep.GetSortedRefundTransactionLines(Now, Now).GetType.FullName)
    End Sub

    <TestMethod()> _
    Public Sub GetSortedRefundTransactionLines_ReturnsDataTableSortedOn1stSortColumn()
        Dim testRefundRep As New TestRefundRepositoryOVerridesGetRefundTransactionLinesWithUnsortedDataTableOfTestTransactionLines
        Dim previousLine As DataRow = Nothing
        Dim IsNotSorted As Boolean

        For Each tranLine As DataRow In testRefundRep.GetSortedRefundTransactionLines(Now, Now).Rows
            If previousLine Is Nothing Then
                previousLine = tranLine
            Else
                If CDate(tranLine(testRefundRep.GetSortColumn(1))) < CDate(previousLine(testRefundRep.GetSortColumn(1))) Then
                    IsNotSorted = True
                    Exit For
                ElseIf CDate(tranLine(testRefundRep.GetSortColumn(1))) > CDate(previousLine(testRefundRep.GetSortColumn(1))) Then
                    previousLine = tranLine
                End If
            End If
        Next
        Assert.IsFalse(IsNotSorted)
    End Sub

    <TestMethod()> _
    Public Sub GetSortedRefundTransactionLines_ReturnsDataTableSortedOn2ndSortColumnWithin1st()
        Dim testRefundRep As New TestRefundRepositoryOVerridesGetRefundTransactionLinesWithUnsortedDataTableOfTestTransactionLines
        Dim previousSortCol1Line As DataRow = Nothing
        Dim previousSortCol2Line As DataRow = Nothing
        Dim IsNotSorted As Boolean

        For Each tranLine As DataRow In testRefundRep.GetSortedRefundTransactionLines(Now, Now).Rows
            If previousSortCol1Line Is Nothing Then
                previousSortCol1Line = tranLine
                previousSortCol2Line = Nothing
            Else
                If CDate(tranLine(testRefundRep.GetSortColumn(1))) < CDate(previousSortCol1Line(testRefundRep.GetSortColumn(1))) Then
                    IsNotSorted = True
                    Exit For
                ElseIf CDate(tranLine(testRefundRep.GetSortColumn(1))) > CDate(previousSortCol1Line(testRefundRep.GetSortColumn(1))) Then
                    previousSortCol1Line = tranLine
                    previousSortCol2Line = Nothing
                Else
                    If previousSortCol2Line Is Nothing Then
                        previousSortCol2Line = tranLine
                        previousSortCol1Line = tranLine
                    Else
                        If CStr((testRefundRep.GetSortColumn(2))) < CStr(previousSortCol2Line(testRefundRep.GetSortColumn(2))) Then
                            IsNotSorted = True
                            Exit For
                        ElseIf CStr(tranLine(testRefundRep.GetSortColumn(2))) > CStr(previousSortCol2Line(testRefundRep.GetSortColumn(2))) Then
                            previousSortCol2Line = tranLine
                            previousSortCol1Line = tranLine
                        End If
                    End If
                End If
            End If
            If IsNotSorted Then
                Exit For
            End If
        Next
        Assert.IsFalse(IsNotSorted)
    End Sub

    <TestMethod()> _
    Public Sub GetSortedRefundTransactionLines_ReturnsDataTableSortedOn3rdSortColumnWithin2ndWithin1st()
        Dim testRefundRep As New TestRefundRepositoryOVerridesGetRefundTransactionLinesWithUnsortedDataTableOfTestTransactionLines
        Dim previousSortCol1Line As DataRow = Nothing
        Dim previousSortCol2Line As DataRow = Nothing
        Dim previousSortCol3Line As DataRow = Nothing
        Dim IsNotSorted As Boolean

        For Each tranLine As DataRow In testRefundRep.GetSortedRefundTransactionLines(Now, Now).Rows
            If previousSortCol1Line Is Nothing Then
                previousSortCol1Line = tranLine
                previousSortCol2Line = Nothing
                previousSortCol3Line = Nothing
            Else
                If CDate(tranLine(testRefundRep.GetSortColumn(1))) < CDate(previousSortCol1Line(testRefundRep.GetSortColumn(1))) Then
                    IsNotSorted = True
                    Exit For
                ElseIf CDate(tranLine(testRefundRep.GetSortColumn(1))) > CDate(previousSortCol1Line(testRefundRep.GetSortColumn(1))) Then
                    previousSortCol1Line = tranLine
                    previousSortCol2Line = Nothing
                    previousSortCol3Line = Nothing
                Else
                    If previousSortCol2Line Is Nothing Then
                        previousSortCol2Line = tranLine
                        previousSortCol1Line = tranLine
                    Else
                        If CStr(tranLine(testRefundRep.GetSortColumn(2))) < CStr(previousSortCol2Line(testRefundRep.GetSortColumn(2))) Then
                            IsNotSorted = True
                            Exit For
                        ElseIf CStr(tranLine(testRefundRep.GetSortColumn(2))) > CStr(previousSortCol2Line(testRefundRep.GetSortColumn(2))) Then
                            previousSortCol2Line = tranLine
                            previousSortCol1Line = tranLine
                            previousSortCol3Line = Nothing
                        Else
                            If previousSortCol3Line Is Nothing Then
                                previousSortCol3Line = tranLine
                                previousSortCol2Line = tranLine
                                previousSortCol1Line = tranLine
                            Else
                                If CStr(tranLine(testRefundRep.GetSortColumn(3))) < CStr(previousSortCol2Line(testRefundRep.GetSortColumn(3))) Then
                                    IsNotSorted = True
                                    Exit For
                                ElseIf CStr(tranLine(testRefundRep.GetSortColumn(3))) > CStr(previousSortCol2Line(testRefundRep.GetSortColumn(3))) Then
                                    previousSortCol3Line = tranLine
                                    previousSortCol2Line = tranLine
                                    previousSortCol1Line = tranLine
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            If IsNotSorted Then
                Exit For
            End If
        Next
        Assert.IsFalse(IsNotSorted)
    End Sub
#End Region

#Region "CreateRefundTransactionBeingProcessedFromRefundRow Tests"

    <TestMethod()> _
    Public Sub CreateRefundTransactionBeingProcessedFromRefundRow_CallsSetRefundTransactionBeingProcessedToBlankRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryOverridesSetRefundTransactionBeingProcessedToBlankRefundTransactionToLogWhetherItHasBeenCalledAndProvideTestRefundRow

        With testRefundRep
            .CreateRefundTransactionBeingProcessedFromRefundRow(.TestRefundRow)
            Assert.IsTrue(.SetRefundTransactionBeingProcessedToBlankRefundTransactionWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CreateRefundTransactionBeingProcessedFromRefundRow_CallsRefundRowContainsRefundTransactionFields()
        Dim testRefundRep As New TestRefundRepositoryOverridesRefundRowContainsRefundTransactionFieldsToLogWhetherItHasBeenCalledAndProvideTestRefundRow

        With testRefundRep
            .CreateRefundTransactionBeingProcessedFromRefundRow(.TestRefundRow)
            Assert.IsTrue(.RefundRowContainsRefundTransactionFieldsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CreateRefundTransactionBeingProcessedFromRefundRow_CallsRefundRowContainsRefundTransactionFieldsPassingInRefundRowParameter()
        Dim testRefundRep As New TestRefundRepositoryOverridesRefundRowContainsRefundTransactionFieldsToExposeTheRefundRwoParameterAndProvideTestRefundRow
        Dim expected As DataRow

        With testRefundRep
            expected = .TestRefundRow
            .CreateRefundTransactionBeingProcessedFromRefundRow(expected)
            Assert.AreEqual(expected, .RefundRowContainsRefundTransactionFieldsRefundRowParameter)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CreateRefundTransactionBeingProcessedFromRefundRow_RefundTransactionBeingProcessedIsNothing_DoesNotCallPopulateRefundTransactionBeingProcessedFromRefundRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesPopulateRefundTransactionBeingProcessedFromRefundRowToLogWhetherItHasBeenCalledAndOverridesSetRefundTransactionBeingProcessedToBlankRefundTransactionToSetItToNothingAndProvideTestRefundRow

        With testRefundRep
            .CreateRefundTransactionBeingProcessedFromRefundRow(.TestRefundRow)
            Assert.IsFalse(.PopulateRefundTransactionBeingProcessedFromRefundRowWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CreateRefundTransactionBeingProcessedFromRefundRow_RefundRowContainsRefundTransactionFieldsReturnsFalse_DoesNotCallPopulateRefundTransactionBeingProcessedFromRefundRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesPopulateRefundTransactionBeingProcessedFromRefundRowToLogWhetherItHasBeenCalledAndOverridesRefundRowContainsRefundTransactionFieldsToReturnFalseAndProvideTestRefundRow

        With testRefundRep
            .CreateRefundTransactionBeingProcessedFromRefundRow(.TestRefundRow)
            Assert.IsFalse(.PopulateRefundTransactionBeingProcessedFromRefundRowWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CreateRefundTransactionBeingProcessedFromRefundRow_RefundTransactionBeingProcessedIsSomethingAndRefundRowContainsRefundTransactionFieldsReturnsTrue_CallsPopulateRefundTransactionBeingProcessedFromRefundRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRefundRowContainsRefundTransactionFieldsToReturnTrueAndOverridesPopulateRefundTransactionBeingProcessedFromRefundRowToLogWhetherItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow

        With testRefundRep
            .CreateRefundTransactionBeingProcessedFromRefundRow(.TestRefundRow)
            Assert.IsTrue(.PopulateRefundTransactionBeingProcessedFromRefundRowWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CreateRefundTransactionBeingProcessedFromRefundRow_RefundTransactionBeingProcessedIsSomethingAndRefundRowContainsRefundTransactionFieldsReturnsTrue_CallsPopulateRefundTransactionBeingProcessedFromRefundRowPassingInRefundRow()
        Dim testRefundRep As New TestRefundRepositoryOverridesRefundRowContainsRefundTransactionFieldsToReturnTrueAndOverridesPopulateRefundTransactionBeingProcessedFromRefundRowToLogWhetherItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow
        Dim expected As DataRow

        With testRefundRep
            expected = .TestRefundRow
            .CreateRefundTransactionBeingProcessedFromRefundRow(expected)
            Assert.AreEqual(expected, .PopulateRefundTransactionBeingProcessedFromRefundRowRefundRowParameter)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CreateRefundTransactionBeingProcessedFromRefundRow_ReturnsRefundTransactionBeingProcessed()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRow
        Dim expected As RefundTransaction
        Dim actual As RefundTransaction

        With testRefundRep
            actual = .CreateRefundTransactionBeingProcessedFromRefundRow(.TestRefundRow)
            expected = ._refundTransactionBeingProcessed
            Assert.AreEqual(expected, actual)
        End With
    End Sub
#End Region

#Region "SetRefundTransactionBeingProcessedToBlankRefundTransaction Tests"

    <TestMethod()> _
    Public Sub SetRefundTransactionBeingProcessedToBlankRefundTransaction_SetsRefundTransactionBeingProcessedToNewRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRow
        Dim overwriteThis As New RefundTransaction
        Dim useForNewValues As New RefundTransaction

        With overwriteThis
            .TransactionDate = #4/1/2012#
            .Till = "01"
            .TransactionNumber = "0001"
        End With
        With testRefundRep
            ._refundTransactionBeingProcessed = overwriteThis
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            With ._refundTransactionBeingProcessed
                Assert.IsTrue(.TransactionDate = useForNewValues.TransactionDate And .Till = useForNewValues.Till And .TransactionNumber = useForNewValues.TransactionNumber)
            End With
        End With
    End Sub
#End Region

#Region "PopulateRefundTransactionBeingProcessedFromRefundRow Tests"

    <TestMethod()> _
    Public Sub PopulateRefundTransactionBeingProcessedFromRefundRow_CallsCopyRefundRowFieldValuesToRefundTransactionBeingProcessed()
        Dim testRefundRep As New TestRefundRepositoryOverridesCopyRefundRowFieldValuesToRefundTransactionBeingProcessedToLogItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow

        With testRefundRep
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            .PopulateRefundTransactionBeingProcessedFromRefundRow(.TestRefundRow)
            Assert.IsTrue(.CopyRefundRowFieldValuesToRefundTransactionBeingProcessedWasCalled())
        End With
    End Sub

    <TestMethod()> _
    Public Sub PopulateRefundTransactionBeingProcessedFromRefundRow_CallsCopyRefundRowFieldValuesToRefundTransactionBeingProcessedPassingInRefundRwoParameter()
        Dim testRefundRep As New TestRefundRepositoryOverridesCopyRefundRowFieldValuesToRefundTransactionBeingProcessedToLogItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow
        Dim expected As DataRow

        With testRefundRep
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            expected = .TestRefundRow
            .PopulateRefundTransactionBeingProcessedFromRefundRow(expected)
            Assert.AreEqual(expected, .CopyRefundRowFieldValuesToRefundTransactionBeingProcessedRefundRowParameter)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PopulateRefundTransactionBeingProcessedFromRefundRow_CallsAddRefundLineToRefundTransactionBeingProcessedsLines()
        Dim testRefundRep As New TestRefundRepositoryOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow

        With testRefundRep
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            .PopulateRefundTransactionBeingProcessedFromRefundRow(.TestRefundRow)
            Assert.IsTrue(.AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled())
        End With
    End Sub

    <TestMethod()> _
    Public Sub PopulateRefundTransactionBeingProcessedFromRefundRow_CallsAddRefundLineToRefundTransactionBeingProcessedsLinesPassingInRefundRwoParameter()
        Dim testRefundRep As New TestRefundRepositoryOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow
        Dim expected As DataRow

        With testRefundRep
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            expected = .TestRefundRow
            .PopulateRefundTransactionBeingProcessedFromRefundRow(expected)
            Assert.AreEqual(expected, .AddRefundLineToRefundTransactionBeingProcessedsLinesRefundRowParameter)
        End With
    End Sub
#End Region

#Region "CopyRefundRowFieldValuesToRefundTransactionBeingProcessed Tests"

    <TestMethod()> _
    Public Sub CopyRefundRowFieldValuesToRefundTransactionBeingProcessed_RefundRowToRefundTransactionDateIsDBNull_DoesNotCopyTransactionDateFromRefundRowToRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
        Dim expected As DateTime = #4/1/2012#
        Dim actual As DateTime

        With testRefundRep
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            ._refundTransactionBeingProcessed.TransactionDate = expected
            .CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(.TestRefundRow)
            actual = ._refundTransactionBeingProcessed.TransactionDate
            Assert.IsTrue(DateDiff(DateInterval.Second, expected, actual) = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CopyRefundRowFieldValuesToRefundTransactionBeingProcessed_RefundRowToRefundTillDateIsDBNull_DoesNotCopyTransactionTillFromRefundRowToRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
        Dim expected As String = "11"
        Dim actual As String

        With testRefundRep
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            ._refundTransactionBeingProcessed.Till = expected
            .CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(.TestRefundRow)
            actual = ._refundTransactionBeingProcessed.Till
            Assert.IsTrue(String.Compare(expected, actual) = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CopyRefundRowFieldValuesToRefundTransactionBeingProcessed_RefundRowToRefundTransactionNumberIsDBNull_DoesNotCopyTransactionNumberFromRefundRowToRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
        Dim expected As String = "2222"
        Dim actual As String

        With testRefundRep
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            ._refundTransactionBeingProcessed.TransactionNumber = expected
            .CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(.TestRefundRow)
            actual = ._refundTransactionBeingProcessed.TransactionNumber
            Assert.IsTrue(String.Compare(expected, actual) = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CopyRefundRowFieldValuesToRefundTransactionBeingProcessed_CopiesTransactionDateFromRefundRowToRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
        Dim expected As DateTime
        Dim actual As DateTime

        With testRefundRep
            expected = .TestRefundRowsTransactionDate
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            .CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(.TestRefundRow)
            actual = ._refundTransactionBeingProcessed.TransactionDate
            Assert.IsTrue(DateDiff(DateInterval.Second, expected, actual) = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CopyRefundRowFieldValuesToRefundTransactionBeingProcessed_CopiesTransactionTillFromRefundRowToRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
        Dim expected As String
        Dim actual As String

        With testRefundRep
            expected = .TestRefundRowsTransactionTill
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            .CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(.TestRefundRow)
            actual = ._refundTransactionBeingProcessed.Till
            Assert.IsTrue(String.Compare(expected, actual) = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CopyRefundRowFieldValuesToRefundTransactionBeingProcessed_CopiesTransactionNumberFromRefundRowToRefundTransaction()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
        Dim expected As String
        Dim actual As String

        With testRefundRep
            expected = .TestRefundRowsTransactionNumber
            .SetRefundTransactionBeingProcessedToBlankRefundTransaction()
            .CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(.TestRefundRow)
            actual = ._refundTransactionBeingProcessed.TransactionNumber
            Assert.IsTrue(String.Compare(expected, actual) = 0)
        End With
    End Sub
#End Region

#Region "RefundRowContainsRefundTransactionFields Tests"

    <TestMethod()> _
    Public Sub RefundRowContainsRefundTransactionFields_RefundRowDoesNotContainTransactionDate_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowWithoutTransactionDateField

        With testRefundRep
            Assert.IsFalse(.RefundRowContainsRefundTransactionFields(.TestRefundRow))
        End With
    End Sub

    <TestMethod()> _
    Public Sub RefundRowContainsRefundTransactionFields_RefundRowDoesNotContainTransactionTill_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowWithoutTransactionTillField

        With testRefundRep
            Assert.IsFalse(.RefundRowContainsRefundTransactionFields(.TestRefundRow))
        End With
    End Sub

    <TestMethod()> _
    Public Sub RefundRowContainsRefundTransactionFields_RefundRowDoesNotContainTransactionNumber_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowWithoutTransactionNumberField

        With testRefundRep
            Assert.IsFalse(.RefundRowContainsRefundTransactionFields(.TestRefundRow))
        End With
    End Sub

    <TestMethod()> _
    Public Sub RefundRowContainsRefundTransactionFields_RefundRowContainsTransactionDateAndTransactionTillAndTransactionNumberFields_ReturnsTrue()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowWithOnlyTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField

        With testRefundRep
            Assert.IsTrue(.RefundRowContainsRefundTransactionFields(.TestRefundRow))
        End With
    End Sub
#End Region

#Region "RowSameAsTransaction Tests"

    <TestMethod()> _
    Public Sub RowSameAsTransaction_TransactionAndRowDataAreComparableIsTrue_CallsComparableRefundTransactionWithRefundRepositoryRowAreTheSame()
        Dim testRefundRep As New TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToLogWhetherItHasBeenCalled

        testRefundRep.RowSameAsTransaction(Nothing, Nothing)
        Assert.IsTrue(testRefundRep.ComparableRefundTransactionWithRefundRepositoryRowAreTheSameWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub RowSameAsTransaction_TransactionAndRowDataAreComparableIsFalse_DoesNotCallComparableRefundTransactionWithRefundRepositoryRowAreTheSame()
        Dim testRefundRep As New TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnFalseAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToLogWhetherItHasBeenCalled

        testRefundRep.RowSameAsTransaction(Nothing, Nothing)
        Assert.IsFalse(testRefundRep.ComparableRefundTransactionWithRefundRepositoryRowAreTheSameWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub RowSameAsTransaction_TransactionAndRowDataAreComparableIsTrueAndComparableRefundTransactionWithRefundRepositoryRowAreTheSameReturnsTrue_ReturnsTrue()
        Dim testRefundRep As New TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToReturnTrue

        Assert.IsTrue(testRefundRep.RowSameAsTransaction(Nothing, Nothing))
    End Sub

    <TestMethod()> _
    Public Sub RowSameAsTransaction_TransactionAndRowDataAreComparableIsTrueAndComparableRefundTransactionWithRefundRepositoryRowAreTheSameReturnsFalse_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToReturnFalse

        Assert.IsFalse(testRefundRep.RowSameAsTransaction(Nothing, Nothing))
    End Sub
#End Region

#Region "TransactionAndRowDataAreComparable Tests"

    <TestMethod()> _
    Public Sub TransactionAndRowDataAreComparable_RefundTransactionToCompareIsNothingAndDataRowToCompareWithIsNothing_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim testrefTran As RefundTransaction = Nothing
        Dim testRefRow As DataRow = Nothing

        Assert.IsFalse(testRefundRep.TransactionAndRowDataAreComparable(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub TransactionAndRowDataAreComparable_RefundTransactionToCompareIsNothingAndDataRowToCompareWithIsSomething_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim testrefTran As RefundTransaction = Nothing
        Dim createFrom As New TestRefundRepositoryCreateTransactionAndRowDataAreComparableTestRows
        Dim testRefRow As DataRow = createFrom.CreateTestRowWithTransactionDateFieldAndTransactionTillFieldAnTransactionNumberField

        Assert.IsFalse(testRefundRep.TransactionAndRowDataAreComparable(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub TransactionAndRowDataAreComparable_RefundTransactionToCompareIsSomethingAndDataRowToCompareWithIsNothing_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim testrefTran As RefundTransaction = New RefundTransaction
        Dim testRefRow As DataRow = Nothing

        Assert.IsFalse(testRefundRep.TransactionAndRowDataAreComparable(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub TransactionAndRowDataAreComparable_RefundTransactionNewAndDataRowDoesNotHaveTransactionDateField_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim testrefTran As RefundTransaction = New RefundTransaction
        Dim createFrom As New TestRefundRepositoryCreateTransactionAndRowDataAreComparableTestRows
        Dim testRefRow As DataRow = createFrom.CreateTestRowWithoutTransactionDateField

        Assert.IsFalse(testRefundRep.TransactionAndRowDataAreComparable(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub TransactionAndRowDataAreComparable_RefundTransactionNewAndDataRowDoesNotHaveTransactionTillField_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim testrefTran As RefundTransaction = New RefundTransaction
        Dim createFrom As New TestRefundRepositoryCreateTransactionAndRowDataAreComparableTestRows
        Dim testRefRow As DataRow = createFrom.CreateTestRowWithoutTransactionTillField

        Assert.IsFalse(testRefundRep.TransactionAndRowDataAreComparable(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub TransactionAndRowDataAreComparable_RefundTransactionNewAndDataRowDoesNotHaveTransactionNumberField_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim testrefTran As RefundTransaction = New RefundTransaction
        Dim createFrom As New TestRefundRepositoryCreateTransactionAndRowDataAreComparableTestRows
        Dim testRefRow As DataRow = createFrom.CreateTestRowWithoutTransactionNumberField

        Assert.IsFalse(testRefundRep.TransactionAndRowDataAreComparable(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub TransactionAndRowDataAreComparable_RefundTransactionNewAndDataRowHasTransactionDateFieldAndTransationTillFieldAndTransactionNumberField_ReturnTrue()
        Dim testRefundRep As New TestRefundRepository
        Dim testrefTran As RefundTransaction = New RefundTransaction
        Dim createFrom As New TestRefundRepositoryCreateTransactionAndRowDataAreComparableTestRows
        Dim testRefRow As DataRow = createFrom.CreateTestRowWithTransactionDateFieldAndTransactionTillFieldAnTransactionNumberField

        Assert.IsTrue(testRefundRep.TransactionAndRowDataAreComparable(testrefTran, testRefRow))
    End Sub
#End Region

#Region "ComparableRefundTransactionWithRefundRepositoryRowAreTheSame Tests"

    <TestMethod()> _
    Public Sub ComparableRefundTransactionWithRefundRepositoryRowAreTheSame_RefundTransactionAndDataRowToCompareHaveDifferentTransactionDates_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim createFrom As New TestRefundRepositoryCreateComparableRefundTransactionWithRefundRepositoryRowAreTheSameTestRows
        Dim testRefRow As DataRow = createFrom.TestDataRow
        Dim testrefTran As RefundTransaction = createFrom.TestTransactionWithDifferentTransactionDate

        Assert.IsFalse(testRefundRep.ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub ComparableRefundTransactionWithRefundRepositoryRowAreTheSame_RefundTransactionAndDataRowToCompareHaveDifferentTransactionTill_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim createFrom As New TestRefundRepositoryCreateComparableRefundTransactionWithRefundRepositoryRowAreTheSameTestRows
        Dim testRefRow As DataRow = createFrom.TestDataRow
        Dim testrefTran As RefundTransaction = createFrom.TestTransactionWithDifferentTransactionTill

        Assert.IsFalse(testRefundRep.ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub ComparableRefundTransactionWithRefundRepositoryRowAreTheSame_RefundTransactionAndDataRowToCompareHaveDifferentTransactionNumber_ReturnsFalse()
        Dim testRefundRep As New TestRefundRepository
        Dim createFrom As New TestRefundRepositoryCreateComparableRefundTransactionWithRefundRepositoryRowAreTheSameTestRows
        Dim testRefRow As DataRow = createFrom.TestDataRow
        Dim testrefTran As RefundTransaction = createFrom.TestTransactionWithDifferentTransactionNumber

        Assert.IsFalse(testRefundRep.ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(testrefTran, testRefRow))
    End Sub

    <TestMethod()> _
    Public Sub ComparableRefundTransactionWithRefundRepositoryRowAreTheSame_RefundTransactionAndDataRowToCompareHaveSameTransactionDateAndSameTransationTillAndSameTransactionNumber_ReturnTrue()
        Dim testRefundRep As New TestRefundRepository
        Dim createFrom As New TestRefundRepositoryCreateComparableRefundTransactionWithRefundRepositoryRowAreTheSameTestRows
        Dim testRefRow As DataRow = createFrom.TestDataRow
        Dim testrefTran As RefundTransaction = createFrom.TestTransactionWithSameFieldValues

        Assert.IsTrue(testRefundRep.ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(testrefTran, testRefRow))
    End Sub
#End Region

#Region "NewRefundLine Tests"

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowSkuPriceIsDBNull_DoesNotCopySkuPriceFromRefundRowToRefundLineItemPrice()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine
        Dim useThisForInitialValues As New RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.ItemPrice, useThisForInitialValues.ItemPrice)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowSkuPriceIsValid_CopiesSkuPriceFromRefundRowToRefundLineItemPrice()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.ItemPrice, .TestRefundRowsSkuPrice)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowTranLineNumberIsDBNull_DoesNotCopyTranLineNumberFromRefundRowToRefundLineLineNumber()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine
        Dim useThisForInitialValues As New RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.LineNumber, useThisForInitialValues.LineNumber)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowTranLineNumberIsValid_CopiesTranLineNumberFromRefundRowToRefundRefundLineLineNumber()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.LineNumber, .TestRefundRowsTransactionLineNumber)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowSkuNumberIsDBNull_DoesNotCopySkuNumberFromRefundRowToRefundLineSkuNumber()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine
        Dim useThisForInitialValues As New RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.SkuNumber, useThisForInitialValues.SkuNumber)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowSkuNumberIsValid_CopiesSkuNumberFromRefundRowToRefundLineSkuNumber()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.SkuNumber, .TestRefundRowsSkuNumber)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowSkuQuantityIsDBNull_DoesNotCopySkuQuantityFromRefundRowToRefundLineQuantity()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine
        Dim useThisForInitialValues As New RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.Quantity, useThisForInitialValues.Quantity)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowSkuQuantityIsValid_CopiesSkuQuantityMultipliedByMinus1FromRefundRowToRefundRefundLineQuantity()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.Quantity, .TestRefundRowsSkuQuantity * -1)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowOriginalTranNumberIsDBNull_DoesNotCopyOriginalTranNumberFromRefundRowToRefundLineOriginalTransactionNumber()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine
        Dim useThisForInitialValues As New RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.OriginalTransactionNumber, useThisForInitialValues.OriginalTransactionNumber)
        End With
    End Sub

    <TestMethod()> _
    Public Sub NewRefundLine_RefundRowOriginalTranNumberIsValid_CopiesOriginalTranNumberFromRefundRowToRefundLineOriginalTransactionNumber()
        Dim testRefundRep As New TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsRefundLineFields
        Dim testRefundLine As RefundLine

        With testRefundRep
            testRefundLine = .NewRefundLine(.TestRefundRow)
            Assert.AreEqual(testRefundLine.OriginalTransactionNumber, .TestRefundRowsOriginalTransactionNumber)
        End With
    End Sub
#End Region
#End Region
End Class
