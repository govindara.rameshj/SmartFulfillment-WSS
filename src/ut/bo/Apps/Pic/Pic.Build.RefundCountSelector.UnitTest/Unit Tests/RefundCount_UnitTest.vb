﻿Imports System
Imports System.Text
Imports System.Collections
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class RefundCount_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Constructor Tests"

    '''<summary>
    '''A test for RefundCount Constructor creates right type
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_Constructor_ReturnsCorrectType()
        Dim target As RefundCount = New RefundCount

        Assert.AreEqual("Pic.Build.RefundCountSelector.RefundCount", target.GetType.FullName)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor 
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_Constructor_InitialisesStartDate()
        Dim StartDate As DateTime = Date.MinValue
        Dim target As RefundCount = New RefundCount()

        Assert.AreEqual(StartDate, target.StartDate)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor 
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_Constructor_InitialisesEndDate()
        Dim EndDate As DateTime = Date.MinValue
        Dim target As RefundCount = New RefundCount()

        Assert.AreEqual(EndDate, target.EndDate)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_Constructor_InitialisesTransactionLimit()
        Dim target As RefundCount = New TestRefundCountOverridesGetLimitsToReturnTestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77

        Assert.AreEqual(44.77D, target.TransactionLimit)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor StartDate, EndDate
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_Constructor_InitialisesLineLimit()
        Dim target As RefundCount = New TestRefundCountOverridesGetLimitsToReturnTestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77

        Assert.AreEqual(33.66D, target.LineLimit)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_Constructor_InitialisesSkuQuantityLimit()
        Dim target As RefundCount = New TestRefundCountOverridesGetLimitsToReturnTestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77

        Assert.AreEqual(22, target.SkuQuantityLimit)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor StartDate(, EndDate)
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_StartDateEndDateConstructor_InitialisesStartDate()
        Dim StartDate As DateTime = #4/1/2012#
        Dim EndDate As DateTime = Date.MinValue
        Dim target As RefundCount = New RefundCount(StartDate, EndDate)

        Assert.AreEqual(StartDate, target.StartDate)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor (StartDate,) EndDate
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_StartDateEndDateConstructor_InitialisesEndDate()
        Dim StartDate As DateTime = Date.MinValue
        Dim EndDate As DateTime = #4/1/2012#
        Dim target As RefundCount = New RefundCount(StartDate, EndDate)

        Assert.AreEqual(EndDate, target.EndDate)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor StartDate, EndDate
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_StartDateEndDateConstructor_InitialisesTransactionLimit()
        Dim StartDate As DateTime = Date.MinValue
        Dim EndDate As DateTime = Date.MinValue
        Dim target As RefundCount = New TestRefundCountOverridesGetLimitsToReturnTestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77(StartDate, EndDate)

        Assert.AreEqual(44.77D, target.TransactionLimit)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor StartDate, EndDate
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_StartDateEndDateConstructor_InitialisesLineLimit()
        Dim StartDate As DateTime = Date.MinValue
        Dim EndDate As DateTime = Date.MinValue
        Dim target As RefundCount = New TestRefundCountOverridesGetLimitsToReturnTestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77(StartDate, EndDate)

        Assert.AreEqual(33.66D, target.LineLimit)
    End Sub

    '''<summary>
    '''A test for RefundCount Constructor StartDate, EndDate
    '''</summary>
    <TestMethod()> _
    Public Sub RefundCount_StartDateEndDateConstructor_InitialisesSkuQuantityLimit()
        Dim StartDate As DateTime = Date.MinValue
        Dim EndDate As DateTime = Date.MinValue
        Dim target As RefundCount = New TestRefundCountOverridesGetLimitsToReturnTestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77(StartDate, EndDate)

        Assert.AreEqual(22, target.SkuQuantityLimit)
    End Sub
#End Region

#Region "Property Tests"

    '''<summary>
    '''A test for Transactions Property Set and Get tests
    '''</summary>
    <TestMethod()> _
    Public Sub TransactionsTest()
        Dim target As RefundCount = New RefundCount
        Dim expected As List(Of RefundTransaction)
        Dim actual As List(Of RefundTransaction)

        expected = New List(Of RefundTransaction)
        target.Transactions = expected
        actual = target.Transactions
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for TransactionLimit Property Set and Get tests
    '''</summary>
    <TestMethod()> _
    Public Sub TransactionLimitTest()
        Dim target As RefundCount = New RefundCount
        Dim expected As [Decimal] = 1.23D
        Dim actual As [Decimal]

        target.TransactionLimit = expected
        actual = target.TransactionLimit
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for StartDate Property Set and Get tests
    '''</summary>
    <TestMethod()> _
    Public Sub StartDateTest()
        Dim target As RefundCount = New RefundCount
        Dim expected As DateTime = #4/1/2012#
        Dim actual As DateTime

        target.StartDate = expected
        actual = target.StartDate
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for Skus Property Set and Get tests
    '''</summary>
    <TestMethod()> _
    Public Sub SkusTest()
        Dim target As RefundCount = New RefundCount
        Dim expected As ArrayList = New ArrayList
        Dim actual As ArrayList

        target.Skus = expected
        actual = target.Skus
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for SkuQuantityLimit Property Set and Get tests
    '''</summary>
    <TestMethod()> _
    Public Sub SkuQuantityLimitTest()
        Dim target As RefundCount = New RefundCount
        Dim expected As Integer = 29
        Dim actual As Integer

        target.SkuQuantityLimit = expected
        actual = target.SkuQuantityLimit
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for LineLimit Property Set and Get tests
    '''</summary>
    <TestMethod()> _
    Public Sub LineLimitTest()
        Dim target As RefundCount = New RefundCount ' TODO: Initialize to an appropriate value
        Dim expected As [Decimal] = 2.34D
        Dim actual As [Decimal]

        target.LineLimit = expected
        actual = target.LineLimit
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for EndDate Property Set and Get tests
    '''</summary>
    <TestMethod()> _
    Public Sub EndDateTest()
        Dim target As RefundCount = New RefundCount
        Dim expected As DateTime = #4/2/2012#
        Dim actual As DateTime

        target.EndDate = expected
        actual = target.EndDate
        Assert.AreEqual(expected, actual)
    End Sub
#End Region

#Region "Method Tests"

#Region "Load Tests"

    <TestMethod()> _
    Public Sub Load_WithStartDateEndDate_InitialisesStartDate()
        Dim testRefCount As TestRefundCountOverrideLoadToSetFlagIfCalled = New TestRefundCountOverrideLoadToSetFlagIfCalled
        Dim StartDate As Date = #4/1/2012#

        testRefCount.Load(StartDate, #4/2/2012#)
        Assert.AreEqual(StartDate, testRefCount.StartDate)
    End Sub

    <TestMethod()> _
    Public Sub Load_WithStartDateEndDate_InitialisesEndDate()
        Dim testRefCount As TestRefundCountOverrideLoadToSetFlagIfCalled = New TestRefundCountOverrideLoadToSetFlagIfCalled
        Dim EndDate As Date = #4/2/2012#

        testRefCount.Load(#4/1/2012#, EndDate)
        Assert.AreEqual(EndDate, testRefCount.EndDate)
    End Sub

    <TestMethod()> _
    Public Sub Load_WithStartDateEndDate_CallsLoad()
        Dim testRefCount As TestRefundCountOverrideLoadToSetFlagIfCalled = New TestRefundCountOverrideLoadToSetFlagIfCalled

        testRefCount.Load(#4/1/2012#, #4/2/2012#)
        Assert.IsTrue(testRefCount.LoadMethodWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub Load_WhenNoStartDate_DoesNotCall_RefundRepository_GetTestRefundTransactions()
        Dim testRefCount As TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled = New TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled

        testRefCount.StartDate = Date.MinValue
        testRefCount.EndDate = #4/2/2012#
        testRefCount.Load()
        Assert.IsFalse(testRefCount.TestRefundRepository.GetRefundTransactionsWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub Load_WhenNoEndDate_DoesNotCall_RefundRepository_GetTestRefundTransactions()
        Dim testRefCount As TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled = New TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled

        testRefCount.StartDate = #4/1/2012#
        testRefCount.EndDate = Date.MinValue
        testRefCount.Load()
        Assert.IsFalse(testRefCount.TestRefundRepository.GetRefundTransactionsWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub Load_NoParameters_CallsRefundRepository_GetTestRefundTransactions_PassingOnStartDate()
        Dim testRefCount As TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled = New TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled

        testRefCount.StartDate = #4/1/2012#
        testRefCount.EndDate = #4/2/2012#
        testRefCount.Load()
        Assert.AreEqual(testRefCount.StartDate, testRefCount.TestRefundRepository.StartDate)
    End Sub

    <TestMethod()> _
    Public Sub Load_NoParameters_CallsRefundRepository_GetTestRefundTransactions_PassingOnEndDate()
        Dim testRefCount As TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled = New TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled

        testRefCount.StartDate = #4/1/2012#
        testRefCount.EndDate = #4/2/2012#
        testRefCount.Load()
        Assert.AreEqual(testRefCount.EndDate, testRefCount.TestRefundRepository.EndDate)
    End Sub

    <TestMethod()> _
    Public Sub Load_NoParameters_CallsRefundRepository_GetTestRefundTransactions()
        Dim testRefCount As TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled = New TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled

        testRefCount.StartDate = #4/1/2012#
        testRefCount.EndDate = #4/2/2012#
        testRefCount.Load()
        Assert.IsTrue(testRefCount.TestRefundRepository.GetRefundTransactionsWasCalled)
    End Sub
#End Region
#End Region

#Region "Internals Tests"

#Region "GetLimits Tests"

    <TestMethod()> _
    Public Sub GetLimits_ReturnsRefundRepositoryType()
        Dim testRefCount As New RefundCount

        Assert.AreEqual("Pic.Build.RefundCountSelector.RefundRepository", testRefCount.GetLimits.GetType.FullName)
    End Sub
#End Region

#Region "GetRefundSkuList Tests"

    <TestMethod()> _
    Public Sub GetRefundSkuList_NoTransactions_ReturnsSomething()
        Dim testRefundCount As RefundCount = New RefundCount

        Assert.IsTrue(testRefundCount.GetRefundSkuList IsNot Nothing)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundSkuList_NoTransactions_ReturnsNoRefundSkus()
        Dim testRefundCount As RefundCount = New RefundCount

        Assert.IsTrue(testRefundCount.GetRefundSkuList.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundSkuList_ThreeTransactionsOneTransactionOverLimit_CallsAddAllTransactionLinesToSkuListOnce()
        Dim testRefundCount As TestRefundCountThreeTransactionsEachWithThreeRefundLinesOnlyOneOverLimit = New TestRefundCountThreeTransactionsEachWithThreeRefundLinesOnlyOneOverLimit

        testRefundCount.GetRefundSkuList()
        Assert.IsTrue(testRefundCount.AddAllTransactionLinesToSkuListCount = 1)
    End Sub

    <TestMethod()> _
    Public Sub GetRefundSkuList_ThreeTransactionsNoneOverLimit_CallsAddTransactionLinesOverLimitToSkuListThreeTimes()
        Dim testRefundCount As TestRefundCountThreeTransactionsNoneOverLimitOneWithThreeRefundLinesOverLimit = New TestRefundCountThreeTransactionsNoneOverLimitOneWithThreeRefundLinesOverLimit

        testRefundCount.GetRefundSkuList()
        Assert.IsTrue(testRefundCount.AddTransactionLinesOverLimitToSkuListCount = 2)
    End Sub

    '<TestMethod()> _
    'Public Sub GetRefundSkuList_OneTransactionsOneOverLimitWith2OutOf3LinesOverLineLimitAsWell_CallsAddAllTransactionLinesToSkuListOnce()

    'End Sub

    '<TestMethod()> _
    'Public Sub GetRefundSkuList_OneTransactionsOneOverLimitWith2OutOf3LinesOverLineLimitAsWell_DoesNotCallAddTransactionLinesOverLimitToSkuList()

    'End Sub
#End Region

#Region "AddAllTransactionLinesToSkuList Tests"

    <TestMethod()> _
    Public Sub AddAllTransactionLinesToSkuList_NoSkuList_DoesNothing()
        Dim testRefundCount As RefundCount = New RefundCount
        Dim SkuListParam As List(Of RefundSku) = Nothing

        testRefundCount.AddAllTransactionLinesToSkuList(SkuListParam, New RefundTransaction())
        Assert.IsTrue(SkuListParam Is Nothing)
    End Sub

    <TestMethod()> _
    Public Sub AddAllTransactionLinesToSkuList_NoTransaction_DoesNothing()
        Dim testRefundCount As RefundCount = New RefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As RefundTransaction = Nothing

        testRefundCount.AddAllTransactionLinesToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub AddAllTransactionLinesToSkuList_TransactionNoRefundLines_DoesNothing()
        Dim testRefundCount As RefundCount = New RefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As RefundTransaction = New RefundTransaction

        testRefundCount.AddAllTransactionLinesToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub AddAllTransactionLinesToSkuList_TransactionWithThreeRefundLines_AddsThreeRefundSkus()
        Dim testRefundCount As RefundCount = New RefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As TestRefundTransactionTotal30With3TestRefundLine_Total30_SkuNumberThisOneLines = New TestRefundTransactionTotal30With3TestRefundLine_Total30_SkuNumberThisOneLines

        testRefundCount.AddAllTransactionLinesToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Count = 3)
    End Sub
#End Region

#Region "AddTransactionLinesOverLimitToSkuList Tests"

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_NoSkuList_DoesNothingToSkuList()
        Dim testRefundCount As RefundCount = New RefundCount
        Dim SkuListParam As List(Of RefundSku) = Nothing

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, New RefundTransaction())
        Assert.IsTrue(SkuListParam Is Nothing)
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_NoTransaction_DoesNothingToSkuList()
        Dim testRefundCount As RefundCount = New RefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As RefundTransaction = Nothing

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_TransactionNoRefundLines_DoesNothing()
        Dim testRefundCount As RefundCount = New RefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As RefundTransaction = New RefundTransaction

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_TransactionWithThreeRefundLinesOneOverLimit_AddsOneRefundSku()
        Dim testRefundCount As TestRefundCount = New TestRefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As TestRefundTransactionTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines = New TestRefundTransactionTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Count = 1)
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_TransactionWithThreeRefundLinesOneOverLimit_AddsRefundSkuOverTheLimit()
        Dim testRefundCount As TestRefundCount = New TestRefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As TestRefundTransactionTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines = New TestRefundTransactionTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Item(0).Number = "ThisOne")
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_TransactionWithThreeRefundLinesUnderLimitButForSameSkuAndTotalIsOverLimitAndOneSkuUnderLimitWithTotalJustUnderTotalLimit_AddsTheRefundSku()
        Dim testRefundCount As TestRefundCount = New TestRefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As New TestRefundTransactionTotal49_99With3TestRefundLine_Total10_SkuNumberThisOneLinesAnd1TestRefundLine_Total19_99_SkuNumberNotThisOneLines

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Item(0).Number = "ThisOne")
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_TransactionWithThreeRefundLinesUnderLimitButForSameSkuAndTotalIsOverLimitAndOneSkuUnderLimitWithTotalJustUnderTotalLimit_AddsThreeLines()
        Dim testRefundCount As TestRefundCount = New TestRefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As New TestRefundTransactionTotal49_99With3TestRefundLine_Total10_SkuNumberThisOneLinesAnd1TestRefundLine_Total19_99_SkuNumberNotThisOneLines

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Count = 3)
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_TransactionWithThreeRefundLinesUnderLimitButForSameSkuAndTotalIsOverLimitAndOneSkuUnderLimitWithTranTotalJustUnderTotalLimit_AddsTheThreeLinesForSkuOverTheLimit()
        Dim testRefundCount As TestRefundCount = New TestRefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As New TestRefundTransactionTotal49_99With3TestRefundLine_Total10_SkuNumberThisOneLinesAnd1TestRefundLine_Total19_99_SkuNumberNotThisOneLines

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        For Each NextRefSku As RefundSku In SkuListParam
            Assert.IsTrue(NextRefSku.Number = RefTranParam.SkuNumberFor3LinesOverLimit)
        Next
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_TransactionWithThreeRefundLinesUnderLimitButForSameSkuButDifferentPricesAndTotalIsOnLimit_AndOther2SkusBelowLineLimit_WithOverallTotalUnderTranLimit_AddsTheThreeLines()
        Dim testRefundCount As TestRefundCount = New TestRefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As New TestRefundTransactionTotal38_99With3TestRefundLinesEachWithSameSkuButDifferentPriceAnd2TestRefundLinesWithDifferentSkusAlsoUnderLimit

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        Assert.IsTrue(SkuListParam.Count = 3)
    End Sub

    <TestMethod()> _
    Public Sub AddTransactionLinesOverLimitToSkuList_TransactionWithThreeRefundLinesUnderLimitButForSameSkuButDifferentPricesAndTotalIsOnLimit_AndOther2SkusBelowLineLimit_WithOverallTotalUnderTranLimit_AddsTheThreeLinesForSkuWithTotalOnTheLimit()
        Dim testRefundCount As TestRefundCount = New TestRefundCount
        Dim SkuListParam As List(Of RefundSku) = New List(Of RefundSku)
        Dim RefTranParam As New TestRefundTransactionTotal38_99With3TestRefundLinesEachWithSameSkuButDifferentPriceAnd2TestRefundLinesWithDifferentSkusAlsoUnderLimit

        testRefundCount.AddTransactionLinesOverLimitToSkuList(SkuListParam, RefTranParam)
        For Each NextRefSku As RefundSku In SkuListParam
            Assert.IsTrue(NextRefSku.Number = RefTranParam.SkuNumberFor3LinesOverLimit)
        Next
    End Sub
#End Region

#Region "GetDistinctRefundSkuValueList"

    <TestMethod()> _
    Public Sub GetDistinctRefundSkuValueList_EmptyRefundSkuList_ReturnsSomething()
        Dim testRefundCount As RefundCount = New RefundCount

        Assert.IsTrue(testRefundCount.GetDistinctRefundSkuValueList IsNot Nothing)
    End Sub

    <TestMethod()> _
    Public Sub GetDistinctRefundSkuValueList_RefundSkuListWith1RefundSku_Returns1RefundSku()
        Dim testRefundCount As TestRefundCountOverrideGetRefundSkuListReturn1RefundSkuInList = New TestRefundCountOverrideGetRefundSkuListReturn1RefundSkuInList

        Assert.IsTrue(testRefundCount.GetDistinctRefundSkuValueList.Count = 1)
    End Sub

    <TestMethod()> _
    Public Sub GetDistinctRefundSkuValueList_RefundSkuListWith2DifferentRefundSkus_Returns2RefundSkus()
        Dim testRefundCount As TestRefundCountOverrideGetRefundSkuListWith2DifferentRefundSkus = New TestRefundCountOverrideGetRefundSkuListWith2DifferentRefundSkus

        Assert.IsTrue(testRefundCount.GetDistinctRefundSkuValueList.Count = 2)
    End Sub

    <TestMethod()> _
    Public Sub GetDistinctRefundSkuValueList_RefundSkuListWith2PairOfSameRefundSkus_Returns2RefundSku()
        Dim testRefundCount As TestRefundCountOverrideGetRefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34 = New TestRefundCountOverrideGetRefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34

        Assert.IsTrue(testRefundCount.GetDistinctRefundSkuValueList.Count = 2)
    End Sub

    <TestMethod()> _
    Public Sub GetDistinctRefundSkuValueList_RefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34_ReturnsTestSku1RefundSkuWithTotalValue58_01()
        Dim testRefundCount As TestRefundCountOverrideGetRefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34 = New TestRefundCountOverrideGetRefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34
        Dim Found As Boolean = False

        For Each refSku As RefundSku In testRefundCount.GetDistinctRefundSkuValueList
            If String.Compare(refSku.Number, testRefundCount.TestSkuIDForValue58_01) = 0 Then
                Found = True
                Assert.IsTrue(refSku.Value = 58.01D)
                Exit For
            End If
        Next
        If Not Found Then
            Assert.Inconclusive("Could not find RefundSku item (" & testRefundCount.TestSkuIDForValue58_01 & ") to verify its value.")
        End If
    End Sub

    <TestMethod()> _
    Public Sub GetDistinctRefundSkuValueList_RefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34_ReturnsTestSku2RefundSkuWithTotalValue91_34()
        Dim testRefundCount As TestRefundCountOverrideGetRefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34 = New TestRefundCountOverrideGetRefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34
        Dim Found As Boolean = False

        For Each refSku As RefundSku In testRefundCount.GetDistinctRefundSkuValueList
            If String.Compare(refSku.Number, testRefundCount.TestSkuIDForValue91_34) = 0 Then
                Found = True
                Assert.IsTrue(refSku.Value = 91.34D)
                Exit For
            End If
        Next
        If Not Found Then
            Assert.Inconclusive("Could not find RefundSku item (" & testRefundCount.TestSkuIDForValue91_34 & ") to verify its value.")
        End If
    End Sub
#End Region

#Region "GetSortedDistinctRefundSkuValueList Tests"

    <TestMethod()> _
    Public Sub GetSortedDistinctRefundSkuValueList_EmptyDistinctRefundSkuList_ReturnsSomething()
        Dim testRefundCount As RefundCount = New RefundCount

        Assert.IsTrue(testRefundCount.GetSortedDistinctRefundSkuValueList IsNot Nothing)
    End Sub

    <TestMethod()> _
    Public Sub GetSortedDistinctRefundSkuValueList_UnsortedDistinctRefundSkuListOf6DistinctValues_ReturnsDistinctSkuValueListSortedOnValue()
        Dim testRefundCount As TestRefundCountOverrideGetDistinctSkuValueListWithListOf6DistinctValues = New TestRefundCountOverrideGetDistinctSkuValueListWithListOf6DistinctValues
        Dim AllSkusInOrder As New StringBuilder

        For Each refSku As RefundSku In testRefundCount.GetSortedDistinctRefundSkuValueList
            AllSkusInOrder.Append(refSku.Number)
        Next
        Assert.IsTrue(String.Compare(testRefundCount.GetCorrectAllSkusInOrder, AllSkusInOrder.ToString) = 0)
    End Sub
#End Region

#Region "GetTruncatedSortedDistinctRefundSkuValueAndRelatedItemList Tests"

    <TestMethod()> _
    Public Sub GetTruncatedSortedDistinctRefundSkuValueList_EmptyDistinctRefundSkuList_ReturnsSomething()
        Dim testRefundCount As RefundCount = New RefundCount

        Assert.IsTrue(testRefundCount.GetTruncatedSortedDistinctRefundSkuValueAndRelatedItemList IsNot Nothing)
    End Sub

    <TestMethod()> _
    Public Sub GetTruncatedSortedDistinctRefundSkuValueList_UnsortedDistinctRefundSkuListOf6DistinctValues_Returns3RefundSkus()
        Dim testRefundCount As TestRefundCountSkuQuantityLimitOf3OverrideGetSortedDistinctSkuValueListWithListOf6DistinctValues = New TestRefundCountSkuQuantityLimitOf3OverrideGetSortedDistinctSkuValueListWithListOf6DistinctValues

        Assert.IsTrue(testRefundCount.GetTruncatedSortedDistinctRefundSkuValueAndRelatedItemList.Count = 3)
    End Sub

    <TestMethod()> _
    Public Sub GetTruncatedSortedDistinctRefundSkuValueList_SkuQuantityLimitOf3AndSortedDistinctRefundSkuListOf6RefundSkus_ReturnsTop3RefundSkus()
        Dim testRefundCount As TestRefundCountSkuQuantityLimitOf3OverrideGetSortedDistinctSkuValueListWithListOf6DistinctValues = New TestRefundCountSkuQuantityLimitOf3OverrideGetSortedDistinctSkuValueListWithListOf6DistinctValues
        Dim AllSkusInOrder As New StringBuilder

        For Each refSku As RefundSku In testRefundCount.GetTruncatedSortedDistinctRefundSkuValueAndRelatedItemList
            AllSkusInOrder.Append(refSku.Number)
        Next
        Assert.IsTrue(String.Compare(testRefundCount.GetTruncatedSortedRefundSkuValueListAllSkusInOrder, AllSkusInOrder.ToString) = 0)
    End Sub
#End Region
#End Region
End Class
