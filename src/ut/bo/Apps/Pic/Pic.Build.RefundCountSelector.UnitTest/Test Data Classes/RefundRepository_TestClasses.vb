﻿Imports System.Data

Friend Class TestRefundRepository
    Inherits RefundRepository

    Friend Overrides Function GetRefundTransactionLines(ByVal StartDate As Date, ByVal EndDate As Date) As System.Data.DataTable

        GetRefundTransactionLines = Me.GetInitialisedDataTable()
    End Function

    Friend Overrides Function GetLineLimit() As Decimal

        Return 25D
    End Function

    Friend Overrides Function GetSkuQuantityLimit() As Integer

        Return 10
    End Function

    Friend Overrides Function GetTransactionLimit() As Decimal

        Return 50D
    End Function
End Class

Friend Class TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled
    Inherits TestRefundRepository

    Private _endDate As Date
    Private _getRefundTransactionsWasCalled As Boolean
    Private _startDate As Date

    Public Overrides Function GetRefundTransactions(ByVal StartDate As Date, ByVal EndDate As Date) As System.Collections.Generic.List(Of RefundCountSelector.RefundTransaction)

        GetRefundTransactions = Nothing
        _startDate = StartDate
        _endDate = EndDate
        _getRefundTransactionsWasCalled = True
    End Function

    Public ReadOnly Property GetRefundTransactionsWasCalled() As Boolean
        Get
            Return _getRefundTransactionsWasCalled
        End Get
    End Property

    Public ReadOnly Property EndDate() As Date
        Get
            Return _endDate
        End Get
    End Property

    Public ReadOnly Property StartDate() As Date
        Get
            Return _startDate
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77
    Inherits TestRefundRepository

    Friend Overrides Function GetLineLimit() As Decimal

        Return 33.66D
    End Function

    Friend Overrides Function GetSkuQuantityLimit() As Integer

        Return 22
    End Function

    Friend Overrides Function GetTransactionLimit() As Decimal

        Return 44.77D
    End Function
End Class

Friend Class TestRefundRepositoryOVerridesGetRefundTransactionLinesWithUnsortedDataTableOfTestTransactionLines
    Inherits TestRefundRepository

    Friend Overrides Function GetRefundTransactionLines(ByVal StartDate As Date, ByVal EndDate As Date) As System.Data.DataTable
        Dim RefundRow As System.Data.DataRow

        GetRefundTransactionLines = Me.GetInitialisedDataTable
        With GetRefundTransactionLines
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "11"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 3
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0011"
                .Item(_fieldName_TranLineNumber) = 3
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "05"
                .Item(_fieldName_TranNumber) = "0111"
                .Item(_fieldName_TranLineNumber) = 3
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "11"
                .Item(_fieldName_TranNumber) = "0002"
                .Item(_fieldName_TranLineNumber) = 3
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0012"
                .Item(_fieldName_TranLineNumber) = 3
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "05"
                .Item(_fieldName_TranNumber) = "0112"
                .Item(_fieldName_TranLineNumber) = 3
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "11"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 2
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0011"
                .Item(_fieldName_TranLineNumber) = 2
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "05"
                .Item(_fieldName_TranNumber) = "0111"
                .Item(_fieldName_TranLineNumber) = 2
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "11"
                .Item(_fieldName_TranNumber) = "0002"
                .Item(_fieldName_TranLineNumber) = 2
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0012"
                .Item(_fieldName_TranLineNumber) = 2
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "05"
                .Item(_fieldName_TranNumber) = "0112"
                .Item(_fieldName_TranLineNumber) = 2
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "11"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 1
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0011"
                .Item(_fieldName_TranLineNumber) = 1
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "05"
                .Item(_fieldName_TranNumber) = "0111"
                .Item(_fieldName_TranLineNumber) = 1
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "11"
                .Item(_fieldName_TranNumber) = "0002"
                .Item(_fieldName_TranLineNumber) = 1
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0012"
                .Item(_fieldName_TranLineNumber) = 1
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "05"
                .Item(_fieldName_TranNumber) = "0112"
                .Item(_fieldName_TranLineNumber) = 1
            End With
            .Rows.Add(RefundRow)
        End With
    End Function
End Class

Friend Class TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToSupply3RefundTransactionLinesFromSameTransaction
    Inherits TestRefundRepository

    Friend Overrides Function GetSortedRefundTransactionLines(ByVal StartDate As Date, ByVal EndDate As Date) As System.Data.DataTable
        Dim RefundRow As System.Data.DataRow

        GetSortedRefundTransactionLines = Me.GetInitialisedDataTable
        With GetSortedRefundTransactionLines
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 1
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 2
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 3
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
        End With
    End Function
End Class

Friend Class TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToSupply2RefundTransactionEachWith3Lines
    Inherits TestRefundRepository

    Friend Overrides Function GetSortedRefundTransactionLines(ByVal StartDate As Date, ByVal EndDate As Date) As System.Data.DataTable
        Dim RefundRow As System.Data.DataRow

        GetSortedRefundTransactionLines = Me.GetInitialisedDataTable
        With GetSortedRefundTransactionLines
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 1
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 2
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0001"
                .Item(_fieldName_TranLineNumber) = 3
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0002"
                .Item(_fieldName_TranLineNumber) = 1
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0002"
                .Item(_fieldName_TranLineNumber) = 2
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
            RefundRow = .NewRow()
            With RefundRow
                .Item(_fieldName_TranDate) = #4/1/2012#
                .Item(_fieldName_TranTill) = "01"
                .Item(_fieldName_TranNumber) = "0002"
                .Item(_fieldName_TranLineNumber) = 3
                .Item(_fieldName_OriginalTranNumber) = "0000"
            End With
            .Rows.Add(RefundRow)
        End With
    End Function
End Class

Friend Class TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToLogWhetherItHasBeenCalled
    Inherits TestRefundRepository

    Private _initialiseSortedRefundRepositoryWasCalled As Boolean = False

    Friend Overrides Sub InitialiseSortedRefundRepository(ByVal StartDate As Date, ByVal EndDate As Date)

        _initialiseSortedRefundRepositoryWasCalled = True
    End Sub

    Public Function InitialiseSortedRefundRepositoryWasCalled() As Boolean

        Return _initialiseSortedRefundRepositoryWasCalled
    End Function
End Class

Friend Class TestRefundRepositoryOverridesInitialiseRefundTransactionsListToLogWhetherItHasBeenCalled
    Inherits TestRefundRepository

    Private _initialiseRefundTransactionsListWasCalled As Boolean

    Friend Overrides Sub InitialiseRefundTransactionsList()

        _initialiseRefundTransactionsListWasCalled = True
    End Sub

    Public Function InitialiseRefundTransactionsListWasCalled() As Boolean

        Return _initialiseRefundTransactionsListWasCalled
    End Function
End Class

Friend Class TestRefundRepositoryOverridesGetSortedRefundTransactionLinesToLogWhetherItHasBeenCalled
    Inherits TestRefundRepository

    Private _getSortedRefundTransactionLines As Boolean
    Private _startDate As Date = Date.MinValue
    Private _endDate As Date = Date.MinValue

    Friend Overrides Function GetSortedRefundTransactionLines(ByVal StartDate As Date, ByVal EndDate As Date) As System.Data.DataTable

        _getSortedRefundTransactionLines = True
        _startDate = StartDate
        _endDate = EndDate
        Return New System.Data.DataTable
    End Function

    Public Function GetSortedRefundTransactionLinesWasCalled() As Boolean

        Return _getSortedRefundTransactionLines
    End Function

    Public ReadOnly Property EndDate() As Date
        Get
            Return _endDate
        End Get
    End Property

    Public ReadOnly Property StartDate() As Date
        Get
            Return _startDate
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToLogWhetherItHasBeenCalled
    Inherits TestRefundRepository

    Private _initialiseFilteredRefundRepositoryFromSortedRefundRepositoryWasCalled As Boolean

    Friend Overrides Sub InitialiseFilteredRefundRepositoryFromSortedRefundRepository()

        _initialiseFilteredRefundRepositoryFromSortedRefundRepositoryWasCalled = True
    End Sub

    Public ReadOnly Property InitialiseFilteredRefundRepositoryFromSortedRefundRepositoryWasCalled() As Boolean
        Get
            Return _initialiseFilteredRefundRepositoryFromSortedRefundRepositoryWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesNewToCallMyBaseNewAndThenReset_refundTransactions_BackToNothing
    Inherits TestRefundRepository

    Public Sub New()

        MyBase.New()
        Me._refundTransactions = Nothing
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesSortedRefundRepositoryHasDataToLogWhetherItHasBeenCalled
    Inherits TestRefundRepository

    Private _sortedRefundRepositoryHasDataWasCalled As Boolean

    Friend Overrides Function SortedRefundRepositoryHasData() As Boolean

        _sortedRefundRepositoryHasDataWasCalled = True
    End Function

    Public ReadOnly Property SortedRefundRepositoryHasDataWasCalled() As Boolean
        Get
            Return _sortedRefundRepositoryHasDataWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToNothing
    Inherits TestRefundRepository

    Friend Overrides Sub InitialiseSortedRefundRepository(ByVal StartDate As Date, ByVal EndDate As Date)

        _sortedRefundRepository = Nothing
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToEmptyDataTable
    Inherits TestRefundRepository

    Friend Overrides Sub InitialiseSortedRefundRepository(ByVal StartDate As Date, ByVal EndDate As Date)

        _sortedRefundRepository = New System.Data.DataTable
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToDataTableWith1Row
    Inherits RefundRepositoryProvidesColumnNamesArray

    Friend Overrides Sub InitialiseSortedRefundRepository(ByVal StartDate As Date, ByVal EndDate As Date)

        _sortedRefundRepository = GetInitialisedDataTable()
        With _sortedRefundRepository
            .Rows.Add(.NewRow)
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesRowIsNoProofOfPurchaseRefundToFlagWhetherItWasCalledAndReturnTrue
    Inherits TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToDataTableWith1Row

    Private _rowIsNoProofOfPurchaseRefundWasCalled As Boolean

    Friend Overrides Function RowIsNoProofOfPurchaseRefund(ByVal RefundRow As System.Data.DataRow) As Boolean

        _rowIsNoProofOfPurchaseRefundWasCalled = True
        Return True
    End Function

    Public ReadOnly Property RowIsNoProofOfPurchaseRefundWasCalled() As Boolean
        Get
            Return _rowIsNoProofOfPurchaseRefundWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesRowIsNoProofOfPurchaseRefundToFlagWhetherItWasCalledAndReturnTrueAndExposeTheInitialisedRow
    Inherits TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToDataTableWith1Row

    Private _rowIsNoProofOfPurchaseRefundWasCalled As Boolean
    Private _initialisedRow As DataRow

    Friend Overrides Function RowIsNoProofOfPurchaseRefund(ByVal RefundRow As System.Data.DataRow) As Boolean

        _rowIsNoProofOfPurchaseRefundWasCalled = True
        Return True
    End Function

    Friend Overrides Sub InitialiseSortedRefundRepository(ByVal StartDate As Date, ByVal EndDate As Date)

        _sortedRefundRepository = GetInitialisedDataTable()
        With _sortedRefundRepository
            _initialisedRow = .NewRow
            With InitialisedRow
                .Item(_fieldName_TranDate) = #4/2/2012#
                .Item(_fieldName_TranTill) = "02"
                .Item(_fieldName_TranNumber) = "0002"
                .Item(_fieldName_TranLineNumber) = 1
                .Item(_fieldName_SkuNumber) = "123456"
                .Item(_fieldName_SkuPrice) = 14.44
                .Item(_fieldName_SkuQuantity) = -1
                .Item(_fieldName_OriginalTranNumber) = "0000"
                .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = 0
            End With
            .Rows.Add(InitialisedRow)
        End With
    End Sub

    Public ReadOnly Property RowIsNoProofOfPurchaseRefundWasCalled() As Boolean
        Get
            Return _rowIsNoProofOfPurchaseRefundWasCalled
        End Get
    End Property

    Public ReadOnly Property InitialisedRow() As DataRow
        Get
            Return _initialisedRow
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesRefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensToFlagWhetherItWasCalledAndOverridesRowIsNoProofOfPurchaseRefundToReturnFalse
    Inherits TestRefundRepositoryOverridesInitialiseSortedRefundRepositoryToSetSortedDataToDataTableWith1Row

    Private _refundRowFullyOrPartiallyPaidInCashAndOrGiftTokensWasCalled As Boolean

    Friend Overrides Function RowIsNoProofOfPurchaseRefund(ByVal RefundRow As System.Data.DataRow) As Boolean

        Return False
    End Function

    Friend Overrides Function RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokens(ByVal RefundRow As System.Data.DataRow) As Boolean

        _refundRowFullyOrPartiallyPaidInCashAndOrGiftTokensWasCalled = True
        Return True
    End Function

    Public ReadOnly Property RefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensWasCalled() As Boolean
        Get
            Return _refundRowFullyOrPartiallyPaidInCashAndOrGiftTokensWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesRefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensToFlagWhetherItWasCalledAndReturnFalseAndOverridesRowIsNoProofOfPurchaseRefundToReturnTrueAndExposeTheInitialisedRow
    Inherits TestRefundRepositoryOverridesRefundRowFullyOrPartiallyPaidInCashAndOrGiftTokensToFlagWhetherItWasCalledAndOverridesRowIsNoProofOfPurchaseRefundToReturnFalse

    Private _initialisedRow As DataRow

    Friend Overrides Sub InitialiseSortedRefundRepository(ByVal StartDate As Date, ByVal EndDate As Date)

        _sortedRefundRepository = GetInitialisedDataTable()
        With _sortedRefundRepository
            _initialisedRow = .NewRow
            With InitialisedRow
                .Item(_fieldName_TranDate) = #4/2/2012#
                .Item(_fieldName_TranTill) = "02"
                .Item(_fieldName_TranNumber) = "0002"
                .Item(_fieldName_TranLineNumber) = 1
                .Item(_fieldName_SkuNumber) = "123456"
                .Item(_fieldName_SkuPrice) = 14.44
                .Item(_fieldName_SkuQuantity) = -1
                .Item(_fieldName_OriginalTranNumber) = "0000"
                .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = 0
            End With
            .Rows.Add(InitialisedRow)
        End With
    End Sub

    Public ReadOnly Property InitialisedRow() As DataRow
        Get
            Return _initialisedRow
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedAndFilteredDataToNothing
    Inherits TestRefundRepository

    Friend Overrides Sub InitialiseFilteredRefundRepositoryFromSortedRefundRepository()

        _sortedAndFilteredRefundRepository = Nothing
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToEmptyDataTable
    Inherits TestRefundRepository

    Friend Overrides Sub InitialiseFilteredRefundRepositoryFromSortedRefundRepository()

        _sortedAndFilteredRefundRepository = New System.Data.DataTable
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1Row
    Inherits RefundRepositoryProvidesColumnNamesArray

    Friend Overrides Sub InitialiseFilteredRefundRepositoryFromSortedRefundRepository()

        _sortedAndFilteredRefundRepository = GetInitialisedDataTable()
        With _sortedAndFilteredRefundRepository
            .Rows.Add(.NewRow)
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnTrueAndOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalled
    Inherits TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1Row

    Private _setRefundTransactionBeingProcessedToNewRefundTransactionWasCalled As Boolean

    Public Sub New()

        MyBase.New()
        InitialiseSortedRefundRepository(#4/1/2012#, #4/1/2012#)
    End Sub

    Friend Overrides Function RefundTransactionBeingProcessedIsNothing() As Boolean

        Return True
    End Function

    Friend Overrides Sub SetRefundTransactionBeingProcessedToNewRefundTransaction(ByVal RefundRow As System.Data.DataRow)

        _setRefundTransactionBeingProcessedToNewRefundTransactionWasCalled = True
    End Sub

    Friend Overrides Sub ProcessRefundRepositoryRow(ByVal RefundRow As System.Data.DataRow)

    End Sub

    Friend Overrides Sub AddRefundTransactionBeingProcessedToRefundTransactionsList()

    End Sub

    Public ReadOnly Property SetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled() As Boolean
        Get
            Return _setRefundTransactionBeingProcessedToNewRefundTransactionWasCalled
        End Get
    End Property

    Friend Overrides Function RowSameAsTransaction(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        Return False
    End Function

    Friend Overrides Function CreateRefundTransactionBeingProcessedFromRefundRow(ByVal RefundRow As System.Data.DataRow) As RefundCountSelector.RefundTransaction

        Return New RefundTransaction
    End Function
End Class

Friend Class TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnFalseAndOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalled
    Inherits TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnTrueAndOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalled

    Friend Overrides Function RefundTransactionBeingProcessedIsNothing() As Boolean

        Return False
    End Function
End Class

Friend Class TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnFalseAndOverridesProcessRefundRepositoryRowToLogWhetherItHasBeenCalled
    Inherits TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnFalseAndOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalled

    Private _processRefundRepositoryRowWasCalled As Boolean

    Public ReadOnly Property ProcessRefundRepositoryRowWasCalled() As Boolean
        Get
            Return _processRefundRepositoryRowWasCalled
        End Get
    End Property

    Friend Overrides Sub ProcessRefundRepositoryRow(ByVal RefundRow As System.Data.DataRow)

        _processRefundRepositoryRowWasCalled = True
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnTrueAndOverridesProcessRefundRepositoryRowToLogWhetherItHasBeenCalled
    Inherits TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToDataTableWith1RowAndOverridesRefundTransactionBeingProcessedIsNothingToReturnFalseAndOverridesProcessRefundRepositoryRowToLogWhetherItHasBeenCalled

    Friend Overrides Function RefundTransactionBeingProcessedIsNothing() As Boolean

        Return True
    End Function
End Class

Friend Class TestRefundRepositoryUsingEmptySortedRefundRepositoryOverridesRefundTransactionBeingProcessedIsNothingToReturnTrueAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListToLogWhetherItHasBeenCalled
    Inherits TestRefundRepositoryOverridesInitialiseRefundTranToLogWhetherItHasBeenCalledUsingEmptySortedRefundRepository

    Private _addRefundTransactionBeingProcessedToRefundTransactionsListWasCalled As Boolean

    Friend Overrides Function RefundTransactionBeingProcessedIsNothing() As Boolean

        Return True
    End Function

    Friend Overrides Sub AddRefundTransactionBeingProcessedToRefundTransactionsList()

        _addRefundTransactionBeingProcessedToRefundTransactionsListWasCalled = True
    End Sub


    Public ReadOnly Property AddRefundTransactionBeingProcessedToRefundTransactionsListWasCalled() As Boolean
        Get
            Return _addRefundTransactionBeingProcessedToRefundTransactionsListWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryUsingEmptySortedRefundRepositoryOverridesRefundTransactionBeingProcessedIsNothingToReturnFalseAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListToLogWhetherItHasBeenCalled
    Inherits TestRefundRepositoryUsingEmptySortedRefundRepositoryOverridesRefundTransactionBeingProcessedIsNothingToReturnTrueAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListToLogWhetherItHasBeenCalled


    Friend Overrides Function RefundTransactionBeingProcessedIsNothing() As Boolean

        Return False
    End Function
End Class

Friend Class TestRefundRepositoryOverridesInitialiseRefundTranToLogWhetherItHasBeenCalledUsingEmptySortedRefundRepository
    Inherits TestRefundRepositoryOverridesInitialiseFilteredRefundRepositoryFromSortedRefundRepositoryToSetSortedDataToEmptyDataTable

    Private _initialiseRefundTranWasCalled As Boolean

    Public Sub New()

        MyBase.New()
        InitialiseFilteredRefundRepositoryFromSortedRefundRepository()
    End Sub

    Friend Overrides Sub InitialiseRefundTran()

        _initialiseRefundTranWasCalled = True
    End Sub

    Public ReadOnly Property InitialiseRefundTranWasCalled() As Boolean
        Get
            Return _initialiseRefundTranWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesProcessSortedAndFilteredRefundRepositoryToLogWhetherItHasBeenCalledAndOverridesSortedRefundRepositoryHasDataToReturnFalse
    Inherits TestRefundRepository

    Private _ProcessSortedAndFilteredRefundRepositoryWasCalled As Boolean = False

    Friend Overrides Sub ProcessSortedAndFilteredRefundRepository()

        _ProcessSortedAndFilteredRefundRepositoryWasCalled = True
    End Sub

    Friend Overrides Function SortedRefundRepositoryHasData() As Boolean

        Return False
    End Function

    Public ReadOnly Property ProcessSortedAndFilteredRefundRepositoryWasCalled() As Boolean
        Get
            Return _ProcessSortedAndFilteredRefundRepositoryWasCalled
        End Get
    End Property

End Class

Friend Class TestRefundRepositoryOverridesProcessSortedAndFilteredRefundRepositoryToLogWhetherItHasBeenCalledAndOverridesSortedAndFilteredRefundRepositoryHasDataToReturnTrueAndOverridesSortedRefundRepositoryHasDataToReturnTrue
    Inherits TestRefundRepository

    Private _ProcessSortedAndFilteredRefundRepositoryWasCalled As Boolean = False

    Friend Overrides Sub ProcessSortedAndFilteredRefundRepository()

        _ProcessSortedAndFilteredRefundRepositoryWasCalled = True
    End Sub

    Friend Overrides Function SortedRefundRepositoryHasData() As Boolean

        Return True
    End Function

    Friend Overrides Function SortedAndFilteredRefundRepositoryHasData() As Boolean

        Return True
    End Function

    Public ReadOnly Property ProcessSortedAndFilteredRefundRepositoryWasCalled() As Boolean
        Get
            Return _ProcessSortedAndFilteredRefundRepositoryWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesProcessSortedAndFilteredRefundRepositoryToLogWhetherItHasBeenCalled
    Inherits TestRefundRepository

    Private _ProcessSortedAndFilteredRefundRepositoryRowsWasCalled As Boolean = False

    Friend Overrides Sub ProcessSortedAndFilteredRefundRepositoryRows()

        _ProcessSortedAndFilteredRefundRepositoryRowsWasCalled = True
    End Sub

    Public Function GetProcessSortedAndFilteredRefundRepositoryRowsWasCalled() As Boolean

        Return _ProcessSortedAndFilteredRefundRepositoryRowsWasCalled
    End Function
End Class

Friend Class TestRefundRepositoryUsingRefundTransactionBeingProcessedSetToNothing
    Inherits TestRefundRepository

    Public Sub New()

        MyBase.New()
        _refundTransactionBeingProcessed = Nothing
    End Sub
End Class

Friend Class TestRefundRepositoryUsingRefundTransactionBeingProcessedSetToNewRefundTransaction
    Inherits TestRefundRepository

    Public Sub New()

        MyBase.New()
        _refundTransactionBeingProcessed = New RefundTransaction
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToLogWhetherItHasBeenCalled
    Inherits TestRefundRepository

    Private _comparableRefundTransactionWithRefundRepositoryRowAreTheSameWasCalled As Boolean

    Friend Overrides Function TransactionAndRowDataAreComparable(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        Return True
    End Function

    Friend Overrides Function ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        _comparableRefundTransactionWithRefundRepositoryRowAreTheSameWasCalled = True
    End Function

    Public ReadOnly Property ComparableRefundTransactionWithRefundRepositoryRowAreTheSameWasCalled() As Boolean
        Get
            Return _comparableRefundTransactionWithRefundRepositoryRowAreTheSameWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnFalseAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToLogWhetherItHasBeenCalled
    Inherits TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToLogWhetherItHasBeenCalled


    Friend Overrides Function TransactionAndRowDataAreComparable(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        Return False
    End Function
End Class

Friend Class TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToReturnTrue
    Inherits TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToLogWhetherItHasBeenCalled

    Friend Overrides Function ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        Return True
    End Function
End Class

Friend Class TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToReturnFalse
    Inherits TestRefundRepositoryOverridesTransactionAndRowDataAreComparableToReturnTrueAndOverridesComparableRefundTransactionWithRefundRepositoryRowAreTheSameToLogWhetherItHasBeenCalled

    Friend Overrides Function ComparableRefundTransactionWithRefundRepositoryRowAreTheSame(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        Return False
    End Function
End Class

Friend Class TestRefundRepositoryCreateTransactionAndRowDataAreComparableTestRows
    Inherits TestRefundRepository

    Public Function CreateTestRowWithoutTransactionDateField() As DataRow
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        CreateFrom.Columns.Remove(Me._fieldName_TranDate)

        Return CreateFrom.NewRow
    End Function

    Public Function CreateTestRowWithoutTransactionTillField() As DataRow
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        CreateFrom.Columns.Remove(Me._fieldName_TranTill)

        Return CreateFrom.NewRow
    End Function

    Public Function CreateTestRowWithoutTransactionNumberField() As DataRow
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        CreateFrom.Columns.Remove(Me._fieldName_TranNumber)

        Return CreateFrom.NewRow
    End Function

    Public Function CreateTestRowWithTransactionDateFieldAndTransactionTillFieldAnTransactionNumberField() As DataRow
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        Return CreateFrom.NewRow
    End Function
End Class

Friend Class TestRefundRepositoryCreateComparableRefundTransactionWithRefundRepositoryRowAreTheSameTestRows
    Inherits TestRefundRepository

    Private _testDataRow As DataRow

    Public Sub New()

        MyBase.New()
        CreateTestRow()
    End Sub

    Public Function TestTransactionWithDifferentTransactionDate() As RefundTransaction
        Dim TestRefundTransaction As New RefundTransaction

        With TestRefundTransaction
            .TransactionDate = DateAdd("d", -1, _testDataRow.Item(_fieldName_TranDate))
            .Till = CStr(_testDataRow.Item(_fieldName_TranTill))
            .TransactionNumber = CStr(_testDataRow.Item(_fieldName_TranNumber))
        End With

        Return TestRefundTransaction
    End Function

    Public Function TestTransactionWithDifferentTransactionTill() As RefundTransaction
        Dim TestRefundTransaction As New RefundTransaction

        With TestRefundTransaction
            .TransactionDate = CDate(_testDataRow.Item(_fieldName_TranDate))
            .Till = CStr(_testDataRow.Item(_fieldName_TranTill)) & "1"
            .TransactionNumber = CStr(_testDataRow.Item(_fieldName_TranNumber))
        End With

        Return TestRefundTransaction
    End Function

    Public Function TestTransactionWithDifferentTransactionNumber() As RefundTransaction
        Dim TestRefundTransaction As New RefundTransaction

        With TestRefundTransaction
            .TransactionDate = CDate(_testDataRow.Item(_fieldName_TranDate))
            .Till = CStr(_testDataRow.Item(_fieldName_TranTill))
            .TransactionNumber = CStr(_testDataRow.Item(_fieldName_TranNumber)) & "1"
        End With

        Return TestRefundTransaction
    End Function

    Public Function TestTransactionWithSameFieldValues() As RefundTransaction
        Dim TestRefundTransaction As New RefundTransaction

        With TestRefundTransaction
            .TransactionDate = CDate(_testDataRow.Item(_fieldName_TranDate))
            .Till = CStr(_testDataRow.Item(_fieldName_TranTill))
            .TransactionNumber = CStr(_testDataRow.Item(_fieldName_TranNumber))
        End With

        Return TestRefundTransaction
    End Function

    Public ReadOnly Property TestDataRow() As DataRow
        Get
            Return _testDataRow
        End Get
    End Property

    Private Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        _testDataRow = CreateFrom.NewRow

        With _testDataRow
            .Item(_fieldName_TranDate) = #4/2/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherItHasBeenCalledAndProvidesTestDataRows
    Inherits TestRefundRepository

    Private _AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled As Boolean
    Private _testDataRow As DataRow

    Public Sub New()

        MyBase.New()
        CreateTestRow()
    End Sub

    Friend Overrides Function RowSameAsTransaction(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        Return True
    End Function

    Friend Overrides Sub AddRefundLineToRefundTransactionBeingProcessedsLines(ByVal RefundRow As System.Data.DataRow)

        _AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled = True
    End Sub

    Public ReadOnly Property AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled() As Boolean
        Get
            Return _AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled
        End Get
    End Property

    Public ReadOnly Property TestRefundRow() As DataRow
        Get
            Return _testDataRow
        End Get
    End Property

    Private Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        _testDataRow = CreateFrom.NewRow

        With _testDataRow
            .Item(_fieldName_TranDate) = #4/2/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesRowSameAsTransactionToReturnFalseAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherItHasBeenCalledAndProvidesTestDataRows
    Inherits TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherItHasBeenCalledAndProvidesTestDataRows

    Friend Overrides Function RowSameAsTransaction(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        Return False
    End Function
End Class

Friend Class TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherRefundRowIsPassedIn
    Inherits TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherItHasBeenCalledAndProvidesTestDataRows

    Private _AddRefundLineToRefundTransactionBeingProcessedsLinesPassedInRefundRow As DataRow

    Friend Overrides Sub AddRefundLineToRefundTransactionBeingProcessedsLines(ByVal RefundRow As System.Data.DataRow)

        _AddRefundLineToRefundTransactionBeingProcessedsLinesPassedInRefundRow = RefundRow
    End Sub

    Public ReadOnly Property AddRefundLineToRefundTransactionBeingProcessedsLinesPassedInRefundRow() As DataRow
        Get
            Return _AddRefundLineToRefundTransactionBeingProcessedsLinesPassedInRefundRow
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalledAndProvidesTestDataRows
    Inherits TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogWhetherItHasBeenCalledAndProvidesTestDataRows

    Private _addRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled As Boolean

    Friend Overrides Sub AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction(ByVal RefundRow As System.Data.DataRow)

        _addRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled = True
    End Sub

    Public ReadOnly Property AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled() As Boolean
        Get
            Return _addRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesRowSameAsTransactionToReturnFalseAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalledAndProvidesTestDataRows
    Inherits TestRefundRepositoryOverridesRowSameAsTransactionToReturnTrueAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalledAndProvidesTestDataRows

    Friend Overrides Function RowSameAsTransaction(ByVal ToCompare As RefundCountSelector.RefundTransaction, ByVal CompareWith As System.Data.DataRow) As Boolean

        Return False
    End Function
End Class

Friend Class TestRefundRepositoryOverridesRowSameAsTransactionToReturnFalseAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherRefundRowIsPassedIn
    Inherits TestRefundRepositoryOverridesRowSameAsTransactionToReturnFalseAndOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItHasBeenCalledAndProvidesTestDataRows

    Private _addRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionPassedInRefundRow As DataRow

    Friend Overrides Sub AddRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransaction(ByVal RefundRow As System.Data.DataRow)

        _addRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionPassedInRefundRow = RefundRow
    End Sub

    Public ReadOnly Property AddRefundLineToRefundTransactionBeingProcessedsLinesAndSetRefundTransactionBeingProcessedToNewRefundTransactionPassedInRefundRow() As DataRow
        Get
            Return _addRefundTransactionBeingProcessedToRefundTransactionsListAndSetRefundTransactionBeingProcessedToNewRefundTransactionPassedInRefundRow
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListToLogWhetherItWasCalledAndProvidesTestDataRow
    Inherits RefundRepository

    Private _addRefundTransactionBeingProcessedToRefundTransactionsListWasCalled As Boolean
    Private _testDataRow As DataRow

    Public Sub New()

        MyBase.New()
        CreateTestRow()
    End Sub

    Friend Overrides Sub AddRefundTransactionBeingProcessedToRefundTransactionsList()

        _addRefundTransactionBeingProcessedToRefundTransactionsListWasCalled = True
    End Sub

    Public ReadOnly Property AddRefundTransactionBeingProcessedToRefundTransactionsListWasCalled() As Boolean
        Get
            Return _addRefundTransactionBeingProcessedToRefundTransactionsListWasCalled
        End Get
    End Property

    Public ReadOnly Property TestRefundRow() As DataRow
        Get
            Return _testDataRow
        End Get
    End Property

    Protected Overridable Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = #4/2/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesSetRefundTransactionBeingProcessedToNewRefundTransactionToLogWhetherItWasCalledAndExposeTheRefundRowParameterUsedAndProvidesTestDataRow
    Inherits TestRefundRepositoryOverridesAddRefundTransactionBeingProcessedToRefundTransactionsListToLogWhetherItWasCalledAndProvidesTestDataRow

    Private _setRefundTransactionBeingProcessedToNewRefundTransactionWasCalled As Boolean
    Private _setRefundTransactionBeingProcessedToNewRefundTransactionRefundRowParameter As DataRow

    Friend Overrides Sub SetRefundTransactionBeingProcessedToNewRefundTransaction(ByVal RefundRow As System.Data.DataRow)

        _setRefundTransactionBeingProcessedToNewRefundTransactionWasCalled = True
        _setRefundTransactionBeingProcessedToNewRefundTransactionRefundRowParameter = RefundRow
    End Sub

    Public ReadOnly Property SetRefundTransactionBeingProcessedToNewRefundTransactionWasCalled() As Boolean
        Get
            Return _setRefundTransactionBeingProcessedToNewRefundTransactionWasCalled
        End Get
    End Property

    Public ReadOnly Property SetRefundTransactionBeingProcessedToNewRefundTransactionRefundRowParameter() As DataRow
        Get
            Return _setRefundTransactionBeingProcessedToNewRefundTransactionRefundRowParameter
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesNewRefundTransactionToLogWhetehrItWasCalledAndExposeTheRefundRowParameterUsedAndTheNewRefundTransactionAndProvideTestDataRow
    Inherits TestRefundRepository

    Private _newRefundTransactionWasCalled As Boolean
    Private _newRefundTransactionRefundRowParameter As DataRow
    Private _testDataRow As DataRow
    Private _newRefundTransaction As RefundTransaction

    Public Sub New()

        MyBase.New()
        CreateTestRow()
    End Sub

    Friend Overrides Function CreateRefundTransactionBeingProcessedFromRefundRow(ByVal RefundRow As System.Data.DataRow) As RefundCountSelector.RefundTransaction

        _newRefundTransactionRefundRowParameter = RefundRow
        _newRefundTransaction = MyBase.CreateRefundTransactionBeingProcessedFromRefundRow(RefundRow)
        _newRefundTransactionWasCalled = True
        Return _newRefundTransaction
    End Function

    Public ReadOnly Property NewRefundTransactionRefundRowParameter() As DataRow
        Get
            Return _newRefundTransactionRefundRowParameter
        End Get
    End Property

    Public ReadOnly Property NewRefundTransactionWasCalled() As Boolean
        Get
            Return _newRefundTransactionWasCalled
        End Get
    End Property

    Public ReadOnly Property CreatedRefundTransaction() As RefundTransaction
        Get
            Return _newRefundTransaction
        End Get
    End Property

    Public ReadOnly Property TestRefundRow() As DataRow
        Get
            Return _testDataRow
        End Get
    End Property

    Protected Overridable Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = #4/2/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesNewRefundLineToLogWhetherItHasBeenCalledAndExposeTheRefundRwoParameterAndExposeTheCreatedRefundLineAndProvideTestRefundRow
    Inherits TestRefundRepository

    Private _newRefundLineWasCalled As Boolean
    Private _newRefundLineRefundRowParameter As DataRow
    Private _testDataRow As DataRow
    Private _newRefundLine As RefundLine

    Public Sub New()

        MyBase.New()
        CreateTestRow()
        _refundTransactionBeingProcessed = New RefundTransaction
    End Sub

    Friend Overrides Function NewRefundLine(ByVal RefundRow As System.Data.DataRow) As RefundCountSelector.RefundLine

        _newRefundLineRefundRowParameter = RefundRow
        _newRefundLine = MyBase.NewRefundLine(RefundRow)
        _newRefundLineWasCalled = True
        Return _newRefundLine
    End Function

    Public ReadOnly Property NewRefundLineRefundRowParameter() As DataRow
        Get
            Return _newRefundLineRefundRowParameter
        End Get
    End Property

    Public ReadOnly Property NewRefundLineWasCalled() As Boolean
        Get
            Return _newRefundLineWasCalled
        End Get
    End Property

    Public ReadOnly Property CreatedRefundLine() As RefundLine
        Get
            Return _newRefundLine
        End Get
    End Property

    Public ReadOnly Property TestRefundRow() As DataRow
        Get
            Return _testDataRow
        End Get
    End Property

    Protected Overridable Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = #4/2/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
            .Item(_fieldName_TranLineNumber) = 1
            .Item(_fieldName_SkuNumber) = "111111"
            .Item(_fieldName_SkuQuantity) = -1
            .Item(_fieldName_SkuPrice) = 11.11
            .Item(_fieldName_OriginalTranNumber) = "0000"
            .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesNewRefundLineToReturnNothingAndProvideTestRefundRow
    Inherits TestRefundRepository
    Private _testDataRow As DataRow

    Public Sub New()

        MyBase.New()
        CreateTestRow()
        _refundTransactionBeingProcessed = New RefundTransaction
    End Sub

    Friend Overrides Function NewRefundLine(ByVal RefundRow As System.Data.DataRow) As RefundCountSelector.RefundLine

        Return Nothing
    End Function

    Public ReadOnly Property TestRefundRow() As DataRow
        Get
            Return _testDataRow
        End Get
    End Property

    Protected Overridable Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = #4/2/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
            .Item(_fieldName_TranLineNumber) = 1
            .Item(_fieldName_SkuNumber) = "111111"
            .Item(_fieldName_SkuQuantity) = -1
            .Item(_fieldName_SkuPrice) = 11.11
            .Item(_fieldName_OriginalTranNumber) = "0000"
            .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesNewRefundLineToReturnTestRefundLine
    Inherits TestRefundRepository
    Private _testRefundLine As RefundLine

    Public Sub New()

        MyBase.New()
        CreateTestRefundLine()
        _refundTransactionBeingProcessed = New RefundTransaction
    End Sub

    Friend Overrides Function NewRefundLine(ByVal RefundRow As System.Data.DataRow) As RefundCountSelector.RefundLine

        Return _testRefundLine
    End Function

    Public ReadOnly Property TestRefundLine() As RefundLine
        Get
            Return _testRefundLine
        End Get
    End Property

    Protected Overridable Sub CreateTestRefundLine()

        _testRefundLine = New RefundLine
        With _testRefundLine
            .LineNumber = 1
            .SkuNumber = "111111"
            .Quantity = -1
            .ItemPrice = CDec(11.11)
            .OriginalTransactionNumber = "0000"
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRow
    Inherits TestRefundRepository

    Friend _testDataRow As DataRow

    Public Sub New()

        MyBase.New()
        CreateTestRow()
    End Sub

    Public ReadOnly Property TestRefundRow() As DataRow
        Get
            Return _testDataRow
        End Get
    End Property

    Protected Overridable Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = #4/2/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
            .Item(_fieldName_TranLineNumber) = 1
            .Item(_fieldName_SkuNumber) = "111111"
            .Item(_fieldName_SkuQuantity) = -1
            .Item(_fieldName_SkuPrice) = 11.11
            .Item(_fieldName_OriginalTranNumber) = "0000"
            .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowOfAllDBNulls
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Protected Overrides Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = DBNull.Value
            .Item(_fieldName_TranTill) = DBNull.Value
            .Item(_fieldName_TranNumber) = DBNull.Value
            .Item(_fieldName_TranLineNumber) = DBNull.Value
            .Item(_fieldName_SkuNumber) = DBNull.Value
            .Item(_fieldName_SkuQuantity) = DBNull.Value
            .Item(_fieldName_SkuPrice) = DBNull.Value
            .Item(_fieldName_OriginalTranNumber) = DBNull.Value
            .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = DBNull.Value
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryOverridesSetRefundTransactionBeingProcessedToBlankRefundTransactionToLogWhetherItHasBeenCalledAndProvideTestRefundRow
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Private _setRefundTransactionBeingProcessedToBlankRefundTransactionWasCalled As Boolean

    Friend Overrides Sub SetRefundTransactionBeingProcessedToBlankRefundTransaction()

        _setRefundTransactionBeingProcessedToBlankRefundTransactionWasCalled = True
    End Sub

    Public ReadOnly Property SetRefundTransactionBeingProcessedToBlankRefundTransactionWasCalled() As Boolean
        Get
            Return _setRefundTransactionBeingProcessedToBlankRefundTransactionWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesRefundRowContainsRefundTransactionFieldsToLogWhetherItHasBeenCalledAndProvideTestRefundRow
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Private _refundRowContainsRefundTransactionFieldsWasCalled As Boolean


    Friend Overrides Function RefundRowContainsRefundTransactionFields(ByVal RefundRow As DataRow) As Boolean

        _refundRowContainsRefundTransactionFieldsWasCalled = True
    End Function

    Public ReadOnly Property RefundRowContainsRefundTransactionFieldsWasCalled() As Boolean
        Get
            Return _refundRowContainsRefundTransactionFieldsWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesRefundRowContainsRefundTransactionFieldsToExposeTheRefundRwoParameterAndProvideTestRefundRow
    Inherits TestRefundRepositoryOverridesRefundRowContainsRefundTransactionFieldsToLogWhetherItHasBeenCalledAndProvideTestRefundRow

    Private _refundRrowParameter As DataRow

    Friend Overrides Function RefundRowContainsRefundTransactionFields(ByVal RefundRow As DataRow) As Boolean

        MyBase.RefundRowContainsRefundTransactionFields(RefundRow)
        _refundRrowParameter = RefundRow
    End Function

    Public ReadOnly Property RefundRowContainsRefundTransactionFieldsRefundRowParameter() As DataRow
        Get
            Return _refundRrowParameter
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesPopulateRefundTransactionBeingProcessedFromRefundRowToLogWhetherItHasBeenCalledAndOverridesSetRefundTransactionBeingProcessedToBlankRefundTransactionToSetItToNothingAndProvideTestRefundRow
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Private _populateRefundTransactionBeingProcessedFromRefundRowWasCalled As Boolean

    Friend Overrides Sub PopulateRefundTransactionBeingProcessedFromRefundRow(ByVal RefundRow As System.Data.DataRow)

        _populateRefundTransactionBeingProcessedFromRefundRowWasCalled = True
    End Sub

    Friend Overrides Sub SetRefundTransactionBeingProcessedToBlankRefundTransaction()

        Me._refundTransactionBeingProcessed = Nothing
    End Sub

    Public ReadOnly Property PopulateRefundTransactionBeingProcessedFromRefundRowWasCalled() As Boolean
        Get
            Return _populateRefundTransactionBeingProcessedFromRefundRowWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesPopulateRefundTransactionBeingProcessedFromRefundRowToLogWhetherItHasBeenCalledAndOverridesRefundRowContainsRefundTransactionFieldsToReturnFalseAndProvideTestRefundRow
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Private _populateRefundTransactionBeingProcessedFromRefundRowWasCalled As Boolean
    Private _refundRowContainsRefundTransactionFieldsRefundRowParameter As DataRow

    Friend Overrides Sub PopulateRefundTransactionBeingProcessedFromRefundRow(ByVal RefundRow As System.Data.DataRow)

        _populateRefundTransactionBeingProcessedFromRefundRowWasCalled = True
    End Sub

    Friend Overrides Sub SetRefundTransactionBeingProcessedToBlankRefundTransaction()

        Me._refundTransactionBeingProcessed = New RefundTransaction
    End Sub

    Friend Overrides Function RefundRowContainsRefundTransactionFields(ByVal RefundRow As System.Data.DataRow) As Boolean

        _refundRowContainsRefundTransactionFieldsRefundRowParameter = RefundRow
        Return False
    End Function

    Public ReadOnly Property PopulateRefundTransactionBeingProcessedFromRefundRowWasCalled() As Boolean
        Get
            Return _populateRefundTransactionBeingProcessedFromRefundRowWasCalled
        End Get
    End Property

    Public ReadOnly Property RefundRowContainsRefundTransactionFieldsRefundRowParameter() As DataRow
        Get
            Return _refundRowContainsRefundTransactionFieldsRefundRowParameter
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesRefundRowContainsRefundTransactionFieldsToReturnTrueAndOverridesPopulateRefundTransactionBeingProcessedFromRefundRowToLogWhetherItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Private _populateRefundTransactionBeingProcessedFromRefundRowWasCalled As Boolean
    Private _populateRefundTransactionBeingProcessedFromRefundRowRefundRowParameter As DataRow

    Friend Overrides Sub PopulateRefundTransactionBeingProcessedFromRefundRow(ByVal RefundRow As System.Data.DataRow)

        _populateRefundTransactionBeingProcessedFromRefundRowRefundRowParameter = RefundRow
        _populateRefundTransactionBeingProcessedFromRefundRowWasCalled = True
    End Sub

    Friend Overrides Function RefundRowContainsRefundTransactionFields(ByVal RefundRow As System.Data.DataRow) As Boolean

        Return True
    End Function

    Public ReadOnly Property PopulateRefundTransactionBeingProcessedFromRefundRowRefundRowParameter() As DataRow
        Get
            Return _populateRefundTransactionBeingProcessedFromRefundRowRefundRowParameter
        End Get
    End Property

    Public ReadOnly Property PopulateRefundTransactionBeingProcessedFromRefundRowWasCalled() As Boolean
        Get
            Return _populateRefundTransactionBeingProcessedFromRefundRowWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesCopyRefundRowFieldValuesToRefundTransactionBeingProcessedToLogItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Private _copyRefundRowFieldValuesToRefundTransactionBeingProcessedWasCalled As Boolean
    Private _copyRefundRowFieldValuesToRefundTransactionBeingProcessedRefundRowParameter As DataRow

    Friend Overrides Sub CopyRefundRowFieldValuesToRefundTransactionBeingProcessed(ByVal RefundRow As System.Data.DataRow)

        _copyRefundRowFieldValuesToRefundTransactionBeingProcessedWasCalled = True
        _copyRefundRowFieldValuesToRefundTransactionBeingProcessedRefundRowParameter = RefundRow
    End Sub

    Public ReadOnly Property CopyRefundRowFieldValuesToRefundTransactionBeingProcessedRefundRowParameter() As DataRow
        Get
            Return _copyRefundRowFieldValuesToRefundTransactionBeingProcessedRefundRowParameter
        End Get
    End Property

    Public ReadOnly Property CopyRefundRowFieldValuesToRefundTransactionBeingProcessedWasCalled() As Boolean
        Get
            Return _copyRefundRowFieldValuesToRefundTransactionBeingProcessedWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryOverridesAddRefundLineToRefundTransactionBeingProcessedsLinesToLogItHasBeenCalledAndExposeRefundRowParameterAndProvideTestRefundRow
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Private _AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled As Boolean
    Private _AddRefundLineToRefundTransactionBeingProcessedsLinesRefundRowParameter As DataRow

    Friend Overrides Sub AddRefundLineToRefundTransactionBeingProcessedsLines(ByVal RefundRow As System.Data.DataRow)

        _AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled = True
        _AddRefundLineToRefundTransactionBeingProcessedsLinesRefundRowParameter = RefundRow
    End Sub

    Public ReadOnly Property AddRefundLineToRefundTransactionBeingProcessedsLinesRefundRowParameter() As DataRow
        Get
            Return _AddRefundLineToRefundTransactionBeingProcessedsLinesRefundRowParameter
        End Get
    End Property

    Public ReadOnly Property AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled() As Boolean
        Get
            Return _AddRefundLineToRefundTransactionBeingProcessedsLinesWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Overridable ReadOnly Property TestRefundRowsTransactionDate() As DateTime
        Get
            Return CDate(Me.TestRefundRow(Me._fieldName_TranDate))
        End Get
    End Property

    Overridable ReadOnly Property TestRefundRowsTransactionTill() As String
        Get
            Return CStr(Me.TestRefundRow(Me._fieldName_TranTill))
        End Get
    End Property

    Overridable ReadOnly Property TestRefundRowsTransactionNumber() As String
        Get
            Return CStr(Me.TestRefundRow(Me._fieldName_TranNumber))
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
    Inherits TestRefundRepositoryProvidesTestRefundRowOfAllDBNulls

    Overridable ReadOnly Property TestRefundRowsTransactionDate() As DateTime
        Get
            Return CDate(Me.TestRefundRow(Me._fieldName_TranDate))
        End Get
    End Property

    Overridable ReadOnly Property TestRefundRowsTransactionTill() As String
        Get
            Return CStr(Me.TestRefundRow(Me._fieldName_TranTill))
        End Get
    End Property

    Overridable ReadOnly Property TestRefundRowsTransactionNumber() As String
        Get
            Return CStr(Me.TestRefundRow(Me._fieldName_TranNumber))
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowWithoutTransactionDateField
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Protected Overrides Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        CreateFrom.Columns.Remove(_fieldName_TranDate)
        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
            .Item(_fieldName_TranLineNumber) = 1
            .Item(_fieldName_SkuNumber) = "111111"
            .Item(_fieldName_SkuQuantity) = -1
            .Item(_fieldName_SkuPrice) = 11.11
            .Item(_fieldName_OriginalTranNumber) = "0000"
            .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowWithoutTransactionTillField
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Protected Overrides Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        CreateFrom.Columns.Remove(_fieldName_TranTill)
        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = #4/1/2012#
            .Item(_fieldName_TranNumber) = "0002"
            .Item(_fieldName_TranLineNumber) = 1
            .Item(_fieldName_SkuNumber) = "111111"
            .Item(_fieldName_SkuQuantity) = -1
            .Item(_fieldName_SkuPrice) = 11.11
            .Item(_fieldName_OriginalTranNumber) = "0000"
            .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowWithoutTransactionNumberField
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Protected Overrides Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        CreateFrom.Columns.Remove(_fieldName_TranNumber)
        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = #4/1/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranLineNumber) = 1
            .Item(_fieldName_SkuNumber) = "111111"
            .Item(_fieldName_SkuQuantity) = -1
            .Item(_fieldName_SkuPrice) = 11.11
            .Item(_fieldName_OriginalTranNumber) = "0000"
            .Item(_fieldName_NumberCashAndGiftTokenRefundPayments) = 0
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowWithOnlyTransactionDateFieldAndTransactionTillFieldAndTransactionNumberField
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Protected Overrides Sub CreateTestRow()
        Dim CreateFrom As DataTable = Me.GetInitialisedDataTable()

        CreateFrom.Columns.Remove(_fieldName_TranLineNumber)
        CreateFrom.Columns.Remove(_fieldName_SkuNumber)
        CreateFrom.Columns.Remove(_fieldName_SkuQuantity)
        CreateFrom.Columns.Remove(_fieldName_SkuPrice)
        CreateFrom.Columns.Remove(_fieldName_OriginalTranNumber)
        CreateFrom.Columns.Remove(_fieldName_NumberCashAndGiftTokenRefundPayments)
        _testDataRow = CreateFrom.NewRow
        With _testDataRow
            .Item(_fieldName_TranDate) = #4/1/2012#
            .Item(_fieldName_TranTill) = "02"
            .Item(_fieldName_TranNumber) = "0002"
        End With
    End Sub
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowAndExposesTestRefundRowsRefundLineFields
    Inherits TestRefundRepositoryProvidesTestRefundRow

    Public ReadOnly Property TestRefundRowsTransactionLineNumber() As Int16
        Get
            Return CShort(TestRefundRow(_fieldName_TranLineNumber))
        End Get
    End Property

    Public ReadOnly Property TestRefundRowsSkuNumber() As String
        Get
            Return CStr(TestRefundRow(_fieldName_SkuNumber))
        End Get
    End Property

    Public ReadOnly Property TestRefundRowsSkuQuantity() As Integer
        Get
            Return CInt(TestRefundRow(_fieldName_SkuQuantity))
        End Get
    End Property

    Public ReadOnly Property TestRefundRowsSkuPrice() As Decimal
        Get
            Return CDec(TestRefundRow(_fieldName_SkuPrice))
        End Get
    End Property

    Public ReadOnly Property TestRefundRowsOriginalTransactionNumber() As String
        Get
            Return CStr(TestRefundRow(_fieldName_OriginalTranNumber))
        End Get
    End Property
End Class

Friend Class TestRefundRepositoryProvidesTestRefundRowOfAllDBNullsAndExposesTestRefundRowsRefundLineFields
    Inherits TestRefundRepositoryProvidesTestRefundRowOfAllDBNulls

    Public ReadOnly Property TestRefundRowsTransactionLineNumber() As Integer
        Get
            If TestRefundRow(_fieldName_TranLineNumber) Is DBNull.Value Then
                Return Nothing
            Else
                Return CInt(TestRefundRow(_fieldName_TranLineNumber))
            End If
        End Get
    End Property

    Public ReadOnly Property TestRefundRowsSkuNumber() As String
        Get
            If TestRefundRow(_fieldName_SkuNumber) Is DBNull.Value Then
                Return Nothing
            Else
                Return CStr(TestRefundRow(_fieldName_SkuNumber))
            End If
        End Get
    End Property

    Public ReadOnly Property TestRefundRowsSkuQuantity() As Integer
        Get
            If TestRefundRow(_fieldName_SkuQuantity) Is DBNull.Value Then
                Return Nothing
            Else
                Return CInt(TestRefundRow(_fieldName_SkuQuantity))
            End If
        End Get
    End Property

    Public ReadOnly Property TestRefundRowsSkuPrice() As Decimal
        Get
            If TestRefundRow(_fieldName_SkuPrice) Is DBNull.Value Then
                Return Nothing
            Else
                Return CDec(TestRefundRow(_fieldName_SkuPrice))
            End If
        End Get
    End Property

    Public ReadOnly Property TestRefundRowsOriginalTransactionNumber() As String
        Get
            If TestRefundRow(_fieldName_OriginalTranNumber) Is DBNull.Value Then
                Return Nothing
            Else
                Return CStr(TestRefundRow(_fieldName_OriginalTranNumber))
            End If
        End Get
    End Property

End Class

Friend Class RefundRepositoryProvidesColumnNamesArray
    Inherits TestRefundRepository

    Friend Function GetColumnNames() As String()
        Dim ColumnNames(8) As String

        ColumnNames(0) = Me._fieldName_TranDate
        ColumnNames(1) = Me._fieldName_TranTill
        ColumnNames(2) = Me._fieldName_TranNumber
        ColumnNames(3) = Me._fieldName_SkuNumber
        ColumnNames(4) = Me._fieldName_SkuPrice
        ColumnNames(5) = Me._fieldName_SkuQuantity
        ColumnNames(6) = Me._fieldName_TranLineNumber
        ColumnNames(7) = Me._fieldName_OriginalTranNumber
        ColumnNames(8) = Me._fieldName_NumberCashAndGiftTokenRefundPayments

        Return ColumnNames
    End Function
End Class