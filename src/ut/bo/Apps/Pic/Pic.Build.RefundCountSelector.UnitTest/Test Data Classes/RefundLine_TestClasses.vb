﻿Friend Class TestRefundLine
    Inherits RefundLine

    Public Sub New(ByVal Total As Decimal)

        SkuNumber = "TestRefundLine"
        ItemPrice = Total
        Quantity = 1
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Total10_SkuNumberNotThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "NotThisOne"
        ItemPrice = 5D
        Quantity = 2
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Total20_SkuNumberThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 2.5D
        Quantity = 8
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Total30_SkuNumberThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 7.5D
        Quantity = 4
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Total10_SkuNumberThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 2.5D
        Quantity = 4
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Total19_99_SkuNumberNotThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "NotThisOne"
        ItemPrice = 19.99D
        Quantity = 1
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Price3_33_And_Quantity3_SkuNumberThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 3.33D
        Quantity = 3
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Price2_50_And_Quantity2_SkuNumberNotThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "NotThisOne"
        ItemPrice = 2.5D
        Quantity = 2
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Price3_And_Quantity4_SkuNumberThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 3D
        Quantity = 4
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub

End Class

Friend Class TestRefundLine_Price1_And_Quantity4_SkuNumberStillNotThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "StillNotThisOne"
        ItemPrice = 1D
        Quantity = 4
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLine_Price4_And_Quantity2_SkuNumberThisOne
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 4D
        Quantity = 2
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLineTotalIs5_01
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 5.01D
        Quantity = 1
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLineTotalIs5Exactly
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 5D
        Quantity = 1
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class

Friend Class TestRefundLineTotalIs4_99
    Inherits RefundLine

    Public Sub New()

        SkuNumber = "ThisOne"
        ItemPrice = 4.99D
        Quantity = 1
        LineNumber = 1
        OriginalTransactionNumber = "0000"
    End Sub
End Class
