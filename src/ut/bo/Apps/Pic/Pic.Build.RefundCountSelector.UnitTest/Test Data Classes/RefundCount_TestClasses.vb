﻿Friend Class TestRefundCount
    Inherits RefundCount

    Public Sub New()

        MyBase.New(#4/1/2012#, #4/1/2012#)
    End Sub
End Class

Friend Class TestRefundCountOverridesGetLimitsToReturnTestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77
    Inherits RefundCount

    Public Sub New()

        MyBase.New()
    End Sub

    Public Sub New(ByVal StartDate As DateTime, ByVal EndDate As DateTime)

        MyBase.New(StartDate, EndDate)
    End Sub

    Friend Overrides Function GetLimits() As RefundCountSelector.RefundRepository

        Return New TestRefundRepositoryOverridesGetLineLimitToReturn33_66AndSkuQuantityLimitToReturn22AndOverridesGetTransactionLimitToReturn44_77
    End Function
End Class

Friend Class TestRefundCountThreeTransactionsEachWithThreeRefundLinesOnlyOneOverLimit
    Inherits TestRefundCount

    Private _addAllTransactionLinesToSkuListCount As Integer

    Public Sub New()

        MyBase.New()
        TransactionLimit = 31
        AddTestTransactions()
        _addAllTransactionLinesToSkuListCount = 0
    End Sub

    Private Sub AddTestTransactions()

        With Transactions
            .Add(New TestRefundTransactionTotal30With3TestRefundLine_Total10_SkuNumberNotThisOneLines)
            .Add(New TestRefundTransactionTotal60With3TestRefundLine_Total20_SkuNumberThisOneLines)
            .Add(New TestRefundTransactionTotal30With3TestRefundLine_Total10_SkuNumberNotThisOneLines)
        End With
    End Sub

    Public ReadOnly Property AddAllTransactionLinesToSkuListCount() As Integer
        Get
            Return _addAllTransactionLinesToSkuListCount
        End Get
    End Property

    Friend Overrides Sub AddAllTransactionLinesToSkuList(ByRef SkuList As System.Collections.Generic.List(Of RefundCountSelector.RefundSku), ByVal refTran As RefundCountSelector.RefundTransaction)

        _addAllTransactionLinesToSkuListCount += 1
    End Sub
End Class

Friend Class TestRefundCountThreeTransactionsNoneOverLimitOneWithThreeRefundLinesOverLimit
    Inherits TestRefundCount

    Private _addTransactionLinesOverLimitToSkuListCalledCount As Integer

    Public Sub New()

        MyBase.New()
        TransactionLimit = 31
        AddTestTransactions()
    End Sub

    Private Sub AddTestTransactions()

        With Transactions
            .Add(New TestRefundTransactionTotal30With3TestRefundLine_Total10_SkuNumberNotThisOneLines)
            .Add(New TestRefundTransactionTotal30With3TestRefundLine_Total30_SkuNumberThisOneLines)
            .Add(New TestRefundTransactionTotal30With3TestRefundLine_Total10_SkuNumberNotThisOneLines)
        End With
    End Sub

    Public ReadOnly Property AddTransactionLinesOverLimitToSkuListCount() As Integer
        Get
            Return _addTransactionLinesOverLimitToSkuListCalledCount
        End Get
    End Property

    Friend Overrides Sub AddTransactionLinesOverLimitToSkuList(ByRef SkuList As System.Collections.Generic.List(Of RefundCountSelector.RefundSku), ByVal refTran As RefundCountSelector.RefundTransaction)

        _addTransactionLinesOverLimitToSkuListCalledCount += 1
    End Sub
End Class

Friend Class TestRefundCountThreeTransactionsNoneOverLimitEachWithOneRefundLineOverLimit
    Inherits TestRefundCount

    Public Sub New()

        MyBase.New()
        AddTestTransactions()
    End Sub

    Private Sub AddTestTransactions()

        With Transactions
            .Add(New TestRefundTransactionTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines)
            .Add(New TestRefundTransactionTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines)
            .Add(New TestRefundTransactionTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines)
        End With
    End Sub
End Class

Friend Class TestRefundCountOverrideGetRefundSkuListReturn1RefundSkuInList
    Inherits TestRefundCount

    Friend Overrides Function GetRefundSkuList() As System.Collections.Generic.List(Of RefundSku)

        GetRefundSkuList = New System.Collections.Generic.List(Of RefundSku)
        GetRefundSkuList.Add(New RefundSku("TestSku", 12.34D))
    End Function
End Class

Friend Class TestRefundCountOverrideGetRefundSkuListWith2DifferentRefundSkus
    Inherits TestRefundCount

    Friend Overrides Function GetRefundSkuList() As System.Collections.Generic.List(Of RefundSku)

        GetRefundSkuList = New System.Collections.Generic.List(Of RefundSku)
        GetRefundSkuList.Add(New RefundSku("TestSku1", 12.34D))
        GetRefundSkuList.Add(New RefundSku("TestSku2", 45.67D))
    End Function
End Class

Friend Class TestRefundCountOverrideGetRefundSkuListWith2PairsOfSameRefundSkusTotalValue58_01And91_34
    Inherits TestRefundCount

    Private _testSku1 As String = "TestSku1"
    Private _testSku2 As String = "TestSku2"

    Friend Overrides Function GetRefundSkuList() As System.Collections.Generic.List(Of RefundSku)

        GetRefundSkuList = New System.Collections.Generic.List(Of RefundSku)
        With GetRefundSkuList
            .Add(New RefundSku(TestSkuIDForValue58_01, 12.34D))
            .Add(New RefundSku(TestSkuIDForValue91_34, 23.45D))
            .Add(New RefundSku(TestSkuIDForValue58_01, 45.67D))
            .Add(New RefundSku(TestSkuIDForValue91_34, 67.89D))
        End With
    End Function

    Public ReadOnly Property TestSkuIDForValue58_01() As String
        Get
            Return _testSku1
        End Get
    End Property

    Public ReadOnly Property TestSkuIDForValue91_34() As String
        Get
            Return _testSku2
        End Get
    End Property
End Class

Friend Class TestRefundCountOverrideGetDistinctSkuValueListWithListOf6DistinctValues
    Inherits TestRefundCount

    Private _testSku1 As String = "First"
    Private _testSku2 As String = "Second"
    Private _testSku3 As String = "Third"
    Private _testSku4 As String = "Fourth"
    Private _testSku5 As String = "Fifth"
    Private _testSku6 As String = "Sixth"

    Friend Overrides Function GetDistinctRefundSkuValueList() As System.Collections.Generic.List(Of RefundSku)

        GetDistinctRefundSkuValueList = New System.Collections.Generic.List(Of RefundSku)
        With GetDistinctRefundSkuValueList
            .Add(New RefundSku(_testSku3, 42.34D))
            .Add(New RefundSku(_testSku1, 63.45D))
            .Add(New RefundSku(_testSku6, 15.67D))
            .Add(New RefundSku(_testSku4, 37.89D))
            .Add(New RefundSku(_testSku2, 51.67D))
            .Add(New RefundSku(_testSku5, 23.89D))
        End With
    End Function

    Public Function GetCorrectAllSkusInOrder() As String

        Return _testSku1 & _testSku2 & _testSku3 & _testSku4 & _testSku5 & _testSku6
    End Function
End Class

Friend Class TestRefundCountOverrideGetDistinctRefundSkuValueReturn1RefundSkuInList
    Inherits TestRefundCount

    Friend Overrides Function GetDistinctRefundSkuValueList() As System.Collections.Generic.List(Of RefundSku)

        GetDistinctRefundSkuValueList = New System.Collections.Generic.List(Of RefundSku)
        GetDistinctRefundSkuValueList.Add(New RefundSku("TestSku", 12.34D))
    End Function
End Class

Friend Class TestRefundCountOverrideGetRefundSkuListWith2SameRefundSkusWithDifferentRelatedItemSkus
    Inherits TestRefundCount

    Friend Overrides Function GetDistinctRefundSkuValueList() As System.Collections.Generic.List(Of RefundSku)

        GetDistinctRefundSkuValueList = New System.Collections.Generic.List(Of RefundSku)
        GetDistinctRefundSkuValueList.Add(New RefundSku("TestSku", 12.34D))
        GetDistinctRefundSkuValueList.Add(New RefundSku("TestSku", 12.34D))
    End Function
End Class

Friend Class TestRefundCountSkuQuantityLimitOf3OverrideGetSortedDistinctSkuValueListWithListOf6DistinctValues
    Inherits TestRefundCount

    Private _testSku1 As String = "First"
    Private _testSku2 As String = "Second"
    Private _testSku3 As String = "Third"
    Private _testSku4 As String = "Fourth"
    Private _testSku5 As String = "Fifth"
    Private _testSku6 As String = "Sixth"

    Public Sub New()

        MyBase.New()
        SkuQuantityLimit = 3
    End Sub

    Friend Overrides Function GetSortedDistinctRefundSkuValueList() As System.Collections.Generic.List(Of RefundSku)

        GetSortedDistinctRefundSkuValueList = New System.Collections.Generic.List(Of RefundSku)
        With GetSortedDistinctRefundSkuValueList
            .Add(New RefundSku(_testSku1, 63.45D))
            .Add(New RefundSku(_testSku2, 51.67D))
            .Add(New RefundSku(_testSku3, 42.34D))
            .Add(New RefundSku(_testSku4, 37.89D))
            .Add(New RefundSku(_testSku5, 23.89D))
            .Add(New RefundSku(_testSku6, 15.67D))
        End With
    End Function

    Public Function GetTruncatedSortedRefundSkuValueListAllSkusInOrder() As String
        Dim SkusInOrder As New System.Text.StringBuilder

        For refSkuCount = 1 To IIf(SkuQuantityLimit < GetSortedDistinctRefundSkuValueList.Count, SkuQuantityLimit, GetSortedDistinctRefundSkuValueList.Count)
            SkusInOrder.Append(GetSortedDistinctRefundSkuValueList.Item(CInt(refSkuCount) - 1).Number)
        Next

        Return SkusInOrder.ToString
    End Function
End Class

Friend Class TestRefundCountOverrideLoadToSetFlagIfCalled
    Inherits TestRefundCount

    Private _loadMethodWasCalled As Boolean

    Public Overrides Sub Load()

        _loadMethodWasCalled = True
    End Sub

    Public ReadOnly Property LoadMethodWasCalled() As Boolean
        Get
            Return _loadMethodWasCalled
        End Get
    End Property
End Class


Friend Class TestRefundCountOverrideGetRefundRepositoryToReturnAndExpose_TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled
    Inherits TestRefundCount

    Private _testRefundRepository As TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled

    Public Sub New()

        _testRefundRepository = New TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled
    End Sub

    Friend Overrides Function GetRefundRepository() As RefundCountSelector.RefundRepository

        Return _testRefundRepository
    End Function

    Public ReadOnly Property TestRefundRepository() As TestRefundRepositoryOverridesGetRefundTransactionsToRecordStartAndEndDateParamsAndLogIfGetRefundTransactionsWasCalled
        Get
            Return _testRefundRepository
        End Get
    End Property
End Class