﻿Friend Class TestRefundLines
    Inherits RefundLines

    Private _total As Decimal

    Public Sub New(ByVal Total As Decimal)

        _total = Total
    End Sub

    Friend Overrides Function GetTotal() As Decimal

        Return _total
    End Function
End Class

Friend Class TestRefundLinesTotal30With3TestRefundLine_Total10_SkuNumberNotThisOneLines
    Inherits TestRefundLines

    Public Sub New()

        MyBase.New(30)
        AddLines()
    End Sub

    Private Sub AddLines()

        With Me.Lines
            .Add(New TestRefundLine_Total10_SkuNumberNotThisOne)
            .Add(New TestRefundLine_Total10_SkuNumberNotThisOne)
            .Add(New TestRefundLine_Total10_SkuNumberNotThisOne)
        End With
    End Sub
End Class

Friend Class TestRefundLinesTotal60With3TestRefundLine_Total20_SkuNumberThisOneLines
    Inherits TestRefundLines

    Public Sub New()

        MyBase.New(60)
        AddLines()
    End Sub

    Private Sub AddLines()

        With Me.Lines
            .Add(New TestRefundLine_Total20_SkuNumberThisOne)
            .Add(New TestRefundLine_Total20_SkuNumberThisOne)
            .Add(New TestRefundLine_Total20_SkuNumberThisOne)
        End With
    End Sub
End Class

Friend Class TestRefundLinesTotal30With3TestRefundLine_Total30_SkuNumberThisOneLines
    Inherits TestRefundLines

    Public Sub New()

        MyBase.New(30)
        AddLines()
    End Sub

    Private Sub AddLines()

        With Me.Lines
            .Add(New TestRefundLine_Total30_SkuNumberThisOne)
            .Add(New TestRefundLine_Total30_SkuNumberThisOne)
            .Add(New TestRefundLine_Total30_SkuNumberThisOne)
        End With
    End Sub
End Class

Friend Class TestRefundLinesTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines
    Inherits TestRefundLines

    Public Sub New()

        MyBase.New(30)
        AddLines()
    End Sub

    Private Sub AddLines()

        With Me.Lines
            .Add(New TestRefundLine_Total10_SkuNumberNotThisOne)
            .Add(New TestRefundLine_Total30_SkuNumberThisOne)
            .Add(New TestRefundLine_Total10_SkuNumberNotThisOne)
        End With
    End Sub
End Class

Friend Class TestRefundLinesTotal49_99With3TestRefundLine_Total10_SkuNumberThisOneLinesAnd1TestRefundLine_Total19_99_SkuNumberNotThisOneLines
    Inherits TestRefundLines

    Public Sub New()

        MyBase.New(CDec(49.99))
        AddLines()
    End Sub

    Private Sub AddLines()

        With Me.Lines
            .Add(New TestRefundLine_Total10_SkuNumberThisOne)
            .Add(New TestRefundLine_Total19_99_SkuNumberNotThisOne)
            .Add(New TestRefundLine_Total10_SkuNumberThisOne)
            .Add(New TestRefundLine_Total10_SkuNumberThisOne)
        End With
    End Sub

    Public Function SkuNumberFor3LinesOverLimit() As String

        Return (New TestRefundLine_Total10_SkuNumberThisOne).SkuNumber
    End Function
End Class

Friend Class TestRefundLinesTotal38_99With3TestRefundLinesEachWithSameSkuButDifferentPriceAnd2TestRefundLinesWithDifferentSkusAlsoUnderLimit
    Inherits TestRefundLines

    Public Sub New()

        MyBase.New(CDec(38.99))
        AddLines()
    End Sub

    Private Sub AddLines()

        With Me.Lines
            .Add(New TestRefundLine_Price3_33_And_Quantity3_SkuNumberThisOne)
            .Add(New TestRefundLine_Price2_50_And_Quantity2_SkuNumberNotThisOne)
            .Add(New TestRefundLine_Price3_And_Quantity4_SkuNumberThisOne)
            .Add(New TestRefundLine_Price1_And_Quantity4_SkuNumberStillNotThisOne)
            .Add(New TestRefundLine_Price4_And_Quantity2_SkuNumberThisOne)
        End With
    End Sub

    Public Function SkuNumberFor3LinesOverLimit() As String

        Return (New TestRefundLine_Price3_33_And_Quantity3_SkuNumberThisOne).SkuNumber
    End Function
End Class

Friend Class TestRefundLines_OverrideTotalIsOverLimitToFlagIfItIsCalled
    Inherits TestRefundLines

    Private _totalIsOverLimitWasCalled As Boolean

    Public Sub New()

        MyBase.New(30)
    End Sub

    Public Overrides Function TotalIsOverLimit(ByVal Limit As Decimal) As Boolean

        _totalIsOverLimitWasCalled = True
    End Function

    Public ReadOnly Property TotalIsOverLimitWasCalled() As Boolean
        Get
            Return _totalIsOverLimitWasCalled
        End Get
    End Property
End Class

Friend Class TestRefundLines_OverrideTotalIsOnOrOverLimitToFlagIfItIsCalled
    Inherits TestRefundLines

    Private _totalIsOnOrOverLimitWasCalled As Boolean

    Public Sub New()

        MyBase.New(30)
    End Sub

    Public Overrides Function TotalIsOnOrOverLimit(ByVal Limit As Decimal) As Boolean

        _totalIsOnOrOverLimitWasCalled = True
    End Function

    Public ReadOnly Property TotalIsOnOrOverLimitWasCalled() As Boolean
        Get
            Return _totalIsOnOrOverLimitWasCalled
        End Get
    End Property
End Class