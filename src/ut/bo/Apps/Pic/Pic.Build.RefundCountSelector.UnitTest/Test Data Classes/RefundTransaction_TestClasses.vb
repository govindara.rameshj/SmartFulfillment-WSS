﻿Friend Class TestRefundTransactionTotal30With3TestRefundLine_Total10_SkuNumberNotThisOneLines
    Inherits RefundTransaction

    Public Sub New()

        MyBase.New()

        Dim SetLinesFrom As RefundLines = New RefundLines((New TestRefundLinesTotal30With3TestRefundLine_Total10_SkuNumberNotThisOneLines).Lines)
        SetRefundLines(SetLinesFrom)
    End Sub
End Class

Friend Class TestRefundTransactionTotal60With3TestRefundLine_Total20_SkuNumberThisOneLines
    Inherits RefundTransaction

    Public Sub New()

        MyBase.New()

        Dim SetLinesFrom As RefundLines = New RefundLines((New TestRefundLinesTotal60With3TestRefundLine_Total20_SkuNumberThisOneLines).Lines)
        SetRefundLines(SetLinesFrom)
    End Sub
End Class

Friend Class TestRefundTransactionTotal30With3TestRefundLine_Total30_SkuNumberThisOneLines
    Inherits RefundTransaction

    Public Sub New()

        MyBase.New()

        Dim SetLinesFrom As RefundLines = New RefundLines((New TestRefundLinesTotal30With3TestRefundLine_Total30_SkuNumberThisOneLines).Lines)
        SetRefundLines(SetLinesFrom)
    End Sub
End Class

Friend Class TestRefundTransactionTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines
    Inherits RefundTransaction

    Public Sub New()

        MyBase.New()

        Dim SetLinesFrom As RefundLines = New RefundLines((New TestRefundLinesTotal30With2TestRefundLine_Total10_SkuNumberNotThisOneLinesAnd1TestRefundLine_Total30_SkuNumberThisOneLines).Lines)
        SetRefundLines(SetLinesFrom)
    End Sub
End Class

Friend Class TestRefundTransactionTotal49_99With3TestRefundLine_Total10_SkuNumberThisOneLinesAnd1TestRefundLine_Total19_99_SkuNumberNotThisOneLines
    Inherits RefundTransaction

    Public Sub New()

        MyBase.New()

        Dim SetLinesFrom As RefundLines = New RefundLines((New TestRefundLinesTotal49_99With3TestRefundLine_Total10_SkuNumberThisOneLinesAnd1TestRefundLine_Total19_99_SkuNumberNotThisOneLines).Lines)
        SetRefundLines(SetLinesFrom)
    End Sub

    Public Function SkuNumberFor3LinesOverLimit() As String

        Return (New TestRefundLinesTotal49_99With3TestRefundLine_Total10_SkuNumberThisOneLinesAnd1TestRefundLine_Total19_99_SkuNumberNotThisOneLines).SkuNumberFor3LinesOverLimit
    End Function
End Class

Friend Class TestRefundTransactionTotal38_99With3TestRefundLinesEachWithSameSkuButDifferentPriceAnd2TestRefundLinesWithDifferentSkusAlsoUnderLimit
    Inherits RefundTransaction

    Public Sub New()

        MyBase.New()

        Dim SetLinesFrom As RefundLines = New RefundLines((New TestRefundLinesTotal38_99With3TestRefundLinesEachWithSameSkuButDifferentPriceAnd2TestRefundLinesWithDifferentSkusAlsoUnderLimit).Lines)
        SetRefundLines(SetLinesFrom)
    End Sub

    Public Function SkuNumberFor3LinesOverLimit() As String

        Return (New TestRefundLinesTotal49_99With3TestRefundLine_Total10_SkuNumberThisOneLinesAnd1TestRefundLine_Total19_99_SkuNumberNotThisOneLines).SkuNumberFor3LinesOverLimit
    End Function
End Class