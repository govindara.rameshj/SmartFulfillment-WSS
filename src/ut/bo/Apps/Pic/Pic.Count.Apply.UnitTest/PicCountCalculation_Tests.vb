﻿<TestClass()> Public Class PicCountCalculation_Tests

    Private testContextInstance As TestContext
    Private piccodeNewInstance As PicCountCalculation
    'Private m_objHhtDetails As IHandHeldTerminalDetail
    Private m_objHhtDetails As StockAdjustments.HandHeldTerminalDetail

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassPicCountCalculation()

        Dim NewVersionOnly As IPicCodeCalculation
        Dim Factory As IBaseFactory(Of IPicCodeCalculation)

        Factory = New PicCountCalculationFactory
        NewVersionOnly = (New PicCountCalculationFactory).GetImplementation

        Assert.AreEqual("Pic.Count.Apply.PicCountCalculation", NewVersionOnly.GetType.FullName)

    End Sub

#Region "PicCountAcceptance Tests"

    <TestMethod()> Public Sub HHTDETWithProductCodeAndMDVARSetZero_CallsSetCode2StockLog_SetsStockLogSkun()
        Dim expectedValue As String

        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        expectedValue = m_objHhtDetails.ProductCode
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.MDVAR = 0
        piccodeNewInstance.SetCode2StockLog(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockLogFactoryInstance.SKUN)

    End Sub

    <TestMethod()> Public Sub HHTDETWithProductCodeAndMDVARSetZero_CallsSetCode2StockLog_SetsStockLogEndStock()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        expectedValue = piccodeNewInstance.StockFactoryInstance.OnHand_ONHA
        piccodeNewInstance.MDVAR = 0
        piccodeNewInstance.SetCode2StockLog(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockLogFactoryInstance.EndingStock_ESTK)

    End Sub

    <TestMethod()> Public Sub StockONHAandPositiveMDVARSet_CallsUpdateStock_UpdatesStockONHA()
        Dim expectedValue As Integer = 4

        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.MDVAR = 1
        piccodeNewInstance.UpdateStock()
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.OnHand_ONHA)

    End Sub

    <TestMethod()> Public Sub StockONHAandNegativeMDVARSet_CallsUpdateStock_UpdatesStockONHA()
        Dim expectedValue As Integer = 6

        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.MDVAR = -1
        piccodeNewInstance.UpdateStock()
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.OnHand_ONHA)

    End Sub

    <TestMethod()> Public Sub ONHASettoZeroAndMDVARSet_CallsGetAdjustmentQuantity_ReturnsMDVAR()
        Dim expectedValue As Integer

        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.MDVAR = 5
        expectedValue = piccodeNewInstance.MDVAR
        piccodeNewInstance.OHVAR = 0
        Assert.AreEqual(expectedValue, piccodeNewInstance.GetAdjustmentQuantity())

    End Sub

    <TestMethod()> Public Sub ONHASetNottoZeroAndMDVARSet_CallsGetAdjustmentQuantity_ReturnsOHVAR()
        Dim expectedValue As Integer

        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.OHVAR = 5
        expectedValue = piccodeNewInstance.OHVAR
        Assert.AreEqual(expectedValue, piccodeNewInstance.GetAdjustmentQuantity())

    End Sub

    <TestMethod()> Public Sub Code53TrigerredFlagSettoTrueAndMDVARSet_CallsGetAdjustmentQuantity_ReturnsMDVAR()
        Dim expectedValue As Integer

        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.MDVAR = 5
        piccodeNewInstance.Code53TrigerredFlag = True
        expectedValue = piccodeNewInstance.MDVAR
        Assert.AreEqual(expectedValue, piccodeNewInstance.GetAdjustmentQuantity())

    End Sub

    <TestMethod()> Public Sub Code53TrigerredFlagSettoFalseAndMDVARSet_CallsGetAdjustmentQuantity_ReturnsOHVAR()
        Dim expectedValue As Integer = 3

        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.Code53TrigerredFlag = False
        Assert.AreEqual(expectedValue, piccodeNewInstance.GetAdjustmentQuantity())

    End Sub

    <TestMethod()> Public Sub StockMDNQSetToFiveAndMDVARSetToThree_CallsSetStockAndAdjustmentAndStockLog_ReturnsMDNQValueEight()
        Dim expectedValue As Integer = 8

        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.StockFactoryInstance.MarkdownQty_MDNQ = 5
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.SetStockAndAdjustmentAndStockLog()
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.MarkdownQty_MDNQ)

    End Sub

    <TestMethod()> Public Sub StockMDNQSetToFiveAndMDVARSetToNegativeThree_CallsSetStockAndAdjustmentAndStockLog_ReturnsMDNQValueTwo()
        Dim expectedValue As Integer = 2

        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.StockFactoryInstance.MarkdownQty_MDNQ = 5
        piccodeNewInstance.MDVAR = -3
        piccodeNewInstance.SetStockAndAdjustmentAndStockLog()
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.MarkdownQty_MDNQ)

    End Sub

    <TestMethod()> Public Sub HHTDETSkuNumberSetAndOHVARSetToZero_CallsSetStockLog_ReturnsSetsSkunForStockLog()
        Dim expectedValue As String = "100803"
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100803"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.OHVAR = 0
        piccodeNewInstance.SetStockLog(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockLogFactoryInstance.SKUN)

    End Sub

    <TestMethod()> Public Sub HHTDETSkuNumberSetAndOHVARSetToZero_CallsSetStockLog_ReturnsSetsEndingStockForStockLog()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100803"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        expectedValue = piccodeNewInstance.StockFactoryInstance.OnHand_ONHA
        piccodeNewInstance.OHVAR = 0
        piccodeNewInstance.SetStockLog(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockLogFactoryInstance.EndingStock_ESTK)

    End Sub

    <TestMethod()> Public Sub HHTDETWithProductCodeAndOtherDetailsSetAndOHVARSetZero_CallsUpdateStockValues_SetsStockLogSkun()
        Dim expectedValue As String
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        expectedValue = m_objHhtDetails.ProductCode
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.OHVAR = 0
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockLogFactoryInstance.SKUN)

    End Sub

    <TestMethod()> Public Sub StockWithONHAAndOtherDetailsSetAndOHVARSetZero_CallsUpdateStockValues_SetsStockLogEndingStock()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        expectedValue = piccodeNewInstance.StockFactoryInstance.OnHand_ONHA
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.OHVAR = 0
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockLogFactoryInstance.EndingStock_ESTK)

    End Sub

    <TestMethod()> Public Sub MDVARandOtherDetailsSetAndOHVARSetZero_CallsUpdateStockValues_SetsStockOnHand()
        Dim expectedValue As Integer = 8
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.OHVAR = 0
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.OnHand_ONHA)

    End Sub

    <TestMethod()> Public Sub HHTHeaderDateandOtherDetailsSetAndOHVARSetZero_CallsUpdateStockValues_SetsStockCycleDate()
        Dim expectedValue As Date = CDate("2012-01-01")
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.OHVAR = 0
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.CycleDate_ADAT1)

    End Sub

    <TestMethod()> Public Sub MDVARandOtherDetailsSetAndOHVARSetZero_CallsUpdateStockValues_SetsStockAdjustmentQty021()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 = 6
        piccodeNewInstance.MDVAR = 3
        expectedValue = piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 + piccodeNewInstance.MDVAR
        piccodeNewInstance.OHVAR = 0
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021)

    End Sub

    <TestMethod()> Public Sub MDVARandOtherDetailsSetAndOHVARSetZero_CallsUpdateStockValues_SetsStockAdjustmentValue021()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 = 6
        piccodeNewInstance.MDVAR = 3
        expectedValue = piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021 + (piccodeNewInstance.MDVAR * piccodeNewInstance.StockFactoryInstance.Price_PRIC)
        piccodeNewInstance.OHVAR = 0
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021)

    End Sub

    <TestMethod()> Public Sub StockWithONHAAndOtherDetailsSetAndSetCode53TrigerredFlagTrue_CallsUpdateStockValues_SetsStockLogEndingStock()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        expectedValue = piccodeNewInstance.StockFactoryInstance.OnHand_ONHA
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.Code53TrigerredFlag = True
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockLogFactoryInstance.EndingStock_ESTK)

    End Sub

    <TestMethod()> Public Sub MDVARandOtherDetailsSetAndSetCode53TrigerredFlagTrue_CallsUpdateStockValues_SetsStockOnHand()
        Dim expectedValue As Integer = 8
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.Code53TrigerredFlag = True
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.OnHand_ONHA)

    End Sub

    <TestMethod()> Public Sub HHTHeaderDateandOtherDetailsSetAndSetCode53TrigerredFlagTrue_CallsUpdateStockValues_SetsStockCycleDate()
        Dim expectedValue As Date = CDate("2012-01-01")
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.MDVAR = 3
        piccodeNewInstance.Code53TrigerredFlag = True
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.CycleDate_ADAT1)

    End Sub

    <TestMethod()> Public Sub MDVARandOtherDetailsSetAndSetCode53TrigerredFlagTrue_CallsUpdateStockValues_SetsStockAdjustmentQty021()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 = 6
        piccodeNewInstance.MDVAR = 3
        expectedValue = piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 + piccodeNewInstance.MDVAR
        piccodeNewInstance.Code53TrigerredFlag = True
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021)

    End Sub

    <TestMethod()> Public Sub MDVARandOtherDetailsSetAndSetCode53TrigerredFlagTrue_CallsUpdateStockValues_SetsStockAdjustmentValue021()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 = 6
        piccodeNewInstance.MDVAR = 3
        expectedValue = piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021 + (piccodeNewInstance.MDVAR * piccodeNewInstance.StockFactoryInstance.Price_PRIC)
        piccodeNewInstance.Code53TrigerredFlag = True
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021)

    End Sub

    <TestMethod()> Public Sub OHVARandOtherDetailsSetAndOHVARSetToNonZero_CallsUpdateStockValues_SetsStockOnHand()
        Dim expectedValue As Integer = 8
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.OHVAR = 3
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.OnHand_ONHA)

    End Sub

    <TestMethod()> Public Sub HHTHeaderDateandOtherDetailsSetAndOHVARSetToNonZero_CallsUpdateStockValues_SetsStockCycleDate()
        Dim expectedValue As Date = CDate("2012-01-01")
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.OHVAR = 3
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.CycleDate_ADAT1)

    End Sub

    <TestMethod()> Public Sub OHVARandOtherDetailsSetAndOHVARSetToNonZero_CallsUpdateStockValues_SetsStockAdjustmentQty021()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 = 6
        piccodeNewInstance.OHVAR = 3
        expectedValue = piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 + piccodeNewInstance.OHVAR
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021)

    End Sub

    <TestMethod()> Public Sub OHVARandOtherDetailsSetAndOHVARSetToNonZero_CallsUpdateStockValues_SetsStockAdjustmentValue021()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 = 6
        piccodeNewInstance.OHVAR = 3
        expectedValue = piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021 + (piccodeNewInstance.OHVAR * piccodeNewInstance.StockFactoryInstance.Price_PRIC)
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021)

    End Sub

    <TestMethod()> Public Sub OHVARandOtherDetailsSetAndSetCode53TrigerredFlagFalse_CallsUpdateStockValues_SetsStockOnHand()
        Dim expectedValue As Integer = 8
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.OHVAR = 3
        piccodeNewInstance.Code53TrigerredFlag = False
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.OnHand_ONHA)

    End Sub

    <TestMethod()> Public Sub HHTHeaderDateAndOHVARandOtherDetailsSetAndSetCode53TrigerredFlagFalse_CallsUpdateStockValues_SetsStockCycleDate()
        Dim expectedValue As Date = CDate("2012-01-01")
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.OHVAR = 3
        piccodeNewInstance.Code53TrigerredFlag = False
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.CycleDate_ADAT1)

    End Sub

    <TestMethod()> Public Sub OHVARandOtherDetailsSetAndSetCode53TrigerredFlagFalse_CallsUpdateStockValues_SetsStockAdjustmentQty021()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 = 6
        piccodeNewInstance.OHVAR = 3
        expectedValue = piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021 + piccodeNewInstance.OHVAR
        piccodeNewInstance.Code53TrigerredFlag = False
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.AdjustmentQty_AQ021)

    End Sub

    <TestMethod()> Public Sub OHVARandOtherDetailsSetAndSetCode53TrigerredFlagFalse_CallsUpdateStockValues_SetsStockAdjustmentValue021()
        Dim expectedValue As Decimal
        m_objHhtDetails = New StockAdjustments.HandHeldTerminalDetail

        m_objHhtDetails.ProductCode = "100807"
        piccodeNewInstance = New PicCountCalculation
        piccodeNewInstance.StockLogFactoryInstance = New StockLog
        piccodeNewInstance.StockFactoryInstance = New Stock
        piccodeNewInstance.HhtHeaderFactoryInstance = New HandHeldTerminalHeader
        piccodeNewInstance.StockFactoryInstance.OnHand_ONHA = 5
        piccodeNewInstance.StockFactoryInstance.Price_PRIC = 2
        piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021 = 6
        piccodeNewInstance.OHVAR = 3
        expectedValue = piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021 + (piccodeNewInstance.OHVAR * piccodeNewInstance.StockFactoryInstance.Price_PRIC)
        piccodeNewInstance.Code53TrigerredFlag = False
        piccodeNewInstance.HhtHeaderFactoryInstance.HHTDate = CDate("2012-01-01")
        piccodeNewInstance.UpdateStockValues(m_objHhtDetails)
        Assert.AreEqual(expectedValue, piccodeNewInstance.StockFactoryInstance.AdjustmentValue_AV021)

    End Sub

#End Region

End Class