﻿<TestClass()> Public Class StockVarianceFormulaUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory Unit Test - IStockVarianceFormula"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassStockVarianceFormulaExcludePresold()

        Dim NewVersionOnly As IStockVarianceFormula
        Dim Factory As IBaseFactory(Of IStockVarianceFormula)

        Factory = New StockVarianceFormulaFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("Pic.Count.Apply.StockVarianceFormulaExcludePresold", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - Stock Variance Formula Test"

    <TestMethod()> Public Sub StockVarianceFormulaCheck_ParameterTrue_ReturnExcludePresoldFormula()

        Dim V As IStockVarianceFormula = New StockVarianceFormulaExcludePresold

        Assert.AreEqual(V.Formula(), "RC[-1]+RC[-2]-RC[-3]")

    End Sub

#End Region

End Class