﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports IbtCore
Imports BOStore.cStore
Imports OasysDBBO
Imports BOStore


<TestClass()> Public Class IbtStoreNameUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property


    Friend Class GetTestStoreClass

        Public Function GetStore() As BOStore.cStore
            Dim store As New BOStore.cStore
            store.AddressLine1.Value = "Test Address Line 1"
            store.StoreNameOnReceipt.Value = "Test Store Name"
            Return store
        End Function

    End Class

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "IbtStoreName tests"
    <TestMethod()> Public Sub IbtStoreName_SwitchEnabled_ReturnsStoreName()
        Dim StoreGetter As New GetTestStoreClass
        Dim Store As cStore = StoreGetter.GetStore
        Dim returned As New IbtCore.IbtStoreName

        Assert.AreEqual(returned.ReturnAddress(Store).ToString, Store.StoreNameOnReceipt.Value.ToString)
    End Sub
    <TestMethod()> Public Sub IbtStoreName_SwitchEnabled_ReturnsAddress1()
        Dim StoreGetter As New GetTestStoreClass
        Dim Store As cStore = StoreGetter.GetStore
        Dim returned As New IbtCore.IbtStoreNameOldWay

        Assert.AreEqual(returned.ReturnAddress(Store).ToString, Store.AddressLine1.Value.ToString)
    End Sub
#End Region

#Region "Switch Tests"

    <TestMethod()> Public Sub IbtStoreNameFactory_SwitchEnabled_ReturnsNewImplementation()
        Dim myFactory As New IbtStoreNameFactory_SwitchEnabledScenario

        Dim returned As IIbtStoreName
        returned = myFactory.GetImplementation()

        Assert.AreEqual(returned.GetType.FullName.ToUpper, "IbtCore.IbtStoreName".ToUpper)

    End Sub
    <TestMethod()> Public Sub IbtStoreNameFactory_SwitchDisabled_ReturnsOldImplementation()
        Dim myFactory As New IbtStoreNameFactory_SwitchDisabledScenario

        Dim returned As IIbtStoreName
        returned = myFactory.GetImplementation()

        Assert.AreEqual(returned.GetType.FullName.ToUpper, "IbtCore.IbtStoreNameOldWay".ToUpper)

    End Sub
    <TestMethod()> Public Sub IbtStoreNameFactory_ReturnsParameterIDMinus22035()
        Dim myFactory As New IbtStoreNameFactory

        Assert.AreEqual(-22035, myFactory.SwitchParameterID)

    End Sub
    Friend Class IbtStoreNameFactory_SwitchEnabledScenario
        Inherits IbtStoreNameFactory
        Public Overrides Function ImplementationA_IsActive() As Boolean
            Return True
        End Function
    End Class
    Friend Class IbtStoreNameFactory_SwitchDisabledScenario
        Inherits IbtStoreNameFactory
        Public Overrides Function ImplementationA_IsActive() As Boolean
            Return False
        End Function
    End Class

#End Region
End Class
