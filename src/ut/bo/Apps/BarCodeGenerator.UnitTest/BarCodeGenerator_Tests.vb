﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data

<TestClass()> Public Class BarCodeGenerator_Tests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    '''

    Private barCodeGeneratorInstance As BarCodeGenerator = New BarCodeGenerator
    Private barCodeGeneratorTestClassInstance As BarCodeGeneratorDerivedTestClass = New BarCodeGeneratorDerivedTestClass


    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub Number123456Passed_GetOddPositionInNumberTotal_ReturnsTotal9()

        Assert.AreEqual(9, barCodeGeneratorInstance.GetOddPositionInNumberTotal("123456"))

    End Sub
    <TestMethod()> Public Sub Number2323232323Passed_GetOddPositionInNumberTotal_ReturnsTotal10()

        Assert.AreEqual(10, barCodeGeneratorInstance.GetOddPositionInNumberTotal("2323232323"))

    End Sub

    <TestMethod()> Public Sub Number01010101010Passed_GetOddPositionInNumberTotal_ReturnsTotal0()

        Assert.AreEqual(0, barCodeGeneratorInstance.GetOddPositionInNumberTotal("0101010101010"))

    End Sub

    <TestMethod()> Public Sub NumberNothingPassed_GetOddPositionInNumberTotal_ReturnsTotal0()

        Assert.AreEqual(0, barCodeGeneratorInstance.GetOddPositionInNumberTotal(String.Empty))

    End Sub
    <TestMethod()> Public Sub Number123456Passed_GetEvenPositionInNumberTotal_ReturnsTotal12()

        Assert.AreEqual(12, barCodeGeneratorInstance.GetEvenPositionInNumberTotal("123456"))

    End Sub
    <TestMethod()> Public Sub Number2323232323Passed_GetEvenPositionInNumberTotal_ReturnsTotal15()

        Assert.AreEqual(15, barCodeGeneratorInstance.GetEvenPositionInNumberTotal("2323232323"))

    End Sub

    <TestMethod()> Public Sub Number101010101010Passed_GetEvenPositionInNumberTotal_ReturnsTotal0()

        Assert.AreEqual(0, barCodeGeneratorInstance.GetEvenPositionInNumberTotal("101010101010"))

    End Sub

    <TestMethod()> Public Sub NumberNothingPassed_GetEvenPositionInNumberTotal_ReturnsTotal0()

        Assert.AreEqual(0, barCodeGeneratorInstance.GetEvenPositionInNumberTotal(String.Empty))

    End Sub

    <TestMethod()> Public Sub Number20Passed_GetNumberMultipliedByThree_Returns60()

        Assert.AreEqual(60, barCodeGeneratorInstance.GetNumberMultipliedByThree(20))

    End Sub

    <TestMethod()> Public Sub Number0Passed_GetNumberMultipliedByThree_Returns0()

        Assert.AreEqual(0, barCodeGeneratorInstance.GetNumberMultipliedByThree(0))

    End Sub

    <TestMethod()> Public Sub OddNumberTotal12AndEvenNumberTotal20Passed_GetSumOfOddNumberTotalAndEvenNumberTotal_Returns32()

        Assert.AreEqual(32, barCodeGeneratorInstance.GetSumOfOddNumberTotalAndEvenNumberTotal(12, 20))

    End Sub
    <TestMethod()> Public Sub OddNumberTotal0AndEvenNumberTotal20Passed_GetSumOfOddNumberTotalAndEvenNumberTotal_Returns20()

        Assert.AreEqual(20, barCodeGeneratorInstance.GetSumOfOddNumberTotalAndEvenNumberTotal(0, 20))

    End Sub
    <TestMethod()> Public Sub OddNumberTotal0AndEvenNumberTotal0Passed_GetSumOfOddNumberTotalAndEvenNumberTotal_Returns0()

        Assert.AreEqual(0, barCodeGeneratorInstance.GetSumOfOddNumberTotalAndEvenNumberTotal(0, 0))

    End Sub

    <TestMethod()> Public Sub Number23Passed_CheckDigitCalculation_Returns7()

        Assert.AreEqual(7, barCodeGeneratorInstance.CheckDigitCalculation(23))

    End Sub

    <TestMethod()> Public Sub Number0Passed_CheckDigitCalculation_Returns0()

        Assert.AreEqual(0, barCodeGeneratorInstance.CheckDigitCalculation(0))

    End Sub
    <TestMethod()> Public Sub Number123Passed_CheckDigitCalculation_Returns7()

        Assert.AreEqual(7, barCodeGeneratorInstance.CheckDigitCalculation(123))

    End Sub

    <TestMethod()> Public Sub Number455555557Passed_CheckDigitCalculation_Returns3()

        Assert.AreEqual(3, barCodeGeneratorInstance.CheckDigitCalculation(455555557))

    End Sub

    <TestMethod()> Public Sub Number2100803Passed_GetCheckDigit_Returns0()

        Assert.AreEqual(0, barCodeGeneratorInstance.GetCheckDigit("2100803"))

    End Sub

    <TestMethod()> Public Sub Number2100804Passed_GetCheckDigit_Returns7()

        Assert.AreEqual(7, barCodeGeneratorInstance.GetCheckDigit("2100804"))

    End Sub

    <TestMethod()> Public Sub Number2101857Passed_GetCheckDigit_Returns2()

        Assert.AreEqual(2, barCodeGeneratorInstance.GetCheckDigit("2101857"))

    End Sub

    <TestMethod()> Public Sub Number2123456Passed_GetCheckDigit_Returns9()

        Assert.AreEqual(9, barCodeGeneratorInstance.GetCheckDigit("2123456"))

    End Sub

    <TestMethod()> Public Sub SkuNumber100803Passed_GetBarcode_Returns0000000021008030()

        Assert.AreEqual("0000000021008030", barCodeGeneratorInstance.GetBarcode("100803"))

    End Sub

    <TestMethod()> Public Sub SkuNumber109991Passed_GetBarcode_Returns0000000021099915()

        Assert.AreEqual("0000000021099915", barCodeGeneratorInstance.GetBarcode("109991"))

    End Sub

    <TestMethod()> Public Sub SkuNumber260329Passed_GetBarcode_Returns0000000021099915()

        Assert.AreEqual("0000000022603296", barCodeGeneratorInstance.GetBarcode("260329"))

    End Sub

    <TestMethod()> Public Sub SkuNumber260395Passed_GetBarcode_Returns0000000022603951()

        Assert.AreEqual("0000000022603951", barCodeGeneratorInstance.GetBarcode("260395"))

    End Sub
    <TestMethod()> Public Sub SkuNumber260398Passed_GetBarcode_Returns0000000022603982()

        Assert.AreEqual("0000000022603982", barCodeGeneratorInstance.GetBarcode("260398"))

    End Sub

    <TestMethod()> Public Sub SkuNumber260844Passed_GetBarcode_Returns0000000022608444()

        Assert.AreEqual("0000000022608444", barCodeGeneratorInstance.GetBarcode("260844"))

    End Sub

    <TestMethod()> Public Sub SkuNumber999998Passed_GetBarcode_Returns0000000029999989()

        Assert.AreEqual("0000000029999989", barCodeGeneratorInstance.GetBarcode("999998"))

    End Sub

    <TestMethod()> Public Sub DataTableLoadedWith10RecordsPassed_GenerateBarCode_Returns10Records()
        barCodeGeneratorTestClassInstance.populateDataFlag = True
        barCodeGeneratorTestClassInstance.GenerateBarCode()
        Assert.AreEqual(barCodeGeneratorTestClassInstance.PopulateData.Rows.Count, barCodeGeneratorTestClassInstance.eanMasUpdated.Count)

    End Sub

    <TestMethod()> Public Sub DataTableLoadedWith0RecordsPassed_GenerateBarCode_Returns0Records()

        barCodeGeneratorTestClassInstance.populateDataFlag = False
        barCodeGeneratorTestClassInstance.GenerateBarCode()
        Assert.AreEqual(barCodeGeneratorTestClassInstance.DoesNotPopulateData.Rows.Count, barCodeGeneratorTestClassInstance.eanMasUpdated.Count)

    End Sub

    <TestMethod()> Public Sub DataTableLoadedWith10RecordsPassed_GenerateBarCode_Returns10RecordsWithBarcode()

        barCodeGeneratorTestClassInstance.populateDataFlag = True
        barCodeGeneratorTestClassInstance.GenerateBarCode()
        Assert.AreEqual(barCodeGeneratorTestClassInstance.GetBarcode("100803"), barCodeGeneratorTestClassInstance.eanMasUpdated(0).BarCode)

    End Sub

    <TestMethod()> Public Sub DataTableLoadedWith10RecordsPassedWithNoBarCode_GenerateBarCode_Returns10RecordsWithBarcode()

        barCodeGeneratorTestClassInstance.populateDataFlag = True
        barCodeGeneratorTestClassInstance.GenerateBarCode()
        Assert.AreEqual(True, CheckIfBarcodeAvailable(barCodeGeneratorTestClassInstance.PopulateData, barCodeGeneratorTestClassInstance.eanMasUpdated))

    End Sub


    Private Function CheckIfBarcodeAvailable(ByVal Data As DataTable, ByVal EanmasUpdated As BarCodeEanmas()) As Boolean
        For i = 0 To Data.Rows.Count - 1
            If Not Data.Rows(i)(1).Equals(EanmasUpdated(i).BarCode) Then
                Return True
            End If
        Next
    End Function


    Public Class BarCodeGeneratorDerivedTestClass
        Inherits BarCodeGenerator

        Private _eanMasUpdated As BarCodeEanmas()
        Private _populateData As Boolean
        Friend Overrides Sub LoadDataTobeInsertedtoEanmas()
            If populateDataFlag Then
                PopulateData()
            Else
                DoesNotPopulateData()
            End If
        End Sub

        Public Property eanMasUpdated() As BarCodeEanmas()
            Get
                Return _eanMasUpdated
            End Get
            Set(ByVal value As BarCodeEanmas())
                _eanMasUpdated = value
            End Set
        End Property
        Public Property populateDataFlag() As Boolean
            Get
                Return _populateData

            End Get
            Set(ByVal value As Boolean)
                _populateData = value
            End Set
        End Property


        Friend Overrides Sub CreateBarCodeAndSaveData()
            Dim eanMasInstance As BarCodeEanmas() = GetEanmasData()
            For Each record In eanMasInstance
                record.BarCode = GetBarcode(record.SkuNumber)
            Next
            eanMasUpdated = eanMasInstance
        End Sub

        Public Function PopulateData() As DataTable
            Dim DT As DataTable = New DataTable
            DT.Columns.Add("SkuNumber", System.Type.GetType("System.String"))
            DT.Columns.Add("BarCode", System.Type.GetType("System.String"))
            DT.Rows.Add("100803", "")
            DT.Rows.Add("100804", "")
            DT.Rows.Add("100805", "")
            DT.Rows.Add("100806", "")
            DT.Rows.Add("100807", "")
            DT.Rows.Add("100809", "")
            DT.Rows.Add("100810", "")
            DT.Rows.Add("100811", "")
            DT.Rows.Add("100812", "")
            DT.Rows.Add("100813", "")

            EanmasDataTable = DT
            Return DT

        End Function

        Public Function DoesNotPopulateData() As DataTable
            Dim DT As DataTable = New DataTable
            DT.Columns.Add("SkuNumber", System.Type.GetType("System.String"))
            DT.Columns.Add("BarCode", System.Type.GetType("System.String"))

            EanmasDataTable = DT
            Return DT


        End Function

    End Class



End Class
