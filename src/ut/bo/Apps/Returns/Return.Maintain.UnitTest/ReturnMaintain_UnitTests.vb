﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TpWickes.Library
'Imports Rhino.Mocks
Imports System.Data
Imports NSubstitute

<TestClass()> Public Class ReturnsMaintainUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub ReturnsMaintain_RequirementSwitch_ReturnsRefreshReturnsHeader()
        Dim V As Returns.Maintain.IRefreshReturnsHeader

        ArrangeRequirementSwitchDependency(True)

        V = (New Returns.Maintain.RefreshReturnsHeaderFactory).GetImplementation

        Assert.AreEqual("Returns.Maintain.RefreshReturnsHeader", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub ReturnsMaintain_RequirementSwitch_ReturnsRefreshReturnsHeaderOldWay()
        Dim V As Returns.Maintain.IRefreshReturnsHeader

        ArrangeRequirementSwitchDependency(False)

        V = (New Returns.Maintain.RefreshReturnsHeaderFactory).GetImplementation

        Assert.AreEqual("Returns.Maintain.RefreshReturnsHeaderOldWay", V.GetType.FullName)

    End Sub
    <TestMethod()> Public Sub GetReturnsHeader_CalledOnce_ReturnsCorrectValuesForHeader()
        Dim V As New Returns.Maintain.RefreshReturnsHeader

        V._returnsHeader = Substitute.For(Of Returns.Maintain.IReturnsHeaderRepository)()

        Dim dr1 As DataRow = New RETHEDR_Row1().Row

        Dim inputRetHeader1 As New BOPurchases.cReturnHeader
        Dim outputRetHeader1 As New BOPurchases.cReturnHeader

        Dim Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB

        inputRetHeader1.DrlNumber.Value = "1"

        V._returnsHeader.GetReturnsHeader(Oasys3DB, dr1).ReturnsForAnyArgs(inputRetHeader1)

        outputRetHeader1 = V.ReturnsHeader(inputRetHeader1, Oasys3DB, dr1)

        Assert.AreEqual(inputRetHeader1.DrlNumber.Value, outputRetHeader1.DrlNumber.Value)

    End Sub

    <TestMethod()> Public Sub GetReturnsHeader_CalledTwice_ReturnsDifferentValuesForHeaders()
        Dim V As New Returns.Maintain.RefreshReturnsHeader

        V._returnsHeader = Substitute.For(Of Returns.Maintain.IReturnsHeaderRepository)()

        Dim dr1 As DataRow = New RETHEDR_Row1().Row
        Dim dr2 As DataRow = New RETHEDR_Row2().Row

        Dim inputRetHeader1 As New BOPurchases.cReturnHeader
        Dim inputRetHeader2 As New BOPurchases.cReturnHeader
        Dim outputRetHeader1 As New BOPurchases.cReturnHeader
        Dim outputRetHeader2 As New BOPurchases.cReturnHeader

        Dim Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB

        inputRetHeader1.DrlNumber.Value = "1"
        inputRetHeader2.DrlNumber.Value = "2"

        V._returnsHeader.GetReturnsHeader(Oasys3DB, dr1).ReturnsForAnyArgs(inputRetHeader1, inputRetHeader2)

        outputRetHeader1 = V.ReturnsHeader(inputRetHeader1, Oasys3DB, dr1)
        outputRetHeader2 = V.ReturnsHeader(inputRetHeader2, Oasys3DB, dr2)

        Assert.AreNotEqual(outputRetHeader1.DrlNumber.Value, outputRetHeader2.DrlNumber.Value)

    End Sub
    <TestMethod()> Public Sub GetReturnsHeader_CalledTwice_ReturnsCorrectValuesForSecondHeader()
        Dim V As New Returns.Maintain.RefreshReturnsHeader

        V._returnsHeader = Substitute.For(Of Returns.Maintain.IReturnsHeaderRepository)()

        Dim dr1 As DataRow = New RETHEDR_Row1().Row
        Dim dr2 As DataRow = New RETHEDR_Row2().Row

        Dim inputRetHeader1 As New BOPurchases.cReturnHeader
        Dim inputRetHeader2 As New BOPurchases.cReturnHeader
        Dim outputRetHeader1 As New BOPurchases.cReturnHeader
        Dim outputRetHeader2 As New BOPurchases.cReturnHeader

        Dim Oasys3DB As New OasysDBBO.Oasys3.DB.clsOasys3DB

        inputRetHeader1.DrlNumber.Value = "1"
        inputRetHeader2.DrlNumber.Value = "2"

        V._returnsHeader.GetReturnsHeader(Oasys3DB, dr1).ReturnsForAnyArgs(inputRetHeader1, inputRetHeader2)

        outputRetHeader1 = V.ReturnsHeader(inputRetHeader1, Oasys3DB, dr1)
        outputRetHeader2 = V.ReturnsHeader(inputRetHeader2, Oasys3DB, dr2)

        Assert.AreEqual(inputRetHeader2.DrlNumber.Value, outputRetHeader2.DrlNumber.Value)

    End Sub
#Region "Functionality Test"

    '<TestMethod()> Public Sub ReturnsMaintain_CallsRefreshMetod_ReturnsTrue()

    'Dim retHeader As New BOPurchases.cReturnHeader
    'Dim Oasys3 As New OasysDBBO.Oasys3.DB.clsOasys3DB
    'Dim dr As DataRow
    'Dim X As New TestRefreshReturnsHeader

    '        X.RefreshReturnsHeader(retHeader, Oasys3, dr)
    'Assert.IsTrue(X._RefreshReturnsHeaderCalled)
    'End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)
        Dim requirement = Substitute.For(Of IRequirementRepository)()

        requirement.IsSwitchPresentAndEnabled(Arg.Any(Of Integer)).Returns(returnValue)
        RequirementRepositoryFactory.FactorySet(requirement)

    End Sub

#End Region

End Class

#Region " Helper Classes "

Public Class RETHDRDataRow
    Private _Table As New DataTable("RETHDR")
    Private _Row As DataRow = _Table.NewRow

    Public Sub New(ByVal tkey As Integer, ByVal numb As String, ByVal supp As String, ByVal edat As DateTime, _
                   ByVal rdat As DateTime, ByVal drln As String, ByVal valu As Decimal, ByVal eprt As Boolean, _
                   ByVal rprt As Boolean, ByVal isDeleted As Boolean, ByVal rti As String)
        _Table.Columns.Add("TKEY", System.Type.GetType("System.Int32"))
        _Table.Columns.Add("NUMB", System.Type.GetType("System.String"))
        _Table.Columns.Add("SUPP", System.Type.GetType("System.String"))
        _Table.Columns.Add("EDAT", System.Type.GetType("System.DateTime"))
        _Table.Columns.Add("RDAT", System.Type.GetType("System.DateTime"))
        _Table.Columns.Add("DRLN", System.Type.GetType("System.String"))
        _Table.Columns.Add("VALU", System.Type.GetType("System.Decimal"))
        _Table.Columns.Add("EPRT", System.Type.GetType("System.Boolean"))
        _Table.Columns.Add("RPRT", System.Type.GetType("System.Boolean"))
        _Table.Columns.Add("IsDeleted", System.Type.GetType("System.Boolean"))
        _Table.Columns.Add("RTI", System.Type.GetType("System.String"))

        _Row("TKEY") = tkey
        _Row("NUMB") = numb
        _Row("SUPP") = supp
        _Row("EDAT") = edat
        _Row("RDAT") = rdat
        _Row("DRLN") = drln
        _Row("VALU") = valu
        _Row("EPRT") = eprt
        _Row("RPRT") = rprt
        _Row("IsDeleted") = isDeleted
        _Row("RTI") = rti

        _Table.Rows.Add(_Row)

    End Sub

    Friend ReadOnly Property Row() As DataRow
        Get
            Return _Row
        End Get
    End Property

End Class

Friend Class RETHEDR_Row1

    Friend Function Row() As DataRow
        Dim r As New RETHDRDataRow(111, "111111", "RED", "2012 Oct 21 11:23:00", "2012 Oct 2 11:23:00", "DRLN", 1233.43, True, True, False, "Y")
        Return r.Row()
    End Function

End Class

Friend Class RETHEDR_Row2

    Friend Function Row() As DataRow
        Dim r As New RETHDRDataRow(222, "222222", "BLUE", "2011 Mar 2 1:00:00", "2011 Mar 2 1:00:00", "TWO", 1.0, False, False, True, "N")
        Return r.Row()
    End Function

End Class
Public Class TestRefreshReturnsHeader
    Inherits Returns.Maintain.RefreshReturnsHeader

    Public _RefreshReturnsHeaderCalled As Boolean = False

    'Friend Overridable Function GetMyReturnsHeader(ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal dr As System.Data.DataRow) As BOPurchases.cReturnHeader
    '    Dim retHeader As New BOPurchases.cReturnHeader
    '    dr = SetupDataRow()

    'End Function

    'Friend Overrides Function GetMyReturnsHeader(ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal dr As System.Data.DataRow) As BOPurchases.cReturnHeader

    'End Function

    'Public Overrides Sub RefreshReturnsHeader(ByRef retHeader As BOPurchases.cReturnHeader, ByVal Oasys3DB As OasysDBBO.Oasys3.DB.clsOasys3DB, ByVal dr As System.Data.DataRow)
    '    _RefreshReturnsHeaderCalled = True
    'End Sub
End Class
#End Region
