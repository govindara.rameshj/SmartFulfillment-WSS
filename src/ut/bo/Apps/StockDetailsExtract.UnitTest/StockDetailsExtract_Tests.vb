﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data
Imports StockDetailsExtract

<TestClass()> Public Class StockDetailsExtract_Tests

    Private testContextInstance As TestContext

    Private stockdetailsextractInstance As New StockDetailsExtract
    Private Shared stkadj As New DataTable("STKADJ")
    Private Shared stkadjRow As DataRow = stkadj.NewRow



    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        stkadj.Columns.Add("skunumber", System.Type.GetType("System.String"))
        stkadj.Columns.Add("quantity", System.Type.GetType("System.String"))
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub CheckXMLOutputPathBackslashNotSuppliedForPathReturnsPathWithBackslash()

        Dim expectedValue As String = "C:\Wix\"

        stockdetailsextractInstance.StockXmlFileOutputPath = "C:\Wix"
        stockdetailsextractInstance.CheckXMLOutputPath()

        Assert.AreEqual(expectedValue, stockdetailsextractInstance.StockXmlFileOutputPath)
    End Sub

    <TestMethod()> Public Sub CheckXMLOutputPathBackslashSuppliedForPathReturnsPathWithBackslash()

        Dim expectedValue As String = "C:\Wix\"

        stockdetailsextractInstance.StockXmlFileOutputPath = "C:\Wix\"
        stockdetailsextractInstance.CheckXMLOutputPath()

        Assert.AreEqual(expectedValue, stockdetailsextractInstance.StockXmlFileOutputPath)
    End Sub

    <TestMethod()> Public Sub CheckStockDetailsPopulatedArrayNumberOfRecordsMatch()

        stockdetailsextractInstance.StockDataTable = PopulateDataTable()
        Dim stkInstance(stockdetailsextractInstance.StockDataTable.Rows.Count - 1) As StockDetails
        stkInstance = stockdetailsextractInstance.GetStockDetails

        Assert.AreEqual(stockdetailsextractInstance.StockDataTable.Rows.Count, stkInstance.Length)


    End Sub


    Private Function PopulateDataTable() As DataTable
        stkadjRow("skunumber") = "100803"
        stkadjRow("quantity") = 10
        stkadj.Rows.Add(stkadjRow)

        stkadjRow = stkadj.NewRow
        stkadjRow("skunumber") = "100804"
        stkadjRow("quantity") = 50
        stkadj.Rows.Add(stkadjRow)

        stkadjRow = stkadj.NewRow
        stkadjRow("skunumber") = "100805"
        stkadjRow("quantity") = 40
        stkadj.Rows.Add(stkadjRow)

        stkadjRow = stkadj.NewRow
        stkadjRow("skunumber") = "100806"
        stkadjRow("quantity") = 30
        stkadj.Rows.Add(stkadjRow)

        stkadjRow = stkadj.NewRow
        stkadjRow("skunumber") = "100807"
        stkadjRow("quantity") = 20
        stkadj.Rows.Add(stkadjRow)

        Return stkadj

    End Function
End Class
