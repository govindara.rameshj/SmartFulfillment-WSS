﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports SplitBulkToSingleItems.Structures

<TestClass()> Public Class ItemAdjustmentTest

    Private _testContextInstance As TestContext

    Public Property TestContext() As TestContext
        Get
            Return _testContextInstance
        End Get
        Set(ByVal value As TestContext)
            _testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region
    <TestMethod()> Public Sub ItemAdjustment_PositiveAdjustmentQuantity_CheckEndQuantity()
        Dim itm = New ItemAdjustment("123456", -5, CDec(12.99))
        itm.AdjustmentQuantity = 8
        Assert.AreEqual(3, itm.EndQuantity)
    End Sub
    <TestMethod()> Public Sub ItemAdjustment_NoAdjustmentQuantity_CheckEndQuantitySameAsStartQuantity()
        Dim itm = New ItemAdjustment("123456", -5, CDec(12.99))
        Assert.AreEqual(itm.StartQuantity, itm.EndQuantity)
    End Sub
    <TestMethod()> Public Sub ItemAdjustment_AdjustmentQuantity_CheckAdjustmentValueIsPrice()
        Dim itm = New ItemAdjustment("123456", -5, CDec(12.99))
        itm.AdjustmentQuantity = 1
        Assert.AreEqual(itm.Price, itm.AdjustmentValue)
    End Sub
    <TestMethod()> Public Sub ItemAdjustment_MoreThanOneAdjustmentQuantity_CheckAdjustmentValueIsScaled()
        Dim itm = New ItemAdjustment("123456", -5, CDec(12.99))
        itm.AdjustmentQuantity = 2
        Assert.AreEqual(CDec(25.98), itm.AdjustmentValue)
    End Sub
    <TestMethod()> Public Sub ItemAdjustment_NoAdjustmentQuantity_CheckAdjustmentValueZero()
        Dim itm = New ItemAdjustment("123456", -5, CDec(12.99))
        Assert.AreEqual(CDec(0), itm.AdjustmentValue)
    End Sub
End Class
