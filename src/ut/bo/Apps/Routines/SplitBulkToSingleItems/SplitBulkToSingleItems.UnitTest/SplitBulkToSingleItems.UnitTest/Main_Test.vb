﻿Imports System.Data
Imports SplitBulkToSingleItems.Interfaces
Imports SplitBulkToSingleItems.Implementations
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class Main_Test
    Private testContextInstance As TestContext

    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    '<ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    '    _stockRequiringSplitting.Columns.Add("SingleSku", System.Type.GetType("System.String"))
    '    _stockRequiringSplitting.Columns.Add("SinglePrice", System.Type.GetType("System.Decimal"))
    '    _stockRequiringSplitting.Columns.Add("SingleOnHand", System.Type.GetType("System.Integer"))
    '    _stockRequiringSplitting.Columns.Add("PackSku", System.Type.GetType("System.String"))
    '    _stockRequiringSplitting.Columns.Add("PackPrice", System.Type.GetType("System.Decimal"))
    '    _stockRequiringSplitting.Columns.Add("PackOnHand", System.Type.GetType("System.Integer"))
    '    _stockRequiringSplitting.Columns.Add("ItemPackSize", System.Type.GetType("System.Integer"))

    '    _stockRequiringSplittingRow("SingleSku") = "107069"
    '    _stockRequiringSplittingRow("SinglePrice") = CDec(2.81)
    '    _stockRequiringSplittingRow("SingleOnHand") = 19
    '    _stockRequiringSplittingRow("PackSku") = "107068"
    '    _stockRequiringSplittingRow("PackPrice") = CDec(17.95)
    '    _stockRequiringSplittingRow("PackOnHand") = 31
    '    _stockRequiringSplittingRow("ItemPackSize") = 8
    '    _stockRequiringSplitting.Rows.Add(_stockRequiringSplittingRow)

    '    '        _stockRequiringSplittingRow = _evtchg.NewRow


    'End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '

#End Region

    <TestMethod()> _
    Public Sub GetStockToSplit_ReturnsOneRow()
        Dim dt As DataTable
        Dim testMain As New Main_WithSingleRow(1, 1, 1, "asd")
        dt = testMain.GetStockToSplit()

        Assert.AreEqual(1, dt.Rows.Count)
    End Sub

    <TestMethod()> _
    Public Sub GetStockToSplit_ReturnsMoreThanOneRow()
        Dim dt As DataTable
        Dim testMain As New Main_WithMultiRow
        dt = testMain.GetStockToSplit()

        Assert.IsTrue(dt.Rows.Count > 1)
    End Sub

    
    <TestMethod()> _
    Public Sub ParseParameters_ConnectionStringParameterSupplied_ConnectionStringValueSet()
        Const connectionString As String = "TestConnection"
        Dim main As New Main(1, 1, 1, connectionString)

        Assert.AreEqual(connectionString, main._connectionString)
    End Sub

    Protected Class Main_WithSingleRow
        Inherits Main

        Private StockRequiringSplitting As New DataTable("usp_GetRelatedItemsRequiringAdjustments")
        Private _stockRequiringSplittingRow As DataRow = StockRequiringSplitting.NewRow

        Public Sub New(ByVal userId As Integer, ByVal workstationId As Integer, ByVal securityLevel As Integer, _
               ByVal RunParameters As String)
            _userId = userId.ToString("000")
            _connectionString = RunParameters

            _environment = GetEnvironment()
        End Sub

        Friend Overrides Function GetStockToSplit() As DataTable
            StockRequiringSplitting.Columns.Add("SingleSku", Type.GetType("System.String"))
            StockRequiringSplitting.Columns.Add("SinglePrice", Type.GetType("System.Decimal"))
            StockRequiringSplitting.Columns.Add("SingleOnHand", Type.GetType("System.Int32"))
            StockRequiringSplitting.Columns.Add("PackSku", Type.GetType("System.String"))
            StockRequiringSplitting.Columns.Add("PackPrice", Type.GetType("System.Decimal"))
            StockRequiringSplitting.Columns.Add("PackOnHand", Type.GetType("System.Int32"))
            StockRequiringSplitting.Columns.Add("ItemPackSize", Type.GetType("System.Int32"))

            _stockRequiringSplittingRow("SingleSku") = "107069"
            _stockRequiringSplittingRow("SinglePrice") = CDec(2.81)
            _stockRequiringSplittingRow("SingleOnHand") = 19
            _stockRequiringSplittingRow("PackSku") = "107068"
            _stockRequiringSplittingRow("PackPrice") = CDec(17.95)
            _stockRequiringSplittingRow("PackOnHand") = 31
            _stockRequiringSplittingRow("ItemPackSize") = 8
            StockRequiringSplitting.Rows.Add(_stockRequiringSplittingRow)
            Return StockRequiringSplitting
        End Function
    End Class

    Protected Class Main_WithMultiRow
        Inherits Main

        Private StockRequiringSplitting As New DataTable("usp_GetRelatedItemsRequiringAdjustments")
        Private _stockRequiringSplittingRow As DataRow = StockRequiringSplitting.NewRow

        Friend Overrides Function GetStockToSplit() As DataTable
            StockRequiringSplitting.Columns.Add("SingleSku", Type.GetType("System.String"))
            StockRequiringSplitting.Columns.Add("SinglePrice", Type.GetType("System.Decimal"))
            StockRequiringSplitting.Columns.Add("SingleOnHand", Type.GetType("System.Int32"))
            StockRequiringSplitting.Columns.Add("PackSku", Type.GetType("System.String"))
            StockRequiringSplitting.Columns.Add("PackPrice", Type.GetType("System.Decimal"))
            StockRequiringSplitting.Columns.Add("PackOnHand", Type.GetType("System.Int32"))
            StockRequiringSplitting.Columns.Add("ItemPackSize", Type.GetType("System.Int32"))

            _stockRequiringSplittingRow("SingleSku") = "107069"
            _stockRequiringSplittingRow("SinglePrice") = CDec(2.81)
            _stockRequiringSplittingRow("SingleOnHand") = 19
            _stockRequiringSplittingRow("PackSku") = "107068"
            _stockRequiringSplittingRow("PackPrice") = CDec(17.95)
            _stockRequiringSplittingRow("PackOnHand") = 31
            _stockRequiringSplittingRow("ItemPackSize") = 8
            StockRequiringSplitting.Rows.Add(_stockRequiringSplittingRow)

            _stockRequiringSplittingRow = StockRequiringSplitting.NewRow

            _stockRequiringSplittingRow("SingleSku") = "120003"
            _stockRequiringSplittingRow("SinglePrice") = CDec(5.49)
            _stockRequiringSplittingRow("SingleOnHand") = -4
            _stockRequiringSplittingRow("PackSku") = "120141"
            _stockRequiringSplittingRow("PackPrice") = CDec(14.99)
            _stockRequiringSplittingRow("PackOnHand") = 9
            _stockRequiringSplittingRow("ItemPackSize") = 4
            StockRequiringSplitting.Rows.Add(_stockRequiringSplittingRow)

            Return StockRequiringSplitting
        End Function
    End Class
End Class


