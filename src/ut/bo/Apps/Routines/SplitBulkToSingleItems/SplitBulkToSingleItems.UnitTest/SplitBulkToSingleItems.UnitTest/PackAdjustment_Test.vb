﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports SplitBulkToSingleItems.Structures
Imports SplitBulkToSingleItems.Implementations
Imports SplitBulkToSingleItems.Interfaces
Imports System.Data.SqlClient

<TestClass()> Public Class PackAdjustmentTest

    Private _testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return _testContextInstance
        End Get
        Set(ByVal value As TestContext)
            _testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub PackAdjustment_BreakDownSinglePack_SingleStockGoesUpByPackSize()
        Dim packAdjuster As PackAdjuster = GetSinglePackAdjuster()
        Dim rep As IPackSplitOutput = New TestStockRepository()
        packAdjuster.SplitPacks(rep)

        Assert.AreEqual(3, packAdjuster.SingleItem.EndQuantity)
    End Sub

    <TestMethod()> Public Sub PackAdjustment_BreakDownSinglePack_BulkStockGoesDownByOne()
        Dim packAdjuster As PackAdjuster = GetSinglePackAdjuster()
        Dim rep As IPackSplitOutput = New TestStockRepository()
        packAdjuster.SplitPacks(rep)

        Assert.AreEqual(30, packAdjuster.BulkItem.EndQuantity)
    End Sub

    <TestMethod()> Public Sub PackAdjustment_BreakDownDoublePack_SingleStockGoesUpByDoublePackSizes()
        Dim packAdjuster As PackAdjuster = GetTwoPackAdjuster()
        Dim rep As IPackSplitOutput = New TestStockRepository()
        packAdjuster.SplitPacks(rep)

        Assert.AreEqual(3, packAdjuster.SingleItem.EndQuantity)
    End Sub

    <TestMethod()> Public Sub PackAdjustment_BreakDownDoublePack_BulkStockGoesDownByTwo()
        Dim packAdjuster As PackAdjuster = GetTwoPackAdjuster()
        Dim rep As IPackSplitOutput = New TestStockRepository()
        packAdjuster.SplitPacks(rep)

        Assert.AreEqual(29, packAdjuster.BulkItem.EndQuantity)
    End Sub

    <TestMethod()> Public Sub PackAdjustment_SaveCalled()
        Dim packAdjuster As PackAdjuster = GetSinglePackAdjuster()
        Dim rep As New TestStockRepository()
        packAdjuster.SplitPacks(rep)

        Assert.IsTrue(rep.SaveWasCalled)
    End Sub

    <TestMethod()> Public Sub NumberOfPacks_NegativeStockLessThanPackSize_Returns1()
        Dim singleItem = New ItemAdjustment("107069", -1, CDec(2.81))
        Dim bulkItem = New ItemAdjustment("107068", 1, CDec(17.95))
        Const itemsPerPack As Integer = 2

        Dim packAdjuster As New PackAdjuster(singleItem, bulkItem, itemsPerPack, CInt("099"))

        Assert.AreEqual(1, packAdjuster.NumberOfPacks())
    End Sub
    <TestMethod()> Public Sub NumberOfPacks_NegativeStockEqualsPackSize_Returns1()
        Dim singleItem = New ItemAdjustment("107069", -2, CDec(2.81))
        Dim bulkItem = New ItemAdjustment("107068", 1, CDec(17.95))
        Const itemsPerPack As Integer = 2

        Dim packAdjuster As New PackAdjuster(singleItem, bulkItem, itemsPerPack, CInt("099"))

        Assert.AreEqual(1, packAdjuster.NumberOfPacks())
    End Sub

    <TestMethod()> Public Sub NumberOfPacks_NegativeStockBetweenPackSizeAndTwoPackSizes_Return2()
        Dim singleItem = New ItemAdjustment("107069", -3, CDec(2.81))
        Dim bulkItem = New ItemAdjustment("107068", 2, CDec(17.95))
        Const itemsPerPack As Integer = 2

        Dim packAdjuster As New PackAdjuster(singleItem, bulkItem, itemsPerPack, CInt("099"))

        Assert.AreEqual(2, packAdjuster.NumberOfPacks())
    End Sub

    <TestMethod()> Public Sub NumberOfPacks_NegativeStockBetweenTwoAndThreePackSizes_Return3()
        Dim singleItem = New ItemAdjustment("107069", -5, CDec(2.81))
        Dim bulkItem = New ItemAdjustment("107068", 2, CDec(17.95))
        Const itemsPerPack As Integer = 2

        Dim packAdjuster As New PackAdjuster(singleItem, bulkItem, itemsPerPack, CInt("099"))

        Assert.AreEqual(3, packAdjuster.NumberOfPacks())
    End Sub

    <TestMethod()> Public Sub PackAdjustment_SaveAdjustedPack()
        Dim packAdjuster As PackAdjuster = GetSinglePackAdjuster()
        Dim rep As IPackSplitOutput = New TestStockRepository()
        packAdjuster.SplitPacks(rep)

        Assert.AreEqual(30, packAdjuster.BulkItem.EndQuantity)
    End Sub


    Friend Function GetSinglePackAdjuster() As PackAdjuster

        Dim singleItem = New ItemAdjustment("107069", -5, CDec(2.81))
        Dim bulkItem = New ItemAdjustment("107068", 31, CDec(17.95))
        Const itemsPerPack As Integer = 8
        Return New PackAdjuster(singleItem, bulkItem, itemsPerPack, CInt("099"))
    End Function
    Friend Function GetTwoPackAdjuster() As PackAdjuster

        Dim singleItem = New ItemAdjustment("107069", -13, CDec(2.81))
        Dim bulkItem = New ItemAdjustment("107068", 31, CDec(17.95))
        Const itemsPerPack As Integer = 8
        Return New PackAdjuster(singleItem, bulkItem, itemsPerPack, CInt("099"))
    End Function
End Class

Public Class TestStockRepository
    Implements IPackSplitOutput

    Friend SaveWasCalled As Boolean = False

    Public Sub Save(ByVal packAdjustment As SplitBulkToSingleItems.Implementations.PackAdjuster, ByVal userId As String) Implements SplitBulkToSingleItems.Interfaces.IPackSplitOutput.Save
        SaveWasCalled = True
    End Sub
End Class

