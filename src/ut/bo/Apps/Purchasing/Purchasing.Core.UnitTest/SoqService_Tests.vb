﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Purchasing.Core

<TestClass()> _
Public Class SoqService_Tests

    Private testContextInstance As TestContext
    Private Const slowLumpyKeyValueOne As Integer = 1
    Private Const slowLumpyKeyValueTwo As Integer = 2
    Private Const slowLumpyKeyValueThree As Integer = 3
    Private Const slowLumpyKeyValueFour As Integer = 4
    Private Const slowLumpyKeyValueFive As Integer = 5
    Private Const slowLumpyKeyValueSix As Integer = 6
    Private Const slowLumpyKeyValueSeven As Integer = 7
    Private Const slowLumpyKeyValueEight As Integer = 8
    Private Const slowLumpyKeyValueNine As Integer = 9
    Private Const slowLumpyKeyValueTen As Integer = 10
    Private Const slowLumpyKeyValueEleven As Integer = 11
    Private Const slowLumpyKeyValueTwelve As Integer = 12
    Private Const slowLumpyKeyValueThirteen As Integer = 13

    Private Const slowLumpyKeyValueFourteen As Integer = 14

    Private Const slowLumpyValueOne As Integer = 0
    Private Const slowLumpyValueTwo As Integer = 0
    Private Const slowLumpyValueThree As Integer = 0
    Private Const slowLumpyValueFour As Integer = 1
    Private Const slowLumpyValueFive As Integer = 1
    Private Const slowLumpyValueSix As Integer = 2
    Private Const slowLumpyValueSeven As Integer = 2
    Private Const slowLumpyValueEight As Integer = 3
    Private Const slowLumpyValueNine As Integer = 3
    Private Const slowLumpyValueTen As Integer = 4
    Private Const slowLumpyValueEleven As Integer = 4
    Private Const slowLumpyValueTwelve As Integer = 5
    Private Const slowLumpyValueThirteen As Integer = 5

    Private Const zeroDemandLumpyKeyValueOne As Integer = 1
    Private Const zeroDemandLumpyKeyValueTwo As Integer = 2
    Private Const zeroDemandLumpyKeyValueThree As Integer = 3
    Private Const zeroDemandLumpyKeyValueFour As Integer = 4
    Private Const zeroDemandLumpyKeyValueFive As Integer = 5
    Private Const zeroDemandLumpyKeyValueSix As Integer = 6
    Private Const zeroDemandLumpyKeyValueSeven As Integer = 7
    Private Const zeroDemandLumpyKeyValueEight As Integer = 8
    Private Const zeroDemandLumpyKeyValueNine As Integer = 9
    Private Const zeroDemandLumpyKeyValueTen As Integer = 10
    Private Const zeroDemandLumpyKeyValueEleven As Integer = 11
    Private Const zeroDemandLumpyKeyValueTwelve As Integer = 12
    Private Const zeroDemandLumpyKeyValueThirteen As Integer = 13

    Private Const zeroDemandLumpyKeyValueFourteen As Integer = 14

    Private Const zeroDemandLumpyValueOne As Integer = 0
    Private Const zeroDemandLumpyValueTwo As Integer = 0
    Private Const zeroDemandLumpyValueThree As Integer = 0
    Private Const zeroDemandLumpyValueFour As Integer = 3
    Private Const zeroDemandLumpyValueFive As Integer = 3
    Private Const zeroDemandLumpyValueSix As Integer = 4
    Private Const zeroDemandLumpyValueSeven As Integer = 4
    Private Const zeroDemandLumpyValueEight As Integer = 5
    Private Const zeroDemandLumpyValueNine As Integer = 5
    Private Const zeroDemandLumpyValueTen As Integer = 6
    Private Const zeroDemandLumpyValueEleven As Integer = 6
    Private Const zeroDemandLumpyValueTwelve As Integer = 7
    Private Const zeroDemandLumpyValueThirteen As Integer = 7

    Private tst As Integer
    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "SlowLumpy Tests"

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement1_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueOne)
        If tst.Equals(slowLumpyValueOne) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement2_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueTwo)
        If tst.Equals(slowLumpyValueTwo) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement3_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueThree)
        If tst.Equals(slowLumpyValueThree) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement4_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueFour)
        If tst.Equals(slowLumpyValueFour) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement5_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueFive)
        If tst.Equals(slowLumpyValueFive) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement6_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueSix)
        If tst.Equals(slowLumpyValueSix) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement7_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueSeven)
        If tst.Equals(slowLumpyValueSeven) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement8_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueEight)
        If tst.Equals(slowLumpyValueEight) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement9_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueNine)
        If tst.Equals(slowLumpyValueNine) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement10_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueTen)
        If tst.Equals(slowLumpyValueTen) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement11_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueEleven)
        If tst.Equals(slowLumpyValueEleven) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement12_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueTwelve)
        If tst.Equals(slowLumpyValueTwelve) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsSlowLumpy_QueryElement13_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueThirteen)
        If tst.Equals(slowLumpyValueThirteen) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    ' No 14 key in SlowLumpyCollection
    <TestMethod(), ExpectedException(GetType(System.Collections.Generic.KeyNotFoundException))> _
    Public Sub SoqConstantsSlowLumpy_QueryElement3_ValuesMatch_InvokedWithNull()

        tst = (New SoqConstantsFactory).GetImplementation().SlowLumpyCheck(slowLumpyKeyValueFourteen)
    End Sub

#End Region

#Region "ZeroDemandLumpy Tests"

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement4_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueFour)
        If tst.Equals(zeroDemandLumpyValueFour) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement5_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueFive)
        If tst.Equals(zeroDemandLumpyValueFive) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement6_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueSix)
        If tst.Equals(zeroDemandLumpyValueSix) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement7_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueSeven)
        If tst.Equals(zeroDemandLumpyValueSeven) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement8_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueEight)
        If tst.Equals(zeroDemandLumpyValueEight) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement9_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueNine)
        If tst.Equals(zeroDemandLumpyValueNine) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement10_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueTen)
        If tst.Equals(zeroDemandLumpyValueTen) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> Public Sub SoqConstantsZeroDemandLumpy_QueryElement11_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueEleven)
        If tst.Equals(zeroDemandLumpyValueEleven) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement12_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueTwelve)
        If tst.Equals(zeroDemandLumpyValueTwelve) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    <TestMethod()> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement13_ExpectedValuesMatch()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueThirteen)
        If tst.Equals(zeroDemandLumpyValueThirteen) Then
            Assert.IsTrue(True)
        Else
            Assert.IsTrue(False)
        End If
    End Sub

    ' No 14 key in ZeroDemandLumpyCollection

    <TestMethod(), ExpectedException(GetType(System.Collections.Generic.KeyNotFoundException))> _
    Public Sub SoqConstantsZeroDemandLumpy_QueryElement3_ValuesMatch_InvokedWithNull()

        tst = (New SoqConstantsFactory).GetImplementation().ZeroDemandLumpyCheck(zeroDemandLumpyKeyValueFourteen)
    End Sub

#End Region
End Class
