﻿<TestClass()> Public Class BusinessLockIWUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Class - DisabledBusinessLockIW"

    Private Class TestDisabledBusinessLockIW
        Inherits DisabledBusinessLockIW

        Private _ExecuteCreateLockCalled As Boolean
        Private _ExecuteRemoveLockCalled As Boolean

        Public ReadOnly Property ExecuteCreateLockCalled() As Boolean
            Get
                Return _ExecuteCreateLockCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteRemoveLockCalled() As Boolean
            Get
                Return _ExecuteRemoveLockCalled
            End Get
        End Property

        Protected Overrides Function ExecuteCreateLock(ByVal WorkStationID As Integer, _
                                                       ByVal CurrentDateAndTime As Date, _
                                                       ByVal AssemblyName As String, _
                                                       ByVal ParameterValues As String) As Integer

            _ExecuteCreateLockCalled = True

        End Function

        Protected Overrides Sub ExecuteRemoveLock(ByVal RecordID As Integer)

            _ExecuteRemoveLockCalled = True

        End Sub

    End Class

#End Region

#Region "Test Class - EnabledBusinessLockIW"

    Private Class TestEnabledBusinessLockIW
        Inherits EnabledBusinessLockIW

        Private _ExecuteCreateLockCalled As Boolean
        Private _ExecuteRemoveLockCalled As Boolean

        Public ReadOnly Property ExecuteCreateLockCalled() As Boolean
            Get
                Return _ExecuteCreateLockCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteRemoveLockCalled() As Boolean
            Get
                Return _ExecuteRemoveLockCalled
            End Get
        End Property

        Protected Overrides Function ExecuteCreateLock(ByVal WorkStationID As Integer, _
                                                       ByVal CurrentDateAndTime As Date, _
                                                       ByVal AssemblyName As String, _
                                                       ByVal ParameterValues As String) As Integer

            _ExecuteCreateLockCalled = True

        End Function

        Protected Overrides Sub ExecuteRemoveLock(ByVal RecordID As Integer)

            _ExecuteRemoveLockCalled = True

        End Sub

    End Class

#End Region

#Region "Unit Test - Factory"

    <TestMethod()> Public Sub RequirementSwitchOn_ReturnEnabledBusinessLockIW()

        Dim BusinessLockIW As IBusinessLockIW

        ArrangeRequirementSwitchDependency(True)
        BusinessLockIW = (New BusinessLockIWFactory).GetImplementation

        Assert.AreEqual("Purchasing.Core.EnabledBusinessLockIW", BusinessLockIW.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementSwitchOff_ReturnDisabledBusinessLockIW()

        Dim BusinessLockIW As IBusinessLockIW

        ArrangeRequirementSwitchDependency(False)
        BusinessLockIW = (New BusinessLockIWFactory).GetImplementation
        Assert.AreEqual("Purchasing.Core.DisabledBusinessLockIW", BusinessLockIW.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - DisabledBusinessLockIW"

    <TestMethod()> Public Sub DisabledBusinessLockIW_CreateLock_DoesNotCallInternalFunctionCreateLock()

        Dim Disabled As New TestDisabledBusinessLockIW

        Disabled.CreateLock(Nothing, Nothing, Nothing, Nothing)

        Assert.IsFalse(Disabled.ExecuteCreateLockCalled)

    End Sub

    <TestMethod()> Public Sub DisabledBusinessLockIW_CreateLock_ReturnRecordIdZero()

        Dim Disabled As New TestDisabledBusinessLockIW

        Assert.AreEqual(0, Disabled.CreateLock(Nothing, Nothing, Nothing, Nothing))

    End Sub

    <TestMethod()> Public Sub DisabledBusinessLockIW_RemoveLock_DoesNotCallInternalFunctionRemoveLock()

        Dim Disabled As New TestDisabledBusinessLockIW

        Disabled.RemoveLock(Nothing)

        Assert.IsFalse(Disabled.ExecuteRemoveLockCalled)

    End Sub

#End Region

#Region "Unit Test - EnabledBusinessLockIW"

    <TestMethod()> Public Sub EnabledBusinessLockIW_CreateLock_DoesCallInternalFunctionCreateLock()

        Dim Enabled As New TestEnabledBusinessLockIW

        Enabled.CreateLock(Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(Enabled.ExecuteCreateLockCalled)

    End Sub

    <TestMethod()> Public Sub EnabledBusinessLockIW_CreateLock_ReturnRecordIdOne()

        'not applicable

    End Sub

    <TestMethod()> Public Sub EnabledBusinessLockIW_RemoveLock_DoesCallInternalFunctionRemoveLock()

        Dim Enabled As New TestEnabledBusinessLockIW

        Enabled.RemoveLock(Nothing)

        Assert.IsTrue(Enabled.ExecuteRemoveLockCalled)

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class