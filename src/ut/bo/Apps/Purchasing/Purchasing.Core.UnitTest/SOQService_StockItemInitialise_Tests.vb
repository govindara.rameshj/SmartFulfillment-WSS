﻿<TestClass()> Public Class SOQService_StockItemInitialise_Tests

    Private testContextInstance As TestContext
    Private stockItemInstance As New Stock

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassStockItemDemandPatternNew()

        Dim NewVersionOnly As ISoqStockItemInitialise
        Dim Factory As IBaseFactory(Of ISoqStockItemInitialise)

        Factory = New SoqStockItemInitialiseFactory
        NewVersionOnly = (New SoqStockItemInitialiseFactory).GetImplementation

        Assert.AreEqual("Purchasing.Core.StockItemDemandPatternNew", NewVersionOnly.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub NewClassStockItem_DemandPatternSettoNewReturnsTrue()

        stockItemInstance.DemandPattern = "New"
        Assert.IsTrue((New SoqStockItemInitialiseFactory).GetImplementation.SoqGetStockItemDemandPattern(stockItemInstance))

    End Sub

    <TestMethod()> Public Sub NewClassStockItem_DemandPatternSettoBlankReturnsTrue()

        stockItemInstance.DemandPattern = ""
        Assert.IsTrue((New SoqStockItemInitialiseFactory).GetImplementation.SoqGetStockItemDemandPattern(stockItemInstance))
    End Sub

    <TestMethod()> Public Sub NewClassStockItem_DemandPatternSettoTrendDownAndPeriodDemandSettoZeroReturnsTrue()

        stockItemInstance.DemandPattern = "TrendDown"
        stockItemInstance.PeriodDemand = 0
        Assert.IsTrue((New SoqStockItemInitialiseFactory).GetImplementation.SoqGetStockItemDemandPattern(stockItemInstance))

    End Sub

    <TestMethod()> Public Sub NewClassStockItem_DemandPatternSettoTrendDownAndPeriodDemandNotSettoZeroReturnsFalse()

        stockItemInstance.DemandPattern = "TrendDown"
        stockItemInstance.PeriodDemand = 12
        Assert.IsFalse((New SoqStockItemInitialiseFactory).GetImplementation.SoqGetStockItemDemandPattern(stockItemInstance))

    End Sub

    <TestMethod()> Public Sub NewClassStockItem_DemandPatternSettoSupercededReturnsTrue()

        stockItemInstance.DemandPattern = "Superceded"
        Assert.IsTrue((New SoqStockItemInitialiseFactory).GetImplementation.SoqGetStockItemDemandPattern(stockItemInstance))

    End Sub

    <TestMethod()> Public Sub NewClassStockItem_DemandPatternSettoObsoleteReturnsTrue()

        stockItemInstance.DemandPattern = "Obsolete"
        Assert.IsTrue((New SoqStockItemInitialiseFactory).GetImplementation.SoqGetStockItemDemandPattern(stockItemInstance))

    End Sub

    <TestMethod()> Public Sub NewClassStockItem_DemandPatternSettoFastReturnsFalse()

        stockItemInstance.DemandPattern = "Fast"
        Assert.IsFalse((New SoqStockItemInitialiseFactory).GetImplementation.SoqGetStockItemDemandPattern(stockItemInstance))

    End Sub

    <TestMethod()> Public Sub NewClassStockItem_CallsUpdateOrderLineReturnsTrue()

        Assert.IsTrue((New SoqStockItemInitialiseFactory).GetImplementation.SoqUpdateOrderLevel)

    End Sub

End Class