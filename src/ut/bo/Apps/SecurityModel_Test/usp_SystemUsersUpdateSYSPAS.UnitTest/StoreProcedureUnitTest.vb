﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class StoreProcedureUnitTest

    Private testContextInstance As TestContext

    'Common
    Private Const STOREDPROCEDURENAME As String = "usp_SystemUsersUpdateSYSPAS"

    'Data User to Update or Insert by the store procedure
    Private Const EMPLOYEE_CODE As String = "999"
    Private Const EMPLOYEE_NAME As String = "Mr Test"
    Private Const INITIALS As String = "TT"
    Private Const POSITION As String = "Manager"
    Private Const PAYROLL_ID As String = "123456"
    Private Const PASSWORD As String = "12345"
    Private Const PASSEXPIRES As Date = #5/31/2012#
    Private Const IS_SUPERVISOR As Boolean = True
    Private Const SUPER_PASSWORD As String = "54321"
    Private Const SUPER_PASSEXPIRES As Date = #7/31/2012#
    Private Const OUTLET As String = "00"
    Private Const IS_DELETED As Boolean = False
    Private Const DELETED_DATE As Date = #1/1/1900#
    Private Const DELETED_TIME As String = ""
    Private Const DELETED_BY As String = ""
    Private Const DELETED_WHERE As String = ""
    Private Const TILL_NAME As String = "Mr Test"
    Private Const DEFAULT_AMOUNT As String = "100.00"
    Private Const LANGUAGE_CODE As String = "000"
    Private Const IS_MANAGER As Boolean = True
    Private Const SECURITY_PROFILE As String = "110"

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

    Public Sub New()

    End Sub


    ' Use ClassCleanup to run code after all tests in a class have run
    <ClassCleanup()> Public Shared Sub MyClassCleanup()

        Dim SB As New StringBuilder

        SB.Append("DELETE ")
        SB.Append("FROM SYSPAS ")
        SB.Append("WHERE EEID='" & EMPLOYEE_CODE & "'")

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using
    End Sub
#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Stored Procedure - Unit Test"

    <TestMethod()> Public Sub CheckStoredProcedureExistsOK()
        'Arrange
        Dim boolSucess As Boolean

        'Act
        boolSucess = CheckForStoredProcedure()

        'Assert
        Assert.IsTrue(boolSucess)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_EEID_IsEqualTo_EmployeeCode()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("EEID")


        Assert.AreEqual(EMPLOYEE_CODE, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_NAME_IsEqualTo_EmployeeName()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("NAME")


        Assert.AreEqual(EMPLOYEE_NAME, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_INIT_IsEqualTo_Initials()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("INIT")


        Assert.AreEqual(INITIALS, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_POSI_IsEqualTo_Position()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("POSI")


        Assert.AreEqual(POSITION, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_EPAY_IsEqualTo_PayrollId()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("EPAY")


        Assert.AreEqual(PAYROLL_ID, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_PASS_IsEqualTo_Password()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("PASS")


        Assert.AreEqual(PASSWORD, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_DPAS_IsEqualTo_PasswordExpires()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("DPAS")


        Assert.AreEqual(PASSEXPIRES, DataRead)

    End Sub


    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_SUPV_IsEqualTo_IsSupervisor()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("SUPV")


        Assert.AreEqual(IS_SUPERVISOR, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_AUTH_IsEqualTo_SuperPassword()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("AUTH")


        Assert.AreEqual(SUPER_PASSWORD, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_DAUT_IsEqualTo_SuperPasswordExpires()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("DAUT")


        Assert.AreEqual(SUPER_PASSEXPIRES, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_OUTL_IsEqualTo_Outlet()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("OUTL")


        Assert.AreEqual(OUTLET, DataRead)

    End Sub


    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_DELC_IsEqualTo_IsDeleted()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("DELC")


        Assert.AreEqual(IS_DELETED, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_DDAT_IsEqualTo_DeletedDate()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("DDAT")


        Assert.AreEqual(DELETED_DATE, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_DTIM_IsEqualTo_DeletedTime()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("DTIM")


        Assert.AreEqual(DELETED_TIME, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_DWHO_IsEqualTo_DeletedBy()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("DWHO")


        Assert.AreEqual(DELETED_BY, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_DOUT_IsEqualTo_DeletedWhere()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("DOUT")


        Assert.AreEqual(DELETED_WHERE, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_TNAM_IsEqualTo_TillName()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("TNAM")


        Assert.AreEqual(TILL_NAME, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_DFAM_IsEqualTo_DefaultAmount()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("DFAM")


        Assert.AreEqual(DEFAULT_AMOUNT, DataRead.ToString.Trim)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_LANG_IsEqualTo_Language()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("LANG")


        Assert.AreEqual(LANGUAGE_CODE, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_MANA_IsEqualTo_IsManager()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("MANA")


        Assert.AreEqual(IS_MANAGER, DataRead)

    End Sub

    <TestMethod()> Public Sub StoredProcedure_Test_SYSPASS_SECL77_IsEqualTo_SecurityProfileId()

        Dim StoredProcedureCount As Integer
        Dim DataRead As Object

        StoredProcedureCount = StoredProcedureCall()
        DataRead = DynamicSequelSelectByField("SECL77")

        Assert.AreEqual(SECURITY_PROFILE, DataRead, "New Security Model Not Presently Available")

    End Sub


#Region "Private Procedures And Functions"

    Private Function DynamicSequelSelect() As DataTable

        Dim SB As New StringBuilder

        SB.Append("Select EmpolyeeCode = [EEID]")
        SB.Append(",Name = [NAME] ")
        SB.Append(",Initials = [INIT]")
        SB.Append(",Position = [POSI] ")
        SB.Append(",PayrollId = [EPAY] ")
        SB.Append(",Password = [PASS]")
        SB.Append(",IsSupervisor = [SUPV]")
        SB.Append(",SuperPassword = [AUTH]")
        SB.Append(",SuperPasswordExpires = [DAUT]")
        SB.Append(",Outlet = [OUTL]")
        SB.Append(",IsDeleted = [DELC]")
        SB.Append(",DeletedDate = [DDAT] ")
        SB.Append(",DeletedTime = [DTIM] ")
        SB.Append(",DeletedBy = [DWHO] ")
        SB.Append(",DeletedWhere = [DOUT] ")
        SB.Append(",TillReceiptName = [TNAM] ")
        SB.Append(",DefaultAmount = [DFAM] ")
        SB.Append(",LanguageCode = [LANG] ")
        SB.Append(",IsManager = [MANA] ")
        SB.Append(",SecurityProfileId =[SECL77] ")
        SB.Append("FROM SYSPAS")
        SB.Append("WHERE EEID='" & EMPLOYEE_CODE & "'")

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                Return com.ExecuteDataTable

            End Using
        End Using
        Return Nothing

    End Function

    Private Function DynamicSequelSelectByField(ByVal Field As String) As Object

        Dim SB As New StringBuilder

        SB.Append("Select [" & Field & "] ")
        SB.Append("FROM SYSPAS ")
        SB.Append("WHERE EEID='" & EMPLOYEE_CODE & "'")

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                Return com.ExecuteValue

            End Using
        End Using
        Return Nothing

    End Function


    Private Function StoredProcedureCall() As Integer

        Using con As New Connection
            Using com As Command = con.NewCommand(STOREDPROCEDURENAME)
                com.AddParameter("sp_EmployeeCode", EMPLOYEE_CODE)
                com.AddParameter("sp_Name", EMPLOYEE_NAME)
                com.AddParameter("sp_Initials", INITIALS)
                com.AddParameter("sp_Position", POSITION)
                com.AddParameter("sp_PayrollId", PAYROLL_ID)
                com.AddParameter("sp_Password", PASSWORD)
                com.AddParameter("sp_PasswordExpires", PASSEXPIRES)
                com.AddParameter("sp_IsSupervisor", IS_SUPERVISOR)
                com.AddParameter("sp_SuperPassword", SUPER_PASSWORD)
                com.AddParameter("sp_SuperPasswordExpires", SUPER_PASSEXPIRES)
                com.AddParameter("sp_Outlet", OUTLET)
                com.AddParameter("sp_IsDeleted", IS_DELETED)
                com.AddParameter("sp_DeletedDate", "")
                com.AddParameter("sp_DeletedTime", "")
                com.AddParameter("sp_DeletedBy", "")
                com.AddParameter("sp_DeletedWhere", "")
                com.AddParameter("sp_TillReceiptName", TILL_NAME)
                com.AddParameter("sp_DefaultAmount", DEFAULT_AMOUNT)
                com.AddParameter("sp_LanguageCode", LANGUAGE_CODE)
                com.AddParameter("sp_IsManager", IS_MANAGER)
                com.AddParameter("sp_SecurityProfileId", SECURITY_PROFILE)
                Return com.ExecuteNonQuery
            End Using
        End Using
        Return 0

    End Function

    Private Function CheckForStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim spCheck As DataTable

                    com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & STOREDPROCEDURENAME & "'"
                    spCheck = com.ExecuteDataTable
                    If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                        CheckForStoredProcedure = True
                    Else
                        CheckForStoredProcedure = False
                    End If
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & STOREDPROCEDURENAME & ".")
        End Try
    End Function

    


#End Region

#End Region


End Class
