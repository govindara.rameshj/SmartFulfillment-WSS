﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class FloatAssignVMWithComments_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "SetFloatAssignGridControl Tests"

    <TestMethod()> _
    Public Sub SetFloatAssignGridControl_Sets_FloatAssignGrid_Member_To_FloatAssignGrid_Parameter()
        Dim FAVM As New FloatAssignVMWithComments
        Dim expected As New FpSpread

        FAVM.SetFloatAssignGridControl(expected)
        Assert.AreSame(expected, FAVM._FloatAssignGrid)
    End Sub
#End Region

#Region "FloatAssignGridPopulate Tests"

    <TestMethod()> _
    Public Sub FloatAssignGridPopulate_AddsRowForComments()
        Dim FAVM As New TestFloatAssignVMWithComments_WithTestDataExposed
        Dim actual As String = String.Empty

        With FAVM
            .SetFloatAssignGridControl(New FpSpread)
            .FloatAssignGridFormat()
            .FloatAssignGridPopulate(1)
            With ._FloatAssignGrid.Sheets(0)
                If .Cells(FAVM._CommentsRowIndex, 0).Value IsNot Nothing Then
                    actual = .Cells(FAVM._CommentsRowIndex, 0).Value.ToString
                End If
            End With
        End With
        Assert.IsTrue(actual IsNot Nothing AndAlso String.Compare(actual, "Comments") = 0)
    End Sub

    <TestMethod()> _
    Public Sub FloatAssignGridPopulate_PopulatesCommentsCellWithTestComments()
        Dim FAVM As New TestFloatAssignVMWithComments_WithTestDataExposed
        Dim actual As String = String.Empty

        With FAVM
            .SetFloatAssignGridControl(New FpSpread)
            .FloatAssignGridFormat()
            .FloatAssignGridPopulate(1)
            With ._FloatAssignGrid.Sheets(0)
                If .Cells(FAVM._CommentsRowIndex, FAVM._CommentsColumnIndex).Value IsNot Nothing Then
                    actual = .Cells(FAVM._CommentsRowIndex, FAVM._CommentsColumnIndex).Value.ToString
                End If
            End With
        End With
        Assert.IsTrue(actual IsNot Nothing AndAlso String.Compare(actual, FAVM._TestComments) = 0, "Comments row not populated. Expected - " & FAVM._TestComments & "; actual - " & IIf(String.IsNullOrEmpty(actual), "nothing", actual).ToString & ".")
    End Sub
#End Region

#Region "FloatAssignGridSetupCommentsCell Tests"

    <TestMethod()> _
    Public Sub FloatAssignGridSetupCommentsCell_MakeEditableIsTrue_MakesCommentsCellEditable()
        Dim FAVM As New TestFloatAssignVMWithComments_WithTestDataExposed
        Dim actual As Boolean

        With FAVM
            .SetFloatAssignGridControl(New FpSpread)
            .FloatAssignGridFormat()
            .FloatAssignGridPopulate(1)
            .FloatAssignGridSetupCommentsCell(True)
            With ._FloatAssignGrid.Sheets(0)
                If .Cells(FAVM._CommentsRowIndex, FAVM._CommentsColumnIndex) IsNot Nothing Then
                    actual = .Cells(FAVM._CommentsRowIndex, FAVM._CommentsColumnIndex).Locked
                End If
            End With
        End With
        Assert.IsFalse(actual, "The comments cell is not editable")
    End Sub

    <TestMethod()> _
    Public Sub FloatAssignGridSetupCommentsCell_MakeEditableIsFalse_MakesCommentsCellNonEditable()
        Dim FAVM As New TestFloatAssignVMWithComments_WithTestDataExposed
        Dim actual As Boolean

        With FAVM
            .SetFloatAssignGridControl(New FpSpread)
            .FloatAssignGridFormat()
            .FloatAssignGridPopulate(1)
            .FloatAssignGridSetupCommentsCell(False)
            With ._FloatAssignGrid.Sheets(0)
                If .Cells(FAVM._CommentsRowIndex, FAVM._CommentsColumnIndex) IsNot Nothing Then
                    actual = .Cells(FAVM._CommentsRowIndex, FAVM._CommentsColumnIndex).Locked
                End If
            End With
        End With
        Assert.IsTrue(actual, "The comments cell is editable")
    End Sub

    <TestMethod()> _
    Public Sub FloatAssignGridSetupCommentsCell_MakeEditableIsTrue_LeavesCommentsCellContents()
        Dim FAVM As New TestFloatAssignVMWithComments_WithTestDataExposed
        Dim expected As String = ""
        Dim actual As String = ""

        With FAVM
            .SetFloatAssignGridControl(New FpSpread)
            .FloatAssignGridFormat()
            .FloatAssignGridPopulate(1)
            expected = ._TestComments
            .FloatAssignGridSetupCommentsCell(True)
            With ._FloatAssignGrid.Sheets(0)
                If .Cells(FAVM._CommentsRowIndex, FAVM._CommentsColumnIndex).Value IsNot Nothing Then
                    actual = .Cells(FAVM._CommentsRowIndex, FAVM._CommentsColumnIndex).Value.ToString
                End If
            End With
        End With
        Assert.AreEqual(expected, actual, "The comments cell contents have not been left alone")
    End Sub
#End Region

#Region "GetFloatAssignGridTotalsRowIndex Tests"

    <TestMethod()> _
    Public Sub GetFloatAssignGridTotalsRowIndex_Returns_CommentsRowIndex_Minus2()
        Dim CDVM As New FloatAssignVMWithComments
        Dim expected As Integer = -1

        With CDVM
            .SetFloatAssignGridControl(New FpSpread)
            .FloatAssignGridFormat()
            expected = ._CommentsRowIndex - 1
            Assert.AreEqual(expected, .GetFloatAssignGridTotalsRowIndex)
        End With
    End Sub
#End Region

#Region "AssignCommentsFromGrid Tests"

    '<TestMethod()> _
    'Public Sub AssignCommentsFromGrid_CopiesGridsCommentCellsContentsToFloatBankingParametersCommentsProperty()
    '    Dim FAVM As New TestFloatAssignVMWithComments_WithTestDataExposed
    '    Dim expected As String = ""
    '    Dim actual As String = ""
    '    Dim CheckCommentsAssigned As New Banking.Core.FloatBanking

    '    With FAVM
    '        .SetFloatAssignGridControl(New FpSpread)
    '        .FloatAssignGridFormat()
    '        .FloatAssignGridPopulate(1)
    '        expected = ._FloatAssignGrid.ActiveSheet.Cells(._CommentsRowIndex, ._CommentsColumnIndex).Value.ToString
    '        .FloatAssignGridSetupCommentsCell(True)
    '        .AssignCommentsFromGrid(CheckCommentsAssigned)
    '        actual = CheckCommentsAssigned.Comments
    '    End With
    '    Assert.AreEqual(expected, actual, "The comments cell contents were not copied from the grid to the float banking business object")
    'End Sub
#End Region

#Region "FloatAssignGridActiveCellIsCommentsCell Tests"

    <TestMethod()> _
    Public Sub FloatAssignGridActiveCellIsCommentsCell_CommentsCellIsNotActiveCell_ReturnsFalse()
        Dim FAVM As New TestFloatAssignVMWithComments_WithTestDataExposed

        With FAVM
            .SetFloatAssignGridControl(New FpSpread)
            .FloatAssignGridFormat()
            .FloatAssignGridPopulate(1)
            SpreadCellMakeActive(._FloatAssignGrid.ActiveSheet, ._CommentsRowIndex - 1, ._CommentsColumnIndex)
            Assert.IsFalse(.FloatAssignGridActiveCellIsCommentsCell)
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatAssignGridActiveCellIsCommentsCell_CommentsCellIsActiveCell_ReturnsTrue()
        Dim FAVM As New TestFloatAssignVMWithComments_WithTestDataExposed

        With FAVM
            .SetFloatAssignGridControl(New FpSpread)
            .FloatAssignGridFormat()
            .FloatAssignGridPopulate(1)
            SpreadCellMakeActive(._FloatAssignGrid.ActiveSheet, ._CommentsRowIndex, ._CommentsColumnIndex)
            Assert.IsTrue(.FloatAssignGridActiveCellIsCommentsCell)
        End With
    End Sub
#End Region

#Region "SetFloatAssignForm Tests"

    <TestMethod()> _
    Public Sub SetFloatAssignFormControl_Sets_FloatAssignForm_Member_To_FloatAssignForm_Parameter()
        Dim FAVM As New FloatAssignVMWithComments
        Dim expected As New System.Windows.Forms.Form

        FAVM.SetFloatAssignForm(expected)
        Assert.AreSame(expected, FAVM._FloatAssignForm)
    End Sub
#End Region

#Region "FloatAssignFormFormat Tests"

    <TestMethod()> _
    Public Sub FloatAssignFormFormat_Sets_FloatAssignForm_Member_ToBe80Higher()
        Dim FAVM As New FloatAssignVMWithComments
        Dim NewForm As New System.Windows.Forms.Form
        Dim expected As Integer = NewForm.Height + 80

        With FAVM
            .SetFloatAssignForm(NewForm)
            .FloatAssignFormFormat()
            Assert.AreEqual(expected, ._FloatAssignForm.Height)
        End With
    End Sub
#End Region

#Region "Internals"
#End Region
End Class
