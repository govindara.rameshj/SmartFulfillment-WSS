﻿<TestClass()> Public Class SafeCheckReportUI_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestSafeCheckReportUIPrint
        Inherits SafeCheckReportUIPrint

        Friend Overrides Sub ConfigureReport()

            'do nothing

        End Sub

        Friend Overrides Sub ExecuteReport()

            'do nothing

        End Sub

    End Class

    Private Class TestSafeCheckReportUIView
        Inherits SafeCheckReportUIView

        Friend Overrides Sub ShowView()

            'do nothing

        End Sub

    End Class

#End Region

#Region "Unit Test - Requirement Test"

    <TestMethod()> Public Sub SafeCheckReportUIFactory_RequirementCheck_False_ReturnSafeCheckReportUIPrint()

        Dim Report As ISafeCheckReportUI

        ArrangeRequirementSwitchDependency(False)

        Report = (New SafeCheckReportUIFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.SafeCheckReportUIPrint", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeCheckReportUIFactory_RequirementCheck_True_ReturnSafeCheckReportUIView()

        Dim Report As ISafeCheckReportUI

        ArrangeRequirementSwitchDependency(True)

        Report = (New SafeCheckReportUIFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.SafeCheckReportUIView", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeCheckReportUISafeMaintenanceInfoFactory_RequirementCheck_False_ReturnSafeCheckReportUIHideInfo()

        Dim Report As ISafeCheckReportUI

        ArrangeRequirementSwitchDependency(False)

        Report = (New SafeCheckReportUISafeMaintenanceInfoFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.SafeCheckReportUIHideInfo", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeCheckReportUISafeMaintenanceInfoFactory_RequirementCheck_True_ReturnSafeCheckReportUIDisplayInfo()

        Dim Report As ISafeCheckReportUI

        ArrangeRequirementSwitchDependency(True)

        Report = (New SafeCheckReportUISafeMaintenanceInfoFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.SafeCheckReportUIDisplayInfo", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeCheckReportUISafeMaintenanceAuthoriserFactory_RequirementCheck_False_ReturnSafeCheckReportUIHideAuthorisers()

        Dim Report As ISafeCheckReportUI

        ArrangeRequirementSwitchDependency(False)

        Report = (New SafeCheckReportUISafeMaintenanceAuthoriserFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.SafeCheckReportUIHideAuthorisers", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeCheckReportUISafeMaintenanceAuthoriserFactory_RequirementCheck_True_ReturnSafeCheckReportUIDisplayAuthorisers()

        Dim Report As ISafeCheckReportUI

        ArrangeRequirementSwitchDependency(True)

        Report = (New SafeCheckReportUISafeMaintenanceAuthoriserFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.SafeCheckReportUIDisplayAuthorisers", Report.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - SafeCheckReportUIPrint"

    <TestMethod()> Public Sub SafeCheckReportUIPrint_CallsSafeCheckReport()

        Dim Report As New TestSafeCheckReportUIPrint

        Report.RunReport(Nothing, Nothing, Nothing)
        Assert.AreEqual("NewBanking.Form.SafeCheckReport", Report._Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeCheckReportUIPrint_CreatesNewReportGridInSafeCheckReport()

        Dim Report As New TestSafeCheckReportUIPrint

        Report.RunReport(Nothing, Nothing, Nothing)
        Assert.AreEqual(String.Empty, Report._Report._ReportGrid.Name)

    End Sub

#End Region

#Region "Unit Test - SafeCheckReportUIView"

    <TestMethod()> Public Sub SafeCheckReportUIView_CallSafeCheckReportView()

        Dim Report As New TestSafeCheckReportUIView

        Report.RunReport(Nothing, Nothing, Nothing)
        Assert.AreEqual("NewBanking.Form.SafeCheckReportView", Report._View.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeCheckReportUIView_PassExistingReportGridIntoSafeCheckReport()

        Dim Report As New TestSafeCheckReportUIView

        Report.RunReport(Nothing, Nothing, Nothing)
        Assert.AreEqual("spdSafeCheckReport", Report._View._Report._ReportGrid.Name)

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

End Class