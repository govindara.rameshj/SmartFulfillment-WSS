﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class CashierReportVMWithComments_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "SetCashierReportDisplayGrid Tests"

    <TestMethod()> _
    Public Sub SetCashierReportDisplayGrid_Sets_CashierReportForm_Member_To_CashierReportForm_Parameter()
        Dim CRVM As New CashierReportVMWithComments
        Dim expected As New FpSpread

        With CRVM
            .SetCashierReportDisplayGrid(expected)
            Assert.AreSame(expected, ._CashierReportDisplayGrid)
        End With
    End Sub
#End Region

#Region "DisplayCashierOnCurrentSheet Tests"

    <TestMethod()> _
    Public Sub DisplayCashierOnCurrentSheet_AddsACommentsRowToDisplayGrid()
        Dim CRVM As New TestCashierReportVMWithComments_WithTestDataExposed
        Dim TestGrid As New FpSpread
        Dim TestCurrentSheet As New SheetView
        Dim TestCashier As ActiveCashierList

        With CRVM
            .SetCashierReportDisplayGrid(TestGrid)
            TestCashier = ._TestActiveCashierList
            With TestCashier
                CustomiseSheet(TestGrid, TestCurrentSheet, .CashierEmployeeCode & " " & .CashierUserName)
            End With
            .SetCashierReportCurrentSheet(TestCurrentSheet)
            .DisplayCashierOnCurrentSheet(TestCashier)
            Assert.IsTrue(._CashierReportDisplayGrid.ActiveSheet.RowCount > ._CommentsRowIndex AndAlso String.Compare(._CashierReportDisplayGrid.ActiveSheet.Cells(._CommentsRowIndex, 0).Text, "Comments") = 0)
        End With
    End Sub

    <TestMethod()> _
    Public Sub DisplayCashierOnCurrentSheet_PopulatesCommentsCellForCashiersPickupWithAComment()
        Dim CRVM As New TestCashierReportVMWithComments_WithTestDataExposed
        Dim TestGrid As New FpSpread
        Dim TestCurrentSheet As New SheetView
        Dim TestCashier As ActiveCashierList

        With CRVM
            .SetCashierReportDisplayGrid(TestGrid)
            TestCashier = ._TestActiveCashierList
            With TestCashier
                CustomiseSheet(TestGrid, TestCurrentSheet, .CashierEmployeeCode & " " & .CashierUserName)
            End With
            .SetCashierReportCurrentSheet(TestCurrentSheet)
            .DisplayCashierOnCurrentSheet(TestCashier)
            Assert.IsTrue(._CashierReportDisplayGrid.ActiveSheet.Cells(._CommentsRowIndex, ._PickupCommentsColumnIndex).Value IsNot Nothing)
        End With
    End Sub

    <TestMethod()> _
    Public Sub DisplayCashierOnCurrentSheet_PopulatesCommentsCellForCashiersPickupWithCorrectComment()
        Dim CRVM As New TestCashierReportVMWithComments_WithTestDataExposed
        Dim TestGrid As New FpSpread
        Dim TestCurrentSheet As New SheetView
        Dim TestCashier As ActiveCashierList
        Dim Expected As String

        With CRVM
            Expected = ._TestPickupComments
            .SetCashierReportDisplayGrid(TestGrid)
            TestCashier = ._TestActiveCashierList
            With TestCashier
                CustomiseSheet(TestGrid, TestCurrentSheet, .CashierEmployeeCode & " " & .CashierUserName)
            End With
            .SetCashierReportCurrentSheet(TestCurrentSheet)
            .DisplayCashierOnCurrentSheet(TestCashier)
            Assert.AreEqual(Expected, ._CashierReportDisplayGrid.ActiveSheet.Cells(._CommentsRowIndex, ._PickupCommentsColumnIndex).Text)
        End With
    End Sub

    <TestMethod()> _
    Public Sub DisplayCashierOnCurrentSheet_PopulatesCommentsCellForCashiersCashDropWithAComment()
        Dim CRVM As New TestCashierReportVMWithComments_WithTestDataExposed
        Dim TestGrid As New FpSpread
        Dim TestCurrentSheet As New SheetView
        Dim TestCashier As ActiveCashierList

        With CRVM
            .SetCashierReportDisplayGrid(TestGrid)
            TestCashier = ._TestActiveCashierList
            With TestCashier
                CustomiseSheet(TestGrid, TestCurrentSheet, .CashierEmployeeCode & " " & .CashierUserName)
            End With
            .SetCashierReportCurrentSheet(TestCurrentSheet)
            .DisplayCashierOnCurrentSheet(TestCashier)
            Assert.IsTrue(._CashierReportDisplayGrid.ActiveSheet.Cells(._CommentsRowIndex, ._CashDropCommentsColumnIndex).Value IsNot Nothing)
        End With
    End Sub

    <TestMethod()> _
    Public Sub DisplayCashierOnCurrentSheet_PopulatesCommentsCellForCashiersCashDropWithCorrectComment()
        Dim CRVM As New TestCashierReportVMWithComments_WithTestDataExposed
        Dim TestGrid As New FpSpread
        Dim TestCurrentSheet As New SheetView
        Dim TestCashier As ActiveCashierList
        Dim Expected As String

        With CRVM
            Expected = ._TestCashDropComments
            .SetCashierReportDisplayGrid(TestGrid)
            TestCashier = ._TestActiveCashierList
            With TestCashier
                CustomiseSheet(TestGrid, TestCurrentSheet, .CashierEmployeeCode & " " & .CashierUserName)
            End With
            .SetCashierReportCurrentSheet(TestCurrentSheet)
            .DisplayCashierOnCurrentSheet(TestCashier)
            Assert.AreEqual(Expected, ._CashierReportDisplayGrid.ActiveSheet.Cells(._CommentsRowIndex, ._CashDropCommentsColumnIndex).Text)
        End With
    End Sub
#End Region

#Region "SetCashierReportForm Tests"

    <TestMethod()> _
    Public Sub SetCashierReportForm_Sets_CashierReportForm_Member_To_CashierReportForm_Parameter()
        Dim CRVM As New CashierReportVMWithComments
        Dim expected As New System.Windows.Forms.Form

        With CRVM
            .SetCashierReportForm(expected)
            Assert.AreSame(expected, ._CashierReportForm)
        End With
    End Sub
#End Region

#Region "CashierReportFormFormat Tests"

    <TestMethod()> _
    Public Sub CashierReportFormFormat_Sets_CashierReportForm_Member_ToBe80Higher()
        Dim CRVM As New CashierReportVMWithComments
        Dim NewForm As New System.Windows.Forms.Form
        Dim Expected As Integer = NewForm.Height + 80

        With CRVM
            .SetCashierReportForm(NewForm)
            .CashierReportFormFormat()
            Assert.AreEqual(Expected, CRVM._CashierReportForm.Height)
        End With
    End Sub
#End Region

#Region "Internals"


    Private Sub CustomiseSheet(ByRef spd As FarPoint.Win.Spread.FpSpread, ByRef CurrentSheet As FarPoint.Win.Spread.SheetView, ByVal strSheetName As String)

        SpreadSheetCustomise(CurrentSheet, 1, Model.SelectionUnit.Cell)
        SpreadSheetCustomiseHeader(CurrentSheet, gintHeaderHeightTriple)
        SpreadColumnCustomise(CurrentSheet, 0, "Tender / Denom", gcintColumnWidthAllTendersWide, False)     'Screen Column - Denomination
        SpreadColumnAlignLeft(CurrentSheet, 0)
        SpreadGridSheetAdd(spd, CurrentSheet, True, strSheetName)
    End Sub
#End Region
End Class
