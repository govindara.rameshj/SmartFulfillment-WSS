﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class FloatedPickupVMFactory_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "FloatedPickupVMFactory Factory"

    <TestMethod()> _
    Public Sub GetImplementation_RequirementSwitch_IsNotPresentOrEnabled_Returns_FloatedPickupVMWithoutComments()
        Dim FPVM As IFloatedPickupVM

        ArrangeRequirementSwitchDependency(False)
        FPVM = (New FloatedPickupVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.FloatedPickupVMWithoutComments", FPVM.GetType.FullName)
    End Sub

    <TestMethod()> _
    Public Sub GetImplementation_RequirementSwitch_IsPresentAndEnabled_Returns_FloatedPickupVMWithComments()
        Dim FPVM As IFloatedPickupVM

        ArrangeRequirementSwitchDependency(True)
        FPVM = (New FloatedPickupVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.FloatedPickupVMWithComments", FPVM.GetType.FullName)
    End Sub
#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)
        Dim MockRep As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = MockRep.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        MockRep.ReplayAll()
    End Sub
#End Region
End Class
