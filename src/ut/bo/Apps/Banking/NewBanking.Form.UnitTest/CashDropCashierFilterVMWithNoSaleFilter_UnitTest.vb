﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class CashDropCashierFilterVMWithNoSaleFilter_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "LoadCashierDropDown Tests"

    <TestMethod()> _
    Public Sub LoadCashierDropDown_CallsInitialise()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingInitialiseAndAssociatedMethodsToFlagWhetherTheyAreCalledAndExposePassedInParameters

        CDCFVM.LoadCashierDropDown(New CashierCollection, 1)
        Assert.AreEqual(True, CDCFVM._InitialiseWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_PassesCashiersToLoadParameter_To_Initialise()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingInitialiseAndAssociatedMethodsToFlagWhetherTheyAreCalledAndExposePassedInParameters
        Dim Parameter As New CashierCollection

        CDCFVM.LoadCashierDropDown(Parameter, 1)
        Assert.AreSame(Parameter, CDCFVM._Initialise_CashiersToLoad_Parameter)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_Passes_BankingPeriodIDParameter_To_Initialise()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingInitialiseAndAssociatedMethodsToFlagWhetherTheyAreCalledAndExposePassedInParameters
        Dim Parameter As Integer = 45678

        CDCFVM.LoadCashierDropDown(New CashierCollection, Parameter)
        Assert.AreEqual(Parameter, CDCFVM._Initialise_BankingPeriodID_Parameter)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_CallsHaveInitialised()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingHaveInitialisedAndLoadCashiersMethodsToFlagWhetherTheyAreCalled(False)

        CDCFVM.LoadCashierDropDown(New CashierCollection, 1)
        Assert.AreEqual(True, CDCFVM._HaveInitialisedWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_HaveInitialisedIsFalse_DoesNotCall_LoadCashiers()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingHaveInitialisedAndLoadCashiersMethodsToFlagWhetherTheyAreCalled(False)

        CDCFVM.LoadCashierDropDown(New CashierCollection, 1)
        Assert.AreEqual(False, CDCFVM._LoadCashiersWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_HaveInitialisedIsTrue_DoesCall_LoadCashiers()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingHaveInitialisedAndLoadCashiersMethodsToFlagWhetherTheyAreCalled(True)

        CDCFVM.LoadCashierDropDown(New CashierCollection, 1)
        Assert.AreEqual(True, CDCFVM._LoadCashiersWasCalled)
    End Sub
#End Region

#Region "Initialise Tests"

    <TestMethod()> _
    Public Sub Initialise_CallsSetCashiersToLoad()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingInitialiseAndAssociatedMethodsToFlagWhetherTheyAreCalledAndExposePassedInParameters

        CDCFVM.Initialise(New CashierCollection, 1)
        Assert.AreEqual(True, CDCFVM._SetCashiersToLoadWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub Initialise_CallsSetBankingPeriodID()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingInitialiseAndAssociatedMethodsToFlagWhetherTheyAreCalledAndExposePassedInParameters

        CDCFVM.Initialise(New CashierCollection, 1)
        Assert.AreEqual(True, CDCFVM._SetBankingPeriodIDWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub Initialise_PassesCashiersToLoadParameter_To_SetCashiersToLoad()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingInitialiseAndAssociatedMethodsToFlagWhetherTheyAreCalledAndExposePassedInParameters
        Dim Parameter As New CashierCollection

        CDCFVM.Initialise(Parameter, 1)
        Assert.AreSame(Parameter, CDCFVM._SetCashiersToLoad_CashiersToLoad_Parameter)
    End Sub

    <TestMethod()> _
    Public Sub Initialise_Passes_BankingPeriodIDParameter_To_SetBankingPeriodID()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridingInitialiseAndAssociatedMethodsToFlagWhetherTheyAreCalledAndExposePassedInParameters
        Dim Parameter As Integer = 45678

        CDCFVM.Initialise(New CashierCollection, Parameter)
        Assert.AreEqual(Parameter, CDCFVM._SetBankingPeriodID_BankingPeriodID_Parameter)
    End Sub
#End Region

#Region "SetCashiersToLoad Tests"

    <TestMethod()> _
    Public Sub SetCashiersToLoad_AssignsParameterToModularVariable()
        Dim CDCFVM As New CashDropCashierFilterVMWithNoSaleFilter
        Dim Parameter As New CashierCollection

        CDCFVM.SetCashiersToLoad(Parameter)
        Assert.AreSame(Parameter, CDCFVM._CashiersToLoad)
    End Sub
#End Region

#Region "SetBankingPeriodID Tests"

    <TestMethod()> _
    Public Sub SetBankingPeriodID_AssignsParameterToModularVariable()
        Dim CDCFVM As New CashDropCashierFilterVMWithNoSaleFilter
        Dim Parameter As Integer = 32158

        CDCFVM.SetBankingPeriodID(Parameter)
        Assert.AreEqual(Parameter, CDCFVM._BankingPeriodID.Value)
    End Sub
#End Region

#Region "CashiersToLoadIsSet Tests"

    <TestMethod()> _
    Public Sub CashiersToLoadIsSet_ModularCashiersToLoadIsNothing_ReturnsFalse()
        Dim CDCFVM As New CashDropCashierFilterVMWithNoSaleFilter

        CDCFVM._CashiersToLoad = Nothing
        Assert.AreEqual(False, CDCFVM.CashiersToLoadIsSet)
    End Sub

    <TestMethod()> _
    Public Sub CashiersToLoadIsSet_ModularCashiersToLoadIsSomething_ReturnsTrue()
        Dim CDCFVM As New CashDropCashierFilterVMWithNoSaleFilter

        CDCFVM._CashiersToLoad = New CashierCollection
        Assert.AreEqual(True, CDCFVM.CashiersToLoadIsSet)
    End Sub
#End Region

#Region "BankingPeriodIDIsSet Tests"

    <TestMethod()> _
    Public Sub BankingPeriodIDIsSet_ModularBankingPeriodIDHasNoValue_ReturnsFalse()
        Dim CDCFVM As New CashDropCashierFilterVMWithNoSaleFilter

        CDCFVM._BankingPeriodID = Nothing
        Assert.AreEqual(False, CDCFVM.BankingPeriodIDIsSet)
    End Sub

    <TestMethod()> _
    Public Sub BankingPeriodIDIsSet_ModularBankingPeriodIDIsSomething_ReturnsTrue()
        Dim CDCFVM As New CashDropCashierFilterVMWithNoSaleFilter

        CDCFVM._BankingPeriodID = 32158
        Assert.AreEqual(True, CDCFVM.BankingPeriodIDIsSet)
    End Sub
#End Region

#Region "HaveInitialised Tests"

    <TestMethod()> _
    Public Sub HaveInitialised_CashiersToLoadIsSetIsFalseAndBankingPeriodIDIsSetIsFalse_ReturnsFalse()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridesCashiersToLoadIsSetToReturnFalse_AndOverridesBankingPeriodIDToReturnFalse

        Assert.AreEqual(False, CDCFVM.HaveInitialised)
    End Sub

    <TestMethod()> _
    Public Sub HaveInitialised_CashiersToLoadIsSetIsFalseAndBankingPeriodIDIsSetIsTrue_ReturnsFalse()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridesCashiersToLoadIsSetToReturnFalse_AndOverridesBankingPeriodIDToReturnTrue

        Assert.AreEqual(False, CDCFVM.HaveInitialised)
    End Sub

    <TestMethod()> _
    Public Sub HaveInitialised_CashiersToLoadIsSetIsTrueAndBankingPeriodIDIsSetIsFalse_ReturnsFalse()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridesCashiersToLoadIsSetToReturnTrue_AndOverridesBankingPeriodIDToReturnFalse

        Assert.AreEqual(False, CDCFVM.HaveInitialised)
    End Sub

    <TestMethod()> _
    Public Sub HaveInitialised_CashiersToLoadIsSetIsTrueAndBankingPeriodIDIsSetIsTrue_ReturnsTrue()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithNoSaleFilter_OverridesCashiersToLoadIsSetToReturnTrue_AndOverridesBankingPeriodIDToReturnTrue

        Assert.AreEqual(True, CDCFVM.HaveInitialised)
    End Sub
#End Region
End Class

Friend Class TestCashDropCashierFilterVMWithNoSaleFilter_OverridesCashiersToLoadIsSetToReturnFalse_AndOverridesBankingPeriodIDToReturnFalse
    Inherits CashDropCashierFilterVMWithNoSaleFilter

    Friend Overrides Function CashiersToLoadIsSet() As Boolean

        Return False
    End Function

    Friend Overrides Function BankingPeriodIDIsSet() As Boolean

        Return False
    End Function
End Class

Friend Class TestCashDropCashierFilterVMWithNoSaleFilter_OverridesCashiersToLoadIsSetToReturnFalse_AndOverridesBankingPeriodIDToReturnTrue
    Inherits CashDropCashierFilterVMWithNoSaleFilter

    Friend Overrides Function CashiersToLoadIsSet() As Boolean

        Return False
    End Function

    Friend Overrides Function BankingPeriodIDIsSet() As Boolean

        Return True
    End Function
End Class

Friend Class TestCashDropCashierFilterVMWithNoSaleFilter_OverridesCashiersToLoadIsSetToReturnTrue_AndOverridesBankingPeriodIDToReturnFalse
    Inherits CashDropCashierFilterVMWithNoSaleFilter

    Friend Overrides Function CashiersToLoadIsSet() As Boolean

        Return True
    End Function

    Friend Overrides Function BankingPeriodIDIsSet() As Boolean

        Return False
    End Function
End Class

Friend Class TestCashDropCashierFilterVMWithNoSaleFilter_OverridesCashiersToLoadIsSetToReturnTrue_AndOverridesBankingPeriodIDToReturnTrue
    Inherits CashDropCashierFilterVMWithNoSaleFilter

    Friend Overrides Function CashiersToLoadIsSet() As Boolean

        Return True
    End Function

    Friend Overrides Function BankingPeriodIDIsSet() As Boolean

        Return True
    End Function
End Class

Friend Class TestCashDropCashierFilterVMWithNoSaleFilter_OverridingInitialiseAndAssociatedMethodsToFlagWhetherTheyAreCalledAndExposePassedInParameters
    Inherits CashDropCashierFilterVMWithNoSaleFilter

    Friend _Initialise_BankingPeriodID_Parameter As Integer
    Friend _Initialise_CashiersToLoad_Parameter As CashierCollection
    Friend _InitialiseWasCalled As Boolean
    Friend _SetBankingPeriodID_BankingPeriodID_Parameter As Integer
    Friend _SetBankingPeriodIDWasCalled As Boolean
    Friend _SetCashiersToLoad_CashiersToLoad_Parameter As CashierCollection
    Friend _SetCashiersToLoadWasCalled As Boolean

    Friend Overrides Sub Initialise(ByRef CashiersToLoad As Core.CashierCollection, ByVal BankingPeriodID As Integer)

        _InitialiseWasCalled = True
        _Initialise_CashiersToLoad_Parameter = CashiersToLoad
        _Initialise_BankingPeriodID_Parameter = BankingPeriodID
        MyBase.Initialise(CashiersToLoad, BankingPeriodID)
    End Sub

    Friend Overrides Sub SetCashiersToLoad(ByVal CashiersToLoad As Core.CashierCollection)

        _SetCashiersToLoadWasCalled = True
        _SetCashiersToLoad_CashiersToLoad_Parameter = CashiersToLoad
    End Sub

    Friend Overrides Sub SetBankingPeriodID(ByVal BankingPeriodID As Integer)

        _SetBankingPeriodIDWasCalled = True
        _SetBankingPeriodID_BankingPeriodID_Parameter = BankingPeriodID
    End Sub
End Class

Friend Class TestCashDropCashierFilterVMWithNoSaleFilter_OverridingHaveInitialisedAndLoadCashiersMethodsToFlagWhetherTheyAreCalled
    Inherits CashDropCashierFilterVMWithNoSaleFilter

    Friend _HaveInitialisedWasCalled As Boolean
    Friend _LoadCashiersWasCalled As Boolean
    Friend _Overriden_HaveInitialised_ReturnValue As Boolean

    Public Sub New(ByVal Overriden_HaveInitialised_ReturnValue As Boolean)

        _Overriden_HaveInitialised_ReturnValue = Overriden_HaveInitialised_ReturnValue
    End Sub

    Friend Overrides Function HaveInitialised() As Boolean

        _HaveInitialisedWasCalled = True
        Return _Overriden_HaveInitialised_ReturnValue
    End Function

    Friend Overrides Sub LoadCashiers()

        _LoadCashiersWasCalled = True
    End Sub
End Class