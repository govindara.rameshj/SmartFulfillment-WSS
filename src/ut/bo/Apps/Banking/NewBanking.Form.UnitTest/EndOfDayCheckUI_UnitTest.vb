﻿<TestClass()> Public Class EndOfDayCheckUI_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub RequirementEndOfDayCheck_False_ReturnEndOfDayCheckEmptyViewModel()

        Dim V As IEndOfDayCheckUI

        ArrangeRequirementSwitchDependency(False)
        'V = (New TestFactory(False)).GetImplementation
        V = (New EndOfDayCheckUIFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.EndOfDayCheckUIBankingReport", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementEndOfDayCheck_True_RequirementSealComments_False_ReturnEndOfDayCheckViewModel()

        Dim V As IEndOfDayCheckUI

        ArrangeRequirementSwitchDependency(True)
        'V = (New TestFactory(True)).GetImplementation
        V = (New EndOfDayCheckUIFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.EndOfDayCheckUI", V.GetType.FullName)

    End Sub

#End Region

#Region "Class: EndOfDayCheckUIBankingReport | Interface | Button Caption Name"

    <TestMethod()> Public Sub EndOfDayCheckUIBankingReport_ButtonCaptionName_ReturnCorrectName()

        Dim VM As New EndOfDayCheckUIBankingReport

        Assert.AreEqual("F9 Banking Report", VM.ButtonCaptionName())

    End Sub

#End Region

#Region "Class: EndOfDayCheckUIBankingReport | Interface | Button Enabled"

    <TestMethod()> Public Sub EndOfDayCheckUIBankingReport_ButtonEnabled_ReturnTrue()

        Dim VM As New EndOfDayCheckUIBankingReport

        Assert.IsTrue(VM.ButtonEnabled(Nothing, Nothing, Nothing))

    End Sub

#End Region

#Region "Class: EndOfDayCheckUIBankingReport | Interface | Execute End Of Day Check Process UI"

    <TestMethod()> Public Sub EndOfDayCheckUIBankingReport_ExecuteEndOfDayCheckProcessUI_ReturnFalse()

        Dim VM As New EndOfDayCheckUIBankingReport

        Assert.IsFalse(VM.ExecuteEndOfDayCheckProcessUI())

    End Sub

#End Region

#Region "Class: EndOfDayCheckUI | Interface | Button Caption Name"

    <TestMethod()> Public Sub EndOfDayCheckUI_ButtonCaptionName()

        Dim VM As New EndOfDayCheckUI

        Assert.AreEqual("F9 End Of Day", VM.ButtonCaptionName())

    End Sub

#End Region

#Region "Class: EndOfDayCheckUI | Interface | Button Enabled"

    <TestMethod()> Public Sub EndOfDayCheckUI_ButtonEnabled_EndOfDayCheckDoneFalse_CurrentDateEqualsSelectedDate_ReturnTrue()

        Dim VM As New EndOfDayCheckUI

        Assert.IsTrue(VM.ButtonEnabled(Now.Date, Now.Date, False))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckUI_ButtonEnabled_EndOfDayCheckDoneFalse_CurrentDateLessThanSelectedDate_ReturnFalse()

        Dim VM As New EndOfDayCheckUI

        Assert.IsFalse(VM.ButtonEnabled(Now.Date, Now.Date.AddDays(1), False))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckUI_ButtonEnabled_EndOfDayCheckDoneFalse_CurrentDateGreaterThanSelectedDate_ReturnFalse()

        Dim VM As New EndOfDayCheckUI

        Assert.IsFalse(VM.ButtonEnabled(Now.Date.AddDays(1), Now.Date, False))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckUI_ButtonEnabled_EndOfDayCheckDoneTrue_CurrentDateEqualsSelectedDate_ReturnFalse()

        Dim VM As New EndOfDayCheckUI

        Assert.IsFalse(VM.ButtonEnabled(Now.Date, Now.Date, True))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckUI_ButtonEnabled_EndOfDayCheckDoneTrue_CurrentDateLessThanSelectedDate_ReturnFalse()

        Dim VM As New EndOfDayCheckUI

        Assert.IsFalse(VM.ButtonEnabled(Now.Date, Now.Date.AddDays(1), True))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckUI_ButtonEnabled_EndOfDayCheckDoneTrue_CurrentDateGreaterThanSelectedDate_ReturnFalse()

        Dim VM As New EndOfDayCheckUI

        Assert.IsFalse(VM.ButtonEnabled(Now.Date.AddDays(1), Now.Date, True))

    End Sub

#End Region

#Region "Class: EndOfDayCheckUI | Interface | Execute End Of Day Check Process UI"

    <TestMethod()> Public Sub EndOfDayCheckUI_ExecuteEndOfDayCheckProcessUI_ReturnTrue()

        Dim VM As New EndOfDayCheckUI

        Assert.IsTrue(VM.ExecuteEndOfDayCheckProcessUI())

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

End Class