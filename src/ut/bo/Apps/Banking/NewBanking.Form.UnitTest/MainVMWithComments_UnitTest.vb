﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class MainVMWithComments_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "SetFloatedCashierGridControl Tests"

    <TestMethod()> _
    Public Sub SetFloatedCashierGridControl_Sets_FloatedCashierGrid_Member_To_FloatedCashierGrid_Parameter()
        Dim MVM As New MainVMWithComments
        Dim expected As New FpSpread

        MVM.SetFloatedCashierGridControl(expected)
        Assert.AreSame(expected, MVM._FloatedCashierGrid)
    End Sub
#End Region

#Region "FloatedCashierGridFormat Tests"

    <TestMethod()> _
    Public Sub FloatedCashierGridFormat_AddsAtLeastEnoughColumnsToIncludeCommentsColumn()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetFloatedCashierGridControl(New FpSpread)
            .FloatedCashierGridFormat()
            Assert.IsTrue(._FloatedCashierGrid.Sheets(0).ColumnCount >= 11, "Expected 11 or more columns, has " & ._FloatedCashierGrid.Sheets(0).ColumnCount.ToString & " columns.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatedCashierGridFormat_Sets5thColumnHeaderToComments()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetFloatedCashierGridControl(New FpSpread)
            .FloatedCashierGridFormat()
            Assert.IsTrue(._FloatedCashierGrid.Sheets(0).Columns(10).Label = "Comments", "Expected 'Comment' header for column 11, actually is '" & ._FloatedCashierGrid.Sheets(0).Columns(10).Label & "'.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatedCashierGridFormat_Sets11thColumnWidthTo400()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetFloatedCashierGridControl(New FpSpread)
            .FloatedCashierGridFormat()
            Assert.IsTrue(._FloatedCashierGrid.Sheets(0).Columns(10).Width = 400, "Expected column 11 width to be 400, actually it is " & ._FloatedCashierGrid.Sheets(0).Columns(10).Width.ToString & ".")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatedCashierGridFormat_SetsCommentsColumnHorizontallyAlignmentToBeGeneral()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetFloatedCashierGridControl(New FpSpread)
            .FloatedCashierGridFormat()
            Assert.IsTrue(._FloatedCashierGrid.Sheets(0).Columns(10).HorizontalAlignment = CellHorizontalAlignment.General, "Expected column 11's horizontal alignment to be 'General', actually it is " & ._FloatedCashierGrid.Sheets(0).Columns(10).HorizontalAlignment.ToString & ".")
        End With
    End Sub
    <TestMethod()> _
    Public Sub FloatedCashierGridFormat_SetsCommentsColumnToBeResizable()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetFloatedCashierGridControl(New FpSpread)
            .FloatedCashierGridFormat()
            Assert.IsTrue(._FloatedCashierGrid.Sheets(0).Columns(10).Resizable, "Expected column 11 to be resizable, actually it is not.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatedCashierGridFormat_RemovesEndOfDayBagCheckColumn()
        Dim MVM As New MainVMWithComments
        Dim StillHaveRemovedColumn As Boolean = False

        With MVM
            .SetFloatedCashierGridControl(New FpSpread)
            .FloatedCashierGridFormat()
            For Each GridColumn As FarPoint.Win.Spread.Column In ._FloatedCashierGrid.ActiveSheet.Columns
                If String.Compare(GridColumn.Label, ._FloatedCashierGrid_EndOfDayBagCheck_ColumnHeader) = 0 Then
                    StillHaveRemovedColumn = True
                    Exit For
                End If
            Next
            Assert.IsFalse(StillHaveRemovedColumn)
        End With
    End Sub
#End Region

#Region "CashDropAndUnFloatedPickupGridFormat Tests"

    <TestMethod()> _
    Public Sub CashDropAndUnFloatedPickupGridFormat_AddsAtLeastEnoughColumnsToIncludeCommentsColumn()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetCashDropAndUnFloatedPickupGridControl(New FpSpread)
            .CashDropAndUnFloatedPickupGridFormat()
            Assert.IsTrue(._CashDropAndUnFloatedPickupGrid.Sheets(0).ColumnCount >= 6, "Expected 6 or more columns, has " & ._CashDropAndUnFloatedPickupGrid.Sheets(0).ColumnCount.ToString & " columns.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub CashDropAndUnFloatedPickupGridFormat_Sets6thColumnHeaderToComments()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetCashDropAndUnFloatedPickupGridControl(New FpSpread)
            .CashDropAndUnFloatedPickupGridFormat()
            Assert.IsTrue(._CashDropAndUnFloatedPickupGrid.Sheets(0).Columns(5).Label = "Comments", "Expected 'Comment' header for column 11, actually is '" & ._CashDropAndUnFloatedPickupGrid.Sheets(0).Columns(5).Label & "'.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub CashDropAndUnFloatedPickupGridFormat_Sets6thColumnWidthTo662()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetCashDropAndUnFloatedPickupGridControl(New FpSpread)
            .CashDropAndUnFloatedPickupGridFormat()
            Assert.IsTrue(._CashDropAndUnFloatedPickupGrid.Sheets(0).Columns(5).Width = 662, "Expected column 11 width to be 400, actually it is " & ._CashDropAndUnFloatedPickupGrid.Sheets(0).Columns(5).Width.ToString & ".")
        End With
    End Sub

    <TestMethod()> _
    Public Sub CashDropAndUnFloatedPickupGridFormat_Sets6thColumnToBeResizable()
        Dim MVM As New MainVMWithComments

        With MVM
            .SetCashDropAndUnFloatedPickupGridControl(New FpSpread)
            .CashDropAndUnFloatedPickupGridFormat()
            Assert.IsTrue(._CashDropAndUnFloatedPickupGrid.Sheets(0).Columns(5).Resizable, "Expected column 11 to be resizable, actually it is not.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub CashDropAndUnFloatedPickupGridFormat_RemovesEndOfDayBagCheckColumn()
        Dim MVM As New MainVMWithComments
        Dim StillHaveRemovedColumn As Boolean = False

        With MVM
            .SetCashDropAndUnFloatedPickupGridControl(New FpSpread)
            .CashDropAndUnFloatedPickupGridFormat()
            For Each GridColumn As FarPoint.Win.Spread.Column In ._CashDropAndUnFloatedPickupGrid.ActiveSheet.Columns
                If String.Compare(GridColumn.Label, ._CashDropAndUnFloatedPickupGrid_EndOfDayBagCheck_ColumnHeader) = 0 Then
                    StillHaveRemovedColumn = True
                    Exit For
                End If
            Next
            Assert.IsFalse(StillHaveRemovedColumn)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CashDropAndUnFloatedPickupGridFormat_SetsHorizontalScrollBarPolicyToAsNeeded()
        Dim MVM As New MainVMWithComments
        Dim StillHaveRemovedColumn As Boolean = False

        With MVM
            .SetCashDropAndUnFloatedPickupGridControl(New FpSpread)
            .CashDropAndUnFloatedPickupGridFormat()
            Assert.IsTrue(._CashDropAndUnFloatedPickupGrid.VerticalScrollBarPolicy = ScrollBarPolicy.AsNeeded)
        End With
    End Sub
#End Region
End Class
