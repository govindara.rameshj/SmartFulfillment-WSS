﻿<TestClass()> Public Class BankingBagCreationUI_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

    Private _Pickup As TenderDenominationListCollection
    Private _Info As BankingBagInfo

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Requirement Test"

    <TestMethod()> Public Sub RequirementCheckFalse_ReturnSafeCheckReportUIPrint()

        Dim Report As IBankingBagCreationUI

        ArrangeRequirementSwitchDependency(False)

        Report = (New BankingBagCreationUIFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.BankingBagCreationToBankNotPopulated", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementCheckTrue_ReturnSafeCheckReportUIView()

        Dim Report As IBankingBagCreationUI

        ArrangeRequirementSwitchDependency(True)

        Report = (New BankingBagCreationUIFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.BankingBagCreationToBankPopulated", Report.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - BankingBagInfo Class"

#Region "Safe"

    <TestMethod()> Public Sub SetSafeTenderID()

        CreatePickup()
        CreateSafe(1, Nothing, Nothing)

        Assert.AreEqual(1, _Info._SafeTenderID)

    End Sub

    <TestMethod()> Public Sub SetSafeDenominationID()

        CreatePickup()
        CreateSafe(Nothing, 0.01, Nothing)

        Assert.AreEqual(0.01, Decimal.ToDouble(_Info._SafeDenominationID))

    End Sub

    <TestMethod()> Public Sub SetSafeTotal()

        CreatePickup()
        CreateSafe(Nothing, Nothing, 15)

        Assert.AreEqual(15.0, Decimal.ToDouble(_Info._SafeTotal.Value))

    End Sub

#End Region

#Region "Pickup"

    <TestMethod()> Public Sub SetPickupSafeMinimum()

        CreatePickup()
        CreatePickupTD(1, 10, 0, Nothing, Nothing)
        CreatePickupTD(1, 5, 150, Nothing, Nothing)
        CreateSafe(1, 5, Nothing)

        Assert.AreEqual(150.0, Decimal.ToDouble(_Info._PickupSafeMinimum))

    End Sub

    <TestMethod()> Public Sub SetPickupBullionMultiple()

        CreatePickup()
        CreatePickupTD(1, 10, Nothing, 10, Nothing)
        CreatePickupTD(1, 5, Nothing, 500, Nothing)
        CreateSafe(1, 5, Nothing)

        Assert.AreEqual(500.0, Decimal.ToDouble(_Info._PickupBullionMultiple))

    End Sub

    <TestMethod()> Public Sub SetPickupTotal()

        CreatePickup()
        CreatePickupTD(1, 20, Nothing, Nothing, 100)
        CreatePickupTD(1, 10, Nothing, Nothing, 50)
        CreateSafe(1, 10, Nothing)

        Assert.AreEqual(50.0, Decimal.ToDouble(_Info._PickupTotal.Value))

    End Sub

#End Region

#Region "System Total"

#Region "Cash Tender"

    <TestMethod()> Public Sub CashTender_SafeNonZeroAmount_PickupNonZeroAmount_ReturnSystemTotal()

        CreatePickup()
        CreatePickupTD(1, 10, Nothing, Nothing, 100)
        CreatePickupTD(1, 5, Nothing, Nothing, 250)
        CreateSafe(1, 5, 150)

        Assert.AreEqual(400.0, Decimal.ToDouble(_Info.SystemTotal))

    End Sub

    <TestMethod()> Public Sub CashTender_SafeZeroAmount_PickupZeroAmount_ReturnSystemTotal()

        CreatePickup()
        CreatePickupTD(1, 10, Nothing, Nothing, 100)
        CreatePickupTD(1, 5, Nothing, Nothing, 0)
        CreateSafe(1, 5, 0)

        Assert.AreEqual(0.0, Decimal.ToDouble(_Info.SystemTotal))

    End Sub

    <TestMethod()> Public Sub CashTender_SafeNonZeroAmount_PickupZeroAmount_ReturnSystemTotal()

        CreatePickup()
        CreatePickupTD(1, 10, Nothing, Nothing, 100)
        CreatePickupTD(1, 5, Nothing, Nothing, 0)
        CreateSafe(1, 5, 100)

        Assert.AreEqual(100.0, Decimal.ToDouble(_Info.SystemTotal))

    End Sub

    <TestMethod()> Public Sub CashTender_SafeZeroAmount_PickupNonZeroAmount_ReturnSystemTotal()

        CreatePickup()
        CreatePickupTD(1, 10, Nothing, Nothing, 100)
        CreatePickupTD(1, 5, Nothing, Nothing, 300)
        CreateSafe(1, 5, 0)

        Assert.AreEqual(300.0, Decimal.ToDouble(_Info.SystemTotal))

    End Sub

#End Region

#Region "Non Cash Tender"

    <TestMethod()> Public Sub NonCashTender_SafeZeroAmount_PickupZeroAmount_ReturnSystemTotal()

        CreatePickup()
        CreatePickupTD(8, Nothing, Nothing, Nothing, 100)
        CreatePickupTD(9, Nothing, Nothing, Nothing, 0)
        CreateSafe(9, Nothing, 0)

        Assert.AreEqual(0.0, Decimal.ToDouble(_Info.SystemTotal))

    End Sub

    <TestMethod()> Public Sub NonCashTender_SafeZeroAmount_PickupNonZeroAmount_ReturnSystemTotal()

        CreatePickup()
        CreatePickupTD(8, Nothing, Nothing, Nothing, 100)
        CreatePickupTD(9, Nothing, Nothing, Nothing, 300)
        CreateSafe(9, Nothing, 0)

        Assert.AreEqual(300.0, Decimal.ToDouble(_Info.SystemTotal))

    End Sub

    <TestMethod()> Public Sub NonCashTender_SafeZeroAmount_PickupNegativeNonZeroAmount_ReturnSystemTotal()

        CreatePickup()
        CreatePickupTD(8, Nothing, Nothing, Nothing, 100)
        CreatePickupTD(9, Nothing, Nothing, Nothing, -600)
        CreateSafe(9, Nothing, 0)

        Assert.AreEqual(-600.0, Decimal.ToDouble(_Info.SystemTotal))

    End Sub

#End Region

#End Region

#Region "Suggested Total"

#Region "Cash Tender: Tenner"

    <TestMethod()> Public Sub CashTenderTenner_SafeNonZeroAmount_PickupNonZeroAmount_ReturnSuggestedTotal()

        CreatePickup()
        CreatePickupTD(1, 10, 0, 10, 300)
        CreatePickupTD(1, 5, 150, 500, 200)
        CreateSafe(1, 10, 150)

        Assert.AreEqual(450.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

    <TestMethod()> Public Sub CashTenderTenner_SafeZeroAmount_PickupZeroAmount_ReturnSuggestedTotal()

        CreatePickup()
        CreatePickupTD(1, 10, 0, 10, 0)
        CreatePickupTD(1, 5, 150, 500, 200)
        CreateSafe(1, 10, 0)

        Assert.AreEqual(0.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

    <TestMethod()> Public Sub CashTenderTenner_SafeNonZeroAmount_PickupZeroAmount_ReturnSuggestedTotal()

        CreatePickup()
        CreatePickupTD(1, 10, 0, 10, 0)
        CreatePickupTD(1, 5, 150, 500, 200)
        CreateSafe(1, 10, 100)

        Assert.AreEqual(100.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

    <TestMethod()> Public Sub CashTenderTenner_SafeZeroAmount_PickupNonZeroAmount_ReturnSuggestedTotal()

        CreatePickup()
        CreatePickupTD(1, 10, 0, 10, 300)
        CreatePickupTD(1, 5, 150, 500, 200)
        CreateSafe(1, 10, 0)

        Assert.AreEqual(300.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

#End Region

#Region "Cash Tender: Fiver"

    <TestMethod()> Public Sub CashTenderFiver_SuggestedTotal_LessThanOrEqualToBullionMultiple_ReturnSuggestedTotalZero()

        CreatePickup()
        CreatePickupTD(1, 10, 0, 10, 300)
        CreatePickupTD(1, 5, 150, 500, 500)
        CreateSafe(1, 5, 0)

        Assert.AreEqual(0.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

    <TestMethod()> Public Sub CashTenderFiver_SuggestedTotal_GreaterThanBullionMultiple_LessThanSafeMin_ReturnSuggestedTotalZero()

        CreatePickup()
        CreatePickupTD(1, 10, 0, 10, 300)
        CreatePickupTD(1, 5, 150, 500, 645)
        CreateSafe(1, 5, 0)

        Assert.AreEqual(0.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

    <TestMethod()> Public Sub CashTenderFiver_SuggestedTotal_GreaterThanBullionMultiple_GreaterThanOrEqualToSafeMin_ReturnSuggestedTotalNonZero()

        CreatePickup()
        CreatePickupTD(1, 10, 0, 10, 300)
        CreatePickupTD(1, 5, 150, 500, 650)
        CreateSafe(1, 5, 0)

        Assert.AreEqual(500.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

#End Region

#Region "Non Cash Tender"

    <TestMethod()> Public Sub NonCashTender_SafeZeroAmount_PickupZeroAmount_ReturnSuggestedTotal()

        CreatePickup()
        CreatePickupTD(8, Nothing, 0, 0.01, 100)
        CreatePickupTD(9, Nothing, 0, 0.01, 0)
        CreateSafe(9, Nothing, 0)

        Assert.AreEqual(0.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

    <TestMethod()> Public Sub NonCashTender_SafeZeroAmount_PickupNonZeroAmount_ReturnSuggestedTotal()

        CreatePickup()
        CreatePickupTD(8, Nothing, 0, 0.01, 100)
        CreatePickupTD(9, Nothing, 0, 0.01, 300)
        CreateSafe(9, Nothing, 0)

        Assert.AreEqual(300.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

    <TestMethod()> Public Sub NonCashTender_SafeZeroAmount_PickupNegativeNonZeroAmount_ReturnSuggestedTotal()

        CreatePickup()
        CreatePickupTD(8, Nothing, 0, 0.01, 100)
        CreatePickupTD(9, Nothing, 0, 0.01, -600)
        CreateSafe(9, Nothing, 0)

        Assert.AreEqual(-600.0, Decimal.ToDouble(_Info.SuggestedTotal))

    End Sub

#End Region

#End Region

#End Region

#Region "Private Test Functions"

    Private Sub CreateSafe(ByVal TenderID As Integer, _
                           ByVal DenominationID As Double, _
                           ByVal SystemSafe As Nullable(Of Double))

        Dim SafeDenom As New SafeDenomination

        With SafeDenom
            .CurrencyID = String.Empty

            .TenderID = TenderID
            .DenominationID = CType(DenominationID, Decimal)

            .TenderText = String.Empty
            .TenderReadOnly = Nothing
            .BankingAmountMultiple = Nothing

            .SystemSafe = CType(SystemSafe, Nullable(Of Decimal))

            .MainSafe = Nothing
            .ChangeSafe = Nothing

        End With

        _Info = New BankingBagInfo(SafeDenom, _Pickup)

    End Sub

    Private Sub CreatePickup()

        _Pickup = New TenderDenominationListCollection

    End Sub

    Private Sub CreatePickupTD(ByVal TenderID As Integer, _
                               ByVal DenominationID As Double, _
                               ByVal SafeMinimum As Double, _
                               ByVal BullionMultiple As Double, _
                               ByVal Amount As Nullable(Of Double))

        Dim Item As New TenderDenominationList

        With Item

            .DenominationID = CType(DenominationID, Decimal)
            .TenderText = String.Empty
            .TenderID = TenderID
            .TenderReadOnly = Nothing
            .BullionMultiple = CType(BullionMultiple, Decimal)
            .SafeMinimum = CType(SafeMinimum, Decimal)

            .Amount = CType(Amount, Nullable(Of Decimal))

        End With

        _Pickup.Add(Item)

    End Sub

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

End Class