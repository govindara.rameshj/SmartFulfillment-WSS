﻿<TestClass()> _
Public Class FloatVMWithComments_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unassign Float"

#Region "Test Classes"

    Private Class TestWithManualCheck
        Inherits FloatViewModelUnassignFloatWithManualCheck

        Private _AskToPerformManualCheck As Boolean

        Public Sub New(ByVal AskToPerformManualCheck As Boolean)

            _AskToPerformManualCheck = AskToPerformManualCheck

        End Sub

        Friend Overrides Function AskManualCheckQuestion() As Boolean

            Return _AskToPerformManualCheck

        End Function

        Friend Overrides Sub CreateManualCheckScreen(ByVal StartFloatID As Integer)

            'n/a

        End Sub

        Friend Overrides Sub ShowManualCheckScreen()

            'n/a

        End Sub

    End Class

#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub FloatUnAssignment_RequirementFalse_ReturnFloatViewModelUnassignFloatWithoutManualCheck()

        Dim V As IFloatVM

        ArrangeRequirementSwitchDependency(False)
        V = (New UnAssignFloatVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.FloatViewModelUnassignFloatWithoutManualCheck", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub FloatUnAssignment_RequirementTrue_ReturnFloatViewModelUnassignFloatWithManualCheck()

        Dim V As IFloatVM

        ArrangeRequirementSwitchDependency(True)
        V = (New UnAssignFloatVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.FloatViewModelUnassignFloatWithManualCheck", V.GetType.FullName)

    End Sub

#End Region

#Region "Class: FloatViewModelUnassignFloatWithoutManualCheck"

    <TestMethod()> Public Sub WithoutManualCheck_IsManualCheckRequired_ReturnFalse()

        Dim WithoutManualCheck As New FloatViewModelUnassignFloatWithoutManualCheck()

        Assert.IsFalse(WithoutManualCheck.IsManualCheckRequired)

    End Sub

    <TestMethod()> Public Sub WithoutManualCheck_PerformManualCheck_ReturnFalse()

        Dim WithoutManualCheck As New FloatViewModelUnassignFloatWithoutManualCheck()

        Assert.IsFalse(WithoutManualCheck.PerformManualCheck(Nothing))

    End Sub

#End Region

#Region "Class: FloatViewModelUnassignFloatWithManualCheck"

    <TestMethod()> Public Sub WithManualCheck_IsManualCheckRequired_SelectYes_ReturnTrue()

        Dim TestMC As New TestWithManualCheck(True)

        Assert.IsTrue(TestMC.IsManualCheckRequired)

    End Sub

    <TestMethod()> Public Sub WithManualCheck_IsManualCheckRequired_SelectNo_ReturnFalse()

        Dim TestMC As New TestWithManualCheck(False)

        Assert.IsFalse(TestMC.IsManualCheckRequired)

    End Sub

    <TestMethod()> Public Sub WithManualCheck_PerformManualCheck_ReturnStateCancelAndExit_ReturnFalse()

        Dim TestMC As New TestWithManualCheck(Nothing)

        ConfigureManualCheckScreenUsingTestVersion(TestMC, ManualCheck.ExitMode.CancelAndExit)

        Assert.IsFalse(TestMC.PerformManualCheck(Nothing))

    End Sub

    <TestMethod()> Public Sub WithManualCheck_PerformManualCheck_ReturnStateSaveAndExit_ReturnTrue()

        Dim TestMC As New TestWithManualCheck(Nothing)

        ConfigureManualCheckScreenUsingTestVersion(TestMC, ManualCheck.ExitMode.SaveAndExit)

        Assert.IsTrue(TestMC.PerformManualCheck(Nothing))

    End Sub

    <TestMethod()> Public Sub WithManualCheck_ReturnManualCheckScreenState_ReturnStateCancelAndExit_ReturnFalse()

        Dim MC As New FloatViewModelUnassignFloatWithManualCheck

        ConfigureManualCheckScreen(MC, ManualCheck.ExitMode.CancelAndExit)

        Assert.IsFalse(MC.ReturnManualCheckScreenState())

    End Sub

    <TestMethod()> Public Sub WithManualCheck_ReturnManualCheckScreenState_ReturnStateSaveAndExit_ReturnTrue()

        Dim MC As New FloatViewModelUnassignFloatWithManualCheck

        ConfigureManualCheckScreen(MC, ManualCheck.ExitMode.SaveAndExit)

        Assert.IsTrue(MC.ReturnManualCheckScreenState())

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim MockRep As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = MockRep.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        MockRep.ReplayAll()

    End Sub

    Private Sub ConfigureManualCheckScreenUsingTestVersion(ByVal WithManualCheck As TestWithManualCheck, _
                                                           ByVal State As ManualCheck.ExitMode)

        ArrangeRequirementSwitchDependency(True)
        WithManualCheck._ManualCheckScreen = New ManualCheck(Nothing)
        WithManualCheck._ManualCheckScreen._Exit = State

    End Sub

    Private Sub ConfigureManualCheckScreen(ByVal WithManualCheck As FloatViewModelUnassignFloatWithManualCheck, _
                                           ByVal State As ManualCheck.ExitMode)

        ArrangeRequirementSwitchDependency(True)
        WithManualCheck._ManualCheckScreen = New ManualCheck(Nothing)
        WithManualCheck._ManualCheckScreen._Exit = State

    End Sub

#End Region

#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub GetImplementation_RequirementSwitch_IsNotPresentOrEnabled_Returns_FloatVM()
        Dim FAVM As IFloatVM

        ArrangeRequirementSwitchDependency(False)
        FAVM = (New FloatVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.FloatVMWithoutComments", FAVM.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub GetImplementation_RequirementSwitch_IsPresentAndEnabled_Returns_FloatVM()
        Dim FAVM As IFloatVM

        ArrangeRequirementSwitchDependency(True)
        FAVM = (New FloatVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.FloatVMWithComments", FAVM.GetType.FullName)
    End Sub

#End Region

#Region "SetFloatGridControl Tests"

    <TestMethod()> _
    Public Sub SetFloatGridControl_Sets_FloatGrid_Member_To_FloatGrid_Parameter()
        Dim FVM As New FloatVMWithComments
        Dim expected As New FpSpread

        FVM.SetFloatGridControl(expected)
        Assert.AreSame(expected, FVM._FloatGrid)
    End Sub
#End Region

#Region "FloatGridFormat Tests"

    <TestMethod()> _
    Public Sub FloatGridFormat_AddsAtLeastEnoughColumnsToIncludeCommentsColumn()
        Dim FVM As New FloatVMWithComments

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            Assert.IsTrue(._FloatGrid.Sheets(0).ColumnCount >= FVM._CommentsColumnIndex + 1, "Expected " & (FVM._CommentsColumnIndex + 1).ToString & " or more columns, has " & ._FloatGrid.Sheets(0).ColumnCount.ToString & " columns.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatGridFormat_Sets5thColumnHeaderToComments()
        Dim FVM As New FloatVMWithComments

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            Assert.IsTrue(._FloatGrid.Sheets(0).Columns(FVM._CommentsColumnIndex).Label = "Comment", "Expected 'Comment' header for column " & (FVM._CommentsColumnIndex + 1).ToString & ", actually is '" & ._FloatGrid.Sheets(0).Columns(FVM._CommentsColumnIndex).Label & "'.")
        End With
    End Sub

    <TestMethod()> <Ignore()> _
    Public Sub FloatGridFormat_Sets5thColumnWidthTo240()
        Dim FVM As New FloatVMWithComments

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            Assert.IsTrue(._FloatGrid.Sheets(0).Columns(FVM._CommentsColumnIndex).Width = 240, "Expected column " & (FVM._CommentsColumnIndex + 1).ToString & " width to be 240, actually it is " & ._FloatGrid.Sheets(0).Columns(FVM._CommentsColumnIndex).Width.ToString & ".")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatGridFormat_SetsCommentsColumnToBeResizable()
        Dim FVM As New FloatVMWithComments

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            Assert.IsTrue(._FloatGrid.Sheets(0).Columns(FVM._CommentsColumnIndex).Resizable, "Expected column " & (FVM._CommentsColumnIndex + 1).ToString & " to be resizable, actually it is not.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatGridFormat_SetsCommentsColumnHorizontalAlignmentToBeGeneral()
        Dim FVM As New FloatVMWithComments

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            Assert.IsTrue(._FloatGrid.Sheets(0).Columns(FVM._CommentsColumnIndex).HorizontalAlignment = CellHorizontalAlignment.General, "Expected column " & (FVM._CommentsColumnIndex + 1).ToString & " to have 'General' horizontal alignment, actually it is '" & ._FloatGrid.Sheets(0).Columns(FVM._CommentsColumnIndex).HorizontalAlignment.ToString & "'.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatGridFormat_SetsHorizontalScrollBarPolicyToBeAsNeeded()
        Dim FVM As New FloatVMWithComments

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            Assert.IsTrue(._FloatGrid.HorizontalScrollBarPolicy = ScrollBarPolicy.AsNeeded, "Expected Float Grid to have 'AsNeeded' horizontal scrollbar policy, actually it is '" & ._FloatGrid.HorizontalScrollBarPolicy.ToString & "'.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatGridFormat_SetsLeftToBe11()
        Dim FVM As New FloatVMWithComments

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            Assert.IsTrue(._FloatGrid.Location.X = 11, "Expected Float Grid to have Left position of 11, actually it is " & ._FloatGrid.Location.X.ToString & ".")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatGridFormat_SetsWidthToBe496()
        Dim FVM As New FloatVMWithComments

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            Assert.IsTrue(._FloatGrid.Width = 496, "Expected Float Grid to have a width of 496, actually it is " & ._FloatGrid.Width.ToString & ".")
        End With
    End Sub
#End Region

#Region "FloatGridPopulate Tests"

    <TestMethod()> _
    Public Sub FloatGridPopulate_PopulatesCommentsColumnForEachRowWithAComment()
        Dim FVM As New TestFloatVMWithComments_WithTestDataExposed
        Dim actual As String = String.Empty
        Dim AllCommentsColumnsPopulatedWhereCommentIsInSourceData As Boolean
        Dim AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData As Boolean
        Dim TestData As FloatListCollection = Nothing

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            TestData = .GetFloatList(1)
            .FloatGridPopulate(TestData)
            With ._FloatGrid.Sheets(0)
                If .RowCount > 0 Then
                    AllCommentsColumnsPopulatedWhereCommentIsInSourceData = True
                    AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData = True
                    For Row As Integer = 0 To .RowCount - 1
                        Dim DataRowId As Integer

                        If Integer.TryParse(FVM._FloatListTestData.Rows(Row).Item(FVM._FloatList_FieldName_FloatID).ToString, DataRowId) Then
                            If IsDBNull(FVM._FloatListTestData.Rows(Row).Item(FVM._FloatList_FieldName_Comments)) Then
                                AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData = AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData And Not GridRowForDataRowHasComments(FVM._FloatGrid, DataRowId, FVM._CommentsColumnIndex)
                            Else
                                AllCommentsColumnsPopulatedWhereCommentIsInSourceData = AllCommentsColumnsPopulatedWhereCommentIsInSourceData And GridRowForDataRowHasComments(FVM._FloatGrid, DataRowId, FVM._CommentsColumnIndex)
                            End If
                        Else
                            Assert.Inconclusive("Failed to establish row id from test data so cannot confirm if comments should be present or not")
                        End If
                    Next
                End If
            End With
        End With
        If AllCommentsColumnsPopulatedWhereCommentIsInSourceData And AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData Then
            Assert.IsTrue(True)
        Else
            If Not AllCommentsColumnsPopulatedWhereCommentIsInSourceData Then
                Assert.Fail("Not all comments columns populated where there are comments in the test data")
            Else
                Assert.Fail("Some comments columns populated where there are no comments in the test data")
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Sub FloatGridPopulate_PopulatesCommentsCellWithTestComments()
        Dim FVM As New TestFloatVMWithComments_WithTestDataExposed
        Dim AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData As Boolean
        Dim TestData As FloatListCollection = Nothing

        With FVM
            .SetFloatGridControl(New FpSpread)
            .FloatGridFormat()
            TestData = .GetFloatList(1)
            .FloatGridPopulate(TestData)
            With ._FloatGrid.Sheets(0)
                If .RowCount > 0 Then
                    AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData = True
                    For Row As Integer = 0 To .RowCount - 1
                        Dim DataRowId As Integer

                        If Integer.TryParse(FVM._FloatListTestData.Rows(Row).Item(FVM._FloatList_FieldName_FloatID).ToString, DataRowId) Then
                            If Not IsDBNull(FVM._FloatListTestData.Rows(Row).Item(FVM._FloatList_FieldName_Comments)) Then
                                AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData = AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData And GridRowForDataRowHasMatchingComments(FVM._FloatGrid, DataRowId, FVM._CommentsColumnIndex, FVM._FloatListTestData.Rows(Row).Item(FVM._FloatList_FieldName_Comments).ToString)
                            End If
                        Else
                            Assert.Inconclusive("Failed to establish row id from test data so cannot confirm if comments should be present or not")
                        End If
                    Next
                End If
            End With
        End With
        Assert.IsTrue(AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData, "Not all Comments were populated.")
    End Sub
#End Region

#Region "SetFloatForm Tests"

    <TestMethod()> _
    Public Sub SetFloatForm_Sets_FloatForm_Member_To_FloatForm_Parameter()
        Dim FVM As New FloatVMWithComments
        Dim expected As New System.Windows.Forms.Form

        With FVM
            .SetFloatForm(expected)
            Assert.AreSame(expected, ._FloatForm)
        End With
    End Sub
#End Region

#Region "FloatFormFormat Tests"

    <TestMethod()> _
    Public Sub FloatFormFormat_Sets_FloatForm_Member_ToBe240Wider()
        Dim FVM As New FloatVMWithComments
        Dim NewForm As New System.Windows.Forms.Form
        Dim Expected As Integer = NewForm.Width + 240

        With FVM
            .SetFloatForm(NewForm)
            .FloatFormFormat()
            Assert.AreEqual(Expected, FVM._FloatForm.Width)
        End With
    End Sub
#End Region

#Region "Internals"

    Private Function GridRowForDataRowHasComments(ByRef FloatGrid As FpSpread, ByVal DataRowFloatId As Integer, ByVal CommentsColumnIndex As Integer) As Boolean
        Dim RowIndexToCheck As Integer = GetGridRowIndexForDataRow(FloatGrid, DataRowFloatId)

        GridRowForDataRowHasComments = False
        If RowIndexToCheck <> -1 Then
            With FloatGrid.ActiveSheet
                GridRowForDataRowHasComments = .Cells(RowIndexToCheck, CommentsColumnIndex).Value IsNot Nothing
            End With
        End If
    End Function

    Private Function GridRowForDataRowHasMatchingComments(ByRef FloatGrid As FpSpread, ByVal DataRowFloatId As Integer, ByVal CommentsColumnIndex As Integer, ByVal Comments As String) As Boolean
        Dim RowIndexToCheck As Integer = GetGridRowIndexForDataRow(FloatGrid, DataRowFloatId)

        GridRowForDataRowHasMatchingComments = False
        If RowIndexToCheck <> -1 Then
            With FloatGrid.ActiveSheet
                If .Cells(RowIndexToCheck, CommentsColumnIndex) IsNot Nothing Then
                    GridRowForDataRowHasMatchingComments = String.Compare(.Cells(RowIndexToCheck, CommentsColumnIndex).Value.ToString, Comments) = 0
                End If
            End With
        End If
    End Function

    Private Function GetGridRowIndexForDataRow(ByRef FloatGrid As FpSpread, ByVal DataRowFloatId As Integer) As Integer
        Dim Row As Integer

        GetGridRowIndexForDataRow = -1

        With FloatGrid.ActiveSheet
            For Row = 0 To .RowCount - 1
                If .Cells(Row, 0).Value IsNot Nothing Then
                    If .Cells(Row, 0).Value.ToString = DataRowFloatId.ToString Then
                        GetGridRowIndexForDataRow = Row
                        Exit For
                    End If
                End If
            Next
        End With
    End Function
#End Region

End Class