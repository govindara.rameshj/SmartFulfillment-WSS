﻿Friend Class TestBankingPickupCheckVMWIthComments_ProvidesTestGridAndCommentsToBeExtracted
    Inherits BankingPickupCheckVMWithComments

    Friend _TestGrid As FarPoint.Win.Spread.FpSpread
    Friend _Bag1Id As Integer = 1111
    Friend _Bag2Id As Integer = 2222
    Friend _Bag3Id As Integer = 3333
    Friend _TestCommentsForBag1 As String = "Bag 1 Test Comments"
    Friend _TestCommentsForBag2 As String = "Bag 2 Test Comments"
    Friend _TestCommentsForBag3 As String = "Bag 3 Test Comments"

    Public Sub New()

        SetupTestGrid()
    End Sub

    Friend Overridable Sub SetupTestGrid()
        Dim CurrentRowIndex As Integer = 0
        Dim CurrentColumnIndex As Integer = 0
        Dim ActiveSheet As New SheetView

        _TestGrid = New FarPoint.Win.Spread.FpSpread
        With ActiveSheet
            .ReferenceStyle = Model.ReferenceStyle.R1C1
            .RowCount = 0
            .ColumnCount = 0
        End With
        With _TestGrid
            SpreadGridSheetAdd(_TestGrid, ActiveSheet, True, String.Empty)
            SpreadRowAdd(.ActiveSheet, CurrentRowIndex)
            _CommentsRowIndex = CurrentRowIndex
            SpreadColumnAdd(.ActiveSheet, CurrentColumnIndex)
            .ActiveSheet.Columns(CurrentColumnIndex).Tag = Nothing
            SpreadColumnAdd(.ActiveSheet, CurrentColumnIndex)
            .ActiveSheet.Columns(CurrentColumnIndex).Tag = "TestBag1|" & _Bag1Id.ToString
            .ActiveSheet.Cells(_CommentsRowIndex, CurrentColumnIndex).Value = _TestCommentsForBag1
            SpreadColumnAdd(.ActiveSheet, CurrentColumnIndex)
            .ActiveSheet.Columns(CurrentColumnIndex).Tag = "TestBag2|" & _Bag2Id.ToString
            .ActiveSheet.Cells(_CommentsRowIndex, CurrentColumnIndex).Value = _TestCommentsForBag2
            SpreadColumnAdd(.ActiveSheet, CurrentColumnIndex)
            .ActiveSheet.Columns(CurrentColumnIndex).Tag = "TestBag3|" & _Bag3Id.ToString
            .ActiveSheet.Cells(_CommentsRowIndex, CurrentColumnIndex).Value = _TestCommentsForBag3
        End With
        _BankingPickupCheckGrid = _TestGrid
    End Sub
End Class
