﻿Friend Class TestFloatVMWithComments_WithTestDataExposed
    Inherits FloatVMWithComments

    Friend _FloatList As FloatListCollection
    Friend _FloatListTestData As DataTable
    Friend _FloatList_FieldName_FloatID As String = "FloatID"
    Friend _FloatList_FieldName_FloatSealNumber As String = "FloatSealNumber"
    Friend _FloatList_FieldName_FloatValue As String = "FloatValue"
    Friend _FloatList_FieldName_FloatCreatedFromUserName As String = "FloatCreatedFromUserName"
    Friend _FloatList_FieldName_AssignedToUserName As String = "AssignedToUserName"
    Friend _FloatList_FieldName_Comments As String = "Comments"

    Public Overrides Function GetFloatList(ByVal BankingPeriodID As Integer) As Core.FloatListCollection

        GetFloatListData()
        _FloatList = New FloatListCollection
        _FloatList.Load(_FloatListTestData)

        Return _FloatList
    End Function

    Private Sub GetFloatListData()

        _FloatListTestData = New DataTable
        With _FloatListTestData
            .Columns.Add(New DataColumn(_FloatList_FieldName_FloatID, GetType(Integer)))
            .Columns.Add(New DataColumn(_FloatList_FieldName_FloatSealNumber, GetType(String)))
            .Columns.Add(New DataColumn(_FloatList_FieldName_FloatValue, GetType(Decimal)))
            .Columns.Add(New DataColumn(_FloatList_FieldName_FloatCreatedFromUserName, GetType(String)))
            .Columns.Add(New DataColumn(_FloatList_FieldName_AssignedToUserName, GetType(String)))
            .Columns.Add(New DataColumn(_FloatList_FieldName_Comments, GetType(String)))
            .Rows.Add(PopulateFloatListTestRow(.NewRow, 1, "11111111", 111.11D, "Created From 1", "Assigned To 1", "Comment 1"))
            .Rows.Add(PopulateFloatListTestRow(.NewRow, 2, "22222222", 222.22D, "Created From 2", "Assigned To 2", Nothing))
            .Rows.Add(PopulateFloatListTestRow(.NewRow, 3, "33333333", 333.33D, "Created From 3", "Assigned To 3", Nothing))
            .Rows.Add(PopulateFloatListTestRow(.NewRow, 4, "44444444", 444.44D, "Created From 4", "Assigned To 4", "Comment 4"))
            .Rows.Add(PopulateFloatListTestRow(.NewRow, 5, "55555555", 555.55D, "Created From 5", "Assigned To 5", "Comment 5"))
        End With
    End Sub

    Private Function PopulateFloatListTestRow(ByRef ToPopulate As DataRow, _
                                              ByVal FloatID As Integer, _
                                              ByVal SealNumber As String, _
                                              ByVal Value As Decimal, _
                                              ByVal CreatedFromUserName As String, _
                                              ByVal AssignedToUserName As String, _
                                              ByVal Comments As String) As DataRow

        With ToPopulate
            .Item(_FloatList_FieldName_FloatID) = FloatID
            .Item(_FloatList_FieldName_FloatSealNumber) = FloatID
            .Item(_FloatList_FieldName_FloatValue) = Value
            .Item(_FloatList_FieldName_FloatCreatedFromUserName) = CreatedFromUserName
            .Item(_FloatList_FieldName_AssignedToUserName) = AssignedToUserName
            If Comments IsNot Nothing Then
                .Item(_FloatList_FieldName_Comments) = Comments
            End If
        End With

        Return ToPopulate
    End Function
End Class
