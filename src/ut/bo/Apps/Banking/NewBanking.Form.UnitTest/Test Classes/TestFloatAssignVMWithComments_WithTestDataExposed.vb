﻿Friend Class TestFloatAssignVMWithComments_WithTestDataExposed
    Inherits FloatAssignVMWithComments

    Friend _FloatBagTestData As DataTable
    Friend _TenderDenominationTestData As DataTable
    Friend _FloatBag_FieldName_TenderID As String = "TenderID"
    Friend _FloatBag_FieldName_DenominationID As String = "DenominationID"
    Friend _FloatBag_FieldName_FloatValue As String = "FloatValue"
    Friend _TenderDenomination_FieldName_ID As String = "ID"
    Friend _TenderDenomination_FieldName_CurrencyID As String = "CurrencyID"
    Friend _TenderDenomination_FieldName_TenderID As String = "TenderID"
    Friend _TenderDenomination_FieldName_DisplayText As String = "DisplayText"
    Friend _TenderDenomination_FieldName_BullionMultiple As String = "BullionMultiple"
    Friend _TenderDenomination_FieldName_BankingBagLimit As String = "BankingBagLimit"
    Friend _TenderDenomination_FieldName_SafeMinimum As String = "SafeMinimum"
    Friend _TenderDenomination_FieldName_Amendable As String = "Amendable"
    Friend _TestComments As String = "Test Comments"

    Friend Overrides Function GetTenderDenominationListCollection() As TenderDenominationListCollection

        _TenderDenominationTestData = GetTenderDenominationTestData()
        GetTenderDenominationListCollection = New TenderDenominationListCollection
        GetTenderDenominationListCollection.Load(_TenderDenominationTestData)
    End Function

    Friend Overrides Function GetFloatBagCollection(ByVal FloatID As Integer) As FloatBagCollection

        _FloatBagTestData = GetFloatBagCollectionTestData()
        GetFloatBagCollection = New FloatBagCollection
        GetFloatBagCollection.Load(_FloatBagTestData)
    End Function

    Friend Overrides Function GetFloatBagComments(ByVal FloatID As Integer) As String

        Return _TestComments
    End Function

    Private Function GetTenderDenominationTestData() As DataTable

        GetTenderDenominationTestData = New DataTable
        With GetTenderDenominationTestData
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_ID, GetType(Decimal)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_CurrencyID, GetType(String)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_TenderID, GetType(Integer)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_DisplayText, GetType(String)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_BullionMultiple, GetType(Decimal)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_BankingBagLimit, GetType(Decimal)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_SafeMinimum, GetType(Decimal)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_Amendable, GetType(Boolean)))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 100D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 50D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 20D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 10D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 5D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 2D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 1D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.5D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.2D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.1D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.05D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.02D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.01D))
        End With
    End Function

    Private Function GetFloatBagCollectionTestData() As DataTable
        Dim NewRow As DataRow = Nothing

        GetFloatBagCollectionTestData = New DataTable
        With GetFloatBagCollectionTestData
            .Columns.Add(New DataColumn(_FloatBag_FieldName_TenderID, GetType(Integer)))
            .Columns.Add(New DataColumn(_FloatBag_FieldName_DenominationID, GetType(Decimal)))
            .Columns.Add(New DataColumn(_FloatBag_FieldName_FloatValue, GetType(Decimal)))
            NewRow = .NewRow
            With NewRow
                .Item(_FloatBag_FieldName_TenderID) = 1
                .Item(_FloatBag_FieldName_DenominationID) = 10D
                .Item(_FloatBag_FieldName_FloatValue) = 100D
            End With
        End With
    End Function

    Private Function PopulateTenderDenominationTestRow(ByRef NewRow As DataRow, ByVal TenderDenomination As Decimal) As DataRow
        Dim DisplayText As String = String.Empty

        If TenderDenomination >= 1 Then
            DisplayText = Chr(163) & TenderDenomination.ToString("##0")
        Else
            DisplayText = (TenderDenomination * 100).ToString("##") & "p"
        End If
        With NewRow
            .Item(_TenderDenomination_FieldName_ID) = TenderDenomination
            .Item(_TenderDenomination_FieldName_CurrencyID) = "GBP"
            .Item(_TenderDenomination_FieldName_TenderID) = 1
            .Item(_TenderDenomination_FieldName_DisplayText) = DisplayText.PadRight(20, " "c)
            Select Case TenderDenomination
                Case Is > 5
                    .Item(_TenderDenomination_FieldName_BullionMultiple) = TenderDenomination
                    .Item(_TenderDenomination_FieldName_BankingBagLimit) = 10000D
                    .Item(_TenderDenomination_FieldName_SafeMinimum) = 0
                Case Is = 5
                    .Item(_TenderDenomination_FieldName_BullionMultiple) = 500
                    .Item(_TenderDenomination_FieldName_BankingBagLimit) = 10000D
                    .Item(_TenderDenomination_FieldName_SafeMinimum) = 150
                Case Else
                    .Item(_TenderDenomination_FieldName_BullionMultiple) = 0D
                    .Item(_TenderDenomination_FieldName_BankingBagLimit) = 0D
                    .Item(_TenderDenomination_FieldName_SafeMinimum) = 0
            End Select
            .Item(_TenderDenomination_FieldName_Amendable) = 0
        End With
        PopulateTenderDenominationTestRow = NewRow
    End Function
End Class
