﻿Friend Class TestFloatedPickupVMWithComments_WithTestDataExposed
    Inherits FloatedPickupVMWithComments

    Friend _FloatedPickupList As FloatedPickupListCollection
    Friend _FloatedPickupListTestData As DataTable
    Friend _FloatedPickupList_FieldName_CashierID As String = "CashierID"
    Friend _FloatedPickupList_FieldName_StartFloatSealNumber As String = "StartFloatSealNumber"
    Friend _FloatedPickupList_FieldName_CashierUserName As String = "CashierUserName"
    Friend _FloatedPickupList_FieldName_PickupSealNumber As String = "PickupSealNumber"
    Friend _FloatedPickupList_FieldName_PickupComment As String = "PickupComment"

    Public Overrides Function GetFloatedPickupList(ByVal BankingPeriodID As Integer) As Core.FloatedPickupListCollection

        GetFloatedPickupListData()
        _FloatedPickupList = New FloatedPickupListCollection
        _FloatedPickupList.Load(_FloatedPickupListTestData)

        Return _FloatedPickupList
    End Function

    Private Sub GetFloatedPickupListData()

        _FloatedPickupListTestData = New DataTable
        With _FloatedPickupListTestData
            .Columns.Add(New DataColumn(_FloatedPickupList_FieldName_CashierID, GetType(Integer)))
            .Columns.Add(New DataColumn(_FloatedPickupList_FieldName_StartFloatSealNumber, GetType(String)))
            .Columns.Add(New DataColumn(_FloatedPickupList_FieldName_CashierUserName, GetType(String)))
            .Columns.Add(New DataColumn(_FloatedPickupList_FieldName_PickupSealNumber, GetType(String)))
            .Columns.Add(New DataColumn(_FloatedPickupList_FieldName_PickupComment, GetType(String)))
            .Rows.Add(PopulateFloatedPickupListTestRow(.NewRow, 1, "11111111", "Cashier Number 1", "11111112", "Comment 1"))
            .Rows.Add(PopulateFloatedPickupListTestRow(.NewRow, 2, "22222222", "Cashier Number 2", "22222223", Nothing))
            .Rows.Add(PopulateFloatedPickupListTestRow(.NewRow, 3, "33333333", "Cashier Number 3", "33333334", Nothing))
            .Rows.Add(PopulateFloatedPickupListTestRow(.NewRow, 4, "44444444", "Cashier Number 4", "44444445", "Comment 4"))
            .Rows.Add(PopulateFloatedPickupListTestRow(.NewRow, 5, "55555555", "Cashier Number 5", "55555556", "Comment 5"))
        End With
    End Sub

    Private Function PopulateFloatedPickupListTestRow(ByRef ToPopulate As DataRow, _
                                              ByVal CashierID As Integer, _
                                              ByVal StartFloatSealNumber As String, _
                                              ByVal CashierUerName As String, _
                                              ByVal PickupSealNumber As String, _
                                              ByVal Comments As String) As DataRow

        With ToPopulate
            .Item(_FloatedPickupList_FieldName_CashierID) = CashierID
            .Item(_FloatedPickupList_FieldName_StartFloatSealNumber) = StartFloatSealNumber
            .Item(_FloatedPickupList_FieldName_CashierUserName) = CashierUerName
            .Item(_FloatedPickupList_FieldName_PickupSealNumber) = PickupSealNumber
            If Not String.IsNullOrEmpty(Comments) Then
                .Item(_FloatedPickupList_FieldName_PickupComment) = Comments
            End If
        End With

        Return ToPopulate
    End Function
End Class
