﻿Friend Class TestBankingPickupCheckVMWithComments_OverridesBankingPeriodIsSetToReturnFalse_OverridesLoadAllCashiersToFlagIfItIsCalled
    Inherits BankingPickupCheckVMWithComments

    Private _loadAllCashiersWasCalled As Boolean

    Friend Overrides Function BankingPeriodIDIsSet() As Boolean

        Return False
    End Function

    Friend Overrides Sub LoadAllCashiers(ByRef ToLoadInto As Core.ActiveCashierListCollection)

        LoadAllCashiersWasCalled = True
    End Sub

    Public Property LoadAllCashiersWasCalled() As Boolean
        Get
            Return _loadAllCashiersWasCalled
        End Get
        Set(ByVal value As Boolean)
            _loadAllCashiersWasCalled = value
        End Set
    End Property
End Class
