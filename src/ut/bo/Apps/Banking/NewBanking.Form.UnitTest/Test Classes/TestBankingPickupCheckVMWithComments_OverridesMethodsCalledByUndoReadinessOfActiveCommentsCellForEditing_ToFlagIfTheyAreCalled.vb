﻿Friend Class TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled
    Inherits TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean)

        MyBase.New(SetBankingPickupCheckGridControlIsSetToReturnTrue)
    End Sub

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean, ByVal SetActiveSheetIsSetToReturnTrue As Boolean)

        MyBase.New(SetBankingPickupCheckGridControlIsSetToReturnTrue, SetActiveSheetIsSetToReturnTrue)
    End Sub

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean, ByVal SetActiveSheetIsSetToReturnTrue As Boolean, ByVal SetCellIsCommentsCellToReturnTrue As Boolean)

        MyBase.New(SetBankingPickupCheckGridControlIsSetToReturnTrue, SetActiveSheetIsSetToReturnTrue, SetCellIsCommentsCellToReturnTrue)
    End Sub

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean, ByVal SetActiveSheetIsSetToReturnTrue As Boolean, ByVal SetCellIsCommentsCellToReturnTrue As Boolean, ByVal SetColumnIsPotentiallyEditableToReturnTrue As Boolean)

        MyBase.New(SetBankingPickupCheckGridControlIsSetToReturnTrue, SetActiveSheetIsSetToReturnTrue, SetCellIsCommentsCellToReturnTrue, SetColumnIsPotentiallyEditableToReturnTrue)
    End Sub

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean, ByVal SetActiveSheetIsSetToReturnTrue As Boolean, ByVal SetCellIsCommentsCellToReturnTrue As Boolean, ByVal SetColumnIsPotentiallyEditableToReturnTrue As Boolean, ByVal SetBagNotAlreadyMarkedAsCheckedToReturnTrue As Boolean)

        MyBase.New(SetBankingPickupCheckGridControlIsSetToReturnTrue, SetActiveSheetIsSetToReturnTrue, SetCellIsCommentsCellToReturnTrue, SetCellIsCommentsCellToReturnTrue, SetBagNotAlreadyMarkedAsCheckedToReturnTrue)
    End Sub

    Friend Overrides Sub SetupGrid()

        MyBase.SetupGrid()
        With _BankingPickupCheckGrid
            .EditMode = True
            .EditModeReplace = False
            .EditModePermanent = True
        End With
    End Sub
End Class
