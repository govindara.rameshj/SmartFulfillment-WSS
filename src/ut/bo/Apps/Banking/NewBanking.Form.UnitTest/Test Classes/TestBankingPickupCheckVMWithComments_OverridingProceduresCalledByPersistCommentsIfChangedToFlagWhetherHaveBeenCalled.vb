﻿Friend Class TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled
    Inherits BankingPickupCheckVMWithComments

    Private _activeCashierListsIsSetShouldReturnTrue As Boolean
    Private _NewCommentsDoNotMatchOldCommentsShouldReturnTrue As Boolean
    Private _getCommentsWasCalled As Boolean
    Private _getBagCommentFromActiveCashierListsWasCalled As Boolean
    Private _NewCommentsDoNotMatchOldCommentsWasCalled As Boolean
    Private _pickupBagSaveCommentsWasCalled As Boolean
    Private _comments As String = "Comments have been got"

    Public Sub New(ByVal ActiveCashierListsIsSetShouldReturnTrue As Boolean)

        _activeCashierListsIsSetShouldReturnTrue = ActiveCashierListsIsSetShouldReturnTrue
        _ActiveCashierLists = New ActiveCashierListCollection
    End Sub

    Public Sub New(ByVal ActiveCashierListsIsSetShouldReturnTrue As Boolean, ByVal NewCommentsDoNotMatchOldCommentsShouldReturnTrue As Boolean)

        Me.New(ActiveCashierListsIsSetShouldReturnTrue)
        _NewCommentsDoNotMatchOldCommentsShouldReturnTrue = NewCommentsDoNotMatchOldCommentsShouldReturnTrue
    End Sub

    Friend Overrides Function ActiveCashierListsIsSet() As Boolean

        Return _activeCashierListsIsSetShouldReturnTrue
    End Function

    Public Overrides Function GetComments(ByVal BagID As Integer) As String

        GetCommentsWasCalled = True
        GetComments = _comments
    End Function

    Friend Overrides Function GetBagCommentFromActiveCashierLists(ByVal PickupBagID As Integer) As String

        GetBagCommentFromActiveCashierListsWasCalled = True
        GetBagCommentFromActiveCashierLists = _comments
    End Function

    Friend Overrides Function NewCommentsDoNotMatchOldComments(ByVal NewComments As String, ByVal OldComments As String) As Boolean

        NewCommentsDoNotMatchOldCommentsWasCalled = True
        Return _NewCommentsDoNotMatchOldCommentsShouldReturnTrue
    End Function

    Friend Overrides Sub PickupBagSaveComments(ByVal PickupBag As Core.Pickup, ByVal NewComments As String)

        PickupBagSaveCommentsWasCalled = True
    End Sub

    Public Property GetCommentsWasCalled() As Boolean
        Get
            Return _getCommentsWasCalled
        End Get
        Set(ByVal value As Boolean)
            _getCommentsWasCalled = value
        End Set
    End Property

    Public Property GetBagCommentFromActiveCashierListsWasCalled() As Boolean
        Get
            Return _getBagCommentFromActiveCashierListsWasCalled
        End Get
        Set(ByVal value As Boolean)
            _getBagCommentFromActiveCashierListsWasCalled = value
        End Set
    End Property

    Public Property NewCommentsDoNotMatchOldCommentsWasCalled() As Boolean
        Get
            Return _NewCommentsDoNotMatchOldCommentsWasCalled
        End Get
        Set(ByVal value As Boolean)
            _NewCommentsDoNotMatchOldCommentsWasCalled = value
        End Set
    End Property

    Public Property PickupBagSaveCommentsWasCalled() As Boolean
        Get
            Return _pickupBagSaveCommentsWasCalled
        End Get
        Set(ByVal value As Boolean)
            _pickupBagSaveCommentsWasCalled = value
        End Set
    End Property
End Class
