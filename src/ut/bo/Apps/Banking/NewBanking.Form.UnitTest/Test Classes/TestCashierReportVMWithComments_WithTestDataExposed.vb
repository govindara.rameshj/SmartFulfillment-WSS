﻿Friend Class TestCashierReportVMWithComments_WithTestDataExposed
    Inherits CashierReportVMWithComments

    Friend _TenderDenomination_FieldName_ID As String = "ID"
    Friend _TenderDenomination_FieldName_CurrencyID As String = "CurrencyID"
    Friend _TenderDenomination_FieldName_TenderID As String = "TenderID"
    Friend _TenderDenomination_FieldName_DisplayText As String = "DisplayText"
    Friend _TenderDenomination_FieldName_BullionMultiple As String = "BullionMultiple"
    Friend _TenderDenomination_FieldName_BankingBagLimit As String = "BankingBagLimit"
    Friend _TenderDenomination_FieldName_SafeMinimum As String = "SafeMinimum"
    Friend _TenderDenomination_FieldName_Amendable As String = "Amendable"
    Friend _TestTenderDenomList As DataTable
    Friend _TestActiveCashierLists As ActiveCashierListCollection
    Friend _TestActiveCashierList As ActiveCashierList
    Friend _ActiveCashierList_FieldName_CashierID As String = "CashierID"
    Friend _ActiveCashierList_FieldName_CashierUserName As String = "CashierUserName"
    Friend _ActiveCashierList_FieldName_CashierEmployeeCode As String = "CashierEmployeeCode"
    Friend _ActiveCashierList_FieldName_SystemSales As String = "SystemSales"
    Friend _ActiveCashierList_FieldName_TotalPickups As String = "TotalPickups"
    Friend _ActiveCashierList_FieldName_StartFloatValue As String = "StartFloatValue"
    Friend _ActiveCashierList_FieldName_NewFloatValue As String = "NewFloatValue"

    Friend _EndOfDayPickupListTestData As DataTable
    Friend _CashDropListTestData As DataTable
    Friend _PickupList_FieldName_PickupID As String = "PickupID"
    Friend _PickupList_FieldName_PickupPeriodID As String = "PickupPeriodID"
    Friend _PickupList_FieldName_PickupDate As String = "PickupDate"
    Friend _PickupList_FieldName_PickupSealNumber As String = "PickupSealNumber"
    Friend _PickupList_FieldName_PickupValue As String = "PickupValue"
    Friend _PickupList_FieldName_PickupComment As String = "PickupComment"

    Friend _TestPickupComments As String = "Test Pickup Comments"
    Friend _PickupCommentsColumnIndex As Integer = 1
    Friend _TestCashDropComments As String = "Test Cash Drop Comments"
    Friend _CashDropCommentsColumnIndex As Integer = 2

    Public Sub New()

        _TestActiveCashierLists = New ActiveCashierListCollection
        _TestActiveCashierLists.Load(GetActiveCashierListTestData())
        _EndOfDayPickupListTestData = GetEndOfDayPickupListTestData()
        _CashDropListTestData = GetCashDropListTestData()
        _TestActiveCashierList = _TestActiveCashierLists.Item(0)
        With _TestActiveCashierList
            .EndOfDayPickupList = New PickupCollection
            .EndOfDayPickupList.Load(_EndOfDayPickupListTestData)
            .CashDropsPickupList = New PickupCollection
            .CashDropsPickupList.Load(_CashDropListTestData)
        End With
    End Sub

    Friend Overrides Sub LoadTenderDenominations()

        _TestTenderDenomList = GetTenderDenominationTestData()
        _TenderDenomList = New TenderDenominationListCollection
        _TenderDenomList.Load(_TestTenderDenomList)
    End Sub

    Friend Overridable Function GetActiveCashierListTestData() As DataTable
        Dim NewRow As DataRow

        GetActiveCashierListTestData = New DataTable
        With GetActiveCashierListTestData
            .Columns.Add(New DataColumn(_ActiveCashierList_FieldName_CashierID, GetType(Integer)))
            .Columns.Add(New DataColumn(_ActiveCashierList_FieldName_CashierUserName, GetType(String)))
            .Columns.Add(New DataColumn(_ActiveCashierList_FieldName_CashierEmployeeCode, GetType(String)))
            .Columns.Add(New DataColumn(_ActiveCashierList_FieldName_SystemSales, GetType(Decimal)))
            .Columns.Add(New DataColumn(_ActiveCashierList_FieldName_TotalPickups, GetType(Decimal)))
            .Columns.Add(New DataColumn(_ActiveCashierList_FieldName_StartFloatValue, GetType(Decimal)))
            .Columns.Add(New DataColumn(_ActiveCashierList_FieldName_NewFloatValue, GetType(Decimal)))
            NewRow = .NewRow
            With NewRow
                .Item(_ActiveCashierList_FieldName_CashierID) = 1
                .Item(_ActiveCashierList_FieldName_CashierUserName) = "Carvis Jocker"
                .Item(_ActiveCashierList_FieldName_CashierEmployeeCode) = "200"
                .Item(_ActiveCashierList_FieldName_SystemSales) = 100.0
                .Item(_ActiveCashierList_FieldName_TotalPickups) = 50.0
                .Item(_ActiveCashierList_FieldName_StartFloatValue) = 120.0
                .Item(_ActiveCashierList_FieldName_NewFloatValue) = 100.0
            End With
            .Rows.Add(NewRow)
        End With
    End Function

    Friend Overridable Function GetEndOfDayPickupListTestData() As DataTable
        Dim NewRow As DataRow

        GetEndOfDayPickupListTestData = New DataTable
        With GetEndOfDayPickupListTestData
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupID, GetType(Integer)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupPeriodID, GetType(Integer)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupDate, GetType(Date)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupSealNumber, GetType(String)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupValue, GetType(Decimal)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupComment, GetType(String)))
            NewRow = .NewRow
            With NewRow
                .Item(_PickupList_FieldName_PickupID) = 1
                .Item(_PickupList_FieldName_PickupPeriodID) = 2000
                .Item(_PickupList_FieldName_PickupDate) = Date.Today
                .Item(_PickupList_FieldName_PickupSealNumber) = "20002000"
                .Item(_PickupList_FieldName_PickupValue) = 202.0
                .Item(_PickupList_FieldName_PickupComment) = _TestPickupComments
            End With
            .Rows.Add(NewRow)
        End With
    End Function

    Friend Overridable Function GetCashDropListTestData() As DataTable
        Dim NewRow As DataRow

        GetCashDropListTestData = New DataTable
        With GetCashDropListTestData
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupID, GetType(Integer)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupPeriodID, GetType(Integer)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupDate, GetType(Date)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupSealNumber, GetType(String)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupValue, GetType(Decimal)))
            .Columns.Add(New DataColumn(_PickupList_FieldName_PickupComment, GetType(String)))
            NewRow = .NewRow
            With NewRow
                .Item(_PickupList_FieldName_PickupID) = 2
                .Item(_PickupList_FieldName_PickupPeriodID) = 2000
                .Item(_PickupList_FieldName_PickupDate) = Date.Today
                .Item(_PickupList_FieldName_PickupSealNumber) = "30003000"
                .Item(_PickupList_FieldName_PickupValue) = 303.0
                .Item(_PickupList_FieldName_PickupComment) = _TestCashDropComments
            End With
            .Rows.Add(NewRow)
        End With
    End Function

    Friend Overridable Function GetTenderDenominationTestData() As DataTable

        GetTenderDenominationTestData = New DataTable
        With GetTenderDenominationTestData
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_ID, GetType(Decimal)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_CurrencyID, GetType(String)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_TenderID, GetType(Integer)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_DisplayText, GetType(String)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_BullionMultiple, GetType(Decimal)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_BankingBagLimit, GetType(Decimal)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_SafeMinimum, GetType(Decimal)))
            .Columns.Add(New DataColumn(_TenderDenomination_FieldName_Amendable, GetType(Boolean)))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 100D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 50D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 20D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 10D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 5D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 2D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 1D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.5D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.2D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.1D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.05D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.02D))
            .Rows.Add(PopulateTenderDenominationTestRow(.NewRow, 0.01D))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 2, "Cheque"))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 3, "Visa"))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 4, "Old Tokens"))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 5, "Project Loan"))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 6, "Coupon"))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 7, "Gift Voucher"))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 8, "Maestro"))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 9, "AmEx"))
            .Rows.Add(PopulateNonCashTenderDenominationTestRow(.NewRow, 10, "H/O Cheque"))
        End With
    End Function

    Private Function PopulateTenderDenominationTestRow(ByRef NewRow As DataRow, ByVal TenderDenomination As Decimal) As DataRow
        Dim DisplayText As String = String.Empty

        If TenderDenomination >= 1 Then
            DisplayText = Chr(163) & TenderDenomination.ToString("##0")
        Else
            DisplayText = (TenderDenomination * 100).ToString("##") & "p"
        End If
        With NewRow
            .Item(_TenderDenomination_FieldName_ID) = TenderDenomination
            .Item(_TenderDenomination_FieldName_CurrencyID) = "GBP"
            .Item(_TenderDenomination_FieldName_TenderID) = 1
            .Item(_TenderDenomination_FieldName_DisplayText) = DisplayText.PadRight(20, " "c)
            Select Case TenderDenomination
                Case Is > 5
                    .Item(_TenderDenomination_FieldName_BullionMultiple) = TenderDenomination
                    .Item(_TenderDenomination_FieldName_BankingBagLimit) = 10000D
                    .Item(_TenderDenomination_FieldName_SafeMinimum) = 0
                Case Is = 5
                    .Item(_TenderDenomination_FieldName_BullionMultiple) = 500
                    .Item(_TenderDenomination_FieldName_BankingBagLimit) = 10000D
                    .Item(_TenderDenomination_FieldName_SafeMinimum) = 150
                Case Else
                    .Item(_TenderDenomination_FieldName_BullionMultiple) = 0D
                    .Item(_TenderDenomination_FieldName_BankingBagLimit) = 0D
                    .Item(_TenderDenomination_FieldName_SafeMinimum) = 0
            End Select
            .Item(_TenderDenomination_FieldName_Amendable) = 0
        End With
        PopulateTenderDenominationTestRow = NewRow
    End Function

    Private Function PopulateNonCashTenderDenominationTestRow(ByRef NewRow As DataRow, ByVal TenderID As Integer, ByVal DisplayText As String) As DataRow
        Dim BankingBagLimit As Decimal

        If TenderID = 9 Then
            BankingBagLimit = 10000D
        Else
            BankingBagLimit = 100000D
        End If
        With NewRow
            .Item(_TenderDenomination_FieldName_ID) = 0.01
            .Item(_TenderDenomination_FieldName_CurrencyID) = "GBP"
            .Item(_TenderDenomination_FieldName_TenderID) = TenderID
            .Item(_TenderDenomination_FieldName_DisplayText) = DisplayText
            .Item(_TenderDenomination_FieldName_BullionMultiple) = 0.01
            .Item(_TenderDenomination_FieldName_BankingBagLimit) = BankingBagLimit
            .Item(_TenderDenomination_FieldName_SafeMinimum) = 0
            .Item(_TenderDenomination_FieldName_Amendable) = 1
        End With
        PopulateNonCashTenderDenominationTestRow = NewRow
    End Function
End Class
