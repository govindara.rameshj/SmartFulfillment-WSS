﻿Friend Class TestUnFloatedPickupVMWithComments_WithTestDataExposed
    Inherits UnFloatedPickupVMWithComments

    Friend _UnFloatedPickupList As UnFloatedPickupListCollection
    Friend _UnFloatedPickupListTestData As DataTable
    Friend _UnFloatedPickupList_FieldName_CashierID As String = "CashierID"
    Friend _UnFloatedPickupList_FieldName_StartFloatSealNumber As String = "StartFloatSealNumber"
    Friend _UnFloatedPickupList_FieldName_CashierUserName As String = "CashierUserName"
    Friend _UnFloatedPickupList_FieldName_PickupSealNumber As String = "PickupSealNumber"
    Friend _UnFloatedPickupList_FieldName_PickupComment As String = "PickupComment"

    'Public Overrides Function GetUnFloatedPickupList(ByVal BankingPeriodID As Integer) As Core.UnFloatedPickupListCollection

    '    GetUnFloatedPickupListData()
    '    _UnFloatedPickupList = New UnFloatedPickupListCollection
    '    _UnFloatedPickupList.Load(_UnFloatedPickupListTestData)

    '    Return _UnFloatedPickupList
    'End Function

    Private Sub GetUnFloatedPickupListData()

        _UnFloatedPickupListTestData = New DataTable
        With _UnFloatedPickupListTestData
            .Columns.Add(New DataColumn(_UnFloatedPickupList_FieldName_CashierID, GetType(Integer)))
            .Columns.Add(New DataColumn(_UnFloatedPickupList_FieldName_StartFloatSealNumber, GetType(String)))
            .Columns.Add(New DataColumn(_UnFloatedPickupList_FieldName_CashierUserName, GetType(String)))
            .Columns.Add(New DataColumn(_UnFloatedPickupList_FieldName_PickupSealNumber, GetType(String)))
            .Columns.Add(New DataColumn(_UnFloatedPickupList_FieldName_PickupComment, GetType(String)))
            .Rows.Add(PopulateUnFloatedPickupListTestRow(.NewRow, 1, "11111111", "Cashier Number 1", "11111112", "Comment 1"))
            .Rows.Add(PopulateUnFloatedPickupListTestRow(.NewRow, 2, "22222222", "Cashier Number 2", "22222223", Nothing))
            .Rows.Add(PopulateUnFloatedPickupListTestRow(.NewRow, 3, "33333333", "Cashier Number 3", "33333334", Nothing))
            .Rows.Add(PopulateUnFloatedPickupListTestRow(.NewRow, 4, "44444444", "Cashier Number 4", "44444445", "Comment 4"))
            .Rows.Add(PopulateUnFloatedPickupListTestRow(.NewRow, 5, "55555555", "Cashier Number 5", "55555556", "Comment 5"))
        End With
    End Sub

    Private Function PopulateUnFloatedPickupListTestRow(ByRef ToPopulate As DataRow, _
                                              ByVal CashierID As Integer, _
                                              ByVal StartFloatSealNumber As String, _
                                              ByVal CashierUerName As String, _
                                              ByVal PickupSealNumber As String, _
                                              ByVal Comments As String) As DataRow

        With ToPopulate
            .Item(_UnFloatedPickupList_FieldName_CashierID) = CashierID
            .Item(_UnFloatedPickupList_FieldName_StartFloatSealNumber) = StartFloatSealNumber
            .Item(_UnFloatedPickupList_FieldName_CashierUserName) = CashierUerName
            .Item(_UnFloatedPickupList_FieldName_PickupSealNumber) = PickupSealNumber
            If Not String.IsNullOrEmpty(Comments) Then
                .Item(_UnFloatedPickupList_FieldName_PickupComment) = Comments
            End If
        End With

        Return ToPopulate
    End Function
End Class
