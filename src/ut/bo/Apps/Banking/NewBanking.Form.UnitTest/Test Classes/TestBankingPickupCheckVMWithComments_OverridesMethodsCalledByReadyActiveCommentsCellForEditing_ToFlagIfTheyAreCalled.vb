﻿Friend Class TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled
    Inherits BankingPickupCheckVMWithComments

    Private _SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean
    Private _SetActiveSheetIsSetToReturnTrue As Boolean
    Private _SetCellIsCommentsCellToReturnTrue As Boolean
    Private _SetColumnIsPotentiallyEditableToReturnTrue As Boolean
    Private _SetBagNotAlreadyMarkedAsCheckedToReturnTrue As Boolean
    Private _setUpActiveCellForTextEditingAndStartWasCalled As Boolean

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean)

        _SetBankingPickupCheckGridControlIsSetToReturnTrue = SetBankingPickupCheckGridControlIsSetToReturnTrue
        SetupGrid()
    End Sub

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean, ByVal SetActiveSheetIsSetToReturnTrue As Boolean)

        Me.New(SetBankingPickupCheckGridControlIsSetToReturnTrue)
        _SetActiveSheetIsSetToReturnTrue = SetActiveSheetIsSetToReturnTrue
    End Sub

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean, ByVal SetActiveSheetIsSetToReturnTrue As Boolean, ByVal SetCellIsCommentsCellToReturnTrue As Boolean)

        Me.New(SetBankingPickupCheckGridControlIsSetToReturnTrue, SetActiveSheetIsSetToReturnTrue)
        _SetCellIsCommentsCellToReturnTrue = SetCellIsCommentsCellToReturnTrue
    End Sub

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean, ByVal SetActiveSheetIsSetToReturnTrue As Boolean, ByVal SetCellIsCommentsCellToReturnTrue As Boolean, ByVal SetColumnIsPotentiallyEditableToReturnTrue As Boolean)

        Me.New(SetBankingPickupCheckGridControlIsSetToReturnTrue, SetActiveSheetIsSetToReturnTrue, SetCellIsCommentsCellToReturnTrue)
        _SetColumnIsPotentiallyEditableToReturnTrue = SetColumnIsPotentiallyEditableToReturnTrue
    End Sub

    Public Sub New(ByVal SetBankingPickupCheckGridControlIsSetToReturnTrue As Boolean, ByVal SetActiveSheetIsSetToReturnTrue As Boolean, ByVal SetCellIsCommentsCellToReturnTrue As Boolean, ByVal SetColumnIsPotentiallyEditableToReturnTrue As Boolean, ByVal SetBagNotAlreadyMarkedAsCheckedToReturnTrue As Boolean)

        Me.New(SetBankingPickupCheckGridControlIsSetToReturnTrue, SetActiveSheetIsSetToReturnTrue, SetCellIsCommentsCellToReturnTrue, SetColumnIsPotentiallyEditableToReturnTrue)
        _SetBagNotAlreadyMarkedAsCheckedToReturnTrue = SetBagNotAlreadyMarkedAsCheckedToReturnTrue
    End Sub

    Friend Overrides Function BankingPickupCheckGridControlIsSet() As Boolean

        Return _SetBankingPickupCheckGridControlIsSetToReturnTrue
    End Function

    Friend Overrides Function ActiveSheetIsSet() As Boolean

        Return _SetActiveSheetIsSetToReturnTrue
    End Function

    Friend Overrides Function CellIsCommentsCell(ByRef AmIACommentsCell As FarPoint.Win.Spread.Cell) As Boolean

        Return _SetCellIsCommentsCellToReturnTrue
    End Function

    Friend Overrides Function ColumnIsPotentiallyEditable(ByVal ColumnIndex As Integer) As Boolean

        Return _SetColumnIsPotentiallyEditableToReturnTrue
    End Function

    Friend Overrides Function BagNotAlreadyMarkedAsChecked(ByVal BagColumnIndex As Integer) As Boolean

        Return _SetBagNotAlreadyMarkedAsCheckedToReturnTrue
    End Function

    Friend Overrides Sub SetUpActiveCellForTextEditingAndStart(ByVal CellRowIndex As Integer, ByVal CellColumnIndex As Integer)

        MyBase.SetUpActiveCellForTextEditingAndStart(CellRowIndex, CellColumnIndex)
        SetUpActiveCellForTextEditingAndStartWasCalled = True
    End Sub

    'Friend Overrides Sub StartEditingInBankingPickupGridActiveCell()


    'End Sub

    Public Property SetUpActiveCellForTextEditingAndStartWasCalled() As Boolean
        Get
            Return _setUpActiveCellForTextEditingAndStartWasCalled
        End Get
        Set(ByVal value As Boolean)
            _setUpActiveCellForTextEditingAndStartWasCalled = value
        End Set
    End Property

    Friend Overridable Sub SetupGrid()
        Dim NewSheet As New SheetView
        Dim CurrentRowIndex As Integer
        Dim CurrentColumnIndex As Integer

        _BankingPickupCheckGrid = New FpSpread
        With NewSheet
            .ReferenceStyle = Model.ReferenceStyle.R1C1
            .RowCount = 0
            .ColumnCount = 0
        End With
        SpreadGridSheetAdd(_BankingPickupCheckGrid, NewSheet, True, String.Empty)
        With _BankingPickupCheckGrid
            SpreadRowAdd(.ActiveSheet, CurrentRowIndex)
            SpreadRowAdd(.ActiveSheet, CurrentRowIndex)
            SpreadColumnAdd(.ActiveSheet, CurrentColumnIndex)
            SpreadColumnAdd(.ActiveSheet, CurrentColumnIndex)
            SpreadCellSetAsTextFormat(.ActiveSheet, 0, 1)
        End With
    End Sub
End Class
