﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class UnFloatedPickupVMWithComments_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "SetUnFloatedPickupGridControl Tests"

    <TestMethod()> _
    Public Sub SetUnFloatedPickupGridControl_Sets_FloatedPickupGrid_Member_To_FloatedPickupGrid_Parameter()
        Dim UFPVM As New UnFloatedPickupVMWithComments
        Dim expected As New FpSpread

        With UFPVM
            .SetUnFloatedPickupGridControl(expected)
            Assert.AreSame(expected, ._UnFloatedPickupGrid)
        End With
    End Sub
#End Region

#Region "UnFloatedPickupGridFormat Tests"

    <TestMethod()> _
    Public Sub UnFloatedPickupGridFormat_SetsCommentsColumnToBeResizable()
        Dim UFPVM As New UnFloatedPickupVMWithComments

        With UFPVM
            .SetUnFloatedPickupGridControl(New FpSpread)
            .UnFloatedPickupGridFormat()
            Assert.IsTrue(._UnFloatedPickupGrid.Sheets(0).Columns(2).Resizable, "Expected column 3 to be resizable, actually it is not.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub UnFloatedPickupGridFormat_SetsHorizontalScrollBarPolicyToBeAsNeeded()
        Dim UFPVM As New UnFloatedPickupVMWithComments

        With UFPVM
            .SetUnFloatedPickupGridControl(New FpSpread)
            .UnFloatedPickupGridFormat()
            Assert.IsTrue(._UnFloatedPickupGrid.HorizontalScrollBarPolicy = ScrollBarPolicy.AsNeeded, "Expected UnFloatedPickup Grid to have 'AsNeeded' horizontal scrollbar policy, actually it is '" & ._UnFloatedPickupGrid.HorizontalScrollBarPolicy.ToString & "'.")
        End With
    End Sub
#End Region
End Class
