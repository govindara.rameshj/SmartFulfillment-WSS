﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class FloatAssignSaveUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "FloatAssignSaveFactory Tests"

    <TestMethod()> _
    Public Sub FloatAssignSaveFactory_ReturnsFloatAssignSaveImplementation()
        Dim TestImplementation As IFloatAssignSave = (New FloatAssignSaveFactory).Implementation

        Assert.AreEqual("NewBanking.Form.FloatAssignSave", TestImplementation.GetType.FullName)
    End Sub
#End Region

#Region "SaveFloatAssign Tests"

    <TestMethod()> _
    Public Sub SaveFloatAssign_BankingPeriodAndTodaysPeriodNotMatch_CallsSaveWithBankingPeriodParameter()
        Dim TestImplementation As ITestFloatAssignSave = New TestFloatAssignSave

        TestImplementation.SaveFloatAssign(Nothing, 1, 2)
        Assert.IsTrue(TestImplementation.SaveWithBankingPeriodCalled)
    End Sub

    <TestMethod()> _
    Public Sub SaveFloatAssign_BankingPeriodAndTodaysPeriodNotMatch_DoesNotCallSaveWithoutBankingPeriodParameter()
        Dim TestImplementation As ITestFloatAssignSave = New TestFloatAssignSave

        TestImplementation.SaveFloatAssign(Nothing, 1, 2)
        Assert.IsFalse(TestImplementation.SaveWithoutBankingPeriodCalled)
    End Sub

    <TestMethod()> _
    Public Sub SaveFloatAssign_BankingPeriodAndTodaysPeriodMatch_CallsSaveWithoutBankingPeriodParameter()
        Dim TestImplementation As ITestFloatAssignSave = New TestFloatAssignSave

        TestImplementation.SaveFloatAssign(Nothing, 1, 1)
        Assert.IsTrue(TestImplementation.SaveWithoutBankingPeriodCalled)
    End Sub

    <TestMethod()> _
    Public Sub SaveFloatAssign_BankingPeriodAndTodaysPeriodMatch_DoesNotCallSaveWithBankingPeriodParameter()
        Dim TestImplementation As ITestFloatAssignSave = New TestFloatAssignSave

        TestImplementation.SaveFloatAssign(Nothing, 1, 1)
        Assert.IsFalse(TestImplementation.SaveWithBankingPeriodCalled)
    End Sub
#End Region

    Private Class TestFloatAssignSave
        Inherits FloatAssignSave
        Implements ITestFloatAssignSave

        Private _saveWithBankingPeriodCalled As Boolean
        Private _saveWithoutBankingPeriodCalled As Boolean

        Protected Overrides Sub SaveWithBankingPeriod(ByRef AssigningFloat As Banking.Core.FloatBanking, ByVal BankingPeriodId As Integer)

            _saveWithBankingPeriodCalled = True
        End Sub

        Protected Overrides Sub SaveWithoutBankingPeriod(ByRef AssigningFloat As Banking.Core.FloatBanking)

            _saveWithoutBankingPeriodCalled = True
        End Sub

        ReadOnly Property SaveWithBankingPeriodCalled() As Boolean Implements ITestFloatAssignSave.SaveWithBankingPeriodCalled
            Get
                Return _saveWithBankingPeriodCalled
            End Get
        End Property

        ReadOnly Property SaveWithoutBankingPeriodCalled() As Boolean Implements ITestFloatAssignSave.SaveWithoutBankingPeriodCalled
            Get
                Return _saveWithoutBankingPeriodCalled
            End Get
        End Property
    End Class

    Private Interface ITestFloatAssignSave
        Inherits IFloatAssignSave

        ReadOnly Property SaveWithBankingPeriodCalled() As Boolean
        ReadOnly Property SaveWithoutBankingPeriodCalled() As Boolean
    End Interface
End Class
