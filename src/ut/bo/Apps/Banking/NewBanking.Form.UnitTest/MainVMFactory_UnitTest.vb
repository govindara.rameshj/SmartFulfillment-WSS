﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class MainVMFactory_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "MainVMFactory Factory"

    <TestMethod()> _
    Public Sub GetImplementation_RequirementSwitch_IsNotPresentOrEnabled_Returns_MainVMWithoutComments()
        Dim MVM As IMainVM

        ArrangeRequirementSwitchDependency(False)
        MVM = (New MainVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.MainVMWithoutComments", MVM.GetType.FullName)
    End Sub

    <TestMethod()> _
    Public Sub GetImplementation_RequirementSwitch_IsPresentAndEnabled_Returns_MainVMWithComments()
        Dim MVM As IMainVM

        ArrangeRequirementSwitchDependency(True)
        MVM = (New MainVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.MainVMWithComments", MVM.GetType.FullName)
    End Sub
#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)
        Dim MockRep As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = MockRep.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        MockRep.ReplayAll()
    End Sub
#End Region
End Class
