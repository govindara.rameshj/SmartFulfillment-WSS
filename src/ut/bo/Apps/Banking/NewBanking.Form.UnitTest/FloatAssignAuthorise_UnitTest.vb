﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class FloatAssignAuthorise_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region
#Region "Tests Supporting Code"
    Friend Class UnitTestRequirementCheck
        Inherits FloatAssignAuthoriseFactory
        Friend _IsEnabled As Boolean = False
        Public Overrides Function IsEnabled() As Boolean
            Return _IsEnabled
        End Function
    End Class

    Friend Class UnitTestManagerCheck
        Inherits FloatAssignAuthorise
        Friend _IsAManager As Boolean = False
        Friend Overrides Function IsUserAManager(ByVal UserId As Integer) As Boolean
            Return _IsAManager
        End Function
    End Class
#End Region

#Region "Requirement Check"

    <TestMethod()> Public Sub FloatAssignAutorise_RequirementCheck_ReturnsFloatAssignAuthoriseNotImplemented()
        Dim Repository As IFloatAssignAuthorise
        Dim Factory As New UnitTestRequirementCheck

        Factory._IsEnabled = False
        Repository = Factory.FactoryGet()
        Assert.AreEqual("NewBanking.Form.FloatAssignAuthoriseNotImplemented", Repository.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub FloatAssignAutorise_RequirementCheck_ReturnsFloatAssignAuthorise()
        Dim Repository As IFloatAssignAuthorise
        Dim Factory As New UnitTestRequirementCheck

        Factory._IsEnabled = True
        Repository = Factory.FactoryGet()
        Assert.AreEqual("NewBanking.Form.FloatAssignAuthorise", Repository.GetType.FullName)
    End Sub

#End Region
#Region "Test Functionality"

    <TestMethod()> Public Sub FloatAssignAutorise_IsUserAManager_ReturnsTrue()
        Dim Factory As New UnitTestManagerCheck

        Factory._IsAManager = True
        Assert.IsTrue(Factory.FloatAssignAuthorise(4, 0))
    End Sub

    <TestMethod()> Public Sub FloatAssignAutorise_IsUserAManager_ReturnsFalse()
        Dim Factory As New UnitTestManagerCheck

        Factory._IsAManager = False
        Assert.IsFalse(Factory.FloatAssignAuthorise(44, 0))
    End Sub

#End Region
End Class
