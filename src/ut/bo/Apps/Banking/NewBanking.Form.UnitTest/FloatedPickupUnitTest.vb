﻿<TestClass()> Public Class FloatedPickupUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - ButtonActive"

    <TestMethod()> Public Sub btnReAssignFloat_RequirementCheckFalse_ButtonActive()

        Dim Form As New FloatedPickup(String.Empty, Nothing, Nothing, Nothing, Nothing, String.Empty, String.Empty, String.Empty, String.Empty)

        ArrangeRequirementSwitchDependency(False)

        Form.ReAssignButtonAvailability()

        Assert.IsTrue(Form.btnReAssignFloat.Enabled)

    End Sub

    <TestMethod()> Public Sub btnReAssignFloat_RequirementCheckTrue_ButtonInactive()

        Dim Form As New FloatedPickup(String.Empty, Nothing, Nothing, Nothing, Nothing, String.Empty, String.Empty, String.Empty, String.Empty)

        ArrangeRequirementSwitchDependency(True)

        Form.ReAssignButtonAvailability()

        Assert.IsFalse(Form.btnReAssignFloat.Enabled)

    End Sub

#End Region

#Region "Complete Pickup Button Tests"

    <TestMethod()> _
    Public Sub btnCompletePickup_GetCompletePickupAvailabilityReturnsFalse_ButtonIsDisabled()
        Dim Form As New TestFloatedPickup_OverrideGetCompletePickupAvailabilityToReturnFalse
        Dim Selected As New FloatedPickupList

        Form.SetCompletePickupAvailability(Selected)
        Assert.IsFalse(Form.btnCompletePickup.Enabled)
    End Sub

    <TestMethod()> _
    Public Sub btnCompletePickup_GetCompletePickupAvailabilityReturnsTrue_ButtonIsEnabled()
        Dim Form As New TestFloatedPickup_OverrideGetCompletePickupAvailabilityToReturnTrue
        Dim Selected As New FloatedPickupList

        Form.SetCompletePickupAvailability(Selected)
        Assert.IsTrue(Form.btnCompletePickup.Enabled)
    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

#Region "Test Scenario Classes"

    Private Class TestFloatedPickup_OverrideGetCompletePickupAvailabilityToReturnTrue
        Inherits FloatedPickup

        Public Sub New()

            MyBase.new(String.Empty, Nothing, Nothing, Nothing, Nothing, String.Empty, String.Empty, String.Empty, String.Empty)
        End Sub

        Friend Overrides Function GetCompletePickupAvailability(ByRef Selected As FloatedPickupList, ByVal CurrentAvailability As Boolean) As Boolean

            Return True
        End Function
    End Class

    Private Class TestFloatedPickup_OverrideGetCompletePickupAvailabilityToReturnFalse
        Inherits FloatedPickup

        Public Sub New()

            MyBase.new(String.Empty, Nothing, Nothing, Nothing, Nothing, String.Empty, String.Empty, String.Empty, String.Empty)
        End Sub

        Friend Overrides Function GetCompletePickupAvailability(ByRef Selected As FloatedPickupList, ByVal CurrentAvailability As Boolean) As Boolean

            Return False
        End Function
    End Class
#End Region
End Class