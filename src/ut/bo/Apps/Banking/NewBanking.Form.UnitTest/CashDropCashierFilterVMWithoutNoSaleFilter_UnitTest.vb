﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class CashDropCashierFilterVMWithoutNoSaleFilter_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region


#Region "LoadCashierDropDown Tests"

    <TestMethod()> _
    Public Sub LoadCashierDropDown_Calls_SetCashiersToLoad()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithoutNoSaleFilter_OverridingSetCashiersToLoadToFlagWhetherItIsCalledAndExposePassedInParameter

        CDCFVM.LoadCashierDropDown(New CashierCollection, 1)
        Assert.AreEqual(True, CDCFVM._SetCashiersToLoadWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_PassesCashiersToLoadParameter_To_SetCashiersToLoad()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithoutNoSaleFilter_OverridingSetCashiersToLoadToFlagWhetherItIsCalledAndExposePassedInParameter
        Dim Parameter As New CashierCollection

        CDCFVM.LoadCashierDropDown(Parameter, 1)
        Assert.AreSame(Parameter, CDCFVM._SetCashiersToLoad_CashiersToLoad_Parameter)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_CallsHaveInitialised()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithoutNoSaleFilter_OverridingCashiersToLoadIsSetAndLoadCashiersMethodsToFlagWhetherTheyAreCalled(False)

        CDCFVM.LoadCashierDropDown(New CashierCollection, 1)
        Assert.AreEqual(True, CDCFVM._CashiersToLoadIsSetWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_HaveInitialisedIsFalse_DoesNotCall_LoadCashiers()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithoutNoSaleFilter_OverridingCashiersToLoadIsSetAndLoadCashiersMethodsToFlagWhetherTheyAreCalled(False)

        CDCFVM.LoadCashierDropDown(New CashierCollection, 1)
        Assert.AreEqual(False, CDCFVM._LoadCashiersWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub LoadCashierDropDown_HaveInitialisedIsTrue_DoesCall_LoadCashiers()
        Dim CDCFVM As New TestCashDropCashierFilterVMWithoutNoSaleFilter_OverridingCashiersToLoadIsSetAndLoadCashiersMethodsToFlagWhetherTheyAreCalled(True)

        CDCFVM.LoadCashierDropDown(New CashierCollection, 1)
        Assert.AreEqual(True, CDCFVM._LoadCashiersWasCalled)
    End Sub
#End Region

#Region "SetCashiersToLoad Tests"

    <TestMethod()> _
    Public Sub SetCashiersToLoad_AssignsParameterToModularVariable()
        Dim CDCFVM As New CashDropCashierFilterVMWithoutNoSaleFilter
        Dim Parameter As New CashierCollection

        CDCFVM.SetCashiersToLoad(Parameter)
        Assert.AreSame(Parameter, CDCFVM._CashiersToLoad)
    End Sub
#End Region

#Region "CashiersToLoadIsSet Tests"

    <TestMethod()> _
    Public Sub CashiersToLoadIsSet_ModularCashiersToLoadIsNothing_ReturnsFalse()
        Dim CDCFVM As New CashDropCashierFilterVMWithoutNoSaleFilter

        CDCFVM._CashiersToLoad = Nothing
        Assert.AreEqual(False, CDCFVM.CashiersToLoadIsSet)
    End Sub

    <TestMethod()> _
    Public Sub CashiersToLoadIsSet_ModularCashiersToLoadIsSomething_ReturnsTrue()
        Dim CDCFVM As New CashDropCashierFilterVMWithoutNoSaleFilter

        CDCFVM._CashiersToLoad = New CashierCollection
        Assert.AreEqual(True, CDCFVM.CashiersToLoadIsSet)
    End Sub
#End Region

End Class

Friend Class TestCashDropCashierFilterVMWithoutNoSaleFilter_OverridingSetCashiersToLoadToFlagWhetherItIsCalledAndExposePassedInParameter
    Inherits CashDropCashierFilterVMWithoutNoSaleFilter

    Friend _SetCashiersToLoad_CashiersToLoad_Parameter As CashierCollection
    Friend _SetCashiersToLoadWasCalled As Boolean

    Friend Overrides Sub SetCashiersToLoad(ByVal CashiersToLoad As Core.CashierCollection)

        _SetCashiersToLoadWasCalled = True
        _SetCashiersToLoad_CashiersToLoad_Parameter = CashiersToLoad
    End Sub
End Class

Friend Class TestCashDropCashierFilterVMWithoutNoSaleFilter_OverridingCashiersToLoadIsSetAndLoadCashiersMethodsToFlagWhetherTheyAreCalled
    Inherits CashDropCashierFilterVMWithoutNoSaleFilter

    Friend _CashiersToLoadIsSetWasCalled As Boolean
    Friend _LoadCashiersWasCalled As Boolean
    Friend _Overriden_CashiersToLoadIsSet_ReturnValue As Boolean

    Public Sub New(ByVal Overriden_CashiersToLoadIsSet_ReturnValue As Boolean)

        _Overriden_CashiersToLoadIsSet_ReturnValue = Overriden_CashiersToLoadIsSet_ReturnValue
    End Sub

    Friend Overrides Function CashiersToLoadIsSet() As Boolean

        _CashiersToLoadIsSetWasCalled = True
        Return _Overriden_CashiersToLoadIsSet_ReturnValue
    End Function

    Friend Overrides Sub LoadCashiers()

        _LoadCashiersWasCalled = True
    End Sub
End Class