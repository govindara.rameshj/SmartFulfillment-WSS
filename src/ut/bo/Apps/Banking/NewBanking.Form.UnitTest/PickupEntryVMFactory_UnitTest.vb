﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class PickupEntryVMFactory_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "PickupEntryVMFactory Factory"

    <TestMethod()> _
    Public Sub GetImplementation_RequirementSwitch_IsNotPresentOrEnabled_Returns_PickupEntryVM()
        Dim PEVM As IPickupEntryVM

        ArrangeRequirementSwitchDependency(False)
        PEVM = (New PickupEntryVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.PickupEntryVMWithoutComments", PEVM.GetType.FullName)
    End Sub

    <TestMethod()> _
    Public Sub GetImplementation_RequirementSwitch_IsPresentAndEnabled_Returns_PickupEntryVM()
        Dim PEVM As IPickupEntryVM

        ArrangeRequirementSwitchDependency(True)
        PEVM = (New PickupEntryVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.PickupEntryVMWithComments", PEVM.GetType.FullName)
    End Sub
#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)
        Dim MockRep As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = MockRep.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        MockRep.ReplayAll()
    End Sub
#End Region

End Class
