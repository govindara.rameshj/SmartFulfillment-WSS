﻿Imports System
<TestClass()> Public Class UserUnitTest

    Private testContextInstance As TestContext
    Private UserAuthority As New UserAuthority
    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region


    <TestMethod()> Public Sub UsersAreDifferent_ManagerAndWitnessAreDifferentUsers_ReturnTrue()

        UserAuthority._ManagerUserID = 10
        UserAuthority._PrimaryWitnessUserID = 20
        Assert.IsTrue(UserAuthority.UsersAreDifferent())

    End Sub

    <TestMethod()> Public Sub UsersAreDifferent_ManagerAndWitnessAreSameUsers_ReturnFalse()

        UserAuthority._ManagerUserID = 10
        UserAuthority._PrimaryWitnessUserID = 10

        Assert.IsFalse(UserAuthority.UsersAreDifferent())

    End Sub

End Class
