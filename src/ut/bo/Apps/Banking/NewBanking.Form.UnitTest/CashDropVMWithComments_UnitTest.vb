﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class CashDropVMWithComments_UnitTest
    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "SetCashDropGridControl Tests"

    <TestMethod()> _
    Public Sub SetCashDropGridControl_Sets_CashDropGrid_Member_To_CashDropGrid_Parameter()
        Dim CDVM As New CashDropVMWithComments
        Dim expected As New FpSpread

        CDVM.SetCashDropGridControl(expected)
        Assert.AreSame(expected, CDVM._CashDropGrid)
    End Sub
#End Region

#Region "CashDropGridPopulate Tests"

    <TestMethod()> _
    Public Sub CashDropGridPopulate_AddsRowForComments()
        Dim CDVM As New CashDropVMWithComments
        Dim actual As String = String.Empty

        With CDVM
            .SetCashDropGridControl(New FpSpread)
            .CashDropGridFormat()
            .CashDropGridPopulate()
            With ._CashDropGrid.Sheets(0)
                If .Cells(CDVM._CommentsRowIndex - 1, 0).Value IsNot Nothing Then
                    actual = .Cells(CDVM._CommentsRowIndex - 1, 0).Value.ToString
                End If
            End With
        End With
        Assert.IsTrue(actual IsNot Nothing AndAlso String.Compare(actual, "Comments") = 0)
    End Sub
#End Region

#Region "CashDropGridActiveCellIsCommentsCell Tests"

    <TestMethod()> _
    Public Sub CashDropGridActiveCellIsCommentsCell_CommentsCellIsNotActiveCell_ReturnsFalse()
        Dim CDVM As New CashDropVMWithComments

        With CDVM
            .SetCashDropGridControl(New FpSpread)
            .CashDropGridFormat()
            .CashDropGridPopulate()
            SpreadCellMakeActive(._CashDropGrid.ActiveSheet, ._CommentsRowIndex - 2, ._CommentsColumnIndex)
            Assert.IsFalse(.CashDropGridActiveCellIsCommentsCell)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CashDropGridActiveCellIsCommentsCell_CommentsCellIsActiveCell_ReturnsTrue()
        Dim CDVM As New CashDropVMWithComments

        With CDVM
            .SetCashDropGridControl(New FpSpread)
            .CashDropGridFormat()
            .CashDropGridPopulate()
            SpreadCellMakeActive(._CashDropGrid.ActiveSheet, ._CommentsRowIndex, ._CommentsColumnIndex)
            Assert.IsTrue(.CashDropGridActiveCellIsCommentsCell)
        End With
    End Sub

    <TestMethod()> _
    Public Sub CashDropGridActiveCellIsCommentsCell_CommentsHeaderCellIsActiveCell_ReturnsTrue()
        Dim CDVM As New CashDropVMWithComments

        With CDVM
            .SetCashDropGridControl(New FpSpread)
            .CashDropGridFormat()
            .CashDropGridPopulate()
            SpreadCellMakeActive(._CashDropGrid.ActiveSheet, ._CommentsRowIndex - 1, ._CommentsColumnIndex)
            Assert.IsTrue(.CashDropGridActiveCellIsCommentsCell)
        End With
    End Sub
#End Region

#Region "SetCashDropForm Tests"

    <TestMethod()> _
    Public Sub SetCashDropFormControl_Sets_CashDropForm_Member_To_CashDropForm_Parameter()
        Dim CDVM As New CashDropVMWithComments
        Dim expected As New System.Windows.Forms.Form

        CDVM.SetCashDropForm(expected)
        Assert.AreSame(expected, CDVM._CashDropForm)
    End Sub
#End Region

#Region "CashDropFormFormat Tests"

    <TestMethod()> _
    Public Sub CashDropFormFormat_Sets_CashDropForm_Member_ToBe120Higher()
        Dim CDVM As New CashDropVMWithComments
        Dim NewForm As New System.Windows.Forms.Form
        Dim expected As Integer = NewForm.Height + 120

        With CDVM
            .SetCashDropForm(NewForm)
            .CashDropFormFormat()
            Assert.AreEqual(expected, ._CashDropForm.Height)
        End With
    End Sub
#End Region

#Region "GetCashDropGridTotalsRowIndex Tests"

    <TestMethod()> _
    Public Sub GetCashDropGridTotalsRowIndex_Returns_CommentsRowIndex_Minus2()
        Dim CDVM As New CashDropVMWithComments
        Dim expected As Integer = -1

        With CDVM
            .SetCashDropGridControl(New FpSpread)
            .CashDropGridFormat()
            expected = ._CommentsRowIndex - 2
            Assert.AreEqual(expected, .GetCashDropGridTotalsRowIndex)
        End With
    End Sub
#End Region
End Class
