﻿<TestClass()> Public Class EndOfDayCheckSummaryVM_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestSummaryVMUnPopulated
        Inherits EndOfDayCheckSummaryVMUnPopulated

        Friend _SummaryLoadDataCalled As Boolean
        Friend _SummaryPrintLoadDataCalled As Boolean
        Friend _FormatGridCalled As Boolean
        Friend _RowZeroFormattingCalled As Boolean
        Friend _PopulateGridCalled As Boolean

        Friend _MethodCallOrder As New List(Of String)

        Friend Overrides Sub SummaryLoadData()

            _SummaryLoadDataCalled = True
            _MethodCallOrder.Add("SummaryLoadData")

        End Sub

        Friend Overrides Sub SummaryPrintLoadData()

            _SummaryPrintLoadDataCalled = True
            _MethodCallOrder.Add("SummaryPrintLoadData")

        End Sub

        Friend Overrides Sub FormatGrid(ByRef Grid As FpSpread)

            _FormatGridCalled = True
            _MethodCallOrder.Add("FormatGrid")

        End Sub

        Friend Overrides Sub PopulateGrid()

            _PopulateGridCalled = True
            _MethodCallOrder.Add("PopulateGrid")

        End Sub

        Friend Overrides Sub RowZeroFormatting()

            _RowZeroFormattingCalled = True
            _MethodCallOrder.Add("RowZeroFormatting")

        End Sub

    End Class

    Private Class TestSummaryVM
        Inherits EndOfDayCheckSummaryVM

        Friend _SummaryLoadDataCalled As Boolean
        Friend _FormatGridCalled As Boolean
        Friend _PopulateGridCalled As Boolean

        Friend _MethodCallOrder As New List(Of String)

        Friend Overrides Sub SummaryLoadData(ByVal PeriodID As Integer)

            _SummaryLoadDataCalled = True
            _MethodCallOrder.Add("SummaryLoadData")

            MyBase.SummaryLoadData(PeriodID)

        End Sub

        Friend Overrides Sub FormatGrid(ByRef Grid As FpSpread)

            _FormatGridCalled = True
            _MethodCallOrder.Add("FormatGrid")

        End Sub

        Friend Overrides Sub PopulateGrid()

            _PopulateGridCalled = True
            _MethodCallOrder.Add("PopulateGrid")

        End Sub

        Friend Overrides Sub GetData()

            'do nothing

        End Sub

    End Class

#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub RequirementCheck_ParameterFalse_ReturnEndOfDayCheckSummaryVMUnPopulated()

        Dim V As IEndOfDayCheckSummaryVM

        ArrangeRequirementSwitchDependency(False)

        V = (New EndOfDayCheckSummaryVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.EndOfDayCheckSummaryVMUnPopulated", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementCheck_ParameterTrue_ReturnEndOfDayCheckSummaryVM()

        Dim V As IEndOfDayCheckSummaryVM

        ArrangeRequirementSwitchDependency(True)

        V = (New EndOfDayCheckSummaryVMFactory).GetImplementation

        Assert.AreEqual("NewBanking.Form.EndOfDayCheckSummaryVM", V.GetType.FullName)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVMUnPopulated | Interface | Property | Sheet View"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SheetView_ReturnSheetView()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(9, VM.SheetView.ColumnCount)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVMUnPopulated | Interface | Procedure | Summary"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_Summary_LoadDataCalled()

        Dim VM As New TestSummaryVMUnPopulated

        VM.Summary(New FpSpread, Nothing)

        Assert.IsTrue(VM._SummaryLoadDataCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_Summary_FormatGridCalled()

        Dim VM As New TestSummaryVMUnPopulated

        VM.Summary(New FpSpread, Nothing)

        Assert.IsTrue(VM._FormatGridCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_Summary_PopulateGridCalled()

        Dim VM As New TestSummaryVMUnPopulated

        VM.Summary(New FpSpread, Nothing)

        Assert.IsTrue(VM._PopulateGridCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_Summary_LoadDataCalledFirst()

        Dim VM As New TestSummaryVMUnPopulated

        VM.Summary(New FpSpread, Nothing)

        Assert.AreEqual("SummaryLoadData", VM._MethodCallOrder.Item(0))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_Summary_FormatGridCalledSecond()

        Dim VM As New TestSummaryVMUnPopulated

        VM.Summary(New FpSpread, Nothing)

        Assert.AreEqual("FormatGrid", VM._MethodCallOrder.Item(1))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_Summary_PopulateGridCalledThird()

        Dim VM As New TestSummaryVMUnPopulated

        VM.Summary(New FpSpread, Nothing)

        Assert.AreEqual("PopulateGrid", VM._MethodCallOrder.Item(2))

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVMUnPopulated | Interface | Procedure | Summary Print"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrint_LoadDataCalled()

        Dim VM As New TestSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.IsTrue(VM._SummaryPrintLoadDataCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrint_FormatGridCalled()

        Dim VM As New TestSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.IsTrue(VM._FormatGridCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrint_PopulateGridCalled()

        Dim VM As New TestSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.IsTrue(VM._PopulateGridCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrint_RowZeroFormattingCalled()

        Dim VM As New TestSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.IsTrue(VM._RowZeroFormattingCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrint_LoadDataCalledFirst()

        Dim VM As New TestSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual("SummaryPrintLoadData", VM._MethodCallOrder.Item(0))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrint_FormatGridCalledSecond()

        Dim VM As New TestSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual("FormatGrid", VM._MethodCallOrder.Item(1))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrint_PopulateGridCalledThird()

        Dim VM As New TestSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual("PopulateGrid", VM._MethodCallOrder.Item(2))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrint_RowZeroFormattingCalledFourth()

        Dim VM As New TestSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual("RowZeroFormatting", VM._MethodCallOrder.Item(3))

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVMUnPopulated | Internal | Procedure | SummaryLoadData"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryLoadData_UseCorrectClass()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryLoadData()

        Assert.AreEqual("NewBanking.Core.EndOfDayCheckSummaryModelUnPopulated", VM._Data.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryLoadData_CreateInternalVariable()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryLoadData()

        Assert.IsNotNull(VM._Data)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVMUnPopulated | Internal | Procedure | SummaryPrintLoadData"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_UseCorrectClass()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("NewBanking.Core.EndOfDayCheckSummaryModelUnPopulated", VM._Data.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_CreateInternalVariable()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.IsNotNull(VM._Data)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnOneReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<Name + Signature>", VM._Data.ManagerID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnTwoReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<Name + Signature>", VM._Data.WitnessID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnThreeReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<Time>", VM._Data.TimelockSet)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnForReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<Hours>", VM._Data.Duration)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnFiveReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<Manual entry for now>", VM._Data.BagCount)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnSixReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<System Count of Floats unassigned + Pickups not Banked>", VM._Data.SystemCount)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnSevenReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<Manual entry for now>", VM._Data.ChequeBagNumber)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnEightReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<Manual entry for now>", VM._Data.CashBagNumber)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_SummaryPrintLoadData_ColumnNineReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrintLoadData()

        Assert.AreEqual("<Comments>", VM._Data.Comments)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVMUnPopulated | Internal | Procedure | Format Grid"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ReturnNineColumns()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(9, VM._View.ColumnCount)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnOneName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Manager ID", VM._View.Columns(0).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnTwoName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Witness ID", VM._View.Columns(1).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnThreeName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Timelock Set", VM._View.Columns(2).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnFourName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Duration", VM._View.Columns(3).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnFiveName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Bag Count", VM._View.Columns(4).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnSixName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("System Count", VM._View.Columns(5).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnSevenName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Cheque Bag Numbers", VM._View.Columns(6).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnEightName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Cash Bag Number", VM._View.Columns(7).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_ColumnNineName()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Comments", VM._View.Columns(8).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_AddViewToGrid()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(1, VM._Grid.Sheets.Count)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_HorizontalScrollNever()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(ScrollBarPolicy.Never, VM._Grid.HorizontalScrollBarPolicy)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_FormatGrid_VerticallScrollAsNeeded()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(ScrollBarPolicy.AsNeeded, VM._Grid.VerticalScrollBarPolicy)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVMUnPopulated | Internal | Procedure | Populate Grid"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ClearDownExistingAndAddOneNewRow()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryLoadData()
        VM.FormatGrid(New FpSpread)
        VM._View.Rows.Add(0, 2)
        VM.PopulateGrid()

        Assert.AreEqual(1, VM._View.Rows.Count)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnOneReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Manager ID", VM._View.Cells(0, 0).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnTwoReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Witness ID", VM._View.Cells(0, 1).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnThreeReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("TimeLockSet", VM._View.Cells(0, 2).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnFourReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Duration", VM._View.Cells(0, 3).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnFiveReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Bag Count", VM._View.Cells(0, 4).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnSixReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("System Count", VM._View.Cells(0, 5).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnSevenReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Cheque Bag Number", VM._View.Cells(0, 6).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnEightReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Cash Bag Number", VM._View.Cells(0, 7).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_PopulateGrid_ColumnNineReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM._Data = LoadEndOfDayCheckNotPopulated("Manager ID", "Witness ID", "TimeLockSet", "Duration", "Bag Count", "System Count", "Cheque Bag Number", "Cash Bag Number", "Comments")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Comments", VM._View.Cells(0, 8).Value.ToString)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVMUnPopulated | Internal | Procedure | RowZeroFormatting"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_RowZeroFormatting_HeightEighty()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual(CType(80, Single), VM._View.Rows.Item(0).Height)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_RowZeroFormatting_VerticalCellAlignmentCentre()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual(CellVerticalAlignment.Center, VM._View.Rows.Item(0).VerticalAlignment)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVMUnPopulated_RowZeroFormatting_ForeColourGrey()

        Dim VM As New EndOfDayCheckSummaryVMUnPopulated

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual(Color.LightGray, VM._View.Rows.Item(0).ForeColor)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVM | Interface | Property | Sheet View"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SheetView_ReturnSheetView()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(8, VM.SheetView.ColumnCount)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVM | Interface | Procedure | Summary"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_Summary_LoadDataCalled()

        Dim VM As New TestSummaryVM

        VM.Summary(New FpSpread, Nothing)

        Assert.IsTrue(VM._SummaryLoadDataCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_Summary_FormatGridCalled()

        Dim VM As New TestSummaryVM

        VM.Summary(New FpSpread, Nothing)

        Assert.IsTrue(VM._FormatGridCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_Summary_PopulateGridCalled()

        Dim VM As New TestSummaryVM

        VM.Summary(New FpSpread, Nothing)

        Assert.IsTrue(VM._PopulateGridCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_Summary_LoadDataCalledFirst()

        Dim VM As New TestSummaryVM

        VM.Summary(New FpSpread, Nothing)

        Assert.AreEqual("SummaryLoadData", VM._MethodCallOrder.Item(0))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_Summary_FormatGridCalledSecond()

        Dim VM As New TestSummaryVM

        VM.Summary(New FpSpread, Nothing)

        Assert.AreEqual("FormatGrid", VM._MethodCallOrder.Item(1))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_Summary_PopulateGridCalledThird()

        Dim VM As New TestSummaryVM

        VM.Summary(New FpSpread, Nothing)

        Assert.AreEqual("PopulateGrid", VM._MethodCallOrder.Item(2))

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVM | Interface | Procedure | Summary Print"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryPrint_LoadDataCalled()

        Dim VM As New TestSummaryVM

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.IsTrue(VM._SummaryLoadDataCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryPrint_FormatGridCalled()

        Dim VM As New TestSummaryVM

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.IsTrue(VM._FormatGridCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryPrint_PopulateGridCalled()

        Dim VM As New TestSummaryVM

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.IsTrue(VM._PopulateGridCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryPrint_LoadDataCalledFirst()

        Dim VM As New TestSummaryVM

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual("SummaryLoadData", VM._MethodCallOrder.Item(0))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryPrint_FormatGridCalledSecond()

        Dim VM As New TestSummaryVM

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual("FormatGrid", VM._MethodCallOrder.Item(1))

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryPrint_PopulateGridCalledThird()

        Dim VM As New TestSummaryVM

        VM.SummaryPrint(New FpSpread, Nothing)

        Assert.AreEqual("PopulateGrid", VM._MethodCallOrder.Item(2))

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVM | Internal | Procedure | Summary Load Data"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryLoadData_SetPeriodID()

        Dim VM As New TestSummaryVM

        VM.SummaryLoadData(999)

        Assert.AreEqual(999, VM._PeriodID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryLoadData_UseCorrectClass()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.SummaryLoadData(Nothing)

        Assert.AreEqual("NewBanking.Core.EndOfDayCheckSummaryModel", VM._Data.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_SummaryLoadData_GetData_Called()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.SummaryLoadData(Nothing)

        Assert.IsNotNull(VM._Data)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVM | Internal | Procedure | Format Grid"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ReturnEightColumns()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(8, VM._View.ColumnCount)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ColumnOneName()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Pickup Bag Count", VM._View.Columns(0).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ColumnTwoName()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Pickup Bag System Count", VM._View.Columns(1).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ColumnThreeName()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Banking Cash Count", VM._View.Columns(2).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ColumnFourName()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Banking Cash System Count", VM._View.Columns(3).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ColumnFiveName()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Banking Cheque Count", VM._View.Columns(4).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ColumnSixName()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Banking Cheque System Count", VM._View.Columns(5).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ColumnSevenName()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Manager", VM._View.Columns(6).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_ColumnEightName()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual("Witness", VM._View.Columns(7).Label)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_AddViewToGrid()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(1, VM._Grid.Sheets.Count)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_HorizontalScrollNever()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(ScrollBarPolicy.Never, VM._Grid.HorizontalScrollBarPolicy)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_FormatGrid_VerticallScrollAsNeeded()

        Dim VM As New EndOfDayCheckSummaryVM

        VM.FormatGrid(New FpSpread)

        Assert.AreEqual(ScrollBarPolicy.AsNeeded, VM._Grid.VerticalScrollBarPolicy)

    End Sub

#End Region

#Region "Class: EndOfDayCheckSummaryVM | Internal | Procedure | Populate Grid"

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ClearDownExistingAndAddOneNewRow()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = New EndOfDayCheckSummaryModel
        VM.FormatGrid(New FpSpread)
        VM._View.Rows.Add(0, 2)
        VM.PopulateGrid()

        Assert.AreEqual(1, VM._View.Rows.Count)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ColumnOneReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = LoadEndOfDayCheck(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual(6, VM._View.Cells(0, 0).Value)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ColumnTwoReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = LoadEndOfDayCheck(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual(8, VM._View.Cells(0, 1).Value)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ColumnThreeReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = LoadEndOfDayCheck(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual(10, VM._View.Cells(0, 2).Value)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ColumnFourReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = LoadEndOfDayCheck(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual(12, VM._View.Cells(0, 3).Value)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ColumnFiveReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = LoadEndOfDayCheck(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual(14, VM._View.Cells(0, 4).Value)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ColumnSixReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = LoadEndOfDayCheck(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual(16, VM._View.Cells(0, 5).Value)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ColumnSevenReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = LoadEndOfDayCheck(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Manager ID", VM._View.Cells(0, 6).Value.ToString)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckSummaryVM_PopulateGrid_ColumnEightReturnsCorrectValue()

        Dim VM As New EndOfDayCheckSummaryVM

        VM._Data = LoadEndOfDayCheck(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID")
        VM.FormatGrid(New FpSpread)
        VM.PopulateGrid()

        Assert.AreEqual("Witness ID", VM._View.Cells(0, 7).Value.ToString)

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

    Private Function LoadEndOfDayCheckNotPopulated(ByVal ManagerID As String, _
                                                   ByVal WitnessID As String, _
                                                   ByVal TimelockSet As String, _
                                                   ByVal Duration As String, _
                                                   ByVal BagCount As String, _
                                                   ByVal SystemCount As String, _
                                                   ByVal ChequeBagNumber As String, _
                                                   ByVal CashBagNumber As String, _
                                                   ByVal Comments As String) As EndOfDayCheckSummaryModelUnPopulated

        Dim EOD As New EndOfDayCheckSummaryModelUnPopulated

        With EOD
            .ManagerID = ManagerID
            .WitnessID = WitnessID
            .TimelockSet = TimelockSet
            .Duration = Duration
            .BagCount = BagCount
            .SystemCount = SystemCount
            .ChequeBagNumber = ChequeBagNumber
            .CashBagNumber = CashBagNumber
            .Comments = Comments
        End With

        Return EOD

    End Function

    Private Function LoadEndOfDayCheck(ByVal PickupBagCount As Integer, _
                                       ByVal PickupBagSystemCount As Integer, _
                                       ByVal BankingCashCount As Integer, _
                                       ByVal BankingCashSystemCount As Integer, _
                                       ByVal BankingChequeCount As Integer, _
                                       ByVal BankingChequeSystemCount As Integer, _
                                       ByVal Manager As String, _
                                       ByVal Witness As String) As EndOfDayCheckSummaryModel

        Dim EOD As New EndOfDayCheckSummaryModel

        With EOD

            .PickupBagCount = PickupBagCount
            .PickupBagSystemCount = PickupBagSystemCount
            .BankingCashCount = BankingCashCount
            .BankingCashSystemCount = BankingCashSystemCount
            .BankingChequeCount = BankingChequeCount
            .BankingChequeSystemCount = BankingChequeSystemCount
            .Manager = Manager
            .Witness = Witness

        End With

        Return EOD

    End Function

#End Region

End Class