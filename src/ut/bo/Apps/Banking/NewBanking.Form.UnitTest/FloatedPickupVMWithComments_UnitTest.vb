﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class FloatedPickupVMWithComments_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "SetFloatedPickupGridControl Tests"

    <TestMethod()> _
    Public Sub SetFloatedPickupGridControl_Sets_FloatedPickupGrid_Member_To_FloatedPickupGrid_Parameter()
        Dim FPVM As New FloatedPickupVMWithComments
        Dim expected As New FpSpread

        With FPVM
            .SetFloatedPickupGridControl(expected)
            Assert.AreSame(expected, ._FloatedPickupGrid)
        End With
    End Sub
#End Region

#Region "FloatedPickupGridFormat Tests"

    <TestMethod()> _
    Public Sub FloatedPickupGridFormat_AddsAtLeastEnoughColumnsToIncludeCommentsColumn()
        Dim FPVM As New FloatedPickupVMWithComments

        With FPVM
            .SetFloatedPickupGridControl(New FpSpread)
            .FloatedPickupGridFormat()
            Assert.IsTrue(._FloatedPickupGrid.Sheets(0).ColumnCount >= 4, "Expected 4 or more columns, has " & ._FloatedPickupGrid.Sheets(0).ColumnCount.ToString & " columns.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatedPickupGridFormat_Sets4thColumnHeaderToComments()
        Dim FPVM As New FloatedPickupVMWithComments

        With FPVM
            .SetFloatedPickupGridControl(New FpSpread)
            .FloatedPickupGridFormat()
            Assert.IsTrue(._FloatedPickupGrid.Sheets(0).Columns(3).Label = "Comment", "Expected 'Comment' header for column 4, actually is '" & ._FloatedPickupGrid.Sheets(0).Columns(3).Label & "'.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatedPickupGridFormat_Sets4thColumnWidthTo240()
        Dim FPVM As New FloatedPickupVMWithComments

        With FPVM
            .SetFloatedPickupGridControl(New FpSpread)
            .FloatedPickupGridFormat()
            Assert.IsTrue(._FloatedPickupGrid.Sheets(0).Columns(3).Width = 240, "Expected column 4 width to be 240, actually it is " & ._FloatedPickupGrid.Sheets(0).Columns(3).Width.ToString & ".")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatedPickupGridFormat_SetsCommentsColumnToBeResizable()
        Dim FPVM As New FloatedPickupVMWithComments

        With FPVM
            .SetFloatedPickupGridControl(New FpSpread)
            .FloatedPickupGridFormat()
            Assert.IsTrue(._FloatedPickupGrid.Sheets(0).Columns(3).Resizable, "Expected column 4 to be resizable, actually it is not.")
        End With
    End Sub

    <TestMethod()> _
    Public Sub FloatedPickupGridFormat_SetsHorizontalScrollBarPolicyToBeAsNeeded()
        Dim FPVM As New FloatedPickupVMWithComments

        With FPVM
            .SetFloatedPickupGridControl(New FpSpread)
            .FloatedPickupGridFormat()
            Assert.IsTrue(._FloatedPickupGrid.HorizontalScrollBarPolicy = ScrollBarPolicy.AsNeeded, "Expected FloatedPickup Grid to have 'AsNeeded' horizontal scrollbar policy, actually it is '" & ._FloatedPickupGrid.HorizontalScrollBarPolicy.ToString & "'.")
        End With
    End Sub
#End Region

#Region "FloatedPickupGridPopulate Tests"

    <TestMethod()> _
    Public Sub FloatedPickupGridPopulate_PopulatesCommentsColumnForEachRowWithAComment()
        Dim FPVM As New TestFloatedPickupVMWithComments_WithTestDataExposed
        Dim actual As String = String.Empty
        Dim AllCommentsColumnsPopulatedWhereCommentIsInSourceData As Boolean
        Dim AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData As Boolean
        Dim TestData As FloatedPickupListCollection = Nothing

        With FPVM
            .SetFloatedPickupGridControl(New FpSpread)
            .FloatedPickupGridFormat()
            TestData = .GetFloatedPickupList(1)
            .FloatedPickupGridPopulate(TestData)
            With ._FloatedPickupGrid.Sheets(0)
                If .RowCount > 0 Then
                    AllCommentsColumnsPopulatedWhereCommentIsInSourceData = True
                    AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData = True
                    For Row As Integer = 0 To .RowCount - 1
                        Dim DataRowId As Integer = -1

                        If Integer.TryParse(FPVM._FloatedPickupListTestData.Rows(Row).Item(FPVM._FloatedPickupList_FieldName_CashierID).ToString, DataRowId) Then
                            If IsDBNull(FPVM._FloatedPickupListTestData.Rows(Row).Item(FPVM._FloatedPickupList_FieldName_PickupComment)) Then
                                AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData = AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData And Not GridRowForDataRowHasComments(FPVM._FloatedPickupGrid, DataRowId, FPVM._CommentsColumnIndex)
                            Else
                                AllCommentsColumnsPopulatedWhereCommentIsInSourceData = AllCommentsColumnsPopulatedWhereCommentIsInSourceData And GridRowForDataRowHasComments(FPVM._FloatedPickupGrid, DataRowId, FPVM._CommentsColumnIndex)
                            End If
                        Else
                            Assert.Inconclusive("Failed to establish row id from test data so cannot confirm if comments should be present or not")
                        End If
                    Next
                End If
            End With
        End With
        If AllCommentsColumnsPopulatedWhereCommentIsInSourceData And AllCommentsColumnsNotPopulatedWhereCommentIsNotInSourceData Then
            Assert.IsTrue(True)
        Else
            If Not AllCommentsColumnsPopulatedWhereCommentIsInSourceData Then
                Assert.Fail("Not all comments columns populated where there are comments in the test data")
            Else
                Assert.Fail("Some comments columns populated where there are no comments in the test data")
            End If
        End If
    End Sub

    <TestMethod()> _
    Public Sub FloatedPickupGridPopulate_PopulatesCommentsCellWithTestComments()
        Dim FPVM As New TestFloatedPickupVMWithComments_WithTestDataExposed
        Dim AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData As Boolean
        Dim TestData As FloatedPickupListCollection = Nothing

        With FPVM
            .SetFloatedPickupGridControl(New FpSpread)
            .FloatedPickupGridFormat()
            TestData = .GetFloatedPickupList(1)
            .FloatedPickupGridPopulate(TestData)
            With ._FloatedPickupGrid.Sheets(0)
                If .RowCount > 0 Then
                    AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData = True
                    For Row As Integer = 0 To .RowCount - 1
                        Dim DataRowId As Integer = -1

                        If Integer.TryParse(FPVM._FloatedPickupListTestData.Rows(Row).Item(FPVM._FloatedPickupList_FieldName_CashierID).ToString, DataRowId) Then
                            If Not IsDBNull(FPVM._FloatedPickupListTestData.Rows(Row).Item(FPVM._FloatedPickupList_FieldName_PickupComment)) Then
                                AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData = AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData And GridRowForDataRowHasMatchingComments(FPVM._FloatedPickupGrid, DataRowId, FPVM._CommentsColumnIndex, FPVM._FloatedPickupListTestData.Rows(Row).Item(FPVM._FloatedPickupList_FieldName_PickupComment).ToString)
                            End If
                        Else
                            Assert.Inconclusive("Failed to establish row id from test data so cannot confirm if comments should be present or not")
                        End If
                    Next
                End If
            End With
        End With
        Assert.IsTrue(AllCommentsColumnsPopulatedWithCommentWhereCommentIsInSourceData, "Not all Comments were populated.")
    End Sub
#End Region

#Region "SetFloatedPickupForm Tests"

    <TestMethod()> _
    Public Sub SetFloatedPickupForm_Sets_FloatedPickupForm_Member_To_FloatedPickupForm_Parameter()
        Dim FPVM As New FloatedPickupVMWithComments
        Dim expected As New System.Windows.Forms.Form

        With FPVM
            .SetFloatedPickupForm(expected)
            Assert.AreSame(expected, ._FloatedPickupForm)
        End With
    End Sub
#End Region

#Region "FloatedPickupFormFormat Tests"

    <TestMethod()> _
    Public Sub FloatedPickupFormFormat_Sets_FloatedPickupForm_Member_ToBe40Wider()
        Dim FPVM As New FloatedPickupVMWithComments
        Dim NewForm As New System.Windows.Forms.Form
        Dim Expected As Integer = NewForm.Width + 40

        With FPVM
            .SetFloatedPickupForm(NewForm)
            .FloatedPickupFormFormat()
            Assert.AreEqual(Expected, FPVM._FloatedPickupForm.Width)
        End With
    End Sub
#End Region

#Region "Internals"

    Private Function GridRowForDataRowHasComments(ByRef FloatedPickupGrid As FpSpread, ByVal DataRowCashierId As Integer, ByVal CommentsColumnIndex As Integer) As Boolean
        Dim RowIndexToCheck As Integer = GetGridRowIndexForDataRow(FloatedPickupGrid, DataRowCashierId)

        GridRowForDataRowHasComments = False
        If RowIndexToCheck <> -1 Then
            With FloatedPickupGrid.ActiveSheet
                GridRowForDataRowHasComments = .Cells(RowIndexToCheck, CommentsColumnIndex).Value IsNot Nothing
            End With
        End If
    End Function

    Private Function GridRowForDataRowHasMatchingComments(ByRef FloatedPickupGrid As FpSpread, ByVal DataRowCashierId As Integer, ByVal CommentsColumnIndex As Integer, ByVal Comments As String) As Boolean
        Dim RowIndexToCheck As Integer = GetGridRowIndexForDataRow(FloatedPickupGrid, DataRowCashierId)

        GridRowForDataRowHasMatchingComments = False
        If RowIndexToCheck <> -1 Then
            With FloatedPickupGrid.ActiveSheet
                If .Cells(RowIndexToCheck, CommentsColumnIndex) IsNot Nothing Then
                    GridRowForDataRowHasMatchingComments = String.Compare(.Cells(RowIndexToCheck, CommentsColumnIndex).Value.ToString, Comments) = 0
                End If
            End With
        End If
    End Function

    Private Function GetGridRowIndexForDataRow(ByRef FloatedPickupGrid As FpSpread, ByVal DataRowCashierId As Integer) As Integer
        Dim Row As Integer

        GetGridRowIndexForDataRow = -1

        With FloatedPickupGrid.ActiveSheet
            For Row = 0 To .RowCount - 1
                If .Rows(Row).Tag IsNot Nothing Then
                    If .Rows(Row).Tag.ToString = DataRowCashierId.ToString Then
                        GetGridRowIndexForDataRow = Row
                        Exit For
                    End If
                End If
            Next
        End With
    End Function
#End Region
End Class
