﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class BankingPickupCheckVMWithComments_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "SetBankingPickupCheckGridControl Tests"

    <TestMethod()> _
    Public Sub SetBankingPickupCheckGridControl()
        Dim BPCVM As New BankingPickupCheckVMWithComments
        Dim expected As New FpSpread

        With BPCVM
            .SetBankingPickupCheckGridControl(expected)
            Assert.AreSame(expected, ._BankingPickupCheckGrid)
        End With
    End Sub
#End Region

#Region "BankingPickupCheckGridFormat Tests"

    <TestMethod()> _
    Public Sub BankingPickupCheckGridFormat_SetsGridsVerticalScrollBarPolicyToAsNeeded()
        Dim BPCVM As New BankingPickupCheckVMWithComments

        With BPCVM
            .SetBankingPickupCheckGridControl(New FpSpread)
            .BankingPickupCheckGridFormat()
            Assert.AreEqual(ScrollBarPolicy.AsNeeded, ._BankingPickupCheckGrid.VerticalScrollBarPolicy)
        End With
    End Sub
#End Region

#Region "SetBankingPickupCheckForm Tests"

    <TestMethod()> _
    Public Sub SetBankingPickupCheckForm_Sets_BankingPickupCheckForm_Member_To_BankingPickupCheckForm_Parameter()
        Dim BPCVM As New BankingPickupCheckVMWithComments
        Dim expected As New System.Windows.Forms.Form

        BPCVM.SetBankingPickupCheckForm(expected)
        Assert.AreSame(expected, BPCVM._BankingPickupCheckForm)
    End Sub
#End Region

#Region "BankingPickupCheckFormFormat Tests"

    <TestMethod()> _
    Public Sub BankingPickupCheckFormFormat_Sets_BankingPickupCheckForm_Member_ToBe100Higher()
        Dim BPCVM As New BankingPickupCheckVMWithComments
        Dim NewForm As New System.Windows.Forms.Form
        Dim expected As Integer = NewForm.Height + 100

        With BPCVM
            .SetBankingPickupCheckForm(NewForm)
            .BankingPickupCheckFormFormat()
            Assert.AreEqual(expected, ._BankingPickupCheckForm.Height)
        End With
    End Sub
#End Region

#Region "ActiveCellIsCommentsCell Tests"

    <TestMethod()> _
    Public Sub ActiveCellIsCommentsCell_ActiveCellIsCommentCaptionCell_ReturnsTrue()
        Dim BPCVM As New BankingPickupCheckVMWithComments
        Dim RowIndex As Integer = 0
        Dim ColumnIndex As Integer = 0
        Dim TestGrid As FpSpread = GetActiveCellIsCommentsCellTestGrid(RowIndex, ColumnIndex)

        SpreadCellMakeActive(TestGrid.ActiveSheet, RowIndex, ColumnIndex - 2)
        With BPCVM
            .SetBankingPickupCheckGridControl(TestGrid)
            ._CommentsRowIndex = RowIndex
            ._TotalsColumnIndex = ColumnIndex
            Assert.AreEqual(True, .ActiveCellIsCommentsCell)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ActiveCellIsCommentsCell_ActiveCellIsCommentDataCell_ReturnsTrue()
        Dim BPCVM As New BankingPickupCheckVMWithComments
        Dim RowIndex As Integer = 0
        Dim ColumnIndex As Integer = 0
        Dim TestGrid As FpSpread = GetActiveCellIsCommentsCellTestGrid(RowIndex, ColumnIndex)

        SpreadCellMakeActive(TestGrid.ActiveSheet, RowIndex, ColumnIndex - 1)
        With BPCVM
            .SetBankingPickupCheckGridControl(TestGrid)
            ._CommentsRowIndex = RowIndex
            ._TotalsColumnIndex = ColumnIndex
            Assert.AreEqual(True, .ActiveCellIsCommentsCell)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ActiveCellIsCommentsCell_ActiveCellIsInTotalsColumn_ReturnsFalse()
        Dim BPCVM As New BankingPickupCheckVMWithComments
        Dim RowIndex As Integer = 0
        Dim ColumnIndex As Integer = 0
        Dim TestGrid As FpSpread = GetActiveCellIsCommentsCellTestGrid(RowIndex, ColumnIndex)

        SpreadCellMakeActive(TestGrid.ActiveSheet, RowIndex, ColumnIndex)
        With BPCVM
            .SetBankingPickupCheckGridControl(TestGrid)
            ._CommentsRowIndex = RowIndex
            ._TotalsColumnIndex = ColumnIndex
            Assert.AreEqual(False, .ActiveCellIsCommentsCell)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ActiveCellIsCommentsCell_ActiveCellIsNotInCommentsRow_ReturnsFalse()
        Dim BPCVM As New BankingPickupCheckVMWithComments
        Dim RowIndex As Integer = 0
        Dim ColumnIndex As Integer = 0
        Dim TestGrid As FpSpread = GetActiveCellIsCommentsCellTestGrid(RowIndex, ColumnIndex)

        SpreadCellMakeActive(TestGrid.ActiveSheet, RowIndex - 1, ColumnIndex - 1)
        With BPCVM
            .SetBankingPickupCheckGridControl(TestGrid)
            ._CommentsRowIndex = RowIndex
            ._TotalsColumnIndex = ColumnIndex
            Assert.AreEqual(False, .ActiveCellIsCommentsCell)
        End With
    End Sub

    Private Function GetActiveCellIsCommentsCellTestGrid(ByRef GetRowIndex As Integer, ByRef GetColumnIndex As Integer) As FpSpread
        Dim TestSheet As New SheetView

        GetRowIndex = 0
        GetColumnIndex = 0
        TestSheet.ColumnCount = 0
        SpreadSheetClearDown(TestSheet)
        SpreadRowAdd(TestSheet, GetRowIndex)
        SpreadRowAdd(TestSheet, GetRowIndex)
        SpreadColumnAdd(TestSheet, GetColumnIndex)
        SpreadColumnAdd(TestSheet, GetColumnIndex)
        SpreadColumnAdd(TestSheet, GetColumnIndex)
        GetActiveCellIsCommentsCellTestGrid = New FpSpread
        SpreadGridSheetAdd(GetActiveCellIsCommentsCellTestGrid, TestSheet, False, String.Empty)
    End Function
#End Region

#Region "SetBankingPeriodID Tests"

    <TestMethod()> _
    Public Sub SetBankingPeriodID_Sets_BankingPeriodID_Member_To_BankingPeriodID_Parameter()
        Dim BPCVM As New BankingPickupCheckVMWithComments
        Dim expected As Integer = -22017

        BPCVM.SetBankingPeriodID(expected)
        Assert.AreEqual(expected, BPCVM._BankingPeriodID)
    End Sub
#End Region

#Region "BankingPickupCheckGridPopulate Tests"

    <TestMethod()> _
    Public Sub BankingPickupCheckGridPopulate_AddsRowForComments()
        Dim BPCVM As New TestBankingPickupCheckVMWithComments_WithTestDataExposed
        Dim actual As String = String.Empty

        With BPCVM
            .SetBankingPickupCheckGridControl(New FpSpread)
            .BankingPickupCheckGridFormat()
            .BankingPickupCheckGridPopulate(.TestActiveCashierList)
            With ._BankingPickupCheckGrid.ActiveSheet
                If .Cells(BPCVM._CommentsRowIndex, 0).Value IsNot Nothing Then
                    actual = .Cells(BPCVM._CommentsRowIndex, 0).Value.ToString
                End If
            End With
        End With
        Assert.IsTrue(actual IsNot Nothing AndAlso String.Compare(actual, "Comments") = 0)
    End Sub


    ' Ignored as this test seems to be not working. It always set actual as Nothing
    <Ignore()> _
    <TestMethod()> _
    Public Sub BankingPickupCheckGridPopulate_PopulatesPickupCommentsCellWithTestComments()
        Dim BPCVM As New TestBankingPickupCheckVMWithComments_WithTestDataExposed
        Dim actual As String = String.Empty

        With BPCVM
            .SetBankingPickupCheckGridControl(New FpSpread)
            .BankingPickupCheckGridFormat()
            .BankingPickupCheckGridPopulate(.TestActiveCashierList)
            With ._BankingPickupCheckGrid.Sheets(0)
                If .Cells(BPCVM._CommentsRowIndex, 1).Value IsNot Nothing Then
                    actual = .Cells(BPCVM._CommentsRowIndex, BPCVM._PickupCommentsColumnIndex).Value.ToString
                End If
            End With
        End With
        Assert.IsTrue(actual IsNot Nothing AndAlso String.Compare(actual, BPCVM._TestPickupComments) = 0, "Comments row not populated. Expected - " & BPCVM._TestPickupComments & "; actual - " & IIf(String.IsNullOrEmpty(actual), "nothing", actual).ToString & ".")
    End Sub

    <TestMethod()> _
    Public Sub BankingPickupCheckGridPopulate_PopulatesCashDropCommentsCellWithTestComments()
        Dim BPCVM As New TestBankingPickupCheckVMWithComments_WithTestDataExposed
        Dim actual As String = String.Empty

        With BPCVM
            .SetBankingPickupCheckGridControl(New FpSpread)
            .BankingPickupCheckGridFormat()
            .BankingPickupCheckGridPopulate(.TestActiveCashierList)
            With ._BankingPickupCheckGrid.Sheets(0)
                If .Cells(BPCVM._CommentsRowIndex, 1).Value IsNot Nothing Then
                    actual = .Cells(BPCVM._CommentsRowIndex, BPCVM._CashDropCommentsColumnIndex).Value.ToString
                End If
            End With
        End With
        Assert.IsTrue(actual IsNot Nothing AndAlso String.Compare(actual, BPCVM._TestCashDropComments) = 0, "Comments row not populated. Expected - " & BPCVM._TestCashDropComments & "; actual - " & IIf(String.IsNullOrEmpty(actual), "nothing", actual).ToString & ".")
    End Sub
#End Region

#Region "PersistCommentsIfChanged Tests"

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsNotSet_DoesNotCall_GetComments()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(False)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsFalse(.GetCommentsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsNotSet_DoesNotCall_GetBagCommentFromActiveCashierLists()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(False)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsFalse(.GetBagCommentFromActiveCashierListsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsNotSet_DoesNotCall_NewCommentsDoNotMatchOldComments()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(False)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsFalse(.NewCommentsDoNotMatchOldCommentsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsNotSet_DoesNotCall_PickupBagSaveComments()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(False)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsFalse(.PickupBagSaveCommentsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsSet_DoesCall_GetComments()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(True)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsTrue(.GetCommentsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsSet_DoesCall_GetBagCommentFromActiveCashierLists()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(True)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsTrue(.GetBagCommentFromActiveCashierListsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsSet_DoesCall_NewCommentsDoNotMatchOldComments()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(True)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsTrue(.NewCommentsDoNotMatchOldCommentsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsSetAndNewCommentsDoNotMatchOldCommentsReturnsFalse_DoesNotCall_PickupBagSaveComments()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(True, False)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsFalse(.PickupBagSaveCommentsWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub PersistCommentsIfChanged_ActiveCashierListsIsSetAndNewCommentsDoNotMatchOldCommentsReturnsFalse_DoesCall_PickupBagSaveComments()
        Dim TestPersistCommentIfChanged As New TestBankingPickupCheckVMWithComments_OverridingProceduresCalledByPersistCommentsIfChangedToFlagWhetherHaveBeenCalled(True, True)

        With TestPersistCommentIfChanged
            .PersistCommentsIfChanged(1)
            Assert.IsTrue(.PickupBagSaveCommentsWasCalled)
        End With
    End Sub
#End Region

#Region "GetActiveCashierListCollection Tests"

    <TestMethod()> _
    Public Sub GetActiveCashierListCollection_BankingPeriodIsNotSet_ReturnsEmptyActiveCashierListCollection()
        Dim TestPickupComments As New TestBankingPickupCheckVMWithComments_OverridesBankingPeriodIsSetToReturnFalse_OverridesLoadAllCashiersToFlagIfItIsCalled
        Dim ReturnedCashiers As ActiveCashierListCollection = TestPickupComments.GetActiveCashierListCollection()

        Assert.IsTrue(ReturnedCashiers IsNot Nothing AndAlso ReturnedCashiers.Count = 0)
    End Sub

    <TestMethod()> _
    Public Sub GetActiveCashierListCollection_BankingPeriodIsNotSet_DoesNotCallLoadAllCashiers()
        Dim TestPickupComments As New TestBankingPickupCheckVMWithComments_OverridesBankingPeriodIsSetToReturnFalse_OverridesLoadAllCashiersToFlagIfItIsCalled
        Dim ReturnedCashiers As ActiveCashierListCollection = Nothing

        With TestPickupComments
            ReturnedCashiers = .GetActiveCashierListCollection()
            Assert.IsFalse(.LoadAllCashiersWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetActiveCashierListCollection_BankingPeriodIsSet_CallsLoadAllCashiers()
        Dim TestPickupComments As New TestBankingPickupCheckVMWithComments_OverridesBankingPeriodIsSetToReturnTrue_OverridesLoadAllCashiersToFlagIfItIsCalled
        Dim ReturnedCashiers As ActiveCashierListCollection = Nothing

        With TestPickupComments
            ReturnedCashiers = .GetActiveCashierListCollection()
            Assert.IsTrue(.LoadAllCashiersWasCalled)
        End With
    End Sub
#End Region

#Region "ReadyActiveCommentsCellForEditing Tests"

    <TestMethod()> _
    Public Sub ReadyActiveCommentsCellForEditing_When_BankingPickupCheckGridControlIsSet_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestReadyActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(False)

        With TestReadyActiveCommentsCellForEditing
            .ReadyActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ReadyActiveCommentsCellForEditing_When_ActiveSheetIsSet_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestReadyActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, False)

        With TestReadyActiveCommentsCellForEditing
            .ReadyActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ReadyActiveCommentsCellForEditing_When_CellIsCommentsCell_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestReadyActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, False)

        With TestReadyActiveCommentsCellForEditing
            .ReadyActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ReadyActiveCommentsCellForEditing_When_ColumnIsPotentiallyEditable_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestReadyActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, False)

        With TestReadyActiveCommentsCellForEditing
            .ReadyActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub


    <TestMethod()> _
    Public Sub ReadyActiveCommentsCellForEditing_When_BagNotAlreadyMarkedAsChecked_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestReadyActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, False)

        With TestReadyActiveCommentsCellForEditing
            .ReadyActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ReadyActiveCommentsCellForEditing_When_GridCellIsEditableCommentCell_MakesCellTheActiveCell()
        Dim TestReadyActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, True)

        With TestReadyActiveCommentsCellForEditing
            .ReadyActiveCommentsCellForEditing(0, 1)
            With ._BankingPickupCheckGrid
                Assert.IsTrue(.ActiveSheet.ActiveCell.Row.Index = 0 AndAlso .ActiveSheet.ActiveCell.Column.Index = 1)
            End With
        End With
    End Sub

    <TestMethod()> _
    Public Sub ReadyActiveCommentsCellForEditing_When_GridCellIsEditableCommentCell_SetsGridsEditModeReplaceToFalse()
        Dim TestReadyActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, True)

        With TestReadyActiveCommentsCellForEditing
            .ReadyActiveCommentsCellForEditing(0, 1)
            Assert.AreEqual(False, ._BankingPickupCheckGrid.EditModeReplace)
        End With
    End Sub

    <TestMethod()> _
    Public Sub ReadyActiveCommentsCellForEditing_When_GridCellIsEditableCommentCell_SetsGridsEditModePermanentToTrue()
        Dim TestReadyActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByReadyActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, True)

        With TestReadyActiveCommentsCellForEditing
            .ReadyActiveCommentsCellForEditing(0, 1)
            Assert.AreEqual(True, ._BankingPickupCheckGrid.EditModePermanent)
        End With
    End Sub
#End Region

#Region "UndoReadinessOfActiveCommentsCellForEditing Tests"

    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_BankingPickupCheckGridControlIsSet_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(False)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_ActiveSheetIsSet_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, False)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_CellIsCommentsCell_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, False)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_ColumnIsPotentiallyEditable_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, False)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub


    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_BagNotAlreadyMarkedAsChecked_ReturnsFalse_DoesNotCall_SetUpActiveCellForTextEditingAndStart()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, False)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(1, 1)
            Assert.IsFalse(.SetUpActiveCellForTextEditingAndStartWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_GridCellIsEditableCommentCell_MakesCellTheActiveCell()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, True)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(0, 1)
            With ._BankingPickupCheckGrid
                Assert.IsTrue(.ActiveSheet.ActiveCell.Row.Index = 0 AndAlso .ActiveSheet.ActiveCell.Column.Index = 1)
            End With
        End With
    End Sub

    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_GridCellIsEditableCommentCell_SetsGridsEditModeToFalse()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, True)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(0, 1)
            Assert.AreEqual(False, ._BankingPickupCheckGrid.EditMode)
        End With
    End Sub

    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_GridCellIsEditableCommentCell_SetsGridsEditModeReplaceToTrue()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, True)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(0, 1)
            Assert.AreEqual(True, ._BankingPickupCheckGrid.EditModeReplace)
        End With
    End Sub

    <TestMethod()> _
    Public Sub UndoReadinessOfActiveCommentsCellForEditing_When_GridCellIsEditableCommentCell_SetsGridsEditModePermanentToFalse()
        Dim TestUndoReadinessOfActiveCommentsCellForEditing As New TestBankingPickupCheckVMWithComments_OverridesMethodsCalledByUndoReadinessOfActiveCommentsCellForEditing_ToFlagIfTheyAreCalled(True, True, True, True, True)

        With TestUndoReadinessOfActiveCommentsCellForEditing
            .UndoReadinessOfActiveCommentsCellForEditing(0, 1)
            Assert.AreEqual(False, ._BankingPickupCheckGrid.EditModePermanent)
        End With
    End Sub
#End Region

#Region "GetComments Tests"

    <TestMethod()> _
    Public Sub GetComments_BankingPickupCheckGridControlIsNotSet_ReturnsEmptyString()
        Dim TestGetComments As New TestBankingPickupCheckVMWIthComments_OverridesBankingPickupCheckGridControlIsSetToReturnFalse
        Dim expected As String = ""
        Dim actual As String = TestGetComments.GetComments(1)

        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub GetComments_BankingPickupCheckGridControlIsSetButActiveSheetIsNotSet_ReturnsEmptyString()
        Dim TestGetComments As New TestBankingPickupCheckVMWIthComments_OverridesBankingPickupCheckGridControlIsSetToReturnTrue_OverridesActiveIsSetToReturnFalse
        Dim expected As String = ""
        Dim actual As String = TestGetComments.GetComments(1)

        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub GetComments_BankingPickupCheckGridControlIsSetAndActiveSheetIsSet_ReturnsCorrectCommentFromGrid()
        Dim TestGetComments As New TestBankingPickupCheckVMWIthComments_ProvidesTestGridAndCommentsToBeExtracted
        Dim expected As String = TestGetComments._TestCommentsForBag2
        Dim actual As String = TestGetComments.GetComments(TestGetComments._Bag2Id)

        Assert.AreEqual(expected, actual)
    End Sub
#End Region

#Region "GetCommentsRowIndex Tests"


    <TestMethod()> _
    Public Sub GetCommentsRowIndex_ReturnsValueAssignedInternallyForCommentsRowIndexWhenCommentsRowIsCreatedMinusOne()
        Dim TestGetComments As New BankingPickupCheckVMWithComments
        Dim expected As Integer = -1
        Dim actual As Integer = -1

        With TestGetComments
            .SetBankingPickupCheckGridControl(New FpSpread)
            .BankingPickupCheckGridFormat()
            expected = ._CommentsRowIndex - 1
            actual = .GetCommentsRowIndex
        End With
        Assert.AreEqual(expected, actual)
    End Sub
#End Region
End Class
