﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class SafeCheckValidation_UnitTests


    Private testContextInstance As TestContext

    Private safeCheckValidationFactoryInstance As SafeCheckValidationFactory = New SafeCheckValidationFactory

#Region "Unit Tests"

    <TestMethod()> Public Sub SetRequirementOn_CallSafeCheckValidationRequired_ReturnsTrue()
        Assert.AreEqual(True, safeCheckValidationFactoryInstance.ImplementationA.AllowSafeCheckCompletion)
    End Sub

    <TestMethod()> Public Sub SetRequirementOff_CallSafeCheckValidationNotRequired_ReturnsFalse()
        Assert.AreEqual(False, safeCheckValidationFactoryInstance.ImplementationB.AllowSafeCheckCompletion)
    End Sub
#End Region


End Class
