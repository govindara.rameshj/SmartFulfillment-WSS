﻿<TestClass()> Public Class EndOfDayCheckSummaryModelUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Class"

    Private Class TestEndOfDayCheckSummaryModel
        Inherits EndOfDayCheckSummaryModel

        Private _LocalDT As DataTable

        Public Sub New(ByVal DT As DataTable)

            _LocalDT = DT

        End Sub

        Friend Overrides Sub PopulateDataTable()

            MyBase._DT = _LocalDT

        End Sub

    End Class

#End Region

#Region "Properties"

    <TestMethod()> Public Sub PropertyPickupBagCountGet()

        Dim EOD As New EndOfDayCheckSummaryModel

        EOD.PickupBagCount = 1

        Assert.AreEqual(1, EOD.PickupBagCount)

    End Sub

    <TestMethod()> Public Sub PropertyPickupSystemCountGet()

        Dim EOD As New EndOfDayCheckSummaryModel

        EOD.PickupBagSystemCount = 2

        Assert.AreEqual(2, EOD.PickupBagSystemCount)

    End Sub

    <TestMethod()> Public Sub PropertyBankingCashCountGet()

        Dim EOD As New EndOfDayCheckSummaryModel

        EOD.BankingCashCount = 3

        Assert.AreEqual(3, EOD.BankingCashCount)

    End Sub

    <TestMethod()> Public Sub PropertyBankingCashSystemCountGet()

        Dim EOD As New EndOfDayCheckSummaryModel

        EOD.BankingCashSystemCount = 4

        Assert.AreEqual(4, EOD.BankingCashSystemCount)

    End Sub

    <TestMethod()> Public Sub PropertyBankingChequeCountGet()

        Dim EOD As New EndOfDayCheckSummaryModel

        EOD.BankingChequeCount = 5

        Assert.AreEqual(5, EOD.BankingChequeCount)

    End Sub

    <TestMethod()> Public Sub PropertyBankingChequeSystemCountGet()

        Dim EOD As New EndOfDayCheckSummaryModel

        EOD.BankingChequeSystemCount = 6

        Assert.AreEqual(6, EOD.BankingChequeSystemCount)

    End Sub

    <TestMethod()> Public Sub PropertyManagerGet()

        Dim EOD As New EndOfDayCheckSummaryModel

        EOD.Manager = "Mr Jo Bloggs"

        Assert.AreEqual("Mr Jo Bloggs", EOD.Manager)

    End Sub

    <TestMethod()> Public Sub PropertyWitnessGet()

        Dim EOD As New EndOfDayCheckSummaryModel

        EOD.Witness = "Mr John Smith"

        Assert.AreEqual("Mr John Smith", EOD.Witness)

    End Sub

#End Region

#Region "Load Data"

    <TestMethod()> Public Sub LoadData_DataTableNull_PropertyPickupBagCountGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(Nothing)

        EOD.LoadData(Nothing)

        Assert.AreEqual(0, EOD.PickupBagCount)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableEmpty_PropertyPickupBagCountGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(New DataTable)

        EOD.LoadData(Nothing)

        Assert.AreEqual(0, EOD.PickupBagCount)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableNotEmpty_PropertyPickupBagCountGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(CreateAndPopulateDataTable(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID"))

        EOD.LoadData(Nothing)

        Assert.AreEqual(6, EOD.PickupBagCount)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableNotEmpty_PropertyPickupSystemCountGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(CreateAndPopulateDataTable(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID"))

        EOD.LoadData(Nothing)

        Assert.AreEqual(8, EOD.PickupBagSystemCount)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableNotEmpty_PropertyBankingCashCountGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(CreateAndPopulateDataTable(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID"))

        EOD.LoadData(Nothing)

        Assert.AreEqual(10, EOD.BankingCashCount)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableNotEmpty_PropertyBankingCashSystemCountGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(CreateAndPopulateDataTable(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID"))

        EOD.LoadData(Nothing)

        Assert.AreEqual(12, EOD.BankingCashSystemCount)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableNotEmpty_PropertyBankingChequeCountGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(CreateAndPopulateDataTable(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID"))

        EOD.LoadData(Nothing)

        Assert.AreEqual(14, EOD.BankingChequeCount)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableNotEmpty_PropertyBankingChequeSystemCountGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(CreateAndPopulateDataTable(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID"))

        EOD.LoadData(Nothing)

        Assert.AreEqual(16, EOD.BankingChequeSystemCount)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableNotEmpty_PropertyManagerGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(CreateAndPopulateDataTable(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID"))

        EOD.LoadData(Nothing)

        Assert.AreEqual("Manager ID", EOD.Manager)

    End Sub

    <TestMethod()> Public Sub LoadData_DataTableNotEmpty_PropertyWitnessGet()

        Dim EOD As TestEndOfDayCheckSummaryModel

        EOD = New TestEndOfDayCheckSummaryModel(CreateAndPopulateDataTable(6, 8, 10, 12, 14, 16, "Manager ID", "Witness ID"))

        EOD.LoadData(Nothing)

        Assert.AreEqual("Witness ID", EOD.Witness)

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Function CreateAndPopulateDataTable(ByVal PickupBagCount As Integer, _
                                                ByVal PickupBagSystemCount As Integer, _
                                                ByVal BankingCashCount As Integer, _
                                                ByVal BankingCashSystemCount As Integer, _
                                                ByVal BankingChequeCount As Integer, _
                                                ByVal BankingChequeSystemCount As Integer, _
                                                ByVal Manager As String, _
                                                ByVal Witness As String) As DataTable

        Dim DT As DataTable

        DT = New DataTable

        DT.Columns.Add("PickupBagCount", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("PickupBagSystemCount", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("BankingCashCount", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("BankingCashSystemCount", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("BankingChequeCount", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("BankingChequeSystemCount", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("Manager", System.Type.GetType("System.String"))
        DT.Columns.Add("Witness", System.Type.GetType("System.String"))

        DT.Rows.Add(PickupBagCount, PickupBagSystemCount, BankingCashCount, BankingCashSystemCount, BankingChequeCount, BankingChequeSystemCount, Manager, Witness)

        Return DT

    End Function

#End Region

End Class