﻿<TestClass()> Public Class NoSaleBankingUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Requirement Test"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassNoSaleBanking()

        Dim NewVersionOnly As INoSaleBanking
        Dim Factory As IBaseFactory(Of INoSaleBanking)

        Factory = New NoSaleBankingFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("NewBanking.Core.TpWickes.NoSaleBanking", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - No Sale Banking Test"

    <TestMethod()> Public Sub NoSaleBankingCheck_ParameterMissing_ReturnFalse()

        Dim V As INoSaleBanking = (New NoSaleBankingFactory).GetImplementation

        ArrangeNoSaleBankingCheck(V, New System.Nullable(Of Boolean))
        Assert.IsFalse(V.AllowNoSaleBanking())

    End Sub

    <TestMethod()> Public Sub NoSaleBankingCheck_ParameterFalse_ReturnFalse()

        Dim V As INoSaleBanking = (New NoSaleBankingFactory).GetImplementation

        ArrangeNoSaleBankingCheck(V, False)
        Assert.IsFalse(V.AllowNoSaleBanking())

    End Sub

    <TestMethod()> Public Sub NoSaleBankingCheck_ParameterTrue_ReturnTrue()

        Dim V As INoSaleBanking = (New NoSaleBankingFactory).GetImplementation

        ArrangeNoSaleBankingCheck(V, True)
        Assert.IsTrue(V.AllowNoSaleBanking())

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeNoSaleBankingCheck(ByRef V As INoSaleBanking, ByVal NoSaleBanking As System.Nullable(Of Boolean))

        Dim NoSaleStub As New NoSaleBankingRepositoryStub

        NoSaleStub.ConfigureStub(NoSaleBanking)
        NoSaleBankingRepositoryFactory.FactorySet(NoSaleStub)

    End Sub

#End Region

End Class