﻿<TestClass()> Public Class SafeBagModelUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestSafeBagCollectionModel
        Inherits SafeBagCollectionModel

        Private _LocalDT As DataTable

        Public Sub New(ByVal DT As DataTable)

            _LocalDT = DT

        End Sub

        Friend Overrides Sub LoadPickupBagExistInSafeData()

            MyBase._EndOfDayData = _LocalDT

        End Sub

        Friend Overrides Sub LoadBankingBagExistInSafeData()

            MyBase._EndOfDayData = _LocalDT

        End Sub

        Friend Overrides Sub LoadSafeBag()

            MyBase._SafeBagData = _LocalDT
        End Sub
    End Class

#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub SafeBagModelFactory_ReturnEndOfDayCheckEmptyViewModel()

        Dim V As ISafeBagModel

        V = (New SafeBagModelFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.SafeBagModel", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeBagCollectionModelFactory_ReturnSafeBagCollectionModel()

        Dim V As ISafeBagCollectionModel

        V = (New SafeBagCollectionModelFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.SafeBagCollectionModel", V.GetType.FullName)

    End Sub

#End Region

#Region "Interface: Properties"

    <TestMethod()> _
    Public Sub PropertyIDGet()

        Dim Model As New SafeBagModel

        Model.ID = 2

        Assert.AreEqual(2, Model.ID)

    End Sub

    <TestMethod()> _
    Public Sub PropertySealNumberGet()

        Dim Model As New SafeBagModel

        Model.SealNumber = "12345678"

        Assert.AreEqual("12345678", Model.SealNumber)

    End Sub

    <TestMethod()> _
    Public Sub PropertyTypeGet()

        Dim Model As New SafeBagModel

        Model.BagType = "P"

        Assert.AreEqual("P", Model.BagType)

    End Sub

    <TestMethod()> _
    Public Sub PropertyStateGet()

        Dim Model As New SafeBagModel

        Model.BagState = "S"

        Assert.AreEqual("S", Model.BagState)

    End Sub

    <TestMethod()> _
    Public Sub PropertyValueGet()

        Dim Model As New SafeBagModel

        Model.Value = CType(123.45, Decimal)

        Assert.AreEqual(CType(123.45, Decimal), Model.Value)

    End Sub

    <TestMethod()> _
    Public Sub PropertyCommentGet()
        Dim Model As New SafeBagModel

        Model.Comments = "A test comment"
        Assert.AreEqual("A test comment", Model.Comments)
    End Sub
#End Region

#Region "Interface: Load Routines: Load Pickup Bag Exist In Safe"

    <TestMethod()> Public Sub LoadPickupBagExistInSafe_DataTableNull_ReturnCountZero()

        Dim SafeBagCollection As TestSafeBagCollectionModel

        SafeBagCollection = New TestSafeBagCollectionModel(Nothing)
        SafeBagCollection.LoadPickupBagExistInSafe(Nothing)

        Assert.AreEqual(0, SafeBagCollection.Count)

    End Sub

    <TestMethod()> Public Sub LoadPickupBagExistInSafe_DataTableEmpty_ReturnCountZero()

        Dim SafeBagCollection As TestSafeBagCollectionModel

        SafeBagCollection = New TestSafeBagCollectionModel(New DataTable)
        SafeBagCollection.LoadPickupBagExistInSafe(Nothing)

        Assert.AreEqual(0, SafeBagCollection.Count)

    End Sub

    <TestMethod()> Public Sub LoadPickupBagExistInSafe_DataTableNotEmpty_ReturnCountThree()

        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim DT As DataTable

        DT = CreateDataTable()
        PopulateDataTable(DT, 1, "12345678", "P", "S", 123.45)
        PopulateDataTable(DT, 2, "23456789", "P", "S", 234.56)
        PopulateDataTable(DT, 3, "34567890", "P", "S", 345.67)

        SafeBagCollection = New TestSafeBagCollectionModel(DT)

        SafeBagCollection.LoadPickupBagExistInSafe(Nothing)

        Assert.AreEqual(3, SafeBagCollection.Count)

    End Sub

    <TestMethod()> Public Sub LoadPickupBagExistInSafe_DataTableNotEmpty_RowThree_PropertyIDGet()

        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim DT As DataTable

        DT = CreateDataTable()
        PopulateDataTable(DT, 1, "12345678", "P", "S", 123.45)
        PopulateDataTable(DT, 2, "23456789", "P", "S", 234.56)
        PopulateDataTable(DT, 3, "34567890", "P", "S", 345.67)

        SafeBagCollection = New TestSafeBagCollectionModel(DT)

        SafeBagCollection.LoadPickupBagExistInSafe(Nothing)

        Assert.AreEqual(3, SafeBagCollection.Item(2).ID)

    End Sub

    <TestMethod()> Public Sub LoadPickupBagExistInSafe_DataTableNotEmpty_RowThree_PropertySealNumberGet()

        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim DT As DataTable

        DT = CreateDataTable()
        PopulateDataTable(DT, 1, "12345678", "P", "S", 123.45)
        PopulateDataTable(DT, 2, "23456789", "P", "S", 234.56)
        PopulateDataTable(DT, 3, "34567890", "P", "S", 345.67)

        SafeBagCollection = New TestSafeBagCollectionModel(DT)

        SafeBagCollection.LoadPickupBagExistInSafe(Nothing)

        Assert.AreEqual("34567890", SafeBagCollection.Item(2).SealNumber)

    End Sub

    <TestMethod()> Public Sub LoadPickupBagExistInSafe_DataTableNotEmpty_RowThree_PropertyBagTypeGet()

        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim DT As DataTable

        DT = CreateDataTable()
        PopulateDataTable(DT, 1, "12345678", "P", "S", 123.45)
        PopulateDataTable(DT, 2, "23456789", "P", "S", 234.56)
        PopulateDataTable(DT, 3, "34567890", "P", "S", 345.67)

        SafeBagCollection = New TestSafeBagCollectionModel(DT)

        SafeBagCollection.LoadPickupBagExistInSafe(Nothing)

        Assert.AreEqual("P", SafeBagCollection.Item(2).BagType)

    End Sub

    <TestMethod()> Public Sub LoadPickupBagExistInSafe_DataTableNotEmpty_RowThree_PropertyBagStateGet()

        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim DT As DataTable

        DT = CreateDataTable()
        PopulateDataTable(DT, 1, "12345678", "P", "S", 123.45)
        PopulateDataTable(DT, 2, "23456789", "P", "S", 234.56)
        PopulateDataTable(DT, 3, "34567890", "P", "S", 345.67)

        SafeBagCollection = New TestSafeBagCollectionModel(DT)

        SafeBagCollection.LoadPickupBagExistInSafe(Nothing)

        Assert.AreEqual("S", SafeBagCollection.Item(2).BagState)

    End Sub

    <TestMethod()> Public Sub LoadPickupBagExistInSafe_DataTableNotEmpty_RowThree_PropertyValueGet()

        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim DT As DataTable

        DT = CreateDataTable()
        PopulateDataTable(DT, 1, "12345678", "P", "S", 123.45)
        PopulateDataTable(DT, 2, "23456789", "P", "S", 234.56)
        PopulateDataTable(DT, 3, "34567890", "P", "S", 345.67)

        SafeBagCollection = New TestSafeBagCollectionModel(DT)

        SafeBagCollection.LoadPickupBagExistInSafe(Nothing)

        Assert.AreEqual(CType(345.67, Decimal), SafeBagCollection.Item(2).Value)

    End Sub

#End Region

#Region "Interface: Load Routines: Load Banking Bag Exist In Safe"

    <TestMethod()> Public Sub LoadBankingBagExistInSafe_DataTable_DataTableNull_ReturnCountZero()

        Dim SafeBagCollection As TestSafeBagCollectionModel

        SafeBagCollection = New TestSafeBagCollectionModel(Nothing)
        SafeBagCollection.LoadBankingBagExistInSafe(Nothing)

        Assert.AreEqual(0, SafeBagCollection.Count)

    End Sub

    <TestMethod()> Public Sub LoadBankingBagExistInSafe_DataTable_DataTableEmpty_ReturnCountZero()

        Dim SafeBagCollection As TestSafeBagCollectionModel

        SafeBagCollection = New TestSafeBagCollectionModel(New DataTable)
        SafeBagCollection.LoadBankingBagExistInSafe(Nothing)

        Assert.AreEqual(0, SafeBagCollection.Count)

    End Sub

    <TestMethod()> Public Sub LoadBankingBagExistInSafe_DataTable_DataTableNotEmpty_ReturnCountThree()

        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim DT As DataTable

        DT = CreateDataTable()
        PopulateDataTable(DT, 1, "201-34666920-7", "B", "S", 123.45)
        PopulateDataTable(DT, 2, "201-34666920-7", "B", "S", 234.56)
        PopulateDataTable(DT, 3, "201-34666920-7", "B", "S", 345.67)

        SafeBagCollection = New TestSafeBagCollectionModel(DT)
        SafeBagCollection.LoadBankingBagExistInSafe(Nothing)

        Assert.AreEqual(3, SafeBagCollection.Count)

    End Sub

#End Region

#Region "Interface: Load Routines: List"

    <TestMethod()> Public Sub ListBags_DataNotEmpty_ReturnListCountThree()

        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim DT As DataTable

        DT = CreateDataTable()
        PopulateDataTable(DT, 1, "201-34666920-7", "B", "S", 123.45)
        PopulateDataTable(DT, 2, "201-34666920-7", "B", "S", 234.56)
        PopulateDataTable(DT, 3, "201-34666920-7", "B", "S", 345.67)

        SafeBagCollection = New TestSafeBagCollectionModel(DT)
        SafeBagCollection.LoadBankingBagExistInSafe(Nothing)

        Assert.AreEqual(3, SafeBagCollection.ListBags.Count)

    End Sub

#End Region

#Region "Interface: Load Routines: Load Safe Bag"

    <TestMethod()> _
    Public Sub LoadSafeBag_DataTable_DataTableNull_ReturnCountZero()
        Dim SafeBagCollection As TestSafeBagCollectionModel

        SafeBagCollection = New TestSafeBagCollectionModel(Nothing)
        SafeBagCollection.LoadSafeBag(Nothing)
        Assert.AreEqual(0, SafeBagCollection.Count)
    End Sub

    <TestMethod()> _
    Public Sub LoadSafeBag_DataTable_DataTableEmpty_ReturnCountZero()
        Dim SafeBagCollection As TestSafeBagCollectionModel

        SafeBagCollection = New TestSafeBagCollectionModel(New DataTable)
        SafeBagCollection.LoadSafeBag(Nothing)
        Assert.AreEqual(0, SafeBagCollection.Count)
    End Sub

    <TestMethod()> _
    Public Sub LoadSafeBag_DataTable_DataTableWith3Rows_ReturnCountThree()
        Dim SafeBagCollection As TestSafeBagCollectionModel
        Dim SafeBagCollectionTestData As DataTable

        SafeBagCollectionTestData = CreateDataTable()
        PopulateDataTable(SafeBagCollectionTestData, 1, "201-34666920-7", "B", "S", 123.45, "Test Comment 1")
        PopulateDataTable(SafeBagCollectionTestData, 2, "201-34666920-7", "B", "S", 234.56, "Test Comment 2")
        PopulateDataTable(SafeBagCollectionTestData, 3, "201-34666920-7", "B", "S", 345.67, "Test Comment 3")

        SafeBagCollection = New TestSafeBagCollectionModel(SafeBagCollectionTestData)
        SafeBagCollection.LoadSafeBag(Nothing)

        Assert.AreEqual(3, SafeBagCollection.Count)
    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Function CreateDataTable() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        DT.Columns.Add("ID", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("SealNumber", System.Type.GetType("System.String"))
        DT.Columns.Add("Type", System.Type.GetType("System.String"))
        DT.Columns.Add("State", System.Type.GetType("System.String"))
        DT.Columns.Add("Value", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("Comments", System.Type.GetType("System.String"))

        Return DT
    End Function

    Private Sub PopulateDataTable(ByRef DT As DataTable, _
                                  ByVal ID As Integer, _
                                  ByVal SealNumber As String, _
                                  ByVal BagType As String, _
                                  ByVal BagState As String, _
                                  ByVal Value As Double, _
                                  Optional ByVal Comments As String = Nothing)

        If String.IsNullOrEmpty(Comments) Then
            DT.Rows.Add(ID, SealNumber, BagType, BagState, Value)
        Else
            DT.Rows.Add(ID, SealNumber, BagType, BagState, Value)
        End If
    End Sub

#End Region

End Class