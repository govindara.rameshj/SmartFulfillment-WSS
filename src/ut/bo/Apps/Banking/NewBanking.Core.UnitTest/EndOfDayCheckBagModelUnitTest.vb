﻿<TestClass()> Public Class EndOfDayCheckBagModelUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub RequirementCheck_ReturnSafeBagScannedModel()

        Dim V As ISafeBagScannedModel

        V = (New SafeBagScannedModelFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.SafeBagScannedModel", V.GetType.FullName)

    End Sub

#End Region

#Region "EndOfDayCheckBagModel: Properties"

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertyIDGet()

        Dim Model As New SafeBagScannedModel

        Model.ID = 1

        Assert.AreEqual(1, Model.ID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertyProcessIDGet()

        Dim Model As New SafeBagScannedModel

        Model.ProcessID = 222

        Assert.AreEqual(222, Model.ProcessID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertyPeriodIDGet()

        Dim Model As New SafeBagScannedModel

        Model.PeriodID = 2

        Assert.AreEqual(2, Model.PeriodID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertySafeBagsIDGet()

        Dim Model As New SafeBagScannedModel

        Model.SafeBagsID = 2

        Assert.AreEqual(2, Model.SafeBagsID.Value)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertySafeBagsIDGet_Null()

        Dim Model As New SafeBagScannedModel

        Assert.IsFalse(Model.SafeBagsID.HasValue)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertySealNumberGet()

        Dim Model As New SafeBagScannedModel

        Model.SealNumber = "12345678"

        Assert.AreEqual("12345678", Model.SealNumber)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertyBagTypeGet()

        Dim Model As New SafeBagScannedModel

        Model.BagType = "P"

        Assert.AreEqual("P", Model.BagType)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertyCommentGet()

        Dim Model As New SafeBagScannedModel

        Model.Comment = "Comment A"

        Assert.AreEqual("Comment A", Model.Comment)

    End Sub

#End Region

#Region "EndOfDayCheckBagModel: Methods"

    'N/A

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

End Class