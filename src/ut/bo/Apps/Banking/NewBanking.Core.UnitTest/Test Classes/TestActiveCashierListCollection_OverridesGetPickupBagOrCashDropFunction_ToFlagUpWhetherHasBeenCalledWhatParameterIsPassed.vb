﻿Friend Class TestActiveCashierListCollection_OverridesGetPickupBagOrCashDropFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
    Inherits ActiveCashierListCollection

    Private _GetPickupBagOrCashDropWasCalled As Boolean
    Private _GetPickupBagOrCashDropBagIdParameter As Integer
    Private _TestPickupBag As Pickup

    Friend Overrides Function GetPickupBagOrCashDrop(ByVal BagId As Integer) As Pickup

        _GetPickupBagOrCashDropWasCalled = True
        GetPickupBagOrCashDropBagIdParameter = BagId
        _TestPickupBag = New Pickup
        _TestPickupBag.PickupComment = "Test Pickup Comment"

        Return _TestPickupBag
    End Function

    Friend ReadOnly Property GetPickupBagOrCashDropWasCalled() As Boolean
        Get
            Return _GetPickupBagOrCashDropWasCalled
        End Get
    End Property

    Friend Property GetPickupBagOrCashDropBagIdParameter() As Integer
        Get
            Return _GetPickupBagOrCashDropBagIdParameter
        End Get
        Set(ByVal value As Integer)
            _GetPickupBagOrCashDropBagIdParameter = value
        End Set
    End Property

    Friend ReadOnly Property TestPickupBag() As Pickup
        Get
            Return _TestPickupBag
        End Get
    End Property
End Class
