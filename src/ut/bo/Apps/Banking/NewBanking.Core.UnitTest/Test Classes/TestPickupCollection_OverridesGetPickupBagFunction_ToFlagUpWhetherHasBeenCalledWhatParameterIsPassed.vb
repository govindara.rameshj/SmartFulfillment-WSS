﻿Friend Class TestPickupCollection_OverridesGetPickupBagFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
    Inherits PickupCollection

    Private _GetPickupBagWasCalled As Boolean
    Private _GetPickupBagBagIdParameter As Integer
    Private _TestPickupBag As Pickup

    Friend Overrides Function GetPickupBag(ByVal BagId As Integer) As Pickup

        _GetPickupBagWasCalled = True
        GetPickupBagBagIdParameter = BagId
        _TestPickupBag = New Pickup
        _TestPickupBag.PickupComment = "Test Pickup Comment"

        Return _TestPickupBag
    End Function

    Friend ReadOnly Property GetPickupBagWasCalled() As Boolean
        Get
            Return _GetPickupBagWasCalled
        End Get
    End Property

    Friend Property GetPickupBagBagIdParameter() As Integer
        Get
            Return _GetPickupBagBagIdParameter
        End Get
        Set(ByVal value As Integer)
            _GetPickupBagBagIdParameter = value
        End Set
    End Property

    Friend ReadOnly Property TestPickupBag() As Pickup
        Get
            Return _TestPickupBag
        End Get
    End Property
End Class
