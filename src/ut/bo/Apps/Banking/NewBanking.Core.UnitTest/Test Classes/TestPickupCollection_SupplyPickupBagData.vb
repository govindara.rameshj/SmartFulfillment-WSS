﻿Friend Class TestPickupCollection_SupplyPickupBagData
    Inherits PickupCollection

    Private _TestPickupBag As Pickup
    Friend _TestPickupBagID As Integer

    Public Sub New()

        MyBase.New()
        GenerateTestData()
    End Sub

    Private Sub GenerateTestData()

        PopulateTestPickupBag()
        PopulatePickupCollection()
    End Sub

    Public ReadOnly Property TestPickupBag() As NewBanking.Core.Pickup
        Get
            Return _testPickupBag
        End Get
    End Property

    Private Sub PopulateTestPickupBag()

        _TestPickupBag = New Pickup
        _TestPickupBagID = 1
        If _TestPickupBag IsNot Nothing Then
            With _TestPickupBag
                .PickupID = _TestPickupBagID
                .PickupComment = "Test Pickup Comment"
                .PickupDate = Today
                .PickupPeriodID = 1367
                .PickupSealNumber = "11111111"
                .PickupValue = 100D
            End With
        End If
    End Sub

    Private Sub PopulatePickupCollection()

        Me.Items.Add(_TestPickupBag)
        For NewBag As Integer = 2 To 6
            Me.Items.Add(GenerateAnotherPickupBag(NewBag))
        Next
    End Sub

    Private Function GenerateAnotherPickupBag(ByVal NewBagSeed As Integer) As Pickup

        GenerateAnotherPickupBag = New Pickup
        With GenerateAnotherPickupBag
            .PickupID = NewBagSeed
            .PickupComment = "Test Pickup Comment " & NewBagSeed.ToString
            .PickupDate = Today
            .PickupPeriodID = 1367
            .PickupSealNumber = New String(NewBagSeed.ToString.ToCharArray()(0), 8)
            .PickupValue = NewBagSeed * 111.11D
        End With
    End Function
End Class
