﻿Friend Class TestActiveCashierListCollection_SupplyPickupBagAndCashDropTestData
    Inherits ActiveCashierListCollection

    Private _TestActiveCashier As ActiveCashierList
    Private _TestPickupBag As Pickup
    Private _TestCashDrop As Pickup
    Friend _TestPickupBagID As Integer
    Friend _TestCashDropID As Integer

    Public Sub New()

        MyBase.New()
        GenerateTestData()
    End Sub

    Private Sub GenerateTestData()

        PopulateTestPickupBag()
        PopulateTestCashDrop()
        PopulateTestActiveCashier()
        PopulateActiveCashierListCollection()
    End Sub

    Public ReadOnly Property TestPickupBag() As NewBanking.Core.Pickup
        Get
            Return _testPickupBag
        End Get
    End Property

    Public ReadOnly Property TestCashDrop() As Pickup
        Get
            Return _TestCashDrop
        End Get
    End Property

    Private Sub PopulateTestPickupBag()

        _TestPickupBag = New Pickup
        _TestPickupBagID = 1
        If _TestPickupBag IsNot Nothing Then
            With _TestPickupBag
                .PickupID = _TestPickupBagID
                .PickupComment = "Test Pickup Comment"
                .PickupDate = Today
                .PickupPeriodID = 1367
                .PickupSealNumber = "11111111"
                .PickupValue = 100D
            End With
        End If
    End Sub

    Private Sub PopulateTestCashDrop()

        _TestCashDrop = New Pickup
        _TestCashDropID = 2
        If _TestCashDrop IsNot Nothing Then
            With _TestCashDrop
                .PickupID = _TestCashDropID
                .PickupComment = "Test Cash Drop Comment"
                .PickupDate = Today
                .PickupPeriodID = 1367
                .PickupSealNumber = "22222222"
                .PickupValue = 200D
            End With
        End If
    End Sub

    Private Sub PopulateTestActiveCashier()

        _TestActiveCashier = New ActiveCashierList
        If _TestActiveCashier IsNot Nothing Then
            With _TestActiveCashier
                .EndOfDayPickupList = New NewBanking.Core.PickupCollection
                .EndOfDayPickupList.Add(_TestPickupBag)
                .CashDropsPickupList = New NewBanking.Core.PickupCollection
                .CashDropsPickupList.Add(_TestCashDrop)
            End With
        End If
    End Sub

    Private Sub PopulateActiveCashierListCollection()

        Me.Items.Add(_TestActiveCashier)
    End Sub
End Class
