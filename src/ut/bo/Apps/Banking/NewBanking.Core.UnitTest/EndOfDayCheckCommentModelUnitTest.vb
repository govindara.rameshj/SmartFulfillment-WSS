﻿<TestClass()> Public Class EndOfDayCheckCommentModelUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub RequirementCheck_ParameterFalse_ReturnSafeCommentModel()

        Dim V As ISafeCommentModel

        V = (New SafeCommentModelFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.SafeCommentModel", V.GetType.FullName)

    End Sub

#End Region

#Region "Properties"

    <TestMethod()> Public Sub PropertyIDGet()

        Dim Model As New SafeCommentModel

        Model.ID = 1

        Assert.AreEqual(1, Model.ID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckBagModel_PropertyProcessIDGet()

        Dim Model As New SafeCommentModel

        Model.ProcessID = 222

        Assert.AreEqual(222, Model.ProcessID)

    End Sub

    <TestMethod()> Public Sub PropertyPeriodIDGet()

        Dim Model As New SafeCommentModel

        Model.PeriodID = 2

        Assert.AreEqual(2, Model.PeriodID)

    End Sub

    <TestMethod()> Public Sub PropertyBagTypeGet()

        Dim Model As New SafeCommentModel

        Model.BagType = "P"

        Assert.AreEqual("P", Model.BagType)

    End Sub

    <TestMethod()> Public Sub PropertyCommentGet()

        Dim Model As New SafeCommentModel

        Model.Comment = "Comment"

        Assert.AreEqual("Comment", Model.Comment)

    End Sub

#End Region

End Class