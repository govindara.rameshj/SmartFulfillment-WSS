﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class CompletePickupAvailability_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Requirement Test"

    <TestMethod()> _
    Public Sub RequirementCheck_ParameterFalse_ReturnExistingBusinessObject()
        Dim V As ICompletePickupAvailability

        ArrangeRequirementSwitchDependency(False)
        V = (New CompletePickupAvailabilityImplementationFactory).GetImplementation
        Assert.AreEqual("NewBanking.Core.CompletePickupAvailabilityLive", V.GetType.FullName)
    End Sub

    <TestMethod()> _
    Public Sub RequirementCheck_ParameterTrue_ReturnNewBusinessObject()
        Dim V As ICompletePickupAvailability

        ArrangeRequirementSwitchDependency(True)
        V = (New CompletePickupAvailabilityImplementationFactory).GetImplementation
        Assert.AreEqual("NewBanking.Core.CompletePickupAvailabilityAlsoCheckIsLoggedOn", V.GetType.FullName)
    End Sub
#End Region

#Region "Unit Tests - CompletePickupAvailabilityLive"

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = True
        Assert.IsTrue(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = True
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = False
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = False
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsTrue()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Assert.IsTrue(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = True
        Assert.IsTrue(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = True
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = False
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = False
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsTrue()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Assert.IsTrue(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityLive

        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub
#End Region

#Region "Unit Tests - CompletePickupAvailabilityAlsoCheckIsLoggedOn"

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Assert.IsTrue(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsTrue()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Assert.IsTrue(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = (New CompletePickupAvailabilityImplementationFactory).GetImplementation

        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsTrue_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, True))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Assert.IsTrue(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsTrue()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Assert.IsTrue(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsFalseAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.PickupID = 1
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsTrue()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.LoggedOnTillId = "01"
        Assert.IsTrue(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasNoValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsTrueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = True
        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_FloatedPickup_SelectedPickupIDHasValueAndSelectedSaleTakenIsFalseAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New FloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.SaleTaken = False
        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsTrue()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.LoggedOnTillId = "01"
        Assert.IsTrue(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasNoValueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = (New CompletePickupAvailabilityImplementationFactory).GetImplementation

        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsTrueAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, True, False))
    End Sub

    <TestMethod()> _
    Public Sub IsAvailable_UnFloatedPickup_SelectedPickupIDHasValueAndCurrentAvailabilityIsFalseAndIsLoggedOnIsTrueAndCheckCashierIsLoggedOnIsFalse_IsAvailableReturnsFalse()
        Dim Selected As New UnFloatedPickupList
        Dim V As ICompletePickupAvailability = New CompletePickupAvailabilityAlsoCheckIsLoggedOn

        Selected.PickupID = 1
        Selected.LoggedOnTillId = "01"
        Assert.IsFalse(V.IsAvailable(Selected, False, False))
    End Sub
#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()
    End Sub
#End Region
End Class
