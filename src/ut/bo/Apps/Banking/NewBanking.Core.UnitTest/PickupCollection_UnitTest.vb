﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class PickupCollection_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "GetBag Tests"

    <TestMethod()> _
    Public Sub GetBag_Calls_GetPickupBag()
        Dim TestPickup As New TestPickupCollection_OverridesGetPickupBagFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
        Dim Expected As Boolean = True

        With TestPickup
            .GetBag(1)
            Assert.AreEqual(Expected, .GetPickupBagWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBag_Calls_GetPickupBagOrCashDrop_Passing_BagId_Parameter()
        Dim TestPickup As New TestPickupCollection_OverridesGetPickupBagFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
        Dim Expected As Integer = 343434

        With TestPickup
            .GetBag(Expected)
            Assert.AreEqual(Expected, .GetPickupBagBagIdParameter)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBag_WhenCallToGetPickupBagOrCashDropIsCalledItReturnsNothing_ReturnsNothing()
        Dim TestPickup As New TestPickupCollection_OverridesGetPickupBagFunction_ToReturnNothing
        Dim Expected As Pickup = Nothing

        With TestPickup
            Assert.AreEqual(Expected, .GetBag(1))
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBag_ReturnsCorrectPickupBagFromEndOfDayPickupBagList()
        Dim TestPickup As New TestPickupCollection_SupplyPickupBagData
        Dim Expected As Pickup
        Dim Actual As Pickup

        With TestPickup
            Actual = .GetBag(._TestPickupBagID)
            Expected = .TestPickupBag
        End With
        Assert.AreSame(Expected, Actual)
    End Sub
#End Region
End Class
