﻿<TestClass()> Public Class FloatedPickupUI_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Requirement Test"

    <TestMethod()> Public Sub RequirementCheck_ParameterFalse_ReturnExistingBusinessObject()

        Dim V As IFloatedPickupUI

        ArrangeRequirementSwitchDependency(False)

        V = (New FloatedPickupUIFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.FloatedPickupUIReassignButtonActive", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementCheck_ParameterTrue_ReturnNewBusinessObject()

        Dim V As IFloatedPickupUI

        ArrangeRequirementSwitchDependency(True)

        V = (New FloatedPickupUIFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.FloatedPickupUIReassignButtonInActive", V.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - ButtonActive"

    <TestMethod()> Public Sub FunctionReassignButtonActive_RequirementCheckFalse_ReturnTrue()

        Dim V As IFloatedPickupUI

        ArrangeRequirementSwitchDependency(False)

        V = (New FloatedPickupUIFactory).GetImplementation

        Assert.IsTrue(V.ReassignButtonActive())

    End Sub

    <TestMethod()> Public Sub FunctionReassignButtonActive_RequirementCheckTrue_ReturnFalse()

        Dim V As IFloatedPickupUI

        ArrangeRequirementSwitchDependency(True)

        V = (New FloatedPickupUIFactory).GetImplementation

        Assert.IsFalse(V.ReassignButtonActive())

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

End Class