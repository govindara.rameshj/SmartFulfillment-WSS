﻿<TestClass()> Public Class EndOfDayCheckRepositoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestConnection
        Inherits Connection

        Protected Overrides Sub SetDataProvider()

            MyBase.DataProviderField = Cts.Oasys.Data.DataProvider.Sql

        End Sub

        Protected Overrides Sub InitialiseConnection()

            MyBase.SqlConnectionField = New SqlConnection

        End Sub

    End Class

    Private Class TestRepository
        Inherits EndOfDayCheckRepository

        Friend _LocalConnection As Connection
        Friend _ResetCalled As Boolean
        Friend _SetConnectionCalled As Boolean
        Friend _SetPeriodCalled As Boolean
        Friend _SetBagCalled As Boolean
        Friend _SetCommentCalled As Boolean
        Friend _SetSaveCalled As Boolean
        Friend _SetSealNumberCalled As Boolean

        Friend _ExecuteGetSummaryCalled As Boolean
        Friend _ExecuteGetBankingBagExistInSafeCalled As Boolean
        Friend _ExecuteGetPickupBagExistInSafeCalled As Boolean

        Friend _ExecuteSaveBagCalled As Boolean
        Friend _ExecuteSaveCommentCalled As Boolean
        Friend _ExecuteSaveSafeCalled As Boolean
        Friend _ExecuteValidateSealNumberCalled As Boolean

        Public Sub New()

        End Sub

        Public Sub New(ByRef Con As Connection)

            _LocalConnection = Con

        End Sub

        Friend Overrides Sub Reset()

            _ResetCalled = True

        End Sub

        Friend Overrides Sub SetConnection(ByRef Con As Connection)

            _SetConnectionCalled = True

            If Con Is Nothing Then

                MyBase.SetConnection(_LocalConnection)

            Else

                MyBase.SetConnection(Con)

            End If

        End Sub

        Friend Overrides Sub SetPeriodID(ByVal Value As Integer)

            _SetPeriodCalled = True

        End Sub

        Friend Overrides Sub SetBags(ByRef Bags As ISafeBagScannedModel)

            _SetBagCalled = True
            MyBase.SetBags(Bags)

        End Sub

        Friend Overrides Sub SetComments(ByRef Comments As ISafeCommentModel)

            _SetCommentCalled = True

        End Sub

        Friend Overrides Sub SetSafe(ByRef Safe As ISafeModel)

            _SetSaveCalled = True

        End Sub

        Friend Overrides Sub SetSealNumber(ByVal SealNumber As String)

            _SetSealNumberCalled = True

        End Sub

        Friend Overrides Sub ExecuteGetSummary()

            _ExecuteGetSummaryCalled = True

        End Sub

        Friend Overrides Sub ExecuteGetBankingBagExistInSafe()

            _ExecuteGetBankingBagExistInSafeCalled = True

        End Sub

        Friend Overrides Sub ExecuteGetPickupBagExistInSafe()

            _ExecuteGetPickupBagExistInSafeCalled = True

        End Sub

        Friend Overrides Sub ExecuteSaveBag()

            _ExecuteSaveBagCalled = True

            MyBase.ExecuteSaveBag()

        End Sub

        Friend Overrides Sub ExecuteSaveBagCommand()

            'prevent actual execution

        End Sub

        Friend Overrides Sub ExecuteSaveComment()

            _ExecuteSaveCommentCalled = True

        End Sub

        Friend Overrides Sub ExecuteSaveSafe()

            _ExecuteSaveSafeCalled = True

        End Sub

        Friend Overrides Sub ExecuteValidateSealNumber()

            _ExecuteValidateSealNumberCalled = True

        End Sub

    End Class

#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub RequirementCheck_ReturnEndOfDayCheckRepository()

        Dim V As IEndOfDayCheckRepository

        V = (New EndOfDayCheckRepositoryFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.EndOfDayCheckRepository", V.GetType.FullName)

    End Sub

#End Region

#Region "EndOfDayCheckRepository | Interface | Function | Get Summary"

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetSummary_ResetCalled()

        Dim Repository As New TestRepository

        Repository.GetSummary(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ResetCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetSummary_SetConnectionCalled()

        Dim Repository As New TestRepository

        Repository.GetSummary(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetConnectionCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetSummary_SetPeriodCalled()

        Dim Repository As New TestRepository

        Repository.GetSummary(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetPeriodCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetSummary_ExecuteGetSummaryCalled()

        Dim Repository As New TestRepository

        Repository.GetSummary(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ExecuteGetSummaryCalled)

    End Sub

    'TDD ExecuteGetSummary, ConfigureGetSummaryCommand, ExecuteGetSummaryCommand

#End Region

#Region "EndOfDayCheckRepository | Interface | Function | Get Banking Bag Exist In Safe"

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetBankingBagExistInSafe_ResetCalled()

        Dim Repository As New TestRepository

        Repository.GetBankingBagExistInSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ResetCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetBankingBagExistInSafe_SetConnectionCalled()

        Dim Repository As New TestRepository

        Repository.GetBankingBagExistInSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetConnectionCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetBankingBagExistInSafe_SetPeriodCalled()

        Dim Repository As New TestRepository

        Repository.GetBankingBagExistInSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetPeriodCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetBankingBagExistInSafe_ExecuteGetBankingBagExistInSafeCalled()

        Dim Repository As New TestRepository

        Repository.GetBankingBagExistInSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ExecuteGetBankingBagExistInSafeCalled)

    End Sub

    'TDD ExecuteGetPhysicalBankingBags, ConfigureGetPhysicalBankingBagsCommand, ExecuteGetPhysicalBankingBagsCommand

#End Region

#Region "EndOfDayCheckRepository | Interface | Function | Get Pickup Bag Exist In Safe"

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetPickupBagExistInSafe_ResetCalled()

        Dim Repository As New TestRepository

        Repository.GetPickupBagExistInSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ResetCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetPickupBagExistInSafe_SetConnectionCalled()

        Dim Repository As New TestRepository

        Repository.GetPickupBagExistInSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetConnectionCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetPickupBagExistInSafe_SetPeriodCalled()

        Dim Repository As New TestRepository

        Repository.GetPickupBagExistInSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetPeriodCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_GetPickupBagExistInSafe_ExecuteGetPickupBagExistInSafeCalled()

        Dim Repository As New TestRepository

        Repository.GetPickupBagExistInSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ExecuteGetPickupBagExistInSafeCalled)

    End Sub

    'TDD ExecuteGetPhysicalPickupBags, ConfigureGetPhysicalPickupBagsCommand, ExecuteGetPhysicalPickupBagsCommand

#End Region

#Region "EndOfDayCheckRepository | Interface | Function | Safe Bags Scanned Persist"

    <TestMethod()> Public Sub EndOfDayCheckRepository_SafeBagsScannedPersist_ResetCalled()

        Dim Repository As New TestRepository

        Repository.SafeBagsScannedPersist(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ResetCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SafeBagsScannedPersist_SetConnectionCalled()

        Dim Repository As New TestRepository

        Repository.SafeBagsScannedPersist(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetConnectionCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SafeBagsScannedPersist_SetBagsCalled()

        Dim Repository As New TestRepository

        Repository.SafeBagsScannedPersist(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetBagCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SafeBagsScannedPersist_ExecuteSaveBagCalled()

        Dim Repository As New TestRepository

        Repository.SafeBagsScannedPersist(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ExecuteSaveBagCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_ConfigureSaveBagCommand_SixParametersPassed()

        Dim Repository As New TestRepository(New TestConnection)

        Repository.SafeBagsScannedPersist(New TestConnection, New SafeBagScannedModel)

        Assert.AreEqual(6, Repository._Command.ParameterCollectionCount)

    End Sub

    'TDD ExecuteSaveBag, ConfigureSaveBagCommand, ExecuteSaveBagCommand

#End Region

#Region "EndOfDayCheckRepository | Interface | Function | Safe Comment Persist"

    <TestMethod()> Public Sub EndOfDayCheckRepository_SafeCommentPersist_ResetCalled()

        Dim Repository As New TestRepository

        Repository.SafeCommentPersist(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ResetCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SafeCommentPersist_SetConnectionCalled()

        Dim Repository As New TestRepository

        Repository.SafeCommentPersist(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetConnectionCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SafeCommentPersist_SetCommentsCalled()

        Dim Repository As New TestRepository

        Repository.SafeCommentPersist(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetCommentCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SafeCommentPersist_ExecuteSaveCommentCalled()

        Dim Repository As New TestRepository

        Repository.SafeCommentPersist(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ExecuteSaveCommentCalled)

    End Sub

    'TDD ExecuteSaveComment, ConfigureSaveCommentCommand, ExecuteSaveCommentCommand

#End Region

#Region "EndOfDayCheckRepository | Interface | Function | Persist Safe"

    <TestMethod()> Public Sub EndOfDayCheckRepository_PersistSafe_ResetCalled()

        Dim Repository As New TestRepository

        Repository.PersistSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ResetCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_PersistSafe_SetConnectionCalled()

        Dim Repository As New TestRepository

        Repository.PersistSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetConnectionCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_PersistSafe_SetSaveCalled()

        Dim Repository As New TestRepository

        Repository.PersistSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._SetSaveCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_PersistSafe_ExecuteSaveSafeCalled()

        Dim Repository As New TestRepository

        Repository.PersistSafe(New TestConnection, Nothing)

        Assert.IsTrue(Repository._ExecuteSaveSafeCalled)

    End Sub

    'TDD ExecuteSaveSafe, ConfigureSaveSafeCommand, ExecuteSaveSafeCommand

#End Region

#Region "EndOfDayCheckRepository | Interface | Function | Validate Seal Number"

    <TestMethod()> Public Sub EndOfDayCheckRepository_ValidateSealNumber_ResetCalled()

        Dim Repository As New TestRepository(New TestConnection)

        Repository.ValidateSealNumber(Nothing)

        Assert.IsTrue(Repository._ResetCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_ValidateSealNumber_SetConnectionCalled()

        Dim Repository As New TestRepository(New TestConnection)

        Repository.ValidateSealNumber(Nothing)

        Assert.IsTrue(Repository._SetConnectionCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_ValidateSealNumber_SetSealCalled()

        Dim Repository As New TestRepository(New TestConnection)

        Repository.ValidateSealNumber(Nothing)

        Assert.IsTrue(Repository._SetSealNumberCalled)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_ValidateSealNumber_ExecuteValidateSealNumberCalled()

        Dim Repository As New TestRepository(New TestConnection)

        Repository.ValidateSealNumber(Nothing)

        Assert.IsTrue(Repository._ExecuteValidateSealNumberCalled)

    End Sub

    'TDD ExecuteValidateSealNumber, ConfigureValidateSealNumberCommand, ExecuteValidateSealNumberCommand

#End Region

#Region "Internal | Procedures"

    <TestMethod()> Public Sub EndOfDayCheckRepository_Reset_SetConnectionToNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository._Connection = New TestConnection

        Repository.Reset()

        Assert.IsNull(Repository._Connection)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_Reset_SetCommandToNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository._Command = New Command(New TestConnection)

        Repository.Reset()

        Assert.IsNull(Repository._Command)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_Reset_SetDataTableToNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository._DT = New DataTable

        Repository.Reset()

        Assert.IsNull(Repository._DT)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_Reset_SetPeriodIDToNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository._PeriodID = 10

        Repository.Reset()

        Assert.AreEqual(0, Repository._PeriodID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_Reset_SetBagsToNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository._Bag = New SafeBagScannedModel

        Repository.Reset()

        Assert.IsNull(Repository._Bag)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_Reset_SetCommetsToNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository._Comment = New SafeCommentModel

        Repository.Reset()

        Assert.IsNull(Repository._Comment)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_Reset_SetSafeToNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository._Safe = New SafeModel

        Repository.Reset()

        Assert.IsNull(Repository._Safe)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_Reset_SetSealToNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository._SealNumber = "abcde"

        Repository.Reset()

        Assert.IsNull(Repository._SealNumber)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SetPeriodID_ReturnTen()

        Dim Repository As New EndOfDayCheckRepository

        Repository.SetPeriodID(10)

        Assert.AreEqual(10, Repository._PeriodID)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SetBags_NotNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository.SetBags(New SafeBagScannedModel)

        Assert.IsNotNull(Repository._Bag)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SetComments_NotNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository.SetComments(New SafeCommentModel)

        Assert.IsNotNull(Repository._Comment)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SetSafe_NotNothing()

        Dim Repository As New EndOfDayCheckRepository

        Repository.SetSafe(New SafeModel)

        Assert.IsNotNull(Repository._Safe)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SetSealABCDDE_ReturnABCDDE()

        Dim Repository As New EndOfDayCheckRepository

        Repository.SetSealNumber("ABCDE")

        Assert.AreEqual("ABCDE", Repository._SealNumber)

    End Sub

    <TestMethod()> Public Sub EndOfDayCheckRepository_SetSealNothing_ReturnEmptyString()

        Dim Repository As New EndOfDayCheckRepository

        Repository.SetSealNumber(Nothing)

        Assert.AreEqual(String.Empty, Repository._SealNumber)

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

End Class