﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class ActiveCashierList_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "GetBag Tests"

    <TestMethod()> _
    Public Sub GetBag_Calls_GetPickupBagOrCashDrop()
        Dim TestActiveCashierList As New TestActiveCashierList_OverridesGetPickupBagOrCashDropFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
        Dim Expected As Boolean = True

        With TestActiveCashierList
            .GetBag(1)
            Assert.AreEqual(Expected, .GetPickupBagOrCashDropWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBag_Calls_GetPickupBagOrCashDrop_Passing_BagId_Parameter()
        Dim TestActiveCashierList As New TestActiveCashierList_OverridesGetPickupBagOrCashDropFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
        Dim Expected As Integer = 343434

        With TestActiveCashierList
            .GetBag(Expected)
            Assert.AreEqual(Expected, .GetPickupBagOrCashDropBagIdParameter)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBag_WhenCallToGetPickupBagOrCashDropIsCalledItReturnsNothing_ReturnsNothing()
        Dim TestActiveCashierList As New TestActiveCashierList_OverridesGetPickupBagOrCashDropFunction_ToReturnNothing
        Dim Expected As Pickup = Nothing

        With TestActiveCashierList
            Assert.AreEqual(Expected, .GetBag(1))
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBag_ReturnsCorrectPickupBagFromEndOfDayPickupBagList()
        Dim TestActiveCashierList As New TestActiveCashierList_SupplyPickupBagAndCashDropTestData
        Dim Expected As Pickup
        Dim Actual As Pickup

        With TestActiveCashierList
            Actual = .GetBag(._TestPickupBagID)
            Expected = .TestPickupBag
        End With
        Assert.AreSame(Expected, Actual)
    End Sub

    <TestMethod()> _
    Public Sub GetBag_ReturnsCorrectCashDropFromCaashDropList()
        Dim TestActiveCashierList As New TestActiveCashierList_SupplyPickupBagAndCashDropTestData
        Dim Expected As Pickup
        Dim Actual As Pickup

        With TestActiveCashierList
            Actual = .GetBag(._TestCashDropID)
            Expected = .TestCashDrop
        End With
        Assert.AreSame(Expected, Actual)
    End Sub
#End Region

#Region "GetBagComment Tests"

    <TestMethod()> _
    Public Sub GetBagComment_Calls_GetPickupBagOrCashDrop()
        Dim TestActiveCashierList As New TestActiveCashierList_OverridesGetPickupBagOrCashDropFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
        Dim Expected As Boolean = True

        With TestActiveCashierList
            .GetBagComment(1)
            Assert.AreEqual(Expected, .GetPickupBagOrCashDropWasCalled)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBagComment_Calls_GetPickupBagOrCashDrop_Passing_BagId_Parameter()
        Dim TestActiveCashierList As New TestActiveCashierList_OverridesGetPickupBagOrCashDropFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
        Dim Expected As Integer = 343434

        With TestActiveCashierList
            .GetBagComment(Expected)
            Assert.AreEqual(Expected, .GetPickupBagOrCashDropBagIdParameter)
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBagComment_WhenCallToGetPickupBagOrCashDropIsCalledItReturnsNothing_ReturnsEmptyString()
        Dim TestActiveCashierList As New TestActiveCashierList_OverridesGetPickupBagOrCashDropFunction_ToReturnNothing
        Dim Expected As String = ""

        With TestActiveCashierList
            Assert.AreEqual(Expected, .GetBagComment(1))
        End With
    End Sub

    <TestMethod()> _
    Public Sub GetBagComment_WhenCallToGetPickupBagOrCashDropWasCalledReturnsSomething_ReturnsPickupCommentFieldFromWhatWasReturnedByGetBag()
        Dim TestActiveCashierList As New TestActiveCashierList_OverridesGetPickupBagOrCashDropFunction_ToFlagUpWhetherHasBeenCalledWhatParameterIsPassed
        Dim Expected As String
        Dim Actual As String

        With TestActiveCashierList
            Actual = .GetBagComment(1)
            Expected = .TestPickupBag.PickupComment
        End With
        Assert.AreEqual(Expected, Actual)
    End Sub
#End Region
End Class
