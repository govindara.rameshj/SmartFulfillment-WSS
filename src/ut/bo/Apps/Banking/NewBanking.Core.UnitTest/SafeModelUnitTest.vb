﻿<TestClass()> Public Class SafeModelUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub RequirementCheck_ParameterFalse_ReturnEndOfDayCheckEmptyViewModel()

        Dim V As ISafeModel

        V = (New SafeModelFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.SafeModel", V.GetType.FullName)

    End Sub

#End Region

#Region "Properties"

    <TestMethod()> Public Sub PropertyPeriodIDGet()

        Dim Model As New SafeModel

        Model.PeriodID = 2

        Assert.AreEqual(2, Model.PeriodID)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckDoneGet()

        Dim Model As New SafeModel

        Model.EndOfDayCheckDone = True

        Assert.IsTrue(Model.EndOfDayCheckDone)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckLockedFromGet()

        Dim Model As New SafeModel

        Model.EndOfDayCheckLockedFrom = Now.Date.AddDays(5)

        Assert.AreEqual(Now.Date.AddDays(5), Model.EndOfDayCheckLockedFrom)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckLockedFromGet_Null()

        Dim Model As New SafeModel

        Assert.IsFalse(Model.EndOfDayCheckLockedFrom.HasValue)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckLockedToGet()

        Dim Model As New SafeModel

        Model.EndOfDayCheckLockedTo = Now.Date.AddDays(7)

        Assert.AreEqual(Now.Date.AddDays(7), Model.EndOfDayCheckLockedTo)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckLockedToGet_Null()

        Dim Model As New SafeModel

        Assert.IsFalse(Model.EndOfDayCheckLockedTo.HasValue)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckManagerIDGet()

        Dim Model As New SafeModel

        Model.EndOfDayCheckManagerID = 888

        Assert.AreEqual(888, Model.EndOfDayCheckManagerID)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckManagerIDGet_Null()

        Dim Model As New SafeModel

        Assert.IsFalse(Model.EndOfDayCheckManagerID.HasValue)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckWitnessIDGet()

        Dim Model As New SafeModel

        Model.EndOfDayCheckWitnessID = 999

        Assert.AreEqual(999, Model.EndOfDayCheckWitnessID)

    End Sub

    <TestMethod()> Public Sub PropertyEndOfDayCheckWitnessIDGet_Null()

        Dim Model As New SafeModel

        Assert.IsFalse(Model.EndOfDayCheckWitnessID.HasValue)

    End Sub

#End Region

End Class