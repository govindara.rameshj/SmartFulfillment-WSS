﻿<TestClass()> Public Class BagScanningEngineUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestModel
        Inherits BagScanningEngineModel

        Friend _SealNumberBagID As Integer

        Friend _SetPeriodIDCalled As Boolean
        Friend _SetProcessIDCalled As Boolean

        Friend _SetSealNumberCalled As Boolean
        Friend _SealNumberBagIDCalled As Boolean
        Friend _ValidSealCalled As Boolean

        Friend _GetPickupBagExistInSafeDataCalled As Boolean
        Friend _GetBankingBagExistInSafeDataCalled As Boolean

        Friend _CheckPickupBagExistInSafeCalled As Boolean
        Friend _CheckBankingBagExistInSafeCalled As Boolean

        Public Sub New()

        End Sub

        Public Sub New(ByVal SealNumberBagID As Integer)

            _SealNumberBagID = SealNumberBagID

        End Sub

        Friend Overrides Sub SetPeriodID(ByVal PeriodID As Integer)

            _SetPeriodIDCalled = True

            MyBase.SetPeriodID(PeriodID)

        End Sub

        Friend Overrides Sub SetProcessID(ByVal ProcessID As IBagScanningEngine.BusinessProcess)

            _SetProcessIDCalled = True

            MyBase.SetProcessID(ProcessID)

        End Sub

        Friend Overrides Sub SetSealNumber(ByVal SealNumber As String)

            _SetSealNumberCalled = True

            MyBase.SetSealNumber(SealNumber)

        End Sub

        Friend Overrides Function SealNumberBagID() As Integer

            _SealNumberBagIDCalled = True

            Return _SealNumberBagID

        End Function

        Friend Overrides Function ValidSeal() As Boolean

            _ValidSealCalled = True

            Return MyBase.ValidSeal()

        End Function

        Friend Overrides Sub GetPickupBagExistInSafeData()

            _GetPickupBagExistInSafeDataCalled = True

        End Sub

        Friend Overrides Sub GetBankingBagExistInSafeData()

            _GetBankingBagExistInSafeDataCalled = True

        End Sub

        Friend Overrides Function CheckPickupBagExistInSafe() As Boolean

            _CheckPickupBagExistInSafeCalled = True

            Return MyBase.CheckPickupBagExistInSafe()

        End Function

        Friend Overrides Function CheckBankingBagExistInSafe() As Boolean

            _CheckBankingBagExistInSafeCalled = True

            Return MyBase.CheckBankingBagExistInSafe()

        End Function



        Friend Overrides Function GetBagModelImplementation() As ISafeBagScannedModel

            Return New SafeBagScannedModel

        End Function

        Friend Overrides Function GetCommentModelImplementation() As ISafeCommentModel

            Return New SafeCommentModel

        End Function

        Friend Overrides Function GetRepository() As IEndOfDayCheckRepository

            Return New EndOfDayCheckRepository

        End Function

        Friend Overrides Function ValidateSealNumber(ByVal Repository As IEndOfDayCheckRepository) As Integer

            Return MyBase.ValidateSealNumber(Repository)

        End Function

    End Class

#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub RequirementCheck_ReturnBagScanningEngineModel()

        Dim V As IBagScanningEngine

        V = (New BagScanningEngineFactory).GetImplementation

        Assert.AreEqual("NewBanking.Core.BagScanningEngineModel", V.GetType.FullName)

    End Sub

#End Region

#Region "BagScanningEngineModel"

#Region "BagScanningEngineModel | Interface | Procedure | Initialise"

    <TestMethod()> Public Sub BagScanningEngineModel_Initialise_SetPeriodIDCalled()

        Dim Model As New TestModel

        Model.Initialise(Nothing, Nothing)

        Assert.IsTrue(Model._SetPeriodIDCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_Initialise_PeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, Nothing)

        Assert.AreEqual(1350, Model._PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_Initialise_SetProcessIDCalled()

        Dim Model As New TestModel

        Model.Initialise(Nothing, Nothing)

        Assert.IsTrue(Model._SetProcessIDCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_Initialise_ProcessID()

        Dim Model As New TestModel

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Assert.AreEqual(IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess, Model._ProcessID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_Initialise_EndOfDayCheckProcess_SafeCreated()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Assert.IsNotNull(Model._Safe)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_Initialise_SafeMaintenanceProcess_SafeNotCreated()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.SafeMaintenanceProcess)

        Assert.IsNull(Model._Safe)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | End Of Day Check Additional Information"

    <TestMethod()> Public Sub BagScanningEngineModel_EndOfDayCheckAdditionalInformation_CheckDone()

        Dim Model As New TestModel

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.EndOfDayCheckAdditionalInformation(True, Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(Model._Safe.EndOfDayCheckDone)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_EndOfDayCheckAdditionalInformation_LockedFrom()

        Dim Model As New TestModel
        Dim SpecificDate As Date

        SpecificDate = Now.Date.AddDays(3).AddHours(7).AddMinutes(25).AddMilliseconds(374)

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.EndOfDayCheckAdditionalInformation(Nothing, SpecificDate, Nothing, Nothing, Nothing)

        Assert.AreEqual(SpecificDate, Model._Safe.EndOfDayCheckLockedFrom)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_EndOfDayCheckAdditionalInformation_LockedTo()

        Dim Model As New TestModel
        Dim SpecificDate As Date

        SpecificDate = Now.Date.AddDays(3).AddHours(9).AddMinutes(43).AddMilliseconds(789)

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.EndOfDayCheckAdditionalInformation(Nothing, Nothing, SpecificDate, 123, 456)

        Assert.AreEqual(SpecificDate, Model._Safe.EndOfDayCheckLockedTo)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_EndOfDayCheckAdditionalInformation_ManagerID()

        Dim Model As New TestModel

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.EndOfDayCheckAdditionalInformation(Nothing, Nothing, Nothing, 123, Nothing)

        Assert.AreEqual(123, Model._Safe.EndOfDayCheckManagerID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_EndOfDayCheckAdditionalInformation_WitnessID()

        Dim Model As New TestModel

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.EndOfDayCheckAdditionalInformation(Nothing, Nothing, Nothing, Nothing, 456)

        Assert.AreEqual(456, Model._Safe.EndOfDayCheckWitnessID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_EndOfDayCheckAdditionalInformation_IfSafeMaintenanceThenSafeIsNothing()

        Dim Model As New TestModel

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.SafeMaintenanceProcess)
        Model.EndOfDayCheckAdditionalInformation(Nothing, Nothing, Nothing, Nothing, 456)

        Assert.IsNull(Model._Safe)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Create Pickup Bag"

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_SetSealNumberCalled()

        Dim Model As New TestModel

        Model.CreatePickupBag(Nothing)

        Assert.IsTrue(Model._SetSealNumberCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_SealNumberBagIDCalled()

        Dim Model As New TestModel

        Model.CreatePickupBag(Nothing)

        Assert.IsTrue(Model._SealNumberBagIDCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_BagTypeP()

        Dim Model As New TestModel

        Model.CreatePickupBag(Nothing)

        Assert.AreEqual("P", Model._PickupBagList.Item(0).BagType)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_ValidFirstRowSealNumber()

        Dim Model As New TestModel

        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("22222222")

        Assert.AreEqual("11111111", Model._PickupBagList.Item(0).SealNumber)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_ValidSecondRowSealNumber()

        Dim Model As New TestModel

        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("22222222")

        Assert.AreEqual("22222222", Model._PickupBagList.Item(1).SealNumber)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_ValidFirstRowSafeBagsID()

        Dim Model As New TestModel

        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("22222222")

        Assert.AreEqual(0, Model._PickupBagList.Item(0).SafeBagsID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_ValidSecondRowSafeBagsID()

        Dim Model As New TestModel

        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("22222222")

        Assert.AreEqual(0, Model._PickupBagList.Item(1).SafeBagsID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_SafeNotCreated_ValidFirstRowPeriodID()

        Dim Model As New TestModel

        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("22222222")

        Assert.AreEqual(0, Model._PickupBagList.Item(0).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_SafeNotCreated_ValidSecondRowPeriodID()

        Dim Model As New TestModel

        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("22222222")

        Assert.AreEqual(0, Model._PickupBagList.Item(1).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_SafeCreated_ValidFirstRowPeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("22222222")

        Assert.AreEqual(1350, Model._PickupBagList.Item(0).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_SafeCreated_ValidSecondRowPeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("22222222")

        Assert.AreEqual(1350, Model._PickupBagList.Item(1).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_ValidFirstRowComment()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreatePickupBag(Nothing, "Comment A")
        Model.CreatePickupBag(Nothing, "Comment B")

        Assert.AreEqual("Comment A", Model._PickupBagList.Item(0).Comment)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupBag_ValidSecondRowComment()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreatePickupBag(Nothing, "Comment A")
        Model.CreatePickupBag(Nothing, "Comment B")

        Assert.AreEqual("Comment B", Model._PickupBagList.Item(1).Comment)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Pickup Bag List"

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagList_ReturnCorrectClass()

        Dim Model As New TestModel
        Dim PickupList As New List(Of ISafeBagScannedModel)

        Assert.AreEqual(PickupList.GetType.FullName, Model.PickupBagList.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagList_ReturnListFourItems()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("11111111")
        Model.CreatePickupBag("11111111")

        Assert.AreEqual(4, Model.PickupBagList.Count)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Create Banking Bag"

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_SetSealNumberCalled()

        Dim Model As New TestModel

        Model.CreateBankingBag(Nothing)

        Assert.IsTrue(Model._SetSealNumberCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_SealNumberBagIDCalled()

        Dim Model As New TestModel

        Model.CreateBankingBag(Nothing)

        Assert.IsTrue(Model._SealNumberBagIDCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_BagTypeB()

        Dim Model As New TestModel

        Model.CreateBankingBag(Nothing)

        Assert.AreEqual("B", Model._BankingBagList.Item(0).BagType)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_ValidFirstRowSealNumber()

        Dim Model As New TestModel

        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("555-55555555-5")

        Assert.AreEqual("444-44444444-4", Model._BankingBagList.Item(0).SealNumber)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_ValidSecondRowSealNumber()

        Dim Model As New TestModel

        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("555-55555555-5")

        Assert.AreEqual("555-55555555-5", Model._BankingBagList.Item(1).SealNumber)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_ValidFirstRowSafeBagsID()

        Dim Model As New TestModel

        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("555-55555555-5")

        Assert.AreEqual(0, Model._BankingBagList.Item(0).SafeBagsID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_ValidSecondRowSafeBagsID()

        Dim Model As New TestModel

        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("555-55555555-5")

        Assert.AreEqual(0, Model._BankingBagList.Item(1).SafeBagsID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_SafeNotCreated_ValidFirstRowPeriodID()

        Dim Model As New TestModel

        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("555-55555555-5")

        Assert.AreEqual(0, Model._BankingBagList.Item(0).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_SafeNotCreated_ValidSecondRowPeriodID()

        Dim Model As New TestModel

        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("555-55555555-5")

        Assert.AreEqual(0, Model._BankingBagList.Item(1).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_SafeCreated_ValidFirstRowPeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("555-55555555-5")

        Assert.AreEqual(1350, Model._BankingBagList.Item(0).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_SafeCreated_ValidSecondRowPeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("555-55555555-5")

        Assert.AreEqual(1350, Model._BankingBagList.Item(1).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_ValidFirstRowComment()

        Dim Model As New TestModel

        Model.CreateBankingBag(Nothing, "Comment A")
        Model.CreateBankingBag(Nothing, "Comment B")

        Assert.AreEqual("Comment A", Model._BankingBagList.Item(0).Comment)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingBag_ValidSecondRowComment()

        Dim Model As New TestModel

        Model.CreateBankingBag(Nothing, "Comment A")
        Model.CreateBankingBag(Nothing, "Comment B")

        Assert.AreEqual("Comment B", Model._BankingBagList.Item(1).Comment)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Banking Bag List"

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagList_ReturnCorrectClass()

        Dim Model As New TestModel
        Dim BankingList As New List(Of ISafeBagScannedModel)

        Assert.AreEqual(BankingList.GetType.FullName, Model.BankingBagList.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagList_ReturnListFiveItems()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("444-44444444-4")
        Model.CreateBankingBag("444-44444444-4")

        Assert.AreEqual(5, Model.BankingBagList.Count)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Create Pickup Comment"

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupComment_ValidFirstRowComment()

        Dim Model As New TestModel

        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment B")

        Assert.AreEqual("Pickup Comment A", Model._PickupCommentList.Item(0).Comment)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupComment_ValidSecondRowComment()

        Dim Model As New TestModel

        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment B")

        Assert.AreEqual("Pickup Comment B", Model._PickupCommentList.Item(1).Comment)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupComment_ValidFirstRowBagType()

        Dim Model As New TestModel

        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment B")

        Assert.AreEqual("P", Model._PickupCommentList.Item(0).BagType)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupComment_ValidSecondRowBagType()

        Dim Model As New TestModel

        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment B")

        Assert.AreEqual("P", Model._PickupCommentList.Item(1).BagType)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupComment_SafeNotCreated_ValidFirstRowPeriodID()

        Dim Model As New TestModel

        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment B")

        Assert.AreEqual(0, Model._PickupCommentList.Item(0).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupComment_SafeNotCreated_ValidSecondRowPeriodID()

        Dim Model As New TestModel

        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment B")

        Assert.AreEqual(0, Model._PickupCommentList.Item(1).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupComment_SafeCreated_ValidFirstRowPeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment B")

        Assert.AreEqual(1350, Model._PickupCommentList.Item(0).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreatePickupComment_SafeCreated_ValidSecondRowPeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment B")

        Assert.AreEqual(1350, Model._PickupCommentList.Item(1).PeriodID)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Pickup Comment List"

    <TestMethod()> Public Sub BagScanningEngineModel_PickupCommentList_ReturnCorrectClass()

        Dim Model As New TestModel
        Dim PickupCommentList As New List(Of ISafeCommentModel)

        Assert.AreEqual(PickupCommentList.GetType.FullName, Model.PickupCommentList.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupCommentList_ReturnListSixItems()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment A")
        Model.CreatePickupComment("Pickup Comment A")

        Assert.AreEqual(6, Model.PickupCommentList.Count)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Create Banking Comment"

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingComment_ValidFirstRowComment()

        Dim Model As New TestModel

        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment B")

        Assert.AreEqual("Banking Comment A", Model._BankingCommentList.Item(0).Comment)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingComment_ValidSecondRowComment()

        Dim Model As New TestModel

        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment B")

        Assert.AreEqual("Banking Comment B", Model._BankingCommentList.Item(1).Comment)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingComment_ValidFirstRowBagType()

        Dim Model As New TestModel

        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment B")

        Assert.AreEqual("B", Model._BankingCommentList.Item(0).BagType)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingComment_ValidSecondRowBagType()

        Dim Model As New TestModel

        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment B")

        Assert.AreEqual("B", Model._BankingCommentList.Item(1).BagType)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingComment_SafeNotCreated_ValidFirstRowPeriodID()

        Dim Model As New TestModel

        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment B")

        Assert.AreEqual(0, Model._BankingCommentList.Item(0).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingComment_SafeNotCreated_ValidSecondRowPeriodID()

        Dim Model As New TestModel

        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment B")

        Assert.AreEqual(0, Model._BankingCommentList.Item(1).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingComment_SafeCreated_ValidFirstRowPeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment B")

        Assert.AreEqual(1350, Model._BankingCommentList.Item(0).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateBankingComment_SafeCreated_ValidSecondRowPeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment B")

        Assert.AreEqual(1350, Model._BankingCommentList.Item(1).PeriodID)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Banking Comment List"

    <TestMethod()> Public Sub BagScanningEngineModel_BankingCommentList_ReturnCorrectClass()

        Dim Model As New TestModel

        Dim BankingCommentList As New List(Of ISafeCommentModel)

        Assert.AreEqual(BankingCommentList.GetType.FullName, Model.BankingCommentList.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingCommentList_ReturnListSevenItems()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment A")
        Model.CreateBankingComment("Banking Comment A")

        Assert.AreEqual(7, Model.BankingCommentList.Count)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Pickup Bag Already Scanned"

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagAlreadyScanned_ListEmpty_ReturnFalse()

        Dim Model As New BagScanningEngineModel

        Assert.IsFalse(Model.PickupBagAlreadyScanned("ABCDEFGHI"))

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagAlreadyScanned_ListNotEmpty_NoMatchExist_ReturnFalse()

        Dim Model As New BagScanningEngineModel

        Model._PickupBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "12345678", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "23456789", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "91234567", "P"))

        Assert.IsFalse(Model.PickupBagAlreadyScanned("ABCDEFGHI"))

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagAlreadyScanned_ListNotEmpty_MatchExist_ReturnTrue()

        Dim Model As New BagScanningEngineModel

        Model._PickupBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "12345678", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "23456789", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "91234567", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(4, 1350, 1237, "ABCDEFGHI", "P"))

        Assert.IsTrue(Model.PickupBagAlreadyScanned("ABCDEFGHI"))

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Banking Bag Already Scanned"

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagAlreadyScanned_ListEmpty_ReturnFalse()

        Dim Model As New BagScanningEngineModel

        Assert.IsFalse(Model.BankingBagAlreadyScanned("111-11111111-1"))

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagAlreadyScanned_ListNotEmpty_NoMatchExist_ReturnFalse()

        Dim Model As New BagScanningEngineModel

        Model._BankingBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "201-34666920-7", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "201-34666921-4", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "226-40573690-4", "B"))

        Assert.IsFalse(Model.BankingBagAlreadyScanned("111-11111111-1"))

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagAlreadyScanned_ListNotEmpty_MatchExist_ReturnTrue()

        Dim Model As New BagScanningEngineModel

        Model._BankingBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "201-34666920-7", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "201-34666921-4", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "226-40573690-4", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1237, "111-11111111-1", "B"))

        Assert.IsTrue(Model.BankingBagAlreadyScanned("111-11111111-1"))

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Pickup Bag Has Valid Seal"

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagHasValidSeal_SetSealNumberCalled()

        Dim Model As New TestModel

        Model.PickupBagHasValidSeal(Nothing)

        Assert.IsTrue(Model._SetSealNumberCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagHasValidSeal_ValidSealCalled()

        Dim Model As New TestModel

        Model.PickupBagHasValidSeal(Nothing)

        Assert.IsTrue(Model._ValidSealCalled)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Banking Bag Has Valid Seal"

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagHasValidSeal_SetSealNumberCalled()

        Dim Model As New TestModel

        Model.BankingBagHasValidSeal(Nothing)

        Assert.IsTrue(Model._SetSealNumberCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagHasValidSeal_ValidSealCalled()

        Dim Model As New TestModel

        Model.BankingBagHasValidSeal(Nothing)

        Assert.IsTrue(Model._ValidSealCalled)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Pickup Bag Exist In Safe"

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagExistInSafe_SetSealNumberCalled()

        Dim Model As New TestModel

        Model.PickupBagExistInSafe(Nothing)

        Assert.IsTrue(Model._SetSealNumberCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagExistInSafe_GetPickupBagExistInSafeDataCalled()

        Dim Model As New TestModel

        Model.PickupBagExistInSafe(Nothing)

        Assert.IsTrue(Model._GetPickupBagExistInSafeDataCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagExistInSafe_CheckPickupBagExistInSafeCalled()

        Dim Model As New TestModel

        Model.PickupBagExistInSafe(Nothing)

        Assert.IsTrue(Model._CheckPickupBagExistInSafeCalled)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Banking Bag Exist In Safe"

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagExistInSafe_SetSealNumberCalled()

        Dim Model As New TestModel

        Model.BankingBagExistInSafe(Nothing)

        Assert.IsTrue(Model._SetSealNumberCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagExistInSafe_GetBankingBagExistInSafeDataCalled()

        Dim Model As New TestModel

        Model.BankingBagExistInSafe(Nothing)

        Assert.IsTrue(Model._GetBankingBagExistInSafeDataCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagExistInSafe_CheckBankingBagExistInSafeCalled()

        Dim Model As New TestModel

        Model.BankingBagExistInSafe(Nothing)

        Assert.IsTrue(Model._CheckBankingBagExistInSafeCalled)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Match System Pickup Expectation"

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemPickupExpectation_GetPickupBagExistInSafeDataCalled()

        Dim Model As New TestModel

        Model.MatchSystemPickupExpectation()

        Assert.IsTrue(Model._GetPickupBagExistInSafeDataCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemPickupExpectation_ScannedBagsZero_SystemBagsNull_ReturnTrue()

        Dim Model As New TestModel

        Assert.IsTrue(Model.MatchSystemPickupExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemPickupExpectation_ScannedBagsNonZero_SystemBagsNull_ReturnFalse()

        Dim Model As New TestModel

        Model._PickupBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "12345678", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "23456789", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "91234567", "P"))

        Assert.IsFalse(Model.MatchSystemPickupExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemPickupExpectation_ScannedBagsZero_SystemBagsZero_ReturnTrue()

        Dim Model As New TestModel

        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)

        Assert.IsTrue(Model.MatchSystemPickupExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemPickupExpectation_ScannedBagsNonZero_SystemBagsZero_ReturnFalse()

        Dim Model As New TestModel

        Model._PickupBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "12345678", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "23456789", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "91234567", "P"))

        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)

        Assert.IsFalse(Model.MatchSystemPickupExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemPickupExpectation_ScannedBagsZero_SystemBagsNonZero_ReturnFalse()

        Dim Model As New TestModel

        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(1, "12345678", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(2, "23456789", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(3, "91234567", "P", "S", 123.435))

        Assert.IsFalse(Model.MatchSystemPickupExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemPickupExpectation_ScannedBagsSystemBagsCountMatch_SealMatch_ReturnTrue()

        Dim Model As New TestModel

        Model._PickupBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "12345678", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "23456789", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "91234567", "P"))

        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(1, "12345678", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(2, "23456789", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(3, "91234567", "P", "S", 123.435))

        Assert.IsTrue(Model.MatchSystemPickupExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemPickupExpectation_ScannedBagsSystemBagsCountMatch_SealNonMatch_ReturnFalse()

        Dim Model As New TestModel

        Model._PickupBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "12345678", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "23456789", "P"))
        Model._PickupBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "91234567", "P"))

        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(1, "12345678", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(2, "99999999", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(3, "91234567", "P", "S", 123.435))

        Assert.IsFalse(Model.MatchSystemPickupExpectation)

    End Sub

#End Region

#Region "BagScanningEngineModel | Interface | Procedure | Match System Banking Expectation"

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemBankingExpectation_GetBankingBagExistInSafeDataCalled()

        Dim Model As New TestModel

        Model.MatchSystemBankingExpectation()

        Assert.IsTrue(Model._GetBankingBagExistInSafeDataCalled)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemBankingExpectation_ScannedBagsZero_SystemBagsNull_ReturnTrue()

        Dim Model As New TestModel

        Assert.IsTrue(Model.MatchSystemBankingExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemBankingExpectation_ScannedBagsNonZero_SystemBagsNull_ReturnFalse()

        Dim Model As New TestModel

        Model._BankingBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "201-34666920-7", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "201-34666921-4", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "226-40573690-4", "B"))

        Assert.IsFalse(Model.MatchSystemBankingExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemBankingExpectation_ScannedBagsZero_SystemBagsZero_ReturnTrue()

        Dim Model As New TestModel

        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)

        Assert.IsTrue(Model.MatchSystemBankingExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemBankingExpectation_ScannedBagsNonZero_SystemBagsZero_ReturnFalse()

        Dim Model As New TestModel

        Model._BankingBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "201-34666920-7", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "201-34666921-4", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "226-40573690-4", "B"))

        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)

        Assert.IsFalse(Model.MatchSystemBankingExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemBankingExpectation_ScannedBagsZero_SystemBagsNonZero_ReturnFalse()

        Dim Model As New TestModel

        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(1, "201-34666920-7", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(2, "201-34666921-4", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(3, "226-40573690-4", "B", "M", 123.435))

        Assert.IsFalse(Model.MatchSystemBankingExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemBankingExpectation_ScannedBagsSystemBagsCountMatch_SealMatch_ReturnTrue()

        Dim Model As New TestModel

        Model._BankingBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "201-34666920-7", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "201-34666921-4", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "226-40573690-4", "B"))

        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(1, "201-34666920-7", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(2, "201-34666921-4", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(3, "226-40573690-4", "B", "M", 123.435))

        Assert.IsTrue(Model.MatchSystemBankingExpectation)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_MatchSystemBankingExpectation_ScannedBagsSystemBagsCountMatch_SealNonMatch_ReturnFalse()

        Dim Model As New TestModel

        Model._BankingBagList.Add(CreateEndOfDayCheckModel(1, 1350, 1234, "201-34666920-7", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(2, 1350, 1235, "201-34666921-4", "B"))
        Model._BankingBagList.Add(CreateEndOfDayCheckModel(3, 1350, 1236, "226-40573690-4", "B"))

        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(1, "201-34666920-7", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(2, "111-11111111-1", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(3, "226-40573690-4", "B", "M", 123.435))

        Assert.IsFalse(Model.MatchSystemBankingExpectation)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | CurrentPeriodID"

    <TestMethod()> Public Sub BagScanningEngineModel_CurrentPeriodID_SafeNotCreatedReturnZero()

        Dim Model As New TestModel

        Assert.AreEqual(0, Model.CurrentPeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CurrentPeriodID_SafeCreatedReturnNonZero()

        Dim Model As New TestModel

        Model.Initialise(1234, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Assert.AreEqual(1234, Model.CurrentPeriodID)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | CreateSafeModel"

    <TestMethod()> Public Sub BagScanningEngineModel_CreateSafeModel_SafeNotCreated()

        Dim Model As New TestModel

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.SafeMaintenanceProcess)
        Model.CreateSafeModel()

        Assert.IsNull(Model._Safe)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CreateSafeModel_SafeCreated()

        Dim Model As New TestModel

        Model.Initialise(Nothing, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        Model.CreateSafeModel()

        Assert.IsNotNull(Model._Safe)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | Update Pickup Bag Properties"

    <TestMethod()> Public Sub BagScanningEngineModel_UpdatePickupBagProperties_PeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Model.CreatePickupBag("123456")
        Model.CreatePickupBag("234567")
        Model.CreatePickupBag("345678")
        Model.CreatePickupBag("456789")

        Model.UpdatePickupBagProperties()

        Assert.AreEqual(1350, Model._PickupBagList.Item(3).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_UpdatePickupBagProperties_ProcessID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Model.CreatePickupBag("123456")
        Model.CreatePickupBag("234567")
        Model.CreatePickupBag("345678")
        Model.CreatePickupBag("456789")

        Model.UpdatePickupBagProperties()

        Assert.AreEqual(IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess, CType(Model._PickupBagList.Item(3).ProcessID, IBagScanningEngine.BusinessProcess))

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | Update Pickup Comment Properties"

    <TestMethod()> Public Sub BagScanningEngineModel_UpdatePickupCommentProperties_PeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Model.CreatePickupComment("comment a")
        Model.CreatePickupComment("comment b")
        Model.CreatePickupComment("comment c")
        Model.CreatePickupComment("comment d")

        Model.UpdatePickupCommentProperties()

        Assert.AreEqual(1350, Model._PickupCommentList.Item(3).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_UpdatePickupCommentProperties_ProcessID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Model.CreatePickupComment("comment a")
        Model.CreatePickupComment("comment b")
        Model.CreatePickupComment("comment c")
        Model.CreatePickupComment("comment d")

        Model.UpdatePickupCommentProperties()

        Assert.AreEqual(IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess, CType(Model._PickupCommentList.Item(3).ProcessID, IBagScanningEngine.BusinessProcess))

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | Update Banking Bag Properties"

    <TestMethod()> Public Sub BagScanningEngineModel_UpdateBankingBagProperties_PeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Model.CreateBankingBag("123456")
        Model.CreateBankingBag("234567")
        Model.CreateBankingBag("345678")
        Model.CreateBankingBag("456789")

        Model.UpdateBankingBagProperties()

        Assert.AreEqual(1350, Model._BankingBagList.Item(3).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_UpdateBankingBagProperties_ProcessID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Model.CreateBankingBag("123456")
        Model.CreateBankingBag("234567")
        Model.CreateBankingBag("345678")
        Model.CreateBankingBag("456789")

        Model.UpdateBankingBagProperties()

        Assert.AreEqual(IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess, CType(Model._BankingBagList.Item(3).ProcessID, IBagScanningEngine.BusinessProcess))

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | Update Banking Comment Properties"

    <TestMethod()> Public Sub BagScanningEngineModel_UpdateBankingCommentListPeriodID_PeriodID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Model.CreateBankingComment("comment a")
        Model.CreateBankingComment("comment b")
        Model.CreateBankingComment("comment c")
        Model.CreateBankingComment("comment d")

        Model.UpdateBankingCommentProperties()

        Assert.AreEqual(1350, Model._BankingCommentList.Item(3).PeriodID)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_UpdateBankingCommentListPeriodID_ProcessID()

        Dim Model As New TestModel

        Model.Initialise(1350, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)

        Model.CreateBankingComment("comment a")
        Model.CreateBankingComment("comment b")
        Model.CreateBankingComment("comment c")
        Model.CreateBankingComment("comment d")

        Model.UpdateBankingCommentProperties()

        Assert.AreEqual(IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess, CType(Model._BankingCommentList.Item(3).ProcessID, IBagScanningEngine.BusinessProcess))

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | Safe Is Nothing"

    <TestMethod()> Public Sub BagScanningEngineModel_SafeIsNothing_SafeExist_ReturnFalse()

        Dim Model As New TestModel

        Model._Safe = New SafeModel

        Assert.IsFalse(Model.SafeIsNothing)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_SafeIsNothing_SafeNotExist_ReturnTrue()

        Dim Model As New TestModel

        Assert.IsTrue(Model.SafeIsNothing)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | Set Seal Number"

    <TestMethod()> Public Sub BagScanningEngineModel_SetSealNumber_ReturnABCDE()

        Dim Model As New TestModel

        Model.SetSealNumber("ABCDE")

        Assert.AreEqual("ABCDE", Model._SealNumber)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Procedure | Set Period ID"

    <TestMethod()> Public Sub BagScanningEngineModel_SetPeriodID_ReturnTen()

        Dim Model As New TestModel

        Model.SetPeriodID(10)

        Assert.AreEqual(10, Model._PeriodID)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Function | Banking Bag Exist In Safe Data Exist"

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagExistInSafeDataExist_DataExists_ReturnTrue()

        Dim Model As New TestModel

        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)
        Model._BankingBagExistInSafeList.Add(New SafeBagModel)
        Model._BankingBagExistInSafeList.Add(New SafeBagModel)

        Assert.IsTrue(Model.BankingBagExistInSafeDataExist)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagExistInSafeDataExist_DataEmpty_ReturnTrue()

        Dim Model As New TestModel

        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)

        Assert.IsFalse(Model.BankingBagExistInSafeDataExist)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_BankingBagExistInSafeDataExist_DataNull_ReturnFalse()

        Dim Model As New TestModel

        Model._BankingBagExistInSafeList = Nothing

        Assert.IsFalse(Model.BankingBagExistInSafeDataExist)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Function | Pickup Bag Exist In Safe Data Exist"

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagExistInSafeDataExist_DataExists_ReturnTrue()

        Dim Model As New TestModel

        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)
        Model._PickupBagExistInSafeList.Add(New SafeBagModel)
        Model._PickupBagExistInSafeList.Add(New SafeBagModel)

        Assert.IsTrue(Model.PickupBagExistInSafeDataExist)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagExistInSafeDataExist_DataEmpty_ReturnTrue()

        Dim Model As New TestModel

        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)

        Assert.IsFalse(Model.PickupBagExistInSafeDataExist)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_PickupBagExistInSafeDataExist_DataNull_ReturnFalse()

        Dim Model As New TestModel

        Model._PickupBagExistInSafeList = Nothing

        Assert.IsFalse(Model.PickupBagExistInSafeDataExist)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Function | Check Banking Bag Exist In Safe"

    <TestMethod()> Public Sub BagScanningEngineModel_CheckBankingBagExistInSafe_Match_ReturnTrue()

        Dim Model As New TestModel

        Model._SealNumber = "201-34666921-4"
        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(1, "201-34666920-7", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(2, "201-34666921-4", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(3, "226-40573690-4", "B", "M", 123.435))

        Assert.IsTrue(Model.CheckBankingBagExistInSafe)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CheckBankingBagExistInSafe_DataExists_NotMatched_ReturnFalse()

        Dim Model As New TestModel

        Model._SealNumber = "XXX-XXXXXXXX-X"
        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(1, "201-34666920-7", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(2, "201-34666921-4", "B", "M", 123.435))
        Model._BankingBagExistInSafeList.Add(CreateSafeBagModel(3, "226-40573690-4", "B", "M", 123.435))

        Assert.IsFalse(Model.CheckBankingBagExistInSafe)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CheckBankingBagExistInSafe_DataEmpty_NotMatched_ReturnFalse()

        Dim Model As New TestModel

        Model._SealNumber = "12345678"
        Model._BankingBagExistInSafeList = New List(Of ISafeBagModel)

        Assert.IsFalse(Model.CheckBankingBagExistInSafe)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CheckBankingBagExistInSafe_DataNull_NotMatched_ReturnFalse()

        Dim Model As New TestModel

        Model._SealNumber = "12345678"
        Model._BankingBagExistInSafeList = Nothing

        Assert.IsFalse(Model.CheckBankingBagExistInSafe)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Function | Check Pickup Bag Exist In Safe"

    <TestMethod()> Public Sub BagScanningEngineModel_CheckPickupBagExistInSafe_ReturnTrue()

        Dim Model As New TestModel

        Model._SealNumber = "12345678"
        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(1, "91234567", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(2, "12345678", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(3, "23456789", "P", "S", 123.435))

        Assert.IsTrue(Model.CheckPickupBagExistInSafe)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CheckPickupBagExistInSafe_DataExists_NotMatched_ReturnFalse()

        Dim Model As New TestModel

        Model._SealNumber = "99999999"
        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(1, "91234567", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(2, "12345678", "P", "S", 123.435))
        Model._PickupBagExistInSafeList.Add(CreateSafeBagModel(3, "23456789", "P", "S", 123.435))

        Assert.IsFalse(Model.CheckPickupBagExistInSafe)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CheckPickupBagExistInSafe_DataEmpty_NotMatched_ReturnFalse()

        Dim Model As New TestModel

        Model._SealNumber = "12345678"
        Model._PickupBagExistInSafeList = New List(Of ISafeBagModel)

        Assert.IsFalse(Model.CheckPickupBagExistInSafe)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_CheckPickupBagExistInSafe_DataNull_NotMatched_ReturnFalse()

        Dim Model As New TestModel

        Model._SealNumber = "12345678"
        Model._PickupBagExistInSafeList = Nothing

        Assert.IsFalse(Model.CheckPickupBagExistInSafe)

    End Sub

#End Region

#Region "BagScanningEngineModel | Internal | Function | Valid Seal"

    <TestMethod()> Public Sub BagScanningEngineModel_ValidSeal_BadSeal_ReturnFalse()

        Dim Model As New TestModel(0)

        Assert.IsFalse(Model.ValidSeal)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_ValidSeal_GoodSeal_ReturnTrue()

        Dim Model As New TestModel(10)

        Assert.IsTrue(Model.ValidSeal)

    End Sub

#End Region

#End Region

#Region "Integration Tests"

    <TestMethod()> Public Sub BagScanningEngineModel_EndOfDayCheckProcess_Persist()

        'Dim Engine As IBagScanningEngine
        'Dim LockedFrom As Date
        'Dim LockedTo As Date
        'Dim Temp As String

        'LockedFrom = New Date(2012, 8, 9, 20, 10, 0)
        'LockedTo = New Date(2012, 8, 10, 7, 10, 0)

        'Engine = (New BagScanningEngineFactory).GetImplementation

        'With Engine

        '    .Initialise(1336, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess)
        '    .EndOfDayCheckAdditionalInformation(True, LockedFrom, LockedTo, 9, 888)

        '    .CreatePickupBag("01682083")
        '    .CreatePickupBag("01674071")
        '    .CreatePickupBag("01674699")
        '    .CreatePickupBag("01673531")
        '    .CreatePickupBag("01674682")
        '    .CreatePickupBag("01674729")
        '    .CreatePickupBag("01673807")
        '    .CreatePickupBag("01673777")
        '    .CreatePickupBag("01673753")
        '    .CreatePickupBag("01674651")
        '    .CreatePickupBag("01673395")

        '    'pickup bag does exists in safe - no comment written
        '    If .PickupBagExistInSafe("01674736") = False Then

        '        .CreatePickupBag("01674736", "Good pickup seal. Not in safe - 01674736")

        '    Else

        '        .CreatePickupBag("01674736")

        '    End If

        '    'pickup bag does not exists in safe - comment written
        '    If .PickupBagExistInSafe("01395828") = False Then .CreatePickupBag("01395828", "Good pickup seal. Not in safe - 01395828 (5623)")

        '    .CreateBankingBag("201-34666920-7")
        '    .CreateBankingBag("201-34666921-4")
        '    .CreateBankingBag("226-40573690-4")

        '    'banking bag does exists in safe - no comment written
        '    If .BankingBagExistInSafe("201-34666922-1") = False Then

        '        .CreateBankingBag("201-34666922-1", "Good banking seal. Not in safe - 201-34666922-1")

        '    Else

        '        .CreateBankingBag("201-34666922-1")

        '    End If

        '    'banking bag does not exists in safe - comment written
        '    If .BankingBagExistInSafe("201-29564083-8") = False Then .CreateBankingBag("201-29564083-8", "Good banking seal. Not in safe - 201-29564083-8 (5634)")

        '    Temp = "ABCDEFGH"
        '    If .PickupBagHasValidSeal(Temp) = False Then .CreatePickupBag(Temp, "Bad pickup seal - " & Temp)
        '    Temp = "IJKLMNOP"
        '    If .PickupBagHasValidSeal(Temp) = False Then .CreatePickupBag(Temp, "Bad pickup seal - " & Temp)
        '    Temp = "QRSTUVWX"
        '    If .PickupBagHasValidSeal(Temp) = False Then .CreatePickupBag(Temp, "Bad pickup seal - " & Temp)
        '    Temp = "YZABCDEF"
        '    If .PickupBagHasValidSeal(Temp) = False Then .CreatePickupBag(Temp, "Bad pickup seal - " & Temp)
        '    Temp = "ABC-DEFGHIJK-L"
        '    If .BankingBagHasValidSeal("ABC-DEFGHIJK-L") = False Then .CreateBankingBag(Temp, "Bad banking seal - " & Temp)

        '    .CreatePickupComment("1350: Comment Six")
        '    .CreateBankingComment("1350: Comment Seven")
        '    .CreateBankingComment("1350: Comment Eight")

        'End With

        'Assert.IsTrue(Engine.EndOfDayCheckPersist)

    End Sub

    <TestMethod()> Public Sub BagScanningEngineModel_SafeMaintenanceProcess_Persist()

        'Dim Engine As IBagScanningEngine
        'Dim Temp As String

        'Engine = (New BagScanningEngineFactory).GetImplementation

        'With Engine

        '    .Initialise(1336, IBagScanningEngine.BusinessProcess.SafeMaintenanceProcess)

        '    .CreateBankingBag("201-34666920-7")
        '    .CreateBankingBag("201-34666921-4")
        '    .CreateBankingBag("226-40573690-4")

        '    'banking bag does exists in safe - no comment written
        '    If .BankingBagExistInSafe("201-34666922-1") = False Then

        '        .CreateBankingBag("201-34666922-1", "(Safe Maintenance) Good banking seal. Not in safe - 201-34666922-1")

        '    Else

        '        .CreateBankingBag("201-34666922-1")

        '    End If

        '    'banking bag does not exists in safe - comment written
        '    If .BankingBagExistInSafe("201-29564083-8") = False Then .CreateBankingBag("201-29564083-8", "(Safe Maintenance) Good banking seal. Not in safe - 201-29564083-8 (5634)")

        '    Temp = "ABC-DEFGHIJK-L"
        '    If .BankingBagHasValidSeal("ABC-DEFGHIJK-L") = False Then .CreateBankingBag(Temp, "(Safe Maintenance) Bad banking seal - " & Temp)

        '    .CreateBankingComment("(Safe Maintenance) banking bag mismatch")

        'End With





        'Dim OdbcConnection As clsOasys3DB
        'Dim Success As Boolean

        'CreateConnectionXmlFile()

        'OdbcConnection = New clsOasys3DB("Default", 0)

        'Try
        '    OdbcConnection.BeginTransaction()

        '    Engine.SafeMaintenancePersist(OdbcConnection)

        '    OdbcConnection.CommitTransaction()

        '    Success = True

        'Catch ex As Exception

        '    OdbcConnection.RollBackTransaction()

        '    Success = False

        'Finally

        '    OdbcConnection = Nothing

        'End Try


        ''2nd persist - overwrite first
        'OdbcConnection = New clsOasys3DB("Default", 0)

        'Try
        '    OdbcConnection.BeginTransaction()

        '    Engine.SafeMaintenancePersist(OdbcConnection)

        '    OdbcConnection.CommitTransaction()

        '    Success = True

        'Catch ex As Exception

        '    OdbcConnection.RollBackTransaction()

        '    Success = False

        'Finally

        '    OdbcConnection = Nothing

        'End Try

        'Assert.IsTrue(Success)

    End Sub

#End Region

#Region "Private Functions & Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

    Private Function CreateSafeBagModel(ByVal ID As Integer, _
                                        ByVal SealNumber As String, _
                                        ByVal BagType As String, _
                                        ByVal BagState As String, _
                                        ByVal Value As Double) As SafeBagModel

        Dim X As New SafeBagModel

        With X

            .ID = ID
            .SealNumber = SealNumber
            .BagType = BagType
            .BagState = BagState
            .Value = CType(Value, Decimal)

        End With

        Return X

    End Function

    Private Function CreateEndOfDayCheckModel(ByVal ID As Integer, _
                                              ByVal PeriodID As Integer, _
                                              ByVal SafeBagsID As Integer, _
                                              ByVal SealNumber As String, _
                                              ByVal BagType As String) As SafeBagScannedModel

        Dim X As New SafeBagScannedModel

        With X

            .ID = ID
            .PeriodID = PeriodID
            .SafeBagsID = SafeBagsID
            .SealNumber = SealNumber
            .BagType = BagType

        End With

        Return X

    End Function

#End Region

#Region "Private Functions And Procedures"

#End Region

End Class