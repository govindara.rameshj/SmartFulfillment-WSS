﻿<TestClass()> Public Class Validation

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Global"

    Private Enum ValidationType
        BankingSafeMaintenance
    End Enum

#End Region

#Region "Banking Safe Maintenance: Perform Check"

    <TestMethod()> Public Sub BankingSafeMaintenanceCheck_RequirementParameterMissing_ReturnTrue()
        Dim V As IValidation = Nothing

        Arrange(V, ValidationType.BankingSafeMaintenance, New System.Nullable(Of Boolean), 120, 8120, 8076)
        Assert.AreEqual(True, V.PerformCheck())
    End Sub

    <TestMethod()> Public Sub BankingSafeMaintenanceCheck_RequirementParameterFalse_ReturnTrue()
        Dim V As IValidation = Nothing

        Arrange(V, ValidationType.BankingSafeMaintenance, False, 120, 8120, 8076)
        Assert.AreEqual(True, V.PerformCheck())
    End Sub

    <TestMethod()> Public Sub BankingSafeMaintenanceCheck_RequirementParameterTrue_Zero_StoreID_ReturnTrue()
        Dim V As IValidation = Nothing

        Arrange(V, ValidationType.BankingSafeMaintenance, True, 0, 8120, 8076)
        Assert.AreEqual(True, V.PerformCheck())
    End Sub

    <TestMethod()> Public Sub BankingSafeMaintenanceCheck_RequirementParameterTrue_Empty_EStoreShopID_And_EStoreWarehouseID_ReturnTrue()
        Dim V As IValidation = Nothing

        Arrange(V, ValidationType.BankingSafeMaintenance, True, 1, 0, 0)
        Assert.AreEqual(True, V.PerformCheck())
    End Sub

    <TestMethod()> Public Sub BankingSafeMaintenanceCheck_RequirementParameterTrue_StoreID_Match_EStoreShopID_ReturnFalse()
        Dim V As IValidation = Nothing

        Arrange(V, ValidationType.BankingSafeMaintenance, True, 120, 8120, 0)
        Assert.AreEqual(False, V.PerformCheck())
    End Sub

    <TestMethod()> Public Sub BankingSafeMaintenanceCheck_RequirementParameterTrue_StoreID_Match_EStoreWarehouseID_ReturnFalse()
        Dim V As IValidation = Nothing

        Arrange(V, ValidationType.BankingSafeMaintenance, True, 76, 0, 8076)
        Assert.AreEqual(False, V.PerformCheck())
    End Sub

    <TestMethod()> Public Sub BankingSafeMaintenanceCheck_RequirementParameterTrue_StoreID_DoesNotMatch_EStoreShopID_And_EStoreWarehouseID_ReturnTrue()
        Dim V As IValidation = Nothing

        Arrange(V, ValidationType.BankingSafeMaintenance, True, 100, 8120, 8076)
        Assert.AreEqual(True, V.PerformCheck())
    End Sub

#End Region

#Region "Private Test Procedures And Functions"

    Private Sub Arrange(ByRef V As IValidation, ByVal VT As ValidationType, ByVal RequirementEnabled As System.Nullable(Of Boolean), ByVal StoreID As Integer, ByVal eStoreShopID As Integer, ByVal eStoreWarehouseID As Integer)

        Dim Stub As New ValidationRepositoryStub

        Stub.ConfigureStub(RequirementEnabled, StoreID, eStoreShopID, eStoreWarehouseID)

        Select Case VT
            Case ValidationType.BankingSafeMaintenance
                BankingSafeMaintenanceValidationRepositoryFactory.FactorySet(Stub)
                V = BankingSafeMaintenanceValidationFactory.FactoryGet
        End Select

    End Sub

#End Region

End Class
