﻿<TestClass()> Public Class BankingPeriodUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Requirement Factory"

    <TestMethod()> Public Sub BankingPeriodFactory_FunctionRequirementSwitch_P022024_SwitchOff_P022073_SwitchOff_ReturnFalse()

        Dim Factory As New BankingPeriodFactory

        Factory._SwitchP022024 = False
        Factory._SwitchP022073 = False

        Assert.IsFalse(Factory.RequirementSwitch)

    End Sub

    <TestMethod()> Public Sub BankingPeriodFactory_FunctionRequirementSwitch_P022024_SwitchOn_P022073_SwitchOff_ReturnTrue()

        Dim Factory As New BankingPeriodFactory

        Factory._SwitchP022024 = True
        Factory._SwitchP022073 = False

        Assert.IsTrue(Factory.RequirementSwitch)

    End Sub

    <TestMethod()> Public Sub BankingPeriodFactory_FunctionRequirementSwitch_P022024_SwitchOff_P022073_SwitchOn_ReturnTrue()

        Dim Factory As New BankingPeriodFactory

        Factory._SwitchP022024 = False
        Factory._SwitchP022073 = True

        Assert.IsTrue(Factory.RequirementSwitch)

    End Sub

    <TestMethod()> Public Sub BankingPeriodFactory_FunctionRequirementSwitch_P022024_SwitchOn_P022073_SwitchOn_ReturnTrue()

        Dim Factory As New BankingPeriodFactory

        Factory._SwitchP022024 = True
        Factory._SwitchP022073 = True

        Assert.IsTrue(Factory.RequirementSwitch)

    End Sub

    'FOLLOWING TESTS HITTING FILE SYSTEM

    <TestMethod()> Public Sub RequirementCheck_ParameterFalse_ReturnBankingPeriod()

        Dim V As IBankingPeriod

        ArrangeRequirementSwitchDependency(False)

        V = (New BankingPeriodFactory).GetImplementation

        Assert.AreEqual("Banking.Core.BankingPeriod", V.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementCheck_ParameterTrue_ReturnBankingPeriodSafeMaintenanceTransactionSafe()

        Dim V As IBankingPeriod

        ArrangeRequirementSwitchDependency(True)

        V = (New BankingPeriodFactory).GetImplementation

        Assert.AreEqual("Banking.Core.BankingPeriodSafeMaintenanceTransactionSafe", V.GetType.FullName)

    End Sub

#End Region


#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

End Class