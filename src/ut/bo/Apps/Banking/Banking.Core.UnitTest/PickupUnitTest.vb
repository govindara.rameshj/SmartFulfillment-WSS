﻿<TestClass()> Public Class PickupUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Requirement Switch"

    <TestMethod()> Public Sub PickupFactory_PickupConstructor_RealisticParamters_InternalFirstUserVariablePopulated()

        Dim Factory As New PickupFactory

        Factory.PickupConstructor(123, 456, "C")

        Assert.AreEqual(123, Factory._FirstUserID)

    End Sub

    <TestMethod()> Public Sub PickupFactory_PickupConstructor_RealisticParamters_InternalSecondUserVariablePopulated()

        Dim Factory As New PickupFactory

        Factory.PickupConstructor(123, 456, "C")

        Assert.AreEqual(456, Factory._SecondUserID)

    End Sub

    <TestMethod()> Public Sub PickupFactory_PickupConstructor_RealisticParamters_InternalAccountingModelVariablePopulated()

        Dim Factory As New PickupFactory

        Factory.PickupConstructor(123, 456, "C")

        Assert.AreEqual("C", Factory._AccountingModel)

    End Sub

    <TestMethod()> Public Sub PickupFactory_ParameterFalse_ReturnPickup_NoteIntegrationTest()

        Dim Pickup As IPickup
        Dim Factory As PickupFactory

        ArrangeRequirementSwitchDependency(False)

        Factory = New PickupFactory
        Factory.PickupConstructor(Nothing, Nothing, Nothing)
        Pickup = Factory.GetImplementation

        Assert.AreEqual("Banking.Core.Pickup", Pickup.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub PickupFactory_ParameterTrue_ReturnPickupPreserveCommentForCancelledBag_NoteIntegrationTest()

        Dim Pickup As IPickup
        Dim Factory As PickupFactory

        ArrangeRequirementSwitchDependency(True)

        Factory = New PickupFactory
        Factory.PickupConstructor(Nothing, Nothing, Nothing)
        Pickup = Factory.GetImplementation

        Assert.AreEqual("Banking.Core.PickupPreserveCommentForCancelledBag", Pickup.GetType.FullName)

    End Sub

#End Region

#Region "Piup & PickupPreserveCommentForCancelledBag Class(s)"

    <TestMethod()> <Ignore()> Public Sub Pickup_SetComments_WillOverWriteOriginalBagComments_NoteIntegrationTest()

        Dim Bag As Pickup

        Bag = New Pickup(Nothing, Nothing, Nothing)
        ConfigureOriginalPickupBag(CType(Bag, PickupPreserveCommentForCancelledBag), "Old Comment")

        Bag.SetComments("New Comment")

        Assert.AreNotEqual("Old Comment", Bag._originalBag.Comments.Value)

    End Sub

    <TestMethod()> Public Sub PickupPreserveCommentForCancelledBag_SetComments_WillNotOverWriteOriginalBagComments_NoteIntegrationTest()

        Dim Bag As PickupPreserveCommentForCancelledBag

        Bag = New PickupPreserveCommentForCancelledBag(Nothing, Nothing, Nothing)
        ConfigureOriginalPickupBag(Bag, "Old Comment")

        Bag.SetComments("New Comment")

        Assert.AreEqual("Old Comment", Bag._originalBag.Comments.Value)

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

    Private Sub ConfigureOriginalPickupBag(ByVal Bag As PickupPreserveCommentForCancelledBag, ByVal Comment As String)

        Bag._originalBag = New cSafeBags()
        Bag._originalBag.Comments.Value = Comment

    End Sub

#End Region

End Class