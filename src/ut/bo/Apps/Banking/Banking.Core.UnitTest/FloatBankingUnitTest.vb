﻿<TestClass()> Public Class FloatBankingUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Scenerio Class - FloatBankingAssignmentPreviousDayUseSelectedPeriodID"

    Private Class TestFloatBankingAssignmentPreviousDayUseSelectedPeriodID
        Inherits FloatBankingAssignmentSelectedPeriod

        Private _LocalDR As DataRow

        Public Sub New(ByVal CreateRow As Boolean, ByVal StartDate As Nullable(Of Date))

            ArrangeDataRow(CreateRow, StartDate)

        End Sub

        Friend Overrides Sub GetData()

            MyBase._DR = _LocalDR

        End Sub

        Private Sub ArrangeDataRow(ByVal CreateRow As Boolean, ByVal Value As Nullable(Of Date))

            If CreateRow = False Then Exit Sub

            Dim DT As DataTable

            DT = New DataTable

            DT.Columns.Add("ID", System.Type.GetType("System.Decimal"))
            DT.Columns.Add("StartDate", System.Type.GetType("System.DateTime"))
            DT.Columns.Add("EndDate", System.Type.GetType("System.DateTime"))
            DT.Columns.Add("IsClosed", System.Type.GetType("System.Boolean"))

            DT.Rows.Add(Nothing, Value, Nothing, Nothing)

            _LocalDR = DT.Rows(0)

        End Sub

    End Class

#End Region

#Region "Unit Test - Requirement Test"

    <TestMethod()> Public Sub RequirementCheck_ParameterFalse_ReturnExistingBusinessObject()

        Dim FB As IFloatBanking
        Dim Factory As IRequirementSwitchFactory(Of IFloatBanking)

        ArrangeRequirementSwitchDependency(False)

        Factory = New FloatBankingFactory
        FB = Factory.GetImplementation

        Assert.AreEqual("Banking.Core.FloatBankingAssignmentCurrentPeriod", FB.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementCheck_ParameterTrue_ReturnNewBusinessObject()

        Dim FB As IFloatBanking
        Dim Factory As IRequirementSwitchFactory(Of IFloatBanking)

        ArrangeRequirementSwitchDependency(True)

        Factory = New FloatBankingFactory
        FB = Factory.GetImplementation

        Assert.AreEqual("Banking.Core.FloatBankingAssignmentSelectedPeriod", FB.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - Float Banking - FloatBankingAssignmentCurrentPeriod"

    <TestMethod()> Public Sub FloatBankingAssignmentCurrentPeriod_FunctionOutPeriodID_ReturnCurrentPeriodID_1000()

        Dim FB As IFloatBanking

        FB = New FloatBankingAssignmentCurrentPeriod

        Assert.AreEqual(1000, FB.OutPeriodID(1000, 10))

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentCurrentPeriod_FunctionOutPeriodID_ReturnCurrentPeriodID_2000()

        Dim FB As IFloatBanking

        FB = New FloatBankingAssignmentCurrentPeriod

        Assert.AreEqual(2000, FB.OutPeriodID(2000, 10))

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentCurrentPeriod_FunctionOutDate_ReturnToday()

        Dim FB = New FloatBankingAssignmentCurrentPeriod
        Dim outDate = FB.OutDate(1000)
        Assert.AreEqual(Now.Date, outDate.Value.Date)

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentCurrentPeriod_FunctionOutDate_ReturnYesterday()

        Dim FB = New FloatBankingAssignmentCurrentPeriod
        Dim outDate = FB.OutDate(2000)
        Assert.AreEqual(Now.Date, outDate.Value.Date)

    End Sub

#End Region

#Region "Unit Test - Float Banking - FloatBankingAssignmentSelectedPeriod"

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionOutPeriodID_ReturnBankingPeriodID_1000()

        Dim FB As IFloatBanking

        FB = New FloatBankingAssignmentSelectedPeriod

        Assert.AreEqual(1000, FB.OutPeriodID(10, 1000))

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionOutPeriodID_ReturnBankingPeriodID_2000()

        Dim FB As IFloatBanking

        FB = New FloatBankingAssignmentSelectedPeriod

        Assert.AreEqual(2000, FB.OutPeriodID(10, 2000))

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionOutDate_InternalBankingPeriodIdReturn1234()

        Dim FB = New TestFloatBankingAssignmentPreviousDayUseSelectedPeriodID(False, Nothing)

        FB.OutDate(1234)

        Assert.AreEqual(1234, FB._BankingPeriodID)

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionOutDate_InternalDataRowDoesNotExist()

        Dim FB = New TestFloatBankingAssignmentPreviousDayUseSelectedPeriodID(False, Nothing)

        FB.OutDate(Nothing)

        Assert.IsNull(FB._DR)

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionOutDate_InternalDataRowExist()

        Dim FB = New TestFloatBankingAssignmentPreviousDayUseSelectedPeriodID(True, Now)

        FB.OutDate(1234)

        Assert.IsNotNull(FB._DR)

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionSystemPeriodStartDate_DataRowNothingReturnNothing()

        Dim FB = New FloatBankingAssignmentSelectedPeriod

        FB._DR = Nothing

        Assert.IsFalse(FB.SystemPeriodStartDate.HasValue)

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionSystemPeriodStartDate_DataRowStartDateNothingReturnNothing()

        Dim FB = New FloatBankingAssignmentSelectedPeriod

        FB._DR = ArrangeDataRow(Nothing)

        Assert.IsFalse(FB.SystemPeriodStartDate.HasValue)

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionOutDate_ReturnBankingPeriod20120326()

        Dim FB As IFloatBanking

        FB = New TestFloatBankingAssignmentPreviousDayUseSelectedPeriodID(True, New System.DateTime(2012, 3, 26))

        Assert.AreEqual(New System.DateTime(2012, 3, 26), FB.OutDate(1200))

    End Sub

    <TestMethod()> Public Sub FloatBankingAssignmentSelectedPeriod_FunctionOutDate_ReturnBankingPeriod20120704()

        Dim FB As IFloatBanking

        FB = New TestFloatBankingAssignmentPreviousDayUseSelectedPeriodID(True, New System.DateTime(2012, 7, 4))

        Assert.AreEqual(New System.DateTime(2012, 7, 4), FB.OutDate(1300))

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal returnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(returnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

    Private Function ArrangeDataRow(ByVal Value As Nullable(Of Date)) As DataRow

        Dim DT As DataTable

        DT = New DataTable

        DT.Columns.Add("ID", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("StartDate", System.Type.GetType("System.DateTime"))
        DT.Columns.Add("EndDate", System.Type.GetType("System.DateTime"))
        DT.Columns.Add("IsClosed", System.Type.GetType("System.Boolean"))

        DT.Rows.Add(Nothing, Value, Nothing, Nothing)

        Return DT.Rows(0)

    End Function

#End Region

End Class