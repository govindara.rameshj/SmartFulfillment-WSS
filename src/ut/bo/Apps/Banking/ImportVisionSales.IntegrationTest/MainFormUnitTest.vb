﻿<TestClass()> Public Class MainFormUnitTest

    Private Const ConnectionXmlFilePath As String = "C:\Program Files\CTS Retail\Oasys3\Resources\"
    Private Const StagingAreaPath As String = "C:\Projects\Back Office Salisbury Pilot\Back Office\Staging Area\"

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"

    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

        Assert.IsTrue(IO.File.Exists(ConnectionXmlFilePath & "Connection.xml"), "Cannot find connection xml file")
        Assert.IsTrue(IO.File.Exists(StagingAreaPath & "ImportVisionSales.exe"), "Cannot find import vision sales executable")
        Assert.IsTrue(IO.File.Exists(StagingAreaPath & "CashierBalancingUpdate.exe"), "Cannot find cashier balancing update executable")

    End Sub

    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub

#End Region

#Region "Enumuerators"

    Private Enum VisionLocation
        VisIn
        VisOut
        VisErr
    End Enum

#End Region

#Region "Test XML Data"

    Private KB_DepositIn_SingleTender_StockNotPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                                     <CTSVISIONDEPOSIT>
                                                                         <PVTOTS>
                                                                             <DATE>23/10/11</DATE>
                                                                             <TILL>55</TILL>
                                                                             <TRAN>6610</TRAN>
                                                                             <CASH>012</CASH>
                                                                             <TIME>124607</TIME>
                                                                             <TCOD>M+</TCOD>
                                                                             <MISC/>
                                                                             <DESC/>
                                                                             <ORDN>373964</ORDN>
                                                                             <MERC>000000.00</MERC>
                                                                             <NMER>000249.00</NMER>
                                                                             <TAXA>000041.50</TAXA>
                                                                             <DISC>000000.00</DISC>
                                                                             <TOTL>000249.00</TOTL>
                                                                             <IEMP>N</IEMP>
                                                                             <PVEM>000000.00</PVEM>
                                                                             <PIVT>N</PIVT>
                                                                         </PVTOTS>
                                                                         <PVPAID>
                                                                             <TENDER>
                                                                                 <DATE>23/10/11</DATE>
                                                                                 <TILL>55</TILL>
                                                                                 <TRAN>6610</TRAN>
                                                                                 <NUMB>0001</NUMB>
                                                                                 <TYPE>03</TYPE>
                                                                                 <AMNT>000249.00-</AMNT>
                                                                                 <CARD>492181******9646</CARD>
                                                                                 <EXDT>1410</EXDT>
                                                                                 <PIVT>N</PIVT>
                                                                             </TENDER>
                                                                         </PVPAID>
                                                                     </CTSVISIONDEPOSIT>

    Private KB_DepositIn_MultipleTender_StockNotPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                                       <CTSVISIONDEPOSIT>
                                                                           <PVTOTS>
                                                                               <DATE>19/02/12</DATE>
                                                                               <TILL>55</TILL>
                                                                               <TRAN>6748</TRAN>
                                                                               <CASH>012</CASH>
                                                                               <TIME>125552</TIME>
                                                                               <TCOD>M+</TCOD>
                                                                               <MISC/>
                                                                               <DESC/>
                                                                               <ORDN>374291</ORDN>
                                                                               <MERC>000000.00</MERC>
                                                                               <NMER>005600.00</NMER>
                                                                               <TAXA>000933.33</TAXA>
                                                                               <DISC>006146.00</DISC>
                                                                               <TOTL>005600.00</TOTL>
                                                                               <IEMP>N</IEMP>
                                                                               <PVEM>000000.00</PVEM>
                                                                               <PIVT>N</PIVT>
                                                                           </PVTOTS>
                                                                           <PVPAID>
                                                                               <TENDER>
                                                                                   <DATE>19/02/12</DATE>
                                                                                   <TILL>55</TILL>
                                                                                   <TRAN>6748</TRAN>
                                                                                   <NUMB>0001</NUMB>
                                                                                   <TYPE>05</TYPE>
                                                                                   <AMNT>005040.00-</AMNT>
                                                                                   <CARD/>
                                                                                   <EXDT/>
                                                                                   <PIVT>N</PIVT>
                                                                               </TENDER>
                                                                               <TENDER>
                                                                                   <DATE>19/02/12</DATE>
                                                                                   <TILL>55</TILL>
                                                                                   <TRAN>6748</TRAN>
                                                                                   <NUMB>0002</NUMB>
                                                                                   <TYPE>03</TYPE>
                                                                                   <AMNT>000560.00-</AMNT>
                                                                                   <CARD>************0786</CARD>
                                                                                   <EXDT>313</EXDT>
                                                                                   <PIVT>N</PIVT>
                                                                               </TENDER>
                                                                           </PVPAID>
                                                                       </CTSVISIONDEPOSIT>

    Private KB_DepositOut_SingleTender_StockNotPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                                      <CTSVISIONDEPOSIT>
                                                                          <PVTOTS>
                                                                              <DATE>30/10/11</DATE>
                                                                              <TILL>55</TILL>
                                                                              <TRAN>6621</TRAN>
                                                                              <CASH>012</CASH>
                                                                              <TIME>132025</TIME>
                                                                              <TCOD>M-</TCOD>
                                                                              <MISC/>
                                                                              <DESC/>
                                                                              <ORDN>373974</ORDN>
                                                                              <MERC>000000.00</MERC>
                                                                              <NMER>000526.58-</NMER>
                                                                              <TAXA>000087.76-</TAXA>
                                                                              <DISC>000593.42-</DISC>
                                                                              <TOTL>000526.58-</TOTL>
                                                                              <IEMP>N</IEMP>
                                                                              <PVEM>000000.00</PVEM>
                                                                              <PIVT>N</PIVT>
                                                                          </PVTOTS>
                                                                          <PVPAID>
                                                                              <TENDER>
                                                                                  <DATE>30/10/11</DATE>
                                                                                  <TILL>55</TILL>
                                                                                  <TRAN>6621</TRAN>
                                                                                  <NUMB>0001</NUMB>
                                                                                  <TYPE>03</TYPE>
                                                                                  <AMNT>000526.58</AMNT>
                                                                                  <CARD>465942******8457</CARD>
                                                                                  <EXDT>1410</EXDT>
                                                                                  <PIVT>N</PIVT>
                                                                              </TENDER>
                                                                          </PVPAID>
                                                                      </CTSVISIONDEPOSIT>

    Private KB_DepositOut_MultipleTender_StockNotPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                                        <CTSVISIONDEPOSIT>
                                                                            <PVTOTS>
                                                                                <DATE>20/11/11</DATE>
                                                                                <TILL>55</TILL>
                                                                                <TRAN>6644</TRAN>
                                                                                <CASH>012</CASH>
                                                                                <TIME>144256</TIME>
                                                                                <TCOD>M-</TCOD>
                                                                                <MISC/>
                                                                                <DESC/>
                                                                                <ORDN>373981</ORDN>
                                                                                <MERC>000000.00</MERC>
                                                                                <NMER>005779.44-</NMER>
                                                                                <TAXA>000963.24-</TAXA>
                                                                                <DISC>007445.56-</DISC>
                                                                                <TOTL>005779.44-</TOTL>
                                                                                <IEMP>N</IEMP>
                                                                                <PVEM>000000.00</PVEM>
                                                                                <PIVT>N</PIVT>
                                                                            </PVTOTS>
                                                                            <PVPAID>
                                                                                <TENDER>
                                                                                    <DATE>20/11/11</DATE>
                                                                                    <TILL>55</TILL>
                                                                                    <TRAN>6644</TRAN>
                                                                                    <NUMB>0001</NUMB>
                                                                                    <TYPE>05</TYPE>
                                                                                    <AMNT>005201.49</AMNT>
                                                                                    <CARD/>
                                                                                    <EXDT/>
                                                                                    <PIVT>N</PIVT>
                                                                                </TENDER>
                                                                                <TENDER>
                                                                                    <DATE>20/11/11</DATE>
                                                                                    <TILL>55</TILL>
                                                                                    <TRAN>6644</TRAN>
                                                                                    <NUMB>0002</NUMB>
                                                                                    <TYPE>03</TYPE>
                                                                                    <AMNT>000577.95</AMNT>
                                                                                    <CARD>540758******8777</CARD>
                                                                                    <EXDT>1307</EXDT>
                                                                                    <PIVT>N</PIVT>
                                                                                </TENDER>
                                                                            </PVPAID>
                                                                        </CTSVISIONDEPOSIT>

    Private NonKB_SingleTender_StockNotPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                              <CTSINTEGRATION>
                                                                  <PVTOTS>
                                                                      <DATE>03/03/12</DATE>
                                                                      <TILL>55</TILL>
                                                                      <TRAN>6773</TRAN>
                                                                      <CASH>003</CASH>
                                                                      <TIME>143335</TIME>
                                                                      <TCOD>SA</TCOD>
                                                                      <MISC/>
                                                                      <DESC/>
                                                                      <ORDN>374376</ORDN>
                                                                      <MERC>000105.25</MERC>
                                                                      <NMER>000000.00</NMER>
                                                                      <TAXA>000017.54</TAXA>
                                                                      <DISC>000097.75</DISC>
                                                                      <TOTL>000105.25</TOTL>
                                                                      <IEMP>N</IEMP>
                                                                      <PVEM>000000.00</PVEM>
                                                                      <PIVT>N</PIVT>
                                                                  </PVTOTS>
                                                                  <PVLINE>
                                                                      <LINE>
                                                                          <DATE>03/03/12</DATE>
                                                                          <TILL>55</TILL>
                                                                          <TRAN>6773</TRAN>
                                                                          <NUMB>0001</NUMB>
                                                                          <SKUN>156855</SKUN>
                                                                          <QUAN>000001</QUAN>
                                                                          <SPRI>000000.00</SPRI>
                                                                          <PRIC>000105.25</PRIC>
                                                                          <PRVE>000087.71</PRVE>
                                                                          <EXTP>000105.25</EXTP>
                                                                          <RITM>N</RITM>
                                                                          <VSYM>a</VSYM>
                                                                          <PIVI>N</PIVI>
                                                                          <PIVM/>
                                                                          <CTGY>125424</CTGY>
                                                                          <GRUP>184494</GRUP>
                                                                          <SGRP>184496</SGRP>
                                                                          <STYL>184504</STYL>
                                                                          <SALT>S</SALT>
                                                                          <PIVT>N</PIVT>
                                                                      </LINE>
                                                                  </PVLINE>
                                                                  <PVPAID>
                                                                      <TENDER>
                                                                          <DATE>03/03/12</DATE>
                                                                          <TILL>55</TILL>
                                                                          <TRAN>6773</TRAN>
                                                                          <NUMB>0001</NUMB>
                                                                          <TYPE>03</TYPE>
                                                                          <AMNT>000105.25-</AMNT>
                                                                          <CARD>************7796</CARD>
                                                                          <EXDT>1213</EXDT>
                                                                          <PIVT>N</PIVT>
                                                                      </TENDER>
                                                                  </PVPAID>
                                                              </CTSINTEGRATION>

    Private NonKB_MultipleTender_StockNotPresent As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                                <CTSINTEGRATION>
                                                                    <PVTOTS>
                                                                        <DATE>03/03/12</DATE>
                                                                        <TILL>55</TILL>
                                                                        <TRAN>6774</TRAN>
                                                                        <CASH>003</CASH>
                                                                        <TIME>143335</TIME>
                                                                        <TCOD>SA</TCOD>
                                                                        <MISC/>
                                                                        <DESC/>
                                                                        <ORDN>374376</ORDN>
                                                                        <MERC>000105.25</MERC>
                                                                        <NMER>000000.00</NMER>
                                                                        <TAXA>000017.54</TAXA>
                                                                        <DISC>000097.75</DISC>
                                                                        <TOTL>000105.25</TOTL>
                                                                        <IEMP>N</IEMP>
                                                                        <PVEM>000000.00</PVEM>
                                                                        <PIVT>N</PIVT>
                                                                    </PVTOTS>
                                                                    <PVLINE>
                                                                        <LINE>
                                                                            <DATE>03/03/12</DATE>
                                                                            <TILL>55</TILL>
                                                                            <TRAN>6774</TRAN>
                                                                            <NUMB>0001</NUMB>
                                                                            <SKUN>156855</SKUN>
                                                                            <QUAN>000001</QUAN>
                                                                            <SPRI>000000.00</SPRI>
                                                                            <PRIC>000105.25</PRIC>
                                                                            <PRVE>000087.71</PRVE>
                                                                            <EXTP>000105.25</EXTP>
                                                                            <RITM>N</RITM>
                                                                            <VSYM>a</VSYM>
                                                                            <PIVI>N</PIVI>
                                                                            <PIVM/>
                                                                            <CTGY>125424</CTGY>
                                                                            <GRUP>184494</GRUP>
                                                                            <SGRP>184496</SGRP>
                                                                            <STYL>184504</STYL>
                                                                            <SALT>S</SALT>
                                                                            <PIVT>N</PIVT>
                                                                        </LINE>
                                                                    </PVLINE>
                                                                    <PVPAID>
                                                                        <TENDER>
                                                                            <DATE>03/03/12</DATE>
                                                                            <TILL>55</TILL>
                                                                            <TRAN>6774</TRAN>
                                                                            <NUMB>0001</NUMB>
                                                                            <TYPE>03</TYPE>
                                                                            <AMNT>000100.00-</AMNT>
                                                                            <CARD>************7796</CARD>
                                                                            <EXDT>1213</EXDT>
                                                                            <PIVT>N</PIVT>
                                                                        </TENDER>
                                                                        <TENDER>
                                                                            <DATE>03/03/12</DATE>
                                                                            <TILL>55</TILL>
                                                                            <TRAN>6774</TRAN>
                                                                            <NUMB>0002</NUMB>
                                                                            <TYPE>05</TYPE>
                                                                            <AMNT>000005.25-</AMNT>
                                                                            <CARD/>
                                                                            <EXDT/>
                                                                            <PIVT>N</PIVT>
                                                                        </TENDER>
                                                                    </PVPAID>
                                                                </CTSINTEGRATION>


    Private KB_StockPresentVersionOne As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                     <CTSVISIONDEPOSIT>
                                                         <PVTOTS>
                                                             <DATE>19/02/12</DATE>
                                                             <TILL>55</TILL>
                                                             <TRAN>6748</TRAN>
                                                             <CASH>012</CASH>
                                                             <TIME>125552</TIME>
                                                             <TCOD>M+</TCOD>
                                                             <MISC/>
                                                             <DESC/>
                                                             <ORDN>374291</ORDN>
                                                             <MERC>000000.00</MERC>
                                                             <NMER>005600.00</NMER>
                                                             <TAXA>000933.33</TAXA>
                                                             <DISC>006146.00</DISC>
                                                             <TOTL>005600.00</TOTL>
                                                             <IEMP>N</IEMP>
                                                             <PVEM>000000.00</PVEM>
                                                             <PIVT>N</PIVT>
                                                         </PVTOTS>
                                                         <PVPAID>
                                                             <TENDER>
                                                                 <DATE>19/02/12</DATE>
                                                                 <TILL>55</TILL>
                                                                 <TRAN>6748</TRAN>
                                                                 <NUMB>0001</NUMB>
                                                                 <TYPE>05</TYPE>
                                                                 <AMNT>005040.00-</AMNT>
                                                                 <CARD/>
                                                                 <EXDT/>
                                                                 <PIVT>N</PIVT>
                                                             </TENDER>
                                                             <TENDER>
                                                                 <DATE>19/02/12</DATE>
                                                                 <TILL>55</TILL>
                                                                 <TRAN>6748</TRAN>
                                                                 <NUMB>0002</NUMB>
                                                                 <TYPE>03</TYPE>
                                                                 <AMNT>000560.00-</AMNT>
                                                                 <CARD>************0786</CARD>
                                                                 <EXDT>313</EXDT>
                                                                 <PIVT>N</PIVT>
                                                             </TENDER>
                                                         </PVPAID>
                                                         <STOCK>
                                                             <LINE>
                                                                 <SKUN>100803</SKUN>
                                                                 <QUAN>000005</QUAN>
                                                             </LINE>
                                                             <LINE>
                                                                 <SKUN>100804</SKUN>
                                                                 <QUAN>000014</QUAN>
                                                             </LINE>
                                                         </STOCK>
                                                     </CTSVISIONDEPOSIT>

    Private KB_StockPresentVersionTwo As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                     <CTSVISIONDEPOSIT>
                                                         <PVTOTS>
                                                             <DATE>19/02/12</DATE>
                                                             <TILL>55</TILL>
                                                             <TRAN>6749</TRAN>
                                                             <CASH>012</CASH>
                                                             <TIME>125552</TIME>
                                                             <TCOD>M+</TCOD>
                                                             <MISC/>
                                                             <DESC/>
                                                             <ORDN>374291</ORDN>
                                                             <MERC>000000.00</MERC>
                                                             <NMER>005600.00</NMER>
                                                             <TAXA>000933.33</TAXA>
                                                             <DISC>006146.00</DISC>
                                                             <TOTL>005600.00</TOTL>
                                                             <IEMP>N</IEMP>
                                                             <PVEM>000000.00</PVEM>
                                                             <PIVT>N</PIVT>
                                                         </PVTOTS>
                                                         <PVPAID>
                                                             <TENDER>
                                                                 <DATE>19/02/12</DATE>
                                                                 <TILL>55</TILL>
                                                                 <TRAN>6749</TRAN>
                                                                 <NUMB>0001</NUMB>
                                                                 <TYPE>05</TYPE>
                                                                 <AMNT>005040.00-</AMNT>
                                                                 <CARD/>
                                                                 <EXDT/>
                                                                 <PIVT>N</PIVT>
                                                             </TENDER>
                                                             <TENDER>
                                                                 <DATE>19/02/12</DATE>
                                                                 <TILL>55</TILL>
                                                                 <TRAN>6749</TRAN>
                                                                 <NUMB>0002</NUMB>
                                                                 <TYPE>03</TYPE>
                                                                 <AMNT>000560.00-</AMNT>
                                                                 <CARD>************0786</CARD>
                                                                 <EXDT>313</EXDT>
                                                                 <PIVT>N</PIVT>
                                                             </TENDER>
                                                         </PVPAID>
                                                         <STOCK>
                                                             <LINE>
                                                                 <SKUN>100803</SKUN>
                                                                 <QUAN>000005</QUAN>
                                                             </LINE>
                                                             <LINE>
                                                                 <SKUN>100804</SKUN>
                                                                 <QUAN>000014</QUAN>
                                                             </LINE>
                                                         </STOCK>
                                                     </CTSVISIONDEPOSIT>

    Private NonKB_StockPresentVersionOne As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                        <CTSINTEGRATION>
                                                            <PVTOTS>
                                                                <DATE>03/03/12</DATE>
                                                                <TILL>55</TILL>
                                                                <TRAN>6774</TRAN>
                                                                <CASH>003</CASH>
                                                                <TIME>143335</TIME>
                                                                <TCOD>SA</TCOD>
                                                                <MISC/>
                                                                <DESC/>
                                                                <ORDN>374376</ORDN>
                                                                <MERC>000105.25</MERC>
                                                                <NMER>000000.00</NMER>
                                                                <TAXA>000017.54</TAXA>
                                                                <DISC>000097.75</DISC>
                                                                <TOTL>000105.25</TOTL>
                                                                <IEMP>N</IEMP>
                                                                <PVEM>000000.00</PVEM>
                                                                <PIVT>N</PIVT>
                                                            </PVTOTS>
                                                            <PVLINE>
                                                                <LINE>
                                                                    <DATE>03/03/12</DATE>
                                                                    <TILL>55</TILL>
                                                                    <TRAN>6774</TRAN>
                                                                    <NUMB>0001</NUMB>
                                                                    <SKUN>156855</SKUN>
                                                                    <QUAN>000001</QUAN>
                                                                    <SPRI>000000.00</SPRI>
                                                                    <PRIC>000105.25</PRIC>
                                                                    <PRVE>000087.71</PRVE>
                                                                    <EXTP>000105.25</EXTP>
                                                                    <RITM>N</RITM>
                                                                    <VSYM>a</VSYM>
                                                                    <PIVI>N</PIVI>
                                                                    <PIVM/>
                                                                    <CTGY>125424</CTGY>
                                                                    <GRUP>184494</GRUP>
                                                                    <SGRP>184496</SGRP>
                                                                    <STYL>184504</STYL>
                                                                    <SALT>S</SALT>
                                                                    <PIVT>N</PIVT>
                                                                </LINE>
                                                            </PVLINE>
                                                            <PVPAID>
                                                                <TENDER>
                                                                    <DATE>03/03/12</DATE>
                                                                    <TILL>55</TILL>
                                                                    <TRAN>6774</TRAN>
                                                                    <NUMB>0001</NUMB>
                                                                    <TYPE>03</TYPE>
                                                                    <AMNT>000100.00-</AMNT>
                                                                    <CARD>************7796</CARD>
                                                                    <EXDT>1213</EXDT>
                                                                    <PIVT>N</PIVT>
                                                                </TENDER>
                                                                <TENDER>
                                                                    <DATE>03/03/12</DATE>
                                                                    <TILL>55</TILL>
                                                                    <TRAN>6774</TRAN>
                                                                    <NUMB>0002</NUMB>
                                                                    <TYPE>05</TYPE>
                                                                    <AMNT>000005.25-</AMNT>
                                                                    <CARD/>
                                                                    <EXDT/>
                                                                    <PIVT>N</PIVT>
                                                                </TENDER>
                                                            </PVPAID>
                                                            <STOCK>
                                                                <LINE>
                                                                    <SKUN>100805</SKUN>
                                                                    <QUAN>000015</QUAN>
                                                                </LINE>
                                                                <LINE>
                                                                    <SKUN>100806</SKUN>
                                                                    <QUAN>000114</QUAN>
                                                                </LINE>
                                                            </STOCK>
                                                        </CTSINTEGRATION>

    Private NonKB_StockPresentVersionTwo As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                        <CTSINTEGRATION>
                                                            <PVTOTS>
                                                                <DATE>03/03/12</DATE>
                                                                <TILL>55</TILL>
                                                                <TRAN>6775</TRAN>
                                                                <CASH>003</CASH>
                                                                <TIME>143335</TIME>
                                                                <TCOD>SA</TCOD>
                                                                <MISC/>
                                                                <DESC/>
                                                                <ORDN>374376</ORDN>
                                                                <MERC>000105.25</MERC>
                                                                <NMER>000000.00</NMER>
                                                                <TAXA>000017.54</TAXA>
                                                                <DISC>000097.75</DISC>
                                                                <TOTL>000105.25</TOTL>
                                                                <IEMP>N</IEMP>
                                                                <PVEM>000000.00</PVEM>
                                                                <PIVT>N</PIVT>
                                                            </PVTOTS>
                                                            <PVLINE>
                                                                <LINE>
                                                                    <DATE>03/03/12</DATE>
                                                                    <TILL>55</TILL>
                                                                    <TRAN>6775</TRAN>
                                                                    <NUMB>0001</NUMB>
                                                                    <SKUN>156855</SKUN>
                                                                    <QUAN>000001</QUAN>
                                                                    <SPRI>000000.00</SPRI>
                                                                    <PRIC>000105.25</PRIC>
                                                                    <PRVE>000087.71</PRVE>
                                                                    <EXTP>000105.25</EXTP>
                                                                    <RITM>N</RITM>
                                                                    <VSYM>a</VSYM>
                                                                    <PIVI>N</PIVI>
                                                                    <PIVM/>
                                                                    <CTGY>125424</CTGY>
                                                                    <GRUP>184494</GRUP>
                                                                    <SGRP>184496</SGRP>
                                                                    <STYL>184504</STYL>
                                                                    <SALT>S</SALT>
                                                                    <PIVT>N</PIVT>
                                                                </LINE>
                                                            </PVLINE>
                                                            <PVPAID>
                                                                <TENDER>
                                                                    <DATE>03/03/12</DATE>
                                                                    <TILL>55</TILL>
                                                                    <TRAN>6775</TRAN>
                                                                    <NUMB>0001</NUMB>
                                                                    <TYPE>03</TYPE>
                                                                    <AMNT>000100.00-</AMNT>
                                                                    <CARD>************7796</CARD>
                                                                    <EXDT>1213</EXDT>
                                                                    <PIVT>N</PIVT>
                                                                </TENDER>
                                                                <TENDER>
                                                                    <DATE>03/03/12</DATE>
                                                                    <TILL>55</TILL>
                                                                    <TRAN>6775</TRAN>
                                                                    <NUMB>0002</NUMB>
                                                                    <TYPE>05</TYPE>
                                                                    <AMNT>000005.25-</AMNT>
                                                                    <CARD/>
                                                                    <EXDT/>
                                                                    <PIVT>N</PIVT>
                                                                </TENDER>
                                                            </PVPAID>
                                                            <STOCK>
                                                                <LINE>
                                                                    <SKUN>100805</SKUN>
                                                                    <QUAN>000015</QUAN>
                                                                </LINE>
                                                                <LINE>
                                                                    <SKUN>100806</SKUN>
                                                                    <QUAN>000114</QUAN>
                                                                </LINE>
                                                            </STOCK>
                                                        </CTSINTEGRATION>


    Private KB_StockNotPresent_BAD As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                                  <CTSVISIONDEPOSIT>
                                                      <PVTOTS>
                                                          <DATE></DATE>
                                                          <TILL></TILL>
                                                          <TRAN>1111</TRAN>
                                                          <CASH></CASH>
                                                          <TIME></TIME>
                                                          <TCOD></TCOD>
                                                          <MISC/>
                                                          <DESC/>
                                                          <ORDN></ORDN>
                                                          <MERC></MERC>
                                                          <NMER></NMER>
                                                          <TAXA></TAXA>
                                                          <DISC></DISC>
                                                          <TOTL></TOTL>
                                                          <IEMP></IEMP>
                                                          <PVEM></PVEM>
                                                          <PIVT></PIVT>
                                                      </PVTOTS>
                                                      <PVPAID>
                                                          <TENDER>
                                                              <DATE></DATE>
                                                              <TILL></TILL>
                                                              <TRAN>1111</TRAN>
                                                              <NUMB></NUMB>
                                                              <TYPE></TYPE>
                                                              <AMNT></AMNT>
                                                              <CARD></CARD>
                                                              <EXDT></EXDT>
                                                              <PIVT></PIVT>
                                                          </TENDER>
                                                      </PVPAID>
                                                  </CTSVISIONDEPOSIT>

    Private KB_StockPresent_BAD As XDocument = <?xml version="1.0" encoding="utf-16"?>
                                               <CTSVISIONDEPOSIT>
                                                   <PVTOTS>
                                                       <DATE></DATE>
                                                       <TILL></TILL>
                                                       <TRAN>2222</TRAN>
                                                       <CASH></CASH>
                                                       <TIME></TIME>
                                                       <TCOD></TCOD>
                                                       <MISC/>
                                                       <DESC/>
                                                       <ORDN></ORDN>
                                                       <MERC></MERC>
                                                       <NMER></NMER>
                                                       <TAXA></TAXA>
                                                       <DISC></DISC>
                                                       <TOTL></TOTL>
                                                       <IEMP></IEMP>
                                                       <PVEM></PVEM>
                                                       <PIVT></PIVT>
                                                   </PVTOTS>
                                                   <PVPAID>
                                                       <TENDER>
                                                           <DATE></DATE>
                                                           <TILL></TILL>
                                                           <TRAN>2222</TRAN>
                                                           <NUMB></NUMB>
                                                           <TYPE></TYPE>
                                                           <AMNT></AMNT>
                                                           <CARD/>
                                                           <EXDT/>
                                                           <PIVT></PIVT>
                                                       </TENDER>
                                                       <TENDER>
                                                           <DATE></DATE>
                                                           <TILL></TILL>
                                                           <TRAN>2222</TRAN>
                                                           <NUMB></NUMB>
                                                           <TYPE></TYPE>
                                                           <AMNT></AMNT>
                                                           <CARD></CARD>
                                                           <EXDT></EXDT>
                                                           <PIVT></PIVT>
                                                       </TENDER>
                                                   </PVPAID>
                                                   <STOCK>
                                                       <LINE>
                                                           <SKUN></SKUN>
                                                           <QUAN></QUAN>
                                                       </LINE>
                                                       <LINE>
                                                           <SKUN></SKUN>
                                                           <QUAN></QUAN>
                                                       </LINE>
                                                   </STOCK>
                                               </CTSVISIONDEPOSIT>

#End Region

#Region "Integration Tests | Directly Utilise Vision Business Objects | Stock Not Present | KB Sale"

#Region "Integration Tests | Directly Utilise Vision Business Objects | Stock Not Present | KB Sale | Deposit In"

    <TestMethod()> Public Sub KB_DepositIn_SingleTender_TestScenarios()

        'PLEASE NOTE: <PVTOTS> child tags <PREM>, <PIVT> is not used to update DLTOTS; ignore testing these properties
        '             <TENDER> child tags         <PIVT> is not used to update DLPAID; ignore testing these properties

        Dim TempDocument As XDocument

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_DepositIn_SingleTender_StockNotPresent)
        'sale date = system date
        TempDocument = KitchenOrBathroomSaleUpdateDocumentTransactionDate(KB_DepositIn_SingleTender_StockNotPresent)
        KitchenOrBathroomSaleAssertionTests(TempDocument)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_DepositIn_SingleTender_StockNotPresent)
        'sale date = system date
        TempDocument = KitchenOrBathroomSaleUpdateDocumentTransactionDate(KB_DepositIn_SingleTender_StockNotPresent)
        KitchenOrBathroomSaleAssertionTests(TempDocument)

    End Sub

    <TestMethod()> Public Sub KB_DepositIn_MultipleTenders_TestScenarios()

        'PLEASE NOTE: <PVTOTS> child tags <PREM>, <PIVT> is not used to update DLTOTS; ignore testing these properties
        '             <TENDER> child tags         <PIVT> is not used to update DLPAID; ignore testing these properties

        Dim TempDocument As XDocument

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_DepositIn_MultipleTender_StockNotPresent)
        'sale date = system date
        TempDocument = KitchenOrBathroomSaleUpdateDocumentTransactionDate(KB_DepositIn_SingleTender_StockNotPresent)
        KitchenOrBathroomSaleAssertionTests(TempDocument)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_DepositIn_MultipleTender_StockNotPresent)
        'sale date = system date
        TempDocument = KitchenOrBathroomSaleUpdateDocumentTransactionDate(KB_DepositIn_SingleTender_StockNotPresent)
        KitchenOrBathroomSaleAssertionTests(TempDocument)

    End Sub

#End Region

#Region "Integration Tests | Directly Utilise Vision Business Objects | Stock Not Present | KB Sale | Deposit Out"

    <TestMethod()> Public Sub KB_DepositOut_SingleTender_TestScenarios()

        'PLEASE NOTE: <PVTOTS> child tags <PREM>, <PIVT> is not used to update DLTOTS; ignore testing these properties
        '             <TENDER> child tags         <PIVT> is not used to update DLPAID; ignore testing these properties

        Dim TempDocument As XDocument

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_DepositOut_SingleTender_StockNotPresent)
        'sale date = system date
        TempDocument = KitchenOrBathroomSaleUpdateDocumentTransactionDate(KB_DepositOut_SingleTender_StockNotPresent)
        KitchenOrBathroomSaleAssertionTests(TempDocument)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_DepositOut_SingleTender_StockNotPresent)
        'sale date = system date
        TempDocument = KitchenOrBathroomSaleUpdateDocumentTransactionDate(KB_DepositOut_SingleTender_StockNotPresent)
        KitchenOrBathroomSaleAssertionTests(TempDocument)

    End Sub

    <TestMethod()> Public Sub KB_DepositOut_MultipleTenders_TestScenarios()

        'PLEASE NOTE: <PVTOTS> child tags <PREM>, <PIVT> is not used to update DLTOTS; ignore testing these properties
        '             <TENDER> child tags         <PIVT> is not used to update DLPAID; ignore testing these properties

        Dim TempDocument As XDocument

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_DepositOut_MultipleTender_StockNotPresent)
        'sale date = system date
        TempDocument = KitchenOrBathroomSaleUpdateDocumentTransactionDate(KB_DepositOut_MultipleTender_StockNotPresent)
        KitchenOrBathroomSaleAssertionTests(TempDocument)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_DepositOut_MultipleTender_StockNotPresent)
        'sale date = system date
        TempDocument = KitchenOrBathroomSaleUpdateDocumentTransactionDate(KB_DepositOut_MultipleTender_StockNotPresent)
        KitchenOrBathroomSaleAssertionTests(TempDocument)

    End Sub

#End Region

#End Region

#Region "Integration Tests | Directly Utilise Vision Business Objects | Stock Not Present | NonKB Sale"

    <TestMethod()> Public Sub NonKB_SingleTender_TestScenarios()

        'PLEASE NOTE: <TENDER> child tags <CARD>, <EXDT> is not used to update Vision Payment; ignore testing these properties

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        'sale date <> system date
        NonKitchenOrBathroomSaleAssertionTests(NonKB_SingleTender_StockNotPresent)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")
        'sale date <> system date
        NonKitchenOrBathroomSaleAssertionTests(NonKB_SingleTender_StockNotPresent)

    End Sub

    <TestMethod()> Public Sub NonKB_MultipleTender_TestScenarios()

        'PLEASE NOTE: <TENDER> child tags <CARD>, <EXDT> is not used to update Vision Payment; ignore testing these properties

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        'sale date <> system date
        NonKitchenOrBathroomSaleAssertionTests(NonKB_MultipleTender_StockNotPresent)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")
        'sale date <> system date
        NonKitchenOrBathroomSaleAssertionTests(NonKB_MultipleTender_StockNotPresent)

    End Sub

#End Region

#Region "Integration Tests | Directly Utilise Vision Business Objects | Stock Present | KB Sale"

    <TestMethod()> Public Sub KB_MultipleStockPresent_TestScenarios()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_StockPresentVersionOne)
        KitchenOrBathroomSaleAssertionTests(KB_StockPresentVersionTwo)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")
        'sale date <> system date
        KitchenOrBathroomSaleAssertionTests(KB_StockPresentVersionOne)
        KitchenOrBathroomSaleAssertionTests(KB_StockPresentVersionTwo)

    End Sub

#End Region

#Region "Integration Tests | Directly Utilise Vision Business Objects | Stock Present | NonKB Sale"

    <TestMethod()> Public Sub NonKB_MultipleStockPresent_TestScenarios()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        'sale date <> system date
        NonKitchenOrBathroomSaleAssertionTests(NonKB_StockPresentVersionOne)
        NonKitchenOrBathroomSaleAssertionTests(NonKB_StockPresentVersionTwo)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")
        'sale date <> system date
        NonKitchenOrBathroomSaleAssertionTests(NonKB_StockPresentVersionOne)
        NonKitchenOrBathroomSaleAssertionTests(NonKB_StockPresentVersionTwo)

    End Sub

#End Region

#Region "Integration Tests | Main Application"

    <TestMethod()> Public Sub MainApplication_TestScenarios()

        Dim VisionSales As List(Of XDocument)

        VisionSales = New List(Of XDocument)
        VisionSales.Add(KB_DepositIn_SingleTender_StockNotPresent)
        VisionSales.Add(KB_DepositIn_MultipleTender_StockNotPresent)
        VisionSales.Add(KB_DepositOut_SingleTender_StockNotPresent)
        VisionSales.Add(KB_DepositOut_MultipleTender_StockNotPresent)
        VisionSales.Add(NonKB_SingleTender_StockNotPresent)
        VisionSales.Add(NonKB_MultipleTender_StockNotPresent)
        VisionSales.Add(KB_StockPresentVersionOne)
        VisionSales.Add(KB_StockPresentVersionTwo)
        VisionSales.Add(NonKB_StockPresentVersionOne)
        VisionSales.Add(NonKB_StockPresentVersionTwo)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(False), "Unable to switch requirement off")
        CreateEnvironmentForImportVisionSales(VisionSales)

        RunImportVisionSales(StagingAreaPath, "ImportVisionSales.exe", "")

        Assert.IsTrue(VisionDirectoryEmpty(VisionLocation.VisIn, VisionSales), "Test A: One or more vision sales in the input directory not processed")
        Assert.IsTrue(VisionDirectoryEmpty(VisionLocation.VisErr, VisionSales), "Test B: One or more vision sales in the error directory")
        Assert.IsTrue(AllVisionSalesExistInDirectory(VisionLocation.VisOut, VisionSales), "Test C: One or more vision sales not in output directory")

        'CleanUpDatabaseAfterImportVisionSales(VisionSales)
        CleanUpEnvironmentAfterImportVisionSales(VisionSales)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")

        CreateEnvironmentForImportVisionSales(VisionSales)

        RunImportVisionSales(StagingAreaPath, "ImportVisionSales.exe", "")

        Assert.IsTrue(VisionDirectoryEmpty(VisionLocation.VisIn, VisionSales), "Test D: One or more vision sales in the input directory not processed")
        Assert.IsTrue(VisionDirectoryEmpty(VisionLocation.VisErr, VisionSales), "Test E: One or more vision sales in the error directory")
        Assert.IsTrue(AllVisionSalesExistInDirectory(VisionLocation.VisOut, VisionSales), "Test F: One or more vision sales not in output directory")

        'CleanUpDatabaseAfterImportVisionSales(VisionSales)
        CleanUpEnvironmentAfterImportVisionSales(VisionSales)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(RequirementSwitch(True), "Unable to switch requirement on")

        VisionSales = New List(Of XDocument)
        VisionSales.Add(KB_StockNotPresent_BAD)
        VisionSales.Add(KB_StockPresent_BAD)

        CreateEnvironmentForImportVisionSales(VisionSales)

        RunImportVisionSales(StagingAreaPath, "ImportVisionSales.exe", "")

        Assert.IsTrue(VisionDirectoryEmpty(VisionLocation.VisIn, VisionSales), "Test G: One or more vision sales in the input directory not processed")
        Assert.IsTrue(AllVisionSalesExistInDirectory(VisionLocation.VisErr, VisionSales), "Test F: One or more BAD vision sales not in error directory")
        Assert.IsTrue(VisionDirectoryEmpty(VisionLocation.VisOut, VisionSales), "Test I: One or more vision sales in output directory, incorrectly processed BAD sale")


        'CleanUpDatabaseAfterImportVisionSales(VisionSales)
        CleanUpEnvironmentAfterImportVisionSales(VisionSales)

    End Sub

#End Region

#Region "Private Functions And Procedures"

#Region "Kitchen Or Bathroom Sale"

    Private Sub KitchenOrBathroomSaleAssertionTests(ByVal XmlPacket As XDocument)

        Dim KB_Sale As KitchenAndBathroom

        'DLTOTS
        Dim TransactionStringDate As String
        Dim TransactionDate As Date
        Dim TillID As String
        Dim TransactionNo As String
        Dim CashierID As String
        Dim TransactionTime As String
        Dim TransactionCode As String
        'Dim ReasonCode As integer
        Dim ReasonCode As String
        Dim ReasonDescription As String
        Dim OrderNumber As String
        Dim MerchandiseAmount As Decimal
        Dim NonMerchandiseAmount As Decimal
        Dim TaxAmount As Decimal
        Dim DiscountAmount As Decimal
        Dim TotalAmount As Decimal
        Dim EmployeeDiscount As String

        'DLPAID
        Dim TenderTransactionStringDate As String
        Dim TenderTransactionDate As Date
        Dim TenderTillID As String
        Dim TenderTransactionNo As String
        Dim TenderLineNumber As Integer
        Dim TenderType As Decimal
        Dim TenderAmount As Decimal
        Dim TenderCreditCardNumber As String
        Dim TenderCreditCardExpiryDate As String

        'sale total
        TransactionStringDate = XmlPacket.Descendants("PVTOTS").Elements("DATE").ElementAt(0).Value
        TransactionDate = New Date(CType("20" & TransactionStringDate.Substring(6, 2), Integer), _
                                   CType(TransactionStringDate.Substring(3, 2), Integer), _
                                   CType(TransactionStringDate.Substring(0, 2), Integer))
        TillID = XmlPacket.Descendants("PVTOTS").Elements("TILL").ElementAt(0).Value
        TransactionNo = XmlPacket.Descendants("PVTOTS").Elements("TRAN").ElementAt(0).Value
        CashierID = XmlPacket.Descendants("PVTOTS").Elements("CASH").ElementAt(0).Value
        TransactionTime = XmlPacket.Descendants("PVTOTS").Elements("TIME").ElementAt(0).Value
        TransactionCode = XmlPacket.Descendants("PVTOTS").Elements("TCOD").ElementAt(0).Value
        ReasonCode = XmlPacket.Descendants("PVTOTS").Elements("MISC").ElementAt(0).Value
        ReasonDescription = XmlPacket.Descendants("PVTOTS").Elements("DESC").ElementAt(0).Value
        OrderNumber = XmlPacket.Descendants("PVTOTS").Elements("ORDN").ElementAt(0).Value
        MerchandiseAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("MERC").ElementAt(0).Value, Decimal)
        NonMerchandiseAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("NMER").ElementAt(0).Value, Decimal)
        TaxAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("TAXA").ElementAt(0).Value, Decimal)
        DiscountAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("DISC").ElementAt(0).Value, Decimal)
        TotalAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("TOTL").ElementAt(0).Value, Decimal)
        EmployeeDiscount = XmlPacket.Descendants("PVTOTS").Elements("IEMP").ElementAt(0).Value

        ''sale does not exist
        'Assert.IsFalse(SaleTotalExistInDatabase(TransactionDate, TillID, TransactionNo), " DLTOTS already exist".Trim)
        'Assert.IsFalse(SalePaymentsExistInDatabase(TransactionDate, TillID, TransactionNo), "DLPAID already exist".Trim)

        'create sale
        KB_Sale = KitchenAndBathroom.Deserialise(XmlPacket.ToString)
        KB_Sale.Persist()

        'sale total exist with correct values
        Assert.IsTrue(SaleTotalExistInDatabase(TransactionDate, TillID, TransactionNo), "                                                                                                               DLTOTS record not created".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "DATE1", TransactionDate.ToString("dd/MM/yyyy")), "                                                            DLTOTS:DATE1 is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TILL", TillID), "                                                                                             DLTOTS:TILL  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TRAN", TransactionNo), "                                                                                      DLTOTS:TRAN  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "CASH", CashierID), "                                                                                          DLTOTS:CASH  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TIME", TransactionTime), "                                                                                    DLTOTS:TIME  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TCOD", TransactionCode), "                                                                                    DLTOTS:TCOD  is incorrect".Trim)

        'value in <MISC> ignored
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "MISC", Nothing, TransactionCode), "                                                                           DLTOTS:MISC  is incorrect".Trim)
        'value in <DESCR> ignored
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "DESCR", Nothing, TransactionCode), "                                                                          DLTOTS:DESCR is incorrect".Trim)

        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "ORDN", "000000"), "                                                                                           DLTOTS:ORDN  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "DOCN", OrderNumber), "                                                                                        DLTOTS:DOCN  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "MERC", MerchandiseAmount.ToString), "                                                                         DLTOTS:MERC  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "NMER", NonMerchandiseAmount.ToString), "                                                                      DLTOTS:NMER  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TAXA", "0"), "                                                                                                DLTOTS:TAXA  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "DISC", "0"), "                                                                                                DLTOTS:DISC  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TOTL", TotalAmount.ToString), "                                                                               DLTOTS:TOTL  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "IEMP", EmployeeDiscount.ToString), "                                                                          DLTOTS:IEMP  is incorrect".Trim)
        Assert.IsTrue(SaleTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "CBBU", CType(IIf(Date.Compare(Today, TransactionDate) <> 0, Today.ToString("dd/MM/yy"), Space(8)), String)), "DLTOTS:CBBU  is incorrect".Trim)

        'sale payments exist with correct values
        For Each TenderNode As XElement In XmlPacket.Descendants("PVPAID").Elements

            TenderTransactionStringDate = TenderNode.Element("DATE").Value
            TenderTransactionDate = New Date(CType("20" & TenderTransactionStringDate.Substring(6, 2), Integer), _
                                       CType(TenderTransactionStringDate.Substring(3, 2), Integer), _
                                       CType(TenderTransactionStringDate.Substring(0, 2), Integer))
            TenderTillID = TenderNode.Element("TILL").Value
            TenderTransactionNo = TenderNode.Element("TRAN").Value
            TenderLineNumber = CType(TenderNode.Element("NUMB").Value, Integer)
            TenderType = CType(TenderNode.Element("TYPE").Value, Decimal)
            TenderAmount = CType(TenderNode.Element("AMNT").Value, Decimal)
            TenderCreditCardNumber = TenderNode.Element("CARD").Value
            TenderCreditCardExpiryDate = TenderNode.Element("EXDT").Value

            Assert.IsTrue(SalePaymentExistInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber), "                                                                                                          DLPAID record not created".Trim)
            Assert.IsTrue(SalePaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "DATE1", TransactionDate.ToString("dd/MM/yyyy")), "                                                       DLPAID:DATE1 is incorrect".Trim)
            Assert.IsTrue(SalePaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "TILL", TillID), "                                                                                        DLPAID:TILL  is incorrect".Trim)
            Assert.IsTrue(SalePaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "TRAN", TransactionNo), "                                                                                 DLPAID:TRAN  is incorrect".Trim)
            Assert.IsTrue(SalePaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "NUMB", TenderLineNumber.ToString), "                                                                     DLPAID:NUMB  is incorrect".Trim)
            Assert.IsTrue(SalePaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "TYPE", TenderType.ToString), "                                                                           DLPAID:TYPE  is incorrect".Trim)
            Assert.IsTrue(SalePaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "AMNT", TenderAmount.ToString), "                                                                         DLPAID:AMNT  is incorrect".Trim)
            Assert.IsTrue(SalePaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "CARD", IIf(TenderCreditCardNumber.Length = 0, "0000000000000000000", TenderCreditCardNumber).ToString), "DLPAID:CARD  is incorrect".Trim)
            Assert.IsTrue(SalePaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "EXDT", IIf(TenderCreditCardExpiryDate.Length = 0, "0000", TenderCreditCardExpiryDate).ToString), "       DLPAID:EXDT  is incorrect".Trim)

        Next

        'stock exist with correct values
        For Each StockNode As XElement In XmlPacket.Descendants("STOCK").Elements

            'do nothing

        Next

        ''delete sale
        'KitchenOrBathroomSaleDeleteFromDatabase(TransactionDate, TillID, TransactionNo)

    End Sub

    Private Sub KitchenOrBathroomSaleDeleteFromDatabase(ByVal Date1 As Date, ByVal TillID As String, ByVal TransactionID As String)

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("delete DLTOTS                                         ")
            .Append("where DATE1  = '" & Date1.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TILL   = '" & TillID & "'                       ")
            .Append("and   [TRAN] = '" & TransactionID & "'                ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        With SB
            .Remove(0, .Length)

            .Append("delete DLPAID                                         ")
            .Append("where DATE1  = '" & Date1.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TILL   = '" & TillID & "'                       ")
            .Append("and   [TRAN] = '" & TransactionID & "'                ")

        End With

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        With SB
            .Remove(0, .Length)

            .Append("delete DLLINE                                         ")
            .Append("where DATE1  = '" & Date1.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TILL   = '" & TillID & "'                       ")
            .Append("and   [TRAN] = '" & TransactionID & "'                ")

        End With

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

    End Sub

    Private Function KitchenOrBathroomSaleUpdateDocumentTransactionDate(ByVal XmlPacket As XDocument) As XDocument

        KitchenOrBathroomSaleUpdateDocumentTransactionDate = XmlPacket

        KitchenOrBathroomSaleUpdateDocumentTransactionDate.Descendants("PVTOTS").Elements("DATE").ElementAt(0).Value = Today.ToString("dd/MM/yy")

        For Each TenderNode As XElement In KitchenOrBathroomSaleUpdateDocumentTransactionDate.Descendants("PVPAID").Elements

            TenderNode.Element("DATE").Value = Today.ToString("dd/MM/yy")

        Next

    End Function

#Region "DLTOTS Stuff"

    Private Function SaleTotalExistInDatabase(ByVal TransactionDate As Date, _
                                              ByVal TillID As String, _
                                              ByVal TransactionNo As String) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("select *                                                        ")
            .Append("from DLTOTS                                                     ")
            .Append("where DATE1  = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TILL   = '" & TillID & "'                                 ")
            .Append("and   [TRAN] = '" & TransactionNo & "'                          ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then SaleTotalExistInDatabase = True

    End Function

    Private Function SaleTotalCorrectInDatabase(ByVal TransactionDate As Date, _
                                                 ByVal TillID As String, _
                                                 ByVal TransactionNo As String, _
                                                 ByVal ColumnName As String, _
                                                 ByVal ColumnValue As String, _
                                                 Optional ByVal TransactionCode As String = "") As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable
        Dim DR As DataRow

        With SB

            .Append("select *                                                        ")
            .Append("from DLTOTS                                                     ")
            .Append("where DATE1  = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TILL   = '" & TillID & "'                                 ")
            .Append("and   [TRAN] = '" & TransactionNo & "'                          ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then

            DR = DT.Rows.Item(0)

            Select Case ColumnName
                Case "DATE1"
                    If DateTime.Compare(CType(DR("DATE1"), Date), CType(ColumnValue, Date)) = 0 Then Return True

                Case "TILL", "TRAN", "CASH", "TIME", "TCOD", "ORDN", "DOCN"
                    If CType(DR(ColumnName), String).Trim = ColumnValue Then Return True

                Case "MISC"
                    Select Case TransactionCode
                        Case "M+"
                            If CType(DR(ColumnName), Integer) = ReturnStoreDepositInCode() Then Return True

                        Case "M-"
                            If CType(DR(ColumnName), Integer) = ReturnStoreDepositOutCode() Then Return True

                    End Select

                Case "DESCR"
                    Select Case TransactionCode
                        Case "M+"
                            If CType(DR(ColumnName), String) = ReturnStoreDepositInDescription() Then Return True

                        Case "M-"
                            If CType(DR(ColumnName), String) = ReturnStoreDepositInDescription() Then Return True

                    End Select

                Case "MERC", "NMER", "TAXA", "DISC", "TOTL"
                    If CType(DR(ColumnName), Decimal) = CType(ColumnValue, Decimal) Then Return True

                Case "IEMP"
                    If CType(DR(ColumnName), Boolean) = CType(IIf(ColumnValue = "Y", True, False), Boolean) Then Return True

                Case "CBBU"
                    Dim TempValue As String

                    If IsDBNull(DR(ColumnName)) = True Then
                        TempValue = Space(8)

                    Else
                        TempValue = CType(DR(ColumnName), String).Trim

                    End If

                    If TempValue = ColumnValue Then Return True

                Case Nothing
                    Return False

                Case ""
                    Return False

                Case Else
                    Return False

            End Select

        End If

        Return False

    End Function

#End Region

#Region "DLPAID Stuff"

    Private Function SalePaymentsExistInDatabase(ByVal TransactionDate As Date, _
                                                 ByVal TillID As String, _
                                                 ByVal TransactionNo As String) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("select *                                                        ")
            .Append("from DLPAID                                                     ")
            .Append("where DATE1  = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TILL   = '" & TillID & "'                                 ")
            .Append("and   [TRAN] = '" & TransactionNo & "'                          ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then SalePaymentsExistInDatabase = True

    End Function

    Private Function SalePaymentExistInDatabase(ByVal TransactionDate As Date, _
                                                ByVal TillID As String, _
                                                ByVal TransactionNo As String, _
                                                ByVal LineNumber As Integer) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("select *                                                        ")
            .Append("from DLPAID                                                     ")
            .Append("where DATE1  = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TILL   = '" & TillID & "'                                 ")
            .Append("and   [TRAN] = '" & TransactionNo & "'                          ")
            .Append("and   NUMB   = " & LineNumber.ToString & "                      ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then SalePaymentExistInDatabase = True

    End Function

    Private Function SalePaymentCorrectInDatabase(ByVal TransactionDate As Date, _
                                                  ByVal TillID As String, _
                                                  ByVal TransactionNo As String, _
                                                  ByVal LineNumber As Integer, _
                                                  ByVal ColumnName As String, _
                                                  ByVal ColumnValue As String) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable
        Dim DR As DataRow

        With SB

            .Append("select *                                                        ")
            .Append("from DLPAID                                                     ")
            .Append("where DATE1  = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TILL   = '" & TillID & "'                                 ")
            .Append("and   [TRAN] = '" & TransactionNo & "'                          ")
            .Append("and   NUMB   = " & LineNumber.ToString & "                      ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then

            DR = DT.Rows.Item(0)

            Select Case ColumnName
                Case "DATE1"
                    If DateTime.Compare(CType(DR("DATE1"), Date), CType(ColumnValue, Date)) = 0 Then Return True

                Case "TILL", "TRAN", "CARD", "EXDT"
                    If CType(DR(ColumnName), String).Trim = ColumnValue Then Return True

                Case "NUMB"
                    If CType(DR(ColumnName), Integer) = CType(ColumnValue, Integer) Then Return True

                Case "TYPE", "AMNT"
                    If CType(DR(ColumnName), Decimal) = CType(ColumnValue, Decimal) Then Return True

                Case Nothing
                    Return False

                Case ""
                    Return False

                Case Else
                    Return False

            End Select

        End If

        Return False

    End Function

#End Region

#Region "RETOPT Stuff"

    Private Function ReturnStoreDepositInCode() As Integer

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "select SDIO from RETOPT"
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then ReturnStoreDepositInCode = CType(DT.Rows.Item(0).Item(0), Integer)

    End Function

    Private Function ReturnStoreDepositInDescription() As String

        Dim DT As DataTable

        ReturnStoreDepositInDescription = String.Empty

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "select MPRC20 from RETOPT"
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then ReturnStoreDepositInDescription = CType(DT.Rows.Item(0).Item(0), String)

    End Function

    Private Function ReturnStoreDepositOutCode() As Integer

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "select SDOO from RETOPT"
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then ReturnStoreDepositOutCode = CType(DT.Rows.Item(0).Item(0), Integer)

    End Function

    Private Function ReturnStoreDepositOutDescription() As String

        Dim DT As DataTable

        ReturnStoreDepositOutDescription = String.Empty

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "select MMRC20 from RETOPT"
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then ReturnStoreDepositOutDescription = CType(DT.Rows.Item(0).Item(0), String)

    End Function

#End Region

#End Region

#Region "Non Kitchen Or Bathroom Sale"

    Private Sub NonKitchenOrBathroomSaleAssertionTests(ByVal XmlPacket As XDocument)

        Dim NonKB_Sale As NonKitchenAndBathroom

        'Vision Total
        Dim TransactionStringDate As String
        Dim TransactionDate As Date
        Dim TillID As Integer
        Dim TransactionNo As Integer

        Dim CashierID As Integer
        Dim TransactionTime As String
        Dim TransactionCode As String
        Dim ReasonCode As String
        Dim ReasonDescription As String
        Dim OrderNumber As String
        Dim MerchandiseAmount As Decimal
        Dim NonMerchandiseAmount As Decimal
        Dim TaxAmount As Decimal
        Dim DiscountAmount As Decimal
        Dim TotalAmount As Decimal
        Dim EmployeeDiscount As String
        Dim IsPivotal As String

        'Vision Payment
        Dim TenderTransactionStringDate As String
        Dim TenderTransactionDate As Date
        Dim TenderTillID As Integer
        Dim TenderTransactionNo As Integer
        Dim TenderLineNumber As Integer
        Dim TenderType As Integer
        Dim TenderAmount As Decimal
        Dim TenderIsPivotal As String

        'Vision Line
        Dim LineTransactionStringDate As String
        Dim LineTransactionDate As Date
        Dim LineTillID As Integer
        Dim LineTransactionNo As Integer
        Dim LineLineNumber As Integer
        Dim LineSkuNumber As String
        Dim LineQuantity As Integer
        Dim LinePriceLookup As Decimal
        Dim LinePrice As Decimal
        Dim LinePriceExcludeVAT As Decimal
        Dim LinePriceExtended As Decimal
        Dim LineSkuRelatedItem As String
        Dim LineVatSymbol As String

        Dim LineInStoreStockItem As String
        Dim LineMovementTypeCode As String

        Dim LineHierarchyCategoryCode As String
        Dim LineHierarchyGroupCode As String
        Dim LineHierarchySubGroupCode As String
        Dim LineHierarchyStyleCode As String
        Dim LineSaleType As String
        Dim LineIsPivotal As String

        'store vision total data
        TransactionStringDate = XmlPacket.Descendants("PVTOTS").Elements("DATE").ElementAt(0).Value
        TransactionDate = New Date(CType("20" & TransactionStringDate.Substring(6, 2), Integer), _
                                   CType(TransactionStringDate.Substring(3, 2), Integer), _
                                   CType(TransactionStringDate.Substring(0, 2), Integer))
        TillID = CType(XmlPacket.Descendants("PVTOTS").Elements("TILL").ElementAt(0).Value, Integer)
        TransactionNo = CType(XmlPacket.Descendants("PVTOTS").Elements("TRAN").ElementAt(0).Value, Integer)
        CashierID = CType(XmlPacket.Descendants("PVTOTS").Elements("CASH").ElementAt(0).Value, Integer)
        TransactionTime = XmlPacket.Descendants("PVTOTS").Elements("TIME").ElementAt(0).Value

        TransactionCode = XmlPacket.Descendants("PVTOTS").Elements("TCOD").ElementAt(0).Value
        ReasonCode = XmlPacket.Descendants("PVTOTS").Elements("MISC").ElementAt(0).Value
        ReasonDescription = XmlPacket.Descendants("PVTOTS").Elements("DESC").ElementAt(0).Value
        OrderNumber = XmlPacket.Descendants("PVTOTS").Elements("ORDN").ElementAt(0).Value
        MerchandiseAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("MERC").ElementAt(0).Value, Decimal)
        NonMerchandiseAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("NMER").ElementAt(0).Value, Decimal)
        TaxAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("TAXA").ElementAt(0).Value, Decimal)
        DiscountAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("DISC").ElementAt(0).Value, Decimal)
        TotalAmount = CType(XmlPacket.Descendants("PVTOTS").Elements("TOTL").ElementAt(0).Value, Decimal)
        EmployeeDiscount = XmlPacket.Descendants("PVTOTS").Elements("IEMP").ElementAt(0).Value
        IsPivotal = XmlPacket.Descendants("PVTOTS").Elements("PIVT").ElementAt(0).Value

        ''sale does not exist
        'Assert.IsFalse(VisionTotalExistInDatabase(TransactionDate, TillID, TransactionNo), " VisionTotal already exist".Trim)
        'Assert.IsFalse(VisionPaymentsExistInDatabase(TransactionDate, TillID, TransactionNo), "VisionPayment already exist".Trim)
        'Assert.IsFalse(VisionLinesExistInDatabase(TransactionDate, TillID, TransactionNo), "VisionLine already exist".Trim)

        'create sale
        NonKB_Sale = NonKitchenAndBathroom.Deserialise(XmlPacket.ToString)
        NonKB_Sale.PersistAndUpdateFlashTotals()

        'vision total exist with correct values
        Assert.IsTrue(VisionTotalExistInDatabase(TransactionDate, TillID, TransactionNo), "                                                          VisionTotal record not created".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TranDate", TransactionDate.ToString("dd/MM/yyyy")), "    VisionTotal:TranDate              is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TillId", TillID.ToString), "                             VisionTotal:TillId                is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TranNumber", TransactionNo.ToString), "                  VisionTotal:TranNumber            is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "CashierId", CashierID.ToString), "                       VisionTotal:CashierId             is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "TranDateTime", TransactionTime), "                       VisionTotal:TranDateTime          is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "Type", TransactionCode), "                               VisionTotal:Type                  is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "ReasonCode", ReasonCode), "                              VisionTotal:ReasonCode            is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "ReasonDescription", ReasonDescription), "                VisionTotal:ReasonDescription     is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "OrderNumber", OrderNumber), "                            VisionTotal:OrderNumber           is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "ValueMerchandising", MerchandiseAmount.ToString), "      VisionTotal:ValueMerchandising    is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "ValueNonMerchandising", NonMerchandiseAmount.ToString), "VisionTotal:ValueNonMerchandising is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "ValueTax", TaxAmount.ToString), "                        VisionTotal:ValueTax              is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "ValueDiscount", DiscountAmount.ToString), "              VisionTotal:ValueDiscount         is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "Value", TotalAmount.ToString), "                         VisionTotal:Value                 is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "IsColleagueDiscount", EmployeeDiscount), "               VisionTotal:IsColleagueDiscount   is incorrect".Trim)
        Assert.IsTrue(VisionTotalCorrectInDatabase(TransactionDate, TillID, TransactionNo, "IsPivotal", IsPivotal), "                                VisionTotal:IsPivotal             is incorrect".Trim)

        'vision payments exist with correct values
        For Each TenderNode As XElement In XmlPacket.Descendants("PVPAID").Elements

            TenderTransactionStringDate = TenderNode.Element("DATE").Value
            TenderTransactionDate = New Date(CType("20" & TenderTransactionStringDate.Substring(6, 2), Integer), _
                                             CType(TenderTransactionStringDate.Substring(3, 2), Integer), _
                                             CType(TenderTransactionStringDate.Substring(0, 2), Integer))
            TenderTillID = CType(TenderNode.Element("TILL").Value, Integer)
            TenderTransactionNo = CType(TenderNode.Element("TRAN").Value, Integer)
            TenderLineNumber = CType(TenderNode.Element("NUMB").Value, Integer)
            TenderType = CType(TenderNode.Element("TYPE").Value, Integer)
            TenderAmount = CType(TenderNode.Element("AMNT").Value, Decimal)
            TenderIsPivotal = TenderNode.Element("PIVT").Value

            Assert.IsTrue(VisionPaymentExistInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber), "                                                            VisionPayment record not created".Trim)
            Assert.IsTrue(VisionPaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "TranDate", TenderTransactionDate.ToString("dd/MM/yyyy")), "VisionPayment:TranDate     is incorrect".Trim)
            Assert.IsTrue(VisionPaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "TillId", TenderTillID.ToString), "                         VisionPayment:TillId       is incorrect".Trim)
            Assert.IsTrue(VisionPaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "TranNumber", TenderTransactionNo.ToString), "              VisionPayment:TranNumber   is incorrect".Trim)
            Assert.IsTrue(VisionPaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "Number", TenderLineNumber.ToString), "                     VisionPayment:Number       is incorrect".Trim)
            Assert.IsTrue(VisionPaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "TenderTypeId", TenderType.ToString), "                     VisionPayment:TenderTypeId is incorrect".Trim)
            Assert.IsTrue(VisionPaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "ValueTender", TenderAmount.ToString), "                    VisionPayment:ValueTender  is incorrect".Trim)
            Assert.IsTrue(VisionPaymentCorrectInDatabase(TenderTransactionDate, TenderTillID, TenderTransactionNo, TenderLineNumber, "IsPivotal", TenderIsPivotal), "                            VisionPayment:IsPivotal    is incorrect".Trim)

        Next

        'vision lines exist with correct values
        For Each LineNode As XElement In XmlPacket.Descendants("PVLINE").Elements

            LineTransactionStringDate = LineNode.Element("DATE").Value
            LineTransactionDate = New Date(CType("20" & LineTransactionStringDate.Substring(6, 2), Integer), _
                                           CType(LineTransactionStringDate.Substring(3, 2), Integer), _
                                           CType(LineTransactionStringDate.Substring(0, 2), Integer))
            LineTillID = CType(LineNode.Element("TILL").Value, Integer)
            LineTransactionNo = CType(LineNode.Element("TRAN").Value, Integer)
            LineLineNumber = CType(LineNode.Element("NUMB").Value, Integer)
            LineSkuNumber = LineNode.Element("SKUN").Value
            LineQuantity = CType(LineNode.Element("QUAN").Value, Integer)
            LinePriceLookup = CType(LineNode.Element("SPRI").Value, Decimal)
            LinePrice = CType(LineNode.Element("PRIC").Value, Decimal)
            LinePriceExcludeVAT = CType(LineNode.Element("PRVE").Value, Decimal)
            LinePriceExtended = CType(LineNode.Element("EXTP").Value, Decimal)
            LineSkuRelatedItem = LineNode.Element("RITM").Value
            LineVatSymbol = LineNode.Element("VSYM").Value
            LineInStoreStockItem = LineNode.Element("PIVI").Value
            LineMovementTypeCode = LineNode.Element("PIVM").Value
            LineHierarchyCategoryCode = LineNode.Element("CTGY").Value
            LineHierarchyGroupCode = LineNode.Element("GRUP").Value
            LineHierarchySubGroupCode = LineNode.Element("SGRP").Value
            LineHierarchyStyleCode = LineNode.Element("STYL").Value
            LineSaleType = LineNode.Element("SALT").Value
            LineIsPivotal = LineNode.Element("PIVT").Value

            Assert.IsTrue(VisionLineExistInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber), "                                                          VisionLine record not created".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "TranDate", LineTransactionDate.ToString("dd/MM/yyyy")), "VisionLine:TranDate            is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "TillId", LineTillID.ToString), "                         VisionLine:TillId              is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "TranNumber", LineTransactionNo.ToString), "              VisionLine:TranNumber          is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "Number", LineLineNumber.ToString), "                     VisionLine:Number              is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "SkuNumber", LineSkuNumber), "                            VisionLine:SkuNumber           is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "Quantity", LineQuantity.ToString), "                     VisionLine:Quantity            is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "PriceLookup", LinePriceLookup.ToString), "               VisionLine:PriceLookup         is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "Price", LinePrice.ToString), "                           VisionLine:Price               is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "PriceExVat", LinePriceExcludeVAT.ToString), "            VisionLine:PriceExVat          is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "PriceExtended", LinePriceExtended.ToString), "           VisionLine:PriceExtended       is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "IsRelatedItemSingle", LineSkuRelatedItem), "             VisionLine:IsRelatedItemSingle is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "VatSymbol", LineVatSymbol), "                            VisionLine:VatSymbol           is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "IsInStoreStockItem", LineInStoreStockItem), "            VisionLine:IsInStoreStockItem  is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "MovementTypeCode", LineMovementTypeCode), "              VisionLine:MovementTypeCode    is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "HieCategory", LineHierarchyCategoryCode), "              VisionLine:HieCategory         is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "HieGroup", LineHierarchyGroupCode), "                    VisionLine:HieGroup            is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "HieSubgroup", LineHierarchySubGroupCode), "             VisionLine:HierSubgroup        is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "HieStyle", LineHierarchyStyleCode), "                    VisionLine:HieStyle            is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "SaleType", LineSaleType), "                              VisionLine:SaleType            is incorrect".Trim)
            Assert.IsTrue(VisionLineCorrectInDatabase(LineTransactionDate, LineTillID, LineTransactionNo, LineLineNumber, "IsPivotal", LineIsPivotal), "                            VisionLine:IsPivotal           is incorrect".Trim)

        Next

        'stock exist with correct values
        For Each StockNode As XElement In XmlPacket.Descendants("STOCK").Elements

            'do nothing

        Next

        ''delete sale
        'NonKitchenOrBathroomSaleDeleteFromDatabase(TransactionDate, TillID, TransactionNo)

    End Sub

    Private Sub NonKitchenOrBathroomSaleDeleteFromDatabase(ByVal TransactionDate As Date, _
                                                           ByVal TillID As Integer, _
                                                           ByVal TransactionNo As Integer)

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("delete VisionTotal                                                  ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        With SB
            .Remove(0, .Length)

            .Append("delete VisionPayment                                                ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")

        End With

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        With SB
            .Remove(0, .Length)

            .Append("delete VisionLine                                                   ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")

        End With

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

    End Sub

#Region "Vision Total Stuff"

    Private Function VisionTotalExistInDatabase(ByVal TransactionDate As Date, _
                                                ByVal TillID As Integer, _
                                                ByVal TransactionNo As Integer) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("select *                                                            ")
            .Append("from VisionTotal                                                    ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then VisionTotalExistInDatabase = True

    End Function

    Private Function VisionTotalCorrectInDatabase(ByVal TransactionDate As Date, _
                                                  ByVal TillID As Integer, _
                                                  ByVal TransactionNo As Integer, _
                                                  ByVal ColumnName As String, _
                                                  ByVal ColumnValue As String) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable
        Dim DR As DataRow

        With SB

            .Append("select *                                                            ")
            .Append("from VisionTotal                                                    ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then

            DR = DT.Rows.Item(0)

            Select Case ColumnName
                Case "TranDate"
                    If DateTime.Compare(CType(DR("TranDate"), Date), CType(ColumnValue, Date)) = 0 Then Return True

                Case "Type", "ReasonCode", "ReasonDescription", "OrderNumber"
                    If CType(DR(ColumnName), String).Trim = ColumnValue Then Return True

                Case "TranDateTime"
                    If CType(DR(ColumnName), String) = CType(ColumnValue.Substring(0, 2) & ":" & _
                                                             ColumnValue.Substring(2, 2) & ":" & _
                                                             ColumnValue.Substring(4, 2), String) Then Return True
                Case "TillId", "TranNumber", "CashierId"
                    If CType(DR(ColumnName), Integer) = CType(ColumnValue, Integer) Then Return True

                Case "Value", "ValueMerchandising", "ValueNonMerchandising", "ValueTax", "ValueDiscount"
                    If CType(DR(ColumnName), Decimal) = CType(ColumnValue, Decimal) Then Return True

                Case "IsColleagueDiscount", "IsPivotal"
                    If CType(DR(ColumnName), Boolean) = CType(IIf(ColumnValue = "Y", True, False), Boolean) Then Return True

                Case Nothing
                    Return False

                Case ""
                    Return False

                Case Else
                    Return False

            End Select

        End If

        Return False

    End Function

#End Region

#Region "Vision Payment Stuff"

    Private Function VisionPaymentsExistInDatabase(ByVal TransactionDate As Date, _
                                                   ByVal TillID As Integer, _
                                                   ByVal TransactionNo As Integer) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("select *                                                            ")
            .Append("from VisionPayment                                                  ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then VisionPaymentsExistInDatabase = True

    End Function

    Private Function VisionPaymentExistInDatabase(ByVal TransactionDate As Date, _
                                                      ByVal TillID As Integer, _
                                                      ByVal TransactionNo As Integer, _
                                                      ByVal LineNumber As Integer) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("select *                                                            ")
            .Append("from VisionPayment                                                  ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")
            .Append("and   Number     = " & LineNumber.ToString & "                      ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then VisionPaymentExistInDatabase = True

    End Function

    Private Function VisionPaymentCorrectInDatabase(ByVal TransactionDate As Date, _
                                                    ByVal TillID As Integer, _
                                                    ByVal TransactionNo As Integer, _
                                                    ByVal LineNumber As Integer, _
                                                    ByVal ColumnName As String, _
                                                    ByVal ColumnValue As String) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable
        Dim DR As DataRow

        With SB

            .Append("select *                                                            ")
            .Append("from VisionPayment                                                  ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")
            .Append("and   Number     = " & LineNumber.ToString & "                      ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then

            DR = DT.Rows.Item(0)

            Select Case ColumnName

                Case "TranDate"
                    If DateTime.Compare(CType(DR("TranDate"), Date), CType(ColumnValue, Date)) = 0 Then Return True

                Case "TillId", "TranNumber", "Number", "TenderTypeId"
                    If CType(DR(ColumnName), Integer) = CType(ColumnValue, Integer) Then Return True

                Case "ValueTender"
                    If CType(DR(ColumnName), Decimal) = CType(ColumnValue, Decimal) Then Return True

                Case "IsPivotal"
                    If CType(DR(ColumnName), Boolean) = CType(IIf(ColumnValue = "Y", True, False), Boolean) Then Return True

                Case Nothing
                    Return False

                Case ""
                    Return False

                Case Else
                    Return False

            End Select

        End If

        Return False

    End Function

#End Region

#Region "Vision Line Stuff"

    Private Function VisionLinesExistInDatabase(ByVal TransactionDate As Date, _
                                                ByVal TillID As Integer, _
                                                ByVal TransactionNo As Integer) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("select *                                                            ")
            .Append("from VisionLine                                                     ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then VisionLinesExistInDatabase = True

    End Function

    Private Function VisionLineExistInDatabase(ByVal TransactionDate As Date, _
                                               ByVal TillID As Integer, _
                                               ByVal TransactionNo As Integer, _
                                               ByVal LineNumber As Integer) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB

            .Append("select *                                                            ")
            .Append("from VisionLine                                                     ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")
            .Append("and   Number     = " & LineNumber.ToString & "                      ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then VisionLineExistInDatabase = True

    End Function

    Private Function VisionLineCorrectInDatabase(ByVal TransactionDate As Date, _
                                                 ByVal TillID As Integer, _
                                                 ByVal TransactionNo As Integer, _
                                                 ByVal LineNumber As Integer, _
                                                 ByVal ColumnName As String, _
                                                 ByVal ColumnValue As String) As Boolean

        Dim SB As New StringBuilder
        Dim DT As DataTable
        Dim DR As DataRow

        With SB

            .Append("select *                                                            ")
            .Append("from VisionLine                                                     ")
            .Append("where TranDate   = '" & TransactionDate.ToString("yyyy-MM-dd") & "' ")
            .Append("and   TillId     = " & TillID.ToString & "                          ")
            .Append("and   TranNumber = " & TransactionNo.ToString & "                   ")
            .Append("and   Number     = " & LineNumber.ToString & "                      ")

        End With

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 Then

            DR = DT.Rows.Item(0)

            Select Case ColumnName

                Case "TranDate"
                    If DateTime.Compare(CType(DR("TranDate"), Date), CType(ColumnValue, Date)) = 0 Then Return True

                Case "TillId", "TranNumber", "Number", "Quantity"
                    If CType(DR(ColumnName), Integer) = CType(ColumnValue, Integer) Then Return True

                Case "SkuNumber", "VatSymbol", "HieCategory", "HieGroup", "HieSubgroup", "HieStyle", "SaleType", "MovementTypeCode"
                    If CType(DR(ColumnName), String).Trim = CType(ColumnValue, String) Then Return True

                Case "PriceLookup", "Price", "PriceExVat", "PriceExtended"
                    If CType(DR(ColumnName), Decimal) = CType(ColumnValue, Decimal) Then Return True

                Case "IsRelatedItemSingle", "IsInStoreStockItem", "IsPivotal"
                    If CType(DR(ColumnName), Boolean) = CType(IIf(ColumnValue = "Y", True, False), Boolean) Then Return True

                Case Nothing
                    Return False

                Case ""
                    Return False

                Case Else
                    Return False

            End Select

        End If

        Return False

    End Function

#End Region

#End Region

#Region "Main Application"

    Private Sub CreateEnvironmentForImportVisionSales(ByVal VisionSales As List(Of XDocument))

        ConfigureVisionDirectories()

        RemoveVisionSaleFiles(VisionSales)

        CreateNewVisionSales(VisionSales)

    End Sub

    Private Sub ConfigureVisionDirectories()

        Dim TempPath As String

        TempPath = VisionInputPath()
        If IO.Directory.Exists(TempPath) = False Then IO.Directory.CreateDirectory(TempPath)

        TempPath = VisionOutputPath()
        If IO.Directory.Exists(TempPath) = False Then IO.Directory.CreateDirectory(TempPath)

        TempPath = VisionErrorPath()
        If IO.Directory.Exists(TempPath) = False Then IO.Directory.CreateDirectory(TempPath)

    End Sub

    Private Sub RemoveVisionSaleFiles(ByVal VisionSales As List(Of XDocument))

        Dim TempPath As String
        Dim FileName As String

        For Each VisionSale As XDocument In VisionSales

            FileName = CreateVisionSaleFileName(VisionSale)

            TempPath = VisionInputPath()
            IfFileExistDelete(TempPath & "\" & FileName)

            TempPath = VisionOutputPath()
            IfFileExistDelete(TempPath & "\" & FileName)

            TempPath = VisionErrorPath()
            IfFileExistDelete(TempPath & "\" & FileName)

        Next

    End Sub

    Private Function VisionRootPath() As String

        Dim ConnectionXML As XmlDocument = New XmlDocument()

        ConnectionXML.Load(ConnectionXmlFilePath & "Connection.xml")

        VisionRootPath = String.Empty

        For Each node As XmlNode In ConnectionXML.SelectNodes("configuration/path")
            If node.Attributes.GetNamedItem("name").Value = "CommunicationsPath" Then

                VisionRootPath = node.Attributes.GetNamedItem("value").Value

            End If
        Next

    End Function

    Private Function VisionInputPath() As String

        Return VisionRootPath() & "\" & "VISIN"

    End Function

    Private Function VisionOutputPath() As String

        Return VisionRootPath() & "\" & "VISDONE"

    End Function

    Private Function VisionErrorPath() As String

        Return VisionRootPath() & "\" & "VISERR"

    End Function

    Private Sub IfFileExistDelete(ByVal PathAndFilename As String)

        If IO.File.Exists(PathAndFilename) Then IO.File.Delete(PathAndFilename)

    End Sub

    Private Function IfFileExist(ByVal PathAndFilename As String) As Boolean

        If IO.File.Exists(PathAndFilename) Then Return True

    End Function

    Private Sub CreateNewVisionSales(ByVal VisionSales As List(Of XDocument))

        Dim TempPath As String
        Dim FileName As String

        For Each VisionSale As XDocument In VisionSales

            FileName = CreateVisionSaleFileName(VisionSale)

            TempPath = VisionInputPath()
            CreateFile(TempPath & "\" & FileName, VisionSale)

        Next

    End Sub

    Private Function CreateVisionSaleFileName(ByVal VisionSale As XDocument) As String

        Return "CTS" & VisionSale.Descendants("PVTOTS").Elements("TRAN").ElementAt(0).Value & ".XML"

    End Function

    Private Sub CreateFile(ByVal PathAndFilename As String, ByVal VisionSale As XDocument)

        Dim FileContent As IO.StreamWriter

        FileContent = IO.File.CreateText(PathAndFilename)
        FileContent.Write(VisionSale.ToString)
        FileContent.Flush()
        FileContent.Close()

    End Sub

    Private Sub RunImportVisionSales(ByVal WorkingDirectory As String, ByVal ExecutableFileName As String, ByVal Arguments As String)

        Dim NewProcess As Diagnostics.Process = New System.Diagnostics.Process()

        With NewProcess

            .StartInfo.WorkingDirectory = WorkingDirectory
            .StartInfo.FileName = WorkingDirectory & ExecutableFileName
            .StartInfo.Arguments = Arguments
            .StartInfo.WindowStyle = Diagnostics.ProcessWindowStyle.Normal
            .StartInfo.UseShellExecute = False
            .EnableRaisingEvents = True
            .Start()
            .WaitForExit()

        End With

    End Sub

    Private Function VisionDirectoryEmpty(ByVal Location As VisionLocation, ByVal VisionSales As List(Of XDocument)) As Boolean

        Dim TempPath As String
        Dim FileName As String

        VisionDirectoryEmpty = True

        For Each VisionSale As XDocument In VisionSales
            TempPath = String.Empty

            Select Case Location
                Case VisionLocation.VisIn
                    TempPath = VisionInputPath()

                Case VisionLocation.VisErr
                    TempPath = VisionErrorPath()

                Case VisionLocation.VisOut
                    TempPath = VisionOutputPath()

            End Select

            FileName = CreateVisionSaleFileName(VisionSale)

            If IfFileExist(TempPath & "\" & FileName) = True Then VisionDirectoryEmpty = False

        Next

    End Function

    Private Function AllVisionSalesExistInDirectory(ByVal Location As VisionLocation, ByVal VisionSales As List(Of XDocument)) As Boolean

        Dim TempPath As String
        Dim FileName As String

        AllVisionSalesExistInDirectory = True

        For Each VisionSale As XDocument In VisionSales

            TempPath = String.Empty

            Select Case Location
                Case VisionLocation.VisIn
                    TempPath = VisionInputPath()

                Case VisionLocation.VisErr
                    TempPath = VisionErrorPath()

                Case VisionLocation.VisOut
                    TempPath = VisionOutputPath()

            End Select



            FileName = CreateVisionSaleFileName(VisionSale)

            If IfFileExist(TempPath & "\" & FileName) = False Then AllVisionSalesExistInDirectory = False

        Next

    End Function

    Private Sub CleanUpEnvironmentAfterImportVisionSales(ByVal VisionSales As List(Of XDocument))

        RemoveVisionSaleFiles(VisionSales)

    End Sub

    'Private Sub CleanUpDatabaseAfterImportVisionSales(ByVal VisionSales As List(Of XDocument))


    'End Sub

#End Region

#Region "Rest"

    Private Function RequirementSwitch(ByVal BooleanValue As Boolean) As Boolean

        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)

                Select Case BooleanValue
                    Case True
                        com.CommandText = "update Parameters set BooleanValue = 1 where ParameterID = -15"

                    Case False
                        com.CommandText = "update Parameters set BooleanValue = 0 where ParameterID = -15"

                End Select

                com.ExecuteNonQuery()

            End Using
        End Using

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = "select * from Parameters where ParameterID = -15"
                DT = com.ExecuteDataTable

            End Using
        End Using

        If DT IsNot Nothing AndAlso DT.Rows.Count = 1 AndAlso CType(DT.Rows.Item(0).Item("BooleanValue"), Boolean) = BooleanValue Then RequirementSwitch = True

    End Function

#End Region

#End Region

End Class