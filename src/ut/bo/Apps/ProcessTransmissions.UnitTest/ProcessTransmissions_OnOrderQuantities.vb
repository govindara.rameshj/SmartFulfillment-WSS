﻿<TestClass()> Public Class ProcessTransmissions_OnOrderQuantities

    Private testContextInstance As TestContext

    'Partha's version
    Private Const WORKING_DIRECTORY As String = "..\..\..\..\Staging Area"
    Private Const FILENAME As String = "..\..\..\..\Staging Area\ProcessTransmissions.exe"

    'Common
    Private Const ARGUMENTS As String = "OO"
    Private Const STHOO_PATH As String = "C:\WIX\COMMS\STHOO"
    Private Const STHOO_DIRECTORY As String = "C:\WIX\COMMS"
    Private Const STOREDPROCEDURENAME As String = "usp_OnOrderProcessTransmissions"

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '


    <ClassInitialize()> _
   Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        'Assert.IsTrue(InitialiseTests(testContext), "Failed initialising for the " & SafeGetTestContextProperty(testContext, TC_KEY_ASSEMBLYNAME, GetType(String)) & " Stored Procedures tests.")
                    Case Else
                        Assert.Fail("Cannot perform Stored Procedures tests as not connected to a SQL Server database")
                End Select
            End Using
        End Using
    End Sub
#End Region

#Region "Stored Procedure - Unit Test"

    <TestMethod()> <Ignore()> Public Sub Ireplen_StoredProcedure_RecordCountMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProecdureDT As DataTable

        DynamicSequelDT = IreplenDynamicSequelSelect()
        StoredProecdureDT = IreplenStoredProcedureSelect()

        Assert.AreEqual(DynamicSequelDT.Rows.Count, StoredProecdureDT.Rows.Count)

    End Sub

    <TestMethod()> Public Sub Ireplen_StoredProcedure_FieldMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProecdureDT As DataTable
        Dim AllColumnNamesMatch As Boolean

        DynamicSequelDT = IreplenDynamicSequelSelect()
        StoredProecdureDT = IreplenStoredProcedureSelect()

        AllColumnNamesMatch = True
        For ColumnIndex As Integer = 0 To DynamicSequelDT.Columns.Count - 1 Step 1

            If DynamicSequelDT.Columns.Item(ColumnIndex).ColumnName <> _
               StoredProecdureDT.Columns.Item(ColumnIndex).ColumnName Then AllColumnNamesMatch = False

        Next

        Assert.IsTrue(AllColumnNamesMatch)

    End Sub

    <TestMethod()> <Ignore()> Public Sub Ireplen_StoredProcedure_DataMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProecdureDT As DataTable
        Dim DataMatch As Boolean
        Dim ColumnCount As Integer

        DynamicSequelDT = IreplenDynamicSequelSelect()
        StoredProecdureDT = IreplenStoredProcedureSelect()

        DataMatch = True
        ColumnCount = DynamicSequelDT.Columns.Count - 1
        For RowIndex As Integer = 0 To DynamicSequelDT.Rows.Count - 1 Step 1

            For ColumnIndex As Integer = 0 To ColumnCount Step 1

                Select Case DynamicSequelDT.Columns.Item(ColumnIndex).DataType.FullName
                    Case "System.Int32"
                        If CType(DynamicSequelDT.Rows.Item(RowIndex).Item(ColumnIndex), Integer) <> _
                           CType(StoredProecdureDT.Rows.Item(RowIndex).Item(ColumnIndex), Integer) Then DataMatch = False

                    Case "System.String"
                        If CType(DynamicSequelDT.Rows.Item(RowIndex).Item(ColumnIndex), String) <> _
                           CType(StoredProecdureDT.Rows.Item(RowIndex).Item(ColumnIndex), String) Then DataMatch = False

                End Select

            Next

        Next

        Assert.IsTrue(DataMatch)

    End Sub

#Region "Private Procedures And Functions"

    Private Function IreplenDynamicSequelSelect() As DataTable

        Dim SB As New StringBuilder

        SB.Append("select SkuNumber             = SKUN, ")
        SB.Append("       PrimarySupplierNumber = SUPP, ")
        SB.Append("       OnOrderQuantity       = ONOR  ")
        SB.Append("from STKMAS                          ")
        SB.Append("where IOBS = 0                       ")
        SB.Append("and   IDEL = 0                       ")
        SB.Append("and   INON = 0                       ")

        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                Return com.ExecuteDataTable

            End Using
        End Using
        Return Nothing

    End Function

    Private Function IreplenStoredProcedureSelect() As DataTable

        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = "usp_OnOrderProcessTransmissions"
                Return com.ExecuteDataTable

            End Using
        End Using
        Return Nothing

    End Function

#End Region

#End Region

    '<TestMethod()> Public Sub XXX()

    '    'Dim CurrentProcess As Process = Process.GetCurrentProcess()


    '    ''CurrentProcess.StartInfo.WorkingDirectory = WORKING_DIRECTORY
    '    'CurrentProcess.StartInfo.Arguments = ARGUMENTS
    '    ''CurrentProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
    '    'ProcessTransmissions.MainModule.Main()



    '    'System.AppDomain.CurrentDomain.BaseDirectory


    '    'StockMaster.Weight.Value = CDec(strTransmissionFileData.Substring(205, 7).PadLeft(7, "0")) / 100

    '    Dim strTransmissionFileData As String = "U418/10/11     619.00 30841036190000855181011000000  51902103619    050EACH050000    889000000000000Sawn Treated Kiln Dry C16 47x100mmx3.6m Sawn Treated Kiln Dr       11810110000011291 0000000             00009300000000N00005000000000000000000000NY  0.00 18/12/02  /  /          /  /    /  /   00001512500112500912501905905010N0000 N0003.600000 M      B NN N000NNN000000"

    '    'Assert.AreEqual(CDec(0.75), strTransmissionFileData.PadLeft(7, "0") / 100, "test a")
    '    'Assert.AreEqual(CDec(0.75), CDec(strTransmissionFileData.Substring(205, 7).PadLeft(7, "0")) / 100, "test b")


    '    strTransmissionFileData = "U418/10/11     003.00 30841070037000183181011000000S 51902107003    001EACH001000    889107002000000Planed Softwood 12x44mmx2.4m Single     Planed Softwood 12x4       11810110001011291 0000000             00000570000000N00000000000000000000000000NY  0.00 01/11/04  /  /          /  /    /  /   00001512500312503412503705905010N0000 N0002.400000 M      B NN N000NNN000000"


    '    'strTransmissionFileData.Substring(205, 7).PadLeft(7, "0")
    '    Assert.AreEqual("4", strTransmissionFileData.Substring(1, 1), "test qqqq")


    '    Assert.AreEqual(" 000005", strTransmissionFileData.Substring(205, 7), "test xx")


    '    Assert.AreEqual(CDec(0.75), CDec(strTransmissionFileData.Substring(205, 7).PadLeft(7, "0")) / 100, "test b")

    'End Sub

    ' Move to Fitnesse 
    <Ignore()>
    <TestMethod()> Public Sub STHOOFileCreatedOK()
        'Arrange

        Dim newProc As Diagnostics.Process = New System.Diagnostics.Process()
        Dim boolSucess As Boolean
        newProc.StartInfo.WorkingDirectory = WORKING_DIRECTORY
        newProc.StartInfo.FileName = FILENAME
        newProc.StartInfo.Arguments = ARGUMENTS
        newProc.StartInfo.WindowStyle = ProcessWindowStyle.Normal
        newProc.StartInfo.UseShellExecute = False
        newProc.EnableRaisingEvents = True

        'Act
        newProc.Start()
        newProc.WaitForExit()

        'Assert
        If IO.Directory.Exists(STHOO_DIRECTORY) Then
            If IO.File.Exists(STHOO_PATH) Then
                boolSucess = True
            End If
        End If
        Assert.IsTrue(boolSucess)

    End Sub

    <TestMethod()> Public Sub STHOOStoredProcedureExistsOK()
        'Arrange
        Dim boolSucess As Boolean

        'Act
        boolSucess = CheckForStoredProcedure()

        'Assert
        Assert.IsTrue(boolSucess)

    End Sub

    Private Function CheckForStoredProcedure() As Boolean

        Try
            Using con As New Connection
                Using com As New Command(con)
                    Dim spCheck As DataTable

                    com.CommandText = "Select * From sys.objects Where type = 'P' And name = '" & STOREDPROCEDURENAME & "'"
                    spCheck = com.ExecuteDataTable
                    If spCheck IsNot Nothing AndAlso spCheck.Rows.Count = 1 Then
                        CheckForStoredProcedure = True
                    Else
                        CheckForStoredProcedure = False
                    End If
                End Using
            End Using
        Catch ex As Exception
            Assert.Inconclusive("Initialisation for tests failed.  Failed in the attempt to check for existence of the stored procedure (" & STOREDPROCEDURENAME & ".")
        End Try
    End Function

End Class