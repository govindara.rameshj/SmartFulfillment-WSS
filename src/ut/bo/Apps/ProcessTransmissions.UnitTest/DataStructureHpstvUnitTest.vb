﻿Imports ProcessTransmissions.HPSTV.Classes

<TestClass()> Public Class DataStructureHpstvUnitTest

    Private testContextInstance As TestContext

    Private _Record As HPSTV.Interface.IVtRecord

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub

    <TestInitialize()> Public Sub MyTestInitialize()

        _Record = New VtRecord("VT16/06/10      21.00 N0115501/08/10N534W 21   3   4   3   5   4   5   4 ")

    End Sub

    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub RecordType()

        Assert.AreEqual("VT", _Record.RecordType)

    End Sub

    <TestMethod()> Public Sub UpdateDate()

        Assert.AreEqual("16/06/10", _Record.UpdateDate)

    End Sub

    <TestMethod()> Public Sub Hash()

        Assert.AreEqual("21.00", _Record.Hash)

    End Sub

    <TestMethod()> Public Sub DeletionIndicator()

        Assert.AreEqual("N", _Record.DeletionIndicator)

    End Sub

    <TestMethod()> Public Sub SupplierNumber()

        Assert.AreEqual("01155", _Record.SupplierNumber)

    End Sub

    <TestMethod()> Public Sub EffectiveDate()

        Assert.AreEqual("01/08/10", _Record.EffectiveDate)

    End Sub

    <TestMethod()> Public Sub OrderType()

        Assert.AreEqual("N", _Record.OrderType)

    End Sub

    <TestMethod()> Public Sub BbcSiteNumber()

        Assert.AreEqual("534", _Record.BbcSiteNumber)

    End Sub

    <TestMethod()> Public Sub BbcOrderType()

        Assert.AreEqual("W", _Record.BbcOrderType)

    End Sub

    <TestMethod()> Public Sub OrderDayCode()

        Assert.AreEqual("21", _Record.OrderDayCode)

    End Sub

    <TestMethod()> Public Sub LeadTimeMonday()

        Assert.AreEqual("3", _Record.LeadTimeMonday)

    End Sub

    <TestMethod()> Public Sub LeadTimeTuesday()

        Assert.AreEqual("4", _Record.LeadTimeTuesday)

    End Sub

    <TestMethod()> Public Sub LeadTimeWednesday()

        Assert.AreEqual("3", _Record.LeadTimeWednesday)

    End Sub

    <TestMethod()> Public Sub LeadTimeThursday()

        Assert.AreEqual("5", _Record.LeadTimeThursday)

    End Sub

    <TestMethod()> Public Sub LeadTimeFriday()

        Assert.AreEqual("4", _Record.LeadTimeFriday)

    End Sub

    <TestMethod()> Public Sub LeadTimeSaturday()

        Assert.AreEqual("5", _Record.LeadTimeSaturday)

    End Sub

    <TestMethod()> Public Sub LeadTimeSunday()

        Assert.AreEqual("4", _Record.LeadTimeSunday)

    End Sub

    <TestMethod()> Public Sub DeliveryCheckMethod()

        Assert.AreEqual("", _Record.DeliveryCheckMethod)

    End Sub

End Class