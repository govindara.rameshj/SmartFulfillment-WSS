﻿Imports System.Collections.Generic
Imports OasysDBBO.Oasys3.DB
Imports BOPurchases
Imports OasysDBBO
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports ProcessTransmissions



'''<summary>
'''This is a test class for STHPOUnitTest and is intended
'''to contain all STHPOUnitTest Unit Tests
'''</summary>
<TestClass()> _
Public Class STHPOUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

#Region "GetPurchaseLines Tests"
    '''<summary>
    '''A test for GetPurchaseLines
    '''</summary>
    <TestMethod()> _
    Public Sub GetPurchaseLinesTest_UsesRetrievePurchaseOrderDetailLinesOverride()
        Dim target As ISTHPO = New TestSTHPO_ListOf1PurchaseLineWithIdOf999
        Dim expected As List(Of cPurchaseLine) = New List(Of BOPurchases.cPurchaseLine)
        Dim NewLine As New cPurchaseLine()
        Dim actual As List(Of cPurchaseLine)

        NewLine.PurchaseLineID = New ColField(Of Integer)("TestName", 999)
        expected.Add(NewLine)
        actual = target.GetPurchaseLines(New cPurchaseHeader(), New clsOasys3DB())
        Assert.IsTrue(actual IsNot Nothing AndAlso actual.Count = 1 AndAlso actual(0).PurchaseLineID.Value = 999, "GetPurchaseLines failed to use the overridable RetrievePurchaseOrderDetailLines method")
    End Sub
#End Region

#Region "FormatODSkuNumber Tests"
    '''<summary>
    '''A test for FormatODSkuNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODSkuNumberTest_FieldValueOfEmptyString_ReturnsMinimumFormatLengthWithValue0()
        Dim target As ISTHPO = New STHPO
        Dim SkuNumber As ColField(Of String) = New ColField(Of String)("TestName", String.Empty)
        Dim expected As String = "000000"
        Dim actual As String

        actual = target.FormatODSkuNumber(SkuNumber)
        Assert.AreEqual(expected, actual, "Empty ODSkuNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODSkuNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODSkuNumberTest_FieldValueOfBlankString_MinimumFormatLengthWithValue0()
        Dim target As ISTHPO = New STHPO
        Dim SkuNumber As ColField(Of String) = New ColField(Of String)("TestName", "")
        Dim expected As String = "000000"
        Dim actual As String

        actual = target.FormatODSkuNumber(SkuNumber)
        Assert.AreEqual(expected, actual, "Blank ODSkuNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODSkuNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODSkuNumberTest_FieldValueOfBelowMinimumFormatLength_ReturnsMinimumFormatLengthWithSameIntrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim SkuNumber As ColField(Of String) = New ColField(Of String)("TestName", "1")
        Dim expected As String = "000001"
        Dim actual As String

        actual = target.FormatODSkuNumber(SkuNumber)
        Assert.AreEqual(expected, actual, "Below minimum format length ODSkuNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODSkuNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODSkuNumberTest_FieldValueOfMinimumFormatLength_ReturnsMinimumFormatLengthWithSameIntrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim SkuNumber As ColField(Of String) = New ColField(Of String)("TestName", "123456")
        Dim expected As String = "123456"
        Dim actual As String

        actual = target.FormatODSkuNumber(SkuNumber)
        Assert.AreEqual(expected, actual, "Minimum format length ODSkuNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODSkuNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODSkuNumberTest_FieldValueExceedingMinimumFormatLength_ReturnsSameLengthStringWithSameIntrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim SkuNumber As ColField(Of String) = New ColField(Of String)("TestName", "1234567")
        Dim expected As String = "1234567"
        Dim actual As String

        actual = target.FormatODSkuNumber(SkuNumber)
        Assert.AreEqual(expected, actual, "ODSkuNumber exceeding minimum format length is not correctly formatted")
    End Sub
#End Region

#Region "FormatODPurchaseOrderNumber Tests"

    '''<summary>
    '''A test for FormatODPurchaseOrderNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODPurchaseOrderNumberTest_FieldValueOfEmptyString_ReturnsMinimumFormatLengthWithValue0()
        Dim target As ISTHPO = New STHPO
        Dim PurchaseOrderNumber As ColField(Of String) = New ColField(Of String)("TestName", String.Empty)
        Dim expected As String = "000000"
        Dim actual As String

        actual = target.FormatODPurchaseOrderNumber(PurchaseOrderNumber)
        Assert.AreEqual(expected, actual, "Empty ODPurchaseOrderNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODPurchaseOrderNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODPurchaseOrderNumberTest_FieldValueOfBlankString_ReturnsMinimumFormatLengthWithValue0()
        Dim target As ISTHPO = New STHPO
        Dim PurchaseOrderNumber As ColField(Of String) = New ColField(Of String)("TestName", "")
        Dim expected As String = "000000"
        Dim actual As String

        actual = target.FormatODPurchaseOrderNumber(PurchaseOrderNumber)
        Assert.AreEqual(expected, actual, "Blank ODPurchaseOrderNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODPurchaseOrderNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODPurchaseOrderNumberTest_FieldValueOfBelowMinimumFormatLength_ReturnsMinimumFormatLengthWithSameIntrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim PurchaseOrderNumber As ColField(Of String) = New ColField(Of String)("TestName", "1")
        Dim expected As String = "000001"
        Dim actual As String

        actual = target.FormatODPurchaseOrderNumber(PurchaseOrderNumber)
        Assert.AreEqual(expected, actual, "Below minimum format length ODPurchaseOrderNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODPurchaseOrderNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODPurchaseOrderNumberTest_FieldValueOfMinimumFormatLength_ReturnsMinimumFormatLengthWithSameIntrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim PurchaseOrderNumber As ColField(Of String) = New ColField(Of String)("TestName", "123456")
        Dim expected As String = "123456"
        Dim actual As String

        actual = target.FormatODPurchaseOrderNumber(PurchaseOrderNumber)
        Assert.AreEqual(expected, actual, "Minimum format length ODPurchaseOrderNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODPurchaseOrderNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODPurchaseOrderNumberTest_FieldValueExceedingMinimumFormatLength_ReturnsSameLengthStringWithSameIntrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim PurchaseOrderNumber As ColField(Of String) = New ColField(Of String)("TestName", "1234567")
        Dim expected As String = "1234567"
        Dim actual As String

        actual = target.FormatODPurchaseOrderNumber(PurchaseOrderNumber)
        Assert.AreEqual(expected, actual, "ODPurchaseOrderNumber exceeding minimum format length is not correctly formatted")
    End Sub
#End Region

#Region "FormatODLineNumber Tests"

    '''<summary>
    '''A test for FormatODLineNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODLineNumberTest_LineNumberZero_ReturnsMinimumFormatLengthWithSameINtrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim LineNumber As Integer = 0
        Dim expected As String = "0000"
        Dim actual As String

        actual = target.FormatODLineNumber(LineNumber)
        Assert.AreEqual(expected, actual, "ODLineNumber of value 0 is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODLineNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODLineNumberTest_LineNumberOfMinimumFormatLength_ReturnsMinimumFormatLengthWithSameIntrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim LineNumber As Integer = 1234
        Dim expected As String = "1234"
        Dim actual As String

        actual = target.FormatODLineNumber(LineNumber)
        Assert.AreEqual(expected, actual, "Minimum format length ODLineNumber is not correctly formatted")
    End Sub

    '''<summary>
    '''A test for FormatODLineNumber
    '''</summary>
    <TestMethod()> _
    Public Sub FormatODLineNumberTest_FieldValueExceedingMinimumFormatLength_ReturnsSameLengthStringWithSameIntrinsicValue()
        Dim target As ISTHPO = New STHPO
        Dim LineNumber As Integer = 12345
        Dim expected As String = "12345"
        Dim actual As String

        actual = target.FormatODLineNumber(LineNumber)
        Assert.AreEqual(expected, actual, "ODLineNumber exceeding minimum format length is not correctly formatted")
    End Sub
#End Region
End Class

Friend Class TestSTHPO_ListOf1PurchaseLineWithIdOf999
    Inherits STHPO

    Friend Overrides Function RetrievePurchaseOrderDetailLines(ByRef PurchaseOrderDetail As BOPurchases.cPurchaseLine, ByRef HeaderIdentityFieldID As OasysDBBO.ColField(Of Integer), ByVal HeaderId As Integer) As System.Collections.Generic.List(Of BOPurchases.cPurchaseLine)
        Dim NewLine As New cPurchaseLine()

        RetrievePurchaseOrderDetailLines = New List(Of BOPurchases.cPurchaseLine)
        NewLine.PurchaseLineID = New ColField(Of Integer)("TestName", 999)
        RetrievePurchaseOrderDetailLines.Add(NewLine)
    End Function
End Class
