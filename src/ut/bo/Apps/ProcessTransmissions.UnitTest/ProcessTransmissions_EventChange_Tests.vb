﻿<TestClass()> Public Class ProcessTransmissions_EventChange_Tests

    Private testContextInstance As TestContext

    Private startDate As Nullable(Of Date) = CDate("2012-01-01")
    Private endDate As Nullable(Of Date) = CDate("2012-01-30")

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "EventHeaderDates Tests"

    <TestMethod()> Public Sub EventHeaderStartDate_StartDatePassed_ReturnsEventHeaderWithStartDateSet()

        Dim EventHeader As New cEventHeader
        Dim X As ICouponsEventHeaderDate = (New CouponsEventHeaderFactory).GetImplementation

        X.SetEventHeaderDate(startDate, endDate, EventHeader)

        Assert.AreEqual(startDate, EventHeader.StartDate.Value)

    End Sub

    <TestMethod()> Public Sub EventHeaderEndDate_EndDatePassed_ReturnsEventHeaderWithEndDateSet()

        Dim EventHeader As New cEventHeader
        Dim X As ICouponsEventHeaderDate = (New CouponsEventHeaderFactory).GetImplementation

        X.SetEventHeaderDate(startDate, endDate, EventHeader)

        Assert.AreEqual(endDate, EventHeader.EndDate.Value)

    End Sub

    <TestMethod()> Public Sub EventHeaderStartDate_NoStartDatePassed_ReturnsEventHeaderWithoutStartDate()

        Dim EventHeader As New cEventHeader
        Dim X As ICouponsEventHeaderDate = (New CouponsEventHeaderFactory).GetImplementation

        EventHeader.StartDate.Value = CDate(startDate)
        startDate = EventHeader.StartDate.DefaultValue

        X.SetEventHeaderDate(Nothing, endDate, EventHeader)

        Assert.AreEqual(startDate, EventHeader.StartDate.Value)

    End Sub

    <TestMethod()> Public Sub EventHeaderEndDate_NoEndDatePassed_ReturnsEventHeaderWithoutEndDate()

        Dim EventHeader As New cEventHeader
        Dim X As ICouponsEventHeaderDate = (New CouponsEventHeaderFactory).GetImplementation

        EventHeader.EndDate.Value = CDate(endDate)
        endDate = EventHeader.EndDate.DefaultValue

        X.SetEventHeaderDate(startDate, Nothing, EventHeader)

        Assert.AreEqual(endDate, EventHeader.EndDate.Value)

    End Sub

#End Region

End Class