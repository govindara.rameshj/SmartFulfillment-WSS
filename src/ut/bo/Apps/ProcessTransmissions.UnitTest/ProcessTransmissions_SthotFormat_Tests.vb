﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class ProcessTransmissions_SthotFormat_Tests

    Private testContextInstance As TestContext

    Private DocumentNumber As String
    Private expectedFormattedDocumentNumber As String
    Private actualFormattedDocumentNumber As String

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub FormatSthotDocumentNumber_SixDigitNumberSupplied_ReturnsNumberPaddedWithLeadingZeros()

        DocumentNumber = "111111"
        expectedFormattedDocumentNumber = "00111111"
        SthotFormatFactory.FactorySet(New SthotFormatNew)

        actualFormattedDocumentNumber = SthotFormatFactory.FactoryGet.FormatSthotDocumentNumber(DocumentNumber)

        Assert.AreEqual(expectedFormattedDocumentNumber, actualFormattedDocumentNumber)

    End Sub

    <TestMethod()> Public Sub FormatSthotDocumentNumber_EmptyNumberSupplied_ReturnsAllZeros()

        DocumentNumber = ""
        expectedFormattedDocumentNumber = "00000000"
        SthotFormatFactory.FactorySet(New SthotFormatNew)

        actualFormattedDocumentNumber = SthotFormatFactory.FactoryGet.FormatSthotDocumentNumber(DocumentNumber)

        Assert.AreEqual(expectedFormattedDocumentNumber, actualFormattedDocumentNumber)

    End Sub
    <TestMethod()> Public Sub FormatSthotDocumentNumber_EightDigitNumberSupplied_ReturnsAllZeros()

        DocumentNumber = "12345678"
        expectedFormattedDocumentNumber = "12345678"
        SthotFormatFactory.FactorySet(New SthotFormatNew)

        actualFormattedDocumentNumber = SthotFormatFactory.FactoryGet.FormatSthotDocumentNumber(DocumentNumber)

        Assert.AreEqual(expectedFormattedDocumentNumber, actualFormattedDocumentNumber)

    End Sub
    <TestMethod()> Public Sub FormatSthotDocumentNumber_EmptyNumberSupplied_ReturnsEightDigits()

        DocumentNumber = ""
        SthotFormatFactory.FactorySet(New SthotFormatNew)

        actualFormattedDocumentNumber = SthotFormatFactory.FactoryGet.FormatSthotDocumentNumber(DocumentNumber)

        Assert.AreEqual(8, actualFormattedDocumentNumber.Length)

    End Sub

    <TestMethod()> Public Sub FormatSthotDocumentNumber_SixDigitWithTrailingSpacesSupplied_ReturnsNumericPartWithLeadingZeros()

        DocumentNumber = "123456              "
        expectedFormattedDocumentNumber = "00123456"
        SthotFormatFactory.FactorySet(New SthotFormatNew)

        actualFormattedDocumentNumber = SthotFormatFactory.FactoryGet.FormatSthotDocumentNumber(DocumentNumber)

        Assert.AreEqual(expectedFormattedDocumentNumber, actualFormattedDocumentNumber)

    End Sub

    <TestMethod()> Public Sub FormatSthotDocumentNumber_MoreThanEightDigitsWithTrailingSpacesSupplied_ReturnsFirstEightDigits()

        DocumentNumber = "1234567890              "
        expectedFormattedDocumentNumber = "12345678"
        SthotFormatFactory.FactorySet(New SthotFormatNew)

        actualFormattedDocumentNumber = SthotFormatFactory.FactoryGet.FormatSthotDocumentNumber(DocumentNumber)

        Assert.AreEqual(expectedFormattedDocumentNumber, actualFormattedDocumentNumber)

    End Sub
End Class
