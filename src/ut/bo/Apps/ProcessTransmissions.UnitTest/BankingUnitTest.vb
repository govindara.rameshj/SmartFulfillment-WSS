﻿<TestClass()> Public Class BankingUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    Private Sub ArrangeRequirementCheck(ByRef V As IBankingFloat, ByVal RequirementEnabled As System.Nullable(Of Boolean))

        Dim X As New BankingCurrentNew


        '   X.FloatVariance()

        'BankingCurrentNew

        '        Assert.AreEqual(Value, V.FloatVariance(ArrangeDataSetFloatVariance(10)))


    End Sub

#Region "Private Test Functions"

    Private Sub ArrangeRequirementCheck(ByVal RequirementEnabled As System.Nullable(Of Boolean))

        Dim Stub As New RequirementRepositoryStub

        'Stub.ConfigureStub(RequirementEnabled)
        RequirementRepositoryFactory.FactorySet(Stub)







    End Sub

    Private Function ArrangeDataSetFloatVariance(ByVal Value As System.Nullable(Of Decimal)) As DataSet

        Dim DS As DataSet
        Dim DT As DataTable

        DS = New DataSet
        DT = New DataTable

        DT.Columns.Add("FloatVariance", System.Type.GetType("System.Decimal"))
        DT.Rows.Add(Value)

        DS.Tables.Add(DT)

        Return DS

    End Function

#End Region

End Class
