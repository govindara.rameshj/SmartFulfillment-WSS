﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class ProcessTransmissions_HeadOfficeToStoreImportDateAdjust_Tests

    Private testContextInstance As TestContext
    Private dateAdjusterInstance As IDateAdjuster

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub DateNotAdjusted_GetAdjustedDatePopulated_PassUBREcordWithDate_ReturnsModifiedDate()
        dateAdjusterInstance = New DateNotAdjusted
        Dim chk As String = dateAdjusterInstance.GetAdjustedDate("UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY", "ddMMyyyy", 70, 8)
        Assert.AreEqual("UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY", dateAdjusterInstance.GetAdjustedDate("UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY", "ddMMyyyy", 70, 8))
    End Sub

    <TestMethod()> Public Sub DateNotAdjusted_GetAdjustedDatePopulated_PassDateddMMyyyy_ReturnsModifiedDate()
        dateAdjusterInstance = New DateNotAdjusted
        Dim originalString As String = "01/08/12"
        Dim dateFormat As String = "ddMMyyyy"
        Dim modifiedString As String = dateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 0, 8)

        Assert.AreEqual(originalString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassUBREcordWithDate_ReturnsModifiedDate()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -14
        Dim originalString As String = "UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY"
        Dim expectedString As String = "UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30220520120000310820120000NNNNNNNY"
        Dim dateFormat As String = "ddMMyyyy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 70, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub
    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassUBREcordWithUpdateDate01082012_ReturnsModifiedDate30072012()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -2
        Dim originalString As String = "UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY"
        Dim expectedString As String = "UB30/07/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY"
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 2, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassUBREcordWithUpdateDate01082012_ReturnsModifiedDate06082012()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = 5
        Dim originalString As String = "UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY"
        Dim expectedString As String = "UB06/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY"
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 2, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassUBREcordWithUpdateDate01082012_ReturnsModifiedDate01082012()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = 0
        Dim originalString As String = "UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY"
        Dim expectedString As String = "UB01/08/12       1.00 073356ZO- BBAS Aggregates Buy 4 GetOneFree(SG)30050620120000310820120000NNNNNNNY"
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 2, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassU4REcordWithDate_ReturnsModifiedDate()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -14
        Dim originalString As String = "U401/08/12     150.00 05241001507000692010812000000S 00599100150    001EACH001000    8892253000000008mm Himalayan Slate Laminate sng        8mm Himalayan Slate        10108120000612214 0000000             00000000000000Y00002000000000000000000000YN  0.00 22/06/12  /  /          /  /    /  /   00000700003800003900004707438510N0000 N0000.000000        B NY N000NNN000000"
        Dim expectedString As String = "U401/08/12     150.00 05241001507000692180712000000S 00599100150    001EACH001000    8892253000000008mm Himalayan Slate Laminate sng        8mm Himalayan Slate        10108120000612214 0000000             00000000000000Y00002000000000000000000000YN  0.00 22/06/12  /  /          /  /    /  /   00000700003800003900004707438510N0000 N0000.000000        B NY N000NNN000000"
        Dim dateFormat As String = "ddMMyy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 39, 6)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub
    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassU4REcordWithUpdateDate01082012_ReturnsModifiedDate18072012()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -14
        Dim originalString As String = "U401/08/12     150.00 05241001507000692010812000000S 00599100150    001EACH001000    8892253000000008mm Himalayan Slate Laminate sng        8mm Himalayan Slate        10108120000612214 0000000             00000000000000Y00002000000000000000000000YN  0.00 22/06/12  /  /          /  /    /  /   00000700003800003900004707438510N0000 N0000.000000        B NY N000NNN000000"
        Dim expectedString As String = "U418/07/12     150.00 05241001507000692010812000000S 00599100150    001EACH001000    8892253000000008mm Himalayan Slate Laminate sng        8mm Himalayan Slate        10108120000612214 0000000             00000000000000Y00002000000000000000000000YN  0.00 22/06/12  /  /          /  /    /  /   00000700003800003900004707438510N0000 N0000.000000        B NY N000NNN000000"
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 2, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassU4REcordWithDate_ReturnsModifiedintialorderdate()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -14
        Dim originalString As String = "U401/08/12     150.00 05241001507000692010812000000S 00599100150    001EACH001000    8892253000000008mm Himalayan Slate Laminate sng        8mm Himalayan Slate        10108120000612214 0000000             00000000000000Y00002000000000000000000000YN  0.00 22/06/12  /  /          /  /    /  /   00000700003800003900004707438510N0000 N0000.000000        B NY N000NNN000000"
        Dim expectedString As String = "U401/08/12     150.00 05241001507000692180712000000S 00599100150    001EACH001000    8892253000000008mm Himalayan Slate Laminate sng        8mm Himalayan Slate        10108120000612214 0000000             00000000000000Y00002000000000000000000000YN  0.00 22/06/12  /  /          /  /    /  /   00000700003800003900004707438510N0000 N0000.000000        B NY N000NNN000000"
        Dim dateFormat As String = "ddMMyy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 39, 6)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub
    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassHPSTVIREcordWithDate_ReturnsModifiedDate()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -14
        Dim originalString As String = "CS05/08/12      77.00 514 000088745514 000776827Double Pallet                            8                                                                                                                                                                                                                                                                                                                     ."
        Dim expectedString As String = "CS22/07/12      77.00 514 000088745514 000776827Double Pallet                            8                                                                                                                                                                                                                                                                                                                     ."
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 2, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassHPSTVIREcordWithUpdateDate05082012_ReturnsModifiedDate22072012()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -14
        Dim originalString As String = "CS05/08/12      77.00 514 000088745514 000776827Double Pallet                            8                                                                                                                                                                                                                                                                                                                     ."
        Dim expectedString As String = "CS22/07/12      77.00 514 000088745514 000776827Double Pallet                            8                                                                                                                                                                                                                                                                                                                     ."
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 2, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassHOSTCRecordWithUpdateDate11062012_ReturnsModifiedDate28052012()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -14
        Dim originalString As String = "CM11/06/2012       1.00 05098259098262000000000000"
        Dim expectedString As String = "CM28/05/2012       1.00 05098259098262000000000000"
        Dim dateFormat As String = "dd/MM/yyyy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 2, 10)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub


    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassHOSTGRecordWithDate_ReturnsModifiedDatettt()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -14
        Dim originalString As String = "HG20/07/12    2039.00  4428660623019/07/12032039    1007TS 00000010.00 "
        Dim expectedString As String = "HG20/07/12    2039.00  4428660623005/07/12032039    1007TS 00000010.00 "
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 34, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassHOSTGRecordWithInvalidDate00000000_ReturnsSameRecordWithoutModification()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -12
        Dim originalString As String = "HG20/07/12    2039.00  4428660623000000000032039    1007TS 00000010.00 "
        Dim expectedString As String = "HG20/07/12    2039.00  4428660623000000000032039    1007TS 00000010.00 "
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 34, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassHOSTGRecordWithInvalidDate_ReturnsSameRecordWithoutModification()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -11
        Dim originalString As String = "HG20/07/12    2039.00  4428660623  /  /   032039    1007TS 00000010.00 "
        Dim expectedString As String = "HG20/07/12    2039.00  4428660623  /  /   032039    1007TS 00000010.00 "
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 34, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub

    <TestMethod()> Public Sub DateAdjusted_GetAdjustedDatePopulated_PassHOSTGRecordWithUpdateDate20072012_ReturnsModifiedDate06072012()
        Dim testdateAdjusterInstance As DateAdjustedTestClass = New DateAdjustedTestClass
        testdateAdjusterInstance.SetDaysToBeAdjsuted = -10
        Dim originalString As String = "HG20/07/12    2039.00  4428660623         032039    1007TS 00000010.00 "
        Dim expectedString As String = "HG20/07/12    2039.00  4428660623         032039    1007TS 00000010.00 "
        Dim dateFormat As String = "dd/MM/yy"
        Dim modifiedString As String = testdateAdjusterInstance.GetAdjustedDate(originalString, dateFormat, 34, 8)

        Assert.AreEqual(expectedString, modifiedString)
    End Sub


End Class

Public Class DateAdjustedTestClass
    Inherits DateAdjusted

    Private daysSet As Integer

    Public Property SetDaysToBeAdjsuted() As Integer
        Get
            Return daysSet
        End Get
        Set(ByVal value As Integer)
            daysSet = value
        End Set
    End Property

    Friend Overrides Function GetDaysToAdjustBy() As Integer
        Return SetDaysToBeAdjsuted
    End Function


End Class
