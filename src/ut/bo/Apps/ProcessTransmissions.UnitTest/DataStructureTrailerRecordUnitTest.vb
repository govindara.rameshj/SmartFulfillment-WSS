﻿<TestClass()> Public Class DataStructureTrailerRecordUnitTest

    Private testContextInstance As TestContext

    Private _Trailer As ITrailerRecord

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub

    <TestInitialize()> Public Sub MyTestInitialize()

        _Trailer = New TrailerRecord("TR12321/07/11ABCDE77123456221555VT     7      163.00 VM     6    17543.00 VR     2       20.00 VQ     6         .00 VD     4        4.00 VH    29  1745884.00 YY     8    12345.67 ZZ     9    89012.34")

    End Sub

    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub RecordType()

        Assert.AreEqual("TR", _Trailer.RecordType)

    End Sub

    <TestMethod()> Public Sub StoreNumber()

        Assert.AreEqual("123", _Trailer.StoreNumber)

    End Sub

    <TestMethod()> Public Sub [Date]()

        Assert.AreEqual("21/07/11", _Trailer.Date)

    End Sub

    <TestMethod()> Public Sub FileType()

        Assert.AreEqual("ABCDE", _Trailer.FileType)

    End Sub

    <TestMethod()> Public Sub VersionNumber()

        Assert.AreEqual("77", _Trailer.VersionNumber)

    End Sub

    <TestMethod()> Public Sub SequenceNumber()

        Assert.AreEqual("123456", _Trailer.SequenceNumber)

    End Sub

    <TestMethod()> Public Sub Time()

        Assert.AreEqual("221555", _Trailer.Time)

    End Sub

    <TestMethod()> Public Sub Bucket1RecordType()

        Assert.AreEqual("VT", _Trailer.Bucket1RecordType)

    End Sub

    <TestMethod()> Public Sub Bucket1RecordCount()

        Assert.AreEqual("7", _Trailer.Bucket1RecordCount)

    End Sub

    <TestMethod()> Public Sub Bucket1RecordHashValue()

        Assert.AreEqual("163.00", _Trailer.Bucket1RecordHashValue)

    End Sub

    <TestMethod()> Public Sub Bucket2RecordType()

        Assert.AreEqual("VM", _Trailer.Bucket2RecordType)

    End Sub

    <TestMethod()> Public Sub Bucket2RecordCount()

        Assert.AreEqual("6", _Trailer.Bucket2RecordCount)

    End Sub

    <TestMethod()> Public Sub Bucket2RecordHashValue()

        Assert.AreEqual("17543.00", _Trailer.Bucket2RecordHashValue)

    End Sub

    <TestMethod()> Public Sub Bucket3RecordType()

        Assert.AreEqual("VR", _Trailer.Bucket3RecordType)

    End Sub

    <TestMethod()> Public Sub Bucket3RecordCount()

        Assert.AreEqual("2", _Trailer.Bucket3RecordCount)

    End Sub

    <TestMethod()> Public Sub Bucket3RecordHashValue()

        Assert.AreEqual("20.00", _Trailer.Bucket3RecordHashValue)

    End Sub

    <TestMethod()> Public Sub Bucket4RecordType()

        Assert.AreEqual("VQ", _Trailer.Bucket4RecordType)

    End Sub

    <TestMethod()> Public Sub Bucket4RecordCount()

        Assert.AreEqual("6", _Trailer.Bucket4RecordCount)

    End Sub

    <TestMethod()> Public Sub Bucket4RecordHashValue()

        Assert.AreEqual(".00", _Trailer.Bucket4RecordHashValue)

    End Sub

    <TestMethod()> Public Sub Bucket5RecordType()

        Assert.AreEqual("VD", _Trailer.Bucket5RecordType)

    End Sub

    <TestMethod()> Public Sub Bucket5RecordCount()

        Assert.AreEqual("4", _Trailer.Bucket5RecordCount)

    End Sub

    <TestMethod()> Public Sub Bucket5RecordHashValue()

        Assert.AreEqual("4.00", _Trailer.Bucket5RecordHashValue)

    End Sub

    <TestMethod()> Public Sub Bucket6RecordType()

        Assert.AreEqual("VH", _Trailer.Bucket6RecordType)

    End Sub

    <TestMethod()> Public Sub Bucket6RecordCount()

        Assert.AreEqual("29", _Trailer.Bucket6RecordCount)

    End Sub

    <TestMethod()> Public Sub Bucket6RecordHashValue()

        Assert.AreEqual("1745884.00", _Trailer.Bucket6RecordHashValue)

    End Sub

    <TestMethod()> Public Sub Bucket7RecordType()

        Assert.AreEqual("YY", _Trailer.Bucket7RecordType)

    End Sub

    <TestMethod()> Public Sub Bucket7RecordCount()

        Assert.AreEqual("8", _Trailer.Bucket7RecordCount)

    End Sub

    <TestMethod()> Public Sub Bucket7RecordHashValue()

        Assert.AreEqual("12345.67", _Trailer.Bucket7RecordHashValue)

    End Sub

    <TestMethod()> Public Sub Bucket8RecordType()

        Assert.AreEqual("ZZ", _Trailer.Bucket8RecordType)

    End Sub

    <TestMethod()> Public Sub Bucket8RecordCount()

        Assert.AreEqual("9", _Trailer.Bucket8RecordCount)

    End Sub

    <TestMethod()> Public Sub Bucket8RecordHashValue()

        Assert.AreEqual("89012.34", _Trailer.Bucket8RecordHashValue)

    End Sub

End Class