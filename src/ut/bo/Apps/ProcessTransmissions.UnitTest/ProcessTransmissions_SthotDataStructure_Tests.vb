﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class ProcessTransmissions_SthotDataStructure_Tests

    Private testContextInstance As TestContext
    'Private sthotDdRecordInstance As DataStructureSthot.Interface.IDdRecord = New DataStructureSthot.Implementation.SthotDdRecord

    Private sthotDdRecordInstance As DataStructureSthot.Implementation.SthotDdRecord = New DataStructureSthot.Implementation.SthotDdRecord

    Private expectedValue As String
    Private actualValue As String

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region


    <TestMethod()> Public Sub FormatDate_EightDigitDateSupplied_ReturnsEightDigitDate()
        expectedValue = "12/12/10"

        sthotDdRecordInstance.Date = "12/12/10"

        actualValue = sthotDdRecordInstance.Date

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatDate_SevenDigitDateSupplied_ReturnsEightDigitDateWithLeadinZeros()
        expectedValue = "02/12/10"

        sthotDdRecordInstance.Date = "2/12/10"

        actualValue = sthotDdRecordInstance.Date

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatDate_SixDigitDateSupplied_ReturnsEightDigitDate()
        expectedValue = "00/12/10"

        sthotDdRecordInstance.Date = "/12/10"

        actualValue = sthotDdRecordInstance.Date

        Assert.AreEqual(expectedValue, actualValue)



    End Sub
    <TestMethod()> Public Sub FormatDate_MoreThanEightDigitDateSupplied_ReturnsEightDigitDate()
        expectedValue = "00/12/10"

        sthotDdRecordInstance.Date = "00/12/101111111111111111111111111111111111111111"

        actualValue = sthotDdRecordInstance.Date

        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub FormatCatchAllItem_NothingSupplied_ReturnsSingleLeadingZero()
        expectedValue = "N"

        sthotDdRecordInstance.CatchAllItem = ""

        actualValue = sthotDdRecordInstance.CatchAllItem

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatCatchAllItem_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "N"

        sthotDdRecordInstance.CatchAllItem = "0"

        actualValue = sthotDdRecordInstance.CatchAllItem

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatCatchAllItem_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "Y"

        sthotDdRecordInstance.CatchAllItem = "110000000"

        actualValue = sthotDdRecordInstance.CatchAllItem

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatCollectedAtBackDoorFlag_NothingSupplied_ReturnsSingleSpacePaddedValue()
        expectedValue = "N"

        sthotDdRecordInstance.CollectedAtBackDoorFlag = ""

        actualValue = sthotDdRecordInstance.CollectedAtBackDoorFlag

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatCollectedAtBackDoorFlag_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "Y"

        sthotDdRecordInstance.CollectedAtBackDoorFlag = "11111111111111111"

        actualValue = sthotDdRecordInstance.CollectedAtBackDoorFlag

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatCollectedAtBackDoorFlag_SingleDigitSupplied_ReturnsSingleDigitValue()
        expectedValue = "Y"

        sthotDdRecordInstance.CollectedAtBackDoorFlag = "1"

        actualValue = sthotDdRecordInstance.CollectedAtBackDoorFlag

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatDealGroupErosion_FiveDigitsSupplied_ReturnsTenDigitsWithLeadingSpace()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.DealGroupErosion = "12345"

        actualValue = sthotDdRecordInstance.DealGroupErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatDealGroupErosion_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.DealGroupErosion = "123456"

        actualValue = sthotDdRecordInstance.DealGroupErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatDealGroupErosion_ValueZeroSupplied_ReturnsTenDigitsZeroValue()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.DealGroupErosion = "0"

        actualValue = sthotDdRecordInstance.DealGroupErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
   
    <TestMethod()> Public Sub FormatDealGroupErosionCode_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.DealGroupErosionCode = "012345"

        actualValue = sthotDdRecordInstance.DealGroupErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatDealGroupErosionCode_NothingSupplied_ReturnsSixDigitZero()
        expectedValue = "000000"

        sthotDdRecordInstance.DealGroupErosionCode = ""

        actualValue = sthotDdRecordInstance.DealGroupErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatDealGroupErosionCode_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.DealGroupErosionCode = "012345777777777777777777777777"

        actualValue = sthotDdRecordInstance.DealGroupErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatDealGroupErosionCode_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.DealGroupErosionCode = "12345"

        actualValue = sthotDdRecordInstance.DealGroupErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatDepartmentNumber_TwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "01"

        sthotDdRecordInstance.DepartmentNumber = "01"

        actualValue = sthotDdRecordInstance.DepartmentNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatDepartmentNumber_OneDigitSupplied_ReturnsTwoDigitsWithLeadingZero()
        expectedValue = "01"

        sthotDdRecordInstance.DepartmentNumber = "1"

        actualValue = sthotDdRecordInstance.DepartmentNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatDepartmentNumber_MoreThanTwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "01"

        sthotDdRecordInstance.DepartmentNumber = "0133333333333333333333333"

        actualValue = sthotDdRecordInstance.DepartmentNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatDepartmentNumber_NothingSupplied_ReturnsTwoDigitZeros()
        expectedValue = "00"

        sthotDdRecordInstance.DepartmentNumber = ""

        actualValue = sthotDdRecordInstance.DepartmentNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatEmployeeSaleMarginErosionCode_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.EmployeeSaleMarginErosionCode = "012345"

        actualValue = sthotDdRecordInstance.EmployeeSaleMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatEmployeeSaleMarginErosionCode_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.EmployeeSaleMarginErosionCode = ""

        actualValue = sthotDdRecordInstance.EmployeeSaleMarginErosionCode


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatEmployeeSaleMarginErosionCode_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.EmployeeSaleMarginErosionCode = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.EmployeeSaleMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatEmployeeSaleMarginErosionCode_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.EmployeeSaleMarginErosionCode = "12345"

        actualValue = sthotDdRecordInstance.EmployeeSaleMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatEmployeeSalePriceDifference_ZeroValueSupplied_ReturnsTenDigits()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.EmployeeSalePriceDifference = "0"

        actualValue = sthotDdRecordInstance.EmployeeSalePriceDifference

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatEmployeeSalePriceDifference_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.EmployeeSalePriceDifference = "12345"

        actualValue = sthotDdRecordInstance.EmployeeSalePriceDifference

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatEmployeeSalePriceDifference_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.EmployeeSalePriceDifference = "123456"

        actualValue = sthotDdRecordInstance.EmployeeSalePriceDifference

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
   
    <TestMethod()> Public Sub FormatExtendedCost_ValueZeroSupplied_ReturnsElevenDigits()
        expectedValue = "     0.000+"

        sthotDdRecordInstance.ExtendedCost = "0"

        actualValue = sthotDdRecordInstance.ExtendedCost

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatExtendedCost_FiveDigitsSupplied_ReturnsElevenDigits()
        expectedValue = " 12345.000+"

        sthotDdRecordInstance.ExtendedCost = "12345"

        actualValue = sthotDdRecordInstance.ExtendedCost

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatExtendedCost_SevenDigitsSupplied_ReturnsElevenDigits()
        expectedValue = "1234567.000+"

        sthotDdRecordInstance.ExtendedCost = "1234567"

        actualValue = sthotDdRecordInstance.ExtendedCost

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatExtendedValue_ValueZeroSupplied_ReturnsTenDigits()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.ExtendedValue = "0"

        actualValue = sthotDdRecordInstance.ExtendedValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatExtendedValue_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.ExtendedValue = "12345"

        actualValue = sthotDdRecordInstance.ExtendedValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatExtendedValue_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.ExtendedValue = "123456"

        actualValue = sthotDdRecordInstance.ExtendedValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
   

    <TestMethod()> Public Sub FormatHierarchyCategory_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyCategory = "012345"

        actualValue = sthotDdRecordInstance.HierarchyCategory

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyCategory_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.HierarchyCategory = ""

        actualValue = sthotDdRecordInstance.HierarchyCategory

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyCategory_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyCategory = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.HierarchyCategory

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyCategory_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyCategory = "12345"

        actualValue = sthotDdRecordInstance.HierarchyCategory

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatHierarchyGroup_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyGroup = "012345"

        actualValue = sthotDdRecordInstance.HierarchyGroup

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyGroup_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.HierarchyGroup = ""

        actualValue = sthotDdRecordInstance.HierarchyGroup

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyGroup_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyGroup = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.HierarchyGroup

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyGroup_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyGroup = "12345"

        actualValue = sthotDdRecordInstance.HierarchyGroup

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatHierarchySpendLevelErosion_ValueZeroSupplied_ReturnsTenDigits()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.HierarchySpendLevelErosion = "0"

        actualValue = sthotDdRecordInstance.HierarchySpendLevelErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchySpendLevelErosion_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.HierarchySpendLevelErosion = "123456"

        actualValue = sthotDdRecordInstance.HierarchySpendLevelErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchySpendLevelErosion_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.HierarchySpendLevelErosion = "12345"

        actualValue = sthotDdRecordInstance.HierarchySpendLevelErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    
    <TestMethod()> Public Sub FormatHierarchyStyle_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyStyle = "012345"

        actualValue = sthotDdRecordInstance.HierarchyStyle

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyStyle_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.HierarchyStyle = ""

        actualValue = sthotDdRecordInstance.HierarchyStyle

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyStyle_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyStyle = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.HierarchyStyle

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchyStyle_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchyStyle = "12345"

        actualValue = sthotDdRecordInstance.HierarchyStyle

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatHierarchySubGroup_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchySubGroup = "012345"

        actualValue = sthotDdRecordInstance.HierarchySubGroup

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchySubGroup_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.HierarchySubGroup = ""

        actualValue = sthotDdRecordInstance.HierarchySubGroup

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchySubGroup_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchySubGroup = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.HierarchySubGroup

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatHierarchySubGroup_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.HierarchySubGroup = "12345"

        actualValue = sthotDdRecordInstance.HierarchySubGroup

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatInputByScannerFlag_NothingSupplied_ReturnsSingleLeadingZero()
        expectedValue = "N"

        sthotDdRecordInstance.InputByScanner = ""

        actualValue = sthotDdRecordInstance.InputByScanner

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatInputByScannerFlag_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "N"

        sthotDdRecordInstance.InputByScanner = "0"

        actualValue = sthotDdRecordInstance.InputByScanner

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatInputByScannerFlag_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "Y"

        sthotDdRecordInstance.InputByScanner = "110000000"

        actualValue = sthotDdRecordInstance.InputByScanner

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatItemCollectedIndicator_NothingSupplied_ReturnsSingleLeadingZero()
        expectedValue = "0"

        sthotDdRecordInstance.ItemCollectedIndicator = ""

        actualValue = sthotDdRecordInstance.ItemCollectedIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatItemCollectedIndicator_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "0"

        sthotDdRecordInstance.ItemCollectedIndicator = "0"

        actualValue = sthotDdRecordInstance.ItemCollectedIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatItemCollectedIndicator_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "1"

        sthotDdRecordInstance.ItemCollectedIndicator = "110000000"

        actualValue = sthotDdRecordInstance.ItemCollectedIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatItemPrice_NothingSupplied_ReturnsTenDigits()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.ItemPrice = "0"

        actualValue = sthotDdRecordInstance.ItemPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatItemPrice_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.ItemPrice = "12345"

        actualValue = sthotDdRecordInstance.ItemPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatItemPrice_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.ItemPrice = "123456"

        actualValue = sthotDdRecordInstance.ItemPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatItemPrice_FiveDigitsSuppliedWithNegativeSign_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.ItemPrice = "-12345"

        actualValue = sthotDdRecordInstance.ItemPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatItemPriceVatExclusive_ValueZeroSupplied_ReturnsTenDigits()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.ItemPriceVatExclusive = "0"

        actualValue = sthotDdRecordInstance.ItemPriceVatExclusive

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatItemPriceVatExclusive_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.ItemPriceVatExclusive = "012345"

        actualValue = sthotDdRecordInstance.ItemPriceVatExclusive

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatItemPriceVatExclusive_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.ItemPriceVatExclusive = "123456"

        actualValue = sthotDdRecordInstance.ItemPriceVatExclusive

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatItemPriceVatExclusive_FiveDigitsSuppliedWithNegativeSign_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.ItemPriceVatExclusive = "-12345"

        actualValue = sthotDdRecordInstance.ItemPriceVatExclusive

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatLastEventSequenceNumber_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.LastEventSequenceNumber = "012345"

        actualValue = sthotDdRecordInstance.LastEventSequenceNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatLastEventSequenceNumber_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.LastEventSequenceNumber = ""

        actualValue = sthotDdRecordInstance.LastEventSequenceNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatLastEventSequenceNumber_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.LastEventSequenceNumber = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.LastEventSequenceNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatLastEventSequenceNumber_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.LastEventSequenceNumber = "12345"

        actualValue = sthotDdRecordInstance.LastEventSequenceNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatLineReversalFlag_NothingSupplied_ReturnsSingleLeadingZero()
        expectedValue = "N"

        sthotDdRecordInstance.LineReversal = ""

        actualValue = sthotDdRecordInstance.LineReversal

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatLineReversalFlag_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "N"

        sthotDdRecordInstance.LineReversal = "0"

        actualValue = sthotDdRecordInstance.LineReversal

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatLineReversalFlag_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "Y"

        sthotDdRecordInstance.LineReversal = "110000000"

        actualValue = sthotDdRecordInstance.LineReversal

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatLineReversalReasonCode_TwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "12"

        sthotDdRecordInstance.LineReversalReasonCode = "12"

        actualValue = sthotDdRecordInstance.LineReversalReasonCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatLineReversalReasonCode_NothingSupplied_ReturnsTwoDigitZeros()
        expectedValue = "00"

        sthotDdRecordInstance.LineReversalReasonCode = ""

        actualValue = sthotDdRecordInstance.LineReversalReasonCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatLineReversalReasonCode_MoreThanTwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "12"

        sthotDdRecordInstance.LineReversalReasonCode = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.LineReversalReasonCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatLineReversalReasonCode_OneDigitSupplied_ReturnsTwoDigitsWithLeadingZero()
        expectedValue = "01"

        sthotDdRecordInstance.LineReversalReasonCode = "1"

        actualValue = sthotDdRecordInstance.LineReversalReasonCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatMultiBuyErosion_ValueZeroSupplied_ReturnsTenDigits()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.MultiBuyErosion = "0"

        actualValue = sthotDdRecordInstance.MultiBuyErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatMultiBuyErosion_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.MultiBuyErosion = "12345"

        actualValue = sthotDdRecordInstance.MultiBuyErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatMultiBuyErosion_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.MultiBuyErosion = "123456"

        actualValue = sthotDdRecordInstance.MultiBuyErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatMultiBuyMarginErosionCode_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.MultiBuyMarginErosionCode = "012345"

        actualValue = sthotDdRecordInstance.MultiBuyMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatMultiBuyMarginErosionCode_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.MultiBuyMarginErosionCode = ""

        actualValue = sthotDdRecordInstance.MultiBuyMarginErosionCode


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatMultiBuyMarginErosionCode_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.MultiBuyMarginErosionCode = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.MultiBuyMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatMultiBuyMarginErosionCode_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.MultiBuyMarginErosionCode = "12345"

        actualValue = sthotDdRecordInstance.MultiBuyMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPartialQuarantine_ThreeDigitsSupplied_ReturnsThreeDigits()
        expectedValue = "123"

        sthotDdRecordInstance.PartialQuarantine = "123"

        actualValue = sthotDdRecordInstance.PartialQuarantine

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPartialQuarantine_NothingSupplied_ReturnsThreeDigitZeros()
        expectedValue = "000"

        sthotDdRecordInstance.PartialQuarantine = ""

        actualValue = sthotDdRecordInstance.PartialQuarantine


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPartialQuarantine_MoreThanThreeDigitsSupplied_ReturnsThreeDigits()
        expectedValue = "123"

        sthotDdRecordInstance.PartialQuarantine = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.PartialQuarantine

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPartialQuarantine_TwoDigitsSupplied_ReturnsThreeDigitsWithLeadingZero()
        expectedValue = "012"

        sthotDdRecordInstance.PartialQuarantine = "12"

        actualValue = sthotDdRecordInstance.PartialQuarantine

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideMarginErosionCode_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.PriceOverrideMarginErosionCode = "012345"

        actualValue = sthotDdRecordInstance.PriceOverrideMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideMarginErosionCode_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.PriceOverrideMarginErosionCode = ""

        actualValue = sthotDdRecordInstance.PriceOverrideMarginErosionCode


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideMarginErosionCode_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.PriceOverrideMarginErosionCode = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.PriceOverrideMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideMarginErosionCode_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.PriceOverrideMarginErosionCode = "12345"

        actualValue = sthotDdRecordInstance.PriceOverrideMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverridePriceDifference_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.PriceOverridePriceDifference = "12345"

        actualValue = sthotDdRecordInstance.PriceOverridePriceDifference

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatPriceOverridePriceDifference_ValueZeroSupplied_ReturnsTenDigitsZero()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.PriceOverridePriceDifference = "0"

        actualValue = sthotDdRecordInstance.PriceOverridePriceDifference

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverridePriceDifference_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.PriceOverridePriceDifference = "123456"

        actualValue = sthotDdRecordInstance.PriceOverridePriceDifference

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideReason_TwentyCharactersSupplied_ReturnsTwentyCharacters()
        expectedValue = "THIS IS A TEST PRICE"

        sthotDdRecordInstance.PriceOverrideReason = "THIS IS A TEST PRICE"

        actualValue = sthotDdRecordInstance.PriceOverrideReason

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatPriceOverrideReason_NothingSupplied_ReturnsTwentySpaceCharacters()
        expectedValue = Space(20)

        sthotDdRecordInstance.PriceOverrideReason = ""

        actualValue = sthotDdRecordInstance.PriceOverrideReason

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideReason_MoreThanTwentyCharactersSupplied_ReturnsTwentyCharacters()
        expectedValue = "THIS IS A TEST PRICE"

        sthotDdRecordInstance.PriceOverrideReason = "THIS IS A TEST PRICE AND DEMO"

        actualValue = sthotDdRecordInstance.PriceOverrideReason

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideReason_NinteenCharactersSupplied_ReturnsTwentyCharactersWithTrailingSpaces()
        expectedValue = "THISIS A TEST PRICE "

        sthotDdRecordInstance.PriceOverrideReason = "THISIS A TEST PRICE"

        actualValue = sthotDdRecordInstance.PriceOverrideReason

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatPriceOverrideReasonCodeNumber_TwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "12"

        sthotDdRecordInstance.PriceOverrideReasonCodeNumber = "12"

        actualValue = sthotDdRecordInstance.PriceOverrideReasonCodeNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideReasonCodeNumber_NothingSupplied_ReturnsTwoLeadingSpaces()
        expectedValue = Space(2)

        sthotDdRecordInstance.PriceOverrideReasonCodeNumber = ""

        actualValue = sthotDdRecordInstance.PriceOverrideReasonCodeNumber


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideReasonCodeNumber_MoreThanTwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "12"

        sthotDdRecordInstance.PriceOverrideReasonCodeNumber = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.PriceOverrideReasonCodeNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatPriceOverrideReasonCodeNumber_OneDigitSupplied_ReturnsTwoDigitsWithLeadingSpace()
        expectedValue = " 1"

        sthotDdRecordInstance.PriceOverrideReasonCodeNumber = "1"

        actualValue = sthotDdRecordInstance.PriceOverrideReasonCodeNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatQuanityBreakErosion_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.QuanityBreakErosion = "12345"

        actualValue = sthotDdRecordInstance.QuanityBreakErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatQuanityBreakErosion_ValueZeroSupplied_ReturnsTenDigitsZero()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.QuanityBreakErosion = "0"

        actualValue = sthotDdRecordInstance.QuanityBreakErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatQuanityBreakErosion_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.QuanityBreakErosion = "123456"

        actualValue = sthotDdRecordInstance.QuanityBreakErosion

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
   
    <TestMethod()> Public Sub FormatQuantityBreakMarginErosionCode_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.QuantityBreakMarginErosionCode = "012345"

        actualValue = sthotDdRecordInstance.QuantityBreakMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatQuantityBreakMarginErosionCode_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.QuantityBreakMarginErosionCode = ""

        actualValue = sthotDdRecordInstance.QuantityBreakMarginErosionCode


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatQuantityBreakMarginErosionCode_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.QuantityBreakMarginErosionCode = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.QuantityBreakMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatQuantityBreakMarginErosionCode_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.QuantityBreakMarginErosionCode = "12345"

        actualValue = sthotDdRecordInstance.QuantityBreakMarginErosionCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatQuantitySold_TwoDigitsSupplied_ReturnsSevenDigits()
        expectedValue = "    12+"

        sthotDdRecordInstance.QuantitySold = "12"

        actualValue = sthotDdRecordInstance.QuantitySold

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatQuantitySold_ValueZeroSupplied_ReturnsSevenDigits()
        expectedValue = "     0+"

        sthotDdRecordInstance.QuantitySold = "0"

        actualValue = sthotDdRecordInstance.QuantitySold


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatQuantitySold_ThreeDigitsSupplied_ReturnsSevenDigits()
        expectedValue = "   123+"


        sthotDdRecordInstance.QuantitySold = "123"

        actualValue = sthotDdRecordInstance.QuantitySold

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
   

    <TestMethod()> Public Sub FormatRecordType_TwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "12"

        sthotDdRecordInstance.RecordType = "12"

        actualValue = sthotDdRecordInstance.RecordType

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatRecordType_NothingSupplied_ReturnsTwoLeadingZeros()
        expectedValue = "00"

        sthotDdRecordInstance.RecordType = ""

        actualValue = sthotDdRecordInstance.RecordType


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatRecordType_MoreThanTwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "12"

        sthotDdRecordInstance.RecordType = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.RecordType

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatRecordType_OneDigitSupplied_ReturnsTwoDigitsWithLeadingZero()
        expectedValue = "01"

        sthotDdRecordInstance.RecordType = "1"

        actualValue = sthotDdRecordInstance.RecordType

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatRelatedItemsSingleFlag_NothingSupplied_ReturnsSingleLeadingZero()
        expectedValue = "N"

        sthotDdRecordInstance.RelatedItemsSingle = ""

        actualValue = sthotDdRecordInstance.RelatedItemsSingle

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatRelatedItemsSingleFlag_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "N"

        sthotDdRecordInstance.RelatedItemsSingle = "0"

        actualValue = sthotDdRecordInstance.RelatedItemsSingle

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatRelatedItemsSingleFlag_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "Y"

        sthotDdRecordInstance.RelatedItemsSingle = "110000000"

        actualValue = sthotDdRecordInstance.RelatedItemsSingle

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatSaleTypeIndicator_NothingSupplied_ReturnsSingleLeadingSpace()
        expectedValue = Space(1)


        sthotDdRecordInstance.SaleTypeIndicator = ""

        actualValue = sthotDdRecordInstance.SaleTypeIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatSaleTypeIndicator_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "0"

        sthotDdRecordInstance.SaleTypeIndicator = "0"

        actualValue = sthotDdRecordInstance.SaleTypeIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatSaleTypeIndicator_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "1"


        sthotDdRecordInstance.SaleTypeIndicator = "110000000"

        actualValue = sthotDdRecordInstance.SaleTypeIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatSecondarySaleErosionValue_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.SecondarySaleErosionValue = "12345"

        actualValue = sthotDdRecordInstance.SecondarySaleErosionValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatSecondarySaleErosionValue_ZeroValueSupplied_ReturnsTenDigits()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.SecondarySaleErosionValue = "0"

        actualValue = sthotDdRecordInstance.SecondarySaleErosionValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSecondarySaleErosionValue_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.SecondarySaleErosionValue = "123456"

        actualValue = sthotDdRecordInstance.SecondarySaleErosionValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    
    <TestMethod()> Public Sub FormatSequenceNumber_FourDigitsSupplied_ReturnsFourDigits()
        expectedValue = "1234"

        sthotDdRecordInstance.SequenceNumber = "1234"

        actualValue = sthotDdRecordInstance.SequenceNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSequenceNumber_NothingSupplied_ReturnsFourLeadingSpaces()
        expectedValue = Space(4)

        sthotDdRecordInstance.SequenceNumber = ""

        actualValue = sthotDdRecordInstance.SequenceNumber


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSequenceNumber_MoreThanFourDigitsSupplied_ReturnsFourDigits()
        expectedValue = "1234"

        sthotDdRecordInstance.SequenceNumber = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.SequenceNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSequenceNumber_TwoDigitsSupplied_ReturnsFourDigitsWithLeadingSpace()
        expectedValue = "  12"


        sthotDdRecordInstance.SequenceNumber = "12"

        actualValue = sthotDdRecordInstance.SequenceNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSkuNumber_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.SkuNumber = "012345"

        actualValue = sthotDdRecordInstance.SkuNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSkuNumber_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.SkuNumber = ""

        actualValue = sthotDdRecordInstance.SkuNumber


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSkuNumber_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.SkuNumber = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.SkuNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSkuNumber_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.SkuNumber = "12345"

        actualValue = sthotDdRecordInstance.SkuNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSoldFromIndicator_NothingSupplied_ReturnsSingleLeadingZero()
        expectedValue = "N"


        sthotDdRecordInstance.SoldFromIndicator = ""

        actualValue = sthotDdRecordInstance.SoldFromIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatSoldFromIndicator_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "N"


        sthotDdRecordInstance.SoldFromIndicator = "0"

        actualValue = sthotDdRecordInstance.SoldFromIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatSoldFromIndicator_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "Y"

        sthotDdRecordInstance.SoldFromIndicator = "110000000"

        actualValue = sthotDdRecordInstance.SoldFromIndicator

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatSupervisorCashierNumber_ThreeDigitsSupplied_ReturnsThreeDigits()
        expectedValue = "123"

        sthotDdRecordInstance.SupervisorCashierNumber = "123"

        actualValue = sthotDdRecordInstance.SupervisorCashierNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSupervisorCashierNumber_NothingSupplied_ReturnsThreeDigitZeros()
        expectedValue = "000"

        sthotDdRecordInstance.SupervisorCashierNumber = ""

        actualValue = sthotDdRecordInstance.SupervisorCashierNumber


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub SupervisorCashierNumber_MoreThanThreeDigitsSupplied_ReturnsThreeDigits()
        expectedValue = "123"

        sthotDdRecordInstance.SupervisorCashierNumber = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.SupervisorCashierNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub SupervisorCashierNumber_TwoDigitsSupplied_ReturnsThreeDigitsWithTrailingZero()
        expectedValue = "012"

        sthotDdRecordInstance.SupervisorCashierNumber = "12"

        actualValue = sthotDdRecordInstance.SupervisorCashierNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSystemLookUpPrice_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.SystemLookUpPrice = "12345"

        actualValue = sthotDdRecordInstance.SystemLookUpPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatSystemLookUpPrice_FiveDigitsSuppliedWithNegativeSign_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.SystemLookUpPrice = "-12345"

        actualValue = sthotDdRecordInstance.SystemLookUpPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatSystemLookUpPrice_ValueZeroSupplied_ReturnsTenDigits()
        expectedValue = "     0.00+"


        sthotDdRecordInstance.SystemLookUpPrice = "0"

        actualValue = sthotDdRecordInstance.SystemLookUpPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatSystemLookUpPrice_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.SystemLookUpPrice = "123456"


        actualValue = sthotDdRecordInstance.SystemLookUpPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
  
    <TestMethod()> Public Sub FormatTaggedItemFlag_NothingSupplied_ReturnsSingleLeadingZero()
        expectedValue = "N"

        sthotDdRecordInstance.TaggedItem = ""

        actualValue = sthotDdRecordInstance.TaggedItem

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatTaggedItemFlag_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "N"

        sthotDdRecordInstance.TaggedItem = "0"

        actualValue = sthotDdRecordInstance.TaggedItem

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatTaggedItemFlag_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "Y"

        sthotDdRecordInstance.TaggedItem = "110000000"

        actualValue = sthotDdRecordInstance.TaggedItem

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatTemporaryPriceChangeMargin_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.TemporaryPriceChangeMargin = "012345"

        actualValue = sthotDdRecordInstance.TemporaryPriceChangeMargin

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTemporaryPriceChangeMargin_NothingSupplied_ReturnsSixDigitZeros()
        expectedValue = "000000"

        sthotDdRecordInstance.TemporaryPriceChangeMargin = ""

        actualValue = sthotDdRecordInstance.TemporaryPriceChangeMargin

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTemporaryPriceChangeMargin_MoreThanSixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "012345"

        sthotDdRecordInstance.TemporaryPriceChangeMargin = "01234588888888888888888888"

        actualValue = sthotDdRecordInstance.TemporaryPriceChangeMargin

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTemporaryPriceChangeMargin_FiveDigitsSupplied_ReturnsSixDigitsWithLeadingZero()
        expectedValue = "012345"

        sthotDdRecordInstance.TemporaryPriceChangeMargin = "12345"

        actualValue = sthotDdRecordInstance.TemporaryPriceChangeMargin

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTemporaryPriceChangePrice_FiveDigitsSupplied_ReturnsTenDigits()
        expectedValue = " 12345.00+"

        sthotDdRecordInstance.TemporaryPriceChangePrice = "12345"

        actualValue = sthotDdRecordInstance.TemporaryPriceChangePrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatTemporaryPriceChangePrice_ValueZeroSupplied_ReturnsTenDigitsZero()
        expectedValue = "     0.00+"

        sthotDdRecordInstance.TemporaryPriceChangePrice = "0"

        actualValue = sthotDdRecordInstance.TemporaryPriceChangePrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTemporaryPriceChangePrice_SixDigitsSupplied_ReturnsTenDigits()
        expectedValue = "123456.00+"

        sthotDdRecordInstance.TemporaryPriceChangePrice = "123456"

        actualValue = sthotDdRecordInstance.TemporaryPriceChangePrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTillId_TwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "12"

        sthotDdRecordInstance.TillId = "12"

        actualValue = sthotDdRecordInstance.TillId

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTillId_NothingSupplied_ReturnsTwoLeadingZeros()
        expectedValue = "00"

        sthotDdRecordInstance.TillId = ""

        actualValue = sthotDdRecordInstance.TillId


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTillId_MoreThanTwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "12"

        sthotDdRecordInstance.TillId = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.TillId

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTillId_OneDigitSupplied_ReturnsTwoDigitsWithLeadingZeros()
        expectedValue = "01"

        sthotDdRecordInstance.TillId = "1"

        actualValue = sthotDdRecordInstance.TillId

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTransactionNumber_FourDigitsSupplied_ReturnsFourDigits()
        expectedValue = "1234"

        sthotDdRecordInstance.TransactionNumber = "1234"

        actualValue = sthotDdRecordInstance.TransactionNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTransactionNumber_NothingSupplied_ReturnsFourLeadingZeros()
        expectedValue = "0000"

        sthotDdRecordInstance.TransactionNumber = ""

        actualValue = sthotDdRecordInstance.TransactionNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTransactionNumber_MoreThanFourDigitsSupplied_ReturnsFourDigits()
        expectedValue = "1234"

        sthotDdRecordInstance.TransactionNumber = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.TransactionNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatTransactionNumber_TwoDigitsSupplied_ReturnsFourDigitsWithLeadingZeros()
        expectedValue = "0012"

        sthotDdRecordInstance.TransactionNumber = "12"

        actualValue = sthotDdRecordInstance.TransactionNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatVatCode_TwoDigitsSupplied_ReturnsOneDigit()
        expectedValue = "1"

        sthotDdRecordInstance.VatCode = "12"

        actualValue = sthotDdRecordInstance.VatCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatVatCode_NothingSupplied_ReturnsOneLeadingZero()
        expectedValue = "0"

        sthotDdRecordInstance.VatCode = ""

        actualValue = sthotDdRecordInstance.VatCode


        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatVatCode_MoreThanTwoDigitsSupplied_ReturnsOneDigit()
        expectedValue = "1"

        sthotDdRecordInstance.VatCode = "1234588888888888888888888"

        actualValue = sthotDdRecordInstance.VatCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatVatCode_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "1"

        sthotDdRecordInstance.VatCode = "1"

        actualValue = sthotDdRecordInstance.VatCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatVatSymbolUser_NothingSupplied_ReturnsSingleLeadingSpace()
        expectedValue = Space(1)

        sthotDdRecordInstance.VatSymbolUser = ""

        actualValue = sthotDdRecordInstance.VatSymbolUser

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatVatSymbolUser_OneDigitSupplied_ReturnsOneDigit()
        expectedValue = "0"

        sthotDdRecordInstance.VatSymbolUser = "0"

        actualValue = sthotDdRecordInstance.VatSymbolUser

        Assert.AreEqual(expectedValue, actualValue)


    End Sub

    <TestMethod()> Public Sub FormatVatSymbolUser_MoreThanOneDigitSupplied_ReturnsOneDigit()
        expectedValue = "1"

        sthotDdRecordInstance.VatSymbolUser = "110000000"

        actualValue = sthotDdRecordInstance.VatSymbolUser

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatVatValue_ZeroValueSupplied_ReturnsElevenDigits()
        expectedValue = "       0.00+"

        sthotDdRecordInstance.VatValue = "0"

        actualValue = sthotDdRecordInstance.VatValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatVatValue_SixDigitsSupplied_ReturnsElevenDigits()
        expectedValue = "  123456.00+"

        sthotDdRecordInstance.VatValue = "123456"

        actualValue = sthotDdRecordInstance.VatValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatVatValue_SevenDigitsSupplied_ReturnsElevenDigits()
        expectedValue = " 1234567.00+"

        sthotDdRecordInstance.VatValue = "1234567"

        actualValue = sthotDdRecordInstance.VatValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatInputByScanner_SuppliedOneReturnsZero()
        expectedValue = "Y"

        sthotDdRecordInstance.InputByScanner = "1"

        actualValue = sthotDdRecordInstance.InputByScanner

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    
    <TestMethod()> Public Sub FormatVatValue_GetCompleteRecord()
        expectedValue = "DD28/10/11     117.00+010117   119023400N000     2+    14.99+    14.99+     0.00+    29.98+     0.000+N12                    NNb     0.00+000000     0.00+000000     0.00+000000     0.00+000000     0.00+000000     0.00+000000    23.00+012345N000000173698205612012345205619000     0.00+NO        2.00+NN01"

        sthotDdRecordInstance.Date = "28/10/11"
        sthotDdRecordInstance.CatchAllItem = "0"
        sthotDdRecordInstance.CollectedAtBackDoorFlag = "0"
        sthotDdRecordInstance.DealGroupErosion = "0"
        sthotDdRecordInstance.DealGroupErosionCode = ""
        sthotDdRecordInstance.DepartmentNumber = "00"
        sthotDdRecordInstance.EmployeeSaleMarginErosionCode = "12345"
        sthotDdRecordInstance.EmployeeSalePriceDifference = "23"
        sthotDdRecordInstance.ExtendedCost = "0"
        sthotDdRecordInstance.ExtendedValue = "29.98"
        sthotDdRecordInstance.Hash = "0117"
        sthotDdRecordInstance.HierarchyCategory = "173698"
        sthotDdRecordInstance.HierarchyGroup = "205612"
        sthotDdRecordInstance.HierarchySpendLevelErosion = "0"
        sthotDdRecordInstance.HierarchySpendLevelErosionCode = "0"
        sthotDdRecordInstance.HierarchyStyle = "205619"
        sthotDdRecordInstance.HierarchySubGroup = "205616"
        sthotDdRecordInstance.HierarchySubGroup = "12345"
        sthotDdRecordInstance.InputByScanner = "0"
        sthotDdRecordInstance.ItemCollectedIndicator = ""
        sthotDdRecordInstance.ItemPrice = "14.99"
        sthotDdRecordInstance.ItemPriceVatExclusive = "0"
        sthotDdRecordInstance.LastEventSequenceNumber = ""
        sthotDdRecordInstance.LineReversal = "0"
        sthotDdRecordInstance.LineReversalReasonCode = "1"
        sthotDdRecordInstance.MultiBuyErosion = "0"
        sthotDdRecordInstance.MultiBuyMarginErosionCode = "0"
        sthotDdRecordInstance.PartialQuarantine = ""
        sthotDdRecordInstance.PriceOverrideMarginErosionCode = ""
        sthotDdRecordInstance.PriceOverridePriceDifference = "0"
        sthotDdRecordInstance.PriceOverrideReason = ""
        sthotDdRecordInstance.PriceOverrideReasonCodeNumber = "12"
        sthotDdRecordInstance.QuanityBreakErosion = "0"
        sthotDdRecordInstance.QuantityBreakMarginErosionCode = ""
        sthotDdRecordInstance.QuantitySold = "2"
        sthotDdRecordInstance.RecordType = "DD"
        sthotDdRecordInstance.RelatedItemsSingle = "0"
        sthotDdRecordInstance.SaleTypeIndicator = "O"
        sthotDdRecordInstance.SecondarySaleErosionValue = "0"
        sthotDdRecordInstance.SequenceNumber = "1"
        sthotDdRecordInstance.SkuNumber = "190234"
        sthotDdRecordInstance.SoldFromIndicator = "0"
        sthotDdRecordInstance.SupervisorCashierNumber = ""
        sthotDdRecordInstance.SystemLookUpPrice = "14.99"
        sthotDdRecordInstance.TaggedItem = "0"
        sthotDdRecordInstance.TemporaryPriceChangeMargin = ""
        sthotDdRecordInstance.TemporaryPriceChangePrice = "0"
        sthotDdRecordInstance.TillId = "01"
        sthotDdRecordInstance.TransactionNumber = "0117"
        sthotDdRecordInstance.VatCode = "b"
        sthotDdRecordInstance.VatSymbolUser = ""
        sthotDdRecordInstance.VatValue = "2"

        actualValue = sthotDdRecordInstance.Record

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
End Class
