﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class ProcessTransmissions_SthoaDataStructure_Tests

    Private testContextInstance As TestContext

    Private sthoaAdRecordInstance As DataStructureSthoa.Implementation.SthotAdRecord = New DataStructureSthoa.Implementation.SthotAdRecord

    Private expectedValue As String
    Private actualValue As String

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub FormatDate_EightDigitDateSupplied_ReturnsEightDigitDate()
        expectedValue = "12/12/10"

        sthoaAdRecordInstance.Date = "12/12/10"

        actualValue = sthoaAdRecordInstance.Date

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatDate_SevenDigitDateSupplied_ReturnsEightDigitDateWithLeadinZeros()
        expectedValue = "02/12/10"

        sthoaAdRecordInstance.Date = "2/12/10"

        actualValue = sthoaAdRecordInstance.Date

        Assert.AreEqual(expectedValue, actualValue)


    End Sub
    <TestMethod()> Public Sub FormatDate_SixDigitDateSupplied_ReturnsEightDigitDate()
        expectedValue = "00/12/10"

        sthoaAdRecordInstance.Date = "/12/10"

        actualValue = sthoaAdRecordInstance.Date

        Assert.AreEqual(expectedValue, actualValue)



    End Sub
    <TestMethod()> Public Sub FormatDate_MoreThanEightDigitDateSupplied_ReturnsEightDigitDate()
        expectedValue = "00/12/10"

        sthoaAdRecordInstance.Date = "00/12/101111111111111111111111111111111111111111"

        actualValue = sthoaAdRecordInstance.Date

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustedUnits_EightDigitSupplied_ReturnsEightDigits()
        expectedValue = "12345678"

        sthoaAdRecordInstance.AdjustedUnits = "12345678"

        actualValue = sthoaAdRecordInstance.AdjustedUnits

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustedUnits_SevenDigitsSupplied_ReturnsEightDigits()
        expectedValue = "02345678"

        sthoaAdRecordInstance.AdjustedUnits = "2345678"

        actualValue = sthoaAdRecordInstance.AdjustedUnits

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustedUnits_MoreThanEightDigitsSupplied_ReturnsEightDigits()
        expectedValue = "12345678"

        sthoaAdRecordInstance.AdjustedUnits = "1234567899999999999999999999999999999999999999999"

        actualValue = sthoaAdRecordInstance.AdjustedUnits

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatAdjustedUnits_NothingSupplied_ReturnsEightDigitZeros()
        expectedValue = "00000000"

        sthoaAdRecordInstance.AdjustedUnits = ""

        actualValue = sthoaAdRecordInstance.AdjustedUnits

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustedValue_TwelveDigitSupplied_ReturnsTwelveDigits()
        expectedValue = "123456789123"

        sthoaAdRecordInstance.AdjustedValue = "123456789123"

        actualValue = sthoaAdRecordInstance.AdjustedValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatAdjustedValue_MoreThanTwelveDigitSupplied_ReturnsTwelveDigits()
        expectedValue = "123456789123"

        sthoaAdRecordInstance.AdjustedValue = "1234567891236666666666666666666666666666"

        actualValue = sthoaAdRecordInstance.AdjustedValue

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustmentAssemblyDepot_MoreThanTenDigitsSupplied_ReturnsThreeDigitsFromSeventhPosition()
        expectedValue = "543"

        sthoaAdRecordInstance.AdjustmentAssemblyDepot = "1238976543999999999999999999999999999999"

        actualValue = sthoaAdRecordInstance.AdjustmentAssemblyDepot

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatAdjustmentCode_TwoDigitsSupplied_ReturnsTwoDigits()
        expectedValue = "22"

        sthoaAdRecordInstance.AdjustmentCode = "22"

        actualValue = sthoaAdRecordInstance.AdjustmentCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustmentCode_OneDigitSupplied_ReturnsTwoDigitsWithLeadingZero()
        expectedValue = "02"

        sthoaAdRecordInstance.AdjustmentCode = "2"

        actualValue = sthoaAdRecordInstance.AdjustmentCode

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustmentReference_SixDigitSupplied_ReturnsSixDigits()
        expectedValue = "123456"

        sthoaAdRecordInstance.AdjustmentReference = "123456"

        actualValue = sthoaAdRecordInstance.AdjustmentReference

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatAdjustmentReference_MoreThanEightDigitsSupplied_ReturnsFirstSixDigits()
        expectedValue = "123456"

        sthoaAdRecordInstance.AdjustmentReference = "12345677777777777777777"

        actualValue = sthoaAdRecordInstance.AdjustmentReference

        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub FormatAdjustmentSellingPrice_TwelveDigitsSupplied_ReturnsTwelveDigits()
        expectedValue = "123456789123"

        sthoaAdRecordInstance.AdjustmentSellingPrice = "123456789123"

        actualValue = sthoaAdRecordInstance.AdjustmentSellingPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustmentSellingPrice_MoreThanTwelveDigitsSupplied_ReturnsFirstTwelveDigits()
        expectedValue = "123456789123"

        sthoaAdRecordInstance.AdjustmentSellingPrice = "12345678912399999999999999999999999"

        actualValue = sthoaAdRecordInstance.AdjustmentSellingPrice

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustmentSkuNumber_SixDigitsSupplied_ReturnsSixDigits()
        expectedValue = "123456"

        sthoaAdRecordInstance.AdjustmentSkuNumber = "123456"

        actualValue = sthoaAdRecordInstance.AdjustmentSkuNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustmentSkuNumber_MoreThanSixDigitsSupplied_ReturnsFirstSixDigits()
        expectedValue = "123456"

        sthoaAdRecordInstance.AdjustmentSkuNumber = "12345678912399999999999999999999999"

        actualValue = sthoaAdRecordInstance.AdjustmentSkuNumber

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatAdjustmentWriteOffId_ThreeDigitsSupplied_ReturnsThreeDigits()
        expectedValue = "123"

        sthoaAdRecordInstance.AdjustmentWriteOffId = "123"

        actualValue = sthoaAdRecordInstance.AdjustmentWriteOffId

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub FormatAdjustmentWriteOffId_MoreThanThreeDigitsSupplied_ReturnsFirstThreeDigits()
        expectedValue = "123"

        sthoaAdRecordInstance.AdjustmentWriteOffId = "12345678912399999999999999999999999"

        actualValue = sthoaAdRecordInstance.AdjustmentWriteOffId

        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub FormatAdjustmentWriteOffId_OneDigitSupplied_ReturnsThreeDigitsWithLeadingZeros()
        expectedValue = "001"

        sthoaAdRecordInstance.AdjustmentWriteOffId = "1"

        actualValue = sthoaAdRecordInstance.AdjustmentWriteOffId

        Assert.AreEqual(expectedValue, actualValue)

    End Sub

End Class
