﻿<TestClass()> Public Class FactoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory Unit Test - CouponsEventHeaderDate"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassEventHeaderDateNew()

        Dim NewVersionOnly As ICouponsEventHeaderDate
        Dim Factory As IBaseFactory(Of ICouponsEventHeaderDate)

        Factory = New CouponsEventHeaderFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("ProcessTransmissions.EventHeaderDateNew", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Factory Unit Test - STHOA:AD Record"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassSthoaAdRecordNew()

        Dim NewVersionOnly As ISthoaAdRecord
        Dim Factory As IBaseFactory(Of ISthoaAdRecord)

        Factory = New SthoaAdRecordFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("ProcessTransmissions.SthoaAdRecordNew", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Factory Unit Test - SupplierMaster"

    <TestMethod()> Public Sub RequirementSwitchOn_ReturnSupplierMasterUpdateBBCN()

        Dim SupplierMaster As ISupplierMaster

        ArrangeRequirementSwitchDependency(True)
        SupplierMaster = (New SupplierMasterFactory).GetImplementation
        Assert.AreEqual("ProcessTransmissions.SupplierMasterUpdateBBCN", SupplierMaster.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementSwitchOn_ReturnSupplierMasterNotUpdateBBCN()

        Dim SupplierMaster As ISupplierMaster

        ArrangeRequirementSwitchDependency(False)
        SupplierMaster = (New SupplierMasterFactory).GetImplementation
        Assert.AreEqual("ProcessTransmissions.SupplierMasterNotUpdateBBCN", SupplierMaster.GetType.FullName)

    End Sub

#End Region

#Region "Factory Unit Test - UpdateSthoc"

    <TestMethod()> Public Sub RequirementSwitchOn_ReturnSthocPreventUpdate()

        Dim UpdateSthoc As IUpdateSthoc

        ArrangeRequirementSwitchDependency(True)
        UpdateSthoc = (New UpdateSthocFactory).GetImplementation
        Assert.AreEqual("ProcessTransmissions.SthocPreventUpdate", UpdateSthoc.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementSwitchOn_ReturnSthocAllowUpdate()

        Dim UpdateSthoc As IUpdateSthoc

        ArrangeRequirementSwitchDependency(False)
        UpdateSthoc = (New UpdateSthocFactory).GetImplementation
        Assert.AreEqual("ProcessTransmissions.SthocAllowUpdate", UpdateSthoc.GetType.FullName)

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class