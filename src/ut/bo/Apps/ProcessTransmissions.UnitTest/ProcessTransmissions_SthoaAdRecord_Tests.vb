﻿<TestClass()> Public Class ProcessTransmissions_SthoaAdRecord_Tests

    Private testContextInstance As TestContext
    Private actualFormattedSellingPrice As String

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub FormatNewSthoaAdRecordSellingPrice_TwoDigitsBeforeAndAfterDecimalPointNumberSupplied_ReturnsTwelveDigitPaddedWithLeadingSpacesAndATrailingPositiveSign()

        Dim X As ISthoaAdRecord = (New SthoaAdRecordFactory).GetImplementation

        X.FormatDecToString(CDec(11.11), actualFormattedSellingPrice, 11, " ", "0.00")
        Assert.AreEqual("      11.11+", actualFormattedSellingPrice)

    End Sub

    <TestMethod()> Public Sub FormatNewSthoaAdRecordSellingPrice_TwoDigitsBeforeDecimalPointNumberSupplied_ReturnsTwelveDigitPaddedWithLeadingSpacesAndATrailingPositiveSign()

        Dim X As ISthoaAdRecord = (New SthoaAdRecordFactory).GetImplementation

        X.FormatDecToString(CDec(11), actualFormattedSellingPrice, 11, " ", "0.00")
        Assert.AreEqual("      11.00+", actualFormattedSellingPrice)

    End Sub

    <TestMethod()> Public Sub FormatNewSthoaAdRecordSellingPrice_ZeroSupplied_ReturnsTwelveDigitPaddedWithLeadingSpacesAndATrailingPositiveSign()

        Dim X As ISthoaAdRecord = (New SthoaAdRecordFactory).GetImplementation

        X.FormatDecToString(CDec(0), actualFormattedSellingPrice, 11, " ", "0.00")
        Assert.AreEqual("       0.00+", actualFormattedSellingPrice)

    End Sub

    <TestMethod()> Public Sub FormatNewSthoaAdRecordSellingPrice_NothingSupplied_ReturnsTwelveDigitPaddedWithLeadingSpacesAndATrailingPositiveSign()

        Dim X As ISthoaAdRecord = (New SthoaAdRecordFactory).GetImplementation

        X.FormatDecToString(CDec(0), actualFormattedSellingPrice, 11, " ", "0.00")
        Assert.AreEqual("       0.00+", actualFormattedSellingPrice)

    End Sub


    <TestMethod()> Public Sub FormatNewSthoaAdRecordSellingPrice_TwoDigitsAfterDecimalPointSupplied_ReturnsTwelveDigitPaddedWithLeadingSpacesAndATrailingPositiveSign()

        Dim X As ISthoaAdRecord = (New SthoaAdRecordFactory).GetImplementation

        X.FormatDecToString(CDec(0.12), actualFormattedSellingPrice, 11, " ", "0.00")
        Assert.AreEqual("       0.12+", actualFormattedSellingPrice)

    End Sub

    <TestMethod()> Public Sub FormatNewSthoaAdRecordSellingPrice_NegativeValueWithTwoDigitsAfterDecimalPointSupplied_ReturnsTwelveDigitPaddedWithLeadingSpacesAndATrailingNegativeSign()

        Dim X As ISthoaAdRecord = (New SthoaAdRecordFactory).GetImplementation

        X.FormatDecToString(CDec(-0.12), actualFormattedSellingPrice, 11, " ", "0.00")
        Assert.AreEqual("       0.12-", actualFormattedSellingPrice)

    End Sub

End Class