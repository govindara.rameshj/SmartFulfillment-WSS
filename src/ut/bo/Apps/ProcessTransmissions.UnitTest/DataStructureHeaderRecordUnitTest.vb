﻿<TestClass()> Public Class DataStructureHeaderRecordUnitTest

    Private testContextInstance As TestContext

    Private _Header As IHeaderRecord

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub

    <TestInitialize()> Public Sub MyTestInitialize()

        _Header = New HeaderRecord("HR85816/04/12HPSTV13000695150017UPDATE    ")

    End Sub

    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub RecordType()

        Assert.AreEqual("HR", _Header.RecordType)

    End Sub

    <TestMethod()> Public Sub StoreNumber()

        Assert.AreEqual("858", _Header.StoreNumber)

    End Sub

    <TestMethod()> Public Sub [Date]()

        Assert.AreEqual("16/04/12", _Header.Date)

    End Sub

    <TestMethod()> Public Sub FileType()

        Assert.AreEqual("HPSTV", _Header.FileType)

    End Sub

    <TestMethod()> Public Sub VersionNumber()

        Assert.AreEqual("13", _Header.VersionNumber)

    End Sub

    <TestMethod()> Public Sub SequenceNumber()

        Assert.AreEqual("000695", _Header.SequenceNumber)

    End Sub

    <TestMethod()> Public Sub Time()

        Assert.AreEqual("150017", _Header.Time)

    End Sub

    <TestMethod()> Public Sub FileDescription()

        Assert.AreEqual("UPDATE", _Header.FileDescription)

    End Sub

End Class