﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Friend Class TestOrderConfirmationReport
    Inherits GetOrderConfirmationReportPrintHandlerFactory

    Friend _Enabled As Boolean = False

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Return _Enabled
    End Function
End Class
<TestClass()> Public Class ProcessTransmissions_RemoveOrderConfirmationReportFromClose_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub CallFromCloseFactory_RequirementSwitch_ReturnsNoDRLFilter()
        Dim Repository As IReportPrintingHandler
        Dim Factory As New TestOrderConfirmationReport
        Factory._Enabled = False
        Repository = Factory.GetImplementation
        Assert.AreEqual("ProcessTransmissions.PrintOrderConfirmationReport", Repository.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub CallFromCloseFactory_RequirementsSwitch_ReturnsReceiptsOnly()
        Dim Repository As IReportPrintingHandler
        Dim Factory As New TestOrderConfirmationReport
        Factory._Enabled = True
        Repository = Factory.GetImplementation
        Assert.AreEqual("ProcessTransmissions.NonPrintingOrderConfirmationReport", Repository.GetType.FullName)
    End Sub

End Class
