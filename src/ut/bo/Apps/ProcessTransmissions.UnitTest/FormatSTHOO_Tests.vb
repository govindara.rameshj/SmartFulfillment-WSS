﻿<TestClass()> Public Class FormatSTHOO_Tests

    Private testContextInstance As TestContext

    Private Const ShowSignSymbol As Boolean = True
    Private Const PostiveSignSymbol As String = " "
    Private Const NegativeSignSymbol As String = "-"

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub FormatSthooString_SingleRow_PositiveValue_CorrectOuput()

        Dim DR As DataRow
        Dim X As TpWickes.ISthooFormatOutput
        Dim ActualOutput As String
        Dim ExpectedOutput As String

        'Before writting output string to file, further formatting occurrs
        'Actual process not defined here but for example the '+' sign will be removed

        DR = ConfigureStockDataTable().Rows.Add("100803", "51902", 10)
        X = TpWickes.SthooFormatOutputFactory.FactoryGet("080", New System.DateTime(2011, 8, 14))

        ActualOutput = X.FormatData(DR, "0.00", 11, CType(" ", Char))
        ExpectedOutput = "AL" & Now.Date.ToString("dd/MM/yy") & "      10.00 14/08/1108010080351902000010"

        Assert.AreEqual(ExpectedOutput, ActualOutput)

    End Sub

    <TestMethod()> Public Sub DataSetTest()

        Dim DS As DataSet
        Dim RepositoryStub As New TpWickes.StockRepositoryStub
        Dim FileSystemStub As New TpWickes.SthooFileSystemStub

        Dim ActualOutput As New List(Of String)
        Dim ExpectedOutput As New List(Of String)

        'arrange
        'configure stub data
        DS = ConfigureDataSet()
        With DS
            'stock data
            .Tables.Item(0).Rows.Add("100803", "51902", 10)
            .Tables.Item(0).Rows.Add("222222", "12345", 20)
            .Tables.Item(0).Rows.Add("333333", "67890", 30)
            'store name
            .Tables.Item(1).Rows.Add("080")
            'today date
            .Tables.Item(2).Rows.Add(New System.DateTime(2011, 8, 14))
        End With
        RepositoryStub.SetDataSet = DS

        'use stubs
        TpWickes.StockRepositoryFactory.FactorySet(RepositoryStub)
        TpWickes.SthooFileSystemFactory.FactorySet(FileSystemStub)

        '
        ExpectedOutput.Add("AL" & Now.Date.ToString("dd/MM/yy") & "      10.00 14/08/1108010080351902000010")
        ExpectedOutput.Add("AL" & Now.Date.ToString("dd/MM/yy") & "      20.00 14/08/1108022222212345000020")
        ExpectedOutput.Add("AL" & Now.Date.ToString("dd/MM/yy") & "      30.00 14/08/1108033333367890000030")

        'act
        MainModule.StockOnHand()

        'asert
        For Each Line As String In FileSystemStub.StubLineText
            ActualOutput.Add(Line)
        Next

        Assert.IsTrue(ExpectedOutput.SequenceEqual(ActualOutput))

    End Sub

#Region "String Formatting Code"

#Region "Hash Formatting"

    <TestMethod()> Public Sub HashingValue_ValuePositiveTen_PadCharacterBlankSpace_PadLengthEleven()

        Assert.AreEqual("      10.00 ", TpWickes.StringFormatting.HashingValue(10, "0.00", 11, CType(" ", Char), ShowSignSymbol, PostiveSignSymbol, NegativeSignSymbol))

    End Sub

    <TestMethod()> Public Sub HashingValue_ValueNegativeTen_PadCharacterBlankSpace_PadLengthEleven()

        Assert.AreEqual("      10.00-", TpWickes.StringFormatting.HashingValue(-10, "0.00", 11, CType(" ", Char), ShowSignSymbol, PostiveSignSymbol, NegativeSignSymbol))

    End Sub

    <TestMethod()> Public Sub HashingValue_ValuePositiveTen_PadCharacterZero_PadLengthEleven()

        Assert.AreEqual("00000010.00 ", TpWickes.StringFormatting.HashingValue(10, "0.00", 11, CType("0", Char), ShowSignSymbol, PostiveSignSymbol, NegativeSignSymbol))

    End Sub

    <TestMethod()> Public Sub HashingValue_ValueNegativeTen_PadCharacterZero_PadLengthEleven()

        Assert.AreEqual("00000010.00-", TpWickes.StringFormatting.HashingValue(-10, "0.00", 11, CType("0", Char), ShowSignSymbol, PostiveSignSymbol, NegativeSignSymbol))

    End Sub

#End Region

#Region "Date Formatting"

    <TestMethod()> Public Sub FormatDateDDMMYY()

        Assert.AreEqual("20/12/10", TpWickes.StringFormatting.FormatDateDDMMYY(New DateTime(2010, 12, 20)))

    End Sub

    <TestMethod()> Public Sub FormatDateDDYYMM()

        Assert.AreEqual("20/12/2010", TpWickes.StringFormatting.FormatDateDDMMYYYY(New DateTime(2010, 12, 20)))

    End Sub

#End Region

#Region "Pad Left"

#Region "String"

    <TestMethod()> Public Sub PaddingLeft_TypeString_ValueOne_PadCharacterZero_PadLengthOne()

        Assert.AreEqual("1", TpWickes.StringFormatting.PaddingLeft("1", 1, CType("0", Char)))

    End Sub

    <TestMethod()> Public Sub PaddingLeft_TypeString_ValueOne_PadCharacterZero_PadLengthSix()

        Assert.AreEqual("000001", TpWickes.StringFormatting.PaddingLeft("1", 6, CType("0", Char)))

    End Sub

#End Region

#Region "Decimal"

    <TestMethod()> Public Sub PaddingLeft_TypeDecimal_ValueOne_PadCharacterZero_PadLengthOne()

        Assert.AreEqual("1", TpWickes.StringFormatting.PaddingLeft(1, 1, CType("0", Char), False))

    End Sub

    <TestMethod()> Public Sub PaddingLeft_TypeDecimal_ValueOne_PadCharacterZero_PadLengthSix()

        Assert.AreEqual("000001", TpWickes.StringFormatting.PaddingLeft(1, 6, CType("0", Char), False))

    End Sub

    <TestMethod()> Public Sub PaddingLeft_TypeDecimal_ValuePlusOne_PadCharacterZero_PadLengthSix()

        Assert.AreEqual("+000001", TpWickes.StringFormatting.PaddingLeft(1, 6, CType("0", Char), True))

    End Sub

    <TestMethod()> Public Sub PaddingLeft_TypeDecimal_ValueMinusOne_PadCharacterZero_PadLengthSix()

        Assert.AreEqual("-000001", TpWickes.StringFormatting.PaddingLeft(-1, 6, CType("0", Char), True))

    End Sub





    <TestMethod()> Public Sub PaddingLeftFormatSpecifier_TypeDecimal_ValueMisc_PadCharacterZero_PadLengthSix_TestTypeOne()

        Assert.AreEqual("012.56", TpWickes.StringFormatting.PaddingLeftFormatSpecifier(CType(12.56, Decimal), "0.00", 6, CType("0", Char), False))

    End Sub

    <TestMethod()> Public Sub PaddingLeftFormatSpecifier_TypeDecimal_ValueMisc_PadCharacterZero_PadLengthSix_TestTypeTwo()

        Dim ExpectedOutput As String = CType(12.56, Decimal).ToString("0.00").PadLeft(6, CType("0", Char))
        Dim ActualOutput As String = TpWickes.StringFormatting.PaddingLeftFormatSpecifier(CType(12.56, Decimal), "0.00", 6, CType("0", Char), False)

        Assert.AreEqual(ExpectedOutput, ActualOutput)

    End Sub

    <TestMethod()> Public Sub PaddingLeftFormatSpecifier_TypeDecimal_ValuePlusOne_PadCharacterZero_PadLengthSix()

        Assert.AreEqual("-001.00", TpWickes.StringFormatting.PaddingLeftFormatSpecifier(-1, "0.00", 6, CType("0", Char), True))

    End Sub

    <TestMethod()> Public Sub PaddingLeftFormatSpecifier_TypeDecimal_ValueMinusOne_PadCharacterZero_PadLengthSix()

        Assert.AreEqual("-001.00", TpWickes.StringFormatting.PaddingLeftFormatSpecifier(-1, "0.00", 6, CType("0", Char), True))

    End Sub

#End Region

#End Region

#Region "Pad Right"

    <TestMethod()> Public Sub PaddingRight_TypeString_ValueOne_PadCharacterZero_PadLengthOne()

        Assert.AreEqual("1", TpWickes.StringFormatting.PaddingRight("1", 1, CType("0", Char)))

    End Sub

    <TestMethod()> Public Sub PaddingRight_TypeString_ValueOne_PadCharacterZero_PadLengthSix()

        Assert.AreEqual("100000", TpWickes.StringFormatting.PaddingRight("1", 6, CType("0", Char)))

    End Sub

    <TestMethod()> Public Sub PaddingRight_TypeDecimal_ValueOne_PadCharacterZero_PadLengthOne()

        Assert.AreEqual("1", TpWickes.StringFormatting.PaddingRight(1, 1, CType("0", Char), False))

    End Sub

    <TestMethod()> Public Sub PaddingRight_TypeDecimal_ValueOne_PadCharacterZero_PadLengthSix()

        Assert.AreEqual("100000", TpWickes.StringFormatting.PaddingRight(1, 6, CType("0", Char), False))

    End Sub

#End Region

#End Region

#Region "Private Procedures And Functions"

    Private Function ConfigureDataSet() As DataSet

        Dim DS As New DataSet

        With DS
            .Tables.Add(ConfigureStockDataTable())
            .Tables.Add(ConfigureStoreDataTable())
            .Tables.Add(ConfigureTodayDateDataTable())
        End With
        Return DS

    End Function

    Private Function ConfigureStockDataTable() As DataTable

        Dim DT As New DataTable

        With DT
            .Columns.Add(New Data.DataColumn("SkuNumber", GetType(System.String)))
            .Columns.Add(New Data.DataColumn("PrimarySupplierNumber", GetType(System.String)))
            .Columns.Add(New Data.DataColumn("OnOrderQuantity", GetType(System.Int32)))
        End With
        Return DT

    End Function

    Private Function ConfigureStoreDataTable() As DataTable

        Dim DT As New DataTable

        With DT
            .Columns.Add(New Data.DataColumn("StoreNumber", GetType(System.String)))
        End With
        Return DT

    End Function

    Private Function ConfigureTodayDateDataTable() As DataTable

        Dim DT As New DataTable

        With DT
            .Columns.Add(New Data.DataColumn("RecordDate", GetType(System.DateTime)))
        End With
        Return DT

    End Function

#End Region

End Class
