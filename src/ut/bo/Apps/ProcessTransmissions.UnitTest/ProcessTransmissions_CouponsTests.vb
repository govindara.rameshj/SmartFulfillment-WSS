﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class ProcessTransmissions_CouponsTests

    Private testContextInstance As TestContext

    Private couponsUCRecordInstance As DataStructure.Implementation.UcRecord = New DataStructure.Implementation.UcRecord("UC23/09/11       1.00 003691TS16236900000000000016.34 000000 000000 000000.00 000.00 Y12345677654321")
    Private couponsUTRecordInstance As DataStructure.Implementation.UtRecord = New DataStructure.Implementation.UtRecord("UT23/09/11       1.00 123456701ML1234567891123456782212345678931234567894123456789512N")
    Private couponsUMRecordInstance As DataStructure.Implementation.UmRecord = New DataStructure.Implementation.UmRecord("UM23/09/11       1.00 1234567This is a textcouponNNNNNN")


    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region


    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedCouponId()
        Dim expectedValue As String = "1234567"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.BuyCouponNumber
        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedDate()

        Dim expectedValue As String = "23/09/11"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.Date
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedBuyQuantity()

        Dim expectedValue As String = "000000 "
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.BuyQuantity
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedDeleteIndicator()

        Dim expectedValue As String = "Y"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.DeleteIndicator
        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedDiscountValue()

        Dim expectedValue As String = "000000.00 "
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.DiscountValue
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedEventNumber()

        Dim expectedValue As String = "003691"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.EventNumber
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedEventType()

        Dim expectedValue As String = "TS"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.EventType
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedGetCouponNumber()

        Dim expectedValue As String = "7654321"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.GetCouponNumber
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedGetQuantity()

        Dim expectedValue As String = "000000 "
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.GetQuantity
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedGetQuantityDiscountPercentage()

        Dim expectedValue As String = "000.00 "
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.GetQuantityDiscountPercentage
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedItemKey1()

        Dim expectedValue As String = "162369"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.ItemKey1
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedItemKey2()

        Dim expectedValue As String = "00000000"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.ItemKey2
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedPrice()

        Dim expectedValue As String = "000016.34 "
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.Price
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UcRecordPassEntireStringGetFormattedRecordType()

        Dim expectedValue As String = "UC"
        Dim actualValue As String = ""
        actualValue = couponsUCRecordInstance.RecordType
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UtRecordPassEntireStringGetFormattedCouponNumber()
        Dim expectedValue As String = "1234567"
        Dim actualValue As String = ""
        actualValue = couponsUTRecordInstance.CouponNumber
        Assert.AreEqual(expectedValue, actualValue)

    End Sub



    <TestMethod()> Public Sub UtRecordPassEntireStringGetFormattedAlignment()
        Dim expectedValue As String = "L"
        Dim actualValue As String = ""
        actualValue = couponsUTRecordInstance.Alignment
        Assert.AreEqual(expectedValue, actualValue)

    End Sub



    <TestMethod()> Public Sub UtRecordPassEntireStringGetFormattedDate()
        Dim expectedValue As String = "23/09/11"
        Dim actualValue As String = ""
        actualValue = couponsUTRecordInstance.Date
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UtRecordPassEntireStringGetFormattedDeletedIndicator()
        Dim expectedValue As String = "N"
        Dim actualValue As String = ""
        actualValue = couponsUTRecordInstance.Deleted
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UtRecordPassEntireStringGetFormattedPrintSize()
        Dim expectedValue As String = "M"
        Dim actualValue As String = ""
        actualValue = couponsUTRecordInstance.PrintSize
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UtRecordPassEntireStringGetFormattedRecordType()
        Dim expectedValue As String = "UT"
        Dim actualValue As String = ""
        actualValue = couponsUTRecordInstance.RecordType
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UtRecordPassEntireStringGetFormattedText()
        Dim expectedValue As String = "1234567891123456782212345678931234567894123456789512"
        Dim actualValue As String = ""
        actualValue = couponsUTRecordInstance.Text
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UtRecordPassEntireStringGetFormattedTextLineDisplaySequence()
        Dim expectedValue As String = "01"
        Dim actualValue As String = ""
        actualValue = couponsUTRecordInstance.TextLineDisplaySequence
        Assert.AreEqual(expectedValue, actualValue)

    End Sub
    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedCouponNumber()

        Dim expectedValue As String = "1234567"
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.CouponNumber
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedCouponNumberDescription()

        Dim expectedValue As String = ""
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.CouponNumberDescription
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedDate()

        Dim expectedValue As String = "23/09/11"
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.Date
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedDeletedIndicator()

        Dim expectedValue As String = ""
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.DeletedIndicator
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedExclusiveIndicator()

        Dim expectedValue As String = ""
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.ExclusiveIndicator
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedRecordType()

        Dim expectedValue As String = "UM"
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.RecordType
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedReusableIndicator()

        Dim expectedValue As String = ""
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.ReusableIndicator
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedSerialNumberIndicator()

        Dim expectedValue As String = ""
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.SerialNumberIndicator
        Assert.AreEqual(expectedValue, actualValue)

    End Sub


    <TestMethod()> Public Sub UmRecordPassEntireStringGetFormattedStoreGenIndicator()

        Dim expectedValue As String = ""
        Dim actualValue As String = ""
        actualValue = couponsUMRecordInstance.StoreGenIndicator
        Assert.AreEqual(expectedValue, actualValue)

    End Sub

End Class
