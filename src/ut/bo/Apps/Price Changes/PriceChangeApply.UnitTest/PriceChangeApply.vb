﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class PriceChangeApply

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub PriceChangeApply_StoreIDMatchEStoreShopID_HenceNewBusinessObject_EmptyData_ZeroRecordsReturned()

        Dim V As IPriceChange = Nothing
        Dim DT As DataTable

        'arrange
        DT = CreateTable()
        Arrange(V, 120, 8120, 0, DT)

        Assert.AreEqual(True, IIf(V.PriceChanges.Count = 0, True, False))
    End Sub

    <TestMethod()> Public Sub PriceChangeApply_StoreIDMatchEStoreShopID_HenceNewBusinessObject_NonEmptyData_NonZeroRecordsReturned()

        Dim V As IPriceChange = Nothing
        Dim DT As DataTable

        'arrange
        DT = CreateTable()
        DT.Rows.Add("160596", Now.Date, 888.88, "U", True, Now.Date, Now.Date, 999.99, False, False, False, False, String.Empty, String.Empty, String.Empty, String.Empty)
        Arrange(V, 120, 8120, 0, DT)

        Assert.AreEqual(True, IIf(V.PriceChanges.Count <> 0, True, False))
    End Sub

    'Improvement: could compare state between existing and new busines class instances, SHOULD match?

#End Region

#Region "Private Test Procedures And Functions"

    Private Function CreateTable() As DataTable

        Dim DT As DataTable

        'arrange
        DT = New DataTable
        DT.Columns.Add("SKUN", System.Type.GetType("System.String"))
        DT.Columns.Add("PDAT", System.Type.GetType("System.DateTime"))
        DT.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("PSTA", System.Type.GetType("System.String"))
        DT.Columns.Add("SHEL", System.Type.GetType("System.Boolean"))
        DT.Columns.Add("AUDT", System.Type.GetType("System.DateTime"))
        DT.Columns.Add("AUAP", System.Type.GetType("System.DateTime"))
        DT.Columns.Add("MARK", System.Type.GetType("System.Decimal"))
        DT.Columns.Add("MCOM", System.Type.GetType("System.Boolean"))
        DT.Columns.Add("LABS", System.Type.GetType("System.Boolean"))
        DT.Columns.Add("LABM", System.Type.GetType("System.Boolean"))
        DT.Columns.Add("LABL", System.Type.GetType("System.Boolean"))
        DT.Columns.Add("EVNT", System.Type.GetType("System.String"))
        DT.Columns.Add("PRIO", System.Type.GetType("System.String"))
        DT.Columns.Add("EEID", System.Type.GetType("System.String"))
        DT.Columns.Add("MEID", System.Type.GetType("System.String"))

        Return DT

    End Function

    Private Sub Arrange(ByRef V As IPriceChange, ByVal StoreID As Integer, ByVal eStoreShopID As Integer, ByVal eStoreWarehouseID As Integer, ByRef DT As DataTable)

        Dim Stub As New PriceChangeRepositoryStub

        Stub.ConfigureStub(StoreID, eStoreShopID, eStoreWarehouseID, DT)
        PriceChangeRepositoryFactory.FactorySet(Stub)
        V = PriceChangeFactory.FactoryGet
        V.Load(New System.DateTime) 'date not important, using stub

    End Sub

#End Region

End Class
