﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data
Imports UpdPriceChgFrmEvts
Imports BOStock
Imports Rhino.Mocks

#Region "Scenarios"

Friend Class ProposedPriceChange_OneRowScenario

    Public Shared ReadOnly Property DataTable() As DataTable
        Get
            Dim dt As New DataTable("ProposedPriceChange")
            Dim dr As DataRow = dt.NewRow

            dt.Columns.Add("SKUN", System.Type.GetType("System.String"))
            dt.Columns.Add("SDAT", System.Type.GetType("System.String"))
            dt.Columns.Add("PRIO", System.Type.GetType("System.String"))
            dt.Columns.Add("NUMB", System.Type.GetType("System.String"))
            dt.Columns.Add("EDAT", System.Type.GetType("System.String"))
            dt.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
            dt.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

            Dim newTable As DataTable = dt.Clone

            dr("SKUN") = "100803"
            dr("SDAT") = CDate("1 Apr 2010")
            dr("PRIO") = "10"
            dr("NUMB") = "000100"
            dr("EDAT") = DBNull.Value
            dr("PRIC") = CDec(5.0)
            dr("IDEL") = False

            dt.Rows.Add(dr)

            Return dt
        End Get
    End Property

End Class

Friend Class ProposedPriceChange_TwoRowScenario

    Public Shared ReadOnly Property DataTable() As DataTable
        Get
            Dim dt As New DataTable("ProposedPriceChange")
            Dim dr As DataRow = dt.NewRow

            dt.Columns.Add("SKUN", System.Type.GetType("System.String"))
            dt.Columns.Add("SDAT", System.Type.GetType("System.String"))
            dt.Columns.Add("PRIO", System.Type.GetType("System.String"))
            dt.Columns.Add("NUMB", System.Type.GetType("System.String"))
            dt.Columns.Add("EDAT", System.Type.GetType("System.String"))
            dt.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
            dt.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

            Dim newTable As DataTable = dt.Clone

            dr("SKUN") = "100803"
            dr("SDAT") = CDate("1 Apr 2010")
            dr("PRIO") = "10"
            dr("NUMB") = "000100"
            dr("EDAT") = DBNull.Value
            dr("PRIC") = CDec(5.0)
            dr("IDEL") = False

            dt.Rows.Add(dr)
            dr = dt.NewRow

            dr("SKUN") = "562103"
            dr("SDAT") = CDate("21 May 2012")
            dr("PRIO") = "20"
            dr("NUMB") = "222100"
            dr("EDAT") = DBNull.Value
            dr("PRIC") = CDec(13.49)
            dr("IDEL") = False

            dt.Rows.Add(dr)

            Return dt

        End Get
    End Property

End Class

#End Region

<TestClass()> Public Class UpdPriceChgFrmEvts_UpdPriceChgFrmEvts_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub CreatePriceChange_DataColumnToDelimitedTextConverter_WithOnePriceChange_ReturnsExpectedValues()

        Dim pChg As New DataColumnToDelimitedTextConverter

        pChg.DataTable = ProposedPriceChange_OneRowScenario.DataTable

        Assert.AreEqual("100803", pChg.DelimitedValues("SKUN"), "SKUN")
        Assert.AreEqual(CDate("1 Apr 2010").ToString, pChg.DelimitedValues("SDAT"), "SDAT")
        Assert.AreEqual(CDec(5.0).ToString, pChg.DelimitedValues("PRIC"), "PRIC")

    End Sub

    <TestMethod()> Public Sub CreatePriceChange_DataColumnToDelimitedTextConverter_WithTwoPriceChanges_ReturnsExpectedValues()

        Dim pChg As New DataColumnToDelimitedTextConverter

        pChg.DataTable = ProposedPriceChange_TwoRowScenario.DataTable

        Assert.AreEqual("100803,562103", pChg.DelimitedValues("SKUN"), "SKUN")
        Assert.AreEqual(CDate("1 Apr 2010").ToString & "," & CDate("21 May 2012").ToString, pChg.DelimitedValues("SDAT"), "SDAT")
        Assert.AreEqual(CDec(5.0).ToString & "," & CDec(13.49).ToString, pChg.DelimitedValues("PRIC"), "PRIC")

    End Sub

End Class
