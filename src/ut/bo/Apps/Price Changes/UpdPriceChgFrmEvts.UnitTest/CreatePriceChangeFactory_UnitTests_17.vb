﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data
Imports System.Data.DataColumn
Imports UpdPriceChgFrmEvts
Imports Rhino.Mocks

<TestClass()> Public Class CreatePriceChangeFactory_UnitTests_17

    Private testContextInstance As TestContext

    Private Shared _useNewFunctionality As Boolean = True
    Private Shared _PriceChange As CreatePriceChange
    Private Shared _GetData As GetData
    Private Shared _StartDate As Date
    Private Shared _Today As Date
    Private Shared _DaysPrior As Integer
    Private Shared _strEdat As String
    Private Shared _evtchg As New DataTable("EVTCHG")
    Private Shared _evtchgRow As DataRow = _evtchg.NewRow
    Private Shared _mocks As New MockRepository
    Private Shared _rowStock As IStock = _mocks.Stub(Of IStock)()

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Start"

    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        _evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        _evtchg.Columns.Add("SDAT", System.Type.GetType("System.DateTime"))
        _evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        _evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        _evtchg.Columns.Add("EDAT", System.Type.GetType("System.DateTime"))
        _evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        _evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))
    End Sub

    <TestInitialize()> Public Sub MyTestInitialize()

        _PriceChange = New CreatePriceChange

        _Today = CDate("28 may 2010")
        _DaysPrior = 7
        _strEdat = "29 May 2010"

        _evtchgRow("SKUN") = "100803"
        _evtchgRow("SDAT") = CDate("1 Apr 2010")
        _evtchgRow("PRIO") = "10"
        _evtchgRow("NUMB") = "000100"
        _evtchgRow("EDAT") = DBNull.Value
        _evtchgRow("PRIC") = CDec(25.0)
        _evtchgRow("IDEL") = False
        _evtchg.Rows.Add(_evtchgRow)

        _evtchgRow = _evtchg.NewRow
        _evtchgRow("SKUN") = "100803"
        _evtchgRow("SDAT") = CDate("26 May 2010")
        _evtchgRow("PRIO") = "20"
        _evtchgRow("NUMB") = "000105"
        _evtchgRow("EDAT") = CDate("29 May 2010")
        _evtchgRow("PRIC") = CDec(20.0)
        _evtchgRow("IDEL") = False
        _evtchg.Rows.Add(_evtchgRow)

        _evtchgRow = _evtchg.NewRow
        _evtchgRow("SKUN") = "100803"
        _evtchgRow("SDAT") = CDate("30 May 2010")
        _evtchgRow("PRIO") = "10"
        _evtchgRow("NUMB") = "000120"
        _evtchgRow("EDAT") = DBNull.Value
        _evtchgRow("PRIC") = CDec(45.0)
        _evtchgRow("IDEL") = False
        _evtchg.Rows.Add(_evtchgRow)

        _evtchgRow = _evtchg.NewRow
        _evtchgRow("SKUN") = "100803"
        _evtchgRow("SDAT") = CDate("31 May 2010")
        _evtchgRow("PRIO") = "10"
        _evtchgRow("NUMB") = "000125"
        _evtchgRow("EDAT") = DBNull.Value
        _evtchgRow("PRIC") = CDec(25.0)
        _evtchgRow("IDEL") = False
        _evtchg.Rows.Add(_evtchgRow)

        _GetData = New GetData
        _GetData.GetEffectiveEVTCHGRecordsForDateRange(_Today, 7, _evtchg)

        With _rowStock
            .RetailPriceEventNo = "000105"
            .RetailPricePriority = "20"
            .NormalSellPrice = CDec(20.0)
            .SkuNumber = "100803"
        End With

        _mocks.ReplayAll()

    End Sub

    <TestCleanup()> Public Sub MyTestCleanup()

        _evtchg.Clear()

    End Sub

#End Region

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Row 1"

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_NumberOfRows()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual(2, newTable.Rows.Count)

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_skun_Row1()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual("100803", newTable.Rows(0).Item("SKUN"))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_PriorityRow1()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual("10", newTable.Rows(0).Item("PRIO"))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_NUMBRow1()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual("000120", newTable.Rows(0).Item("NUMB"))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_PriceRow1()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual(CDec(45.0), CDec(newTable.Rows(0).Item("PRIC")))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_EDATRow1()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual(True, IsDBNull(newTable.Rows(0).Item("EDAT")))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_SDATRow1()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual(CDate("30 May 2010"), CDate(newTable.Rows(0).Item("SDAT")))

    End Sub

#End Region

#Region "ROW 2"

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_skun_Row2()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual("100803", newTable.Rows(1).Item("SKUN"))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_SDAT_Row2()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual(CDate("31 May 2010"), CDate(newTable.Rows(1).Item("SDAT")))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_Priority_Row2()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual("10", newTable.Rows(1).Item("PRIO"))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_NUMB_Row2()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual("000125", newTable.Rows(1).Item("NUMB"))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_Price_Row2()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual(CDec(25.0), CDec(newTable.Rows(1).Item("PRIC")))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_EDAT_Row2()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual(True, IsDBNull(newTable.Rows(1).Item("EDAT")))

    End Sub

    <TestMethod()> Public Sub SKUOnPromotionAnd2NewBasePricesSent_ReturnsPriceChanges_WithExpected_IDEL_Row2()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, _rowStock, newTable, _Today)
        Next

        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)

        ' assert
        Assert.AreEqual(False, newTable.Rows(1).Item("IDEL"))

    End Sub

#End Region

End Class