﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports UpdPriceChgFrmEvts
Imports System.Data
Imports System.Data.DataColumn
Imports Rhino.Mocks

<TestClass()> Public Class CreatePriceChangeFactory_UnitTests

    Private testContextInstance As TestContext
    Private Shared _useNewFunctionality As Boolean = True

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        _useNewFunctionality = True
    End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub CreatePriceChangeFactory_PriceChangeAlreadyActive_Test1_NewTableIsEmpty()
        Dim mocks As New MockRepository

        Dim priceChange As New CreatePriceChange

        Dim startDate As Date
        Dim Today As Date
        Dim DaysPrior As Integer
        Dim strEdat As String
        Dim evtchg As New DataTable("EVTCHG")
        Dim evtchgRow As DataRow = evtchg.NewRow
        Dim rowStock As IStock = mocks.Stub(Of IStock)()

        startDate = CDate("1 apr 2010")
        Today = CDate("27 may 2010")
        DaysPrior = 7
        strEdat = ""

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim newTable As DataTable = evtchg.Clone

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("1 Apr 2010")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "000100"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(5.0)
        evtchgRow("IDEL") = False

        evtchg.Rows.Add(evtchgRow)

        With rowStock
            .RetailPriceEventNo = "000100"
            .RetailPricePriority = "10"
            .NormalSellPrice = 5
            .SkuNumber = "100803"
        End With

        ' act
        priceChange.CheckAndApplyPriceChange(startDate, DaysPrior, strEdat, evtchgRow, rowStock, newTable, Today)

        ' assert
        Assert.AreEqual(0, newTable.Rows.Count)
    End Sub

    <TestMethod()> Public Sub CreatePriceChangeFactory_PromotionalPriceChangeEffectiveNextDayWithExistingBasePrice_Test2_PriceChangeSKUAsExpected()
        Dim PriceChange As New CreatePriceChange

        Dim mocks As New MockRepository

        Dim StartDate As Date
        Dim Today As Date
        Dim DaysPrior As Integer
        Dim strEdat As String
        Dim evtchg As New DataTable("EVTCHG")
        Dim evtchgRow As DataRow = evtchg.NewRow
        Dim rowStock As IStock = mocks.Stub(Of IStock)()

        StartDate = CDate("1 apr 2010")
        Today = CDate("27 may 2010")
        DaysPrior = 7
        strEdat = ""

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim newTable As DataTable = evtchg.Clone

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("1 Apr 2010")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "000100"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(5.0)
        evtchgRow("IDEL") = False


        evtchg.Rows.Add(evtchgRow)

        evtchgRow = evtchg.NewRow

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("27 May 2010")
        evtchgRow("PRIO") = "20"
        evtchgRow("NUMB") = "000105"
        evtchgRow("EDAT") = CDate("29 July 2010")
        evtchgRow("PRIC") = CDec(7.5)
        evtchgRow("IDEL") = False

        evtchg.Rows.Add(evtchgRow)

        With rowStock
            .RetailPriceEventNo = "000100"
            .RetailPricePriority = "10"
            .NormalSellPrice = CDec(5)
            .SkuNumber = "100803"
        End With

        ' act 
        For Each row As DataRow In evtchg.Rows
            PriceChange.CheckAndApplyPriceChange(StartDate, DaysPrior, strEdat, row, rowStock, newTable, Today)
        Next

        ' assert
        Assert.AreEqual("100803", newTable.Rows(0).Item("SKUN"))
    End Sub

    <TestMethod()> Public Sub CreatePriceChangeFactory_PromotionalPriceChangeEffectiveNextDayWithExistingBasePrice_Test2_PriceChangeDateAsExpected()
        Dim PriceChange As New CreatePriceChange

        Dim mocks As New MockRepository

        Dim StartDate As Date
        Dim Today As Date
        Dim DaysPrior As Integer
        Dim strEdat As String
        Dim evtchg As New DataTable("EVTCHG")
        Dim evtchgRow As DataRow = evtchg.NewRow
        Dim rowStock As IStock = mocks.Stub(Of IStock)()

        StartDate = CDate("1 apr 2010")
        Today = CDate("27 may 2010")
        DaysPrior = 7
        strEdat = ""

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim newTable As DataTable = evtchg.Clone

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("1 Apr 2010")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "000100"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(5.0)
        evtchgRow("IDEL") = False


        evtchg.Rows.Add(evtchgRow)

        evtchgRow = evtchg.NewRow

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("27 May 2010")
        evtchgRow("PRIO") = "20"
        evtchgRow("NUMB") = "000105"
        evtchgRow("EDAT") = CDate("29 July 2010")
        evtchgRow("PRIC") = CDec(7.5)
        evtchgRow("IDEL") = False

        evtchg.Rows.Add(evtchgRow)

        With rowStock
            .RetailPriceEventNo = "000100"
            .RetailPricePriority = "10"
            .NormalSellPrice = CDec(5)
            .SkuNumber = "100803"
        End With

        mocks.ReplayAll()

        ' act
        For Each row As DataRow In evtchg.Rows
            PriceChange.CheckAndApplyPriceChange(StartDate, DaysPrior, strEdat, row, rowStock, newTable, Today)
        Next

        ' assert
        Assert.AreEqual(CDate("27 May 2010"), CDate(newTable.Rows(0).Item("SDAT")))
    End Sub

    <TestMethod()> Public Sub CreatePriceChangeFactory_PromotionalPriceChangeEffectiveNextDayWithExistingBasePrice_Test2_PriceChangePriorityAsExpected()
        Dim PriceChange As New CreatePriceChange

        Dim mocks As New MockRepository

        Dim StartDate As Date
        Dim Today As Date
        Dim DaysPrior As Integer
        Dim strEdat As String
        Dim evtchg As New DataTable("EVTCHG")
        Dim evtchgRow As DataRow = evtchg.NewRow
        Dim rowStock As IStock = mocks.Stub(Of IStock)()

        StartDate = CDate("1 apr 2010")
        Today = CDate("27 may 2010")
        DaysPrior = 7
        strEdat = ""

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim newTable As DataTable = evtchg.Clone

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("1 Apr 2010")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "000100"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(5.0)
        evtchgRow("IDEL") = False


        evtchg.Rows.Add(evtchgRow)

        evtchgRow = evtchg.NewRow

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("27 May 2010")
        evtchgRow("PRIO") = "20"
        evtchgRow("NUMB") = "000105"
        evtchgRow("EDAT") = CDate("29 July 2010")
        evtchgRow("PRIC") = CDec(7.5)
        evtchgRow("IDEL") = False

        evtchg.Rows.Add(evtchgRow)

        With rowStock
            .RetailPriceEventNo = "000100"
            .RetailPricePriority = "10"
            .NormalSellPrice = CDec(5)
            .SkuNumber = "100803"
        End With

        mocks.ReplayAll()

        ' act
        For Each row As DataRow In evtchg.Rows
            PriceChange.CheckAndApplyPriceChange(StartDate, DaysPrior, strEdat, row, rowStock, newTable, Today)
        Next

        ' assert
        Assert.AreEqual("20", newTable.Rows(0).Item("PRIO"))
    End Sub

    <TestMethod()> Public Sub CreatePriceChangeFactory_PromotionalPriceChangeEffectiveNextDayWithExistingBasePrice_Test2_PriceChangeEventNumberAsExpected()
        Dim PriceChange As New CreatePriceChange

        Dim mocks As New MockRepository

        Dim StartDate As Date
        Dim Today As Date
        Dim DaysPrior As Integer
        Dim strEdat As String
        Dim evtchg As New DataTable("EVTCHG")
        Dim evtchgRow As DataRow = evtchg.NewRow
        Dim rowStock As IStock = mocks.Stub(Of IStock)()

        StartDate = CDate("1 apr 2010")
        Today = CDate("27 may 2010")
        DaysPrior = 7
        strEdat = ""

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim newTable As DataTable = evtchg.Clone

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("1 Apr 2010")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "000100"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(5.0)
        evtchgRow("IDEL") = False


        evtchg.Rows.Add(evtchgRow)

        evtchgRow = evtchg.NewRow

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("27 May 2010")
        evtchgRow("PRIO") = "20"
        evtchgRow("NUMB") = "000105"
        evtchgRow("EDAT") = CDate("29 July 2010")
        evtchgRow("PRIC") = CDec(7.5)
        evtchgRow("IDEL") = False

        evtchg.Rows.Add(evtchgRow)

        With rowStock
            .RetailPriceEventNo = "000100"
            .RetailPricePriority = "10"
            .NormalSellPrice = CDec(5)
            .SkuNumber = "100803"
        End With

        mocks.ReplayAll()

        ' act
        For Each row As DataRow In evtchg.Rows
            PriceChange.CheckAndApplyPriceChange(StartDate, DaysPrior, strEdat, row, rowStock, newTable, Today)
        Next

        ' assert
        Assert.AreEqual("000105", newTable.Rows(0).Item("NUMB"))
    End Sub

    <TestMethod()> Public Sub CreatePriceChangeFactory_PromotionalPriceChangeEffectiveNextDayWithExistingBasePrice_Test2_PriceChangeEDATAsExpected()
        Dim PriceChange As New CreatePriceChange

        Dim mocks As New MockRepository

        Dim StartDate As Date
        Dim Today As Date
        Dim DaysPrior As Integer
        Dim strEdat As String
        Dim evtchg As New DataTable("EVTCHG")
        Dim evtchgRow As DataRow = evtchg.NewRow
        Dim rowStock As IStock = mocks.Stub(Of IStock)()

        StartDate = CDate("1 apr 2010")
        Today = CDate("27 may 2010")
        DaysPrior = 7
        strEdat = ""

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim newTable As DataTable = evtchg.Clone

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("1 Apr 2010")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "000100"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(5.0)
        evtchgRow("IDEL") = False


        evtchg.Rows.Add(evtchgRow)

        evtchgRow = evtchg.NewRow

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("27 May 2010")
        evtchgRow("PRIO") = "20"
        evtchgRow("NUMB") = "000105"
        evtchgRow("EDAT") = CDate("29 July 2010")
        evtchgRow("PRIC") = CDec(7.5)
        evtchgRow("IDEL") = False

        evtchg.Rows.Add(evtchgRow)

        With rowStock
            .RetailPriceEventNo = "000100"
            .RetailPricePriority = "10"
            .NormalSellPrice = CDec(5)
            .SkuNumber = "100803"
        End With

        mocks.ReplayAll()

        ' act
        For Each row As DataRow In evtchg.Rows
            PriceChange.CheckAndApplyPriceChange(StartDate, DaysPrior, strEdat, row, rowStock, newTable, Today)
        Next

        ' assert
        Assert.AreEqual(CDate("29 July 2010"), CDate(newTable.Rows(0).Item("EDAT")))
    End Sub

    <TestMethod()> Public Sub CreatePriceChangeFactory_PromotionalPriceChangeEffectiveNextDayWithExistingBasePrice_Test2_PriceChangePriceAsExpected()
        Dim PriceChange As New CreatePriceChange

        Dim mocks As New MockRepository

        Dim StartDate As Date
        Dim Today As Date
        Dim DaysPrior As Integer
        Dim strEdat As String
        Dim evtchg As New DataTable("EVTCHG")
        Dim evtchgRow As DataRow = evtchg.NewRow
        Dim rowStock As IStock = mocks.Stub(Of IStock)()

        StartDate = CDate("1 apr 2010")
        Today = CDate("27 May 2010")
        DaysPrior = 7
        strEdat = ""

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim newTable As DataTable = evtchg.Clone

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("1 Apr 2010")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "000100"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(5.0)
        evtchgRow("IDEL") = False
        evtchg.Rows.Add(evtchgRow)

        evtchgRow = evtchg.NewRow
        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("27 May 2010")
        evtchgRow("PRIO") = "20"
        evtchgRow("NUMB") = "000105"
        evtchgRow("EDAT") = CDate("29 July 2010")
        evtchgRow("PRIC") = CDec(7.5)
        evtchgRow("IDEL") = False
        evtchg.Rows.Add(evtchgRow)

        With rowStock
            .RetailPriceEventNo = "000100"
            .RetailPricePriority = "10"
            .NormalSellPrice = CDec(5)
            .SkuNumber = "100803"
        End With

        mocks.ReplayAll()

        ' act
        For Each row As DataRow In evtchg.Rows
            PriceChange.CheckAndApplyPriceChange(StartDate, DaysPrior, strEdat, row, rowStock, newTable, Today)
        Next

        ' assert
        Assert.AreEqual(CDec(7.5), newTable.Rows(0).Item("PRIC"))
    End Sub

    <TestMethod()> Public Sub CreatePriceChangeFactory_PromotionalPriceChangeEffectiveNextDayWithExistingBasePrice_Test2_PriceChangeIDELAsExpected()
        Dim PriceChange As New CreatePriceChange

        Dim mocks As New MockRepository

        Dim StartDate As Date
        Dim Today As Date
        Dim DaysPrior As Integer
        Dim strEdat As String
        Dim evtchg As New DataTable("EVTCHG")
        Dim evtchgRow As DataRow = evtchg.NewRow
        Dim rowStock As IStock = mocks.Stub(Of IStock)()

        StartDate = CDate("1 apr 2010")
        Today = CDate("27 may 2010")
        DaysPrior = 7
        strEdat = ""

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.String"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim newTable As DataTable = evtchg.Clone

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("1 Apr 2010")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "000100"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(5.0)
        evtchgRow("IDEL") = False

        evtchg.Rows.Add(evtchgRow)

        evtchgRow = evtchg.NewRow

        evtchgRow("SKUN") = "100803"
        evtchgRow("SDAT") = CDate("27 May 2010")
        evtchgRow("PRIO") = "20"
        evtchgRow("NUMB") = "000105"
        evtchgRow("EDAT") = CDate("29 July 2010")
        evtchgRow("PRIC") = CDec(7.5)
        evtchgRow("IDEL") = False

        evtchg.Rows.Add(evtchgRow)

        With rowStock
            .RetailPriceEventNo = "000100"
            .RetailPricePriority = "10"
            .NormalSellPrice = CDec(5)
            .SkuNumber = "100803"
        End With

        mocks.ReplayAll()

        ' act
        For Each row As DataRow In evtchg.Rows
            PriceChange.CheckAndApplyPriceChange(StartDate, DaysPrior, strEdat, row, rowStock, newTable, Today)
        Next

        ' assert
        Assert.AreEqual(False, newTable.Rows(0).Item("IDEL"))
    End Sub

End Class
