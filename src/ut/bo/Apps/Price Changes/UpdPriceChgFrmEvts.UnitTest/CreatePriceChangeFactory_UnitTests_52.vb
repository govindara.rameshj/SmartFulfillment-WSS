﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data
Imports System.Data.DataColumn
Imports UpdPriceChgFrmEvts
Imports Rhino.Mocks

<TestClass()> Public Class CreatePriceChangeFactory_UnitTests_52

    Private testContextInstance As TestContext

    Private Shared _useNewFunctionality As Boolean = True
    Private Shared _PriceChange As CreatePriceChange
    Private Shared _GetData As GetData
    Private Shared _StartDate As Date
    Private Shared _Today As Date
    Private Shared _DaysPrior As Integer
    Private Shared _strEdat As String
    Private Shared _evtchg As New DataTable("EVTCHG")
    Private Shared _evtchgRow As DataRow = _evtchg.NewRow
    Private Shared _mocks As New MockRepository
    Private Shared _rowStock As IStock = _mocks.Stub(Of IStock)()
    Private Shared _skun As String

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Start"

    <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        _evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        _evtchg.Columns.Add("SDAT", System.Type.GetType("System.DateTime"))
        _evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        _evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        _evtchg.Columns.Add("EDAT", System.Type.GetType("System.DateTime"))
        _evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        _evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))
    End Sub

    <TestInitialize()> Public Sub MyTestInitialize()

        _PriceChange = New CreatePriceChange

        _Today = CDate("01 July 2010")
        _DaysPrior = 7
        _strEdat = ""

        _evtchgRow("SKUN") = "100803"
        _evtchgRow("SDAT") = CDate("01 July 2010")
        _evtchgRow("PRIO") = "20"
        _evtchgRow("NUMB") = "000105"
        _evtchgRow("EDAT") = CDate("01 October 2010")
        _evtchgRow("PRIC") = CDec(5.0)
        _evtchgRow("IDEL") = False
        _evtchg.Rows.Add(_evtchgRow)

        _evtchgRow = _evtchg.NewRow
        _evtchgRow("SKUN") = "100803"
        _evtchgRow("SDAT") = CDate("01 September 2010")
        _evtchgRow("PRIO") = "10"
        _evtchgRow("NUMB") = "000100"
        _evtchgRow("EDAT") = DBNull.Value
        _evtchgRow("PRIC") = CDec(10.0)
        _evtchgRow("IDEL") = False
        _evtchg.Rows.Add(_evtchgRow)

        _GetData = New GetData
        _GetData.GetEffectiveEVTCHGRecordsForDateRange(_Today, 7, _evtchg)

    End Sub

    <TestCleanup()> Public Sub MyTestCleanup()

        _evtchg.Clear()

    End Sub

    Private Function GetStockMasterRow(ByVal SkuNumber As String, ByVal newTable As DataTable) As IStock
        Dim stock As IStock = New Stock

        Select Case SkuNumber

            Case "100803"
                With stock
                    .NormalSellPrice = CDec(10.0)
                    .RetailPriceEventNo = "000100"
                    .RetailPricePriority = "10"
                    .SkuNumber = "100803"
                    _strEdat = "01 October 2010"
                End With

        End Select

        'Need to check if the skunumber has changed since the last iteration.
        'If so we need to do a regression test and re-set working variables.
        'This is what would happen in the real code implementation.
        If SkuNumber <> _skun Then
            _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, SkuNumber, newTable, stock)
        End If

        _skun = SkuNumber

        Return stock

    End Function

#End Region

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Row 1"

    <TestMethod()> Public Sub SKUAAPCFlagTestWithOnHandStock_ReturnsPriceChanges_WithExpected_NumberOfRows()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            Dim rowStock As IStock = New Stock
            rowStock = GetStockMasterRow(row("SKUN").ToString, newTable)
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, rowStock, newTable, _Today)
        Next


        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)


        ' assert
        Assert.AreEqual(1, newTable.Rows.Count)

    End Sub


    <TestMethod()> Public Sub SKUAAPCFlagTestWithOnHandStock_ReturnsPriceChanges_WithExpected_skun()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            Dim rowStock As IStock = New Stock
            rowStock = GetStockMasterRow(row("SKUN").ToString, newTable)
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, rowStock, newTable, _Today)
        Next


        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)


        ' assert
        Assert.AreEqual("100803", newTable.Rows(0).Item("SKUN"))

    End Sub

    <TestMethod()> Public Sub SKUAAPCFlagTestWithOnHandStock_ReturnsPriceChanges_WithExpected_SDAT()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            Dim rowStock As IStock = New Stock
            rowStock = GetStockMasterRow(row("SKUN").ToString, newTable)
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, rowStock, newTable, _Today)
        Next


        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)


        ' assert
        Assert.AreEqual(CDate("02 July 2010"), CDate(newTable.Rows(0).Item("SDAT")))

    End Sub

    <TestMethod()> Public Sub SKUAAPCFlagTestWithOnHandStock_ReturnsPriceChanges_WithExpected_Priority()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            Dim rowStock As IStock = New Stock
            rowStock = GetStockMasterRow(row("SKUN").ToString, newTable)
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, rowStock, newTable, _Today)
        Next


        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)


        ' assert
        Assert.AreEqual("20", newTable.Rows(0).Item("PRIO"))

    End Sub

    <TestMethod()> Public Sub SKUAAPCFlagTestWithOnHandStock_ReturnsPriceChanges_WithExpected_NUMB()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            Dim rowStock As IStock = New Stock
            rowStock = GetStockMasterRow(row("SKUN").ToString, newTable)
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, rowStock, newTable, _Today)
        Next


        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)


        ' assert
        Assert.AreEqual("000105", newTable.Rows(0).Item("NUMB"))

    End Sub

    <TestMethod()> Public Sub SKUAAPCFlagTestWithOnHandStock_ReturnsPriceChanges_WithExpected_Price()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            Dim rowStock As IStock = New Stock
            rowStock = GetStockMasterRow(row("SKUN").ToString, newTable)
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, rowStock, newTable, _Today)
        Next


        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)


        ' assert
        Assert.AreEqual(CDec(5.0), CDec(newTable.Rows(0).Item("PRIC")))

    End Sub

    <TestMethod()> Public Sub SKUAAPCFlagTestWithOnHandStock_ReturnsPriceChanges_WithExpected_EDAT()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            Dim rowStock As IStock = New Stock
            rowStock = GetStockMasterRow(row("SKUN").ToString, newTable)
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, rowStock, newTable, _Today)
        Next


        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)


        ' assert
        Assert.AreEqual(CDate("01 October 2010"), CDate(newTable.Rows(0).Item("EDAT")))

    End Sub

    <TestMethod()> Public Sub SKUAAPCFlagTestWithOnHandStock_ReturnsPriceChanges_WithExpected_IDEL()

        Dim newTable As DataTable = _evtchg.Clone

        ' act
        For Each row As DataRow In _evtchg.Rows
            _StartDate = CDate(row("SDAT"))
            Dim rowStock As IStock = New Stock
            rowStock = GetStockMasterRow(row("SKUN").ToString, newTable)
            _PriceChange.CheckAndApplyPriceChange(_StartDate, _DaysPrior, _strEdat, row, rowStock, newTable, _Today)
        Next


        _PriceChange.RegressAndReset(_StartDate, _DaysPrior, _strEdat, _rowStock.SkuNumber, newTable, _rowStock)


        ' assert
        Assert.AreEqual(False, newTable.Rows(0).Item("IDEL"))

    End Sub

#End Region

End Class
