﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports System.Data
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports UpdPriceChgFrmEvts
Imports BOStock

#Region " Data Scenarios "
Friend Class PriceChangeTableHelper

    Friend Function GetEmptyPriceChangeDataTable() As DataTable
        Dim dt As New DataTable("ProposedPriceChange")

        dt.Columns.Add("SKUN", System.Type.GetType("System.String"))
        dt.Columns.Add("PDAT", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        dt.Columns.Add("PSTA", System.Type.GetType("System.String"))
        dt.Columns.Add("SHEL", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("AUDT", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("AUAP", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("MARK", System.Type.GetType("System.Decimal"))
        dt.Columns.Add("MCOM", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("LABS", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("LABM", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("LABL", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("EVNT", System.Type.GetType("System.String"))
        dt.Columns.Add("PRIO", System.Type.GetType("System.String"))
        dt.Columns.Add("EEID", System.Type.GetType("System.String"))
        dt.Columns.Add("MEID", System.Type.GetType("System.String"))

        Return dt

    End Function

    Friend Function GetPriceChangeDataTableContainingTwoIdenticalRows(ByVal dt As DataTable) As DataTable
        Dim dr As DataRow = dt.NewRow

        dr("SKUN") = "212530"
        dr("PDAT") = CDate("22 Jun 2012")
        dr("PRIC") = CDec(89.0)
        dr("PSTA") = "U"
        dr("SHEL") = 0
        dr("AUDT") = CDate("22 Jun 2012")
        dr("AUAP") = DBNull.Value
        dr("MARK") = DBNull.Value
        dr("MCOM") = 0
        dr("LABS") = 1
        dr("LABM") = 1
        dr("LABL") = 0
        dr("EVNT") = "059458"
        dr("PRIO") = "10"
        dr("EEID") = DBNull.Value
        dr("MEID") = DBNull.Value
        dt.Rows.Add(dr)

        dr = dt.NewRow()

        dr("SKUN") = "212530"
        dr("PDAT") = CDate("22 Jun 2012")
        dr("PRIC") = CDec(89.0)
        dr("PSTA") = "U"
        dr("SHEL") = 0
        dr("AUDT") = CDate("22 Jun 2012")
        dr("AUAP") = DBNull.Value
        dr("MARK") = DBNull.Value
        dr("MCOM") = 0
        dr("LABS") = 1
        dr("LABM") = 1
        dr("LABL") = 0
        dr("EVNT") = "059458"
        dr("PRIO") = "10"
        dr("EEID") = DBNull.Value
        dr("MEID") = DBNull.Value

        dt.Rows.Add(dr)

        Return dt

    End Function

    Friend Function SetStockItemTrue() As BOStock.cStock
        Dim stk As New BOStock.cStock

        stk.NonStockItem.Value = True
        stk.AutoApplyPriceChgs.Value = True
        stk.ItemObsolete.Value = True
        stk.StockOnHand.Value = 0
        stk.MarkDownQuantity.Value = 0

        Return stk

    End Function

    Friend Function SetStockItemFalse() As BOStock.cStock
        Dim stk As New BOStock.cStock

        stk.NonStockItem.Value = False
        stk.AutoApplyPriceChgs.Value = False
        stk.ItemObsolete.Value = False
        stk.StockOnHand.Value = 120
        stk.MarkDownQuantity.Value = 3

        Return stk

    End Function

    Friend Function GetEVTCHGRecord() As DataRow
        Dim evtchg As New DataTable("EVTCHG")

        evtchg.Columns.Add("SKUN", System.Type.GetType("System.String"))
        evtchg.Columns.Add("SDAT", System.Type.GetType("System.DateTime"))
        evtchg.Columns.Add("PRIO", System.Type.GetType("System.String"))
        evtchg.Columns.Add("NUMB", System.Type.GetType("System.String"))
        evtchg.Columns.Add("EDAT", System.Type.GetType("System.DateTime"))
        evtchg.Columns.Add("PRIC", System.Type.GetType("System.Decimal"))
        evtchg.Columns.Add("IDEL", System.Type.GetType("System.Boolean"))

        Dim evtchgRow As DataRow = evtchg.NewRow
        evtchgRow("SKUN") = "212530"
        evtchgRow("SDAT") = CDate("22 June 2012")
        evtchgRow("PRIO") = "10"
        evtchgRow("NUMB") = "059458"
        evtchgRow("EDAT") = DBNull.Value
        evtchgRow("PRIC") = CDec(89.0)
        evtchgRow("IDEL") = False
        evtchg.Rows.Add(evtchgRow)

        Return evtchgRow
    End Function
End Class

#End Region

Friend Class SetLabel_Scenario

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend Overrides Function LabelPrintedToBeSet(ByVal stockItem As cStock) As Boolean
        Return True
    End Function

End Class

Friend Class InsertPriceChange_Scenario

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend _InsertWasCalled As Boolean = False

    Friend Overrides Sub InsertPriceChange(ByVal drEvtcgh As DataRow, ByVal DaysAfter As Integer, ByVal SavePriceChange As PriceChangeDataRepository)
        _InsertWasCalled = True
    End Sub

End Class

Friend Class UpdatePriceChangeResetLabel_Scenario

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend _UpdateWasCalled As Boolean = False

    Friend Overrides Function IsPriceTheSame(ByVal EvtcghPrice As Object, ByVal PrccghPrice As Object) As Boolean
        Return False
    End Function

    Friend Overrides Sub UpdatePriceChangeRecordResetLabelWithOriginalDate(ByVal drEvtchg As DataRow, ByVal daysAfter As Integer, ByVal SavePriceChange As PriceChangeDataRepository, ByVal OriginalEventNumber As String, ByVal OriginalPric As Decimal, ByVal OriginalStatus As String)
        _UpdateWasCalled = True
    End Sub


End Class

Friend Class UpdatePriceChangeNoResetLabel_Scenario

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend _UpdateWasCalled As Boolean = False

    Friend Overrides Function IsPriceTheSame(ByVal EvtcghPrice As Object, ByVal PrccghPrice As Object) As Boolean
        Return True
    End Function

    Friend Overrides Sub UpdatePriceChangeRecordWithOriginalDate(ByVal drEvtchg As DataRow, ByVal daysAfter As Integer, ByVal SavePriceChange As PriceChangeDataRepository, ByVal OriginalEventNumber As String, ByVal OriginalPric As Decimal, ByVal OriginalStatus As String)
        _UpdateWasCalled = True
    End Sub
End Class


Friend Class SavingPriceChange_SkuAndDateMatchScenario

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Dim _UpdateCalled As Boolean = False
    Public Property UpdateCalled() As Boolean
        Get
            Return _UpdateCalled
        End Get
        Set(ByVal value As Boolean)
            _UpdateCalled = value
        End Set
    End Property

    Friend Overrides Function SkuDateAndEventMatch(ByVal dt As DataTable, ByVal SKU As String, ByVal EventNo As String, ByVal PriceChangeDate As Date) As Boolean
        Return True
    End Function

    Friend Overrides Function SkuAndDateMatchFound(ByVal dt As DataTable, ByVal SKU As String, ByVal PriceChangeDate As Date) As Boolean
        Return True
    End Function

    Friend Overrides Function SkuUnAppliedAndLabelPrinted(ByVal dt As DataTable, ByVal SKU As String, ByVal PriceChangeDate As Date, ByVal EventNo As String) As Boolean
        Return True
    End Function

    Friend Overrides Sub UpdatePriceChange(ByVal drEvtcgh As DataRow, ByVal DaysAfter As Integer)
        _UpdateCalled = True
    End Sub
End Class

Friend Class SavingPriceChange_MatchesReturnTrueScenario

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend Overrides Function SkuDateAndEventMatch(ByVal dt As DataTable, ByVal SKU As String, ByVal EventNo As String, ByVal PriceChangeDate As Date) As Boolean
        Return True
    End Function

    Friend Overrides Function SkuAndDateMatchFound(ByVal dt As DataTable, ByVal SKU As String, ByVal PriceChangeDate As Date) As Boolean
        Return True
    End Function

    Friend Overrides Function SkuUnAppliedAndLabelPrinted(ByVal dt As DataTable, ByVal SKU As String, ByVal PriceChangeDate As Date, ByVal EventNo As String) As Boolean
        Return True
    End Function

    Friend Overrides Function LabelPrintedToBeSet(ByVal stockItem As cStock) As Boolean
        Return True
    End Function
End Class

Friend Class SavingPriceChange_MatchesReturnFalseScenario

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend Overrides Function SkuDateAndEventMatch(ByVal dt As DataTable, ByVal SKU As String, ByVal EventNo As String, ByVal PriceChangeDate As Date) As Boolean
        Return False
    End Function

    Friend Overrides Function SkuAndDateMatchFound(ByVal dt As DataTable, ByVal SKU As String, ByVal PriceChangeDate As Date) As Boolean
        Return False
    End Function

    Friend Overrides Function SkuUnAppliedAndLabelPrinted(ByVal dt As DataTable, ByVal SKU As String, ByVal PriceChangeDate As Date, ByVal EventNo As String) As Boolean
        Return False
    End Function

    Friend Overrides Function LabelPrintedToBeSet(ByVal stockItem As cStock) As Boolean
        Return False
    End Function

End Class

Friend Class SavingPriceChange_UpdatePriceChangeCalled_Scenario
    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend _UpdateWasCalled As Boolean = False

    Friend Overrides Sub UpdatePriceChange(ByVal drEvtchg As DataRow, ByVal daysAfter As Integer)
        _UpdateWasCalled = True
    End Sub

    Friend Overrides Function MatchingPriceChangeFound(ByVal dt As DataTable, ByVal Sku As String, ByVal PriceChangeDate As Date, ByVal EventNo As String, ByVal Price As Decimal) As Boolean
        Return True
    End Function
End Class

Friend Class SavingPriceChange_InsertPriceChangeCalled_Scenario

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend _InsertWasCalled As Boolean = False

    Friend Overrides Sub InsertPriceChange(ByVal drEvtcgh As DataRow, ByVal DaysAfter As Integer)
        _InsertWasCalled = True
    End Sub

    Friend Overrides Function MatchingPriceChangeFound(ByVal dt As DataTable, ByVal Sku As String, ByVal PriceChangeDate As Date, ByVal EventNo As String, ByVal Price As Decimal) As Boolean
        Return False
    End Function

End Class


Friend Class UpdatePriceChange_PriceSame_Scenarios

    Inherits UpdPriceChgFrmEvts.AddPriceChangeRecord

    Friend _UpdateNoResetWasCalled As Boolean = False
    Friend _UpdateLabelResetWasCalled As Boolean = False

    Friend Overrides Function IsPriceTheSame(ByVal EvtcghPrice As Object, ByVal PrccghPrice As Object) As Boolean
        Return True
    End Function

    Friend Overrides Sub UpdatePriceChange(ByVal drEvtchg As DataRow, ByVal daysAfter As Integer)
        If IsPriceTheSame(8, 8) Then
            _UpdateNoResetWasCalled = True
        Else
            _UpdateLabelResetWasCalled = True
        End If
    End Sub

End Class

<TestClass()> Public Class AddPriceChangeRecord_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region " Test Set Up of Data Scenarios "
    <TestMethod()> Public Sub Self_GetEmptyPriceChangeDataTable_ReturnsZeroRows()
        Dim hlp As New PriceChangeTableHelper
        Dim dt As DataTable = hlp.GetEmptyPriceChangeDataTable()

        Assert.AreEqual(0, dt.Rows.Count)
    End Sub

#End Region


#Region "Scenarios For Saving Price Change Record"
#End Region

    <TestMethod()> Public Sub IsPriceTheSame_MatchingRealCodeTest_ReturnsTrue()
        Dim o As New AddPriceChangeRecord
        Assert.IsTrue(o.IsPriceTheSame(89, 89))
    End Sub

    <TestMethod()> Public Sub IsPriceTheSame_MatchingRealCodeTest_ReturnsFalse()
        Dim o As New AddPriceChangeRecord
        Assert.IsFalse(o.IsPriceTheSame(89, 90))
    End Sub

    <TestMethod()> Public Sub LabelPrintedToBeSet_RealCodeTest_ReturnsTrue()
        Dim prc As New PriceChangeTableHelper
        Dim stockItem As New BOStock.cStock

        stockItem = prc.SetStockItemTrue
        Dim o As New AddPriceChangeRecord
        Assert.IsTrue(o.LabelPrintedToBeSet(stockItem))
    End Sub

    <TestMethod()> Public Sub LabelPrintedToBeSet_RealCodeTest_Returnsfalse()
        Dim prc As New PriceChangeTableHelper
        Dim stockItem As New BOStock.cStock

        stockItem = prc.SetStockItemFalse
        Dim o As New AddPriceChangeRecord
        Assert.IsFalse(o.LabelPrintedToBeSet(stockItem))
    End Sub

    <TestMethod()> Public Sub SetLabelAsPrinted_IsLargeLabel_ReturnsFalse()
        Dim o As New AddPriceChangeRecord
        o.SetLabelAsPrinted()
        Assert.IsFalse(o.IsLargeLabelNeeded)
    End Sub

    <TestMethod()> Public Sub SetLabelAsPrinted_IsMediumLabel_ReturnsFalse()
        Dim o As New AddPriceChangeRecord
        o.SetLabelAsPrinted()
        Assert.IsFalse(o.IsMediumLabelNeeded)
    End Sub

    <TestMethod()> Public Sub SetLabelAsPrinted_IsSmallLabel_ReturnsFalse()
        Dim o As New AddPriceChangeRecord
        o.SetLabelAsPrinted()
        Assert.IsFalse(o.IsSmallLabelNeeded)
    End Sub

    <TestMethod()> Public Sub SetLabelAsPrinted_IsShelfLAbel_ReturnsTrue()
        Dim o As New AddPriceChangeRecord
        o.SetLabelAsPrinted()
        Assert.IsTrue(o.IsShelfLabelNeeded)
    End Sub

    <TestMethod()> Public Sub LabelPrintedToBeSet_ReturnsTrue()
        Dim prc As New PriceChangeTableHelper
        Dim stockItem As New BOStock.cStock
        stockItem = prc.SetStockItemTrue
        Dim o As New AddPriceChangeRecord

        Assert.IsTrue(o.LabelPrintedToBeSet(stockItem))
    End Sub

    <TestMethod()> Public Sub LabelPrintedToBeSet_ReturnsFalse()
        Dim prc As New PriceChangeTableHelper
        Dim stockItem As New BOStock.cStock
        stockItem = prc.SetStockItemFalse
        Dim o As New AddPriceChangeRecord

        Assert.IsFalse(o.LabelPrintedToBeSet(stockItem))
    End Sub

    <TestMethod()> Public Sub SetLabel_WasCalled()

        Dim o As New SetLabel_Scenario
        Dim prc As New PriceChangeTableHelper
        Dim stockItem As New BOStock.cStock

        stockItem = prc.SetStockItemFalse
        o.SetLabelFromStockRecord(stockItem)
        Assert.IsTrue(o.IsShelfLabelNeeded)
    End Sub

End Class

