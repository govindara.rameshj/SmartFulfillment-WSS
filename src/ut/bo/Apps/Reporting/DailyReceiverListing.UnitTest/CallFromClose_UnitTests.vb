﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports DailyReceiverListing

Friend Class TestGetCallFromClose
    Inherits GetCallFromCloseFactory

    Friend _Enabled As Boolean = False

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Return _Enabled
    End Function

End Class

<TestClass()> Public Class CallFromClose_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region
    <TestMethod()> Public Sub CallFromCloseFactory_RequirementSwitch_ReturnsNoDRLFilter()
        Dim Repository As ICallFromClose
        Dim Factory As New TestGetCallFromClose
        Factory._Enabled = False
        Repository = Factory.GetImplementation
        Assert.AreEqual("DailyReceiverListing.NoDRLFilter", Repository.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub CallFromCloseFactory_RequirementsSwitch_ReturnsReceiptsOnly()
        Dim Repository As ICallFromClose
        Dim Factory As New TestGetCallFromClose
        Factory._Enabled = True
        Repository = Factory.GetImplementation
        Assert.AreEqual("DailyReceiverListing.ReceiptsOnly", Repository.GetType.FullName)
    End Sub

    <TestMethod()> Public Sub CallFromCloseFactory_ReceiptFilter_ReturnsExpectedSQL()
        Dim Repository As ICallFromClose
        Dim Factory As New TestGetCallFromClose
        Factory._Enabled = True
        Repository = Factory.GetImplementation
        Assert.AreEqual("AND ((DS.[TYPE] = 0 and DS.[0BBC] = 0) OR (DS.[TYPE] in (1, 2) and init <> 'Auto'))", Repository.ReceiptFilterSQL)
    End Sub

    <TestMethod()> Public Sub CallFromCloseFactory_ReceiptFilter_ReturnsEmptyString()
        Dim Repository As ICallFromClose
        Dim Factory As New TestGetCallFromClose
        Factory._Enabled = False
        Repository = Factory.GetImplementation
        Assert.AreEqual(String.Empty, Repository.ReceiptFilterSQL)
    End Sub

End Class
