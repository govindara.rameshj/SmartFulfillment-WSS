﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports RefundsList
Imports NSubstitute
Imports TpWickes.Library

<TestClass()> Public Class RefundsList_UnitTests

    Private testContextInstance As TestContext
    Private refundList As IRefundList
    Private testFactoryInstance As New TestFactory



    <TestMethod()> Public Sub RefundListNew_ReportColumnDescriptionIndex_Returns8()
        refundList = (New RefundListFactory).ImplementationA
        Assert.AreEqual(8, refundList.ReportColumnDescriptionIndex)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_ReportColumnDescriptionIndex_Returns6()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(6, refundList.ReportColumnDescriptionIndex)
    End Sub
    <TestMethod()> Public Sub RefundListNew_ReportColumnExtpIndex_Returns12()
        refundList = (New RefundListFactory).ImplementationA
        Assert.AreEqual(12, refundList.ReportColumnExtpIndex)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_ReportColumnExtpIndex_Returns11()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(11, refundList.ReportColumnExtpIndex)
    End Sub
    <TestMethod()> Public Sub RefundListNew_ReportColumnInonIndex_Returns9()
        refundList = (New RefundListFactory).ImplementationA
        Assert.AreEqual(9, refundList.ReportColumnInonIndex)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_ReportColumnInonIndex_Returns7()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(7, refundList.ReportColumnInonIndex)
    End Sub
    <TestMethod()> Public Sub RefundListNew_ReportColumnPriceIndex_Returns10()
        refundList = (New RefundListFactory).ImplementationA
        Assert.AreEqual(10, refundList.ReportColumnPriceIndex)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_ReportColumnPriceIndex_Returns9()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(9, refundList.ReportColumnPriceIndex)
    End Sub
    <TestMethod()> Public Sub RefundListNew_ReportColumnQuantityIndex_Returns11()
        refundList = (New RefundListFactory).ImplementationA
        Assert.AreEqual(11, refundList.ReportColumnQuantityIndex)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_ReportColumnQuantityIndex_Returns10()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(10, refundList.ReportColumnQuantityIndex)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_ReportColumnSaltIndex_Returns8()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(8, refundList.ReportColumnSaltIndex)
    End Sub
    <TestMethod()> Public Sub RefundListNew_ReportColumnSaltIndex_Returns14()
        refundList = (New RefundListFactory).ImplementationA
        Assert.AreEqual(14, refundList.ReportColumnTenderAmountIndex)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_ReportColumnSaltIndex_Returns13()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(13, refundList.ReportColumnTenderAmountIndex)
    End Sub
    <TestMethod()> Public Sub RefundListNew_ReportColumnTenderIndex_Returns13()
        refundList = (New RefundListFactory).ImplementationA
        Assert.AreEqual(13, refundList.ReportColumnTenderDescriptionIndex)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_ReportColumnSaltIndex_Returns12()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(12, refundList.ReportColumnTenderDescriptionIndex)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnCount_Returns16()
        refundList = (New RefundListFactory).ImplementationA
        Assert.AreEqual(16, refundList.SetReportColumnCount)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnCount_Returns14()
        refundList = (New RefundListFactory).ImplementationB
        Assert.AreEqual(14, refundList.SetReportColumnCount)
    End Sub
    <TestMethod()> Public Sub RequirementSwitch_SetTrue_ReturnsNewImplementation()
        testFactoryInstance.SetRequirement = True
        refundList = testFactoryInstance.GetImplementation
        Assert.AreEqual("RefundsList.RefundList", refundList.GetType.FullName)
    End Sub
    <TestMethod()> Public Sub RequirementSwitch_SetFalse_ReturnsOldImplementation()
        testFactoryInstance.SetRequirement = False
        refundList = testFactoryInstance.GetImplementation
        Assert.AreEqual("RefundsList.RefundListOld", refundList.GetType.FullName)
    End Sub

#Region "Acceptance Tests"
    <TestMethod()> Public Sub RefundListNew_SwitchIsActive_ReturnsWithColumn3AsTime()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        testFactoryInstance.SetRequirement = True
        refundList = testFactoryInstance.GetImplementation
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("TIME", Sheet.Columns(4).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SwitchIsNotActive_ReturnsWithoutColumn3AsTime()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        testFactoryInstance.SetRequirement = False
        refundList = testFactoryInstance.GetImplementation
        refundList.SetReportColumnNames(Sheet)
        Assert.AreNotEqual("TIME", Sheet.Columns(3).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SwitchIsActive_ReturnsWithColumn15AsRefundType()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        testFactoryInstance.SetRequirement = True
        refundList = testFactoryInstance.GetImplementation
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("REFUND TYPE", Sheet.Columns(15).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SwitchIsNotActive_ReturnsWithoutColumn15AsRefundType()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        testFactoryInstance.SetRequirement = False
        refundList = testFactoryInstance.GetImplementation
        refundList.SetReportColumnNames(Sheet)
        Assert.AreNotEqual("REFUND TYPE", Sheet.Columns(15).Label)
    End Sub
#End Region

#Region "Sheet Tests"

    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column4AsTime()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("TIME", Sheet.Columns(4).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column5AsRefundCashier()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("REFUND CASHIER", Sheet.Columns(5).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column6AsSupv()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("SUPV", Sheet.Columns(6).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column7AsSkuNo()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("SKU NO", Sheet.Columns(7).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column8AsDescription()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("DESCRIPTION", Sheet.Columns(8).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column9AsNonStock()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("NON STOCK", Sheet.Columns(9).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column10AsPrice()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("PRICE", Sheet.Columns(10).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column11AsQuantity()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("QUAN", Sheet.Columns(11).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column12AsValue()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("VALUE", Sheet.Columns(12).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column13AsTender()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("TENDER", Sheet.Columns(13).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column14AsAmount()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("AMOUNT", Sheet.Columns(14).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column15AsRefundType()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationA
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("REFUND TYPE", Sheet.Columns(15).Label)
    End Sub

    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column3AsRefundCashier()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("REFUND CASHIER", Sheet.Columns(3).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column4AsSupv()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("SUPV", Sheet.Columns(4).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column5AsSkuNo()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("SKU NO", Sheet.Columns(5).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column6AsDescription()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("DESCRIPTION", Sheet.Columns(6).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column7AsNonStock()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("NON STOCK", Sheet.Columns(7).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column8AsStockType()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("STOCK TYPE", Sheet.Columns(8).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column9AsPrice()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("PRICE", Sheet.Columns(9).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column10AsQuantity()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("QUAN", Sheet.Columns(10).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column11AsValue()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("VALUE", Sheet.Columns(11).Label)
    End Sub
    <TestMethod()> Public Sub RefundListExisting_SetReportColumnNames_ReturnsPopulatedSheet_Column12AsTender()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("TENDER", Sheet.Columns(12).Label)
    End Sub
    <TestMethod()> Public Sub RefundListNew_SetReportColumnNames_ReturnsPopulatedSheet_Column13AsAmount()
        Dim Sheet As New FarPoint.Win.Spread.SheetView
        refundList = (New RefundListFactory).ImplementationB
        refundList.SetReportColumnNames(Sheet)
        Assert.AreEqual("AMOUNT", Sheet.Columns(13).Label)
    End Sub

#End Region

#Region "Test Class"

    Public Class TestFactory
        Inherits RefundListFactory

        Private _requirement As Boolean

        Public Property SetRequirement() As Boolean
            Get
                Return _requirement
            End Get
            Set(ByVal value As Boolean)
                _requirement = value
            End Set
        End Property

        Public Overrides Function ImplementationA_IsActive() As Boolean
            If SetRequirement Then
                Return True
            Else
                Return False
            End If

        End Function
    End Class
#End Region


End Class
