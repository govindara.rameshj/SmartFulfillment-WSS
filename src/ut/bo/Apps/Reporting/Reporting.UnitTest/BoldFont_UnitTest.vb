﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Drawing
Imports Reporting

Friend Class TestReportControlFactory
    Inherits ReportControlFactory

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Return True
    End Function

End Class
<TestClass()> Public Class BoldFont_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    'Does not work. Font is always bold
    <Ignore()>
    <TestMethod()> Public Sub ReportControl_SetBoldAtrribute_ReturnsFontBoldSetFalse()
        Dim x As IReportControl = (New TestReportControlFactory).GetImplementation()
        Dim myFont As New Font("Arial", 8.25)
        Dim EmboldenThisValue As Object = False

        Assert.IsFalse(x.SetBoldAttribute("", myFont, EmboldenThisValue).Bold)
    End Sub

    <TestMethod()> Public Sub ReportControl_SetBoldAtrribute_ReturnsFontBoldSetTrue()
        Dim x As IReportControl = (New TestReportControlFactory).GetImplementation
        Dim myFont As New Font("Arial", 8.25)
        Dim EmboldenThisValue As Object = True

        Assert.IsTrue(x.SetBoldAttribute("", myFont, EmboldenThisValue).Bold)
    End Sub

End Class
