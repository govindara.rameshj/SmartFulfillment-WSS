﻿Imports Cts.Oasys.Core.System.MenuOption

<TestClass()> Public Class ReportControlUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestReportControlHyperLink
        Inherits ReportControlHyperLink

        Friend Overrides Function GetMenu(ByVal MenuID As Integer) As Cts.Oasys.Core.System.MenuOption.Item

            Return New Item()

        End Function

    End Class

#End Region

#Region "Unit Test - Requirement Test"

    <TestMethod()> Public Sub SafeCheckReportUIFactory_RequirementFalse_ReturnReportControlHyperLinkExisting()

        Dim Report As IReportControlUI

        ArrangeRequirementSwitchDependency(False)

        Report = (New ReportControlUIFactory).GetImplementation

        Assert.AreEqual("Reporting.ReportControlHyperLinkExisting", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub SafeCheckReportUIFactory_RequirementTrue_ReturnReportControlHyperLink()

        Dim Report As IReportControlUI

        ArrangeRequirementSwitchDependency(True)

        Report = (New ReportControlUIFactory).GetImplementation

        Assert.AreEqual("Reporting.ReportControlHyperLink", Report.GetType.FullName)

    End Sub

#End Region

#Region "ReportControlHyperLink"

    <TestMethod()> Public Sub ReportControlHyperLink_GetSingleMenuItem_ProvideMenuID_ReturnMenuItem()

        Dim Report As New TestReportControlHyperLink

        Assert.IsNotNull(Report.GetSingleGetMenuItem(123))

    End Sub

#End Region

#Region "Private Test Functions"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim mock As New MockRepository
        Dim requirement As IRequirementRepository

        requirement = mock.Stub(Of IRequirementRepository)()
        SetupResult.On(requirement).Call(requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(requirement)
        mock.ReplayAll()

    End Sub

#End Region

End Class