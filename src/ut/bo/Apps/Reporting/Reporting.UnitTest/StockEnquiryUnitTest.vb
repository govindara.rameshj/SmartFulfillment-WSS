﻿<TestClass()> Public Class StockEnquiryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Requirement Switch: IStockEnquiryUI"

    <TestMethod()> Public Sub RequirementCheckFalse_ReturnExclusionFiltersActive()

        Dim Report As IStockEnquiryUI

        ArrangeRequirementSwitchDependency(False)

        Report = (New StockEnquiryUIFactory).GetImplementation

        Assert.AreEqual("Stock.Enquiry.ExclusionFiltersActive", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementCheckTrue_ReturnExclusionFiltersInactive()

        Dim Report As IStockEnquiryUI

        ArrangeRequirementSwitchDependency(True)

        Report = (New StockEnquiryUIFactory).GetImplementation

        Assert.AreEqual("Stock.Enquiry.ExclusionFiltersInactive", Report.GetType.FullName)

    End Sub

#End Region

#Region "Requirement Switch: IStockEnquirySortUI"

    <TestMethod()> Public Sub RequirementCheckFalse_ReturnCategorySort()

        Dim Report As IStockEnquirySortUI

        ArrangeRequirementSwitchDependency(False)

        Report = (New StockEnquirySortUIFactory).GetImplementation

        Assert.AreEqual("Stock.Enquiry.CategorySort", Report.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementCheckTrue_ReturnNaturalSort()

        Dim Report As IStockEnquirySortUI

        ArrangeRequirementSwitchDependency(True)

        Report = (New StockEnquirySortUIFactory).GetImplementation

        Assert.AreEqual("Stock.Enquiry.NaturalSort", Report.GetType.FullName)

    End Sub

#End Region

#Region "Class: ExclusionFiltersActive"

    <TestMethod()> Public Sub ExclusionFiltersActive_ExcludeFilterNonStockDefaultCheckedState_ReturnTrue()

        Dim Filter As New ExclusionFiltersActive

        Assert.IsTrue(Filter.ExcludeFilterNonStockDefaultCheckedState)

    End Sub

    <TestMethod()> Public Sub ExclusionFiltersActive_ExcludeFilterObsoleteDeleteDefaultCheckedState_ReturnTrue()

        Dim Filter As New ExclusionFiltersActive

        Assert.IsTrue(Filter.ExcludeFilterObsoleteDeleteDefaultCheckedState)

    End Sub

#End Region

#Region "Class: ExclusionFiltersInactive"

    <TestMethod()> Public Sub ExclusionFiltersInactive_ExcludeFilterNonStockDefaultCheckedState_ReturnFalse()

        Dim Filter As New ExclusionFiltersInactive

        Assert.IsFalse(Filter.ExcludeFilterNonStockDefaultCheckedState)

    End Sub

    <TestMethod()> Public Sub ExclusionFiltersInactive_ExcludeFilterObsoleteDeleteDefaultCheckedState_ReturnFalse()

        Dim Filter As New ExclusionFiltersInactive

        Assert.IsFalse(Filter.ExcludeFilterObsoleteDeleteDefaultCheckedState)

    End Sub

#End Region

#Region "Class: CategorySort"

    'Does not work, there is no Column "Strength" in the new View
    <Ignore()>
    <TestMethod()> Public Sub NaturalSort_GridControlReferenceSet()

        Dim Sort As New CategorySort

        Sort.ConfigureSort(New GridView)

        Assert.IsNotNull(Sort._View)

    End Sub

#End Region

#Region "Class: NaturalSort"

    <TestMethod()> Public Sub NaturalSort_GridControlReferenceNotSet()

        Dim Sort As New NaturalSort

        Sort.ConfigureSort(New GridView)

        Assert.IsNull(Sort._View)

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class