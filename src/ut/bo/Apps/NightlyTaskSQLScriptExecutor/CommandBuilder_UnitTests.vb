﻿<TestClass()> Public Class CommandBuilder_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> _
    Public Sub Application_SetProperties_GetPropertyPasses()
        Dim builder As New TestCommandBuilder_WithIntegrationDependanciesRemoved

        builder.Server = "SRV8099"
        Assert.AreEqual("SRV8099", builder.Server, "Server")

        builder.Database = "Oasys"
        Assert.AreEqual("Oasys", builder.Database, "Database")

        builder.ScriptInputFile = "c:\temp\script.sql"
        Assert.AreEqual("c:\temp\script.sql", builder.ScriptInputFile, "Input Script File")
    End Sub

    <TestMethod()> _
    Public Sub Application_DeriveOutputFileNameFromInputFileName_FormedCorrectly()
        Dim builder As New TestCommandBuilder_WithIntegrationDependanciesRemoved

        builder.ScriptInputFile = "c:\temp\script.sql"
        builder._ScriptOutputRepository = New ScriptOutputRepository_UnitTests.ScriptOutputRepository_SpecificTime_FirstMay2012TwoMinutesToMidnight

        Assert.AreEqual("c:\temp\log\20120501235800 script.log".ToUpper, builder.ScriptOutputFile.ToUpper)
    End Sub

    <TestMethod()> _
    Public Sub Application_GenerateSQLCMDString_FormedCorrectly()
        Dim builder As New TestCommandBuilder_WithIntegrationDependanciesRemoved

        builder.Server = "SRV8099"
        builder.Database = "Oasys"
        builder.ScriptInputFile = "c:\temp\script.sql"
        builder._ScriptOutputRepository = New ScriptOutputRepository_UnitTests.ScriptOutputRepository_SpecificTime_FirstMay2012TwoMinutesToMidnight

        Assert.AreEqual("sqlcmd.exe -S SRV8099 -d Oasys -E -i ""c:\temp\script.sql"" -o ""c:\temp\log\20120501235800 script.log""", builder.CommandToExecute)
    End Sub

#Region "Test Scenario classes"

    Private Class TestCommandBuilder_WithIntegrationDependanciesRemoved
        Inherits CommandBuilder

        Friend Overrides Function GetConnectionString() As String

            Return "Data Source=(local);Initial Catalog=Oasys; User ID=sa; Password=m1thr4nd1r"
        End Function
    End Class
#End Region
End Class
