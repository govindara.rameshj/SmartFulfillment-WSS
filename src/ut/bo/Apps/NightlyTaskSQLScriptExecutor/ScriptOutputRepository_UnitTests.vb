﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class ScriptOutputRepository_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub ScriptOutputRepository_ScriptOutputFileNameWithSpecificDate_ExpectedValue()
        ' TODO: Add test logic here
        Dim rep As New ScriptOutputRepository_SpecificTime_FirstMay2012TwoMinutesToMidnight

        Assert.AreEqual("c:\temp\log\20120501235800 script.log".ToUpper, rep.ScriptOutputFileName("c:\temp\script.sql").ToUpper)
    End Sub

    <TestMethod()> Public Sub ScriptOutputRepository_TestFileNamePrefixWithSpecificDate_ExpectedValue()
        ' TODO: Add test logic here
        Dim rep As New ScriptOutputRepository_SpecificTime_FirstMay2012TwoMinutesToMidnight

        Assert.AreEqual("20120501235800 ", rep.FileNamePrefix())
    End Sub

    Friend Class ScriptOutputRepository_SpecificTime_FirstMay2012TwoMinutesToMidnight
        Inherits ScriptOutputRepository

        Friend Overrides Function CurrentDateTime() As DateTime
            Return New DateTime(2012, 5, 1, 23, 58, 0)
        End Function
    End Class

End Class
