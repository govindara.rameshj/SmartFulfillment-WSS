﻿<TestClass()> _
Public Class SQLScriptExecutor_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ScriptExecuted_SuccessScenario()
        Dim Executor As New TestSQLScriptExecutor_GetFileContent_SuccessScenario

        ArrangeScriptExecutor(CType(Executor, SQLScriptExecutor))
        Assert.IsTrue(Executor.ScriptExecutedSuccessfully)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ScriptExecuted_FailureScenario()
        Dim Executor As New TestSQLScriptExecutor_GetFileContent_FailureScenario

        ArrangeScriptExecutor(CType(Executor, SQLScriptExecutor))
        Assert.IsFalse(Executor.ScriptExecutedSuccessfully)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_PassCommandParameters_Test()
        Dim Executor As New SQLScriptExecutor
        Dim expectedBuilder As New CommandBuilder

        expectedBuilder.Server = "SRV8099"
        expectedBuilder.Database = "Oasys"
        expectedBuilder.ScriptInputFile = "c:\temp\script.sql"

        ArrangeScriptExecutor(Executor)
        Assert.AreEqual(expectedBuilder.ToStringExcludingOutputFileTime, Executor.CommandBuilder.ToStringExcludingOutputFileTime)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_Execute_Test()
        Dim Executor As New TestSQLScriptExecutor_ExecuteSQLScript_NoSQLExecutedScenario
        Dim parm As New List(Of String)

        With parm
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With

        Dim genericParam As New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)
        Executor.Execute(genericParam)

        Dim expectedBuilder As New CommandBuilder

        With expectedBuilder
            .Server = "SRV8099"
            .Database = "Oasys"
            .ScriptInputFile = "c:\temp\script.sql"
        End With


        Assert.AreEqual(expectedBuilder.ToStringExcludingOutputFileTime, Executor.CommandBuilder.ToStringExcludingOutputFileTime)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_TooFewParameters_IncorrectNumberOfParametersPassed_IsTrue()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("1")
        End With

        Dim genericParam As New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)

        Assert.IsTrue(Executor.IncorrectNumberOfParametersPassed(genericParam))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_TooManyParameters_IncorrectNumberOfParametersPassed_IsTrue()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("1")
            .Add("2")
            .Add("3")
            .Add("4")
            .Add("5")
        End With

        Dim genericParam As New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)

        Assert.IsTrue(Executor.IncorrectNumberOfParametersPassed(genericParam))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_IncorrectNumberOfParametersPassed_JustInsideLowerBoundary_IsFalse()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With

        Dim genericParam As New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)

        Assert.IsFalse(Executor.IncorrectNumberOfParametersPassed(genericParam))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_IncorrectNumberOfParametersPassed_JustInsideUpperBoundary_IsFalse()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With

        Dim genericParam As New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)

        Assert.IsFalse(Executor.IncorrectNumberOfParametersPassed(genericParam))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_IncorrectServerSwitchUsedInParameters_InvalidServerSwitch_IsTrue()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With

        Assert.IsTrue(Executor.InvalidSwitchInParams(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_IncorrectDatabaseSwitchUsedInParameters_InvalidDatabaseSwitch_IsTrue()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("-Fc:\temp\script.sql")
            .Add("/DOasys")
            .Add("-SSRV8099")
            .Add("(/U=888,/P=',CFC')")
        End With

        Assert.IsTrue(Executor.InvalidSwitchInParams(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_PopulateBuilder_NoServerSwitchUsedInParameters_DefaultServer_IsUsed()
        Dim Executor As New TestSQLScriptExecutor_WithOverridesToRemoveIntegrationDependancies
        Dim expected As New TestCommandBuilder_Override_InitialiseDefaultServerAndDefaultDatabase_ToReduceIntegration
        Dim parm As New List(Of String)

        With parm
            .Add("-Fc:\temp\script.sql")
            .Add("-DOasys")
            .Add("(/U=888,/P=',CFC')")
        End With

        Executor.PopulateBuilder(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm))

        Assert.AreEqual(expected.Server, Executor.CommandBuilder.Server)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_PopulateBuilderNoDatabaseSwitchUsedInParameters_DeafultDatabase_IsUsed()
        Dim Executor As New TestSQLScriptExecutor_WithOverridesToRemoveIntegrationDependancies
        Dim expected As New TestCommandBuilder_Override_InitialiseDefaultServerAndDefaultDatabase_ToReduceIntegration
        Dim parm As New List(Of String)

        With parm
            .Add("-Fc:\temp\script.sql")
            .Add("-SSRV8099")
            .Add("(/U=888,/P=',CFC')")
        End With

        Executor.PopulateBuilder(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm))

        Assert.AreEqual(expected.Database, Executor.CommandBuilder.Database)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_IncorrectFileSwitchUsedInParameters_InvalidFileSwitch_IsTrue()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("(/u=888,/P=',CFC')")
            .Add("-S SRV8099")
            .Add("Fc:\temp\script.sql")
            .Add("-DOasys")
        End With

        Assert.IsTrue(Executor.InvalidSwitchInParams(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_IncorrectNightlyRoutineSwitchUsedInParameters_InvalidNightlyRoutineSwitch_IsTrue()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("-fc:\temp\script.sql")
            .Add("-dOasys")
            .Add("-sSRV8099")
            .Add("(\U=888,/P=',CFC')")
        End With

        Assert.IsTrue(Executor.InvalidSwitchInParams(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ParamsIncludeAllNonOptionalSwitches_NoFileSwitchUsedInParameters_IsFalse()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("-dOasys")
            .Add("-sSRV8099")
            .Add("(\U=888,/P=',CFC')")
        End With

        Assert.IsFalse(Executor.ParamsIncludeAllNonOptionalSwitches(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ParamsIncludeAllNonOptionalSwitches_FileSwitchUsedInParameters_IsTrue()
        Dim Executor As New SQLScriptExecutor
        Dim parm As New List(Of String)

        With parm
            .Add("-fc:\temp\script.sql")
        End With

        Assert.IsTrue(Executor.ParamsIncludeAllNonOptionalSwitches(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm)))
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_CallsIncorrectNumberOfParametersPassed()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_IncorrectNumberOfParametersPassed_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.IncorrectNumberOfParametersPassedWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_PassesParamsToIncorrectNumberOfParametersPassed()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_IncorrectNumberOfParametersPassed_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.AreEqual(params, Executor.PassedParams)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_IncorrectNumberOfParametersPassed_IsTrue_CallsDisplayConsoleHelpAndEnd()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_IncorrectNumberOfParametersPassed_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.DisplayConsoleHelpAndEndWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_CallsParamsIncludeAllNonOptionalSwitches()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ParamsIncludeAllNonOptionalSwitches_IsFalse_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.ParamsIncludeAllNonOptionalSwitchesWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_PassesParamsToParamsIncludeAllNonOptionalSwitches()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ParamsIncludeAllNonOptionalSwitches_IsFalse_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.AreEqual(params, Executor.PassedParams)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_ParamsIncludeAllNonOptionalSwitches_IsFalse_CallsDisplayConsoleHelpAndEnd()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ParamsIncludeAllNonOptionalSwitches_IsFalse_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.DisplayConsoleHelpAndEndWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_CallsInvalidSwitchInParams()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_InvalidSwitchInParams_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.InvalidSwitchInParamsWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_PassesParamsToInvalidSwitchInParams()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_InvalidSwitchInParams_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.AreEqual(params, Executor.PassedParams)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_InvalidSwitchInParams_IsTrue_CallsDisplayConsoleHelpAndEnd()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_InvalidSwitchInParams_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.DisplayConsoleHelpAndEndWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_CallsScriptFilePathOrLogPathNotExist()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ScriptFilePathOrLogPathNotExist_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.ScriptFilePathOrLogPathNotExistWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_PassesParamsToScriptFilePathOrLogPathNotExist()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ScriptFilePathOrLogPathNotExist_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.AreEqual(params, Executor.PassedParams)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_ScriptFilePathOrLogPathNotExist_IsTrue_CallsDisplayConsoleHelpAndEnd()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ScriptFilePathOrLogPathNotExist_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.DisplayConsoleHelpAndEndWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_CallsScriptFileNotExist()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ScriptFileNotExist_IsTrue_CallsWriteScriptFileNotExistErrorToLogFileAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.ScriptFileNotExistWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_PassesParamsToScriptFileNotExist()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ScriptFileNotExist_IsTrue_CallsWriteScriptFileNotExistErrorToLogFileAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.AreEqual(params, Executor.PassedParams)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_ScriptFileNotExist_IsTrue_CallsWriteScriptFileNotExistErrorToLogFileAndEnd()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ScriptFileNotExist_IsTrue_CallsWriteScriptFileNotExistErrorToLogFileAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsTrue(Executor.WriteScriptFileNotExistErrorToLogFileAndEndwasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_ScriptFileNotExist_IsTrue_PassesParamsToWriteScriptFileNotExistErrorToLogFileAndEnd()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_ScriptFileNotExist_IsTrue_CallsWriteScriptFileNotExistErrorToLogFileAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.AreEqual(params, Executor.WriteScriptFileNotExistErrorToLogFileAndEndPassedParams)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_AllValidParams_Scenario_DoesNotCallsDisplayConsoleHelpAndEnd()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_AllCalledValidationRoutinesFindParamsValid_DoesNotCallDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsFalse(Executor.DisplayConsoleHelpAndEndWasCalled)
    End Sub

    <TestMethod()> _
    Public Sub SQLScriptExecutor_ValidateParameters_AllValidParams_Scenario_DoesNotCallsWriteScriptFileNotExistErrorToLogFileAndEndwasCalled()
        Dim Executor As New TestSQLScriptExecutor_ValidateParameters_AllCalledValidationRoutinesFindParamsValid_DoesNotCallDisplayConsoleHelpAndEnd_Scenario
        Dim paras As New List(Of String)
        Dim params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        With paras
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
            .Add("(/U=888,/P=',CFC')")
        End With
        params = New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(paras)

        Executor.ValidateParameters(params)
        Assert.IsFalse(Executor.WriteScriptFileNotExistErrorToLogFileAndEndWasCalled)
    End Sub

#Region "SQLScriptExecutor Test Scenarios"

    Private Class TestCommandBuilder_Override_InitialiseDefaultServerAndDefaultDatabase_ToReduceIntegration
        Inherits CommandBuilder

        Friend Overrides Sub InitialiseDefaultServerAndDefaultDatabase()

            Server = "DefaultServer"
            Database = "DefaultDatabase"
        End Sub
    End Class

    Private Class TestSQLScriptExecutor_WithOverridesToRemoveIntegrationDependancies
        Inherits SQLScriptExecutor

        Friend Overrides Function GetNewCommandBuilder() As Model.CommandBuilder

            Return New TestCommandBuilder_Override_InitialiseDefaultServerAndDefaultDatabase_ToReduceIntegration
        End Function

        Friend Overrides Function ScriptFileNotExist(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Return False
        End Function

        Friend Overrides Function ScriptFilePathOrLogPathNotExist(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Return False
        End Function

        Friend Overrides Sub WriteScriptFileNotExistErrorToLogFileAndEnd(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

            ' LEFT INTENTIONALLY BLANK
        End Sub

        Friend Overrides Sub DisplayConsoleHelpAndEnd()

            ' LEFT INTENTIONALLY BLANK
        End Sub
    End Class

    Private Class TestSQLScriptExecutor_GetFileContent_SuccessScenario
        Inherits TestSQLScriptExecutor_WithOverridesToRemoveIntegrationDependancies

        Friend Overrides Function GetFileContent() As String

            Return "jhwekfjjk;  jkwer Scheduled Script Completed Successfully grlokghlokl;qerkl;jkeghlqk    "

        End Function

    End Class

    Private Class TestSQLScriptExecutor_GetFileContent_FailureScenario
        Inherits TestSQLScriptExecutor_WithOverridesToRemoveIntegrationDependancies

        Friend Overrides Function GetFileContent() As String

            Return "jhwekfjjk;  jkwer suXXCCess grlokghlokl;qerkl;jkeghlqk    "

        End Function

    End Class

    Private Class TestSQLScriptExecutor_ExecuteSQLScript_NoSQLExecutedScenario
        Inherits TestSQLScriptExecutor_WithOverridesToRemoveIntegrationDependancies

        Friend Overrides Sub ExecuteSQLScript(ByVal cmdline As String, ByVal cdArguements As String)
            ' intentionally left blank
        End Sub
    End Class

    Private Class TestSQLScriptExecutor_ValidateParameters_OverrideDisplayConsoleHelpAndEndToFlagWhetherItIsCalled_And_FileSystemValidationToPreventHittingFileSystem
        Inherits TestSQLScriptExecutor_WithOverridesToRemoveIntegrationDependancies

        Private _displayConsoleHelpAndEndWasCalled As Boolean
        Friend _passedParams As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = Nothing

        Friend Overrides Sub DisplayConsoleHelpAndEnd()

            _displayConsoleHelpAndEndWasCalled = True
        End Sub

        Friend Overrides Function ScriptFilePathOrLogPathNotExist(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Return False
        End Function

        Friend Overrides Function ScriptFileNotExist(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Return False
        End Function

        Public ReadOnly Property DisplayConsoleHelpAndEndWasCalled() As Boolean
            Get
                Return _displayConsoleHelpAndEndWasCalled
            End Get
        End Property

        Public ReadOnly Property PassedParams() As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
            Get
                Return _passedParams
            End Get
        End Property
    End Class

    Private Class TestSQLScriptExecutor_ValidateParameters_IncorrectNumberOfParametersPassed_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Inherits TestSQLScriptExecutor_ValidateParameters_OverrideDisplayConsoleHelpAndEndToFlagWhetherItIsCalled_And_FileSystemValidationToPreventHittingFileSystem

        Private _incorrectNumberOfParametersPassedWasCalled As Boolean

        Friend Overrides Function IncorrectNumberOfParametersPassed(ByVal param As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            _incorrectNumberOfParametersPassedWasCalled = True
            _passedParams = param
            Return True
        End Function

        Public ReadOnly Property IncorrectNumberOfParametersPassedWasCalled() As Boolean
            Get
                Return _incorrectNumberOfParametersPassedWasCalled
            End Get
        End Property

    End Class

    Private Class TestSQLScriptExecutor_ValidateParameters_ParamsIncludeAllNonOptionalSwitches_IsFalse_CallsDisplayConsoleHelpAndEnd_Scenario
        Inherits TestSQLScriptExecutor_ValidateParameters_OverrideDisplayConsoleHelpAndEndToFlagWhetherItIsCalled_And_FileSystemValidationToPreventHittingFileSystem

        Private _ParamsIncludeAllNonOptionalSwitchesWasCalled As Boolean

        Friend Overrides Function ParamsIncludeAllNonOptionalSwitches(ByVal param As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            _ParamsIncludeAllNonOptionalSwitchesWasCalled = True
            _passedParams = param

            Return False
        End Function

        Public ReadOnly Property ParamsIncludeAllNonOptionalSwitchesWasCalled() As Boolean
            Get
                Return _ParamsIncludeAllNonOptionalSwitchesWasCalled
            End Get
        End Property
    End Class

    Private Class TestSQLScriptExecutor_ValidateParameters_InvalidSwitchInParams_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Inherits TestSQLScriptExecutor_ValidateParameters_OverrideDisplayConsoleHelpAndEndToFlagWhetherItIsCalled_And_FileSystemValidationToPreventHittingFileSystem

        Private _InvalidSwitchInParamsWasCalled As Boolean

        Friend Overrides Function InvalidSwitchInParams(ByVal param As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            _InvalidSwitchInParamsWasCalled = True
            _passedParams = param

            Return True
        End Function

        Public ReadOnly Property InvalidSwitchInParamsWasCalled() As Boolean
            Get
                Return _InvalidSwitchInParamsWasCalled
            End Get
        End Property
    End Class

    Private Class TestSQLScriptExecutor_ValidateParameters_ScriptFilePathOrLogPathNotExist_IsTrue_CallsDisplayConsoleHelpAndEnd_Scenario
        Inherits TestSQLScriptExecutor_ValidateParameters_OverrideDisplayConsoleHelpAndEndToFlagWhetherItIsCalled_And_FileSystemValidationToPreventHittingFileSystem

        Private _ScriptFilePathOrLogPathNotExistWasCalled As Boolean

        Friend Overrides Function ScriptFilePathOrLogPathNotExist(ByVal param As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            _ScriptFilePathOrLogPathNotExistWasCalled = True
            _passedParams = param

            Return True
        End Function

        Public ReadOnly Property ScriptFilePathOrLogPathNotExistWasCalled() As Boolean
            Get
                Return _ScriptFilePathOrLogPathNotExistWasCalled
            End Get
        End Property
    End Class

    Private Class TestSQLScriptExecutor_ValidateParameters_ScriptFileNotExist_IsTrue_CallsWriteScriptFileNotExistErrorToLogFileAndEnd_Scenario
        Inherits TestSQLScriptExecutor_ValidateParameters_OverrideDisplayConsoleHelpAndEndToFlagWhetherItIsCalled_And_FileSystemValidationToPreventHittingFileSystem

        Private _ScriptFileNotExistWasCalled As Boolean
        Private _WriteScriptFileNotExistErrorToLogFileAndEndWasCalled As Boolean
        Private _WriteScriptFileNotExistErrorToLogFileAndEndPassedParams As System.Collections.ObjectModel.ReadOnlyCollection(Of String)

        Friend Overrides Function ScriptFileNotExist(ByVal param As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            _ScriptFileNotExistWasCalled = True
            _passedParams = param

            Return True
        End Function

        Friend Overrides Sub WriteScriptFileNotExistErrorToLogFileAndEnd(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

            _WriteScriptFileNotExistErrorToLogFileAndEndWasCalled = True
            _WriteScriptFileNotExistErrorToLogFileAndEndPassedParams = params
        End Sub

        Public ReadOnly Property ScriptFileNotExistWasCalled() As Boolean
            Get
                Return _ScriptFileNotExistWasCalled
            End Get
        End Property

        Public ReadOnly Property WriteScriptFileNotExistErrorToLogFileAndEndWasCalled() As Boolean
            Get
                Return _WriteScriptFileNotExistErrorToLogFileAndEndWasCalled
            End Get
        End Property

        Public ReadOnly Property WriteScriptFileNotExistErrorToLogFileAndEndPassedParams() As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
            Get
                Return _WriteScriptFileNotExistErrorToLogFileAndEndPassedParams
            End Get
        End Property
    End Class

    Private Class TestSQLScriptExecutor_ValidateParameters_AllCalledValidationRoutinesFindParamsValid_DoesNotCallDisplayConsoleHelpAndEnd_Scenario
        Inherits TestSQLScriptExecutor_ValidateParameters_OverrideDisplayConsoleHelpAndEndToFlagWhetherItIsCalled_And_FileSystemValidationToPreventHittingFileSystem

        Private _WriteScriptFileNotExistErrorToLogFileAndEndWasCalled As Boolean

        Friend Overrides Function IncorrectNumberOfParametersPassed(ByVal param As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Return False
        End Function

        Friend Overrides Function InvalidSwitchInParams(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Return False
        End Function

        Friend Overrides Function ParamsIncludeAllNonOptionalSwitches(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Return True
        End Function

        Friend Overrides Sub WriteScriptFileNotExistErrorToLogFileAndEnd(ByVal params As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

            _WriteScriptFileNotExistErrorToLogFileAndEndWasCalled = True
        End Sub

        Public ReadOnly Property WriteScriptFileNotExistErrorToLogFileAndEndWasCalled() As Boolean
            Get
                Return _WriteScriptFileNotExistErrorToLogFileAndEndWasCalled
            End Get
        End Property
    End Class

#End Region

#Region "Private functions and procedures"

    Private Sub ArrangeScriptExecutor(ByRef Executor As SQLScriptExecutor)
        Dim parm As New List(Of String)

        With parm
            .Add("-S SRV8099")
            .Add("-D Oasys")
            .Add("-F c:\temp\script.sql")
        End With

        Executor.PopulateBuilder(New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(parm))
    End Sub

#End Region

End Class
