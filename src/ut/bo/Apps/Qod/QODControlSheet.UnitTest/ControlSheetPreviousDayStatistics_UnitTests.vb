﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data

<TestClass()> Public Class ControlSheetPreviousDayStatistics_UnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestPreviousDaysStatisticFactory
        Inherits PreviousDaysStatisticsFactory

        Private _RequirementEnabled As Nullable(Of Boolean)

        Public Sub New(ByVal RequirementEnabled As Nullable(Of Boolean))

            _RequirementEnabled = RequirementEnabled

        End Sub

        Friend Overrides Function Enabled() As Boolean
            Return _RequirementEnabled.Value
        End Function

    End Class

    Private Class TestRepository
        Inherits PreviousDaysStatisticsRepository

        Private Function GetData(ByVal prevDay As Date) As DataTable

            Dim dt As New DataTable

            dt.Columns.Add("NewStatus", GetType(Integer))
            dt.Columns.Add("Total", GetType(Integer))

            Dim row As DataRow = dt.NewRow
            row("NewStatus") = 700
            row("Total") = 17
            dt.Rows.Add(row)

            row = dt.NewRow
            row("NewStatus") = 800
            row("Total") = 6
            dt.Rows.Add(row)

            row = dt.NewRow
            row("NewStatus") = 900
            row("Total") = 11
            dt.Rows.Add(row)

            Return dt
        End Function

        Friend Overrides Function ReadPreviousDaysStatistics(ByVal PrevDay As Date) As System.Data.DataTable
            Dim dt As DataTable = GetData(PrevDay)

            Return dt
        End Function
    End Class
#End Region

#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitchEnabled_ReturnPreviousDaysStatistics()

        Dim Repository As IControlSheetReport
        Dim Factory As New TestPreviousDaysStatisticFactory(True)

        Repository = Factory.GetImplementation
        Assert.AreEqual("QODControlSheet.PreviousDaysStatistics", Repository.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementSwitchDisabled_ReturnPreviousDaysStatisticsSwitchedOff()

        Dim Repository As IControlSheetReport
        Dim Factory As New TestPreviousDaysStatisticFactory(False)

        Repository = Factory.GetImplementation
        Assert.AreEqual("QODControlSheet.PreviousDaysStatisticsSwitchedOff", Repository.GetType.FullName)

    End Sub

#End Region

#Region "Unit Tests"

    <TestMethod()> Public Sub ReportFooterRequired_GetFirstRowData_ReturnsNewStatus700()

        Dim Repository As New TestRepository
        Dim dt As DataTable

        dt = Repository.ReadPreviousDaysStatistics(CDate("2012-07-25"))
        Assert.AreEqual(700, CInt(dt.Rows(0).Item(0)))

    End Sub

    <TestMethod()> Public Sub ReportFooterRequired_GetFirstRowData_ReturnsTotal17()

        Dim Repository As New TestRepository
        Dim dt As DataTable

        dt = Repository.ReadPreviousDaysStatistics(CDate("2012-07-25"))
        Assert.AreEqual(17, CInt(dt.Rows(0).Item(1)))
    End Sub


    <TestMethod()> Public Sub ReportFooterRequired_GetSecondRowData_ReturnsNewStatus800()

        Dim Repository As New TestRepository
        Dim dt As DataTable

        dt = Repository.ReadPreviousDaysStatistics(CDate("2012-07-25"))
        Assert.AreEqual(800, CInt(dt.Rows(1).Item(0)))

    End Sub

    <TestMethod()> Public Sub ReportFooterRequired_GetSecondRowData_ReturnsTotal6()

        Dim Repository As New TestRepository
        Dim dt As DataTable

        dt = Repository.ReadPreviousDaysStatistics(CDate("2012-07-25"))
        Assert.AreEqual(6, CInt(dt.Rows(1).Item(1)))
    End Sub

    <TestMethod()> Public Sub ReportFooterRequired_GetThirdRowData_ReturnsNewStatus900()

        Dim Repository As New TestRepository
        Dim dt As DataTable

        dt = Repository.ReadPreviousDaysStatistics(CDate("2012-07-25"))
        Assert.AreEqual(900, CInt(dt.Rows(2).Item(0)))

    End Sub

    <TestMethod()> Public Sub ReportFooterRequired_GetThirdRowData_ReturnsTotal11()

        Dim Repository As New TestRepository
        Dim dt As DataTable

        dt = Repository.ReadPreviousDaysStatistics(CDate("2012-07-25"))
        Assert.AreEqual(11, CInt(dt.Rows(2).Item(1)))
    End Sub

#End Region

End Class
