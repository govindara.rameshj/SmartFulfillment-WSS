﻿<TestClass()> Public Class ControlSheetReportUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestControlSheetReportFactory
        Inherits ControlSheetReportFactory

        Private _RequirementEnabled As Nullable(Of Boolean)

        Public Sub New(ByVal RequirementEnabled As Nullable(Of Boolean))

            _RequirementEnabled = RequirementEnabled

        End Sub

        Friend Overrides Function Enabled() As Boolean

            If _RequirementEnabled.HasValue = False Then

                Return MyBase.Enabled()

            Else

                Return _RequirementEnabled.Value

            End If

        End Function

    End Class

#End Region

#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitchDisabled_ReturnWithAddressInfo()

        Dim Repository As IControlSheetReport
        Dim Factory As New TestControlSheetReportFactory(False)

        Repository = Factory.GetImplementation

        Assert.AreEqual("QODControlSheet.WithAddressInfo", Repository.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementSwitchEnabled_ReturnWithoutAddressInfo()

        Dim Repository As IControlSheetReport
        Dim Factory As New TestControlSheetReportFactory(True)

        Repository = Factory.GetImplementation

        Assert.AreEqual("QODControlSheet.WithoutAddressInfo", Repository.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - DataAdapter"

    <TestMethod()> Public Sub TableAdaptorWillReturnAddressInfo()

        Dim Info As New WithAddressInfo
        Dim DT As New csDataSet.CORHDRDataTable
        Dim TA As New csDataSetTableAdapters.CORHDRTableAdapter

        Try

            TA.ExplicitSetConnectionPropertyOnly = Nothing
            Info.GetData(TA, DT, Nothing)

        Catch ex As Exception

            'exception due to connection not provided; still an unit test

        End Try

        Assert.IsTrue(TA.Adapter.SelectCommand.CommandText.ToUpper.Contains("ADDR1"))

    End Sub

    <TestMethod()> Public Sub TableAdaptorWillNotReturnAddressInfo()

        Dim Info As New WithoutAddressInfo
        Dim DT As New csDataSet.CORHDRDataTable
        Dim TA As New csDataSetTableAdapters.CORHDRTableAdapter

        Try

            TA.ExplicitSetConnectionPropertyOnly = Nothing
            Info.GetData(TA, DT, Nothing)

        Catch ex As Exception

            'exception due to connection not provided; still an unit test

        End Try

        Assert.IsFalse(TA.Adapter.SelectCommand.CommandText.ToUpper.Contains("ADDR1"))

    End Sub

#End Region

End Class