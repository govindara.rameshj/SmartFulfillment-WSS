﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Data
<TestClass()> Public Class ControlSheetExtraSubTotals_UnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    Private Class TestExtraSubTotalsFactory
        Inherits ExtraSubTotalsFactory

        Private _RequirementEnabled As Nullable(Of Boolean)

        Public Sub New(ByVal RequirementEnabled As Nullable(Of Boolean))

            _RequirementEnabled = RequirementEnabled

        End Sub

        Friend Overrides Function Enabled() As Boolean
            Return _RequirementEnabled.Value
        End Function

    End Class

    Private Class TestRepository
        Inherits ExtraSubTotalsRepository


        Private Function GetData(ByVal prevDay As Date) As DataTable

            Dim dt As New DataTable

            dt.Columns.Add("NewStatus", GetType(Integer))
            dt.Columns.Add("Total", GetType(Integer))

            Dim row As DataRow = dt.NewRow
            row("NewStatus") = 700
            row("Total") = 17
            dt.Rows.Add(row)

            row = dt.NewRow
            row("NewStatus") = 800
            row("Total") = 6
            dt.Rows.Add(row)

            row = dt.NewRow
            row("NewStatus") = 900
            row("Total") = 11
            dt.Rows.Add(row)

            Return dt
        End Function

        Friend Overrides Function ReadTotalDeliveriesToday(ByVal Today As Date) As Integer
            Return CInt(78)
        End Function
    End Class


#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitchEnabled_ReturnExtraSubTotals()

        Dim Repository As IExtraSubTotals
        Dim Factory As New TestExtraSubTotalsFactory(True)

        Repository = CType(Factory.GetImplementation, IExtraSubTotals)
        Assert.AreEqual("QODControlSheet.ExtraSubTotals", Repository.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementSwitchDisabled_ReturnExtraSubTotalsSwitchedOff()

        Dim Repository As IExtraSubTotals
        Dim Factory As New TestExtraSubTotalsFactory(False)

        Repository = CType(Factory.GetImplementation, IExtraSubTotals)
        Assert.AreEqual("QODControlSheet.ExtraSubTotalsSwitchedOff", Repository.GetType.FullName)

    End Sub

#End Region

#Region "Unit Tests"

    <TestMethod()> Public Sub ReportFooterRequired_GetFirstRowData_ReturnsNewStatus700()

        Dim Repository As New TestRepository
        Assert.AreEqual(78, Repository.ReadTotalDeliveriesToday(CDate("2012-07-25")))

    End Sub

#End Region

End Class
