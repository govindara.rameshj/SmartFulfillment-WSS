﻿<TestClass()> Public Class StockAdjustmentManagerUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub RequirementFactoryTestScenerios()

        ArrangeRequirementSwitchDependency(True)
        Assert.IsFalse((New StockAdjustmentManagerRollbackTransactionReturnStateFactory).GetImplementation, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        Assert.IsTrue((New StockAdjustmentManagerRollbackTransactionReturnStateFactory).GetImplementation, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        Assert.IsTrue((New StockAdjustmentManagerRollbackTransactionReturnStateFactory).GetImplementation, "Test Two: Return existing implementation")

    End Sub

    <TestMethod()> Public Sub Procedure_MakeEqual_TestScenarios()

        Dim Manager As New StockAdjustmentManager
        Dim Source As New Adjustment
        Dim Target As New Adjustment

        PopulateSourceAdjustment(Source)

        Manager.AdjustmentMakeEqual(CType(Source, IAdjustment), CType(Target, IAdjustment))

        Assert.AreEqual(Now.Date, Target.AdjustmentDate, "Test: AdjustmentDate")
        Assert.AreEqual("12", Target.Code, "Test: Code")
        Assert.AreEqual("123456", Target.SKUN, "Test: SKUN")
        Assert.AreEqual("34", Target.SEQN, "Test: SEQN")
        Assert.AreEqual(9876, Target.AmendId, "Test: AmendId")
        Assert.AreEqual("ABCDE", Target.EmployeeInitials, "Test: EmployeeInitials")
        Assert.AreEqual("ZY", Target.Department, "Test: Department")
        Assert.AreEqual(CType(456, Decimal), Target.StartingStock, "Test: StartingStock")
        Assert.AreEqual(CType(789, Decimal), Target.Quantity, "Test: Quantity")
        Assert.AreEqual(CType(1234.56, Decimal), Target.Price, "Test: Price")
        Assert.AreEqual(CType(5678.9, Decimal), Target.Cost, "Test: Cost")
        Assert.AreEqual(True, Target.IsSentToHO, "Test: IsSentToHO")
        Assert.AreEqual("N", Target.AdjustType, "Test: AdjustType")
        Assert.AreEqual("comment", Target.Comment, "Test: Comment")
        Assert.AreEqual("918273", Target.DeliveryNumber, "Test: DeliveryNumber")
        Assert.AreEqual("X", Target.ReversalCode, "Test: ReversalCode")
        Assert.AreEqual("W", Target.MarkDownOrWriteOff, "Test: MarkDownOrWriteOff")
        Assert.AreEqual("ZZZ", Target.WriteOffAuthority, "Test: WriteOffAuthority")
        Assert.AreEqual(Now.Date.AddDays(5), Target.WriteOffAuthorityDate, "Test: WriteOffAuthorityDate")
        Assert.AreEqual(987, Target.PeriodId, "Test: PeriodId")
        Assert.AreEqual("987654", Target.TransferSku_TSKU, "Test: TransferSku_TSKU")
        Assert.AreEqual(CType(9182, Decimal), Target.TransferValue, "Test: TransferValue")
        Assert.AreEqual(CType(7364, Decimal), Target.TransferStart, "Test: TransferStart")
        Assert.AreEqual(CType(5574, Decimal), Target.TransferPrice, "Test: TransferPrice")
        Assert.AreEqual(True, Target.IsReversed, "Test: IsReversed")
        Assert.AreEqual("Y", Target.RTI, "Test: RTI")
        Assert.AreEqual(True, Target.ExistsInDatabase, "Test: ExistsInDatabase")



        'SOLVES THE PROBLEM!!!!!!!!!!!!!!!
        Assert.AreNotEqual(CType(3456, Decimal), Target.EndingStock, "Test: EndingStock")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

    Private Sub PopulateSourceAdjustment(ByRef Value As Adjustment)

        With Value

            .AdjustmentDate = Now.Date
            .Code = "12"
            .SKUN = "123456"
            .SEQN = "34"
            .AmendId = 9876
            .EmployeeInitials = "ABCDE"
            .Department = "ZY"
            .StartingStock = 456
            .Quantity = 789
            .Price = CType(1234.56, Decimal)
            .Cost = CType(5678.9, Decimal)
            .IsSentToHO = True
            .AdjustType = "N"
            .Comment = "comment"
            .DeliveryNumber = "918273"
            .ReversalCode = "X"
            .MarkDownOrWriteOff = "W"
            .WriteOffAuthority = "ZZZ"
            .WriteOffAuthorityDate = Now.Date.AddDays(5)
            .PeriodId = 987
            .TransferSku_TSKU = "987654"
            .TransferValue = 9182
            .TransferStart = 7364
            .TransferPrice = 5574
            .IsReversed = True
            .RTI = "Y"
            .ExistsInDatabase = True
            .EndingStock = 3456

        End With

    End Sub

#End Region

End Class
