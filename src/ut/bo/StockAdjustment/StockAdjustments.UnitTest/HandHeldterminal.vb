﻿<TestClass()> Public Class HandHeldterminal

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub HandHeldTerminalDetail_LoadRefundedItems_ValidateCount_StoredProcedure_Against_ExistingDynamicSequel_MinusDaysOne()

        Dim colHHTDetail As HandHeldTerminalDetailCollection = Nothing
        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        'arrange & act
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(Now.Date, -1)
        StoredProcedureDT = LoadRefundedItemsNewStoredProcedure(Now.Date, -1)

        Assert.AreEqual(True, IIf(DynamicSequelDT.Rows.Count = StoredProcedureDT.Rows.Count, True, False))

    End Sub

    <TestMethod()> Public Sub HandHeldTerminalDetail_LoadRefundedItems_ValidateCount_StoredProcedure_Against_ExistingDynamicSequel_MinusDaysSeven()

        Dim colHHTDetail As HandHeldTerminalDetailCollection = Nothing
        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        'arrange & act
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(Now.Date, -7)
        StoredProcedureDT = LoadRefundedItemsNewStoredProcedure(Now.Date, -7)

        Assert.AreEqual(True, IIf(DynamicSequelDT.Rows.Count = StoredProcedureDT.Rows.Count, True, False))

    End Sub

    <TestMethod()> Public Sub HandHeldTerminalDetail_LoadRefundedItems_ValidateFieldNames_StoredProcedure_Against_ExistingDynamicSequel_MinusDaysZero()

        Dim colHHTDetail As HandHeldTerminalDetailCollection = Nothing
        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        'arrange & act
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(Now.Date, 0)
        StoredProcedureDT = LoadRefundedItemsNewStoredProcedure(Now.Date, 0)

        'column zero - SKUN
        Assert.AreEqual(True, IIf(DynamicSequelDT.Columns.Item(0).ColumnName = StoredProcedureDT.Columns.Item(0).ColumnName, True, False))

    End Sub


#End Region

#Region "Private Test Procedures And Functions"

    Private Function CreateTable() As DataTable

        Dim DT As DataTable

        DT = New DataTable
        DT.Columns.Add("SKUN", System.Type.GetType("System.String"))

        Return DT

    End Function

    Private Function LoadRefundedItemsExistingDynamicSequel(ByVal DateCreated As Date, ByVal Offset As Integer) As DataTable

        Dim sb As New StringBuilder
        Dim DT As DataTable

        sb.Append("Select  Distinct SKUN  from HHTDET where ORIG = 'R' and ICNT = '0' and DATE1 >= ")
        sb.Append("'" & Format(DateCreated.AddDays(Offset), "yyyy-MM-dd") & "'")

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        com.CommandText = sb.ToString
                        DT = com.ExecuteDataTable
                End Select
            End Using
        End Using
        Return DT

    End Function

    Private Function LoadRefundedItemsNewStoredProcedure(ByVal DateCreated As Date, ByVal Offset As Integer) As DataTable

        Dim DT As DataTable

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)
                Select Case con.DataProvider
                    Case DataProvider.Sql
                        com.StoredProcedureName = "usp_HandHeldTerminalDetailRefundItemsDistinctSKUs"
                        com.AddParameter("@SelectedDate", DateCreated.AddDays(Offset), SqlDbType.Date)
                        DT = com.ExecuteDataTable
                End Select
            End Using
        End Using
        Return DT

    End Function

#End Region

End Class
