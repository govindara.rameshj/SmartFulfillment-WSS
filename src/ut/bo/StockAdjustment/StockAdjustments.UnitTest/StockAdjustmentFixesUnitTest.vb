﻿<TestClass()> Public Class StockAdjustmentFixesUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestStockAdjustmentManager
        Inherits StockAdjustmentManager

#Region "Private Variables"

        Private _NormalAdjustmentOrginalCalled As Boolean
        Private _NormalAdjustmentWithConnectionReferenceCalled As Boolean
        Private _NormalAdjustmentWithReversalCalled As Boolean

#End Region

#Region "Local Properties"

        Public ReadOnly Property NormalAdjustmentOrginalCalled() As Boolean
            Get
                Return _NormalAdjustmentOrginalCalled
            End Get
        End Property

        Public ReadOnly Property NormalAdjustmentWithConnectionReferenceCalled() As Boolean
            Get
                Return _NormalAdjustmentWithConnectionReferenceCalled
            End Get
        End Property

        Public ReadOnly Property NormalAdjustmentWithReversalCalled() As Boolean
            Get
                Return _NormalAdjustmentWithReversalCalled
            End Get
        End Property

#End Region

#Region "Overrides"

        Public Overrides Function NormalAdjustment(ByVal SACode As ISaCode, ByRef Adjustment As IAdjustment, ByVal SkuNumber As String, ByVal AdjDate As Date, ByVal DRLNumber As String, ByRef AdjQty As Integer, ByVal Comment As String, ByVal EmployeId As Integer) As Boolean

            _NormalAdjustmentOrginalCalled = True

            'Return MyBase.NormalAdjustment(SACode, Adjustment, SkuNumber, AdjDate, DRLNumber, AdjQty, Comment, EmployeId)

        End Function

        Public Overrides Function NormalAdjustment(ByRef Con As Connection, ByVal SACode As ISaCode, ByRef Adjustment As IAdjustment, ByVal SkuNumber As String, ByVal AdjDate As Date, ByVal DRLNumber As String, ByRef AdjQty As Integer, ByVal Comment As String, ByVal EmployeeID As Integer) As Boolean

            _NormalAdjustmentWithConnectionReferenceCalled = True

            'Return MyBase.NormalAdjustment(Con, SACode, Adjustment, SkuNumber, AdjDate, DRLNumber, AdjQty, Comment, EmployeeID)

        End Function

        Public Overrides Function NormalAdjustment(ByVal AdjustmentCode As ISaCode, ByVal ActualStockAdjustment As IAdjustment, ByVal ExistingStockAdjustment As IAdjustment, ByVal SkuNumber As String, ByVal AdjustmentDate As Date, ByVal DRLNumber As String, ByVal AdjustmentQuantity As Integer, ByVal Comment As String, ByVal EmployeeID As Integer) As Boolean

            _NormalAdjustmentOrginalCalled = True

            If AdjustmentCode.CodeNumber = "04" Then _NormalAdjustmentWithReversalCalled = True

            'Return MyBase.NormalAdjustment(AdjustmentCode, ActualStockAdjustment, ExistingStockAdjustment, SkuNumber, AdjustmentDate, DRLNumber, AdjustmentQuantity, Comment, EmployeeID)

        End Function

#End Region

    End Class

    Private Class TestStockAdjustmentFixes
        Inherits StockAdjustmentFixes

#Region "Private Variables"

        Private _EnableMaintainAdjustmentButtonCalled As Boolean
        Private _EnableReverseAdjustmentButtonCalled As Boolean

#End Region

#Region "Local Properties"

        Public ReadOnly Property EnableMaintainAdjustmentButtonCalled() As Boolean
            Get
                Return _EnableMaintainAdjustmentButtonCalled
            End Get
        End Property

        Public ReadOnly Property EnableReverseAdjustmentButtonCalled() As Boolean
            Get
                Return _EnableReverseAdjustmentButtonCalled
            End Get
        End Property

#End Region

#Region "Constructor"

        Public Sub New()

            _EnableMaintainAdjustmentButtonCalled = False
            _EnableReverseAdjustmentButtonCalled = False

        End Sub

#End Region

#Region "Overrides"

        Friend Overrides Function EnableMaintainAdjustmentButton(ByVal HeadOfficeCommed As Boolean, ByVal IsReversed As Boolean) As Boolean

            _EnableMaintainAdjustmentButtonCalled = True

            'Return MyBase.EnableMaintainAdjustmentButton(HeadOfficeCommed, IsReversed)

        End Function

        Friend Overrides Function EnableReverseAdjustmentButton(ByVal HeadOfficeCommed As Boolean, ByVal IsReversed As Boolean) As Boolean

            _EnableReverseAdjustmentButtonCalled = True

            'Return MyBase.EnableReverseAdjustmentButton(HeadOfficeCommed, IsReversed)

        End Function

#End Region

    End Class

#End Region

#Region "Class : StockAdjustmentFixes"

    <TestMethod()> Public Sub RequirementFactory_TestScenerios()

        Dim Fixes As IStockAdjustmentFixes
        Dim Factory As New StockAdjustmentFixesFactory

        ArrangeRequirementSwitchDependency(True)

        Fixes = Factory.GetImplementation
        Assert.AreEqual("StockAdjustments.StockAdjustmentFixes", Fixes.GetType.FullName, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        Fixes = Factory.GetImplementation
        Assert.AreEqual("StockAdjustments.StockAdjustmentFixesExisting", Fixes.GetType.FullName, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        Fixes = Factory.GetImplementation
        Assert.AreEqual("StockAdjustments.StockAdjustmentFixesExisting", Fixes.GetType.FullName, "Test Three: Return existing implementation")

    End Sub

    <TestMethod()> Public Sub Function_ProcessReverseAdjustment_TestScenarios()

        Dim Fixes As New StockAdjustmentFixes
        Dim ExistingStockAdjustment As IAdjustment
        Dim AdjustmentCode As ISaCode

        ExistingStockAdjustment = New Adjustment
        AdjustmentCode = New SACode.SaCode

        AdjustmentCode.CodeNumber = String.Empty
        Assert.IsFalse(Fixes.ProcessReverseAdjustment(AdjustmentCode, Nothing), "Test: AdjustmentCode: String.Empty | StockAdjustment: Nothing | Return: False")

        AdjustmentCode.CodeNumber = String.Empty
        Assert.IsFalse(Fixes.ProcessReverseAdjustment(AdjustmentCode, ExistingStockAdjustment), "Test: AdjustmentCode: String.Empty | StockAdjustment: Exist | Return: False")

        AdjustmentCode.CodeNumber = "04"
        Assert.IsTrue(Fixes.ProcessReverseAdjustment(AdjustmentCode, ExistingStockAdjustment), "Test: AdjustmentCode: '04' | StockAdjustment: Exist | Return: True")

    End Sub

    <TestMethod()> Public Sub Function_PersistNormalAdjustments_TestScenarios()

        Dim Fixes As IStockAdjustmentFixes
        Dim Manager As TestStockAdjustmentManager
        Dim AdjustmentCode As New SACode.SaCode

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(True)

        Manager = New TestStockAdjustmentManager

        Fixes = (New StockAdjustmentFixesFactory).GetImplementation

        AdjustmentCode.CodeNumber = "01"
        Fixes.PersistNormalAdjustments(CType(Manager, IStockAdjustmentManager), AdjustmentCode, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
        Assert.IsTrue(Manager.NormalAdjustmentOrginalCalled, "Test: Switch On | Normal Stock Adjustment")

        AdjustmentCode.CodeNumber = "04"
        Fixes.PersistNormalAdjustments(CType(Manager, IStockAdjustmentManager), AdjustmentCode, Nothing, New Adjustment, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
        Assert.IsTrue(Manager.NormalAdjustmentWithReversalCalled, "Test: Switch On | Reverse Stock Adjustment")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(False)

        Manager = New TestStockAdjustmentManager

        Fixes = (New StockAdjustmentFixesFactory).GetImplementation
        Fixes.PersistNormalAdjustments(CType(Manager, IStockAdjustmentManager), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(Manager.NormalAdjustmentOrginalCalled, "Test: Switch Off | Normal Stock Adjustment")

    End Sub

    <TestMethod()> Public Sub Procedure_ExistingCodeFourAdjustmentToBeReversed_TestScenarios()

        Dim Fixes As IStockAdjustmentFixes

        Dim Source As New Adjustment
        Dim Target As IAdjustment

        Dim Manager As New StockAdjustmentManager
        Dim AdjustmentCode As New SACode.SaCode

        PopulateSourceAdjustment(Source)

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(True)
        Fixes = (New StockAdjustmentFixesFactory).GetImplementation

        Target = New Adjustment

        AdjustmentCode.CodeNumber = "01"
        Fixes.ConfigureExistingAdjustment(CType(Manager, IStockAdjustmentManager), AdjustmentCode, CType(Source, IAdjustment), Target)
        Assert.AreEqual(String.Empty, Target.Code, "Test: Switch On | Normal Adjustment | Adjustment Code: String.Empty")

        AdjustmentCode.CodeNumber = "04"
        Fixes.ConfigureExistingAdjustment(CType(Manager, IStockAdjustmentManager), AdjustmentCode, CType(Source, IAdjustment), Target)
        Assert.AreEqual("12", Target.Code, "Test: Switch On | Code Four Adjustment | Adjustment Code: 12")
        Assert.IsTrue(Target.IsReversed, "Test: Switch On | Code Four Adjustment | IsReversed: True")
        Assert.IsTrue(Target.ExistsInDatabase, "Test: Switch On | Code Four Adjustment | ExistsInDatabase: True")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(False)
        Fixes = (New StockAdjustmentFixesFactory).GetImplementation

        Target = New Adjustment

        AdjustmentCode.CodeNumber = "01"
        Fixes.ConfigureExistingAdjustment(CType(Manager, IStockAdjustmentManager), AdjustmentCode, CType(Source, IAdjustment), Target)
        Assert.AreEqual(String.Empty, Target.Code, "Test: Switch Off | Normal Adjustment | Adjustment Code: String.Empty")

        AdjustmentCode.CodeNumber = "04"
        Fixes.ConfigureExistingAdjustment(CType(Manager, IStockAdjustmentManager), AdjustmentCode, CType(Source, IAdjustment), Target)
        Assert.AreEqual(String.Empty, Target.Code, "Test: Switch Off | Code Four Adjustment | Adjustment Code: String.Empty")

    End Sub

    <TestMethod()> Public Sub Function_EnableMaintainAdjustment_TestScenarios()

        Dim Fixes As New StockAdjustmentFixes

        Assert.IsFalse(Fixes.EnableMaintainAdjustmentButton(True, False), "Test: HeadOfficeCommed: True | IsReversed: False | Return: False")
        Assert.IsFalse(Fixes.EnableMaintainAdjustmentButton(False, True), "Test: HeadOfficeCommed: False | IsReversed: True | Return: False")
        Assert.IsFalse(Fixes.EnableMaintainAdjustmentButton(True, True), "Test: HeadOfficeCommed: True | IsReversed: True | Return: False")
        Assert.IsTrue(Fixes.EnableMaintainAdjustmentButton(False, False), "Test: HeadOfficeCommed: False | IsReversed: False | Return: True")

    End Sub

    <TestMethod()> Public Sub Function_EnableReverseAdjustment_TestScenarios()

        Dim Fixes As New StockAdjustmentFixes

        Assert.IsTrue(Fixes.EnableReverseAdjustmentButton(True, False), "Test: HeadOfficeCommed: True | IsReversed: False | Return: True")
        Assert.IsFalse(Fixes.EnableReverseAdjustmentButton(False, True), "Test: HeadOfficeCommed: False | IsReversed: True | Return: False")
        Assert.IsFalse(Fixes.EnableReverseAdjustmentButton(True, True), "Test: HeadOfficeCommed: True | IsReversed: True | Return: False")
        Assert.IsFalse(Fixes.EnableReverseAdjustmentButton(False, False), "Test: HeadOfficeCommed: False | IsReversed: False | Return: False")

    End Sub

    <TestMethod()> Public Sub Procedure_EnableMaintainAndReverseAdjustmentButtons_TestScenarios()

        Dim Fixes As TestStockAdjustmentFixes


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Fixes = New TestStockAdjustmentFixes
        Fixes.EnableMaintainAndReverseAdjustmentButtons(False, New Adjustment, Nothing, Nothing)
        Assert.IsTrue(Fixes.EnableMaintainAdjustmentButtonCalled, "Test A: EnableMaintainAdjustmentButton function called")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Fixes = New TestStockAdjustmentFixes
        Fixes.EnableMaintainAndReverseAdjustmentButtons(True, New Adjustment, Nothing, Nothing)
        Assert.IsFalse(Fixes.EnableMaintainAdjustmentButtonCalled, "Test B: EnableMaintainAdjustmentButton function not called")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Fixes = New TestStockAdjustmentFixes
        Fixes.EnableMaintainAndReverseAdjustmentButtons(False, Nothing, Nothing, Nothing)
        Assert.IsFalse(Fixes.EnableMaintainAdjustmentButtonCalled, "Test C: EnableMaintainAdjustmentButton function not called")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Fixes = New TestStockAdjustmentFixes
        Fixes.EnableMaintainAndReverseAdjustmentButtons(False, New Adjustment, Nothing, Nothing)
        Assert.IsTrue(Fixes.EnableMaintainAdjustmentButtonCalled, "Test D: EnableMaintainAdjustmentButton function called")

    End Sub

    <TestMethod()> Public Sub Function_AlreadyReversed_TestScenarios()

        Dim Fixes As New StockAdjustmentFixes

        Assert.IsFalse(Fixes.AlreadyReversed(New KeyPressEventArgs(ChrW(Keys.Down)), Nothing), "Test: EventArguement: Down | IsReversed: Nothing | Return: False")
        Assert.IsFalse(Fixes.AlreadyReversed(New KeyPressEventArgs(ChrW(Keys.Enter)), False), "Test: EventArguement: Enter | IsReversed: False | Return: False")
        Assert.IsTrue(Fixes.AlreadyReversed(New KeyPressEventArgs(ChrW(Keys.Enter)), True), "Test: EventArguement: Enter | IsReversed: True | Return: True")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

    Private Sub PopulateSourceAdjustment(ByRef Value As Adjustment)

        With Value

            .AdjustmentDate = Now.Date
            .Code = "12"
            .SKUN = "123456"
            .SEQN = "34"
            .AmendId = 9876
            .EmployeeInitials = "ABCDE"
            .Department = "ZY"
            .StartingStock = 456
            .Quantity = 789
            .Price = CType(1234.56, Decimal)
            .Cost = CType(5678.9, Decimal)
            .IsSentToHO = False
            .AdjustType = "N"
            .Comment = "comment"
            .DeliveryNumber = "918273"
            .ReversalCode = "X"
            .MarkDownOrWriteOff = "W"
            .WriteOffAuthority = "ZZZ"
            .WriteOffAuthorityDate = Now.Date.AddDays(5)
            .PeriodId = 987
            .TransferSku_TSKU = "987654"
            .TransferValue = 9182
            .TransferStart = 7364
            .TransferPrice = 5574
            .IsReversed = False
            .RTI = "Y"
            .ExistsInDatabase = False
            .EndingStock = 3456

        End With

    End Sub

#End Region

End Class