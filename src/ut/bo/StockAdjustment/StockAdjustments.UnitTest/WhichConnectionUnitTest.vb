﻿<TestClass()> Public Class WhichConnectionUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitch_TestScenerios()

        Dim WhichConnection As IWhichConnection
        Dim Factory As New WhichConnectionFactory

        ArrangeRequirementSwitchDependency(True)
        WhichConnection = Factory.GetImplementation
        Assert.AreEqual("StockAdjustments.UseExistingConnection", WhichConnection.GetType.FullName, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        WhichConnection = Factory.GetImplementation
        Assert.AreEqual("StockAdjustments.UseNewConnection", WhichConnection.GetType.FullName, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        WhichConnection = Factory.GetImplementation
        Assert.AreEqual("StockAdjustments.UseNewConnection", WhichConnection.GetType.FullName, "Test Three: Return existing implementation")

    End Sub

#End Region

#Region "Unit Test - Connection"

    <TestMethod()> Public Sub CorrectConnectionReturned_TestScenarios()

        Dim TestConnection As IConnection
        Dim WhichConnection As IWhichConnection
        Dim Factory As New WhichConnectionFactory

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(True)
        WhichConnection = Factory.GetImplementation

        TestConnection = New Connection

        TestConnection.StartTransaction()
        WhichConnection.ConnectionObject(TestConnection)

        Assert.IsNotNull(TestConnection.SqlTransaction, "Test: Use existing connection object")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(False)
        WhichConnection = Factory.GetImplementation

        TestConnection = New Connection

        TestConnection.StartTransaction()
        WhichConnection.ConnectionObject(TestConnection)

        Assert.IsNull(TestConnection.SqlTransaction, "Test: Create new connection object")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class
