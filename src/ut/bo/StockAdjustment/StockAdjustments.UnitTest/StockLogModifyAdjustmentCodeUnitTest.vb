﻿<TestClass()> Public Class StockLogModifyAdjustmentCodeUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitch_TestScenerios()

        Dim Modify As IStockLogModifyAdjustmentCode

        ArrangeRequirementSwitchDependency(True)
        Modify = (New StockLogModifyAdjustmentCodeFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.StockLogAdjustmentCodeUnaltered", Modify.GetType.FullName, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        Modify = (New StockLogModifyAdjustmentCodeFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.StockLogAdjustmentCodeIncorrectlyModified", Modify.GetType.FullName, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        Modify = (New StockLogModifyAdjustmentCodeFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.StockLogAdjustmentCodeIncorrectlyModified", Modify.GetType.FullName, "Test Three: Return existing implementation")

    End Sub

#End Region

#Region "Unit Test - Modify Adjustment Code"

    <TestMethod()> Public Sub ProcedureModifyAdjustmentCode_TestScenarios()

        Dim Modify As IStockLogModifyAdjustmentCode
        Dim AdjustmentCode As ISaCode


        AdjustmentCode = New SACode.SaCode

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(True)
        Modify = (New StockLogModifyAdjustmentCodeFactory).GetImplementation

        AdjustmentCode.MarkdownWriteoff = String.Empty
        Modify.ModifyAdjustmentCode(AdjustmentCode)
        Assert.AreEqual(String.Empty, AdjustmentCode.MarkdownWriteoff, "Test: Switch Off | MarkDown Or WriteOff Property: String.Empty | Return String.Empty")

        AdjustmentCode.MarkdownWriteoff = "ABC"
        Modify.ModifyAdjustmentCode(AdjustmentCode)
        Assert.AreEqual("ABC", AdjustmentCode.MarkdownWriteoff, "Test: Switch Off | MarkDown Or WriteOff Property: String.Empty | Return ABC")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(False)
        Modify = (New StockLogModifyAdjustmentCodeFactory).GetImplementation

        AdjustmentCode.MarkdownWriteoff = String.Empty
        Modify.ModifyAdjustmentCode(AdjustmentCode)
        Assert.AreEqual(Space(1), AdjustmentCode.MarkdownWriteoff, "Test: Switch On | MarkDown Or WriteOff Property: String.Empty | Return Single Blank String")

        AdjustmentCode.MarkdownWriteoff = "ABC"
        Modify.ModifyAdjustmentCode(AdjustmentCode)
        Assert.AreEqual("ABC", AdjustmentCode.MarkdownWriteoff, "Test: Switch On | MarkDown Or WriteOff Property: String.Empty | Return ABC")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class