﻿<TestClass()> Public Class FactoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory Unit Test - ClearButtons"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassClearButtons()

        Dim NewVersionOnly As Create.IClearButtons
        Dim Factory As IBaseFactory(Of Create.IClearButtons)

        Factory = New Create.ClearButtonsFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("EnterStockAdjustment.Create.ClearButtons", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Factory Unit Test - StockAdjustmentPrice"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassStockAdjustmentPrice()

        Dim NewVersionOnly As IStockAdjustmentPrice
        Dim Factory As IBaseFactory(Of IStockAdjustmentPrice)

        Factory = New StockAdjustmentPriceFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("StockAdjustments.StockAdjustmentPrice", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Factory Unit Test - AdjustmentRepository"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassAdjustmentRepository()

        Dim NewVersionOnly As IAdjustmentRepository
        Dim Factory As IBaseFactory(Of IAdjustmentRepository)

        Factory = New ReadAdjustmentPicCountFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("StockAdjustments.AdjustmentRepository", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

End Class