﻿<TestClass()> Public Class StockLogOverloadDecideUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestStockLog
        Inherits StockLog

#Region "Private Variables"

        Private _ParametersProvided As Boolean
        Private _ReadStockLogIgnoreConnectionCalled As Boolean
        Private _ReadStockLogUseConnectionCalled As Boolean

#End Region

#Region "Local Properties"

        Public ReadOnly Property ReadStockLogIgnoreConnectionCalled() As Boolean
            Get
                Return _ReadStockLogIgnoreConnectionCalled
            End Get
        End Property

        Public ReadOnly Property ReadStockLogUseConnectionCalled() As Boolean
            Get
                Return _ReadStockLogUseConnectionCalled
            End Get
        End Property

#End Region

#Region "Constructor"

        Public Sub New(ByVal ParametersProvided As Boolean)

            _ParametersProvided = ParametersProvided

        End Sub

#End Region

#Region "Overrides"

        Public Overrides Function ReadStockLog(ByVal ProductCode As String) As Boolean

            _ReadStockLogIgnoreConnectionCalled = True

            If _ParametersProvided = True Then Return MyBase.ReadStockLog(ProductCode)

        End Function

        Public Overrides Function ReadStockLog(ByRef Con As Connection, ByVal SkuNumber As String) As Boolean

            _ReadStockLogUseConnectionCalled = True

            If _ParametersProvided = True Then Return MyBase.ReadStockLog(Con, SkuNumber)

        End Function

#End Region

    End Class

#End Region

#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitch_TestScenerios()

        Dim StockLogOverloaded As IStockLogOverloadDecide

        ArrangeRequirementSwitchDependency(True)
        StockLogOverloaded = (New StockLogOverloadDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.StockLogOverloadDecideUseConnection", StockLogOverloaded.GetType.FullName, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        StockLogOverloaded = (New StockLogOverloadDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.StockLogOverloadDecideIgnoreConnection", StockLogOverloaded.GetType.FullName, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        StockLogOverloaded = (New StockLogOverloadDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.StockLogOverloadDecideIgnoreConnection", StockLogOverloaded.GetType.FullName, "Test Three: Return existing implementation")

    End Sub

#End Region

#Region "Unit Test - Connection"

    <TestMethod()> Public Sub OverloadedReadDecide_TestScenarios()

        Dim StockLog As TestStockLog
        Dim StockLogOverloaded As IStockLogOverloadDecide

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(True)
        StockLog = New TestStockLog(False)

        StockLogOverloaded = (New StockLogOverloadDecideFactory).GetImplementation
        StockLogOverloaded.ReadStockLog(Nothing, Nothing, CType(StockLog, IStockLog))

        Assert.IsTrue(StockLog.ReadStockLogUseConnectionCalled, "Test A: Overloaded read with connection used")
        Assert.IsFalse(StockLog.ReadStockLogIgnoreConnectionCalled, "Test B: Overloaded read with connection used")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(False)
        StockLog = New TestStockLog(False)

        StockLogOverloaded = (New StockLogOverloadDecideFactory).GetImplementation
        StockLogOverloaded.ReadStockLog(Nothing, Nothing, CType(StockLog, IStockLog))

        Assert.IsTrue(StockLog.ReadStockLogIgnoreConnectionCalled, "Test C: Overloaded read without connection used")
        Assert.IsFalse(StockLog.ReadStockLogUseConnectionCalled, "Test D: Overloaded read without connection used")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class
