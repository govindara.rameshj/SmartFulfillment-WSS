﻿<TestClass()> Public Class AllowedAdjustments_UnitTest

    Private testContextInstance As TestContext
    Private _StockAdjustment As IAdjustment
    Private _SACode As ISaCode = SACodeFactory.FactoryGet
    Private _Stock As IStock = StockFactory.FactoryGet
    Private _Con As Connection

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory Unit Test - AllowedAdjustment"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassAllowedAdjustment()

        Dim NewVersionOnly As IAllowedAdjustment
        Dim Factory As IBaseFactory(Of IAllowedAdjustment)

        Factory = New AllowedAdjustmentFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("StockAdjustments.AllowedAdjustmentNew", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - AllowedAdjustment"

    <TestMethod()> Public Sub AllowedAdjusments_SACODEFiftyEight_AdjCodeThree_HasAllowedAdjustmentCode_ReturnsTrue()

        Dim X As IAllowedAdjustment = New AllowedAdjustmentNew

        ArrangeStock("3")
        ArrangeSACode("58", "No Returns Policy", "1,3,5,7")
        Assert.IsTrue(X.IsAdjustmentAllowed(_SACode, _Stock, _StockAdjustment, _Con, "101000", CDate("28 Feb 2012")))

    End Sub

    <TestMethod()> Public Sub AllowedAdjusments_SACODEFiftyEight_AdjCodeTwo_HasAllowedAdjustmentCode_ReturnsFalse()

        Dim X As IAllowedAdjustment = New AllowedAdjustmentNew

        ArrangeStock("2")
        ArrangeSACode("58", "No Returns Policy", "1,3,5,7")
        Assert.IsFalse(X.IsAdjustmentAllowed(_SACode, _Stock, _StockAdjustment, _Con, "101000", CDate("28 Feb 2012")))

    End Sub

    <TestMethod()> Public Sub AllowedAdjusments_SACODEFiftyEight_AdjCodeEmpty_HasAllowedAdjustmentCode_ReturnsFalse()

        Dim X As IAllowedAdjustment = New AllowedAdjustmentNew

        ArrangeStock("")
        ArrangeSACode("58", "No Returns Policy", "1,3,5,7")
        Assert.IsFalse(X.IsAdjustmentAllowed(_SACode, _Stock, _StockAdjustment, _Con, "101000", CDate("28 Feb 2012")))

    End Sub

    <TestMethod()> Public Sub AllowedAdjusments_SACODEFiftyNine_AdjCodeThree_HasAllowedAdjustmentCode_ReturnsTrue()

        Dim X As IAllowedAdjustment = New AllowedAdjustmentNew

        ArrangeStock("3")
        ArrangeSACode("59", "HDC Mark-downs", "2,3,6,7")
        Assert.IsTrue(X.IsAdjustmentAllowed(_SACode, _Stock, _StockAdjustment, _Con, "101000", CDate("28 Feb 2012")))

    End Sub

    <TestMethod()> Public Sub AllowedAdjusments_SACODEFiftyNine_AdjCodeOne_HasAllowedAdjustmentCode_ReturnsFalse()

        Dim X As IAllowedAdjustment = New AllowedAdjustmentNew

        ArrangeStock("1")
        ArrangeSACode("59", "HDC Mark-downs", "2,3,6,7")
        Assert.IsFalse(X.IsAdjustmentAllowed(_SACode, _Stock, _StockAdjustment, _Con, "101000", CDate("28 Feb 2012")))

    End Sub

    <TestMethod()> Public Sub AllowedAdjusments_SACODEFiftyNine_AdjCodeEmpty_HasAllowedAdjustmentCode_ReturnsFalse()

        Dim X As IAllowedAdjustment = New AllowedAdjustmentNew

        ArrangeStock("")
        ArrangeSACode("59", "HDC Mark-downs", "2,3,6,7")
        Assert.IsFalse(X.IsAdjustmentAllowed(_SACode, _Stock, _StockAdjustment, _Con, "101000", CDate("28 Feb 2012")))

    End Sub

    <TestMethod()> Public Sub AllowedAdjusments_SACODETwo_AdjCodeEmpty_HasAllowedAdjustmentCode_ReturnsTrue()

        Dim X As IAllowedAdjustment = New AllowedAdjustmentNew

        ArrangeStock("")
        ArrangeSACode("02", "Normal Adjustment", "")
        Assert.IsTrue(X.IsAdjustmentAllowed(_SACode, _Stock, _StockAdjustment, _Con, "101000", CDate("28 Feb 2012")))

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub ArrangeSACode(ByVal Code As String, ByVal Description As String, ByVal AllowCodes As String)

        With _SACode
            .CodeNumber = Code
            .CodeSign = "S"
            .CodeType = "N"
            .Description = Description
            .AllowCodes = AllowCodes
            .IsAuthRequired = False
            .IsCommentable = True
            .IsIssueQuery = False
            .IsKnownTheft = False
            .IsMarkdown = False
            .IsWriteOff = False
            .IsStockLoss = True
            .IsReserved = False
            .IsPassRequired = False
            .SecurityLevel = "7"
            .MarkdownWriteoff = ""
        End With

    End Sub

    Private Sub ArrangeStock(ByVal AlllowAdjustmentCode As String)
        With _Stock
            .SKUN = "101000"
            .Department = "10"
            .Price_PRIC = CDec(9.99)
            .Cost = CDec(3.33)
            .AllowAdjustments_AADJ = AlllowAdjustmentCode
        End With

    End Sub

#End Region

End Class