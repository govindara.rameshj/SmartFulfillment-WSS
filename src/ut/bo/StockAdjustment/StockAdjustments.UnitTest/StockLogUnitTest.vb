﻿<TestClass()> Public Class StockLogUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestStockLog
        Inherits StockLog

#Region "Private Variables"

        Private _ParametersProvided As Boolean
        Private _ResetCalled As Boolean
        Private _SetConnectionCalled As Boolean
        Private _SetSkuNumberCalled As Boolean
        Private _CreateStockLogRepositoryCalled As Boolean
        Private _ExecuteGetStockLogCalled As Boolean
        Private _LoadMeCalled As Boolean

#End Region

#Region "Local Properties"

        Public ReadOnly Property ResetCalled() As Boolean
            Get
                Return _ResetCalled
            End Get
        End Property

        Public ReadOnly Property SetConnectionCalled() As Boolean
            Get
                Return _SetConnectionCalled
            End Get
        End Property

        Public ReadOnly Property SetSkuNumberCalled() As Boolean
            Get
                Return _SetSkuNumberCalled
            End Get
        End Property

        Public ReadOnly Property CreateStockLogRepositoryCalled() As Boolean
            Get
                Return _CreateStockLogRepositoryCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteGetStockLogCalled() As Boolean
            Get
                Return _ExecuteGetStockLogCalled
            End Get
        End Property

        Public ReadOnly Property LoadMeCalled() As Boolean
            Get
                Return _LoadMeCalled
            End Get
        End Property

#End Region

#Region "Constructor"

        Public Sub New(ByVal ParametersProvided As Boolean)

            _ParametersProvided = ParametersProvided

        End Sub

#End Region

#Region "Access Parent Variables"

        Public Function AccessInternalParentVariable_Connection() As IConnection

            Return MyBase._Connection

        End Function

        Public Sub ModifyInternalParentVariable_Connection(ByRef Connection As IConnection)

            MyBase._Connection = CType(Connection, Cts.Oasys.Data.Connection)

        End Sub

        Public Function AccessInternalParentVariable_DR() As DataRow

            Return MyBase._DR

        End Function

        Public Sub ModifyInternalParentVariable_DR(ByRef DR As DataRow)

            MyBase._DR = DR

        End Sub

        Public Function AccessInternalParentVariable_SkuNumber() As String

            Return MyBase._SkuNumber

        End Function

        Public Sub ModifyInternalParentVariable_SkuNumber(ByVal SkuNumber As String)

            MyBase._SkuNumber = SkuNumber

        End Sub

        Public Function AccessInternalParentVariable_Repository() As IStockLogRepository

            Return MyBase._Repository

        End Function

        Public Sub ModifyInternalParentVariable_Repository(ByRef Repository As IStockLogRepository)

            MyBase._Repository = Repository

        End Sub

#End Region

#Region "Overrides"

        Friend Overrides Sub Reset()

            _ResetCalled = True

            MyBase.Reset()

        End Sub

        Friend Overrides Sub SetConnection(ByRef Con As Connection)

            _SetConnectionCalled = True

            If _ParametersProvided = True Then MyBase.SetConnection(Con)

        End Sub

        Friend Overrides Sub SetSkuNumber(ByVal SkuNumber As String)

            _SetSkuNumberCalled = True

            If _ParametersProvided = True Then MyBase.SetSkuNumber(SkuNumber)

        End Sub

        Friend Overrides Sub CreateStockLogRepository()

            _CreateStockLogRepositoryCalled = True

            If _ParametersProvided = True Then MyBase.CreateStockLogRepository()

        End Sub

        Friend Overrides Sub ExecuteGetStockLog()

            _ExecuteGetStockLogCalled = True

            'do not execute actual command
            'If _ParametersProvided = True Then MyBase.ExecuteGetStockLog()
            If _ParametersProvided = True Then

                Dim DT As New DataTable

                DT.Columns.Add("StringField", System.Type.GetType("System.String"))
                DT.Rows.Add("ABC")

                MyBase._DR = DT.Rows(0)

            End If

        End Sub

        Friend Overrides Sub LoadMe()

            _LoadMeCalled = True

            If _ParametersProvided = True Then MyBase.LoadMe()

        End Sub

#End Region

    End Class

    Private Class TestConnection
        Inherits Connection

        Protected Overrides Sub SetDataProvider()

            MyBase.DataProviderField = Cts.Oasys.Data.DataProvider.Sql

        End Sub

        Protected Overrides Sub InitialiseConnection()

            MyBase.SqlConnectionField = New SqlConnection

        End Sub

    End Class

#End Region

#Region "Unit Test: ReadStockLog"

    <TestMethod()> Public Sub PrivateMethods_TestScenarios()

        Dim Repository As TestStockLog

        Repository = New TestStockLog(False)
        Repository.ReadStockLog(Nothing, Nothing)

        Assert.IsTrue(Repository.DataRowIsNothing, "Test: DataRow is nothing returns true")
        Repository.ModifyInternalParentVariable_DR(TestDataRow())
        Assert.IsFalse(Repository.DataRowIsNothing, "Test: DataRow is nothing returns false")

    End Sub

    <TestMethod()> Public Sub FunctionReadStockLog_TestScenarios()

        Dim StockLog As TestStockLog
        Dim ReturnValue As Boolean

        StockLog = New TestStockLog(False)

        StockLog.ModifyInternalParentVariable_Connection(New TestConnection)
        StockLog.ModifyInternalParentVariable_SkuNumber("123456")
        StockLog.ModifyInternalParentVariable_Repository(StockLogRepositoryFactory.FactoryGet)
        StockLog.ModifyInternalParentVariable_DR(TestDataRow())

        StockLog.ReadStockLog(Nothing, Nothing)

        Assert.IsNull(StockLog.AccessInternalParentVariable_Connection, "Test: Connection empty")
        Assert.IsNull(StockLog.AccessInternalParentVariable_SkuNumber, "Test: SkuNumber empty")
        Assert.IsNull(StockLog.AccessInternalParentVariable_Repository, "Test: Repository empty")
        Assert.IsNull(StockLog.AccessInternalParentVariable_DR, "Test: DataRow empty")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(StockLog.ResetCalled, "Test: Reset called")
        Assert.IsTrue(StockLog.SetConnectionCalled, "Test: SetConnection called")
        Assert.IsTrue(StockLog.SetSkuNumberCalled, "Test: SetSkuNumber called")
        Assert.IsTrue(StockLog.CreateStockLogRepositoryCalled, "Test: CreateStockLogRepository called")
        Assert.IsTrue(StockLog.ExecuteGetStockLogCalled, "Test: ExecuteGetStockLog called")
        Assert.IsFalse(StockLog.LoadMeCalled, "Test: LoadMe not called; empty data row")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        StockLog = New TestStockLog(True)
        ReturnValue = StockLog.ReadStockLog(New TestConnection, "123456")

        Assert.IsNotNull(StockLog.AccessInternalParentVariable_Connection, "Test: Connection stored correctly in internal variable")
        Assert.AreEqual("123456", StockLog.AccessInternalParentVariable_SkuNumber, "Test: SkuNumber stored correctly in internal variable")
        Assert.IsNotNull(StockLog.AccessInternalParentVariable_Repository, "Test: StockLogRepository stored correctly in internal variable")
        Assert.IsNotNull(StockLog.AccessInternalParentVariable_DR, "Test: DataRow stored correctly in internal variable")

        Assert.IsTrue(StockLog.LoadMeCalled, "Test: LoadMe called; non empty data row")

        Assert.IsTrue(ReturnValue, "Test: Return value true")

    End Sub

#End Region

#Region "Unit Test: SetType Test Scenarios"

    <TestMethod()> Public Sub FunctionSetType_TestScenarios()

        Dim StockLog As StockLog
        Dim AdjustmentCode As SACode.SaCode

        StockLog = New StockLog
        AdjustmentCode = New SACode.SaCode

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(True)

        AdjustmentCode.CodeNumber = "07"
        AdjustmentCode.MarkdownWriteoff = ""
        AdjustmentCode.IsIssueQuery = False

        StockLog.SetType(-1, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("31", StockLog.StockLogType_TYPE, "Test: Non Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: False | Add zero to type")

        StockLog.SetType(0, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("31", StockLog.StockLogType_TYPE, "Test: Non Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: False | Add zero to type")

        StockLog.SetType(999, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("32", StockLog.StockLogType_TYPE, "Test: Non Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: False | Add one to type")

        AdjustmentCode.IsIssueQuery = True

        StockLog.SetType(-1, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("35", StockLog.StockLogType_TYPE, "Test: Non Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: True | Add four to type")

        StockLog.SetType(0, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("35", StockLog.StockLogType_TYPE, "Test: Non Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: True | Add four to type")

        StockLog.SetType(999, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("36", StockLog.StockLogType_TYPE, "Test: Non Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: True | Add five to type")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = "W"
        AdjustmentCode.IsIssueQuery = Nothing

        StockLog.SetType(-1, AdjustmentCode, Nothing, False)
        Assert.AreEqual("68", StockLog.StockLogType_TYPE, "Test: Non Markdown | Write Off | IsReversed: False | IsIssueQuery: Ignore | Quantity: -1 | Add zero to type")

        StockLog.SetType(0, AdjustmentCode, Nothing, False)
        Assert.AreEqual("68", StockLog.StockLogType_TYPE, "Test: Non Markdown | Write Off | IsReversed: False | IsIssueQuery: Ignore | Quantity: 0  | Add zero to type")

        StockLog.SetType(9, AdjustmentCode, Nothing, False)
        Assert.AreEqual("68", StockLog.StockLogType_TYPE, "Test: Non Markdown | Write Off | IsReversed: False | IsIssueQuery: Ignore | Quantity: 9  | Add one to type")

        StockLog.SetType(-1, AdjustmentCode, Nothing, True)
        Assert.AreEqual("69", StockLog.StockLogType_TYPE, "Test: Non Markdown | Write Off | IsReversed: True  | IsIssueQuery: Ignore | Quantity: -1 | Add zero to type")

        StockLog.SetType(0, AdjustmentCode, Nothing, True)
        Assert.AreEqual("69", StockLog.StockLogType_TYPE, "Test: Non Markdown | Write Off | IsReversed: True  | IsIssueQuery: Ignore | Quantity: 0  | Add zero to type")

        StockLog.SetType(9, AdjustmentCode, Nothing, True)
        Assert.AreEqual("69", StockLog.StockLogType_TYPE, "Test: Non Markdown | Write Off | IsReversed: True  | IsIssueQuery: Ignore | Quantity: 9  | Add one to type")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = "M"
        AdjustmentCode.IsIssueQuery = Nothing

        StockLog.SetType(-1, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("66", StockLog.StockLogType_TYPE, "Test: Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: Ignore | Quantity: -1 | Add zero to type")

        StockLog.SetType(0, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("66", StockLog.StockLogType_TYPE, "Test: Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: Ignore | Quantity: 0  | Add zero to type")

        StockLog.SetType(9, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual("67", StockLog.StockLogType_TYPE, "Test: Markdown | Non Write Off | IsReversed: Ignore | IsIssueQuery: Ignore | Quantity: 9  | Add one to type")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = ""
        AdjustmentCode.IsIssueQuery = Nothing

        Assert.IsFalse(StockLog.SetType(0, AdjustmentCode, Nothing, Nothing), "Test: Function SetType always return false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(False)

        AdjustmentCode.MarkdownWriteoff = ""
        StockLog.SetType(0, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual(Space(1), AdjustmentCode.MarkdownWriteoff, "Test: Adjustment code markdown/writeoff altered to single character blank string")

        ArrangeRequirementSwitchDependency(True)
        AdjustmentCode.MarkdownWriteoff = ""
        StockLog.SetType(0, AdjustmentCode, Nothing, Nothing)
        Assert.AreEqual(String.Empty, AdjustmentCode.MarkdownWriteoff, "Test: Adjustment code markdown/writeoff unaltered")

    End Sub

    <TestMethod()> Public Sub SetMarkDown_TestScenarios()

        Dim StockLog As StockLog
        Dim AdjustmentCode As SACode.SaCode

        StockLog = New StockLog
        AdjustmentCode = New SACode.SaCode

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = ""
        StockLog.StockLogType_TYPE = "5"

        StockLog.SetTypeMarkDown(AdjustmentCode, -1)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | Quantity: -1 | Type unaltered")

        StockLog.SetTypeMarkDown(AdjustmentCode, 0)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | Quantity: 0  | Type unaltered")

        StockLog.SetTypeMarkDown(AdjustmentCode, 9)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | Quantity: 9  | Type unaltered")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = "W"
        StockLog.StockLogType_TYPE = "5"

        StockLog.SetTypeMarkDown(AdjustmentCode, -1)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Write Off | Quantity: -1 | Type unaltered")

        StockLog.SetTypeMarkDown(AdjustmentCode, 0)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Write Off | Quantity: 0  | Type unaltered")

        StockLog.SetTypeMarkDown(AdjustmentCode, 9)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Write Off | Quantity: 9  | Type unaltered")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = "M"

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeMarkDown(AdjustmentCode, -1)
        Assert.AreEqual("66", StockLog.StockLogType_TYPE, "Test: Markdown | Quantity: -1 | Type: 66")

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeMarkDown(AdjustmentCode, 0)
        Assert.AreEqual("66", StockLog.StockLogType_TYPE, "Test: Markdown | Quantity: 0  | Type: 66")

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeMarkDown(AdjustmentCode, 9)
        Assert.AreEqual("67", StockLog.StockLogType_TYPE, "Test: Markdown | Quantity: 9  | Type: 67")

    End Sub

    <TestMethod()> Public Sub SetWriteOff_TestScenarios()

        Dim StockLog As StockLog
        Dim AdjustmentCode As SACode.SaCode

        StockLog = New StockLog
        AdjustmentCode = New SACode.SaCode

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = ""
        StockLog.StockLogType_TYPE = "5"

        StockLog.SetTypeWriteOff(AdjustmentCode, Nothing)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsReversed: Nothing | Type unaltered")

        StockLog.SetTypeWriteOff(AdjustmentCode, False)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsReversed: False   | Type unaltered")

        StockLog.SetTypeWriteOff(AdjustmentCode, True)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsReversed: True    | Type unaltered")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = "M"
        StockLog.StockLogType_TYPE = "5"

        StockLog.SetTypeWriteOff(AdjustmentCode, Nothing)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Markdown | IsReversed: Nothing | Type unaltered")

        StockLog.SetTypeWriteOff(AdjustmentCode, False)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Markdown | IsReversed: False   | Type unaltered")

        StockLog.SetTypeWriteOff(AdjustmentCode, True)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Markdown | IsReversed: True    | Type unaltered")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = "W"
        StockLog.StockLogType_TYPE = "5"

        StockLog.SetTypeWriteOff(AdjustmentCode, False)
        Assert.AreEqual("68", StockLog.StockLogType_TYPE, "Test: Write Off | IsReversed: False | Type: 68")

        StockLog.SetTypeWriteOff(AdjustmentCode, True)
        Assert.AreEqual("69", StockLog.StockLogType_TYPE, "Test: Write Off | IsReversed: True  | Type: 69")

    End Sub

    <TestMethod()> Public Sub SetTypeRest_TestScenarios()

        Dim StockLog As StockLog
        Dim AdjustmentCode As SACode.SaCode

        StockLog = New StockLog
        AdjustmentCode = New SACode.SaCode

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = "W"
        StockLog.StockLogType_TYPE = "5"

        StockLog.SetTypeRest(AdjustmentCode, -1)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Write Off | Quantity: -1 | Type unaltered")

        StockLog.SetTypeRest(AdjustmentCode, 0)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Write Off | Quantity: 0  | Type unaltered")

        StockLog.SetTypeRest(AdjustmentCode, 9)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Write Off | Quantity: 9  | Type unaltered")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = "M"
        StockLog.StockLogType_TYPE = "5"

        StockLog.SetTypeRest(AdjustmentCode, -1)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Markdown | Quantity: -1 | Type unaltered")

        StockLog.SetTypeRest(AdjustmentCode, 0)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Markdown | Quantity: 0  | Type unaltered")

        StockLog.SetTypeRest(AdjustmentCode, 9)
        Assert.AreEqual("5", StockLog.StockLogType_TYPE, "Test: Markdown | Quantity: 9  | Type unaltered")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdjustmentCode.MarkdownWriteoff = ""
        AdjustmentCode.IsIssueQuery = Nothing

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, -1)
        Assert.AreEqual("31", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: Ignore | Quantity: -1 | Type: 31")

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, 0)
        Assert.AreEqual("31", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: Ignore | Quantity: 0  | Type: 31")

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, 9)
        Assert.AreEqual("32", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: Ignore | Quantity: 9  | Type: 32")


        AdjustmentCode.IsIssueQuery = False

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, -1)
        Assert.AreEqual("31", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: False | Quantity: -1 | Type: 31")

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, 0)
        Assert.AreEqual("31", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: False | Quantity: 0  | Type: 31")

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, 9)
        Assert.AreEqual("32", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: False | Quantity: 9  | Type: 32")


        AdjustmentCode.IsIssueQuery = True

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, -1)
        Assert.AreEqual("35", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: True | Quantity: -1 | Type: 35")

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, 0)
        Assert.AreEqual("35", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: True | Quantity: 0  | Type: 35")

        StockLog.StockLogType_TYPE = "5"
        StockLog.SetTypeRest(AdjustmentCode, 9)
        Assert.AreEqual("36", StockLog.StockLogType_TYPE, "Test: Non Markdown Or Write Off | IsIssueQuery: True | Quantity: 9  | Type: 36")

    End Sub

#End Region

#Region "Unit Test: Depreciated"

    '<TestMethod()> Public Sub SetType_SACodeTypeIs31QtyEqualZero_ReturnsTrue()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .CodeNumber = "07"
    '    End With

    '    StockLog.SetType(0, SACode, Nothing, False)
    '    Assert.AreEqual("31", StockLog.StockLogType_TYPE)

    'End Sub

    '<TestMethod()> Public Sub SetType_SACodeTypeIs31QtyEqualOne_ReturnsFalse()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .CodeNumber = "07"
    '    End With

    '    StockLog.SetType(1, SACode, Nothing, False)
    '    Assert.AreNotEqual("31", StockLog.StockLogType_TYPE)

    'End Sub

    '<TestMethod()> Public Sub SetType_SACodeTypeIs32QtyEqualOne_ReturnsTrue()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .IsIssueQuery = False
    '        .MarkdownWriteoff = String.Empty
    '    End With

    '    StockLog.SetType(1, SACode, Nothing, False)
    '    Assert.AreEqual("32", StockLog.StockLogType_TYPE)

    'End Sub

    '<TestMethod()> Public Sub SetType_SACodeTypeIs31QtyMinus1_ReturnsTrue()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .CodeNumber = "07"
    '    End With

    '    StockLog.SetType(-1, SACode, Nothing, False)
    '    Assert.AreEqual("31", StockLog.StockLogType_TYPE)

    'End Sub

    '<TestMethod()> Public Sub Validate_SACodeTypeIs32_ReturnsTrue()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .CodeNumber = "05"
    '        .CodeSign = "S"
    '        .CodeType = "N" 'normal
    '        .SecurityLevel = 9
    '    End With

    '    StockLog.SetType(1, SACode, Nothing, False)

    '    Assert.AreEqual("32", StockLog.StockLogType_TYPE)

    'End Sub

    '<TestMethod()> Public Sub Validate_SACodeTypeIs66_ReturnsTrue()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .CodeNumber = "53"
    '        .MarkdownWriteoff = "M"
    '    End With

    '    StockLog.SetType(-1, SACode, Nothing, False)

    '    Assert.AreEqual("66", StockLog.StockLogType_TYPE)

    'End Sub

    '<TestMethod()> Public Sub Validate_SACodeTypeIs67_ReturnsTrue()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .CodeNumber = "54"
    '        .MarkdownWriteoff = "M"
    '    End With

    '    StockLog.SetType(1, SACode, Nothing, True)

    '    Assert.AreEqual("67", StockLog.StockLogType_TYPE)

    'End Sub

    '<TestMethod()> Public Sub Validate_SACodeTypeIs68_ReturnsTrue()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .CodeNumber = "53"
    '        .MarkdownWriteoff = "W"
    '    End With

    '    StockLog.SetType(0, SACode, Nothing, False)

    '    Assert.AreEqual("68", StockLog.StockLogType_TYPE)

    'End Sub

    '<TestMethod()> Public Sub Validate_SACodeTypeIs69_ReturnsTrue()

    '    Dim StockLog As StockAdjustments.StockLog = StockLogFactory.FactoryGet
    '    Dim SACode As New SACode.SaCode

    '    With SACode
    '        .CodeNumber = "54"
    '        .MarkdownWriteoff = "W"
    '    End With

    '    StockLog.SetType(1, SACode, Nothing, True)

    '    Assert.AreEqual("69", StockLog.StockLogType_TYPE)

    'End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Function TestDataRow() As DataRow

        Dim DT As DataTable

        DT = New DataTable
        DT.Columns.Add("StringField", System.Type.GetType("System.String"))
        DT.Rows.Add("ABC")

        Return DT.Rows(0)

    End Function

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class