﻿<TestClass()> Public Class Adjustment_Tests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub Procedure_MakeEqual_TestScenarios()

        Dim Source As New Adjustment
        Dim Target As New Adjustment

        PopulateSourceAdjustment(Source)

        Target.MakeEqual(Source)

        Assert.AreEqual(Now.Date, Target.AdjustmentDate, "Test: AdjustmentDate")
        Assert.AreEqual("12", Target.Code, "Test: Code")
        Assert.AreEqual("123456", Target.SKUN, "Test: SKUN")
        Assert.AreEqual("34", Target.SEQN, "Test: SEQN")
        Assert.AreEqual(9876, Target.AmendId, "Test: AmendId")
        Assert.AreEqual("ABCDE", Target.EmployeeInitials, "Test: EmployeeInitials")
        Assert.AreEqual("ZY", Target.Department, "Test: Department")
        Assert.AreEqual(CType(456, Decimal), Target.StartingStock, "Test: StartingStock")
        Assert.AreEqual(CType(789, Decimal), Target.Quantity, "Test: Quantity")
        Assert.AreEqual(CType(1234.56, Decimal), Target.Price, "Test: Price")
        Assert.AreEqual(CType(5678.9, Decimal), Target.Cost, "Test: Cost")
        Assert.AreEqual(True, Target.IsSentToHO, "Test: IsSentToHO")
        Assert.AreEqual("N", Target.AdjustType, "Test: AdjustType")
        Assert.AreEqual("comment", Target.Comment, "Test: Comment")
        Assert.AreEqual("918273", Target.DeliveryNumber, "Test: DeliveryNumber")
        Assert.AreEqual("X", Target.ReversalCode, "Test: ReversalCode")
        Assert.AreEqual("W", Target.MarkDownOrWriteOff, "Test: MarkDownOrWriteOff")
        Assert.AreEqual("ZZZ", Target.WriteOffAuthority, "Test: WriteOffAuthority")
        Assert.AreEqual(Now.Date.AddDays(5), Target.WriteOffAuthorityDate, "Test: WriteOffAuthorityDate")
        Assert.AreEqual(987, Target.PeriodId, "Test: PeriodId")
        Assert.AreEqual("987654", Target.TransferSku_TSKU, "Test: TransferSku_TSKU")
        Assert.AreEqual(CType(9182, Decimal), Target.TransferValue, "Test: TransferValue")
        Assert.AreEqual(CType(7364, Decimal), Target.TransferStart, "Test: TransferStart")
        Assert.AreEqual(CType(5574, Decimal), Target.TransferPrice, "Test: TransferPrice")
        Assert.AreEqual(True, Target.IsReversed, "Test: IsReversed")
        Assert.AreEqual("Y", Target.RTI, "Test: RTI")
        Assert.AreEqual(True, Target.ExistsInDatabase, "Test: ExistsInDatabase")



        'SOLVES THE PROBLEM!!!!!!!!!!!!!!!
        Assert.AreNotEqual(CType(3456, Decimal), Target.EndingStock, "Test: EndingStock")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub PopulateSourceAdjustment(ByRef Value As Adjustment)

        With Value

            .AdjustmentDate = Now.Date
            .Code = "12"
            .SKUN = "123456"
            .SEQN = "34"
            .AmendId = 9876
            .EmployeeInitials = "ABCDE"
            .Department = "ZY"
            .StartingStock = 456
            .Quantity = 789
            .Price = CDec(1234.56)
            .Cost = CDec(5678.9)
            .IsSentToHO = True
            .AdjustType = "N"
            .Comment = "comment"
            .DeliveryNumber = "918273"
            .ReversalCode = "X"
            .MarkDownOrWriteOff = "W"
            .WriteOffAuthority = "ZZZ"
            .WriteOffAuthorityDate = Now.Date.AddDays(5)
            .PeriodId = 987
            .TransferSku_TSKU = "987654"
            .TransferValue = 9182
            .TransferStart = 7364
            .TransferPrice = 5574
            .IsReversed = True
            .RTI = "Y"
            .ExistsInDatabase = True
            .EndingStock = 3456

        End With

    End Sub

#End Region

#Region "??????????"

    'Private _StockAdjustment As IAdjustment
    'Private _SACode As ISaCode
    'Private _Stock As IStock
    'Private _StockLog As IStockLog
    'Private _Con As Connection
    '
    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_Price_ReturnsTrue()
    '    'Dim expectedResult As Decimal = 9.99
    '    'Dim actualResult As Decimal = 0.0

    '    ' '' arrange
    '    ' '' done on new

    '    ' '' act
    '    'actualResult = _StockAdjustment.Price

    '    ' '' assert
    '    'Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_Cost_ReturnsTrue()
    '    'Dim expectedResult As Decimal = 3.33
    '    'Dim actualResult As Decimal = 0.0

    '    ' '' arrange
    '    ' '' done on new

    '    ' '' act
    '    'actualResult = _StockAdjustment.Cost

    '    ' '' assert
    '    'Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_Date_ReturnsTrue()
    '    Dim expectedResult As Date = Now.ToString("yyyy-MM-dd")
    '    'Dim actualResult As Date = Nothing

    '    ' '' arrange
    '    ' '' done on new

    '    ' '' act
    '    'actualResult = _StockAdjustment.AdjustmentDate_DATE1

    '    ' '' assert
    '    'Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_SKUN_ReturnsTrue()
    '    Dim expectedResult As String = "101000"
    '    Dim actualResult As String = String.Empty

    '    '' arrange
    '    '' Was done on new

    '    '' act
    '    actualResult = _StockAdjustment.SKUN

    '    '' assert
    '    Assert.AreEqual(expectedResult, actualResult)

    'End Sub


    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_SEQN_ReturnsTrue()
    '    Dim expectedResult As String = "00"
    '    Dim actualResult As String = String.Empty

    '    '' arrange
    '    '' Was done on new

    '    '' act
    '    actualResult = _StockAdjustment.SEQN

    '    '' assert
    '    Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_StartingStock_ReturnsTrue()
    '    Dim expectedResult As Decimal = 340.0
    '    Dim actualResult As Decimal = 0.0

    '    '' arrange
    '    '' Was done on new

    '    '' act
    '    actualResult = _StockAdjustment.StartingStock_SSTK '.StartingStock_SSTK

    '    '' assert
    '    Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_Qty_ReturnsTrue()
    '    Dim expectedResult As Integer = 2
    '    Dim actualResult As Integer = 0

    '    '' arrange
    '    '' Was done on new

    '    '' act
    '    actualResult = _StockAdjustment.Quantity_QUAN '_QUAN

    '    '' assert
    '    Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_Type_ReturnsTrue()
    '    Dim expectedResult As String = "2"
    '    Dim actualResult As String = String.Empty

    '    '' arrange
    '    '' Was done on new

    '    '' act
    '    actualResult = _StockAdjustment.Quantity_QUAN '_QUAN

    '    '' assert
    '    Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_Comment_ReturnsTrue()
    '    Dim expectedResult As String = "TEST"
    '    Dim actualResult As String = String.Empty

    '    '' arrange
    '    '' Was done on new

    '    '' act
    '    actualResult = _StockAdjustment.Comment_INFO '_INFO

    '    '' assert
    '    Assert.AreEqual(expectedResult, actualResult)

    'End Sub


    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_Employee_ReturnsTrue()
    '    Dim expectedResult As String = "MO'C"
    '    Dim actualResult As String = String.Empty

    '    '' arrange
    '    '' Was done on new

    '    '' act
    '    actualResult = _StockAdjustment.EmployeeInitials_INIT '_INIT

    '    '' assert
    '    Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    '<TestMethod()> Public Sub SetType_SaveAdjustment_SetsStockAdj_Department_ReturnsTrue()
    '    Dim expectedResult As String = "10"
    '    Dim actualResult As String = String.Empty

    '    '' arrange
    '    '' Was done on new

    '    '' act
    '    actualResult = _StockAdjustment.Department

    '    '' assert
    '    Assert.AreEqual(expectedResult, actualResult)

    'End Sub

    'Public Sub New()

    '    _SACode = SACodeFactory.FactoryGet
    '    With _SACode
    '        .CodeNumber = "02"
    '        .CodeSign = "S"
    '        .CodeType = "N"
    '        .Description = "Normal Adjustment"
    '        .IsAuthRequired = False
    '        .IsCommentable = True
    '        .IsIssueQuery = False
    '        .IsKnownTheft = False
    '        .IsMarkdown = False
    '        .IsWriteOff = False
    '        .IsStockLoss = True
    '        .IsReserved = False
    '        .IsPassRequired = False
    '        .SecurityLevel = "7"
    '        .MarkdownWriteoff = ""
    '    End With

    '    _Stock = StockFactory.FactoryGet
    '    With _Stock
    '        .Department = "10"
    '        .Price_PRIC = 9.99
    '        .Cost = 3.33
    '    End With

    '    _StockLog = StockLogFactory.FactoryGet
    '    With _StockLog
    '        .EmployeeId_EEID = "001"
    '        .EndingMarkDown_EMDN = -2
    '        .EndingWriteOff_EWTF = -44
    '        .IsSentToHO_ICOM = True
    '        .SKUN = "101000"
    '        .StartingMarkDown_SMDN = -1
    '        .StartingWriteOff_SWTF = -44
    '        .StockLogType_TYPE = "32"
    '        .StockLogTime_TIME = "010256"
    '        .StartingStock_SSTK = 341
    '        .EndingStock_ESTK = 340
    '        .EndingPrice_EPRI = 9.99
    '        .StartingPrice_PRIC = 9.99
    '        .StockLogDate_DATE1 = "2011-07-07"
    '    End With

    '    ''Dim Adjustment As New StubAdjustment
    '    ''AdjustmentFactory.FactorySet(Adjustment)
    '    ''_StockAdjustment = AdjustmentFactory.FactoryGet
    '    ''_StockAdjustment.SaveAdjustment(_Con, 2, Now, "101000", "", "TEST", _SACode, _Stock, _StockLog, 1)

    '    'Dim mock As MockRepository = New MockRepository
    '    'Dim SimulatedService As IAdjustment = MockRepository.GenerateMock(_StockAdjustment, Nothing)
    '    ''Using mock As New MockRepository
    '    'Using mock.Record()
    '    '    ' We Expect <.....> ?
    '    '    SimulatedService.SaveAdjustment(_Con, 2, Now, "101000", "", "TEST", _SACode, _Stock, _StockLog, 1)
    '    'End Using

    'End Sub

#End Region

End Class