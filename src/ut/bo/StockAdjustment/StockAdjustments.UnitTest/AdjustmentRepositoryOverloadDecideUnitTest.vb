﻿<TestClass()> Public Class AdjustmentRepositoryOverloadDecideUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestAdjustmentRepository
        Inherits AdjustmentRepository

#Region "Private Variables"

        Private _ParametersProvided As Boolean
        Private _IgnoreConnectionCalled As Boolean
        Private _UseConnectionCalled As Boolean

#End Region

#Region "Local Properties"

        Public ReadOnly Property IgnoreConnectionCalled() As Boolean
            Get
                Return _IgnoreConnectionCalled
            End Get
        End Property

        Public ReadOnly Property UseConnectionCalled() As Boolean
            Get
                Return _UseConnectionCalled
            End Get
        End Property

#End Region

#Region "Constructor"

        Public Sub New(ByVal ParametersProvided As Boolean)

            _ParametersProvided = ParametersProvided

        End Sub

#End Region

#Region "Overrides"

        Public Overrides Function GetNextSequenceNumberForSkunCodeDate(ByVal Skun As String, ByVal Code As String, ByVal AdjDate As Date) As String

            _IgnoreConnectionCalled = True

            GetNextSequenceNumberForSkunCodeDate = String.Empty
            If _ParametersProvided = True Then Return MyBase.GetNextSequenceNumberForSkunCodeDate(Skun, Code, AdjDate)

        End Function


        Public Overrides Function GetNextSequenceNumberForSkunCodeDate(ByRef Con As Connection, ByVal SkuNumber As String, ByVal AdjustmentCode As String, ByVal AdjustmentDate As Date) As String

            _UseConnectionCalled = True

            GetNextSequenceNumberForSkunCodeDate = String.Empty
            If _ParametersProvided = True Then Return MyBase.GetNextSequenceNumberForSkunCodeDate(Con, SkuNumber, AdjustmentCode, AdjustmentDate)

        End Function

#End Region

    End Class

#End Region

#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitch_TestScenerios()

        Dim AdjustmentRepositoryOverload As IAdjustmentRepositoryOverloadDecide

        ArrangeRequirementSwitchDependency(True)
        AdjustmentRepositoryOverload = (New AdjustmentRepositoryOverloadDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.AdjustmentRepositoryOverloadDecideUseConnection", AdjustmentRepositoryOverload.GetType.FullName, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        AdjustmentRepositoryOverload = (New AdjustmentRepositoryOverloadDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.AdjustmentRepositoryOverloadDecideIgnoreConnection", AdjustmentRepositoryOverload.GetType.FullName, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        AdjustmentRepositoryOverload = (New AdjustmentRepositoryOverloadDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.AdjustmentRepositoryOverloadDecideIgnoreConnection", AdjustmentRepositoryOverload.GetType.FullName, "Test Three: Return existing implementation")

    End Sub

#End Region

#Region "Unit Test - Connection"

    <TestMethod()> Public Sub OverloadedReadDecide_TestScenarios()

        Dim AdjustmentRepository As TestAdjustmentRepository
        Dim OverloadDecide As IAdjustmentRepositoryOverloadDecide


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(True)
        AdjustmentRepository = New TestAdjustmentRepository(False)

        OverloadDecide = (New AdjustmentRepositoryOverloadDecideFactory).GetImplementation
        OverloadDecide.GetNextSequenceNumberForSkunCodeDate(CType(AdjustmentRepository, IAdjustmentRepository), Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(AdjustmentRepository.UseConnectionCalled, "Test A: Overloaded read with connection used")
        Assert.IsFalse(AdjustmentRepository.IgnoreConnectionCalled, "Test B: Overloaded read with connection used")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(False)
        AdjustmentRepository = New TestAdjustmentRepository(False)

        OverloadDecide = (New AdjustmentRepositoryOverloadDecideFactory).GetImplementation
        OverloadDecide.GetNextSequenceNumberForSkunCodeDate(CType(AdjustmentRepository, IAdjustmentRepository), Nothing, Nothing, Nothing, Nothing)

        Assert.IsTrue(AdjustmentRepository.IgnoreConnectionCalled, "Test C: Overloaded read without connection used")
        Assert.IsFalse(AdjustmentRepository.UseConnectionCalled, "Test D: Overloaded read without connection used")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class