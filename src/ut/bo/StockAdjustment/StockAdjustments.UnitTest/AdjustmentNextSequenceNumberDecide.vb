﻿<TestClass()> Public Class AdjustmentNextSequenceNumberDecide

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestAdjustment
        Inherits Adjustment

#Region "Private Variables"

        Private _ParametersProvided As Boolean
        Private _GenerateNextSequenceNumber As Boolean

#End Region

#Region "Local Properties"

        Public ReadOnly Property GenerateNextSequenceNumber() As Boolean
            Get
                Return _GenerateNextSequenceNumber
            End Get
        End Property

#End Region

#Region "Constructor"

        Public Sub New(ByVal ParametersProvided As Boolean)

            _ParametersProvided = ParametersProvided

        End Sub

#End Region

#Region "Overrides"

        Public Overrides Function SaveAdjustment(ByRef Con As Connection, _
                                                         ByVal AdjQty As Integer, _
                                                         ByVal AdjDate As Date, _
                                                         ByVal SkuNumber As String, _
                                                         ByVal DRLNumber As String, _
                                                         ByVal Comment As String, _
                                                         ByVal SACode As ISaCode, _
                                                         ByVal Stock As IStock, _
                                                         ByVal StockLog As IStockLog, _
                                                         ByVal EmployeeId As Integer, _
                                                         Optional ByVal TransferSku As String = "", _
                                                         Optional ByVal TransferValue As Decimal = 0, _
                                                         Optional ByVal TransferPrice As Decimal = 0, _
                                                         Optional ByVal TransferStart As Decimal = 0, _
                                                         Optional ByVal PicCountAcceptance As Boolean = False, _
                                                         Optional ByVal PicCountAcceptanceCode53 As Boolean = False) As Boolean


            _GenerateNextSequenceNumber = PicCountAcceptance

            If _ParametersProvided = True Then Return MyBase.SaveAdjustment(Con, _
                                                                            AdjQty, _
                                                                            AdjDate, _
                                                                            SkuNumber, _
                                                                            DRLNumber, _
                                                                            Comment, _
                                                                            SACode, _
                                                                            Stock, _
                                                                            StockLog, _
                                                                            EmployeeId, _
                                                                            TransferSku, _
                                                                            TransferValue, _
                                                                            TransferPrice, _
                                                                            TransferStart, _
                                                                            PicCountAcceptance, _
                                                                            PicCountAcceptanceCode53)

        End Function

#End Region

    End Class

#End Region

#Region "Unit Test - Requirement Switch"

    <TestMethod()> Public Sub RequirementSwitch_TestScenerios()

        Dim AdjustmentRepositoryOverload As IAdjustmentNextSequenceNumberDecide

        ArrangeRequirementSwitchDependency(True)
        AdjustmentRepositoryOverload = (New AdjustmentNextSequenceNumberDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.AdjustmentNextSequenceNumberGenerate", AdjustmentRepositoryOverload.GetType.FullName, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        AdjustmentRepositoryOverload = (New AdjustmentNextSequenceNumberDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.AdjustmentNextSequenceNumberNotGenerate", AdjustmentRepositoryOverload.GetType.FullName, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        AdjustmentRepositoryOverload = (New AdjustmentNextSequenceNumberDecideFactory).GetImplementation
        Assert.AreEqual("StockAdjustments.AdjustmentNextSequenceNumberNotGenerate", AdjustmentRepositoryOverload.GetType.FullName, "Test Three: Return existing implementation")

    End Sub

#End Region

#Region "Unit Test - Connection"

    <TestMethod()> Public Sub SaveAdjustment_GenerateNextSequenceNo_TestScenarios()

        Dim Adjustment As TestAdjustment
        Dim NextSequenceNumberDecide As IAdjustmentNextSequenceNumberDecide


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(True)
        Adjustment = New TestAdjustment(False)

        NextSequenceNumberDecide = (New AdjustmentNextSequenceNumberDecideFactory).GetImplementation
        NextSequenceNumberDecide.SaveAdjustment(CType(Adjustment, IAdjustment), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, , , , , True)
        Assert.IsTrue(Adjustment.GenerateNextSequenceNumber, "Test: Generate next sequence no on save adjustment")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ArrangeRequirementSwitchDependency(False)
        Adjustment = New TestAdjustment(False)

        NextSequenceNumberDecide = (New AdjustmentNextSequenceNumberDecideFactory).GetImplementation
        NextSequenceNumberDecide.SaveAdjustment(CType(Adjustment, IAdjustment), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, , , , , True)

        Assert.IsFalse(Adjustment.GenerateNextSequenceNumber, "Test: Not generate next sequence no on save adjustment")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class
