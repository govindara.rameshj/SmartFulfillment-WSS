﻿<TestClass()> Public Class AdjustmentsUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Tests"

    '<TestMethod()> Public Sub FunctionHighestSequenceNo_TestScenerios()

    'Dim StockAdjustments As New Adjustments
    'Dim DT As DataTable

    'DT = StockAdjustmentsConfigureStructure()

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'StubStockAdjustmentsRepository(DT)
    'StockAdjustments.ReadAdjustments(Nothing, Nothing, Nothing)

    'Assert.AreEqual(0, StockAdjustments.HighestSequenceNo, "Test One: No adjustments exist")

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'StockAdjustmentsAddRow(DT, Now, "04", "100807", "00", 0, "SC", "10", 39, 8, CType(5.99, Decimal), 0, True, "N", "Comment   ", "914809", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, True)
    'StubStockAdjustmentsRepository(DT)
    'StockAdjustments.ReadAdjustments(Nothing, Nothing, Nothing)

    'Assert.AreEqual(0, StockAdjustments.HighestSequenceNo, "Test Two: One adjustment; Highest SEQN = 00")

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'StockAdjustmentsAddRow(DT, Now, "04", "100807", "01", 0, "SC", "10", 47, 2, CType(5.99, Decimal), 0, True, "N", "Comment   ", "914810", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
    'StubStockAdjustmentsRepository(DT)
    'StockAdjustments.ReadAdjustments(Nothing, Nothing, Nothing)

    'Assert.AreEqual(1, StockAdjustments.HighestSequenceNo, "Test Three: Two adjustments; Highest SEQN = 01")

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'StockAdjustmentsAddRow(DT, Now, "04", "100807", "06", 0, "SC", "10", 85, 10, CType(5.99, Decimal), 0, False, "N", "Comment ", "914813", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
    'StockAdjustmentsAddRow(DT, Now, "04", "100807", "04", 0, "SC", "10", 80, 5, CType(5.99, Decimal), 0, False, "N", "Comment  ", "914809", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
    'StockAdjustmentsAddRow(DT, Now, "04", "100807", "07", 0, "SC", "10", 114, -4, CType(5.99, Decimal), 0, False, "N", "Comment", "914815", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
    'StockAdjustmentsAddRow(DT, Now, "04", "100807", "02", 0, "SC", "10", 108, -8, CType(5.99, Decimal), 0, False, "N", "Comment", "914811", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
    'StubStockAdjustmentsRepository(DT)
    'StockAdjustments.ReadAdjustments(Nothing, Nothing, Nothing)

    'Assert.AreEqual(7, StockAdjustments.HighestSequenceNo, "Test Four: Six adjustments; Highest SEQN = 07")

    'End Sub


    '<TestMethod()> Public Sub FunctionMaintainableDailyReceiverListingAdjustment_TestScenerios()

    '    Dim StockAdjustments As New Adjustments
    '    Dim DT As DataTable

    '    DT = StockAdjustmentsConfigureStructure()

    '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '    StubStockAdjustmentsRepository(DT)
    '    StockAdjustments.ReadAdjustments(Nothing, Nothing, Nothing)
    '    Assert.IsFalse(StockAdjustments.MaintainableDailyReceiverListingAdjustment(Nothing), "Test One: No adjustments exist")

    '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '    StockAdjustmentsLoadData(DT)
    '    StubStockAdjustmentsRepository(DT)
    '    StockAdjustments.ReadAdjustments(Nothing, Nothing, Nothing)

    '    Assert.IsFalse(StockAdjustments.MaintainableDailyReceiverListingAdjustment(Nothing), "Test Two: DRL Number is empty")
    '    Assert.IsFalse(StockAdjustments.MaintainableDailyReceiverListingAdjustment("123456"), "Test Three: DRL Number does not exist")
    '    Assert.IsTrue(StockAdjustments.MaintainableDailyReceiverListingAdjustment("914815"), "Test Four: DRL Number has stock adjustment that is maintainable")

    '    Assert.IsTrue(StockAdjustments.MaintainableDailyReceiverListingAdjustment("914809"), "Test Five: DRL Number has stock adjustment that has been reversed but new stock adjustment exist")

    'End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub StubStockAdjustmentsRepository(ByRef DT As DataTable)

        Dim Mock As New MockRepository
        Dim Repository As IAdjustmentsRepository

        'arrange
        Repository = Mock.Stub(Of IAdjustmentsRepository)()
        SetupResult.On(Repository).Call(Repository.ReadAdjustments(Nothing, Nothing, Nothing)).Return(DT).IgnoreArguments()
        AdjustmentsRepositoryFactory.FactorySet(Repository)
        Mock.ReplayAll()

    End Sub

    Private Function StockAdjustmentsConfigureStructure() As DataTable

        Dim DT As DataTable

        DT = New DataTable

        With DT.Columns

            .Add("AdjustmentDate", System.Type.GetType("System.DateTime"))         'not null
            .Add("AdjustmentCode", System.Type.GetType("System.String"))           '
            .Add("SKUN", System.Type.GetType("System.String"))                     'not null
            .Add("SEQN", System.Type.GetType("System.String"))                     'not null
            .Add("AmendId", System.Type.GetType("System.Int16"))                   'not null
            .Add("Initials", System.Type.GetType("System.String"))
            .Add("Department", System.Type.GetType("System.String"))
            .Add("StartingStock", System.Type.GetType("System.Decimal"))
            .Add("AdjustmentQty", System.Type.GetType("System.Decimal"))
            .Add("AdjustmentPrice", System.Type.GetType("System.Decimal"))
            .Add("AdjustmentCost", System.Type.GetType("System.Decimal"))
            .Add("IsSentToHO", System.Type.GetType("System.Boolean"))              'not null
            .Add("AdjustmentType", System.Type.GetType("System.String"))
            .Add("Comment", System.Type.GetType("System.String"))
            .Add("DRLNumber", System.Type.GetType("System.String"))
            .Add("ReversalCode", System.Type.GetType("System.String"))
            .Add("MarkDownWriteOff", System.Type.GetType("System.String"))
            .Add("WriteOffAuthorised", System.Type.GetType("System.String"))
            .Add("DateAuthorised", System.Type.GetType("System.DateTime"))
            .Add("RTI", System.Type.GetType("System.String"))
            .Add("PeriodId", System.Type.GetType("System.Int16"))                  'not null
            .Add("TransferSku", System.Type.GetType("System.String"))
            .Add("TransferValue", System.Type.GetType("System.Decimal"))
            .Add("TransferStart", System.Type.GetType("System.Decimal"))
            .Add("TransferPrice", System.Type.GetType("System.Decimal"))
            .Add("IsReversed", System.Type.GetType("System.Boolean"))              'not null
        End With

        Return DT

    End Function

    Private Sub StockAdjustmentsAddRow(ByRef DT As DataTable, _
                                      ByVal AdjustmentDate As Date, _
                                      ByVal AdjustmentCode As String, _
                                      ByVal SKUN As String, _
                                      ByVal SEQN As String, _
                                      ByVal AmendId As Integer, _
                                      ByVal Initials As String, _
                                      ByVal Department As String, _
                                      ByVal StartingStock As Decimal, _
                                      ByVal AdjustmentQty As Decimal, _
                                      ByVal AdjustmentPrice As Decimal, _
                                      ByVal AdjustmentCost As Decimal, _
                                      ByVal IsSentToHO As Boolean, _
                                      ByVal AdjustmentType As String, _
                                      ByVal Comment As String, _
                                      ByVal DRLNumber As String, _
                                      ByVal ReversalCode As String, _
                                      ByVal MarkDownWriteOff As String, _
                                      ByVal WriteOffAuthorised As String, _
                                      ByVal DateAuthorised As Date, _
                                      ByVal RTI As String, _
                                      ByVal PeriodId As Integer, _
                                      ByVal TransferSku As String, _
                                      ByVal TransferValue As Decimal, _
                                      ByVal TransferStart As Decimal, _
                                      ByVal TransferPrice As Decimal, _
                                      ByVal IsReversed As Boolean)

        DT.Rows.Add(AdjustmentDate, AdjustmentCode, SKUN, SEQN, AmendId, Initials, Department, StartingStock, AdjustmentQty, _
                    AdjustmentPrice, AdjustmentCost, IsSentToHO, AdjustmentType, Comment, DRLNumber, ReversalCode, MarkDownWriteOff, _
                    WriteOffAuthorised, DateAuthorised, RTI, PeriodId, TransferSku, TransferValue, TransferStart, TransferPrice, IsReversed)

    End Sub

    Private Sub StockAdjustmentsLoadData(ByRef DT As DataTable)

        StockAdjustmentsAddRow(DT, Now, "04", "100807", "07", 0, "SC", "10", 114, -4, CType(5.99, Decimal), 0, False, "N", "Comment", "914815", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
        StockAdjustmentsAddRow(DT, Now, "04", "100807", "06", 0, "SC", "10", 85, 10, CType(5.99, Decimal), 0, False, "N", "Comment ", "914813", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
        StockAdjustmentsAddRow(DT, Now, "04", "100807", "04", 0, "SC", "10", 80, 5, CType(5.99, Decimal), 0, False, "N", "Comment  ", "914809", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
        StockAdjustmentsAddRow(DT, Now, "04", "100807", "02", 0, "SC", "10", 108, -8, CType(5.99, Decimal), 0, False, "N", "Comment", "914811", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
        StockAdjustmentsAddRow(DT, Now, "04", "100807", "01", 0, "SC", "10", 47, 2, CType(5.99, Decimal), 0, True, "N", "Comment   ", "914810", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, False)
        StockAdjustmentsAddRow(DT, Now, "04", "100807", "00", 0, "SC", "10", 39, 8, CType(5.99, Decimal), 0, True, "N", "Comment   ", "914809", "", "", "0", Nothing, "N", 0, "000000", 0, 0, 0, True)

    End Sub

#End Region

End Class
