﻿<TestClass()> Public Class AdjustmentRepositoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestAdjustmentRepository
        Inherits AdjustmentRepository

#Region "Private Variables"

        Private _ParametersProvided As Boolean
        Private _State As StateEnumerator

        Private _ResetCalled As Boolean
        Private _SetConnectionCalled As Boolean
        Private _SetSkuNumberCalled As Boolean
        Private _SetAdjustmentCodeCalled As Boolean
        Private _SetAdjustmentDateCalled As Boolean

        Private _ExecuteGetNextSequenceNoCalled As Boolean
        Private _ConfigureGetNextSequenceNoCommandCalled As Boolean
        Private _ExecuteGetNextSequenceNoCommandCalled As Boolean
        Private _ExecuteDataTableCalled As Boolean

#End Region

#Region "Enumerators"

        Enum StateEnumerator
            NoData
            NoRow
            OneRow
            TwoRow
        End Enum

#End Region

#Region "Constructor"

        Public Sub New(ByVal ParametersProvided As Boolean, ByVal State As StateEnumerator)

            _ParametersProvided = ParametersProvided
            _State = State

        End Sub

#End Region

#Region "Access Parent Variables"

        Public Function AccessInternalParentVariable_Connection() As IConnection

            Return MyBase._Connection

        End Function

        Public Sub ModifyInternalParentVariable_Connection(ByRef Connection As IConnection)

            MyBase._Connection = Connection

        End Sub

        Public Function AccessInternalParentVariable_Command() As ICommand

            Return MyBase._Command

        End Function

        Public Sub ModifyInternalParentVariable_Command(ByRef Command As ICommand)

            MyBase._Command = Command

        End Sub

        Public Function AccessInternalParentVariable_DT() As DataTable

            Return MyBase._DT

        End Function

        Public Sub ModifyInternalParentVariable_DT(ByRef DT As DataTable)

            MyBase._DT = DT

        End Sub

        Public Function AccessInternalParentVariable_SkuNumber() As String

            Return MyBase._SkuNumber

        End Function

        Public Sub ModifyInternalParentVariable_SkuNumber(ByVal SkuNumber As String)

            MyBase._SkuNumber = SkuNumber

        End Sub

        Public Function AccessInternalParentVariable_AdjustmentCode() As String

            Return MyBase._AdjustmentCode

        End Function

        Public Sub ModifyInternalParentVariable_AdjustmentCode(ByVal AdjustmentCode As String)

            MyBase._AdjustmentCode = AdjustmentCode

        End Sub

        Public Function AccessInternalParentVariable_AdjustmentDate() As System.Nullable(Of Date)

            Return MyBase._AdjustmentDate

        End Function

        Public Sub ModifyInternalParentVariable_AdjustmentDate(ByVal AdjustmentDate As System.Nullable(Of Date))

            MyBase._AdjustmentDate = AdjustmentDate

        End Sub

#End Region

#Region "Local Properties"

        Public ReadOnly Property ResetCalled() As Boolean
            Get
                Return _ResetCalled
            End Get
        End Property

        Public ReadOnly Property SetConnectionCalled() As Boolean
            Get
                Return _SetConnectionCalled
            End Get
        End Property

        Public ReadOnly Property SetSkuNumberCalled() As Boolean
            Get
                Return _SetSkuNumberCalled
            End Get
        End Property

        Public ReadOnly Property SetAdjustmentCodeCalled() As Boolean
            Get
                Return _SetAdjustmentCodeCalled
            End Get
        End Property

        Public ReadOnly Property SetAdjustmentDateCalled() As Boolean
            Get
                Return _SetAdjustmentDateCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteGetNextSequenceNoCalled() As Boolean
            Get
                Return _ExecuteGetNextSequenceNoCalled
            End Get
        End Property

        Public ReadOnly Property ConfigureGetNextSequenceNoCommandCalled() As Boolean
            Get
                Return _ConfigureGetNextSequenceNoCommandCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteGetNextSequenceNoCommandCalled() As Boolean
            Get
                Return _ExecuteGetNextSequenceNoCommandCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteDataTableCalled() As Boolean
            Get
                Return _ExecuteDataTableCalled
            End Get
        End Property

#End Region

#Region "Overrides"

        Friend Overrides Sub Reset()

            _ResetCalled = True

            MyBase.Reset()

        End Sub

        Friend Overrides Sub SetConnection(ByRef Con As Connection)

            _SetConnectionCalled = True

            If _ParametersProvided = True Then MyBase.SetConnection(Con)

        End Sub

        Friend Overrides Sub SetSkuNumber(ByVal SkuNumber As String)

            _SetSkuNumberCalled = True

            If _ParametersProvided = True Then MyBase.SetSkuNumber(SkuNumber)

        End Sub

        Friend Overrides Sub SetAdjustmentCode(ByVal AdjustmentCode As String)

            _SetAdjustmentCodeCalled = True

            If _ParametersProvided = True Then MyBase.SetAdjustmentCode(AdjustmentCode)

        End Sub

        Friend Overrides Sub SetAdjustmentDate(ByVal AdjustmentDate As Date)

            _SetAdjustmentDateCalled = True

            If _ParametersProvided = True Then MyBase.SetAdjustmentDate(AdjustmentDate)

        End Sub

        Friend Overrides Function ExecuteGetNextSequenceNo() As String

            _ExecuteGetNextSequenceNoCalled = True

            ExecuteGetNextSequenceNo = Nothing
            If _ParametersProvided = True Then Return MyBase.ExecuteGetNextSequenceNo()

        End Function

        Friend Overrides Sub ConfigureGetNextSequenceNoCommand()

            _ConfigureGetNextSequenceNoCommandCalled = True

            If _ParametersProvided = True Then MyBase.ConfigureGetNextSequenceNoCommand()

        End Sub

        Friend Overrides Function ExecuteGetNextSequenceNoCommand() As String

            _ExecuteGetNextSequenceNoCommandCalled = True

            ExecuteGetNextSequenceNoCommand = Nothing
            If _ParametersProvided = True Then Return MyBase.ExecuteGetNextSequenceNoCommand()

        End Function

        Friend Overrides Sub ExecuteDataTable()

            _ExecuteDataTableCalled = True
            'do not execute actual command

            Select Case _State

                Case StateEnumerator.NoData
                    'n/a

                Case StateEnumerator.NoRow
                    MyBase._DT = New DataTable

                Case StateEnumerator.OneRow
                    MyBase._DT = TestDataTableWithOneRecord()

                Case StateEnumerator.TwoRow
                    MyBase._DT = TestDataTableWithTwoRecords()

            End Select

        End Sub

#End Region

#Region "Local Procedures And Functions"

        Private Function TestDataTableWithOneRecord() As DataTable

            Dim DT As DataTable

            DT = New DataTable
            DT.Columns.Add("SEQN", System.Type.GetType("System.String"))
            DT.Rows.Add("1")

            Return DT

        End Function

        Private Function TestDataTableWithTwoRecords() As DataTable

            Dim DT As DataTable

            DT = New DataTable
            DT.Columns.Add("SEQN", System.Type.GetType("System.String"))
            DT.Rows.Add("2")
            DT.Rows.Add("3")

            Return DT

        End Function

#End Region

    End Class

    Private Class TestConnection
        Inherits Connection

        Protected Overrides Sub SetDataProvider()

            MyBase.DataProviderField = Cts.Oasys.Data.DataProvider.Sql

        End Sub

        Protected Overrides Sub InitialiseConnection()

            MyBase.SqlConnectionField = New SqlConnection

        End Sub

    End Class

#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub FunctionGetNextSequenceNumberForSkunCodeDate_TestScenarios()

        Dim Repository As TestAdjustmentRepository

        Repository = New TestAdjustmentRepository(False, TestAdjustmentRepository.StateEnumerator.NoData)

        Repository.ModifyInternalParentVariable_Connection(New TestConnection)
        Repository.ModifyInternalParentVariable_Command(New Command(New TestConnection))
        Repository.ModifyInternalParentVariable_DT(New DataTable)
        Repository.ModifyInternalParentVariable_SkuNumber("123456")
        Repository.ModifyInternalParentVariable_AdjustmentCode("30")
        Repository.ModifyInternalParentVariable_AdjustmentDate(Today)

        Repository.GetNextSequenceNumberForSkunCodeDate(Nothing, Nothing, Nothing, Nothing)

        Assert.IsNull(Repository.AccessInternalParentVariable_Connection, "Test: Connection empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_DT, "Test: DataTable empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_SkuNumber, "Test: SkuNumber empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_AdjustmentCode, "Test: AdjustmentCode empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_AdjustmentDate, "Test: AdjustmentDate empty")


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.ResetCalled, "Test: Reset called")
        Assert.IsTrue(Repository.SetConnectionCalled, "Test: SetConnection called")
        Assert.IsTrue(Repository.SetSkuNumberCalled, "Test: SetSkuNumber called")
        Assert.IsTrue(Repository.SetAdjustmentCodeCalled, "Test: SetAdjustmentCode called")
        Assert.IsTrue(Repository.SetAdjustmentDateCalled, "Test: SetAdjustmentDate called")
        Assert.IsTrue(Repository.ExecuteGetNextSequenceNoCalled, "Test: ExecuteGetNextSequenceNoCalled called")


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestAdjustmentRepository(True, TestAdjustmentRepository.StateEnumerator.NoData)
        Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, "123456", "30", Today)

        Assert.IsTrue(Repository.ConfigureGetNextSequenceNoCommandCalled, "Test: ConfigureGetNextSequenceNoCommand called")
        Assert.IsTrue(Repository.ExecuteGetNextSequenceNoCommandCalled, "Test: ExecuteGetNextSequenceNoCommand called")
        Assert.IsTrue(Repository.ExecuteDataTableCalled, "Test: ExecuteDataTable called")

        Assert.IsNotNull(Repository.AccessInternalParentVariable_Connection, "Test: Connection stored correctly in internal variable")
        Assert.AreEqual("123456", Repository.AccessInternalParentVariable_SkuNumber, "Test: SkuNumber stored correctly in internal variable")
        Assert.AreEqual("30", Repository.AccessInternalParentVariable_AdjustmentCode, "Test: AdjustmentCode stored correctly in internal variable")
        Assert.AreEqual(Today, Repository.AccessInternalParentVariable_AdjustmentDate, "Test: AdjustmentDate stored correctly in internal variable")


        Assert.AreEqual(3, Repository.AccessInternalParentVariable_Command.ParameterCollectionCount, "Test: Parameter Count: Not Adjustment Code 04")
        Assert.AreEqual("usp_GetStockAdjustmentForSkuToday", Repository.AccessInternalParentVariable_Command.StoredProcedureName, "Test: Stored Procedure Name")


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, "123456", "04", Today)

        Assert.AreEqual(2, Repository.AccessInternalParentVariable_Command.ParameterCollectionCount, "Test: Parameter Count: Adjustment Code 04")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository.GetNextSequenceNumberForSkunCodeDate(Nothing, "123456", "30", Today)
        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command is null because connection not provided")

        Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, String.Empty, "30", Today)
        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command is null because empty sku provided")


        Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, "123456", String.Empty, Today)
        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command is null because empty adjustment code  provided")

        Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, "123456", "30", Nothing)
        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command is null because empty adjustment date  provided")


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestAdjustmentRepository(True, TestAdjustmentRepository.StateEnumerator.NoData)
        Assert.IsNull(Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, "123456", "30", Today), "Test: Return empty string; data table is nothing")

        Repository = New TestAdjustmentRepository(True, TestAdjustmentRepository.StateEnumerator.NoRow)
        Assert.IsNull(Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, "123456", "30", Today), "Test: Return empty string; data table not nothing but has no rows")

        Repository = New TestAdjustmentRepository(True, TestAdjustmentRepository.StateEnumerator.OneRow)
        Assert.AreEqual("02", Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, "123456", "30", Today), "Test: Return string value 01")

        Repository = New TestAdjustmentRepository(True, TestAdjustmentRepository.StateEnumerator.TwoRow)
        Assert.AreEqual("03", Repository.GetNextSequenceNumberForSkunCodeDate(New TestConnection, "123456", "30", Today), "Test: Return string value 02")

    End Sub

    <TestMethod()> Public Sub PrivateMethods_TestScenarios()

        Dim Repository As TestAdjustmentRepository

        Repository = New TestAdjustmentRepository(False, TestAdjustmentRepository.StateEnumerator.NoData)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.ConnectionIsNothing, "Test: Connection is nothing returns true")
        Repository.ModifyInternalParentVariable_Connection(New TestConnection)
        Assert.IsFalse(Repository.ConnectionIsNothing, "Test: Connection is nothing returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.CommandIsNothing, "Test: Command is nothing returns true")
        Repository.ModifyInternalParentVariable_Command(New Command(New TestConnection))
        Assert.IsFalse(Repository.CommandIsNothing, "Test: Command is nothing returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.DataTableIsNothing, "Test: Data table is nothing returns true")
        Repository.ModifyInternalParentVariable_DT(New DataTable)
        Assert.IsFalse(Repository.DataTableIsNothing, "Test: Data table is nothing returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.DataTableZeroRecordCount, "Test: Data table zero record count returns true")
        Repository.ModifyInternalParentVariable_DT(TestDataTableWithOneRecord())
        Assert.IsFalse(Repository.DataTableZeroRecordCount, "Test: Data table zero record count returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.SkuNumberIsEmpty, "Test: SkuNumber is empty returns true")
        Repository.ModifyInternalParentVariable_SkuNumber("ABC")
        Assert.IsFalse(Repository.SkuNumberIsEmpty, "Test: SkuNumber is empty returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.AdjustmentCodeIsEmpty, "Test: AdjustmentCode is empty returns true")
        Repository.ModifyInternalParentVariable_AdjustmentCode("30")
        Assert.IsFalse(Repository.AdjustmentCodeIsEmpty, "Test: AdjustmentCode is empty returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.AdjustmentDateIsEmpty, "Test: AdjustmentDate is empty returns true")
        Repository.ModifyInternalParentVariable_AdjustmentDate(Today)
        Assert.IsFalse(Repository.AdjustmentDateIsEmpty, "Test: AdjustmentDate is empty returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository.ModifyInternalParentVariable_AdjustmentCode("04")
        Assert.IsTrue(Repository.CodeFour, "Test: Adjustment Code 04 return true")
        Repository.ModifyInternalParentVariable_AdjustmentCode("01")
        Assert.IsFalse(Repository.CodeFour, "Test: Adjustment Code not 04 return false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.MinimumDateValue(Nothing), "Test: Date is min value")
        Assert.IsFalse(Repository.MinimumDateValue(Today), "Test: Date is not min value")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.AreEqual(2, Repository.NextSequenceNo(1), "Test: NextSequenceNo return correct value")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.AreEqual("03", Repository.IntegerConvertToStringWithPadding(3), "Test: IntegerConvertToStringWithPadding return correctly padded 1 digit value")
        Assert.AreEqual("44", Repository.IntegerConvertToStringWithPadding(44), "Test: IntegerConvertToStringWithPadding return correctly padded 2 digit value")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Function TestDataTableWithOneRecord() As DataTable

        Dim DT As DataTable

        DT = New DataTable
        DT.Columns.Add("StringField", System.Type.GetType("System.String"))
        DT.Rows.Add("ABC")

        Return DT

    End Function

#End Region

End Class
