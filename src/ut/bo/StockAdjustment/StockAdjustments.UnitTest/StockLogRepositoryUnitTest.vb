﻿<TestClass()> Public Class StockLogRepositoryUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Test Classes"

    Private Class TestStockLogRepository
        Inherits StockLogRepository

#Region "Private Variables"

        Private _ParametersProvided As Boolean
        Private _State As StateEnumerator

        Private _ResetCalled As Boolean
        Private _SetConnectionCalled As Boolean
        Private _SetStockLogCalled As Boolean
        Private _SetSkuNumberCalled As Boolean
        Private _CreateWhichConnectionCalled As Boolean
        Private _DecideWhichConnectionToUseCalled As Boolean

        Private _ExecuteSaveCalled As Boolean
        Private _ConfigureSaveCommandCalled As Boolean
        Private _ExecuteSaveCommandCalled As Boolean
        Private _ExecuteNonQueryCalled As Boolean

        Private _ExecuteGetStockLogCalled As Boolean
        Private _ConfigureGetStockLogCommandCalled As Boolean
        Private _ExecuteGetStockLogCommandCalled As Boolean
        Private _ExecuteDataTableCalled As Boolean

#End Region

#Region "Enumerators"

        Enum StateEnumerator
            NoData
            NoRow
            OneRow
            TwoRow
        End Enum

#End Region

#Region "Constructor"

        Public Sub New(ByVal ParametersProvided As Boolean, ByVal State As StateEnumerator)

            _ParametersProvided = ParametersProvided
            _State = State

        End Sub

#End Region

#Region "Access Parent Variables"

        Public Function AccessInternalParentVariable_Connection() As IConnection

            Return MyBase._Connection

        End Function

        Public Sub ModifyInternalParentVariable_Connection(ByRef Connection As IConnection)

            MyBase._Connection = Connection

        End Sub

        Public Function AccessInternalParentVariable_Command() As ICommand

            Return MyBase._Command

        End Function

        Public Sub ModifyInternalParentVariable_Command(ByRef Command As ICommand)

            MyBase._Command = Command

        End Sub

        Public Function AccessInternalParentVariable_DT() As DataTable

            Return MyBase._DT

        End Function

        Public Sub ModifyInternalParentVariable_DT(ByRef DT As DataTable)

            MyBase._DT = DT

        End Sub

        Public Function AccessInternalParentVariable_StockLog() As IStockLog

            Return MyBase._StockLog

        End Function

        Public Sub ModifyInternalParentVariable_StockLog(ByRef StockLog As IStockLog)

            MyBase._StockLog = StockLog

        End Sub

        Public Function AccessInternalParentVariable_SkuNumber() As String

            Return MyBase._SkuNumber

        End Function

        Public Sub ModifyInternalParentVariable_SkuNumber(ByVal SkuNumber As String)

            MyBase._SkuNumber = SkuNumber

        End Sub

        Public Function AccessInternalParentVariable_WhichConnection() As IWhichConnection

            Return MyBase._WhichConnection

        End Function

        Public Sub ModifyInternalParentVariable_WhichConnection(ByRef WhichConnection As IWhichConnection)

            MyBase._WhichConnection = WhichConnection

        End Sub

#End Region

#Region "Local Properties"

        Public ReadOnly Property ResetCalled() As Boolean
            Get
                Return _ResetCalled
            End Get
        End Property

        Public ReadOnly Property SetConnectionCalled() As Boolean
            Get
                Return _SetConnectionCalled
            End Get
        End Property

        Public ReadOnly Property SetStockLogCalled() As Boolean
            Get
                Return _SetStockLogCalled
            End Get
        End Property

        Public ReadOnly Property SetSkuNumberCalled() As Boolean
            Get
                Return _SetSkuNumberCalled
            End Get
        End Property

        Public ReadOnly Property CreateWhichConnectionCalled() As Boolean
            Get
                Return _CreateWhichConnectionCalled
            End Get
        End Property

        Public ReadOnly Property DecideWhichConnectionToUseCalled() As Boolean
            Get
                Return _DecideWhichConnectionToUseCalled
            End Get
        End Property

#Region "Method: Save Properties"

        Public ReadOnly Property ExecuteSaveCalled() As Boolean
            Get
                Return _ExecuteSaveCalled
            End Get
        End Property

        Public ReadOnly Property ConfigureSaveCommandCalled() As Boolean
            Get
                Return _ConfigureSaveCommandCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteSaveCommandCalled() As Boolean
            Get
                Return _ExecuteSaveCommandCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteNonQueryCalled() As Boolean
            Get
                Return _ExecuteNonQueryCalled
            End Get
        End Property

#End Region

#Region "Method: GetLastStockLogForSkun Properties"

        Public ReadOnly Property ExecuteGetStockLogCalled() As Boolean
            Get
                Return _ExecuteGetStockLogCalled
            End Get
        End Property

        Public ReadOnly Property ConfigureGetStockLogCommandCalled() As Boolean
            Get
                Return _ConfigureGetStockLogCommandCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteGetStockLogCommandCalled() As Boolean
            Get
                Return _ExecuteGetStockLogCommandCalled
            End Get
        End Property

        Public ReadOnly Property ExecuteDataTableCalled() As Boolean
            Get
                Return _ExecuteDataTableCalled
            End Get
        End Property

#End Region

#End Region

#Region "Local Procedures And Functions"

        Private Function TestDataTableWithOneRecord() As DataTable

            Dim DT As DataTable

            DT = New DataTable
            DT.Columns.Add("StringField", System.Type.GetType("System.String"))
            DT.Rows.Add("ABC")

            Return DT

        End Function

        Private Function TestDataTableWithTwoRecords() As DataTable

            Dim DT As DataTable

            DT = New DataTable
            DT.Columns.Add("StringField", System.Type.GetType("System.String"))
            DT.Rows.Add("ABC")
            DT.Rows.Add("DEF")

            Return DT

        End Function

#End Region

#Region "Overrides"

#Region "Rest"

        Friend Overrides Sub Reset()

            _ResetCalled = True

            MyBase.Reset()

        End Sub

        Friend Overrides Sub SetConnection(ByRef Con As Connection)

            _SetConnectionCalled = True

            If _ParametersProvided = True Then MyBase.SetConnection(Con)

        End Sub

        Friend Overrides Sub SetStockLog(ByVal StockLog As IStockLog)

            _SetStockLogCalled = True

            If _ParametersProvided = True Then MyBase.SetStockLog(StockLog)

        End Sub

        Friend Overrides Sub SetSkuNumber(ByVal SkuNumber As String)

            _SetSkuNumberCalled = True

            If _ParametersProvided = True Then MyBase.SetSkuNumber(SkuNumber)

        End Sub

        Friend Overrides Sub CreateWhichConnection()

            _CreateWhichConnectionCalled = True

            If _ParametersProvided = True Then MyBase.CreateWhichConnection()

        End Sub

        Friend Overrides Sub DecideWhichConnectionToUse()

            _DecideWhichConnectionToUseCalled = True

            If _ParametersProvided = True Then MyBase.DecideWhichConnectionToUse()

        End Sub

#End Region

#Region "Method: Save"

        Friend Overrides Sub ExecuteSave()

            _ExecuteSaveCalled = True

            If _ParametersProvided = True Then MyBase.ExecuteSave()

        End Sub

        Friend Overrides Sub ConfigureSaveCommand()

            _ConfigureSaveCommandCalled = True

            If _ParametersProvided = True Then MyBase.ConfigureSaveCommand()

        End Sub

        Friend Overrides Sub ExecuteSaveCommand()

            _ExecuteSaveCommandCalled = True

            If _ParametersProvided = True Then MyBase.ExecuteSaveCommand()

        End Sub

        Friend Overrides Sub ExecuteNonQuery()

            _ExecuteNonQueryCalled = True
            'do not execute actual command

        End Sub

#End Region

#Region "Method: GetLastStockLogForSkun"

        Friend Overrides Function ExecuteGetStockLog() As DataRow

            _ExecuteGetStockLogCalled = True

            ExecuteGetStockLog = Nothing
            If _ParametersProvided = True Then Return MyBase.ExecuteGetStockLog()

        End Function

        Friend Overrides Sub ConfigureGetStockLogCommand()

            _ConfigureGetStockLogCommandCalled = True

            If _ParametersProvided = True Then MyBase.ConfigureGetStockLogCommand()

        End Sub

        Friend Overrides Function ExecuteGetStockLogCommand() As DataRow

            _ExecuteGetStockLogCommandCalled = True

            ExecuteGetStockLogCommand = Nothing
            If _ParametersProvided = True Then Return MyBase.ExecuteGetStockLogCommand()

        End Function

        Friend Overrides Sub ExecuteDataTable()

            _ExecuteDataTableCalled = True
            'do not execute actual command

            Select Case _State

                Case StateEnumerator.NoData
                    'n/a

                Case StateEnumerator.NoRow
                    MyBase._DT = New DataTable

                Case StateEnumerator.OneRow
                    MyBase._DT = TestDataTableWithOneRecord()

                Case StateEnumerator.TwoRow
                    MyBase._DT = TestDataTableWithTwoRecords()

            End Select

        End Sub

#End Region

#End Region

    End Class

    Private Class TestConnection
        Inherits Connection

        Protected Overrides Sub SetDataProvider()

            MyBase.DataProviderField = Cts.Oasys.Data.DataProvider.Sql

        End Sub

        Protected Overrides Sub InitialiseConnection()

            MyBase.SqlConnectionField = New SqlConnection

        End Sub

    End Class

#End Region

#Region "Unit Test"

    <TestMethod()> Public Sub PrivateMethods_TestScenarios()

        Dim Repository As TestStockLogRepository

        Repository = New TestStockLogRepository(False, TestStockLogRepository.StateEnumerator.NoData)

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.ConnectionIsNothing, "Test: Connection is nothing returns true")
        Repository.ModifyInternalParentVariable_Connection(New TestConnection)
        Assert.IsFalse(Repository.ConnectionIsNothing, "Test: Connection is nothing returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.CommandIsNothing, "Test: Command is nothing returns true")
        Repository.ModifyInternalParentVariable_Command(New Command(New TestConnection))
        Assert.IsFalse(Repository.CommandIsNothing, "Test: Command is nothing returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.DataTableIsNothing, "Test: Data table is nothing returns true")
        Repository.ModifyInternalParentVariable_DT(New DataTable)
        Assert.IsFalse(Repository.DataTableIsNothing, "Test: Data table is nothing returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.DataTableZeroRecordCount, "Test: Data table zero record count returns true")
        Repository.ModifyInternalParentVariable_DT(TestDataTableWithOneRecord())
        Assert.IsFalse(Repository.DataTableZeroRecordCount, "Test: Data table zero record count returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.StockLogIsNothing, "Test: StockLog is nothing returns true")
        Repository.ModifyInternalParentVariable_StockLog(New StockLog)
        Assert.IsFalse(Repository.StockLogIsNothing, "Test: StockLog is nothing returns false")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.SkuNumberIsEmpty, "Test: SkuNumber is empty returns true")
        Repository.ModifyInternalParentVariable_SkuNumber("ABC")
        Assert.IsFalse(Repository.SkuNumberIsEmpty, "Test: SkuNumber is empty returns false")

    End Sub

    <TestMethod()> Public Sub FunctionSave_TestScenarios()

        Dim Repository As TestStockLogRepository

        ArrangeRequirementSwitchDependency(True)
        Repository = New TestStockLogRepository(False, TestStockLogRepository.StateEnumerator.NoData)

        Repository.ModifyInternalParentVariable_Connection(New TestConnection)
        Repository.ModifyInternalParentVariable_Command(New Command(New TestConnection))
        Repository.ModifyInternalParentVariable_DT(New DataTable)
        Repository.ModifyInternalParentVariable_StockLog(StockLogFactory.FactoryGet)
        Repository.ModifyInternalParentVariable_SkuNumber("123456")
        Repository.ModifyInternalParentVariable_WhichConnection((New WhichConnectionFactory).GetImplementation)

        Repository.Save(Nothing, Nothing)

        Assert.IsNull(Repository.AccessInternalParentVariable_Connection, "Test: Connection empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_DT, "Test: DataTable empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_StockLog, "Test: StockLog empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_SkuNumber, "Test: SkuNumber empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_WhichConnection, "Test: WhichConnection empty")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.ResetCalled, "Test: Reset called")
        Assert.IsTrue(Repository.SetConnectionCalled, "Test: SetConnection called")
        Assert.IsTrue(Repository.SetStockLogCalled, "Test: SetStockLog called")
        Assert.IsTrue(Repository.CreateWhichConnectionCalled, "Test: CreateWhichConnection called")
        Assert.IsTrue(Repository.DecideWhichConnectionToUseCalled, "Test: DecideWhichConnectionToUse called")
        Assert.IsTrue(Repository.ExecuteSaveCalled, "Test: ExecuteSave called")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.NoData)
        Repository.Save(New StockLog, New TestConnection)

        Assert.IsTrue(Repository.ConfigureSaveCommandCalled, "Test: ConfigureSaveCommand called")
        Assert.IsTrue(Repository.ExecuteSaveCommandCalled, "Test: ExecuteSaveCommand called")
        Assert.IsTrue(Repository.ExecuteNonQueryCalled, "Test: ExecuteNonQuery called")

        Assert.IsNotNull(Repository.AccessInternalParentVariable_Connection, "Test: Connection stored correctly in internal variable")
        Assert.IsNotNull(Repository.AccessInternalParentVariable_StockLog, "Test: StockLog stored correctly in internal variable")

        Assert.AreEqual(17, Repository.AccessInternalParentVariable_Command.ParameterCollectionCount, "Test: Parameter Count")
        Assert.AreEqual("usp_InsertStockLogForSku", Repository.AccessInternalParentVariable_Command.StoredProcedureName, "Test: Stored Procedure Name")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.NoData)
        Repository.Save(New StockLog, Nothing)

        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command is null because connection not provided")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.NoData)
        Repository.Save(Nothing, New TestConnection)

        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command is null because empty stock log provided")

    End Sub

    <TestMethod()> Public Sub FunctionGetLastStockLogForSkun_TestScenarios()

        Dim Repository As TestStockLogRepository

        ArrangeRequirementSwitchDependency(True)
        Repository = New TestStockLogRepository(False, TestStockLogRepository.StateEnumerator.NoData)

        Repository.ModifyInternalParentVariable_Connection(New TestConnection)
        Repository.ModifyInternalParentVariable_Command(New Command(New TestConnection))
        Repository.ModifyInternalParentVariable_DT(New DataTable)
        Repository.ModifyInternalParentVariable_StockLog(StockLogFactory.FactoryGet)
        Repository.ModifyInternalParentVariable_SkuNumber("123456")
        Repository.ModifyInternalParentVariable_WhichConnection((New WhichConnectionFactory).GetImplementation)

        Repository.GetLastStockLogForSkun(Nothing, Nothing)

        Assert.IsNull(Repository.AccessInternalParentVariable_Connection, "Test: Connection empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_DT, "Test: DataTable empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_StockLog, "Test: StockLog empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_SkuNumber, "Test: SkuNumber empty")
        Assert.IsNull(Repository.AccessInternalParentVariable_WhichConnection, "Test: WhichConnection empty")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Assert.IsTrue(Repository.ResetCalled, "Test: Reset called")
        Assert.IsTrue(Repository.SetConnectionCalled, "Test: SetConnection called")
        Assert.IsTrue(Repository.SetSkuNumberCalled, "Test: SetSkuNumber called")
        Assert.IsTrue(Repository.ExecuteGetStockLogCalled, "Test: ExecuteGetStockLog called")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.NoData)
        Repository.GetLastStockLogForSkun(New TestConnection, "123456")

        Assert.IsTrue(Repository.ConfigureGetStockLogCommandCalled, "Test: ConfigureGetStockLogCommand called")
        Assert.IsTrue(Repository.ExecuteGetStockLogCommandCalled, "Test: ExecuteGetStockLogCommand called")
        Assert.IsTrue(Repository.ExecuteDataTableCalled, "Test: ExecuteDataTable called")

        Assert.IsNotNull(Repository.AccessInternalParentVariable_Connection, "Test: Connection stored correctly in internal variable")
        Assert.AreEqual("123456", Repository.AccessInternalParentVariable_SkuNumber, "Test: SkuNumber stored correctly in internal variable")

        Assert.AreEqual(1, Repository.AccessInternalParentVariable_Command.ParameterCollectionCount, "Test: Parameter Count")
        Assert.AreEqual("usp_GetLastStockLogForSku", Repository.AccessInternalParentVariable_Command.StoredProcedureName, "Test: Stored Procedure Name")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.NoData)
        Repository.GetLastStockLogForSkun(Nothing, "123456")

        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command is null because connection not provided")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.NoData)
        Repository.GetLastStockLogForSkun(New TestConnection, "")

        Assert.IsNull(Repository.AccessInternalParentVariable_Command, "Test: Command is null because empty sku provided")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.NoData)
        Assert.IsNull(Repository.GetLastStockLogForSkun(New TestConnection, "123456"), "Test: Return no data rows; data table is nothing")

        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.NoRow)
        Assert.IsNull(Repository.GetLastStockLogForSkun(New TestConnection, "123456"), "Test: Return no data rows; data table not nothing but has no rows")

        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.OneRow)
        Assert.IsNotNull(Repository.GetLastStockLogForSkun(New TestConnection, "123456"), "Test: Return one data row")

        Repository = New TestStockLogRepository(True, TestStockLogRepository.StateEnumerator.TwoRow)
        Assert.AreEqual("ABC", Repository.GetLastStockLogForSkun(New TestConnection, "123456").Item("StringField").ToString, "Test: Return first data row")

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

    Private Function TestDataTableWithOneRecord() As DataTable

        Dim DT As DataTable

        DT = New DataTable
        DT.Columns.Add("StringField", System.Type.GetType("System.String"))
        DT.Rows.Add("ABC")

        Return DT

    End Function

#End Region

End Class