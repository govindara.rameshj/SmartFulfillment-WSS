﻿<TestClass()> _
Public Class StockLogMarkDownWriteOffInitialValuesTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Factory Unit Test - IStockLogMarkDownWriteOffInitialValues"

    <TestMethod()> Public Sub FactorySingleImplementationVersion_ReturnsClassStockLogMarkDownWriteOffInitialValuesUseEndValue()

        Dim NewVersionOnly As IStockLogMarkDownWriteOffInitialValues
        Dim Factory As IBaseFactory(Of IStockLogMarkDownWriteOffInitialValues)

        Factory = New Create.StockLogMarkDownWriteOffInitialValuesFactory
        NewVersionOnly = Factory.GetImplementation

        Assert.AreEqual("EnterStockAdjustment.Create.StockLogMarkDownWriteOffInitialValuesUseEndValue", NewVersionOnly.GetType.FullName)

    End Sub

#End Region

#Region "Unit Test - Stock Log Moarkdown / Write Off Initial Values Test"

    <TestMethod()> Public Sub StockLogMarkDownWriteOffInitialValuesCheck_ParameterTrue_ReturnEndMarkDown()

        Dim V As IStockLogMarkDownWriteOffInitialValues = New StockLogMarkDownWriteOffInitialValuesUseEndValue()
        Dim SL As IStockLog = Nothing

        SL = GenerateMockStockLog()
        Assert.AreEqual(V.GetInitialMarkDownValue(SL), SL.EndingMarkDown_EMDN)

    End Sub

    <TestMethod()> Public Sub StockLogMarkDownWriteOffInitialValuesCheck_ParameterTrue_ReturnEndWriteOff()

        Dim V As IStockLogMarkDownWriteOffInitialValues = New StockLogMarkDownWriteOffInitialValuesUseEndValue()
        Dim SL As IStockLog = Nothing

        SL = GenerateMockStockLog()
        Assert.AreEqual(V.GetInitialWriteOffValue(SL), SL.EndingWriteOff_EWTF)

    End Sub

#End Region

#Region "Private Test Functions"

    Private Function GenerateMockStockLog() As IStockLog

        Dim MockStockLog As IStockLog = MockRepository.GenerateStub(Of IStockLog)()

        With MockStockLog
            .StockLogId_TKEY = 1
            .EndingMarkDown_EMDN = 1D
            .EndingWriteOff_EWTF = 2D
            .StartingMarkDown_SMDN = 3D
            .StartingWriteOff_SWTF = 4D
        End With
        Return MockStockLog

    End Function

#End Region

End Class