﻿<TestClass()> Public Class CreateUIUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Tests"

    <TestMethod()> Public Sub RequirementFactory_TestScenerios()

        Dim CreateUI As ICreateUI
        Dim Factory As New CreateUIFactory

        ArrangeRequirementSwitchDependency(True)
        CreateUI = Factory.GetImplementation
        Assert.AreEqual("EnterStockAdjustment.Create.CreateUICodeFourAdjustments", CreateUI.GetType.FullName, "Test One: Return new implementation")

        ArrangeRequirementSwitchDependency(False)
        CreateUI = Factory.GetImplementation
        Assert.AreEqual("EnterStockAdjustment.Create.CreateUIExisting", CreateUI.GetType.FullName, "Test Two: Return existing implementation")

        ArrangeRequirementSwitchDependency(Nothing)
        CreateUI = Factory.GetImplementation
        Assert.AreEqual("EnterStockAdjustment.Create.CreateUIExisting", CreateUI.GetType.FullName, "Test Three: Return existing implementation")

    End Sub

    <TestMethod()> Public Sub Procedure_SetControlFocus_TestScenerios()

        Dim Existing As New CreateUIExisting
        Dim CodeFour As New CreateUICodeFourAdjustments

        Dim StartingStock As New DevExpress.XtraEditors.TextEdit
        Dim AdjustmentValue As New DevExpress.XtraEditors.TextEdit
        Dim EndingStock As New DevExpress.XtraEditors.TextEdit
        Dim Comment As New DevExpress.XtraEditors.TextEdit


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        DisableControls(StartingStock, AdjustmentValue, EndingStock, Comment)

        'EndingStock.Enabled = True
        'Existing.SetControlFocus(ICreateUI.ControlProcessedOrder.Standard, StartingStock, AdjustmentValue, EndingStock, Comment)

        'Assert.IsTrue(EndingStock.Focused, "Test")



        'StartingStock.Focuse()

        'to do
        'standard order
        'reverse order


        'existing
        'Select Case Order
        '    Case ICreateUI.ControlProcessedOrder.Standard
        '        Select Case True
        '            Case StartingStock.Enabled : StartingStock.Focus()
        '            Case AdjustmentValue.Enabled : AdjustmentValue.Focus()
        '            Case EndingStock.Enabled : EndingStock.Focus()

        '        End Select

        '    Case ICreateUI.ControlProcessedOrder.Reverse
        '        Select Case True
        '            Case EndingStock.Enabled : EndingStock.Focus()
        '            Case AdjustmentValue.Enabled : AdjustmentValue.Focus()
        '            Case StartingStock.Enabled : StartingStock.Focus()

        '        End Select

        'End Select







        'new
        'Select Case Order
        '    Case ICreateUI.ControlProcessedOrder.Standard
        '        Select Case True
        '            Case StartingStock.Enabled : StartingStock.Focus()
        '            Case AdjustmentValue.Enabled : AdjustmentValue.Focus()
        '            Case EndingStock.Enabled : EndingStock.Focus()
        '            Case Comment.Enabled : Comment.Focus()

        '        End Select

        '    Case ICreateUI.ControlProcessedOrder.Reverse
        '        Select Case True
        '            Case Comment.Enabled : Comment.Focus()
        '            Case EndingStock.Enabled : EndingStock.Focus()
        '            Case AdjustmentValue.Enabled : AdjustmentValue.Focus()
        '            Case StartingStock.Enabled : StartingStock.Focus()

        '        End Select

        'End Select




    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As TpWickes.Library.IRequirementRepository

        Requirement = Mock.Stub(Of TpWickes.Library.IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        TpWickes.Library.RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

    Private Sub DisableControls(ByRef StartingStock As TextEdit, ByRef AdjustmentValue As TextEdit, ByRef EndingStock As TextEdit, ByRef Comment As TextEdit)

        StartingStock.Enabled = False
        AdjustmentValue.Enabled = False
        EndingStock.Enabled = False
        Comment.Enabled = False

    End Sub

#End Region

End Class
