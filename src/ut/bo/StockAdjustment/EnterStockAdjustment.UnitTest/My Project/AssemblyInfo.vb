﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic.Strings


' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("EnterStockAdjustment.UnitTest")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Travis Perkins PLC")> 
<Assembly: AssemblyProduct("EnterStockAdjustment.UnitTest")> 
<Assembly: AssemblyCopyright("Copyright " & Chrw(169) & " Travis Perkins PLC 2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: CLSCompliant(True)> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c8171823-ece6-4162-844a-6fa8ba6539fd")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
