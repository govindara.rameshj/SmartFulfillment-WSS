﻿Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Cts.Oasys
Imports System.Data

<TestClass()> Public Class DeletedUsersTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Private userInstance As Core.System.User.User

    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub UserNotSetAsDeletedPassed_ValidateDeletedUsers_ReturnsFalse()

        userInstance = New Core.System.User.User(PopulateData(False))
        Assert.AreEqual(False, New Cts.Oasys.WinForm.User.ValidateDeletedUsersFactory().ImplementationA.ValidateDeletedUsers(userInstance))

    End Sub
    <TestMethod()> Public Sub UserSetAsDeletedPassed_ValidateDeletedUsers_ReturnsTrue()

        userInstance = New Core.System.User.User(PopulateData(True))
        Assert.AreEqual(True, New Cts.Oasys.WinForm.User.ValidateDeletedUsersFactory().ImplementationA.ValidateDeletedUsers(userInstance))
    End Sub
    <TestMethod()> Public Sub UserNotSetAsDeletedPassed_OldClass_DeletedUsersNotValidated_ValidateDeletedUsers_ReturnsFalse()

        userInstance = New Core.System.User.User(PopulateData(False))
        Assert.AreEqual(False, New Cts.Oasys.WinForm.User.ValidateDeletedUsersFactory().ImplementationB.ValidateDeletedUsers(userInstance))

    End Sub
    <TestMethod()> Public Sub UserSetAsDeletedPassed_OldClass_DeletedUsersNotValidated_ValidateDeletedUsers_ReturnsFalse()

        userInstance = New Core.System.User.User(PopulateData(True))
        Assert.AreEqual(False, New Cts.Oasys.WinForm.User.ValidateDeletedUsersFactory().ImplementationB.ValidateDeletedUsers(userInstance))
    End Sub

    Private Function PopulateData(ByVal SetAsDeleted As Boolean) As DataRow
        Dim DT As DataTable = New DataTable
        DT.Columns.Add("IsDeleted", System.Type.GetType("System.Boolean"))

        If SetAsDeleted Then
            DT.Rows.Add(True)
        Else
            DT.Rows.Add(False)
        End If

        Return DT.Rows(0)

    End Function


End Class
