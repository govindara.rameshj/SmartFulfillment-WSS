﻿<TestClass()> Public Class PasswordValidationUnitTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Unit Test - Function AcceptSupervisorPassword"

    <TestMethod()> Public Sub AcceptSupervisorPassword_RequirementOn_StandardPasswordSupervisorPasswordMatch_ReturnFalse()

        Dim PasswordValidation As IPasswordValidation

        ArrangeRequirementSwitchDependency(True)
        PasswordValidation = (New PasswordValidationFactory).GetImplementation

        Assert.IsFalse(PasswordValidation.AcceptSupervisorPassword("PasswordABCDE", "passwordabcde"))

    End Sub

    <TestMethod()> Public Sub AcceptwSupervisorPassword_RequirementOn_StandardPasswordSupervisorPasswordMisMatch_ReturnTrue()

        Dim PasswordValidation As IPasswordValidation

        ArrangeRequirementSwitchDependency(True)
        PasswordValidation = (New PasswordValidationFactory).GetImplementation

        Assert.IsTrue(PasswordValidation.AcceptSupervisorPassword("12345", "6789"))

    End Sub

    <TestMethod()> Public Sub AcceptSupervisorPassword_RequirementOff_StandardPasswordSupervisorPasswordMatch_ReturnTrue()

        Dim PasswordValidation As IPasswordValidation

        ArrangeRequirementSwitchDependency(False)
        PasswordValidation = (New PasswordValidationFactory).GetImplementation

        Assert.IsTrue(PasswordValidation.AcceptSupervisorPassword("Password12345", "password12345"))

    End Sub

    <TestMethod()> Public Sub AcceptSupervisorPassword_RequirementOff_StandardPasswordSupervisorPasswordMisMatch_ReturnTrue()

        Dim PasswordValidation As IPasswordValidation

        ArrangeRequirementSwitchDependency(False)
        PasswordValidation = (New PasswordValidationFactory).GetImplementation

        Assert.IsTrue(PasswordValidation.AcceptSupervisorPassword("12345", "6789"))

    End Sub

#End Region

#Region "Unit Test - Factory"

    <TestMethod()> Public Sub RequirementSwitchOn_ReturnEnabledPasswordValidation()

        Dim PasswordValidation As IPasswordValidation

        ArrangeRequirementSwitchDependency(True)
        PasswordValidation = (New PasswordValidationFactory).GetImplementation

        Assert.AreEqual("Cts.Oasys.WinForm.User.UniqueSupervisorPasswordEnforce", PasswordValidation.GetType.FullName)

    End Sub

    <TestMethod()> Public Sub RequirementSwitchOff_ReturnDisabledPasswordValidation()

        Dim PasswordValidation As IPasswordValidation

        ArrangeRequirementSwitchDependency(False)
        PasswordValidation = (New PasswordValidationFactory).GetImplementation
        Assert.AreEqual("Cts.Oasys.WinForm.User.UniqueSupervisorPasswordIgnore", PasswordValidation.GetType.FullName)

    End Sub

#End Region

#Region "Private Functions And Procedures"

    Private Sub ArrangeRequirementSwitchDependency(ByVal ReturnValue As Boolean)

        Dim Mock As New MockRepository
        Dim Requirement As IRequirementRepository

        Requirement = Mock.Stub(Of IRequirementRepository)()
        SetupResult.On(Requirement).Call(Requirement.IsSwitchPresentAndEnabled(Nothing)).Return(ReturnValue).IgnoreArguments()
        RequirementRepositoryFactory.FactorySet(Requirement)
        Mock.ReplayAll()

    End Sub

#End Region

End Class