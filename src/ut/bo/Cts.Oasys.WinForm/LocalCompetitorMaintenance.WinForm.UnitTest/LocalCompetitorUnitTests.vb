﻿Imports System
Imports System.Reflection
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting


<TestClass()> Public Class LocalCompetitorUnitTests

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Test Scenarios"
    Friend Class NewCompetitor

        Inherits LocalCompetitors

        Friend Overrides Function DoesCompetitorExist(ByVal Name As String) As Boolean
            Return False
        End Function

        Friend Overrides Function AddaCompetitor(ByVal Name As String) As Boolean
            Return DoesCompetitorExist(Name)
        End Function

        Friend Overrides Function MaintainaCompetitor(ByVal Id As Integer, ByVal Name As String) As Boolean
            Return DoesCompetitorExist(Name)
        End Function

        Friend Overrides Function DeleteaCompetitor(ByVal Id As Integer) As Boolean
            Return False
        End Function

    End Class

    Friend Class ExistingCompetitor

        Inherits LocalCompetitors

        Friend Overrides Function DoesCompetitorExist(ByVal Name As String) As Boolean
            Return True
        End Function

        Friend Overrides Function AddaCompetitor(ByVal Name As String) As Boolean
            Return False
        End Function

        Friend Overrides Function MaintainaCompetitor(ByVal Id As Integer, ByVal Name As String) As Boolean
            Return DoesCompetitorExist(Name)
        End Function

        Friend Overrides Function DeleteaCompetitor(ByVal Id As Integer) As Boolean
            Return True
        End Function

    End Class

#End Region

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()> Public Sub LocalCompetitor_CallAddCompetitor_ReturnsFalse()
        Dim Add As ILocalCompetitor = New ExistingCompetitor

        Assert.IsFalse(Add.AddCompetitor("Testing 123"))
    End Sub

    <TestMethod()> Public Sub LocalCompetitor_CallMaintainCompetitor_ReturnsFalse()
        Dim Add As ILocalCompetitor = New NewCompetitor

        Assert.IsFalse(Add.MaintainCompetitor(1, "Testing 123"))
    End Sub

    <TestMethod()> Public Sub LocalCompetitor_CallMaintainCompetitor_ReturnsTrue()
        Dim Add As ILocalCompetitor = New ExistingCompetitor

        Assert.IsTrue(Add.MaintainCompetitor(1, "Testing 122"))
    End Sub

    <TestMethod()> Public Sub LocalCompetitor_CallDeleteCompetitor_ReturnsTrue()
        Dim Add As ILocalCompetitor = New ExistingCompetitor

        Assert.IsTrue(Add.DeleteCompetitor(1))
    End Sub

End Class
