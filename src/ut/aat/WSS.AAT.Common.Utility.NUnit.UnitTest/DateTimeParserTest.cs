using System;
using WSS.AAT.Common.Utility.Utility;
using NUnit.Framework;

namespace WSS.AAT.Common.Utility.NUnit.UnitTest
{
    [TestFixture]
    public class DateTimeParserTest
    {
        [Test, ExpectedException(typeof(Exception))]
        public void TestValidDateSupplied()
        {
            DateTimeParser.Parse(DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        #region "Today"

        [Test]
        public void TestToday()
        {
            string controlDate = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("today"));
        }

        [Test]
        public void TestTodayAtFivePastThree()
        {
            string controlDate = DateTime.Today.AddHours(15).AddMinutes(5).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("today @ 15:05"));
        }

        [Test]
        public void TestTwoDaysAfterToday()
        {
            string controlDate = DateTime.Today.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("2 days after today"));
        }

        [Test]
        public void TestFourDaysAfterTodayAtTenPastElevenAndTwentyThreeSeconds()
        {
            string controlDate = DateTime.Today.AddDays(4).AddHours(11).AddMinutes(10).AddSeconds(23).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("4 days after today @ 11:10:23"));
        }

        [Test]
        public void TestOneWeekBeforeToday()
        {
            string controlDate = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("1 week before today"));
        }

        [Test]
        public void TestTOneMonthBeforeToday()
        {
            string controlDate = DateTime.Today.AddMonths(-1).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("1 month before today"));
        }

        [Test]
        public void TestThreeMonthsBeforeToday()
        {
            string controlDate = DateTime.Today.AddMonths(-3).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("3 months before today"));
        }

        #endregion

        #region "Tomorrow"

        [Test]
        public void TestTomorrow()
        {
            string controlDate = DateTime.Today.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("tomorrow"));
        }

        [Test]
        public void TestDayAfterTomorrow()
        {
            string controlDate = DateTime.Today.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("day after tomorrow"));
        }

        [Test]
        public void TestTwoDaysAfterTomorrow()
        {
            string controlDate = DateTime.Today.AddDays(3).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("2 days after tomorrow"));
        }

        [Test]
        public void TestThreeWeeksAfterTomorrow()
        {
            string controlDate = DateTime.Today.AddDays(22).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("3 weeks after tomorrow"));
        }

        #endregion

        #region "Yesterday"

        [Test]
        public void TestYesterday()
        {
            string controlDate = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("yesterday"));
        }

        [Test]
        public void TestDayBeforeYesterday()
        {
            string controlDate = DateTime.Today.AddDays(-2).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("day before yesterday"));
        }

        [Test]
        public void TestFiveDaysBeforeYesterday()
        {
            string controlDate = DateTime.Today.AddDays(-6).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("5 days before yesterday"));
        }

        [Test]
        public void TestWeekBeforeYesterday()
        {
            string controlDate = DateTime.Today.AddDays(-8).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("week before yesterday"));
        }

        [Test]
        public void TestFourWeeksBeforeYesterdayAtTwentyPastMidnightAndTwelveSeconds()
        {
            string controlDate = DateTime.Today.AddDays(-29).AddMinutes(20).AddSeconds(12).ToString("yyyy-MM-dd HH:mm:ss");

            Assert.AreEqual(controlDate, DateTimeParser.Parse("4 weeks before yesterday @ 00:20:12"));
        }

        #endregion

    }
}
