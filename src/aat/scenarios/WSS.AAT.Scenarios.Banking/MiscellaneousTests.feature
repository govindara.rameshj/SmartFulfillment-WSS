﻿Feature: MiscellaneousTests
@MiscellaneousTests

Scenario: Cash Drop Dropdown Does not Show Non Sale Cashiers
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 600 pounds
When The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
Then There is a record about 'Fred Bloggs' under Id '003' for today
Then There is a record about 'Bill Smith' under Id '004' for today