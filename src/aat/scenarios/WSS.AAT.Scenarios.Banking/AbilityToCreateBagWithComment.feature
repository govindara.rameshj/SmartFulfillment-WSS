﻿@AbilityToCreateBagWithComment
Feature: AbilityToCreateBagWithComment

Scenario: Cashier Report Shows Pickup Comment
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
When The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
Then Pickup Bag in Database with Seal Number '33333331' has comment value 'Pickup One'
And In Cashier Report Pickup Bag with Seal Number '33333331' displays Comment 'Pickup One'

Scenario: Create Cash Drop With Comment
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
When Cash Drop of 5600 pounds is done under Seal Number '92738463' with comment 'I done dropped my cash' by the Cashier 'Bill Smith'
Then There are comments 'Pickup Two' and 'I done dropped my cash' in Database for the Cashier 'Bill Smith'

Scenario: Create PickUp With Comment
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
When The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
Then Pickup Bag in Database with Seal Number '33333331' has comment value 'Pickup One'
And Pickup Bag in Database with Seal Number '33333332' has comment value 'Pickup Two'
And Pickup Bag in Database with Seal Number '33333333' has comment value 'Pickup Three'

Scenario: Recheck Float with Comment
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
When Float under Seal Number '11111111' is rechecked with comment 'Comment recheck A'
And Float under Seal Number '11111112' is rechecked with comment 'Comment recheck B'
And Float under Seal Number '11111113' is rechecked with comment 'Comment recheck C'
Then Float Bag in Database with Seal Number '11111111' has comment value 'Comment recheck A'
And Float Bag in Database with Seal Number '11111112' has comment value 'Comment recheck B'
And Float Bag in Database with Seal Number '11111113' has comment value 'Comment recheck C'

Scenario: Recheck PickUp with Comment
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
When PickUp under Seal Number '33333331' is rechecked with comment 'Comment recheck A'
And PickUp under Seal Number '33333332' is rechecked with comment 'Comment recheck B'
And PickUp under Seal Number '33333333' is rechecked with comment 'Comment recheck C'
Then Pickup Bag in Database with Seal Number '33333331' has comment value 'Comment recheck A'
And Pickup Bag in Database with Seal Number '33333332' has comment value 'Comment recheck B'
And Pickup Bag in Database with Seal Number '33333333' has comment value 'Comment recheck C'
