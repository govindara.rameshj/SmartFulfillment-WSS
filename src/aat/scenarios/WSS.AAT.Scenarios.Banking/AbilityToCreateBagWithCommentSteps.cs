﻿using Cts.Oasys.Core.SystemEnvironment;
using NUnit.Framework;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Banking;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using System.Linq;
using WSS.AAT.Common.Utility.Bindings;
using System.Globalization;

namespace WSS.AAT.Scenarios.Banking
{
    [Binding]
    public class AbilityToCreateBagWithCommentSteps : BaseStepDefinitions
    {
        private readonly IDataLayerFactory dataLayerFactory;
        ISystemEnvironment systemEnvironment;

        private DateTime currentDate;
        private SystemUser loggedUser;
        private SystemUser authoriser;

        private CoreBanking coreBanking;
        private BankingSuiteRepository repo;
        private BankingBagCollectionReport bankingBagCollectionReport;

        public AbilityToCreateBagWithCommentSteps(ISystemEnvironment systemEnvironment, IDataLayerFactory dataLayerFactory)
        {
            this.dataLayerFactory = dataLayerFactory;
            this.systemEnvironment = systemEnvironment;
            coreBanking = new CoreBanking(systemEnvironment);
            bankingBagCollectionReport = new BankingBagCollectionReport();

            repo = dataLayerFactory.Create<BankingSuiteRepository>();
            repo.ResetDataBeforetestRun();
        }

        [When(@"Logged User is '(.*)' and Authoriser was '(.*)'")]
        [Given(@"Logged User was '(.*)' and Authoriser was '(.*)'")]
        public void GivenLoggedUserNumberAndAuthoriserNumber(string loggedUserName, string authoriserName)
        {
            loggedUser = repo.GetSystemUserByName(loggedUserName);
            authoriser = repo.GetSystemUserByName(authoriserName);
            Thread.Sleep(1500);
            coreBanking.setLoggedOnUserAndAuthoriser(loggedUser.Id, authoriser.Id);
        }

        [Given(@"Banking Day was today")]
        public void GivenBankingDayWasToday()
        {
            currentDate = DateTime.Now;
            coreBanking.setBankingDate(currentDate);
        }

        [Given(@"Banking Day was yesterday")]
        public void GivenBankingDayWasYesterday()
        {
            currentDate = DateTime.Now.AddDays(-1);
            coreBanking.setBankingDate(currentDate);
        }

        [Given(@"Bankinking day is two days ago")]
        [When(@"Bankinking day is two days ago")]
        public void WhenBankinkingDayIsTwoDaysAgo()
        {
            coreBanking.setBankingDate(DateTime.Now.AddDays(-2));
        }

        [Given(@"Initial Safe was (.*) pounds")]
        public void GivenInitialSafeWasPounds(int safeValue)
        {
            coreBanking.createInitialSafeWithValue(safeValue);
            coreBanking.setSafeMaintenanceOnCurrentSafe(currentDate);
        }

        [Given(@"There was (.*) pounds in Main Safe and (.*) pounds in Change Safe")]
        public void GivenTereWasPoundsInMainSafeAndPoundsInChangeSafe(int mainSafeValue, int changeSafeValue)
        {
            var mainSafe = GetMainSafe(mainSafeValue);
            var changeSafe = GetChangeSafe(changeSafeValue);
            coreBanking.performSafeMaintenanceWithThisMainSafeAndThisChangeSafe(mainSafe, changeSafe);
        }

        [Given(@"Float of (.*) pounds was taken from Safe under Seal Number '(.*)'")]
        public void GivenFloatOfPoundsWasTakenFromSafeUnderSealNumber(int floatValue, string sealNumber)
        {
            string floatBag = GetFloatBagDenominationFromFloatValue(floatValue);

            coreBanking.createFloatFromSafeWithThisSealAndDenominations(sealNumber, floatBag);
        }

        [Given(@"The float was assigned to cashier '(.*)' under Seal Number '(.*)'")]
        public void GivenTheFloatWasAssignedToCashierUnderSealNumber(string cashierName, string sealNumber)
        {
            coreBanking.assignFloatUsingSealToCashier(sealNumber, cashierName);
        }

        [When(@"The cashier '(.*)' made sales with total cost of (.*) pounds")]
        [Given(@"The cashier '(.*)' made sales with total cost of (.*) pounds")]
        public void GivenTheCashierMadeSalesWithTotalCostOfPounds(string cashierName, int total)
        {
            coreBanking.createSaleForCashierWithBasketValue(cashierName, total.ToString());
        }

        [Given(@"One day was advanced")]
        public void GivenOneDayWasAdvanced()
        {
            coreBanking.advanceOneDay();
        }

        [Given(@"There was a Safe for today")]
        public void GivenThereWasASafeForToday()
        {
            coreBanking.safeCreatedToday();
        }

        [Given(@"The last Safe Bags line had ID '(.*)' in db")]
        public void GivenTheLastSafeBagsLineHadIDInDb(int identificator)
        {
            coreBanking.configureSafeBagsWithInitialIdentitySeed(identificator + 1);
        }

        [Given(@"It was non-trading day")]
        public void GivenItWasNon_TradingDay()
        {
            coreBanking.createNonTradingDay();
        }

        [Given(@"Paid Out for Customer Concern with Value (.*) nad with Invoice Number '(.*)' was made by Cashier '(.*)'")]
        public void GivenPaidOutForCustomerConcernWithValueNadWithInvoiceNumberWasMadeByCashier(decimal paidOutValue, string invoiceNumber, string cashierName)
        {
            coreBanking.createPaidOutForCustomerConcernForCashierWithValueWithInvoiceNumber(cashierName, paidOutValue, invoiceNumber);
        }

        [Given(@"Paid Out for Stationery with Value (.*) nad with Invoice Number '(.*)' was made by Cashier '(.*)'")]
        public void GivenPaidOutForStationeryWithValueNadWithInvoiceNumberWasMadeByCashier(decimal paidOutValue, string invoiceNumber, string cashierName)
        {
            coreBanking.createPaidOutForStationeryForCashierWithValueWithInvoiceNumber(cashierName, paidOutValue, invoiceNumber);
        }

        [Given(@"Paid Out Correction for Breakfast Voucher with Value (.*) nad with Invoice Number '(.*)' was made by Cashier '(.*)'")]
        public void GivenPaidOutCorrectionForBreakfastVoucherWithValueNadWithInvoiceNumberWasMadeByCashier(decimal paidOutValue, string invoiceNumber, string cashierName)
        {
            coreBanking.createPaidOutCorrectionForBreakfastVoucherForCashierWithValueWithInvoiceNumber(cashierName, paidOutValue, invoiceNumber);
        }

        [Given(@"Paid Out Correction for Social Fund with Value (.*) nad with Invoice Number '(.*)' was made by Cashier '(.*)'")]
        public void GivenPaidOutCorrectionForSocialFundWithValueNadWithInvoiceNumberWasMadeByCashier(decimal paidOutValue, string invoiceNumber, string cashierName)
        {
            coreBanking.createPaidOutCorrectionForSocialFundForCashierWithValueWithInvoiceNumber(cashierName, paidOutValue, invoiceNumber);
        }

        [Given(@"Paid Out for Travel with Value (.*) nad with Invoice Number '(.*)' was made by Cashier '(.*)'")]
        public void GivenPaidOutForTravelWithValueNadWithInvoiceNumberWasMadeByCashier(decimal paidOutValue, string invoiceNumber, string cashierName)
        {
            coreBanking.createPaidOutForTravelForCashierWithValueWithInvoiceNumber(cashierName, paidOutValue, invoiceNumber);
        }

        [Given(@"Paid Out Correction for Travel with Value (.*) nad with Invoice Number '(.*)' was made by Cashier '(.*)'")]
        public void GivenPaidOutCorrectionForTravelWithValueNadWithInvoiceNumberWasMadeByCashier(decimal paidOutValue, string invoiceNumber, string cashierName)
        {
            coreBanking.createPaidOutCorrectionForTravelForCashierWithValueWithInvoiceNumber(cashierName, paidOutValue, invoiceNumber);
        }

        [Given(@"Miscellaneous Income For Bad Cheque with Value (.*) nad with Invoice Number '(.*)' was made by Cashier '(.*)'")]
        public void GivenMiscellaneousIncomeForBadChequeWithValueNadWithInvoiceNumberWasMadeByCashier(decimal paidOutValue, string invoiceNumber, string cashierName)
        {
            coreBanking.createMiscellaneousIncomeForBadChequeForCashierWithValueWithInvoiceNumber(cashierName, paidOutValue, invoiceNumber);
        }

        [Given(@"Miscellaneous Income Correction For Social Fund with Value (.*) nad with Invoice Number '(.*)' was made by Cashier '(.*)'")]
        public void GivenMiscellaneousIncomeCorrectionForSocialFundWithValueNadWithInvoiceNumberWasMadeByCashier(decimal paidOutValue, string invoiceNumber, string cashierName)
        {
            coreBanking.createMiscellaneousIncomeCorrectionForSocialFundForCashierWithValueWithInvoiceNumber(cashierName, paidOutValue, invoiceNumber);
        }

        [Given(@"There was a bag with Value (.*) and with Seal Number '(.*)' for period (.*) and date (.*) with comment '(.*)'")]
        public void GivenThereWasABagWithValueAndWithSealNumberForPeriodAndDateWithComment(decimal value, string sealNumber, int period, string date, string comment)
        {
            IFormatProvider culture = new System.Globalization.CultureInfo("en-GB", true);
            bankingBagCollectionReport.addItemForPeriodAndDateWithBagSealNumberAndBagValueWithComment(period, DateTime.ParseExact(date, @"dd/MM/yyyy", CultureInfo.InvariantCulture), sealNumber, value, comment);
        }

        [Given(@"Collection Slip '(.*)' was set")]
        public void GivenCollectionSlipWasSet(string collectionSlip)
        {
            bankingBagCollectionReport.setCollectionSlip(collectionSlip);
        }

        [Given(@"Comment '(.*)' was set")]
        public void GivenCommentWasSet(string comment)
        {
            bankingBagCollectionReport.setComment(comment);
        }

        [Given(@"End of Day Check Process for today was started")]
        public void GivenEndOfDayCheckProcessForTodayWasStarted()
        {
            coreBanking.startEndOfDayCheckProcessForToday();
        }

        [Given(@"Valid Pickup Bag under Seal Number '(.*)' was scanned")]
        public void GivenValidPickupBagUnderSealNumberWasScanned(string sealNumber)
        {
            coreBanking.scanValidBankingBag(sealNumber);
        }

        [Given(@"End Of Day Check Process was completed")]
        public void GivenEndOfDayCheckProcessWasCompleted()
        {
            coreBanking.completeEndOfDayCheckProcess();
        }

        [When(@"All floats were assigned by the authoriser")]
        [Given(@"All floats were assigned by the authoriser")]
        public void GivenAllFloatsWereAssignedByTheAuthoriser()
        {
            Assert.AreEqual("OK", coreBanking.allFloatsAssignedByAuthorizer(authoriser.EmployeeName).FirstOrDefault());
        }

        [Given(@"Banking Day was closed")]
        [When(@"Banking Day was closed")]
        public void GivenBankingDayWasClosed()
        {
            coreBanking.initialiseEndOfDayBanking();
        }

        [Given(@"Safe was locked from (.*) of today till (.*) of tomorrow by Manager '(.*)' and witnessed by '(.*)'")]
        public void GivenSafeWasLockedFromOfTodayTillOfTomorrowByManagerAndWitnessedBy(string timeOfStart, string timeOfEnd, string managerName, string witnessName)
        {
            coreBanking.safeLockedFromDateAndTimeToDateAndTimeByManagerAndWitnessedBy(DateTime.Now.Date, timeOfStart, DateTime.Now.AddDays(1).Date, timeOfEnd, managerName, witnessName);
        }

        [Given(@"Banking Day was closed with Seal Number '(.*)' and Slip Number '(.*)' with comment '(.*)' and Banking Value of (.*)")]
        [When(@"Banking Day was closed with Seal Number '(.*)' and Slip Number '(.*)' with comment '(.*)' and Banking Value of (.*)")]
        public void WhenBankingDayWasClosedWithSealNumberAndSlipNumberWithCommentAndBankingValueOf(string seal, string slip, string comment, int value)
        {
            var bankingValue = GetBankingValue(value);

            coreBanking.performEndOfDayBankingCashBagOnlyWithThisSealThisSlipThisCommentThisValue(seal, slip, comment, bankingValue);
        }

        [When(@"The cashier '(.*)' makes a (.*) pounds pickup under Seal Number '(.*)' from a Float under Seal Number '(.*)' with comment '(.*)' with value (.*)")]
        [Given(@"The cashier '(.*)' makes a (.*) pounds pickup under Seal Number '(.*)' from a Float under Seal Number '(.*)' with comment '(.*)' with value (.*)")]
        public void GivenTheCashierMakesAPoundsPickupUnderSealNumberFromAFloatUnderSealNumberWithCommentWithValue(string cashierName, int pickupValue, string pickupSealNumber, string floatSealNumber, string comment, int floatValue)
        {
            var cashier = repo.GetSystemUserByName(cashierName);

            var floatValueDenom = GetFloatBagDenominationFromFloatValue(floatValue);
            var pickupValueDenom = GetPickUpDenominationFromPickUpValue(pickupValue);

            coreBanking.configureFloatCreatedFromPickupWithThisSealAndDenominations(floatSealNumber, floatValueDenom);
            coreBanking.configurePickupWithThisSealAndDenominations(pickupSealNumber, pickupValueDenom);
            coreBanking.createPickupAndFloatForThisCashierWithComment(cashier.Id, comment);
        }

        [When(@"Cash Drop of (.*) pounds is done under Seal Number '(.*)' with comment '(.*)' by the Cashier '(.*)'")]
        public void WhenCashDropOfPoundsIsDoneUnderSealNumberWithCommentByTheCashier(int cashDropValue, string cashDropSealNumber, string comment, string cashierName)
        {
            coreBanking.addCashDropForCashierSealIsValueIsCommentIs(cashierName, cashDropSealNumber, GetFloatBagDenominationFromFloatValue(cashDropValue), comment);
        }

        [When(@"Float under Seal Number '(.*)' is rechecked with comment '(.*)'")]
        public void WhenFloatUnderSealNumberIsRecheckedWithComment(string floatSealNumber, string comment)
        {
            coreBanking.recheckFloatSealIsCommentIs(floatSealNumber, comment);
        }

        [When(@"PickUp under Seal Number '(.*)' is rechecked with comment '(.*)'")]
        public void WhenPickUpUnderSealNumberIsRecheckedWithComment(string floatSealNumber, string comment)
        {
            coreBanking.recheckPickupSealIsCommentIs(floatSealNumber, comment);
        }

        [Given(@"Banking File For Head Office without Cost Code is created")]
        [When(@"Banking File For Head Office without Cost Code is created")]
        public void WhenBankingFileForHeadOfficeWithoutCostCodeIsCreated()
        {
            coreBanking.createBankingFileForHeadOfficeWithoutCostCode();
        }

        [When(@"Report was prepared")]
        public void WhenReportWasPrepared()
        {
            bankingBagCollectionReport.prepareReport();
        }

        [When(@"End of Day Check Report Selected Date is the day before yesterday")]
        public void WhenEndOfDayCheckReportSelectedDateIsTheDayBeforeYesterday()
        {
            coreBanking.endOfDayCheckReportSelectedDateIs(DateTime.Now.Date.AddDays(-2));
        }

        [When(@"End of Day Check Report Selected Date is yesterday")]
        public void WhenEndOfDayCheckReportSelectedDateIsYesterday()
        {
            coreBanking.endOfDayCheckReportSelectedDateIs(DateTime.Now.Date.AddDays(-1));
        }

        [When(@"End of Day Check Report Selected Date is today")]
        public void WhenEndOfDayCheckReportSelectedDateIsToday()
        {
            coreBanking.endOfDayCheckReportSelectedDateIs(DateTime.Now.Date);
        }

        [Then(@"Pickup Bag in Database with Seal Number '(.*)' has comment value '(.*)'")]
        public void ThenPickupBagInDatabaseWithSealNumberHasCommentValue(string sealNumber, string comment)
        {
            Assert.AreEqual(comment, coreBanking.pickupBagInDatabaseWithThisSealHasThisCommentValue(sealNumber));
        }

        [Then(@"In Cashier Report Pickup Bag with Seal Number '(.*)' displays Comment '(.*)'")]
        public void ThenInCashierReportPickupBagWithSealNumberDisplaysComment(string sealNumber, string comment)
        {
            Assert.AreEqual(comment, coreBanking.inCashierReportPickupBagWithThisSealDisplaysThisCommentValue(sealNumber));
        }

        [Then(@"There are comments '(.*)' and '(.*)' in Database for the Cashier '(.*)'")]
        public void ThenThereAreCommentsAndInDatabaseForTheCashier(string pickUpComment, string cashDropComment, string cashierName)
        {
            var comments = coreBanking.bagCommentsOnDatabaseForCashierHasThisCommentValue(cashierName);

            Assert.IsNotNull(comments);
            Assert.AreEqual(pickUpComment, comments[0]);
            Assert.AreEqual(cashDropComment, comments[1]);
        }

        [Then(@"Float Bag in Database with Seal Number '(.*)' has comment value '(.*)'")]
        public void ThenFloatBagInDatabaseWithSealNumberHasCommentValue(string sealNumber, string comment)
        {
            Assert.AreEqual(comment, coreBanking.floatBagInDatabaseWithThisSealHasThisCommentValue(sealNumber));
        }

        [Then(@"There is a record about '(.*)' under Id '(.*)' for today")]
        public void ThenThereIsARecordAboutUnderIdForToday(string cashierName, string cashierId)
        {
            string cashierLine = string.Format("{0} - {1}", cashierId, cashierName);

            var cashiersList = coreBanking.cashDropDropdownHasCashiersForToday();

            Assert.IsTrue(cashiersList.Any(x => x == cashierLine));
        }

        [Then(@"Bad Safe Does Not Exist")]
        public void ThenBadSafeDoesNotExist()
        {
            Assert.IsTrue(coreBanking.badSafeDoesNotExist());
        }

        [Then(@"Safe has Total (.*)")]
        public void ThenSafeHasTotal(int total)
        {
            string expectedSafe = GetSafeTotal(total);
            string actualSafe = coreBanking.safeHasCorrectTotal();
            Assert.AreEqual(expectedSafe, actualSafe);
        }

        [Then(@"Banking Bag with Seal Number '(.*)' is created today")]
        public void ThenBankingBagWithSealNumberIsCreatedToday(string sealNumber)
        {
            string bagCreationDay = coreBanking.bankingBagWithThisSealWasCreatedToday(sealNumber);
            Assert.AreEqual("Today", bagCreationDay);
        }

        [Then(@"Banking Bag with Seal Number '(.*)' is Manager Checked today")]
        public void ThenBankingBagWithSealNumberIsManagerCheckedToday(string sealNumber)
        {
            string bagCheckedToday = coreBanking.bankingBagWithThisSealThenManagerCheckedToday(sealNumber);
            Assert.AreEqual("Today", bagCheckedToday);
        }

        [Then(@"After being Manager Checked the Banking Bag with Seal Number '(.*)' is destroyed today")]
        public void ThenAfterBeingManagerCheckedTheBankingBagWithSealNumberIsDestroyedToday(string sealNumber)
        {
            string bagDestroyedToday = coreBanking.bankingBagWithThisSealAfterBeingManagerCheckedWasDestoryedToday(sealNumber);
            Assert.AreEqual("Today", bagDestroyedToday);
        }

        [Then(@"Banking File '(.*)' for Account Number (.*) has value (.*)")]
        public void ThenBankingFileSHTOAForAccountNumberHasValue(string fileName, string accountNumber, string value)
        {
            var accountValues = coreBanking.bankingFileForAccountNumberHasValues(fileName, accountNumber);

            foreach (var accountValue in accountValues)
            {
                Assert.AreEqual(value, accountValue);
            }
        }

        [Then(@"Banking File '(.*)' has Value (.*) for MiscIn Bad Cheque Account '(.*)'")]
        [Then(@"Banking File '(.*)' has Value (.*) for MiscIn Social Fund Account '(.*)'")]
        [Then(@"Banking File '(.*)' has Value (.*) for MiscOut Breakfast Voucher Account '(.*)'")]
        [Then(@"Banking File '(.*)' has Value (.*) for MiscOut Social Fund Account '(.*)'")]
        [Then(@"Banking File '(.*)' has Value (.*) for MiscOut Stationery Account '(.*)'")]
        [Then(@"Banking File '(.*)' has Value (.*) for MiscOut Customer Concern Account '(.*)'")]
        public void ThenBankingFileHasValueForMiscOutCustomerConcernAccount(string fileName, string value, string accountNumber)
        {
            Assert.AreEqual(value, coreBanking.bankingFileForAccountNumberHasValues(fileName, accountNumber)[0]);
        }

        [Then(@"Banking File '(.*)' does not have MiscOut for Travel Account Account '(.*)'")]
        public void ThenBankingFileDoesNotHaveMiscOutForTravelAccountAccount(string fileName, string accountNumber)
        {
            Assert.AreEqual("true", coreBanking.bankingFileForAccountNumberDoesNotExist(fileName, accountNumber));
        }

        [Then(@"Report with next data is displayed")]
        public void ThenReportWithNextDataIsDisplayed(Table expectedTable)
        {
            int rowNumber = 0;

            foreach (var row in expectedTable.Rows)
            {
                if (row["Bag Collection"] == "true")
                {
                    Assert.IsTrue(bankingBagCollectionReport.getCheckBoxValueForRow(rowNumber));
                }
                else
                {
                    Assert.IsFalse(bankingBagCollectionReport.getCheckBoxValueForRow(rowNumber));
                }

                //Assert.AreEqual(row["Banking Day"], bankingBagCollectionReport.getValueForRowNumberColumnNumber(rowNumber, 1));
                Assert.AreEqual(row["Banking Seal"], bankingBagCollectionReport.getValueForRowNumberColumnNumber(rowNumber, 2));
                Assert.AreEqual(row["Value"], bankingBagCollectionReport.getValueForRowNumberColumnNumber(rowNumber, 3));
                Assert.AreEqual(row["Comment"], bankingBagCollectionReport.getValueForRowNumberColumnNumber(rowNumber, 4));

                rowNumber++;
            }
        }

        [Then(@"Banking Bag Collection Report Collection Slip is '(.*)'")]
        public void ThenBankingBagCollectionReportCollectionSlipIs(string slip)
        {
            Assert.AreEqual(slip, bankingBagCollectionReport.getValueForRowNumberColumnNumber(3, 1));
        }

        [Then(@"Banking Bag Collection Report Comment is '(.*)'")]
        public void ThenBankingBagCollectionReportCommentIs(string comment)
        {
            Assert.AreEqual(comment, bankingBagCollectionReport.getValueForRowNumberColumnNumber(4, 1));
        }

        [Then(@"Sales for '(.*)' during the period from a week ago to a week today has values (.*)")]
        public void ThenSalesForDuringThePeriodFromAWeekAgoToAWeekTodayHasValues(string cashierName, string value)
        {
            Assert.AreEqual(value, coreBanking.salesOnDatabaseForCashierWithStartDateAndEndDateHasValue(cashierName, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7)));
        }

        [Then(@"Number of assigned bags is (.*)")]
        public void ThenNumberOfAssignedBagsIs(int bagsCount)
        {
            Assert.AreEqual(bagsCount, coreBanking.numberOfUnScannedBagsAre());
        }

        [Then(@"List of Unscanned Bags is empty")]
        public void ThenListOfUnscannedBagsIsEmpty()
        {
            string unscannedBags = coreBanking.unscannedBagsAre();

            Assert.AreEqual(string.Empty, unscannedBags);
        }

        [Then(@"List of Unscanned Bags contains next Seal Numbers: '(.*)'")]
        public void ThenListOfUnscannedBagsContainsNextSealNumbers(string sealNumber)
        {
            string unscannedBags = coreBanking.unscannedBagsAre();

            Assert.AreEqual(sealNumber, unscannedBags);
        }

        [Then(@"Refund Report has Column Name '(.*)' for Index (.*)")]
        public void ThenRefundReportHasColumnNameForIndex(string columnName, int columnIndex)
        {
            Assert.AreEqual(columnName, coreBanking.refundReportHasColumnNameForIndex(columnIndex));
        }

        private string GetFloatBagDenominationFromFloatValue(int floatValue)
        {
            switch (floatValue)
            {
                case 95:
                    return BankingSuiteConstants.FloatBagOne;
                case 100:
                    return BankingSuiteConstants.FloatBagTwo;
                case 105:
                    return BankingSuiteConstants.FloatBagThree;
                case 5600:
                    return BankingSuiteConstants.CashDropValue;
                default:
                    return string.Empty;
            }
        }

        private string GetPickUpDenominationFromPickUpValue(int floatValue)
        {
            switch (floatValue)
            {
                case 1600:
                    return BankingSuiteConstants.DayOnePickupOne;
                case 1050:
                    return BankingSuiteConstants.DayOnePickupTwo;
                case 250:
                    return BankingSuiteConstants.DayOnePickupThree;
                case 1250:
                    return BankingSuiteConstants.DayTwoPickupOne;
                case 1350:
                    return BankingSuiteConstants.DayTwoPickupTwo;
                case 800:
                    return BankingSuiteConstants.DayTwoPickupThree;
                default:
                    return string.Empty;
            }
        }

        private string GetSafeTotal(int safeValue)
        {
            switch (safeValue)
            {
                case 5600:
                    return BankingSuiteConstants.GoodSafeDayOne;
                case 7450:
                    return BankingSuiteConstants.GoodSafeDayTwo;
                default:
                    return string.Empty;
            }
        }

        private string GetMainSafe(int mainSafe)
        {
            switch (mainSafe)
            {
                case 9500:
                    return BankingSuiteConstants.MainSafeDayOne;
                case 5090:
                    return BankingSuiteConstants.MainSafeDayTwo;
                default:
                    return string.Empty;
            }
        }

        private string GetChangeSafe(int changeSafe)
        {
            switch (changeSafe)
            {
                case 500:
                    return BankingSuiteConstants.ChangeSafeDayOne;
                case 510:
                    return BankingSuiteConstants.ChangeSafeDayTwo;
                default:
                    return string.Empty;
            }
        }

        private string GetBankingValue(int bankingValue)
        {
            switch (bankingValue)
            {
                case 7000:
                    return BankingSuiteConstants.DayOneBankingValue;
                case 1500:
                    return BankingSuiteConstants.DayTwoBankingValue;
                default:
                    return string.Empty;
            }
        }
    }
}
