﻿Feature: ReportTests
@ReportTests

Scenario: Banking Bag Collection Report
Given There was a bag with Value 4455.00 and with Seal Number '777-77777777-7' for period 567 and date 02/03/2013 with comment ''
And There was a bag with Value 2670.00 and with Seal Number '123' for period 777 and date 06/07/2008 with comment 'test item comment'
And Collection Slip '13131' was set
And Comment 'Test Comment' was set
When Report was prepared
Then Report with next data is displayed
| Bag Collection | Banking Day    | Banking Seal   | Value   | Comment           |
| true           | 567:03/02/2013 | 777-77777777-7 | 4455.00 |                   |
| false          | 777:07/06/2008 | 123            | 2670.00 | test item comment |
And Banking Bag Collection Report Collection Slip is '13131'
And Banking Bag Collection Report Comment is 'Test Comment'

Scenario: Cashier Performance Report Displays Null Values Correctly
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
When The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
Then Sales for 'Fred Bloggs' during the period from a week ago to a week today has values 1600.00 
And Sales for 'Bill Smith' during the period from a week ago to a week today has values 0.00 

Scenario: End Of Day Check Report with UnScanned Bags Displayed On Report for Multiday Scenario for the First Day
Given The last Safe Bags line had ID '9545' in db
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And End of Day Check Process for today was started
And Valid Pickup Bag under Seal Number '33333331' was scanned
And Valid Pickup Bag under Seal Number '33333332' was scanned
And Valid Pickup Bag under Seal Number '33333333' was scanned
And Safe was locked from 20:45 of today till 06:30 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
And The cashier 'Fred Bloggs' makes a 1250 pounds pickup under Seal Number '55555551' from a Float under Seal Number '44444441' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1350 pounds pickup under Seal Number '55555552' from a Float under Seal Number '44444442' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 800 pounds pickup under Seal Number '55555553' from a Float under Seal Number '44444443' with comment 'Pickup Three' with value 105
And End of Day Check Process for today was started
And Safe was locked from 21:00 of today till 06:45 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And Banking Day was yesterday
And There was 5090 pounds in Main Safe and 510 pounds in Change Safe
And Bankinking day is two days ago
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking Day was yesterday
And Banking Day was closed with Seal Number '777-77777777-7' and Slip Number '456' with comment 'Day Two Banking Comment' and Banking Value of 1500
And End of Day Check Process for today was started
And Valid Pickup Bag under Seal Number '666-66666666-6' was scanned
And Safe was locked from 20:45 of today till 06:30 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
When End of Day Check Report Selected Date is the day before yesterday
Then Number of assigned bags is 0
And List of Unscanned Bags is empty

Scenario: End Of Day Check Report with UnScanned Bags Displayed On Report for Multiday Scenario for the Second Day
Given The last Safe Bags line had ID '9545' in db
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And End of Day Check Process for today was started
And Valid Pickup Bag under Seal Number '33333331' was scanned
And Valid Pickup Bag under Seal Number '33333332' was scanned
And Valid Pickup Bag under Seal Number '33333333' was scanned
And Safe was locked from 20:45 of today till 06:30 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
And The cashier 'Fred Bloggs' makes a 1250 pounds pickup under Seal Number '55555551' from a Float under Seal Number '44444441' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1350 pounds pickup under Seal Number '55555552' from a Float under Seal Number '44444442' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 800 pounds pickup under Seal Number '55555553' from a Float under Seal Number '44444443' with comment 'Pickup Three' with value 105
And End of Day Check Process for today was started
And Safe was locked from 21:00 of today till 06:45 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And Banking Day was yesterday
And There was 5090 pounds in Main Safe and 510 pounds in Change Safe
And Bankinking day is two days ago
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking Day was yesterday
And Banking Day was closed with Seal Number '777-77777777-7' and Slip Number '456' with comment 'Day Two Banking Comment' and Banking Value of 1500
And End of Day Check Process for today was started
And Valid Pickup Bag under Seal Number '666-66666666-6' was scanned
And Safe was locked from 20:45 of today till 06:30 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
When End of Day Check Report Selected Date is yesterday
Then Number of assigned bags is 6
And List of Unscanned Bags contains next Seal Numbers: '33333331 33333332 33333333 55555551 55555552 55555553'

Scenario: End Of Day Check Report with UnScanned Bags Displayed On Report for Multiday Scenario for the Third Day
Given The last Safe Bags line had ID '9545' in db
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And End of Day Check Process for today was started
And Valid Pickup Bag under Seal Number '33333331' was scanned
And Valid Pickup Bag under Seal Number '33333332' was scanned
And Valid Pickup Bag under Seal Number '33333333' was scanned
And Safe was locked from 20:45 of today till 06:30 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
And The cashier 'Fred Bloggs' makes a 1250 pounds pickup under Seal Number '55555551' from a Float under Seal Number '44444441' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1350 pounds pickup under Seal Number '55555552' from a Float under Seal Number '44444442' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 800 pounds pickup under Seal Number '55555553' from a Float under Seal Number '44444443' with comment 'Pickup Three' with value 105
And End of Day Check Process for today was started
And Safe was locked from 21:00 of today till 06:45 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And Banking Day was yesterday
And There was 5090 pounds in Main Safe and 510 pounds in Change Safe
And Bankinking day is two days ago
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking Day was yesterday
And Banking Day was closed with Seal Number '777-77777777-7' and Slip Number '456' with comment 'Day Two Banking Comment' and Banking Value of 1500
And End of Day Check Process for today was started
And Valid Pickup Bag under Seal Number '666-66666666-6' was scanned
And Safe was locked from 20:45 of today till 06:30 of tomorrow by Manager 'Amy Murphy' and witnessed by 'George Hunt'
And End Of Day Check Process was completed
When End of Day Check Report Selected Date is today
Then Number of assigned bags is 1
And List of Unscanned Bags contains next Seal Numbers: '777-77777777-7'

Scenario: End Of Day Check Report with UnScanned Bags Displayed On Report for Singleday Scenario for the First Day
Given The last Safe Bags line had ID '9545' in db
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
When End of Day Check Report Selected Date is today
Then Number of assigned bags is 0
And List of Unscanned Bags is empty

Scenario: End Of Day Check Report with UnScanned Bags Displayed On Report for Singleday Scenario for the Second Day
Given The last Safe Bags line had ID '9545' in db
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And One day was advanced
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
When End of Day Check Report Selected Date is today
Then Number of assigned bags is 3
And List of Unscanned Bags contains next Seal Numbers: '33333331 33333332 33333333'

Scenario: Refund Report
When Logged User is 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
Then Refund Report has Column Name 'OVC TRAN' for Index 3
Then Refund Report has Column Name 'AMOUNT' for Index 14