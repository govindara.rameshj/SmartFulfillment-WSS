﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.AAT.Scenarios.Banking
{
    public static class BankingSuiteConstants
    {
        public const string MainSafeDayOne = "\u00a39500 (\u00a350:100,\u00a320:100,\u00a310:200,\u00a35:100)";
        public const string ChangeSafeDayOne = "\u00a3500 (\u00a32:100,\u00a31:200,50p:60,20p:200,10p:150,5p:100,2p:200,1p:600)";
        public const string MainSafeDayTwo = "\u00a35090 (\u00a350:81,\u00a320:18,\u00a310:51,\u00a35:34)";
        public const string ChangeSafeDayTwo = "\u00a3510 (\u00a32:105,\u00a31:221,50p:60,20p:200,5p:100,2p:200)";

        //CashierOneCode   {003
        //CashierTwoCode   {004
        //CashierThreeCode {005

        public const string CashDropValue = "\u00a35600 (\u00a350:81,\u00a320:18,\u00a310:51,\u00a35:34,\u00a32:105,\u00a31:221,50p:60,20p:200,5p:100,2p:200)";

        public const string FloatBagOne = "\u00a395 (\u00a310:2,\u00a35:6,\u00a32:10,\u00a31:18,10p:50,1p:200)";
        public const string FloatBagTwo = "\u00a3100 (\u00a310:2,\u00a35:6,\u00a32:10,\u00a31:23,10p:50,1p:200)";
        public const string FloatBagThree = "\u00a3105 (\u00a310:2,\u00a35:6,\u00a32:10,\u00a31:28,10p:50,1p:200)";

        public const string DayOnePickupOne = "\u00a31600 (\u00a350:12,\u00a320:12,\u00a310:46,\u00a35:40,\u00a32:30,\u00a31:40)";
        public const string DayOnePickupTwo = "\u00a31050 (\u00a350:19,\u00a320:1,\u00a310:1,\u00a35:2,\u00a32:5,\u00a31:50)";
        public const string DayOnePickupThree = "\u00a3250 (\u00a320:5,\u00a310:10,\u00a35:10)";


        public const string DayTwoPickupOne = "\u00a31250 (\u00a350:25)";
        public const string DayTwoPickupTwo = "\u00a31300 (\u00a350:26)";
        public const string DayTwoPickupThree = "\u00a3800 (\u00a350:16)";

        public const string GoodSafeDayOne = "\u00a35600 (\u00a350:81,\u00a320:18,\u00a310:51,\u00a35:34,\u00a32:105,\u00a31:221,50p:60,20p:200,5p:100,2p:200)";
        public const string GoodSafeDayTwo = "\u00a37450 (\u00a350:118,\u00a320:18,\u00a310:51,\u00a35:34,\u00a32:105,\u00a31:221,50p:60,20p:200,5p:100,2p:200)";

        public const string DayOneBankingValue = "\u00a37000 (\u00a350:50,\u00a320:100,\u00a310:200,\u00a35:100)";

        public const string DayTwoBankingValue = "\u00a31500 (\u00a350:30)";
        public const string DayTwoBankingSeal = "777-77777777-7";
        public const string DayTwoBankingSlip = "456";
        public const string DayTwoBankingComment = "Day Two Banking Comment";
    }
}
    

