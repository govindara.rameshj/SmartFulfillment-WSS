﻿Feature: PerformBankingProcess
@PerformBankingProcess

Scenario: Banking After Non Trading Day for Banking for Day One
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And One day was advanced
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
When Bankinking day is two days ago
And Banking Day was closed
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking File For Head Office without Cost Code is created
Then Bad Safe Does Not Exist
And Safe has Total 5600
And Banking Bag with Seal Number '666-66666666-6' is created today
And Banking Bag with Seal Number '666-66666666-6' is Manager Checked today
And After being Manager Checked the Banking Bag with Seal Number '666-66666666-6' is destroyed today
And Banking File 'STHOA' for Account Number 9001 has value 5900.00

Scenario: Banking After Non Trading Day for Banking For Today
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And One day was advanced
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
And Bankinking day is two days ago
And Banking Day was closed
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking File For Head Office without Cost Code is created
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was 5090 pounds in Main Safe and 510 pounds in Change Safe
And The cashier 'Fred Bloggs' makes a 1250 pounds pickup under Seal Number '55555551' from a Float under Seal Number '44444441' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1350 pounds pickup under Seal Number '55555552' from a Float under Seal Number '44444442' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 800 pounds pickup under Seal Number '55555553' from a Float under Seal Number '44444443' with comment 'Pickup Three' with value 105
When Banking Day was closed
And Banking Day was closed with Seal Number '777-77777777-7' and Slip Number '456' with comment 'Day Two Banking Comment' and Banking Value of 1500
And Banking File For Head Office without Cost Code is created
Then Bad Safe Does Not Exist
And Safe has Total 7450
And Banking Bag with Seal Number '777-77777777-7' is created today
And Banking Bag with Seal Number '777-77777777-7' is Manager Checked today
And After being Manager Checked the Banking Bag with Seal Number '777-77777777-7' is destroyed today
And Banking File 'STHOA' for Account Number 9001 has value 7750.00

Scenario: Banking On Non Trading Day
Given The last Safe Bags line had ID '9544' in db
Given Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
And The cashier 'Fred Bloggs' makes a 1250 pounds pickup under Seal Number '55555551' from a Float under Seal Number '44444441' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1350 pounds pickup under Seal Number '55555552' from a Float under Seal Number '44444442' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 800 pounds pickup under Seal Number '55555553' from a Float under Seal Number '44444443' with comment 'Pickup Three' with value 105
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And It was non-trading day
When Bankinking day is two days ago
And Banking Day was closed
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking File For Head Office without Cost Code is created
Then Bad Safe Does Not Exist
And Safe has Total 5600
And Banking Bag with Seal Number '666-66666666-6' is created today
And Banking Bag with Seal Number '666-66666666-6' is Manager Checked today
And After being Manager Checked the Banking Bag with Seal Number '666-66666666-6' is destroyed today
And Banking File 'STHOA' for Account Number 9001 has value 5900.00

Scenario: Banking On Trading Day For Banking On day One
Given The last Safe Bags line had ID '9544' in db
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And All floats were assigned by the authoriser
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
And The cashier 'Fred Bloggs' makes a 1250 pounds pickup under Seal Number '55555551' from a Float under Seal Number '44444441' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1350 pounds pickup under Seal Number '55555552' from a Float under Seal Number '44444442' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 800 pounds pickup under Seal Number '55555553' from a Float under Seal Number '44444443' with comment 'Pickup Three' with value 105
And All floats were assigned by the authoriser
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '44444441'
And The float was assigned to cashier 'Bill Smith' under Seal Number '44444442'
And The float was assigned to cashier 'George Hunt' under Seal Number '44444443'
And The cashier 'Fred Bloggs' made sales with total cost of 800 pounds
And The cashier 'Bill Smith' made sales with total cost of 1650 pounds
And The cashier 'George Hunt' made sales with total cost of 1250 pounds
And Bankinking day is two days ago
When Bankinking day is two days ago
And Banking Day was closed
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking File For Head Office without Cost Code is created
And All floats were assigned by the authoriser
Then Bad Safe Does Not Exist
And Safe has Total 5600
And Banking Bag with Seal Number '666-66666666-6' is created today
And Banking Bag with Seal Number '666-66666666-6' is Manager Checked today
And After being Manager Checked the Banking Bag with Seal Number '666-66666666-6' is destroyed today
And Banking File 'STHOA' for Account Number 9001 has value 5900.00

Scenario: Banking On Trading Day For Banking On Day Two
Given The last Safe Bags line had ID '9544' in db
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And All floats were assigned by the authoriser
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
And The cashier 'Fred Bloggs' makes a 1250 pounds pickup under Seal Number '55555551' from a Float under Seal Number '44444441' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1350 pounds pickup under Seal Number '55555552' from a Float under Seal Number '44444442' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 800 pounds pickup under Seal Number '55555553' from a Float under Seal Number '44444443' with comment 'Pickup Three' with value 105
And All floats were assigned by the authoriser
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '44444441'
And The float was assigned to cashier 'Bill Smith' under Seal Number '44444442'
And The float was assigned to cashier 'George Hunt' under Seal Number '44444443'
And The cashier 'Fred Bloggs' made sales with total cost of 800 pounds
And The cashier 'Bill Smith' made sales with total cost of 1650 pounds
And The cashier 'George Hunt' made sales with total cost of 1250 pounds
And Bankinking day is two days ago
And Bankinking day is two days ago
And Banking Day was closed
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking File For Head Office without Cost Code is created
And All floats were assigned by the authoriser
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was yesterday
And There was 5090 pounds in Main Safe and 510 pounds in Change Safe
When Banking Day was closed with Seal Number '777-77777777-7' and Slip Number '456' with comment 'Day Two Banking Comment' and Banking Value of 1500
Then Bad Safe Does Not Exist
And Safe has Total 7450
And Banking Bag with Seal Number '777-77777777-7' is created today
And Banking Bag with Seal Number '777-77777777-7' is Manager Checked today
And After being Manager Checked the Banking Bag with Seal Number '777-77777777-7' is destroyed today

Scenario: Banking On Trading Day Miscellaneous In And Out
Given The last Safe Bags line had ID '9544' in db
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And Initial Safe was 10000 pounds
And There was 9500 pounds in Main Safe and 500 pounds in Change Safe
And Float of 95 pounds was taken from Safe under Seal Number '11111111'
And Float of 100 pounds was taken from Safe under Seal Number '11111112'
And Float of 105 pounds was taken from Safe under Seal Number '11111113'
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '11111111'
And The float was assigned to cashier 'Bill Smith' under Seal Number '11111112'
And The float was assigned to cashier 'George Hunt' under Seal Number '11111113'
And The cashier 'Fred Bloggs' made sales with total cost of 1600 pounds
And The cashier 'Bill Smith' made sales with total cost of 1050 pounds
And The cashier 'George Hunt' made sales with total cost of 250 pounds
And Paid Out for Customer Concern with Value 5.99 nad with Invoice Number '11111111' was made by Cashier 'Fred Bloggs'
And Paid Out for Stationery with Value 7.99 nad with Invoice Number '22222222' was made by Cashier 'Fred Bloggs'
And Paid Out Correction for Breakfast Voucher with Value 9.99 nad with Invoice Number '33333333' was made by Cashier 'Fred Bloggs'
And Paid Out Correction for Social Fund with Value 11.99 nad with Invoice Number '44444444' was made by Cashier 'Fred Bloggs'
And Paid Out for Travel with Value 13.99 nad with Invoice Number '55555555' was made by Cashier 'Fred Bloggs'
And Paid Out Correction for Travel with Value 13.99 nad with Invoice Number '66666666' was made by Cashier 'Fred Bloggs'
And Miscellaneous Income For Bad Cheque with Value 15.99 nad with Invoice Number '77777777' was made by Cashier 'Fred Bloggs'
And Miscellaneous Income Correction For Social Fund with Value 17.99 nad with Invoice Number '88888888' was made by Cashier 'Fred Bloggs'
And The cashier 'Fred Bloggs' makes a 1600 pounds pickup under Seal Number '33333331' from a Float under Seal Number '22222221' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1050 pounds pickup under Seal Number '33333332' from a Float under Seal Number '22222222' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 250 pounds pickup under Seal Number '33333333' from a Float under Seal Number '22222223' with comment 'Pickup Three' with value 105
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '22222221'
And The float was assigned to cashier 'Bill Smith' under Seal Number '22222222'
And The float was assigned to cashier 'George Hunt' under Seal Number '22222223'
And The cashier 'Fred Bloggs' made sales with total cost of 1250 pounds
And The cashier 'Bill Smith' made sales with total cost of 1300 pounds
And The cashier 'George Hunt' made sales with total cost of 800 pounds
And The cashier 'Fred Bloggs' makes a 1250 pounds pickup under Seal Number '55555551' from a Float under Seal Number '44444441' with comment 'Pickup One' with value 95
And The cashier 'Bill Smith' makes a 1350 pounds pickup under Seal Number '55555552' from a Float under Seal Number '44444442' with comment 'Pickup Two' with value 100
And The cashier 'George Hunt' makes a 800 pounds pickup under Seal Number '55555553' from a Float under Seal Number '44444443' with comment 'Pickup Three' with value 105
And One day was advanced
And Logged User was 'Stephen Holyoak' and Authoriser was 'Amy Murphy'
And Banking Day was today
And There was a Safe for today
And The float was assigned to cashier 'Fred Bloggs' under Seal Number '44444441'
And The float was assigned to cashier 'Bill Smith' under Seal Number '44444442'
And The float was assigned to cashier 'George Hunt' under Seal Number '44444443'
And The cashier 'Fred Bloggs' made sales with total cost of 800 pounds
And The cashier 'Bill Smith' made sales with total cost of 1650 pounds
And The cashier 'George Hunt' made sales with total cost of 1250 pounds
When Bankinking day is two days ago
And Banking Day was closed
And Banking Day was closed with Seal Number '666-66666666-6' and Slip Number '123' with comment 'Day One Banking Comment' and Banking Value of 7000
And Banking File For Head Office without Cost Code is created
Then Banking File 'STHOA' has Value 5.99 for MiscOut Customer Concern Account '4003'
And Banking File 'STHOA' has Value 7.99 for MiscOut Stationery Account '5604'
And Banking File 'STHOA' has Value 9.99- for MiscOut Breakfast Voucher Account '6705'
And Banking File 'STHOA' has Value 11.99- for MiscOut Social Fund Account '6602'
And Banking File 'STHOA' has Value 15.99- for MiscIn Bad Cheque Account '1115'
And Banking File 'STHOA' has Value 17.99 for MiscIn Social Fund Account '6601'
And Banking File 'STHOA' does not have MiscOut for Travel Account Account '6502'