﻿Feature: RelatedItems
@RelatedItems

Scenario: Adjust Related Items Markup
    When Related Items markup for Sku '100150' with value 8.90 was adjusted
    Then Single SKU '100150' has this week units transfered of 6
    And Single SKU '100150' has this week to Date Markup of 8.90
    And Single SKU '100150' has this period to Date Markup of 23.95
    And Single SKU '100150' has this year to Date Markup 23.95

Scenario: Adjust Singles Sold Not Updated Yet
    When Number of Singles sold not updated yet for SKU '100157' was adjusted
    Then Single SKU '100157' has this singles sold not Updated Yet Value -10

Scenario: Adjust Stock For Pack Split
    When Stock for Pack Split for SKU '100004' with quantity 7 of value 12.34 was adjusted
    Then Single SKU '100004' has Units On Hand of quantity 12
    Then Single SKU '100004' has this Bulk To Single Transfer quantity of 7
    Then Single SKU '100004' has this Bulk To Single Transfer value of 12.34

Scenario: Adjust Units Sold In Current Week
    When Units Sold in current week for Sku '100005' with quantity of 8 were adjusted
    And Units Sold in current week for Sku '100006' with quantity of -3 were adjusted
    Then Single SKU '100005' has this Units Sold in Current Week number of 8
    And Single SKU '100006' has this Units Sold in Current Week number of -3

Scenario: Get Related Items Requiring Adjustments
    When Related Items Requiring Adjustments were selected
    Then Selected Items are:
    | SKU Number | Price | On Hand Quantity | Pack SKU Number | Pack Price | Pack On Hand Quantity | Item Pack Size |
    | 100147     | 4.83  | < 0              | 158058          | 28.98      | > 0                   | 6              |
    | 100147     | 4.83  | -5               | 158058          | 28.98      | 10                    | 6              |
    | 100149     | 5.49  | < 0              | 158057          | 14.99      | > 0                   | 4              |
    | 100151     | 6.30  | < 0              | 225301          | 37.77      | > 0                   | 5              |

Scenario: Split Bulk To Single Items
    Given There were next Related Items:
    | SKU Number | Price | Pack SKU Number | Pack Price |
    | 100147     | 4.83  | 158058          | 28.98      |
    | 100149     | 5.49  | 158057          | 14.99      |
    | 100151     | 6.30  | 225301          | 37.77      |
    | 100153     | 5.30  | 225302          | 17.77      |
    | 100152     | 1.30  | 215613          | 7.77       |
    And User was 999 and Workstation Id was 1 and Security Level was 1
    And Empty STHOT file existed
    When Related Item Packs were splited
    Then Stock Log Entry for Single SKU '100147' has Quantity Update of 6
    And Stock Log Entry for Pack SKU '158058' has Quantity Update of -1
    And Single Sku '100147' Units On Hand quantity is 1
    And Pack Sku '158058' Units On Hand quantity is 9
    And Stock Log Entry for Single SKU '100149' has Quantity Update of 8
    And Stock Log Entry for Pack SKU '158057' has Quantity Update of -2
    And Single Sku '100149' Units On Hand quantity is 2
    And Pack Sku '158057' Units On Hand quantity is 29
    And Stock Log Entry for Single SKU '100151' has Quantity Update of 15
    And Stock Log Entry for Pack SKU '225301' has Quantity Update of -3
    And Single Sku '100151' Units On Hand quantity is 3
    And Pack Sku '225301' Units On Hand quantity is -1
    And Single Sku '100153' Units On Hand quantity is 0
    And Pack Sku '225302' Units On Hand quantity is 10
    And Single Sku '100152' Units On Hand quantity is 3
    And Pack Sku '215613' Units On Hand quantity is 19
    And Sthot file contains records with next data:
    | Single SKU Number | Record Type | Transaction Date | Hash Value | Single SKU Code | Single Sku Adjustment Quantity | Pack Sku Code | Pack Sku Adjustment Quantity | Pack Sku Price | Single Sku Price |
    | 100147            | BS          | yesterday        | 6          | 100147          | 6                              | 158058        | -1                           | 28.98          | 4.83             |
    | 100149            | BS          | yesterday        | 8          | 100149          | 8                              | 158057        | -2                           | 14.99          | 5.49             |
    | 100151            | BS          | yesterday        | 15         | 100151          | 15                             | 225301        | -3                           | 37.77          | 6.30             |
    And Sthot record for SKU '100153' doesn't exist
    And Sthot record for SKU '100152' doesn't exist