using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Cts.Oasys.Core.Tests;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;
using System.IO;
using Cts.Oasys.Core;

namespace WSS.AAT.Scenarios.RelatedItems
{
    public class SplitBulkToSingleItems
    {
        #region "Internal Variables"

        private string _systemUnderTestLocation;
        private int _userId;
        private int _workstationId;
        private int _securityLevelId;
        private readonly string _connectionString;

        private readonly TestRelatedItemRepository _relatedItemsRepository;

        private string[] _outputFileContent;
        private int _outputFileIndex;

        #endregion

        #region "Setters"

        public void StartDebug()
        {
            Debugger.Launch();
        }

        public SplitBulkToSingleItems(string connection, IDataLayerFactory dataLayerFactory)
        {
            _connectionString = connection;
            _relatedItemsRepository = dataLayerFactory.Create<TestRelatedItemRepository>();
        }

        public void SystemUnderTestIsLocatedHere(string system)
        {
            _systemUnderTestLocation = system;
        }

        public void UserIs(int userId)
        {
            _userId = userId;
        }

        public void WorkstationIdIs(int workstationId)
        {
            _workstationId = workstationId;
        }

        public void SecurityLevelIs(int securityLevelId)
        {
            _securityLevelId = securityLevelId;
        }

        public void LoadOutputFile()
        {
            LoadOutputFileCalledSthot();            
        }

        public void ForLoadedOutputFindLineUsingSinglePackSku(string sku)
        {
            SelectSingleSku(sku);
        }

        #endregion

        #region "System Under Test: Daily Close Related Item Process"

        public void SplitRelatedItemPacks()
        {
            //example command line to executable
            //999,1,1,"Data Source=%SERVERNAMETOKEN%;Initial Catalog=OASYS;Integrated Security=True",

            RemoveOutputFileCalledSthot();

            var proc = new Process();

            proc.StartInfo.FileName = _systemUnderTestLocation + "SplitBulkToSingleItems.exe";
            proc.StartInfo.Arguments = _userId.ToString() + "," + _workstationId.ToString() + "," +
                                       _securityLevelId.ToString() + "," + _connectionString;
            proc.StartInfo.UseShellExecute = false;

            TestEnvironmentSetup.SetupSpawnedProcess(proc.StartInfo);

            proc.Start();
            proc.WaitForExit();

            LoadOutputFileCalledSthot();
        }

        #endregion

        #region "Tests"

        public int StockLogEntryForSingleSkuShowsQuantityUpdateOf(string sku)
        {
            return _relatedItemsRepository.StockMovementForSku(sku);
        }

        public int StockLogEntryForPackSkuShowsQuantityUpdateOf(string sku)
        {
            return _relatedItemsRepository.StockMovementForSku(sku);
        }

        public int SingleSkuUnitsOnHandIs(string sku)
        {
            return _relatedItemsRepository.UnitsOnHandForSku(sku);
        }

        public int PackSkuUnitsOnHandIs(string sku)
        {
            return _relatedItemsRepository.UnitsOnHandForSku(sku);
        }

        public int NumberOfSinglesPerPackIs(string singleSku, string packSku)
        {
            return _relatedItemsRepository.NumberOfSinglesPerPack(singleSku, packSku);
        }

        public decimal NumberOfPacksToSplitIs(int singlesPerPack, int singleSkuOnha)
        {
            return Math.Ceiling(Math.Abs((decimal)singleSkuOnha) / singlesPerPack);
        }

        public bool ForThisSkuOutputFileDoesNotHaveAnEntry(string sku)
        {
            return !SelectedSingleSkuExists(sku);
        }

        #endregion

        #region "Stored procedures stuff"

        public int SingleSkuHasThisBulkToSingleTransferQuantity(string sku)
        {
            return _relatedItemsRepository.BulkToSingleTransferQuantityForSku(sku);
        }

        public decimal SingleSkuHasThisBulkToSingleTransferValue(string sku)
        {
            return _relatedItemsRepository.BulkToSingleTransferValueForSku(sku);
        }

        public int SingleSkuHasThisUnitsSoldInCurrentWeek(string sku)
        {
            return _relatedItemsRepository.UnitsSoldInCurrentWeekForSku(sku);
        }

        public int SingleSkuHasThisWeekUnitsTransferedOf(string sku)
        {
            return _relatedItemsRepository.ThisWeekUnitsTransfered(sku);
        }

        public decimal SingleSkuHasThisWeekToDateMarkup(string sku)
        {
            return _relatedItemsRepository.WeekToDateMarkup(sku);
        }

        public decimal SingleSkuHasThisPeriodToDateMarkup(string sku)
        {
            return _relatedItemsRepository.PeriodToDateMarkup(sku);
        }

        public decimal SingleSkuHasThisYearToDateMarkup(string sku)
        {
            return _relatedItemsRepository.YearToDateMarkup(sku);
        }

        public int SingleSkuHasThisSinglesSoldNotUpdatedYetValue(string sku)
        {
            return _relatedItemsRepository.SinglesSoldNotUpdatedYetValue(sku);
        }

        public void AdjustStockForPackSplitForSkuWithQuantityOfValue(int sku, int quantity, decimal value)
        {
            _relatedItemsRepository.AdjustStockForPackSplitForSkuWithQuantityOfValue(sku, quantity, value);
        }

        public void AdjustUnitsSoldInCurrentWeekForSkuWithQuantity(int sku, int quantity)
        {
            _relatedItemsRepository.AdjustUnitsSoldInCurrentWeekForSkuWithQuantity(sku, quantity);
        }

        public void AdjustRelatedItemsMarkupForSkuWithValue(int sku, decimal value)
        {
            _relatedItemsRepository.AdjustRelatedItemsMarkupForSkuWithValue(sku, value);
        }

        public void ReverseAdjustmentOfRelatedItemsMarkupForSkuWithValue(int sku, decimal value)
        {
            _relatedItemsRepository.ReverseAdjustmentOfRelatedItemsMarkupForSkuWithValue(sku, value);
        }

        public void AdjustSinglesSoldNotUpdatedYetForSku(int sku)
        {
            _relatedItemsRepository.AdjustSinglesSoldNotUpdatedYetForSku(sku);
        }

        public void ReverseAdjustmentOfSinglesSoldNotUpdatedYetForSku(int sku)
        {
            _relatedItemsRepository.ReverseAdjustmentOfSinglesSoldNotUpdatedYetForSku(sku);
        }

        #endregion

        public void RemoveOutputFileCalledSthot()
        {
            string filePath = Path.Combine(OutputFileLocation(), "STHOT");
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public void LoadOutputFileCalledSthot()
        {
            string blabla = OutputFileLocation() + "STHOT";
            _outputFileContent = File.ReadAllLines(OutputFileLocation() + "STHOT");
        }

        public void SelectSingleSku(string sku)
        {
            OutputFileIndexForSelectedSku(sku);
        }

        public bool SelectedSingleSkuExists(string sku)
        {
            if (_outputFileIndex == -1)
            {
                return false;
            }
            return true;
        }

        private string OutputFileLocation()
        {
            return GlobalVars.SystemEnvironment.GetCommsDirectoryPath();
        }

        private void OutputFileIndexForSelectedSku(string sku)
        {
            _outputFileIndex = -1;

            for (int index = 0; index < _outputFileContent.Length; index++)
            {
                if (_outputFileContent[index].Substring(22, 6) == sku)
                {
                    _outputFileIndex = index;
                    break;
                }
            }
        }
    }
}
