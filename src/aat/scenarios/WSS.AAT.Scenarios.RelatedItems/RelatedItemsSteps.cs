﻿using Cts.Oasys.Core.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using System;
using WSS.AAT.Common.Utility.FileParsers;
using System.IO;

namespace WSS.AAT.Scenarios.RelatedItems
{
    [Binding]
    public class RelatedItemsSteps : BaseStepDefinitions
    {
        private readonly TestSystemEnvironment systemEnvironment;

        private readonly SplitBulkToSingleItems splitBulkToSingleItems;

        private readonly TestRelatedItemRepository relatedItemsRepo;

        private List<SimplifiedRelatedItem> relatedItems;
        private List<SimplifiedRelatedItem> savedRelatedItems; 

        public RelatedItemsSteps(TestSystemEnvironment systemEnvironment, IDataLayerFactory dataLayerFactory)
        {
            this.systemEnvironment = systemEnvironment;

            splitBulkToSingleItems = new SplitBulkToSingleItems(ConfigurationManager.ConnectionStrings["Oasys"].ConnectionString, dataLayerFactory);

            relatedItemsRepo = dataLayerFactory.Create<TestRelatedItemRepository>();

            relatedItems = new List<SimplifiedRelatedItem>();
            savedRelatedItems = new List<SimplifiedRelatedItem>();
        }

        [Given(@"There were next Related Items:")]
        public void GivenThereWereNextRelatedItems(Table table)
        {
            foreach (var row in table.Rows)
            {
                var simplifiedRelatedItem = new SimplifiedRelatedItem()
                {
                    SingleSkun = row["SKU Number"],
                    SinglePrice = decimal.Parse(row["Price"]),
                    SingleOnHand = relatedItemsRepo.UnitsOnHandForSku(row["SKU Number"]),
                    PackSkun = row["Pack SKU Number"],
                    PackPrice = decimal.Parse(row["Pack Price"]),
                    PackOnHand = relatedItemsRepo.UnitsOnHandForSku(row["Pack SKU Number"]),
                    ItemPackSize = relatedItemsRepo.NumberOfSinglesPerPack(row["SKU Number"], row["Pack SKU Number"])
                };

                savedRelatedItems.Add(simplifiedRelatedItem);
            }
        }

        [Given(@"User was (.*) and Workstation Id was (.*) and Security Level was (.*)")]
        public void GivenUserWasAndWorkstationIdWasAndSecurityLevelWas(int userId, int workstationId, int securityLevelId)
        {
            splitBulkToSingleItems.SystemUnderTestIsLocatedHere(ConfigurationManager.AppSettings["executablesDir"]);
            splitBulkToSingleItems.UserIs(userId);
            splitBulkToSingleItems.WorkstationIdIs(workstationId);
            splitBulkToSingleItems.SecurityLevelIs(securityLevelId);
        }

        [Given(@"Empty STHOT file existed")]
        public void GivenEmptySTHOTFileExisted()
        {
            string path = Path.Combine(ConfigurationManager.AppSettings["commsDir"], "STHOT");
            if (!File.Exists(path))
            {
                File.CreateText(path).Close();
            }
        }

        [When(@"Related Items markup for Sku '(.*)' with value (.*) was adjusted")]
        public void WhenRelatedItemsMarkupForSkuWithValueWasAdjusted(int skuNumber, decimal price)
        {
            //AdjustRelatedItemsMarkup(@Skun, @Value) updates the Related Item
            //This Week Units Transfered by adding (same) Related Item's 'Number of Singles Per Pack'
            //Week To Date Markup by adding @Value
            //Period To Date Markup by adding @Value
            //Year To Date Markup by adding @Value
            //for the given sku (@Skun)

            splitBulkToSingleItems.AdjustRelatedItemsMarkupForSkuWithValue(skuNumber, price);
        }

        [When(@"Stock for Pack Split for SKU '(.*)' with quantity (.*) of value (.*) was adjusted")]
        public void WhenStockForPackSplitForSKUWithQuantityOfValueWasAdjusted(int skuNumber, int quantity, decimal value)
        {
            //AdjustStockForPackSplit(@Skun, @Quantity, @Value) updates the Stock
            //On Hand value by adding @Quantity
            //Bulk to single transfer quantity by adding @Quantity
            //Bulk to single transfer value by adding @Value
            //for the given sku (@Skun)

            splitBulkToSingleItems.AdjustStockForPackSplitForSkuWithQuantityOfValue(skuNumber, quantity, value);
        }

        [When(@"Number of Singles sold not updated yet for SKU '(.*)' was adjusted")]
        public void GivenNumberOfSinglesSoldNotUpdatedYetForSKUWasAdjusted(int skuNumber)
        {
            splitBulkToSingleItems.AdjustSinglesSoldNotUpdatedYetForSku(skuNumber);
        }

        [When(@"Units Sold in current week for Sku '(.*)' with quantity of (.*) were adjusted")]
        public void WhenUnitsSoldInCurrentWeekForSkuWithQuantityOfWereAdjusted(int skuNumber, int quantity)
        {
            splitBulkToSingleItems.AdjustUnitsSoldInCurrentWeekForSkuWithQuantity(skuNumber, quantity);
        }

        [When(@"Related Items Requiring Adjustments were selected")]
        public void WhenRelatedItemsRequiringAdjustmentsWereSelected()
        {
            relatedItems = relatedItemsRepo.GetRelatedItemsRequiringAdjustments().ToList();
            Assert.IsNotNull(relatedItems);
        }

        [When(@"Related Item Packs were splited")]
        public void WhenRelatedItemPacksWereSplited()
        {
            splitBulkToSingleItems.SplitRelatedItemPacks();
        }

        [Then(@"Single SKU '(.*)' has this week units transfered of (.*)")]
        public void ThenSingleSKUHasThisWeekUnitsTransferedOf(string skuNumber, int numberOfUnits)
        {
            Assert.AreEqual(numberOfUnits, splitBulkToSingleItems.SingleSkuHasThisWeekUnitsTransferedOf(skuNumber));
        }

        [Then(@"Single SKU '(.*)' has this week to Date Markup of (.*)")]
        public void ThenSingleSKUHasThisWeekToDateMarkup(string skuNumber, decimal value)
        {
            Assert.AreEqual(value, splitBulkToSingleItems.SingleSkuHasThisWeekToDateMarkup(skuNumber));
        }

        [Then(@"Single SKU '(.*)' has this period to Date Markup of (.*)")]
        public void ThenSingleSKUHasThisPeriodToDateMarkupOf(string skuNumber, decimal value)
        {
            Assert.AreEqual(value, splitBulkToSingleItems.SingleSkuHasThisPeriodToDateMarkup(skuNumber));
        }

        [Then(@"Single SKU '(.*)' has this year to Date Markup (.*)")]
        public void ThenSingleSKUHasThisYearToDateMarkup(string skuNumber, decimal value)
        {
            Assert.AreEqual(value, splitBulkToSingleItems.SingleSkuHasThisYearToDateMarkup(skuNumber));
        }

        [Then(@"Single SKU '(.*)' has this singles sold not Updated Yet Value (.*)")]
        public void ThenSingleSKUHasThisSinglesSoldNotUpdatedYetValue(string skuNumber, decimal value)
        {
            Assert.AreEqual(value, splitBulkToSingleItems.SingleSkuHasThisSinglesSoldNotUpdatedYetValue(skuNumber));
        }

        [Then(@"Single SKU '(.*)' has Units On Hand of quantity (.*)")]
        public void ThenSingleSKUHasUnitsOnHandOfQuantity(string skuNumber, int quantity)
        {
            Assert.AreEqual(quantity, splitBulkToSingleItems.SingleSkuUnitsOnHandIs(skuNumber));
        }

        [Then(@"Single SKU '(.*)' has this Bulk To Single Transfer quantity of (.*)")]
        public void ThenSingleSKUHasThisBulkToSingleTransferQuantityOf(string skuNumber, int quantity)
        {
            Assert.AreEqual(quantity, splitBulkToSingleItems.SingleSkuHasThisBulkToSingleTransferQuantity(skuNumber));
        }

        [Then(@"Single SKU '(.*)' has this Bulk To Single Transfer value of (.*)")]
        public void ThenSingleSKUHasThisBulkToSingleTransferValueOf(string skuNumber, decimal value)
        {
            Assert.AreEqual(value, splitBulkToSingleItems.SingleSkuHasThisBulkToSingleTransferValue(skuNumber));
        }

        [Then(@"Single SKU '(.*)' has this Units Sold in Current Week number of (.*)")]
        public void ThenSingleSKUHasThisUnitsSoldInCurrentWeekNumberOf(string skuNumber, int quantity)
        {
            Assert.AreEqual(quantity, splitBulkToSingleItems.SingleSkuHasThisUnitsSoldInCurrentWeek(skuNumber));
        }

        [Then(@"Selected Items are:")]
        public void ThenSelectedItemsAre(Table table)
        {
            foreach (var row in table.Rows)
            {
                var relatedItem = relatedItems
                    .Where(x => 
                        x.SingleSkun == row["SKU Number"] && 
                        x.SinglePrice == decimal.Parse(row["Price"]) && 
                        ProcessOnHandQuantity(x.SingleOnHand, row["On Hand Quantity"]) &&
                        x.PackSkun == row["Pack SKU Number"] &&
                        x.PackPrice == decimal.Parse(row["Pack Price"]) && 
                        ProcessOnHandQuantity(x.PackOnHand, row["Pack On Hand Quantity"]) &&
                        x.ItemPackSize == decimal.Parse(row["Item Pack Size"])
                        );

                Assert.IsNotNull(relatedItem);
            }
        }

        [Then(@"Stock Log Entry for Single SKU '(.*)' has Quantity Update of (.*)")]
        public void ThenStockLogEntryForSingleSKUHasQuantityUpdateOf(string skuNumber, int quantity)
        {
            Assert.AreEqual(quantity, splitBulkToSingleItems.StockLogEntryForSingleSkuShowsQuantityUpdateOf(skuNumber));
        }

        [Then(@"Stock Log Entry for Pack SKU '(.*)' has Quantity Update of (.*)")]
        public void ThenStockLogEntryForPackSKUHasQuantityUpdateOf(string skuNumber, int quantity)
        {
            Assert.AreEqual(quantity, splitBulkToSingleItems.StockLogEntryForPackSkuShowsQuantityUpdateOf(skuNumber));
        }

        [Then(@"Single Sku '(.*)' Units On Hand quantity is (.*)")]
        public void ThenSingleSkuUnitsOnHandQuantityIs(string skuNumber, int quantity)
        {
            Assert.AreEqual(quantity, splitBulkToSingleItems.SingleSkuUnitsOnHandIs(skuNumber));
        }

        [Then(@"Pack Sku '(.*)' Units On Hand quantity is (.*)")]
        public void ThenPackSkuUnitsOnHandQuantityIs(string skuNumber, int quantity)
        {
            Assert.AreEqual(quantity, splitBulkToSingleItems.PackSkuUnitsOnHandIs(skuNumber));
        }

        [Then(@"Sthot file contains records with next data:")]
        public void ThenSthotFileContainsRecordsWithNextData(Table table)
        {
            var sthotFileParser = new SthotFileParser(systemEnvironment);

            foreach (var row in table.Rows)
            {
                string skuNumber = row["Single SKU Number"];

                Assert.AreEqual(row["Record Type"], sthotFileParser.GetBSRecordType(skuNumber));
                Assert.AreEqual(row["Transaction Date"], sthotFileParser.GetBSTransactionDate(skuNumber));
                Assert.AreEqual(int.Parse(row["Hash Value"]), sthotFileParser.GetBSHashValue(skuNumber));
                Assert.AreEqual(row["Single SKU Code"], sthotFileParser.GetBSSingleSkuCode(skuNumber));
                Assert.AreEqual(int.Parse(row["Single Sku Adjustment Quantity"]), sthotFileParser.GetBSSingleSkuAdjustmentQuantity(skuNumber));
                Assert.AreEqual(row["Pack Sku Code"], sthotFileParser.GetBSPackSkuCode(skuNumber));
                Assert.AreEqual(int.Parse(row["Pack Sku Adjustment Quantity"]), sthotFileParser.GetBSPackSkuAdjustmentQuantity(skuNumber));
                Assert.AreEqual(decimal.Parse(row["Pack Sku Price"]), sthotFileParser.GetBSPackSkuPrice(skuNumber));
                Assert.AreEqual(decimal.Parse(row["Single Sku Price"]), sthotFileParser.GetBSSingleSkuPrice(skuNumber));
            }
        }

        [Then(@"Sthot record for SKU '(.*)' doesn't exist")]
        public void ThenSthotRecordForSKUDoesnTExist(string skuNumber)
        {
            var sthotFileParser = new SthotFileParser(systemEnvironment);

            Assert.IsFalse(sthotFileParser.IsExistsBSRecordForSku(skuNumber));
        }

        private bool ProcessOnHandQuantity(int quantity, string stringExpectedQuantity)
        {
            int expectedQuantity = 0;

            if (Int32.TryParse(stringExpectedQuantity, out expectedQuantity))
            {
                return expectedQuantity == quantity;
            }
            else
            {
                switch (stringExpectedQuantity)
                {
                    case "> 0":
                        return quantity > 0;
                    case "< 0":
                        return quantity < 0;
                    default:
                        return false;
                }
            }
        }
    }
}
