using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Configuration;
using NUnit.Framework;

namespace WSS.AAT.Scenarios.ActivityLog
{
    [Binding]
    [SetUpFixture]
    class SetUpBinding : BaseSetUpBinding<BaseSetUp>
    {
        [SetUp]
        public static void BeforeTestRun()
        {
            DoBeforeTestRun();
        }

        [BeforeScenario]
        public static void BeforeScenario()
        {
            DoBeforeScenario();
        }
    }
}
