﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.ActivityLog
{
    [Binding]
    public class ActivityLogSteps : BaseStepDefinitions
    {
        private readonly TestDailyTillRepository _dailyRepo;

        public ActivityLogSteps(IDataLayerFactory dataLayerFactory)
        {
            _dailyRepo = dataLayerFactory.Create<TestDailyTillRepository>();
        }

        [Given(@"A user accessing the back office system")]
        public void GivenAUserAccessingTheBackOfficeSystem()
        {
            //Actually, no additional logic there...
        }

        [When(@"A user '(.*)' starts an application with '(.*)' id within the back office menu at a workstation '(.*)'")
        ]
        public void WhenAUserStartsAnApplicationWithinTheBackOfficeMenu(int userId, int menuId, int workstationId)
        {
            Cts.Oasys.Core.System.ActivityLog.RecordAppStarted(menuId, userId, workstationId);
        }

        [When(@"A user '(.*)' logs on at a workstation '(.*)'")]
        public void WhenAUserLogsOnAtAWorkstation(int userId, int workstationId)
        {
            Cts.Oasys.Core.System.ActivityLog.RecordUserLogIn(userId, workstationId);
        }

        [When(@"A user '(.*)' logs out of the back office menu at a workstation '(.*)'")]
        public void WhenAUserLogsOutOfTheBackOfficeMenu(int userId, int workstationId)
        {
            Cts.Oasys.Core.System.ActivityLog.RecordUserLogOut(userId.ToString(), workstationId.ToString(), false);
        }

        [Then(@"The workstation id '(.*)' is recorded in the activity log '(.*)' record")]
        public void ThenTheWorkstationIdIsRecordedInTheActivityLogRecord(int workstationId, string recordType)
        {
            if (recordType == ActivityLogType.Login.ToString())
            {
                CheckActivityLogNewRow(workstationId, true, false);
            }

            if (recordType == ActivityLogType.Logout.ToString())
            {
                CheckActivityLogNewRow(workstationId, false, false);
            }

            if (recordType == ActivityLogType.ApplicationStart.ToString())
            {
                CheckActivityLogNewRow(workstationId, true, false, true);
            }
        }

        private void CheckActivityLogNewRow(int workstationId, bool expectedLoginValue, bool expectedLogoutValue, bool expectedMenuIsNotZero = false)
        {
            var activityLog = _dailyRepo.GetLastActivityLogRecord();

            Assert.AreEqual(workstationId.ToString(), activityLog.WorkstationID);
            Assert.AreEqual(expectedLoginValue, activityLog.LoggedIn);
            Assert.AreEqual(expectedLogoutValue, activityLog.ForcedLogOut);
            if (expectedMenuIsNotZero)
            {
                Assert.AreNotEqual(0, activityLog.MenuOptionID);
            }
            else
            {
                Assert.AreEqual(0, activityLog.MenuOptionID);
            }
        }

        private enum ActivityLogType
        {
            Login,
            Logout,
            ApplicationStart
        }
    }
}
