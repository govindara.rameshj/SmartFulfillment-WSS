﻿@ActivityLog
Feature: ActivityLog
In order to check that we write activities in database
As a developer
I write scenarios for tests

Scenario: Activity log stores workstation id on app startup
    Given A user accessing the back office system
    When A user '10' starts an application with '1' id within the back office menu at a workstation '99'
    Then The workstation id '99' is recorded in the activity log 'ApplicationStart' record

Scenario: Activity log stores workstation id on login
    Given A user accessing the back office system
    When A user '10' logs on at a workstation '99'
    Then The workstation id '99' is recorded in the activity log 'Login' record

Scenario: Activity log stores workstation id on logout
    Given A user accessing the back office system
    When A user '10' logs out of the back office menu at a workstation '99'
    Then The workstation id '99' is recorded in the activity log 'Logout' record

