@iPadio
Feature: Check delivery service is not notified in case of collect orders and partial cancellations
    As a store colleague
    I want delivery service not to receive any notifications for collection orders and partial consignment cancellations
    So that delivery service does not have irrelevant information

Scenario: No notifications for in store collect order
    When new in-store collect order is created and processed by fulfilling store
    Then the notification about new consignment for delivery service is not created

Scenario: No notifications for click & collect order
    When new click & collect order is created and processed by fulfilling store
    Then the notification about new consignment for delivery service is not created

Scenario: No notifications for new MCFC store order
    When new in store order is created and fulfilled in MCFC
    Then the notification about new consignment for delivery service is not created

Scenario: No notifications for partial web cancellation
    When partial web cancellation is created and processed by fulfilling store
    Then the notification about cancelled consignment for delivery service is not created

Scenario: No notifications for partial store cancellation
    When partial in store cancellation is created and processed by fulfilling store
    Then the notification about cancelled consignment for delivery service is not created

Scenario: No notifications for store order cancellation, fulfilled in MCFC 
    When new in store order is created and fulfilled in MCFC 
    And store delivery order, fulfilled in MCFC is cancelled
    Then the notification about cancelled consignment for delivery service is not created

Scenario: No notifications for Wickes Hourly Service order
    When new wickes hourly service order is created and processed by fulfilling store
    Then the notification about new consignment for delivery service is not created

Scenario: No notifications for click & collect order cancellation
    When new click & collect order is created and processed by fulfilling store
    And click & collect order is fully cancelled
    Then the notification about cancelled consignment for delivery service is not created
 
Scenario: No notifications for Wickes Hourly Service order cancellation
    When new wickes hourly service order is created and processed by fulfilling store
    And wickes hourly service order is fully cancelled
    Then the notification about cancelled consignment for delivery service is not created