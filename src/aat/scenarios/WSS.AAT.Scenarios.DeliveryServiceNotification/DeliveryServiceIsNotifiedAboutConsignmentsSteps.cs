using System;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Qod.CtsOrderManager;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.AAT.Scenarios.DeliveryServiceNotification
{
    [Binding]
    public class DeliveryServiceIsNotifiedAboutConsignmentsSteps : BaseSteps
    {
        protected DeliveryServiceIsNotifiedAboutConsignmentsSteps(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
            SetDeliveryNotificationFlag(true);
        }

        [Given(@"new delivery order created in a selling store")]
        public void GivenNewDeliveryOrderCreatedInASellingStore()
        {
            PrepareFulfillmentRequestForStoreDeliveryOrder();
        }

        [When(@"the fulfilling store receives it")]
        public void WhenTheFulfillingStoreReceivesIt()
        {
            ProcessPreparedFulfillmentRequest();
        }

        [Then(@"the notification about new consignment for delivery service is created")]
        public void ThenTheNotificationAboutNewConsignmentForDeliveryServiceIsCreated()
        {
            Assert.IsNotNull(FindExternalRequest(ExternalRequestType.NewConsignment));
        }

        [Given(@"new delivery order created on a web site")]
        public void GivenNewDeliveryOrderCreatedOnAWebSite()
        {
            PrepareFulfillmentRequestForStoreDeliveryWebOrder();
        }

        [Given(@"new delivery order is created in a selling store with delivery date ""today""")]
        public void GivenNewDeliveryOrderIsCreatedInASellingStoreWithDeliveryDateToday()
        {
            PrepareFulfillmentRequestForStoreDeliveryOrder();
            FulfillmentRoutine.CurrentRequest.OrderHeader.RequiredDeliveryDate = new CTSFulfilmentRequestOrderHeaderRequiredDeliveryDate { Value = DateTime.Now};

            ProcessPreparedFulfillmentRequest();
        }

        [Given(@"new delivery web order is created with delivery date ""today""")]
        public void GivenNewDeliveryWebOrderIsCreatedInASellingStoreWithDeliveryDateToday()
        {
            PrepareFulfillmentRequestForStoreDeliveryWebOrder();
            FulfillmentRoutine.CurrentRequest.OrderHeader.RequiredDeliveryDate = new CTSFulfilmentRequestOrderHeaderRequiredDeliveryDate { Value = DateTime.Now };

            ProcessPreparedFulfillmentRequest();
        }

        [Given(@"new delivery web order created")]
        public void GivenNewDeliveryWebOrderCreatedInASellingStore()
        {
            PrepareFulfillmentRequestForStoreDeliveryWebOrder();
        } 

        [When(@"delivery date is changed from ""today"" to ""tomorrow""")]
        public void WhenDeliveryDateIsChangedFromTodayToTomorrow()
        {
            UpdateOrderRoutine routine = new UpdateOrderRoutine(DataLayerFactory);
            routine.InitializeOrderForm(LoadOrderHeader());
            routine.SetDeliveryDate(GetTomorrowDate());
            routine.AcceptOrder();
        }

        [When(@"customer contact details are updated")]
        public void WhenCustomerContactDetailsAreUpdated()
        {
            ProcessPreparedFulfillmentRequest();
            UpdateOrderRoutine routine = new UpdateOrderRoutine(DataLayerFactory);
            routine.InitializeOrderForm(LoadOrderHeader());
            routine.SetPhoneHomeNumber("123456789");
            routine.AcceptOrder();
        }

        [When(@"customer delivery details are updated")]
        public void WhenCustomerDeliveryDetailsAreUpdated()
        {
            ProcessPreparedFulfillmentRequest();
            UpdateOrderRoutine routine = new UpdateOrderRoutine(DataLayerFactory);
            routine.InitializeOrderForm(LoadOrderHeader());
            routine.SetAddress("Noname street");
            routine.AcceptOrder();
        }  

        [Then(@"the notification about update consignment for delivery service is created")]
        public void ThenTheNotificationAboutUpdateConsignmentForDeliveryServiceIsCreated()
        {
            Assert.IsNotNull(FindExternalRequest(ExternalRequestType.UpdateConsignment));
        }

        [Given(@"the the fulfilling store receives it")]
        public void GivenTheTheFulfillingStoreReceivesIt()
        {
            ProcessPreparedFulfillmentRequest();
        }

        [Given(@"full delivery order cancellation consignment is sent to store")]
        public void GivenFullDeliveryOrderCancellationConsignmentIsSentToStore()
        {
            PrepareRefundRequestForStoreDeliveryOrder();
        }

        [When(@"the fulfilling store receives cancellation consignment")]
        public void WhenTheFulfillingStoreReceivesCancellationConsignment()
        {
            ProcessPreparedRefundRequest();
        }

        [Then(@"the notification about cancellation consignment for delivery service is created")]
        public void ThenTheNotificationAboutCancellationConsignmentForDeliveryServiceIsCreated()
        {
            Assert.IsNotNull(FindExternalRequest(ExternalRequestType.CancelConsignment));
        }

    }
}
