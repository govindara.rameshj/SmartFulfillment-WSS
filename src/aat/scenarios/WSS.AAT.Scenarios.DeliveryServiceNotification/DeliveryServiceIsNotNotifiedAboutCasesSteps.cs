using Cts.Oasys.Core.Tests;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.AAT.Scenarios.DeliveryServiceNotification
{
    [Binding]
    public class DeliveryServiceIsNotNotifiedAboutCasesSteps : BaseSteps
    {
        protected DeliveryServiceIsNotNotifiedAboutCasesSteps(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
            SetDeliveryNotificationFlag(true);
        }

        [When(@"new in-store collect order is created and processed by fulfilling store")]
        public void WhenNewInStoreCollectOrderIsCreatedAndProcessedByFulfillingStore()
        {
            PrepareFulfillmentRequestForStoreCollectionOrder();
            ProcessPreparedFulfillmentRequest();
        }

        [Then(@"the notification about new consignment for delivery service is not created")]
        public void ThenTheNotificationAboutNewConsignmentForDeliveryServiceIsNotCreated()
        {
            Assert.IsNull(FindExternalRequest(ExternalRequestType.NewConsignment));
        }

        [When(@"new click & collect order is created and processed by fulfilling store")]
        public void WhenNewClickCollectOrderIsCreatedAndProcessedByFulfillingStore()
        {
            PrepareRequestForClickCollectOrder();
            ProcessPreparedEnableRequest();
        }

        [When(@"new in store order is created and fulfilled in MCFC")]
        public void WhenNewInStoreOrderIsCreatedAndFulfilledInMcfc()
        {
            SetDeliveryNotificationFlag(false);
            PrepareFulfillmentRequestForStoreDeliveryOrder();
            ProcessPreparedFulfillmentRequest();
        }

        [When(@"partial web cancellation is created and processed by fulfilling store")]
        public void WhenPartialWebCancellationIsCreatedAndProcessedByFulfillingStore()
        {
            PrepareFulfillmentRequestForPartitialCancellation();
            ProcessPreparedFulfillmentRequest();
            PrepareRefundRequestForPartitialCancellation();
            ProcessPreparedRefundRequest();
        }

        [Then(@"the notification about cancelled consignment for delivery service is not created")]
        public void ThenTheNotificationAboutCancelledConsignmentForDeliveryServiceIsNotCreated()
        {
            Assert.IsNull(FindExternalRequest(ExternalRequestType.CancelConsignment));
        }

        [When(@"store delivery order, fulfilled in MCFC is cancelled")]
        public void WhenStoreDeliveryOrderFulfilledInMcfcIsCancelled()
        {
            SetDeliveryNotificationFlag(false);
            PrepareRefundRequestForStoreDeliveryOrder();
            ProcessPreparedRefundRequest();
        }

        [When(@"partial in store cancellation is created and processed by fulfilling store")]
        public void WhenPartialInStoreCancellationIsCreatedAndProcessedByFulfillingStore()
        {
            PrepareFulfillmentRequestForPartitialCancellation();
            ProcessPreparedFulfillmentRequest();
            PrepareRefundRequestForPartitialCancellation();
            ProcessPreparedRefundRequest();
        }

        [When(@"new wickes hourly service order is created and processed by fulfilling store")]
        public void WhenNewWickesHourlyServiceOrderIsCreatedAndProcessedByFulfillingStore()
        {
            PrepareRequestForWickesHourlyServiceOrder();
            ProcessPreparedEnableRequest();
        }

        [When(@"click & collect order is fully cancelled")]
        public void WhenClickCollectOrderIsFullyCancelled()
        {
            PrepareRefundRequestForClickCollectOrder();
            ProcessPreparedEnableRefundRequest();
        }

        [When(@"wickes hourly service order is fully cancelled")]
        public void WhenWickesHourlyServiceOrderIsFullyCancelled()
        {
            PrepareRefundRequestForWickedHourlyServiceOrder();
            ProcessPreparedEnableRefundRequest();
        }
    }
}
