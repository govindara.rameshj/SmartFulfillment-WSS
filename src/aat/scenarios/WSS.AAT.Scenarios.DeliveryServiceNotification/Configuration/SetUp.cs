using Cts.Oasys.Core.Tests;
using WSS.AAT.Common.Utility.Configuration;

namespace WSS.AAT.Scenarios.DeliveryServiceNotification
{
    class SetUp : BaseSetUp
    {
        protected override void InitializeTestEnvironmentSettings(TestEnvironmentSettings settings)
        {
            base.InitializeTestEnvironmentSettings(settings);
            settings.XsdFilesDirectoryPath = TestConfiguration.ResolvePathFromAppSettings("xsdFilesDir");
        }
    }
}
