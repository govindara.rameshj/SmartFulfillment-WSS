using System;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.Qod;
using Cts.Oasys.Hubs.Core.Order.WebService;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Qod.CtsOrderManager;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.AAT.Common.Qod;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.AAT.Scenarios.DeliveryServiceNotification
{
    [Binding]
    public class BaseSteps: BaseStepDefinitions
    {
        protected FulfillmentRoutine FulfillmentRoutine { get; private set; }
        protected UpdateRefundRoutine UpdateRefundRoutine { get; private set; }
        protected CreationRoutine CreationRoutine { get; private set; }
        protected EnableRefundCreationRoutine EnableRefundRoutine { get; private set; }

        private readonly DateTime startDateTime;
        private string omOrderNumber;
        private string orderNumber;
        private string sourceOrderNumber;

        protected IDataLayerFactory DataLayerFactory { get; private set; }

        protected BaseSteps(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
        {
            DataLayerFactory = dataLayerFactory;

            FulfillmentRoutine = new FulfillmentRoutine(DataLayerFactory, systemEnvironment);
            UpdateRefundRoutine = new UpdateRefundRoutine(DataLayerFactory, systemEnvironment);
            CreationRoutine = new CreationRoutine(DataLayerFactory, systemEnvironment);
            EnableRefundRoutine = new EnableRefundCreationRoutine(DataLayerFactory, systemEnvironment);

            omOrderNumber = null;
            orderNumber = null;
            sourceOrderNumber = null;

            startDateTime = DateTime.Now;
        }

        protected void PrepareFulfillmentRequestForPartitialCancellation()
        {
            FulfillmentRoutine.CreateRequestOrderHeader();
            FulfillmentRoutine.AddOrderLine("100005", "Heavy Duty Staple Gun (FOB)", 2, 2.2M);
            FulfillmentRoutine.CurrentRequest.OrderHeader.Source = string.Empty;
        }

        protected void PrepareFulfillmentRequestForStoreCollectionOrder()
        {
            PrepareFulfillmentRequestForStoreDeliveryOrder();
            FulfillmentRoutine.CurrentRequest.OrderHeader.ToBeDelivered = new CTSFulfilmentRequestOrderHeaderToBeDelivered() { Value = false };
            FulfillmentRoutine.CurrentRequest.OrderHeader.DeliveryCharge = new CTSFulfilmentRequestOrderHeaderDeliveryCharge() { Value = 0M };
        }

        protected void ProcessPreparedFulfillmentRequest()
        {
            FulfillmentRoutine.ProcessPreparedRequest();
            omOrderNumber = FulfillmentRoutine.CurrentResponse.OrderHeader.OMOrderNumber.Value;
            orderNumber = FulfillmentRoutine.CurrentResponse.FulfillingSiteOrderNumber.Value;
            sourceOrderNumber = FulfillmentRoutine.CurrentResponse.OrderHeader.SourceOrderNumber.Value;
        }

        protected void PrepareRefundRequestForStoreDeliveryOrder()
        {
            UpdateRefundRoutine.CreateRequestOrderHeader(omOrderNumber);
            foreach (CTSFulfilmentRequestOrderLine orderLine in FulfillmentRoutine.CurrentRequest.OrderLines)
            {
                UpdateRefundRoutine.AddOrderRefund(FulfillmentRoutine.StoreId, orderLine.OMOrderLineNo.Value, orderLine.ProductCode.Value, Int32.Parse(orderLine.TotalOrderQuantity.Value.ToString()), Decimal.Parse(orderLine.LineValue.Value.ToString()));
            }
        }

        protected void PrepareRefundRequestForClickCollectOrder()
        {
            EnableRefundRoutine.PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);
        }

        protected void PrepareRefundRequestForWickedHourlyServiceOrder()
        {
            EnableRefundRoutine.PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);
            EnableRefundRoutine.CurrentRequest.RefundHeader.DeliveryChargeRefundValue = 10m;
        }

        protected void ProcessPreparedRefundRequest()
        {
            UpdateRefundRoutine.ProcessPreparedRequest();
            omOrderNumber = UpdateRefundRoutine.CurrentResponse.OMOrderNumber.Value;
        }

        protected void ProcessPreparedEnableRefundRequest()
        {
            EnableRefundRoutine.ProcessPreparedRequest();
        }

        protected void PrepareRequestForClickCollectOrder()
        {
            CreationRoutine.CreateRequestOrderHeader();
            CreationRoutine.CreateOrderLines(1);
            CreationRoutine.SetupOrderLine(0, "100005", 1, 5.05M);
        }

        protected void PrepareRequestForWickesHourlyServiceOrder()
        {
            PrepareRequestForClickCollectOrder();
            CreationRoutine.CurrentRequest.OrderHeader.ToBeDelivered.Value = true;
            CreationRoutine.CurrentRequest.OrderHeader.DeliveryCharge.Value = 10M;
        }

        protected void ProcessPreparedEnableRequest()
        {
            CreationRoutine.ProcessPreparedRequest();
            orderNumber = CreationRoutine.CurrentResponse.OrderHeader.StoreOrderNumber.Value;
            sourceOrderNumber = CreationRoutine.CurrentResponse.OrderHeader.SourceOrderNumber.Value;

            var qodHeader = LoadOrderHeader();
            omOrderNumber = qodHeader.OmOrderNumber.ToString();
        }

        protected QodHeader LoadOrderHeader()
        {
            return QodOrder.Get(orderNumber).Header;
        }

        protected DateTime GetTomorrowDate()
        {
            return DateTime.Now.AddDays(1);
        }

        protected void SetDeliveryNotificationFlag(bool value)
        {
            DataLayerFactory.Create<TestMiscRepository>().SetSettingValue(ParameterId.NotifyDeliveryService, value);
            }

        #region Methods for Partial Cancellation Consignment

        protected void PrepareFulfillmentRequestForStoreDeliveryWebOrder()
        {
            FulfillmentRoutine.CreateRequestOrderHeader();
            FulfillmentRoutine.AddOrderLine("100005", "Heavy Duty Staple Gun (FOB)", 1, 2.2M);
        }

        protected void PrepareFulfillmentRequestForStoreDeliveryOrder()
        {
            PrepareFulfillmentRequestForStoreDeliveryWebOrder();
            FulfillmentRoutine.CurrentRequest.OrderHeader.Source = string.Empty;
        }

        protected void PrepareRefundRequestForPartitialCancellation()
        {
            UpdateRefundRoutine.CreateRequestOrderHeader(omOrderNumber);
            foreach (CTSFulfilmentRequestOrderLine orderLine in FulfillmentRoutine.CurrentRequest.OrderLines)
            {
                UpdateRefundRoutine.AddOrderRefund(FulfillmentRoutine.StoreId, orderLine.OMOrderLineNo.Value, orderLine.ProductCode.Value, 1, Decimal.Parse(orderLine.LineValue.Value.ToString()));
            }
        }
        
        #endregion

        protected ExternalRequest FindExternalRequest(string type)
        {
            return DataLayerFactory.Create<TestExternalRequestRepository>().FindExternalRequest(type, omOrderNumber, startDateTime);
        }
    }
}
