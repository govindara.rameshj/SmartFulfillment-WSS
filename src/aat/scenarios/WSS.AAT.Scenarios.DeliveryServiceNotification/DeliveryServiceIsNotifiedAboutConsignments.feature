@iPadio
Feature: Check delivery service is to be notified about consignments
    As a store colleague
    I want the delivery service to be notified about new consignments and their changes
    So that delivery service has up-to-date information

Scenario: Store receives new store consignment
    Given new delivery order created in a selling store
    When the fulfilling store receives it
    Then the notification about new consignment for delivery service is created 

Scenario: Store receives new web delivery order consignment
    Given new delivery order created on a web site
    When the fulfilling store receives it
    Then the notification about new consignment for delivery service is created 

Scenario: Store updates consignment date
    Given  new delivery order is created in a selling store with delivery date "today"
    When delivery date is changed from "today" to "tomorrow"
    Then the notification about update consignment for delivery service is created 

Scenario: Customer contact details are updated
    Given new delivery order created in a selling store
    When customer contact details are updated
    Then the notification about update consignment for delivery service is created

Scenario: Customer delivery details are updated
    Given new delivery order created in a selling store
    When customer delivery details are updated
    Then the notification about update consignment for delivery service is created 

Scenario: Web delivery order updates consignment date
    Given  new delivery web order is created with delivery date "today"
    When delivery date is changed from "today" to "tomorrow"
    Then the notification about update consignment for delivery service is created 

Scenario: Customer contact details are updated for web delivery order
    Given new delivery web order created
    When customer contact details are updated
    Then the notification about update consignment for delivery service is created

Scenario: Customer delivery details are updated for web delivery order
    Given new delivery web order created
    When customer delivery details are updated
    Then the notification about update consignment for delivery service is created 

Scenario: Store receives full order cancellation consignment
    Given new delivery order created in a selling store
    And the the fulfilling store receives it
    And full delivery order cancellation consignment is sent to store
    When the fulfilling store receives cancellation consignment
    Then the notification about cancellation consignment for delivery service is created
    
Scenario: Store receives full order cancellation consignment for web delivery order
    Given new delivery order created on a web site
    And the the fulfilling store receives it
    And full delivery order cancellation consignment is sent to store
    When the fulfilling store receives cancellation consignment
    Then the notification about cancellation consignment for delivery service is created  
