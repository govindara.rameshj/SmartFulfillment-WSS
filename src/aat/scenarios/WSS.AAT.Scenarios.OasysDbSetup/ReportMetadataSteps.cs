using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Utility;
using WSS.AAT.Common.DataLayer;
using Cts.Oasys.Core.System.MenuOption;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.OasysDbSetup
{
    [Binding]
    public class ReportMetadataSteps : BaseStepDefinitions
    {
        private readonly TablesRepository _tablesRepo;

        public ReportMetadataSteps(IDataLayerFactory dataLayerFactory)
        {
            _tablesRepo = dataLayerFactory.Create<TablesRepository>();
        }

        [Given(@"that I am viewing the managers Dashboard")]
        public void GivenThatIAmViewingTheManagersDashboard()
        {
            //nothing TODO here
        }
        
        [When(@"I open the tab to view or print a copy of the dashboard")]
        public void WhenIOpenTheTabToViewOrPrintACopyOfTheDashboard()
        {
            //nothing TODO here
        }

        [Given(@"that I am checking database configuration")]
        public void GivenThatIAmCheckingDatabaseConfiguration()
        {
            //nothing TODO here
        }


        [Then(@"I expect the layout of ""(.*)"" table as documented:")]
        public void ThenIExpectTheLayoutOfTableAsDocumented(string tableName, Table tableExpected)
        {
            var tableComparator = new TableComparator(_tablesRepo);

            string tableDifference = string.Empty;
            
            switch (tableName)
            {
                case "Report":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int>(rt.Id), repo => repo.GetTableReport());
                    break;
                case "ReportColumn":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int>(rt.Id), repo => repo.GetTableReportColumn());
                    break;
                case "ReportColumns":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int, int, int>(rt.ReportId, rt.TableId, rt.ColumnId), repo => repo.GetTableReportColumns());
                    break;
                case "ReportHyperlink":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int>(rt.ReportId), repo => repo.GetTableReportHyperlink());
                    break;
                case "ReportParameters":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int, int, int>(rt.ReportId, rt.ParameterId, rt.Sequence), repo => repo.GetTableReportParameters());
                    break;
                case "ReportTable":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int, int>(rt.ReportId, rt.Id), repo => repo.GetTableReportTable());
                    break;
                case "ReportGrouping":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int, int, int, int>(rt.ReportId, rt.TableId, rt.ColumnId, rt.Sequence), repo => repo.GetTableReportGrouping());
                    break;
                case "ReportRelation":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int, string>(rt.ReportId, rt.Name), repo => repo.GetTableReportRelation());
                    break;
                case "ReportSummary":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int, int, int>(rt.ReportId, rt.TableId, rt.ColumnId), repo => repo.GetTableReportSummary());
                    break;
                case "MenuConfig":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => Tuple.Create<int>(rt.Id), repo => repo.GetTableMenuConfig());
                    break;
                case "ProfilemenuAccess":
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => rt.Id, repo => repo.GetTableProfilemenuAccess());
                    break;
                case "Parameters":
                    PropertiesFileHelper propertiesFile = new PropertiesFileHelper();
                    var databaseVersion = propertiesFile.GetDbVersion().ToString(3);
                    var versinRow = tableExpected.Rows.First(r => r["StringValue"] == "<version>");
                    versinRow["StringValue"] = databaseVersion;
                    tableDifference = tableComparator.CompareActualAndExpected(tableExpected, rt => rt.Id, repo => repo.GetTableParameters());
                    break;
                default:
                    Assert.Fail(tableName + " is incorrect table name");
                    break;
            }

            if (tableDifference.Length != 0)
            {
                Assert.Fail(tableDifference.ToString());
            }

        }

        [Then(@"I expect description for menu id ""(.*)"" to be equal to ""(.*)""")]
        public void ThenIExpectDescriptionForMenuIdToBeEqualTo(int menuId, string expectedDescription)
        {
            Item menuItem = Item.GetSingleMenuItem(menuId);
            Assert.That(menuItem.Description == expectedDescription);
        }
    }
}
