@OasysDB
Feature: ReportMetadata
    As a manager
    I want the Managers Dashboard to be configured as documented
    So that don't need to configure it

Scenario: Check report metadata
  Given that I am viewing the managers Dashboard
  When I open the tab to view or print a copy of the dashboard
  Then I expect the layout of "Report" table as documented:
| Id  | Header                      | Title                                        | ProcedureName                    | HideWhenNoData | PrintLandscape | MinWidth | MinHeight |
| 1   | Manager Dashboard           | Store Sales                                  | DashSales2                       | false          | false          | 450      | 315       |
| 2   | Manager Dashboard           | DRL Analysis                                 | DashDrl                          | false          | false          | 420      | 160       |
| 3   | Manager Dashboard           | Price Overrides                              | DashPrices                       | false          | false          | 420      | 95        |
| 4   | Manager Dashboard           | Shrinkage                                    | DashShrinkage                    | false          | false          | 420      | 195       |
| 5   | Manager Dashboard           | Deliveries Sold                              | DashDeliveries2                  | false          | false          | 450      | 160       |
| 6   | Manager Dashboard           | Banking                                      | DashBanking                      | false          | false          | 350      | 200       |
| 7   | Manager Dashboard           | Stock Reports                                | DashStock2                       | false          | false          | 420      | 245       |
| 8   | Manager Dashboard           | Price Changes                                | DashPriceChanges2                | false          | false          | 350      | 250       |
| 9   | Manager Dashboard           | Banking Reports                              | DashActivity2                    | false          | false          | 350      | 300       |
| 10  | Manager Dashboard           | Pending Orders                               | DashQod                          | false          | false          | 450      | 250       |
| 11  | Manager Dashboard           | Daily Order Status                           | DashQodDate                      | false          | false          | 420      | 185       |
| 12  | MissingTrans                | Missing Transactions                         | MissingTransactions              | false          | false          | 300      | 140       |
| 13  | Manager Dashboard           | Pending Shrinkage                            | DashPendingShrinkage             | false          | false          | 420      | 120       |
| 50  | Stock Enquiry               | Pricing                                      | EnquiryPricing                   | false          | false          | 400      | 270       |
| 51  | Stock Enquiry               | Stock                                        | EnquiryStock                     | false          | false          | 400      | 270       |
| 52  | Stock Enquiry               | Sales                                        | EnquirySales                     | false          | false          | 400      | 270       |
| 53  | Stock Enquiry               | Outstanding Orders                           | EnquiryOutstandingOrder          | false          | false          | 400      | 270       |
| 54  | Stock Enquiry               | General                                      | EnquiryGeneral                   | false          | false          | 400      | 270       |
| 55  | Stock Enquiry               | Hierarchy                                    | EnquiryHierarchy                 | false          | false          | not set  | not set   |
| 57  | Stock Enquiry               | Related Item Single                          | EnquiryRelatedItemSingle         | true           | false          | 400      | 270       |
| 58  | Stock Enquiry               | Related Item Bulk                            | EnquiryRelatedItemBulk           | true           | false          | 400      | 270       |
| 59  | Stock Enquiry               | DRL Listing                                  | EnquiryReceipts                  | false          | false          | not set  | not set   |
| 60  | Stock Enquiry               | Labels                                       | EnquiryLabel                     | false          | false          | not set  | not set   |
| 61  | Stock Enquiry               | Image                                        | EnquiryStockPim                  | false          | false          | 440      | 420       |
| 63  | Stock Enquiry               | Related Item                                 | usp_EnquiryRelatedItem           | true           | false          | 400      | 270       |
| 64  | Stock Enquiry               | Stock Management                             | usp_EnquiryStockManagement       | true           | false          | 440      | 420       |
| 70  | Sales                       | Sales                                        | StockGetWeeklySales              | false          | false          | not set  | not set   |
| 71  | Stock Administration Report | PIC Stock Count                              | StockGetPicStockCounts           | false          | false          | not set  | not set   |
| 72  | Stock Movements             | Stock Movements Detail                       | StockLogGetDetailByStockGroup    | false          | true           | 1200     | not set   |
| 73  | Stock Movements             | Stock Movements Weekly                       | StockLogGetWeeklySummaryByStock  | false          | false          | not set  | not set   |
| 74  | Shrinkage History           | Shrinkage History                            | AdjustGetStockShrinkage          | false          | false          | not set  | not set   |
| 75  | Stock Get Information       | Stock Information                            | StockGetProductInformation       | false          | true           | not set  | not set   |
| 76  | Stock Return Policy         | Return Policy                                | StockGetReturnPolicy             | false          | false          | 540      | not set   |
| 77  | Price Changes               | Price Change History                         | PriceChangesGetByStock           | false          | true           | not set  | not set   |
| 78  | Suppliers                   | Supplier Details                             | SupplierGetOrderDetails          | false          | false          | not set  | not set   |
| 79  | Purchase Orders             | PO History                                   | PoGetHistoryByStock              | false          | false          | not set  | not set   |
| 80  | Stock Get Event Header      | Event Header                                 | EventGetHeaderByStock            | false          | false          | not set  | not set   |
| 81  | Sales                       | Last Sale Transaction                        | SalesGetHeaderLines              | false          | false          | not set  | not set   |
| 82  | Stock Get Event Master      | Event Master                                 | GetEventMaster                   | false          | false          | not set  | not set   |
| 83  | Events                      | Events By Type                               | EventGetHeaderByEvent            | false          | false          | not set  | not set   |
| 84  | KEYS                        | Key Comments                                 | StockLogDecodeComment            | false          | false          | not set  | not set   |
| 85  | Events                      | All Events                                   | usp_EventsGetEventsBySku         | false          | true           | not set  | not set   |
| 100 | Stock Administration Report | Out Of Stock Items                           | StockGetOutOfStockItems          | false          | false          | not set  | not set   |
| 200 | Stock Administration Report | PIC Variance Report                          | PicVarianceReport                | false          | true           | not set  | not set   |
| 201 | Stock Administration Report | PIC Stock Count Sample                       | PicGetStockCountSample           | false          | true           | not set  | not set   |
| 203 | Stock Administration Report | Gap Walk Report                              | GapWalkGetItems                  | false          | false          | not set  | not set   |
| 204 | Stock Administration Report | Zero Stock Report                            | StockGetZeroStocks               | false          | false          | not set  | not set   |
| 205 | Stock Administration Report | Non-Stock & Deleted Stock with On Hand Stock | StockGetNonStockDeletedWithStock | false          | true           | not set  | not set   |
| 206 | Stock Administration Report | Markdown Stock Report                        | MarkdownsOnHandGet               | false          | true           | not set  | not set   |
| 210 | Stock Administration Report | Issue Status Report                          | IssueGetStatusReport             | false          | false          | not set  | not set   |
| 220 | PIC Worksheet               | Count                                        | PicWorksheetCount                | false          | true           | not set  | not set   |
| 221 | PIC Worksheet               | Supplier                                     | PicWorksheetSupplier             | false          | true           | not set  | not set   |
| 222 | PIC Worksheet               | Hierarchy                                    | PicWorksheetHierarchy            | false          | true           | not set  | not set   |
| 223 | PIC Worksheet               | Cycle                                        | PicWorksheetCycle                | false          | true           | not set  | not set   |
| 300 | Sales                       | Daily Sales Voucher Report                   | SalesGetVoucherSales             | false          | false          | not set  | not set   |
| 301 | Sales                       | Daily Credit Card Summary Report             | SalesGetCreditCardSummary        | false          | false          | not set  | not set   |
| 302 | Sales                       | Daily Credit Card Detailed Report            | SalesGetCreditCardDetailed       | false          | false          | not set  | not set   |
| 303 | Sales                       | Zero Sales Report                            | SalesGetZeroSales                | false          | false          | not set  | not set   |
| 304 | Sales                       | Price Violations Report                      | PriceViolationsGet               | false          | true           | not set  | not set   |
| 305 | Sales                       | Colleague Discount Report                    | ColleagueDiscountGet             | false          | true           | not set  | not set   |
| 306 | Sales                       | Gift Voucher / Card Report                   | GiftVoucherGet                   | false          | true           | not set  | not set   |
| 307 | Sales                       | Card Report                                  | CardReportGet                    | false          | false          | not set  | not set   |
| 308 | Sales                       | Incorrect Day Price                          | usp_IncorrectDayPriceGet         | false          | true           | not set  | not set   |
| 309 | Sales                       | Temporary Deal Groups                        | usp_TemporaryDealGroupsGet       | false          | true           | not set  | not set   |
| 310 | Sales                       | Audit Roll Enquiry                           | SalesTransactionGet              | false          | true           | not set  | not set   |
| 320 | Sales                       | Vision Sales Report                          | VisionSalesGet                   | false          | true           | not set  | not set   |
| 321 | Sales                       | Duress Code Report                           | DuressCodeReportGet              | false          | true           | not set  | not set   |
| 330 | Sales                       | Explosives Report                            | GetExplosives                    | false          | true           | not set  | not set   |
| 400 | QOD                         | Order Delivery Listing Report                | QODOrderDeliveryListing          | false          | true           | not set  | not set   |
| 401 | QOD                         | Delivery Income Detail                       | QODDeliveryIncome                | false          | true           | not set  | not set   |
| 402 | QOD                         | Delivery Income Detail                       | QODDeliveryIncomeSummary         | false          | true           | not set  | not set   |
| 403 | QOD                         | Despatched Orders                            | QODDespatchedOrders              | false          | true           | not set  | not set   |
| 404 | QOD                         | Orders Awaiting Despatch                     | QODOrdersAwaitingDespatch        | false          | true           | not set  | not set   |
| 405 | QOD                         | Quote Conversions                            | QODQuoteConversions              | false          | true           | not set  | not set   |
| 406 | Purchase Orders             | Order Variances                              | ReceiptGetOrderVariances         | false          | true           | not set  | not set   |
| 420 | Purchase Orders             | DRL Report                                   | ReceiptGetReport                 | false          | false          | not set  | not set   |
| 425 | Purchase Orders             | Below Impacts Report                         | usp_ReportStockBelowImpact       | false          | false          | 520      | 240       |
| 500 | Purchase Orders             | Open Returns                                 | ReturnsGetOpenReturns            | false          | false          | not set  | not set   |
| 501 | Stores                      | Stores                                       | StoreGetAll                      | false          | true           | not set  | not set   |
| 600 | Till Flash                  | Flash Totals                                 | TillGetCurrentTotals             | false          | false          | not set  | not set   |
| 610 | Banking                     | Miscellaneous Income Report                  | BankingMiscIncomeGet             | false          | true           | not set  | not set   |
| 611 | Banking                     | Cashier Performance Report                   | BankingCashierPerformanceGet     | false          | true           | not set  | not set   |
| 612 | Banking                     | End Of Day Check Report                      | NewBankingEndOfDayCheckReport    | false          | false          | not set  | not set   |
| 613 | Banking                     | Banking Report                               | usp_NewBanking_BankingReport     | false          | false          | not set  | not set   |
| 620 | Stock Administration Report | PIC Count History                            | StockGetPicCountHistory          | false          | false          | not set  | not set   |
And I expect the layout of "ReportColumn" table as documented:
| Id    | Name                               | Caption                            | FormatType | Format     | IsImagePath | MinWidth | MaxWidth | Alignment |
| 0     | RowId                              | Row Id                             | 0          | not set    | false       | not set  | not set  | not set   |
| 10    | Column1                            | not set                            | 0          | not set    | false       | 100      | 100      | not set   |
| 20    | Column2                            | not set                            | 0          | not set    | false       | 100      | not set  | not set   |
| 30    | Column3                            | not set                            | 0          | not set    | false       | 100      | 100      | not set   |
| 40    | Column4                            | not set                            | 0          | not set    | false       | 100      | not set  | not set   |
| 50    | Column5                            | not set                            | 0          | not set    | false       | 100      | 100      | 3         |
| 80    | Signature1                         |                                    | 0          | not set    | false       | not set  | not set  | not set   |
| 81    | Signature2                         |                                    | 0          | not set    | false       | not set  | not set  | not set   |
| 100   | Description                        | Description                        | 0          | not set    | false       | 100      | 400      | 1         |
| 101   | Description                        | Description                        | 0          | not set    | false       | not set  | not set  | not set   |
| 102   | Display                            | Display                            | 0          | not set    | false       | not set  | not set  | 3         |
| 103   | Description                        | Description                        | 0          | not set    | false       | 170      | not set  | not set   |
| 105   | Comment                            | Comment                            | 0          | not set    | false       | 55       | not set  | not set   |
| 106   | Type                               | Type                               | 0          | not set    | false       | not set  | not set  | not set   |
| 107   | CardType                           | Card Type                          | 0          | not set    | false       | not set  | not set  | not set   |
| 112   | Sequence                           | Sequence                           | 0          | not set    | false       | 75       | 75       | 2         |
| 130   | Code                               | Code                               | 0          | not set    | false       | 60       | 60       | not set   |
| 131   | ReasonCode                         | Reason Code                        | 0          | not set    | false       | not set  | not set  | not set   |
| 132   | AuthCode                           | Auth Code                          | 0          | not set    | false       | 75       | 75       | not set   |
| 133   | RegionCode                         | Region Code                        | 0          | not set    | false       | not set  | not set  | not set   |
| 134   | CountryCode                        | Country Code                       | 0          | not set    | false       | not set  | not set  | not set   |
| 135   | ProductCode                        | Product Code                       | 0          | not set    | false       | 70       | 70       | 2         |
| 136   | AccountCode                        | Account Code                       | 0          | not set    | false       | not set  | not set  | not set   |
| 140   | PostCode                           | Post Code                          | 0          | not set    | false       | not set  | 75       | not set   |
| 150   | Id                                 | ID                                 | 0          | not set    | false       | 40       | 40       | 1         |
| 151   | TillId                             | Till ID                            | 1          | n0         | false       | not set  | 60       | 2         |
| 152   | UserId                             | User ID                            | 1          |            | false       | 60       | 70       | 2         |
| 153   | ManagerId                          | Manager ID                         | 0          | not set    | false       | 70       | 70       | not set   |
| 154   | CashierId                          | Cashier ID                         | 1          | n0         | false       | 60       | 70       | 2         |
| 155   | SupervisorId                       | Sup Id                             | 0          | not set    | false       | 50       | 60       | not set   |
| 156   | SupervisorIdVoided                 | Sup Voided Id                      | 0          | not set    | false       | not set  | not set  | not set   |
| 158   | TillNo                             | Till                               | 1          | not set    | false       | not set  | not set  | not set   |
| 159   | TranNo                             | Missing Transaction                | 1          | not set    | false       | not set  | not set  | not set   |
| 200   | Number                             | Number                             | 1          | n0         | false       | not set  | not set  | not set   |
| 201   | SkuNumber                          | SKU                                | 0          | not set    | false       | 45       | 45       | 2         |
| 202   | SkuNumber                          | sku                                | 0          | not set    | false       | not set  | not set  | not set   |
| 203   | SkuRelated                         | Bulk SKU                           | 0          | not set    | false       | 45       | 45       | not set   |
| 210   | SupplierNumber                     | Supplier Number                    | 0          | not set    | false       | not set  | not set  | not set   |
| 211   | SupplierNumber                     | Supplier Number                    | 0          | not set    | false       | not set  | not set  | not set   |
| 220   | OrderNumber                        | Order Number                       | 0          | not set    | false       | not set  | 75       | 2         |
| 221   | DrlNumber                          | DRL Number                         | 0          | not set    | false       | not set  | not set  | 2         |
| 222   | PoNumber                           | PO Number                          | 0          | not set    | false       | not set  | not set  | not set   |
| 223   | ReceiptNumber                      | DRL Number                         | 0          | not set    | false       | not set  | not set  | 3         |
| 224   | ContainerNumber                    | Con Number                         | 0          | not set    | false       | not set  | not set  | 3         |
| 225   | CustomerNumber                     | Cust Number                        | 0          | not set    | false       | not set  | not set  | not set   |
| 230   | TranNumber                         | Transaction Number                 | 1          |            | false       | not set  | not set  | 2         |
| 231   | CouponNumber                       | Coupon Number                      | 0          | not set    | false       | not set  | not set  | not set   |
| 232   | MixMatchNumber                     | Mix Match Number                   | 0          | not set    | false       | not set  | not set  | not set   |
| 233   | DealGroupNumber                    | Deal Group Number                  | 0          | not set    | false       | not set  | not set  | not set   |
| 234   | RevisionNumber                     | Rev#                               | 0          | not set    | false       | not set  | not set  | not set   |
| 235   | CardNumber                         | Card                               | 0          | not set    | false       | not set  | not set  | not set   |
| 236   | CycleNumber                        | Cycle Number                       | 1          | n0         | false       | not set  | not set  | 3         |
| 237   | LineNumber                         | Line                               | 1          | not set    | false       | 45       | 45       | not set   |
| 238   | ReasonDescription                  | Reason Description                 | 0          | not set    | false       | 80       | not set  | not set   |
| 239   | SerialNumber                       | Serial Number                      | 0          | not set    | false       | not set  | not set  | not set   |
| 240   | DocumentNumber                     | Doc Number                         | 0          | not set    | false       | not set  | not set  | not set   |
| 241   | AccountNumber                      | Account Number                     | 0          | not set    | false       | not set  | not set  | not set   |
| 250   | PhoneNumber                        | Phone Number                       | 0          | not set    | false       | 70       | 70       | 3         |
| 251   | FaxNumber                          | Fax Number                         | 0          | not set    | false       | 70       | 70       | 3         |
| 280   | ImagePath                          | Image                              | 0          | not set    | true        | not set  | not set  | not set   |
| 281   | Item                               | Item                               | 0          | not set    | false       | not set  | not set  | not set   |
| 282   | Event                              | Event                              | 0          | not set    | false       | not set  | not set  | not set   |
| 300   | Name                               | Name                               | 0          | not set    | false       | 100      | not set  | not set   |
| 301   | UserName                           | User Name                          | 0          | not set    | false       | 90       | not set  | not set   |
| 302   | ManagerName                        | Manager                            | 0          | not set    | false       | not set  | not set  | not set   |
| 303   | SupplierName                       | Supplier Name                      | 0          | not set    | false       | 200      | 300      | not set   |
| 304   | CustomerName                       | Customer Name                      | 0          | not set    | false       | 100      | 120      | not set   |
| 305   | CashierName                        | Cashier Name                       | 0          | not set    | false       | 90       | not set  | 2         |
| 306   | SupervisorName                     | Sup Name                           | 0          | not set    | false       | 60       | not set  | not set   |
| 310   | OrderTaker                         | Order Taker                        | 0          | not set    | false       | not set  | not set  | not set   |
| 320   | ColleagueCard                      | Colleague Card                     | 0          | not set    | false       | not set  | not set  | not set   |
| 321   | ColleagueName                      | Colleague Name                     | 0          | not set    | false       | not set  | not set  | not set   |
| 400   | Origin                             | Origin                             | 0          | not set    | false       | not set  | not set  | not set   |
| 401   | Origin                             | Origin                             | 0          | not set    | false       | 150      | not set  | not set   |
| 430   | DaysOutStock                       | Which Days OOS                     | 0          | not set    | false       | not set  | not set  | not set   |
| 431   | VoucherType                        | Voucher Type                       | 0          | not set    | false       | not set  | not set  | not set   |
| 801   | Peg                                | Peg                                | 1          | n0         | false       | 45       | 45       | 2         |
| 802   | Small                              | Small                              | 1          | n0         | false       | 55       | 55       | 2         |
| 803   | Medium                             | Medium                             | 1          | n0         | false       | 65       | 65       | 2         |
| 804   | Description                        | Description                        | 0          | not set    | false       | 130      | not set  | not set   |
| 805   | Qty                                | Qty                                | 1          | n0         | false       | 40       | 40       | 2         |
| 1000  | Count                              | Count                              | 1          | n0         | false       | not set  | not set  | not set   |
| 1005  | OnHandCount                        | On Hand Count                      | 0          | not set    | false       | not set  | not set  | not set   |
| 1010  | MarkdownCount                      | Markdown Count                     | 0          | not set    | false       | not set  | not set  | not set   |
| 1011  | WriteOffCount                      | Write Off Count                    | 0          | not set    | false       | not set  | not set  | not set   |
| 1020  | QuotesRaised                       | Quotes Raised                      | 1          | n0         | false       | not set  | not set  | not set   |
| 1021  | OrdersRaised                       | Orders Raised                      | 1          | n0         | false       | not set  | not set  | not set   |
| 1022  | QuotesDelivered                    | Quotes Del                         | 1          | n0         | false       | not set  | not set  | not set   |
| 1100  | Qty                                | Qty                                | 1          | n0         | false       | 50       | 50       | 2         |
| 1101  | QtyWtd                             | WTD                                | 1          | n0         | false       | 50       | 50       | 2         |
| 1102  | QtyTotal                           | Total                              | 1          | n0         | false       | 55       | 55       | 3         |
| 1105  | QtyVariance                        | Variance                           | 1          | n0         | false       | 70       | 70       | 3         |
| 1110  | QtyPerPack                         | QPP                                | 1          | n0         | false       | 60       | 60       | 3         |
| 1111  | QtyPreSold                         | P/S                                | 1          | n0         | false       | 50       | 50       | 3         |
| 1112  | QtyBulkToSingle                    | B/S Xfer                           | 1          | n0         | false       | 60       | not set  | 3         |
| 1113  | QtyReceipt                         | Receipt                            | 1          | n0         | false       | 60       | not set  | 3         |
| 1114  | QtyIbts                            | IBT's                              | 1          | n0         | false       | 60       | not set  | 3         |
| 1115  | QtyReturns                         | Returns                            | 1          | n0         | false       | 60       | not set  | 3         |
| 1116  | QtyAdjusts                         | Adjusts                            | 1          | n0         | false       | 60       | not set  | 3         |
| 1117  | QtyRequired                        | Required                           | 1          | n0         | false       | 70       | not set  | 3         |
| 1118  | QtyPick                            | Pick Qty                           | 1          | n0         | false       | 60       | not set  | 3         |
| 1119  | QtyAdjusted                        | Adjusted                           | 1          | n0         | false       | 70       | not set  | 3         |
| 1120  | QtyOnHand                          | SOH                                | 1          | n0         | false       | 50       | 60       | 2         |
| 1121  | QtyOnOrder                         | On Order                           | 1          | n0         | false       | 60       | 60       | 3         |
| 1122  | QtyMarkdown                        | M/D                                | 1          | n0         | false       | 50       | 50       | 3         |
| 1123  | QtyWriteOff                        | Write Off                          | 1          | n0         | false       | 70       | not set  | 3         |
| 1124  | QtyOrdered                         | Ordered                            | 1          |            | false       | 60       | not set  | 2         |
| 1125  | QtyTaken                           | Qty Taken                          | 1          | n0         | false       | 60       | not set  | 3         |
| 1126  | QtyDel                             | Qty to Del                         | 1          | n0         | false       | 60       | not set  | 3         |
| 1127  | QtyReceived                        | Qty Received                       | 1          |            | false       | 60       | not set  | 2         |
| 1128  | QtyShortOver                       | Over/Short Qty                     | 1          | n0         | false       | 60       | not set  | 3         |
| 1129  | QtyDaysOutStock                    | Days OOS                           | 1          | n0         | false       | 60       | not set  | 3         |
| 1130  | QtySold                            | Sold                               | 1          | n0         | false       | 60       | not set  | 3         |
| 1131  | QtySales                           | Sales                              | 1          | n0         | false       | 60       | not set  | 3         |
| 1132  | QtyDaysZeroSales                   | Days Zero Sales                    | 1          | n0         | false       | 60       | not set  | 3         |
| 1133  | QtyDaysZeroStock                   | Days Zero Stock                    | 1          | n0         | false       | 60       | not set  | 3         |
| 1134  | QtyDeliveryItems                   | Items                              | 1          | n0         | false       | 60       | not set  | 3         |
| 1135  | QtyShopFloor                       | S/F                                | 1          | n0         | false       | 50       | 50       | 3         |
| 1136  | QtyWarehouse                       | W/H                                | 1          | n0         | false       | 50       | 50       | 3         |
| 1137  | QtyIssued                          | Issued                             | 1          | n0         | false       | 60       | 60       | 3         |
| 1138  | QtyMarkdownOnHand                  | MOH                                | 1          | n0         | false       | 50       | 60       | 3         |
| 1139  | QtyBuy                             | Buy Qty                            | 1          | n0         | false       | not set  | not set  | not set   |
| 1150  | StockStart                         | Start Stock                        | 1          |            | false       | 60       | not set  | 2         |
| 1151  | StockEnd                           | End Stock                          | 1          |            | false       | 60       | not set  | 2         |
| 1152  | StockCount                         | Stock Count                        | 1          | n0         | false       | not set  | not set  | 3         |
| 1153  | StockOpen                          | Open Stock                         | 1          | n0         | false       | not set  | not set  | 3         |
| 1154  | StockClose                         | Close Stock                        | 1          | n0         | false       | not set  | not set  | 3         |
| 1160  | MarkdownStart                      | Start MD                           | 1          |            | false       | 60       | 60       | 2         |
| 1161  | MarkdownEnd                        | End MD                             | 1          |            | false       | 60       | 60       | 2         |
| 1162  | WriteOffStart                      | Start W/O                          | 1          |            | false       | 60       | 60       | 2         |
| 1163  | WriteOffEnd                        | End W/O                            | 1          |            | false       | 60       | 60       | 2         |
| 1200  | Value                              | Value                              | 1          | c2         | false       | 55       | not set  | 3         |
| 1201  | ValueWtd                           | WTD                                | 1          | c2         | false       | 60       | not set  | 3         |
| 1202  | SaleValue                          | Sale Value                         | 1          | c2         | false       | 60       | not set  | 3         |
| 1203  | TenderValue                        | Tender Value                       | 1          | c2         | false       | 60       | not set  | 3         |
| 1204  | Income                             | Income                             | 1          | c2         | false       | 60       | not set  | 3         |
| 1205  | ValueCash                          | Cash Value                         | 1          | c2         | false       | not set  | not set  | 3         |
| 1206  | ValueNonCash                       | Non Cash value                     | 1          | c2         | false       | not set  | not set  | 3         |
| 1207  | ValueShortOver                     | Over/Short Value                   | 1          | c2         | false       | 60       | not set  | 3         |
| 1208  | ValueAverageSales4Weeks            | 4 Week Average                     | 1          | c2         | false       | 70       | 75       | 3         |
| 1209  | DeliveryCharge                     | Del Income                         | 1          | c2         | false       | not set  | not set  | 3         |
| 1210  | ValueOnHand                        | Value                              | 1          | c2         | false       | not set  | not set  | not set   |
| 1211  | ValueDeleted                       | Value Deleted                      | 1          | c2         | false       | 55       | not set  | 3         |
| 1212  | ValueNonStock                      | Value NonStock                     | 1          | c2         | false       | 55       | not set  | 3         |
| 1213  | ValueTransaction                   | Transaction Value                  | 1          | c2         | false       | not set  | not set  | not set   |
| 1214  | ValueDiscount                      | Discount Value                     | 1          | c2         | false       | not set  | not set  | 2         |
| 1215  | ValueTax                           | Tax Value                          | 1          | c2         | false       | not set  | not set  | not set   |
| 1216  | ValueMerchandise                   | Merchandise Value                  | 1          | c2         | false       | 95       | not set  | not set   |
| 1217  | ValueVat                           | VAT Value                          | 1          | c2         | false       | not set  | not set  | not set   |
| 1218  | ValueExtended                      | Extended Value                     | 1          | c2         | false       | not set  | not set  | not set   |
| 1219  | ValueColleagueDiscount             | Colleague Discount                 | 1          | c2         | false       | not set  | not set  | 2         |
| 1220  | Price                              | Price                              | 1          | c2         | false       | 55       | 55       | 3         |
| 1221  | SalePrice                          | Sale Price                         | 1          | c2         | false       | not set  | not set  | 3         |
| 1222  | ExtendedPrice                      | Extended Price                     | 1          | c2         | false       | not set  | not set  | 3         |
| 1223  | PriceStart                         | Start Price                        | 1          | c2         | false       | 60       | 60       | 3         |
| 1224  | PriceEnd                           | End Price                          | 1          | c2         | false       | 60       | 60       | 3         |
| 1225  | PriceOrdered                       | Ordered Price                      | 1          | c2         | false       | not set  | not set  | 2         |
| 1226  | PriceReceived                      | Received Price                     | 1          | c2         | false       | not set  | not set  | 3         |
| 1227  | PricePrior                         | Prior Price                        | 1          | c2         | false       | 50       | 60       | 3         |
| 1228  | PriceLU                            | L/U Price                          | 1          | c2         | false       | 50       | 60       | not set   |
| 1229  | PriceKey                           | Key Price                          | 1          | c2         | false       | 50       | 60       | not set   |
| 1230  | PriceDifference                    | Price Diff                         | 1          | c2         | false       | 50       | 60       | not set   |
| 1231  | PriceDeal                          | Deal Price                         | 1          | c2         | false       | 50       | not set  | not set   |
| 1240  | ValueExVat1                        | Ex VAT Value 1                     | 1          | c2         | false       | not set  | not set  | not set   |
| 1243  | ValuePickup                        | Pickup                             | 1          | c2         | false       | not set  | not set  | not set   |
| 1244  | ValueFloat                         | Float                              | 1          | c2         | false       | not set  | not set  | not set   |
| 1245  | ValueSales                         | Sales                              | 1          | c2         | false       | not set  | not set  | not set   |
| 1246  | ValueVariance                      | Variance                           | 1          | c2         | false       | not set  | not set  | not set   |
| 1270  | Cost                               | Cost                               | 1          | c2         | false       | 60       | not set  | 3         |
| 1300  | VoucherPercent                     | Voucher Percent                    | 1          | n2         | false       | 60       | not set  | 3         |
| 1301  | PercSold                           | % Sold                             | 1          | n0         | false       | 60       | not set  | 3         |
| 1302  | PercDel                            | % Del                              | 1          | n0         | false       | 60       | not set  | 3         |
| 1303  | PercUnits                          | % Units                            | 1          | n2         | false       | 80       | 80       | 3         |
| 1304  | PercValue                          | % Value                            | 1          | n2         | false       | 80       | 80       | 3         |
| 1305  | PercentPerformance                 | % Performance                      | 1          | n2         | false       | not set  | not set  | not set   |
| 1306  | PercentScanning                    | % Scanning                         | 1          | n2         | false       | not set  | not set  | not set   |
| 1307  | PercentVariance                    | % Variance                         | 1          | n2         | false       | not set  | not set  | not set   |
| 1400  | Weight                             | Weight                             | 1          | n0         | false       | not set  | not set  | 3         |
| 1410  | Volume                             | Volume                             | 1          | n2         | false       | 60       | not set  | 3         |
| 1500  | NumTransactions                    | # Transactions                     | 1          | n0         | false       | not set  | not set  | not set   |
| 1501  | NumCorrections                     | # Corrections                      | 1          | n0         | false       | not set  | not set  | not set   |
| 1502  | NumVoids                           | # Voids                            | 1          | n0         | false       | not set  | not set  | not set   |
| 1503  | NumLinesReversed                   | # Lines Reversed                   | 1          | n0         | false       | not set  | not set  | not set   |
| 1504  | NumTotal                           | # Total                            | 1          | n0         | false       | not set  | not set  | not set   |
| 1505  | NumOpenDrawers                     | # Open Drawers                     | 1          | n0         | false       | not set  | not set  | not set   |
| 1506  | NumLinesSold                       | # Lines Sold                       | 1          | n0         | false       | not set  | not set  | not set   |
| 1507  | NumLinesScanned                    | # Lines Scanned                    | 1          | n0         | false       | not set  | not set  | not set   |
| 2000  | SELYesNo                           | SEL Correct                        | 0          | not set    | false       | not set  | not set  | 2         |
| 2001  | UpdatedProc                        | Updated Proc                       | 0          | not set    | false       | not set  | not set  | not set   |
| 2010  | IsNonStock                         | N/S                                | 0          | not set    | false       | not set  | 45       | not set   |
| 2011  | IsApplied                          | Applied                            | 0          | not set    | false       | 65       | 65       | not set   |
| 2012  | IsLabelled                         | Label OK                           | 0          | not set    | false       | 55       | 55       | not set   |
| 2013  | IsCounted                          | Counted                            | 0          | not set    | false       | not set  | 120      | not set   |
| 2014  | IsAdjusted                         | Adjusted                           | 0          | not set    | false       | not set  | 120      | not set   |
| 2015  | IsClosed                           | Is Closed                          | 0          | not set    | false       | 60       | 60       | not set   |
| 2016  | IsOpenMon                          | Is Open Mon                        | 0          | not set    | false       | not set  | not set  | not set   |
| 2017  | IsOpenTue                          | Is Open Tue                        | 0          | not set    | false       | not set  | not set  | not set   |
| 2018  | IsOpenWed                          | Is Open Wed                        | 0          | not set    | false       | not set  | not set  | not set   |
| 2019  | IsOpenThu                          | Is Open Thu                        | 0          | not set    | false       | not set  | not set  | not set   |
| 2020  | IsOpenFri                          | Is Open Fri                        | 0          | not set    | false       | not set  | not set  | not set   |
| 2021  | IsOpenSat                          | Is Open Sat                        | 0          | not set    | false       | not set  | not set  | not set   |
| 2022  | IsOpenSun                          | Is Open Sun                        | 0          | not set    | false       | not set  | not set  | not set   |
| 2023  | IsHeadOffice                       | Is Head Office                     | 0          | not set    | false       | 60       | 60       | not set   |
| 2030  | OverridePrice                      | New Day Price                      | 1          | c2         | false       | 75       | 85       | not set   |
| 2031  | DealPrice                          | Deal Price                         | 1          | c2         | false       | 75       | 85       | not set   |
| 2032  | SystemPrice                        | System Price                       | 1          | c2         | false       | 75       | 85       | not set   |
| 2033  | AuthName                           | Auth Name                          | 1          | c2         | false       | 75       | not set  | not set   |
| 2034  | DealGroupNo                        | Deal Grp No                        | 1          | c2         | false       | 75       | 85       | not set   |
| 2050  | IsActiveMon                        | Active Mon                         | 0          | not set    | false       | not set  | not set  | not set   |
| 2051  | IsActiveTue                        | Active Tue                         | 0          | not set    | false       | not set  | not set  | not set   |
| 2052  | IsActiveWed                        | Active Wed                         | 0          | not set    | false       | not set  | not set  | not set   |
| 2053  | IsActiveThu                        | Active Thu                         | 0          | not set    | false       | not set  | not set  | not set   |
| 2054  | IsActiveFri                        | Active Fri                         | 0          | not set    | false       | not set  | not set  | not set   |
| 2055  | IsActiveSat                        | Active Sat                         | 0          | not set    | false       | not set  | not set  | not set   |
| 2056  | IsActiveSun                        | Active Sun                         | 0          | not set    | false       | not set  | not set  | not set   |
| 2060  | IsConfirmed                        | Confirmed                          | 0          | not set    | false       | not set  | not set  | not set   |
| 2061  | IsReceived                         | Received                           | 0          | not set    | false       | not set  | 75       | not set   |
| 2063  | IsTraining                         | Training                           | 0          | not set    | false       | not set  | 75       | not set   |
| 2064  | IsDeleted                          | Del                                | 0          | not set    | false       | not set  | 45       | not set   |
| 2065  | IsImport                           | Import                             | 0          | not set    | false       | not set  | 65       | not set   |
| 2066  | IsObsolete                         | Obs                                | 0          | not set    | false       | not set  | 45       | not set   |
| 2067  | IsParked                           | Parked                             | 0          | not set    | false       | not set  | 60       | not set   |
| 2068  | IsSupervisorUsed                   | Sup Used                           | 0          | not set    | false       | not set  | 60       | not set   |
| 2069  | IsComplete                         | Completed                          | 0          | not set    | false       | 85       | 90       | not set   |
| 2070  | IsReversed                         | Reversed                           | 0          | not set    | false       | 70       | 90       | not set   |
| 2071  | IsScanned                          | Scanned                            | 0          | not set    | false       | not set  | 70       | not set   |
| 2072  | IsColleagueDiscount                | Colleague Disc Sale                | 0          | not set    | false       | not set  | not set  | 2         |
| 2100  | LabelShelfPrinted                  | Shelf Label Printed                | 0          | not set    | false       | not set  | 70       | not set   |
| 2101  | LabelPegPrinted                    | Peg Label To Be Printed            | 0          | not set    | false       | not set  | 90       | not set   |
| 2102  | LabelSmallPrinted                  | Small Label To Be Printed          | 0          | not set    | false       | not set  | 115      | not set   |
| 2103  | LabelMediumPrinted                 | Medium Label To Be Printed         | 0          | not set    | false       | not set  | 115      | not set   |
| 2500  | Date                               | Date                               | 2          | dd/MM/yyyy | false       | 65       | 75       | 2         |
| 2501  | DateStart                          | Start Date                         | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2502  | DateEnd                            | End Date                           | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2510  | DateLastSold                       | Last Sold                          | 2          | dd/MM/yyyy | false       | 65       | 65       | 2         |
| 2511  | DateLastReceived                   | Last Receipt                       | 2          | dd/MM/yyyy | false       | 65       | 65       | 2         |
| 2512  | DateNextReceipt                    | Next Receipt                       | 2          | dd/MM/yyyy | false       | 65       | 75       | not set   |
| 2513  | DateDue                            | Due                                | 2          | dd/MM/yyyy | false       | 65       | 75       | not set   |
| 2514  | DateEffective                      | Date Effective                     | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2515  | DateApplied                        | Date Applied                       | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2516  | DateAuto                           | Date Auto                          | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2517  | DateDeleted                        | Date Deleted                       | 2          | dd/MM/yyyy | false       | 65       | 75       | 2         |
| 2518  | DatePrior                          | Last P/C                           | 2          | dd/MM/yyyy | false       | not set  | 65       | 2         |
| 2520  | DateRequested                      | Req Date                           | 2          | dd/MM/yyyy | false       | 65       | 75       | not set   |
| 2521  | DateDelivered                      | Del Date                           | 2          | dd/MM/yyyy | false       | 65       | 75       | not set   |
| 2522  | DateCreated                        | Date Created                       | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2523  | DateCollect                        | Date Expected Collection           | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2524  | DateIssued                         | Issued                             | 2          | dd/MM/yyyy | false       | 65       | 75       | not set   |
| 2525  | DateExpiry                         | Expiry Date                        | 2          | dd/MM/yy   | false       | 65       | 75       | not set   |
| 2530  | WeekStart                          | Week Start                         | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2531  | WeekEnding                         | Week Ending                        | 2          | dd/MM/yyyy | false       | 65       | not set  | not set   |
| 2600  | Time                               | Time                               | 0          | not set    | false       | 60       | not set  | 2         |
| 2601  | TimeStart                          | Start Time                         | 0          | not set    | false       | not set  | not set  | not set   |
| 2602  | TimeEnd                            | End Time                           | 0          | not set    | false       | not set  | not set  | not set   |
| 3000  | Address1                           | Address 1                          | 0          | not set    | false       | 100      | not set  | not set   |
| 3001  | Address2                           | Address 2                          | 0          | not set    | false       | 100      | not set  | not set   |
| 3002  | Address3                           | Address 3                          | 0          | not set    | false       | 100      | not set  | not set   |
| 3003  | Address4                           | Address 4                          | 0          | not set    | false       | 100      | not set  | not set   |
| 3004  | Address5                           | Address 5                          | 0          | not set    | false       | 100      | not set  | not set   |
| 3051  | Code2                              | Code 2                             | 0          | not set    | false       | not set  | not set  | not set   |
| 3052  | Code53                             | Code 53                            | 0          | not set    | false       | not set  | not set  | not set   |
| 3053  | Variance                           | Var                                | 0          | not set    | false       | not set  | not set  | 2         |
| 3054  | VarianceMove                       | Var MD                             | 0          | not set    | false       | not set  | not set  | 2         |
| 3055  | VarianceStock                      | Var W/O                            | 0          | not set    | false       | not set  | not set  | 2         |
| 3056  | Keys                               | Keys                               | 0          | not set    | false       | not set  | not set  | not set   |
| 4000  | Today Qty                          | Today Qty                          | 1          | n0         | false       | 78       | 78       | 2         |
| 4001  | Today Value                        | Today Value                        | 1          | c2         | false       | 91       | 91       | 3         |
| 4002  | Total Qty                          | Total                              | 1          | n0         | false       | 66       | 66       | 2         |
| 4003  | Total Value                        | Total                              | 1          | c2         | false       | 83       | 83       | 3         |
| 4004  | Description                        | Description                        | 0          | not set    | false       | 128      | 128      | 1         |
| 4010  | No. Adj. Day                       | No. Adj. Day                       | 1          | n0         | false       | 90       | 90       | 2         |
| 4011  | No. Adj. WTD                       | WTD                                | 1          | n0         | false       | 70       | 70       | 2         |
| 4012  | Total                              | Total                              | 1          | c2         | false       | 62       | 62       | 3         |
| 4013  | WTD                                | WTD                                | 1          | c2         | false       | 64       | 64       | 3         |
| 4014  | Description                        | Description                        | 0          | not set    | false       | 120      | 120      | not set   |
| 4020  | Day                                | Day                                | 1          | c2         | false       | 60       | 60       | 3         |
| 4021  | WTD                                | WTD                                | 1          | c2         | false       | 60       | 60       | 3         |
| 4022  | Description                        | Description                        | 0          | not set    | false       | 151      | 151      | not set   |
| 4030  | Qty                                | Qty                                | 1          | n0         | false       | 86       | 86       | 2         |
| 4031  | Value                              | Value                              | 1          | c2         | false       | 96       | 96       | 3         |
| 4032  | Description                        | Description                        | 0          | not set    | false       | 224      | 224      | 1         |
| 4033  | Description                        | Description                        | 0          | not set    | false       | 123      | 123      | 1         |
| 4034  | Value                              | Value                              | 1          | c2         | false       | 61       | 61       | 3         |
| 4035  | ValueWtd                           | WTD                                | 1          | c2         | false       | 62       | 62       | 3         |
| 4040  | Today                              | Today                              | 1          | c2         | false       | 55       | 55       | 3         |
| 4050  | Today Qty                          | Today Qty                          | 1          | n0         | false       | 90       | 90       | 2         |
| 4051  | WTD Qty                            | WTD                                | 1          | n0         | false       | 70       | 70       | 2         |
| 4052  | Today Value                        | Today Value                        | 1          | c2         | false       | 62       | 62       | 3         |
| 4053  | WTD Value                          | WTD                                | 1          | c2         | false       | 64       | 64       | 3         |
| 4054  | Description                        | Description                        | 0          | not set    | false       | 120      | 120      | not set   |
| 5000  | PackSize                           | Pack Size                          | 0          |            | false       | 70       | 70       | 2         |
| 5001  | Status                             | Status                             | 0          | not set    | false       | 80       | not set  | not set   |
| 5002  | Initials                           | Initials                           | 0          | not set    | false       | 60       | 60       | not set   |
| 5003  | SaleType                           | Sale Type                          | 0          | not set    | false       | not set  | not set  | not set   |
| 5004  | Day                                | Day                                | 0          | not set    | false       | not set  | 100      | not set   |
| 5005  | Capture                            | Capture                            | 0          | not set    | false       | not set  | not set  | not set   |
| 5006  | DelCol                             | Del/Col                            | 0          | not set    | false       | not set  | not set  | not set   |
| 5007  | EventType                          | Event Type                         | 0          | not set    | false       | not set  | not set  | not set   |
| 5100  | HieCategory                        | Category                           | 0          | not set    | false       | not set  | not set  | not set   |
| 5110  | HieCategoryName                    | Category Name                      | 0          | not set    | false       | not set  | not set  | not set   |
| 5200  | StartTime                          | Start Time                         | 0          | not set    | false       | 50       | not set  | not set   |
| 5201  | Priority                           | Priority                           | 0          | not set    | false       | 60       | not set  | not set   |
| 5202  | Status                             | Status                             | 0          | not set    | false       | 60       | not set  | not set   |
| 5203  | From                               | From                               | 0          | not set    | false       | 60       | not set  | not set   |
| 5204  | To                                 | To                                 | 0          | not set    | false       | 60       | not set  | not set   |
| 5205  | EndTime                            | End Time                           | 0          | not set    | false       | not set  | not set  | not set   |
| 5206  | Mon                                |                                    | 0          | not set    | false       | 30       | 30       | not set   |
| 5207  | Tue                                |                                    | 0          | not set    | false       | 30       | 30       | not set   |
| 5208  | Wed                                |                                    | 0          | not set    | false       | 30       | 30       | not set   |
| 5209  | Thr                                |                                    | 0          | not set    | false       | 30       | 30       | not set   |
| 5210  | Fri                                |                                    | 0          | not set    | false       | 30       | 30       | not set   |
| 5211  | Sat                                |                                    | 0          | not set    | false       | 30       | 30       | not set   |
| 5212  | Sun                                |                                    | 0          | not set    | false       | 30       | 30       | not set   |
| 5300  | Event                              | Event #                            | 0          | not set    | false       | 40       | 60       | not set   |
| 5301  | Part Code                          | Part Code                          | 0          | not set    | false       | 60       | 60       | not set   |
| 5302  | Item #                             | Group #                            | 0          | not set    | false       | 40       | 60       | not set   |
| 5303  | Description                        | Description                        | 0          | not set    | false       | 100      | not set  | not set   |
| 5304  | Deal Price                         | Deal Price                         | 1          | n2         | false       | 60       | 60       | not set   |
| 5305  | DealType                           | Deal Type                          | 0          | not set    | false       | 100      | not set  | not set   |
| 5400  | Event                              | Group #                            | 0          | not set    | false       | 40       | 60       | not set   |
| 5401  | PartCode                           | Part Code                          | 0          | not set    | false       | 40       | 60       | not set   |
| 5402  | Description                        | Description                        | 0          | not set    | false       | 100      | not set  | not set   |
| 5403  | BuyQty                             | Buy Qty                            | 1          | n0         | false       | 40       | 60       | not set   |
| 5404  | DealPrice                          | Deal Price                         | 0          | not set    | false       | not set  | not set  | not set   |
| 6000  | VatRate1                           | VAT Rate 1                         | 1          | n2         | false       | not set  | not set  | not set   |
| 6003  | TKEY                               | Log                                | 0          | not set    | false       | not set  | not set  | 2         |
| 6004  | Quantity Received / IBT / Returned | Quantity Received / IBT / Returned | 1          |            | false       | not set  | not set  | 2         |
| 6005  | ReceiptType                        | Receipt Type                       | 1          |            | false       | not set  | not set  | 2         |
| 6010  | Card Report Date                   | Date                               | 2          | dd/mm/yy   | false       | not set  | not set  | 2         |
| 6011  | Card Report Time                   | Time                               | 0          | not set    | false       | not set  | not set  | 2         |
| 6012  | Card Report TillId                 | Till ID                            | 0          | not set    | false       | not set  | not set  | 2         |
| 6013  | Card Report CashierId              | Cashier ID                         | 0          | not set    | false       | not set  | not set  | 2         |
| 6014  | Card Report CashierName            | Cashier Name                       | 0          | not set    | false       | not set  | not set  | 2         |
| 6015  | Card Report Number                 | Card No                            | 0          | not set    | false       | not set  | not set  | 2         |
| 6016  | Card Report CustomerName           | Customer Name                      | 0          | not set    | false       | not set  | not set  | 2         |
| 6017  | Card Report ValueTransaction       | Value                              | 0          | not set    | false       | not set  | not set  | 2         |
| 6018  | Card Report ValueDiscount          | Value Discount                     | 0          | not set    | false       | not set  | not set  | 2         |
| 6020  | ReturnPolicyDescription            |                                    | 0          | not set    | false       | not set  | not set  | 1         |
| 6021  | ProductInformationDescription      |                                    | 0          | not set    | false       | not set  | not set  | 1         |
| 6024  | Description                        | Description                        | 0          | not set    | false       | 160      | 160      | 1         |
| 6025  | Qty                                | Trans Day                          | 1          | n0         | false       | 77       | 77       | 2         |
| 6026  | QtyWtd                             | WTD                                | 1          | n0         | false       | 68       | 68       | 2         |
| 6027  | Value                              | Day Value                          | 1          | c2         | false       | 77       | 77       | 3         |
| 6028  | ValueWtd                           | WTD                                | 1          | c2         | false       | 64       | 64       | 3         |
| 6029  | Qty                                | Qty                                | 1          | n0         | false       | 77       | 77       | 2         |
| 6030  | QtyWtd                             | WTD                                | 1          | n0         | false       | 81       | 81       | 2         |
| 6031  | Value                              | Value                              | 1          | c2         | false       | 64       | 64       | 3         |
| 6032  | ValueWtd                           | WTD                                | 1          | c2         | false       | 64       | 64       | 3         |
| 7001  | Priority                           | Priority                           | 0          | not set    | false       | 60       | not set  | not set   |
| 7101  | Number                             | Number                             | 0          | not set    | false       | 60       | 80       | not set   |
| 7201  | Active                             | Is Active                          | 0          | not set    | false       | 80       | 100      | not set   |
| 7301  | Description                        | Description                        | 0          | not set    | false       | 200      | not set  | not set   |
| 7302  | Description                        | Description                        | 0          | not set    | false       | 150      | not set  | not set   |
| 7401  | When Active                        | When Active                        | 0          | not set    | false       | 400      | not set  | not set   |
| 7501  | Deal Group Number                  | Deal Group Number                  | 0          | not set    | false       | not set  | not set  | not set   |
| 7601  | MMHS                               | MMHS                               | 0          | not set    | false       | not set  | not set  | not set   |
| 7602  | Hierarchy Group Number             | Hierarchy Group Number             | 0          | not set    | false       | 55       | 60       | not set   |
| 8001  | Deal Group Number                  | Deal Group Number                  | 0          | not set    | false       | 150      | 150      | not set   |
| 8201  | Deal Price                         | Deal Price                         | 1          | c2         | false       | 80       | 80       | not set   |
| 8301  | Category                           | Category                           | 0          | not set    | false       | 150      | not set  | not set   |
| 8401  | Spend Level                        | Spend Level                        | 1          | n2         | false       | 80       | 80       | not set   |
| 8501  | Discount                           | Discount                           | 0          | not set    | false       | 60       | 80       | 3         |
| 8601  | Buy Coupon                         | Buy Coupon                         | 0          | not set    | false       | 80       | not set  | not set   |
| 8701  | Get Coupon                         | Get Coupon                         | 0          | not set    | false       | 80       | not set  | not set   |
| 8801  | Buy Quantity                       | Quantity                           | 1          | n0         | false       | not set  | not set  | 3         |
| 8901  | Mix and Match Number               | Mix and Match Number               | 0          | not set    | false       | not set  | not set  | not set   |
| 9001  | Sku Number                         | Sku Number                         | 0          | not set    | false       | 45       | 45       | not set   |
| 9101  | Event Number                       | Event                              | 0          | not set    | false       | not set  | not set  | not set   |
| 9201  | Hierarchy Group Number             | Hierarchy                          | 0          | not set    | false       | not set  | not set  | not set   |
| 9301  | Buy Quantity                       | Buy Quantity                       | 1          | n0         | false       | not set  | not set  | 3         |
| 9401  | Get Quantity                       | Get Quantity                       | 1          | n0         | false       | not set  | not set  | 3         |
| 9501  | Type                               | Event Type                         | 0          | not set    | false       | 200      | not set  | not set   |
| 9601  | Total Buy Quantity Value           | Event Price (Each)                 | 1          | c2         | false       | not set  | not set  | 3         |
| 9701  | Price                              | Normal Retail Price                | 1          | c2         | false       | 100      | not set  | 3         |
| 9801  | Event Price                        | Event Price (Each)                 | 1          | c2         | false       | not set  | not set  | 3         |
| 9900  | Category                           |                                    | 0          | not set    | false       | not set  | not set  | not set   |
| 9901  | CategoryDescription                | ...                                | 0          | not set    | false       | 140      | 150      | 1         |
| 9902  | SealNumber                         | ...                                | 0          | not set    | false       | 180      | 200      | 1         |
| 9903  | Value                              | ...                                | 0          | not set    | false       | 140      | 150      | 1         |
| 9904  | Comment                            | Comment                            | 0          | not set    | false       | not set  | not set  | 1         |
| 9950  | Denomination                       | Denomination                       | 0          | not set    | false       | 120      | 100      | not set   |
| 9951  | Value                              | Banked                             | 0          | not set    | false       | 100      | 100      | not set   |
| 9952  | SlipNumber                         | Slip Number                        | 0          | not set    | false       | 80       | 100      | not set   |
| 9953  | SealNumber                         | Banking Bag Number                 | 0          | not set    | false       | 80       | 100      | not set   |
| 42501 | SKU                                | SKU                                | 0          | not set    | false       | 45       | 45       | 2         |
| 42502 | Description                        | Description                        | 0          | not set    | false       | 100      | not set  | not set   |
| 42503 | SOH                                | SOH                                | 1          | n0         | false       | 50       | 60       | 2         |
| 42504 | Impact Level                       | Impact Level                       | 1          | n0         | false       | 80       | 80       | 2         |
| 42505 | Description                        | Description                        | 0          | not set    | false       | 50       | 460      | 1         |
| 42506 | SelectedDate                       | SelectedDate                       | 0          | not set    | false       | not set  | not set  | not set   |
| 42550 | OriginalTillId                     | Original Till Id                   | 1          | n0         | false       | 80       | not set  | 2         |
| 42551 | OriginalTranNumber                 | Original Tran Number               | 1          |            | false       | 80       | not set  | 2         |
| 42552 | OriginalDate                       | Original Date                      | 2          | dd/MM/yyyy | false       | 130      | 160      | 2         |
| 42560 | TotalCountItems                    | Total Count Items                  | 0          | not set    | false       | 130      | 160      | not set   |
| 42561 | PhysicallyCountedItems             | Physically Counted Items           | 0          | not set    | false       | 130      | 160      | not set   |
| 42562 | AuthorisedBy                       | Authorised By                      | 0          | not set    | false       | 130      | 160      | not set   |
| 42570 | IsOffline                          | Is Offline                         | 0          | not set    | false       | not set  | 65       | not set   |
| 42571 | Void                               | Void                               | 0          | not set    | false       | not set  | 65       | not set   |
| 42553 | OriginalOVCTranNumber              | Original OVC Tran Number           | 1          | not set    | false       | 130      | 160      | 2         |
| 42600 | OVCTranNumber                      | OVC Tran Number                    | 1          | not set    | false       | 130      | 160      | 2         |
| 42610 | ReasonCode                         | Reason Code                        | 0          | not set    | false       | not set  | 65       | not set   |
| 42611 | SupervisorId                       | Sup Id                             | 0          | not set    | false       | not set  | 45       | not set   |
| 42612 | CashierId                          | Cashier ID                         | 0          | not set    | false       | not set  | 62       | not set   |
| 42613 | Description                        | Description                        | 0          | not set    | false       | 90       | 200      | not set   |
| 42614 | OVCTranNumber                      | OVC Tran Number                    | 0          | not set    | false       | 80       | not set  | not set   |
| 42615 | TranNumber                         | Tran Number                        | 0          | not set    | false       | not set  | 80       | not set   |
| 42616 | TillId                             | Till Id                            | 0          | not set    | false       | not set  | 50       | not set   |
| 42617 | LineNumber                         | Line                               | 0          | not set    | false       | not set  | 45       | not set   |
| 42618 | CashierName                        | Cashier Name                       | 0          | not set    | false       | 60       | not set  | not set   |
| 42619 | Competitor                         | Competitor                         | 0          | not set    | false       | 60       | 150      | not set   |
| 99    | Description                        | Description                        | 0          | not set    | false       | 500      | 600      | 1         |
| 229   | TranNumber                         | Transaction Number                 | 1          | not set    | false       | 100      | not set  | 2         |
And I expect the layout of "ReportColumns" table as documented:
| ReportId | TableId | ColumnId | Sequence | IsVisible | IsBold | IsHighlight | HighlightColour | Fontsize |
| 1        | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 1        | 1       | 6022     | 7        | true      | false  | not set     | not set         | not set  |
| 1        | 1       | 6023     | 8        | true      | false  | not set     | not set         | not set  |
| 1        | 1       | 6024     | 2        | true      | false  | not set     | not set         | not set  |
| 1        | 1       | 6025     | 3        | true      | false  | not set     | not set         | not set  |
| 1        | 1       | 6026     | 4        | true      | false  | not set     | not set         | not set  |
| 1        | 1       | 6027     | 5        | true      | false  | not set     | not set         | not set  |
| 1        | 1       | 6028     | 6        | true      | false  | not set     | not set         | not set  |
| 2        | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 2        | 1       | 103      | 2        | true      | false  | not set     | not set         | not set  |
| 2        | 1       | 1100     | 3        | true      | false  | not set     | not set         | not set  |
| 2        | 1       | 1101     | 4        | true      | false  | not set     | not set         | not set  |
| 2        | 1       | 1201     | 6        | true      | false  | not set     | not set         | not set  |
| 2        | 1       | 4040     | 4        | true      | false  | not set     | not set         | not set  |
| 3        | 1       | 103      | 2        | true      | false  | not set     | not set         | not set  |
| 3        | 1       | 1100     | 3        | true      | false  | not set     | not set         | not set  |
| 3        | 1       | 1101     | 4        | true      | false  | not set     | not set         | not set  |
| 3        | 1       | 1200     | 5        | true      | false  | not set     | not set         | not set  |
| 3        | 1       | 1201     | 6        | true      | false  | not set     | not set         | not set  |
| 4        | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 4        | 1       | 4010     | 2        | true      | false  | not set     | not set         | not set  |
| 4        | 1       | 4011     | 3        | true      | false  | not set     | not set         | not set  |
| 4        | 1       | 4012     | 4        | true      | false  | not set     | not set         | not set  |
| 4        | 1       | 4013     | 5        | true      | false  | not set     | not set         | not set  |
| 4        | 1       | 4014     | 1        | true      | false  | not set     | not set         | not set  |
| 4        | 1       | 42506    | 6        | false     | false  | not set     | not set         | not set  |
| 5        | 1       | 6024     | 2        | true      | false  | not set     | not set         | not set  |
| 5        | 1       | 6029     | 3        | true      | false  | not set     | not set         | not set  |
| 5        | 1       | 6030     | 4        | true      | false  | not set     | not set         | not set  |
| 5        | 1       | 6031     | 5        | true      | false  | not set     | not set         | not set  |
| 5        | 1       | 6032     | 6        | true      | false  | not set     | not set         | not set  |
| 6        | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 6        | 1       | 2500     | 7        | true      | false  | not set     | not set         | not set  |
| 6        | 1       | 4020     | 3        | true      | false  | not set     | not set         | not set  |
| 6        | 1       | 4021     | 4        | true      | false  | not set     | not set         | not set  |
| 6        | 1       | 4022     | 1        | true      | false  | not set     | not set         | not set  |
| 7        | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 7        | 1       | 4030     | 2        | true      | false  | not set     | not set         | 9        |
| 7        | 1       | 4031     | 3        | true      | false  | not set     | not set         | 9        |
| 7        | 1       | 4032     | 1        | true      | false  | not set     | not set         | not set  |
| 7        | 1       | 42506    | 6        | false     | false  | not set     | not set         | not set  |
| 8        | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 8        | 1       | 801      | 4        | true      | false  | not set     | not set         | not set  |
| 8        | 1       | 802      | 5        | true      | false  | not set     | not set         | not set  |
| 8        | 1       | 803      | 6        | true      | false  | not set     | not set         | not set  |
| 8        | 1       | 804      | 2        | true      | false  | not set     | not set         | not set  |
| 8        | 1       | 805      | 3        | true      | false  | not set     | not set         | not set  |
| 9        | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 9        | 1       | 1100     | 3        | true      | false  | not set     | not set         | not set  |
| 9        | 1       | 1101     | 4        | true      | false  | not set     | not set         | not set  |
| 9        | 1       | 4033     | 2        | true      | false  | not set     | not set         | not set  |
| 9        | 1       | 4034     | 5        | true      | false  | not set     | not set         | not set  |
| 9        | 1       | 4035     | 6        | true      | false  | not set     | not set         | not set  |
| 9        | 1       | 42506    | 6        | false     | false  | not set     | not set         | not set  |
| 10       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 10       | 1       | 4000     | 2        | true      | false  | true        | -65536          | not set  |
| 10       | 1       | 4001     | 4        | true      | false  | true        | -65536          | not set  |
| 10       | 1       | 4002     | 3        | true      | false  | true        | -65536          | not set  |
| 10       | 1       | 4003     | 5        | true      | false  | true        | -65536          | not set  |
| 10       | 1       | 4004     | 1        | true      | false  | true        | -65536          | not set  |
| 11       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 11       | 1       | 103      | 0        | true      | false  | not set     | not set         | not set  |
| 11       | 1       | 1100     | 0        | true      | false  | not set     | not set         | not set  |
| 11       | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 12       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 12       | 1       | 158      | 0        | true      | false  | not set     | not set         | not set  |
| 12       | 1       | 159      | 0        | true      | false  | not set     | not set         | not set  |
| 13       | 1       | 0        | 1        | false     | false  | not set     | not set         | not set  |
| 13       | 1       | 4050     | 3        | true      | false  | true        | -65536          | not set  |
| 13       | 1       | 4051     | 4        | true      | false  | true        | -65536          | not set  |
| 13       | 1       | 4052     | 5        | true      | false  | true        | -65536          | not set  |
| 13       | 1       | 4053     | 6        | true      | false  | true        | -65536          | not set  |
| 13       | 1       | 4054     | 2        | true      | false  | true        | -65536          | not set  |
| 50       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 50       | 1       | 101      | 0        | true      | false  | true        | -65536          | 11       |
| 50       | 1       | 102      | 0        | true      | false  | true        | -65536          | 11       |
| 50       | 1       | 202      | 0        | false     | false  | not set     | not set         | not set  |
| 51       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 51       | 1       | 101      | 0        | true      | false  | true        | -65536          | 11       |
| 51       | 1       | 102      | 0        | true      | false  | true        | -65536          | 11       |
| 51       | 1       | 202      | 0        | false     | false  | not set     | not set         | not set  |
| 51       | 1       | 211      | 0        | false     | false  | not set     | not set         | not set  |
| 52       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 52       | 1       | 101      | 0        | true      | false  | not set     | not set         | not set  |
| 52       | 1       | 102      | 0        | true      | false  | not set     | not set         | not set  |
| 52       | 1       | 151      | 0        | false     | false  | not set     | not set         | not set  |
| 52       | 1       | 202      | 0        | false     | false  | not set     | not set         | not set  |
| 52       | 1       | 230      | 0        | false     | false  | not set     | not set         | not set  |
| 52       | 1       | 2500     | 0        | false     | false  | not set     | not set         | not set  |
| 52       | 1       | 42600    | 0        | false     | false  | not set     | not set         | not set  |
| 53       | 1       | 222      | 0        | true      | false  | not set     | not set         | not set  |
| 53       | 1       | 1100     | 0        | true      | false  | not set     | not set         | not set  |
| 53       | 1       | 2513     | 0        | true      | false  | not set     | not set         | not set  |
| 54       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 54       | 1       | 101      | 0        | true      | false  | true        | -65536          | not set  |
| 54       | 1       | 102      | 0        | true      | false  | true        | -65536          | not set  |
| 54       | 1       | 202      | 0        | false     | false  | false       | not set         | not set  |
| 54       | 1       | 211      | 0        | false     | false  | false       | not set         | not set  |
| 54       | 1       | 2500     | 0        | true      | false  | true        | -65536          | not set  |
| 55       | 1       | 101      | 0        | true      | false  | not set     | not set         | not set  |
| 55       | 1       | 102      | 0        | true      | false  | not set     | not set         | not set  |
| 57       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 57       | 1       | 101      | 0        | true      | false  | not set     | not set         | not set  |
| 57       | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 57       | 1       | 1110     | 0        | true      | false  | not set     | not set         | not set  |
| 57       | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 57       | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 58       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 58       | 1       | 101      | 0        | true      | false  | not set     | not set         | not set  |
| 58       | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 58       | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 58       | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 59       | 1       | 106      | 0        | true      | false  | not set     | not set         | not set  |
| 59       | 1       | 152      | 0        | true      | false  | not set     | not set         | not set  |
| 59       | 1       | 221      | 0        | true      | false  | not set     | not set         | not set  |
| 59       | 1       | 301      | 0        | true      | false  | not set     | not set         | not set  |
| 59       | 1       | 1127     | 0        | true      | false  | not set     | not set         | not set  |
| 59       | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 59       | 1       | 6005     | 0        | true      | false  | not set     | not set         | not set  |
| 60       | 1       | 101      | 0        | true      | false  | not set     | not set         | not set  |
| 60       | 1       | 102      | 0        | true      | false  | not set     | not set         | not set  |
| 61       | 1       | 280      | 0        | false     | false  | not set     | not set         | not set  |
| 63       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 63       | 1       | 101      | 0        | true      | false  | not set     | not set         | not set  |
| 64       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 64       | 1       | 101      | 0        | true      | false  | not set     | not set         | not set  |
| 64       | 1       | 102      | 0        | true      | false  | not set     | not set         | not set  |
| 64       | 1       | 202      | 0        | false     | false  | not set     | not set         | not set  |
| 64       | 1       | 211      | 0        | false     | false  | not set     | not set         | not set  |
| 70       | 1       | 430      | 0        | true      | false  | not set     | not set         | not set  |
| 70       | 1       | 1129     | 0        | true      | false  | not set     | not set         | not set  |
| 70       | 1       | 1130     | 0        | true      | false  | not set     | not set         | not set  |
| 70       | 1       | 2530     | 0        | true      | false  | not set     | not set         | not set  |
| 70       | 1       | 2531     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 400      | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 1111     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 1135     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 1136     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 1152     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 1160     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 1161     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 2010     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 2013     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 2014     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 3051     | 0        | true      | false  | not set     | not set         | not set  |
| 71       | 1       | 3052     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 72       | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 105      | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 130      | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 152      | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 1150     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 1151     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 1160     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 1161     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 1162     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 1163     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 1223     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 1224     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 2600     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 3053     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 3054     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 3055     | 0        | true      | false  | not set     | not set         | not set  |
| 72       | 1       | 6003     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1105     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1112     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1113     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1114     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1115     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1116     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1131     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1153     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 1154     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 2530     | 0        | true      | false  | not set     | not set         | not set  |
| 73       | 1       | 2531     | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 105      | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 112      | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 130      | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 1119     | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 1150     | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 1151     | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 74       | 1       | 5002     | 0        | true      | false  | not set     | not set         | not set  |
| 75       | 1       | 6021     | 0        | true      | false  | not set     | not set         | not set  |
| 76       | 1       | 6020     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 152      | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 153      | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 2100     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 2101     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 2102     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 2103     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 2514     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 2515     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 2516     | 0        | true      | false  | not set     | not set         | not set  |
| 77       | 1       | 5001     | 0        | true      | false  | not set     | not set         | not set  |
| 79       | 1       | 222      | 0        | true      | false  | not set     | not set         | not set  |
| 79       | 1       | 1124     | 0        | true      | false  | not set     | not set         | not set  |
| 79       | 1       | 1127     | 0        | true      | false  | not set     | not set         | not set  |
| 79       | 1       | 2061     | 0        | true      | false  | not set     | not set         | not set  |
| 79       | 1       | 2513     | 0        | true      | false  | not set     | not set         | not set  |
| 79       | 1       | 5001     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 80       | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5200     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5201     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5202     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5203     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5204     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5205     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5206     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5207     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5208     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5209     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5210     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5211     | 0        | true      | false  | not set     | not set         | not set  |
| 80       | 1       | 5212     | 0        | true      | false  | not set     | not set         | not set  |
| 82       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 82       | 1       | 5400     | 0        | false     | false  | not set     | not set         | not set  |
| 82       | 1       | 5401     | 0        | true      | false  | not set     | not set         | not set  |
| 82       | 1       | 5402     | 0        | true      | false  | not set     | not set         | not set  |
| 82       | 1       | 5403     | 0        | true      | false  | not set     | not set         | not set  |
| 82       | 1       | 5404     | 0        | true      | false  | not set     | not set         | not set  |
| 83       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 83       | 1       | 5300     | 0        | true      | true   | not set     | not set         | not set  |
| 83       | 1       | 5301     | 0        | false     | false  | not set     | not set         | not set  |
| 83       | 1       | 5302     | 0        | true      | false  | not set     | not set         | not set  |
| 83       | 1       | 5303     | 0        | true      | false  | not set     | not set         | not set  |
| 83       | 1       | 5304     | 0        | true      | false  | not set     | not set         | not set  |
| 83       | 1       | 5305     | 0        | true      | false  | not set     | not set         | not set  |
| 84       | 1       | 3056     | 0        | true      | false  | not set     | not set         | not set  |
| 85       | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 1       | 7001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 1       | 7101     | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 1       | 7201     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 1       | 7301     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 1       | 7401     | 5        | true      | false  | not set     | not set         | not set  |
| 85       | 1       | 7501     | 7        | false     | false  | not set     | not set         | not set  |
| 85       | 1       | 7601     | 8        | false     | false  | not set     | not set         | not set  |
| 85       | 1       | 9001     | 9        | false     | false  | not set     | not set         | not set  |
| 85       | 1       | 9501     | 6        | true      | false  | not set     | not set         | not set  |
| 85       | 2       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 2       | 7001     | 4        | false     | false  | not set     | not set         | not set  |
| 85       | 2       | 7302     | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 2       | 8001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 2       | 8201     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 2       | 9101     | 5        | false     | false  | not set     | not set         | not set  |
| 85       | 3       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 3       | 7001     | 7        | false     | false  | not set     | not set         | not set  |
| 85       | 3       | 7602     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 3       | 8301     | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 3       | 8401     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 3       | 8501     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 3       | 8601     | 5        | true      | false  | not set     | not set         | not set  |
| 85       | 3       | 8701     | 6        | true      | false  | not set     | not set         | not set  |
| 85       | 3       | 9101     | 8        | false     | false  | not set     | not set         | not set  |
| 85       | 4       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 4       | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 4       | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 4       | 9701     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 5       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 5       | 7302     | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 5       | 8001     | 5        | false     | false  | not set     | not set         | not set  |
| 85       | 5       | 8201     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 5       | 8801     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 5       | 8901     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 5       | 9101     | 6        | false     | false  | not set     | not set         | not set  |
| 85       | 6       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 6       | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 6       | 8001     | 5        | false     | false  | not set     | not set         | not set  |
| 85       | 6       | 8901     | 4        | false     | false  | not set     | not set         | not set  |
| 85       | 6       | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 6       | 9101     | 6        | false     | false  | not set     | not set         | not set  |
| 85       | 6       | 9701     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 7       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 7       | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 7       | 8001     | 5        | false     | false  | not set     | not set         | not set  |
| 85       | 7       | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 7       | 9101     | 6        | false     | false  | not set     | not set         | not set  |
| 85       | 7       | 9301     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 7       | 9701     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 8       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 8       | 101      | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 8       | 9001     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 8       | 9101     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 8       | 9201     | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 9       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 9       | 7001     | 6        | false     | false  | not set     | not set         | not set  |
| 85       | 9       | 7301     | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 9       | 8501     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 9       | 8901     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 9       | 9101     | 7        | false     | false  | not set     | not set         | not set  |
| 85       | 9       | 9301     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 9       | 9401     | 5        | true      | false  | not set     | not set         | not set  |
| 85       | 10      | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 10      | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 10      | 8901     | 4        | false     | false  | not set     | not set         | not set  |
| 85       | 10      | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 10      | 9701     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 11      | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 11      | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 11      | 7001     | 7        | false     | false  | not set     | not set         | not set  |
| 85       | 11      | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 11      | 9101     | 6        | false     | false  | not set     | not set         | not set  |
| 85       | 11      | 9301     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 11      | 9401     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 11      | 9701     | 5        | true      | false  | not set     | not set         | not set  |
| 85       | 12      | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 12      | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 12      | 7001     | 6        | false     | false  | not set     | not set         | not set  |
| 85       | 12      | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 12      | 9101     | 5        | false     | false  | not set     | not set         | not set  |
| 85       | 12      | 9301     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 12      | 9601     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 13      | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 13      | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 13      | 8901     | 4        | false     | false  | not set     | not set         | not set  |
| 85       | 13      | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 13      | 9701     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 14      | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 14      | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 14      | 7001     | 7        | false     | false  | not set     | not set         | not set  |
| 85       | 14      | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 14      | 9101     | 6        | false     | false  | not set     | not set         | not set  |
| 85       | 14      | 9301     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 14      | 9601     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 14      | 9701     | 5        | true      | false  | not set     | not set         | not set  |
| 85       | 15      | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 15      | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 15      | 7001     | 5        | false     | false  | not set     | not set         | not set  |
| 85       | 15      | 8201     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 15      | 8901     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 15      | 9101     | 4        | false     | false  | not set     | not set         | not set  |
| 85       | 16      | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 16      | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 16      | 8901     | 5        | false     | false  | not set     | not set         | not set  |
| 85       | 16      | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 16      | 9701     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 16      | 9801     | 3        | true      | false  | not set     | not set         | not set  |
| 85       | 17      | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 85       | 17      | 101      | 2        | true      | false  | not set     | not set         | not set  |
| 85       | 17      | 7001     | 6        | false     | false  | not set     | not set         | not set  |
| 85       | 17      | 9001     | 1        | true      | false  | not set     | not set         | not set  |
| 85       | 17      | 9101     | 5        | false     | false  | not set     | not set         | not set  |
| 85       | 17      | 9701     | 4        | true      | false  | not set     | not set         | not set  |
| 85       | 17      | 9801     | 3        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 303      | 0        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 1121     | 0        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 1208     | 0        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 2510     | 0        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 2511     | 0        | true      | false  | not set     | not set         | not set  |
| 100      | 1       | 2512     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 400      | 0        | false     | false  | not set     | not set         | not set  |
| 200      | 1       | 1102     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1105     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1111     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1122     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1135     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1136     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1138     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 1210     | 0        | false     | false  | not set     | not set         | not set  |
| 200      | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 2010     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 2011     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 1       | 2012     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 401      | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1000     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1102     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1105     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1111     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1122     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1135     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1136     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1138     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1210     | 0        | false     | false  | not set     | not set         | not set  |
| 200      | 2       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1303     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 2       | 1304     | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 3       | 80       | 0        | true      | false  | not set     | not set         | not set  |
| 200      | 3       | 81       | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 135      | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 210      | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 1000     | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 1010     | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 1020     | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 1122     | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 1123     | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 2000     | 0        | true      | false  | not set     | not set         | not set  |
| 201      | 1       | 4032     | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 105      | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 1117     | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 1118     | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 1122     | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 2510     | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 2511     | 0        | true      | false  | not set     | not set         | not set  |
| 203      | 1       | 5001     | 0        | true      | false  | not set     | not set         | not set  |
| 204      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 204      | 1       | 105      | 0        | true      | false  | not set     | not set         | not set  |
| 204      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 204      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 204      | 1       | 1133     | 0        | true      | false  | not set     | not set         | not set  |
| 204      | 1       | 2511     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 103      | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 105      | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 1211     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 1212     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 1227     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 2010     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 2064     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 2066     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 2510     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 2511     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 2517     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 2518     | 0        | true      | false  | not set     | not set         | not set  |
| 205      | 1       | 5100     | 0        | false     | false  | not set     | not set         | not set  |
| 205      | 1       | 5110     | 0        | true      | false  | not set     | not set         | not set  |
| 206      | 1       | 101      | 0        | true      | false  | not set     | not set         | not set  |
| 206      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 206      | 1       | 1122     | 0        | true      | false  | not set     | not set         | not set  |
| 206      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 206      | 1       | 1207     | 0        | true      | false  | not set     | not set         | not set  |
| 206      | 1       | 5110     | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 106      | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 210      | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 222      | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 223      | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 224      | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 225      | 0        | false     | false  | not set     | not set         | not set  |
| 210      | 1       | 303      | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 1124     | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 1137     | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 2061     | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 2065     | 0        | true      | false  | not set     | not set         | not set  |
| 210      | 1       | 2524     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 203      | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 1102     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 1105     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 1111     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 1135     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 1136     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 2000     | 0        | true      | false  | not set     | not set         | not set  |
| 220      | 1       | 5000     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 203      | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 303      | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 1102     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 1105     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 1111     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 1135     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 1136     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 2000     | 0        | true      | false  | not set     | not set         | not set  |
| 221      | 1       | 5000     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 203      | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 1102     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 1105     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 1111     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 1135     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 1136     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 2000     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 5000     | 0        | true      | false  | not set     | not set         | not set  |
| 222      | 1       | 5110     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 203      | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 236      | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 1102     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 1105     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 1111     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 1135     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 1136     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 2000     | 0        | true      | false  | not set     | not set         | not set  |
| 223      | 1       | 5000     | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 151      | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 152      | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 230      | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 231      | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 301      | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 1202     | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 1203     | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 1300     | 0        | true      | false  | not set     | not set         | not set  |
| 300      | 1       | 42600    | 0        | true      | false  | not set     | not set         | not set  |
| 301      | 1       | 1100     | 0        | true      | false  | not set     | not set         | not set  |
| 301      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 301      | 1       | 42505    | 0        | true      | false  | not set     | not set         | not set  |
| 301      | 2       | 1100     | 0        | true      | false  | not set     | not set         | not set  |
| 301      | 2       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 301      | 2       | 42505    | 0        | true      | false  | not set     | not set         | not set  |
| 302      | 1       | 132      | 0        | true      | false  | not set     | not set         | not set  |
| 302      | 1       | 235      | 0        | true      | false  | not set     | not set         | not set  |
| 302      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 302      | 1       | 2525     | 0        | true      | false  | not set     | not set         | not set  |
| 302      | 1       | 5003     | 0        | true      | false  | not set     | not set         | not set  |
| 302      | 1       | 5005     | 0        | true      | false  | not set     | not set         | not set  |
| 303      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 303      | 1       | 105      | 0        | true      | false  | not set     | not set         | not set  |
| 303      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 303      | 1       | 1120     | 0        | true      | false  | not set     | not set         | not set  |
| 303      | 1       | 1132     | 0        | true      | false  | not set     | not set         | not set  |
| 303      | 1       | 2511     | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 238      | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 306      | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 1100     | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 1228     | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 1229     | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 1230     | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42610    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42611    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42612    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42613    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42614    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42615    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42616    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42617    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42618    | 0        | true      | false  | not set     | not set         | not set  |
| 304      | 1       | 42619    | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 151      | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 154      | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 305      | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 320      | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 321      | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 1213     | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 1214     | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 305      | 1       | 2600     | 0        | true      | false  | not set     | not set         | not set  |
| 306      | 1       | 151      | 0        | true      | false  | not set     | not set         | not set  |
| 306      | 1       | 230      | 0        | true      | false  | not set     | not set         | not set  |
| 306      | 1       | 239      | 0        | true      | false  | not set     | not set         | not set  |
| 306      | 1       | 431      | 0        | true      | false  | not set     | not set         | not set  |
| 306      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 306      | 1       | 42600    | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6010     | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6011     | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6012     | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6013     | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6014     | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6015     | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6016     | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6017     | 0        | true      | false  | not set     | not set         | not set  |
| 307      | 1       | 6018     | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 132      | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 154      | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 305      | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 2030     | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 2032     | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 2033     | 0        | true      | false  | not set     | not set         | not set  |
| 308      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 130      | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 132      | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 150      | 0        | false     | false  | not set     | not set         | not set  |
| 309      | 1       | 154      | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 305      | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 2031     | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 2033     | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 2334     | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 309      | 2       | 101      | 4        | true      | false  | not set     | not set         | not set  |
| 309      | 2       | 150      | 0        | false     | false  | not set     | not set         | not set  |
| 309      | 2       | 201      | 1        | true      | false  | not set     | not set         | not set  |
| 309      | 2       | 805      | 3        | true      | false  | not set     | not set         | not set  |
| 309      | 2       | 2032     | 2        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 106      | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 151      | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 154      | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 156      | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 225      | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 230      | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 240      | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 305      | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 320      | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 1214     | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 1215     | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 1216     | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 1240     | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 2001     | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 2063     | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 2067     | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 2068     | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 2069     | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 2600     | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 6000     | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 42570    | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 1       | 42571    | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 1       | 42600    | 0        | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 100      | 5        | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 131      | 11       | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 151      | 2        | false     | false  | not set     | not set         | not set  |
| 310      | 2       | 155      | 10       | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 201      | 4        | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 230      | 3        | false     | false  | not set     | not set         | not set  |
| 310      | 2       | 1100     | 6        | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 1217     | 13       | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 1218     | 8        | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 1220     | 7        | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 2070     | 12       | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 2071     | 9        | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 2500     | 1        | false     | false  | not set     | not set         | not set  |
| 310      | 2       | 5003     | 14       | true      | false  | not set     | not set         | not set  |
| 310      | 2       | 42600    | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 3       | 101      | 9        | true      | false  | not set     | not set         | not set  |
| 310      | 3       | 106      | 5        | true      | false  | not set     | not set         | not set  |
| 310      | 3       | 107      | 10       | true      | false  | not set     | not set         | not set  |
| 310      | 3       | 112      | 4        | true      | false  | not set     | not set         | not set  |
| 310      | 3       | 151      | 2        | false     | false  | not set     | not set         | not set  |
| 310      | 3       | 155      | 8        | true      | false  | not set     | not set         | not set  |
| 310      | 3       | 230      | 3        | false     | false  | not set     | not set         | not set  |
| 310      | 3       | 231      | 11       | true      | false  | not set     | not set         | not set  |
| 310      | 3       | 235      | 7        | true      | false  | not set     | not set         | not set  |
| 310      | 3       | 1200     | 6        | true      | false  | not set     | not set         | not set  |
| 310      | 3       | 2500     | 1        | false     | false  | not set     | not set         | not set  |
| 310      | 3       | 42600    | 0        | false     | false  | not set     | not set         | not set  |
| 310      | 4       | 151      | 2        | false     | false  | not set     | not set         | not set  |
| 310      | 4       | 230      | 3        | false     | false  | not set     | not set         | not set  |
| 310      | 4       | 2500     | 1        | false     | false  | not set     | not set         | not set  |
| 310      | 4       | 42550    | 5        | true      | false  | not set     | not set         | not set  |
| 310      | 4       | 42551    | 6        | true      | false  | not set     | not set         | not set  |
| 310      | 4       | 42552    | 4        | true      | false  | not set     | not set         | not set  |
| 310      | 4       | 42553    | 7        | true      | false  | not set     | not set         | not set  |
| 310      | 4       | 42600    | 0        | false     | false  | not set     | not set         | not set  |
| 320      | 1       | 151      | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 154      | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 220      | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 230      | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 305      | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 1214     | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 1219     | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 2072     | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 320      | 1       | 2600     | 0        | true      | false  | not set     | not set         | not set  |
| 330      | 1       | 151      | 2        | true      | false  | false       | not set         | not set  |
| 330      | 1       | 154      | 5        | true      | false  | false       | not set         | not set  |
| 330      | 1       | 229      | 3        | true      | false  | false       | not set         | not set  |
| 330      | 1       | 2500     | 1        | true      | false  | false       | not set         | not set  |
| 330      | 1       | 42600    | 4        | true      | false  | false       | not set         | not set  |
| 330      | 2       | 99       | 6        | true      | false  | false       | not set         | not set  |
| 330      | 2       | 151      | 1        | false     | false  | false       | not set         | not set  |
| 330      | 2       | 201      | 5        | true      | false  | false       | not set         | not set  |
| 330      | 2       | 229      | 2        | false     | false  | false       | not set         | not set  |
| 330      | 2       | 1100     | 7        | true      | false  | false       | not set         | not set  |
| 330      | 2       | 2500     | 3        | false     | false  | false       | not set         | not set  |
| 330      | 2       | 42600    | 4        | false     | false  | false       | not set         | not set  |
| 400      | 1       | 140      | 0        | true      | false  | not set     | not set         | not set  |
| 400      | 1       | 220      | 0        | true      | false  | not set     | not set         | not set  |
| 400      | 1       | 250      | 0        | true      | false  | not set     | not set         | not set  |
| 400      | 1       | 304      | 0        | true      | false  | not set     | not set         | not set  |
| 400      | 1       | 1134     | 0        | true      | false  | not set     | not set         | not set  |
| 400      | 1       | 1400     | 0        | true      | false  | not set     | not set         | not set  |
| 400      | 1       | 1410     | 0        | true      | false  | not set     | not set         | not set  |
| 400      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 400      | 1       | 5004     | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 140      | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 220      | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 234      | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 304      | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 310      | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 1209     | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 1400     | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 1410     | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 401      | 1       | 5004     | 0        | true      | false  | not set     | not set         | not set  |
| 402      | 1       | 1102     | 0        | true      | false  | not set     | not set         | not set  |
| 402      | 1       | 1204     | 0        | true      | false  | not set     | not set         | not set  |
| 402      | 1       | 1400     | 0        | true      | false  | not set     | not set         | not set  |
| 402      | 1       | 1410     | 0        | true      | false  | not set     | not set         | not set  |
| 402      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 402      | 1       | 5004     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 140      | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 220      | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 234      | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 304      | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 310      | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 1124     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 1125     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 1126     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 1209     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 1400     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 1410     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 2520     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 2521     | 0        | true      | false  | not set     | not set         | not set  |
| 403      | 1       | 5006     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 140      | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 220      | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 234      | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 304      | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 310      | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 1124     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 1125     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 1126     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 1209     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 1400     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 1410     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 2520     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 2521     | 0        | true      | false  | not set     | not set         | not set  |
| 404      | 1       | 5006     | 0        | true      | false  | not set     | not set         | not set  |
| 405      | 1       | 1020     | 0        | true      | false  | not set     | not set         | not set  |
| 405      | 1       | 1021     | 0        | true      | false  | not set     | not set         | not set  |
| 405      | 1       | 1022     | 0        | true      | false  | not set     | not set         | not set  |
| 405      | 1       | 1301     | 0        | true      | false  | not set     | not set         | not set  |
| 405      | 1       | 1302     | 0        | true      | false  | not set     | not set         | not set  |
| 405      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 405      | 1       | 5004     | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 210      | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 222      | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 303      | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 1124     | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 1127     | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 1128     | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 1207     | 0        | true      | false  | not set     | not set         | not set  |
| 406      | 1       | 1220     | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 1       | 10       | 0        | true      | true   | not set     | not set         | not set  |
| 420      | 1       | 20       | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 1       | 30       | 0        | true      | true   | not set     | not set         | not set  |
| 420      | 1       | 40       | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 1       | 50       | 0        | true      | true   | not set     | not set         | not set  |
| 420      | 1       | 1100     | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 100      | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 112      | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 131      | 0        | false     | false  | not set     | not set         | not set  |
| 420      | 2       | 135      | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 201      | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 1115     | 0        | false     | false  | not set     | not set         | not set  |
| 420      | 2       | 1124     | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 1126     | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 1127     | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 1225     | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 5000     | 0        | true      | false  | not set     | not set         | not set  |
| 420      | 2       | 6004     | 0        | true      | false  | not set     | not set         | not set  |
| 425      | 1       | 42501    | 0        | true      | false  | not set     | not set         | not set  |
| 425      | 1       | 42502    | 0        | true      | false  | not set     | not set         | not set  |
| 425      | 1       | 42503    | 0        | true      | false  | not set     | not set         | not set  |
| 425      | 1       | 42504    | 0        | true      | false  | not set     | not set         | not set  |
| 500      | 1       | 200      | 0        | true      | false  | not set     | not set         | not set  |
| 500      | 1       | 210      | 0        | true      | false  | not set     | not set         | not set  |
| 500      | 1       | 221      | 0        | true      | false  | not set     | not set         | not set  |
| 500      | 1       | 303      | 0        | true      | false  | not set     | not set         | not set  |
| 500      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 500      | 1       | 2522     | 0        | true      | false  | not set     | not set         | not set  |
| 500      | 1       | 2523     | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 133      | 0        | false     | false  | not set     | not set         | not set  |
| 501      | 1       | 134      | 0        | false     | false  | not set     | not set         | not set  |
| 501      | 1       | 140      | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 150      | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 250      | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 251      | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 300      | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 302      | 0        | false     | false  | not set     | not set         | not set  |
| 501      | 1       | 2015     | 0        | false     | false  | not set     | not set         | not set  |
| 501      | 1       | 2023     | 0        | false     | false  | not set     | not set         | not set  |
| 501      | 1       | 3000     | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 3001     | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 3002     | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 3003     | 0        | true      | false  | not set     | not set         | not set  |
| 501      | 1       | 3004     | 0        | false     | false  | not set     | not set         | not set  |
| 610      | 1       | 106      | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 136      | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 151      | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 154      | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 230      | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 240      | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 241      | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 305      | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 1200     | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 2500     | 0        | true      | false  | not set     | not set         | not set  |
| 610      | 1       | 42600    | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 154      | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 305      | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1243     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1244     | 0        | false     | false  | not set     | not set         | not set  |
| 611      | 1       | 1245     | 0        | false     | false  | not set     | not set         | not set  |
| 611      | 1       | 1246     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1305     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1306     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1307     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1500     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1501     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1502     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1503     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1504     | 0        | false     | false  | not set     | not set         | not set  |
| 611      | 1       | 1505     | 0        | true      | false  | not set     | not set         | not set  |
| 611      | 1       | 1506     | 0        | false     | false  | not set     | not set         | not set  |
| 611      | 1       | 1507     | 0        | false     | false  | not set     | not set         | not set  |
| 612      | 1       | 0        | 0        | false     | false  | not set     | not set         | not set  |
| 612      | 1       | 9901     | 0        | true      | true   | not set     | not set         | 24       |
| 612      | 1       | 9902     | 0        | true      | false  | not set     | not set         | not set  |
| 612      | 1       | 9903     | 0        | true      | false  | not set     | not set         | not set  |
| 612      | 1       | 9904     | 0        | true      | false  | not set     | not set         | not set  |
| 612      | 1       | 9905     | 0        | true      | false  | not set     | not set         | not set  |
| 612      | 1       | 9906     | 0        | true      | false  | not set     | not set         | not set  |
| 613      | 1       | 9950     | 0        | true      | false  | not set     | not set         | not set  |
| 613      | 1       | 9951     | 0        | true      | false  | not set     | not set         | not set  |
| 613      | 1       | 9952     | 0        | true      | false  | not set     | not set         | not set  |
| 613      | 1       | 9953     | 0        | true      | false  | not set     | not set         | not set  |
| 620      | 1       | 2014     | 5        | true      | false  | not set     | not set         | not set  |
| 620      | 1       | 2500     | 1        | true      | false  | not set     | not set         | not set  |
| 620      | 1       | 42560    | 2        | true      | false  | not set     | not set         | not set  |
| 620      | 1       | 42561    | 3        | true      | false  | not set     | not set         | not set  |
| 620      | 1       | 42562    | 4        | true      | false  | not set     | not set         | not set  |
And I expect the layout of "ReportHyperlink" table as documented:
| ReportId | TableId | RowId | ColumnName  | Value                                         |
| 1        | 1       | 2     | Description | MenuId=3330                                   |
| 1        | 1       | 8     | Description | IncorrectEANReport.exe                        |
| 2        | 1       | 2     | Description | MenuId=7030                                   |
| 2        | 1       | 3     | Description | MenuId=4030                                   |
| 4        | 1       | 1     | Description | MenuId=7030                                   |
| 4        | 1       | 2     | Description | MenuId=7030                                   |
| 4        | 1       | 3     | Description | MenuId=7030                                   |
| 4        | 1       | 4     | Description | MenuId=7030                                   |
| 4        | 1       | 5     | Description | ReportId=304\|[SelectedDate]                  |
| 4        | 1       | 6     | Description | ReportId=308\|[SelectedDate]\|[SelectedDate]  |
| 4        | 1       | 7     | Description | ReportId=309\|[SelectedDate]\|[SelectedDate]  |
| 7        | 1       | 1     | Description | ReportId=203\|[SelectedDate]                  |
| 7        | 1       | 2     | Description | ReportId=100                                  |
| 7        | 1       | 3     | Description | ReportId=206                                  |
| 7        | 1       | 4     | Description | ReportId=205                                  |
| 7        | 1       | 5     | Description | ReportId=205                                  |
| 8        | 1       | 4     | Description | VB Price Change Report                        |
| 8        | 1       | 5     | Description | VB Price Change Confirmation Report           |
| 8        | 1       | 6     | Description | VB Label Request Audit                        |
| 9        | 1       | 1     | Description | ReportId=305\|[SelectedDate]                  |
| 9        | 1       | 2     | Description | ReportId=321\|[SelectedDate]\|[SelectedDate]  |
| 9        | 1       | 3     | Description | ReportId=306                                  |
| 9        | 1       | 4     | Description | ReportId=306                                  |
| 9        | 1       | 5     | Description | ReportId=610\|[SelectedDate]\|[SelectedDate]  |
| 9        | 1       | 6     | Description | ReportId=610\|[SelectedDate]\|[SelectedDate]  |
| 10       | 1       | 1     | Description | saleordermaintain.exe                         |
| 10       | 1       | 2     | Description | saleordermaintain.exe                         |
| 10       | 1       | 3     | Description | saleordermaintain.exe                         |
| 10       | 1       | 4     | Description | saleordermaintain.exe                         |
| 10       | 1       | 5     | Description | saleordermaintain.exe                         |
| 10       | 1       | 6     | Description | saleordermaintain.exe                         |
| 11       | 1       | 1     | Description | MenuId=2035\|3 Awaiting Store Confirmation    |
| 11       | 1       | 2     | Description | MenuId=2035\|4 Awaiting Picking               |
| 11       | 1       | 3     | Description | MenuId=2035\|5 Awaiting Despatch              |
| 11       | 1       | 4     | Description | MenuId=2035\|6 Awaiting Delivery Confirmation |
| 11       | 1       | 5     | Description | MenuId=2035\|7 Failed \ Re-Delivery Orders    |
| 11       | 1       | 6     | Description | MenuId=2035\|8 Completed Orders               |
| 50       | 1       | 1     | Description | ReportId=77\|[SkuNumber]                      |
| 50       | 1       | 3     | Description | ReportId=85\|[SkuNumber]                      |
| 50       | 1       | 4     | Description | ReportId=60\|[SkuNumber]                      |
| 51       | 1       | 1     | Description | ReportId=75\|[SkuNumber]                      |
| 51       | 1       | 2     | Description | ReportId=76\|[SupplierNumber]                 |
| 51       | 1       | 3     | Description | ReportId=79\|[SkuNumber]                      |
| 51       | 1       | 5     | Description | ReportId=59\|[SkuNumber]                      |
| 52       | 1       | 1     | Description | ReportId=70\|[SkuNumber]                      |
| 52       | 1       | 2     | Description | ReportId=81\|[Date]\|[TillId]\|[TranNumber]   |
| 54       | 1       | 5     | Description | ReportId=55\|[SkuNumber]                      |
| 54       | 1       | 6     | Description | ReportId=78\|[SupplierNumber]                 |
| 54       | 1       | 7     | Description | ReportId=78\|[SupplierNumber]                 |
| 57       | 1       | 1     | SkuNumber   | StockDetail\|[SkuNumber]                      |
| 58       | 1       | 1     | SkuNumber   | StockDetail\|[SkuNumber]                      |
| 64       | 1       | 1     | Description | ReportId=72\|[SkuNumber]\|[GroupIds]          |
| 64       | 1       | 2     | Description | ReportId=71\|[SkuNumber]                      |
| 64       | 1       | 3     | Description | ReportId=74\|[SkuNumber]                      |
| 64       | 1       | 4     | Description | ReportId=73\|[SkuNumber]                      |
| 72       | 1       | 1     | TKEY        | ReportId=84\|[TKEY]                           |
| 80       | 1       | 1     | Event       | ReportId=83\|[Event]\|[SkuNumber]             |
| 83       | 1       | 1     | Event       | ReportId=82\|[Event]                          |
| 425      | 1       | 1     | SKU         | MenuId=1010\|[SkuNumber]                      |
And I expect the layout of "ReportParameters" table as documented:
| ReportId | ParameterId | Sequence | AllowMultiple | DefaultValue |
| 1        | 103         | 1        | false         | not set      |
| 2        | 103         | 1        | false         | not set      |
| 3        | 103         | 1        | false         | not set      |
| 4        | 103         | 1        | false         | not set      |
| 5        | 103         | 1        | false         | not set      |
| 7        | 103         | 1        | false         | not set      |
| 8        | 103         | 1        | false         | not set      |
| 9        | 103         | 1        | false         | not set      |
| 10       | 103         | 1        | false         | not set      |
| 11       | 103         | 1        | false         | not set      |
| 12       | 103         | 1        | false         | not set      |
| 50       | 200         | 1        | false         | not set      |
| 51       | 200         | 1        | false         | not set      |
| 52       | 200         | 1        | false         | not set      |
| 53       | 200         | 1        | false         | not set      |
| 54       | 200         | 1        | false         | not set      |
| 55       | 200         | 1        | false         | not set      |
| 57       | 200         | 1        | false         | not set      |
| 58       | 200         | 1        | false         | not set      |
| 59       | 200         | 1        | false         | not set      |
| 60       | 200         | 1        | false         | not set      |
| 61       | 200         | 1        | false         | not set      |
| 63       | 200         | 1        | false         | not set      |
| 64       | 200         | 1        | false         | not set      |
| 70       | 200         | 1        | false         | not set      |
| 71       | 200         | 1        | false         | not set      |
| 72       | 200         | 1        | false         | not set      |
| 72       | 201         | 2        | true          | 1,2,3,4,5    |
| 73       | 200         | 1        | false         | not set      |
| 74       | 200         | 1        | false         | not set      |
| 75       | 200         | 1        | false         | not set      |
| 76       | 4           | 1        | false         | not set      |
| 77       | 200         | 1        | false         | not set      |
| 78       | 4           | 1        | false         | not set      |
| 79       | 200         | 1        | false         | not set      |
| 80       | 200         | 1        | false         | not set      |
| 81       | 101         | 1        | false         | not set      |
| 81       | 120         | 2        | false         | not set      |
| 81       | 121         | 3        | false         | not set      |
| 82       | 10          | 1        | false         | not set      |
| 83       | 10          | 1        | false         | not set      |
| 83       | 200         | 2        | false         | not set      |
| 84       | 11          | 1        | false         | not set      |
| 85       | 200         | 1        | false         | not set      |
| 100      | 1           | 1        | false         | not set      |
| 100      | 2           | 2        | false         | not set      |
| 200      | 100         | 1        | false         | not set      |
| 200      | 300         | 2        | false         | 1            |
| 201      | 50          | 1        | false         | 5            |
| 203      | 101         | 1        | false         | not set      |
| 205      | 205         | 1        | false         | A            |
| 210      | 101         | 1        | false         | not set      |
| 220      | 101         | 1        | false         | not set      |
| 221      | 4           | 1        | false         | not set      |
| 222      | 6           | 1        | false         | not set      |
| 223      | 51          | 1        | false         | 1            |
| 300      | 101         | 1        | false         | not set      |
| 301      | 101         | 1        | false         | not set      |
| 302      | 101         | 1        | false         | not set      |
| 304      | 101         | 1        | false         | not set      |
| 305      | 101         | 1        | false         | not set      |
| 307      | 101         | 1        | false         | not set      |
| 308      | 102         | 1        | false         | not set      |
| 308      | 103         | 2        | false         | not set      |
| 309      | 102         | 1        | false         | not set      |
| 309      | 103         | 2        | false         | not set      |
| 310      | 101         | 1        | false         | not set      |
| 310      | 120         | 2        | false         | not set      |
| 310      | 121         | 3        | false         | not set      |
| 310      | 200         | 4        | false         | not set      |
| 310      | 351         | 6        | false         | not set      |
| 310      | 352         | 5        | false         | not set      |
| 310      | 353         | 3        | false         | not set      |
| 320      | 102         | 1        | false         | not set      |
| 320      | 103         | 2        | false         | not set      |
| 321      | 102         | 1        | false         | not set      |
| 321      | 103         | 2        | false         | not set      |
| 400      | 102         | 1        | false         | not set      |
| 400      | 103         | 2        | false         | not set      |
| 401      | 102         | 1        | false         | not set      |
| 401      | 103         | 2        | false         | not set      |
| 402      | 102         | 1        | false         | not set      |
| 402      | 103         | 2        | false         | not set      |
| 403      | 102         | 1        | false         | not set      |
| 403      | 103         | 2        | false         | not set      |
| 404      | 102         | 1        | false         | not set      |
| 404      | 103         | 2        | false         | not set      |
| 405      | 102         | 1        | false         | not set      |
| 405      | 103         | 2        | false         | not set      |
| 406      | 3           | 3        | false         | not set      |
| 406      | 102         | 1        | false         | not set      |
| 406      | 103         | 2        | false         | not set      |
| 420      | 21          | 1        | false         | not set      |
| 500      | 4           | 1        | false         | not set      |
| 610      | 102         | 1        | false         | not set      |
| 610      | 103         | 2        | false         | not set      |
| 611      | 102         | 1        | false         | not set      |
| 611      | 103         | 2        | false         | not set      |
| 612      | 350         | 1        | false         | not set      |
| 613      | 350         | 1        | false         | not set      |
| 330      | 102         | 1        | false         | not set      |
| 330      | 103         | 2        | false         | not set      |
| 330      | 119         | 3        | false         | not set      |
| 330      | 122         | 4        | false         | not set      |
| 330      | 200         | 6        | false         | not set      |
| 330      | 353         | 5        | false         | not set      |
And I expect the layout of "ReportTable" table as documented:
| ReportId | Id | Name                                  | AllowGrouping | AllowSelection | ShowHeaders | ShowIndicator | ShowLinesVertical | ShowLinesHorizontal | ShowBorder | ShowInPrintOnly | AutoFitColumns | HeaderHeight | RowHeight | AnchorTop | AnchorBottom |
| 1        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 2        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 3        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 4        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 5        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 6        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 7        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 8        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 9        | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 10       | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 11       | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 12       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 13       | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 50       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 51       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 52       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 53       | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 54       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 55       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 57       | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 58       | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 59       | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 60       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 61       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | -1        | true      | true         |
| 63       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 64       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 70       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 71       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 72       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 73       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 74       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 75       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 76       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 77       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 50           | not set   | true      | true         |
| 78       | 1  | Master                                | false         | false          | false       | false         | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 79       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 80       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 81       | 1  | Header                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 81       | 2  | Lines                                 | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 82       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 83       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 84       | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 1  | Events                                | true          | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 85       | 2  | Deal Groups                           | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 3  | Spend Level Savings                   | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 4  | Spend Level Saving Sku Details        | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 5  | Deal Group Mix And Match Events       | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 6  | Deal Group Mix And Match Sku Details  | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 7  | Deal Group Sku Event                  | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 8  | Spend Level Saving Exclusions         | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 9  | Multi-buy Mix And Match Events        | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 10 | Multi-buy Mix And Match SKU Details   | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 11 | Multi-buy Sku Event                   | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 12 | Mix And Match Bulk Saving Events      | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 13 | Mix And Match Bulk Saving SKU Details | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 14 | Sku Bulk Saving Event                 | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 15 | TLP Mix And Match Events              | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 16 | TLP Mix And Match Sku Details         | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 85       | 17 | TLP Sku Event                         | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 100      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 200      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 200      | 2  | Summary                               | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | false     | true         |
| 200      | 3  | Signature                             | false         | false          | true        | true          | true              | true                | true       | true            | true           | not set      | 60        | false     | true         |
| 200      | 4  | SigFooter                             | false         | false          | false       | true          | true              | true                | true       | true            | true           | not set      | not set   | false     | true         |
| 201      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 203      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 204      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 205      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 206      | 1  | Master                                | true          | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 210      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 220      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 221      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 222      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 223      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 300      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 301      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | false        |
| 301      | 2  | Vision                                | false         | false          | false       | true          | true              | true                | true       | false           | true           | not set      | not set   | false     | false        |
| 302      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 303      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 304      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 305      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 306      | 1  | Master                                | true          | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 307      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 308      | 1  | Master                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 309      | 1  | Header                                | false         | false          | true        | false         | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 309      | 2  | Lines                                 | false         | false          | true        | false         | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 310      | 1  | Header                                | true          | false          | true        | true          | true              | true                | true       | false           | false          | 35           | not set   | true      | true         |
| 310      | 2  | Lines                                 | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 310      | 3  | Tenders                               | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 310      | 4  | Original Receipt Details              | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 320      | 1  | Master                                | true          | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 321      | 1  | Master                                | true          | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 400      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 401      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 402      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 403      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 404      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 405      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 406      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 420      | 1  | Header                                | false         | false          | false       | false         | false             | false               | true       | false           | true           | not set      | not set   | true      | false        |
| 420      | 2  | Details                               | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 425      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 500      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 501      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 600      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 610      | 1  | Master                                | true          | false          | true        | true          | true              | true                | true       | false           | true           | not set      | not set   | true      | true         |
| 611      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 612      | 1  | Master                                | false         | false          | true        | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 613      | 1  | Master                                | false         | false          | false       | true          | true              | true                | true       | false           | true           | 35           | not set   | true      | true         |
| 620      | 1  | PIC Count History                     | false         | false          | true        | true          | true              | true                | true       | false           | false          | not set      | not set   | true      | true         |
| 330      | 1  | Header                                | true          | false          | true        | true          | true              | true                | true       | false           | false          | 35           | not set   | true      | true         |
| 330      | 2  | Lines with explosive SKUs             | false         | false          | true        | true          | true              | true                | true       | false           | false          | not set      | not set   | true      | true         |
And I expect the layout of "ReportGrouping" table as documented:
| ReportId | TableId | ColumnId | Sequence | IsDescending | SummaryType | ShowExpanded |
| 200      | 1       | 400      | 1        | false        | 3           | true         |
| 203      | 1       | 21       | 1        | false        | 6           | true         |
| 204      | 1       | 21       | 1        | false        | 6           | true         |
| 205      | 1       | 5110     | 1        | false        | 3           | true         |
| 206      | 1       | 5110     | 1        | false        | 6           | true         |
| 220      | 1       | 2500     | 1        | false        | 6           | true         |
| 221      | 1       | 303      | 1        | false        | 6           | true         |
| 222      | 1       | 5110     | 1        | false        | 6           | true         |
| 223      | 1       | 236      | 1        | false        | 6           | true         |
| 300      | 1       | 2001     | 1        | false        | 6           | true         |
| 303      | 1       | 21       | 1        | false        | 6           | true         |
| 306      | 1       | 431      | 1        | false        | 6           | true         |
| 610      | 1       | 106      | 2        | false        | 6           | false        |
| 610      | 1       | 154      | 1        | false        | 6           | false        |
| 1000     | 1       | 3000     | 1        | false        | 3           | false        |
And I expect the layout of "ReportRelation" table as documented:
| ReportId | Name                                  | ParentTable                      | ParentColumns                                       | ChildTable                            | ChildColumns                                        |
| 81       | LineItems                             | Header                           | Date,TillId,TranNumber                              | Lines                                 | Date,TillId,TranNumber                              |
| 85       | Deal Group Mix And Match Events       | Deal Groups                      | Deal Group Number,Event Number                      | Deal Group Mix And Match Events       | Deal Group Number,Event Number                      |
| 85       | Deal Group Mix And Match Sku Details  | Deal Group Mix And Match Events  | Mix and Match Number,Deal Group Number,Event Number | Deal Group Mix And Match Sku Details  | Mix and Match Number,Deal Group Number,Event Number |
| 85       | Deal Group Sku Event                  | Deal Groups                      | Deal Group Number,Event Number                      | Deal Group Sku Event                  | Deal Group Number,Event Number                      |
| 85       | Deal Groups                           | Events                           | Number,Priority                                     | Deal Groups                           | Event Number,Priority                               |
| 85       | Mix And Match Bulk Saving Events      | Events                           | Number,Priority,MMHS                                | Mix And Match Bulk Saving Events      | Event Number,Priority,Mix and Match Number          |
| 85       | Mix And Match Bulk Saving Sku Details | Mix And Match Bulk Saving Events | Mix and Match Number                                | Mix And Match Bulk Saving Sku Details | Mix and Match Number                                |
| 85       | Multi-buy Mix And Match Events        | Events                           | Number,Priority,MMHS                                | Multi-buy Mix And Match Events        | Event Number,Priority,Mix and Match Number          |
| 85       | Multi-buy Mix And Match Sku Details   | Multi-buy Mix And Match Events   | Mix and Match Number                                | Multi-buy Mix And Match Sku Details   | Mix and Match Number                                |
| 85       | Multi-buy Sku Event                   | Events                           | Number,Priority                                     | Multi-buy Sku Event                   | Event Number,Priority                               |
| 85       | Sku Bulk Saving Event                 | Events                           | Number,Priority                                     | Sku Bulk Saving Event                 | Event Number,Priority                               |
| 85       | Spend Level Saving Exclusions         | Spend Level Savings              | Event Number,Hierarchy Group Number                 | Spend Level Saving Exclusions         | Event Number,Hierarchy Group Number                 |
| 85       | Spend Level Saving Sku Details        | Spend Level Savings              | Hierarchy Group Number,Event Number,Priority        | Spend Level Saving Sku Details        | HS Lookup,Event Number,Priority                     |
| 85       | Spend Level Savings                   | Events                           | Number,Priority,MMHS                                | Spend Level Savings                   | Event Number,Priority,Hierarchy Group Number        |
| 85       | TLP Mix And Match Events              | Events                           | Number,Priority,MMHS                                | TLP Mix And Match Events              | Event Number,Priority,Mix and Match Number          |
| 85       | TLP Mix And Match Sku Details         | TLP Mix And Match Events         | Mix and Match Number                                | TLP Mix And Match Sku Details         | Mix and Match Number                                |
| 85       | TLP Sku Event                         | Events                           | Number,Priority                                     | TLP Sku Event                         | Event Number,Priority                               |
| 309      | Lines                                 | Header                           | Id                                                  | Lines                                 | Id                                                  |
| 310      | Lines                                 | Header                           | Date,TillId,TranNumber                              | Lines                                 | Date,TillId,TranNumber                              |
| 310      | Original Receipt Details              | Header                           | Date,TillId,TranNumber                              | Original Receipt Details              | Date,TillId,TranNumber                              |
| 310      | Tenders                               | Header                           | Date,TillId,TranNumber                              | Tenders                               | Date,TillId,TranNumber                              |
| 330      | Lines with explosive SKUs             | Header                           | Date,TillId,TranNumber                              | Lines with explosive SKUs             | Date,TillId,TranNumber                              |
And I expect the layout of "ReportSummary" table as documented:
| ReportId | TableId | ColumnId | SummaryType | ApplyToGroups | Format         | DenominatorId | NumeratorId |
| 53       | 1       | 3        | 0           |false            | Total   {0:n0} | not set       | not set     |
| 100      | 1       | 303      | 3           |false            | Count   {0:n0} | not set       | not set     |
| 200      | 1       | 1102     | 0           |true            | {0:n0}         | not set       | not set     |
| 200      | 1       | 1105     | 0           |true            | {0:n0}         | not set       | not set     |
| 200      | 1       | 1111     | 0           |true            | {0:n0}         | not set       | not set     |
| 200      | 1       | 1120     | 0           |true            | {0:n0}         | not set       | not set     |
| 200      | 1       | 1122     | 0           |true            | {0:n0}         | not set       | not set     |
| 200      | 1       | 1135     | 0           |true            | {0:n0}         | not set       | not set     |
| 200      | 1       | 1136     | 0           |true            | {0:n0}         | not set       | not set     |
| 200      | 1       | 1138     | 0           |true            | {0:n0}         | not set       | not set     |
| 200      | 1       | 1200     | 0           |true            | {0:c2}         | not set       | not set     |
| 203      | 1       | 100      | 3           |true            | Count   {0:n0} | not set       | not set     |
| 205      | 1       | 1211     | 0           |true            | {0:c2}         | not set       | not set     |
| 205      | 1       | 1212     | 0           |true            | {0:c2}         | not set       | not set     |
| 205      | 1       | 2010     | 0           |true            | {0:n0}         | not set       | not set     |
| 205      | 1       | 2064     | 0           |true            | {0:n0}         | not set       | not set     |
| 206      | 1       | 1122     | 0           |true            | {0:n0}         | not set       | not set     |
| 206      | 1       | 1200     | 0           |true            | {0:c2}         | not set       | not set     |
| 300      | 1       | 1202     | 0           |true            | Total   {0:c2} | not set       | not set     |
| 300      | 1       | 1203     | 0           |true            | Total   {0:c2} | not set       | not set     |
| 302      | 1       | 1200     | 0           |false            | Total {0:c2}   | not set       | not set     |
| 500      | 1       | 1200     | 0           |false            | Total {0:c2}   | not set       | not set     |
| 610      | 1       | 1200     | 0           |true            | {0:c2}         | not set       | not set     |
| 611      | 1       | 1243     | 0           |false            | {0:c2}         | not set       | not set     |
| 611      | 1       | 1246     | 0           |false            | {0:c2}         | not set       | not set     |
| 611      | 1       | 1305     | 5           |false            | {0:n2}         | 1504          | 1500        |
| 611      | 1       | 1306     | 5           |false            | {0:n2}         | 1506          | 1507        |
| 611      | 1       | 1307     | 5           |false            | {0:n2}         | 1243          | 1246        |
| 611      | 1       | 1500     | 0           |false            | {0:n0}         | not set       | not set     |
| 611      | 1       | 1501     | 0           |false            | {0:n0}         | not set       | not set     |
| 611      | 1       | 1502     | 0           |false            | {0:n0}         | not set       | not set     |
| 611      | 1       | 1503     | 0           |false            | {0:n0}         | not set       | not set     |
| 611      | 1       | 1504     | 0           |false            | {0:n0}         | not set       | not set     |
| 611      | 1       | 1505     | 0           |false            | {0:n0}         | not set       | not set     |
| 611      | 1       | 1506     | 0           |false            | {0:n0}         | not set       | not set     |
| 611      | 1       | 1507     | 0           |false            | {0:n0}         | not set       | not set     |