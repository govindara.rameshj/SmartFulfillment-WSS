using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Utility;
using WSS.AAT.Common.Utility.Bindings;
using Cts.Oasys.Core.SystemEnvironment;
using NUnit.Framework;
using System.IO;
using System;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace WSS.AAT.Scenarios.OasysDbSetup
{
    [Binding]
    public class RollbackScriptsSteps : BaseStepDefinitions
    {
        private readonly string pathToScripts;
        private string databaseVersion;
        private bool result;
        private readonly ISystemEnvironment systemEnvironment;

        public RollbackScriptsSteps(ISystemEnvironment systemEnvironment)
        {
            this.systemEnvironment = systemEnvironment;
            pathToScripts = System.Configuration.ConfigurationManager.AppSettings["pathToScripts"];
        }

        [Given(@"that we have rollback scripts")]
        public void GivenThatWeHaveRollbackScripts()
        {
            PropertiesFileHelper propertiesFile = new PropertiesFileHelper();
            databaseVersion = propertiesFile.GetDbVersion().ToString(3);
        }

        [When(@"I run ""(.*)"" scripts")]
        public void WhenIRunScripts(string scriptType)
        {
            ReleaseScriptsRunner runner = new ReleaseScriptsRunner(systemEnvironment);
            result = runner.runReleaseScriptsInLocation(pathToScripts, databaseVersion, scriptType);
        }

        [Then(@"I don't get errors")]
        public void ThenIDonTGetErrors()
        {
            Assert.That(result, Is.True);
        }

        [Then(@"the number of rollback scripts should be equal to rollout scripts")]
        public void ThenTheNumberOfRollbackScriptsShouldBeEqualToRolloutScripts()
        {
            var rollbackScripts = ListScriptNamesFromFolder("\\RollBack\\");
            var rolloutScripts = ListScriptNamesFromFolder("\\RollOut\\");

            var rollbackMissing = rollbackScripts
                .Select(filename => Regex.Replace(filename, @"(_\w{7,8}(.sql)$)", "_RollOut.sql"))
                .Except(rolloutScripts).ToList();
            var rolloutMissing = rolloutScripts
                .Select(filename => Regex.Replace(filename, @"(_\w{7,8}(.sql)$)", "_RollBack.sql"))
                .Except(rollbackScripts).ToList();
            if (rollbackMissing.Any() || rolloutMissing.Any())
            {
                StringBuilder errorMsg = new StringBuilder("Number of Rollback and Rollout scripts does not match!\nCan't find the following RollOut scripts: \n");
                foreach (string name in rollbackMissing)
                {
                    errorMsg.AppendLine(name);
                }
                errorMsg.AppendLine("Can't find the following RollBack scripts: ");
                foreach (string name in rolloutMissing)
                {
                    errorMsg.AppendLine(name);
                }
                Assert.Fail(errorMsg.ToString());
            }
        }

        private List<String> ListScriptNamesFromFolder(string folder)
        {
            List<String> scriptList = Directory
                                          .EnumerateFiles(String.Concat(pathToScripts, databaseVersion, folder), "*")
                                          .Select(f => Path.GetFileName(f))
                                          .Select(filename => Regex.Replace(filename, @"^\d{4}_", ""))
                                          .ToList();
            return scriptList;
        }
    }
}
