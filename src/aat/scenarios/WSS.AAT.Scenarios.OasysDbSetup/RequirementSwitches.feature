@OasysDB
Feature: RequirementSwitches

Scenario: Parameters test
  Given that I am checking database configuration
  Then I expect the layout of "Parameters" table as documented:
  | Id       | Description                                        | StringValue | LongValue | BooleanValue | DecimalValue | ValueType |
  | -2201901 | Remedial work for P022-019                         | not set     | not set   | true         | not set      | 3         |
  | -2201201 | Enable Banking Scanner PO22-012                    | not set     | not set   | false        | not set      | 3         |
  | -2201001 | P0022-010-01 QOD Extra Sub Totals                  | not set     | not set   | true         | not set      | 3         |
  | -22040   | Enable requirement PO22-040                        | not set     | not set   | true         | not set      | 3         |
  | -22035   | User Story P022-035 - IBT Store names              | not set     | not set   | true         | not set      | 3         |
  | -22030   | Enable Requirement P022-030                        | not set     | not set   | true         | not set      | 3         |
  | -22025   | P0022-025 F6 Banking Bag Collections               | not set     | not set   | true         | not set      | 3         |
  | -22023   | P022-023 If manager allow same auth code           | not set     | not set   | true         | not set      | 3         |
  | -22022   | P0022-022 Set Collection Complete after Despatch   | not set     | not set   | true         | not set      | 3         |
  | -22019   | Remedial work for P022-019                         | not set     | not set   | true         | not set      | 3         |
  | -22017   | Enable requirement P022-017                        | not set     | not set   | true         | not set      | 3         |
  | -22016   | Enable requirement PO22-016                        | not set     | not set   | true         | not set      | 3         |
  | -22013   | P0013-022 Launch Audit Roll Enq from Pickup Entry  | not set     | not set   | true         | not set      | 3         |
  | -22012   | Enable requirement PO22-012                        | not set     | not set   | true         | not set      | 3         |
  | -22011   | Enable requirement PO22-011                        | not set     | not set   | true         | not set      | 3         |
  | -22010   | P0022-010 QOD Previous Days Statistics             | not set     | not set   | true         | not set      | 3         |
  | -22008   | Enable requirement P022-008                        | not set     | not set   | true         | not set      | 3         |
  | -22006   | Enable requirement P022-006                        | not set     | not set   | true         | not set      | 3         |
  | -22005   | P0022-05 QOD Maintain Stock Enquiry Btn            | not set     | not set   | true         | not set      | 3         |
  | -22004   | P0022-04 QOD Maintain Col Order                    | not set     | not set   | true         | not set      | 3         |
  | -22002   | Enable Project 22-002                              | not set     | not set   | true         | not set      | 3         |
  | -22001   | Enable requirement PO22-001                        | not set     | not set   | true         | not set      | 3         |
  | -2733    | Enable Coupons ProcessTransmissions                | not set     | not set   | true         | not set      | 3         |
  | -2290    | Enable requirement CR0054-02                       | not set     | not set   | true         | not set      | 3         |
  | -2289    | Enable requirement CR0054-01                       | not set     | not set   | true         | not set      | 3         |
  | -1086    | Enable mis-spelt Stock Item searching              | not set     | not set   | true         | not set      | 3         |
  | -1046    | Referral 1046: Recall Quote                        | not set     | not set   | true         | not set      | 3         |
  | -1034    | Enable requirement RF1034                          | not set     | not set   | true         | not set      | 3         |
  | -1031    | Enable requirement RF1031                          | not set     | not set   | true         | not set      | 3         |
  | -1029    | Enable requirement RF1029                          | not set     | not set   | true         | not set      | 3         |
  | -1000    | Period End Dates Install                           | not set     | not set   | true         | not set      | 3         |
  | -983     | Enable requirement RF0983                          | not set     | not set   | true         | not set      | 3         |
  | -964     | Enable requirement PO15                            | not set     | not set   | true         | not set      | 3         |
  | -963     | RF0963 Refund Holding DRL issue                    | not set     | not set   | true         | not set      | 3         |
  | -961     | Enable Requirement RF961                           | not set     | not set   | true         | not set      | 3         |
  | -951     | Enable requirement RF0951                          | not set     | not set   | true         | not set      | 3         |
  | -946     | Enable requirement RF0946                          | not set     | not set   | true         | not set      | 3         |
  | -922     | Enable Requirement RF922                           | not set     | not set   | true         | not set      | 3         |
  | -904     | RF0904 Hubs QOD Refund Quantities                  | not set     | not set   | true         | not set      | 3         |
  | -901     | Enable requirement RF0901                          | not set     | not set   | true         | not set      | 3         |
  | -892     | Enable requirement RF0892                          | not set     | not set   | true         | not set      | 3         |
  | -845     | Enable requirement RF0845                          | not set     | not set   | true         | not set      | 3         |
  | -844     | Enable requirement RF0844                          | not set     | not set   | true         | not set      | 3         |
  | -824     | Referral 824 - Refresh Returns Header              | not set     | not set   | true         | not set      | 3         |
  | -597     | Enable requirement RF0597                          | not set     | not set   | true         | not set      | 3         |
  | -121     | CR0121 - Negative Stock Issues                     | not set     | not set   | true         | not set      | 3         |
  | -118     | CR0118 Pick Instructions Switch                    | not set     | not set   | true         | not set      | 3         |
  | -109     | CR0109 Delivery Instructions Switch                | not set     | not set   | true         | not set      | 3         |
  | -105     | Hot Service Logo                                   |             | not set   | true         | not set      | 3         |
  | -104     | Enable requirement CR0104                          | not set     | not set   | true         | not set      | 3         |
  | -101     | Disable CR0101 Harrow Back Office Test Environment | not set     | not set   | false        | not set      | 3         |
  | -96      | Enable change request CR0096                       | not set     | not set   | true         | not set      | 3         |
  | -87      | Enable change request 87                           | not set     | not set   | true         | not set      | 3         |
  | -61      | Enable Requirement RF61                            | not set     | 0         | true         | not set      | 3         |
  | -15      | Enable requirement PO15                            | not set     | not set   | true         | not set      | 3         |
  | -8       | CR008 Price Match promise                          | not set     | not set   | true         | not set      | 3         |
  | -4       | Auto Price Change: Disable Label Printed Check     | not set     | not set   | false        | not set      | 3         |
  | 0        | Release Version                                    | <version>   | not set   | false        | not set      | 0         |
  | 980016   | Enable Change request 16                           | not set     | not set   | true         | not set      | 3         |
  | 980064   | Enable Change Request CR0064                       | not set     | not set   | true         | not set      | 3         |
  | 980081   | Enable Change request 81                           | not set     | not set   | true         | not set      | 3         |
  | 980082   | Enable Change Request CR0082                       | not set     | not set   | true         | not set      | 3         |
  | 980097   | Enable CR0097                                      | not set     | not set   | true         | not set      | 3         |
  | 980834   | Enable requirement RF0834                          | not set     | not set   | true         | not set      | 3         |
  | 980870   | Enable Referral 870 Fix                            | not set     | not set   | true         | not set      | 3         |
  | 980876   | Enable Requirement RF0876                          | not set     | not set   | true         | not set      | 3         |
  | 980944   | Enable requirement RF0944                          | not set     | not set   | true         | not set      | 3         |
  | 980949   | Enable Referral 949 Fix                            | not set     | not set   | true         | not set      | 3         |
  | 980979   | Enable Referral 979 fix                            | not set     | not set   | true         | not set      | 3         |
  | 980981   | Enable Referral 981 fix                            | not set     | not set   | true         | not set      | 3         |
  | 980986   | Enable requirement RF0986                          | not set     | not set   | true         | not set      | 3         |
  | 981093   | Enable modal card authorisation                    | not set     | not set   | true         | not set      | 3         |
  | 981094   | Enable requirement RF1094                          | not set     | not set   | true         | not set      | 3         |
  | 981404   | Enable requirement PO14-04                         | not set     | not set   | true         | not set      | 3         |
  | 983000   | Enable requirement SC300e                          | not set     | not set   | true         | not set      | 3         |
  | 983351   | Enable User Story 3351 (Retrieve Coupon)           | not set     | not set   | true         | not set      | 3         |
  | 983352   | Enable requirement PO14-02                         | not set     | not set   | true         | not set      | 3         |
  | 983375   | Enable User Story 3375 (Global Event)              | not set     | not set   | true         | not set      | 3         |
  | 984151   | Enable requirement US4151                          | not set     | not set   | true         | not set      | 3         |
  | 990099   | Exclude price changes from HOSTU processing        | not set     | not set   | false        | not set      | 3         |
  | 9801403  | Enable requirement PO14-03                         | not set     | not set   | true         | not set      | 3         |
