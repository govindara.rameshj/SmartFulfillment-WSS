﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.18444
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace WSS.AAT.Scenarios.OasysDbSetup
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("RequirementSwitches")]
    [NUnit.Framework.CategoryAttribute("OasysDB")]
    public partial class RequirementSwitchesFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "RequirementSwitches.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "RequirementSwitches", "", ProgrammingLanguage.CSharp, new string[] {
                        "OasysDB"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Parameters test")]
        public virtual void ParametersTest()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Parameters test", ((string[])(null)));
#line 4
this.ScenarioSetup(scenarioInfo);
#line 5
  testRunner.Given("that I am checking database configuration", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Description",
                        "StringValue",
                        "LongValue",
                        "BooleanValue",
                        "DecimalValue",
                        "ValueType"});
            table1.AddRow(new string[] {
                        "-2201901",
                        "Remedial work for P022-019",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-2201201",
                        "Enable Banking Scanner PO22-012",
                        "not set",
                        "not set",
                        "false",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-2201001",
                        "P0022-010-01 QOD Extra Sub Totals",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22040",
                        "Enable requirement PO22-040",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22035",
                        "User Story P022-035 - IBT Store names",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22030",
                        "Enable Requirement P022-030",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22025",
                        "P0022-025 F6 Banking Bag Collections",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22023",
                        "P022-023 If manager allow same auth code",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22022",
                        "P0022-022 Set Collection Complete after Despatch",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22019",
                        "Remedial work for P022-019",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22017",
                        "Enable requirement P022-017",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22016",
                        "Enable requirement PO22-016",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22013",
                        "P0013-022 Launch Audit Roll Enq from Pickup Entry",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22012",
                        "Enable requirement PO22-012",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22011",
                        "Enable requirement PO22-011",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22010",
                        "P0022-010 QOD Previous Days Statistics",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22008",
                        "Enable requirement P022-008",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22006",
                        "Enable requirement P022-006",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22005",
                        "P0022-05 QOD Maintain Stock Enquiry Btn",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22004",
                        "P0022-04 QOD Maintain Col Order",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22002",
                        "Enable Project 22-002",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-22001",
                        "Enable requirement PO22-001",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-2733",
                        "Enable Coupons ProcessTransmissions",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-2290",
                        "Enable requirement CR0054-02",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-2289",
                        "Enable requirement CR0054-01",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-1086",
                        "Enable mis-spelt Stock Item searching",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-1046",
                        "Referral 1046: Recall Quote",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-1034",
                        "Enable requirement RF1034",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-1031",
                        "Enable requirement RF1031",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-1029",
                        "Enable requirement RF1029",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-1000",
                        "Period End Dates Install",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-983",
                        "Enable requirement RF0983",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-964",
                        "Enable requirement PO15",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-963",
                        "RF0963 Refund Holding DRL issue",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-961",
                        "Enable Requirement RF961",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-951",
                        "Enable requirement RF0951",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-946",
                        "Enable requirement RF0946",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-922",
                        "Enable Requirement RF922",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-904",
                        "RF0904 Hubs QOD Refund Quantities",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-901",
                        "Enable requirement RF0901",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-892",
                        "Enable requirement RF0892",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-845",
                        "Enable requirement RF0845",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-844",
                        "Enable requirement RF0844",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-824",
                        "Referral 824 - Refresh Returns Header",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-597",
                        "Enable requirement RF0597",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-121",
                        "CR0121 - Negative Stock Issues",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-118",
                        "CR0118 Pick Instructions Switch",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-109",
                        "CR0109 Delivery Instructions Switch",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-105",
                        "Hot Service Logo",
                        "",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-104",
                        "Enable requirement CR0104",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-101",
                        "Disable CR0101 Harrow Back Office Test Environment",
                        "not set",
                        "not set",
                        "false",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-96",
                        "Enable change request CR0096",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-87",
                        "Enable change request 87",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-61",
                        "Enable Requirement RF61",
                        "not set",
                        "0",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-15",
                        "Enable requirement PO15",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-8",
                        "CR008 Price Match promise",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "-4",
                        "Auto Price Change: Disable Label Printed Check",
                        "not set",
                        "not set",
                        "false",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "0",
                        "Release Version",
                        "<version>",
                        "not set",
                        "false",
                        "not set",
                        "0"});
            table1.AddRow(new string[] {
                        "980016",
                        "Enable Change request 16",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980064",
                        "Enable Change Request CR0064",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980081",
                        "Enable Change request 81",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980082",
                        "Enable Change Request CR0082",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980097",
                        "Enable CR0097",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980834",
                        "Enable requirement RF0834",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980870",
                        "Enable Referral 870 Fix",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980876",
                        "Enable Requirement RF0876",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980944",
                        "Enable requirement RF0944",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980949",
                        "Enable Referral 949 Fix",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980979",
                        "Enable Referral 979 fix",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980981",
                        "Enable Referral 981 fix",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "980986",
                        "Enable requirement RF0986",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "981093",
                        "Enable modal card authorisation",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "981094",
                        "Enable requirement RF1094",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "981404",
                        "Enable requirement PO14-04",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "983000",
                        "Enable requirement SC300e",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "983351",
                        "Enable User Story 3351 (Retrieve Coupon)",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "983352",
                        "Enable requirement PO14-02",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "983375",
                        "Enable User Story 3375 (Global Event)",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "984151",
                        "Enable requirement US4151",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "990099",
                        "Exclude price changes from HOSTU processing",
                        "not set",
                        "not set",
                        "false",
                        "not set",
                        "3"});
            table1.AddRow(new string[] {
                        "9801403",
                        "Enable requirement PO14-03",
                        "not set",
                        "not set",
                        "true",
                        "not set",
                        "3"});
#line 6
  testRunner.Then("I expect the layout of \"Parameters\" table as documented:", ((string)(null)), table1, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
