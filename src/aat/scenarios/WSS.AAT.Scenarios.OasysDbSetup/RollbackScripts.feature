@OasysDB
Feature: RollbackScripts

Scenario: Run Rollback scripts
  Given that we have rollback scripts
  When I run "RollBack" scripts
  Then I don't get errors

Scenario: Check the number of scripts
  Given that we have rollback scripts
  Then the number of rollback scripts should be equal to rollout scripts