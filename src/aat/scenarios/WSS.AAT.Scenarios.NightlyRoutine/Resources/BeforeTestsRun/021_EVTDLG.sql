IF OBJECT_ID ('tempdb..#Temp_EVTDLG') IS NOT NULL DROP TABLE #Temp_EVTDLG;
 
CREATE TABLE #Temp_EVTDLG
( 
	[NUMB] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[DLGN] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[TYPE] [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
	[KEY1] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[IDEL] [bit] NOT NULL,
	[QUAN] [decimal](7, 0) NULL,
	[VERO] [decimal](9, 2) NULL,
	[PERO] [decimal](7, 3) NULL,
	[SPARE] [char](40) COLLATE Latin1_General_CI_AS NULL
 );
 
INSERT INTO #Temp_EVTDLG([NUMB], [DLGN], [TYPE], [KEY1], [IDEL], [QUAN], [VERO], [PERO], [SPARE])
VALUES
(N'060000', N'010417', N'M', N'206957', 0, 1, 0.00, 50.000, NULL),
(N'060000', N'010417', N'S', N'110457', 0, 1, 0.00, 50.000, NULL)

MERGE  [dbo].[EVTDLG] AS Target
 USING #Temp_EVTDLG AS Source
	ON Target.[NUMB] = Source.[NUMB]
		AND Target.[DLGN] = Source.[DLGN]
		AND Target.[TYPE] = Source.[TYPE]	
		AND Target.[KEY1] = Source.[KEY1]
WHEN MATCHED THEN
UPDATE SET
 Target.[IDEL] = Source.[IDEL], Target.[QUAN] = Source.[QUAN], Target.[VERO] = Source.[VERO], Target.[PERO] = Source.[PERO], Target.[SPARE] = Source.[SPARE]  
WHEN NOT MATCHED BY TARGET
THEN
   INSERT([NUMB], [DLGN], [TYPE], [KEY1], [IDEL], [QUAN], [VERO], [PERO], [SPARE])
   VALUES(Source.[NUMB], Source.[DLGN], Source.[TYPE], Source.[KEY1], Source.[IDEL], Source.[QUAN], Source.[VERO], Source.[PERO], Source.[SPARE]);
GO