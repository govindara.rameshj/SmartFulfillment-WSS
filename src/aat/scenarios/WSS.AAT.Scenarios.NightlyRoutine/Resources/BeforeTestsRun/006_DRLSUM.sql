IF OBJECT_ID ('tempdb..#Temp_DRLSUM') IS NOT NULL DROP TABLE #Temp_DRLSUM;
 
CREATE TABLE #Temp_DRLSUM
(  	
	[NUMB] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[TYPE] [char](1) COLLATE Latin1_General_CI_AS NULL,
	[VALU] [decimal](9, 2) NOT NULL,
	[DATE1] [date] NULL,
	[CLAS] [char](2) COLLATE Latin1_General_CI_AS NULL,
	[COMM] [bit] NOT NULL,
	[TKEY] [int] NOT NULL,
	[INIT] [char](5) COLLATE Latin1_General_CI_AS NULL,
	[INFO] [char](20) COLLATE Latin1_General_CI_AS NULL,
	[0BBC] [bit] NOT NULL,
	[0SUP] [char](5) COLLATE Latin1_General_CI_AS NULL,
	[0CON] [int] NULL,
	[0PON] [int] NULL,
	[0DAT] [date] NULL,
	[0REL] [int] NOT NULL,
	[0SOQ] [int] NULL,
	[0DL1] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[0DL2] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[0DL3] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[0DL4] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[0DL5] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[0DL6] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[0DL7] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[0DL8] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[0DL9] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[1STR] [char](3) COLLATE Latin1_General_CI_AS NULL,
	[1IBT] [int] NULL,
	[1PRT] [bit] NOT NULL,
	[3SUP] [char](5) COLLATE Latin1_General_CI_AS NULL,
	[3DAT] [date] NULL,
	[3PON] [int] NULL,
	[1CON] [int] NULL,
	[IPSO] [bit] NOT NULL,
	[RTI] [char](1) COLLATE Latin1_General_CI_AS NULL,
	[EmployeeId] [int] NOT NULL
 );
 
INSERT #Temp_DRLSUM ([NUMB], [TYPE], [VALU], [DATE1], [CLAS], [COMM], [TKEY], [INIT], [INFO], [0BBC], [0SUP], [0CON], [0PON], [0DAT], [0REL], [0SOQ], [0DL1], [0DL2], [0DL3], [0DL4], [0DL5], [0DL6], [0DL7], [0DL8], [0DL9], [1STR], [1IBT], [1PRT], [3SUP], [3DAT], [3PON], [1CON], [IPSO], [RTI], [EmployeeId]) VALUES (N'112942', N'2', CAST(69.99 AS Decimal(9, 2)), CAST('2014-09-17' AS Date), N'00', 0, 0, N'mln ', N'OM Reference 1960324', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'120', 0, 0, NULL, NULL, NULL, NULL, 0, N'C', 0)
INSERT #Temp_DRLSUM ([NUMB], [TYPE], [VALU], [DATE1], [CLAS], [COMM], [TKEY], [INIT], [INFO], [0BBC], [0SUP], [0CON], [0PON], [0DAT], [0REL], [0SOQ], [0DL1], [0DL2], [0DL3], [0DL4], [0DL5], [0DL6], [0DL7], [0DL8], [0DL9], [1STR], [1IBT], [1PRT], [3SUP], [3DAT], [3PON], [1CON], [IPSO], [RTI], [EmployeeId]) VALUES (N'112943', N'2', CAST(59.04 AS Decimal(9, 2)), CAST('2014-09-17' AS Date), N'00', 0, 0, N'mln ', N'OM Reference 1960339', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'120', 0, 0, NULL, NULL, NULL, NULL, 0, N'C', 0)
INSERT #Temp_DRLSUM ([NUMB], [TYPE], [VALU], [DATE1], [CLAS], [COMM], [TKEY], [INIT], [INFO], [0BBC], [0SUP], [0CON], [0PON], [0DAT], [0REL], [0SOQ], [0DL1], [0DL2], [0DL3], [0DL4], [0DL5], [0DL6], [0DL7], [0DL8], [0DL9], [1STR], [1IBT], [1PRT], [3SUP], [3DAT], [3PON], [1CON], [IPSO], [RTI], [EmployeeId]) VALUES (N'112944', N'2', CAST(251.12 AS Decimal(9, 2)), CAST('2014-09-17' AS Date), N'00', 0, 0, N'mln ', N'OM Reference 1960402', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'120', 0, 0, NULL, NULL, NULL, NULL, 0, N'C', 0)

MERGE  [dbo].[DRLSUM] AS Target
 USING #Temp_DRLSUM AS Source
	ON Target.[NUMB] = Source.[NUMB]
WHEN MATCHED
THEN
   UPDATE SET
      Target.[NUMB] = Source.[NUMB], Target.[TYPE] = Source.[TYPE], Target.[VALU] = Source.[VALU], Target.[DATE1] = Source.[DATE1], 
      Target.[CLAS] = Source.[CLAS], Target.[COMM] = Source.[COMM], Target.[TKEY] = Source.[TKEY], Target.[INIT] = Source.[INIT], 
      Target.[INFO] = Source.[INFO], Target.[0BBC] = Source.[0BBC], Target.[0SUP] = Source.[0SUP], Target.[0CON] = Source.[0CON], 
      Target.[0PON] = Source.[0PON], Target.[0DAT] = Source.[0DAT], Target.[0REL] = Source.[0REL], Target.[0SOQ] = Source.[0SOQ], 
      Target.[0DL1] = Source.[0DL1], Target.[0DL2] = Source.[0DL2], Target.[0DL3] = Source.[0DL3], Target.[0DL4] = Source.[0DL4], 
      Target.[0DL5] = Source.[0DL5], Target.[0DL6] = Source.[0DL6], Target.[0DL7] = Source.[0DL7], Target.[0DL8] = Source.[0DL8], 
      Target.[0DL9] = Source.[0DL9], Target.[1STR] = Source.[1STR], Target.[1IBT] = Source.[1IBT], Target.[1PRT] = Source.[1PRT], 
      Target.[3SUP] = Source.[3SUP], Target.[3DAT] = Source.[3DAT], Target.[3PON] = Source.[3PON], Target.[1CON] = Source.[1CON], 
      Target.[IPSO] = Source.[IPSO], Target.[RTI] = Source.[RTI], Target.[EmployeeId] = Source.[EmployeeId]
     
WHEN NOT MATCHED BY TARGET
THEN
   INSERT 
   (
		[NUMB], [TYPE], [VALU], [DATE1], [CLAS], [COMM], [TKEY], [INIT], [INFO], [0BBC], [0SUP], [0CON], [0PON], [0DAT], [0REL], [0SOQ], [0DL1], [0DL2], [0DL3], [0DL4], [0DL5], [0DL6], [0DL7], [0DL8], [0DL9], [1STR], [1IBT], [1PRT], [3SUP], [3DAT], [3PON], [1CON], [IPSO], [RTI], [EmployeeId]
	)
   VALUES 
   (
   		Source.[NUMB], Source.[TYPE], Source.[VALU], Source.[DATE1], Source.[CLAS], Source.[COMM], Source.[TKEY], Source.[INIT], Source.[INFO], Source.[0BBC], Source.[0SUP], Source.[0CON], Source.[0PON], Source.[0DAT], Source.[0REL], Source.[0SOQ], Source.[0DL1], Source.[0DL2], Source.[0DL3], Source.[0DL4], Source.[0DL5], Source.[0DL6], Source.[0DL7], Source.[0DL8], Source.[0DL9], Source.[1STR], Source.[1IBT], Source.[1PRT], Source.[3SUP], Source.[3DAT], Source.[3PON], Source.[1CON], Source.[IPSO], Source.[RTI], Source.[EmployeeId]
	);
GO