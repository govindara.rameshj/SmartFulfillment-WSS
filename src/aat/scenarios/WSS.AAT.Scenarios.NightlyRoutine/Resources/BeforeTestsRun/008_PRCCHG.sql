IF OBJECT_ID ('tempdb..#Temp_PRCCHG') IS NOT NULL DROP TABLE #Temp_PRCCHG;
 
CREATE TABLE #Temp_PRCCHG
( 
	[SKUN] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[PDAT] [date] NOT NULL,
	[PRIC] [decimal](9, 2) NOT NULL,
	[PSTA] [char](1) COLLATE Latin1_General_CI_AS NOT NULL,
	[SHEL] [bit] NOT NULL,
	[AUDT] [date] NULL,
	[AUAP] [date] NULL,
	[MARK] [decimal](9, 2) NULL,
	[MCOM] [bit] NOT NULL,
	[LABS] [bit] NOT NULL,
	[LABM] [bit] NOT NULL,
	[LABL] [bit] NOT NULL,
	[EVNT] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[PRIO] [char](2) COLLATE Latin1_General_CI_AS NULL,
	[EEID] [char](3) COLLATE Latin1_General_CI_AS NULL,
	[MEID] [char](3) COLLATE Latin1_General_CI_AS NULL
 );

INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'100002', (SELECT TODT FROM dbo.SYSDAT), CAST(5.49 AS Decimal(9, 2)), N'U', 1, CAST(0x87360B00 AS Date), CAST(0x86360B00 AS Date), CAST(40.74 AS Decimal(9, 2)), 0, 0, 0, 0, N'077963', N'10', N'012', N'012')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'168658', CAST(0x36370B00 AS Date), CAST(20.99 AS Decimal(9, 2)), N'S', 0, CAST(0x36370B00 AS Date), CAST(0x35370B00 AS Date), CAST(9.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'085636', N'10', N'066', N'066')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'203222', CAST(0x69360B00 AS Date), CAST(2574.00 AS Decimal(9, 2)), N'S', 0, CAST(0x69360B00 AS Date), CAST(0x68360B00 AS Date), CAST(0.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'078111', N'20', N'008', N'008')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'203222', CAST(0x6C360B00 AS Date), CAST(2317.00 AS Decimal(9, 2)), N'S', 0, CAST(0x6C360B00 AS Date), CAST(0x6B360B00 AS Date), CAST(0.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'078110', N'10', N'008', N'008')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'203222', CAST(0xCE360B00 AS Date), CAST(1692.00 AS Decimal(9, 2)), N'S', 0, CAST(0xCE360B00 AS Date), CAST(0xCD360B00 AS Date), CAST(0.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'079543', N'10', N'073', N'073')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'203222', CAST(0x27370B00 AS Date), CAST(1777.00 AS Decimal(9, 2)), N'S', 0, CAST(0x27370B00 AS Date), CAST(0x26370B00 AS Date), CAST(0.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'085778', N'10', N'066', N'066')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'203222', CAST(0x4F370B00 AS Date), CAST(1510.45 AS Decimal(9, 2)), N'S', 0, CAST(0x4F370B00 AS Date), CAST(0x4E370B00 AS Date), CAST(0.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'086563', N'20', N'073', N'073')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'203222', CAST(0x5E370B00 AS Date), CAST(1777.00 AS Decimal(9, 2)), N'S', 0, CAST(0x5E370B00 AS Date), CAST(0x5D370B00 AS Date), CAST(0.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'085778', N'10', N'000', N'000')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'203222', CAST(0x82370B00 AS Date), CAST(1830.31 AS Decimal(9, 2)), N'S', 0, CAST(0x82370B00 AS Date), CAST(0x81370B00 AS Date), CAST(0.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'087141', N'10', N'073', N'073')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'203222', CAST(0xAB370B00 AS Date), CAST(1630.31 AS Decimal(9, 2)), N'S', 0, CAST(0xAB370B00 AS Date), CAST(0xAA370B00 AS Date), CAST(0.00 AS Decimal(9, 2)), 0, 1, 0, 0, N'087677', N'20', N'012', N'012')

INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'214523', (SELECT TOP(1) TODT FROM dbo.SYSDAT), CAST(0.03 AS Decimal(9, 2)), N'U', 0,(SELECT TOP(1) TODT FROM dbo.SYSDAT), NULL, CAST(0.00 AS Decimal(9, 2)), 1, 1, 0, 0, N'087666', N'10', N'000', N'000')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'158422', (SELECT TOP(1) DATEADD(day, -2, TODT) TMDT  FROM dbo.SYSDAT), CAST(4.30 AS Decimal(9, 2)), N'U', 0, (SELECT TOP(1) DATEADD(day, -2, TODT) TMDT  FROM dbo.SYSDAT), NULL, CAST(0.00 AS Decimal(9, 2)), 1, 1, 0, 0, N'065255', N'10', N'000', N'000')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'187773', (SELECT TOP(1) DATEADD(day, 1, TODT) TMDT  FROM dbo.SYSDAT), CAST(3.80 AS Decimal(9, 2)), N'U', 0, (SELECT TOP(1) DATEADD(day, 1, TODT) TMDT  FROM dbo.SYSDAT), NULL, CAST(0.00 AS Decimal(9, 2)), 1, 0, 0, 1, N'065256', N'10', N'000', N'000')
INSERT #Temp_PRCCHG ([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID]) VALUES (N'187793', (SELECT TOP(1) DATEADD(day, 2, TODT) TMDT  FROM dbo.SYSDAT), CAST(12.54 AS Decimal(9, 2)), N'U', 0, (SELECT TOP(1) DATEADD(day, 2, TODT) TMDT  FROM dbo.SYSDAT), NULL, CAST(0.00 AS Decimal(9, 2)), 1, 0, 0, 1, N'065257', N'10', N'000', N'000')

MERGE  [dbo].[PRCCHG] AS Target
 USING #Temp_PRCCHG AS Source
	ON Target.[SKUN] = Source.[SKUN]
		AND Target.[PDAT] = Source.[PDAT]
		AND Target.[PRIC] = Source.[PRIC]	
		AND Target.[PSTA] = Source.[PSTA]
		AND Target.[EVNT] = Source.[EVNT]	
WHEN MATCHED THEN
UPDATE SET
 Target.[SHEL] = Source.[SHEL], Target.[AUDT] = Source.[AUDT], Target.[AUAP] = Source.[AUAP], Target.[MARK] = Source.[MARK], Target.[MCOM] = Source.[MCOM], Target.[LABS] = Source.[LABS], Target.[LABM] = Source.[LABM], Target.[LABL] = Source.[LABL], Target.[PRIO] = Source.[PRIO], Target.[EEID] = Source.[EEID], Target.[MEID] = Source.[MEID]
WHEN NOT MATCHED BY TARGET
THEN
   INSERT([SKUN], [PDAT], [PRIC], [PSTA], [SHEL], [AUDT], [AUAP], [MARK], [MCOM], [LABS], [LABM], [LABL], [EVNT], [PRIO], [EEID], [MEID])
   VALUES(Source.[SKUN], Source.[PDAT], Source.[PRIC], Source.[PSTA], Source.[SHEL], Source.[AUDT], Source.[AUAP], Source.[MARK], Source.[MCOM], Source.[LABS], Source.[LABM], Source.[LABL], Source.[EVNT], Source.[PRIO], Source.[EEID], Source.[MEID]);
GO