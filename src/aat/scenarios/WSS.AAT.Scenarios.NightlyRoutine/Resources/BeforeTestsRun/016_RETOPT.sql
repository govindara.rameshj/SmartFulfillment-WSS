UPDATE dbo.RETOPT 
SET 
	DOLR = '17/09/14',
	PCMA = '000',
	PCDA = 0,
	TTDE1 = 'CASH        ', 
	TTDE2 = 'CHEQUE      ', 
	TTDE3 = 'ACCESS/VISA ', 
	TTDE4 = 'OLD TOKEN   ', 
	TTDE5 = 'PROJECT LOAN', 
	TTDE6 = 'VOUCHER     ', 
	TTDE7 = 'GIFT TOKEN  ', 
	TTDE8 = 'MAESTRO     ', 
	TTDE9 = 'AmEx        ', 
	TTDE10 = 'H/O CHEQUE  ',
	TTDE12 = 'Web Tender  ',
	TTDE13 = 'Gift Card   '
WHERE FKEY = '01'
GO

