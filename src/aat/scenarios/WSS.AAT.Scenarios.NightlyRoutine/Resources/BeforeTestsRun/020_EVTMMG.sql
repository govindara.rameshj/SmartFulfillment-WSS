IF OBJECT_ID ('tempdb..#Temp_EVTMMG') IS NOT NULL DROP TABLE #Temp_EVTMMG;
 
CREATE TABLE #Temp_EVTMMG
( 
	[MMGN] [char](6) COLLATE Latin1_General_CI_AS NOT NULL, 
	[SKUN] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[IDEL] [bit] NOT NULL,
	[SPARE] [char](40) COLLATE Latin1_General_CI_AS NULL
 );

INSERT INTO #Temp_EVTMMG([MMGN], [SKUN], [IDEL], [SPARE]) VALUES(N'206957', N'101318', 0, NULL)

MERGE  [dbo].[EVTMMG] AS Target
 USING #Temp_EVTMMG AS Source
	ON Target.[MMGN] = Source.[MMGN]
		AND Target.[SKUN] = Source.[SKUN]
WHEN MATCHED THEN
UPDATE SET
 Target.[IDEL] = Source.[IDEL], Target.[SPARE] = Source.[SPARE]  
WHEN NOT MATCHED BY TARGET
THEN
   INSERT([MMGN], [SKUN], [IDEL], [SPARE])
   VALUES(Source.[MMGN], Source.[SKUN], Source.[IDEL], Source.[SPARE]);
GO