IF OBJECT_ID ('tempdb..#Temp_RELITM') IS NOT NULL DROP TABLE #Temp_RELITM;
 
CREATE TABLE #Temp_RELITM
( 
	[SPOS] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[PPOS] [char](6) COLLATE Latin1_General_CI_AS NULL,
	[DELC] [bit] NOT NULL,
	[NSPP] [decimal](5, 0) NULL,
	[QTYS] [decimal](7, 0) NULL,
	[PTDQ] [decimal](7, 0) NULL,
	[YTDQ] [decimal](9, 0) NULL,
	[PYRQ] [decimal](9, 0) NULL,
	[WTDS] [decimal](7, 0) NULL,
	[PWKS] [decimal](7, 0) NULL,
	[WTDM] [decimal](9, 2) NULL,
	[PWKM] [decimal](9, 2) NULL,
	[PTDM] [decimal](9, 2) NULL,
	[PPDM] [decimal](9, 2) NULL,
	[YTDM] [decimal](9, 2) NULL,
	[PYRM] [decimal](9, 2) NULL,
 );

INSERT #Temp_RELITM ([SPOS], [PPOS], [DELC], [NSPP], [QTYS], [PTDQ], [YTDQ], [PYRQ], [WTDS], [PWKS], [WTDM], [PWKM], [PTDM], [PPDM], [YTDM], [PYRM]) VALUES (N'107003', N'107002', 0, CAST(10 AS Decimal(5, 0)), CAST(-29 AS Decimal(7, 0)), CAST(20 AS Decimal(7, 0)), CAST(20 AS Decimal(9, 0)), CAST(0 AS Decimal(9, 0)), CAST(0 AS Decimal(7, 0)), CAST(10 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), CAST(-14.00 AS Decimal(9, 2)), CAST(65.47 AS Decimal(9, 2)), CAST(3.81 AS Decimal(9, 2)), CAST(65.47 AS Decimal(9, 2)), CAST(83.42 AS Decimal(9, 2)))
INSERT #Temp_RELITM ([SPOS], [PPOS], [DELC], [NSPP], [QTYS], [PTDQ], [YTDQ], [PYRQ], [WTDS], [PWKS], [WTDM], [PWKM], [PTDM], [PPDM], [YTDM], [PYRM]) VALUES (N'107007', N'100804', 0, CAST(10 AS Decimal(5, 0)), CAST(-52 AS Decimal(7, 0)), CAST(0 AS Decimal(7, 0)), CAST(0 AS Decimal(9, 0)), CAST(0 AS Decimal(9, 0)), CAST(0 AS Decimal(7, 0)), CAST(20 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), CAST(-32.00 AS Decimal(9, 2)), CAST(3.12 AS Decimal(9, 2)), CAST(4.82 AS Decimal(9, 2)), CAST(3.12 AS Decimal(9, 2)), CAST(68.00 AS Decimal(9, 2)))
INSERT #Temp_RELITM ([SPOS], [PPOS], [DELC], [NSPP], [QTYS], [PTDQ], [YTDQ], [PYRQ], [WTDS], [PWKS], [WTDM], [PWKM], [PTDM], [PPDM], [YTDM], [PYRM]) VALUES (N'107009', N'100805', 0, CAST(10 AS Decimal(5, 0)), CAST(-59 AS Decimal(7, 0)), CAST(20 AS Decimal(7, 0)), CAST(20 AS Decimal(9, 0)), CAST(0 AS Decimal(9, 0)), CAST(0 AS Decimal(7, 0)), CAST(10 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), CAST(-11.00 AS Decimal(9, 2)), CAST(128.77 AS Decimal(9, 2)), CAST(6.91 AS Decimal(9, 2)), CAST(128.77 AS Decimal(9, 2)), CAST(167.03 AS Decimal(9, 2)))
INSERT #Temp_RELITM ([SPOS], [PPOS], [DELC], [NSPP], [QTYS], [PTDQ], [YTDQ], [PYRQ], [WTDS], [PWKS], [WTDM], [PWKM], [PTDM], [PPDM], [YTDM], [PYRM]) VALUES (N'100158', N'205598', 0, CAST(10 AS Decimal(5, 0)), CAST(-20 AS Decimal(7, 0)), CAST(0 AS Decimal(7, 0)), CAST(0 AS Decimal(9, 0)), CAST(0 AS Decimal(9, 0)), CAST(0 AS Decimal(7, 0)), CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(-32.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(-32.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)))

MERGE  [dbo].[RELITM] AS Target
 USING #Temp_RELITM AS Source
	ON Target.[SPOS] = Source.[SPOS]
WHEN MATCHED THEN
UPDATE SET
 Target.[PPOS] = Source.[DELC], Target.[NSPP] = Source.[QTYS], Target.[PTDQ] = Source.[YTDQ], Target.[PYRQ] = Source.[PYRQ], Target.[WTDS] = Source.[WTDS], Target.[PWKS] = Source.[PWKS], Target.[WTDM] = Source.[WTDM], Target.[PWKM] = Source.[PWKM], Target.[PTDM] = Source.[PTDM], Target.[PPDM] = Source.[PPDM], Target.[YTDM] = Source.[YTDM], Target.[PYRM] = Source.[PYRM]  
WHEN NOT MATCHED BY TARGET
THEN
   INSERT([SPOS], [PPOS], [DELC], [NSPP], [QTYS], [PTDQ], [YTDQ], [PYRQ], [WTDS], [PWKS], [WTDM], [PWKM], [PTDM], [PPDM], [YTDM], [PYRM])
   VALUES(Source.[SPOS], Source.[PPOS], Source.[DELC], Source.[NSPP], Source.[QTYS], Source.[PTDQ], Source.[YTDQ], Source.[PYRQ], Source.[WTDS], Source.[PWKS], Source.[WTDM], Source.[PWKM], Source.[PTDM], Source.[PPDM], Source.[YTDM], Source.[PYRM]);
GO