IF OBJECT_ID ('tempdb..#Temp_EVTMAS') IS NOT NULL DROP TABLE #Temp_EVTMAS;
 
CREATE TABLE #Temp_EVTMAS
( 
	[TYPE] [char](2) COLLATE Latin1_General_CI_AS NOT NULL,
	[KEY1] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[KEY2] [char](8) COLLATE Latin1_General_CI_AS NOT NULL,
	[NUMB] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[PRIO] [char](2) COLLATE Latin1_General_CI_AS NULL,
	[IDEL] [bit] NOT NULL,
	[IDOW] [bit] NOT NULL,
	[SDAT] [date] NULL,
	[EDAT] [date] NULL,
	[PRIC] [decimal](9, 2) NULL,
	[BQTY] [decimal](7, 0) NULL,
	[GQTY] [decimal](7, 0) NULL,
	[VDIS] [decimal](9, 2) NULL,
	[PDIS] [decimal](7, 3) NULL,
	[BUYCPN] [char](7) COLLATE Latin1_General_CI_AS NOT NULL,
	[GETCPN] [char](7) COLLATE Latin1_General_CI_AS NOT NULL,
 );
 
INSERT INTO #Temp_EVTMAS ([TYPE], [KEY1], [KEY2], [NUMB], [PRIO], [IDEL], [IDOW], [SDAT], [EDAT], [PRIC], [BQTY], [GQTY], [VDIS], [PDIS], [BUYCPN], [GETCPN])
VALUES
(N'DG', N'000000', N'01010417', N'060000', N'30', 0, 1, '20130213 00:00:00.000', '20130331 00:00:00.000', 9.98, 0, 0, 0.00, 0.000, N'0000000', N'0000000'),
(N'QS', N'110037', N'01000005', N'060000', N'30', 0, 1, '20110907 00:00:00.000', '20120103 00:00:00.000', 31.57, 0, 0, 0.00, 0.000, N'0000000', N'0000000'),
(N'HS', N'125368', N'01000100', N'060000', N'30', 0, 1, '20130201 00:00:00.000', '20140101 00:00:00.000', 1.00, 0, 0, 0.00, 100.000, N'0000072', N'0000000'),
(N'MM', N'206957', N'01000010', N'060000', N'50', 0, 1, '20120601 00:00:00.000', '20120611 00:00:00.000', 2.28, 0, 0, 0.00, 0.000, N'0000000', N'0000000')

MERGE  [dbo].[EVTMAS] AS Target
 USING #Temp_EVTMAS AS Source
	ON Target.[KEY1] = Source.[KEY1] 
	AND Target.[KEY2] = Source.[KEY2] 
	AND Target.[NUMB] = Source.[NUMB] 
	AND Target.[TYPE] = Source.[TYPE]
WHEN MATCHED THEN
UPDATE SET
  Target.[PRIO] = Source.[PRIO], Target.[IDEL] = Source.[IDEL], Target.[IDOW] = Source.[IDOW], Target.[SDAT] = Source.[SDAT], Target.[EDAT] = Source.[EDAT]
, Target.[PRIC] = Source.[PRIC], Target.[BQTY] = Source.[BQTY], Target.[GQTY] = Source.[GQTY], Target.[VDIS] = Source.[VDIS], Target.[PDIS] = Source.[PDIS]
, Target.[BUYCPN] = Source.[BUYCPN], Target.[GETCPN] = Source.[GETCPN]
WHEN NOT MATCHED THEN
INSERT (
 [TYPE],[KEY1],[KEY2],[NUMB],[PRIO],[IDEL],[IDOW],[SDAT],[EDAT],[PRIC],[BQTY],[GQTY],[VDIS],[PDIS],[BUYCPN],[GETCPN]
)
VALUES (
  Source.[TYPE], Source.[KEY1], Source.[KEY2], Source.[NUMB], Source.[PRIO], Source.[IDEL], Source.[IDOW], Source.[SDAT], Source.[EDAT], Source.[PRIC], Source.[BQTY]
, Source.[GQTY], Source.[VDIS], Source.[PDIS], Source.[BUYCPN], Source.[GETCPN]
);
GO