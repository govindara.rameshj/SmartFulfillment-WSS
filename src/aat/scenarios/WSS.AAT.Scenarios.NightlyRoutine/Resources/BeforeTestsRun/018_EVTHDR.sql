IF OBJECT_ID ('tempdb..#Temp_EVTHDR') IS NOT NULL DROP TABLE #Temp_EVTHDR;
 
CREATE TABLE #Temp_EVTHDR
( 
	[NUMB] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[DESCR] [char](40) COLLATE Latin1_General_CI_AS NULL,
	[PRIO] [char](2) COLLATE Latin1_General_CI_AS NULL,
	[SDAT] [date] NULL,
	[STIM] [char](4) COLLATE Latin1_General_CI_AS NULL,
	[EDAT] [date] NULL,
	[ETIM] [char](4) COLLATE Latin1_General_CI_AS NULL,
	[DACT1] [bit] NOT NULL,
	[DACT2] [bit] NOT NULL,
	[DACT3] [bit] NOT NULL,
	[DACT4] [bit] NOT NULL,
	[DACT5] [bit] NOT NULL,
	[DACT6] [bit] NOT NULL,
	[DACT7] [bit] NOT NULL,
	[IDEL] [bit] NOT NULL,
	[SPARE] [char](40) COLLATE Latin1_General_CI_AS NULL,
 );
 
INSERT INTO #Temp_EVTHDR([NUMB], [DESCR], [PRIO], [SDAT], [STIM], [EDAT], [ETIM], [DACT1], [DACT2], [DACT3], [DACT4], [DACT5], [DACT6], [DACT7], [IDEL], [SPARE]) VALUES ( N'060000', N'Product Price Import ' + CONVERT(varchar, GETDATE()), N'10', '20110108 00:00:00.000', N'0000', DATEADD(day,1,GETDATE()), N'0000', 1, 1, 1, 1, 1, 1, 1, 0, NULL )

MERGE  [dbo].[EVTHDR] AS Target
 USING #Temp_EVTHDR AS Source
	ON Target.[NUMB] = Source.[NUMB]	
WHEN MATCHED THEN
UPDATE SET
  Target.[DESCR] = Source.[DESCR], Target.[PRIO] = Source.[PRIO], Target.[SDAT] = Source.[SDAT], Target.[STIM] = Source.[STIM], Target.[EDAT] = Source.[EDAT]
, Target.[ETIM] = Source.[ETIM], Target.[DACT1] = Source.[DACT1], Target.[DACT2] = Source.[DACT2], Target.[DACT3] = Source.[DACT3], Target.[DACT4] = Source.[DACT4]
, Target.[DACT5] = Source.[DACT5], Target.[DACT6] = Source.[DACT6], Target.[DACT7] = Source.[DACT7], Target.[IDEL] = Source.[IDEL], Target.[SPARE] = Source.[SPARE]
WHEN NOT MATCHED THEN
INSERT (
		[NUMB], [DESCR], [PRIO], [SDAT], [STIM], [EDAT], [ETIM], [DACT1], [DACT2], [DACT3], [DACT4],[DACT5], [DACT6], [DACT7], [IDEL], [SPARE]
		)
VALUES (
		Source.[NUMB], Source.[DESCR], Source.[PRIO], Source.[SDAT], Source.[STIM], Source.[EDAT], Source.[ETIM], Source.[DACT1], Source.[DACT2], Source.[DACT3]
		, Source.[DACT4], Source.[DACT5], Source.[DACT6], Source.[DACT7], Source.[IDEL], Source.[SPARE]
		);
GO
