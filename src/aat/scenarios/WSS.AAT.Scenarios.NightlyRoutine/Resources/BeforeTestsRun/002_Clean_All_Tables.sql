DELETE FROM dbo.DLGIFT

DELETE FROM dbo.DLLINE

DELETE FROM dbo.DLTOTS

DELETE FROM dbo.DRLDET

DELETE FROM dbo.DRLSUM

DELETE FROM dbo.PRCCHG

DELETE FROM dbo.PURHDR

DELETE FROM dbo.RELITM

DELETE FROM dbo.STKADJ

TRUNCATE TABLE dbo.EventDetailOverride

DELETE FROM dbo.STKMAS

DELETE FROM dbo.SUPDET

DELETE FROM dbo.SUPMAS

DELETE FROM dbo.TVCSTR

DELETE FROM dbo.EVTHDR

DELETE FROM dbo.EVTMAS

DELETE FROM dbo.EVTMMG

DELETE FROM dbo.EVTDLG

DELETE FROM dbo.TMPFIL

DELETE FROM dbo.STRMAS

DELETE FROM dbo.HIECAT

TRUNCATE TABLE dbo.NITMAS