IF OBJECT_ID ('tempdb..#Temp_SystemPeriods') IS NOT NULL DROP TABLE #Temp_SystemPeriods;
 
CREATE TABLE #Temp_SystemPeriods
( 
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[IsClosed] [bit] NOT NULL
 );
 
INSERT #Temp_SystemPeriods ([StartDate], [EndDate], [IsClosed]) VALUES (CONVERT (date, GETDATE()), cast(convert(char(8), getdate(), 112) + ' 23:59:59.99' as datetime), 0)

MERGE  [dbo].[SystemPeriods] AS Target
 USING #Temp_SystemPeriods AS Source
	ON Target.[StartDate] = Source.[StartDate]
		AND Target.[EndDate] = Source.[EndDate]			
WHEN MATCHED
THEN
   UPDATE SET
     Target.[IsClosed] = Source.[IsClosed] 
WHEN NOT MATCHED BY TARGET
THEN
   INSERT 
   (
		StartDate, EndDate, IsClosed
	)
   VALUES 
   (
		Source.StartDate, Source.EndDate, Source.IsClosed
   );
GO

