IF OBJECT_ID ('tempdb..#Temp_STKADJ') IS NOT NULL DROP TABLE #Temp_STKADJ;
 
CREATE TABLE #Temp_STKADJ
( 
	[DATE1] [date] NOT NULL,
	[CODE] [char](2) COLLATE Latin1_General_CI_AS NOT NULL,
	[SKUN] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[SEQN] [char](2) COLLATE Latin1_General_CI_AS NOT NULL,
	[AmendId] [int] NOT NULL,
	[INIT] [char](5) COLLATE Latin1_General_CI_AS NULL,
	[DEPT] [char](2) COLLATE Latin1_General_CI_AS NULL,
	[SSTK] [decimal](7, 0) NULL,
	[QUAN] [decimal](7, 0) NULL,
	[PRIC] [decimal](9, 2) NULL,
	[COST] [decimal](11, 4) NULL,
	[COMM] [bit] NOT NULL,
	[TYPE] [char](1) COLLATE Latin1_General_CI_AS NULL,
	[TSKU] [char](6) COLLATE Latin1_General_CI_AS NULL,
	[TVAL] [decimal](9, 2) NULL,
	[INFO] [char](20) COLLATE Latin1_General_CI_AS NULL,
	[DRLN] [char](6) COLLATE Latin1_General_CI_AS NULL,
	[RCOD] [char](1) COLLATE Latin1_General_CI_AS NULL,
	[MOWT] [char](1) COLLATE Latin1_General_CI_AS NULL,
	[WAUT] [char](3) COLLATE Latin1_General_CI_AS NULL,
	[DAUT] [date] NULL,
	[RTI] [char](1) COLLATE Latin1_General_CI_AS NULL,
	[PeriodID] [int] NOT NULL,
	[TransferStart] [decimal](7, 0) NULL,
	[TransferPrice] [decimal](9, 2) NULL,
	[IsReversed] [bit] NOT NULL,
	[ParentSeq] [char](2) COLLATE Latin1_General_CI_AS NOT NULL,
	[AdjustmentCount] [int] NOT NULL,
 );

INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'101000', N'00', 0, N'TCE  ', N'10', CAST(1121 AS Decimal(7, 0)), CAST(-1 AS Decimal(7, 0)), CAST(6.98 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'128603', N'00', 0, N'TCE  ', N'00', CAST(14 AS Decimal(7, 0)), CAST(1 AS Decimal(7, 0)), CAST(22.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'162770', N'00', 0, N'TCE  ', N'00', CAST(1 AS Decimal(7, 0)), CAST(-1 AS Decimal(7, 0)), CAST(13.39 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164025', N'00', 0, N'TCE  ', N'00', CAST(48 AS Decimal(7, 0)), CAST(-2 AS Decimal(7, 0)), CAST(32.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164052', N'00', 0, N'TCE  ', N'00', CAST(14 AS Decimal(7, 0)), CAST(-1 AS Decimal(7, 0)), CAST(17.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164055', N'00', 0, N'TCE  ', N'00', CAST(20 AS Decimal(7, 0)), CAST(6 AS Decimal(7, 0)), CAST(21.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164056', N'00', 0, N'TCE  ', N'00', CAST(2 AS Decimal(7, 0)), CAST(-2 AS Decimal(7, 0)), CAST(28.79 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164058', N'00', 0, N'TCE  ', N'00', CAST(16 AS Decimal(7, 0)), CAST(1 AS Decimal(7, 0)), CAST(8.91 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164059', N'00', 0, N'TCE  ', N'00', CAST(27 AS Decimal(7, 0)), CAST(2 AS Decimal(7, 0)), CAST(8.91 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164060', N'00', 0, N'TCE  ', N'00', CAST(2 AS Decimal(7, 0)), CAST(-1 AS Decimal(7, 0)), CAST(12.79 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164061', N'00', 0, N'TCE  ', N'00', CAST(11 AS Decimal(7, 0)), CAST(-2 AS Decimal(7, 0)), CAST(12.79 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164063', N'00', 0, N'TCE  ', N'00', CAST(18 AS Decimal(7, 0)), CAST(-2 AS Decimal(7, 0)), CAST(5.49 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164074', N'00', 0, N'TCE  ', N'00', CAST(11 AS Decimal(7, 0)), CAST(4 AS Decimal(7, 0)), CAST(5.49 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164078', N'00', 0, N'TCE  ', N'00', CAST(6 AS Decimal(7, 0)), CAST(-6 AS Decimal(7, 0)), CAST(9.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164084', N'00', 0, N'TCE  ', N'00', CAST(5 AS Decimal(7, 0)), CAST(-5 AS Decimal(7, 0)), CAST(0.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164088', N'00', 0, N'TCE  ', N'00', CAST(6 AS Decimal(7, 0)), CAST(2 AS Decimal(7, 0)), CAST(30.49 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'164089', N'00', 0, N'TCE  ', N'00', CAST(4 AS Decimal(7, 0)), CAST(-2 AS Decimal(7, 0)), CAST(30.49 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'209373', N'00', 0, N'TCE  ', N'00', CAST(651 AS Decimal(7, 0)), CAST(-3 AS Decimal(7, 0)), CAST(13.09 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'240025', N'00', 0, N'TCE  ', N'24', CAST(7 AS Decimal(7, 0)), CAST(-1 AS Decimal(7, 0)), CAST(5.79 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'240059', N'00', 0, N'TCE  ', N'24', CAST(81 AS Decimal(7, 0)), CAST(4 AS Decimal(7, 0)), CAST(15.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'240150', N'00', 0, N'TCE  ', N'24', CAST(41 AS Decimal(7, 0)), CAST(-2 AS Decimal(7, 0)), CAST(7.09 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'240165', N'00', 0, N'TCE  ', N'24', CAST(20 AS Decimal(7, 0)), CAST(-1 AS Decimal(7, 0)), CAST(7.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)
INSERT #Temp_STKADJ ([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount]) VALUES (CAST(0xF5360B00 AS Date), N'02', N'242400', N'00', 0, N'TCE  ', N'24', CAST(13 AS Decimal(7, 0)), CAST(1 AS Decimal(7, 0)), CAST(6.99 AS Decimal(9, 2)), CAST(0.0000 AS Decimal(11, 4)), 0, N'N', N'000000', CAST(0.00 AS Decimal(9, 2)), N'PIC - Primary Count ', N'000000', N' ', N' ', N'0  ', NULL, N'C', 0, CAST(0 AS Decimal(7, 0)), CAST(0.00 AS Decimal(9, 2)), 0, N'00', 1)

MERGE  [dbo].[STKADJ] AS Target
 USING #Temp_STKADJ AS Source
	ON Target.[DATE1] = Source.[DATE1]
		AND Target.[CODE] = Source.[CODE]
		AND Target.[SKUN] = Source.[SKUN]	
		AND Target.[SEQN] = Source.[SEQN]
		AND Target.[AmendId] = Source.[AmendId]
		AND Target.[AdjustmentCount] = Source.[AdjustmentCount]	
WHEN MATCHED THEN
UPDATE SET
 Target.[DEPT] = Source.[DEPT]
,Target.[SSTK] = Source.[SSTK]
,Target.[QUAN] = Source.[QUAN]
,Target.[PRIC] = Source.[PRIC]
,Target.[COST] = Source.[COST]
,Target.[COMM] = Source.[COMM]
,Target.[TYPE] = Source.[TYPE]
,Target.[TSKU] = Source.[TSKU]
,Target.[TVAL] = Source.[TVAL]
,Target.[INFO] = Source.[INFO]
,Target.[DRLN] = Source.[DRLN]
,Target.[RCOD] = Source.[RCOD]
,Target.[MOWT] = Source.[MOWT]
,Target.[WAUT] = Source.[WAUT]
,Target.[DAUT] = Source.[DAUT]
,Target.[RTI] = Source.[RTI]
,Target.[PeriodID] = Source.[PeriodID]
,Target.[TransferStart] = Source.[TransferStart]
,Target.[TransferPrice] = Source.[TransferPrice]
,Target.[IsReversed] = Source.[IsReversed]
,Target.[ParentSeq] = Source.[ParentSeq]
WHEN NOT MATCHED BY TARGET
THEN
   INSERT([DATE1], [CODE], [SKUN], [SEQN], [AmendId], [INIT], [DEPT], [SSTK], [QUAN], [PRIC], [COST], [COMM], [TYPE], [TSKU], [TVAL], [INFO], [DRLN], [RCOD], [MOWT], [WAUT], [DAUT], [RTI], [PeriodID], [TransferStart], [TransferPrice], [IsReversed], [ParentSeq], [AdjustmentCount])
   VALUES(
		 Source.[DATE1], Source.[CODE], Source.[SKUN], Source.[SEQN], Source.[AmendId], Source.[INIT], Source.[DEPT], Source.[SSTK], Source.[QUAN], Source.[PRIC], Source.[COST], Source.[COMM], Source.[TYPE], Source.[TSKU]
		,Source.[TVAL], Source.[INFO], Source.[DRLN], Source.[RCOD], Source.[MOWT], Source.[WAUT], Source.[DAUT], Source.[RTI], Source.[PeriodID], Source.[TransferStart], Source.[TransferPrice], Source.[IsReversed]
		,Source.[ParentSeq], Source.[AdjustmentCount]
   );
GO