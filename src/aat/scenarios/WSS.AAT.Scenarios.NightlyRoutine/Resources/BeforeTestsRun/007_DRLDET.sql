IF OBJECT_ID ('tempdb..#Temp_DRLDET') IS NOT NULL DROP TABLE #Temp_DRLDET;
 
CREATE TABLE #Temp_DRLDET
(  	
	[NUMB] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[SEQN] [char](4) COLLATE Latin1_General_CI_AS NOT NULL,
	[SKUN] [char](6) COLLATE Latin1_General_CI_AS NOT NULL,
	[ORDQ] [int] NOT NULL,
	[RECQ] [int] NOT NULL,
	[ORDP] [decimal](9, 2) NOT NULL,
	[BDES] [char](20) COLLATE Latin1_General_CI_AS NULL,
	[BSPC] [char](10) COLLATE Latin1_General_CI_AS NULL,
	[PRIC] [decimal](9, 2) NOT NULL,
	[IBTQ] [int] NOT NULL,
	[RETQ] [int] NOT NULL,
	[POLN] [int] NOT NULL,
	[FOLQ] [int] NOT NULL,
	[RTI] [char](1) COLLATE Latin1_General_CI_AS NULL,
	[ReasonCode] [char](2) COLLATE Latin1_General_CI_AS NULL
 );

INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112942', N'0001', N'222121', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(69.99 AS Decimal(9, 2)), 1, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112943', N'0001', N'224555', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(38.24 AS Decimal(9, 2)), 1, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112943', N'0002', N'224700', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(2.08 AS Decimal(9, 2)), 10, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112944', N'0001', N'500866', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(9.99 AS Decimal(9, 2)), 1, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112944', N'0002', N'224701', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(109.99 AS Decimal(9, 2)), 1, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112944', N'0003', N'107177', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(2.77 AS Decimal(9, 2)), 2, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112944', N'0004', N'224665', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(38.24 AS Decimal(9, 2)), 1, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112944', N'0005', N'153707', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(2.89 AS Decimal(9, 2)), 22, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112944', N'0006', N'160175', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(14.99 AS Decimal(9, 2)), 1, 0, 0, 0, N'C', NULL)
INSERT #Temp_DRLDET ([NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]) VALUES (N'112944', N'0007', N'600776', 0, 0, CAST(0.00 AS Decimal(9, 2)), NULL, NULL, CAST(8.79 AS Decimal(9, 2)), 1, 0, 0, 0, N'C', NULL)

MERGE  [dbo].[DRLDET] AS Target
 USING #Temp_DRLDET AS Source
	ON Target.[NUMB] = Source.[NUMB]
	AND Target.[SEQN] = Source.[SEQN]
WHEN MATCHED
THEN
   UPDATE SET
      Target.[NUMB] = Source.[NUMB], Target.[SEQN] = Source.[SEQN], Target.[SKUN] = Source.[SKUN], Target.[ORDQ] = Source.[ORDQ], 
      Target.[RECQ] = Source.[RECQ], Target.[ORDP] = Source.[ORDP], Target.[BDES] = Source.[BDES], Target.[BSPC] = Source.[BSPC], 
      Target.[PRIC] = Source.[PRIC], Target.[IBTQ] = Source.[IBTQ], Target.[RETQ] = Source.[RETQ], Target.[POLN] = Source.[POLN], 
      Target.[FOLQ] = Source.[FOLQ], Target.[RTI] = Source.[RTI], Target.[ReasonCode] = Source.[ReasonCode]   
WHEN NOT MATCHED BY TARGET
THEN
   INSERT 
   (
		[NUMB], [SEQN], [SKUN], [ORDQ], [RECQ], [ORDP], [BDES], [BSPC], [PRIC], [IBTQ], [RETQ], [POLN], [FOLQ], [RTI], [ReasonCode]
	)
   VALUES 
   (
   		Source.[NUMB], Source.[SEQN], Source.[SKUN], Source.[ORDQ], Source.[RECQ], Source.[ORDP], Source.[BDES], Source.[BSPC], Source.[PRIC], Source.[IBTQ], Source.[RETQ], Source.[POLN], Source.[FOLQ], Source.[RTI], Source.[ReasonCode]
	);	
GO
