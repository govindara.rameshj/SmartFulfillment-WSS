IF OBJECT_ID ('tempdb..#Temp_DLGIFT') IS NOT NULL DROP TABLE #Temp_DLGIFT;
 
CREATE TABLE #Temp_DLGIFT
( 
	[DATE1] [date] NOT NULL, 
	[TILL] [char](2) COLLATE Latin1_General_CI_AS NOT NULL,
	[TRAN] [char](4) COLLATE Latin1_General_CI_AS NOT NULL,
	[LINE] [smallint] NOT NULL,
	[SERI] [char](8) COLLATE Latin1_General_CI_AS NOT NULL,
	[EEID] [char](3) COLLATE Latin1_General_CI_AS NULL,
	[TYPE] [char](2) COLLATE Latin1_General_CI_AS NULL,
	[AMNT] [decimal](9, 2) NULL,
	[COMM] [bit] NOT NULL,
	[RTI] [char](1) COLLATE Latin1_General_CI_AS NULL,
	[SPARE] [char](40) COLLATE Latin1_General_CI_AS NULL
 );

INSERT #Temp_DLGIFT ([DATE1], [TILL], [TRAN], [LINE], [SERI], [EEID], [TYPE], [AMNT], [COMM], [RTI], [SPARE]) VALUES (CAST(0x20340B00 AS Date), N'01', N'7797', 1, N'52862113', N'030', N'TS', CAST(5.00 AS Decimal(9, 2)), 0, N'C', N'                                        ')
INSERT #Temp_DLGIFT ([DATE1], [TILL], [TRAN], [LINE], [SERI], [EEID], [TYPE], [AMNT], [COMM], [RTI], [SPARE]) VALUES (CAST(0x21340B00 AS Date), N'01', N'7992', 1, N'52862120', N'030', N'TS', CAST(5.00 AS Decimal(9, 2)), 0, N'C', N'                                        ')
INSERT #Temp_DLGIFT ([DATE1], [TILL], [TRAN], [LINE], [SERI], [EEID], [TYPE], [AMNT], [COMM], [RTI], [SPARE]) VALUES (CAST(0x21340B00 AS Date), N'01', N'8065', 1, N'05915477', N'030', N'TS', CAST(8.16 AS Decimal(9, 2)), 0, N'C', N'                                        ')
INSERT #Temp_DLGIFT ([DATE1], [TILL], [TRAN], [LINE], [SERI], [EEID], [TYPE], [AMNT], [COMM], [RTI], [SPARE]) VALUES (CAST(0x21340B00 AS Date), N'01', N'8065', 2, N'05857326', N'030', N'TS', CAST(1.87 AS Decimal(9, 2)), 0, N'C', N'                                        ')
INSERT #Temp_DLGIFT ([DATE1], [TILL], [TRAN], [LINE], [SERI], [EEID], [TYPE], [AMNT], [COMM], [RTI], [SPARE]) VALUES (CAST(0x21340B00 AS Date), N'01', N'8065', 3, N'41368695', N'030', N'TS', CAST(30.00 AS Decimal(9, 2)), 0, N'C', N'                                        ')
INSERT #Temp_DLGIFT ([DATE1], [TILL], [TRAN], [LINE], [SERI], [EEID], [TYPE], [AMNT], [COMM], [RTI], [SPARE]) VALUES (CAST(0x21340B00 AS Date), N'01', N'8065', 4, N'41368701', N'030', N'TS', CAST(30.00 AS Decimal(9, 2)), 0, N'C', N'                                        ')
INSERT #Temp_DLGIFT ([DATE1], [TILL], [TRAN], [LINE], [SERI], [EEID], [TYPE], [AMNT], [COMM], [RTI], [SPARE]) VALUES (CAST(0x88340B00 AS Date), N'01', N'7992', 1, N'07665028', N'030', N'TS', CAST(8.29 AS Decimal(9, 2)), 0, N'C', N'                                        ')

MERGE  [dbo].[DLGIFT] AS Target
 USING #Temp_DLGIFT AS Source
	ON Target.[DATE1] = Source.[DATE1]
		AND Target.[TILL] = Source.[TILL]
		AND Target.[TRAN] = Source.[TRAN]	
		AND Target.[LINE] = Source.[LINE]
		AND Target.[SERI] = Source.[SERI]	
WHEN MATCHED THEN
UPDATE SET
 Target.[EEID] = Source.[EEID], Target.[TYPE] = Source.[TYPE], Target.[AMNT] = Source.[AMNT], Target.[COMM] = Source.[COMM], Target.[RTI] = Source.[RTI], Target.[SPARE] = Source.[SPARE]  
WHEN NOT MATCHED BY TARGET
THEN
   INSERT([DATE1], [TILL], [TRAN], [LINE], [SERI], [EEID], [TYPE], [AMNT], [COMM], [RTI], [SPARE])
   VALUES(Source.DATE1, Source.TILL, Source.[TRAN], Source.LINE, Source.SERI, Source.EEID, Source.[TYPE], Source.AMNT, Source.COMM, Source.RTI, Source.SPARE);
GO