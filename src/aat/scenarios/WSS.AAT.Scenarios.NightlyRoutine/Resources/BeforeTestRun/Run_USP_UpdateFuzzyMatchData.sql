Exec usp_UpdateFuzzyMatchData

If @@ERROR = 0
	Print 'Scheduled Script Completed Successfully:  The stored procedure usp_UpdateFuzzyMatchData was run without any errors.'
Else
	Print 'Scheduled Script Failure:  The stored procedure usp_UpdateFuzzyMatchData was NOT run without any errors.'
