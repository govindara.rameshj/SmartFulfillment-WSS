IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_databases WHERE database_id=DB_ID('Oasys'))
BEGIN
    ALTER database [Oasys] SET change_tracking = ON
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PRCCHG'))
BEGIN               
    ALTER TABLE dbo.PRCCHG
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKHIR'))
BEGIN               
    ALTER TABLE dbo.STKHIR
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIESTY'))
BEGIN               
    ALTER TABLE dbo.HIESTY
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIESGP'))
BEGIN               
    ALTER TABLE dbo.HIESGP
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PLANGRAM'))
BEGIN               
    ALTER TABLE dbo.PLANGRAM
    DISABLE CHANGE_TRACKING
END


IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIEGRP'))
BEGIN               
    ALTER TABLE dbo.HIEGRP
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIEMAS'))
BEGIN               
    ALTER TABLE dbo.HIEMAS
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIECAT'))
BEGIN               
    ALTER TABLE dbo.HIECAT
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('GIFHOT'))
BEGIN               
    ALTER TABLE dbo.GIFHOT
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('CARD_LIST'))
BEGIN               
    ALTER TABLE dbo.CARD_LIST
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKMAS'))
BEGIN               
    ALTER TABLE dbo.STKMAS
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('ItemPrompts'))
BEGIN               
    ALTER TABLE dbo.ItemPrompts
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('DRLSUM'))
BEGIN               
    ALTER TABLE dbo.DRLSUM
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('DRLDET'))
BEGIN               
    ALTER TABLE dbo.DRLDET
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PICCTL'))
BEGIN               
    ALTER TABLE dbo.PICCTL
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PURHDR'))
BEGIN               
    ALTER TABLE dbo.PURHDR
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PICAUD'))
BEGIN               
    ALTER TABLE dbo.PICAUD
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('SYSDAT'))
BEGIN               
    ALTER TABLE dbo.SYSDAT
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('SUPMAS'))
BEGIN               
    ALTER TABLE dbo.SUPMAS
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('SUPDET'))
BEGIN               
    ALTER TABLE dbo.SUPDET
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKTXT'))
BEGIN               
    ALTER TABLE dbo.STKTXT
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EANMAS'))
BEGIN               
    ALTER TABLE dbo.EANMAS
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('SYSOPT'))
BEGIN               
    ALTER TABLE dbo.SYSOPT
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('RELITM'))
BEGIN               
    ALTER TABLE dbo.RELITM
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKLOG'))
BEGIN               
    ALTER TABLE dbo.STKLOG
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTMMG'))
BEGIN               
    ALTER TABLE dbo.EVTMMG
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTHDR'))
BEGIN               
    ALTER TABLE dbo.EVTHDR
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTDLG'))
BEGIN               
    ALTER TABLE dbo.EVTDLG
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTHEX'))
BEGIN               
    ALTER TABLE dbo.EVTHEX
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTCHG'))
BEGIN               
    ALTER TABLE dbo.EVTCHG
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTMAS'))
BEGIN               
    ALTER TABLE dbo.EVTMAS
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('COUPONMASTER')) 
BEGIN               
    ALTER TABLE dbo.COUPONMASTER
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('COUPONTEXT'))
BEGIN               
    ALTER TABLE dbo.COUPONTEXT
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('RETOPT'))
BEGIN               
    ALTER TABLE dbo.RETOPT
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('DLTOTS'))
BEGIN               
    ALTER TABLE dbo.DLTOTS
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKADJ'))
BEGIN               
    ALTER TABLE dbo.STKADJ
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('TMPFIL'))
BEGIN               
    ALTER TABLE dbo.TMPFIL
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STRMAS'))
BEGIN               
    ALTER TABLE dbo.STRMAS
    DISABLE CHANGE_TRACKING
END

IF EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('Store'))
BEGIN               
    ALTER TABLE dbo.Store
    DISABLE CHANGE_TRACKING
END

declare @sql sysname
set @sql = 'alter database [' + db_name() + '] set change_tracking = off'
exec(@sql)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

set @sql = 'alter database [' + db_name() + '] set change_tracking = on'
exec(@sql)


IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PRCCHG'))
BEGIN               
    ALTER TABLE dbo.PRCCHG
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKHIR'))
BEGIN               
    ALTER TABLE dbo.STKHIR
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIESTY'))
BEGIN               
    ALTER TABLE dbo.HIESTY
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIESGP'))
BEGIN               
    ALTER TABLE dbo.HIESGP
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIEGRP'))
BEGIN               
    ALTER TABLE dbo.HIEGRP
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIEMAS'))
BEGIN               
    ALTER TABLE dbo.HIEMAS
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('HIECAT'))
BEGIN               
    ALTER TABLE dbo.HIECAT
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('GIFHOT'))
BEGIN               
    ALTER TABLE dbo.GIFHOT
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('CARD_LIST'))
BEGIN               
    ALTER TABLE dbo.CARD_LIST
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKMAS'))
BEGIN               
    ALTER TABLE dbo.STKMAS
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('SUPMAS'))
BEGIN               
    ALTER TABLE dbo.SUPMAS
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('SUPDET'))
BEGIN               
    ALTER TABLE dbo.SUPDET
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKTXT'))
BEGIN               
    ALTER TABLE dbo.STKTXT
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EANMAS'))
BEGIN               
    ALTER TABLE dbo.EANMAS
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('SYSOPT'))
BEGIN               
    ALTER TABLE dbo.SYSOPT
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('RELITM'))
BEGIN               
    ALTER TABLE dbo.RELITM
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKLOG'))
BEGIN               
    ALTER TABLE dbo.STKLOG
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTMMG'))
BEGIN               
    ALTER TABLE dbo.EVTMMG
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTHDR'))
BEGIN               
    ALTER TABLE dbo.EVTHDR
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTDLG'))
BEGIN               
    ALTER TABLE dbo.EVTDLG
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTHEX'))
BEGIN               
    ALTER TABLE dbo.EVTHEX
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTCHG'))
BEGIN               
    ALTER TABLE dbo.EVTCHG
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('EVTMAS'))
BEGIN               
    ALTER TABLE dbo.EVTMAS
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('COUPONMASTER'))
BEGIN               
    ALTER TABLE dbo.COUPONMASTER
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PLANGRAM'))
BEGIN               
    ALTER TABLE dbo.PLANGRAM
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('COUPONTEXT'))
BEGIN           
    ALTER TABLE dbo.COUPONTEXT
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PICCTL'))
BEGIN           
    ALTER TABLE dbo.PICCTL
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PICAUD'))
BEGIN           
    ALTER TABLE dbo.PICAUD
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('SYSDAT'))
BEGIN           
    ALTER TABLE dbo.SYSDAT
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('ItemPrompts'))
BEGIN           
    ALTER TABLE dbo.ItemPrompts
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PLANGRAM'))
BEGIN           
    ALTER TABLE dbo.PLANGRAM
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('DRLSUM'))
BEGIN           
    ALTER TABLE dbo.DRLSUM
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('DRLDET'))
BEGIN           
    ALTER TABLE dbo.DRLDET
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('PURHDR'))
BEGIN           
    ALTER TABLE dbo.PURHDR
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('RETOPT'))
BEGIN           
    ALTER TABLE dbo.RETOPT
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('DLTOTS'))
BEGIN           
    ALTER TABLE dbo.DLTOTS
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STKADJ'))
BEGIN           
    ALTER TABLE dbo.STKADJ
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('TMPFIL'))
BEGIN           
    ALTER TABLE dbo.TMPFIL
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('STRMAS'))
BEGIN           
    ALTER TABLE dbo.STRMAS
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

IF NOT EXISTS (SELECT 1 FROM sys.change_tracking_tables c
                WHERE c.is_track_columns_updated_on = 1 AND c.object_id = object_id('Store'))
BEGIN           
    ALTER TABLE dbo.Store
    ENABLE CHANGE_TRACKING
    WITH (TRACK_COLUMNS_UPDATED = ON)
END

GO