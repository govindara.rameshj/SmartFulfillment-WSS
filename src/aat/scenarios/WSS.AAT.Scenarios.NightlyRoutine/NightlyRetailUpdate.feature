﻿@NightlyRetailUpdate
Feature: NightlyRetailUpdate
    In order to check if nightly retail update tool work correct
    I as a developer
    Create scenarios that test some tricky situations

Scenario: Nightly retail update tool successfully update last sold date in STKMAS based on dlline rows
    Given DateLastSold field in STKMAS for SKU = '500300' is not set
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' in DLTOTS
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '1' in DLLINE
    When Nightly retail update procedure was executed
    Then Last sold date in STKMAS for SKU = '500300' is equal '01-01-2016'

Scenario: Nightly retail update tool update only older date in STKMAS
    Given DateLastSold field in STKMAS for SKU = '500300' is equal '02-01-2016'
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' in DLTOTS
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '1' in DLLINE
    When Nightly retail update procedure was executed
    Then Last sold date in STKMAS for SKU = '500300' is equal '02-01-2016'

Scenario: Nightly retail update tool do NOT ignore Markdown stock items
    Given DateLastSold field in STKMAS for SKU = '500300' is not set
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' in DLTOTS
    And There is markdown row for Date = '01-01-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '1' in DLLINE
    When Nightly retail update procedure was executed
    Then Last sold date in STKMAS for SKU = '500300' is equal '01-01-2016'

Scenario: Nightly retail update tool ignore reversed lines
    Given DateLastSold field in STKMAS for SKU = '500300' is not set
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' in DLTOTS
    And There is reverted row for Date = '01-01-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '1' in DLLINE
    When Nightly retail update procedure was executed
    Then Last sold date in STKMAS for SKU = '500300' is empty

Scenario: Nightly retail update tool ignore refund lines
    Given DateLastSold field in STKMAS for SKU = '500300' is not set
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' in DLTOTS
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '-5' in DLLINE
    When Nightly retail update procedure was executed
    Then Last sold date in STKMAS for SKU = '500300' is empty
