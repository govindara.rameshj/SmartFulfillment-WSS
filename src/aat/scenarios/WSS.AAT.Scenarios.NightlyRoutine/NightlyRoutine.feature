@Nightly @CleanDb
Feature: NightlyRoutine

Scenario: ProcessTransmission execution with HOSTU parameter and ExcludePriceChanges flag in DB is false
    Given Price and event change tables in database are clean
    And We have 'HOSTU' file with name 'FROMRTI\\HOSTU852.01'
    And Current system date is '2014-07-28'
    And ExcludePriceChanges parameter in Parameters table is equal 'false'
    When ProcessTransmission.exe is executed with parameters 'HOSTU'
    Then the following tables have new records inserted or updated: 'STKMAS,EVTMMG,EVTHDR,EVTDLG,EVTHEX,PRCCHG'
    And the following non-empty files should be created with source name 'SAVE\HOSTU01,STHOC,STHOA'

Scenario: ProcessTransmission execution with HOSTU parameter and ExcludePriceChanges flag in DB is true
    Given Price and event change tables in database are clean
    And We have 'HOSTU' file with name 'FROMRTI\\HOSTU852.01'
    And Current system date is '2014-07-28'
    And ExcludePriceChanges parameter in Parameters table is equal 'true'
    When ProcessTransmission.exe is executed with parameters 'HOSTU'
    Then the following tables have new records inserted or updated: 'STKMAS,EVTMMG,EVTHDR,EVTDLG,EVTHEX'
    And the following tables have no changes: 'PRCCHG'
    And the following non-empty files should be created with source name 'SAVE\HOSTU01,STHOC,STHOA'

Scenario: ProcessTransmission execution with PC parameter and ExcludePriceChanges flag in DB is false
    Given Price and event change tables in database are clean
    And ExcludePriceChanges parameter in Parameters table is equal 'false'
    And Current system date is '2010-05-26'
    And Change tracking in the database is turned on
    And There are price change events to process in DB:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-05-27 | 20       | 000105       | 2010-07-29  | 7.50  |
    When ProcessTransmission.exe is executed with parameters 'PC'
    Then the following tables have new records inserted or updated: 'PRCCHG'

Scenario: ProcessTransmission execution with PC parameter and ExcludePriceChanges flag in DB is true
    Given Price and event change tables in database are clean
    And ExcludePriceChanges parameter in Parameters table is equal 'true'
    And Current system date is '2010-05-26'
    And Change tracking in the database is turned on
    And There are price change events to process in DB:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-05-27 | 20       | 000105       | 2010-07-29  | 7.50  |
    When ProcessTransmission.exe is executed with parameters 'PC'
    Then the following tables have new records inserted or updated: 'PRCCHG'

Scenario: ProcessTransmission execution with PC parameter and HOSTU parameter and ExcludePriceChanges flag in DB is false
    Given Price and event change tables in database are clean
    And We have 'HOSTU' file with name 'FROMRTI\\HOSTU852.01'
    And Current system date is '2014-07-28'
    And ExcludePriceChanges parameter in Parameters table is equal 'false'
    When ProcessTransmission.exe is executed with parameters 'HOSTU PC'
    Then the following tables have new records inserted or updated: 'STKMAS,EVTMMG,EVTHDR,EVTDLG,EVTHEX,PRCCHG'
    And the following non-empty files should be created with source name 'SAVE\HOSTU01,STHOC,STHOA'

Scenario: ProcessTransmission execution with PC parameter and HOSTU parameter and ExcludePriceChanges flag in DB is true
    Given Price and event change tables in database are clean
    And We have 'HOSTU' file with name 'FROMRTI\\HOSTU852.01'
    And Current system date is '2014-07-28'
    And ExcludePriceChanges parameter in Parameters table is equal 'true'
    When ProcessTransmission.exe is executed with parameters 'HOSTU PC'
    Then the following tables have new records inserted or updated: 'STKMAS,EVTMMG,EVTHDR,EVTDLG,EVTHEX,PRCCHG'
    And the following non-empty files should be created with source name 'SAVE\HOSTU01,STHOC,STHOA'

Scenario: Process Transmission with MU process
    Given that we have database preparation
    When start step number '105'
    Then the following non-empty files should be created with source name 'STHOA'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with HPSTV file
    Given We have 'HPSTV' file with name 'FROMRTI\\HPSTV852.01'
    When start step number '140'
    Then the following non-empty files should be created with source name 'HPSTV,STHOC,SAVE\HPSTV01,STHPA'
    And the following tables have new records inserted or updated: 'SUPMAS,SUPDET'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with HOSTC file
    Given We have 'HOSTC' file with name 'FROMRTI\\HOSTC852.01'
    When start step number '125'
    Then the following tables have new records inserted or updated: 'PICCTL,SYSDAT'
    And the following non-empty files should be created with source name 'SAVE\HOSTC01,STHOC'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with TIBIPR process
    Given that we have database preparation
    When start step number '135'
    Then the following tables have new records inserted or updated: 'ItemPrompts'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with PO process
    Given that we have database preparation
    When start step number '145'
    Then the following non-empty files should be created with source name 'STHPO'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with HOSTF file
    Given We have 'HOSTF' file with name 'FROMRTI\\HOSTF852.01'
    When start step number '150'
    Then the following tables have new records inserted or updated: 'PLANGRAM'
    And the following non-empty files should be created with source name 'SAVE\HOSTF01,STHOC,STHOA'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with OF process
    Given that we have database preparation
    When start step number '155'
    Then the following non-empty files should be created with source name 'STHOF'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with TIBPFV file
    Given We have 'TIBPFV' file with name 'HPSTV'
    When start step number '139'
    Then the following non-empty files should be created with source name 'STHOC,TEMP\HPSTV.Tmp'
    And the following folder shoud be created: with name 'TEMP\HPSTV.TmpTEMP'
    And the sourse file should be deleted: with name 'HPSTV'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with HPSTO file
    Given We have 'HPSTO' file with name 'FROMRTI\\HPSTO852.01'
    When start step number '160'
    Then the following tables have new records inserted or updated: 'PURHDR'
    And the following non-empty files should be created with source name 'SAVE\HPSTO01,STHOC'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with DR file
    Given that we have database preparation
    When start step number '165'
    Then the following non-empty files should be created with source name 'STHOA'
    And the following tables have new records inserted or updated: 'DRLSUM,DRLDET'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with AD file
    Given that we have database preparation
    When start step number '170'
    Then the following non-empty files should be created with source name 'STHOA'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with SA file
    Given that we have database preparation
    When start step number '175'
    Then the following non-empty files should be created with source name 'STHOA'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with EE file
    Given that we have database preparation
    When start step number '180'
    Then the following non-empty files should be created with source name 'STHOA'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with OT file
    Given that we have database preparation
    When start step number '185'
    Then the following non-empty files should be created with source name 'STHOT'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with OJ file
    Given that we have database preparation
    When start step number '190'
    Then the following non-empty files should be created with source name 'STHOJ'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with OC file
    Given that we have database preparation
    When start step number '195'
    Then the following non-empty files should be created with source name 'PCODE852'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with NIGHT=Y RF file
    Given that we have database preparation
    When start step number '200'
    Then the following non-empty files should be created with source name 'STHPA'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with NIGHT=Y SP file
    Given that we have database preparation
    When start step number '205'
    Then the following non-empty files should be created with source name 'STHPA'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with OL file
    Given that we have database preparation
    And Current system date is '2014-09-17'
    When start step number '210'
    Then the following non-empty files should be created with source name 'STHOL\EI140914.852,STHOL\STHOLIS.CTL,STHOL\STHOLTT.852'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with HOSTA file
    Given We have 'HOSTA' file with name 'HOSTA'
    When start step number '220'
    Then the following non-empty files should be created with source name 'HOSTA.old'
    And the following tables have new records inserted or updated: 'CARD_LIST'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with HOSTG file
    Given We have 'HOSTG' file with name 'FROMRTI/HOSTG852.01'
    When start step number '230'
    Then the following non-empty files should be created with source name 'STHOC,SAVE\HOSTG01'
    And the following tables have new records inserted or updated: 'GIFHOT'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with HOSTH file
    Given that we have 'HOSTH' file with name 'FROMRTI/HOSTH852.01' and run custom 'BeforeTest_CleanTables.sql' script
    When start step number '225'
    Then the following tables have new records inserted or updated: 'HIECAT,HIEMAS,HIEGRP,HIESGP,HIESTY,STKHIR'
    And the following non-empty files should be created with source name 'STHOC,SAVE\HOSTH01'
    And log-file shoud be created: with prefix 'ST'

Scenario: Process Transmission with GSBUHL file
    Given that we have database preparation with custom 'BeforeTest_CleanTables.sql' script
    When start step number '215'
    Then the following tables have new records inserted or updated: 'HIECAT,HIEMAS,HIEGRP,HIESGP,HIESTY'

Scenario: Process Transmission with RI file
    Given that we have database preparation
    When start step number '240'
    Then the following non-empty files should be created with source name 'STHOA'
    
Scenario: Process Transmission with OO file
    Given that we have database preparation
    When start step number '245'
    Then the following non-empty files should be created with source name 'STHOO'

Scenario: Process Transmission with SOCLOS file
    Given We have 'STHOA' file with name 'STHOA'
    When start step number '250'
    Then the following non-empty files should be created: with source name 'TORTI\STHOA852.01' and trailer and header lines include store number '852', current date, file type 'STHOA', next version number '01' and next sequence number '000001' 
    And log-file shoud be created: with prefix 'ST'

Scenario: Roll Dates
    Given that we have database preparation
    When start step number '010'
    Then the following tables have new records inserted or updated: 'SYSDAT,RETOPT'

Scenario: Nightly Retail Updates
    Given that we have database preparation
    When start step number '100'
    Then the following tables have new records inserted or updated: 'DLTOTS,STKMAS,STKADJ,STKLOG'

Scenario: Nightly Stock Calculation
    Given that we have database preparation
    When start step number '110'
    Then the following tables have new records inserted or updated: 'STKMAS'

Scenario: Split Bulk To Single Items
    Given that we have database preparation with custom 'BeforeTest_SplitBulkToSingleItems.sql' script
    When start step number '115'
    Then the following tables have new records inserted or updated: 'STKMAS,STKLOG,RELITM'
    And the following non-empty files should be created with source name 'STHOT'

Scenario: Weekly System Update
    Given that we have database preparation
    When start step number '235'
    Then the following tables have new records inserted or updated: 'STKMAS,RELITM,SYSDAT'

Scenario: Price Apply Changes
    Given that we have database preparation
    When start step number '120'
    Then the following tables have new records inserted or updated: 'PRCCHG,STKMAS,STKLOG'

Scenario: Bar Code Generator
    Given that we have database preparation with custom 'BeforeTest_CleanTables.sql' script
    When start step number '465'
    Then the following tables have new records inserted or updated: 'EANMAS'

Scenario: Refresh Event Enquiry
    Given that we have database preparation
    When start step number '420'
    Then the following tables are not empty: 'EVTENQ'

Scenario: Label Request Purge
    Given that we have database preparation
    When start step number '495'
    Then the following tables have new records inserted or updated: 'TMPFIL'

Scenario: Pic.Build
    Given that we have database preparation
    When start step number '445'
    Then the following tables are not empty: 'HHTHDR,HHTDET,SYSDAT,RETOPT,DRLSUM,DRLDET,STKADJ,STKMAS'

Scenario: Refund List
    When start step number '430'
    Then the report should be created

Scenario: Daily Receiver Listing
    When start step number '440'
    Then the report should be created

Scenario: Colleague Discount Report
    When start step number '475'
    Then the report should be created

Scenario: Miscellaneous Income Report
    When start step number '480'
    Then the report should be created

Scenario: Price Violation Report
    When start step number '490'
    Then the report should be created

Scenario: NightlyTaskSQLScriptExecutor EstateUpdate
    Given that we have database preparation
    When start step number '426'
    Then the following tables have new records inserted or updated: 'STRMAS,Store'	

Scenario: NightlyTaskSQLScriptExecutor Run_USP_UpdateFuzzyMatchData
    Given that we have database preparation with custom 'BeforeTest_NightlyTaskSQLScriptExecutorFuzzy.sql' script
    When start step number '425'
    Then the following tables are not empty: 'SKU,SKUWORD,HIERARCHYWORD'
