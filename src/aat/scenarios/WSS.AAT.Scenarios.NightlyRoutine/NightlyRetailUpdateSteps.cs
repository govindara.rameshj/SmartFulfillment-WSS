using System;
using NightlyRetailUpdates;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.NightlyRoutine
{
    [Binding]
    public class NightlyRetailUpdateSteps : BaseStepDefinitions
    {
        private readonly TestNitmasRepository _nitmasRepository;
        private readonly TestDailyTillRepository _dailyTillRepository;
        private readonly TestStockRepository _stockRepository;
        private readonly NightlyRetailUpdateLogic _nightlyRetailUpdate;

        public NightlyRetailUpdateSteps(IDataLayerFactory dataLayerFactory)
        {
            _nitmasRepository = dataLayerFactory.Create<TestNitmasRepository>();
            _dailyTillRepository = dataLayerFactory.Create<TestDailyTillRepository>();
            _stockRepository = dataLayerFactory.Create<TestStockRepository>();
            _nitmasRepository.CleanDatabaseBeforeTests();
            _nightlyRetailUpdate = new NightlyRetailUpdateLogic(new StubProgressCallback());
        }
        
        [Given(@"DateLastSold field in STKMAS for SKU = '(.*)' is equal '(.*)'")]
        public void GivenDateLastSoldFieldInStkmasForSkuIsEqual(string skuNumber, DateTime dateLastSold)
        {
            _nitmasRepository.SetStkmasLastSoldDate(skuNumber, dateLastSold);
        }

        [Given(@"DateLastSold field in STKMAS for SKU = '(.*)' is not set")]
        public void GivenDateLastSoldFieldInStkmasForSkuIsNotSet(string skuNumber)
        {
            _nitmasRepository.SetStkmasLastSoldDate(skuNumber, null);
        }

        [Given(@"There is row for Date = '(.*)' and TillId = '(.*)' and TranId = '(.*)' in DLTOTS")]
        public void GivenThereIsRowForDateAndTillIdAndTranIdInDltots(DateTime date, string tillId, string tranId)
        {
            _dailyTillRepository.InsertStubDltots(date, tillId, tranId);
        }

        [Given(@"There is row for Date = '(.*)' and TillId = '(.*)' and TranId = '(.*)' and SKU = '(.*)' and Quantity = '(.*)' in DLLINE")]
        public void GivenThereIsRowForDateAndTillIdAndTranIdAndSkuinDlline(DateTime date, string tillId, string tranId, string skuNumber, int quantity)
        {
            _dailyTillRepository.InsertStubDlline(date, tillId, tranId, skuNumber, quantity);
        }

        [Given(@"There is markdown row for Date = '(.*)' and TillId = '(.*)' and TranId = '(.*)' and SKU = '(.*)' and Quantity = '(.*)' in DLLINE")]
        public void GivenThereIsMarkdownRowForDateAndTillIdAndTranIdAndSkuAndQuantityInDlline(DateTime date, string tillId, string tranId, string skuNumber, int quantity)
        {
            _dailyTillRepository.InsertStubDlline(date, tillId, tranId, skuNumber, quantity, isMarkdown:true);
        }

        [Given(@"There is reverted row for Date = '(.*)' and TillId = '(.*)' and TranId = '(.*)' and SKU = '(.*)' and Quantity = '(.*)' in DLLINE")]
        public void GivenThereIsRevertedRowForDateAndTillIdAndTranIdAndSkuAndQuantityInDlline(DateTime date, string tillId, string tranId, string skuNumber, int quantity)
        {
            _dailyTillRepository.InsertStubDlline(date, tillId, tranId, skuNumber, quantity, true);
        }

        [When(@"Nightly retail update procedure was executed")]
        public void WhenNightlyRetailUpdateProcedureWasExecuted()
        {
            _nightlyRetailUpdate.RunNightlyRetailUpdates();
        }

        [Then(@"Last sold date in STKMAS for SKU = '(.*)' is equal '(.*)'")]
        public void ThenLastSoldDateInStkmasForSkuIsEqual(string skuNumber, DateTime lastSoldDate)
        {
            var stkmas = _stockRepository.GetStockMaster(skuNumber);

            Assert.IsNotNull(stkmas);
            Assert.AreEqual(lastSoldDate, stkmas.DateLastSold);
        }

        [Then(@"Last sold date in STKMAS for SKU = '(.*)' is empty")]
        public void ThenLastSoldDateInStkmasForSkuIsEmpty(string skuNumber)
        {
            var stkmas = _stockRepository.GetStockMaster(skuNumber);

            Assert.IsNotNull(stkmas);
            Assert.IsNull(stkmas.DateLastSold);
        }
    }
}
