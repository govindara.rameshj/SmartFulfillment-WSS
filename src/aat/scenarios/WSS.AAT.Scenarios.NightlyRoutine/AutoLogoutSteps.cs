﻿using System.Windows.Forms;
using Cts.Oasys.WinForm;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Bindings;

namespace WSS.AAT.Scenarios.NightlyRoutine
{
    [Binding]
    public class AutoLogoutSteps : BaseStepDefinitions
    {
        private bool _loggedOut;
        private HostForm _hostForm;

        [Given(@"Nightly routine process is executing")]
        public void GivenNightlyRoutineProcessIsExecuting()
        {
            OpenRoutineForm();
        }

        [When(@"Nightly routine process has successfully finished")]
        public void WhenNightlyRoutineProcessHasSuccessfullyFinished()
        {
            EmulateSuccessCompletion();
        }

        [When(@"Nightly routine process failed")]
        public void WhenNightlyRoutineProcessFailed()
        {
            EmulateUnsuccessCompletion();
        }

        [Then(@"User was automatically logged out from the system")]
        public void ThenUserWasAutomaticallyLoggedOutFromTheSystem()
        {
            Assert.IsTrue(_loggedOut);
        }

        [Then(@"User was not logged out from the system")]
        public void ThenUserWasNotLoggedOutFromTheSystem()
        {
            Assert.IsFalse(_loggedOut);
        }

        public void OpenRoutineForm()
        {
            _hostForm = new HostForm();
            _hostForm.FormClosed += _hostForm_FormClosed;
            _hostForm.Show();
        }

        private void _hostForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _loggedOut = _hostForm.LogOutUserAfterClose;
        }

        public void EmulateSuccessCompletion()
        {
            _hostForm.LogOutUserAfterClose = true;
            CloseForm();
        }

        public void EmulateUnsuccessCompletion()
        {
            CloseForm();
        }

        private void CloseForm()
        {
            if (_hostForm == null)
                return;

            _hostForm.Close();
        }
    }
}
