using System.Collections.Generic;
using System.Data;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using Cts.Oasys.Core.SystemEnvironment;
using OpenClose;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.AAT.Scenarios.NightlyRoutine.Utility
{
    public class TaskExecuter
    {
        private readonly frmOpenClose executer;
        private readonly IList<NightlyProcedure> tasks;
        private readonly ISystemEnvironment systemEnvironment;
        private readonly string scriptDir;

        public TaskExecuter(string scriptDir, IList<NightlyProcedure> tasks, ISystemEnvironment systemEnvironment)
        {
            this.tasks = tasks;
            this.systemEnvironment = systemEnvironment;
            executer = new frmOpenClose(1, 1, 999, "close");
            this.scriptDir = scriptDir;
        }

        public void RunTask(string number)
        {
            NightlyProcedure task = tasks.First(row => row.TaskNumber == number);
            AdjustTask(task);
            executer.RunTask(ToDataRow(task), true);
        }

        public void RunTask(NightlyProcedure task)
        {
            executer.RunTask(ToDataRow(task), true);
        }

        private DataRow ToDataRow(NightlyProcedure task)
        {
            DataRow row = new DataTable().NewRow();
            Mapper.ObjectToDataRow(task, row);
            return row;
        }

        private void AdjustTask(NightlyProcedure task)
        {
            string ajustedParameters = task.ProgramToRun.Replace(@"F:\WIX\Scripts", scriptDir);
            ajustedParameters = ajustedParameters.Replace(@"Data Source=SRV8052;Initial Catalog=OASYS;Integrated Security=True", systemEnvironment.GetOasysSqlServerConnectionString());
            if (task.TaskNumber == NitmasTaskId.RollDates)
            {
                ajustedParameters = ajustedParameters + "| /Y ";
            }
            task.ProgramToRun = ajustedParameters;
        }
    }
}
