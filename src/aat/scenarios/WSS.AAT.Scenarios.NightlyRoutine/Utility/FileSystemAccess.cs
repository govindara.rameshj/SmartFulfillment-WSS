using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace WSS.AAT.Scenarios.NightlyRoutine.Utility
{
    public class FileSystemAccess
    {
        private static int NUBMER_OF_ATTEMPT_TO_ACT_WITH_FILE_SYSTEM = 30;

        private string resourcesDir;
        private string beforeTestRunScriptDir;
        private readonly string commsDir;
        private readonly string reportFilePath;

        public string BeforeTestRunScriptDir
        {
            get { return this.beforeTestRunScriptDir; }
        }

        public FileSystemAccess(string commsDir, string reportFilePath)
        {
            this.resourcesDir = Path.GetFullPath("Resources");
            this.beforeTestRunScriptDir = Path.Combine(resourcesDir, "BeforeTestRun");
            this.commsDir = commsDir;
            this.reportFilePath = reportFilePath;
        }

        public void CopyToCommsIfNotExist(string fileName)
        {
            string filePath = GetCommsFilePath(fileName);
            if (!FileExists(filePath))
            {
                string directoryPath = Path.GetDirectoryName(filePath);
                Directory.CreateDirectory(directoryPath);
                File.Copy(GetResourceFilePath(fileName), filePath);
            }
        }

        public bool WasFileProcessedCorrectly(string filePath)
        {
            if (File.Exists(filePath))
            {
                FileInfo file = new FileInfo(filePath);
                return file.Length != 0;
            }

            DeleteFile(filePath);

            return false;
        }

        public void WaitWhileReportFileIsCreated()
        {
            ExecuteRepeatable(() => File.Exists(reportFilePath));
        }

        public bool WasReportProcessedCorrectly(){
            return WasFileProcessedCorrectly(reportFilePath);
        }   
        
        public void DeleteFile(string filePath)
        {
            ExecuteRepeatable(() => TryToDelete(filePath));
        }

        public bool FileExists(string filePath)
        {
            return File.Exists(filePath);
        }

        public string[] GetBeforeTestsRunScripts()
        {
            string[] pathToScripts = Directory.GetFiles(GetResourceFilePath("BeforeTestsRun"));
            List<string> contents = new List<string>();
            foreach (string path in pathToScripts)
            {
                contents.Add(GetContent(path));
            }

            return contents.ToArray();
        }

        public string GetContent(string pathToFile)
        {
            return File.ReadAllText(GetFullPathToScript(pathToFile));
        }

        public bool DirectoryExists(string folderPath)
        {
            return Directory.Exists(folderPath);
        }

        public bool IsConstainsInFirstLine(string pathToFile, string pattern)
        {
            return File.ReadLines(pathToFile).First().Contains(pattern);
        }

        public bool IsConstainsInLastLine(string pathToFile, string pattern)
        {
            return File.ReadLines(pathToFile).Last().Contains(pattern);
        }
        
        public string GetLogFilePath(string prefix)
        {
            string dateSuffix = DateTime.Now.Date.ToString("yyMMdd");
            string fileName = string.Format("{0}{1}.LOG", prefix, dateSuffix);
            return GetCommsFilePath(fileName);
        }
        
        private string GetFullPathToScript(string scriptName)
        {
            return Path.Combine(beforeTestRunScriptDir, scriptName);
        } 

        private void ExecuteRepeatable(Func<Boolean> action)
        {
            bool isDone = false;
            int counter = 0;
            do
            {
                isDone = action();
                if (!isDone)
                {
                    Thread.Sleep(1000);
                    counter++;
                }
            } while (!isDone && counter != NUBMER_OF_ATTEMPT_TO_ACT_WITH_FILE_SYSTEM);
        }

        private bool TryToDelete(string filePath)
        {
            try
            {
                File.Delete(filePath);
                return true;
            }
            catch (IOException)
            {
                return false;
            }
        }

        public string GetResourceFilePath(string fileName)
        {
            return Path.Combine(resourcesDir, fileName);
        }

        public string GetCommsFilePath(string fileName)
        {
            return Path.Combine(commsDir, fileName);
        }
    }
}
