﻿using System;

namespace WSS.AAT.Scenarios.NightlyRoutine.Utility
{
    public class FileValidator
    {
        private FileSystemAccess fileSystemAccess;

        public FileValidator(FileSystemAccess fileSystemAccess)
        {
            this.fileSystemAccess = fileSystemAccess;
        }

        public bool HasRightTrailerAndHeader(string pathToFile, string storeNumber, string fileType, string nextVersionNumber, string nextSequenceNumber)
        {
            string pattern = CreatePattern(storeNumber, fileType, nextVersionNumber, nextSequenceNumber);
            bool fileWasParsed = fileSystemAccess.IsConstainsInFirstLine(pathToFile, pattern)
                && fileSystemAccess.IsConstainsInLastLine(pathToFile, pattern);
            fileSystemAccess.DeleteFile(pathToFile);
            return fileWasParsed;
        }

        private string CreatePattern(string storeNumber, string fileType, string nextVersionNumber, string nextSequenceNumber)
        {
            return string.Format("{0}{1:dd/MM/yy}{2}{3}{4}", storeNumber, DateTime.Now.Date, fileType, nextVersionNumber, nextSequenceNumber);
        }
    }
}
