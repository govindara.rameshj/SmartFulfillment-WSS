using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Cts.Oasys.Core.SystemEnvironment;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.AAT.Common.Utility.Utility;
using WSS.AAT.Scenarios.NightlyRoutine.Utility;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.AAT.Scenarios.NightlyRoutine
{
    [Binding]
    public class NightlyRoutineSteps : BaseStepDefinitions
    {
        private readonly TaskExecuter taskExecuter;
        private readonly FileValidator fileValidator;
        private readonly IDataLayerFactory dataLayerFactory;
        private readonly FileSystemAccess fileSystemAccess;
        private readonly TablesRepository tablesRepo;

        public NightlyRoutineSteps(IDataLayerFactory dataLayerFactory, FileSystemAccess fileSystemAccess, ISystemEnvironment systemEnvironment)
        {
            this.dataLayerFactory = dataLayerFactory;
            this.fileSystemAccess = fileSystemAccess;

            fileValidator = new FileValidator(fileSystemAccess);

            var repo = dataLayerFactory.Create<TestNitmasRepository>();
            var tasks = repo.GetAllNightlyProcedures();
            taskExecuter = new TaskExecuter(fileSystemAccess.BeforeTestRunScriptDir, tasks, systemEnvironment);
            tablesRepo = dataLayerFactory.Create<TablesRepository>();
        }

        [Given(@"that we have database preparation")]
        public void GivenThatWeHaveDatabasePreparation()
        {
            var repo = dataLayerFactory.Create<RawSqlRepository>();
            repo.ExecuteScript(fileSystemAccess.GetContent("PrepareDB.sql"));
            repo.ExecuteScript(fileSystemAccess.GetContent("EnableTracking.sql"));
            repo.ExecuteScript(fileSystemAccess.GetContent("UpdateTVCSTR.sql"));
        }

        [Given(@"Change tracking in the database is turned on")]
        public void GivenChangeTrackingIsTurnedOn()
        {
            GivenThatWeHaveDatabasePreparation();
        }

        [Given(@"Price and event change tables in database are clean")]
        public void GivenPriceChangeTablesAreClean()
        {
            tablesRepo.DeleteFromEventChangeTable();
            tablesRepo.DeleteFromPriceChangeTable();
        }

        [Given(@"that we have database preparation with custom '(.*)' script")]
        public void GivenThatWeHaveDatabasePreparationWithCustomScript(string scriptName)
        {
            var repo = dataLayerFactory.Create<RawSqlRepository>();
            repo.ExecuteScript(fileSystemAccess.GetContent(scriptName));
            GivenThatWeHaveDatabasePreparation();
        }

        [Given(@"that we have '(.*)' file with name '(.*)' and run custom '(.*)' script")]
        public void GivenThatWeHaveFileWithNameAndRunCustomScript(string fileType, string fileName, string scriptName)
        {
            fileSystemAccess.CopyToCommsIfNotExist(fileName);
            GivenThatWeHaveDatabasePreparationWithCustomScript(scriptName);
        }

        [Given(@"We have '(.*)' file with name '(.*)'")]
        public void GivenThatWeHaveFileWithName(string fileType, string fileName)
        {
            fileSystemAccess.CopyToCommsIfNotExist(fileName);
            GivenThatWeHaveDatabasePreparation();
        }

        [Given(@"Current system date is '(.*)'")]
        public void GivenCurrentSystemDateIs(DateTime currentDate)
        {
            tablesRepo.UpdateSystemDateTable(currentDate);
        }

        [Given(@"ExcludePriceChanges parameter in Parameters table is equal '(.*)'")]
        public void GivenExcludePriceChangesParameterInParametersTableIsEqual(bool excludePriceChangesParam)
        {
            var repo = dataLayerFactory.Create<TablesRepository>();
            repo.UpdateBoolParameterValue(excludePriceChangesParam, 990099);
        }

        [Given(@"There are price change events to process in DB:")]
        public void GivenTheFollowingPriceEventsWereUploaded(Table table)
        {
            var wrappedTable = new SpecflowTableWrapper<EventChange>(table);
            var eventChange = wrappedTable.GetEntities();
            tablesRepo.InsertEventChangeTable(eventChange);
        }

        [When(@"ProcessTransmission\.exe is executed with parameters '(.*)'")]
        public void WhenProcessTransmission_ExeIsExecutedWithParameters(string parameters)
        {
            var nitmas = new NightlyProcedure()
            {
                ProgramToRun = "ProcessTransmissions.exe| " + parameters
            };
            taskExecuter.RunTask(nitmas);
        }

        [When(@"start step number '(.*)'")]
        public void WhenStartStepNumber(string number)
        {
            taskExecuter.RunTask(number);
        }

        [When(@"execute '(.*)'")]
        public void WhenExecute(string appName)
        {
            Process.Start(appName);
        }

        [Then(@"the following tables have new records inserted or updated: '(.*)'")]
        public void ThenTheFollowingTablesHaveNewRecordsInsertedOrUpdated(string tables)
        {
            var repo = dataLayerFactory.Create<ChangesTrackingRepository>();
            List<string> tablesList = tables.Split(',').ToList();
            foreach (string table in tablesList)
            {
                Assert.That(repo.GetNumberOfChanges(table), Is.GreaterThan(0), table);
            }
        }

        [Then(@"the following non-empty files should be created with source name '(.*)'")]
        public void ThenTheFollowingNon_EmptyFilesShouldBeCreatedWithSourceName(string files)
        {
            List<string> filesList = files.Split(',').ToList();
            foreach (string file in filesList)
            {
                var filePath = fileSystemAccess.GetCommsFilePath(file);
                bool ok = fileSystemAccess.WasFileProcessedCorrectly(filePath);
                if (!ok)
                {
                    Assert.Fail("File {0} does not exist or has zero size.", filePath);
                }
            }
        }

        [Then(@"the report should be created")]
        public void ThenTheFollowingNon_ReportShouldBeCreated()
        {
            fileSystemAccess.WaitWhileReportFileIsCreated();
            Assert.True(fileSystemAccess.WasReportProcessedCorrectly());
        }

        [Then(@"log-file shoud be created: with prefix '(.*)'")]
        public void ThenLog_FileShoudBeCreatedWithPrefix(string prefix)
        {
            string logFilePath = fileSystemAccess.GetLogFilePath(prefix);
            Assert.True(fileSystemAccess.FileExists(logFilePath));
            fileSystemAccess.DeleteFile(logFilePath);
        }

        [Then(@"the following folder shoud be created: with name '(.*)'")]
        public void ThenTheFollowingFolderShoudBeCreatedWithName(string folderName)
        {
            string folderPath = fileSystemAccess.GetCommsFilePath(folderName);
            Assert.True(fileSystemAccess.DirectoryExists(folderPath));
        }

        [Then(@"the sourse file should be deleted: with name '(.*)'")]
        public void ThenTheSourseFileShouldBeDeletedWithName(string fileName)
        {
            fileSystemAccess.DeleteFile(fileSystemAccess.GetCommsFilePath(fileName));
        }

        [Then(@"the following non-empty files should be created: with source name '(.*)' and trailer and header lines include store number '(.*)', current date, file type '(.*)', next version number '(.*)' and next sequence number '(.*)'")]
        public void ThenTheFollowingNon_EmptyFilesShouldBeCreatedWithSourceNameWithSpecialFormat(string fileName, string storeNumber, string fileType, string nextVersionNumber, string nextSequenceNumber)
        {
            bool fileHasRightTrailerAndHeader;

            string filePath = fileSystemAccess.GetCommsFilePath(fileName);

            if (fileSystemAccess.FileExists(filePath))
            {
                fileHasRightTrailerAndHeader = fileValidator.HasRightTrailerAndHeader(
                    filePath, 
                    storeNumber, fileType, nextVersionNumber, nextSequenceNumber);
            }
            else
            {
                fileHasRightTrailerAndHeader = false;
            }

            Assert.True(fileHasRightTrailerAndHeader);
        }

        [Then(@"the following tables are not empty: '(.*)'")]
        public void ThenTheFollowingTablesAreNotEmpty(string tables)
        {
            var repo = dataLayerFactory.Create<ChangesTrackingRepository>();
            List<string> tablesList = tables.Split(',').ToList();
            foreach (string table in tablesList)
            {
                int count = repo.GetTableRowsCount(table);
                Assert.That(count, Is.GreaterThan(0), table);
            }
        }

        [Then(@"the following tables have no changes: '(.*)'")]
        public void ThenTheFollowingTablesHaveNoChanges(string tables)
        {
            var repo = dataLayerFactory.Create<ChangesTrackingRepository>();
            List<string> tablesList = tables.Split(',').ToList();
            foreach (string table in tablesList)
            {
                Assert.That(repo.GetNumberOfChanges(table), Is.EqualTo(0), table);
            }
        }
    }
}
