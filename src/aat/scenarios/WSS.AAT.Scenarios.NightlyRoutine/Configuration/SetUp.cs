using System;
using BoDi;
using Cts.Oasys.Core.Tests;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Configuration;
using WSS.AAT.Scenarios.NightlyRoutine.Utility;

namespace WSS.AAT.Scenarios.NightlyRoutine
{
    class SetUp : BaseSetUp
    {
        public FileSystemAccess FileSystemAccess { get; private set; }

        public void OnBeforeFeature()
        {
            FileSystemAccess = new FileSystemAccess(
                TestConfiguration.ResolvePathFromAppSettings("commsDir"),
                TestConfiguration.ResolvePathFromAppSettings("reportFilePath"));

            Environment.CurrentDirectory = TestConfiguration.ResolvePathFromAppSettings("executablesDir");
        }
        
        public void OnBeforeFeatureWithDbPreparation()
        {
            var repo = DataLayerFactory.Create<RawSqlRepository>();
            foreach (string sqlScript in FileSystemAccess.GetBeforeTestsRunScripts())
            {
                repo.ExecuteScript(sqlScript);
            }
        }

        protected override void InitializeObjectContainer(IObjectContainer container)
        {
            base.InitializeObjectContainer(container);
            container.RegisterInstanceAs(FileSystemAccess);
        }

        protected override void InitializeTestEnvironmentSettings(TestEnvironmentSettings settings)
        {
            base.InitializeTestEnvironmentSettings(settings);
            settings.CommsDirectoryPath = TestConfiguration.ResolvePathFromAppSettings("commsDir");
        }
    }
}
