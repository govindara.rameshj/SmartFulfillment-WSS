using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Configuration;
using NUnit.Framework;

namespace WSS.AAT.Scenarios.NightlyRoutine
{
    [Binding]
    [SetUpFixture]
    class SetUpBinding : BaseSetUpBinding<SetUp>
    {
        [SetUp]
        public static void BeforeTestRun()
        {
            DoBeforeTestRun();
        }

        [BeforeScenario]
        public static void BeforeScenario()
        {
            DoBeforeScenario();
        }

        [BeforeFeature]
        public static void BeforeFeature()
        {
            Instance.OnBeforeFeature();
        }

        [BeforeFeature("CleanDb")]
        public static void BeforeFeatureWithDbPreparation()
        {
            Instance.OnBeforeFeatureWithDbPreparation();
        }
    }
}
