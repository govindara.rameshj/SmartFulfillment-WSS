﻿@AutoLogout
Feature: AutoLogout
As a security engineer
I want the user to be automatically logged out from the system at the end of the working day
so that there's no security breach

Scenario: Get auto logout successful routine 
    Given Nightly routine process is executing
    When Nightly routine process has successfully finished
    Then User was automatically logged out from the system

Scenario: Get auto logout unsuccessful routine
    Given Nightly routine process is executing
    When Nightly routine process failed
    Then User was not logged out from the system