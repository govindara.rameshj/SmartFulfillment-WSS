using System;
using Cts.Oasys.Core.Tests;
using TechTalk.SpecFlow;
using NUnit.Framework;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.AAT.Scenarios.DayOfChoice
{
    [Binding]
    [Scope(Feature = "New delivery order consignment is fulfilled in store")]
    class NewDeliveryOrderConsignmentIsFulfilledInStoreSteps : BaseSteps
    {
        private DayOfWeek _deliveryDay;

        public NewDeliveryOrderConsignmentIsFulfilledInStoreSteps(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
        }

        [Given(@"delivery slot is set")]
        public void GivenDeliverySlotIsSet()
        {
            AddSlotToFulfillmentRequest();
        }

        [Given(@"fulfillment slot is not set")]
        public void GivenFulfillmentSlotIsNotSet()
        {
            // not set by default
        }

        [Given(@"delivery date is (.*)")]
        public void GivenDeliveryDateIs(DayOfWeek day)
        {
            FulfillmentRoutine.CurrentRequest.OrderHeader.RequiredDeliveryDate.Value = GetWeekDayAfterDate(day,
                FulfillmentRoutine.CurrentRequest.OrderHeader.RequiredDeliveryDate.Value);

            _deliveryDay = day;
        }

        [Then(@"ovc reference is saved to DB")]
        public void ThenOvcReferenceIsSavedToDb()
        {
            var order = Order.Value;
            Assert.That(order.Source, Is.EqualTo("OV"));
            Assert.That(order.SourceOrderNumber, Is.EqualTo(FulfillmentRoutine.CurrentResponse.OrderHeader.TillOrderReference.Value));
        }

        [Then(@"delivery slot is saved to DB")]
        public void ThenDeliverySlotIsSavedToDb()
        {
            var slot = FulfillmentRoutine.CurrentRequest.OrderHeader.RequiredFulfilmentSlot;
            AssertThatSlotIsSavedToDb(slot.Id, slot.Description, slot.StartTime.TimeOfDay, slot.EndTime.TimeOfDay);
        }

        [Then(@"corresponding slot is saved to DB")]
        public void ThenCorrespondingSlotIsSavedToDb()
        {
            var slotId = (_deliveryDay == DayOfWeek.Saturday) ? DeliverySlotType.Saturday : DeliverySlotType.Weekday;
            var slot = GetDeliverySlot(slotId);
            AssertThatSlotIsSavedToDb(slot.Id, slot.Description, slot.StartTime, slot.EndTime);
        }

    }
}
