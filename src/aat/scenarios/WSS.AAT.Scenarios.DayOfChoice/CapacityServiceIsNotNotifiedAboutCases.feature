﻿@DoC
Feature: Capacity service is not notified about wickes hourly service orders
    As a store colleague
    I want capacity service not to receive any notifications for wickes hourly service orders
    So that capacity service does not have irrelevant information

Scenario: No notifications for Wickes Hourly Service order
    When new wickes hourly service order is created and processed by fulfilling store
    Then Allocate update capacity request is not created

Scenario: No notifications for Wickes Hourly Service order cancellation
    When new wickes hourly service order is created and processed by fulfilling store
     And wickes hourly service order is fully cancelled
    Then Deallocate update capacity request is not created