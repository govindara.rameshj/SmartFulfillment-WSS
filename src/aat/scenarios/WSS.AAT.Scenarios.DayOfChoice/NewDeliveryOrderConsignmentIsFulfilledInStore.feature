﻿@DoC
Feature: New delivery order consignment is fulfilled in store
	In order to provide DoC functionality
    As a developer
    I want to save delivery slot, fulfiller and OVC reference from Order Manager

Scenario: Ovc reference is saved
    Given fulfillment consignment for new delivery order from Ovc Till is requested in the store
	When fulfillment request is processed
    Then ovc reference is saved to DB

Scenario: Delivery slot is saved for Ovc Till order
    Given fulfillment consignment for new delivery order from Ovc Till is requested in the store
	When fulfillment request is processed
    Then delivery slot is saved to DB

Scenario: Delivery slot is saved for Cts Till order
    Given fulfillment consignment for new delivery order from Cts Till is requested in the store
	And delivery slot is set
	When fulfillment request is processed
    Then delivery slot is saved to DB

Scenario: Default delivery slot is saved for Saturday Cts Till order
    Given fulfillment consignment for new delivery order from Cts Till is requested in the store
	And fulfillment slot is not set
	And delivery date is Saturday
	When fulfillment request is processed
    Then corresponding slot is saved to DB

Scenario: Default delivery slot is saved for weekday Cts Till order
    Given fulfillment consignment for new delivery order from Cts Till is requested in the store
	And fulfillment slot is not set
	And delivery date is Tuesday
	When fulfillment request is processed
    Then corresponding slot is saved to DB