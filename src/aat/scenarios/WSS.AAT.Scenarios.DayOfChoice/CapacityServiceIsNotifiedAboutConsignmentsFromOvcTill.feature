﻿@DoC
Feature: Capacity service is notified about consignments from Ovc Till
    As a store colleague
    I want the capacity service to be notified about new consignments from Ovc Till and their changes
    So that capacity service has up-to-date information

Scenario: Store receives new consignment
	Given fulfillment consignment for new delivery order from Ovc Till is requested in the store
	When fulfillment request is processed
	Then Allocate update capacity request for specifeid slot is created

Scenario: Store updates customer contact details
	Given delivery order consignment from Ovc Till is fulfilled in the store
	When customer contact details are updated
	Then AllocateDeallocate update capacity request is not created

Scenario: Store updates delivery date
	Given delivery order consignment from Ovc Till is fulfilled in the store
	When delivery date is updated
	Then AllocateDeallocate update capacity request for specifeid slot is created

Scenario: Store updates delivery slot
	Given delivery order consignment from Ovc Till is fulfilled in the store
	When delivery slot is updated
	Then AllocateDeallocate update capacity request for specifeid slot is created

Scenario: Store receives partial order cancellation consignment
	Given delivery order consignment from Ovc Till is fulfilled in the store
	And partial refund is requested
	When refund request is processed
	Then Deallocate update capacity request is not created

Scenario: Store receives full order cancellation consignment
	Given delivery order consignment from Ovc Till is fulfilled in the store
	And full refund is requested
	When refund request is processed
	Then Deallocate update capacity request for specifeid slot is created