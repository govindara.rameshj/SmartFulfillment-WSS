using BoDi;
using Cts.Oasys.Core.Tests;
using WSS.AAT.Common.PosIntegration.Configuration;
using WSS.AAT.Common.Utility.Configuration;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.Common.Hosting;

namespace WSS.AAT.Scenarios.DayOfChoice
{
    class SetUp : BaseSetUp
    {
        protected override void InitializeTestEnvironmentSettings(TestEnvironmentSettings settings)
        {
            base.InitializeTestEnvironmentSettings(settings);
            settings.XsdFilesDirectoryPath = TestConfiguration.ResolvePathFromAppSettings("xsdFilesDir");
        }

        protected override IDependencyInjectionConfig GetDependencyInjectionConfig()
        {
            return new DependencyInjectionConfig();
        }

        protected override void InitializeObjectContainer(IObjectContainer container)
        {
            base.InitializeObjectContainer(container);
            BindObjectContainerTypeFromKernel<IBusinessLogicFacade>(container);
        }
    }
}
