using System;
using System.Linq;
using Cts.Oasys.Core.Tests;
using TechTalk.SpecFlow;
using WSS.BO.DataLayer.Model;
using WSS.AAT.Common.Qod.CtsOrderManager;
using Cts.Oasys.Hubs.Core.Order.WebService;
using Cts.Oasys.Hubs.Core.Order.Qod;
using WSS.AAT.Common.Qod;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using NUnit.Framework;
using WSS.AAT.Common.Utility.Bindings;

namespace WSS.AAT.Scenarios.DayOfChoice
{
    [Binding]
    internal class BaseSteps : BaseStepDefinitions
    {
        protected Lazy<QodHeader> Order { get; private set; }

        protected string OrderNumber { get; set; }
        protected string OmOrderNumber { get; private set; }
        protected string SourceOrderNumber { get; private set; }
        protected DateTime StartDateTime { get; private set; }

        protected IDataLayerFactory DataLayerFactory { get; private set; }
        protected FulfillmentRoutine FulfillmentRoutine { get; private set; }
        protected UpdateRefundRoutine UpdateRefundRoutine { get; private set; }
        protected CreationRoutine CreationRoutine { get; private set; }
        protected EnableRefundCreationRoutine EnableRefundRoutine { get; private set; }

        private QodHeader LoadOrder()
        {
            return QodOrder.Get(OrderNumber).Header;
        }

        public BaseSteps(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
        {
            DataLayerFactory = dataLayerFactory;
            ResetOrderObject();

            OrderNumber = null;
            OmOrderNumber = null;
            SourceOrderNumber = null;
            StartDateTime = DateTime.Now;

            FulfillmentRoutine = new FulfillmentRoutine(DataLayerFactory, systemEnvironment);
            UpdateRefundRoutine = new UpdateRefundRoutine(DataLayerFactory, systemEnvironment);
            CreationRoutine = new CreationRoutine(DataLayerFactory, systemEnvironment);
            EnableRefundRoutine = new EnableRefundCreationRoutine(DataLayerFactory, systemEnvironment); 
        }

        [Given(@"fulfillment consignment for new delivery order from Cts Till is requested in the store")]
        public void GivenFulfillmentConsignmentForNewDeliveryOrderFromCtsTillIsRequestedInTheStore()
        {
            PrepareFulfillmentRequestForStoreDeliveryOrder();
        }

        [Given(@"fulfillment consignment for new delivery order from Ovc Till is requested in the store")]
        public void GivenFulfillmentConsignmentForNewDeliveryOrderFromOvcTillIsRequestedInTheStore()
        {
            PrepareFulfillmentRequestForOvcDeliveryOrder();
        }

        [When(@"fulfillment request is processed")]
        public void WhenFulfillmentRequestIsProcessed()
        {
            ProcessPreparedFulfillmentRequest();
        }

        [Then(@"order is not processed")]
        public void ThenOrderIsNotProcessed()
        {
            CheckThatRequestWasNotSuccessfull(FulfillmentRoutine);
        }

        [Then(@"refund is not processed")]
        public void ThenRefundIsNotProcessed()
        {
            CheckThatRequestWasNotSuccessfull(UpdateRefundRoutine);
        }

        private void CheckThatRequestWasNotSuccessfull(IOrderRoutine routine)
        {
            var response = routine.GetResponse();
            Assert.That(response != null && response.IsSuccessful(), Is.False);
        }

        protected void ResetOrderObject()
        {
            Order = new Lazy<QodHeader>(LoadOrder);
        }

        protected void PrepareFulfillmentRequestForOvcDeliveryOrder()
        {
            FulfillmentRoutine.CreateRequestOrderHeader();
            FulfillmentRoutine.CurrentRequest.OrderHeader.RequiredDeliveryDate.Value = GetWeekDayAfterDate(DayOfWeek.Monday, DateTime.Today);
            FulfillmentRoutine.AddOrderLine("100005", "Heavy Duty Staple Gun (FOB)", 3, 2.2M);

            FulfillmentRoutine.CurrentRequest.OrderHeader.TillOrderReference = FulfillmentRoutine.CurrentRequest.OrderHeader.SourceOrderNumber;
            FulfillmentRoutine.CurrentRequest.OrderHeader.SourceOrderNumber = null;
            FulfillmentRoutine.CurrentRequest.OrderHeader.Source = null;

            AddSlotToFulfillmentRequest();
        }

        protected void PrepareFulfillmentRequestForStoreDeliveryOrder()
        {
            FulfillmentRoutine.CreateRequestOrderHeader();
            FulfillmentRoutine.CurrentRequest.OrderHeader.RequiredDeliveryDate.Value = GetWeekDayAfterDate(DayOfWeek.Monday, DateTime.Today);
            FulfillmentRoutine.AddOrderLine("100005", "Heavy Duty Staple Gun (FOB)", 3, 2.2M);
            FulfillmentRoutine.CurrentRequest.OrderHeader.Source = string.Empty;
        }

        protected void ProcessPreparedFulfillmentRequest()
        {
            FulfillmentRoutine.ProcessPreparedRequest();
            OmOrderNumber = FulfillmentRoutine.CurrentResponse.OrderHeader.OMOrderNumber.Value;
            OrderNumber = FulfillmentRoutine.CurrentResponse.FulfillingSiteOrderNumber.Value;
        }

        protected void AddSlotToFulfillmentRequest()
        {
            var slot = new CTSFulfilmentRequestOrderHeaderRequiredFulfilmentSlot()
            {
                Id = "2",
                Description = "AM slot",
                StartTime = new DateTime(1, 1, 1, 8, 0, 0),
                EndTime = new DateTime(1, 1, 1, 13, 0, 0)
            };
            FulfillmentRoutine.CurrentRequest.OrderHeader.RequiredFulfilmentSlot = slot;
        }

        protected DateTime GetWeekDayAfterDate(DayOfWeek day, DateTime date)
        {
            var result = date;
            while (result.DayOfWeek != day)
            {
                result = result.AddDays(1);
            }
            return result;
        }

        protected DeliverySlot GetDeliverySlot(string slotId)
        {
            return DataLayerFactory.Create<IDictionariesRepository>().GetDeliverySlots().First(s => s.Id == slotId);
        }

        protected void AssertThatSlotIsSavedToDb(string slotId, string slotDescription, TimeSpan slotStartTime, TimeSpan slotEndTime)
        {
            var order = Order.Value;
            Assert.That(order.RequiredDeliverySlotID, Is.EqualTo(slotId));
            Assert.That(order.RequiredDeliverySlotDescription, Is.EqualTo(slotDescription));
            Assert.That(order.RequiredDeliverySlotStartTime, Is.EqualTo(slotStartTime));
            Assert.That(order.RequiredDeliverySlotEndTime, Is.EqualTo(slotEndTime));
        }

        protected void PrepareRequestForWickesHourlyServiceOrder()
        {
            CreationRoutine.CreateRequestOrderHeader();
            CreationRoutine.CreateOrderLines(1);
            CreationRoutine.SetupOrderLine(0, "100005", 1, 5.05M);
            CreationRoutine.CurrentRequest.OrderHeader.ToBeDelivered.Value = true;
            CreationRoutine.CurrentRequest.OrderHeader.DeliveryCharge.Value = 10M;
        }

        protected QodHeader LoadOrderHeader()
        {
            return QodOrder.Get(OrderNumber).Header;
        }

        protected void ProcessPreparedEnableRequest()
        {
            CreationRoutine.ProcessPreparedRequest();
            OrderNumber = CreationRoutine.CurrentResponse.OrderHeader.StoreOrderNumber.Value;
            SourceOrderNumber = CreationRoutine.CurrentResponse.OrderHeader.SourceOrderNumber.Value;

            var qodHeader = LoadOrderHeader();
            OmOrderNumber = qodHeader.OmOrderNumber.ToString();
        }

        protected void PrepareRefundRequestForWickedHourlyServiceOrder()
        {
            EnableRefundRoutine.PrepareRequestForStandardClickAndCollectOrder(SourceOrderNumber);
            EnableRefundRoutine.CurrentRequest.RefundHeader.DeliveryChargeRefundValue = 10m;
        }

        protected void ProcessPreparedEnableRefundRequest()
        {
            EnableRefundRoutine.ProcessPreparedRequest();
        }
    }
}
