using System;
using System.Linq;
using Cts.Oasys.Core.Tests;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Qod.CtsTill;
using Cts.Oasys.Hubs.Core.Order.WebService;
using NUnit.Framework;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.Model.Enums;

namespace WSS.AAT.Scenarios.DayOfChoice
{
    [Binding]
    [Scope(Feature = "Order Manager is notified about new order")]
    class OrderManagerIsNotifiedAboutNewOrderSteps : BaseSteps
    {
        private readonly IBusinessLogicFacade businessLogicFacade;
        private readonly TestSystemEnvironment systemEnvironment;
        private static BaseServiceArgument _request;

        public OrderManagerIsNotifiedAboutNewOrderSteps(IDataLayerFactory dataLayerFactory, IBusinessLogicFacade businessLogicFacade, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
            this.businessLogicFacade = businessLogicFacade;
            this.systemEnvironment = systemEnvironment;
        }

        [Given(@"new delivery order created on Cts Till")]
        public void GivenNewDeliveryOrderCreatedOnCtsTill()
        {
            var routine = new OrderCreationRoutine(DataLayerFactory, systemEnvironment);
            routine.InitHeader(DateTime.Today, true);
            routine.InitLine("100147", 10, 2, 5M);

            string storeId = DataLayerFactory.Create<IDictionariesRepository>().GetStoreId();
            OrderNumber = routine.NewOrder(Int32.Parse(storeId)).Number;
        }

        [When(@"Sale Order Monitor send it to Order Manager")]
        public void WhenSaleOrderMonitorSendItToOrderManager()
        {
            var omService = new CreateService();
            _request = omService.GetRequest();
            _request.SetFieldsFromQod(Order.Value);
        }

        [Then(@"delivery slot information is provided")]
        public void ThenDeliverySlotInformationIsProvided()
        {
            Assert.That(_request, Is.TypeOf<OMOrderCreateRequest>());

            var requiredSlot = ((OMOrderCreateRequest)_request).OrderHeader.RequiredFulfilmentSlot;
            Assert.That(requiredSlot, Is.Not.Null);

            AssertThatSlotIsSavedToDb(requiredSlot.Id, requiredSlot.Description, requiredSlot.StartTime.TimeOfDay, requiredSlot.EndTime.TimeOfDay);
        }

        [Given(@"new delivery order created on Ovc Till")]
        public void GivenNewDeliveryOrderCreatedOnOvcTill()
        {
            // Prepare
            var tran = PrepareTransaction(DataLayerFactory);

            // Save
            SendTransactionToReceiverService(tran);

            // Retrieve
            var dailyTillRepo = DataLayerFactory.Create<TestDailyTillRepository>();
            OrderNumber = dailyTillRepo.SelectDailyTotal(tran.SourceTransactionId, tran.Source).OrderNumber;
        }

        private Transaction PrepareTransaction(IDataLayerFactory dlFactory)
        {
            var builder = new RetailTransactionBuilder(dlFactory, false, DateTime.Now);

            builder.AddTransactionItem(3, "500300", Direction.TO_CUSTOMER);
            builder.AddTransactionItem(5, "100147", Direction.TO_CUSTOMER);

            var item = builder.AddFulfillmentItem<DeliveryFulfillment>(3, 1);
            item.FulfillerNumber = "8080";

            builder.AddFulfillmentItem<DeliveryFulfillment>(2, 2);

            return builder.GetFullyPreparedTransaction();
        }
        
        private void SendTransactionToReceiverService(Transaction tran)
        {
            businessLogicFacade.AddTransaction(tran);
        }

        [Then(@"fulfiller information is provided")]
        public void ThenFulfillerInformationIsProvided()
        {
            var lines = ((OMOrderCreateRequest) _request).OrderLines;
            foreach (var line in lines)
            {
                var qodLine = Order.Value.Lines.First(l => l.Number == line.SellingStoreLineNo.Value);
                if (qodLine.RequiredFulfiller.HasValue)
                {
                    Assert.That(line.RequiredFulfiller, Is.EqualTo(qodLine.RequiredFulfiller.Value.ToString()));
                }
                else
                {
                    Assert.That(line.RequiredFulfiller, Is.Null);
                }
            }
        }

        [Then(@"ovc reference information is provided")]
        public void ThenOvcReferenceInformationIsProvided()
        {
            var header = ((OMOrderCreateRequest) _request).OrderHeader;
            Assert.That(header.TillOrderReference, Is.EqualTo(Order.Value.SourceOrderNumber));

            Assert.That(header.Source, Is.Null);
            Assert.That(header.SourceOrderNumber, Is.Null);
        }

    }
}
