﻿@DoC
Feature: Capacity service is not notified about requests failed to process
    As a store colleague
    I want capacity service not to receive any notifications for failed requests
    So that capacity service does not have irrelevant information

Scenario: No notification for store consignment if failed to save into DB
    Given fulfillment consignment for new delivery order from Cts Till is requested in the store
    And there is duplicate order number in database
    When fulfillment request is tried to process
    Then order is not processed
    And Allocate update capacity request is not created

Scenario: No notification for consignment cancellation if failed to save into DB
    Given delivery order consignment from Cts Till is fulfilled in the store
    And full refund is requested
    And there is duplicate order refund in database
    When refund request is tried to process
    Then refund is not processed
    And Deallocate update capacity request is not created