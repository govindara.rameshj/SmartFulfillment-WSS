﻿@DoC
Feature: Order Manager is notified about new order
	In order to provide DoC functionality
    As a developer
    I want to pass delivery slot, fulfiller and OVC reference of new order to Order Manager

Scenario: Delivery slot is provided for Cts Till order
    Given new delivery order created on Cts Till
    When Sale Order Monitor send it to Order Manager
    Then delivery slot information is provided

Scenario: Delivery slot is provided for Ovc Till order
    Given new delivery order created on Ovc Till
    When Sale Order Monitor send it to Order Manager
    Then delivery slot information is provided

Scenario: Fulfiller is provided for Ovc Till order
    Given new delivery order created on Ovc Till
    When Sale Order Monitor send it to Order Manager
    Then fulfiller information is provided

Scenario: Ovc reference is provided for Ovc Till order
    Given new delivery order created on Ovc Till
    When Sale Order Monitor send it to Order Manager
    Then ovc reference information is provided