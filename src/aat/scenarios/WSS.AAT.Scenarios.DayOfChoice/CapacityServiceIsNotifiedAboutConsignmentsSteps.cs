using System;
using System.Diagnostics;
using Cts.Oasys.Core.Tests;
using TechTalk.SpecFlow;
using NUnit.Framework;
using WSS.AAT.Common.Qod.CtsOrderManager;
using Cts.Oasys.Hubs.Core.Order.Qod;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Entities.ExternalRequestDetails;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.AAT.Scenarios.DayOfChoice
{
    [Binding]
    [Scope(Feature = "Capacity service is notified about consignments from Cts Till")]
    [Scope(Feature = "Capacity service is notified about consignments from Ovc Till")]
    [Scope(Feature = "Capacity service is not notified about wickes hourly service orders")]
    [Scope(Feature = "Capacity service is not notified about requests failed to process")]

    class CapacityServiceIsNotifiedAboutConsignmentsSteps : BaseSteps
    {
        private readonly UpdateOrderRoutine _updateRoutine;
        private QodHeader _orderBeforeUpdate;

        public CapacityServiceIsNotifiedAboutConsignmentsSteps(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
            _updateRoutine = new UpdateOrderRoutine(DataLayerFactory);
            _orderBeforeUpdate = null;
        }

        [Given(@"delivery order consignment from Cts Till is fulfilled in the store")]
        public void GivenDeliveryOrderFromCtsTillIsFulfilledInTheStore()
        {
            PrepareFulfillmentRequestForStoreDeliveryOrder();
            ProcessPreparedFulfillmentRequest();
        }

        [Given(@"delivery order consignment from Ovc Till is fulfilled in the store")]
        public void GivenDeliveryOrderFromOvcTillIsFulfilledInTheStore()
        {
            PrepareFulfillmentRequestForOvcDeliveryOrder();
            ProcessPreparedFulfillmentRequest();
        }

        [Given(@"partial refund is requested")]
        public void GivenPartialRefundIsRequested()
        {
            PrepareRefundRequestForStoreDeliveryOrder(false);
        }

        [Given(@"full refund is requested")]
        public void GivenFullRefundIsRequested()
        {
            PrepareRefundRequestForStoreDeliveryOrder(true);
        }

        [When(@"customer contact details are updated")]
        public void WhenCustomerContactDetailsAreUpdated()
        {
            UpdateOrder(() => _updateRoutine.SetPhoneHomeNumber("123456789"));
        }

        [When(@"delivery date is updated")]
        public void WhenDeliveryDateIsUpdated()
        {
            Debug.Assert(Order.Value.DateDelivery != null, "Order.Value.DateDelivery != null");
            UpdateOrder(() => _updateRoutine.SetDeliveryDate(Order.Value.DateDelivery.Value.AddDays(1)));
        }

        [When(@"delivery slot is updated")]
        public void WhenDeliverySlotIsUpdated()
        {
            UpdateOrder(() => _updateRoutine.SetDeliverySlot(GetDeliverySlot("1")));
        }

        [When(@"fulfillment request is tried to process")]
        public void WhenFulfillmentRequestIsTriedToProcess()
        {
            ProcessPreparedFulfillmentRequest();
            DataLayerFactory.Create<TestCustomerOrderRepository>().GetNextOrderNumber();
        }

        [When(@"refund request is processed")]
        [When(@"refund request is tried to process")]
        public void WhenRefundRequestIsProcessed()
        {
            ProcessPreparedRefundRequest();
        }

        [Then(@"(.*) update capacity request for specifeid slot is created")]
        public void ThenUpdateCapacityRequestForSpecifeidSlotIsCreated(string requestType)
        {
            var extRequest = ThenUpdateCapacityRequestIsCreated(requestType);
            switch (requestType)
            {
                case ExternalRequestType.Allocate:
                case ExternalRequestType.Deallocate:
                    var details = extRequest.GetDetails<UpdateCapacityRequestDetails>();
                    AssertThatSlotAndDateAreCorrect(details.DeliveryDate, details.SlotId, Order.Value);
                    break;
                case ExternalRequestType.AllocateDeallocate:
                    var detailsExtended = extRequest.GetDetails<UpdateCapacityRequestDetailsExtended>();
                    AssertThatSlotAndDateAreCorrect(detailsExtended.DeliveryDate, detailsExtended.SlotId, Order.Value);
                    AssertThatSlotAndDateAreCorrect(detailsExtended.OldDeliveryDate, detailsExtended.OldSlotId, _orderBeforeUpdate);
                    break;
            }
        }

        [Then(@"(.*) update capacity request is created")]
        public ExternalRequest ThenUpdateCapacityRequestIsCreated(string requestType)
        {
            var extRequest = DataLayerFactory.Create<TestExternalRequestRepository>().FindExternalRequest(requestType, OmOrderNumber, StartDateTime);
            Assert.That(extRequest, Is.Not.Null);
            return extRequest;
        }

        [Then(@"(.*) update capacity request is not created")]
        public void ThenUpdateCapacityRequestIsNotCreated(string requestType)
        {
            var extRequest = DataLayerFactory.Create<TestExternalRequestRepository>().FindExternalRequest(requestType, OmOrderNumber, StartDateTime);
            Assert.That(extRequest, Is.Null);
        }

        [Given(@"there is duplicate order number in database")]
        public void GivenThereIsDuplicateOrderNumberInDatabase()
        {
            string orderNum;
            using (var unitOfWork = DataLayerFactory.CreateUnitOfWork())
            {
                var orderRepo = unitOfWork.Create<ICustomerOrderRepository>();
                orderNum = orderRepo.GetNextOrderNumber();
                unitOfWork.Rollback();
            }
            DataLayerFactory.Create<TestCustomerOrderRepository>().InsertIntoCorlin2(
                new CustomerOrderLine2()
                {
                    OrderNumber = orderNum,
                    LineNumber = "0001"
                });
        }

        [Given(@"there is duplicate order refund in database")]
        public void GivenThereIsDuplicateOrderRefundInDatabase()
        {
            var refund = UpdateRefundRoutine.CurrentRequest.OrderRefunds[0];
            DataLayerFactory.Create<TestCustomerOrderRepository>().InsertIntoCorrefund(
                new CustomerOrderRefund()
                {
                    OrderNumber = OrderNumber,
                    LineNumber = refund.RefundLines[0].SellingStoreLineNo,
                    RefundStoreId = Int32.Parse(refund.RefundStoreCode),
                    RefundDate = refund.RefundDate,
                    RefundTill = refund.RefundTill,
                    RefundTransaction = refund.RefundTransaction
                });
        }

        private void PrepareRefundRequestForStoreDeliveryOrder(bool fullRefund)
        {
            UpdateRefundRoutine.CreateRequestOrderHeader(OmOrderNumber);
            foreach (var orderLine in FulfillmentRoutine.CurrentRequest.OrderLines)
            {
                UpdateRefundRoutine.AddOrderRefund(
                    FulfillmentRoutine.StoreId,
                    orderLine.OMOrderLineNo.Value,
                    orderLine.ProductCode.Value,
                    Convert.ToInt32(orderLine.TotalOrderQuantity.Value - (fullRefund ? 0 : 1)),
                    orderLine.LineValue.Value);
            }
        }

        private void ProcessPreparedRefundRequest()
        {
            UpdateRefundRoutine.ProcessPreparedRequest();
        }

        private void UpdateOrder(Action updateAction)
        {
            _orderBeforeUpdate = Order.Value;
            _updateRoutine.InitializeOrderForm(_orderBeforeUpdate);
            updateAction();
            _updateRoutine.AcceptOrder();

            ResetOrderObject();
        }

        private void AssertThatSlotAndDateAreCorrect(DateTime deliveryDate, int slotId, QodHeader order)
        {
            Assert.That(deliveryDate, Is.EqualTo(order.DateDelivery));
            Assert.That(slotId.ToString(), Is.EqualTo(order.RequiredDeliverySlotID));
        }

        [When(@"new wickes hourly service order is created and processed by fulfilling store")]
        public void WhenNewWickesHourlyServiceOrderIsCreatedAndProcessedByFulfillingStore()
        {
            PrepareRequestForWickesHourlyServiceOrder();
            ProcessPreparedEnableRequest();
        }

        [When(@"wickes hourly service order is fully cancelled")]
        public void WhenWickesHourlyServiceOrderIsFullyCancelled()
        {
            PrepareRefundRequestForWickedHourlyServiceOrder();
            ProcessPreparedEnableRefundRequest();
        }
    }
}
