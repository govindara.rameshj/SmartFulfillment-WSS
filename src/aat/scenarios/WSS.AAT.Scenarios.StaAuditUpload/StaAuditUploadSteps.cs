using Cts.Oasys.Core.Tests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;
using WSS.BO.Pic.StaAuditUpload.BL;
using Cts.Oasys.Core.Helpers;

namespace WSS.AAT.Scenarios.StaAuditUpload
{
    [Binding]
    public class StaAuditUploadSteps : BaseStepDefinitions
    {
        List<Adjustment> adjustmentList;

        private readonly IDataLayerFactory dataLayerFactory;

        private int errorsCount = 0;
        private int completedCount = 0;
        private int totalCount = 0;

        public StaAuditUploadSteps(TestSystemEnvironment systemEnvironment, IDataLayerFactory dataLayerFactory)
        {
            this.dataLayerFactory = dataLayerFactory;
        }

        [Given(@"There was a file for STA Audit Upload")]
        public void GivenThereWasAFileForSTAAuditUpload()
        {
            adjustmentList = new List<Adjustment>();
        }


        [Given(@"There was no data about previous Stock Adjustment in db")]
        public void GivenThereWasNoDataAboutPreviousStockAdjustmentInDb()
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            stockRepo.CleanStockTables();
        }

        [Given(@"There was an adjustment for SKU '(.*)' with Normal Stock Quantity = (.*) and Markdown Quantity = (.*)")]
        [Given(@"There was an adjustment for SKU '(.*)' which is not exists in db with Normal Stock Quantity = (.*) and Markdown Quantity = (.*)")]
        public void GivenThereWasAnAdjustmentForSKUWithNormalStockQuantityAndMarkdownQuantity(string skuNumber, int normalStockQty, int markdownStockQty)
        {
            adjustmentList.Add(new Adjustment() { Sku = skuNumber, StandardStockAdjustment = normalStockQty, MarkdownStockAdjustment = markdownStockQty });
        }

        [Given(@"There was SKU '(.*)' with Normal Stock Quantity = (.*) and Markdown Quantity = (.*) in db")]
        public void GivenThereWasSKUWithNormalStockQuantityAndMarkdownQuantityInDb(string skuNumber, int normalStockQty, int markdownStockQty)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            stockRepo.UpdateStockMaster(skuNumber, normalStockQty, markdownStockQty);
        }

        [Given(@"The file was processed by STA Audit Upload")]
        [When(@"The file was processed by STA Audit Upload")]
        [When(@"The file was processed by STA Audit Upload for the second time")]
        public void WhenTheFileWasProcessedBySTAAuditUpload()
        {
            AdjustmentsProcessor adjustmentProcessor = new AdjustmentsProcessor(dataLayerFactory);
            adjustmentProcessor.Process(
                adjustmentList,
                (totalCount, completedCount, errorsCount) => { this.totalCount = totalCount; this.completedCount = completedCount; this.errorsCount = errorsCount; });
        }

        [Given(@"There was only one record in Stock Log file with SKU '(.*)' with Type '(.*)' and Quantity (.*) for yesterday")]
        public void GivenThereWasOnlyOneRecordInStockLogFileWithSKUWithTypeAndQuantityForYesterday(string skuNumber, string type, int endStock)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var yesterdayDate = DateTime.Now.AddDays(-1);

            stockRepo.CleanStockTables();
            stockRepo.InsertIntoStockLog((int)ConversionUtils.GetDaysSinceStartOf1900(yesterdayDate), type, yesterdayDate, "000", skuNumber, 0.00m, 0, endStock);
        }

        [Then(@"(.*) SKU from file is successfully processed")]
        [Then(@"(.*) SKUs from file is successfully processed")]
        public void ThenSKUsFromFileIsSuccessfullyProcessed(int expectedCompletedCount)
        {
            Assert.AreEqual(expectedCompletedCount, completedCount);
        }

        [Then(@"There is (.*) error")]
        [Then(@"There is (.*) errors")]
        public void ThenThereIsErrors(int expectedErrorsCount)
        {
            Assert.AreEqual(expectedErrorsCount, errorsCount);
        }

        [Then(@"There is a line for SKU '(.*)' in Stock Adjustment file with Adjustment Code '(.*)' and Quantity (.*) and Comment '(.*)' and Sequence Number '(.*)' for processed Zebra File for today")]
        public void ThenThereIsALineForSKUInStockAdjustmentFileWithAdjustmentCodeAndQuantityAndCommentAndSequenceNumberForProcessedZebraFileForToday(string skuNumber, string adjustmentCode, decimal adjustmentQuantity, string adjustmentComment, string sequenceNumber)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var stkadj = stockRepo.SelectStockAdjustmentFile(skuNumber, adjustmentCode, true, sequenceNumber, DateTime.Now.Date);

            Assert.IsNotNull(stkadj);
            Assert.AreEqual(stkadj.Quantity, adjustmentQuantity);
            Assert.AreEqual(stkadj.AdjustmentComment.Trim(), adjustmentComment);
        }

        [Then(@"There is a line for SKU '(.*)' in Stock Log file with Movement Type '(.*)' and Movement Key '(.*)' and Starting Stock on Hand Quantity (.*) and Ending Stock on Hand Quantity (.*) for today")]
        public void ThenThereIsALineForSKUInStockLogFileWithMovementTypeAndMovementKeyAndStartingSockOnHandQuantityAndEndingStockOnHandQuantityForToday(string skuNumber, string movementType, string movementKey, int startingStockOnHand, int endingStockOnHand)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var stklog = stockRepo.SelectStockLog(skuNumber, movementType, DateTime.Now.Date);

            Assert.IsNotNull(stklog);

            Assert.AreEqual(stklog.MovementKeys.Trim(), movementKey);
            Assert.AreEqual(stklog.StartingStockOnHand, startingStockOnHand);
            Assert.AreEqual(stklog.EndingStockOnHand, endingStockOnHand);
        }

        [Then(@"There is a line for SKU '(.*)' in Stock Log file with Movement Type '(.*)' and Starting Stock on Hand Quantity (.*) and Ending Stock on Hand Quantity (.*) for today")]
        public void ThenThereIsALineForSKUInStockLogFileWithMovementTypeAndStartingStockOnHandQuantityAndEndingStockOnHandQuantityForToday(string skuNumber, string movementType, int startingStockOnHand, int endingStockOnHand)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var stklog = stockRepo.SelectStockLog(skuNumber, movementType, DateTime.Now.Date);

            Assert.IsNotNull(stklog);

            Assert.AreEqual(stklog.StartingStockOnHand, startingStockOnHand);
            Assert.AreEqual(stklog.EndingStockOnHand, endingStockOnHand);
        }

        [Then(@"There is a line for SKU '(.*)' in Stock Log file with Movement Type '(.*)' and Starting Markdown Stock on Hand Quantity (.*) and Ending Markdown Stock on Hand Quantity (.*) for today")]
        public void ThenThereIsALineForSKUInStockLogFileWithMovementTypeAndStartingMarkdownStockOnHandQuantityAndEndingMarkdownStockOnHandQuantityForToday(string skuNumber, string movementType, int startingStockOnHand, int endingStockOnHand)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var stklog = stockRepo.SelectStockLog(skuNumber, movementType, DateTime.Now.Date);

            Assert.IsNotNull(stklog);

            Assert.AreEqual(stklog.StartingMarkdownStockOnHand, startingStockOnHand);
            Assert.AreEqual(stklog.EndingMarkdownStockOnHand, endingStockOnHand);
        }

        [Then(@"For SKU '(.*)' Stock on Hand Quantity = (.*) and Markdown Stock on Hand Quantity = (.*)")]
        public void ThenForSKUStockOnHandQuantityAndMarkdownStockOnHandQuantity(string skuNumber, int stockOnHand, int markdownStockOnHand)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            Assert.AreEqual(stockOnHand, stockRepo.GetStockOnHand(skuNumber));
            Assert.AreEqual(markdownStockOnHand, stockRepo.GetMarkdownStockOnHand(skuNumber));
        }

        [Then(@"There is no lines for SKU '(.*)' in Stock Adjustment file and in Stock Log file")]
        public void ThenThereIsNoLinesForSKUInStockAdjustmentFileAndInStockLogFile(string skuNumber)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var stkadj = stockRepo.SelectStockAdjustmentFile(skuNumber);
            var stklog = stockRepo.GetStklogForSku(skuNumber);

            Assert.IsNull(stkadj);
            Assert.IsNull(stklog);
        }

        [Then(@"There is correct Movement Key in a line for SKU '(.*)' in Stock Log file with Movement Type '(.*)' and Adjustment Code '(.*)' and Adjustment Sequence Number '(.*)'")]
        public void ThenThereIsCorrectMovementKeyInALineForSKUInStockLogFileWithMovementTypeAndAdjustmentCodeAndAdjustmentSequenceNumber(string skuNumber, string movementType, string adjustmentCode, string adjustmentSequenceNumber)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var stklog = stockRepo.SelectStockLog(skuNumber, movementType, DateTime.Now.Date);

            Assert.IsNotNull(stklog);

            var movementKey = string.Format("{0}  {1}  {2}  {3}", DateTime.Now.Date.ToString("yyyy/MM/dd"), adjustmentCode, skuNumber, adjustmentSequenceNumber);

            Assert.AreEqual(stklog.MovementKeys.Trim(), movementKey);
        }

        [Then(@"There is only one line for SKU '(.*)' in Stock Adjustment file with Adjustment Code '(.*)'")]
        public void ThenThereIsOnlyOneLineForSKUInStockAdjustmentFileWithAdjustmentCode(string skuNumber, string adjustmentCode)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var stkAdjustments = stockRepo.SelectStockAdjustmentFiles(skuNumber, adjustmentCode);

            Assert.IsNotNull(stkAdjustments);
            Assert.AreEqual(1, stkAdjustments.Count);
        }
    }
}
