﻿Feature: StaAuditUpload
@StaAuditUpload

Scenario: Standart STA Audit Upload file processing
Given There was a file for STA Audit Upload
And There was no data about previous Stock Adjustment in db
And There was an adjustment for SKU '500300' with Normal Stock Quantity = 3 and Markdown Quantity = 1
And There was SKU '500300' with Normal Stock Quantity = 10 and Markdown Quantity = 10 in db
And There was an adjustment for SKU '100013' with Normal Stock Quantity = -4 and Markdown Quantity = -2
And There was SKU '100013' with Normal Stock Quantity = 10 and Markdown Quantity = 10 in db
When The file was processed by STA Audit Upload
Then 2 SKUs from file is successfully processed
Then There is 0 errors
And There is a line for SKU '500300' in Stock Adjustment file with Adjustment Code '02' and Quantity 3 and Comment 'PIC - Audit Count' and Sequence Number '00' for processed Zebra File for today
And There is a line for SKU '500300' in Stock Adjustment file with Adjustment Code '53' and Quantity 1 and Comment 'PIC - Audit Count' and Sequence Number '00' for processed Zebra File for today
And There is a line for SKU '100013' in Stock Adjustment file with Adjustment Code '02' and Quantity -4 and Comment 'PIC - Audit Count' and Sequence Number '00' for processed Zebra File for today
And There is a line for SKU '100013' in Stock Adjustment file with Adjustment Code '53' and Quantity -2 and Comment 'PIC - Audit Count' and Sequence Number '00' for processed Zebra File for today
And There is a line for SKU '500300' in Stock Log file with Movement Type '32' and Starting Stock on Hand Quantity 10 and Ending Stock on Hand Quantity 13 for today
And There is a line for SKU '500300' in Stock Log file with Movement Type '32' and Starting Markdown Stock on Hand Quantity 10 and Ending Markdown Stock on Hand Quantity 10 for today
And There is a line for SKU '500300' in Stock Log file with Movement Type '67' and Starting Stock on Hand Quantity 13 and Ending Stock on Hand Quantity 13 for today
And There is a line for SKU '500300' in Stock Log file with Movement Type '67' and Starting Markdown Stock on Hand Quantity 10 and Ending Markdown Stock on Hand Quantity 11 for today
And There is a line for SKU '100013' in Stock Log file with Movement Type '31' and Starting Stock on Hand Quantity 10 and Ending Stock on Hand Quantity 6 for today
And There is a line for SKU '100013' in Stock Log file with Movement Type '31' and Starting Markdown Stock on Hand Quantity 10 and Ending Markdown Stock on Hand Quantity 10 for today
And There is a line for SKU '100013' in Stock Log file with Movement Type '66' and Starting Stock on Hand Quantity 6 and Ending Stock on Hand Quantity 6 for today
And There is a line for SKU '100013' in Stock Log file with Movement Type '66' and Starting Markdown Stock on Hand Quantity 10 and Ending Markdown Stock on Hand Quantity 8 for today
And For SKU '500300' Stock on Hand Quantity = 13 and Markdown Stock on Hand Quantity = 11
And For SKU '100013' Stock on Hand Quantity = 6 and Markdown Stock on Hand Quantity = 8

Scenario: STA Audit Upload file processing with SKU wich does not exist in db
Given There was a file for STA Audit Upload
And There was no data about previous Stock Adjustment in db
And There was an adjustment for SKU '100500' which is not exists in db with Normal Stock Quantity = 3 and Markdown Quantity = 1
And There was an adjustment for SKU '100013' with Normal Stock Quantity = 4 and Markdown Quantity = -2
And There was SKU '100013' with Normal Stock Quantity = 10 and Markdown Quantity = 10 in db
When The file was processed by STA Audit Upload
Then 1 SKU from file is successfully processed
And There is 1 error
And There is no lines for SKU '100500' in Stock Adjustment file and in Stock Log file

Scenario: Correct Movement Keys are saved after STA Audit Upload file process
Given There was a file for STA Audit Upload
And There was no data about previous Stock Adjustment in db
And There was an adjustment for SKU '500300' with Normal Stock Quantity = 3 and Markdown Quantity = 1
And There was an adjustment for SKU '100013' with Normal Stock Quantity = -4 and Markdown Quantity = -2
When The file was processed by STA Audit Upload
Then 2 SKUs from file is successfully processed
Then There is 0 errors
And There is correct Movement Key in a line for SKU '500300' in Stock Log file with Movement Type '32' and Adjustment Code '02' and Adjustment Sequence Number '00'
And There is correct Movement Key in a line for SKU '500300' in Stock Log file with Movement Type '67' and Adjustment Code '53' and Adjustment Sequence Number '00'
And There is correct Movement Key in a line for SKU '100013' in Stock Log file with Movement Type '31' and Adjustment Code '02' and Adjustment Sequence Number '00'
And There is correct Movement Key in a line for SKU '100013' in Stock Log file with Movement Type '66' and Adjustment Code '53' and Adjustment Sequence Number '00'

Scenario: Correct Adjustment Sequence Numbers are saved after STA Audit Upload file process
Given There was a file for STA Audit Upload
And There was no data about previous Stock Adjustment in db
And There was an adjustment for SKU '500300' with Normal Stock Quantity = 3 and Markdown Quantity = 1
And There was an adjustment for SKU '500300' with Normal Stock Quantity = 4 and Markdown Quantity = 2
When The file was processed by STA Audit Upload
Then 2 SKUs from file is successfully processed
Then There is 0 errors
And There is a line for SKU '500300' in Stock Adjustment file with Adjustment Code '02' and Quantity 3 and Comment 'PIC - Audit Count' and Sequence Number '00' for processed Zebra File for today
And There is a line for SKU '500300' in Stock Adjustment file with Adjustment Code '53' and Quantity 1 and Comment 'PIC - Audit Count' and Sequence Number '00' for processed Zebra File for today
And There is a line for SKU '500300' in Stock Adjustment file with Adjustment Code '02' and Quantity 4 and Comment 'PIC - Audit Count' and Sequence Number '01' for processed Zebra File for today
And There is a line for SKU '500300' in Stock Adjustment file with Adjustment Code '53' and Quantity 2 and Comment 'PIC - Audit Count' and Sequence Number '01' for processed Zebra File for today

Scenario: Stock Adjustment from a file can not be processed twice
Given There was a file for STA Audit Upload
And There was no data about previous Stock Adjustment in db
And There was an adjustment for SKU '500300' with Normal Stock Quantity = 4 and Markdown Quantity = 2
And The file was processed by STA Audit Upload
When The file was processed by STA Audit Upload for the second time
Then There is only one line for SKU '500300' in Stock Adjustment file with Adjustment Code '02'
Then There is only one line for SKU '500300' in Stock Adjustment file with Adjustment Code '53'

Scenario: STA Audit Upload file processing with System Adjustment 91
Given There was only one record in Stock Log file with SKU '500300' with Type '01' and Quantity 5 for yesterday
And There was a file for STA Audit Upload
And There was an adjustment for SKU '500300' with Normal Stock Quantity = 3 and Markdown Quantity = 0
And There was SKU '500300' with Normal Stock Quantity = 10 and Markdown Quantity = 10 in db
When The file was processed by STA Audit Upload
Then 1 SKUs from file is successfully processed
And There is 0 errors
And There is a line for SKU '500300' in Stock Log file with Movement Type '91' and Starting Stock on Hand Quantity 5 and Ending Stock on Hand Quantity 10 for today
And There is a line for SKU '500300' in Stock Log file with Movement Type '32' and Starting Stock on Hand Quantity 10 and Ending Stock on Hand Quantity 13 for today