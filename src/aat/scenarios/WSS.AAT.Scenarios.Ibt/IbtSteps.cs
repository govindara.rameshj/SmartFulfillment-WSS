using System.Linq;
using Cts.Oasys.Core.SystemEnvironment;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using NUnit.Framework;
using Purchasing.Core;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Qod.CtsOrderManager;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Repositories;
using WSS.AAT.Common.Qod;
using Cts.Oasys.Hubs.Core.Order.Qod;
using WSS.AAT.Common.Utility.Configuration;
using WSS.AAT.Common.Utility.Executables;
using WSS.AAT.Common.Utility.FileParsers;
using WSS.AAT.Common.Utility.Utility;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.AAT.Scenarios.Ibt
{
    [Binding]
    public class IbtSteps : BaseStepDefinitions
    {
        private readonly ISystemEnvironment systemEnvironment;
        private readonly TestMiscRepository _miscRepo;
        private readonly TestIbtRepository _ibtRepo;
        private readonly CustomerOrderRepository _orderRepo;
        private readonly FulfillmentRoutine _innerRoutine;
        private readonly StatusNotificationRoutine _statusNotificationRoutine;
        private readonly UpdateRefundRoutine _updateRefundRoutine;
        private readonly string executablesDir;
        string orderNumber;

        private string _previousIbtNumber;
        private string _processedReceiptIbtNumber;

        private CTSFulfilmentResponse CurrentResponse
        {
            get { return _innerRoutine.CurrentResponse; }
        }

        private CTSStatusNotificationRequest StatusNotificationRequest
        {
            get { return _statusNotificationRoutine.CurrentRequest; }
        }

        private CTSStatusNotificationResponse StatusNotificationResponse
        {
            get { return _statusNotificationRoutine.CurrentResponse; }
        }

        private CTSUpdateRefundResponse UpdateRefundResponse
        {
            get { return _updateRefundRoutine.CurrentResponse; }
        }

        public IbtSteps(TestConfiguration testConfiguration, IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
        {
            this.systemEnvironment = systemEnvironment;
            _miscRepo = dataLayerFactory.Create<TestMiscRepository>();
            _ibtRepo = dataLayerFactory.Create<TestIbtRepository>();
            _orderRepo = dataLayerFactory.Create<TestCustomerOrderRepository>();
            _innerRoutine = new FulfillmentRoutine(dataLayerFactory, systemEnvironment);
            _statusNotificationRoutine = new StatusNotificationRoutine(dataLayerFactory, systemEnvironment);
            _updateRefundRoutine = new UpdateRefundRoutine(dataLayerFactory, systemEnvironment);

            executablesDir = testConfiguration.ResolvePathFromAppSettings("executablesDir");
        }

        [Given(@"Database contains all nessesary information")]
        public void GivenDatabaseContainsAllNessesaryInformation()
        {
            _ibtRepo.CleanPurAndSupplierTables();
            _ibtRepo.InsertStubPurhdr();
            _ibtRepo.InsertStubPurlin();
            _ibtRepo.InsertStubSupmas();
        }

        [Given(@"Next number for receipt is (.*)")]
        public void GivenNextNumberForReceiptIs(int nextNumber)
        {
            _ibtRepo.SetNextNumberForId(nextNumber, 4);
        }

        [When(@"Store process purchase order")]
        public void WhenWeReceivePurchaseOrder()
        {
            _processedReceiptIbtNumber = GetDrlNumberForSavedReceipt();
        }

        [Then(@"Drl number for saved receipt is equal '(.*)'")]
        public void ThenDrlNumberForSavedReceiptIsEqual(string drlNumber)
        {
            Assert.AreEqual(drlNumber, _processedReceiptIbtNumber);
        }

        [Given(@"An Order was placed in the selling store")]
        public void GivenAnOrderWasPlacedInTheSellingStore()
        {
            _previousIbtNumber = _ibtRepo.GetCurrentIbtCounterValue();
            _innerRoutine.CreateRequestOrderHeader();
            _innerRoutine.AddOrderLine("780296", "Single Drawer Set Tandom 400mm", 5, 50.00M);
            _innerRoutine.AddOrderLine("305082", "Zan Sgle 3 Function", 3, 238.00M);
        }


        [Given(@"There was no similar order in database before")]
        public void GivenThereWereNoSimilarOrderInDatabaseBefore()
        {
            _miscRepo.ClearOrderTables();
        }

        [Given(@"An Order was placed in the selling store with incorrect sku")]
        public void GivenAnOrderWasPlacedInTheSellingStoreWithIncorrectSku()
        {
            _previousIbtNumber = _ibtRepo.GetCurrentIbtCounterValue();
            _innerRoutine.CreateRequestOrderHeader();
            _innerRoutine.AddOrderLine("909090", "Non-existing position", 1, 20M);
        }


        [Given(@"A Status Notification request with missing line number was sent")]
        public void GivenAStatusNotificationWithMissingLineNumberWasSent()
        {
            var order = QodOrder.Get(orderNumber);

            _statusNotificationRoutine.CreateRequestOrderHeader(order.Header.OmOrderNumber.ToString(), "300", orderNumber);
            _statusNotificationRoutine.AddFullFillmentSite(order.Header.OmOrderNumber.ToString(), "300", new[] { "909090" });
            StatusNotificationRequest.FulfilmentSites[0].OrderLines[0].SellingStoreLineNo.Value = "50";
        }

        [Given(@"A Status Notification request was sent")]
        public void GivenAStatusNotificationWasSent()
        {
            var order = QodOrder.Get(orderNumber);

            _statusNotificationRoutine.CreateRequestOrderHeader(order.Header.OmOrderNumber.ToString(), "300", orderNumber);
            _statusNotificationRoutine.AddFullFillmentSite(order.Header.OmOrderNumber.ToString(), "300", new[] { "100008" }, "8338");
            _statusNotificationRoutine.AddFullFillmentSite(order.Header.OmOrderNumber.ToString(), "300", new[] { "780296", "305082" });
        }

        [Given(@"The order was processed")]
        public void GivenTheOrderWasProcessed()
        {
            _innerRoutine.ProcessPreparedRequest();
            Assert.IsTrue(IsLastRequestSuccessful(CurrentResponse));

            orderNumber = _innerRoutine.CurrentResponse.FulfillingSiteOrderNumber.Value;
        }

        [Given(@"Current Ibt number was saved")]
        public void GivenIbtNumberWasSaved()
        {
            _previousIbtNumber = _ibtRepo.GetCurrentIbtCounterValue();
        }

        [Given(@"Delivery status in the Status Notification is (.*)")]
        public void GivenDeliveryStatusInTheStatusNotificationIs(int status)
        {
            StatusNotificationRequest.OrderStatus.Value = status.ToString();

            foreach (var site in StatusNotificationRequest.FulfilmentSites)
            {
                foreach (var line in site.OrderLines)
                {
                    line.LineStatus.Value = status.ToString();
                }
            }
        }

        [Given(@"An Update Refund request with missing line was sent")]
        public void GivenAnUpdateRefundRequestWithMissingLineWasSent()
        {
            var order = QodOrder.Get(orderNumber);

            _updateRefundRoutine.CreateRequestOrderHeader(order.Header.OmOrderNumber.ToString());
            _updateRefundRoutine.AddOrderRefund(0, "0005", "909090", 1, 10M);
        }

        [Given(@"An Update Refund request with exception was sent")]
        public void GivenAnUpdateRefundRequestWithExceptionWasSent()
        {
            var order = QodOrder.Get(orderNumber);

            _updateRefundRoutine.CreateRequestOrderHeader(order.Header.OmOrderNumber.ToString());
            _updateRefundRoutine.AddOrderRefund(_innerRoutine.StoreId, "0002", "780296", 1, 50M);
            _updateRefundRoutine.AddOrderRefund(0, "0001", "100008", 1, 4399.00M);
        }

        [Given(@"An Update Refund request was sent")]
        public void GivenAnUpdateRefundRequestWasSent()
        {
            var order = QodOrder.Get(orderNumber);

            _updateRefundRoutine.CreateRequestOrderHeader(order.Header.OmOrderNumber.ToString());
            _updateRefundRoutine.AddOrderRefund(_innerRoutine.StoreId, "0002", "780296", 1, 50M);
        }


        [When(@"The Fulfillment is requested in the store")]
        public void WhenFulfillmentIsRequestedInTheStore()
        {
            _innerRoutine.ProcessPreparedRequest();
        }

        [When(@"The Status Notification is requested in the store")]
        public void WhenStatusNotificationIsRequestedInTheStore()
        {
            _statusNotificationRoutine.ProcessPreparedRequest();
        }

        [When(@"The Update Refund is requested in the store")]
        public void WhenTheUpdateRefundIsRequestedInTheStore()
        {
            _updateRefundRoutine.ProcessPreparedRequest();
        }

        [Then(@"The Fulfillment is processed successfully")]
        public void ThenFulfillmentIsProcessed()
        {
            Assert.IsTrue(IsLastRequestSuccessful(CurrentResponse));
        }

        [Then(@"The Status Notification is processed successfully")]
        public void ThenStatusNotificationIsProcessedSuccessfully()
        {
            Assert.IsTrue(IsLastRequestSuccessful(StatusNotificationResponse));
        }

        [Then(@"Ibt number in response is matched with last created DB record number")]
        public void ThenIbtNumberInResponseIsMatchedWithLastCreatedDbRecordNumber()
        {
            var responseIbtNumber = GetCreatedIbtNumber();
            Assert.AreEqual(_previousIbtNumber, responseIbtNumber);
        }

        [Then(@"Ibt number in response is matched with numbers in created order lines")]
        public void ThenIbtNumberNumberInResponseIsMatchedWithNumbersInCreatedOrderLines()
        {
            var responseOrderNumber = GetCreatedOrderNumber();
            var orderLines = _orderRepo.SelectCorlins(responseOrderNumber);
            foreach (var orderLine in orderLines)
            {
                Assert.AreEqual(_previousIbtNumber, orderLine.DeliverySourceIbtOut.Trim().PadLeft(6, '0'));
            }
        }

        [Then(@"The Fulfillment is not processed")]
        public void ThenFulfillmentIsNotProcessed()
        {
            Assert.IsFalse(IsLastRequestSuccessful(CurrentResponse));
            Assert.IsTrue(IsProductCodeFieldHasValidationError());
        }

        [Then(@"The Status Notification is not processed")]
        public void ThenStatusNotificationIsNotProcessed()
        {
            Assert.IsFalse(IsLastRequestSuccessful(StatusNotificationResponse));
            Assert.IsTrue(IsSellingStoreLineNoHasValidationError());
        }

        [Then(@"Ibt counter value is not changed")]
        public void ThenIbtCounterValueIsNotChanged()
        {
            var newIbtNumber = _ibtRepo.GetCurrentIbtCounterValue();
            Assert.AreEqual(_previousIbtNumber, newIbtNumber);
        }

        [Then(@"The Update Refund is not processed")]
        public void ThenTheUpdateRefundIsNotProcessed()
        {
            Assert.IsFalse(IsLastRequestSuccessful(UpdateRefundResponse));
        }

        [Then(@"The Update Refund is empty")]
        public void ThenTheUpdateRefundIsEmpty()
        {
            Assert.IsTrue(IsLastResponseEmpty(UpdateRefundResponse));
        }

        [Then(@"The Update Refund is processed successfully")]
        public void ThenTheUpdateRefundIsProcessedSuccessfully()
        {
            Assert.IsTrue(IsLastRequestSuccessful(UpdateRefundResponse));
        }

        [Then(@"Refunded item is saved")]
        public void ThenRefundedItemIsSaved()
        {
            Assert.IsNotNull(GetRefundItem());
        }

        [Given(@"There are only the following records in DRL table")]
        public void GivenThereAreOnlyTheFollowingRecordsInDrlTable(Table table)
        {
            _ibtRepo.CleanDrl();

            var wrappedTable = new SpecflowTableWrapper<DrlSummary>(table);
            wrappedTable.ReplaceDateTimeWithActualValues("CreatedDate");
            var drls = wrappedTable.GetEntities();
            
            // set default values
            foreach (var drl in drls)
            {
                drl.EntryPersonInitials = "WSS";
                drl.OrderSupplierNumber = "00000";
                drl.AssignedConsignmentNoteNumber = "0";
                drl.PurchaseOrderNumber = "0";
                drl.PurchaseOrderSoqControlNumber = "0";
                drl.SourceStoreDrlNumber = "0";
                drl.SupplierNumber = "00000";
                drl.OriginalPoNumberOrReturnNumber = "0";
                drl.ConsignmentNoteNumber = "0";
                drl.RtiFlag = RtiStatus.NotSent;
                drl.EmployeeId = 44;

                _ibtRepo.InsertDrlSummary(drl);
            }
        }

        [When(@"Nightly routine step ""Process Trans - Add DRL to HO Comms"" was run")]
        public void WhenNightlyRoutineStepProcessTransAddDrlToHoCommsWasRun()
        {
            ProcessTransmissions pt = new ProcessTransmissions(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.ProcessFileCode("DR");
        }

        [Then(@"Drl (.*) is set as communicated to Head Office")]
        public void ThenDrlIsSetAsCommunicatedToHeadOffice(string drlNumber)
        {
            var summary = _ibtRepo.GetDrlSummary(drlNumber);
            Assert.That(summary, Is.Not.Null);
            Assert.That(summary.IsAlreadyCommedToHeadOffice, Is.True);
        }

        [Then(@"There are no updates in STHOA file")]
        public void ThenThereAreNoUpdatesInSthoaFile()
        {
            Assert.That(SthoaFileParser.FileExists(systemEnvironment), Is.False);
        }

        public string GetDrlNumberForSavedReceipt()
        {
            var orders = Order.GetOrdersOutstanding();
            var receipt = new Receipt(orders[0], 44) {Comments = "test"};
            receipt.Insert();

            return receipt.Number;
        }

        public bool IsProductCodeFieldHasValidationError()
        {
            return CurrentResponse.OrderLines.Any(line => !string.IsNullOrEmpty(line.ProductCode.ValidationStatus));
        }

        public bool IsLastRequestSuccessful(BaseResponse currentResponse)
        {
            return currentResponse != null && currentResponse.IsSuccessful();
        }

        public bool IsSellingStoreLineNoHasValidationError()
        {
            return StatusNotificationResponse.FulfilmentSites
                .SelectMany(site => site.OrderLines)
                .Any(line => !string.IsNullOrEmpty(line.SellingStoreLineNo.ValidationStatus));
        }

        public string GetCreatedOrderNumber()
        {
            return CurrentResponse != null ? CurrentResponse.FulfillingSiteOrderNumber.Value : "N/A";
        }

        public string GetCreatedIbtNumber()
        {
            return CurrentResponse != null ? CurrentResponse.IBTOutNumber.Value.PadLeft(6, '0') : "N/A";
        }

        private bool IsLastResponseEmpty(BaseResponse currentResponse)
        {
            return currentResponse == null;
        }

        private string GetRefundItem()
        {
            var qodHeaders = new QodHeaderCollection();
            qodHeaders.LoadExistingOrder(orderNumber);
            var qodHeader = qodHeaders.First();

            return qodHeader.Refunds.First(l => l.Number == "0002").FulfillingStoreIbtIn;
        }
    }
}
