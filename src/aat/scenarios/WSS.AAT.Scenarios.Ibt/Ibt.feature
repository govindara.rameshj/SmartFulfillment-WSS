﻿@Ibt
Feature: Ibt

Scenario: Drl number for saved receipt is correct
    Given Database contains all nessesary information
    And Next number for receipt is 100
    When Store process purchase order
    Then Drl number for saved receipt is equal '000100'

Scenario: Fulfillment creates ibt
    Given An Order was placed in the selling store
    And There was no similar order in database before
    When The Fulfillment is requested in the store
    Then The Fulfillment is processed successfully
    And Ibt number in response is matched with last created DB record number
    And Ibt number in response is matched with numbers in created order lines

Scenario: Fulfillment that is going to fail does not change ibt counter
    Given An Order was placed in the selling store with incorrect sku
    When The Fulfillment is requested in the store
    Then The Fulfillment is not processed
    And Ibt counter value is not changed

Scenario: Status Notification that is going to fail does not change ibt counter
    Given An Order was placed in the selling store
    And There was no similar order in database before
    And The order was processed
    And Current Ibt number was saved
    And A Status Notification request with missing line number was sent
    When The Status Notification is requested in the store
    Then The Status Notification is not processed
    And Ibt counter value is not changed

Scenario: Ibt number in response is matched with last created DB record number for correct Status Notification request
    Given An Order was placed in the selling store
    And There was no similar order in database before
    And The order was processed
    And A Status Notification request was sent
    When The Status Notification is requested in the store
    Then The Status Notification is processed successfully
    And Ibt number in response is matched with last created DB record number

Scenario: Counter value has not changed after Status Notification request without Ibt
    Given An Order was placed in the selling store
    And There was no similar order in database before
    And The order was processed
    And Current Ibt number was saved
    And A Status Notification request was sent
    And Delivery status in the Status Notification is 400
    When The Status Notification is requested in the store
    Then The Status Notification is processed successfully
    And Ibt counter value is not changed

Scenario: Counter value has not changed after failed Update Refund request
    Given An Order was placed in the selling store
    And There was no similar order in database before
    And The order was processed
    And Current Ibt number was saved
    And An Update Refund request with missing line was sent
    When The Update Refund is requested in the store
    Then The Update Refund is not processed
    And Ibt counter value is not changed

Scenario: Counter value has not changed after failed Update Refund request with exception
    Given An Order was placed in the selling store
    And There was no similar order in database before
    And The order was processed
    And Current Ibt number was saved
    And An Update Refund request with exception was sent
    When The Update Refund is requested in the store
    Then The Update Refund is empty
    And Ibt counter value is not changed

Scenario: Ibt number in response is matched with last created DB record number for correct Update Refund request and refunded item is saved
    Given An Order was placed in the selling store
    And There was no similar order in database before
    And The order was processed
    And An Update Refund request was sent
    When The Update Refund is requested in the store
    Then The Update Refund is processed successfully
    And Ibt number in response is matched with last created DB record number
    And Refunded item is saved

Scenario: No orphaned DLR records transmitted to Head Office
    Orphaned means records without details

    Given There are only the following records in DRL table
        | Drl Number | Type |  Value | Created date | Comment | Store number | Is needed to be printed |
        |     037233 |    2 |  0.00  |        Today |         |              |                    true |
        |     037234 |    2 | 635.18 |        Today | 1234567 |          514 |                   false |
    When Nightly routine step "Process Trans - Add DRL to HO Comms" was run
    Then Drl 037233 is set as communicated to Head Office
    Then Drl 037234 is set as communicated to Head Office
    Then There are no updates in STHOA file
