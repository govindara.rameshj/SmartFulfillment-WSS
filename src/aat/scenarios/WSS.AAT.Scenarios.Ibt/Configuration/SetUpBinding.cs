using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Configuration;
using NUnit.Framework;

namespace WSS.AAT.Scenarios.Ibt
{
    [Binding]
    [SetUpFixture]
    class SetUpBinding : BaseSetUpBinding<SetUp>
    {
        [SetUp]
        public static void BeforeTestRun()
        {
            DoBeforeTestRun();
        }

        [BeforeScenario]
        public static void BeforeScenario()
        {
            DoBeforeScenario();
        }
    }
}
