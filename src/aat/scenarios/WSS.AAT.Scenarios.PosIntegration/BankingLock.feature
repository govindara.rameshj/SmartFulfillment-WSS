﻿@soes
Feature: BankingLock
    In order to check banking lock scenarios
    As a developer
    I created tests for different states of lock

Scenario: Today's transaction arrives after pickup, but before banking
    Given Sale happened today
    And Safes are created
    And Pickup process for virtual cashier has finished
    And Banking process hasn't started
    And Nightly routine procedure is not started
    When BO received the transaction
    Then There is sale in BO db
    And Sale date equal today in BO db
    And Received date equal today in BO db
    And CBBU field is null in BO db
    And CashBalCashier dates are equal today

Scenario: Today's transaction arrives after banking and after daily close
    Given Sale happened today
    And Safes are created
    And Pickup process for virtual cashier has finished
    And Banking process has finished
    And Nightly routine procedure was finished
    When BO received the transaction
    Then There is sale in BO db
    And Sale date equal today in BO db
    And Received date equal today in BO db
    And CBBU field equal tomorrow
    And CashBalCashier dates are equal tomorrow

Scenario: Yesterday's transaction arrives today
    Given Sale happened yesterday
    And Safes are created
    When BO received the transaction
    Then There is sale in BO db
    And Sale date equal yesterday in BO db 
    And Received date equal today in BO db
    And CBBU field equal today
    And CashBalCashier dates are equal today
