﻿@PosIntegration
Feature: DuplicateTransaction
	In order to avoid duplicate transactions in DB
	As a developer
	I should create each transaction only once

Scenario: Process one transaction twice
    Given Sale happened
	And BO received the transaction
    When the same transaction come again
    Then no new record is created in BO db
