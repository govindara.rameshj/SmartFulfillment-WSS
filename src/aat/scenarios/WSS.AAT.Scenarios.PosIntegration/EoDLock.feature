﻿@soes
Feature: EoDLock
    In order to check EoD lock transaction processing
    I as a developer
    Write some test scenarios

Scenario: Transaction reject while EoD processing
    Given Nightly routine procedure is started
    And Sale happened
    When BO received the transaction
    Then EoD lock error happened

Scenario: After EoD lock end transaction processes successfully
    Given Nightly routine procedure is started
    And Nightly routine procedure was finished
    And Sale happened
    When BO received the transaction
    Then There is sale in BO db
