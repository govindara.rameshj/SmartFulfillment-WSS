﻿@PosIntegration
Feature: Order

Scenario: Save sale transaction to ledger for order
    Given Sale happened
    And Customer name is Vasily
    And 5 SKU 500300 sold
    And Was paid by cash for 169.85 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    When BO received the transaction
    Then There is an order in DB
    And There is line with 5 SKU 500300
    And Customer info for Vasily is added to db
    And There is cash payment for 169.85 pounds
    And Transaction is marked as Online

Scenario: Order has correct number
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And New order number will be 000111
    And There were no records for order number 000111 before
    When BO received the transaction
    Then There is an order in DB
    And New order number is 000111 in all tables

Scenario: Save collection order
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    When BO received the transaction
    Then There is collection order in DB

Scenario: Save voided collection order
    Given There were no orders in db
    And Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And Transaction was voided
    When BO received the transaction
    Then There is no collection order in DB

Scenario: Save delivery order
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    When BO received the transaction
    Then There is delivery order in DB

Scenario: Save voided delivery order
    Given There were no orders in db 
    And Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And Transaction was voided
    When BO received the transaction
    Then There is no delivery order in DB

 Scenario: Save info about taken now items for order
    Given Sale happened
    And Customer name is Vasily
    And 5 SKU 500300 sold
    And 2 SKU 100006 sold
    And Was paid by cash for 761.85 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And 2 items for line 2 were collected instantly
    When BO received the transaction
    Then There is an order in DB
    And Total units taken number is 5 in order info
    And Quantity taken for SKU 500300 is 3
    And Quantity to be delivered for SKU 500300 is 2
    And Quantity taken for SKU 100006 is 2
    And Quantity to be delivered for SKU 100006 is 0
    And Delivery status for SKU 100006 is 999

Scenario: Order with delivery instructions
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And Delivery Instruction was "Deliver only after 2 p.m."
    When BO received the transaction
    Then There is delivery order in DB
    And There is a Delivery Instruction "Deliver only after 2 p.m."

Scenario: Delivery Order Without Items to be taken
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    When BO received the transaction
    Then There is delivery order in DB

Scenario: Order with Delivery Charge Value of Group A with the First Price
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    And Delivery Charge was 7.95
    And There was a Delivery Charge of Group 'A' with Value 7.95 and with SKU '805002'
    When BO received the transaction
    Then There is delivery order in DB
    And There is 1 line with Delivery Charge item with SKU '805002' with amount of 7.95 pounds
    And The Order Deivery Charge amount is 7.95
    And Quantity for Delivery Charge item with SKU '805002' is 1

Scenario: Order with Delivery Charge Value of Group A with the Second Price
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    And There was a Delivery Charge of Group 'A' with Value 12.95 and with SKU '805002'
    And Delivery Charge was 12.95
    When BO received the transaction
    Then There is delivery order in DB
    And There is 1 line with Delivery Charge item with SKU '805002' with amount of 12.95 pounds
    And The Order Deivery Charge amount is 12.95
    And Quantity for Delivery Charge item with SKU '805002' is 1

Scenario: Order with Delivery Charge Value of Group B
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    And Delivery Charge was 9.95
    And There was a Delivery Charge of Group 'B' with Value 9.95 and with SKU '805006'
    When BO received the transaction
    Then There is delivery order in DB
    And There is 1 line with Delivery Charge item with SKU '805006' with amount of 9.95 pounds
    And The Order Deivery Charge amount is 9.95
    And Quantity for Delivery Charge item with SKU '805006' is 1

Scenario: Order with Delivery Charge Value of Group C
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    And Delivery Charge was 25.00
    And There was no Delivery Charge Group with value 25.00 
    When BO received the transaction
    Then There is delivery order in DB
    And There is 1 line with Delivery Charge item with SKU '805000' with amount of 25.00 pounds
    And The Order Deivery Charge amount is 25.00
    And Quantity for Delivery Charge item with SKU '805000' is 1

Scenario: Order with new Delivery Charge Value of Group A after it Group Value has been changed
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    And Delivery Charge was 15.99
    And There was a Delivery Charge of Group 'A' with Value 7.95 and with SKU '805002'
    And Delievery Charge Value for Group 'A' was changed from 7.95 to 15.99
    When BO received the transaction
    Then There is delivery order in DB
    And There is 1 line with Delivery Charge item with SKU '805002' with amount of 15.99 pounds
    And The Order Deivery Charge amount is 15.99
    And Quantity for Delivery Charge item with SKU '805002' is 1

Scenario: Order with obsolete Delivery Charge Value of Group A after it Value has been changed
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    And Delivery Charge was 7.95
    And There was a Delivery Charge of Group 'A' with Value 7.95 and with SKU '805002'
    And Delievery Charge Value for Group 'A' was changed from 7.95 to 17.99
    When BO received the transaction
    Then There is delivery order in DB
    And There is 1 line with Delivery Charge item with SKU '805000' with amount of 7.95 pounds
    And The Order Deivery Charge amount is 7.95
    And Quantity for Delivery Charge item with SKU '805000' is 1

Scenario: Order with different contacts for destination and for buyer
    Given Sale happened
    And Customer name is Vasily
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And Customer provides destination details that are different from his own address
    When BO received the transaction
    Then There is an order in DB
    And Customer info for Vasily is added to db
    And Destination address details for order are correctly stored in database

Scenario: Save collection order with Price Override 
    Given Sale happened
    And 5 SKU 500300 sold
    And Price for SKU 500300 was overriden by 25.00 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    When BO received the transaction
    Then There is collection order in DB
    And Order Merchandise Value Store value is equal to Transaction Total amount
    And Order Line Price is equal to Line Extension Value without promotions

Scenario: Save collection order with Collegue Discount 
    Given Sale happened
    And 5 SKU 500300 sold
    And Collegue Discount amount for SKUs 500300 is 33.95
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    When BO received the transaction
    Then There is collection order in DB
    And Order Merchandise Value Store value is equal to Transaction Total amount
    And Order Line Price is equal to Line Extension Value without promotions

Scenario: Save collection order with same Event Discount for 1 line
    Given Sale happened
    And 5 SKU 500300 sold
    And DealGroup event with code '000587' is applied once to first line with saving amount for the first hit of 1.25 pounds
    And DealGroup event with code '000588' is applied once to first line with saving amount for the first hit of 0.75 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    When BO received the transaction
    Then There is collection order in DB
    And Order Merchandise Value Store value is equal to Transaction Total amount
    And Order Line Price is equal to Line Extension Value without promotions

Scenario: Save collection order with several Event Discount for 1 line
    Given Sale happened
    And 5 SKU 500300 sold
    And HierarchySpend event with code '000111' is applied once to first line with saving amount 1.25 pounds
    And Multibuy event with code '000222' is applied once to first line with saving amount 0.75 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    When BO received the transaction
    Then There is collection order in DB
    And Order Merchandise Value Store value is equal to Transaction Total amount
    And Order Line Price is equal to Line Extension Value without promotions

Scenario: Save collection order with several Event Discount for several lines with different SKU
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 SKU 500300 sold
    And HierarchySpend event with code '000111' is applied once to first line with saving amount 1.25 pounds
    And Multibuy event with code '000222' is applied once to first line with saving amount 0.75 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    When BO received the transaction
    Then There is collection order in DB
    And Order Merchandise Value Store value is equal to Transaction Total amount
    And Order Line Price is equal to Line Extension Value without promotions

Scenario: Save collection order with same Event Discount for 2 line with same SKUs
    Given Sale happened
    And 5 SKU 500300 sold
    And 1 SKU 500300 sold
    And HierarchySpend event with code '000111' is applied once to first line with saving amount 1.25 pounds
    And HierarchySpend event with code '000111' is applied once to second line with saving amount 0.25 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And 1 items for line 2 were collected instantly
    When BO received the transaction
    Then There is collection order in DB
    And Order Merchandise Value Store value is equal to Transaction Total amount
    And Order Line Price is equal to Line Extension Value without promotions

Scenario: Save collection order with DealGroup and Multibuy event discount and employee discount and price override were applied to a line
    Given Sale happened
    And 3 SKU 500300 was sold with VAT rate 20
    And Collegue Discount amount for SKUs 500300 is 17.4
    And Price for SKU 500300 was overriden by 30.00 pounds with reason code Managers Discretion
    And DealGroup event with code '000111' is applied once to first line with saving amount 2.00 pounds
    And Multibuy event with code '000222' is applied once to first line with saving amount 1.00 pounds
    And 1 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    When BO received the transaction
    Then There is collection order in DB
    And Order Merchandise Value Store value is equal to Transaction Total amount
    And Order Line Price is equal to Line Extension Value without promotions

Scenario: Save order with reversed lines
    Given Sale happened
    And Customer name is Vasily
    And 5 SKU 500300 sold
    And 2 SKU 100006 sold
    And Line for SKU 100006 is reversed
    And Was paid by cash for 169.85 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    When BO received the transaction
    Then There is an order in DB
    And There is line with 5 SKU 500300
    And There is no order lines for SKU 100006

Scenario: Order includes survey postcode information
    Given Sale happened
    And Customer name is Vasily
    And 5 SKU 500300 sold
    And Was paid by cash for 169.85 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And Survey Postcode is 'QQ11QQ'
    When BO received the transaction
    Then There is an order in DB
    And There is line with 5 SKU 500300
    And There is Customer Line with the sale Date, Till Number, Transaction Number and Postcode 'QQ11QQ' and other fields with default values

Scenario: Save delivery order with one refunded line
    Given Sale happened
    And 1 SKU 100006 were refunded
    And 5 SKU 500300 sold
    And 5 items for line 2 were delivered
    When BO received the transaction
    Then There is delivery order in DB
    And In previous order for line 1 quantity taken = -1; quantity to be delivered = 0; refund quantity = 0; total quantity = -1
    And Delivery status for SKU 100006 is 999
    And In previous order for line 2 quantity taken = 0; quantity to be delivered = 5; refund quantity = 0; total quantity = 5