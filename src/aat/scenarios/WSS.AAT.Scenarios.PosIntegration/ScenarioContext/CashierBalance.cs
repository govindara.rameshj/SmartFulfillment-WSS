using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.AAT.Scenarios.PosIntegration.ScenarioContext
{
    public class CashierBalance
    {
        public CashierBalance()
        {
            MiscIncomeCount = Enumerable.Repeat(0m, Global.MiscTypesCount + 1).ToArray();
            MiscIncomeValue = Enumerable.Repeat(0m, Global.MiscTypesCount + 1).ToArray();
            MiscOutCount = Enumerable.Repeat(0m, Global.MiscTypesCount + 1).ToArray();
            MiscOutValue = Enumerable.Repeat(0m, Global.MiscTypesCount + 1).ToArray();
        }

        public decimal GrossSalesAmount { get; set; }
        public decimal SalesAmount { get; set; }
        public int TransactionsCount { get; set; }
        public int NumLinesReversed { get; set; }
        public decimal RefundCount { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal RefundAmount { get; set; }
        public int NumOpenDrawer { get; set; }
        public decimal CashTenderAmount { get; set; }
        public decimal CashTenderVarAmount { get; set; }
        public decimal VisaCardTenderAmount { get; set; }
        public decimal VisaCardTenderVarAmount { get; set; }
        public int NumVoids { get; set; }
        public decimal RealCashierBalance { get; set; }
        public int NumCorrections { get; set; }

        public decimal[] MiscIncomeCount { get; set; }
        public decimal[] MiscIncomeValue { get; set; }
        public decimal[] MiscOutCount { get; set; }
        public decimal[] MiscOutValue { get; set; }
    }
}