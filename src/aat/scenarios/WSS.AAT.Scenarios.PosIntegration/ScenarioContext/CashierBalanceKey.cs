namespace WSS.AAT.Scenarios.PosIntegration.ScenarioContext
{
    public class CashierBalanceKey
    {
        public string CurrencyId { get; set; }
        public int PeriodId { get; set; }
        public int CashierId { get; set; }
    }
}