﻿using System;
using System.Collections.Generic;
using System.Linq;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions;
using WSS.BO.BOReceiverService.BL.Validation;

namespace WSS.AAT.Scenarios.PosIntegration.ScenarioContext
{
    public class TransactionScenarioContext
    {
        public TransactionScenarioContext()
        {
            SentTransactions = new List<Transaction>();
            SentTransactionKeys = new List<IDailyTillTranKey>();
        }
        
        internal TransactionBuilder TransactionBuilder { get; private set; }

        internal void StartNewTransaction(TransactionBuilder builder)
        {
            CashierBalanceBefore = null;
            TransactionBuilder = builder;
        }

        internal CashierBalance CashierBalanceBefore { get; private set; }
        internal bool IsEoDLockOccured { get; set; }
        internal bool IsPickupLockOccured { get; set; }
        internal bool IsBankingLockOccured { get; set; }
        internal BankingException BankingException { get; set; }
        internal EodLockException EodLockException { get; set; }
        internal PickupException PickupException { get; set; }
        internal ValidationException ValidationException { get; set; }
        internal DateTime TransactionSentDate { get; set; }
       
        internal Transaction FirstTransaction {
            get { return SentTransactions.First(); }
        }

        internal Transaction SentTransaction {
            get { return SentTransactions.Last(); }
        }

        internal IDailyTillTranKey FirstTransactionKey {
            get { return SentTransactionKeys.First(); }
        }

        internal IDailyTillTranKey SentTransactionKey {
            get { return SentTransactionKeys.Last(); }
        }

        internal List<Transaction> SentTransactions { get; private set; }
        internal List<IDailyTillTranKey> SentTransactionKeys { get; private set; } 

        internal void AfterSentTransaction(Transaction sentTransaction, IDailyTillTranKey sentTransactionKey, CashierBalance cashierBalanceBefore)
        {
            TransactionBuilder = null;

            SentTransactions.Add(sentTransaction);
            SentTransactionKeys.Add(sentTransactionKey);
            CashierBalanceBefore = cashierBalanceBefore;
        }
    }
}
