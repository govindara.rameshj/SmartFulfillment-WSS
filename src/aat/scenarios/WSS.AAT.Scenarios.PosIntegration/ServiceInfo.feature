﻿@PosIntegration
Feature: ServiceInfo
	In order to know the current status of the BOReceiverService instance
	As a support engineer
	I want to have the ability to get the most important information of the service installation and activity

Scenario: Get service information
	When I request information about the service
	Then It returns that it serves the store with number 8852
	And It returns that it serves the store with name "Test Store"
	And It returns version of the store database
	And It returns that the name of the service is "AAT service mock"
	And It returns that the version of the service is "1.2.3.4"
	And It returns current model version
