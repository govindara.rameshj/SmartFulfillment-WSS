﻿Feature: EventDiscounts
    In order to process event discounts correctly
    As a developer
    I should be able to deal with different event discounts scenarios

@EventDiscounts

Scenario: DealGroup event discount applied once to a line
    Given Sale happened
    And 5 SKU 500300 sold
    And DealGroup event with code '000111' is applied once to first line with saving amount 1.00 pounds
    And Was paid by cash for 168.85 pounds
    When BO received the transaction
    Then 'DealGroup' event code is '000111' for SKU '500300'
    And 'DealGroup' event saving is 1.00 pounds for SKU '500300'
    And Event saving for transaction is 1.00 pounds
    And 'DealGroup' event with saving is added to db

    Scenario: Multibuy event discount applied once to a line
    Given Sale happened
    And 5 SKU 500300 sold
    And Multibuy event with code '000222' is applied once to first line with saving amount 2.00 pounds
    And Was paid by cash for 167.85 pounds
    When BO received the transaction
    Then 'Multibuy' event code is '000222' for SKU '500300'
    And 'Multibuy' event saving is 2.00 pounds for SKU '500300'
    And Event saving for transaction is 2.00 pounds
    And 'Multibuy' event with saving is added to db

    Scenario: Quantity Break event discount applied once to a line
    Given There was a parameter in db containing Quantity Break event discount code '006666'
    And Sale happened
    And 5 SKU 500300 sold
    And QuantityBreak event is applied once to first line with saving amount 3.00 pounds
    And Was paid by cash for 166.85 pounds
    When BO received the transaction
    Then 'QuantityBreak' event code is '006666' for SKU '500300'
    And 'QuantityBreak' event saving is 3.00 pounds for SKU '500300'
    And Event saving for transaction is 3.00 pounds

    Scenario: Event discount applied twice to a line
    Given Sale happened
    And 5 SKU 500300 sold
    And DealGroup event with code '000111' is applied once to first line with saving amount for the first hit of 1.25 pounds
    And DealGroup event with code '000111' is applied once to first line with saving amount for the first hit of 0.75 pounds
    And Was paid by cash for 167.85 pounds
    When BO received the transaction
    Then 2 event records are added to db
    And event record with sequence number '000001' contains amount 1.25 pounds
    And event record with sequence number '000002' contains amount 0.75 pounds

    Scenario: Several event discounts of the same type applied to a line
    Given Sale happened
    And 5 SKU 500300 sold
    And DealGroup event with code '000587' is applied once to first line with saving amount for the first hit of 1.25 pounds
    And DealGroup event with code '000588' is applied once to first line with saving amount for the first hit of 0.75 pounds
    When BO received the transaction
    Then 'DealGroup' event code is '000588' for SKU '500300'
    And 'DealGroup' event saving is 2.00 pounds for SKU '500300'
    And Event saving for transaction is 2.00 pounds
    And 2 event records are added to db

    Scenario: Several event discounts of different types applied to a line
    Given Sale happened
    And 5 SKU 500300 sold
    And HierarchySpend event with code '000111' is applied once to first line with saving amount 1.25 pounds
    And Multibuy event with code '000222' is applied once to first line with saving amount 0.75 pounds
    When BO received the transaction
    Then Event saving for transaction is 2.00 pounds
    And event record with sequence number '000001' has type 'HierarchySpend'
    And event record with sequence number '000002' has type 'Multibuy'

    Scenario: Event discount applied to several lines
    Given Sale happened
    And 5 SKU 500300 sold
    And 1 SKU 100006 sold
    And Multibuy event with code '000111' is applied once to 500300 SKU with saving amount 1.25 pounds
    And Multibuy event with code '000111' is applied once to 100006 SKU with saving amount 1.25 pounds
    When BO received the transaction
    Then 2 event records are added to db
    And Both event records have sequence number '000001'

    Scenario: Event discount triggered by coupon
    Given Sale happened
    And 5 SKU 500300 sold
    And 1 SKU 100006 sold
    And Multibuy event with code '000111' is applied once to 500300 SKU with saving amount 1.25 pounds
    And Multibuy event with code '000111' is applied once to 100006 SKU with saving amount 1.25 pounds
    And Applied '000111' event was triggered by coupon with barcode '00000153333650441'
    When BO received the transaction
    Then 1 Coupon record is added to db
    And Coupon record has sequence number '0001' and coupon id '0000015'
    And The Coupon record Marketing Reference Number is '3333' and Coupon Serial Number is 650441

    Scenario: Event discount and price override discount were applied to a line
    Given Sale happened
    And 3 SKU 500300 was sold with VAT rate 20
    And Price for SKU 500300 was overriden by 30.00 pounds with reason code Managers Discretion
    And DealGroup event with code '000111' is applied once to first line with saving amount 2.00 pounds
    When BO received the transaction
    Then There is line for SKU 500300 with price equal 30.00, amount equal 90.00, discount equal 0.00, value added tax equal 14.67
    And Merchandise amount is equal 90.00, total amount is equal 88.00, total tax amount is equal 14.67, total discount amount is equal to 2.00, total value added amount of rate of 20 persents is equal 14.67
    And Initial price of SKU 500300 is equal to db price

    Scenario: DealGroup and Multibuy event discounts and price override discount were applied to a line
    Given Sale happened
    And 3 SKU 500300 was sold with VAT rate 20
    And Price for SKU 500300 was overriden by 30.00 pounds with reason code Managers Discretion
    And DealGroup event with code '000111' is applied once to first line with saving amount 2.00 pounds
    And Multibuy event with code '000222' is applied once to first line with saving amount 1.00 pounds
    When BO received the transaction
    Then There is line for SKU 500300 with price equal 30.00, amount equal 90.00, discount equal 0.00, value added tax equal 14.50
    And Merchandise amount is equal 90.00, total amount is equal 87.00, total tax amount is equal 14.50, total discount amount is equal to 3.00, total value added amount of rate of 20 persents is equal 14.50
    And Initial price of SKU 500300 is equal to db price

    Scenario: Event discount and employee discount were applied to a line
    Given Sale happened
    And 3 SKU 500300 was sold with VAT rate 20
    And Collegue Discount amount for SKUs 500300 is 19.78
    And DealGroup event with code '000111' is applied once to first line with saving amount 3.00 pounds
    When BO received the transaction
    Then There is line for SKU 500300 with price equal 27.18, amount equal 82.13, discount equal 6.59, value added tax equal 13.19
    And Merchandise amount is equal 101.91, total amount is equal 79.13, total tax amount is equal 13.19, total discount amount is equal to 22.78, total value added amount of rate of 20 persents is equal 13.19
    And Initial price of SKU 500300 is equal to db price

    Scenario: Deal group and multibuy event discounts and employee discount were applied to a line
    Given Sale happened
    And 2 SKU 500300 was sold with VAT rate 20
    And Collegue Discount amount for SKUs 500300 is 6.79
    And DealGroup event with code '000111' is applied once to first line with saving amount 2.00 pounds
    And Multibuy event with code '000222' is applied once to first line with saving amount 1.00 pounds
    When BO received the transaction
    Then There is line for SKU 500300 with price equal 30.57, amount equal 61.45, discount equal 3.39, value added tax equal 9.74
    And Merchandise amount is equal 67.94, total amount is equal 58.15, total tax amount is equal 9.74, total discount amount is equal to 9.79, total value added amount of rate of 20 persents is equal 9.74
    And Initial price of SKU 500300 is equal to db price
    And There is a 'HierarchySpend' event discount with amount 0.01 and code from database for SKU '500300'
    And 'DealGroup' event code is '000111' for SKU '500300'

    Scenario: Event discount and employee discount and price override were applied to a line
    Given Sale happened
    And 3 SKU 500300 was sold with VAT rate 20
    And Collegue Discount amount for SKUs 500300 is 17.61
    And Price for SKU 500300 was overriden by 30.00 pounds with reason code Managers Discretion
    And DealGroup event with code '000111' is applied once to first line with saving amount 2.00 pounds
    When BO received the transaction
    Then There is line for SKU 500300 with price equal 24.00, amount equal 72.4, discount equal 5.87, value added tax equal 11.73
    And Merchandise amount is equal 90.00, total amount is equal 70.39, total tax amount is equal 11.73, total discount amount is equal to 19.61, total value added amount of rate of 20 persents is equal 11.73
    And Initial price of SKU 500300 is equal to db price

    Scenario: DealGroup and Multibuy event discount and employee discount and price override were applied to a line
    Given Sale happened
    And 3 SKU 500300 was sold with VAT rate 20
    And Collegue Discount amount for SKUs 500300 is 17.4
    And Price for SKU 500300 was overriden by 30.00 pounds with reason code Managers Discretion
    And DealGroup event with code '000111' is applied once to first line with saving amount 2.00 pounds
    And Multibuy event with code '000222' is applied once to first line with saving amount 1.00 pounds
    When BO received the transaction
    Then There is line for SKU 500300 with price equal 24.00, amount equal 72.6, discount equal 5.8, value added tax equal 11.60
    And Merchandise amount is equal 90.00, total amount is equal 69.6, total tax amount is equal 11.60, total discount amount is equal to 20.4, total value added amount of rate of 20 persents is equal 11.60
    And Initial price of SKU 500300 is equal to db price

    Scenario: Employee discount and price override were applied to a line
    Given Sale happened
    And 3 SKU 500300 was sold with VAT rate 20
    And Collegue Discount amount for SKUs 500300 is 18.00
    And Price for SKU 500300 was overriden by 30.00 pounds with reason code Managers Discretion
    When BO received the transaction
    Then There is line for SKU 500300 with price equal 24.00, amount equal 72.00, discount equal 6.00, value added tax equal 12
    And Merchandise amount is equal 90.00, total amount is equal 72.00, total tax amount is equal 12, total discount amount is equal to 18.00, total value added amount of rate of 20 persents is equal 12
    And Initial price of SKU 500300 is equal to db price

