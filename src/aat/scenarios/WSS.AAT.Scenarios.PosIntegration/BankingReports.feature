﻿@soes
Feature: BankingReports
    In order to check that banking reports are correct
    I as a great developer
    Created tests for these scenarios

Scenario: Today's transaction that arrived after eod lock is in tomorrow's report
    Given Sale happened today
    And Database was empty
    And 1 SKU 500300 sold
    And 1 SKU 100006 sold
    And Was paid by cash for 329.97 pounds
    And Nightly routine procedure was started and finished
    When BO received the transaction
    And Banking report for tomorrow was created
    Then Banking report contains info for SKU 500300 with daily quantity 1 and daily total 33.97
    And Banking report contains info for SKU 100006 with daily quantity 1 and daily total 296.00
    And Banking report for tomorrow contains info for account 'Cash' with total 329.97

Scenario: Today's transaction that arrived after pickup lock but before banking is in today's report
    Given Sale happened today
    And Database was empty
    And 1 SKU 500300 sold
    And 1 SKU 100006 sold
    And Was paid by cash for 329.97 pounds
    And Pickup process for virtual cashier has finished
    And Banking process hasn't started
    When BO received the transaction
    And Banking report for today was created
    Then Banking report contains info for SKU 500300 with daily quantity 1 and daily total 33.97
    And Banking report contains info for SKU 100006 with daily quantity 1 and daily total 296.00
    And Banking report for today contains info for account 'Cash' with total 329.97

Scenario: Yesterday's transaction that arrived today is in today's report
    Given Sale happened yesterday
    And Database was empty
    And 5 SKU 500300 sold
    And 4 SKU 100006 sold
    And Was paid by cash for 1353.85 pounds
    When BO received the transaction
    And Banking report for today was created
    Then Banking report contains info for SKU 500300 with daily quantity 5 and daily total 169.85
    And Banking report contains info for SKU 100006 with daily quantity 4 and daily total 1184.00
    And Banking report for today contains info for account 'Cash' with total 1353.85
