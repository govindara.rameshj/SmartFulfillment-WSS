﻿@soes
Feature: BankingCalculationPeriod
    In order to check different transaction processing scenarios
    As a developer
    I write tests for transaction's interactions with different eod lock states 

Scenario: Today's transaction arrives before EOD start
    Given Sale happened today
    And Nightly routine procedure is not started
    When BO received the transaction
    Then There is sale in BO db 
    And Sale date equal today in BO db
    And Received date equal today in BO db
    And CBBU field is null in BO db
    And CashBalCashier dates are equal today

Scenario: Today's transaction arrives while EOD is executing
    Given Sale happened today
    And Nightly routine procedure is started
    When BO received the transaction
    Then EoD lock error happened

Scenario: Today's transaction arrives after EOD procedure was finished
    Given Sale happened today
    And Nightly routine procedure was started and finished
    When BO received the transaction
    Then There is sale in BO db
    And Sale date equal today in BO db
    And Received date equal today in BO db
    And CBBU field equal tomorrow
    And CashBalCashier dates are equal tomorrow

Scenario: Yesterday's transaction arrives today before EOD start
    Given Sale happened yesterday
    And Nightly routine procedure is not started
    When BO received the transaction
    Then There is sale in BO db
    And Sale date equal yesterday in BO db 
    And Received date equal today in BO db
    And CBBU field equal today
    And CashBalCashier dates are equal today







