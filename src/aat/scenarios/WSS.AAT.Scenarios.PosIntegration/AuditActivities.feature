﻿@PosIntegration
Feature: AuditActivities
    In order to check audit
    As a developer
    I implement test scenarios for audit events

Scenario: Save Login into Till event as transaction to ledger
    Given Login happened
    When BO received audit transaction
    Then There is login information in BO db
    And Transaction is marked as Online

Scenario: Save Logout into Till event as transaction to ledger
    Given Logout happened
    When BO received audit transaction
    Then There is logout information in BO db
    And Transaction is marked as Online