﻿@PosIntegration
Feature: DayOfChoice
    In order to provide DoC functionality
    As a developer
    I want to fill slot and fulfillers for delivery order from OVC

Scenario: Save default delivery slot for saturday
    Given new delivery order is created on Ovc Till
    And fulfillment slot is not set
    And delivery date is Saturday
    When BO received the transaction
    Then corresponding slot is saved to DB

Scenario: Save default delivery slot for weekday
    Given new delivery order is created on Ovc Till
    And fulfillment slot is not set
    And delivery date is Tuesday
    When BO received the transaction
    Then corresponding slot is saved to DB

Scenario Outline: Save delivery slot
    Given new delivery order is created on Ovc Till
    And fulfillment slot id is <Id>
    And description is <Description>
    And start time is <Start Time>
    And end time is <End Time>
    And fulfillment slot is set
    When BO received the transaction
    Then specified slot is saved to DB
    Examples: 
        | Id | Description   | Start Time | End Time |
        | 1  | Full day slot | 08:00      | 20:00    |
        | 2  | Morning slot  | 08:00      | 13:00    |
        | 3  | Saturday slot | 10:00      | 18:00    |

Scenario: Save fulfiller
    Given new delivery order is created on Ovc Till
    And fulfiller is set
    When BO received the transaction
    Then specified fulfiller is saved to DB