﻿@PosIntegration
Feature: Cancellation

 Scenario: Save refund transaction to ledger for order cancellation
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And BO received the transaction
    When Refund happened
    And 5 SKU 500300 were refunded with cancelled quantity = 2 corresponding to line with source item id 1 from last order
    And BO received the transaction
    Then There is refund for order in BO db
    And There is refunded line for 5 SKU 500300
    And There is refunded cash payment for 169.85
    And Original transaction values are filled in customer table
    And There is information about order cancellation for line 1 in previous order with returned quantity 3 and cancelled quantity 2
    And Transaction is marked as Online

    Scenario: Full cancellation of order
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 SKU 100006 sold with line number 2 and source item id equal 2
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And 2 items for line 2 were collected instantly
    And 1 items for line 2 were collected later with ordering
    And BO received the transaction
    When Refund happened
    And 5 SKU 500300 were refunded with cancelled quantity = 2 corresponding to line with source item id 1 from last order
    And 3 SKU 100006 were refunded with cancelled quantity = 1 corresponding to line with source item id 2 from last order
    And BO received the transaction
    Then In previous order for line 1 quantity taken = 0; quantity to be delivered = 0; refund quantity = 5; total quantity = 5
    And In previous order for line 2 quantity taken = 0; quantity to be delivered = 0; refund quantity = 3; total quantity = 3
    And In previous order total units quantity = 8; total quantity taken = 0; total refunded quantity = 8
    And Previous order has cancellation state 1

    Scenario: Partial cancellation of order
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 SKU 100006 sold with line number 2 and source item id equal 2
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And 2 items for line 2 were collected instantly
    And 1 items for line 2 were collected later with ordering
    And BO received the transaction
    When Refund happened
    And 2 SKU 500300 were refunded with cancelled quantity = 1 corresponding to line with source item id 1 from last order
    And 1 SKU 100006 were refunded with cancelled quantity = 1 corresponding to line with source item id 2 from last order
    And BO received the transaction
    Then In previous order for line 1 quantity taken = 2; quantity to be delivered = 1; refund quantity = 2; total quantity = 5 
    And In previous order for line 2 quantity taken = 2; quantity to be delivered = 0; refund quantity = 1; total quantity = 3
    And In previous order total units quantity = 8; total quantity taken = 4; total refunded quantity = 3
    And Previous order has cancellation state 2

    Scenario: Order cancellation with multiple lines with similar SKU numbers
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 SKU 100006 sold with line number 2 and source item id equal 2
    And 10 SKU 500300 sold with line number 3 and source item id equal 3
    And 5 items for line 1 were collected later with ordering
    And 2 items for line 2 were collected instantly
    And 1 items for line 2 were collected later with ordering
    And 5 items for line 3 were collected later with ordering
    And 5 items for line 3 were collected instantly
    And BO received the transaction
    When Refund happened
    And 5 SKU 500300 were refunded corresponding to line with source item id 3 from last order
    And 1 SKU 100006 were refunded with cancelled quantity = 1 corresponding to line with source item id 2 from last order
    And BO received the transaction
    Then In previous order for line 1 quantity taken = 0; quantity to be delivered = 5; refund quantity = 0; total quantity = 5
    And In previous order for line 2 quantity taken = 2; quantity to be delivered = 0; refund quantity = 1; total quantity = 3
    And In previous order for line 3 quantity taken = 0; quantity to be delivered = 5; refund quantity = 5; total quantity = 10
    And In previous order total units quantity = 18; total quantity taken = 2; total refunded quantity = 6

    Scenario: Order cancellation for delivered order
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 10 SKU 500300 sold with line number 2 and source item id equal 2
    And 5 items for line 1 were collected later with ordering
    And 5 items for line 2 were collected later with ordering
    And 5 items for line 2 were collected instantly
    And BO received the transaction
    When Refund happened
    And Previous order has been delivered
    And 5 SKU 500300 were refunded corresponding to line with source item id 1 from last order
    And 7 SKU 100006 were refunded with cancelled quantity = 2 corresponding to line with source item id 2 from last order
    And BO received the transaction
    Then In previous order for line 1 quantity taken = 0; quantity to be delivered = 5; refund quantity = 0; total quantity = 5
    And In previous order for line 2 quantity taken = 5; quantity to be delivered = 5; refund quantity = 0; total quantity = 10
    And In previous order total units quantity = 15; total quantity taken = 5; total refunded quantity = 0

    Scenario: Save delivery order with one cancelled line
    Given Sale happened yesterday
    And 1 SKU 100006 sold
    And 1 items for line 1 were delivered
    And BO received the transaction
    And Refund happened
    And 1 SKU 100006 were refunded with cancelled quantity = 1 corresponding to line with source item id 1 from last order
    And 5 SKU 500300 sold
    And 5 items for line 2 were delivered
    When BO received the transaction
    Then In first order for line 1 quantity taken = 0; quantity to be delivered = 0; refund quantity = 1; total quantity = 1
    And There is delivery order in DB
    And In previous order for line 1 quantity taken = -1; quantity to be delivered = 0; refund quantity = 0; total quantity = -1
    And Delivery status for SKU 100006 is 999
    And In previous order for line 2 quantity taken = 0; quantity to be delivered = 5; refund quantity = 0; total quantity = 5

    Scenario: Cancellation of order that does not exist in db
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And BO received the transaction
    And Data base was cleaned up
    And Refund happened
    And 2 SKU 500300 were refunded with cancelled quantity = 1 corresponding to line with source item id 1 from last order
    When BO received the transaction
    Then There is refund in BO db
    And There is refunded line for 2 SKU 500300
    And Original transaction values are empty in customer table
