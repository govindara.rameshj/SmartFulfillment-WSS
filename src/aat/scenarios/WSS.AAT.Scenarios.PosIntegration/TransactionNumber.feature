﻿@PosIntegration
Feature: TransactionNumber

Scenario: Transaction number for sale transaction
    Given Sale happened
    And Till number was 01
    And Sale transaction number was 0001
    And Source was OVC
    And Next transaction number for till 01 is 5555
    And Source transaction ID was '585f7411-c468-4c47-956d-a1d654a92a1f'
    And Source transaction Receipt Barcode was '111222333444555'
    And 5 SKU 500300 sold
    When BO received the transaction
    Then There is sale in BO db
    And The sale source is OVC
    And The sale source transaction Id is '585f7411-c468-4c47-956d-a1d654a92a1f'
    And The sale source transaction number is 0001
    And The Receipt Barcode number is '111222333444555'
    And The sale transaction number is 5555

Scenario: Transaction number overflow
    Given Next transaction number for till 01 is 9999
    And Sale happened
    And Till number was 01
    When BO received the transaction
    Then The sale transaction number is 9999
    When Another sale happened
    And Till number was 01
    When BO received the transaction
    Then The sale transaction number is 5001
