﻿@soes
Feature: ReverseMapping
    In order to check reverse mapping functionality
    As a developer who implemented this feature
    I create some test scenarios

#Header restoration
    Scenario: Restored transaction has correct header
    Given Sale happened
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered transaction header has similar values with original transaction

    Scenario: Receiver throws error if transaction was not found
    Given Database doesn't contain any transactions
    When Reverse mapping for transaction with TranId='1', TillId='1', Date='1990-10-10' was requested
    Then Exception has been thrown

    Scenario: Receiver throws error if Transaction is parked
    Given Sale happened
    And Transaction was parked
    And Customer name is Vasily
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Exception has been thrown

    Scenario: Receiver throws error if Transaction is training
    Given Sale happened
    And Transaction was training
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Exception has been thrown

    Scenario: Receiver throws error if Transaction is voided
    Given Sale happened
    And Transaction was voided
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Exception has been thrown

#Items restoration
    Scenario: Restored transaction has correct item values
    Given Sale happened
    And 5 SKU 500300 sold
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered item has similar values with original item

    Scenario: Restored legacy sale transaction has Source Item Id equal to Daily Line Sequence Number
    Given Sale happened
    And 5 SKU 500300 sold
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    When Reverse mapping for last transaction was requested
    Then Recovered Source Item Id is equal to '1'

    Scenario: Restored transaction has correct gift card product values
    Given Sale happened
    And A gift card was topped up with 100.00 pounds
    And Was paid by cash for 100.00 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered gift card product has similar values with original product

    Scenario: Sale includes information about lines reverted in transaction
    Given Sale happened
    And 5 SKU 500300 sold
    And 1 SKU 100006 sold
    And Stock for 500300 was 55
    And Line for SKU 500300 is reversed
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There is no item with SKU 500300 in restored transaction

#Payments restoration
    Scenario: Restored transaction has correct cash payment values
    Given Sale happened
    And 5 SKU 500300 sold
    And Was paid by cash for 169.85 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered cash payment has similar values with original payment

    Scenario: Restored transaction has correct cheque payment values
    Given Sale happened
    And 5 SKU 500300 sold
    And Was paid by cheque for 169.85 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered cheque payment has similar values with original payment

    Scenario: Restored transaction has correct eft payment values
    Given Sale happened
    And 5 SKU 500300 sold
    And Was paid by Visa credit card for 169.85 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered eft payment has similar values with original payment

    Scenario: Restored transaction has correct cash change payment values
    Given Sale happened
    And 5 SKU 500300 sold
    And Was paid by cash for 200 pounds
    And Cash change was executed for 30.15 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered cash payment has similar values with original payment
    And Recovered cash change payment has similar values with original payment

    Scenario: Restored transaction has correct gift card payment values
    Given Sale happened
    And 5 SKU 500300 sold
    And Was paid by GiftCard for 169.85 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered gift card payment has similar values with original payment

    Scenario: Restored transaction has correct values for multiple payments
    Given Sale happened
    And 5 SKU 500300 sold
    And Was paid by cash for 100.00 pounds
    And Was paid by Visa credit card for 30.00 pounds
    And Was paid by cheque for 39.85 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered cash payment has similar values with original payment
    And Recovered eft payment has similar values with original payment
    And Recovered cheque payment has similar values with original payment

    Scenario: Restored transaction has vatrate value taken from dltots, not from retopt
    Given Sale happened
    And 5 SKU 500300 sold
    And BO received the transaction
    And VatRate1 for previous transaction was equal 25
    When Reverse mapping for last transaction was requested
    Then Recovered product item has VatRate equal 25

    Scenario: Restored sale transaction has correct vat value
    Given Sale happened
    And 5 SKU 500300 was sold with VAT rate 20
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Absolute Vat Amount of Recovered item for SKU 500300 is equal to 28.31

#Order restoration
    Scenario: Restored transaction for simple delivery order has correct values
    Given Sale happened
    And Customer name is Vasily
    And 5 SKU 500300 sold
    And Was paid by cash for 169.85 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered instant fulfillment has similar values with original instant fulfillment
    And Recovered delivery fulfillment has similar values with original delivery fulfillment

    Scenario: Restored transaction for simple collection order has correct fulfillment transaction item indexes
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And 2 SKU 100006 sold
    And 2 items for line 2 were collected later with ordering
    And BO received the transaction
    And Order Line number for SKU 100006 was changed from 0002 to 0003
    When Reverse mapping for last transaction was requested
    Then Recovered instant fulfillment has similar values with original instant fulfillment
    And Recovered collection fulfillment has similar values with original collection fulfillment

    Scenario: Restored transaction for simple collection order has correct values
    Given Sale happened
    And Customer name is Vasily
    And 5 SKU 500300 sold
    And Was paid by cash for 169.85 pounds
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered instant fulfillment has similar values with original instant fulfillment
    And Recovered collection fulfillment has similar values with original collection fulfillment

    Scenario: Recover order with two customer contacts
    Given Sale happened
    And Customer name is Vasily
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And Customer provides destination details that are different from his own address
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered transaction has two contacts with correct values

    Scenario: Recover order with multiple lines
    Given Sale happened
    And 5 SKU 500300 sold
    And 2 SKU 100006 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And 2 items for line 2 were collected instantly
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered instant fulfillment has similar values with original instant fulfillment
    And Recovered delivery fulfillment has similar values with original delivery fulfillment

    Scenario: Recover order with delivery charge and delivery instruction
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    And Delivery Charge was 25.00
    And Delivery Instruction was "Deliver only after 2 p.m."
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered delivery fulfillment has similar values with original delivery fulfillment
    And Recovered transaction has delivery charge item as in original transaction

    Scenario: Restored transaction for simple collection order with Customer Order Header table Quantity Taken equal to zero
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And 2 SKU 100006 sold
    And 2 items for line 2 were collected later with ordering
    And BO received the transaction
    And Customer Order Header table Quantity Taken was set to zero
    When Reverse mapping for last transaction was requested
    Then Recovered instant fulfillment has similar values with original instant fulfillment
    And Recovered collection fulfillment has similar values with original collection fulfillment

#Refund restoration
    Scenario: Recover simple refund without receipt
    Given Refund happened
    And 5 SKU 500300 were refunded with reason 'Parts Missing'
    And Was refunded by cash for 169.85
    And Customer name is Vasily
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered refund transaction has similar values with original refund
    And Recovered Refund Transaction Item has Description 'Parts Missing'

    Scenario: Recover refund with receipt
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then Recovered refund transaction has correct reference to original transaction

    Scenario: Recover sale transaction with related refund transaction
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has related refund transaction in it

    Scenario: Recover sale transaction with two related refund transactions
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the first transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 1 SKU 500300 from the first transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has two related refund transactions in it

    Scenario: Recover sale for with several refunds with several rerurned products
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And 3 SKU 100006 sold with Source Item Id = '2'
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And 3 SKU 100006 from the previous transaction were refunded with Refund Transaction Item Id = '2'
    And BO received the transaction
    And Refund happened
    And 3 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has two related refund transactions in it

    Scenario: Recover sale with second line refunded for legacy transactions
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And 3 SKU 100006 sold with Source Item Id = '2'
    And BO received the transaction
    And Refund happened
    And 2 SKU 100006 from the previous transaction were refunded with Refund Transaction Item Id = '2'
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    And Second transaction was legacy transaction(SourceTranId and Source fields are empty)
    And Original Line Number for second transaction is NULL
    When Reverse mapping for first transaction was requested
    Then Related Transaction Refund Transaction Item Id for SKU 100006 = '2'

    Scenario: Recover sale with second line refunded for legacy sale and not legacy refund
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And 3 SKU 100006 sold with Source Item Id = '2'
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    And Refund happened
    And 2 SKU 100006 from the first legacy transaction were refunded with Refunded Transaction Item Id = '02'
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Related Transaction Refund Transaction Item Id for SKU 100006 = '2'

    Scenario: Recover sale with all the same Sku Numbers second line refunded for legacy sale and not legacy refund
    Given Sale happened yesterday
    And 1 SKU 500300 sold with Source Item Id = '1'
    And 1 SKU 500300 sold with Source Item Id = '2'
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    And Refund happened
    And 1 SKU 500300 from the first legacy transaction were refunded with Refunded Transaction Item Id = '02'
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Related Transaction Refund Transaction Item Id for SKU 500300 = '2'

    Scenario: Recover sale with second line refunded
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And 3 SKU 100006 sold with Source Item Id = '2'
    And BO received the transaction
    And Refund happened
    And 2 SKU 100006 from the previous transaction were refunded with Refund Transaction Item Id = '2'
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Related Transaction Refund Transaction Item Id for SKU 100006 = '2'

    Scenario: Restored transaction and its' related transaction has correct legacy SourceTranId and Source and RefundedTranId
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the first transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has correct Source ans SourceTranId
    And Restored refund line for SKU 500300 has correct legacy reference to the first transaction

    Scenario: Restored refund for legacy sale transaction has RefundedTranId only for lines from original sale transaction, not for lines without receipt
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the first transaction were refunded with Refund Transaction Item Id = '1'
    And 1 SKU 100006 were refunded
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    When Reverse mapping for first transaction was requested
    Then Restored refund line for SKU 500300 has correct legacy reference to the first transaction
    And Restored refund line for SKU 100006 has empty reference

    Scenario: Restored refund lines has references on two different sales
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    And BO received the transaction
    And Sale happened yesterday
    And 5 SKU 100006 sold
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the first transaction were refunded with Refund Transaction Item Id = '1'
    And 2 SKU 100006 from the second transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    And Second transaction was legacy transaction(SourceTranId and Source fields are empty)
    When Reverse mapping for first transaction was requested
    Then Restored refund line for SKU 500300 has correct legacy reference to the first transaction
    And Restored refund line for SKU 100006 has correct legacy reference to the second transaction

    Scenario: Restored refund lines has legacy reference for legacy transactions and new reference for new transactions
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    And BO received the transaction
    And Sale happened yesterday
    And 5 SKU 100006 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the first transaction were refunded with Refund Transaction Item Id = '1'
    And 2 SKU 100006 from the second transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    When Reverse mapping for first transaction was requested
    Then Restored refund line for SKU 500300 has correct legacy reference to the first transaction
    And Restored refund line for SKU 100006 has correct new reference to the second transaction

    Scenario: Restore 3 transactions that are linked one to another
    Given Sale happened
    And 5 SKU 500300 sold
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the first transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 1 SKU 500300 from the second transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Restored transactions has correct linking to each other

    Scenario: BO receives refund transaction with legacy refundedTranId
    Given Sale happened
    And 1 SKU 500300 sold
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    And Refund happened
    And 2 SKU 500300 from the first legacy transaction were refunded with Refunded Transaction Item Id = '01'
    When BO received the transaction
    Then There is refund in BO db with correct references to the legacy transaction

    Scenario: Restore collegue discount
    Given Sale happened
    And 1 SKU 500300 sold
    And Collegue Discount amount for SKUs 500300 is 6.79
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There is collegue discount in the restored transaction

    Scenario: Recover refund with Unknown Reason
    Given Refund happened
    And 5 SKU 500300 were refunded with reason 'Parts Missing'
    And Was refunded by cash for 169.85
    And Customer name is Vasily
    And BO received the transaction
    And In db Refund Reason Code was changed to Wrong Item
    When Reverse mapping for last transaction was requested
    Then Recovered Refund Transaction Item has Refund Reason Code Unknown
    And Recovered Refund Transaction Item has Description 'Wrong Item'

#Event discounts restoration
    Scenario: Restore single event discount
    Given Sale happened
    And 5 SKU 500300 sold
    And DealGroup event with code '000111' is applied once to first line with saving amount 1.00 pounds
    And Was paid by cash for 168.85 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There are 1 restored events in transaction for SKU '500300'
    And There is 'DealGroup' discount event for SKU = '500300' with saving amount '1.00' and code '000111'

    Scenario: Restore discount that was applied twice to the same line
    Given Sale happened
    And 5 SKU 500300 sold
    And DealGroup event with code '000111' is applied once to first line with saving amount 1.00 pounds
    And DealGroup event with code '000111' is applied once to first line with saving amount 2.00 pounds
    And Was paid by cash for 166.85 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There are 1 restored events in transaction for SKU '500300'
    And There are two discount hits in restored DealGroup event for SKU '500300' with code = '000111' with first amount = 1.00 and second amount = 2.00

    Scenario: Restore several event discounts of the same type applied to the same line
    Given Sale happened
    And 5 SKU 500300 sold
    And DealGroup event with code '000587' is applied once to first line with saving amount for the first hit of 1.25 pounds
    And DealGroup event with code '000588' is applied once to first line with saving amount for the first hit of 0.75 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There are 1 restored events in transaction for SKU '500300'
    And There are two discount hits in restored DealGroup event for SKU '500300' with code = '000588' with first amount = 1.25 and second amount = 0.75

    Scenario: Restore several event discounts of different types applied to the same line
    Given Sale happened
    And 5 SKU 500300 sold
    And DealGroup event with code '000111' is applied once to first line with saving amount 1.00 pounds
    And Multibuy event with code '000222' is applied once to first line with saving amount 2.00 pounds
    And Was paid by cash for 166.85 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There are 2 restored events in transaction for SKU '500300'
    And There is 'DealGroup' discount event for SKU = '500300' with saving amount '1.00' and code '000111'
    And There is 'Multibuy' discount event for SKU = '500300' with saving amount '2.00' and code '000222'

    Scenario: Restore same event discount applied to several lines
    Given Sale happened
    And 5 SKU 500300 sold
    And 1 SKU 100006 sold
    And Multibuy event with code '000111' is applied once to 500300 SKU with saving amount 1.25 pounds
    And Multibuy event with code '000111' is applied once to 100006 SKU with saving amount 1.25 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There are 1 restored events in transaction for SKU '500300'
    And There are 1 restored events in transaction for SKU '100006'
    And There is 'Multibuy' discount event for SKU = '500300' with saving amount '1.25' and code '000111'
    And There is 'Multibuy' discount event for SKU = '100006' with saving amount '1.25' and code '000111'

#Coupon restoration
    Scenario: Restore discount with coupon
    Given Sale happened
    And 5 SKU 500300 sold
    And Multibuy event with code '000111' is applied once to 500300 SKU with saving amount 1.25 pounds
    And Applied '000111' event was triggered by coupon with barcode '00000153333456789'
    And There is master event for code '000111', type 'Multibuy' and coupon '0000015' in database
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There is coupon with code = '000111', barcode = '0000015', event type = 'Multibuy'

#Price override event
    Scenario: Restore simple Price Override
    Given Sale happened
    And 2 SKU 100006 sold
    And Price Override with code Damaged Goods with Amount 60.00 was applied to SKU 100006
    And Was paid by cash for 532.00 pounds
    And BO received the transaction
    Then There is sale in BO db
    When Reverse mapping for last transaction was requested
    Then There is only one price override discount in the restored transaction with Reason Code 'Damaged Goods'
    And The Price Override Discount amount is 60.00

    Scenario: Restore simple Price Override for refund
    Given Refund happened
    And 2 SKU 100006 were refunded
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Refund Price Override
    When BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There is only one price override discount in the restored transaction with Reason Code 'Refund Price Difference'
    And The Price Override Discount amount is 192.00

    Scenario: Restore sale with Price Match or Promise price override
    Given Sale happened
    And 2 SKU 100006 sold
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Price Match or Promise and Competitor Name 'Jewson'
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There is only one Price Match or Promise price override discount in the restored transaction

    Scenario: Restore sale with Unknown price override
    Given Sale happened
    And 2 SKU 100006 sold
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Damaged Goods
    And BO received the transaction
    And In db Price Override Code for SKU 100006 was changed to Incorrect Day Price
    When Reverse mapping for last transaction was requested
    Then There is price override discount in the restored transaction with Reason Code Unknown
    And Description of Price Override Discount is 'Incorrect Day Price'

    Scenario: Restore a Sale which includes information about transaction with several price overrides
    Given Sale happened
    And 2 SKU 100006 sold
    And Price Override with code Manager Discretion with Amount 60.00 was applied to SKU 100006
    And Price Override with code Damaged Goods with Amount 30.00 was applied to SKU 100006
    And Was paid by cash for 502.00 pounds
    And BO received the transaction
    Then There is sale in BO db
    When Reverse mapping for last transaction was requested
    Then There is only one price override discount in the restored transaction with Reason Code 'Damaged Goods'
    And The Price Override Discount amount is 90.00

    Scenario: Restore a Sale which includes information about Price Override and Price Match Or Promise Discount
    Given Sale happened
    And 2 SKU 100006 sold
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Price Match or Promise and Competitor Name 'Jewson'
    And Price Override with amount 30 was applied to SKU 100006
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There is only one Price Match or Promise price override discount in the restored transaction
    And The Price Match discount amount is 222.00
    And There is the same data in the Price Match discount as in original discount

#Receipt Barcode
    Scenario: BO receives refund transaction with legacy refundedTranId and returns correct Receipt Barcode
    Given Sale happened
    And 1 SKU 500300 sold
    And Next transaction number for till 01 is 5555
    And BO received the transaction
    And First transaction was legacy transaction(SourceTranId and Source fields are empty)
    And Refund happened
    And 2 SKU 500300 from the first legacy transaction were refunded with Refunded Transaction Item Id = '01'
    When Reverse mapping for last transaction was requested
    Then Returned barcode contains information about Store Number '852', Till '01', Transaction Number '5555' and Today Date

# Promotions combinations
    Scenario: Restore a Sale which includes information about transaction price override with indivisible remainder
    Given Sale happened
    And 3 SKU 100006 sold
    And Price Override with code Damaged Goods with Amount 200.00 was applied to SKU 100006
    And Was paid by cash for 688.00 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There is sale in BO db
    And There is only one price override discount in the restored transaction with Reason Code 'Damaged Goods'
    And The Price Override Discount amount is 199.98
    And There is 'HierarchySpend' discount event for SKU = '100006' with saving amount '0.02' and code from db

    Scenario: Sale includes information about transaction price override with indivisible remainder that is complement by Employee Discount
    Given Sale happened
    And 3 SKU 100006 sold
    And Price Override with code Damaged Goods with Amount 200.00 was applied to SKU 100006
    And Collegue Discount amount for SKUs 100006 is 221.02
    And Was paid by cash for 688.00 pounds
    And BO received the transaction
    When Reverse mapping for last transaction was requested
    Then There is sale in BO db
    And There is only one price override discount in the restored transaction with Reason Code 'Damaged Goods'
    And The Price Override Discount amount is 200.01
    And There is collegue discount in the restored transaction
    And There is no Event Discount in the restored transaction

#Cancellations

Scenario: Recover order with full cancellation
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 SKU 100006 sold with line number 2 and source item id equal 2
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And 2 items for line 2 were collected instantly
    And 1 items for line 2 were collected later with ordering
    And BO received the transaction
    And Refund happened
    And 5 SKU 500300 were refunded with cancelled quantity = 2 corresponding to line with source item id 1 from last order
    And 3 SKU 100006 were refunded with cancelled quantity = 1 corresponding to line with source item id 2 from last order
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has related refund transaction in it
    And Related Transaction Refund Transaction has cancellation
    And There is a fulfillment with source item id equal 1 and quantity equal 2
    And There is a fulfillment with source item id equal 2 and quantity equal 1

Scenario: Recover order with partial cancellation
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 SKU 100006 sold with line number 2 and source item id equal 2
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And 2 items for line 2 were collected instantly
    And 1 items for line 2 were collected later with ordering
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 were refunded with cancelled quantity = 1 corresponding to line with source item id 1 from last order
    And 1 SKU 100006 were refunded with cancelled quantity = 1 corresponding to line with source item id 2 from last order
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has related refund transaction in it
    And Related Transaction Refund Transaction has cancellation
    And There is a fulfillment with source item id equal 1 and quantity equal 1
    And There is a fulfillment with source item id equal 2 and quantity equal 1

Scenario: Recover order with cancellation of multiple lines with similar SKU numbers
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 SKU 100006 sold with line number 2 and source item id equal 2
    And 10 SKU 500300 sold with line number 3 and source item id equal 3
    And 5 items for line 1 were collected later with ordering
    And 2 items for line 2 were collected instantly
    And 1 items for line 2 were collected later with ordering
    And 5 items for line 3 were collected later with ordering
    And 5 items for line 3 were collected instantly
    And BO received the transaction
    And Refund happened
    And 1 SKU 500300 were refunded with cancelled quantity = 5 corresponding to line with source item id 3 from last order
    And 1 SKU 100006 were refunded with cancelled quantity = 1 corresponding to line with source item id 2 from last order
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has related refund transaction in it
    And Related Transaction Refund Transaction has cancellation
    And There is a fulfillment with source item id equal 3 and quantity equal 5
    And There is a fulfillment with source item id equal 2 and quantity equal 1

Scenario: Recover order with related positive exchange transaction with partial cancellation
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 SKU 100006 sold with line number 2 and source item id equal 2
    And 5 items for line 1 were collected later with ordering
    And 2 items for line 2 were collected instantly
    And 1 items for line 2 were collected later with ordering
    And 5 items for line 3 were collected later with ordering
    And 5 items for line 3 were collected instantly
    And BO received the transaction
    And Sale happened
    And 1 SKU 100006 were refunded with cancelled quantity = 2 corresponding to line with source item id 1 from last order
    And 1 SKU 500300 sold
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has related refund transaction in it
    And Related Transaction Refund Transaction has cancellation
    And There is a fulfillment with source item id equal 1 and quantity equal 2
    And There is no fulfillment with source item id equal 2

Scenario: Recover order with related negative exchange transaction with partial cancellation
    Given Sale happened yesterday
    And 5 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 SKU 100006 sold with line number 2 and source item id equal 2
    And 5 items for line 1 were collected later with ordering
    And 2 items for line 2 were collected instantly
    And 1 items for line 2 were collected later with ordering
    And BO received the transaction
    And Refund happened
    And 1 SKU 100006 were refunded with cancelled quantity = 2 corresponding to line with source item id 2 from last order
    And 1 SKU 500300 sold
    And BO received the transaction
    When Reverse mapping for first transaction was requested
    Then Recovered sale transaction has related refund transaction in it
    And Related Transaction Refund Transaction has cancellation
    And There is a fulfillment with source item id equal 2 and quantity equal 2
    And There is no fulfillment with source item id equal 1