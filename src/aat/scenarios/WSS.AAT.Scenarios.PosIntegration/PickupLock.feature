﻿@soes
Feature: PickupLock
    In order to check pickup lock scenarios
    As a developer
    I created tests for different states of lock

Scenario: Today's transaction arrives before pickup start
    Given Sale happened today
    And Pickup process for virtual cashier is not started
    When BO received the transaction
    Then There is sale in BO db 
    And Sale date equal today in BO db
    And Received date equal today in BO db
    And CBBU field is null in BO db
    And CashBalCashier dates are equal today

Scenario: Today's transaction arrives while pickup is executing
    Given Sale happened today
    And Pickup process for virtual cashier is started
    When BO received the transaction
    Then Pickup lock error happened

Scenario: Today's transaction arrives after pickup has finished
    Given Sale happened today
    And Pickup process for virtual cashier has finished
    When BO received the transaction
    Then There is sale in BO db
    And Sale date equal today in BO db
    And Received date equal today in BO db
    And CBBU field is null in BO db
    And CashBalCashier dates are equal today

Scenario: Yesterday's transaction arrives today before pickup start
    Given Sale happened yesterday
    And Pickup process for virtual cashier is not started
    When BO received the transaction
    Then There is sale in BO db
    And Sale date equal yesterday in BO db 
    And Received date equal today in BO db
    And CBBU field equal today
    And CashBalCashier dates are equal today
