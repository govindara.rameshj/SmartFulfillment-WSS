﻿@PosIntegration
Feature: CashBal

Scenario: Refund updates cashier balance
    Given Refund happened
    And Till number was 1
    And 1 SKU 500300 were refunded
    And Was refunded by cash for 33.97
    When BO received the transaction
    Then Total till 1 virtual cashier balance changed by -33.97
    And Till 1 virtual cashier balance for cash changed by -33.97
    And Till 1 virtual cashier refund balance changed by -33.97
    And Till 1 virtual cashier sale balance changed by 0

Scenario: Sale updates virtual cashier balance 
    Given Sale happened yesterday
    And Was paid by cash for 169.85 pounds
    And Till number was 1
    When BO received the transaction
    Then Balance on yesterday for till 1 virtual cashier increased by 169.85

Scenario: Sale does not update real cashier balance 
    Given Sale happened
    And Cashier was 045
    When BO received the transaction
    Then There is a new sale in BO db for cashier 045
    And Cashier 045 balance total amount changed by 0

Scenario: Sale with colleague's discount updates cashier balance
    Given Sale happened
    And Till number was 1
    And 1 SKU 500300 sold
    And Collegue Discount amount for SKUs 500300 is 6.79
    And Was paid by cash for 27.18 pounds
    When BO received the transaction
    Then Till 1 virtual cashier discount balance changed by 6.79

Scenario: Paid out updates cashier balance
    Given Paid Out transaction happened 
    And The reason was Counsil Account
    And Was refunded by cash for 169.85
    When BO received the transaction
    Then The sale reason code is 09
    And Till virtual cashier balance decreased by 169.85
    And Number Of Transactions increased by one
    And Misc Out count for the till virtual cashier increased by one and value decreased by 169.85 for reason code 09
    And Till virtual cashier balance tenders decreased by 169.85

Scenario: Misc in updates cashier balance
    Given Misc Income transaction happened 
    And The reason was Safe Overs
    And Was paid by cash for 169.85 pounds
    When BO received the transaction
    Then The sale reason code is 05
    And Till virtual cashier balance increased by 169.85
    And Number Of Transactions increased by one
    And Misc Income count for the till virtual cashier increased by one and value increased by 169.85 for reason code 05
    And Till virtual cashier balance tenders increased by 169.85

Scenario: Misc in corrections updates cashier balance
    Given Misc Income Correction transaction happened 
    And The reason was Safe Overs
    And Was refunded by cash for 169.85
    When BO received the transaction
    Then The sale reason code is 05
    And Till virtual cashier balance decreased by 169.85
    And Number of Corrections increased by one
    And Misc Income count for the till virtual cashier decreased by one and value decreased by 169.85 for reason code 05
    And Till virtual cashier balance tenders decreased by 169.85

Scenario: Paid out corrections updates cashier balance
    Given Paid Out Correction transaction happened 
    And The reason was Counsil Account
    And Was paid by cash for 169.85 pounds
    When BO received the transaction
    Then The sale reason code is 09
    And Till virtual cashier balance increased by 169.85
    And Number of Corrections increased by one
    And Misc Out count for the till virtual cashier decreased by one and value increased by 169.85 for reason code 09
    And Till virtual cashier balance tenders increased by 169.85