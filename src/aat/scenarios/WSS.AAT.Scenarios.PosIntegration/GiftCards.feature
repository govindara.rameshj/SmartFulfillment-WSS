﻿@PosIntegration
Feature: GiftCards

Scenario: Sale includes information about gift card sale or topup
    Given Sale happened
    And Till number was 1
    And A gift card was topped up with 100.00 pounds
    And For gift card stock was 0
    And Was paid by cash for 100.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is a generated Refund
    And There is a generated Misc Income in BO db with reason code 12 and description 'Gift Card'
    And There is cash payment for 100.00 pounds
    And There is line with 1 Gift Card SKU for sale
    And For gift card stock is 0
    And Gift card amount in db is -100.00
    And Record is not added to Stock Log for Gift Card
    And There is line for generated refund with 1 Gift Card
    And Total till 1 virtual cashier balance changed by 100.00
    And Till 1 virtual cashier balance for cash changed by 100.00
    And Till 1 virtual cashier Misc Income count changed by 1 and value changed by 100.00 for reason code 12
    And Till 1 virtual cashier refund count changed by 1
    And There is information about GiftCard in DB for Gift Card top up

Scenario: Sale includes information about gift card sale or topup with zero amount
    Given Sale happened
    And Till number was 1
    And A gift card was topped up with 0.00 pounds
    And Was paid by cash for 10.00 pounds
    And Was refunded by cash for 10.00
    When BO received the transaction
    Then There is only a sale in BO db
    And There is cash payment for 10.00 pounds
    And There is refunded cash payment for giftcard for 10.00
    And There is line with 1 Gift Card SKU for sale

Scenario: Refund includes information about gift topup as a result of refund
    Given Refund happened
    And Till number was 1
    And 5 SKU 500300 were refunded
    And Was refunded by giftcard for 169.85
    When BO received the transaction
    Then There is refund in BO db
    And There is refunded cash payment for 169.85
    And There is a generated Misc Income in BO db with reason code 12 and description 'Gift Card'
    And Gift card amount in db is -169.85
    And Total till 1 virtual cashier balance changed by 0
    And Till 1 virtual cashier balance for cash changed by 0
    And Till 1 virtual cashier refund balance changed by -169.85
    And Till 1 virtual cashier refund count changed by 1
    And Till 1 virtual cashier Misc Income count changed by 1 and value changed by 169.85 for reason code 12
    And There is information about GiftCard in DB for Gift Card top up

Scenario: Sale includes information about gift card sale or topup with another items in transaction
    Given Sale happened
    And Till number was 1
    And 5 SKU 500300 sold
    And  A gift card was topped up with 100.00 pounds	
    And For gift card stock was 0
    And Was paid by cash for 80.00 pounds
    And Was paid by Visa credit card for 189.85 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is a generated Refund
    And There is a generated Misc Income in BO db with reason code 12 and description 'Gift Card'
    And There is cash payment for 80.00 pounds
    And There is Visa card payment for 189.85 pounds
    And There is cash payment for 100.00 pounds for generated Refund
    And There is cash payment for 100.00 pounds for generated Misc In
    And Gift card amount in db is -100.00
    And There is line with 5 SKU 500300
    And There is line with 1 Gift Card SKU for sale
    And For gift card stock is 0
    And There is line for generated refund with 1 Gift Card
    And Total till 1 virtual cashier balance changed by 269.85
    And Till 1 virtual cashier balance for cash changed by 80.00
    And Till 1 virtual cashier balance for Visa credit card changed by 189.85
    And Till 1 virtual cashier Misc Income count changed by 1 and value changed by 100.00 for reason code 12
    And Till 1 virtual cashier refund count changed by 1
    And There is information about GiftCard in DB for Gift Card top up

Scenario: Sale includes information about gift card sale or topup with zero amount with another items in transaction
    Given Sale happened
    And Till number was 1
    And 5 SKU 500300 sold
    And  A gift card was topped up with 0.00 pounds	
    And For gift card stock was 0
    And Was paid by cash for 179.85 pounds
    And Was refunded by cash for 10.00 
    When BO received the transaction
    Then There is only a sale in BO db
    And There is cash payment for 179.85 pounds
    And There is refunded cash payment for giftcard for 10.00
    And There is line with 5 SKU 500300
    And There is line with 1 Gift Card SKU for sale

Scenario: Sale includes information about gift card reverse with another items in transaction
    Given Sale happened
    And Till number was 1
    And 5 SKU 500300 sold
    And A gift card was topped up with 100.00 pounds
    And The gift card was reversed
    And For gift card stock was 0
    And Was paid by cash for 169.85 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is no generated Refund
    And There is no generated Misc Income in BO db
    And There is cash payment for 169.85 pounds
    And There is line with 5 SKU 500300
    And Gift card lines are reversed with reason code ''
    And For gift card stock is 0
    And Total till 1 virtual cashier balance changed by 169.85
    And Till 1 virtual cashier balance for cash changed by 169.85
    And Till 1 virtual cashier Misc Income count changed by 0 and value changed by 0.00 for reason code 12
    And Till 1 virtual cashier refund count changed by 0

Scenario: Sale includes information about gift card topup with another items in transaction and gift card payment
    Given Sale happened
    And Till number was 1
    And 5 SKU 500300 sold
    And  A gift card was topped up with 100.00 pounds
    And For gift card stock was 0
    And Was paid by GiftCard for 80.00 pounds
    And Was paid by Visa credit card for 189.85 pounds
    When BO received the transaction
    Then There is information about GiftCard in DB for Gift Card top up 100.00 pounds payment in DB under sequence number 2 
    And There is information about GiftCard with 80.00 pounds payment in DB under sequence number 1

Scenario: Sale includes information about voided gift card sale or topup
    Given Sale happened
    And Till number was 1
    And A gift card was topped up with 100.00 pounds
    And For gift card stock was 0
    And Transaction was voided
    And Was paid by cash for 100.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 1 Gift Card SKU for sale
    And There are no new payments in database
    And There is no information about GiftCard in DB for Gift Card top up

Scenario: Order with Giftcard topup
    Given Sale happened
    And 5 SKU 500300 sold
    And 5 items for line 1 were delivered
    And A gift card was topped up with 100.00 pounds
    When BO received the transaction
    Then There is delivery order in DB
    And There is a generated Refund
    And There is a generated Misc Income in BO db with reason code 12 and description 'Gift Card'
    And There is cash payment for 100.00 pounds for generated Refund
    And There is cash payment for 100.00 pounds for generated Misc In
    And There is line with 5 SKU 500300
    And There is line with 1 Gift Card SKU for sale
    And Gift card amount in db is -100.00
    And There is line for generated refund with 1 Gift Card
    And There is no non zero Daily Customer lines for the Sale
    And There is no Daily Customer lines for generated Refund 
