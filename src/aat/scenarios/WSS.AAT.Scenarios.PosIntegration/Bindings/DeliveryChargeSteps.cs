﻿using System;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class DeliveryChargeSteps : TransactionStepDefinitions
    {
        private readonly TestDeliveryChargeRepository deliveryChargeRepo;

        public DeliveryChargeSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            deliveryChargeRepo = dataLayerFactory.Create<TestDeliveryChargeRepository>();
        }

        [Given(@"Delievery Charge Value for Group '(.*)' was changed from (.*) to (.*)")]
        public void GivenDelieveryChargeValueForGroupWasChangedFromTo(string group, decimal oldValue, decimal newValue)
        {
            deliveryChargeRepo.UpdateDeliveryChargeGroupValue(group, oldValue, newValue);
        }

        [Given(@"There was a Delivery Charge of Group '(.*)' with Value (.*) and with SKU '(.*)'")]
        public void GivenThereWasADeliveryChargeOfGroupWithValueAndWithSKU(string group, decimal deliveryChargeValue, string skuNumber)
        {
            deliveryChargeRepo.InsertDeliveryChargeGroup(group, deliveryChargeValue, skuNumber);
        }

        [Given(@"There was no Delivery Charge Group with value (.*)")]
        public void GivenThereWasNoDeliveryChargeGroupWithValue(decimal deliveryChargeValue)
        {
            deliveryChargeRepo.DeleteDeliveryChargeGroupValue(deliveryChargeValue);
        }

    }
}
