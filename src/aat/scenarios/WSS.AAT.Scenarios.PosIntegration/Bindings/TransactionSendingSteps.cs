﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.PosIntegration.Configuration;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions;
using WSS.BO.BOReceiverService.BL.Validation;
using WSS.BO.BOReceiverService.Host.JsonSerialization;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.Model.Entity.Transactions;
using IConfiguration = WSS.BO.BOReceiverService.BL.Configuration.IConfiguration;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class TransactionSendingSteps : TransactionStepDefinitions
    {
        private readonly IBusinessLogicFacade businessLogicFacade;
        private readonly MockServiceConfiguration _config;

        public TransactionSendingSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory, IBusinessLogicFacade businessLogicFacade, IConfiguration config)
            : base(ctx, dataLayerFactory)
        {
            this.businessLogicFacade = businessLogicFacade;
            _config = (MockServiceConfiguration)config;
        }

        [Given(@"Truncator is turned off")]
        public void GivenTruncatorIsTurnedOff()
        {
            _config.TruncateTooLongStrings = false;
        }

        [Given(@"Truncator is turned on")]
        public void GivenTruncatorIsTurnedOn()
        {
            _config.TruncateTooLongStrings = true;
        }

        [When(@"BO received audit transaction")]
        [When(@"BO received the OpenDrawer transaction")]
        [When(@"BO received the transaction")]
        [Given(@"BO received the transaction")]
        public void WhenBackOfficeReceivedTheTransaction()
        {
            var tran = Ctx.TransactionBuilder.GetFullyPreparedTransaction();
            Ctx.TransactionSentDate = DateTime.Now; //This allows us to get rid of DateTime.Now calls in other places of tests. 
            var expectedCashierBalanceChangeDate = Ctx.IsEoDLockOccured || Ctx.IsBankingLockOccured   //This date is used only for CashBal amounts before test execution.
                ? Ctx.TransactionSentDate.AddDays(1)                       //If its EodLock scenario, EoD lock is executing or has already finished for current day.
                : Ctx.TransactionSentDate;                                 //Thats why CashBal values will be updated for tomorrow, and thats why we save values for tomorrow before test execution.
            var balance = SaveCashierBalance(expectedCashierBalanceChangeDate, tran.CreateDateTime, tran.CashierNumber, tran.TillNumber);
            try
            {
                SendTransactionToReceiverService(tran);
            }
            catch (EodLockException ex)
            {
                Ctx.EodLockException = ex;
                ClearDbAfterLocks(tran);
                return;
            }
            catch (PickupException ex)
            {
                Ctx.PickupException = ex;
                ClearDbAfterLocks(tran);
                return;
            }
            catch (BankingException ex)
            {
                Ctx.BankingException = ex;
                ClearDbAfterLocks(tran);
                return;
            }
            catch (ValidationException ex)
            {
                Ctx.ValidationException = ex;
                return;
            }
            
            ClearDbAfterLocks(tran);

            var key = GetDailyTillTranKey(tran);
            Ctx.AfterSentTransaction(tran, key, balance);
        }

        private void ClearDbAfterLocks(Transaction tran)
        {
            if (Ctx.IsEoDLockOccured)
            {
                var repo = DataLayerFactory.Create<TestNitmasRepository>();
                repo.ClearNitlog();
            }
            if (Ctx.IsPickupLockOccured)
            {
                var repo = DataLayerFactory.Create<TestNitmasRepository>();
                repo.CleanPickupTables();
            }
            if (Ctx.IsBankingLockOccured)
            {
                var repo = DataLayerFactory.Create<TestNitmasRepository>();
                repo.CleanLockTable();
                var miscRepo = DataLayerFactory.Create<TestMiscRepository>();
                var periodId = miscRepo.GetPeriodId(tran.CreateDateTime.Date);
                repo.OpenBankingDay(periodId);
            }
        }

        [When(@"the same transaction come again")]
        public void WhenTheSameTransactionComeAgain()
        {
            try
            {
                SendTransactionToReceiverService(Ctx.SentTransaction);
            }
            catch (DuplicateTransactionException) { }
        }

        [Then(@"Validation exception has been thrown")]
        public void ThenValidationExceptionHasBeenThrown()
        {
            Assert.IsNotNull(Ctx.ValidationException);
        }

        #region Internal

        private CashierBalance SaveCashierBalance(DateTime expectedChangeDate, DateTime transactionCreateDate, string cashierNumber, string tillNumber)
        {
            var testRepo = DataLayerFactory.Create<TestMiscRepository>();
            var dictRepo = DataLayerFactory.Create<IDictionariesRepository>();

            var periodId = testRepo.GetPeriodId(expectedChangeDate);
            var currencyId = dictRepo.GetDefaultSystemCurrency();
            var cashierId = dictRepo.GetVirtualCashierId(Int32.Parse(tillNumber));
            Assert.IsNotNull(cashierId);

            CashierBalance result = new CashierBalance();

            var cashBalRepo = DataLayerFactory.Create<TestCashierBalanceRepository>();
            var cashBalCashier = cashBalRepo.GetCashierBalance(periodId, cashierId.Value, currencyId);
            if (cashBalCashier != null)
            {
                result.GrossSalesAmount = cashBalCashier.GrossSalesAmount;
                result.SalesAmount = cashBalCashier.SalesAmount;
                result.TransactionsCount = cashBalCashier.NumTransactions;
                result.NumLinesReversed = cashBalCashier.NumLinesReversed;
                result.MiscIncomeCount = cashBalCashier.GetMiscIncomeCountsArray();
                result.MiscIncomeValue = cashBalCashier.GetMiscIncomeValuesArray();
                result.MiscOutCount = cashBalCashier.GetMiscOutCountsArray();
                result.MiscOutValue = cashBalCashier.GetMiscOutValuesArray();
                result.RefundCount = cashBalCashier.RefundCount;
                result.DiscountAmount = cashBalCashier.DiscountAmount;
                result.RefundAmount = cashBalCashier.RefundAmount;
                result.NumOpenDrawer = cashBalCashier.NumOpenDrawer;
                result.NumVoids = cashBalCashier.NumVoids;
                result.NumCorrections = cashBalCashier.NumCorrections;
            }

            var cashBalCashierTenForCash = cashBalRepo.GetCashierBalanceTender(TenderType.Cash, periodId, cashierId.Value, currencyId);
            if (cashBalCashierTenForCash != null)
            {
                result.CashTenderAmount = cashBalCashierTenForCash.Amount;
            }

            var cashBalCashierTenForVisaCreditCard = cashBalRepo.GetCashierBalanceTender(TenderType.CreditCard, periodId, cashierId.Value, currencyId);
            if (cashBalCashierTenForVisaCreditCard != null)
            {
                result.VisaCardTenderAmount = cashBalCashierTenForVisaCreditCard.Amount;
            }

            var transactionCreatePeriodId = periodId;
            if (expectedChangeDate != transactionCreateDate)
            {
                transactionCreatePeriodId = testRepo.GetPeriodId(transactionCreateDate);
            }

            var cashBalCashierTenVar = cashBalRepo.GetCashierBalanceTenderVariance(TenderType.Cash, transactionCreatePeriodId, cashierId.Value, currencyId, periodId);
            if (cashBalCashierTenVar != null)
            {
                result.CashTenderVarAmount = cashBalCashierTenVar.Amount;
            }

            var cashBalCashierTenVarForVisaCreditCard = cashBalRepo.GetCashierBalanceTenderVariance(TenderType.CreditCard, transactionCreatePeriodId, cashierId.Value, currencyId, periodId);
            if (cashBalCashierTenVarForVisaCreditCard != null)
            {
                result.VisaCardTenderVarAmount = cashBalCashierTenVarForVisaCreditCard.Amount;
            }

            var realCashierInfo = cashBalRepo.GetCashierBalance(periodId, Int32.Parse(cashierNumber), currencyId);
            if (realCashierInfo != null)
            {
                result.RealCashierBalance = realCashierInfo.GrossSalesAmount;
            }

            return result;
        }

        private readonly ModelJsonSerializer modelJsonSerializer = new ModelJsonSerializer(MissingMemberHandling.Error, Required.AllowNull);

        private void SendTransactionToReceiverService(Transaction tran)
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter writer = new StringWriter(sb))
            {
                modelJsonSerializer.Serialize(writer, tran);
            }

            string json = Regex.Replace(sb.ToString(), @"WSS\.BO\.Model\.Entity(.*?)\.([a-zA-Z]*), WSS\.BO\.Model", @"$2");
            Trace.WriteLine(json);

            businessLogicFacade.AddTransaction(tran);
        }

        private IDailyTillTranKey GetDailyTillTranKey(Transaction tran)
        {
            return DataLayerFactory.Create<IDailyTillRepository>().GetDailyTillTranKey(tran.SourceTransactionId, tran.Source);
        }

        #endregion
    }
}
