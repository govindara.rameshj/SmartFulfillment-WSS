﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class BankingLockSteps : TransactionStepDefinitions
    {
        private readonly TestNitmasRepository _nitmasRepository;
        private readonly TestSafeRepository _safeRepository;
        private readonly TestMiscRepository _miscRepository;

        public BankingLockSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            _nitmasRepository = DataLayerFactory.Create<TestNitmasRepository>();
            _safeRepository = DataLayerFactory.Create<TestSafeRepository>();
            _miscRepository = DataLayerFactory.Create<TestMiscRepository>();
        }

        [Given(@"Banking process is started")]
        public void GivenBankingProcessIsStarted()
        {
            _nitmasRepository.CreateBankingLock(DateTime.Now);
            Ctx.IsBankingLockOccured = true;
        }

        [Given(@"Banking process has finished")]
        public void GivenBankingProcessHasFinished()
        {
            var periodId = _miscRepository.GetPeriodId(DateTime.Now);
            _safeRepository.SetSafeStatusByPeriod(periodId, true);
            Ctx.IsBankingLockOccured = true;
        }

        [Given(@"Safes are created")]
        public void SafesAreCreated()
        {
            _safeRepository.GetSafe(DateTime.Now);
        }

        [Given(@"Banking process hasn't started")]
        public void GivenBankingProcessHasnTStarted()
        {
            var periodId = _miscRepository.GetPeriodId(DateTime.Now);
            _safeRepository.SetSafeStatusByPeriod(periodId, false);
        }

        [Then(@"Banking lock error happened")]
        public void ThenBankingLockErrorHappened()
        {
            Assert.IsNotNull(Ctx.BankingException);
        }
    }
}
