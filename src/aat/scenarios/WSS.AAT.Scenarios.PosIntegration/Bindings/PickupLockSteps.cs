﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class PickupLockSteps : TransactionStepDefinitions
    {
        #region Internal

        private readonly TestNitmasRepository _nitmasRepository;
        private readonly DictionariesRepository _dictRepository;
        private readonly TestMiscRepository _miscRepo;

        public PickupLockSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            _nitmasRepository = DataLayerFactory.Create<TestNitmasRepository>();
            _dictRepository = DataLayerFactory.Create<DictionariesRepository>();
            _miscRepo = DataLayerFactory.Create<TestMiscRepository>();
        }

        #endregion

        #region Steps

        [Given(@"Pickup process for virtual cashier is not started")]
        public void GivenPickupProcessForVirtualCashierIsNotStarted()
        {
            _nitmasRepository.CleanPickupTables();
        }

        [Given(@"Pickup process for virtual cashier is started")]
        public void GivenPickupProcessForVirtualCashierIsStarted()
        {
            var cashierId = _dictRepository.GetVirtualCashierId(Int32.Parse(Ctx.TransactionBuilder.GetTillNumber()));
            _nitmasRepository.StartPickupProcessForCashier(cashierId.ToString(), DateTime.Now);
            Ctx.IsPickupLockOccured = true;
        }

        [Given(@"Pickup process for virtual cashier has finished")]
        public void GivenPickupProcessForVirtualCashierHasFinished()
        {
            var cashierId = _dictRepository.GetVirtualCashierId(Int32.Parse(Ctx.TransactionBuilder.GetTillNumber()));
            var currendDate = DateTime.Now;
            var periodId = _miscRepo.GetPeriodId(currendDate);
            _nitmasRepository.CompletePickupProcessForCashier(cashierId.ToString(), periodId, currendDate);
            Ctx.IsPickupLockOccured = true;
        }

        [Then(@"Pickup lock error happened")]
        public void ThenPickupLockErrorHappened()
        {
            Assert.IsNotNull(Ctx.PickupException);
        }

        #endregion
    }
}
