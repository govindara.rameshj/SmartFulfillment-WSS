using System;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class StockMovementSteps : TransactionStepDefinitions
    {
        private readonly TestStockRepository stockRepo;

        public StockMovementSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory) : base(ctx, dataLayerFactory)
        {
            stockRepo = dataLayerFactory.Create<TestStockRepository>();
        }

        #region Given

        [Given(@"Stock for (.*) was (.*)")]
        [When(@"Stock for (.*) was (.*)")]
        public void GivenStockForWas(string skuNumber, int stockAmount)
        {
            stockRepo.SetStockAmount(skuNumber, stockAmount);
        }

        [Given(@"Sales Units values were equal to (.*) for SKU (.*)")]
        public void GivenSalesUnitsValuesWereEqualToForSKU(int stockUnitsValue, string skuNumber)
        {
            stockRepo.SetStockUnitsValues(skuNumber, stockUnitsValue);
        }

        [Given(@"Sales Values were equal to (.*) for SKU (.*)")]
        public void GivenSalesValuesWereEqualToForSKU(decimal stockValue, string skuNumber)
        {
            stockRepo.SetStockValues(skuNumber, stockValue);
        }

        [Given(@"Adjustments Value is (.*) for SKU (.*)")]
        public void GivenAdjustmentValueIsForSKU(int adjustmentValue, string skuNumber)
        {
            stockRepo.SetAdjustmentValue(skuNumber, adjustmentValue);
        }

        [Given(@"Markdown stock for (.*) was (.*)")]
        public void GivenMarkdownStockForWas(string skuNumber, int markdownStockAmount)
        {
            stockRepo.SetMarkdownStockAmount(skuNumber, markdownStockAmount);
        }

        [Given(@"For gift card stock was (.*)")]
        public void GivenForGiftCardStockWas(int stockAmount)
        {
            string giftCardSku = DataLayerFactory.Create<IDictionariesRepository>().GetSettingStringValue(ParameterId.GiftCardSku);
            stockRepo.SetStockAmount(giftCardSku, stockAmount);
        }

        #endregion ~Given

        #region Then

        [Then(@"Stock for (.*) is (.*)")]
        public void ThenStockForIs(string skuNumber, int stockAmount)
        {
            var stock = stockRepo.GetStockOnHand(skuNumber);
            Assert.AreEqual(stockAmount, stock);
        }

        [Then(@"Markdown Stock for (.*) is (.*)")]
        public void ThenMarkdownStockForIs(string skuNumber, int markdownStockAmount)
        {
            var stock = stockRepo.GetMarkdownStockOnHand(skuNumber);
            Assert.AreEqual(markdownStockAmount, stock);
        }

        [Then(@"Sales Units values are equal to (.*) for SKU (.*)")]
        public void ThenSalesUnitsValuesAreEqualToForSKU(int salesUnitsValue, string skuNumber)
        {
            var stkmas = stockRepo.GetStockMaster(skuNumber);

            Assert.AreEqual(salesUnitsValue, stkmas.SalesUnits1);
            Assert.AreEqual(salesUnitsValue, stkmas.SalesUnits2);
            Assert.AreEqual(salesUnitsValue, stkmas.SalesUnits4);
            Assert.AreEqual(salesUnitsValue, stkmas.SalesUnits6);
        }

        [Then(@"Sales Values are equal to (.*) for SKU (.*)")]
        public void ThenSalesValuesAreEqualToForSKU(decimal salesValue, string skuNumber)
        {
            var stkmas = stockRepo.GetStockMaster(skuNumber);

            Assert.AreEqual(salesValue, stkmas.SalesValue1);
            Assert.AreEqual(salesValue, stkmas.SalesValue2);
            Assert.AreEqual(salesValue, stkmas.SalesValue4);
            Assert.AreEqual(salesValue, stkmas.SalesValue6);
        }

        [Then(@"Adjustments value is equal to (.*) for SKU (.*)")]
        public void ThenAdjustmentsValueIsEqualToForSKU(int adjustmentValue, string skuNumber)
        {
            var stkmas = stockRepo.GetStockMaster(skuNumber);

            Assert.AreEqual(adjustmentValue, stkmas.AdjustmentsValue1);
        }

        [Then(@"Stock movement type for SKU (.*) is (.*)")]
        public void ThenStockMovementTypeForSkuIsNormal(string skuNumber, string formalMovementType)
        {
            var movementType = GetTypeOfMovementByName(formalMovementType);

            var stklog = stockRepo.GetLastStklogForSkuAndType(skuNumber, movementType);
            Assert.IsNotNull(stklog);
        }

        [Then(@"Record is added to Stock Log for SKU (.*)")]
        public void ThenRecordIsAddedToStockLog(string skuNumber)
        {
            var stklog = stockRepo.GetLastStklogForSku(skuNumber);

            Assert.IsNotNull(stklog);
            var tranDate = Ctx.SentTransaction.CreateDateTime;
            Assert.AreEqual(tranDate.Date, stklog.DateOfMovement);
        }

        [Then(@"In Stock Log for SKU (.*) Starting Markdown Quantity is (.*) and Ending Markdown Quantity is (.*)")]
        public void ThenInStockLogForSKUStartingMarkdownQuantityIsAndEndingMarkdownQuantityIs(string skuNumber, int startingMarkdownQuantity, int endingMarkdownQuantity)
        {
            var stklog = stockRepo.GetLastStklogForSku(skuNumber);

            Assert.IsNotNull(stklog);
            Assert.AreEqual(startingMarkdownQuantity, stklog.StartingMarkdownStockOnHand);
            Assert.AreEqual(endingMarkdownQuantity, stklog.EndingMarkdownStockOnHand);
        }

        [Then(@"Stock Log type is (.*) for SKU (.*)")]
        public void ThenStockLogTypeIsForSKU(string type, string skuNumber)
        {
            var stklog = stockRepo.GetLastStklogForSku(skuNumber);

            Assert.IsNotNull(stklog);
            Assert.AreEqual(type, stklog.TypeOfMovement);
        }

        [Then(@"User is added to Stock Log for SKU (.*)")]
        public void ThenUserIsAddedToStockLogForSKU(string skuNumber)
        {
            var stklog = stockRepo.GetLastStklogForSku(skuNumber);

            Assert.IsNotNull(stklog);
            var cashierNumber = Ctx.SentTransaction.CashierNumber;
            Assert.AreEqual(Int32.Parse(cashierNumber), Int32.Parse(stklog.CashierNumber));
        }

        [Then(@"Record is not added to Stock Log for Gift Card")]
        public void ThenRecordWasNotAddedToStockLogForGiftCard()
        {
            string giftCardSku = DataLayerFactory.Create<IDictionariesRepository>().GetSettingStringValue(ParameterId.GiftCardSku);
            var stklog = stockRepo.GetStklogForSku(giftCardSku);

            Assert.IsNull(stklog);
        }

        [Then(@"There are no new (.*) records for SKU (.*) in Stock Log")]
        public void ThenThereAreNoNewRecordsForSkuInStockLog(string formalMovementType, string skuNumber)
        {
            var stklog = stockRepo.GetLastStklogForSku(skuNumber);
            var refundMovementType = GetTypeOfMovementByName(formalMovementType);
            var tranDate = Ctx.SentTransaction.CreateDateTime;
            Assert.IsTrue(stklog == null || stklog.DateOfMovement != tranDate.Date || stklog.TimeOfMovement != tranDate.ToString("HHmmss") || stklog.TypeOfMovement != refundMovementType);
        }

        [Then(@"For gift card stock is (.*)")]
        public void ThenForGiftCardStockIs(int stockAmount)
        {
            string giftCardSku = DataLayerFactory.Create<IDictionariesRepository>().GetSettingStringValue(ParameterId.GiftCardSku);
            var stock = stockRepo.GetStockOnHand(giftCardSku);
            Assert.AreEqual(stockAmount, stock);
        }

        [Then(@"Stklog date of movement for SKU (.*) is equal today")]
        public void ThenStklogDateIsToday(string skuNumber)
        {
            var stklog = stockRepo.GetLastStklogForSku(skuNumber);
            var dltots = DataLayerFactory.Create<IDailyTillRepository>().SelectDailyTillTran(Ctx.SentTransactionKey);

            Assert.IsNotNull(dltots);
            Assert.AreEqual(Ctx.TransactionSentDate.Date, stklog.DateOfMovement);
        }

        #endregion ~Then

        private static string GetTypeOfMovementByName(string typeMovement)
        {
            switch (typeMovement)
            {
                case "Normal stock sale":
                    {
                        return TypeOfMovement.NormalStockSaleItem;
                    }
                case "Normal stock refund":
                    {
                        return TypeOfMovement.NormalStockRefundItem;
                    }
                case "Pss in store sale":
                    {
                        return TypeOfMovement.PssInStoreSaleItem;
                    }
                case "Markdown stock sale":
                    {
                        return TypeOfMovement.MarkdownStockSaleItem;
                    }
                case "Backdoor collection":
                    {
                        return TypeOfMovement.BackdoorCollection;
                    }
                case "C&C Sale":
                    {
                        return TypeOfMovement.ClickAndCollectSale;
                    }
                case "C&C Refund":
                    {
                        return TypeOfMovement.ClickAndCollectRefund;
                    }
                case "PSS Sale":
                    {
                        return TypeOfMovement.PssSaleItem;
                    }
                case "PSS Refund":
                    {
                        return TypeOfMovement.PssRefundItem;
                    }
                case "System Adjustment 91":
                    {
                        return TypeOfMovement.SystemAdjustment91;
                    }
            }
            throw new ArgumentException("Invalid type of movement");
        }

    }
}
