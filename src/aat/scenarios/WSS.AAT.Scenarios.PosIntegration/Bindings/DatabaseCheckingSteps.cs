using System;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;
using EventType = WSS.BO.Model.Enums.EventType;
using WSS.BO.Model.Entity.Discounts;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    sealed class DatabaseCheckingSteps : TransactionStepDefinitions
    {
        public DatabaseCheckingSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            dailyTillRepo = DataLayerFactory.Create<IDailyTillRepository>();
            dictRepo = DataLayerFactory.Create<IDictionariesRepository>();
            miscRepo = DataLayerFactory.Create<TestMiscRepository>();
            bankingRepo = DataLayerFactory.Create<BankingSuiteRepository>();
        }

        private readonly IDailyTillRepository dailyTillRepo;
        private readonly IDictionariesRepository dictRepo;
        private readonly TestMiscRepository miscRepo;
        private readonly BankingSuiteRepository bankingRepo;

        #region Thens

        [Then(@"There is login information in BO db")]
        public void ThenThereIsLoginInformationInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.SignOn, dltots.TransactionCode);
        }

        [Then(@"There is logout information in BO db")]
        public void ThenThereIsLogoutInformationInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.SignOff, dltots.TransactionCode);
        }

        [Then(@"Customer info for (.*) is added to db")]
        public void ThenCustomerInfoForIsAddedToDb(string customerName)
        {
            var dlrcus = dailyTillRepo.SelectDailyCustomer(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlrcus);
            Assert.AreEqual(customerName.Trim(), dlrcus.CustomerName.Trim());
        }

        [Then(@"There is a new sale in BO db for cashier (.*)")]
        public void ThenThereIsANewSaleInBoDbForCashier(string cashierId)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.Sale, dltots.TransactionCode);
            Assert.AreEqual(cashierId, dltots.CashierNumber);
        }

        [Then(@"There is a new sale in BO db marked as with Supervisor Used")]
        public void ThenThereIsANewSaleInBODbMarkedAsWithSupervisorUsed()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.IsTrue(dltots.IsSupervisorUsed);
        }

        [Then(@"Transaction is marked as Supervisor Used")]
        public void ThenTransactionIsMarkedAsSupervisorUsed()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.IsTrue(dltots.IsSupervisorUsed);
        }

        [Then(@"Sale date equal today in BO db")]
        public void ThenSaleDateEqualTodayInBoDb()
        {
            CheckSaleDate(Ctx.TransactionSentDate);
        }

        [Then(@"Sale date equal yesterday in BO db")]
        public void ThenSaleDateEqualYesterdayInBoDb()
        {
            CheckSaleDate(Ctx.TransactionSentDate.AddDays(-1));
        }

        private void CheckSaleDate(DateTime date)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(date.Date, dltots.CreateDate);
        }

        [Then(@"Received date equal today in BO db")]
        public void ThenReceivedDateEqualTodayInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(Ctx.TransactionSentDate.Date, dltots.ReceivedDate.Date);
        }

        [Then(@"CBBU field equal today")]
        public void ThenCbbuFieldEqualToday()
        {
            CheckCbbuField(Ctx.TransactionSentDate);
        }

        [Then(@"CBBU field equal tomorrow")]
        public void ThenCbbuFieldEqualTomorrow()
        {
            CheckCbbuField(Ctx.TransactionSentDate.AddDays(1));
        }

        private void CheckCbbuField(DateTime date)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(date.Date.ToString("dd/MM/yy"), dltots.WasCbbupdUpdated);
        }

        [Then(@"CBBU field is null in BO db")]
        public void ThenCbbuFieldIsNullInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.IsNullOrEmpty(dltots.WasCbbupdUpdated);
        }

        [Then(@"The sale reason code is (.*)")]
        public void WhenTheSaleReasonCodeIs(int reasonCode)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(dltots.Misc, reasonCode);
        }

        [Then(@"There is refunded (.*) card payment for (.*)")]
        public void ThenThereIsRefundedVisaPaymentFor(string cardType, decimal amount)
        {
            CheckCreditCardPayment(cardType, amount, Direction.TO_CUSTOMER);
        }

        [Then(@"There is sale in BO db")]
        public void ThenThereIsSaleInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.Sale, dltots.TransactionCode);
        }

        [Then(@"There is only a sale in BO db")]
        public void ThenThereIsOnlyASaleInBODb()
        {
            Assert.AreEqual(1, dailyTillRepo.GetDailyTillTranCount(Ctx.FirstTransaction.SourceTransactionId, Ctx.FirstTransaction.Source));

            ThenThereIsSaleInBoDb();
        }

        [Then(@"There is refund in BO db")]
        public void ThenThereIsRefundInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.Refund, dltots.TransactionCode);
        }

        [Then(@"There is refund in BO db with correct references to the legacy transaction")]
        public void ThenThereIsRefundInBoDbWithCorrectReferencesToTheLegacyTransaction()
        {
            var saleKey = Ctx.FirstTransactionKey;
            var refundKey = Ctx.SentTransactionKey;
            var refund = DataLayerFactory.Create<DailyTillRepository>().SelectDailyCustomerWithNonZeroLineNumber(refundKey);
            Assert.IsNotNull(refund);
            Assert.AreEqual(saleKey.TransactionNumber, refund.OriginalTransactionNumber);
            Assert.AreEqual(saleKey.TillNumber, refund.OriginalTillNumber);
            Assert.IsNotNull(refund.OriginalSaleDate);
            Assert.AreEqual(saleKey.CreateDate.Date, ((DateTime)refund.OriginalSaleDate).Date);
        }

        [Then(@"Information about tokens and hashes was saved")]
        public void ThenInformationAboutTokensAndHashesWasSaved()
        {
            var dlcomm = dailyTillRepo.SelectDlcomm(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlcomm);
        }

        [Then(@"VAT value of line for SKU (.*) is equal to (.*)")]
        public void ThenVatValuesForVatRateOfPersentsAreCorrect(string sku, decimal vatValue)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, sku);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(vatValue, dlline.VatValue);
        }

        [Then(@"Supervisor Number of line for SKU (.*) is equal to '(.*)'")]
        public void ThenSupervisorNumberOfLineForSKUIsEqualTo(string sku, string supervisorNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, sku);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(dlline.SupervisorNumber, supervisorNumber);
        }

        [Then(@"Quarantine Supervisor Number of line for SKU (.*) is equal to '(.*)'")]
        public void ThenQuarantineSupervisorNumberOfLineForSKUIsEqualTo(string sku, string supervisorNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, sku);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(dlline.PartitialQuarantine, supervisorNumber);
        }

        [Then(@"Total VAT value for VAT rate of (.*) persents is equal to (.*)")]
        public void ThenTotalVatValueForVatRateOfPersentsIsEqualTo(decimal vatRate, decimal vatValue)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            var vatRateCode = GetVatCode(vatRate);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(vatValue, dltots.GetVatValue(vatRateCode));
        }

        [Then(@"There are no lines")]
        public void ThenThereAreNoLines()
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey);
            Assert.IsNull(dlline);
        }

        [Then(@"There is information about GiftCard in DB")]
        public void ThenThereIsInformationAboutGiftCardInDb()
        {
            var payment = ((PayableTransaction)Ctx.SentTransaction).Payments.FirstOrDefault(x => x is GiftCardPayment) as GiftCardPayment;
            Assert.IsNotNull(payment);

            var dlgiftcard = dailyTillRepo.SelectDailyGiftCard(Ctx.SentTransactionKey, payment.GiftCardOperation.AuthCode);
            Assert.IsNotNull(dlgiftcard);
        }

        [Then(@"There is information about GiftCard with (.*) pounds payment in DB under sequence number (.*)")]
        public void ThenThereIsInformationAboutGiftCardWithPoundsPaymentInDBUnderSequenceNumber(decimal tenderAmount, int sequenceNumber)
        {
            var dlgiftcard = dailyTillRepo.SelectDailyGiftCard(Ctx.SentTransactionKey, sequenceNumber, tenderAmount, TransactionTypeForCard.TenderInSale);
            Assert.IsNotNull(dlgiftcard);
            Assert.AreEqual(tenderAmount, dlgiftcard.TenderAmount);
            Assert.AreEqual(TransactionTypeForCard.TenderInSale, dlgiftcard.TransactionType);
        }

        [Then(@"There is information about GiftCard in DB for Gift Card top up")]
        public void ThenThereIsInformationAboutGiftCardInDbForGiftCardTopUp()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            var dlgiftcard = dailyTillRepo.SelectDailyGiftCard(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlgiftcard);
        }

        [Then(@"There is information about GiftCard in DB for Gift Card top up (.*) pounds payment in DB under sequence number (.*)")]
        public void ThenThereIsInformationAboutGiftCardInDBForGiftCardTopUpPoundsPaymentInDBUnderSequenceNumber(decimal tenderAmount, int sequenceNumber)
        {
            var dlgiftcard = dailyTillRepo.SelectDailyGiftCard(Ctx.SentTransactionKey, sequenceNumber , - tenderAmount);
            Assert.IsNotNull(dlgiftcard);
        }

        [Then(@"There is no information about GiftCard in DB for Gift Card top up")]
        public void ThenThereIsNoInformationAboutGiftCardInDBForGiftCardTopUp()
        {
            var dlgiftcard = dailyTillRepo.SelectDailyGiftCard(Ctx.SentTransactionKey);
            Assert.IsNull(dlgiftcard);
        }

        [Then(@"There is cash payment for (.*) pounds")]
        public void ThenThereIsCashPaymentForPounds(decimal cashAmount)
        {
            CheckCashPayment(cashAmount, Direction.FROM_CUSTOMER);
        }

        [Then(@"There is (.*) card payment for (.*) pounds")]
        public void ThenThereIsCardPaymentForPounds(string cardType, decimal cashAmount)
        {
            CheckCreditCardPayment(cardType, cashAmount, Direction.FROM_CUSTOMER);
        }

        [Then(@"In db Issue Number Switch for card payment is empty")]
        public void ThenInDbIssueNumberSwitchForCardPaymentIsAnEmptyString()
        {
            var dlpaid = dailyTillRepo.SelectDailyPayment(Ctx.SentTransactionKey, ModelMapper.GetTenderTypeCode(GetTypeCardByName("Visa")));

            Assert.IsNotNull(dlpaid);
            Assert.AreEqual(new string(' ', 2), dlpaid.IssueNumberSwitch);
        }

        [Then(@"Lines for SKU (.*) are reversed")]
        public void ThenLinesForSkuAreReversed(string skuNumber)
        {
            ThenLinesForSkuAreReversedWithReasonCode(skuNumber, LineReversalReasonCode.NoLineReversal);
        }

        [Then(@"There is Misc Income in BO db with reason code (.*) and description '(.*)'")]
        public void ThenThereIsMiscIncomeInBoDbWithReasonCodeAndDescription(short reasonCode, string description)
        {
            CheckDltots(reasonCode, description, TransactionCodes.MiscIncome);
        }

        [Then(@"There is a generated Misc Income in BO db with reason code (.*) and description '(.*)'")]
        public void ThenThereIsAGeneratedMiscIncomeInBoDbWithReasonCodeAndDescription(short reasonCode, string description)
        {
            CheckGeneratedDltots(reasonCode, description, TransactionCodes.MiscIncome);
        }

        [Then(@"There is no generated Misc Income in BO db")]
        public void ThenThereIsNoGeneratedMiscIncomeInBoDbWithReasonCodeAndDescription()
        {
            Dltots dltots = dailyTillRepo.SelectGeneratedDailyTillTran(Ctx.SentTransaction.TransactionNumber, Ctx.SentTransaction.Source, TransactionCodes.MiscIncome);
            Assert.IsNull(dltots);
        }

        [Then(@"There is Misc Income Correction in BO db with reason code (.*) and description '(.*)'")]
        public void ThenThereIsMiscIncomeCorrectionInBoDbWithReasonCodeAndDescription(short reasonCode, string description)
        {
            CheckDltots(reasonCode, description, TransactionCodes.MiscIncomeCorrection);
        }

        [Then(@"There is a generated Refund")]
        public void ThenThereIsAGeneratedRefund()
        {
            CheckGeneratedDltots(0, string.Empty, TransactionCodes.Refund);
        }

        [Then(@"There is no generated Refund")]
        public void ThenThereIsNoGeneratedRefund()
        {
            Dltots dltots = dailyTillRepo.SelectGeneratedDailyTillTran(Ctx.SentTransaction.TransactionNumber, Ctx.SentTransaction.Source, TransactionCodes.Refund);
            Assert.IsNull(dltots);
        }

        [Then(@"There is Paid Out in BO db with reason code (.*) and description '(.*)'")]
        public void ThenThereIsPaidOutInBoDbWithReasonCodeAndDescription(short reasonCode, string description)
        {
            CheckDltots(reasonCode, description, TransactionCodes.PaidOut);
        }

        [Then(@"There is line for generated refund with (.*) Gift Card")]
        public void ThenThereIsLineForGeneratedRefundWithSku(int quantity)
        {
            string giftCardSku = dictRepo.GetSettingStringValue(ParameterId.GiftCardSku);
            Dltots dltots = dailyTillRepo.SelectGeneratedDailyTillTran(Ctx.SentTransaction.TransactionNumber, Ctx.SentTransaction.Source, TransactionCodes.Refund);
            Assert.IsNotNull(dltots);
            var dlline = dailyTillRepo.SelectDailyTillLine(dltots, giftCardSku);
            Assert.IsNotNull(dlline);
        }

        [Then(@"Gift card amount in db is (.*)")]
        public void ThenGiftCardAmountInDbIs(decimal giftcardAmount)
        {
            var dlgiftcard = dailyTillRepo.SelectDailyGiftCard(Ctx.SentTransactionKey);

            Assert.AreEqual(giftcardAmount, dlgiftcard.TenderAmount);
        }

        [Then(@"There is cash payment for (.*) pounds for generated Refund")]
        public void ThenThereIsCashPaymentForPoundsForGeneratedRefund(decimal amount)
        {
            CheckGeneratedDlpaid(TransactionCodes.Refund, amount);
        }

        [Then(@"There is cash payment for (.*) pounds for generated Misc In")]
        public void ThenThereIsCashPaymentForPoundsForGeneratedMiscIn(decimal amount)
        {
            CheckGeneratedDlpaid(TransactionCodes.MiscIncome, -amount);
        }

        [Then(@"There is Paid Out Correction in BO db with reason code (.*) and description '(.*)'")]
        public void ThenThereIsPaidOutCorrectionInBoDbWithReasonCodeAndDescription(short reasonCode, string description)
        {
            CheckDltots(reasonCode, description, TransactionCodes.PaidOutCorrection);
        }

        [Then(@"Lines for SKU (.*) are reversed with reason code '(.*)'")]
        public void ThenLinesForSkuAreReversedWithReasonCode(string skuNumber, string reasonCode)
        {
            var lines = dailyTillRepo.SelectReversedDailyTillLines(Ctx.SentTransactionKey, skuNumber);
            Assert.AreEqual(2, lines.Count());
            var reversedPositiveLine = lines[0];
            var reversedNegativeLine = lines[1];
            Assert.AreEqual(-reversedPositiveLine.Quantity, reversedNegativeLine.Quantity);
            Assert.AreEqual(-reversedPositiveLine.Amount, reversedNegativeLine.Amount);
            Assert.AreEqual(-reversedPositiveLine.VatValue, reversedNegativeLine.VatValue);
            Assert.AreEqual(reasonCode, reversedPositiveLine.ReversalReasonCode.Trim());
            Assert.AreEqual(reasonCode, reversedNegativeLine.ReversalReasonCode.Trim());
        }

        [Then(@"Gift card lines are reversed with reason code '(.*)'")]
        public void ThenGiftCardLinesAreReversedWithReasonCode(string reasonCode)
        {
            string giftCardSku = dictRepo.GetSettingStringValue(ParameterId.GiftCardSku);
            var lines = dailyTillRepo.SelectReversedDailyTillLines(Ctx.SentTransactionKey, giftCardSku);
            Assert.AreEqual(2, lines.Count());
            var reversedPositiveLine = lines[0];
            var reversedNegativeLine = lines[1];
            Assert.AreEqual(-reversedPositiveLine.Quantity, reversedNegativeLine.Quantity);
            Assert.AreEqual(-reversedPositiveLine.Amount, reversedNegativeLine.Amount);
            Assert.AreEqual(-reversedPositiveLine.VatValue, reversedNegativeLine.VatValue);
            Assert.AreEqual(reasonCode, reversedPositiveLine.ReversalReasonCode.Trim());
            Assert.AreEqual(reasonCode, reversedNegativeLine.ReversalReasonCode.Trim());
        }

        [Then(@"Line for SKU (.*) was collected from backdoor")]
        public void ThenLineForSkuWasCollectedFromBackdoor(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.IsTrue(dlline.IsBackDoor);
            Assert.AreEqual(ConversionUtils.BooleanToYorN(true), dlline.BackDoorCollected);
        }

        [Then(@"There are no new payments in database")]
        public void ThenThereAreNoNewPaymentsInDatabase()
        {
            var dlpaid = dailyTillRepo.SelectDailyPayment(Ctx.SentTransactionKey);
            Assert.IsNull(dlpaid);
        }

        [Then(@"There is no new customer information in database")]
        public void ThenThereIsNoNewCustomerInformationInDatabase()
        {
            var dlrcus = dailyTillRepo.SelectDailyCustomer(Ctx.SentTransactionKey);
            Assert.IsNull(dlrcus);
        }

        [Then(@"There is line with (.*) SKU (.*)")]
        public void ThenThereIsLineWithSku(int quantity, string skuNumber)
        {
            CheckNewDllineInDatabase(quantity, skuNumber, Direction.TO_CUSTOMER);
        }

        [Then(@"There is line with (.*) Gift Card SKU for sale")]
        public void ThenThereIsLineWithSku(int quantity)
        {
            string skuNumber = dictRepo.GetSettingStringValue(ParameterId.GiftCardSku);
            CheckNewDllineInDatabase(quantity, skuNumber, Direction.TO_CUSTOMER);
        }

        [Then(@"There is no line for SKU (.*)")]
        public void ThenThereIsNoLineForSku(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNull(dlline);
        }

        [Then(@"There is refund for order in BO db")]
        public void ThenThereIsRefundForOrderInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.Refund, dltots.TransactionCode);

            var orderNumber = DataLayerFactory.Create<TestCustomerOrderRepository>().GetPreviousOrderNumber();
            Assert.AreEqual(orderNumber, dltots.OrderNumber);
        }

        [Then(@"There is refunded line for (.*) SKU (\d*)")]
        public void ThenThereIsRefundedLineForSku(int quantity, string skuNumber)
        {
            CheckNewDllineInDatabase(quantity, skuNumber, Direction.FROM_CUSTOMER);
        }

        [Then(@"There is line with (.*) sold SKU (.*)")]
        public void ThenThereIsLineWithSoldSku(int quantity, string skuNumber)
        {
            CheckNewDllineInDatabase(quantity, skuNumber, Direction.TO_CUSTOMER);
        }

        [Then(@"There is refunded line for (.*) SKU (\d*) with reason code '(.*)'")]
        public void ThenThereIsRefundedLineForSkuWithReasonCode(int quantity, string skuNumber, string reasonCode)
        {
            var dlline = CheckNewDllineInDatabase(quantity, skuNumber, Direction.FROM_CUSTOMER);
            var dlrcus = dailyTillRepo.SelectDailyCustomer(Ctx.SentTransactionKey, dlline.SequenceNumber);
            Assert.AreEqual(reasonCode, dlrcus.RefundReasonCode);
        }

        [Then(@"There is refunded cash payment for (.*)")]
        public void ThenThereIsRefundedCashPaymentFor(decimal amount)
        {
            CheckCashPayment(amount, Direction.TO_CUSTOMER);
        }

        [Then(@"There is refunded cash payment for giftcard for (.*)")]
        public void ThenThereIsRefundedCashPaymentForGiftcardFor(decimal amount)
        {
            var dlpaid = dailyTillRepo.SelectDailyPayment(Ctx.SentTransactionKey, TenderType.Cash, true);

            Assert.IsNotNull(dlpaid);
            Assert.AreEqual(amount, dlpaid.Amount);
        }

        [Then(@"There is refunded giftcard payment for (.*)")]
        public void ThenThereIsRefundedGiftcardPaymentFor(decimal amount)
        {
            CheckGiftCardPayment(amount, Direction.TO_CUSTOMER);
        }

        [Then(@"Original transaction values are filled in customer table")]
        public void ThenOriginalTransactionValuesAreFilledInCustomerTable()
        {
            var dlrcus = dailyTillRepo.SelectDailyCustomerWithNonZeroLineNumber(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlrcus);
            Assert.AreNotEqual("00", dlrcus.OriginalTillNumber.Trim());
            Assert.AreNotEqual("0000", dlrcus.OriginalTransactionNumber);
            Assert.AreNotEqual(Ctx.SentTransaction.CreateDateTime.Date, dlrcus.OriginalSaleDate);
            Assert.AreEqual(dictRepo.GetStoreId(), dlrcus.OriginalSaleStore);
        }

        [Then(@"Original Line Number in Customer Table for SKU (.*) is (.*)")]
        public void ThenOriginalLineNumberInCustomerTableForSKUIs(string skuNumber, int originalLineNumber)
        {
            var dlrcus = dailyTillRepo.SelectDailyCustomersWithOriginalLineNumber(Ctx.SentTransactionKey, originalLineNumber);

            Assert.IsNotNull(dlrcus);

            var originalDlline = dailyTillRepo.SelectDailyTillLineBySourceItemId(
                new DailyTillTranKey()
                {
                    CreateDate = (DateTime)dlrcus.OriginalSaleDate,
                    TillNumber = dlrcus.OriginalTillNumber,
                    TransactionNumber = dlrcus.OriginalTransactionNumber
                }, originalLineNumber.ToString());

            Assert.IsNotNull(originalDlline);
        }

        [Then(@"There is information about line (.*) in customer table in db")]
        public void ThenThereIsInformationAboutLineInCustomerTableInDb(int lineNumber)
        {
            var dlrcus = dailyTillRepo.SelectDlrcuses(Ctx.SentTransactionKey)
                    .FirstOrDefault(x => x.LineNumber == lineNumber);
            Assert.IsNotNull(dlrcus);
        }

        [Then(@"Where is sale in BO db with total amount equal (.*), discount equal (.*), tax amount equal (.*), value added tax equal (.*)")]
        public void ThenWhereIsSaleInBoDbWithTotalAmountEqualDiscountEqualTaxAmountEqualValueAddedTaxEqual(decimal totalAmount, decimal discountAmount, decimal taxAmount, decimal vat)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(true, dltots.IsEmployeeDiscount);
            Assert.AreEqual(taxAmount, dltots.TaxAmount);
            Assert.AreEqual(totalAmount, dltots.TotalSaleAmount);
            Assert.AreEqual(-discountAmount, dltots.DiscountAmount);
            Assert.AreEqual(vat, dltots.VatValue1);
        }

        [Then(@"There is information about employee card number in BO db")]
        public void ThenThereIsInformationAboutEmployeeCardNumberInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);

            var lastEmployeeDiscount = ((RetailTransaction)Ctx.SentTransaction).Items.Select(x => x.Product).OfType<SkuProduct>().SelectMany(x => x.Discounts).OfType<EmployeeDiscount>().Last();
            Assert.AreEqual(lastEmployeeDiscount.CardNumber, dltots.EmployeeDiscountCard.Trim());
        }

        [Then(@"There is line for SKU (.*) with price equal (.*), amount equal (.*), discount equal (.*), value added tax equal (.*)")]
        public void ThenThereIsLineForSkuWithPriceEqualAmountEqualDiscountEqualValueAddedTaxEqual(string skuNumber, decimal price, decimal amount, decimal discount, decimal vat)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(price, dlline.Price);
            Assert.AreEqual(amount, dlline.Amount);
            Assert.AreEqual(discount, dlline.EmployeeDiscountAmount);
            Assert.AreEqual(vat, dlline.VatValue);
        }

        [Then(@"Merchandise amount is equal (.*), total amount is equal (.*), total tax amount is equal (.*), total discount amount is equal to (.*), total value added amount of rate of (.*) persents is equal (.*)")]
        public void ThenMerchandiseAmountIsEqualTotalAmountIsEqualTotalTaxAmountIsEqualTotalDiscountAmountIsEqualToTotalValueAddedAmountOfRateOfPersentsIsEqual(decimal mercAmount, decimal totalAmount, decimal taxAmount, decimal discountAmount, int vatRate, decimal vatAmount)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(mercAmount, dltots.MerchandiseAmount);
            Assert.AreEqual(totalAmount, dltots.TotalSaleAmount);
            Assert.AreEqual(taxAmount, dltots.TaxAmount);
            Assert.AreEqual(-discountAmount, dltots.DiscountAmount);

            var vatRateCode = GetVatCode(vatRate);
            Assert.AreEqual(vatAmount, dltots.GetVatValue(vatRateCode));
        }



        [Then(@"Merchandice amount equal (.*)")]
        public void ThenMerchandiceAmountEqual(decimal mercAmount)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(mercAmount, dltots.MerchandiseAmount);
        }

        [Then(@"Non merchandice amount equal (.*)")]
        public void ThenNonMerchandiceAmountEqual(decimal nonMercAmount)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(nonMercAmount, dltots.NonMerchandiseAmount);
        }

        [Then(@"Total amount equal (.*)")]
        public void ThenTotalAmountEqual(decimal totalAmount)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(totalAmount, dltots.TotalSaleAmount);
        }

        [Then(@"Price of SKU (.*) is (.*) pounds")]
        public void ThenPriceOfSKUIsPounds(string skuNumber, decimal amount)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(amount, dlline.Price);
        }

        [Then(@"Initial price of SKU (.*) is equal to db price")]
        public void ThenInitialPriceOfSKUIsEqualToDbPrice(string skuNumber)
        {
            decimal stockPrice = DataLayerFactory.Create<TestStockRepository>().GetStockMasterPrice(skuNumber);
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);

            Assert.IsNotNull(dlline);
            Assert.AreEqual(stockPrice, dlline.InitialPrice);
        }

        [Then(@"Price override change price difference of SKU (.*) is (.*) pounds")]
        public void ThenPriceOverrideChangePriceDifferenceOfSkuIsPounds(string skuNumber, decimal amount)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(amount, dlline.PriceOverrideChangePriceDifference);
        }

        [Then(@"Employee discount amount of SKU (.*) is (.*) pounds")]
        public void ThenEmployeeDiscountAmountOfSkuIsPounds(string skuNumber, decimal amount)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(amount, dlline.EmployeeDiscountAmount);
        }

        [Then(@"Price override code of SKU (.*) is Managers Discretion")]
        public void ThenPriceOverrideCodeOfSkuIsManagersDiscretion(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(dlline.PriceOverrideCode, Int32.Parse(ModelMapper.GetDbModelCode(BO.Model.Enums.PriceOverrideReasonCode.MANAGERS_DISCRETION)));
        }

        [Then(@"Price override code of SKU (.*) is Temporary Deal Group")]
        public void ThenPriceOverrideCodeOfSkuIsTemporaryDealGroup(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(dlline.PriceOverrideCode, Int32.Parse(BO.DataLayer.Model.Constants.PriceOverrideReasonCode.TemporaryDealGroup));
        }

        [Then(@"Price override code of SKU (.*) is Damaged Goods")]
        public void ThenPriceOverrideCodeOfSkuIsDamagedGoods(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(dlline.PriceOverrideCode, Int32.Parse(ModelMapper.GetDbModelCode(BO.Model.Enums.PriceOverrideReasonCode.DAMAGED_GOODS)));
        }

        [Then(@"Price override code of SKU (.*) is (.*)")]
        public void ThenPriceOverrideCodeOfSKUIs(string skuNumber, short priceOverrideCode)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(priceOverrideCode, dlline.PriceOverrideCode);
            Assert.AreEqual(ConversionUtils.ToEventCode(BO.DataLayer.Model.Constants.PriceOverrideReasonCode.NoPriceOverride), dlline.PriceOverrideMarginErosionCode);
        }

        [Then(@"Price override code of SKU (.*) is empty")]
        public void ThenPriceOverrideCodeOfSKUIsEmpty(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(0, dlline.PriceOverrideCode);
        }

        [Then(@"Price override code of SKU (.*) is Refund Price Override")]
        public void ThenPriceOverrideCodeOfSkuIsRefundPriceOverride(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(dlline.PriceOverrideCode, Int32.Parse(ModelMapper.GetDbModelCode(BO.Model.Enums.PriceOverrideReasonCode.REFUND_PRICE_DIFFERENCE)));
        }

        [Then(@"There is cheque payment for (.*) pounds")]
        public void ThenThereIsChequePaymentForPounds(decimal amount)
        {
            CheckChequePayment(amount, Direction.FROM_CUSTOMER);
        }

        [Then(@"There is GiftCard payment for (.*) pounds")]
        public void ThenThereIsGiftCardPaymentForPounds(decimal amount)
        {
            CheckGiftCardPayment(amount, Direction.FROM_CUSTOMER);
        }

        [Then(@"There is cash change for (.*) pounds")]
        public void ThenThereIsCashChangeForPounds(decimal amount)
        {
            CheckCashChangePayment(amount);
        }

        [Then(@"Original transaction values are empty in customer table")]
        public void ThenOriginalTransactionValuesAreEmptyInCustomerTable()
        {
            var dlrcus = dailyTillRepo.SelectDailyCustomerWithNonZeroLineNumber(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlrcus);
            Assert.AreEqual(Ctx.SentTransaction.CreateDateTime.Date, dlrcus.OriginalSaleDate);
            Assert.AreEqual("00", dlrcus.OriginalTillNumber.Trim());
            Assert.AreEqual("0000", dlrcus.OriginalTransactionNumber);
            Assert.AreEqual(dictRepo.GetStoreId(), dlrcus.OriginalSaleStore);
        }

        [Then(@"There is (.*) line in customer table for items")]
        public void ThenThereIsLineInCustomerTableForItems(int lineAmount)
        {
            var realLineAmount = dailyTillRepo.GetCountOfDailyCustomerWithNonZeroLineNumber(Ctx.SentTransactionKey);
            Assert.AreEqual(lineAmount, realLineAmount);
        }

        [Then(@"There is transaction in db for parked refund")]
        public void ThenThereIsTransactionInDbForParkedRefund()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.Sale, dltots.TransactionCode);
            Assert.Less(dltots.MerchandiseAmount, 0);
            Assert.Less(dltots.TotalSaleAmount, 0);
            Assert.AreEqual(0, dltots.TaxAmount);
            Assert.AreEqual(0, dltots.VatValue1);
            Assert.AreEqual(0, dltots.ExVatValue1);
            Assert.IsTrue(dltots.IsParked);
            Assert.IsTrue(dltots.IsVoid);
        }

        [Then(@"Data in ledger has RTI flag ToBeSent")]
        public void ThenDataInLedgerHasRtiFlagToBeSent()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(RtiStatus.ToBeSent, dltots.RtiFlag);

            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(RtiStatus.ToBeSent, dlline.RtiFlag);

            var dlpaid = dailyTillRepo.SelectDailyPayment(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(RtiStatus.ToBeSent, dlpaid.RtiFlag);

            var dlcomm = dailyTillRepo.SelectDlcomm(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlcomm);
            Assert.AreEqual(RtiStatus.ToBeSent, dlcomm.RtiFlag);

            var dlrcus = dailyTillRepo.SelectDailyCustomer(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlrcus);
            Assert.AreEqual(RtiStatus.ToBeSent, dlrcus.RtiFlag);

            //var dlevnt = dailyTillRepo.SelectDlevnts(DailyTillTranKey).First();
            //Assert.AreEqual(RtiStatus.ToBeSent, dlevnt.RtiFlag);

            //var dlanas = dailyTillRepo.SelectDlanases(DailyTillTranKey).First();
            //Assert.AreEqual(RtiStatus.ToBeSent, dlanas.RtiFlag);

            //var dlgift = dailyTillRepo.SelectDlgifts(DailyTillTranKey).First();
            //Assert.AreEqual(RtiStatus.ToBeSent, dlgift.RtiFlag);

            //var dlocus = dailyTillRepo.SelectDlocuses(DailyTillTranKey).First();
            //Assert.AreEqual(RtiStatus.ToBeSent, dlocus.RtiFlag );

            //var dlolin = dailyTillRepo.SelectDlolins(DailyTillTranKey).First();
            //Assert.AreEqual(RtiStatus.ToBeSent, dlolin.RtiFlag);

            //var dlreject = dailyTillRepo.SelectDlrejects(DailyTillTranKey).First();
            //Assert.AreEqual(RtiStatus.ToBeSent, dlreject.RtiFlag);
        }

        [Then(@"Data in stock has RTI flag ToBeSent")]
        public void ThenDataInStockHasRtiFlagToBeSent()
        {
            var sku = ((RetailTransaction)Ctx.SentTransaction).Items
                .Select(x => x.Product).OfType<SkuProduct>()
                .Select(x => x.BrandProductId).First();

            var stklog = DataLayerFactory.Create<TestStockRepository>().GetLastStklogForSku(sku);
            Assert.AreEqual(RtiStatus.ToBeSent, stklog.RtiStatus);
        }

        [Then(@"Data in database about gift card has RTI flag ToBeSent")]
        public void ThenDataInDatabaseAboutGiftCardHasRtiFlagToBeSent()
        {
            var giftcard = dailyTillRepo.SelectDailyGiftCard(Ctx.SentTransactionKey);
            Assert.IsNotNull(giftcard);
            Assert.AreEqual(RtiStatus.ToBeSent, giftcard.RtiFlag);
        }

        [Then(@"Data in database about event discount has RTI flag ToBeSent")]
        public void ThenDataInDatabaseAboutEventDiscountHasRTIFlagToBeSent()
        {
            var dlevent = dailyTillRepo.SelectDailyEvent(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlevent);
            Assert.AreEqual(RtiStatus.ToBeSent, dlevent.RtiFlag);
        }

        [Then(@"Data in database about coupon has RTI flag ToBeSent")]
        public void ThenDataInDatabaseAboutCouponHasRTIFlagToBeSent()
        {
            var dlcoupon = dailyTillRepo.SelectDailyCoupon(Ctx.SentTransactionKey);
            Assert.IsNotNull(dlcoupon);
            Assert.AreEqual(RtiStatus.ToBeSent, dlcoupon.RtiFlag);
        }

        [Then(@"There is Misc Income in BO db")]
        public void ThenThereIsMiscIncomeInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.MiscIncome, dltots.TransactionCode);
        }

        [Then(@"There are lines in safe for new trading period")]
        public void ThenThereAreLinesInSafeForNewTradingPeriod()
        {
            var dateTime = Ctx.SentTransaction.CreateDateTime;
            Assert.AreEqual(dateTime.Date, bankingRepo.GetSafeLastPeriodDate().Date); //Check TodayPeriodIdGenerating
            Assert.AreEqual(miscRepo.GetPeriodId(dateTime), bankingRepo.GetSafeDenomsLastPeriodId());

            var safePeriodIds = bankingRepo.GetSafePeriodIdList(); //Check InterveningPeriodIdGenerating
            var safeDenomsPeriodIds = bankingRepo.GetSafeDenomsPeriodIdList();

            Assert.IsTrue(safePeriodIds.SequenceEqual(safeDenomsPeriodIds));

            var isInRightOrder = false;

            for (var i = 0; i < safePeriodIds.Count - 1; i++)
            {
                isInRightOrder = safePeriodIds[i] == safePeriodIds[i + 1] - 1;
            }

            Assert.IsTrue(isInRightOrder);
        }

        [Then(@"Parked transaction is stated as recalled")]
        public void ThenParkedTransactionIsStatedAsRecalled()
        {
            var dltots = dailyTillRepo.SelectDailyTotal(((RetailTransaction)Ctx.SentTransaction).RecalledTransactionId);
            Assert.IsNotNull(dltots);
            Assert.IsFalse(dltots.IsParked);
            Assert.IsTrue(dltots.IsRecalled);
            Assert.AreEqual("0001", dltots.StatusSequence);
            Assert.AreEqual("20", dltots.SaveStatus);
        }

        [Then(@"There is parked sale in BO db")]
        public void ThenThereIsParkedSaleInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.IsTrue(dltots.IsParked);
        }

        [Then(@"no new record is created in BO db")]
        public void ThenNoNewRecordIsCreatedInBackOfficeDatabase()
        {
            var actual = DataLayerFactory.Create<IDailyTillRepository>().GetDailyTillTranCount(Ctx.SentTransaction.SourceTransactionId, Ctx.SentTransaction.Source);
            Assert.AreEqual(1, actual);
        }

        [Then(@"There is (.*) line with Delivery Charge item with SKU '(.*)' with amount of (.*) pounds")]
        public void ThenThereIsLineWithDeliveryChargeItemWithSKUWithAmountOfPounds(int quantity, string skuNumber, decimal amount)
        {
            var dlline = CheckNewDllineInDatabase(quantity, skuNumber, Direction.TO_CUSTOMER);
            Assert.AreEqual(amount, dlline.Price);
        }

        [Then(@"There is a Price Match or Promise line item information with Competitor Name '(.*)'")]
        public void ThenThereIsAPriceMatchOrPromiseLineItemInformationWithPricePoundsAndCompetitorName(string competitorName)
        {
            var dlolin = dailyTillRepo.SelectDlolin(Ctx.SentTransactionKey);

            Assert.IsNotNull(dlolin);
            Assert.AreEqual(competitorName, dlolin.Name.Trim());
        }

        [Then(@"There is a Price Match or Promise line item information with Competitor Name '(.*)' for product line (.*)")]
        public void ThenThereIsAPriceMatchOrPromiseLineItemInformationWithCompetitorNameForProductLine(string competitorName, short lineNumber)
        {
            var dlolin = dailyTillRepo.SelectDlolin(Ctx.SentTransactionKey, lineNumber);

            Assert.IsNotNull(dlolin);
            Assert.AreEqual(competitorName, dlolin.Name.Trim());
        }

        [Then(@"Price Override Code for SKU (.*) is Price Match code")]
        public void ThenPriceOverrideCodeForSKUIsPriceMatchCode(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);

            Assert.IsNotNull(dlline);
            Assert.AreEqual(System.Convert.ToInt16(BO.DataLayer.Model.Constants.PriceOverrideReasonCode.PriceMatchOrPromise), dlline.PriceOverrideCode);
            Assert.AreEqual(ConversionUtils.ToEventCode(BO.DataLayer.Model.Constants.PriceOverrideReasonCode.PriceMatchOrPromise), dlline.PriceOverrideMarginErosionCode);
        }

        [Then(@"'(.*)' event code is '(.*)' for SKU '(.*)'")]
        public void ThenEventCodeIsForSKU(string discountName, string eventCode, string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);

            CheckEventDiscountCode(GetEventDiscountTypeByName(discountName), eventCode, dlline);
        }

        [Then(@"There is a '(.*)' event discount with amount (.*) and code from database for SKU '(.*)'")]
        public void ThenThereIsAEventDiscountWithAmountAndCodeFromDatabaseForSKU(string discountName, decimal amount, string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);

            var eventCode = dictRepo.GetSettingStringValue(ParameterId.HierarchySpendEventDiscountCode);

            CheckEventDiscountCode(GetEventDiscountTypeByName(discountName), eventCode, dlline);

            Assert.AreEqual(amount, dlline.HierachyChangePriceDifference);
        }

        [Then(@"There is no '(.*)' event discount for SKU (.*)")]
        public void ThenThereIsNoEventDiscountForSKU(string discountName, string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);

            Assert.AreEqual(Global.NullEventCode, dlline.HiearchyMarginErosionCode.Trim());
            Assert.AreEqual(dlline.HierachyChangePriceDifference, 0.00m);
        }

        [Then(@"'(.*)' event saving is (.*) pounds for SKU '(.*)'")]
        public void ThenEventSavingIsPoundsForSKU(string discountName, decimal eventAmount, string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);

            CheckEventDiscountAmount(GetEventDiscountTypeByName(discountName), eventAmount, dlline);
        }

        [Then(@"Event saving for transaction is (.*) pounds")]
        public void ThenEventSavingForTransactionIsPounds(decimal eventAmount)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);

            Assert.AreEqual(dltots.DiscountAmount, -eventAmount);
        }


        [Then(@"'(.*)' event with saving is added to db")]
        public void ThenEventWithSavingIsAddedToDb(string discountName)
        {
            var dlevnt = dailyTillRepo.SelectDailyEvent(Ctx.SentTransactionKey, ModelMapper.GetDbModelCode(GetEventDiscountTypeByName(discountName)));

            Assert.IsNotNull(dlevnt);
        }

        [Then(@"(.*) event records are added to db")]
        public void ThenEventRecordsAreAddedToDb(int recordsCount)
        {
            var dlevnts = dailyTillRepo.SelectDlevnts(Ctx.SentTransactionKey);

            Assert.IsNotNull(dlevnts);
            Assert.AreEqual(recordsCount, dlevnts.Count());
        }

        [Then(@"event record with sequence number '(.*)' contains amount (.*) pounds")]
        public void ThenEventRecordWithSequenceNumberContainsAmountPounds(string sequenceNumber, decimal eventAmount)
        {
            var dlevnt = dailyTillRepo.SelectDailyEventBySequenceNumber(Ctx.SentTransactionKey, sequenceNumber);

            Assert.IsNotNull(dlevnt);
            Assert.AreEqual(eventAmount, dlevnt.DiscountAmount);
        }

        [Then(@"event record with sequence number '(.*)' has type '(.*)'")]
        public void ThenEventRecordWithSequenceNumberHasType(string sequenceNumber, string shortType)
        {
            var dlevnt = dailyTillRepo.SelectDailyEvent(Ctx.SentTransactionKey, ModelMapper.GetDbModelCode(GetEventDiscountTypeByName(shortType)));

            Assert.IsNotNull(dlevnt);
            Assert.AreEqual(sequenceNumber, dlevnt.EventSequenceNumber);
        }

        [Then(@"Both event records have sequence number '(.*)'")]
        public void ThenBothEventRecordsHaveSequenceNumber(string sequenceNumber)
        {
            var dlevnts = dailyTillRepo.SelectDlevnts(Ctx.SentTransactionKey);

            foreach(var dlevnt in dlevnts)
            {
                Assert.AreEqual(sequenceNumber, dlevnt.EventSequenceNumber);
            }
        }

        [Then(@"(.*) Coupon record is added to db")]
        public void ThenCouponRecordIsAddedToDb(int couponsCount)
        {
            var dlcoupons = dailyTillRepo.SelectDlcoupons(Ctx.SentTransactionKey);

            Assert.IsNotNull(dlcoupons);
            Assert.AreEqual(couponsCount, dlcoupons.Count());
        }

        [Then(@"Coupon record has sequence number '(.*)' and coupon id '(.*)'")]
        public void ThenCouponRecordHasSequenceNumberAndCouponId(string sequenceNumber, string couponId)
        {
            var dlcoupon = dailyTillRepo.SelectDailyCoupon(Ctx.SentTransactionKey);

            Assert.AreEqual(sequenceNumber, dlcoupon.SequenceNumber);
            Assert.AreEqual(couponId, dlcoupon.CouponId);
        }

        [Then(@"The Coupon record Marketing Reference Number is '(.*)' and Coupon Serial Number is (.*)")]
        public void ThenTheCouponRecordMarketingReferenceNumberIsAndCouponSerialNumberIs(string marketingReferenceNumber, string couponSerialNumber)
        {
            var dlcoupon = dailyTillRepo.SelectDailyCoupon(Ctx.SentTransactionKey);

            Assert.AreEqual(marketingReferenceNumber, dlcoupon.MarketingReferenceNumber);
            Assert.AreEqual(couponSerialNumber, dlcoupon.CouponSerialNumber);
        }

        [Then(@"There is no lines in Rejected Items file")]
        public void ThenThereIsNoLinesInRejectedItemsFile()
        {
            var dlreject = dailyTillRepo.SelectDlrejects(Ctx.SentTransactionKey);

            Assert.IsEmpty(dlreject);
        }

        [Then(@"There is a line in Rejected Items file for SKU (.*) with Authorizer Number '(.*)' and Confirmation Text '(.*)'")]
        public void ThenThereIsALineInRejectedItemsFileForSKUWithAuthorizerNumberAndConfirmationText(string skuNumber, string authorizerNumber, string confirmationText)
        {
            var dlreject = dailyTillRepo.SelectDlreject(Ctx.SentTransactionKey, skuNumber);

            Assert.IsNotNull(dlreject);

            Assert.AreEqual(dlreject.EmployeeId, authorizerNumber);
            Assert.AreEqual(dlreject.RejectText.Trim().Replace("\r\n", string.Empty), confirmationText.Trim());
        }

        [Then(@"There is a line in Rejected Items file for SKU (.*) with Sequence Number (.*)")]
        public void ThenThereIsALineInRejectedItemsFileForSKUWithSequenceNumber(string skuNumber, decimal sequenceNumber)
        {
            var dlreject = dailyTillRepo.SelectDlreject(Ctx.SentTransactionKey, skuNumber);

            Assert.IsNotNull(dlreject);
            Assert.AreEqual(dlreject.SequenceNumber, sequenceNumber);
        }


        [Then(@"A line for SKU (.*) is marked as Explosive")]
        [Then(@"Lines for SKU (.*) are marked as Explosive")]
        public void ThenALineForSKUIsMarkedAsExplosive(string skuNumber)
        {
            var dllines = dailyTillRepo.SelectDailyTillLines(Ctx.SentTransactionKey, skuNumber);

            foreach (var dlline in dllines)
            {
                Assert.IsTrue(dlline.IsExplosive);
            }
        }

        [Then(@"Supervisor Number for the Transaction is '(.*)'")]
        public void ThenSupervisorNumberForTheTransactionIs(string supervisorNumber)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(supervisorNumber, dltots.DiscountSupervisor);
        }

        [Then(@"Transaction is marked as Online")]
        public void ThenTransactionIsMarkedAsOnline()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsTrue(dltots.IsOnline);
        }

        [Then(@"There is Customer Line with the sale Date, Till Number, Transaction Number and Postcode '(.*)' and other fields with default values")]
        public void ThenThereIsCustomerLineWithTheSaleDateTillNumberTransactionNumberAndPostcodeAndOtherFieldsWithDefaultValues(string postcode)
        {
            var dlrcus = dailyTillRepo.SelectDailyCustomer(Ctx.SentTransactionKey);

            Assert.IsNotNull(dlrcus);

            Assert.AreEqual(postcode, dlrcus.Postcode.Trim());

            Assert.IsEmpty(dlrcus.AddressLine1.Trim());
            Assert.IsEmpty(dlrcus.AddressLine2.Trim());
            Assert.IsEmpty(dlrcus.CustomerName.Trim());
            Assert.IsEmpty(dlrcus.Email.Trim());
            Assert.IsEmpty(dlrcus.HouseNumber.Trim());
            Assert.IsFalse(dlrcus.IsOriginalTransactionValidated); 
            Assert.IsFalse(dlrcus.LabelsRequired);
            Assert.AreEqual(0, dlrcus.LineNumber);
            Assert.IsEmpty(dlrcus.MobilePhone.Trim());
            Assert.AreEqual(Global.NullOriginalSaleStore, dlrcus.OriginalSaleStore);
            Assert.AreEqual(Global.NullOriginalTenderType, dlrcus.OriginalTenderType);
            Assert.AreEqual(Global.NullOriginalTillNumber, dlrcus.OriginalTillNumber);
            Assert.AreEqual(Global.NullOriginalTransactionNumber, dlrcus.OriginalTransactionNumber);
            Assert.IsNull(dlrcus.OriginalSaleDate);
            Assert.IsEmpty(dlrcus.PhoneNumber.Trim());
            Assert.AreEqual(dlrcus.RefundReasonCode, Global.NullRefundReasonCode);
            Assert.IsNull(dlrcus.Spare);
            Assert.IsEmpty(dlrcus.Town.Trim());
            Assert.IsEmpty(dlrcus.WebOrderNumber.Trim());
            Assert.IsEmpty(dlrcus.WorkPhone.Trim());
        }

        [Then(@"There is no non zero Daily Customer lines for the Sale")]
        public void ThenThereIsNoNonZeroDailyCustomerLinesForTheSale()
        {
            var dlrcus = dailyTillRepo.SelectDlrcuses(Ctx.SentTransactionKey);

            Assert.IsNotNull(dlrcus);
            Assert.IsFalse(dlrcus.Any(x => x.LineNumber != 0));
        }

        [Then(@"There is no Daily Customer lines for generated Refund")]
        public void ThenThereIsNoDailyCustomerLinesForGeneratedRefund()
        {
            Dltots dltots = dailyTillRepo.SelectGeneratedDailyTillTran(Ctx.SentTransaction.TransactionNumber, Ctx.SentTransaction.Source, TransactionCodes.Refund);

            var dlrcus = dailyTillRepo.SelectDlrcuses
                (
                    new DailyTillTranKey
                    {
                        CreateDate = dltots.CreateDate,
                        TillNumber = dltots.TillNumber,
                        TransactionNumber = dltots.TransactionNumber
                    }
                );

            Assert.IsEmpty(dlrcus);
        }

        [Then(@"Line for SKU (.*) is from markdown stock")]
        public void ThenLineForSKUIsFromMarkdownStock(string skuNumber)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);

            Assert.IsNotNull(dlline);
            Assert.IsTrue(dlline.IsFromMarkDown);
        }

        #endregion

        private Dlpaid CheckPaymentWithTender(decimal amount, int tenderType, Direction direction)
        {
            var dlpaid = dailyTillRepo.SelectDailyPayment(Ctx.SentTransactionKey, tenderType);

            Assert.IsNotNull(dlpaid);
            Assert.AreEqual(direction == Direction.FROM_CUSTOMER ? -amount : amount, dlpaid.Amount);

            return dlpaid;
        }

        private static CardType GetTypeCardByName(string cardType)
        {
            switch (cardType)
            {
                case "Visa":
                    {
                        return CardType.VISA;
                    }
                case "Maestro":
                    {
                        return CardType.MAESTRO;
                    }
                case "Master":
                    {
                        return CardType.MASTER;
                    }
                case "AmEx":
                    {
                        return CardType.AMERICAN_EXPRESS;
                    }
                default:
                    {
                        return CardType.UNKNOWN;
                    }
            }
        }

        private EventType GetEventDiscountTypeByName(string eventName)
        {
            switch (eventName)
            {
                case "DealGroup":
                    return EventType.DEAL_GROUP;
                case "QuantityBreak":
                    return EventType.QUANTITY_BREAK;
                case "Multibuy":
                    return EventType.MULTIBUY;
                case "HierarchySpend":
                    return EventType.HIERARCHY_SPEND;
            }
            throw new ArgumentException("Invalid card type name");
        }

        private void CheckCashPayment(decimal amount, Direction direction)
        {
            CheckPaymentWithTender(amount, TenderType.Cash, direction);
        }

        private void CheckCreditCardPayment(string cardType, decimal amount, Direction direction)
        {
            CheckPaymentWithTender(amount,
                ModelMapper.GetTenderTypeCode(GetTypeCardByName(cardType)),
                direction);
        }

        private void CheckGiftCardPayment(decimal amount, Direction direction)
        {
            var dlpaid = CheckPaymentWithTender(amount, TenderType.GiftCard, direction);
            Assert.IsNotNull(dlpaid);
            Assert.AreEqual(AuthorizationType.Online, dlpaid.AuthType);
            Assert.AreNotEqual(String.Empty, dlpaid.CardDescription.Trim());
        }

        private void CheckChequePayment(decimal amount, Direction direction)
        {
            var dlpaid = CheckPaymentWithTender(amount, TenderType.Cheque, direction);
            Assert.AreEqual(AuthorizationType.Online, dlpaid.AuthType);
            Assert.AreEqual("999", dlpaid.AuthCode.Trim());
        }

        private void CheckCashChangePayment(decimal amount)
        {
            var dlpaid = dailyTillRepo.SelectDailyPayment(Ctx.SentTransactionKey, TenderType.Change);

            Assert.IsNotNull(dlpaid);
            Assert.AreEqual(amount, dlpaid.Amount);
        }

        private int GetVatCode(decimal vatRate)
        {
            var vatRates = dictRepo.GetVatRatesListFromRetopt();
            return vatRates.IndexOf(vatRate) + 1;
        }

        private void CheckDltots(short reasonCode, string description, string transactionCode)
        {
            Dltots dltots = dailyTillRepo.SelectDailyTotal(Ctx.SentTransactionKey, transactionCode);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(reasonCode, dltots.Misc);
            Assert.AreEqual(description, dltots.Description.TrimEnd());
        }

        private void CheckGeneratedDltots(short reasonCode, string description, string transactionCode)
        {
            Dltots dltots = dailyTillRepo.SelectGeneratedDailyTillTran(Ctx.SentTransaction.TransactionNumber, Ctx.SentTransaction.Source, transactionCode);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(reasonCode, dltots.Misc);
            Assert.AreEqual(description, dltots.Description.TrimEnd());
        }

        /// <summary>
        /// Checks existense of new dlline in database with given quantity and skuNumber.
        /// </summary>
        private DailyTillLine CheckNewDllineInDatabase(int quantity, string skuNumber, Direction direction)
        {
            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, skuNumber);
            Assert.IsNotNull(dlline);
            Assert.AreEqual(direction == Direction.TO_CUSTOMER ? quantity : -quantity, dlline.Quantity);

            return dlline;
        }

        private void CheckGeneratedDlpaid(string transactionCode, decimal amount)
        {
            Dltots dltots = dailyTillRepo.SelectGeneratedDailyTillTran(Ctx.SentTransaction.TransactionNumber, Ctx.SentTransaction.Source, transactionCode);
            Assert.IsNotNull(dltots);
            var dlpaid = dailyTillRepo.SelectDailyPayment(new DailyTillTranKey() { TransactionNumber = dltots.TransactionNumber, CreateDate = dltots.CreateDate, TillNumber = dltots.TillNumber }, TenderType.Cash);
            Assert.IsNotNull(dlpaid);
            Assert.AreEqual(dlpaid.Amount, amount);
        }

        private void CheckEventDiscountCode(EventType eventType, string code, DailyTillLine dlline)
        {
            switch (eventType)
            {
                case EventType.DEAL_GROUP:
                    Assert.AreEqual(dlline.DealGroupMarginErosionCode, code);
                    break;
                case EventType.HIERARCHY_SPEND:
                    Assert.AreEqual(dlline.HiearchyMarginErosionCode, code);
                    break;
                case EventType.MULTIBUY:
                    Assert.AreEqual(dlline.MultibuyMarginErosionCode, code);
                    break;
                case EventType.QUANTITY_BREAK:
                    Assert.AreEqual(dlline.QuantityBreakMarginErosionCode, code);
                    break;
            }
        }

        private void CheckEventDiscountAmount(EventType eventType, decimal amount, DailyTillLine dlline)
        {
            switch (eventType)
            {
                case EventType.DEAL_GROUP:
                    Assert.AreEqual(dlline.DealGroupChangePriceDifference, amount);
                    break;
                case EventType.HIERARCHY_SPEND:
                    Assert.AreEqual(dlline.HierachyChangePriceDifference, amount);
                    break;
                case EventType.MULTIBUY:
                    Assert.AreEqual(dlline.MultibuyChangePriceDifference, amount);
                    break;
                case EventType.QUANTITY_BREAK:
                    Assert.AreEqual(dlline.QuantityBreakChangePriceDifference, amount);
                    break;
            }
        }
    }
}
