﻿using WSS.AAT.Common.Utility.Bindings;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    public class TransactionStepDefinitions : BaseStepDefinitions
    {
        protected TransactionScenarioContext Ctx { get; private set; }
        public IDataLayerFactory DataLayerFactory { get; private set; }

        protected TransactionStepDefinitions(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
        {
            Ctx = ctx;
            DataLayerFactory = dataLayerFactory;
        }
    }
}
