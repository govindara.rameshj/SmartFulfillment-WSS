using System;
using System.Collections.Generic;
using Cts.Oasys.Core.SystemEnvironment;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Configuration;
using WSS.AAT.Common.Utility.Executables;
using WSS.AAT.Common.Utility.FileParsers;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class BankingReportingSteps : TransactionStepDefinitions
    {
        private readonly ISystemEnvironment systemEnvironment;
        private readonly TestNitmasRepository _nitmasRepository;
        private readonly TestMiscRepository _miscRepository;
        private readonly Dictionary<string, int> _accounts = new Dictionary<string, int>();
        private readonly string executablesDir;

        public BankingReportingSteps(
            TransactionScenarioContext ctx,
            IDataLayerFactory dataLayerFactory,
            TestConfiguration testConfiguration,
            ISystemEnvironment systemEnvironment) : base(ctx, dataLayerFactory)
        {
            this.systemEnvironment = systemEnvironment;
            executablesDir = testConfiguration.ResolvePathFromAppSettings("executablesDir");

            _nitmasRepository = dataLayerFactory.Create<TestNitmasRepository>();
            _miscRepository = dataLayerFactory.Create<TestMiscRepository>();

            _accounts.Add("Cash", 8415);
        }

        #region Steps

        [When(@"Banking report for tomorrow was created")]
        public void WhenBankingReportForTomorrowWasCreated()
        {
            CreateReport(DateTime.Today.AddDays(1));
        }

        [When(@"Banking report for today was created")]
        public void WhenBankingReportForTodayWasCreated()
        {
            CreateReport(DateTime.Today);
        }

        [Then(@"Banking report contains info for SKU (.*) with daily quantity (.*) and daily total (.*)")]
        public void ThenBankingReportForTodayContainsInfoForSkuWithDailyQuantityAndDailyTotal(string skuNumber, int quantity, decimal total)
        {
            CheckReportForSku(skuNumber, quantity, total);
        }

        [Then(@"Banking report for today contains info for account '(.*)' with total (.*)")]
        public void ThenBankingReportForTodayContainsInfoForAccountWithTotal(string accountName, decimal total)
        {
            CheckReportForAccountBalance(DateTime.Today, accountName, total);
        }

        [Then(@"Banking report for tomorrow contains info for account '(.*)' with total (.*)")]
        public void ThenBankingReportForTomorrowContainsInfoForAccountWithTotal(string accountName, decimal total)
        {
            CheckReportForAccountBalance(DateTime.Today.AddDays(1), accountName, total);
        }

        #endregion

        #region Internal work

        [Given(@"Database was empty")]
        public void GivenDatabaseWasEmpty()
        {
            _nitmasRepository.ClearOrderTables();
            _nitmasRepository.ClearCashBalTables();
        }

        private void CreateReport(DateTime bankingDate)
        {
            var periodId = _miscRepository.GetPeriodId(bankingDate);
            _nitmasRepository.CloseBankingDay(periodId);

            var pt = new ProcessTransmissions(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.RunNightlyRetailUpdates();

            pt.ProcessFileCode("PO SOCLOS=STHPO", bankingDate);
            pt.ProcessFileCode("SA", bankingDate);
            pt.ProcessFileCode("DB", bankingDate);
            pt.ProcessFileCode("OT", bankingDate);

            //We should open the closed day for the next tests
            _nitmasRepository.OpenBankingDay(periodId);
        }

        private void CheckReportForSku(string skuNumber, int quantity, decimal total)
        {
            var parser = new SthoaFileParser(systemEnvironment);
            Assert.AreEqual(quantity, parser.GetA5DailyUnitsForSKU(skuNumber));
            Assert.AreEqual(total, parser.GetA5DailyTotalForSKU(skuNumber));
        }

        private void CheckReportForAccountBalance(DateTime dateTime, string accountName, decimal total)
        {
            var parser = new SthoaFileParser(systemEnvironment);
            var accountNumber = _accounts[accountName];
            Assert.AreEqual(total, parser.GetA8ValueByAccountOn(accountNumber.ToString(), dateTime));
        }

        #endregion
    }
}
