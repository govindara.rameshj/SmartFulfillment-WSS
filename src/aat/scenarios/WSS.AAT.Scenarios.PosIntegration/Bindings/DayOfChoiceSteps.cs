using System;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    class DayOfChoiceSteps : TransactionStepDefinitions
    {
        private readonly TestCustomerOrderRepository orderRepo;

        public DayOfChoiceSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            orderRepo = DataLayerFactory.Create<TestCustomerOrderRepository>();
        }

        private string slotId;
        private string slotDescription;
        private TimeSpan slotStartTime = TimeSpan.MinValue;
        private TimeSpan slotEndTime = TimeSpan.MinValue;
        
        private DayOfWeek deliveryDay;
        private const int Fulfiller = 8080;

        private RetailTransactionBuilder RetailTransactionBuilder
        {
            get { return (RetailTransactionBuilder)Ctx.TransactionBuilder; }
        }

        [Given(@"new delivery order is created on Ovc Till")]
        public void GivenNewDeliveryOrderIsCreatedOnOvcTill()
        {
            var tranCreationSteps = new RetailTransactionCreationSteps(Ctx, DataLayerFactory);
            tranCreationSteps.GivenSaleHappened();
            tranCreationSteps.GivenSkuSold(5, "500300");

            var orderCreationSteps = new OrderCreationSteps(Ctx, DataLayerFactory);
            orderCreationSteps.GivenSkuWereCollectedInstantly(3, 1);
            orderCreationSteps.GivenSkuWereDelivered(2, 1);
        }

        [Given(@"fulfillment slot is set")]
        public void GivenFulfillmentSlotIsSet()
        {
            RetailTransactionBuilder.AddSlotToDeliveryFulFillment(slotId, slotDescription, slotStartTime, slotEndTime);
        }

        [Given(@"fulfillment slot id is (.*)")]
        public void GivenFulfillmentSlotIdIs(string id)
        {
            slotId = id;
        }

        [Given(@"description is (.*)")]
        public void GivenDescriptionIs(string description)
        {
            slotDescription = description;
        }

        [Given(@"start time is (.*)")]
        public void GivenStartTimeIs(string startTime)
        {
            slotStartTime = TimeSpan.Parse(startTime);
        }

        [Given(@"end time is (.*)")]
        public void GivenEndTimeIs(string endTime)
        {
            slotEndTime = TimeSpan.Parse(endTime);
        }

        [Then(@"specified slot is saved to DB")]
        public void ThenSpecifiedSlotIsSavedToDb()
        {
            AssertThatSlotIsSavedToDb(slotId, slotDescription, slotStartTime, slotEndTime);
        }

        [Given(@"fulfiller is set")]
        public void GivenFulfillerIsSet()
        {
            RetailTransactionBuilder.AddRequiredFulfiller(Fulfiller);
        }

        [Then(@"specified fulfiller is saved to DB")]
        public void ThenSpecifiedFulfillerIsSavedToDb()
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corlins = orderRepo.SelectCorlins(orderNumber);
            Assert.That(corlins.Any(), Is.True);
            foreach(var corlin in corlins)
            {
                Assert.That(corlin.RequiredFulfiller, Is.EqualTo(Fulfiller));
            }
        }

        [Given(@"delivery date is (.*)")]
        public void GivenDeliveryDateIsDayOfWeek(DayOfWeek dow)
        {
            deliveryDay = dow;
            RetailTransactionBuilder.MoveDeliveryDateToNextDow(dow);
        }

        [Given(@"fulfillment slot is not set")]
        public void GivenFulfillmentSlotIsNotSet()
        {
            RetailTransactionBuilder.ResetDeliverySlot();
        }

        [Then(@"corresponding slot is saved to DB")]
        public void ThenCorrespondingSlotIsSavedToDb()
        {
            var defaultSlotId = (deliveryDay == DayOfWeek.Saturday) ? DeliverySlotType.Saturday : DeliverySlotType.Weekday;
            var slot = DataLayerFactory.Create<IDictionariesRepository>().GetDeliverySlots().First(s => s.Id == defaultSlotId);

            AssertThatSlotIsSavedToDb(slot.Id, slot.Description, slot.StartTime, slot.EndTime);
        }

        private void AssertThatSlotIsSavedToDb(string innerSlotId, string innerSlotDescription, TimeSpan innerSlotStartTime, TimeSpan innerSlotEndTime)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);
            Assert.That(corhdr.RequiredDeliverySlotID, Is.EqualTo(innerSlotId));
            Assert.That(corhdr.RequiredDeliverySlotDescription, Is.EqualTo(innerSlotDescription));
            Assert.That(corhdr.RequiredDeliverySlotStartTime, Is.EqualTo(innerSlotStartTime));
            Assert.That(corhdr.RequiredDeliverySlotEndTime, Is.EqualTo(innerSlotEndTime));
        }
    }
}
