﻿using System;
using TechTalk.SpecFlow;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Enums;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    sealed class AuditTransactionCreationSteps : TransactionStepDefinitions
    {
        public AuditTransactionCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
        }

        [Given(@"Login happened")]
        public void GivenLoginHappened()
        {
            var builder = new AuditTransactionBuilder(DataLayerFactory, DateTime.Now);
            Ctx.StartNewTransaction(builder);
            builder.SetAuditType(AuditType.SIGN_ON);
        }

        [Given(@"Logout happened")]
        public void GivenLogoutHappened()
        {
            var builder = new AuditTransactionBuilder(DataLayerFactory, DateTime.Now);
            Ctx.StartNewTransaction(builder);
            builder.SetAuditType(AuditType.SIGN_OFF);
        }
    }
}
