﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.Model.Enums;
using EventType = WSS.BO.Model.Enums.EventType;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    public class EventDiscountsCreationSteps : TransactionStepDefinitions
    {
        private readonly IDataLayerFactory dataLayerFactory;

        public EventDiscountsCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            this.dataLayerFactory = dataLayerFactory;
        }

        private RetailTransactionBuilder Builder
        {
            get { return (RetailTransactionBuilder) Ctx.TransactionBuilder; }
        }

        [Given(@"DealGroup event with code '(.*)' is applied once to first line with saving amount (.*) pounds")]
        public void GivenEventWithCodeIsAppliedOnceToFirstLineWithSavingAmountIsPounds(string eventCode, decimal discountAmount)
        {
            Builder.AddEventDiscountToSku(EventType.DEAL_GROUP, discountAmount, eventCode, 1);
        }

        [Given(@"DealGroup event with code '(.*)' is applied once to first line with saving amount for the first hit of (.*) pounds")]
        public void GivenDealGroupEventWithCodeIsAppliedOnceToFirstLineWithSavingAmountForTheFirstHitOfPounds(string eventCode, decimal discountAmount)
        {
            Builder.AddEventDiscountToSku(EventType.DEAL_GROUP, discountAmount, eventCode, 1);
        }

        [Given(@"Multibuy event with code '(.*)' is applied once to first line with saving amount (.*) pounds")]
        public void GivenMultibuyEventWithCodeIsAppliedOnceToFirstLineWithSavingAmountIsPounds(string eventCode, decimal discountAmount)
        {
            Builder.AddEventDiscountToSku(EventType.MULTIBUY, discountAmount, eventCode, 1);
        }

        [Given(@"QuantityBreak event with code '(.*)' is applied once to first line with saving amount (.*) pounds")]
        public void GivenQuantityBreakEventWithCodeIsAppliedOnceToFirstLineWithSavingAmountIsPounds(string eventCode, decimal discountAmount)
        {
            Builder.AddEventDiscountToSku(EventType.QUANTITY_BREAK, discountAmount, eventCode, 1);
        }

        [Given(@"QuantityBreak event is applied once to first line with saving amount (.*) pounds")]
        public void GivenQuantityBreakEventIsAppliedOnceToFirstLineWithSavingAmountIsPounds(decimal discountAmount)
        {
            Builder.AddEventDiscountToSku(EventType.QUANTITY_BREAK, discountAmount, Global.NullEventCode, 1);
        }

        [Given(@"HierarchySpend event with code '(.*)' is applied once to first line with saving amount (.*) pounds")]
        public void GivenHierarchySpendEventWithCodeIsAppliedOnceToFirstLineWithSavingAmountIsPounds(string eventCode, decimal discountAmount)
        {
            Builder.AddEventDiscountToSku(EventType.HIERARCHY_SPEND, discountAmount, eventCode, 1);
        }

        [Given(@"HierarchySpend event with code '(.*)' is applied once to second line with saving amount (.*) pounds")]
        public void GivenHierarchySpendEventWithCodeIsAppliedOnceToSecondLineWithSavingAmountPounds(string eventCode, decimal discountAmount)
        {
            Builder.AddEventDiscountToSku(EventType.HIERARCHY_SPEND, discountAmount, eventCode, 2);
        }

        [Given(@"Multibuy event with code '(.*)' is applied once to (.*) SKU with saving amount (.*) pounds")]
        public void GivenMultibuyEventWithCodeIsAppliedOnceToSKUWithSavingAmountPounds(string eventCode, string skuNumber, decimal discountAmount)
        {
            Builder.AddEventDiscountToSku(EventType.MULTIBUY, discountAmount, eventCode, skuNumber);
        }

        [Given(@"Applied '(.*)' event was triggered by coupon with barcode '(.*)'")]
        public void GivenAppliedEventWasTriggeredByCouponWithBarcode(string eventCode, string couponBarcode)
        {
            Builder.AddCoupon(eventCode, couponBarcode);
        }

        [Given(@"There was a parameter in db containing Quantity Break event discount code '(.*)'")]
        public void GivenThereWasAParameterInDbContainingQuantityBreakEventDiscountCode(string quantityBreakCode)
        {
            var repo = dataLayerFactory.Create<TablesRepository>();
            repo.UpdateStringParameterValue(quantityBreakCode, ParameterId.QuantityBreakEventDiscountCode);
        }

    }
}
