﻿using TechTalk.SpecFlow;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.Model.Enums;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    sealed class PaymentCreationSteps : TransactionStepDefinitions
    {
        public PaymentCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
        }

        private PayableTransactionBuilder Builder
        {
            get { return (PayableTransactionBuilder)Ctx.TransactionBuilder; }
        }

        [Given(@"Was paid by cheque for (.*) pounds")]
        public void GivenWasPaidByChequeForPounds(decimal amount)
        {
            Builder.AddPaymentToTransaction(amount, TenderType.Cheque, Direction.FROM_CUSTOMER);
        }

        [Given(@"Was paid by Visa credit card for (.*) pounds")]
        public void GivenWasPaidByCreditCardForPounds(decimal cashAmount)
        {
            Builder.AddPaymentToTransaction(cashAmount, TenderType.CreditCard, Direction.FROM_CUSTOMER);
        }

        [Given(@"The Card Payment has null Issue Number Switch")]
        public void GivenTheCardPaymentHasNullIssueNumberSwitch()
        {
            Builder.SetIssueNumberSwitchForCardPayment();
        }

        [Given(@"Was paid by GiftCard for (.*) pounds")]
        public void GivenWasPaidByGiftCardForPounds(decimal amount)
        {
            Builder.AddPaymentToTransaction(amount, TenderType.GiftCard, Direction.FROM_CUSTOMER);
        }

        [Given(@"Was paid by GiftCard with number (.*) for (.*) pounds")]
        public void GivenWasPaidByGiftCardWithNumberForPounds(string cardNumber, decimal amount)
        {
            Builder.AddGiftCardPaymentToTransaction(cardNumber, amount, Direction.FROM_CUSTOMER);
        }

        [Given(@"Cash change was executed for (.*) pounds")]
        public void GivenCashChangeWasExecutedForPounds(decimal amount)
        {
            Builder.AddPaymentToTransaction(amount, TenderType.Change, Direction.TO_CUSTOMER);
        }

        [Given(@"Was paid by cash for (.*) pounds")]
        public void GivenWasPaidByCashForPounds(decimal amount)
        {
            Builder.AddPaymentToTransaction(amount, TenderType.Cash, Direction.FROM_CUSTOMER);
        }

        [Given(@"Was refunded by cash for (.*)")]
        [When(@"Was refunded by cash for (.*)")]
        public void GivenWasRefundedByCashFor(decimal amount)
        {
            Builder.AddPaymentToTransaction(amount, TenderType.Cash, Direction.TO_CUSTOMER);
        }

        [Given(@"Was refunded by giftcard for (.*)")]
        public void GivenWasRefundedByGiftcardFor(decimal amount)
        {
            Builder.AddPaymentToTransaction(amount, TenderType.GiftCard, Direction.TO_CUSTOMER);
        }

        [Given(@"Was refunded by Visa credit card for (.*) pounds")]
        public void GivenWasRefundedByVisaCreditCardForPounds(decimal amount)
        {
            Builder.AddPaymentToTransaction(amount, TenderType.CreditCard, Direction.TO_CUSTOMER);
        }
    }
}
