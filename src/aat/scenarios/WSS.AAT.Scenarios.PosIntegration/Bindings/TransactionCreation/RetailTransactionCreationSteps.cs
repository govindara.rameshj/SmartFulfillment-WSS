using System;
using Cts.Oasys.Core.Helpers;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Enums;
using PriceOverrideReasonCode = WSS.BO.Model.Enums.PriceOverrideReasonCode;
using EventType = WSS.BO.Model.Enums.EventType;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    sealed class RetailTransactionCreationSteps : TransactionStepDefinitions
    {
        public RetailTransactionCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
        }

        private RetailTransactionBuilder Builder
        {
            get { return (RetailTransactionBuilder)Ctx.TransactionBuilder; }
        }

        #region Transaction init

        [Given(@"Sale happened")]
        [When(@"Another sale happened")]
        [Given(@"Sale happened today")]
        public void GivenSaleHappened()
        {
            Ctx.StartNewTransaction(new RetailTransactionBuilder(DataLayerFactory, false, DateTime.Now));
        }

        [Given(@"Sale happened yesterday")]
        public void GivenTransactionHappenedYesterday()
        {
            Ctx.StartNewTransaction(new RetailTransactionBuilder(DataLayerFactory, false, DateTime.Now.AddDays(-1)));
        }

        [Given(@"Refund happened")]
        [When(@"Refund happened")]
        public void GivenRefundHappened()
        {
            Ctx.StartNewTransaction(new RetailTransactionBuilder(DataLayerFactory, true, DateTime.Now));
        }

        #endregion

        #region Transaction header

        [Given(@"Source was (.*)")]
        public void GivenSourceWas(string source)
        {
            Builder.SetSource(source);
        }

        [Given(@"Source transaction ID was '(.*)'")]
        public void GivenSourceTransactionIdDWas(string sourceTransactionId)
        {
            Builder.SetSourceTransactionId(sourceTransactionId);
        }

        [Given(@"Source transaction Receipt Barcode was '(.*)'")]
        public void GivenSourceTransactionReceiptBarcodeWas(string receiptBarcode)
        {
            Builder.SetReceiptBarcode(receiptBarcode);
        }

        [Given(@"Sale transaction number was (.*)")]
        public void GivenTransactionNumberWas(string transactionNumber)
        {
            Builder.SetTransactionNumber(transactionNumber);
        }

        [Given(@"Till number was (.*)")]
        [When(@"Till number was (.*)")]
        public void GivenTillNumberWas(string tillNumber)
        {
            Builder.SetTillNumber(tillNumber);
        }

        [Given(@"Cashier was (.*)")]
        public void GivenCashierWas(string cashierNumber)
        {
            Builder.SetCashierNumber(cashierNumber);
        }

        [Given(@"Customer name is (.*)")]
        public void GivenCustomerNameIs(string customerName)
        {
            Builder.AddCustomer(customerName);
        }

        [Given(@"Customer provides destination details that are different from his own address")]
        public void GivenCustomerProvidesDestinationDetailsThatAreDifferentFromHisOwnAddress()
        {
            Builder.AddContactForOrder();
        }

        [Given(@"Transaction was voided")]
        public void GivenTransactionWasVoided()
        {
            Builder.SetTransactionStatus(TransactionStatus.VOIDED);
        }

        [Given(@"Transaction was parked")]
        public void GivenTransactionWasParked()
        {
            Builder.SetTransactionStatus(TransactionStatus.PARKED);
        }

        [Given(@"Transaction was training")]
        public void GivenTransactionWasTraining()
        {
            Builder.SetTransactionIsTraining();
        }

        [Given(@"Collegue Discount amount for SKUs (.*) is (.*)")]
        public void GivenEmployeeDiscountAmountForSKUsIs(string skuNumber, decimal discountAmount)
        {
            Builder.SetEmployeeDiscount(skuNumber, discountAmount);
        }

        [Given(@"Employee '(.*)' discount amount for SKU (.*) was (.*)")]
        public void GivenEmployeeDiscountAmountForSKUWas(string supervisorNumber, string skuNumber, decimal discountAmount)
        {
            Builder.SetEmployeeDiscount(skuNumber, discountAmount, supervisorNumber);
        }

        #endregion

        #region Items

        [Given(@"(.*) SKU (.*) sold")]
        [When(@"(.*) SKU (.*) sold")]
        public void GivenSkuSold(int quantity, string skuNumber)
        {
            Builder.AddTransactionItem(quantity, skuNumber, Direction.TO_CUSTOMER);
        }

        [Given(@"(.*) SKU (.*) sold with Source Item Id = '(.*)'")]
        public void GivenSKUSoldWithSourceItemId(int quantity, string skuNumber, string sourceItemId)
        {
            Builder.AddTransactionItem(quantity, skuNumber, sourceItemId, Direction.TO_CUSTOMER);
        }

        [Given(@"Price of SKU (.*) was (.*)")]
        public void GivenPriceOfSKUWas(string skuNumber, decimal price)
        {
            Builder.SetItemPrice(skuNumber, price);
        }

        [Given(@"In db price of SKU (.*) was (.*)")]
        public void GivenInDbPriceOfSKUWas(string skuNumber, decimal price)
        {
            DataLayerFactory.Create<TestStockRepository>().UpdateStockMaster(skuNumber, price);
        }

        [Given(@"(.*) SKU (.*) sold with line number (.*) and source item id equal (.*)")]
        public void GivenSKUSoldWithLineNumberAndSourceItemIdEqual(int quantity, string skuNumber, short sourceLineNumber, string sourceItemId)
        {
            Builder.AddTransactionItem(quantity, skuNumber, sourceLineNumber, sourceItemId, Direction.TO_CUSTOMER);
        }

        [Given(@"(.*) SKU (.*) sold with line number (.*)")]
        public void GivenSKUSoldWithLineNumber(int quantity, string skuNumber, short sourceLineNumber)
        {
            Builder.AddTransactionItem(quantity, skuNumber, sourceLineNumber, Direction.TO_CUSTOMER);
        }

        [When(@"(.*) SKU (.*) from the previous transaction were refunded")]
        [Given(@"(.*) SKU (.*) from the previous transaction were refunded")]
        public void GivenSkuFromThePreviousTransactionWereRefunded(int quantity, string skuNumber)
        {
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, Ctx.SentTransaction.SourceTransactionId, "Customer Changed Mind");
        }

        [Given(@"(.*) SKU (.*) from the previous transaction were refunded with Refund Transaction Item Id = '(.*)'")]
        [When(@"(.*) SKU (.*) from the previous transaction were refunded with Refund Transaction Item Id = '(.*)'")]
        public void GivenSKUFromThePreviousTransactionWereRefundedWithRefundTransactionItemId(int quantity, string skuNumber, string refundedTransactionItemId)
        {
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, Ctx.FirstTransaction.SourceTransactionId, "Customer Changed Mind", refundedTransactionItemId);
        }

        [Given(@"(.*) SKU (.*) from the previous transaction were refunded with Refund Transaction Item Id = '(.*)' and Price (.*) pounds")]
        public void GivenSKUFromThePreviousTransactionWereRefundedWithRefundTransactionItemIdAndPricePounds(int quantity, string skuNumber, string refundedTransactionItemId, decimal price)
        {
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, Ctx.FirstTransaction.SourceTransactionId, "Customer Changed Mind", refundedTransactionItemId);
            Builder.SetItemPrice(skuNumber, price);
        }

        [Given(@"(.*) SKU (.*) from the first transaction were refunded")]
        public void GivenSkuFromTheFirstTransactionWereRefunded(int quantity, string skuNumber)
        {
            GivenSkuFromTheFirstTransactionWereRefundedWithReason(quantity, skuNumber, "Customer Changed Mind");
        }

        [Given(@"(.*) SKU (.*) from the first transaction were refunded with Refund Transaction Item Id = '(.*)'")]
        public void GivenSKUFromTheFirstTransactionWereRefundedWithRefundTransactionItemId(int quantity, string skuNumber, string refundedTransactionItemId)
        {
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, Ctx.FirstTransaction.SourceTransactionId, "Customer Changed Mind", refundedTransactionItemId);
        }

        [Given(@"(.*) SKU (.*) from the first legacy transaction were refunded")]
        public void GivenSkuFromTheFirstLegacyTransactionWereRefunded(int quantity, string skuNumber)
        {
            var key = Ctx.FirstTransactionKey;
            var storeNumber = DataLayerFactory.Create<DictionariesRepository>().GetStoreId();
            var legacySourceTransactionId = LegacyReceiptBarcodeHelper.GenerateLegacyTranId(storeNumber, key.TransactionNumber, key.CreateDate, key.TillNumber);
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, legacySourceTransactionId, "Customer Changed Mind");
        }

        [Given(@"(.*) SKU (.*) from the first legacy transaction were refunded with Refunded Transaction Item Id = '(.*)'")]
        public void GivenSKUFromTheFirstLegacyTransactionWereRefundedWithRefundedTransactionItemId(int quantity, string skuNumber, string refundedTransactionItemId)
        {
            var key = Ctx.FirstTransactionKey;
            var storeNumber = DataLayerFactory.Create<DictionariesRepository>().GetStoreId();
            var legacySourceTransactionId = LegacyReceiptBarcodeHelper.GenerateLegacyTranId(storeNumber, key.TransactionNumber, key.CreateDate, key.TillNumber);
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, legacySourceTransactionId, "Customer Changed Mind", refundedTransactionItemId);
        }

        [Given(@"(.*) SKU (.*) from the second transaction were refunded")]
        public void GivenSkuFromTheSecondTransactionWereRefunded(int quantity, string skuNumber)
        {
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, Ctx.SentTransaction.SourceTransactionId, "Customer Changed Mind");
        }

        [Given(@"(.*) SKU (.*) from the second transaction were refunded with Refund Transaction Item Id = '(.*)'")]
        public void GivenSKUFromTheSecondTransactionWereRefundedWithRefundTransactionItemId(int quantity, string skuNumber, string refundedTransactionItemId)
        {
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, Ctx.SentTransaction.SourceTransactionId, "Customer Changed Mind", refundedTransactionItemId);
        }

        [Given(@"(.*) SKU (.*) from the first transaction were refunded with reason '(.*)'")]
        public void GivenSkuFromTheFirstTransactionWereRefundedWithReason(int quantity, string skuNumber, string reason)
        {
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, Ctx.FirstTransaction.SourceTransactionId, reason);
        }

        [Given(@"Line for SKU (\d*) is reversed")]
        public void GivenLineForSkuIsReversed(string skuNumber)
        {
            Builder.ReverseLine(skuNumber);
        }

        [Given(@"A gift card was topped up with (.*) pounds")]
        public void GivenAgiftCardWasToppedUpWithXPounds(decimal amount)
        {
            Builder.AddGiftCardTransactionItem(amount);
        }

        [Given(@"The gift card was reversed")]
        public void GivenTheGiftCardWasReversed()
        {
            Builder.ReverseGiftCardLine();
        }

        [Given(@"(.*) SKU (\d*) were refunded")]
        [When(@"(.*) SKU (\d*) were refunded")]
        public void WhenSkuWereRefunded(int quantity, string skuNumber)
        {
            GivenSkuWereRefundedWithReason(quantity, skuNumber, "Customer Changed Mind");
        }

        [Given(@"(.*) SKU (\d*) were refunded with reason '(.*)'")]
        public void GivenSkuWereRefundedWithReason(int quantity, string skuNumber, string reason)
        {
            Builder.AddRefundItemWithReasonCode(quantity, skuNumber, null, reason);
        }

        [Given(@"Price for SKU (.*) was overriden by (.*) pounds with reason code Managers Discretion")]
        public void GivenPriceForSkuWasOverridenByPoundsWithReasonCodeDamagedGoods(string skuNumber, decimal newPrice)
        {
            Builder.AddPriceOverrideDiscountByNewPrice(skuNumber, newPrice, PriceOverrideReasonCode.MANAGERS_DISCRETION);
        }

        [Given(@"Price for SKU (.*) was overriden by (.*) pounds with reason code Managers Discretion by Supervisor '(.*)'")]
        public void GivenPriceForSKUWasOverridenByPoundsWithReasonCodeManagersDiscretionBySupervisor(string skuNumber, decimal newPrice, string supervisorNumber)
        {
            Builder.AddPriceOverrideDiscountByNewPrice(skuNumber, newPrice, PriceOverrideReasonCode.MANAGERS_DISCRETION, supervisorNumber);
        }

        [Given(@"Price for SKU (.*) was overriden by (.*) pounds")]
        public void GivenPriceForSkuWasOverridenByPounds(string skuNumber, decimal newPrice)
        {
            Builder.AddPriceOverrideDiscountByNewPrice(skuNumber, newPrice, PriceOverrideReasonCode.DAMAGED_GOODS);
        }

        [Given(@"Price Override with amount (.*) was applied to SKU (.*)")]
        public void GivenPriceOverrideWithAmountWasAppliedToSKU(decimal savingAmount, string skuNumber)
        {
            Builder.AddPriceOverrideDiscountBySavingAmount(skuNumber, savingAmount, PriceOverrideReasonCode.DAMAGED_GOODS);
        }

        [Given(@"Price Override with code Manager Discretion with Amount (.*) was applied to SKU (.*)")]
        public void GivenPriceOverrideWithCodeManagerDiscretionWithAmountWasAppliedToSKU(decimal savingAmount, string skuNumber)
        {
            Builder.AddPriceOverrideDiscountBySavingAmount(skuNumber, savingAmount, PriceOverrideReasonCode.MANAGERS_DISCRETION);
        }

        [Given(@"Price Override with code Damaged Goods with Amount (.*) was applied to SKU (.*)")]
        public void GivenPriceOverrideWithCodeDamagedGoodsWithAmountWasAppliedToSKU(decimal savingAmount, string skuNumber)
        {
            Builder.AddPriceOverrideDiscountBySavingAmount(skuNumber, savingAmount, PriceOverrideReasonCode.DAMAGED_GOODS);
        }

        [Given(@"Price Override with code HDC Return with Amount (.*) was applied to SKU (.*)")]
        public void GivenPriceOverrideWithCodeHDCReturnWithAmountWasAppliedToSKU(decimal savingAmount, string skuNumber)
        {
            Builder.AddPriceOverrideDiscountBySavingAmount(skuNumber, savingAmount, PriceOverrideReasonCode.HDC_RETURN);
        }

        [Given(@"SKU (.*) was marked as from Markdown stock")]
        public void GivenSKUWasMarkedAsFromMarkdownStock(string skuNumber)
        {
            Builder.SetStockType(skuNumber, true);
        }

        [Given(@"Price for SKU (.*) was overriden by (.*) pounds with reason code Damaged Goods")]
        public void GivenPriceForSkuWasOverridenByPoundsWithREasonCodeDamagedGoods(string skuNumber, decimal newPrice)
        {
            Builder.AddPriceOverrideDiscountByNewPrice(skuNumber, newPrice, PriceOverrideReasonCode.DAMAGED_GOODS);
        }

        [Given(@"Price for SKU (.*) was overriden by (.*) pounds with reason code Refund Price Override")]
        public void GivenPriceForSkuWasOverridenByPoundsWithReasonCodeRefundPriceOverride(string skuNumber, decimal newPrice)
        {
            Builder.AddPriceOverrideDiscountByNewPrice(skuNumber, newPrice, PriceOverrideReasonCode.REFUND_PRICE_DIFFERENCE);
        }

        [Given(@"Price for SKU (.*) was overriden by (.*) pounds with reason code Price Match or Promise and Competitor Name '(.*)'")]
        public void GivenPriceForSkuWasOverridenByPoundsWithReasonCodePriceMatchOrPromiseAndCompetitorName(string skuNumber, decimal newPrice, string competitorName)
        {
            Builder.AddPriceMatchDiscount(skuNumber, newPrice, competitorName);
        }

        [Given(@"Price for SKU (.*) was overriden by Supervisor '(.*)' by (.*) pounds with reason code Price Match or Promise and Competitor Name '(.*)'")]
        public void GivenPriceForSkuWasOverridenByPoundsWithReasonCodePriceMatchOrPromiseAndCompetitorNameBySupervisor(string skuNumber, string supervisorNumber, decimal newPrice, string competitorName)
        {
            Builder.AddPriceMatchDiscount(skuNumber, newPrice, competitorName, supervisorNumber);
        }

        [Given(@"Price for SKU (.*) was overriden by (.*) pounds by Supervisor '(.*)'")]
        public void GivenPriceForSKUWasOverridenByPoundsBySupervisor(string skuNumber, decimal newPrice, string supervisorNumber)
        {
            Builder.AddPriceOverrideDiscountByNewPrice(skuNumber, newPrice, PriceOverrideReasonCode.DAMAGED_GOODS, supervisorNumber);
        }

        [Given(@"(.*) SKU (.*) was sold with VAT rate (.*)")]
        public void GivenSkuWasSoldWithVatRate(int quantity, string skuNumber, decimal vatRate)
        {
            Builder.AddTransactionItem(quantity, skuNumber, Direction.TO_CUSTOMER, vatRate);
        }

        [Given(@"Data base was cleaned up")]
        public void GivenDataBaseWasCleanedUp()
        {
            var dailyRepo = DataLayerFactory.Create<TestDailyTillRepository>();
            var customerRepo = DataLayerFactory.Create<TestCustomerOrderRepository>();

            dailyRepo.RemoveDailies();
            customerRepo.RemoveOrdersFromTables();
        }

        [Given(@"First transaction was legacy transaction\(SourceTranId and Source fields are empty\)")]
        public void GivenFirstTransactionWasLegacyTransactionSourceTranIdAndSourceFieldsAreEmpty()
        {
            var repo = DataLayerFactory.Create<TestDailyTillRepository>();
            repo.ClearSourceTranId(Ctx.SentTransactionKeys[0]);
        }

        [Given(@"Second transaction was legacy transaction\(SourceTranId and Source fields are empty\)")]
        public void GivenSecondTransactionWasLegacyTransactionSourceTranIdAndSourceFieldsAreEmpty()
        {
            var repo = DataLayerFactory.Create<TestDailyTillRepository>();
            repo.ClearSourceTranId(Ctx.SentTransactionKeys[1]);
        }

        [Given(@"Original Line Number for second transaction is NULL")]
        public void GivenOriginalLineNumberForSecondTransactionIsNULL()
        {
            var repo = DataLayerFactory.Create<TestDailyTillRepository>();
            repo.ClearDailyCustomer(Ctx.SentTransactionKeys[1]);
        }

        [Given(@"Refunded Transaction Id of Refund Item of SKU (.*) is legacy")]
        public void GivenTransactionIdOfRefundItemOfSKUIsLegacy(string skuProduct)
        {
            var storeNumber = DataLayerFactory.Create<DictionariesRepository>().GetStoreId();
            var refundedTransactionKeys = Ctx.SentTransactionKeys[0];

            var legacyTransactionId = LegacyReceiptBarcodeHelper.GenerateLegacyTranId(storeNumber, refundedTransactionKeys.TransactionNumber, refundedTransactionKeys.CreateDate, refundedTransactionKeys.TillNumber);

            Builder.SetItemRefundedTransactionId(skuProduct, legacyTransactionId);
        }

        [Given(@"Survey Postcode is '(.*)'")]
        public void GivenSurveyPostcodeIs(string surveyPostcode)
        {
            Builder.SetSurveyPostcode(surveyPostcode);
        }

        [Given(@"Manager id was '(.*)'")]
        public void GivenManagerNumberWas(string managerId)
        {
            Builder.SetManagerId(managerId);
        }
        #endregion
    }
}