﻿using System;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    class SaleSteps : TransactionStepDefinitions
    {
        public SaleSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            _testRepo = DataLayerFactory.Create<BankingSuiteRepository>();
        }

        private readonly BankingSuiteRepository _testRepo;

        [Given(@"In safe there is only one line for three days earlier date")]
        public void GivenInSafeThereIsOnlyOneLineForThreeDaysEarlierDate()
        {
            _testRepo.ClearSafeTablesForTest();
        }

        [When(@"The previous parked transaction was recalled and completed")]
        public void WhenThePreviousParkeddTransactionWasRecalledAndCompleted()
        {
            var builder = new RetailTransactionBuilder(DataLayerFactory, false, DateTime.Now);
            builder.SetRecalledTransactionId(Ctx.SentTransaction.SourceTransactionId);
            Ctx.StartNewTransaction(builder);
        }
    }
}
