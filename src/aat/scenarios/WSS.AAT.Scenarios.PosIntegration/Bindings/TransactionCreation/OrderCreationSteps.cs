using System;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    sealed class OrderCreationSteps : TransactionStepDefinitions
    {
        private readonly TestCustomerOrderRepository orderRepo;
        private readonly IDailyTillRepository dailyTillRepo;
        private readonly IDictionariesRepository dictRepo;

        public OrderCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory) : base(ctx, dataLayerFactory)
        {
            orderRepo = DataLayerFactory.Create<TestCustomerOrderRepository>();
            dailyTillRepo = DataLayerFactory.Create<IDailyTillRepository>();
            dictRepo = DataLayerFactory.Create<IDictionariesRepository>();
        }

        private RetailTransactionBuilder Builder
        {
            get { return (RetailTransactionBuilder)Ctx.TransactionBuilder; }
        }

        [Given(@"(.*) items for line (.*) were collected instantly")]
        public void GivenSkuWereCollectedInstantly(int quantity, short lineNumber)
        {
            Builder.AddFulfillmentItem<InstantFulfillment>(quantity, lineNumber);
        }

        [Given(@"(.*) items for line (.*) were collected later with ordering")]
        public void GivenItemsForLineWereCollectedLaterWithOrdering(int quantity, short lineNumber)
        {
            Builder.AddFulfillmentItem<CollectionFulfillment>(quantity, lineNumber);
        }

        [Given(@"(.*) items for line (.*) were delivered")]
        public void GivenSkuWereDelivered(int quantity, short lineNumber)
        {
            Builder.AddFulfillmentItem<DeliveryFulfillment>(quantity, lineNumber);
        }

        [Given(@"Delivery Instruction was ""(.*)""")]
        public void GivenDeliveryInstruction(string instruction)
        {
            Builder.AddDeliveryInstruction(instruction);
        }

        [Given(@"Delivery Charge was (.*)")]
        public void GivenDeliveryChargeWas(decimal deliveryCharge)
        {
            Builder.AddDeliveryChargeTransactionItem(deliveryCharge);
        }

        [Given(@"New order number will be (.*)")]
        public void GivenNewOrderNumberWillBe(string orderNumber)
        {
            orderRepo.SetPreviousOrderNumber(orderNumber);
        }

        [Given(@"There were no records for order number (.*) before")]
        public void GivenThereWereNoRecordsForOrderNumberBefore(string orderNumber)
        {
            orderRepo.RemoveOrdersFromTables(orderNumber);
        }

        [Given(@"There were no orders in db")]
        public void GivenThereWereNoOrdersInDb()
        {
            orderRepo.RemoveOrdersFromTables();
        }

        [Given(@"Order Line number for SKU (.*) was changed from (.*) to (.*)")]
        public void GivenOrderLineNumberForSKUWasChangedFromTo(string skuNumber, string oldOrderLineNumber, string newOrderLineNumber)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();

            orderRepo.SetOrderLineNumber(orderNumber, oldOrderLineNumber, newOrderLineNumber);
        }

        [When(@"Previous order has been delivered")]
        public void WhenPreviousOrderHasBeenDelivered()
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            orderRepo.SetDeliveryStatusToDeliveredForOrder(orderNumber);
        }

        [Given(@"Customer Order Header table Quantity Taken was set to zero")]
        public void GivenCustomerOrderHeaderTableQuantityTakenWasSetToZero()
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            orderRepo.SetCustomerOrderQuantityTaken(orderNumber, 0);
        }

        [Given(@"(.*) SKU (.*) were refunded corresponding to line with source item id (.*) from last order")]
        [When(@"(.*) SKU (.*) were refunded corresponding to line with source item id (.*) from last order")]
        public void WhenSKUWereRefundedCorrespondingToLineWithSourceItemIdFromLastOrder(int quantity, string skuNumber, string refundedTransactionItemId)
        {
            Builder.AddRefundLineWithSourceInformation(quantity, skuNumber, refundedTransactionItemId,
                    Ctx.SentTransaction.SourceTransactionId);
        }

        [Then(@"There is an order in DB")]
        public void ThenThereIsAnOrderInDb()
        {
            var transaction = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(transaction);
            Assert.IsFalse(transaction.IsVoid);
            Assert.AreNotEqual(0, transaction.TaxAmount);
            Assert.AreNotEqual(Global.NullOrderNumber, transaction.OrderNumber);
        }

        [Then(@"There is collection order in DB")]
        public void ThenThereIsCollectionOrderInDb()
        {
            var fulfillment = ((RetailTransaction)Ctx.SentTransaction).Fulfillments.OfType<CollectionFulfillment>().FirstOrDefault();
            Assert.IsNotNull(fulfillment);
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);
            var corhdr4 = orderRepo.SelectCorhdr4(orderNumber);
            var corlin = orderRepo.SelectCorlin(orderNumber, "0001");
            Assert.IsNotNull(corhdr);
            Assert.IsFalse(corhdr.IsForDelivery);
            Assert.IsNotNull(corhdr4);
            Assert.IsNotNull(corlin);
        }

        [Then(@"There is no collection order in DB")]
        public void ThenThereIsNoCollectionOrderInDB()
        {
            var fulfillment = ((RetailTransaction)Ctx.SentTransaction).Fulfillments.OfType<CollectionFulfillment>().FirstOrDefault();
            Assert.IsNotNull(fulfillment);
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);
            var corhdr4 = orderRepo.SelectCorhdr4(orderNumber);
            var corlin = orderRepo.SelectCorlin(orderNumber, "0001");
            Assert.IsNull(corhdr);
            Assert.IsNull(corhdr4);
            Assert.IsNull(corlin);
        }

        [Then(@"There is delivery order in DB")]
        public void ThenThereIsDeliveryOrderInDb()
        {
            var fulfillment = ((RetailTransaction)Ctx.SentTransaction).Fulfillments.FirstOrDefault(x => x is DeliveryFulfillment);
            Assert.IsNotNull(fulfillment);
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);
            var corhdr4 = orderRepo.SelectCorhdr4(orderNumber);
            var corlin = orderRepo.SelectCorlin(orderNumber, "0001");
            Assert.IsNotNull(corhdr);
            Assert.IsTrue(corhdr.IsForDelivery);
            Assert.IsNotNull(corhdr4);
            Assert.IsNotNull(corlin);
        }

        [Then(@"There is no delivery order in DB")]
        public void ThenThereIsNoDeliveryOrderInDB()
        {
            var fulfillment = ((RetailTransaction)Ctx.SentTransaction).Fulfillments.FirstOrDefault(x => x is DeliveryFulfillment);
            Assert.IsNotNull(fulfillment);
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);
            var corhdr4 = orderRepo.SelectCorhdr4(orderNumber);
            var corlin = orderRepo.SelectCorlin(orderNumber, "0001");
            Assert.IsNull(corhdr);
            Assert.IsNull(corhdr4);
            Assert.IsNull(corlin);
        }

        [Then(@"Order Merchandise Value Store value is equal to Transaction Total amount")]
        public void ThenOrderMerchandiseValueStoreValueIsEqualToTransactionTotalAmount()
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);
            Assert.IsNotNull(corhdr);

            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);

            Assert.AreEqual(dltots.TotalSaleAmount, corhdr.MerchandiseValueStore);
        }

        [Then(@"Order Line Price is equal to Line Extension Value without promotions")]
        public void ThenOrderLinePriceIsEqualToLineExtensionValueWithoutPromotions()
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corlin = orderRepo.SelectCorlin(orderNumber, "0001");
            Assert.IsNotNull(corlin);

            var dlline = dailyTillRepo.SelectDailyTillLine(Ctx.SentTransactionKey, 1);
            Assert.IsNotNull(dlline);

            var extentionValueWithoutPromotions = Math.Round((dlline.Amount - dlline.QuantityBreakChangePriceDifference 
                - dlline.DealGroupChangePriceDifference - dlline.MultibuyChangePriceDifference - dlline.HierachyChangePriceDifference) / dlline.Quantity, 2);

            Assert.AreEqual(extentionValueWithoutPromotions, corlin.Price);
        }

        [Then(@"New order number is (.*) in all tables")]
        public void ThenNewOrderNumberIsInAllTables(string orderNumber)
        {
            var corhdr = orderRepo.SelectCorhdr(orderNumber);
            var corhdr4 = orderRepo.SelectCorhdr4(orderNumber);
            var corlin = orderRepo.SelectCorlin(orderNumber, "0001");
            Assert.IsNotNull(corhdr);
            Assert.IsNotNull(corhdr4);
            Assert.IsNotNull(corlin);

            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(orderNumber, dltots.OrderNumber);
        }

        [Then(@"Total units taken number is (.*) in order info")]
        public void ThenTotalUnitsTakenNumberIsInOrderInfo(decimal totalUnitsTaken)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);

            Assert.AreEqual(totalUnitsTaken, corhdr.TotalUnitsTakenNumber);
        }

        [Then(@"Quantity taken for SKU (.*) is (.*)")]
        public void ThenQuantityTakenForSkuIs(string skuNumber, decimal quantityTaken)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corlin = orderRepo.SelectCorlins(orderNumber).FirstOrDefault(line => line.SkuNumber == skuNumber);

            Assert.IsNotNull(corlin);
            Assert.AreEqual(quantityTaken, corlin.QuantityTaken);
        }

        [Then(@"Quantity to be delivered for SKU (.*) is (.*)")]
        public void ThenQuantityToBeDeliveredForSkuIs(string skuNumber, decimal quantityToBeDelivered)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corlin = orderRepo.SelectCorlins(orderNumber).FirstOrDefault(line => line.SkuNumber == skuNumber);

            Assert.IsNotNull(corlin);
            Assert.AreEqual(quantityToBeDelivered, corlin.QtyToBeDelivered);
        }

        [Then(@"Ordered Quantity for SKU (.*) is (.*)")]
        public void ThenOrderedQuantityForSKUIs(string skuNumber, decimal orderedQuantity)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corlin = orderRepo.SelectCorlins(orderNumber).FirstOrDefault(line => line.SkuNumber == skuNumber);

            Assert.IsNotNull(corlin);
            Assert.AreEqual(orderedQuantity, corlin.Quantity);
        }

        [Then(@"Quantity for Delivery Charge item with SKU '(.*)' is (.*)")]
        public void ThenQuantityForDeliveryChargeItemWithSkuIs(string skuNumber, int quantity)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corlin = orderRepo.SelectCorlins(orderNumber).FirstOrDefault(line => line.SkuNumber == skuNumber);

            Assert.IsNotNull(corlin);
            Assert.AreEqual(quantity, corlin.Quantity);
        }

        [Then(@"Delivery status for SKU (.*) is (.*)")]
        public void ThenDeliveryStatusForSkuIs(string skuNumber, int deliveryStatus)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corlin = orderRepo.SelectCorlins(orderNumber).FirstOrDefault(line => line.SkuNumber == skuNumber);
            
            Assert.IsNotNull(corlin);
            Assert.AreEqual(deliveryStatus, corlin.DeliveryStatus);
        }

        [Then(@"There is a Delivery Instruction ""(.*)""")]
        public void ThenThereIsADeliveryInstruction(string instruction)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var cortxt = orderRepo.SelectCortxts(orderNumber).FirstOrDefault();

            Assert.IsNotNull(cortxt);
            Assert.AreEqual(cortxt.Text, instruction);
        }

        [Then(@"The Order Deivery Charge amount is (.*)")]
        public void ThenTheOrderDeiveryChargeAmountIs(decimal deliveryCharge)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);

            Assert.AreEqual(deliveryCharge, corhdr.DeliveryCharge);
        }

        [Then(@"Destination address details for order are correctly stored in database")]
        public void ThenDestinationAddressDetailsForOrderAreCorrectlyStoredInDatabase()
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var destinationContact = ((RetailTransaction)Ctx.SentTransaction).Contacts[1];
            var corhdr = orderRepo.SelectCorhdr(orderNumber);
            var corhdr4 = orderRepo.SelectCorhdr4(orderNumber);

            Assert.IsNotNull(destinationContact.ContactAddress);
            Assert.AreEqual(destinationContact.ContactAddress.AddressLine1, corhdr.AddressLine1.Trim());
            Assert.AreEqual(destinationContact.ContactAddress.AddressLine2, corhdr.AddressLine2.Trim());
            Assert.AreEqual(destinationContact.ContactAddress.PostCode, corhdr.PostCode.Trim());
            Assert.AreEqual(destinationContact.ContactAddress.Town, corhdr.Town.Trim());
            Assert.AreEqual(destinationContact.MobilePhone, corhdr.MobilePhoneNumber.Trim());
            Assert.AreEqual(destinationContact.Name, corhdr.CustomerName.Trim());
            Assert.AreEqual(destinationContact.Phone, corhdr.PhoneNumber.Trim());

            Assert.AreEqual(destinationContact.WorkPhone, corhdr4.WorkPhone.Trim());
            Assert.AreEqual(destinationContact.Email, corhdr4.Email.Trim());
        }

        [Then(@"There is no order lines for SKU (.*)")]
        public void ThenThereIsNoOrderLinesForSKU(string skuNumber)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corlin = orderRepo.SelectCorlins(orderNumber).FirstOrDefault(line => line.SkuNumber == skuNumber);

            Assert.IsNull(corlin);
        }
    }
}
