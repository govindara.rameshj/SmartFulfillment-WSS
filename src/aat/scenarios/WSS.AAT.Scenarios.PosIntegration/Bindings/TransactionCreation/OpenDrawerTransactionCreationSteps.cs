﻿using System;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    class OpenDrawerTransactionCreationSteps : TransactionStepDefinitions
    {
        public OpenDrawerTransactionCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            dailyTillRepo = DataLayerFactory.Create<IDailyTillRepository>();
        }

        private readonly IDailyTillRepository dailyTillRepo;

        [Given(@"OpenDrawer event happened")]
        public void GivenOpenDrawerEventHappened()
        {
            Ctx.StartNewTransaction(new OpenDrawerTransactionBuilder(DataLayerFactory, DateTime.Now)); 
        }

        [Then(@"There is OpenDrawer transaction in BO db")]
        public void ThenThereIsOpenDrawerTransactionInBoDb()
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(TransactionCodes.OpenDrawer, dltots.TransactionCode);
        }

        [Then(@"Reason code for the transaction in ledger is (.*)")]
        public void ThenReasonCodeForTheTransactionInLedgerIs(int reasonCode)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);

            Assert.IsNotNull(dltots);
            Assert.AreEqual(reasonCode, dltots.OpenDrawerReasonCode);
        }

        [Then(@"Description for the transaction in ledger is '(.*)'")]
        public void ThenDescriptionForTheTransactionInLedgerIs(string description)
        {
            var dltots = dailyTillRepo.SelectDailyTillTran(Ctx.SentTransactionKey);

            Assert.IsNotNull(dltots);
            Assert.AreEqual(description, dltots.Description.Trim());
        }
    }
}
