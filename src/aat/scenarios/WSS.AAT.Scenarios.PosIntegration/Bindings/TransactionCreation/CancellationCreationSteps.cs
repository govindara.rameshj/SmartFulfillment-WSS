using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    sealed class CancellationCreationSteps : TransactionStepDefinitions
    {
        private readonly TestCustomerOrderRepository orderRepo;

        public CancellationCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            orderRepo = DataLayerFactory.Create<TestCustomerOrderRepository>();
        }

        private RetailTransactionBuilder Builder
        {
            get { return (RetailTransactionBuilder)Ctx.TransactionBuilder; }
        }

        [Given(@"(.*) SKU (.*) were refunded with cancelled quantity = (.*) corresponding to line with source item id (.*) from last order")]
        [When(@"(.*) SKU (.*) were refunded with cancelled quantity = (.*) corresponding to line with source item id (.*) from last order")]
        public void WhenSKUWereRefundedWithCancelledQuantityCorrespondingToLineWithSourceItemIdFromLastOrder(int quantity, string skuNumber, int cancelledQuantity, string refundedTransactionItemId)
        {
            Builder.AddRefundLineWithSourceInformationAndCancelledQuantity(
                quantity, skuNumber, cancelledQuantity, Ctx.SentTransaction.SourceTransactionId,
                 refundedTransactionItemId);
        }

        [When(@"(.*) SKU (.*) were refunded with cancelled quantity = (.*) with no source item id stated")]
        public void WhenSKUWereRefundedWithCancelledQuantityWithNoSourceItemIdStated(int quantity, string skuNumber, int cancelledQuantity)
        {
            Builder.AddRefundLineWithSourceInformationAndCancelledQuantity(
                quantity, skuNumber, cancelledQuantity, Ctx.SentTransaction.SourceTransactionId);
        }

        [Then(@"In previous order for line (.*) quantity taken = (.*); quantity to be delivered = (.*); refund quantity = (.*); total quantity = (.*)")]
        public void ThenQuantityTakenForLineInPreviousOrderIs(int sourceOrderLineNo, int quantityTaken, int qtyToBeDelivered, int refundQuantity, int totalQuantity)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();

            var corlin2 = orderRepo.SelectCorlins2(orderNumber).FirstOrDefault(line => line.SourceOrderLineNo == sourceOrderLineNo);
            Assert.IsNotNull(corlin2);

            var corlinLineNumber = corlin2.LineNumber;
            var corlin = orderRepo.SelectCorlin(orderNumber, corlinLineNumber);

            Assert.IsNotNull(corlin);
            Assert.AreEqual(quantityTaken, corlin.QuantityTaken);
            Assert.AreEqual(qtyToBeDelivered, corlin.QtyToBeDelivered);
            Assert.AreEqual(-refundQuantity, corlin.RefundQuantity);
            Assert.AreEqual(totalQuantity, corlin.Quantity);
        }

        [Then(@"In first order for line (.*) quantity taken = (.*); quantity to be delivered = (.*); refund quantity = (.*); total quantity = (.*)")]
        public void ThenInFirstOrderForLineQuantityTakenQuantityToBeDeliveredRefundQuantityTotalQuantity(int sourceOrderLineNo, int quantityTaken, int qtyToBeDelivered, int refundQuantity, int totalQuantity)
        {
            var orderNumber = orderRepo.GetOrderNumberForOrderCancelledInOrder();

            var corlin2 = orderRepo.SelectCorlins2(orderNumber).FirstOrDefault(line => line.SourceOrderLineNo == sourceOrderLineNo);
            Assert.IsNotNull(corlin2);

            var corlinLineNumber = corlin2.LineNumber;
            var corlin = orderRepo.SelectCorlin(orderNumber, corlinLineNumber);

            Assert.IsNotNull(corlin);
            Assert.AreEqual(quantityTaken, corlin.QuantityTaken);
            Assert.AreEqual(qtyToBeDelivered, corlin.QtyToBeDelivered);
            Assert.AreEqual(-refundQuantity, corlin.RefundQuantity);
            Assert.AreEqual(totalQuantity, corlin.Quantity);
        }

        [Then(@"In previous order total units quantity = (.*); total quantity taken = (.*); total refunded quantity = (.*)")]
        public void ThenTotalUnitsQuantityForPreviousOrderIs(int totalUnitsQuantity, int totalQuantityTaken, int totalRefundedQuantity)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();

            var corhdr = orderRepo.SelectCorhdr(orderNumber);

            Assert.IsNotNull(corhdr);
            Assert.AreEqual(totalUnitsQuantity, corhdr.TotalOrderUnitsNumber);
            Assert.AreEqual(totalQuantityTaken, corhdr.TotalUnitsTakenNumber);
            Assert.AreEqual(-totalRefundedQuantity, corhdr.TotalUnitsRefundedNumber);
        }

        [Then(@"Previous order has cancellation state (.*)")]
        public void ThenPreviousOrderHasCancellationState(string cancellationState)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var corhdr = orderRepo.SelectCorhdr(orderNumber);

            Assert.IsNotNull(corhdr);
            Assert.AreEqual(cancellationState, corhdr.CancellationState);
        }

        [Then(@"There is information about order cancellation for line (.*) in previous order with returned quantity (.*) and cancelled quantity (.*)")]
        public void ThenThereIsInformationAboutOrderCancellationForLineInPreviousOrderWithReturnedQuantityAndCancelledQuantity(int lineNumber, int returnedQuantity, int cancelledQuantity)
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var correfund = orderRepo.SelectCorrefunds(orderNumber, lineNumber.ToString("D4")).FirstOrDefault();

            Assert.IsNotNull(correfund);
            Assert.AreEqual(returnedQuantity, correfund.QtyReturned);
            Assert.AreEqual(cancelledQuantity, correfund.QtyCancelled);
        }

        [Then(@"There is no information about order cancellation in previous order")]
        public void ThenThereIsNoInformationAboutOrderCancellationInPreviousOrder()
        {
            var orderNumber = orderRepo.GetPreviousOrderNumber();
            var correfund = orderRepo.SelectCorrefunds(orderNumber).FirstOrDefault();

            Assert.IsNull(correfund);
        }
    }
}
