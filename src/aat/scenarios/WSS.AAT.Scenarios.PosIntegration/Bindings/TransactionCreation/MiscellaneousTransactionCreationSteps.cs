﻿using System;
using TechTalk.SpecFlow;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    sealed class MiscellaneousTransactionCreationSteps : TransactionStepDefinitions
    {
        public MiscellaneousTransactionCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
        }

        private MiscellaneousTransactionBuilder Builder
        {
            get { return (MiscellaneousTransactionBuilder)Ctx.TransactionBuilder; }
        }

        [Given(@"Paid Out transaction happened")]
        [Given(@"Paid Out Correction transaction happened")]
        public void GivenPaidOutTransactionHappened()
        {
            Ctx.StartNewTransaction(new PaidOutTransactionBuilder(DataLayerFactory, DateTime.Now));
        }

        [Given(@"The reason was Counsil Account")]
        public void GivenTheReasonWasCounsilAccount()
        {
            ((PaidOutTransactionBuilder)Builder).SetReasonCode(BO.Model.Enums.PaidOutReasonCode.COUNCIL_ACCOUNT);
        }

        [Given(@"Misc Income transaction happened")]
        [Given(@"Misc Income Correction transaction happened")]
        public void GivenMiscInTransactionHappened()
        {
            Ctx.StartNewTransaction(new MiscInTransactionBuilder(DataLayerFactory, DateTime.Now));
        }

        [Given(@"The reason was Safe Overs")]
        public void GivenTheReasonWasSafeOvers()
        {
            ((MiscInTransactionBuilder)Builder).SetReasonCode(BO.Model.Enums.MiscInReasonCode.SAFE_OVERS);
        }

        [Given(@"Entered invoice number was (.*)")]
        public void GivenEnteredInvoiceNumberWas(string documentNumber)
        {
            Builder.SetInvoiceNumber(documentNumber);
        }
    }
}
