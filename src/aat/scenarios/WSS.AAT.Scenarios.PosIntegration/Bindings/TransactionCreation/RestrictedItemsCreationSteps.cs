﻿using System;
using System.Collections.Generic;
using Cts.Oasys.Core.SystemEnvironment;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Configuration;
using WSS.AAT.Common.Utility.Executables;
using WSS.AAT.Common.Utility.FileParsers;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Enums;
using WSS.AAT.Common.PosIntegration.TransactionBuilders;


namespace WSS.AAT.Scenarios.PosIntegration.Bindings.TransactionCreation
{
    [Binding]
    class RestrictedItemsCreationSteps : TransactionStepDefinitions
    {
        public RestrictedItemsCreationSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
        }

        private RetailTransactionBuilder Builder
        {
            get { return (RetailTransactionBuilder) Ctx.TransactionBuilder; }
        }

        [Given(@"There was (.*) Restricted Item with SKU (.*) and Restriction Type '(.*)' and Authorizer Number '(.*)'")]
        public void GivenThereWasRestrictedItemWithSKUAndRestrictionTypeAndAuthorizerNumber(string restrictedItemAcceptedType, string skuNumber, string restrictionType, string authorizerNumber)
        {
            Builder.AddRestrictedItem(GetRestrictedAcceptedType(restrictedItemAcceptedType), skuNumber, GetRestrictionType(restrictionType), authorizerNumber);
        }

        [Given(@"There was not Supervisor authorized (.*) Restricted Item with SKU (.*) and Restriction Type '(.*)'")]
        public void GivenThereWasRestrictedItemWithSKUAndRestrictionType(string restrictedItemAcceptedType, string skuNumber, string restrictionType)
        {
            Builder.AddRestrictedItem(GetRestrictedAcceptedType(restrictedItemAcceptedType), skuNumber, GetRestrictionType(restrictionType));
        }


        [Given(@"Restricted Item with SKU (.*) had Confirmation Text '(.*)'")]
        public void GivenRestrictedItemWithSKUHadConfirmationText(string skuNumber, string confirmationText)
        {
            Builder.AddConfirmationTextToRestrictedItem(skuNumber, confirmationText);
        }

        private RestrictedItemAcceptType GetRestrictedAcceptedType(string restrictedItemAcceptedType)
        {
            switch (restrictedItemAcceptedType)
            {
                case "Accepted Completely":
                    return RestrictedItemAcceptType.ACCEPTED_COMPLETELY;
                case "Accepted with Confirmation":
                    return RestrictedItemAcceptType.ACCEPTED_WITH_CONFIRMATION;
                case "Rejected Completely":
                    return RestrictedItemAcceptType.REJECTED_COMPLETELY;
                case "Rejected not Confirmed":
                    return RestrictedItemAcceptType.REJECTED_NOT_CONFIRMED;
            }
            throw new ArgumentException("Invalid Restricted Item Accept Type name");
        }

        private RestrictionType GetRestrictionType(string restrictionType)
        {
            switch (restrictionType)
            {
                case "Age Check":
                    return RestrictionType.AGE_CHECK;
                case "Explosives Poisons":
                    return RestrictionType.EXPLOSIVES_POISONS;
                case "Quarantine":
                    return RestrictionType.QUARANTINE;
            }
            throw new ArgumentException("Invalid Restricted Item Accept Type name");
        }
    }
}
