﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class EoDLockSteps : TransactionStepDefinitions
    {
        #region Internal

        private readonly TestNitmasRepository _nitmasRepository;

        public EoDLockSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            _nitmasRepository = DataLayerFactory.Create<TestNitmasRepository>();
        }

        #endregion

#region Steps

        [Given(@"Nightly routine procedure is started")]
        public void GivenNightlyRoutineProcedureIsStarted()
        {
            _nitmasRepository.ClearNitlog();
            _nitmasRepository.AddTaskToRepo(NitmasTaskId.NitmasStart);
            Ctx.IsEoDLockOccured = true;
        }

        [Given(@"Nightly routine procedure was started and finished")]
        public void GivenNightlyRoutineProcedureWasStartedAndFinished()
        {
            _nitmasRepository.ClearNitlog();
            _nitmasRepository.AddTaskToRepo(NitmasTaskId.NitmasStart);
            _nitmasRepository.AddTaskToRepo(NitmasTaskId.NitmasEnd);
            Ctx.IsEoDLockOccured = true;
        }

        [Given(@"Nightly routine procedure is not started")]
        public void GivenNightlyRoutineProcedureIsNotStarted()
        {
            _nitmasRepository.ClearNitlog();
        }

        [Given(@"Nightly routine procedure was finished")]
        public void GivenNightlyRoutineProcedureWasFinished()
        {
            _nitmasRepository.AddTaskToRepo(NitmasTaskId.NitmasEnd);
            Ctx.IsEoDLockOccured = true;
        }

        [Then(@"EoD lock error happened")]
        public void ThenEoDLockErrorHappened()
        {
            Assert.IsNotNull(Ctx.EodLockException);
        }
#endregion
        
    }
}
