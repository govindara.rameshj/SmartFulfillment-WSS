﻿using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    class TransactionNumberSteps : TransactionStepDefinitions
    {
        private readonly TestDailyTillRepository repo;

        public TransactionNumberSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            repo = DataLayerFactory.Create<TestDailyTillRepository>();
        }

        #region Given

        [Given(@"Next transaction number for till (.*) is (.*)")]
        public void GivenLastTransactionNumberOfTheTillWas(string tillNumber, int transactionNumber)
        {
            repo.SetNextTransactionNumber(tillNumber, transactionNumber);
            repo.RemoveDailies(tillNumber);
        }

        #endregion ~Given

        #region Then

        [Then(@"The sale source transaction Id is '(.*)'")]
        public void ThenTheSaleSourceTransactionIdIs(string sourceTransactionId)
        {
            var dltots = repo.SelectDailyTotal(Ctx.SentTransaction.SourceTransactionId);
            Assert.IsNotNull(dltots);
        }

        [Then(@"The sale transaction number is (.*)")]
        public void ThenTheSaleTransactionNumberIs(string transactionNumber)
        {
            var dltots = repo.SelectDailyTotal(Ctx.SentTransaction.SourceTransactionId);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(transactionNumber, dltots.TransactionNumber);
        }

        [Then(@"The sale source is (.*)")]
        public void ThenTheSaleSourceIs(string source)
        {
            var dltots = repo.SelectDailyTotal(Ctx.SentTransaction.SourceTransactionId);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(source, dltots.Source);
        }

        [Then(@"The sale source transaction number is (.*)")]
        public void ThenTheSaleSourceTransactionNumberIs(string sourceTranNumber)
        {
            var dltots = repo.SelectDailyTotal(Ctx.SentTransaction.SourceTransactionId);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(sourceTranNumber, dltots.SourceTranNumber);
        }

        [Then(@"The Receipt Barcode number is '(.*)'")]
        public void ThenTheReceiptBarcodeNumberIs(string receiptBarcode)
        {
            var dltots = repo.SelectDailyTotal(Ctx.SentTransaction.SourceTransactionId);
            Assert.IsNotNull(dltots);
            Assert.AreEqual(receiptBarcode, dltots.ReceiptBarcode);
        }

        [Then(@"Next transaction number is (.*)")]
        public void ThenNextTransactionNumberIs(string transactionNumber)
        {
            var nextTranNumber = repo.GetNumberForNextTransaction(Ctx.SentTransaction.TillNumber);
            Assert.AreEqual(transactionNumber, nextTranNumber);
        }

        #endregion
    }
}
