﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class CashierBalanceChangingSteps : TransactionStepDefinitions
    {
        #region Then

        [Then(@"Till (.*) virtual cashier balance for Visa credit card changed by (.*)")]
        public void ThenCashierBalanceForVisaCreditCardChangedBy(int tillNumber, decimal amount)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);
            Assert.AreEqual(Ctx.CashierBalanceBefore.VisaCardTenderAmount + amount, cashBalRepo.GetCashierBalanceTenderAmount(key.PeriodId, key.CashierId, key.CurrencyId,TenderType.CreditCard));
            Assert.AreEqual(Ctx.CashierBalanceBefore.VisaCardTenderVarAmount + amount, cashBalRepo.GetCashBalCashierTenVarAmount(key.PeriodId, key.CashierId, key.CurrencyId, TenderType.CreditCard, key.PeriodId));
        }

        [Then(@"Balance on yesterday for till (.*) virtual cashier increased by (.*)")]
        public void ThenYesterdayBalanceForCashierIncreasedBy(int tillNumber, decimal balanceChange)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber, Ctx.TransactionSentDate);
            CheckCashierBalancesHaveChanged(key, balanceChange);
        }

        [Then(@"Balance for cashier (.*) was not changed")]
        public void ThenBalanceForCashierWasNotChanged(int cashierId)
        {
            var key = GetCashierBalanceKey(cashierId);
            CheckCashierBalancesHaveChanged(key, 0);
        }

        [Then(@"Till (.*) virtual cashier number of voids changed by (.*)")]
        public void ThenTillVirtualCashierNumberOfVoidsIs(int tillNumber, int numberOfVoids)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);

            var casBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.NumVoids + numberOfVoids, casBalCashier.NumVoids);
        }

        [Then(@"Till (.*) virtual cashier cash balance changed by (.*)")]
        public void ThenTillVIrtualCashierCashBalanceChangedBy(int tillNumber, decimal balance)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);
            var casBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.GrossSalesAmount + balance, casBalCashier.GrossSalesAmount);
            Assert.AreEqual(Ctx.CashierBalanceBefore.SalesAmount + balance, casBalCashier.SalesAmount);
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderAmount + balance, cashBalRepo.GetCashierBalanceTenderAmount(key.PeriodId, key.CashierId, key.CurrencyId, TenderType.Cash));
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderVarAmount + balance, cashBalRepo.GetCashBalCashierTenVarAmount(key.PeriodId, key.CashierId, key.CurrencyId, TenderType.Cash, key.PeriodId));
        }

        [Then(@"Amount of reversed lines for till virtual cashier increased by (.*)")]
        public void ThenAmountOfReversedLinesForTillVirtualCashierIncreasedBy(int quantity)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);
            var cashBal = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(quantity, cashBal.NumLinesReversed - Ctx.CashierBalanceBefore.NumLinesReversed);
        }

        [Then(@"Total till (.*) virtual cashier balance changed by (.*)")]
        public void ThenTotalTillVirtualCashierBalanceChangedBy(int tillNumber, decimal amount)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.GrossSalesAmount + amount, cashBalCashier.GrossSalesAmount);
        }

        [Then(@"Cashier (.*) balance total amount changed by (.*)")]
        public void ThenCashierBalanceTotalAmountChangedBy(int cashierId, decimal amount)
        {
            var key = GetCashierBalanceKey(cashierId);
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            var realAmount = cashBalCashier == null ? 0 : cashBalCashier.GrossSalesAmount;
            Assert.AreEqual(Ctx.CashierBalanceBefore.RealCashierBalance + amount, realAmount);
        }

        [Then(@"Till (.*) virtual cashier balance for cash changed by (.*)")]
        public void ThenCashierBalanceForCashChangedBy(int tillNumber, decimal amount)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderAmount + amount, cashBalRepo.GetCashierBalanceTenderAmount(key.PeriodId, key.CashierId, key.CurrencyId, TenderType.Cash));
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderVarAmount + amount, cashBalRepo.GetCashBalCashierTenVarAmount(key.PeriodId, key.CashierId, key.CurrencyId, TenderType.Cash, key.PeriodId));
        }

        [Then(@"Till (.*) virtual cashier refund balance changed by (.*)")]
        public void ThenTillVirtualCashierRefundBalanceChangedBy(int tillNumber, decimal amount)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.RefundAmount + amount, cashBalCashier.RefundAmount);
        }

        [Then(@"Till (.*) virtual cashier sale balance changed by (.*)")]
        public void ThenTillVirtualCashierSaleBalanceChangedBy(int tillNumber, decimal amount)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.SalesAmount + amount, cashBalCashier.SalesAmount);
        }

        [Then(@"Till (.*) virtual cashier discount balance changed by (.*)")]
        public void ThenTillVirtualCashierDiscountBalanceChangedBy(int tillNumber, decimal amount)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.DiscountAmount - amount, cashBalCashier.DiscountAmount);
        }

        [Then(@"Till (.*) virtual cashier refund count changed by (.*)")]
        public void ThenTillVirtualCashierRefundCountChangedBy(int tillNumber, int refundCount)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);

            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.RefundCount + refundCount, cashBalCashier.RefundCount);
        }

        [Then(@"Till (.*) virtual cashier Misc Income count changed by (.*) and value changed by (.*) for reason code (.*)")]
        public void ThenTillVirtualCashierMiscIncomeCountChangedByAndValueChangedByForReasonCode(int tillNumber, int miscInCount, decimal balance, int reasonCode)
        {
            var key = GetVirtualCashierBalanceKey(tillNumber);

            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscIncomeCount[reasonCode] + miscInCount, cashBalCashier.GetMiscIncomeCount(reasonCode));
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscIncomeValue[reasonCode] + balance, cashBalCashier.GetMiscIncomeValue(reasonCode));
        }

        [Then(@"Amount of OpenDrawer transactions for till virtual cashier changed by (.*)")]
        public void ThenAmountOfOpenDrawerTransactionForTillVirtualCashierChangedBy(int amount)
        {
            var cashierBalanceKey = GetVirtualCashierBalanceKey(Int32.Parse(Ctx.SentTransaction.TillNumber));
            var casBalCashier = cashBalRepo.GetCashierBalance(cashierBalanceKey.PeriodId, cashierBalanceKey.CashierId, cashierBalanceKey.CurrencyId);

            Assert.AreEqual(Ctx.CashierBalanceBefore.NumOpenDrawer + amount, casBalCashier.NumOpenDrawer);
        }

        [Then(@"Misc Income count for the till virtual cashier increased by one and value increased by (.*) for reason code (.*)")]
        public void ThenMiscIncomeCountIncreasedByOneAndValueIncreasedByForReasonCode(decimal balanceChange, int reasonCode)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);

            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscIncomeCount[reasonCode] + 1, cashBalCashier.GetMiscIncomeCount(reasonCode));
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscIncomeValue[reasonCode] + balanceChange, cashBalCashier.GetMiscIncomeValue(reasonCode));
        }

        [Then(@"Misc Income count for the till virtual cashier decreased by one and value decreased by (.*) for reason code (.*)")]
        public void ThenMiscIncomeCountForTheTillVirtualCashierDecreasedByOneAndValueDecreasedByForReasonCode(decimal balanceChange, int reasonCode)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);

            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscIncomeCount[reasonCode] - 1, cashBalCashier.GetMiscIncomeCount(reasonCode));
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscIncomeValue[reasonCode] - balanceChange, cashBalCashier.GetMiscIncomeValue(reasonCode));
        }

        [Then(@"Misc Out count for the till virtual cashier increased by one and value decreased by (.*) for reason code (.*)")]
        public void ThenMiscIncomeCountIncreasedByOneAndValueDecreasedByForReasonCode(decimal balanceChange, int reasonCode)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);

            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscOutCount[reasonCode] + 1, cashBalCashier.GetMiscOutCount(reasonCode));
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscOutValue[reasonCode] - balanceChange, cashBalCashier.GetMiscOutValue(reasonCode));
        }

        [Then(@"Misc Out count for the till virtual cashier decreased by one and value increased by (.*) for reason code (.*)")]
        public void ThenMiscOutCountForTheTillVirtualCashierDecreasedByOneAndValueIncreasedByForReasonCode(decimal balanceChange, int reasonCode)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);

            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscOutCount[reasonCode] - 1, cashBalCashier.GetMiscOutCount(reasonCode));
            Assert.AreEqual(Ctx.CashierBalanceBefore.MiscOutValue[reasonCode] + balanceChange, cashBalCashier.GetMiscOutValue(reasonCode));
        }

        [Then(@"Till virtual cashier balance decreased by (.*)")]
        public void ThenTillVirtualCashierBalanceDecreasedBy(decimal balanceChange)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);
            CheckCashBalanceAmountChanges(key, -balanceChange);
        }

        [Then(@"Number Of Transactions increased by one")]
        public void ThenNumberOfTransactionsIncreasedByOne()
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);
            CheckCashBalanceNumberOfTransactionChanges(key);
        }

        [Then(@"Number of Corrections increased by one")]
        public void ThenNumberOfCorrectionsIncreasedByOne()
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);
            CheckCashBalanceNumberOfCorrectionChanges(key);
        }

        [Then(@"Till virtual cashier balance increased by (.*)")]
        public void ThenTillVirtualCashierBalanceIncreasedBy(decimal balanceChange)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);
            CheckCashBalanceAmountChanges(key, balanceChange);
        }

        [Then(@"Till virtual cashier balance tenders is not changed")]
        public void ThenTillVirtualCashierBalanceTendersIsNotChanged()
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction);
            AssertCashierBalanceTenderChange(key, TenderType.Cash, 0);
        }

        [Then(@"Till virtual cashier balance tenders increased by (.*)")]
        public void ThenTillViIrtualCashierBalanceTendersIncreased(decimal balanceIncrease)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction); 
            AssertCashierBalanceTenderChange(key, TenderType.Cash, balanceIncrease);
        }

        [Then(@"Till virtual cashier balance tenders decreased by (.*)")]
        public void ThenTillVirtualCashierBalanceTendersDecreasedBy(decimal balanceDecrease)
        {
            var key = GetVirtualCashierBalanceKey(Ctx.SentTransaction); 
            AssertCashierBalanceTenderChange(key, TenderType.Cash, -balanceDecrease);
        }

        [Then(@"CashBalCashier dates are equal today")]
        public void ThenCashBalCashierDatesAreEqualToday()
        {
            CheckCashBalCashierDatesAndAmounts(Ctx.TransactionSentDate, ((PayableTransaction)Ctx.SentTransaction).TotalAmount);
        }

        [Then(@"CashBalCashier dates are equal tomorrow")]
        public void ThenCashBalCashierDatesAreEqualTomorrow()
        {
            CheckCashBalCashierDatesAndAmounts(Ctx.TransactionSentDate.AddDays(1), ((PayableTransaction)Ctx.SentTransaction).TotalAmount);
        }

        private void CheckCashBalCashierDatesAndAmounts(DateTime expectedDateTime, decimal transactionTotalAmount, int tenderType = 1)
        {
            var key = GetVirtualCashierBalanceKey(Int32.Parse(Ctx.SentTransaction.TillNumber), expectedDateTime);

            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.IsNotNull(cashBalCashier);
            Assert.AreEqual(Ctx.CashierBalanceBefore.SalesAmount + transactionTotalAmount, cashBalCashier.SalesAmount);

            var cashBalCashierTen = cashBalRepo.GetCashierBalanceTender(tenderType, key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.IsNotNull(cashBalCashierTen);
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderAmount + transactionTotalAmount, cashBalCashierTen.Amount);

            var testRepo = DataLayerFactory.Create<TestMiscRepository>();
            var periodId = testRepo.GetPeriodId(Ctx.SentTransaction.CreateDateTime);
            var tradingPeriodId = key.PeriodId;

            var cashBalCashierTenVar = cashBalRepo.GetCashierBalanceTenderVariance(tenderType, periodId, key.CashierId, key.CurrencyId,
                tradingPeriodId);
            Assert.IsNotNull(cashBalCashierTenVar);
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderVarAmount + transactionTotalAmount, cashBalCashierTenVar.Amount);
        }
        #endregion ~Then

        #region Internal

        private readonly IDictionariesRepository dictRepo;
        private readonly TestCashierBalanceRepository cashBalRepo;

        public CashierBalanceChangingSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory)
            : base(ctx, dataLayerFactory)
        {
            dictRepo = DataLayerFactory.Create<IDictionariesRepository>();
            cashBalRepo = DataLayerFactory.Create<TestCashierBalanceRepository>();
        }

        private CashierBalanceKey GetCashierBalanceKey(int cashierId, DateTime? periodDate = null)
        {
            DateTime periodToGet;
            if (periodDate != null)
            {
                periodToGet = periodDate.Value;
            }
            else
            {
                if (Ctx.SentTransaction != null)
                {
                    periodToGet = Ctx.SentTransaction.CreateDateTime;
                }
                else
                {
                    periodToGet = Ctx.TransactionBuilder.CreationDateTime;
                }
            }

            return new CashierBalanceKey
            {
                CurrencyId = dictRepo.GetDefaultSystemCurrency(),
                PeriodId = DataLayerFactory.Create<TestMiscRepository>().GetPeriodId(periodToGet),
                CashierId = cashierId
            };
        }

        private CashierBalanceKey GetVirtualCashierBalanceKey(Transaction sentTransaction)
        {
            return GetVirtualCashierBalanceKey(Int32.Parse(sentTransaction.TillNumber));
        }

        private CashierBalanceKey GetVirtualCashierBalanceKey(int tillNumber, DateTime? periodDate = null)
        {
            var cashierId = dictRepo.GetVirtualCashierId(tillNumber);
            Assert.IsNotNull(cashierId);
            return GetCashierBalanceKey(cashierId.Value, periodDate);
        }

        private void CheckCashierBalancesHaveChanged(CashierBalanceKey key, decimal balanceChange)
        {
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.GrossSalesAmount + balanceChange, cashBalCashier.GrossSalesAmount);
            Assert.AreEqual(Ctx.CashierBalanceBefore.SalesAmount + balanceChange, cashBalCashier.SalesAmount);

            var cashBalCashierTen = cashBalRepo.GetCashierBalanceTender(TenderType.Cash, key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderAmount + balanceChange, cashBalCashierTen.Amount);

            var createDatePeriodId = key.PeriodId;
            if (Ctx.SentTransaction.CreateDateTime != Ctx.TransactionSentDate)
            {
                var testRepo = DataLayerFactory.Create<TestMiscRepository>();
                createDatePeriodId = testRepo.GetPeriodId(Ctx.SentTransaction.CreateDateTime);
            }
            var cashBalCashierTenVar = cashBalRepo.GetCashierBalanceTenderVariance(TenderType.Cash, createDatePeriodId, key.CashierId, key.CurrencyId, key.PeriodId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderVarAmount + balanceChange, cashBalCashierTenVar.Amount);
        }

        private void AssertCashierBalanceTenderChange(CashierBalanceKey key, int tenderType, decimal change)
        {
            var cashBalCashierTen = cashBalRepo.GetCashierBalanceTender(tenderType, key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderAmount + change, cashBalCashierTen.Amount);

            var cashBalCashierTenVar = cashBalRepo.GetCashierBalanceTenderVariance(tenderType, key.PeriodId, key.CashierId, key.CurrencyId, key.PeriodId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.CashTenderVarAmount + change, cashBalCashierTenVar.Amount);
        }

        private void CheckCashBalanceAmountChanges(CashierBalanceKey key, decimal balanceChange)
        {
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.GrossSalesAmount + balanceChange, cashBalCashier.GrossSalesAmount);
            Assert.AreEqual(Ctx.CashierBalanceBefore.SalesAmount, cashBalCashier.SalesAmount);
        }

        private void CheckCashBalanceNumberOfCorrectionChanges(CashierBalanceKey key)
        {
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.NumCorrections + 1, cashBalCashier.NumCorrections);
        }

        private void CheckCashBalanceNumberOfTransactionChanges(CashierBalanceKey key)
        {
            var cashBalCashier = cashBalRepo.GetCashierBalance(key.PeriodId, key.CashierId, key.CurrencyId);
            Assert.AreEqual(Ctx.CashierBalanceBefore.TransactionsCount + 1, cashBalCashier.NumTransactions);
        }

        #endregion
    }
}
