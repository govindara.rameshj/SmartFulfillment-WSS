using System;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Scenarios.PosIntegration.ScenarioContext;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.Exceptions;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.LegacyEmulation;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Discounts;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using EventType = WSS.BO.Model.Enums.EventType;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    public class ReverseMappingSteps : TransactionStepDefinitions
    {
        private readonly IBusinessLogicFacade _businessLogicFacade;
        private TransactionNotFoundException _thrownExpection;

        private RetailTransaction _recoveredTransaction;
        protected ReverseMappingSteps(TransactionScenarioContext ctx, IDataLayerFactory dataLayerFactory, IBusinessLogicFacade businessLogicFacade) : base(ctx, dataLayerFactory)
        {
            _businessLogicFacade = businessLogicFacade;
        }

        [Given(@"Database doesn't contain any transactions")]
        public void GivenDatabaseDoesnTContainAnyTransactions()
        {
            var repo = DataLayerFactory.Create<TestDailyTillRepository>();
            repo.RemoveDailies();
        }

        [Given(@"VatRate1 for previous transaction was equal (.*)")]
        public void GivenVatRateForPreviousTransactionWasEqual(decimal vatRate)
        {
            var repo = DataLayerFactory.Create<TestDailyTillRepository>();
            repo.UpdateVatRate1ForDltots(Ctx.SentTransactionKey, vatRate);
        }

        [Given(@"There is master event for code '(.*)', type '(.*)' and coupon '(.*)' in database")]
        public void GivenThereIsMasterEventForCodeAndCouponInDatabase(string eventCode, string strEventType, string couponNumber)
        {
            var repo = DataLayerFactory.Create<TestDailyTillRepository>();
            var eventType = GetEventType(strEventType);
            repo.CreateEventMaster(eventCode, ModelMapper.GetDbModelCode(eventType), couponNumber);
        }

        [Given(@"In db Price Override Code for SKU (.*) was changed to Incorrect Day Price")]
        public void GivenInDbPriceOverrideCodeForSKUWasChangedToIncorrectDayPrice(string skuNumber)
        {
            var repo = DataLayerFactory.Create<TestDailyTillRepository>();
            repo.UpdateDailyLine(Ctx.SentTransactionKey, skuNumber, PriceOverrideReasonCode.IncorrectDayPrice);
        }

        [Given(@"In db Refund Reason Code was changed to Wrong Item")]
        public void GivenInDbRefundReasonCodeForSKUWasChangedToWrongItem()
        {
            var repo = DataLayerFactory.Create<TestDailyTillRepository>();
            repo.UpdateDailyCustomer(Ctx.SentTransactionKey, RefundReasonCode.WrongItem);
        }

        [When(@"Reverse mapping for last transaction was requested")]
        public void WhenReverseMappingForLastTransactionWasRequested()
        {
            try
            {
                var storeNumber = DataLayerFactory.Create<DictionariesRepository>().GetStoreId();

                _recoveredTransaction = _businessLogicFacade.FindTransaction(
                        LegacyReceiptBarcodeHelper.GenerateReceiptBarcode(storeNumber, Ctx.SentTransactionKey.TransactionNumber, Ctx.SentTransactionKey.CreateDate, Ctx.SentTransactionKey.TillNumber),
                        storeNumber);
            }
            catch (TransactionNotFoundException ex)
            {
                _thrownExpection = ex;
            }
        }

        [When(@"Reverse mapping for transaction with TranId='(.*)', TillId='(.*)', Date='(.*)' was requested")]
        public void WhenReverseMappingForTransactionWithTranIdTillIdDateWasRequested(string tranId, string tillId, DateTime createDate)
        {
            var storeNumber = DataLayerFactory.Create<DictionariesRepository>().GetStoreId();

            try
            {
                _recoveredTransaction = _businessLogicFacade.FindTransaction(
                    LegacyReceiptBarcodeHelper.GenerateReceiptBarcode(storeNumber, tranId, createDate, tillId),
                    storeNumber);
            }
            catch (TransactionNotFoundException ex)
            {
                _thrownExpection = ex;
            }
        }

        [When(@"Reverse mapping for first transaction was requested")]
        public void WhenReverseMappingForFirstTransactionWasRequested()
        {
            var storeNumber = DataLayerFactory.Create<DictionariesRepository>().GetStoreId();

            _recoveredTransaction = _businessLogicFacade.FindTransaction(
                    LegacyReceiptBarcodeHelper.GenerateReceiptBarcode(storeNumber, Ctx.FirstTransactionKey.TransactionNumber, Ctx.FirstTransactionKey.CreateDate, Ctx.FirstTransactionKey.TillNumber),
                    storeNumber);
        }

        [Then(@"Recovered sale transaction has related refund transaction in it")]
        public void ThenRecoveredSaleTransactionHasRelatedRefundTransactionInIt()
        {
            var saleTran = (RetailTransaction) Ctx.FirstTransaction;
            var refundTran = (RetailTransaction) Ctx.SentTransaction;
            Assert.IsNotNull(_recoveredTransaction.RelatedTransactions);
            Assert.AreEqual(1, _recoveredTransaction.RelatedTransactions.Count);

            //Check original sale transaction
            Assert.AreEqual(saleTran.CreateDateTime.Date, _recoveredTransaction.CreateDateTime.Date);
            Assert.AreEqual(saleTran.TillNumber, _recoveredTransaction.TillNumber);
            Assert.AreEqual(saleTran.TransactionNumber, _recoveredTransaction.TransactionNumber);
            Assert.AreEqual(saleTran.ReceiptBarcode, _recoveredTransaction.ReceiptBarcode);

            //Check related refund transaction
            var recoveredRelatedTran = _recoveredTransaction.RelatedTransactions[0];
            Assert.AreEqual(refundTran.TransactionNumber, recoveredRelatedTran.TransactionNumber);
            Assert.AreEqual(refundTran.TillNumber, recoveredRelatedTran.TillNumber);
            Assert.AreEqual(refundTran.CreateDateTime.Date, recoveredRelatedTran.CreateDateTime.Date);
            Assert.AreEqual(refundTran.CreateDateTime.Hour, recoveredRelatedTran.CreateDateTime.Hour);
            Assert.AreEqual(refundTran.CreateDateTime.Minute, recoveredRelatedTran.CreateDateTime.Minute);
            Assert.AreEqual(refundTran.CreateDateTime.Second, recoveredRelatedTran.CreateDateTime.Second);
        }

        [Then(@"Restored transactions has correct linking to each other")]
        public void ThenRestoreTransactionsHasCorrectLinkingToEachOther()
        {
            var originalSale = (RetailTransaction) Ctx.SentTransactions[0];
            var originalFirstRefund = (RetailTransaction) Ctx.SentTransactions[1];
            var originalSecondRefund = (RetailTransaction) Ctx.SentTransactions[2];
            var restoredSale = _recoveredTransaction;
            var restoredFirstRefund = (RetailTransaction) _recoveredTransaction.RelatedTransactions[0];
            var restoredSecondRefund = (RetailTransaction) _recoveredTransaction.RelatedTransactions[0].RelatedTransactions[0];

            Assert.IsNotNull(restoredFirstRefund);
            Assert.IsNotNull(restoredSecondRefund);
            Assert.AreEqual(originalSale.SourceTransactionId, restoredSale.SourceTransactionId);
            Assert.AreEqual(originalSale.SourceTransactionId, restoredFirstRefund.Items[0].ItemRefund.RefundedTransactionId);
            Assert.AreEqual(originalFirstRefund.SourceTransactionId, restoredFirstRefund.SourceTransactionId);
            Assert.AreEqual(originalFirstRefund.SourceTransactionId, restoredSecondRefund.Items[0].ItemRefund.RefundedTransactionId);
            Assert.AreEqual(originalSecondRefund.SourceTransactionId, restoredSecondRefund.SourceTransactionId);
        }

        [Then(@"Recovered sale transaction has two related refund transactions in it")]
        public void ThenRecoveredSaleTransactionHasTwoRelatedRefundTransactionsInIt()
        {
            var saleTran = (RetailTransaction) Ctx.SentTransactions[0];
            var firstRefund = (RetailTransaction) Ctx.SentTransactions[1];
            var secondRefund = (RetailTransaction)Ctx.SentTransactions[2];

            Assert.IsNotNull(_recoveredTransaction.RelatedTransactions);
            Assert.AreEqual(2, _recoveredTransaction.RelatedTransactions.Count);

            var firstRecoveredRefund = _recoveredTransaction.RelatedTransactions[0];
            var secondRecoveredRefund = _recoveredTransaction.RelatedTransactions[1];

            //Check original sale transaction
            Assert.AreEqual(saleTran.CreateDateTime.Date, _recoveredTransaction.CreateDateTime.Date);
            Assert.AreEqual(saleTran.TillNumber, _recoveredTransaction.TillNumber);
            Assert.AreEqual(saleTran.TransactionNumber, _recoveredTransaction.TransactionNumber);
            Assert.AreEqual(saleTran.ReceiptBarcode, _recoveredTransaction.ReceiptBarcode);

            //Check first related refund
            Assert.AreEqual(firstRefund.TransactionNumber, firstRecoveredRefund.TransactionNumber);
            Assert.AreEqual(firstRefund.TillNumber, firstRecoveredRefund.TillNumber);
            Assert.AreEqual(firstRefund.CreateDateTime.Date, firstRecoveredRefund.CreateDateTime.Date);
            Assert.AreEqual(firstRefund.ReceiptBarcode, _recoveredTransaction.ReceiptBarcode);

            //Check second related refund
            Assert.AreEqual(secondRefund.TransactionNumber, secondRecoveredRefund.TransactionNumber);
            Assert.AreEqual(secondRefund.TillNumber, secondRecoveredRefund.TillNumber);
            Assert.AreEqual(secondRefund.CreateDateTime.Date, secondRecoveredRefund.CreateDateTime.Date);
        }

        [Then(@"Recovered transaction header has similar values with original transaction")]
        public void ThenRecoveredTransactionHeaderHasSimilarValuesWithOriginalTransaction()
        {
            //Fields from transaction
            Assert.AreEqual(Ctx.SentTransaction.CreateDateTime.Date, _recoveredTransaction.CreateDateTime.Date);
            Assert.AreEqual(Ctx.SentTransaction.CreateDateTime.TimeOfDay.Hours, _recoveredTransaction.CreateDateTime.TimeOfDay.Hours);
            Assert.AreEqual(Ctx.SentTransaction.CreateDateTime.TimeOfDay.Minutes, _recoveredTransaction.CreateDateTime.TimeOfDay.Minutes);
            Assert.AreEqual(Ctx.SentTransaction.CreateDateTime.TimeOfDay.Seconds, _recoveredTransaction.CreateDateTime.TimeOfDay.Seconds);
            Assert.AreEqual(Ctx.SentTransaction.Source, _recoveredTransaction.Source);
            Assert.AreEqual(Ctx.SentTransaction.SourceTransactionId, _recoveredTransaction.SourceTransactionId);
            Assert.AreEqual(Ctx.SentTransaction.ReceiptBarcode, _recoveredTransaction.ReceiptBarcode);
            Assert.AreEqual(Ctx.SentTransaction.StoreNumber, _recoveredTransaction.StoreNumber);
            Assert.AreEqual(Ctx.SentTransaction.TillNumber, _recoveredTransaction.TillNumber);
            Assert.AreEqual(Ctx.SentTransaction.TransactionNumber, _recoveredTransaction.TransactionNumber);
            Assert.AreEqual(Ctx.SentTransaction.CashierNumber, _recoveredTransaction.CashierNumber);
            Assert.AreEqual(Ctx.SentTransaction.BrandCode, _recoveredTransaction.BrandCode);
            Assert.AreEqual(Ctx.SentTransaction.IsTraining, _recoveredTransaction.IsTraining);
            //Fields from voidable transaction
            Assert.AreEqual(((VoidableTransaction)Ctx.SentTransaction).AuthorizerNumber, _recoveredTransaction.AuthorizerNumber);
            Assert.AreEqual(((VoidableTransaction)Ctx.SentTransaction).TransactionStatus, _recoveredTransaction.TransactionStatus);
            Assert.AreEqual(((VoidableTransaction)Ctx.SentTransaction).VoidSupervisor, _recoveredTransaction.VoidSupervisor);
            //Fields from payable transaction
            Assert.AreEqual(((PayableTransaction)Ctx.SentTransaction).TotalAmount, _recoveredTransaction.TotalAmount);
            //Fields from retail transaction
            Assert.AreEqual(((RetailTransaction)Ctx.SentTransaction).TaxAmount, _recoveredTransaction.TaxAmount);
            Assert.AreEqual(((RetailTransaction)Ctx.SentTransaction).RecalledTransactionId, _recoveredTransaction.RecalledTransactionId);
            Assert.AreEqual(((RetailTransaction)Ctx.SentTransaction).Manager, _recoveredTransaction.Manager);
            Assert.AreEqual(((RetailTransaction)Ctx.SentTransaction).RefundCashier, _recoveredTransaction.RefundCashier);
        }

        [Then(@"Recovered sale transaction has correct Source ans SourceTranId")]
        public void ThenRecoveredSaleTransactionHasCorrectSourceAnsSourceTranId()
        {
            var recoveredSaleTransaction = _recoveredTransaction;
            var storeNumber = DataLayerFactory.Create<DictionariesRepository>().GetStoreId();

            var tranKey = Ctx.FirstTransactionKey;
            var expectedSourceTranId = LegacyReceiptBarcodeHelper.GenerateLegacyTranId(storeNumber, tranKey.TransactionNumber, tranKey.CreateDate, tranKey.TillNumber);

            Assert.AreEqual(Global.LegacySource, recoveredSaleTransaction.Source);
            Assert.AreEqual(expectedSourceTranId, recoveredSaleTransaction.SourceTransactionId);
        }

        [Then(@"Restored refund line for SKU (.*) has correct legacy reference to the first transaction")]
        public void ThenRestoredRefundLineForSkuHasCorrectReferenceToTheFirstTransaction(string skuNumber)
        {
            CheckLegacyReferenceToTransaction(Ctx.SentTransactionKeys[0], skuNumber);
        }

        [Then(@"Restored refund line for SKU (.*) has correct legacy reference to the second transaction")]
        public void ThenRestoredRefundLineForSkuHasCorrectReferenceToTheSecondTransaction(string skuNumber)
        {
            CheckLegacyReferenceToTransaction(Ctx.SentTransactionKeys[1], skuNumber);
        }

        [Then(@"Restored refund line for SKU (.*) has correct new reference to the second transaction")]
        public void ThenRestoredRefundLineForSkuHasCorrectNewReferenceToTheSecondTransaction(string skuNumber)
        {
            CheckNewReferenceToTransaction((RetailTransaction) Ctx.SentTransactions[1], skuNumber);
        }

        [Then(@"Related Transaction Refund Transaction Item Id for SKU (.*) = '(.*)'")]
        public void ThenRelatedTransactionRefundTransactionItemIdForSKU(string skuNumber, string refundTransactionItemId)
        {
            var recoveredRefundTransaction = (RetailTransaction)_recoveredTransaction.RelatedTransactions[0];

            var relatedTransactionItemrefund = recoveredRefundTransaction.Items.First(x => ((SkuProduct)x.Product).BrandProductId == skuNumber).ItemRefund;

            Assert.AreEqual(refundTransactionItemId, relatedTransactionItemrefund.RefundedTransactionItemId);
        }

        [Then(@"Restored refund line for SKU (.*) has empty reference")]
        public void ThenRestoredRefundLineForSkuHasEmptyReference(string skuNumber)
        {
            var recoveredRefundTransaction = (RetailTransaction)_recoveredTransaction.RelatedTransactions[0];
            Assert.AreEqual(null, recoveredRefundTransaction.Items.First(x => ((SkuProduct)x.Product).BrandProductId == skuNumber).ItemRefund.RefundedTransactionId);
        }

        [Then(@"Recovered item has similar values with original item")]
        public void ThenRecoveredItemHasSimilarValuesWithOriginalItem()
        {
            var originalItem = ((RetailTransaction) Ctx.SentTransaction).Items.First();
            var recoveredItem = _recoveredTransaction.Items.First();

            Assert.AreEqual(originalItem.ItemIndex, recoveredItem.ItemIndex);
            Assert.AreEqual(originalItem.IsReversed, recoveredItem.IsReversed);
            Assert.AreEqual(originalItem.Direction, recoveredItem.Direction);
            Assert.AreEqual(originalItem.ItemRefund, recoveredItem.ItemRefund);
            if (originalItem.ItemRefund != null && recoveredItem.ItemRefund != null)
            { 
                Assert.AreEqual(originalItem.ItemRefund.RefundReasonCode, recoveredItem.ItemRefund.RefundReasonCode);
                Assert.AreEqual(originalItem.ItemRefund.RefundedTransactionId, recoveredItem.ItemRefund.RefundedTransactionId);
                Assert.AreEqual(originalItem.ItemRefund.RefundedTransactionItemId, recoveredItem.ItemRefund.RefundedTransactionItemId);
            }
            Assert.AreEqual(((SkuProduct)originalItem.Product).GroupProductId, ((SkuProduct)recoveredItem.Product).GroupProductId);
            Assert.AreEqual(((SkuProduct)originalItem.Product).BrandProductId, ((SkuProduct)recoveredItem.Product).BrandProductId);
            Assert.AreEqual(((SkuProduct)originalItem.Product).ProductInputType, ((SkuProduct)recoveredItem.Product).ProductInputType);
            Assert.AreEqual(((SkuProduct)originalItem.Product).AbsQuantity, ((SkuProduct)recoveredItem.Product).AbsQuantity);
            Assert.AreEqual(((SkuProduct)originalItem.Product).InitialPrice, ((SkuProduct)recoveredItem.Product).InitialPrice);
            Assert.AreEqual(originalItem.VatRate, recoveredItem.VatRate);
        }

        [Then(@"Recovered gift card product has similar values with original product")]
        public void ThenRecoveredGiftCardProductHasSimilarValuesWithOriginalProduct()
        {
            var originalItem = ((RetailTransaction)Ctx.SentTransaction).Items.First();
            var recoveredItem = _recoveredTransaction.Items.First();

            Assert.AreEqual(originalItem.ItemIndex, recoveredItem.ItemIndex);
            Assert.AreEqual(originalItem.IsReversed, recoveredItem.IsReversed);
            Assert.AreEqual(originalItem.Direction, recoveredItem.Direction);
            Assert.AreEqual(originalItem.ItemRefund, recoveredItem.ItemRefund);

            Assert.AreEqual(((GiftCardProduct) originalItem.Product).AbsAmount, ((GiftCardProduct) recoveredItem.Product).AbsAmount);
            Assert.AreEqual(((GiftCardProduct) originalItem.Product).GiftCardOperation.AuthCode,
                ((GiftCardProduct)recoveredItem.Product).GiftCardOperation.AuthCode);
            Assert.AreEqual(((GiftCardProduct)originalItem.Product).GiftCardOperation.CardNumber,
                ((GiftCardProduct)recoveredItem.Product).GiftCardOperation.CardNumber);
        }

        [Then(@"There is no item with SKU (.*) in restored transaction")]
        public void ThenThereIsNoItemWithSKUInRestoredTransaction(string skuProduct)
        {
            var recoveredSkuProducts = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>();

            Assert.IsEmpty(recoveredSkuProducts.Where(x => x.BrandProductId == skuProduct));
        }

        [Then(@"Recovered cash payment has similar values with original payment")]
        public void ThenRecoveredPaymentHasSimilarValuesWithOriginalPayment()
        {
            var originalPayment = ((RetailTransaction)Ctx.SentTransaction).Payments.OfType<CashPayment>().First();
            var recoveredPayment = _recoveredTransaction.Payments.OfType<CashPayment>().First();
            CheckCashPayment(originalPayment, recoveredPayment);
        }

        [Then(@"Recovered cheque payment has similar values with original payment")]
        public void ThenRecoveredChequePaymentHasSimilarValuesWithOriginalPayment()
        {
            var originalPayment = ((RetailTransaction)Ctx.SentTransaction).Payments.OfType<ChequePayment>().First();
            var recoveredPayment = _recoveredTransaction.Payments.OfType<ChequePayment>().First();
            CheckChequePayment(originalPayment, recoveredPayment);
        }

        [Then(@"Recovered eft payment has similar values with original payment")]
        public void ThenRecoveredEftPaymentHasSimilarValuesWithOriginalPayment()
        {
            var originalPayment = ((RetailTransaction)Ctx.SentTransaction).Payments.OfType<EftPayment>().First();
            var recoveredPayment = _recoveredTransaction.Payments.OfType<EftPayment>().First();
            CheckEftPayment(originalPayment, recoveredPayment);
        }

        [Then(@"Recovered cash change payment has similar values with original payment")]
        public void ThenRecoveredCashChangePaymentHasSimilarValuesWithOriginalPayment()
        {
            var originalChangePayment = ((RetailTransaction)Ctx.SentTransaction).Payments.OfType<ChangePayment>().First();
            var recoveredChangePayment = _recoveredTransaction.Payments.OfType<ChangePayment>().First();
            CheckChangePayment(originalChangePayment, recoveredChangePayment);
        }

        [Then(@"Recovered gift card payment has similar values with original payment")]
        public void ThenRecoveredGiftCardPaymentHasSimilarValuesWithOriginalPayment()
        {
            var originalPayment = ((RetailTransaction)Ctx.SentTransaction).Payments.OfType<GiftCardPayment>().First();
            var recoveredPayment = _recoveredTransaction.Payments.OfType<GiftCardPayment>().First();
            CheckGiftCardPayment(originalPayment, recoveredPayment);
        }

        [Then(@"Exception has been thrown")]
        public void ThenExceptionHasBeenThrown()
        {
            Assert.IsNotNull(_thrownExpection);
        }

        [Then(@"Recovered product item has VatRate equal (.*)")]
        public void ThenRecoveredTransactionHasVatRateEqual(decimal vatRate)
        {
            var recoveredItem = _recoveredTransaction.Items.First();
            Assert.AreEqual(vatRate, recoveredItem.VatRate);
        }

        [Then(@"Absolute Vat Amount of Recovered item for SKU (.*) is equal to (.*)")]
        public void ThenAbsoluteVatAmountOfRecoveredItemForSKUIsEqualTo(string skuNumber, decimal absVatAmount)
        {
            var recoveredItem = _recoveredTransaction.Items.First(x => x.Product is SkuProduct && ((SkuProduct)x.Product).BrandProductId == skuNumber);

            Assert.AreEqual(absVatAmount, recoveredItem.AbsVatAmount);
        }

        [Then(@"Recovered instant fulfillment has similar values with original instant fulfillment")]
        public void ThenRecoveredInstantFulfillmentHasSimilarValuesWithOriginalInstantFulfillment()
        {
            var originalInstantFulfillment = ((RetailTransaction)Ctx.SentTransaction).Fulfillments.OfType<InstantFulfillment>().First();
            var recoveredInstantFulfillment = _recoveredTransaction.Fulfillments.OfType<InstantFulfillment>().First();
            CheckInstantFulfillment(originalInstantFulfillment, recoveredInstantFulfillment);
        }

        [Then(@"Recovered delivery fulfillment has similar values with original delivery fulfillment")]
        public void ThenRecoveredDeliveryFulfillmentHasSimilarValuesWithOriginalDeliveryFulfillment()
        {
            var originalDeliveryFulfillment = ((RetailTransaction)Ctx.SentTransaction).Fulfillments.OfType<DeliveryFulfillment>().First();
            var recoveredDeliveryFulfillment = _recoveredTransaction.Fulfillments.OfType<DeliveryFulfillment>().First();
            CheckDeliveryFulfillment(originalDeliveryFulfillment, recoveredDeliveryFulfillment);
        }

        [Then(@"Recovered collection fulfillment has similar values with original collection fulfillment")]
        public void ThenRecoveredCollectionFulfillmentHasSimilarValuesWithOriginalCollectionFulfillment()
        {
            var originalCollectionFulfillment = ((RetailTransaction)Ctx.SentTransaction).Fulfillments.OfType<CollectionFulfillment>().First();
            var recoveredCollectionFulfillment = _recoveredTransaction.Fulfillments.OfType<CollectionFulfillment>().First();
            CheckCollectionFulfillment(originalCollectionFulfillment, recoveredCollectionFulfillment);
        }

        [Then(@"Recovered transaction has two contacts with correct values")]
        public void ThenRecoveredTransactionHasTwoContactsWithCorrectValues()
        {
            var originalCustomerContact = ((RetailTransaction) Ctx.SentTransaction).Contacts[0];
            var originalOrderContact = ((RetailTransaction) Ctx.SentTransaction).Contacts[1];
            var recoveredCustomerContact = _recoveredTransaction.Contacts[0];
            var recoveredOrderContact = _recoveredTransaction.Contacts[1];

            CheckContact(originalCustomerContact, recoveredCustomerContact);
            CheckContact(originalOrderContact, recoveredOrderContact);
        }

        [Then(@"Recovered transaction has delivery charge item as in original transaction")]
        public void ThenRecoveredTransactionHasDeliveryChargeItemAsInOriginalTransaction()
        {
            var originalDeliveryItem =
                ((RetailTransaction) Ctx.SentTransaction).Items.First(x => x.Product is DeliveryProduct);
            var recoveredDeliveryItem =
                _recoveredTransaction.Items.First(x => x.Product is DeliveryProduct);
            Assert.IsNotNull(originalDeliveryItem);
            Assert.IsNotNull(recoveredDeliveryItem);
            Assert.AreEqual(originalDeliveryItem.Direction, recoveredDeliveryItem.Direction);
            Assert.AreEqual(originalDeliveryItem.IsReversed, recoveredDeliveryItem.IsReversed);
            Assert.AreEqual(originalDeliveryItem.ItemIndex, recoveredDeliveryItem.ItemIndex);
            Assert.AreEqual(originalDeliveryItem.ItemRefund, recoveredDeliveryItem.ItemRefund);
            Assert.AreEqual(((DeliveryProduct) originalDeliveryItem.Product).AbsAmount, ((DeliveryProduct) recoveredDeliveryItem.Product).AbsAmount);
        }

        [Then(@"Recovered refund transaction has similar values with original refund")]
        public void ThenRecoveredRefundTransactionHasSimilarValuesWithOriginalRefund()
        {
            var originalRefundTransaction = ((RetailTransaction) Ctx.SentTransaction);
            var recoveredRefundTransaction = _recoveredTransaction;
            var originalRefundedItem = ((RetailTransaction) Ctx.SentTransaction).Items[0];
            var recoveredRefundedItem = _recoveredTransaction.Items[0];

            Assert.AreEqual(originalRefundTransaction.IsRefund(), recoveredRefundTransaction.IsRefund());
            Assert.AreEqual(originalRefundTransaction.RefundCashier, recoveredRefundTransaction.RefundCashier);
            Assert.AreEqual(originalRefundTransaction.TotalAmount, recoveredRefundTransaction.TotalAmount);
            CheckRefundedItems(originalRefundedItem, recoveredRefundedItem);
        }

        [Then(@"Recovered Refund Transaction Item has Refund Reason Code Unknown")]
        public void ThenRecoveredRefundTransactionItemHasRefundReasonCodeUnknown()
        {;
            var recoveredRefundedItem = _recoveredTransaction.Items[0];

            Assert.AreEqual(BO.Model.Enums.RefundReasonCode.UNKNOWN, recoveredRefundedItem.ItemRefund.RefundReasonCode);
        }

        [Then(@"Recovered Refund Transaction Item has Description '(.*)'")]
        public void ThenRecoveredRefundTransactionItemHasDescription(string description)
        {
            var recoveredRefundedItem = _recoveredTransaction.Items[0];

            Assert.AreEqual(description, recoveredRefundedItem.ItemRefund.Description);
        }

        [Then(@"Recovered refund transaction has correct reference to original transaction")]
        public void ThenRecoveredRefundTransactionHasCorrectReferenceToOriginalTransaction()
        {
            var originalSaleTransaction = ((RetailTransaction) Ctx.FirstTransaction);
            var originalRefundTransaction = ((RetailTransaction) Ctx.SentTransaction);
            var recoveredRefundTransaction = _recoveredTransaction;

            Assert.AreEqual(originalRefundTransaction.Items[0].ItemRefund.RefundReasonCode, recoveredRefundTransaction.Items[0].ItemRefund.RefundReasonCode);
            Assert.AreEqual(originalRefundTransaction.Items[0].ItemRefund.RefundedTransactionId, recoveredRefundTransaction.Items[0].ItemRefund.RefundedTransactionId);
            Assert.AreEqual(originalRefundTransaction.Items[0].ItemRefund.RefundedTransactionItemId, recoveredRefundTransaction.Items[0].ItemRefund.RefundedTransactionItemId);
            Assert.AreEqual(originalSaleTransaction.SourceTransactionId, recoveredRefundTransaction.Items[0].ItemRefund.RefundedTransactionId);
        }

        [Then(@"There is collegue discount in the restored transaction")]
        public void ThenThereIsCollegueDiscountInTheRestoredTransaction()
        {
            var originalTransaction = ((RetailTransaction)Ctx.FirstTransaction);
            var recoveredTransaction = _recoveredTransaction;

            var originalTransactionEmployeeDiscount = originalTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().SelectMany(x => x.Discounts).OfType<EmployeeDiscount>().First();
            var recoveredTransactionEmployeeDiscount = recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().SelectMany(x => x.Discounts).OfType<EmployeeDiscount>().First();

            Assert.IsNotNull(recoveredTransactionEmployeeDiscount);

            Assert.AreEqual(originalTransactionEmployeeDiscount.CardNumber, recoveredTransactionEmployeeDiscount.CardNumber);
            Assert.AreEqual(originalTransactionEmployeeDiscount.SavingAmount, recoveredTransactionEmployeeDiscount.SavingAmount);
            Assert.AreEqual(originalTransactionEmployeeDiscount.SupervisorNumber, recoveredTransactionEmployeeDiscount.SupervisorNumber);
        }

        [Then(@"There are (.*) restored events in transaction for SKU '(.*)'")]
        public void ThenThereAreRestoredEventsInTransaction(int eventCount, string skuNumber)
        {
            var product = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First(x => x.BrandProductId == skuNumber);
            Assert.IsNotNull(product.Discounts);
            Assert.AreEqual(eventCount, product.Discounts.Count);
        }

        [Then(@"There is '(.*)' discount event for SKU = '(.*)' with saving amount '(.*)' and code '(.*)'")]
        public void ThenThereIsDiscountEventForSkuWithSavingAmountAndCode(string strEventType, string skuNumber, decimal discountAmount, string eventCode)
        {
            var eventType = GetEventType(strEventType);
            var discount = GetDiscountByTypeAndIndex(eventType, 0, skuNumber);
            Assert.IsNotNull(discount);
            Assert.AreEqual(eventCode, discount.EventDescriptor.EventCode);
            Assert.AreEqual(discountAmount, discount.HitAmounts.First());
        }

        [Then(@"There is '(.*)' discount event for SKU = '(.*)' with saving amount '(.*)' and code from db")]
        public void ThenThereIsDiscountEventForSkuWithSavingAmountAndCodeFromDb(string strEventType, string skuNumber, decimal discountAmount)
        {
            var dictRepo = DataLayerFactory.Create<DictionariesRepository>();
            var eventType = GetEventType(strEventType);
            var discount = GetDiscountByTypeAndIndex(eventType, 0, skuNumber);
            var eventCode = dictRepo.GetSettingStringValue(ParameterId.HierarchySpendEventDiscountCode);

            Assert.IsNotNull(discount);
            Assert.AreEqual(eventCode, discount.EventDescriptor.EventCode);
            Assert.AreEqual(discountAmount, discount.HitAmounts.First());
        }

        [Then(@"There is no Event Discount in the restored transaction")]
        public void ThenThereIsNoCollegueDiscountInTheRestoredTransaction()
        {
            var restoredProduct = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First();

            Assert.IsNotNull(restoredProduct.Discounts);
            Assert.IsEmpty(restoredProduct.Discounts.OfType<EventDiscount>());
        }

        [Then(@"There is coupon with code = '(.*)', barcode = '(.*)', event type = '(.*)'")]
        public void ThenThereIsCouponWithCodeBarcodeEventType(string eventCode, string barcode, string strEventType)
        {
            var eventType = GetEventType(strEventType);
            var coupon = _recoveredTransaction.Coupons.FirstOrDefault();
            Assert.IsNotNull(coupon);
            Assert.AreEqual(eventType, coupon.EventDescriptor.EventType);
            Assert.AreEqual(eventCode, coupon.EventDescriptor.EventCode);
            Assert.AreEqual(barcode, coupon.CouponBarcode);
        }

        [Then(@"There are two discount hits in restored (.*) event for SKU '(.*)' with code = '(.*)' with first amount = (.*) and second amount = (.*)")]
        public void ThenThereAreTwoDiscountHitsInRestoredEventForSkuWithFirstAmountAndSecondAmount(string strEventType, string skuNumber, string eventCode, decimal firstAmount, decimal secondAmount)
        {
            var eventType = GetEventType(strEventType);
            var discount = GetDiscountByTypeAndIndex(eventType, 0, skuNumber);

            Assert.IsNotNull(discount);
            Assert.AreEqual(eventCode, discount.EventDescriptor.EventCode);
            Assert.AreEqual(firstAmount, discount.HitAmounts[0]);
            Assert.AreEqual(secondAmount, discount.HitAmounts[1]);
        }

        [Then(@"There is only one Price Match or Promise price override discount in the restored transaction")]
        public void ThenThereIsPriceMatchOrPromisePriceOverrideDiscountInTheRestoredTransaction()
        {
            var restoredProduct = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().Single();

            Assert.IsNotNull(restoredProduct.Discounts);
            Assert.IsNotEmpty(restoredProduct.Discounts.OfType<PriceMatchDiscount>());
            Assert.AreEqual(1, restoredProduct.Discounts.Count);
        }

        [Then(@"There is the same data in the Price Match discount as in original discount")]
        public void ThenThereIsTheSameDataInThePriceMatchDiscountAsInOriginalDiscount()
        {
            var originalSaleTransaction = ((RetailTransaction) Ctx.FirstTransaction);
            var originalItem = originalSaleTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().Single();
            var restoredItem = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().Single();

            Assert.IsNotNull(restoredItem.Discounts);
            Assert.IsNotEmpty(restoredItem.Discounts.OfType<PriceMatchDiscount>());
            Assert.AreEqual(originalItem.Discounts.OfType<PriceMatchDiscount>().First().CompetitorName, restoredItem.Discounts.OfType<PriceMatchDiscount>().First().CompetitorName);
            Assert.AreEqual(originalItem.Discounts.OfType<PriceMatchDiscount>().First().IsVATIncluded, restoredItem.Discounts.OfType<PriceMatchDiscount>().First().IsVATIncluded);
            Assert.AreEqual(originalItem.Discounts.OfType<PriceMatchDiscount>().First().SupervisorNumber, restoredItem.Discounts.OfType<PriceMatchDiscount>().First().SupervisorNumber);
        }

        [Then(@"There is price override discount in the restored transaction with Reason Code Unknown")]
        public void ThenThereIsPriceOverrideDiscountInTheRestoredTransactionWithReasonCodeUnknown()
        {
            var originalSaleTransaction = ((RetailTransaction) Ctx.FirstTransaction);
            var originalProduct = originalSaleTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First();
            var restoredProduct = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First();

            Assert.IsNotEmpty(restoredProduct.Discounts.OfType<PriceOverrideDiscount>());
            Assert.AreEqual(BO.Model.Enums.PriceOverrideReasonCode.UNKNOWN, restoredProduct.Discounts.OfType<PriceOverrideDiscount>().First().PriceOverrideCode);
            Assert.AreEqual(originalProduct.Discounts.OfType<PriceOverrideDiscount>().First().SavingAmount, restoredProduct.Discounts.OfType<PriceOverrideDiscount>().First().SavingAmount);
            Assert.AreEqual(originalProduct.Discounts.OfType<PriceOverrideDiscount>().First().SupervisorNumber, restoredProduct.Discounts.OfType<PriceOverrideDiscount>().First().SupervisorNumber);
        }

        [Then(@"Description of Price Override Discount is '(.*)'")]
        public void ThenDescriptionOfPriceOverrideDiscountId(string description)
        {
            var restoredProduct = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First();

            Assert.AreEqual(description, restoredProduct.Discounts.OfType<PriceOverrideDiscount>().Single().Description);
        }

        [Then(@"There is only one price override discount in the restored transaction with Reason Code '(.*)'")]
        public void ThenThereIsOnlyOnePriceOverrideDiscountInTheRestoredTransactionWithReasonCode(string reasonCode)
        {
            var restoredProduct = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First();

            Assert.IsNotNull(restoredProduct.Discounts);
            Assert.IsNotEmpty(restoredProduct.Discounts.OfType<PriceOverrideDiscount>());
            Assert.AreEqual(GetPriceOverrideReasonCode(reasonCode), restoredProduct.Discounts.OfType<PriceOverrideDiscount>().Single().PriceOverrideCode);
        }

        [Then(@"The Price Override Discount amount is (.*)")]
        public void ThenThePriceOverrideDiscountAmountIs(decimal discountAmount)
        {
            var restoredProduct = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First();

            Assert.AreEqual(discountAmount, restoredProduct.Discounts.OfType<PriceOverrideDiscount>().Single().SavingAmount);
        }

        [Then(@"The Price Match discount amount is (.*)")]
        public void ThenThePriceMatchDiscountAmountIs(decimal discountAmount)
        {
            var restoredProduct = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First();

            Assert.AreEqual(discountAmount, restoredProduct.Discounts.OfType<PriceMatchDiscount>().Single().SavingAmount);
        }

        [Then(@"Returned barcode contains information about Store Number '(.*)', Till '(.*)', Transaction Number '(.*)' and Today Date")]
        public void ThenReturnedBarcodeContainsInformationAboutStoreNumberTillTransactionNumberAndTodayDate(string storeNumber, string tillNumber, string tranNumber)
        {
            var expectedBarcode = LegacyReceiptBarcodeHelper.GenerateReceiptBarcode(storeNumber, tranNumber, DateTime.Now, tillNumber);
            Assert.AreEqual(expectedBarcode, _recoveredTransaction.ReceiptBarcode);
        }

        [Then(@"Recovered Source Item Id is equal to '(.*)'")]
        public void ThenRecoveredSourceItemIdIsEqualTo(string sourceItemId)
        {
            var recoveredItem = _recoveredTransaction.Items.First();

            Assert.AreEqual(sourceItemId, recoveredItem.SourceItemId);
        }

        [Then(@"Related Transaction Refund Transaction has cancellation")]
        public void ThenRelatedTransactionRefundTransactionHasCancellation()
        {
            var relatedRetailTransaction = GetRecoveredRetailtransactionIfExists();
            Assert.IsNotNull(relatedRetailTransaction.Cancellation);
        }

        [Then(@"There is a fulfillment with source item id equal (.*) and quantity equal (.*)")]
        public void ThenThereIsAFulfillmentWithSourceItemIdEqualAndQuantityEqual(int sourceItemId, int quantity)
        {
            var relatedRetailTransaction = GetRecoveredRetailtransactionIfExists();

            Assert.IsNotNull(relatedRetailTransaction.Cancellation);

            var cancellation = relatedRetailTransaction.Cancellation.Items.FirstOrDefault(x => x.TransactionItemIndex == sourceItemId);

            Assert.IsNotNull(cancellation);
            Assert.AreEqual(quantity, cancellation.AbsQuantity);
        }

        [Then(@"There is no fulfillment with source item id equal (.*)")]
        public void ThenThereIsNoFulfillmentWithSourceItemIdEqual(int sourceItemId)
        {
            var relatedRetailTransaction = GetRecoveredRetailtransactionIfExists();

            Assert.IsNotNull(relatedRetailTransaction.Cancellation);

            var cancellation = relatedRetailTransaction.Cancellation.Items.FirstOrDefault(x => x.TransactionItemIndex == sourceItemId);

            Assert.IsNull(cancellation);
        }

        private RetailTransaction GetRecoveredRetailtransactionIfExists()
        {
            Assert.IsNotNull(_recoveredTransaction.RelatedTransactions);

            var relatedTransaction = (RetailTransaction)_recoveredTransaction.RelatedTransactions.FirstOrDefault();

            Assert.IsNotNull(relatedTransaction);

            return relatedTransaction;
        }

        private void CheckLegacyReferenceToTransaction(IDailyTillTranKey originalTransactionKey, string skuNumber)
        {
            var recoveredRefundTransaction = (RetailTransaction)_recoveredTransaction.RelatedTransactions[0];
            var storeNumber = DataLayerFactory.Create<DictionariesRepository>().GetStoreId();
            var expectedRefTranId = LegacyReceiptBarcodeHelper.GenerateLegacyTranId(storeNumber, originalTransactionKey.TransactionNumber, originalTransactionKey.CreateDate, originalTransactionKey.TillNumber);
            Assert.AreEqual(expectedRefTranId, recoveredRefundTransaction.Items.First(x => ((SkuProduct)x.Product).BrandProductId == skuNumber).ItemRefund.RefundedTransactionId);
        }

        private void CheckNewReferenceToTransaction(RetailTransaction originalTransaction, string skuNumber)
        {
            var recoveredRefundTransaction = (RetailTransaction)_recoveredTransaction.RelatedTransactions[0];
            Assert.AreEqual(originalTransaction.SourceTransactionId, recoveredRefundTransaction.Items.First(x => ((SkuProduct)x.Product).BrandProductId == skuNumber).ItemRefund.RefundedTransactionId);
        }

        private EventDiscount GetDiscountByTypeAndIndex(EventType eventType, int eventIndex, string skuNumber)
        {
            var product = _recoveredTransaction.Items.Select(x => x.Product).OfType<SkuProduct>().First(x => x.BrandProductId == skuNumber);
            Assert.IsNotNull(product.Discounts.OfType<EventDiscount>());
            return product.Discounts.OfType<EventDiscount>().Where(x => x.EventDescriptor.EventType == eventType).ToList()[eventIndex];
        }

        private void CheckRefundedItems(RetailTransactionItem originalRefundedItem, RetailTransactionItem recoveredRefundedItem)
        {
            Assert.IsNotNull(originalRefundedItem.ItemRefund);
            Assert.IsNotNull(recoveredRefundedItem.ItemRefund);
            Assert.AreEqual(originalRefundedItem.ItemRefund.RefundReasonCode, recoveredRefundedItem.ItemRefund.RefundReasonCode);
            Assert.AreEqual(originalRefundedItem.ItemRefund.RefundedTransactionId, recoveredRefundedItem.ItemRefund.RefundedTransactionId);
            Assert.AreEqual(originalRefundedItem.ItemRefund.RefundedTransactionItemId, recoveredRefundedItem.ItemRefund.RefundedTransactionItemId);
        }

        private void CheckCashPayment(CashPayment originalPayment, CashPayment recoveredPayment)
        {
            Assert.AreEqual(originalPayment.AbsAmount, recoveredPayment.AbsAmount);
            Assert.AreEqual(originalPayment.Direction, recoveredPayment.Direction);
        }

        private void CheckChequePayment(ChequePayment originalPayment, ChequePayment recoveredPayment)
        {
            Assert.AreEqual(originalPayment.AbsAmount, recoveredPayment.AbsAmount);
            Assert.AreEqual(originalPayment.Direction, recoveredPayment.Direction);
        }

        private void CheckEftPayment(EftPayment originalPayment, EftPayment recoveredPayment)
        {
            Assert.AreEqual(originalPayment.AbsAmount, recoveredPayment.AbsAmount);
            Assert.AreEqual(originalPayment.Direction, recoveredPayment.Direction);

            Assert.AreEqual(originalPayment.AuthCode, recoveredPayment.AuthCode);
            Assert.AreEqual(originalPayment.AuthDescription, recoveredPayment.AuthDescription);
            Assert.AreEqual(originalPayment.CardAcceptType, recoveredPayment.CardAcceptType);
            Assert.AreEqual(originalPayment.MerchantNumber, recoveredPayment.MerchantNumber);
            Assert.AreEqual(originalPayment.PaymentCard.PanHash, recoveredPayment.PaymentCard.PanHash);
            Assert.AreEqual(originalPayment.TokenId, recoveredPayment.TokenId);
            Assert.AreEqual(originalPayment.TransactionUid, recoveredPayment.TransactionUid);

            var originalPaymentCard = originalPayment.PaymentCard;
            var recoveredPaymentCard = recoveredPayment.PaymentCard;

            Assert.AreEqual(originalPaymentCard.CardDescription, recoveredPaymentCard.CardDescription);
            Assert.AreEqual(originalPaymentCard.CardExpireDate.Month, recoveredPaymentCard.CardExpireDate.Month);
            Assert.AreEqual(originalPaymentCard.CardExpireDate.Year, recoveredPaymentCard.CardExpireDate.Year);

            Assert.That(recoveredPaymentCard.CardStartDate, originalPaymentCard.CardStartDate == null ? Is.Null : Is.Not.Null);
            if (originalPaymentCard.CardStartDate != null && recoveredPaymentCard.CardStartDate != null)
            {
                Assert.AreEqual(originalPaymentCard.CardStartDate.Value.Month, recoveredPaymentCard.CardStartDate.Value.Month);
                Assert.AreEqual(originalPaymentCard.CardStartDate.Value.Year, recoveredPaymentCard.CardStartDate.Value.Year);
            }

            Assert.AreEqual(originalPaymentCard.CardType, recoveredPaymentCard.CardType);
            Assert.AreEqual(originalPaymentCard.IssueNumberSwitch, recoveredPaymentCard.IssueNumberSwitch);
            Assert.AreEqual(originalPaymentCard.MaskedCardNumber, recoveredPaymentCard.MaskedCardNumber);
        }

        private void CheckChangePayment(ChangePayment originalPayment, ChangePayment recoveredPayment)
        {
            Assert.AreEqual(originalPayment.AbsAmount, recoveredPayment.AbsAmount);
            Assert.AreEqual(originalPayment.Direction, recoveredPayment.Direction);
        }

        private void CheckGiftCardPayment(GiftCardPayment originalPayment, GiftCardPayment recoveredPayment)
        {
            Assert.AreEqual(originalPayment.AbsAmount, recoveredPayment.AbsAmount);
            Assert.AreEqual(originalPayment.Direction, recoveredPayment.Direction);

            Assert.AreEqual(originalPayment.CardAcceptType, recoveredPayment.CardAcceptType);
            Assert.AreEqual(originalPayment.CardDescription, recoveredPayment.CardDescription);
            Assert.AreEqual(originalPayment.GiftCardOperation.AuthCode, recoveredPayment.GiftCardOperation.AuthCode);
            Assert.AreEqual(originalPayment.GiftCardOperation.CardNumber, recoveredPayment.GiftCardOperation.CardNumber);
        }

        private void CheckInstantFulfillment(InstantFulfillment originalFulfillment, InstantFulfillment recoveredFulfillment)
        {
            Assert.AreEqual(originalFulfillment.Items.Count, recoveredFulfillment.Items.Count);
            for (var i = 0; i < originalFulfillment.Items.Count; i++)
            {
                CheckFulfillmentItem(originalFulfillment.Items[i], recoveredFulfillment.Items[i]);
            }
        }

        private void CheckDeliveryFulfillment(DeliveryFulfillment originalFulfillment, DeliveryFulfillment recoveredFulfillment)
        {
            Assert.AreEqual(originalFulfillment.DeliveryDate, recoveredFulfillment.DeliveryDate);
            Assert.AreEqual(originalFulfillment.DeliveryInstructions, recoveredFulfillment.DeliveryInstructions);
            Assert.AreEqual(originalFulfillment.DeliveryType, recoveredFulfillment.DeliveryType);
            Assert.AreEqual(originalFulfillment.FreeDeliveryAuthorizerNumber, recoveredFulfillment.FreeDeliveryAuthorizerNumber);
            
            Assert.AreEqual(originalFulfillment.ContactInfoIndex, recoveredFulfillment.ContactInfoIndex);
            Assert.AreEqual(originalFulfillment.Number, recoveredFulfillment.Number);

            Assert.AreEqual(originalFulfillment.Items.Count, recoveredFulfillment.Items.Count);
            for (var i = 0; i < originalFulfillment.Items.Count; i++)
            {
                CheckFulfillmentItem(originalFulfillment.Items[i], recoveredFulfillment.Items[i]);
            }
        }

        private void CheckCollectionFulfillment(CollectionFulfillment originalFulfillment, CollectionFulfillment recoveredFulfillment)
        {
            Assert.AreEqual(originalFulfillment.CollectionDate, recoveredFulfillment.CollectionDate);
            Assert.AreEqual(originalFulfillment.ContactInfoIndex, recoveredFulfillment.ContactInfoIndex);
            Assert.AreEqual(originalFulfillment.Number, recoveredFulfillment.Number);

            Assert.AreEqual(originalFulfillment.Items.Count, recoveredFulfillment.Items.Count);
            for (var i = 0; i < originalFulfillment.Items.Count; i++)
            {
                CheckFulfillmentItem(originalFulfillment.Items[i], recoveredFulfillment.Items[i]);
            }
        }

        private void CheckFulfillmentItem(FulfillmentItem originalItem, FulfillmentItem recoveredItem)
        {
            Assert.AreEqual(originalItem.AbsQuantity, recoveredItem.AbsQuantity);
            Assert.AreEqual(originalItem.FulfillerNumber, recoveredItem.FulfillerNumber);
            Assert.AreEqual(originalItem.TransactionItemIndex, recoveredItem.TransactionItemIndex);
        }

        private void CheckContact(ContactInfo originalCustomerContact, ContactInfo recoveredCustomerContact)
        {
            Assert.AreEqual(originalCustomerContact.ContactAddress.AddressLine1, recoveredCustomerContact.ContactAddress.AddressLine1);
            Assert.AreEqual(originalCustomerContact.ContactAddress.AddressLine2, recoveredCustomerContact.ContactAddress.AddressLine2);
            Assert.AreEqual(originalCustomerContact.ContactAddress.PostCode, recoveredCustomerContact.ContactAddress.PostCode);
            Assert.AreEqual(originalCustomerContact.ContactAddress.Town, recoveredCustomerContact.ContactAddress.Town);
            Assert.AreEqual(originalCustomerContact.Email, recoveredCustomerContact.Email);
            Assert.AreEqual(originalCustomerContact.MobilePhone, recoveredCustomerContact.MobilePhone);
            Assert.AreEqual(originalCustomerContact.Name, recoveredCustomerContact.Name);
            Assert.AreEqual(originalCustomerContact.Phone, recoveredCustomerContact.Phone);
            Assert.AreEqual(originalCustomerContact.WorkPhone, recoveredCustomerContact.WorkPhone);
        }

        private EventType GetEventType(string strEventType)
        {
            if (strEventType == "DealGroup") return EventType.DEAL_GROUP;
            if (strEventType == "QuantityBreak") return EventType.QUANTITY_BREAK;
            if (strEventType == "Multibuy") return EventType.MULTIBUY;
            if (strEventType == "HierarchySpend") return EventType.HIERARCHY_SPEND;
            throw new ArgumentException(String.Format("Unknown event type = {0}", strEventType));
        }

        private BO.Model.Enums.PriceOverrideReasonCode GetPriceOverrideReasonCode(string priceOverrideCode)
        {
            switch (priceOverrideCode)
            {
                case "Damaged Goods":
                    return BO.Model.Enums.PriceOverrideReasonCode.DAMAGED_GOODS;
                case "HDC Return":
                    return BO.Model.Enums.PriceOverrideReasonCode.HDC_RETURN;
                case "Incorrect Price Disolayed":
                    return BO.Model.Enums.PriceOverrideReasonCode.INCORRECT_PRICE_DISPLAYED;
                case "Managers Discretion":
                    return BO.Model.Enums.PriceOverrideReasonCode.MANAGERS_DISCRETION;
                case "Refund Price Difference":
                    return BO.Model.Enums.PriceOverrideReasonCode.REFUND_PRICE_DIFFERENCE;
                case "Web Return":
                    return BO.Model.Enums.PriceOverrideReasonCode.WEB_RETURN;
                case "Unknown":
                    return BO.Model.Enums.PriceOverrideReasonCode.UNKNOWN;
            }

            throw new ArgumentException(String.Format("Unknown Price Override Reason Code = {0}", priceOverrideCode));
        }
    }
}
