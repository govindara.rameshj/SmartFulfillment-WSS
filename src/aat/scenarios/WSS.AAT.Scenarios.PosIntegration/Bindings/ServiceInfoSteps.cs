﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.BOReceiverService.BL.ServiceInfoProcessing;
using WSS.BO.Model.Metainfo;

namespace WSS.AAT.Scenarios.PosIntegration.Bindings
{
    [Binding]
    class ServiceInfoSteps : BaseStepDefinitions
    {
        private readonly IBusinessLogicFacade businessLogicFacade;
        private ServiceInfo serviceInfo;

        public ServiceInfoSteps(IBusinessLogicFacade businessLogicFacade)
        {
            this.businessLogicFacade = businessLogicFacade;
        }

        [When(@"I request information about the service")]
        public void WhenIRequestInformationAboutTheService()
        {
            serviceInfo = businessLogicFacade.GetServiceInfo();
        }
        
        [Then(@"It returns that it serves the store with number (.*)")]
        public void ThenItReturnsThatItServesTheStoreWithNumber(string storeNumber)
        {
            Assert.AreEqual(storeNumber, serviceInfo.StoreId);
        }
        
        [Then(@"It returns that it serves the store with name ""(.*)""")]
        public void ThenItReturnsThatItServesTheStoreWithName(string storeName)
        {
            Assert.AreEqual(storeName, serviceInfo.StoreName);
        }
        
        [Then(@"It returns version of the store database")]
        public void ThenItReturnsVersionOfTheStoreDatabase()
        {
            Assert.IsNotNullOrEmpty(serviceInfo.StoreDatabaseVersion);
            Assert.AreNotEqual("DB is not available", serviceInfo.StoreDatabaseVersion);
        }

        [Then(@"It returns that the name of the service is ""(.*)""")]
        public void ThenItReturnsThatTheNameOfTheServiceIs(string serviceName)
        {
            Assert.AreEqual(serviceName, serviceInfo.ServiceName);
        }

        [Then(@"It returns that the version of the service is ""(.*)""")]
        public void ThenItReturnsThatTheVersionOfTheServiceIs(string serviceVersion)
        {
            Assert.AreEqual(serviceVersion, serviceInfo.ServiceVersion);
        }

        [Then(@"It returns current model version")]
        public void ThenItReturnsCurrentModelVersion()
        {
            Assert.AreEqual(ModelMetaInfo.Version, serviceInfo.ModelVersion);
        }
    }
}
