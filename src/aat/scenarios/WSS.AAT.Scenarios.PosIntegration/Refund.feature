﻿@PosIntegration
Feature: Refund
    In order to check refund functionality
    As a developer
    I should be able to process different types of refund scenarios

    Scenario: Save refund transaction to ledger
    Given Refund happened
    And 5 SKU 500300 were refunded with reason 'Parts Missing'
    And Was refunded by cash for 169.85
    And Customer name is Vasily
    When BO received the transaction
    Then There is refund in BO db
    And There is refunded line for 5 SKU 500300 with reason code '04'
    And There is refunded cash payment for 169.85
    And Customer info for Vasily is added to db
    And Transaction is marked as Online

    Scenario: Refund does not include information about refunded sale tran for refund without receipt
    Given Refund happened
    When BO received the transaction 
    Then Original transaction values are empty in customer table

    Scenario: Refund includes information about refunded sale tran for refund with receipt
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    When BO received the transaction
    Then Original transaction values are filled in customer table

    Scenario: Void refund transaction
    Given Refund happened
    And Transaction was voided
    And Till number was 1
    And 5 SKU 500300 were refunded
    When BO received the transaction
    Then There is refund in BO db
    And There is refunded line for 5 SKU 500300
    And There are no new payments in database
    And Till 1 virtual cashier cash balance changed by 0
    And Till 1 virtual cashier number of voids changed by 1

    Scenario: Refund includes information about multiple payments
    Given Refund happened
    And Was refunded by cash for 100
    And Was refunded by Visa credit card for 69.85 pounds
    And Till number was 1
    When BO received the transaction
    Then There is refunded cash payment for 100
    And There is refunded Visa card payment for 69.85
    And Total till 1 virtual cashier balance changed by -169.85
    And Till 1 virtual cashier balance for Visa credit card changed by -69.85
    And Till 1 virtual cashier balance for cash changed by -100
    And Information about tokens and hashes was saved

    Scenario: Refund includes information about lines reverted in transaction
    Given Refund happened
    And 1 SKU 500300 were refunded
    And 5 SKU 100006 were refunded
    And Stock for 100006 was 55
    And Line for SKU 100006 is reversed
    When BO received the transaction
    Then Lines for SKU 100006 are reversed
    And Amount of reversed lines for till virtual cashier increased by 1
    And Stock for 100006 is 55

    Scenario: Save partial refund transaction to ledger
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And 1 SKU 100006 sold with Source Item Id = '2'
    And BO received the transaction
    When Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And BO received the transaction
    Then There is refund in BO db
    And There is refunded line for 2 SKU 500300
    And There is no line for SKU 100006
    And There is 1 line in customer table for items

    Scenario: Save parked refund transaction to ledger
    Given Refund happened
    And 2 SKU 500300 were refunded
    And Transaction was parked
    When BO received the transaction
    Then There is transaction in db for parked refund
    And There is refunded line for 2 SKU 500300
    And There are no new payments in database
    And There is no new customer information in database

    Scenario: Refund includes information about transaction price override
    Given Refund happened
    And 2 SKU 100006 were refunded
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Refund Price Override
    When BO received the transaction
    Then There is refund in BO db
    And There is refunded line for 2 SKU 100006
    And Price override change price difference of SKU 100006 is 96.00 pounds
    And Price override code of SKU 100006 is Refund Price Override
    And Employee discount amount of SKU 100006 is 0.00 pounds

    Scenario: Refund includes information about transaction price override with not Refund Price Override code
    Given Refund happened
    And 2 SKU 100006 were refunded
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Damaged Goods
    When BO received the transaction
    Then There is refund in BO db
    And There is refunded line for 2 SKU 100006
    And Price override change price difference of SKU 100006 is 96.00 pounds
    And Price override code of SKU 100006 is Refund Price Override

    Scenario: Refund includes information about transaction Price Erosion
    Given Refund happened
    And 2 SKU 500300 were refunded
    And Price of SKU 500300 was 23.97
    And In db price of SKU 500300 was 33.97
    When BO received the transaction
    Then There is refund in BO db
    And There is refunded line for 2 SKU 500300
    And Price override change price difference of SKU 500300 is 10.00 pounds
    And Price override code of SKU 500300 is Refund Price Override

    Scenario: Refund includes information about transaction price override with several SKU
    Given Refund happened
    And 2 SKU 100006 were refunded
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Refund Price Override
    And 3 SKU 500300 were refunded
    And Price for SKU 500300 was overriden by 35.00 pounds with reason code Refund Price Override
    When BO received the transaction
    Then There is refund in BO db
    And There is refunded line for 2 SKU 100006
    And Price override change price difference of SKU 100006 is 96.00 pounds
    And Price override code of SKU 100006 is Refund Price Override
    And There is refunded line for 3 SKU 500300
    And Price override change price difference of SKU 500300 is -1.03 pounds
    And Price override code of SKU 500300 is Refund Price Override

    Scenario: Refund includes information about refunded sale tran for refund with receipt but missing sale transaction
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And BO received the transaction
    And Data base was cleaned up
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    When BO received the transaction
    Then Original transaction values are empty in customer table

    Scenario: Refund includes information about refunded sale with correct original line number
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And 3 SKU 100006 sold with Source Item Id = '2'
    And 1 SKU 100005 sold with Source Item Id = '3'
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And 1 SKU 100005 from the previous transaction were refunded with Refund Transaction Item Id = '3'
    When BO received the transaction
    Then Original transaction values are filled in customer table
    And Original Line Number in Customer Table for SKU 500300 is 1
    And Original Line Number in Customer Table for SKU 100005 is 3

    Scenario: Refund of Single Item from Package is saved with correct Initial Price
    Given Sale happened yesterday
    And 1 SKU 158057 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 100149 from the previous transaction were refunded with Refund Transaction Item Id = '1' and Price 3.75 pounds
    When BO received the transaction
    Then There is refund in BO db
    And There is line for SKU 100149 with price equal 3.75, amount equal -7.5, discount equal 0.00, value added tax equal 1.25
    And Price override change price difference of SKU 100149 is 0.00 pounds
    And Price override code of SKU 100149 is empty

    Scenario: Refund of Single Item from Package with Price Erosion
    Given Sale happened yesterday
    And 1 SKU 158057 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 100149 from the previous transaction were refunded with Refund Transaction Item Id = '1' and Price 3.8 pounds
    When BO received the transaction
    Then There is refund in BO db
    And There is line for SKU 100149 with price equal 3.8, amount equal -7.6, discount equal 0.00, value added tax equal 1.27
    And Price override change price difference of SKU 100149 is -0.05 pounds
    And Price override code of SKU 100149 is Refund Price Override

    Scenario: Refund of Single Item from Package with Price Override
    Given Sale happened yesterday
    And 1 SKU 158057 sold with Source Item Id = '1'
    And Price for SKU 158057 was overriden by 10.99 pounds
    And BO received the transaction
    And Refund happened
    And 2 SKU 100149 from the previous transaction were refunded with Refund Transaction Item Id = '1' and Price 3.75 pounds
    And Price for SKU 100149 was overriden by 2.75 pounds with reason code Refund Price Override
    When BO received the transaction
    Then There is refund in BO db
    And There is line for SKU 100149 with price equal 2.75, amount equal -5.5, discount equal 0.00, value added tax equal 0.92
    And Price override change price difference of SKU 100149 is 1.00 pounds
    And Price override code of SKU 100149 is Refund Price Override

    Scenario: Refund of Single Item from Package with Price Override and Collegue Discount
    Given Sale happened yesterday
    And 1 SKU 158057 sold with Source Item Id = '1'
    And Price for SKU 158057 was overriden by 10.99 pounds
    And Collegue Discount amount for SKUs 158057 is 2.00
    And BO received the transaction
    And Refund happened
    And 2 SKU 100149 from the previous transaction were refunded with Refund Transaction Item Id = '1' and Price 3.75 pounds
    And Price for SKU 100149 was overriden by 2.75 pounds with reason code Refund Price Override
    And Collegue Discount amount for SKUs 100149 is 0.5
    When BO received the transaction
    Then There is refund in BO db
    And There is line for SKU 100149 with price equal 2.5, amount equal -5.0, discount equal 0.25, value added tax equal 0.83
    And Price override change price difference of SKU 100149 is 1.00 pounds
    And Price override code of SKU 100149 is Refund Price Override

    Scenario: Refund of Single Item from Package is saved with correct Initial Price from Legacy Transaction
    Given Sale happened yesterday
    And 1 SKU 158057 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 100149 from the previous transaction were refunded with Refund Transaction Item Id = '1' and Price 3.75 pounds
    And Refunded Transaction Id of Refund Item of SKU 100149 is legacy
    When BO received the transaction
    Then There is refund in BO db
    And There is line for SKU 100149 with price equal 3.75, amount equal -7.5, discount equal 0.00, value added tax equal 1.25
    And Price override change price difference of SKU 100149 is 0.00 pounds
    And Price override code of SKU 100149 is empty

    Scenario: Refund of Single Item from Package with Price Erosion for Legacy Transaction
    Given Sale happened yesterday
    And 1 SKU 158057 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 100149 from the previous transaction were refunded with Refund Transaction Item Id = '1' and Price 3.8 pounds
    And Refunded Transaction Id of Refund Item of SKU 100149 is legacy
    When BO received the transaction
    Then There is refund in BO db
    And There is line for SKU 100149 with price equal 3.8, amount equal -7.6, discount equal 0.00, value added tax equal 1.27
    And Price override change price difference of SKU 100149 is -0.05 pounds
    And Price override code of SKU 100149 is Refund Price Override

    Scenario: Refund of Single Item from Package with Price Override for Legacy Transaction
    Given Sale happened yesterday
    And 1 SKU 158057 sold with Source Item Id = '1'
    And Price for SKU 158057 was overriden by 10.99 pounds
    And BO received the transaction
    And Refund happened
    And 2 SKU 100149 from the previous transaction were refunded with Refund Transaction Item Id = '1' and Price 3.75 pounds
    And Refunded Transaction Id of Refund Item of SKU 100149 is legacy
    And Price for SKU 100149 was overriden by 2.75 pounds with reason code Refund Price Override
    When BO received the transaction
    Then There is refund in BO db
    And There is line for SKU 100149 with price equal 2.75, amount equal -5.5, discount equal 0.00, value added tax equal 0.92
    And Price override change price difference of SKU 100149 is 1.00 pounds
    And Price override code of SKU 100149 is Refund Price Override

    Scenario: Refund of Single Item from Package with Price Override and Collegue Discount for Legacy Transaction
    Given Sale happened yesterday
    And 1 SKU 158057 sold with Source Item Id = '1'
    And Price for SKU 158057 was overriden by 10.99 pounds
    And Collegue Discount amount for SKUs 158057 is 2.00
    And BO received the transaction
    And Refund happened
    And 2 SKU 100149 from the previous transaction were refunded with Refund Transaction Item Id = '1' and Price 3.75 pounds
    And Refunded Transaction Id of Refund Item of SKU 100149 is legacy
    And Price for SKU 100149 was overriden by 2.75 pounds with reason code Refund Price Override
    And Collegue Discount amount for SKUs 100149 is 0.5
    When BO received the transaction
    Then There is refund in BO db
    And There is line for SKU 100149 with price equal 2.5, amount equal -5.0, discount equal 0.25, value added tax equal 0.83
    And Price override change price difference of SKU 100149 is 1.00 pounds
    And Price override code of SKU 100149 is Refund Price Override

    Scenario: Refund with receipt includes survey postcode information
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And Survey Postcode is 'QQ11QQ'
    When BO received the transaction
    Then There is refund in BO db
    And Original transaction values are filled in customer table
    And  There is Customer Line with the sale Date, Till Number, Transaction Number and Postcode 'QQ11QQ' and other fields with default values

    Scenario: Refund without receipt includes survey postcode information
    Given Refund happened
    And 5 SKU 500300 were refunded with reason 'Parts Missing'
    And Survey Postcode is 'QQ11QQ'
    When BO received the transaction
    Then There is refund in BO db
    And  There is Customer Line with the sale Date, Till Number, Transaction Number and Postcode 'QQ11QQ' and other fields with default values