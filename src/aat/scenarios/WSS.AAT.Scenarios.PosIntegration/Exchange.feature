﻿@PosIntegration
Feature: Exchange

    Scenario: Save Exchange sale transaction to ledger
    Given Sale happened
    And 1 SKU 100006 sold
    And 1 SKU 500300 were refunded
    And Customer name is Vasily
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 1 SKU 100006
    And There is refunded line for 1 SKU 500300
    And Customer info for Vasily is added to db
    And There is information about line 2 in customer table in db

    Scenario: Exchange sale updates cashier balance
    Given Sale happened
    And Till number was 1
    And 1 SKU 100006 sold
    And 1 SKU 500300 were refunded
    And Was paid by cash for 262.03 pounds
    When BO received the transaction
    Then Total till 1 virtual cashier balance changed by 262.03
    And Till 1 virtual cashier balance for cash changed by 262.03
    And Till 1 virtual cashier refund balance changed by 0
    And Till 1 virtual cashier sale balance changed by 262.03

    Scenario: Save Exchange refund transaction to ledger
    Given Refund happened
    And 1 SKU 500300 sold
    And 1 SKU 100006 were refunded
    And Customer name is Vasily
    When BO received the transaction
    Then There is refund in BO db
    And There is line with 1 sold SKU 500300
    And There is refunded line for 1 SKU 100006
    And Customer info for Vasily is added to db
    And There is information about line 2 in customer table in db

    Scenario: Exchange refund updates cashier balance
    Given Refund happened
    And 1 SKU 500300 sold
    And 1 SKU 100006 were refunded
    And Till number was 1
    And Was refunded by cash for 262.03
    When BO received the transaction
    Then Total till 1 virtual cashier balance changed by -262.03
    And Till 1 virtual cashier balance for cash changed by -262.03
    And Till 1 virtual cashier refund balance changed by -262.03
    And Till 1 virtual cashier sale balance changed by 0

    Scenario: Refund includes information about exchange transaction with correct original line number
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And 5 SKU 100006 sold
    And 3 SKU 100005 sold
    When BO received the transaction
    Then Original transaction values are filled in customer table
    And Original Line Number in Customer Table for SKU 500300 is 1

    Scenario: Exchange with items refunded with receipt includes survey postcode information
    Given Sale happened yesterday
    And 5 SKU 500300 sold with Source Item Id = '1'
    And BO received the transaction
    And Refund happened
    And 2 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And 2 SKU 100006 sold
    And Survey Postcode is 'QQ11QQ'
    When BO received the transaction
    Then There is sale in BO db
    And Original transaction values are filled in customer table
    And  There is Customer Line with the sale Date, Till Number, Transaction Number and Postcode 'QQ11QQ' and other fields with default values

    Scenario: Stock movement for exchange refund
    Given Refund happened
    And 1 SKU 500300 sold
    And 1 SKU 100006 were refunded
    And Stock for 500300 was 10
    And Stock for 100006 was 10
    When BO received the transaction
    Then Stock for 500300 is 9
    And Stock for 100006 is 11
    And Record is added to Stock Log for SKU 500300
    And Record is added to Stock Log for SKU 100006
    And Stock movement type for SKU 500300 is Normal stock sale
    And Stock movement type for SKU 100006 is Normal stock refund

    Scenario: Stock movement for exchange stock on hand
    Given Sale happened
    And 5 SKU 500300 sold
    And Stock for 500300 was 40
    And BO received the transaction
    And Sale happened
    And Stock for 500300 was 50
    And 5 SKU 500300 sold
    When BO received the transaction
    Then Stock for 500300 is 45
    And Record is added to Stock Log for SKU 500300
    And Stock movement type for SKU 500300 is System Adjustment 91

    Scenario: Stock movement for exchange sale
    Given Sale happened
    And 1 SKU 100006 sold
    And 1 SKU 500300 were refunded
    And Stock for 500300 was 10
    And Stock for 100006 was 10
    When BO received the transaction
    Then Stock for 500300 is 11
    And Stock for 100006 is 9
    And Record is added to Stock Log for SKU 500300
    And Record is added to Stock Log for SKU 100006
    And Stock movement type for SKU 500300 is Normal stock refund
    And Stock movement type for SKU 100006 is Normal stock sale

    Scenario: Exchange sale includes information about gift card sale or topup
    Given Sale happened
    And Till number was 1
    And A gift card was topped up with 100.00 pounds
    And 1 SKU 500300 were refunded
    And Was paid by cash for 66.03 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is a generated Refund
    And There is a generated Misc Income in BO db with reason code 12 and description 'Gift Card'
    And There is cash payment for 66.03 pounds
    And There is line with 1 Gift Card SKU for sale
    And There is information about line 2 in customer table in db

    Scenario: Exchange sale includes information about transaction Price Erosion
    Given Refund happened
    And 1 SKU 100006 sold
    And 1 SKU 500300 were refunded
    And Price of SKU 500300 was 23.97
    And In db price of SKU 500300 was 33.97
    When BO received the transaction
    Then There is sale in BO db
    And There is information about line 2 in customer table in db
    And There is refunded line for 1 SKU 500300
    And Price override change price difference of SKU 500300 is 10.00 pounds
    And Price override code of SKU 500300 is Refund Price Override

    Scenario: Positive Exchange sale includes information about transaction price override with several SKU
    Given Sale happened
    And 2 SKU 100006 sold
    And 3 SKU 500300 were refunded
    And Price for SKU 500300 was overriden by 30.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 2 SKU 100006
    And There is line with -3 SKU 500300
    And Price of SKU 500300 is 30.00 pounds
    And Price override change price difference of SKU 500300 is 3.97 pounds
    And Price override code of SKU 500300 is Refund Price Override

    Scenario: Negative Exchange sale includes information about transaction price override with several SKU
    Given Sale happened
    And 2 SKU 100006 were refunded
    And 3 SKU 500300 sold
    And Price for SKU 500300 was overriden by 30.00 pounds with reason code Damaged Goods
    When BO received the transaction
    Then There is refund in BO db
    And There is line with -2 SKU 100006
    And There is line with 3 SKU 500300
    And Price of SKU 500300 is 30.00 pounds
    And Price override change price difference of SKU 500300 is 3.97 pounds
    And Price override code of SKU 500300 is Damaged Goods

    Scenario: Positive exchange order with refunded lines
    Given Sale happened
    And Customer name is Vasily
    And 2 SKU 100006 sold
    And 3 SKU 500300 were refunded    
    And 2 items for line 1 were delivered
    When BO received the transaction
    Then There is an order in DB
    And There is line with 2 SKU 100006
    And There is line with -3 SKU 500300
    And There is information about line 2 in customer table in db

    Scenario: Negative exchange order with refunded lines
    Given Sale happened
    And Customer name is Vasily
    And 2 SKU 100006 were refunded 
    And 3 SKU 500300 sold  
    And 2 items for line 2 were delivered
    When BO received the transaction
    Then There is refund in BO db
    And There is line with -2 SKU 100006
    And There is line with 3 SKU 500300
    And There is information about line 1 in customer table in db

 Scenario: Negative exchange for order cancellation
    Given Sale happened yesterday
    And 10 SKU 500300 sold with line number 1 and source item id equal 1
    And 8 items for line 1 were collected instantly
    And 2 items for line 1 were collected later with ordering
    And BO received the transaction
    When Refund happened
    And 10 SKU 500300 were refunded with cancelled quantity = 2 corresponding to line with source item id 1 from last order
    And 1 SKU 100006 sold
    And BO received the transaction
    Then There is refund for order in BO db
    And There is refunded line for 10 SKU 500300
    And There is line with 1 SKU 100006
    And There is refunded cash payment for 43.7
    And Original transaction values are filled in customer table
    And There is information about order cancellation for line 1 in previous order with returned quantity 8 and cancelled quantity 2
    And Transaction is marked as Online

 Scenario: Positive exchange for order cancellation
    Given Sale happened yesterday
    And 3 SKU 500300 sold with line number 1 and source item id equal 1
    And 3 items for line 1 were collected later with ordering
    And BO received the transaction
    When Refund happened
    And 3 SKU 500300 were refunded with cancelled quantity = 3 corresponding to line with source item id 1 from last order
    And 1 SKU 100006 sold
    And BO received the transaction
    Then There is sale in BO db
    And There is refunded line for 3 SKU 500300
    And There is line with 1 SKU 100006
    And There is cash payment for 194.09 pounds
    And Original transaction values are filled in customer table
    And There is information about order cancellation for line 1 in previous order with returned quantity 0 and cancelled quantity 3
    And Transaction is marked as Online

