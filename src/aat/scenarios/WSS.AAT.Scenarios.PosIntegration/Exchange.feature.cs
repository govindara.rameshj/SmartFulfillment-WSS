﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.18444
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace WSS.AAT.Scenarios.PosIntegration
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Exchange")]
    [NUnit.Framework.CategoryAttribute("PosIntegration")]
    public partial class ExchangeFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "Exchange.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Exchange", "", ProgrammingLanguage.CSharp, new string[] {
                        "PosIntegration"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Save Exchange sale transaction to ledger")]
        public virtual void SaveExchangeSaleTransactionToLedger()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Save Exchange sale transaction to ledger", ((string[])(null)));
#line 4
    this.ScenarioSetup(scenarioInfo);
#line 5
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 6
    testRunner.And("1 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 7
    testRunner.And("1 SKU 500300 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 8
    testRunner.And("Customer name is Vasily", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 9
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 10
    testRunner.Then("There is sale in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 11
    testRunner.And("There is line with 1 SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 12
    testRunner.And("There is refunded line for 1 SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 13
    testRunner.And("Customer info for Vasily is added to db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 14
    testRunner.And("There is information about line 2 in customer table in db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Exchange sale updates cashier balance")]
        public virtual void ExchangeSaleUpdatesCashierBalance()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Exchange sale updates cashier balance", ((string[])(null)));
#line 16
    this.ScenarioSetup(scenarioInfo);
#line 17
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 18
    testRunner.And("Till number was 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 19
    testRunner.And("1 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 20
    testRunner.And("1 SKU 500300 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 21
    testRunner.And("Was paid by cash for 262.03 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 22
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 23
    testRunner.Then("Total till 1 virtual cashier balance changed by 262.03", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 24
    testRunner.And("Till 1 virtual cashier balance for cash changed by 262.03", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 25
    testRunner.And("Till 1 virtual cashier refund balance changed by 0", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 26
    testRunner.And("Till 1 virtual cashier sale balance changed by 262.03", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Save Exchange refund transaction to ledger")]
        public virtual void SaveExchangeRefundTransactionToLedger()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Save Exchange refund transaction to ledger", ((string[])(null)));
#line 28
    this.ScenarioSetup(scenarioInfo);
#line 29
    testRunner.Given("Refund happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 30
    testRunner.And("1 SKU 500300 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 31
    testRunner.And("1 SKU 100006 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 32
    testRunner.And("Customer name is Vasily", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 33
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 34
    testRunner.Then("There is refund in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 35
    testRunner.And("There is line with 1 sold SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 36
    testRunner.And("There is refunded line for 1 SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 37
    testRunner.And("Customer info for Vasily is added to db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 38
    testRunner.And("There is information about line 2 in customer table in db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Exchange refund updates cashier balance")]
        public virtual void ExchangeRefundUpdatesCashierBalance()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Exchange refund updates cashier balance", ((string[])(null)));
#line 40
    this.ScenarioSetup(scenarioInfo);
#line 41
    testRunner.Given("Refund happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 42
    testRunner.And("1 SKU 500300 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 43
    testRunner.And("1 SKU 100006 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 44
    testRunner.And("Till number was 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 45
    testRunner.And("Was refunded by cash for 262.03", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 46
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 47
    testRunner.Then("Total till 1 virtual cashier balance changed by -262.03", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 48
    testRunner.And("Till 1 virtual cashier balance for cash changed by -262.03", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 49
    testRunner.And("Till 1 virtual cashier refund balance changed by -262.03", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 50
    testRunner.And("Till 1 virtual cashier sale balance changed by 0", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Refund includes information about exchange transaction with correct original line" +
            " number")]
        public virtual void RefundIncludesInformationAboutExchangeTransactionWithCorrectOriginalLineNumber()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Refund includes information about exchange transaction with correct original line" +
                    " number", ((string[])(null)));
#line 52
    this.ScenarioSetup(scenarioInfo);
#line 53
    testRunner.Given("Sale happened yesterday", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 54
    testRunner.And("5 SKU 500300 sold with Source Item Id = \'1\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 55
    testRunner.And("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 56
    testRunner.And("Refund happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 57
    testRunner.And("2 SKU 500300 from the previous transaction were refunded with Refund Transaction " +
                    "Item Id = \'1\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 58
    testRunner.And("5 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 59
    testRunner.And("3 SKU 100005 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 60
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 61
    testRunner.Then("Original transaction values are filled in customer table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 62
    testRunner.And("Original Line Number in Customer Table for SKU 500300 is 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Exchange with items refunded with receipt includes survey postcode information")]
        public virtual void ExchangeWithItemsRefundedWithReceiptIncludesSurveyPostcodeInformation()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Exchange with items refunded with receipt includes survey postcode information", ((string[])(null)));
#line 64
    this.ScenarioSetup(scenarioInfo);
#line 65
    testRunner.Given("Sale happened yesterday", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 66
    testRunner.And("5 SKU 500300 sold with Source Item Id = \'1\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 67
    testRunner.And("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 68
    testRunner.And("Refund happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 69
    testRunner.And("2 SKU 500300 from the previous transaction were refunded with Refund Transaction " +
                    "Item Id = \'1\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 70
    testRunner.And("2 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 71
    testRunner.And("Survey Postcode is \'QQ11QQ\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 72
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 73
    testRunner.Then("There is sale in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 74
    testRunner.And("Original transaction values are filled in customer table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 75
    testRunner.And("There is Customer Line with the sale Date, Till Number, Transaction Number and Po" +
                    "stcode \'QQ11QQ\' and other fields with default values", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Stock movement for exchange refund")]
        public virtual void StockMovementForExchangeRefund()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Stock movement for exchange refund", ((string[])(null)));
#line 77
    this.ScenarioSetup(scenarioInfo);
#line 78
    testRunner.Given("Refund happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 79
    testRunner.And("1 SKU 500300 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 80
    testRunner.And("1 SKU 100006 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 81
    testRunner.And("Stock for 500300 was 10", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 82
    testRunner.And("Stock for 100006 was 10", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 83
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 84
    testRunner.Then("Stock for 500300 is 9", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 85
    testRunner.And("Stock for 100006 is 11", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 86
    testRunner.And("Record is added to Stock Log for SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 87
    testRunner.And("Record is added to Stock Log for SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 88
    testRunner.And("Stock movement type for SKU 500300 is Normal stock sale", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 89
    testRunner.And("Stock movement type for SKU 100006 is Normal stock refund", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Stock movement for exchange stock on hand")]
        public virtual void StockMovementForExchangeStockOnHand()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Stock movement for exchange stock on hand", ((string[])(null)));
#line 91
    this.ScenarioSetup(scenarioInfo);
#line 92
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 93
    testRunner.And("5 SKU 500300 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 94
    testRunner.And("Stock for 500300 was 40", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 95
    testRunner.And("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 96
    testRunner.And("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 97
    testRunner.And("Stock for 500300 was 50", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 98
    testRunner.And("5 SKU 500300 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 99
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 100
    testRunner.Then("Stock for 500300 is 45", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 101
    testRunner.And("Record is added to Stock Log for SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 102
    testRunner.And("Stock movement type for SKU 500300 is System Adjustment 91", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Stock movement for exchange sale")]
        public virtual void StockMovementForExchangeSale()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Stock movement for exchange sale", ((string[])(null)));
#line 104
    this.ScenarioSetup(scenarioInfo);
#line 105
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 106
    testRunner.And("1 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 107
    testRunner.And("1 SKU 500300 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 108
    testRunner.And("Stock for 500300 was 10", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 109
    testRunner.And("Stock for 100006 was 10", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 110
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 111
    testRunner.Then("Stock for 500300 is 11", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 112
    testRunner.And("Stock for 100006 is 9", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 113
    testRunner.And("Record is added to Stock Log for SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 114
    testRunner.And("Record is added to Stock Log for SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 115
    testRunner.And("Stock movement type for SKU 500300 is Normal stock refund", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 116
    testRunner.And("Stock movement type for SKU 100006 is Normal stock sale", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Exchange sale includes information about gift card sale or topup")]
        public virtual void ExchangeSaleIncludesInformationAboutGiftCardSaleOrTopup()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Exchange sale includes information about gift card sale or topup", ((string[])(null)));
#line 118
    this.ScenarioSetup(scenarioInfo);
#line 119
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 120
    testRunner.And("Till number was 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 121
    testRunner.And("A gift card was topped up with 100.00 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 122
    testRunner.And("1 SKU 500300 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 123
    testRunner.And("Was paid by cash for 66.03 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 124
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 125
    testRunner.Then("There is sale in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 126
    testRunner.And("There is a generated Refund", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 127
    testRunner.And("There is a generated Misc Income in BO db with reason code 12 and description \'Gi" +
                    "ft Card\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 128
    testRunner.And("There is cash payment for 66.03 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 129
    testRunner.And("There is line with 1 Gift Card SKU for sale", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 130
    testRunner.And("There is information about line 2 in customer table in db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Exchange sale includes information about transaction Price Erosion")]
        public virtual void ExchangeSaleIncludesInformationAboutTransactionPriceErosion()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Exchange sale includes information about transaction Price Erosion", ((string[])(null)));
#line 132
    this.ScenarioSetup(scenarioInfo);
#line 133
    testRunner.Given("Refund happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 134
    testRunner.And("1 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 135
    testRunner.And("1 SKU 500300 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 136
    testRunner.And("Price of SKU 500300 was 23.97", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 137
    testRunner.And("In db price of SKU 500300 was 33.97", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 138
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 139
    testRunner.Then("There is sale in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 140
    testRunner.And("There is information about line 2 in customer table in db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 141
    testRunner.And("There is refunded line for 1 SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 142
    testRunner.And("Price override change price difference of SKU 500300 is 10.00 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 143
    testRunner.And("Price override code of SKU 500300 is Refund Price Override", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Positive Exchange sale includes information about transaction price override with" +
            " several SKU")]
        public virtual void PositiveExchangeSaleIncludesInformationAboutTransactionPriceOverrideWithSeveralSKU()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Positive Exchange sale includes information about transaction price override with" +
                    " several SKU", ((string[])(null)));
#line 145
    this.ScenarioSetup(scenarioInfo);
#line 146
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 147
    testRunner.And("2 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 148
    testRunner.And("3 SKU 500300 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 149
    testRunner.And("Price for SKU 500300 was overriden by 30.00 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 150
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 151
    testRunner.Then("There is sale in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 152
    testRunner.And("There is line with 2 SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 153
    testRunner.And("There is line with -3 SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 154
    testRunner.And("Price of SKU 500300 is 30.00 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 155
    testRunner.And("Price override change price difference of SKU 500300 is 3.97 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 156
    testRunner.And("Price override code of SKU 500300 is Refund Price Override", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Negative Exchange sale includes information about transaction price override with" +
            " several SKU")]
        public virtual void NegativeExchangeSaleIncludesInformationAboutTransactionPriceOverrideWithSeveralSKU()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Negative Exchange sale includes information about transaction price override with" +
                    " several SKU", ((string[])(null)));
#line 158
    this.ScenarioSetup(scenarioInfo);
#line 159
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 160
    testRunner.And("2 SKU 100006 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 161
    testRunner.And("3 SKU 500300 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 162
    testRunner.And("Price for SKU 500300 was overriden by 30.00 pounds with reason code Damaged Goods" +
                    "", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 163
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 164
    testRunner.Then("There is refund in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 165
    testRunner.And("There is line with -2 SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 166
    testRunner.And("There is line with 3 SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 167
    testRunner.And("Price of SKU 500300 is 30.00 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 168
    testRunner.And("Price override change price difference of SKU 500300 is 3.97 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 169
    testRunner.And("Price override code of SKU 500300 is Damaged Goods", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Positive exchange order with refunded lines")]
        public virtual void PositiveExchangeOrderWithRefundedLines()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Positive exchange order with refunded lines", ((string[])(null)));
#line 171
    this.ScenarioSetup(scenarioInfo);
#line 172
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 173
    testRunner.And("Customer name is Vasily", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 174
    testRunner.And("2 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 175
    testRunner.And("3 SKU 500300 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 176
    testRunner.And("2 items for line 1 were delivered", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 177
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 178
    testRunner.Then("There is an order in DB", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 179
    testRunner.And("There is line with 2 SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 180
    testRunner.And("There is line with -3 SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 181
    testRunner.And("There is information about line 2 in customer table in db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Negative exchange order with refunded lines")]
        public virtual void NegativeExchangeOrderWithRefundedLines()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Negative exchange order with refunded lines", ((string[])(null)));
#line 183
    this.ScenarioSetup(scenarioInfo);
#line 184
    testRunner.Given("Sale happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 185
    testRunner.And("Customer name is Vasily", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 186
    testRunner.And("2 SKU 100006 were refunded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 187
    testRunner.And("3 SKU 500300 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 188
    testRunner.And("2 items for line 2 were delivered", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 189
    testRunner.When("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 190
    testRunner.Then("There is refund in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 191
    testRunner.And("There is line with -2 SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 192
    testRunner.And("There is line with 3 SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 193
    testRunner.And("There is information about line 1 in customer table in db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Negative exchange for order cancellation")]
        public virtual void NegativeExchangeForOrderCancellation()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Negative exchange for order cancellation", ((string[])(null)));
#line 195
 this.ScenarioSetup(scenarioInfo);
#line 196
    testRunner.Given("Sale happened yesterday", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 197
    testRunner.And("10 SKU 500300 sold with line number 1 and source item id equal 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 198
    testRunner.And("8 items for line 1 were collected instantly", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 199
    testRunner.And("2 items for line 1 were collected later with ordering", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 200
    testRunner.And("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 201
    testRunner.When("Refund happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 202
    testRunner.And("10 SKU 500300 were refunded with cancelled quantity = 2 corresponding to line wit" +
                    "h source item id 1 from last order", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 203
 testRunner.And("1 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 204
    testRunner.And("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 205
    testRunner.Then("There is refund for order in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 206
    testRunner.And("There is refunded line for 10 SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 207
 testRunner.And("There is line with 1 SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 208
    testRunner.And("There is refunded cash payment for 43.7", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 209
    testRunner.And("Original transaction values are filled in customer table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 210
    testRunner.And("There is information about order cancellation for line 1 in previous order with r" +
                    "eturned quantity 8 and cancelled quantity 2", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 211
    testRunner.And("Transaction is marked as Online", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Positive exchange for order cancellation")]
        public virtual void PositiveExchangeForOrderCancellation()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Positive exchange for order cancellation", ((string[])(null)));
#line 213
 this.ScenarioSetup(scenarioInfo);
#line 214
    testRunner.Given("Sale happened yesterday", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 215
    testRunner.And("3 SKU 500300 sold with line number 1 and source item id equal 1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 216
    testRunner.And("3 items for line 1 were collected later with ordering", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 217
    testRunner.And("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 218
    testRunner.When("Refund happened", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 219
    testRunner.And("3 SKU 500300 were refunded with cancelled quantity = 3 corresponding to line with" +
                    " source item id 1 from last order", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 220
 testRunner.And("1 SKU 100006 sold", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 221
    testRunner.And("BO received the transaction", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 222
    testRunner.Then("There is sale in BO db", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 223
    testRunner.And("There is refunded line for 3 SKU 500300", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 224
 testRunner.And("There is line with 1 SKU 100006", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 225
    testRunner.And("There is cash payment for 194.09 pounds", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 226
    testRunner.And("Original transaction values are filled in customer table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 227
    testRunner.And("There is information about order cancellation for line 1 in previous order with r" +
                    "eturned quantity 0 and cancelled quantity 3", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 228
    testRunner.And("Transaction is marked as Online", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
