using BoDi;
using Cts.Oasys.Core.Tests;
using WSS.AAT.Common.PosIntegration.Configuration;
using WSS.AAT.Common.Utility.Configuration;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.Common.Hosting;

namespace WSS.AAT.Scenarios.PosIntegration
{
    class SetUp : BaseSetUp
    {
        protected override void InitializeObjectContainer(IObjectContainer container)
        {
            base.InitializeObjectContainer(container);
            BindObjectContainerTypeFromKernel<IBusinessLogicFacade>(container);
            BindObjectContainerTypeFromKernel<IConfiguration>(container);
        }

        protected override void InitializeTestEnvironmentSettings(TestEnvironmentSettings settings)
        {
            base.InitializeTestEnvironmentSettings(settings); 
            settings.CommsDirectoryPath = TestConfiguration.ResolvePathFromAppSettings("commsDir");
        }

        protected override IDependencyInjectionConfig GetDependencyInjectionConfig()
        {
            return new DependencyInjectionConfig();
        }
    }
}
