﻿@PosIntegration
Feature: Sale
    In order to test sale transaction processing
    As a developer
    I should check different transaction scenarios to be processed correctly.

Scenario: Save sale to ledger
    Given Sale happened
    And 5 SKU 500300 sold
    When BO received the transaction
    Then There is sale in BO db
    And Merchandice amount equal 169.85
    And Non merchandice amount equal 0
    And Total amount equal 169.85
    And There is line with 5 SKU 500300

Scenario: Sale includes information about cash payment
    Given Sale happened
    And Was paid by cash for 169.85 pounds
    When BO received the transaction
    Then There is cash payment for 169.85 pounds
    
Scenario: Save parked sale transaction to ledger
    Given Sale happened
    And Transaction was parked
    And Customer name is Vasily
    When BO received the transaction
    Then There is parked sale in BO db
    And Customer info for Vasily is added to db

Scenario: Sale includes information about card payment with null Issue Number Switch
    Given Sale happened
    And Was paid by Visa credit card for 169.85 pounds
    And The Card Payment has null Issue Number Switch
    When BO received the transaction
    Then There is Visa card payment for 169.85 pounds
    And Information about tokens and hashes was saved
    And In db Issue Number Switch for card payment is empty

Scenario: Sale inits new trading period
    Given Sale happened
    And In safe there is only one line for three days earlier date
    When BO received the transaction
    Then There are lines in safe for new trading period 

Scenario: Saves flag if transaction is voided
    Given Sale happened
    And 5 SKU 500300 sold
    And Transaction was voided
    And Till number was 1
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 5 SKU 500300
    And Till 1 virtual cashier cash balance changed by 0
    And Till 1 virtual cashier number of voids changed by 1

Scenario: Saves info about applied VATs
    Given Sale happened
    And 5 SKU 500300 was sold with VAT rate 20
    And 2 SKU 100006 was sold with VAT rate 20
    And 1 SKU 100004 was sold with VAT rate 5
    And 1 SKU 100005 was sold with VAT rate 15
    When BO received the transaction
    Then VAT value of line for SKU 500300 is equal to 28.31
    Then VAT value of line for SKU 100006 is equal to 98.67
    Then VAT value of line for SKU 100004 is equal to 0.38
    Then VAT value of line for SKU 100005 is equal to 70.30
    And Total VAT value for VAT rate of 20 persents is equal to 126.98
    And Total VAT value for VAT rate of 5 persents is equal to 0.38
    And Total VAT value for VAT rate of 15 persents is equal to 70.30

    Scenario: Sale includes information about cheque payment
    Given Sale happened
    And Was paid by cheque for 169.85 pounds
    When BO received the transaction
    Then There is cheque payment for 169.85 pounds

    Scenario: Sale includes information about gift card payment
    Given Sale happened
    And Was paid by GiftCard for 169.85 pounds
    When BO received the transaction
    Then There is GiftCard payment for 169.85 pounds
    And Gift card amount in db is 169.85
    And Information about tokens and hashes was saved
    And There is information about GiftCard in DB

    Scenario: Sale includes information about lines reverted in transaction
    Given Sale happened
    And 5 SKU 500300 sold
    And 1 SKU 100006 sold
    And Stock for 500300 was 55
    And Line for SKU 500300 is reversed
    When BO received the transaction
    Then Lines for SKU 500300 are reversed with reason code ''
    And Amount of reversed lines for till virtual cashier increased by 1
    And Stock for 500300 is 55

    Scenario: Sale includes information about multiple lines reverted in transaction
    Given Sale happened
    And 1 SKU 500300 sold
    And 1 SKU 100005 sold
    And 1 SKU 100006 sold
    And Line for SKU 500300 is reversed
    And Line for SKU 100006 is reversed
    When BO received the transaction
    Then Amount of reversed lines for till virtual cashier increased by 2

    Scenario:Sale includes information about multiple payments
    Given Sale happened
    And Was paid by cash for 150 pounds
    And Was paid by Visa credit card for 19.85 pounds
    And Till number was 1
    When BO received the transaction
    Then There is cash payment for 150 pounds
    And There is Visa card payment for 19.85 pounds
    And Total till 1 virtual cashier balance changed by 169.85
    And Till 1 virtual cashier balance for Visa credit card changed by 19.85
    And Till 1 virtual cashier balance for cash changed by 150
    And Information about tokens and hashes was saved

    Scenario:Sale included information about line quantity
    Given Sale happened
    And 5 SKU 500300 sold
    When BO received the transaction
    Then There is line with 5 SKU 500300

    Scenario:Sale included information about multiple lines
    Given Sale happened
    And 5 SKU 500300 sold
    And 2 SKU 100006 sold
    When BO received the transaction
    Then There is line with 5 SKU 500300
    Then There is line with 2 SKU 100006

    Scenario: Sale includes information about cash change
    Given Sale happened
    And Was paid by cash for 200 pounds
    And Cash change was executed for 30.15 pounds
    When BO received the transaction
    Then There is cash payment for 200 pounds
    And There is cash change for 30.15 pounds

    Scenario: Stklog date of movement is always now
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    When BO received the transaction
    Then Stklog date of movement for SKU 500300 is equal today

    Scenario: Save sale transaction which was recalled from parked to ledger
    Given Sale happened
    And Transaction was parked
    And Customer name is Vasily
    And BO received the transaction
    When The previous parked transaction was recalled and completed
    And BO received the transaction
    Then There is sale in BO db
    And Parked transaction is stated as recalled
    
    Scenario: Sale includes information about applied colleague discount
    Given Sale happened
    And 1 SKU 500300 sold
    And Collegue Discount amount for SKUs 500300 is 6.79
    And Was paid by cash for 27.18 pounds
    When BO received the transaction
    Then Where is sale in BO db with total amount equal 27.18, discount equal 6.79, tax amount equal 4.53, value added tax equal 4.53
    And There is information about employee card number in BO db
    And There is line for SKU 500300 with price equal 27.18, amount equal 27.18, discount equal 6.79, value added tax equal 4.53

    Scenario: Sale includes information about colleague discount applied to several SKUs
    Given Sale happened
    And 1 SKU 500300 sold
    And 1 SKU 100006 sold
    And Collegue Discount amount for SKUs 500300 is 6.79
    And Collegue Discount amount for SKUs 100006 is 59.2
    And Was paid by cash for 263.98 pounds
    When BO received the transaction
    Then Where is sale in BO db with total amount equal 263.98, discount equal 65.99, tax amount equal 44.00, value added tax equal 44.00
    And There is information about employee card number in BO db
    And There is line for SKU 500300 with price equal 27.18, amount equal 27.18, discount equal 6.79, value added tax equal 4.53
    And There is line for SKU 100006 with price equal 236.80, amount equal 236.80, discount equal 59.2, value added tax equal 39.47

    Scenario: Sale includes information about colleague discount applied to only one of several SKUs
    Given Sale happened
    And 1 SKU 500300 sold
    And 1 SKU 100006 sold
    And Collegue Discount amount for SKUs 500300 is 6.79
    And Was paid by cash for 293.18 pounds
    When BO received the transaction
    Then Where is sale in BO db with total amount equal 323.18, discount equal 6.79, tax amount equal 53.86, value added tax equal 53.86
    And There is information about employee card number in BO db
    And There is line for SKU 500300 with price equal 27.18, amount equal 27.18, discount equal 6.79, value added tax equal 4.53
    And There is line for SKU 100006 with price equal 296.00, amount equal 296.00, discount equal 0.00, value added tax equal 49.33

    Scenario: Sale includes information about transaction with price override
    Given Sale happened
    And 2 SKU 100006 sold
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Managers Discretion by Supervisor '033'
    And Was paid by cash for 400.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 2 SKU 100006
    And Price of SKU 100006 is 200.00 pounds
    And Price override change price difference of SKU 100006 is 96.00 pounds
    And Price override code of SKU 100006 is Managers Discretion
    And Employee discount amount of SKU 100006 is 0.00 pounds

    Scenario: Sale includes information about transaction with several price overrides
    Given Sale happened
    And 2 SKU 100006 sold
    And Price Override with code Manager Discretion with Amount 60.00 was applied to SKU 100006
    And Price Override with code Damaged Goods with Amount 30.00 was applied to SKU 100006
    And Was paid by cash for 502.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 2 SKU 100006
    And Price of SKU 100006 is 251.00 pounds
    And Price override change price difference of SKU 100006 is 45.00 pounds
    And Price override code of SKU 100006 is Damaged Goods

    Scenario: Sale includes information about transaction price override with several SKU
    Given Sale happened
    And 2 SKU 100006 sold
    And Price for SKU 100006 was overriden by 200.00 pounds
    And 3 SKU 500300 sold
    And Price for SKU 500300 was overriden by 30.00 pounds
    And Was paid by cash for 490.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 2 SKU 100006
    And Price of SKU 100006 is 200.00 pounds
    And Price override change price difference of SKU 100006 is 96.00 pounds
    And There is line with 3 SKU 500300
    And Price of SKU 500300 is 30.00 pounds
    And Price override change price difference of SKU 500300 is 3.97 pounds

    Scenario: Sale includes information about transaction price override with indivisible remainder
    Given Sale happened
    And 3 SKU 100006 sold
    And Price Override with amount 200 was applied to SKU 100006
    And Was paid by cash for 688.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 3 SKU 100006
    And Price of SKU 100006 is 229.34 pounds
    And Price override change price difference of SKU 100006 is 66.66 pounds
    And There is a 'HierarchySpend' event discount with amount 0.02 and code from database for SKU '100006'

    Scenario: Sale includes information about transaction price override with indivisible remainder that is complement by Employee Discount
    Given Sale happened
    And 3 SKU 100006 sold
    And Price Override with amount 200 was applied to SKU 100006
    And Collegue Discount amount for SKUs 100006 is 221.02
    And Was paid by cash for 688.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 3 SKU 100006
    And There is line for SKU 100006 with price equal 155.94, amount equal 467.83, discount equal 73.67, value added tax equal 77.97
    And Price override change price difference of SKU 100006 is 66.67 pounds
    And There is no 'HierarchySpend' event discount for SKU 100006

    Scenario: Markdown stock sale
    Given Sale happened
    And 3 SKU 100006 sold
    And Price Override with code HDC Return with Amount 60.00 was applied to SKU 100006
    And SKU 100006 was marked as from Markdown stock
    And Was paid by cash for 708.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 3 SKU 100006
    And Price of SKU 100006 is 276.00 pounds
    And Price override change price difference of SKU 100006 is 20.00 pounds
    And Price override code of SKU 100006 is 53
    And Line for SKU 100006 is from markdown stock

    Scenario: Markdown stock sale without any Price Override Discount
    Given Sale happened
    And 3 SKU 100006 sold
    And Price of SKU 100006 was 276.00
    And SKU 100006 was marked as from Markdown stock
    And Was paid by cash for 708.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 3 SKU 100006
    And Price of SKU 100006 is 276.00 pounds
    And Price override change price difference of SKU 100006 is 20.00 pounds
    And Price override code of SKU 100006 is 53
    And Line for SKU 100006 is from markdown stock

    Scenario: Markdown stock sale with Price Match discount instead of Price Override
    Given Sale happened
    And 3 SKU 100006 sold
    And Price for SKU 100006 was overriden by Supervisor '055' by 276.00 pounds with reason code Price Match or Promise and Competitor Name 'Jewson'
    And SKU 100006 was marked as from Markdown stock
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 3 SKU 100006
    And Price of SKU 100006 is 276.00 pounds
    And Price override change price difference of SKU 100006 is 20.00 pounds
    And Price override code of SKU 100006 is 53
    And Line for SKU 100006 is from markdown stock

    Scenario: Sale includes information about several Collegue Discounts
    Given Sale happened
    And 1 SKU 100006 sold
    And Employee '022' discount amount for SKU 100006 was 30
    And Employee '033' discount amount for SKU 100006 was 29.2
    And Was paid by cash for 798.00 pounds
    When BO received the transaction
    Then There is sale in BO db
    And Transaction is marked as Supervisor Used
    And There is line with 1 SKU 100006
    And There is line for SKU 100006 with price equal 236.8, amount equal 236.8, discount equal 59.2, value added tax equal 39.47
    And Supervisor Number for the Transaction is '033'

    Scenario: Sale includes information about several gift card payments
    Given Sale happened
    And Was paid by GiftCard for 100.00 pounds
    And Was paid by GiftCard for 69.85 pounds
    When BO received the transaction
    Then There is information about GiftCard with 100.00 pounds payment in DB under sequence number 1 
    Then There is information about GiftCard with 69.85 pounds payment in DB under sequence number 2 

    Scenario: Sale includes information about transaction Price Match or Promise price override
    Given Sale happened
    And 2 SKU 100006 sold
    And Price for SKU 100006 was overriden by Supervisor '055' by 200.00 pounds with reason code Price Match or Promise and Competitor Name 'Jewson'
    And Price Override with amount 30 was applied to SKU 100006
    When BO received the transaction
    Then  Price Override Code for SKU 100006 is Price Match code
    And Transaction is marked as Supervisor Used
    And There is a Price Match or Promise line item information with Competitor Name 'Jewson'

Scenario: Sale includes information about transaction Price Match or Promise price override for several products
    Given Sale happened
    And 2 SKU 100006 sold
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Price Match or Promise and Competitor Name 'Jewson'
    And 3 SKU 500300 sold
    And Price for SKU 500300 was overriden by 30.00 pounds with reason code Price Match or Promise and Competitor Name 'Amazon'
    And Price Override with amount 30 was applied to SKU 100006
    When BO received the transaction
    Then Price Override Code for SKU 100006 is Price Match code
    And Price Override Code for SKU 500300 is Price Match code
    And There is a Price Match or Promise line item information with Competitor Name 'Jewson' for product line 1
    And There is a Price Match or Promise line item information with Competitor Name 'Amazon' for product line 2

    Scenario: Sale includes information about transaction Price Match or Promise price override and simple Price Override
    Given Sale happened
    And 2 SKU 100006 sold
    And Price for SKU 100006 was overriden by 200.00 pounds with reason code Price Match or Promise and Competitor Name 'Jewson'
    And Price Override with amount 30 was applied to SKU 100006
    When BO received the transaction
    Then Price override change price difference of SKU 100006 is 111.00 pounds
    And Price Override Code for SKU 100006 is Price Match code
    And There is a Price Match or Promise line item information with Competitor Name 'Jewson'

    Scenario: Sale includes information about positive price erosion
    Given Sale happened
    And 1 SKU 500300 sold
    And Price of SKU 500300 was 23.97
    And In db price of SKU 500300 was 33.97
    When BO received the transaction
    Then Price of SKU 500300 is 23.97 pounds
    And Initial price of SKU 500300 is equal to db price
    And Price override change price difference of SKU 500300 is 10.00 pounds
    And Price override code of SKU 500300 is Temporary Deal Group

    Scenario: Sale includes information about negative price erosion
    Given Sale happened
    And 1 SKU 500300 sold
    And Price of SKU 500300 was 43.97
    And In db price of SKU 500300 was 33.97
    When BO received the transaction
    Then There is sale in BO db
    And Price of SKU 500300 is 43.97 pounds
    And Initial price of SKU 500300 is equal to db price
    And Price override change price difference of SKU 500300 is -10.00 pounds
    And Price override code of SKU 500300 is Temporary Deal Group

   Scenario: Sale includes information about transaction price override and price erosion
    Given Sale happened
    And 1 SKU 500300 sold
    And Price of SKU 500300 was 28.97
    And In db price of SKU 500300 was 33.97
    And Price for SKU 500300 was overriden by 23.97 pounds with reason code Damaged Goods
    When BO received the transaction
    Then There is sale in BO db
    And Price of SKU 500300 is 23.97 pounds
    And Initial price of SKU 500300 is equal to db price
    And Price override change price difference of SKU 500300 is 10.00 pounds
    And Price override code of SKU 500300 is Damaged Goods

    Scenario: Sale includes information about equal transaction price override and price erosion
    Given Sale happened
    And 1 SKU 500300 sold
    And Price of SKU 500300 was 43.97
    And Price for SKU 500300 was overriden by 33.97 pounds
    And In db price of SKU 500300 was 33.97
    When BO received the transaction
    Then There is sale in BO db
    And Price of SKU 500300 is 33.97 pounds
    And Initial price of SKU 500300 is equal to db price
    And Price override change price difference of SKU 500300 is 0.00 pounds
    And Price override code of SKU 500300 is empty

    Scenario: Validation error for too long customer name
    Given Sale happened
    And Customer name is VasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololo
    And Truncator is turned off
    When BO received the transaction
    Then Validation exception has been thrown

    Scenario: Validation error for too long delivery instructions
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And Delivery Instruction was "VasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololo"
    And Truncator is turned off
    When BO received the transaction
    Then Validation exception has been thrown

    Scenario: Too long customer name is truncated
    Given Sale happened
    And Customer name is VasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololo
    And Truncator is turned on
    When BO received the transaction
    Then Customer info for VasiliyIvanovTooLongNameRequir is added to db

    Scenario: Too long delivery instruction is truncated
    Given Sale happened
    And 5 SKU 500300 sold
    And 3 items for line 1 were collected instantly
    And 2 items for line 1 were delivered
    And Delivery Instruction was "VasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololo"
    And Truncator is turned on
    When BO received the transaction
    Then There is a Delivery Instruction "VasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooLongNameRequiredVavavavaKurlikKurlikTrolololoVasiliyIvanovTooL"

    Scenario: Sale is saved to ledger with Online status
    Given Sale happened
    And 5 SKU 500300 sold
    When BO received the transaction
    Then There is sale in BO db
    And Transaction is marked as Online

    Scenario: Sale includes survey postcode information
    Given Sale happened
    And 5 SKU 500300 sold
    And Survey Postcode is 'QQ11QQ'
    When BO received the transaction
    Then There is sale in BO db
    And There is line with 5 SKU 500300
    And  There is Customer Line with the sale Date, Till Number, Transaction Number and Postcode 'QQ11QQ' and other fields with default values

    Scenario:  Sale with all reversed refunded lines and filled customer
    Given Sale happened
    And Customer name is Vasiliy Ivanov
    And 5 SKU 500300 sold
    And 5 SKU 100006 were refunded with reason 'Parts Missing'
    And Line for SKU 100006 is reversed
    When BO received the transaction
    Then There is sale in BO db
    And There is no new customer information in database

    Scenario: Sale includes information about Manager
    Given Sale happened
    And 5 SKU 500300 sold
    And Manager id was '033'
    When BO received the transaction
    Then There is sale in BO db
    And Transaction is marked as Supervisor Used
