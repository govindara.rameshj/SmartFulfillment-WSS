﻿@PosIntegration
Feature: StockMovement
    In order to check if stock movement changes
    As a developer
    I should write scenarios for stock movement

    Scenario: Stock movement for sale
    Given Sale happened
    And 5 SKU 500300 sold
    And Stock for 500300 was 40
    When BO received the transaction
    Then Stock for 500300 is 35
    And Stock movement type for SKU 500300 is Normal stock sale
    And Record is added to Stock Log for SKU 500300
    And User is added to Stock Log for SKU 500300

    Scenario: Stock movement for refund
    Given Refund happened
    And 5 SKU 500300 were refunded
    And Stock for 500300 was 40
    When BO received the transaction
    Then Stock for 500300 is 45
    And Stock movement type for SKU 500300 is Normal stock refund
    And Record is added to Stock Log for SKU 500300

    Scenario: Stock movement for partial refund
    Given Sale happened yesterday
    And 5 SKU 500300 sold
    And 1 SKU 100006 sold
    And BO received the transaction
    When Refund happened
    And 5 SKU 500300 from the previous transaction were refunded with Refund Transaction Item Id = '1'
    And Stock for 500300 was 10
    And Stock for 100006 was 10
    And BO received the transaction
    Then Stock for 500300 is 15
    And Stock for 100006 is 10
    And Record is added to Stock Log for SKU 500300
    And There are no new Normal stock refund records for SKU 100006 in Stock Log

    Scenario: Stock movement for markdown
    Given Sale happened
    And 3 SKU 100006 sold
    And Price Override with code HDC Return with Amount 60.00 was applied to SKU 100006
    And SKU 100006 was marked as from Markdown stock
    And Was paid by cash for 708.00 pounds
    And Stock for 100006 was 10
    And Markdown stock for 100006 was 5
    And Sales Units values were equal to 0 for SKU 100006	
    And Sales Values were equal to 0 for SKU 100006
    And Adjustments Value is 0 for SKU 100006
    When BO received the transaction
    Then There is sale in BO db
    And Stock for 100006 is 10
    And Markdown Stock for 100006 is 2
    And Sales Units values are equal to 3 for SKU 100006
    And Sales Values are equal to 828.00 for SKU 100006
    And Adjustments value is equal to 3 for SKU 100006
    And Record is added to Stock Log for SKU 100006
    And Stock Log type is 04 for SKU 100006
    And In Stock Log for SKU 100006 Starting Markdown Quantity is 5 and Ending Markdown Quantity is 2
