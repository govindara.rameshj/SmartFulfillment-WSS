﻿@PosIntegration
Feature: RestrictedItems

Scenario: Sale includes information about an Age Check Restricted Item with Accepted Completely Accept Type
    Given Sale happened
    And 5 SKU 500300 sold
    And There was Accepted Completely Restricted Item with SKU 500300 and Restriction Type 'Age Check' and Authorizer Number '033'
    When BO received the transaction
    Then There is a new sale in BO db marked as with Supervisor Used
    And Supervisor Number of line for SKU 500300 is equal to '033'
    And There is no lines in Rejected Items file

Scenario: Sale includes information about an Age Check Restricted Item with Accepted With Confirmation Accept Type
    Given Sale happened
    And 5 SKU 500300 sold
    And There was Accepted with Confirmation Restricted Item with SKU 500300 and Restriction Type 'Age Check' and Authorizer Number '033'
    And Restricted Item with SKU 500300 had Confirmation Text 'Photographic Driving Licence'
    When BO received the transaction
    Then There is a new sale in BO db marked as with Supervisor Used
    And Supervisor Number of line for SKU 500300 is equal to '033'
    And There is a line in Rejected Items file for SKU 500300 with Authorizer Number '033' and Confirmation Text 'Photographic Driving Licence'

Scenario: Sale includes information about an Age Check Restricted Item with Rejected Completely Accept Type
    Given Sale happened
    And 5 SKU 500300 sold
    And There was Rejected Completely Restricted Item with SKU 100006 and Restriction Type 'Age Check' and Authorizer Number '033'
    When BO received the transaction
    Then There is a new sale in BO db marked as with Supervisor Used
    And There is a line in Rejected Items file for SKU 100006 with Authorizer Number '033' and Confirmation Text 'Item cannot be sold to Customer.Failed Solvent Age Requirement.'

Scenario: Sale includes information about an Age Check Restricted Item with Rejected not Confirmed Accept Type
    Given Sale happened
    And 5 SKU 500300 sold
    And There was Rejected not Confirmed Restricted Item with SKU 100006 and Restriction Type 'Age Check' and Authorizer Number '033'
    When BO received the transaction
    Then There is a new sale in BO db marked as with Supervisor Used
    And There is a line in Rejected Items file for SKU 100006 with Authorizer Number '033' and Confirmation Text 'Item cannot be sold to Customer.Failed to provide Proof of Age.'

Scenario: Voided sale includes information only about an Age Check Rejected Restricted Item
    Given Sale happened
    And There was Rejected Completely Restricted Item with SKU 100006 and Restriction Type 'Age Check' and Authorizer Number '033'
    And Transaction was voided
    When BO received the transaction
    Then There is a new sale in BO db marked as with Supervisor Used
    And There is a line in Rejected Items file for SKU 100006 with Authorizer Number '033' and Confirmation Text 'Item cannot be sold to Customer.Failed Solvent Age Requirement.'

Scenario: Sale includes information about a Quarantine Restricted Item with Accepted Completely Accept Type
    Given Sale happened
    And 5 SKU 500300 sold
    And There was Accepted Completely Restricted Item with SKU 500300 and Restriction Type 'Quarantine' and Authorizer Number '033'
    When BO received the transaction
    Then There is no lines in Rejected Items file
    And There is a new sale in BO db marked as with Supervisor Used
    And Quarantine Supervisor Number of line for SKU 500300 is equal to '033'

Scenario: Sale includes information about an Explosives Poisons Restricted Item with Accepted Completely Accept Type
    Given Sale happened
    And 5 SKU 500300 sold
    And There was Accepted Completely Restricted Item with SKU 500300 and Restriction Type 'Explosives Poisons' and Authorizer Number '033'
    When BO received the transaction
    Then There is no lines in Rejected Items file
    And There is a new sale in BO db marked as with Supervisor Used
    And Supervisor Number of line for SKU 500300 is equal to '033'
    And A line for SKU 500300 is marked as Explosive

Scenario: Sale includes information about several Explosives Poisons Restricted Items with Accepted Completely Accept Type
    Given Sale happened
    And 5 SKU 500300 sold
    And 1 SKU 500300 sold
    And 2 SKU 100006 sold
    And There was Accepted Completely Restricted Item with SKU 500300 and Restriction Type 'Explosives Poisons' and Authorizer Number '033'
    And There was Accepted Completely Restricted Item with SKU 100006 and Restriction Type 'Explosives Poisons' and Authorizer Number '033'
    When BO received the transaction
    Then There is no lines in Rejected Items file
    And There is a new sale in BO db marked as with Supervisor Used
    And Supervisor Number of line for SKU 500300 is equal to '033'
    And Supervisor Number of line for SKU 100006 is equal to '033'
    And Lines for SKU 500300 are marked as Explosive
    And A line for SKU 100006 is marked as Explosive

Scenario: Sale includes information about an Explosives Poisons Restricted Item with Accepted Completely Accept Type and a Price Override
    Given Sale happened
    And 5 SKU 500300 sold
    And Price for SKU 500300 was overriden by 30 pounds by Supervisor '055'
    And There was Accepted Completely Restricted Item with SKU 500300 and Restriction Type 'Explosives Poisons' and Authorizer Number '033'
    When BO received the transaction
    Then There is no lines in Rejected Items file
    And There is a new sale in BO db marked as with Supervisor Used
    And Supervisor Number of line for SKU 500300 is equal to '033'
    And A line for SKU 500300 is marked as Explosive

Scenario: Sale includes information about an Explosives Poisons Restricted Item with Accepted Completely Accept Type and Empty Supervisor and a Price Override
    Given Sale happened
    And 5 SKU 500300 sold
    And Price for SKU 500300 was overriden by 30 pounds by Supervisor '055'
    And There was not Supervisor authorized Accepted Completely Restricted Item with SKU 500300 and Restriction Type 'Explosives Poisons'
    When BO received the transaction
    Then There is no lines in Rejected Items file
    And Supervisor Number of line for SKU 500300 is equal to '055'
    And A line for SKU 500300 is marked as Explosive

Scenario: Sale includes information about several Age Check Restricted Items with Accepted With Confirmation Accept Type
    Given Sale happened
    And 5 SKU 500300 sold
    And There was Accepted with Confirmation Restricted Item with SKU 500300 and Restriction Type 'Age Check' and Authorizer Number '033'
    And Restricted Item with SKU 500300 had Confirmation Text 'Photographic Driving Licence'
    And There was Rejected Completely Restricted Item with SKU 100006 and Restriction Type 'Age Check' and Authorizer Number '033'
    When BO received the transaction
    Then There is a new sale in BO db marked as with Supervisor Used
    And There is a line in Rejected Items file for SKU 500300 with Sequence Number 1
    And There is a line in Rejected Items file for SKU 100006 with Sequence Number 2
