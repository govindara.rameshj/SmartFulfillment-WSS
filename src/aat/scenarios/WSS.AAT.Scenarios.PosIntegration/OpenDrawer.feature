﻿@PosIntegration
Feature: OpenDrawer
    In order to check cashbox operations
    As a developer
    I write test scenarios for open drawer transaction

Scenario: Save Open drawer event as transaction to ledger
    Given OpenDrawer event happened
    When BO received the OpenDrawer transaction
    Then There is OpenDrawer transaction in BO db
    And Amount of OpenDrawer transactions for till virtual cashier changed by 1

Scenario: Save correct reason code for open drawer event
    Given OpenDrawer event happened
    When BO received the OpenDrawer transaction
    Then Reason code for the transaction in ledger is 01
	And Description for the transaction in ledger is 'Cash Pickup'
