﻿@PosIntegration
Feature: RtiFlag
    In order to check RTI flag update
    As a developer
    I want to check if RTI flag is updated for nessesary entities.

    Scenario: Most recent updates in sale ledger are sent to RTI
    Given Refund happened
    And 5 SKU 500300 were refunded
    And Was refunded by Visa credit card for 169.85 pounds
    When BO received the transaction
    Then Data in ledger has RTI flag ToBeSent

    Scenario: Most recent updates in stock are sent to RTI
    Given Sale happened
    And 5 SKU 500300 sold
    When BO received the transaction
    Then Data in stock has RTI flag ToBeSent

    Scenario: Most recent updates in gift cards are sent to RTI
    Given Sale happened
    And Was paid by GiftCard for 169.85 pounds
    When BO received the transaction
    Then Data in database about gift card has RTI flag ToBeSent

    Scenario: Most recent update in event discounts are sent in RTI
    Given Sale happened
    And 1 SKU 500300 sold
    And HierarchySpend event with code '000111' is applied once to first line with saving amount 1.00 pounds
    When BO received the transaction
    Then Data in database about event discount has RTI flag ToBeSent

    Scenario: Most recent update in event discounts trigerred by a coupon are sent in RTI
    Given Sale happened
    And 1 SKU 500300 sold
    And HierarchySpend event with code '000111' is applied once to first line with saving amount 1.00 pounds
    And Applied '000111' event was triggered by coupon with barcode '00000150000650441'
    When BO received the transaction
    Then Data in database about event discount has RTI flag ToBeSent
    And Data in database about coupon has RTI flag ToBeSent

