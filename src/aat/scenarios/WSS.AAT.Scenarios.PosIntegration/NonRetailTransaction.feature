﻿@PosIntegration
Feature: NonRetailTransaction

Scenario: Save paid out transaction to ledger
    Given Paid Out transaction happened 
    And The reason was Counsil Account
    And Was refunded by cash for 169.85
    And Entered invoice number was 12345678
    When BO received the transaction
    Then There is Paid Out in BO db with reason code 09 and description 'Council Account'
    And There is refunded cash payment for 169.85
    And Merchandice amount equal 0
    And Non merchandice amount equal -169.85
    And Total amount equal -169.85
    And There are no lines

Scenario: Save misc in transaction to ledger
    Given Misc Income transaction happened 
    And The reason was Safe Overs
    And Was paid by cash for 169.85 pounds
    And Entered invoice number was 12345678
    When BO received the transaction
    Then There is Misc Income in BO db with reason code 05 and description 'Safe Overs'
    And There is cash payment for 169.85 pounds
    And Merchandice amount equal 0
    And Non merchandice amount equal 169.85
    And Total amount equal 169.85
    And There are no lines

    Scenario: Save Misc In correction transaction to ledger
    Given Misc Income Correction transaction happened 
    And The reason was Safe Overs
    And Was refunded by cash for 169.85
    And Entered invoice number was 12345678
    When BO received the transaction
    Then There is Misc Income Correction in BO db with reason code 05 and description 'Safe Overs'
    And There is refunded cash payment for 169.85
    And Merchandice amount equal 0
    And Non merchandice amount equal -169.85
    And Total amount equal -169.85
    And There are no lines

    Scenario: Save Paid Out correction transaction to ledger
    Given Paid Out Correction transaction happened 
    And The reason was Counsil Account
    And Was paid by cash for 169.85 pounds
    And Entered invoice number was 12345678
    When BO received the transaction
    Then There is Paid Out Correction in BO db with reason code 09 and description 'Council Account'
    And There is cash payment for 169.85 pounds
    And Merchandice amount equal 0
    And Non merchandice amount equal 169.85
    And Total amount equal 169.85
    And There are no lines