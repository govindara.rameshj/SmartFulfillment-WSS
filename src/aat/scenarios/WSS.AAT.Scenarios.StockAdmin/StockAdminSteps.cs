﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Cts.Oasys.Core.Reporting;
using NUnit.Framework;
using StockAdjustments;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.StockAdmin
{
    [Binding]
    public class StockAdminSteps : BaseStepDefinitions
    {
        private readonly TestPicCountRepository _picRepo;

        private readonly List<string> _skuNumbers = new List<string>();
        private decimal _actualCodeValue;

        public StockAdminSteps(IDataLayerFactory dataLayerFactory)
        {
            _picRepo = dataLayerFactory.Create<TestPicCountRepository>();
        }

        [Given(@"User works with Wickes store application")]
        public void GivenUserWorksWithWickesStoreApplication()
        {
            //nothing to do here
        }

        [Given(@"Stock item information for SKU (.*) exists in db")]
        public void GivenStockItemInformationForSkuExistsInDb(string skuNumber)
        {
            _picRepo.CleanPicCountTables();
            _picRepo.InsertStubPicCountHeader();
            _picRepo.InsertStubPicCountDetail(skuNumber);
        }

        [When(@"User insert product SKU number")]
        public void WhenUserInsertProductSkuNumber()
        {
            _skuNumbers.Add(String.Empty);
            _skuNumbers.Add("1");
            _skuNumbers.Add("12");
            _skuNumbers.Add("123");
            _skuNumbers.Add("1234");
            _skuNumbers.Add("12345");
            _skuNumbers.Add("000001");
            _skuNumbers.Add("100006");
            _skuNumbers.Add("1100006");
        }

        [When(@"We calculate code2 for SKU (.*)")]
        public void WhenWeCalculateCodeForSku(string skuNumber)
        {
            _actualCodeValue = Code2Calculation(skuNumber);
        }

        [Then(@"Description of the product is obtained")]
        public void ThenDescriptionOfTheProductIsObtained()
        {
            foreach (var skuNumber in _skuNumbers)
            {
                var description = GetDescriptionTextForEnteredSkuNumber(skuNumber);

                if (String.IsNullOrEmpty(skuNumber))
                {
                    Assert.AreEqual("", description);
                }

                else
                if (skuNumber.Length != 6)
                {
                    Assert.AreEqual("Please enter 6-digit SKU number", description);
                }

                else
                if (skuNumber == "100006")
                {
                    Assert.AreEqual("Elux 14cm Warmer Draw EED14800AX", description);
                }

                else
                Assert.AreEqual("SKU number does not exist in the database", description);
            }
        }

        [Then(@"Code2 value is equal (.*)")]
        public void ThenCodeValueIsEqual(int expectedCode)
        {
            Assert.AreEqual(expectedCode, _actualCodeValue);
        }

        /// <summary>
        /// Implementation of the interface that is needed in old vb code.
        /// </summary>
        private class SkuNumberAndDescriptionContainerMock : ISkuNumberAndDescriptionContainer
        {
            public string SkuNumberText { private get; set; }
            public string SkuDescriptionText { get; private set; }

            public string GetSkuNumberText()
            {
                return SkuNumberText;
            }

            public void SetSkuDescriptionText(string text, SkuDescriptionStyle style)
            {
                SkuDescriptionText = text;
            }
        }

        /// <summary>
        /// Get the description text, which will be shown to user while he is entering SKU number.
        /// </summary>
        /// <param name="enteredSkuNumber">SKU number or part of it, which user has entered so far</param>
        public string GetDescriptionTextForEnteredSkuNumber(string enteredSkuNumber)
        {
            var container = new SkuNumberAndDescriptionContainerMock {SkuNumberText = enteredSkuNumber};

            var manager = new StockAdjustmentManager();
            manager.SetDescriptionForSkuNumber(container);

            return container.SkuDescriptionText;
        }

        public EnumerableRowCollection<DataRow> PicStockCount(string skuNumber)
        {
            var dic = (new Dictionary<String, String>());
            dic.Add("SkuNumber", skuNumber);
            var rep = Report.GetReport(71);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();
            var ds = rep.Dataset;
            var table = ds.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");

            return table;
        }

        public decimal Code2Calculation(string skuNumber)
        {
            var table = PicStockCount(skuNumber);

            foreach (DataRow row in table)
                if (row.Field<decimal>("Code2") != 0)
                    return row.Field<decimal>("Code2");
            return 0;
        }
    }
}
