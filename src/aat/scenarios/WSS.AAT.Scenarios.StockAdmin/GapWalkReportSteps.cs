﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.AAT.Scenarios.StockAdmin
{
    [Binding]
    public class GapWalkReportSteps : BaseStepDefinitions
    {
        private readonly TestNitmasRepository _nitmasRepository;
        private readonly TestDailyTillRepository _dailyTillRepository;
        private IEnumerable<GapWalkReportLine> _gapWalkReport; 

        public GapWalkReportSteps(IDataLayerFactory dataLayerFactory)
        {
            _nitmasRepository = dataLayerFactory.Create<TestNitmasRepository>();
            _dailyTillRepository = dataLayerFactory.Create<TestDailyTillRepository>();
            _nitmasRepository.CleanDatabaseBeforeTests();
        }

        [Given(@"There is row for Date = '(.*)' and SKU = '(.*)' in GapWalk table")]
        public void GivenThereIsRowForDateAndSkuInGapWalkTable(DateTime gapWalkDate, string skuNumber)
        {
            _nitmasRepository.InsertStubGapWalk(gapWalkDate, skuNumber);
        }

        [Given(@"There is row for Date = '(.*)' and TillId = '(.*)' and TranId = '(.*)' and SKU = '(.*)' and Quantity = '(.*)' in DLLINE")]
        public void GivenThereIsRowForDateAndTillIdAndTranIdAndSkuinDlline(DateTime date, string tillId, string tranId, string skuNumber, int quantity)
        {
            _dailyTillRepository.InsertStubDlline(date, tillId, tranId, skuNumber, quantity);
        }

        [Given(@"There is reverted row for Date = '(.*)' and TillId = '(.*)' and TranId = '(.*)' and SKU = '(.*)' and Quantity = '(.*)' in DLLINE")]
        public void GivenThereIsRevertedRowForDateAndTillIdAndTranIdAndSkuAndQuantityInDlline(DateTime date, string tillId, string tranId, string skuNumber, int quantity)
        {
            _dailyTillRepository.InsertStubDlline(date, tillId, tranId, skuNumber, quantity, true);
        }

        [Given(@"DateLastSold field in STKMAS for SKU = '(.*)' is equal '(.*)'")]
        public void GivenDateLastSoldFieldInStkmasForSkuIsEqual(string skuNumber, DateTime dateLastSold)
        {
            _nitmasRepository.SetStkmasLastSoldDate(skuNumber, dateLastSold);
        }

        [Given(@"DateLastSold field in STKMAS for SKU = '(.*)' is not set")]
        public void GivenDateLastSoldFieldInStkmasForSkuIsNotSet(string skuNumber)
        {
            _nitmasRepository.SetStkmasLastSoldDate(skuNumber, null);
        }

        [Given(@"There are no lines in DLLINE for SKU = '(.*)'")]
        public void GivenThereAreNoLinesInDllineForSku(string skuNumber)
        {
            //Nothing to do here, all tables are cleaned before test execution anyway
        }

        [When(@"Gap walk report was requested for Date = '(.*)'")]
        public void WhenGapWalkReportWasRequestedForDate(DateTime reportDate)
        {
            _gapWalkReport = _nitmasRepository.CreateGapWalkReport(reportDate);
        }

        [Then(@"Last sold date in report for SKU = '(.*)' is equal = '(.*)'")]
        public void ThenLastSoldDateInReportIsEqual(string skuNumber, DateTime lastSoldDate)
        {
            var reportLine = _gapWalkReport.FirstOrDefault(x => x.SkuNumber == skuNumber);

            Assert.IsNotNull(reportLine);
            Assert.AreEqual(lastSoldDate, reportLine.DateLastSold);
        }

        [Then(@"Last sold date in report for SKU = '(.*)' is empty")]
        public void ThenLastSoldDateInReportForSkuIsEmpty(string skuNumber)
        {
            var reportLine = _gapWalkReport.FirstOrDefault(x => x.SkuNumber == skuNumber);

            Assert.IsNotNull(reportLine);
            Assert.IsNull(reportLine.DateLastSold);
        }
    }
}
