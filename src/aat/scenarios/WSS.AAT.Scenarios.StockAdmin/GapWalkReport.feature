﻿@GapWalkReport
Feature: GapWalkReport
    In order to check GapWalkReport content
    As a developer
    I write scenarios for different preset data in tables STKMAS, DLLINE and GapWalk

Scenario: DLLINE date has more priority than STKMAS date
    Given There is row for Date = '11-11-2011' and SKU = '500300' in GapWalk table
    And There is row for Date = '02-02-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '2' in DLLINE
    And DateLastSold field in STKMAS for SKU = '500300' is equal '01-01-2016'
    When Gap walk report was requested for Date = '11-11-2011'
    Then Last sold date in report for SKU = '500300' is equal = '02-02-2016'

Scenario: All nessesary information about sku could be found in DLLINE table
    Given There is row for Date = '11-11-2011' and SKU = '500300' in GapWalk table
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '2' in DLLINE
    And DateLastSold field in STKMAS for SKU = '500300' is not set
    When Gap walk report was requested for Date = '11-11-2011'
    Then Last sold date in report for SKU = '500300' is equal = '01-01-2016'

Scenario: There is information about last sold date in STKMAS table, and there are no lines for that sku in DLLINE table
    Given There is row for Date = '11-11-2011' and SKU = '500300' in GapWalk table
    And There are no lines in DLLINE for SKU = '500300'
    And DateLastSold field in STKMAS for SKU = '500300' is equal '02-02-2012'
    When Gap walk report was requested for Date = '11-11-2011'
    Then Last sold date in report for SKU = '500300' is equal = '02-02-2012'

Scenario: There is Refund line for sku in DLLINE table
    Given There is row for Date = '11-11-2011' and SKU = '500300' in GapWalk table
    And There is row for Date = '01-01-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '-5' in DLLINE
    And DateLastSold field in STKMAS for SKU = '500300' is not set
    When Gap walk report was requested for Date = '11-11-2011'
    Then Last sold date in report for SKU = '500300' is empty

Scenario: There is Reverted line for sku in DLLINE table
    Given There is row for Date = '11-11-2011' and SKU = '500300' in GapWalk table
    And There is reverted row for Date = '01-01-2016' and TillId = '01' and TranId = '01' and SKU = '500300' and Quantity = '2' in DLLINE
    And DateLastSold field in STKMAS for SKU = '500300' is not set
    When Gap walk report was requested for Date = '11-11-2011'
    Then Last sold date in report for SKU = '500300' is empty

Scenario: There is no information about last date sku was sold both in STKMAS table and DLLINE table
    Given There is row for Date = '11-11-2011' and SKU = '500300' in GapWalk table
    And There are no lines in DLLINE for SKU = '500300'
    And DateLastSold field in STKMAS for SKU = '500300' is not set
    When Gap walk report was requested for Date = '11-11-2011'
    Then Last sold date in report for SKU = '500300' is empty