﻿@StockAdmin
Feature: StockAdmin

As a colleague in store
I want the Wickes store application to show me the description of the product once SKU number is entered
So that I have no doubts about what product stock adjustments I work with
Scenario: Get description text for entered SKU number
    Given User works with Wickes store application
    When User insert product SKU number
    Then Description of the product is obtained

Scenario: Get Code2 for pic stock count
    Given Stock item information for SKU 100500 exists in db
    When We calculate code2 for SKU 100500
    Then Code2 value is equal -5