﻿using Cts.Oasys.Core.SystemEnvironment;
using Cts.Oasys.Core.Tests;
using NUnit.Framework;
using QodFixtures;
using System;
using System.Configuration;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Banking;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Qod.CtsOrderManager;
using WSS.AAT.Common.Utility.Bindings;
using WSS.AAT.Common.Utility.Configuration;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.QodMaintain
{
    [Binding]
    public class QodMaintainSteps : BaseStepDefinitions
    {
        private string orderNumber;
        private string orderNumberWO;
        private string orderNumberHY;

        private readonly TestSystemEnvironment systemEnvironment;
        private readonly IDataLayerFactory dataLayerFactory;

        private readonly CreationRoutine creationRoutine;
        private readonly FulfillmentRoutine fulfillmentRoutine;

        private readonly SaleOrderMaintain saleOrderMaintain;
        private readonly CoreBanking coreBanking;

        private readonly TestCustomerOrderRepository orderRepo;

        public QodMaintainSteps(TestSystemEnvironment systemEnvironment, IDataLayerFactory dataLayerFactory)
        {
            this.systemEnvironment = systemEnvironment;
            this.dataLayerFactory = dataLayerFactory;

            creationRoutine = new CreationRoutine(this.dataLayerFactory, systemEnvironment);
            fulfillmentRoutine = new FulfillmentRoutine(this.dataLayerFactory, systemEnvironment);

            saleOrderMaintain = new SaleOrderMaintain();
            coreBanking = new CoreBanking(systemEnvironment);

            orderRepo = this.dataLayerFactory.Create<TestCustomerOrderRepository>();
        }

        [Given(@"Standart Click and Collect order was made")]
        public void GivenStandartClickAndCokkectOrderWasMade()
        {
            creationRoutine.PrepareRequestForStandardClickAndCollectOrder();
        }

        [Given(@"The order was processed")]
        public void GivenTheOrderWasProcessed()
        {
            creationRoutine.ProcessPreparedRequest();

            var response = creationRoutine.GetResponse();
            Assert.That(response, Is.Not.Null, "Response is null, check logs.");
            Assert.That(response.IsSuccessful(), Is.True, "Response is not succesful, check logs.");
            orderNumber = creationRoutine.CurrentResponse.OrderHeader.StoreOrderNumber.Value;
        }

        [Given(@"Fulfillment request for Venda order was made")]
        public void GivenFulfillmentRequestForVendaOrderWasMade()
        {
            fulfillmentRoutine.PrepareRequestForStandardVendaOrder();
        }

        [Given(@"Sale Order Maintain Report Main Form was loaded")]
        [When(@"Sale Order Maintain Report Main Form was loaded")]
        public void GivenSaleOrderMaintainReportMainFormWasLoaded()
        {
            saleOrderMaintain.LoadMainForm();
        }

        [Given(@"Customer Postcode was '(.*)'")]
        public void GivenCustomerPostcodeWas(string postcode)
        {
            fulfillmentRoutine.SetCustomerPostcode(postcode);
        }

        [Given(@"Customer Name was '(.*)'")]
        public void GivenCustomerNameWas(string name)
        {
            fulfillmentRoutine.SetCustomerName(name);
        }

        [Given(@"Delivery Postcode was '(.*)'")]
        public void GivenDeliveryPostcodeWas(string postcode)
        {
            fulfillmentRoutine.SetDeliveryPostcode(postcode);
        }

        [Given(@"Delivery Contact Name was '(.*)'")]
        public void GivenDeliveryContactNameWas(string name)
        {
            fulfillmentRoutine.SetDeliveryContactName(name);
        }

        [Given(@"Home Phone Number was '(.*)'")]
        public void GivenHomePhoneNumberWas(string phoneNumber)
        {
            fulfillmentRoutine.SetDeliveryContactPhoneNumber(phoneNumber);
        }

        [Given(@"Mobile Phone Number was '(.*)'")]
        public void GivenMobilePhoneNumberWas(string phoneNumber)
        {
            fulfillmentRoutine.SeMobilePhoneNumber(phoneNumber);
        }

        [Given(@"Work Phone Number was '(.*)'")]
        public void GivenWorkPhoneNumberWas(string phoneNumber)
        {
            fulfillmentRoutine.SetWorkPhoneNumber(phoneNumber);
        }

        [Given(@"Delivery Contact Phone Number was '(.*)'")]
        public void GivenDeliveryContactPhoneNumberWas(string phoneNumber)
        {
            fulfillmentRoutine.SetDeliveryContactPhoneNumber(phoneNumber);
        }

        [Given(@"And Next Order Number was '(.*)'")]
        public void GivenAndNextOrderNumberWas(string orderNumber)
        {
            orderRepo.UpdateNextOrderNumber(orderNumber);
        }

        [Given(@"Start Date is empty")]
        public void GivenStartDateIsEmpty()
        {
            saleOrderMaintain.SetStartDate("empty");
        }

        [Given(@"End Date is empty")]
        public void GivenEndDateIsEmpty()
        {
            saleOrderMaintain.SetEndDate("empty");
        }

        [Given(@"Start Date is '(.*)'")]
        public void GivenStartDateIs(string date)
        {
            saleOrderMaintain.SetStartDate(date);
        }

        [Given(@"End Date is '(.*)'")]
        public void GivenEndDateIs(string date)
        {
            saleOrderMaintain.SetEndDate(date);
        }


        [Given(@"Name on all Orders Tab was '(.*)'")]
        public void GivenNameOnAllOrdersTabWas(string name)
        {
            saleOrderMaintain.SetNameOnAllOrdersTab(name);
        }

        [Given(@"Main Form was switched to All Orders tab")]
        [When(@"Main Form was switched to All Orders tab")]
        public void GivenMainFormWasSwitchedToAllOrdersTab()
        {
            saleOrderMaintain.SwitchToAllOrdersTab();
        }

        [Given(@"Fulfillment request for HY order was made")]
        [When(@"Fulfillment request for HY order was made")]
        public void GivenFulfillmentRequestForHYOrderWasMade()
        {
            fulfillmentRoutine.PrepareRequestForStandardVendaOrder();
            fulfillmentRoutine.CurrentRequest.OrderHeader.Source = "HY";
            fulfillmentRoutine.ProcessPreparedRequest();

            var response = fulfillmentRoutine.GetResponse();
            Assert.That(response, Is.Not.Null, "Response is null, check logs.");
            Assert.That(response.IsSuccessful(), Is.True, "Response is not succesful, check logs.");
            orderNumberHY = fulfillmentRoutine.CurrentResponse.FulfillingSiteOrderNumber.Value;
        }

        [When(@"Fulfillment Request for Venda order was processed")]
        [Given(@"Fulfillment Request for Venda order was processed")]
        public void GivenFulfillmentRequestForVendaOrderWasProcessed()
        {
            fulfillmentRoutine.ProcessPreparedRequest();

            var response = fulfillmentRoutine.GetResponse();
            Assert.That(response, Is.Not.Null, "Response is null, check logs.");
            Assert.That(response.IsSuccessful(), Is.True, "Response is not succesful, check logs.");
            orderNumberWO = fulfillmentRoutine.CurrentResponse.FulfillingSiteOrderNumber.Value;
        }

        [When(@"Orders were retrieved")]
        public void WhenOrdersWereRetrieved()
        {
            saleOrderMaintain.RetrieveOrders();
        }

        [When(@"Orders were retrieved by using Enter Key")]
        public void WhenOrdersWereRetrievedByUsingEnterKey()
        {
        }

        [Then(@"Click And Collect order does not exist Report Results with search criteria Open Orders")]
        public void ThenClickAndCollectOrderDoesNotExistReportResultsWithSearchCriteria()
        {
            QodReports qodReports = new QodReports();

            Assert.IsFalse(qodReports.CheckOrderExistsInReportResultsSearchCriteriaOpenOrders(orderNumber));
        }

        [Then(@"Venda order exists Report Results with search criteria Open Orders")]
        public void ThenVendaOrderExistsReportResultsWithSearchCriteria()
        {
            QodReports qodReports = new QodReports();

            Assert.IsTrue(qodReports.CheckOrderExistsInReportResultsSearchCriteriaOpenOrders(orderNumberWO));
        }

        [Then(@"HY order exists Report Results with search criteria Open Orders")]
        public void ThenHYOrderExistsReportResultsWithSearchCriteria()
        {
            QodReports qodReports = new QodReports();

            Assert.IsTrue(qodReports.CheckOrderExistsInReportResultsSearchCriteriaOpenOrders(orderNumberHY));
        }

        [Then(@"Click And Collect order does not exist Report Results with search criteria All Orders")]
        public void ThenClickAndCollectOrderDoesNotExistReportResultsWithSearchCriteriaAllOrders()
        {
            QodReports qodReports = new QodReports();

            Assert.IsFalse(qodReports.CheckOrderExistsInReportResultsSearchCriteriaAllOrders(orderNumber, DateTime.Now.AddDays(-1), DateTime.Now));
        }

        [Then(@"Venda order does not exist Report Results with search criteria All Orders")]
        public void ThenVendaOrderDoesNotExistReportResultsWithSearchCriteriaAllOrders()
        {
            QodReports qodReports = new QodReports();

            Assert.IsTrue(qodReports.CheckOrderExistsInReportResultsSearchCriteriaAllOrders(orderNumberWO, DateTime.Now.AddDays(-1), DateTime.Now));
        }

        [Then(@"HY order exists Report Results with search criteria All Orders")]
        public void ThenHYOrderExistsReportResultsWithSearchCriteriaAllOrders()
        {
            QodReports qodReports = new QodReports();

            Assert.IsTrue(qodReports.CheckOrderExistsInReportResultsSearchCriteriaAllOrders(orderNumberHY, DateTime.Now.AddDays(-1), DateTime.Now));
        }

        [Then(@"Search is disallowed in Sale Order Maintain Report")]
        public void ThenSearchIsDisallowedInSaleOrderMaintainReport()
        {
            Assert.IsFalse(saleOrderMaintain.IsSearchAllowed());
        }

        [Then(@"Order Number for Customer Postcode '(.*)' is '(.*)'")]
        public void ThenOrderNumberForCustomerPostcodeIs(string postcode, string orderNumber)
        {
            Assert.AreEqual(orderNumber, coreBanking.orderNumberForCustomerPostcodeIs(postcode));
        }

        [Then(@"Order Number for Delivery Postcode '(.*)' is '(.*)'")]
        public void ThenOrderNumberForDeliveryPostcodeIs(string postcode, string orderNumber)
        {
            Assert.AreEqual(orderNumber, coreBanking.orderNumberForDeliveryPostcodeIs(postcode));
        }

        [Then(@"Order Number for Customer Postcode '(.*)' is not found")]
        public void ThenOrderNumberForCustomerPostcodeIsNotFound(string postcode)
        {
            Assert.AreEqual("Not found", coreBanking.orderNumberForCustomerPostcodeIs(postcode));
        }

        [Then(@"Order Number for Delivery Postcode '(.*)' is not found")]
        public void ThenOrderNumberForDeliveryPostcodeIsNotFound(string postcode)
        {
            Assert.AreEqual("Not found", coreBanking.orderNumberForDeliveryPostcodeIs(postcode));
        }

        [Then(@"Search For Customer Postcode '(.*)' Returns Number Of Matches equal to (.*)")]
        public void ThenSearchForCustomerPostcodeReturnsNumberOfMatchesEqualTo(string postcode, int count)
        {
            Assert.AreEqual(count, coreBanking.searchForCustomerPostcodeReturnsNumberOfMatches(postcode));
        }

        [Then(@"Search For Delivery Postcode '(.*)' Returns Number Of Matches equal to (.*)")]
        public void ThenSearchForDeliveryPostcodeReturnsNumberOfMatchesEqualTo(string postcode, int count)
        {
            Assert.AreEqual(count, coreBanking.searchForCustomerPostcodeReturnsNumberOfMatches(postcode));
        }

        [Then(@"Order Number for Customer Name '(.*)' is '(.*)'")]
        public void ThenOrderNumberForCustomerNameIs(string customerName, string orderNumber)
        {
            Assert.AreEqual(orderNumber, coreBanking.orderNumberForCustomerNameIs(customerName));
        }

        [Then(@"Order Number for Delivery Contact Name '(.*)' is '(.*)'")]
        public void ThenOrderNumberForDeliveryContactNameIs(string deliveryContactName, string orderNumber)
        {
            Assert.AreEqual(orderNumber, coreBanking.orderNumberForDeliveryContactNameIs(deliveryContactName));
        }

        [Then(@"Order Number for Customer Name '(.*)' is not found")]
        public void ThenOrderNumberForCustomerNameIsNotFound(string customerName)
        {
            Assert.AreEqual("Not found", coreBanking.orderNumberForCustomerNameIs(customerName));
        }

        [Then(@"Order Number for Delivery Contact Name '(.*)' is not found")]
        public void ThenOrderNumberForDeliveryContactNameIsNotFound(string deliveryContactName)
        {
            Assert.AreEqual("Not found", coreBanking.orderNumberForDeliveryContactNameIs(deliveryContactName));
        }

        [Then(@"Search For Customer Name '(.*)' Returns Number Of Matches equal to (.*)")]
        public void ThenSearchForCustomerNameReturnsNumberOfMatchesEqualTo(string customerName, int count)
        {
            Assert.AreEqual(count, coreBanking.searchForCustomerNameReturnsNumberOfMatches(customerName));
        }

        [Then(@"Search For Delivery Contact Name '(.*)' Returns Number Of Matches equal to (.*)")]
        public void ThenSearchForDeliveryContactNameReturnsNumberOfMatchesEqualTo(string deliveryContactName, int count)
        {
            Assert.AreEqual(count, coreBanking.searchForDeliveryContactNameReturnsNumberOfMatches(deliveryContactName));
        }

        [Then(@"Order Number for Home Phone Number '(.*)' is '(.*)'")]
        public void ThenOrderNumberForHomePhoneNumberIs(string phoneNumber, string orderNumber)
        {
            Assert.AreEqual(orderNumber, coreBanking.orderNumberForPhoneNumberIs(phoneNumber));
        }

        [Then(@"Order Number for Mobile Phone Number '(.*)' is '(.*)'")]
        public void ThenOrderNumberForMobilePhoneNumberIs(string phoneNumber, string orderNumber)
        {
            Assert.AreEqual(orderNumber, coreBanking.orderNumberForMobileNumberIs(phoneNumber));
        }

        [Then(@"Order Number for Work Phone Number '(.*)' is '(.*)'")]
        public void ThenOrderNumberForWorkPhoneNumberIs(string phoneNumber, string orderNumber)
        {
            Assert.AreEqual(orderNumber, coreBanking.orderNumberForWorkNumberIs(phoneNumber));
        }

        [Then(@"Order Number for Delivery Contact Phone Number '(.*)' is '(.*)'")]
        public void ThenOrderNumberForDeliveryContactPhoneNumberIs(string phoneNumber, string orderNumber)
        {
            Assert.AreEqual(orderNumber, coreBanking.orderNumberForDeliveryContactNumberIs(phoneNumber));
        }

        [Then(@"Search For Home Phone Number '(.*)' Returns Number Of Matches equal to (.*)")]
        public void ThenSearchForHomePhoneNumberReturnsNumberOfMatchesEqualTo(string phoneNumber, int count)
        {
            Assert.AreEqual(count, coreBanking.searchForPhoneNumberReturnsNumberOfMatches(phoneNumber));
        }

        [Then(@"Search For Mobile Phone Number '(.*)' Returns Number Of Matches equal to (.*)")]
        public void ThenSearchForMobilePhoneNumberReturnsNumberOfMatchesEqualTo(string phoneNumber, int count)
        {
            Assert.AreEqual(count, coreBanking.searchForMobileNumberReturnsNumberOfMatches(phoneNumber));
        }

        [Then(@"Search For Work Phone Number '(.*)' Returns Number Of Matches equal to (.*)")]
        public void ThenSearchForWorkPhoneNumberReturnsNumberOfMatchesEqualTo(string phoneNumber, int count)
        {
            Assert.AreEqual(count, coreBanking.searchForWorkNumberReturnsNumberOfMatches(phoneNumber));
        }

        [Then(@"Search For Delivery Contact Phone Number '(.*)' Returns Number Of Matches equal to (.*)")]
        public void ThenSearchForDeliveryContactPhoneNumberReturnsNumberOfMatchesEqualTo(string phoneNumber, int count)
        {
            Assert.AreEqual(count, coreBanking.searchForDeliveryContactNumberReturnsNumberOfMatches(phoneNumber));
        }

        [Then(@"Start Date Value is empty")]
        public void ThenStartDateValueIsEmpty()
        {
            Assert.AreEqual("empty", saleOrderMaintain.StartDateValue());
        }

        [Then(@"End Date Value is empty")]
        public void ThenEndDateValueIsEmpty()
        {
            Assert.AreEqual("empty", saleOrderMaintain.EndDateValue());
        }

        [Then(@"Retrieve Button is enabled")]
        public void ThenRetrieveButtonIsEnabled()
        {
            Assert.AreEqual("enabled",saleOrderMaintain.RetrieveButtonState());
        }

        [Then(@"Orders Retrieve by using Enter Key is done")]
        public void ThenOrdersRetrieveByUsingEnterKeyIsDone()
        {
            Assert.AreEqual("done", saleOrderMaintain.RetrieveOrdersUsingEnterKey());
        }
    }
}
