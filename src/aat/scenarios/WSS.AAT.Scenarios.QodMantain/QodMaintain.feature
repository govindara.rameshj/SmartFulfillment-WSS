﻿@QodMaintain
Feature: QodMaintain

Scenario: Check Order Report For Maintain Qod
Given Standart Click and Collect order was made
And The order was processed
And Fulfillment request for Venda order was made
And Fulfillment Request for Venda order was processed
When Fulfillment request for HY order was made
Then Click And Collect order does not exist Report Results with search criteria Open Orders
And Venda order exists Report Results with search criteria Open Orders
And HY order exists Report Results with search criteria Open Orders
And Click And Collect order does not exist Report Results with search criteria All Orders
And Venda order does not exist Report Results with search criteria All Orders
And HY order exists Report Results with search criteria All Orders

Scenario: Search For Existing Order By Customer Postcode
Given Fulfillment request for Venda order was made
And Customer Postcode was 'CF312AA'
And And Next Order Number was '022307'
When Fulfillment Request for Venda order was processed
Then Order Number for Customer Postcode 'CF312AA' is '022307'
And Order Number for Customer Postcode 'CF3 12AA' is '022307'
And Order Number for Customer Postcode 'QQ11QQ' is not found
And Search For Customer Postcode 'CF312AA' Returns Number Of Matches equal to 1

Scenario: Search For Existing Order By Delivery Postcode
Given Fulfillment request for Venda order was made
And Delivery Postcode was 'CF312BE'
And And Next Order Number was '024444'
When Fulfillment Request for Venda order was processed
Then Order Number for Delivery Postcode 'CF312BE' is '024444'
And Order Number for Delivery Postcode 'CF3 12BE' is '024444'
And Order Number for Delivery Postcode 'QQ11QQ' is not found
And Search For Delivery Postcode 'CF312BE' Returns Number Of Matches equal to 1

Scenario: Search For Existing Order By Customer Name
Given Fulfillment request for Venda order was made
And Customer Name was 'Harry Potter'
And And Next Order Number was '022222'
When Fulfillment Request for Venda order was processed
Then Order Number for Customer Name 'Harry Potter' is '022222'
Then Order Number for Customer Name 'Harry' is '022222'
And Order Number for Customer Name 'James Potter' is not found
And Search For Customer Name 'Harry Potter' Returns Number Of Matches equal to 1

Scenario: Search For Existing Order By Delivery Contact Name
Given Fulfillment request for Venda order was made
And Delivery Contact Name was 'Tom Reddl'
And And Next Order Number was '033333'
When Fulfillment Request for Venda order was processed
Then Order Number for Delivery Contact Name 'Tom Reddl' is '033333'
Then Order Number for Delivery Contact Name 'Tom' is '033333'
And Order Number for Delivery Contact Name 'James Potter' is not found
And Search For Delivery Contact Name 'Tom Reddl' Returns Number Of Matches equal to 1

Scenario: Search For Existing Order By Home Phone Number
Given Fulfillment request for Venda order was made
And Home Phone Number was '0999223344'
And And Next Order Number was '044444'
When Fulfillment Request for Venda order was processed
Then Order Number for Home Phone Number '0999223344' is '044444'
And Order Number for Home Phone Number '0999 22 33 44' is '044444'
And Search For Home Phone Number '0999223344' Returns Number Of Matches equal to 1

Scenario: Search For Existing Order By Mobile Phone Number
Given Fulfillment request for Venda order was made
And Mobile Phone Number was '0111223344'
And And Next Order Number was '077777'
When Fulfillment Request for Venda order was processed
Then Order Number for Mobile Phone Number '0111223344' is '077777'
And Order Number for Mobile Phone Number '0111 22 33 44' is '077777'
And Search For Mobile Phone Number '0111223344' Returns Number Of Matches equal to 1

Scenario: Search For Existing Order By Work Phone Number
Given Fulfillment request for Venda order was made
And Work Phone Number was '0777223344'
And And Next Order Number was '055555'
When Fulfillment Request for Venda order was processed
Then Order Number for Work Phone Number '0777223344' is '055555'
And Order Number for Work Phone Number '0777 22 33 44' is '055555'
And Search For Work Phone Number '0777223344' Returns Number Of Matches equal to 1

Scenario: Search For Existing Order By Delivery Contact Phone Number
Given Fulfillment request for Venda order was made
And Delivery Contact Phone Number was '0888223344'
And And Next Order Number was '066666'
When Fulfillment Request for Venda order was processed
Then Order Number for Delivery Contact Phone Number '0888223344' is '066666'
And Order Number for Delivery Contact Phone Number '0888 22 33 44' is '066666'
And Search For Delivery Contact Phone Number '0888223344' Returns Number Of Matches equal to 1

Scenario: Search Without Parameters Is Disallowed in Sale Order Maintain Report
Given Sale Order Maintain Report Main Form was loaded
When Main Form was switched to All Orders tab
Then Search is disallowed in Sale Order Maintain Report

Scenario: Initial Search Criteria For All Sale Order Maintain Order Tabs
When Sale Order Maintain Report Main Form was loaded
Then Start Date Value is empty
And End Date Value is empty

Scenario: Retrieve Button State After Search All Orders
Given Sale Order Maintain Report Main Form was loaded
And Main Form was switched to All Orders tab
And Start Date is empty
And End Date is empty
And Name on all Orders Tab was 'Smith'
When Orders were retrieved
Then Retrieve Button is enabled

Scenario: Search By Pressing Enter Key
Given Sale Order Maintain Report Main Form was loaded
And Main Form was switched to All Orders tab
And Start Date is '01/10/2013'
And End Date is '01/10/2013'
When Orders were retrieved by using Enter Key
Then Orders Retrieve by using Enter Key is done
