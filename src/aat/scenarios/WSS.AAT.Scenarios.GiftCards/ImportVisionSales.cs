﻿using System;
using System.Diagnostics;
using TpWickes.Library.VisionSale.Vision;

namespace WSS.AAT.Scenarios.GiftCards
{
    public class ImportVisionSales
    {
        /// <summary>
        /// Provides ability to launch debugger from within Wiki
        /// </summary>
        public void StartDebug()
        {
            Debugger.Launch();
        }

        public void ProcessInputFile()
        {
            var xmlString = string.Format("<CTSVISIONDEPOSIT><PVTOTS><DATE>{0}</DATE><TILL>55</TILL><TRAN>0364</TRAN><CASH>055</CASH><TIME>161715</TIME><TCOD>M+</TCOD><MISC></MISC><DESC></DESC><ORDN>000611</ORDN><MERC>000000.00</MERC><NMER>000010.00 </NMER><TAXA>000001.67 </TAXA><DISC>000000.00</DISC><TOTL>000010.00 </TOTL><IEMP>000000.00</IEMP><PVEM>000000.00</PVEM><PIVT>N</PIVT></PVTOTS><PVPAID><TENDER><DATE>{0}</DATE><TILL>55</TILL><TRAN>0364</TRAN><NUMB>0001</NUMB><TYPE>13</TYPE><AMNT>000010.00-</AMNT><CARD>*******************</CARD><EXDT></EXDT><PIVT>N</PIVT><AUTH>108651873</AUTH><CASH>055</CASH><TRID>1827905280</TRID><MSGN>1001</MSGN></TENDER></PVPAID></CTSVISIONDEPOSIT>",
                DateTime.Now.ToString("MM/dd/yy"));
            var sale = KitchenAndBathroom.Deserialise(xmlString);
            sale.Persist();
        }
    }
}
