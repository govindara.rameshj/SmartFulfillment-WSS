using Cts.Oasys.Core.Tests;
using NUnit.Framework;
using System;
using System.Linq;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Banking;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.GiftCards
{
    [Binding]
    public class GiftCardsSteps : BaseStepDefinitions
    {
        private readonly TestSystemEnvironment systemEnvironment;
        private readonly IDataLayerFactory dataLayerFactory;

        private readonly CoreBanking coreBanking;
        private readonly BankingReportRoutine bankingReportRoutine;
        private readonly RetailSalesReport retailSalesReport;
        private readonly ImportVisionSales importVisionSales;

        private const int giftCardTypeCode = 13;
        private const string giftCardDescription = "Gift Card";

        public GiftCardsSteps(TestSystemEnvironment systemEnvironment, IDataLayerFactory dataLayerFactory)
        {
            this.systemEnvironment = systemEnvironment;
            this.dataLayerFactory = dataLayerFactory;

            coreBanking = new CoreBanking(systemEnvironment);
            bankingReportRoutine = new BankingReportRoutine();
            retailSalesReport = new RetailSalesReport();
            importVisionSales = new ImportVisionSales();
        }

        [Given(@"Cashier '(.*)' made a sale payed by Gift Card")]
        public void GivenCashierMadeASalePayedByGiftCard(string cashierName)
        {
            coreBanking.createSaleForCashierPayedByGiftCard(cashierName, giftCardTypeCode);
        }

        [Given(@"All Daily Data Tables were cleaned up")]
        public void GivenAllDailyDataTablesWereCleanedUp()
        {
            var repo = dataLayerFactory.Create<TestDailyTillRepository>();

            repo.RemoveDailies();
        }

        [Given(@"Cashier Balances were cleaned up")]
        public void GivenCashierBalancesWereCleanedUp()
        {
            var repo = dataLayerFactory.Create<TestCashierBalanceRepository>();

            repo.RemoveCashierBalances();
        }

        [Given(@"Data about Daily Gift Cards was removed from db")]
        public void GivenDataAboutDailyGiftCardsWasRemovedFromDb()
        {
            var repo = dataLayerFactory.Create<TestDailyTillRepository>();

            repo.CleanDailyGiftCard();
        }

        [When(@"Data Set of Audit Roll Enqury was loaded")]
        public void WhenDataSetOfAuditRollEnquryWasLoaded()
        {
            bankingReportRoutine.LoadDataSetAuditRollEnqury();
        }

        [When(@"Retail Sales Report was got by Cashier ID '(.*)' Summary")]
        public void WhenRetailSalesReportWasGotByCashierIDSummary(string cashierId)
        {
            retailSalesReport.GetRetailSalesReportByCashierIDSummary(cashierId);
        }

        [When(@"Retail Sales Report was got by Till ID '(.*)' Summary")]
        public void WhenRetailSalesReportWasGotByTillIDSummary(string tillId)
        {
            retailSalesReport.GetRetailSalesReportByTillIDSummary(tillId);
        }

        [When(@"Retail Sales Report was got by Cashier ID '(.*)' Details")]
        public void WhenRetailSalesReportWasGotByCashierIDDetails(string cashierId)
        {
            retailSalesReport.GetRetailSalesReportByCashierIDDetails(cashierId);
        }

        [When(@"Retail Sales Report was got by Till ID '(.*)' Details")]
        public void WhenRetailSalesReportWasGotByTillIDDetails(string tillId)
        {
            retailSalesReport.GetRetailSalesReportByTillIDDetails(tillId);
        }

        [When(@"Vision Sales Input File was processed")]
        public void WhenVisionSalesInputFileWasProcessed()
        {
            importVisionSales.ProcessInputFile();
        }

        [Then(@"There is a Gift Card Payment made today on Till '(.*)' with Transaction Number '(.*)'")]
        public void ThenThereIsAGiftCardPaymentMadeTodayOnTillWithTransactionNumber(string tillNumber, string transactionNumber)
        {
            Assert.AreEqual((Decimal)giftCardTypeCode, bankingReportRoutine.GetTenderTypePayGiftCard(DateTime.Now.Date, tillNumber, transactionNumber));
            Assert.AreEqual(giftCardDescription, bankingReportRoutine.GetDescriptionTenderTypePayGiftCard(DateTime.Now.Date, tillNumber, transactionNumber));
        }

        [Then(@"Retail Sales count is (.*)")]
        public void ThenRetailSalesCountIs(int count)
        {
            Assert.AreEqual(count, retailSalesReport.GetRetailSalesCount());
        }

        [Then(@"There is information about Gift Card in db:")]
        public void ThenThereIsInformationAboutGiftCardInDb(Table table)
        {
            var row = table.Rows.Single();

            var repo = dataLayerFactory.Create<TestDailyTillRepository>();

            var dailyGiftCard = repo.SelectDailyGiftCard();

            Assert.AreEqual(dailyGiftCard.CreateDate, DateTime.Now.Date);
            Assert.AreEqual(dailyGiftCard.TillNumber, row["Till Number"]);
            Assert.AreEqual(dailyGiftCard.TransactionNumber, row["Transaction Number"]);
            Assert.AreEqual(dailyGiftCard.CardNumber, row["Card Number"]);
            Assert.AreEqual(dailyGiftCard.CashierNumber, row["Cashier Number"]);
            Assert.AreEqual(dailyGiftCard.TransactionType, row["Transaction Type"]);
            Assert.AreEqual(dailyGiftCard.TenderAmount, decimal.Parse(row["Tender Amount"]));
            Assert.AreEqual(dailyGiftCard.AuthCode.Replace(" ", string.Empty), row["Authorisation Code"]);
            Assert.AreEqual(dailyGiftCard.MessageNumber.Replace(" ", string.Empty), row["Message Number"]);
            Assert.AreEqual(dailyGiftCard.ExternalTransactionNumber, decimal.Parse(row["External Transaction Number"]));
        }
    }
}
