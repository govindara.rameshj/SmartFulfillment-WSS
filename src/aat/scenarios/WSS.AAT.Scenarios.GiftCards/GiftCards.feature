﻿@GiftCards
Feature: GiftCards

Scenario: Check Audit Roll Enqury Reporting
Given Cashier 'Fred Bloggs' made a sale payed by Gift Card
When Data Set of Audit Roll Enqury was loaded
Then There is a Gift Card Payment made today on Till '01' with Transaction Number '0001'  

Scenario: Retail Sales Report By Cashier ID Summary
Given All Daily Data Tables were cleaned up
And Cashier Balances were cleaned up
And Cashier 'Fred Bloggs' made a sale payed by Gift Card
When Retail Sales Report was got by Cashier ID '003' Summary
Then Retail Sales count is 1

Scenario: Retail Sales Report By Till ID Summary
Given All Daily Data Tables were cleaned up
And Cashier Balances were cleaned up
And Cashier 'Fred Bloggs' made a sale payed by Gift Card
When Retail Sales Report was got by Till ID '01' Summary
Then Retail Sales count is 1

Scenario: Retail Sales Report By Cashier ID Details
Given All Daily Data Tables were cleaned up
And Cashier Balances were cleaned up
And Cashier 'Fred Bloggs' made a sale payed by Gift Card
When Retail Sales Report was got by Cashier ID '003' Details
Then Retail Sales count is 1

Scenario: Retail Sales Report By Till ID Details
Given All Daily Data Tables were cleaned up
And Cashier Balances were cleaned up
And Cashier 'Fred Bloggs' made a sale payed by Gift Card
When Retail Sales Report was got by Till ID '01' Details
Then Retail Sales count is 1

Scenario: Import Vision Sales
Given Data about Daily Gift Cards was removed from db
When Vision Sales Input File was processed
Then There is information about Gift Card in db:
| Create Date | Till Number | Transaction Number | Card Number         | Cashier Number | Transaction Type | Tender Amount | Authorisation Code | Message Number | External Transaction Number |
| today       | 55          | 0364               | ******************* | 055            | TS               | 10.00         | 108651873          | 1001           | 1827905280                  |
