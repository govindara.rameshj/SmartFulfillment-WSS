using WSS.AAT.Common.Utility.Configuration;
using Cts.Oasys.Core.Tests;

namespace WSS.AAT.Scenarios.Pos
{
    class SetUp : BaseSetUp
    {
        protected override void InitializeTestEnvironmentSettings(TestEnvironmentSettings settings)
        {
            base.InitializeTestEnvironmentSettings(settings);
            settings.XsdFilesDirectoryPath = TestConfiguration.ResolvePathFromAppSettings("xsdFilesDir");
            settings.CommsDirectoryPath = TestConfiguration.ResolvePathFromAppSettings("commsDir");
        }
    }
}
