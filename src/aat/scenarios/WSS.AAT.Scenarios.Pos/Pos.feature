﻿Feature: Pos

@Pos
Scenario: Type Of Sale And Tender Buttons
    When A Sale was made
    Then Tender Options file contains next data:
    | Tender Number | Tender Type Identifier | Tender Type Description | TTDS | Over Tender Allowed | Capture Credit Card Details | Use EFTPoS | Open Cash Drawer | Default remaining amount | Allow to Print Front of Cheque | Force Print of Back of Cheque | Minimum Value | Max Value | FLIM | In Use |
    | 01            | 1                      | Cash                    | 1    | True                | False                       | False      | True             | False                    | False                          | False                         | 0.00          | 0.00      | 0    | True   |
    | 05            | 2                      | Cheque                  | 2    | False               | False                       | True       | False            | True                     | True                           | True                          | NULL          | NULL      | NULL | True   |
    | 06            | 3                      | Cr/Dr Card              | 3    | False               | False                       | True       | False            | True                     | False                          | False                         | NULL          | NULL      | NULL | True   |
    | 07            | 7                      | Gift Token              | 7    | True                | False                       | False      | True             | False                    | False                          | False                         | NULL          | NULL      | NULL | True   |
    | 09            | 4                      | Old Tokens              | 6    | False               | False                       | False      | False            | True                     | False                          | False                         | NULL          | NULL      | NULL | True   |
    | 12            | 12                     | Web Tender              | 8    | False               | False                       | False      | False            | False                    | False                          | False                         | NULL          | NULL      | NULL | False  |
    | 13            | 13                     | Gift Card               | 6    | False               | False                       | False      | False            | True                     | False                          | False                         | NULL          | NULL      | NULL | True   |
    | 60            | 60                     | Account Sale            | 9    | False               | False                       | False      | False            | True                     | False                          | False                         | 0.00          | 0.00      | 0    | True   |
    | 66            | 3                      | No Card Present         | 3    | False               | False                       | True       | False            | True                     | False                          | False                         | NULL          | NULL      | NULL | True   |
    | 72            | 5                      | Project Loan            | 5    | False               | False                       | False      | False            | True                     | False                          | False                         | NULL          | NULL      | NULL | True   |
    | 80            | 6                      | Coupon                  | 7    | False               | False                       | False      | False            | True                     | False                          | False                         | 0.00          | 0.00      | 0    | True   |
    | 81            | 5                      | Project Loan            | 5    | False               | False                       | False      | False            | True                     | False                          | False                         | NULL          | NULL      | NULL | True   |
    | 82            | 50                     | H/O Refund              | 10   | False               | False                       | False      | False            | True                     | False                          | False                         | 0.00          | 0.00      | 0    | True   |
    And Tender Control file contains next data
    | Tender Type Identifier | Tender Type Description | In Use |
    | 1                      | Cash                    | True   |
    | 2                      | Cheque                  | True   |
    | 3                      | Cr/Dr Card              | True   |
    | 4                      | Old Tokens              | False  |
    | 5                      | Project Loan            | True   |
    | 6                      | Coupon                  | True   |
    | 7                      | Gift Token              | True   |
    | 8                      | Debit Card              | True   |
    | 9                      | Cash Round              | True   |
    | 11                     | EURO Cheque             | False  |
    | 12                     | Web Tender              | False  |
    | 13                     | Gift Card               | True   |
    | 21                     | Supp Voucher            | False  |
    | 40                     | Credit Note             | False  |
    | 41                     | Exchange                | False  |
    | 50                     | H/O Chq Refund          | False  |
    | 60                     | Account                 | False  |
    | 70                     | Maestro                 | False  |
    | 71                     | ATM Card                | False  |
    | 72                     | Mondex                  | False  |
    | 80                     | Mastercard              | False  |
    | 81                     | Visa                    | False  |
    | 82                     | Amex                    | False  |
    | 83                     | Diners Club             | False  |
    And Till TOS Contol file contains next data:
    | Sale Type Code | Sale Type Description | In Use | Z-Reads Update |
    | C-             | Misc Inc Correc       | True   | True           |
    | C+             | Paid Out Correc       | True   | True           |
    | CC             | Sign Off              | True   | True           |
    | CO             | Sign On               | True   | True           |
    | M-             | Paid Out              | True   | True           |
    | M+             | Misc. Income          | True   | True           |
    | OD             | Open Drawer           | True   | True           |
    | RC             | Refund Correct        | False  | True           |
    | RF             | Refund                | False  | True           |
    | RL             | Reprint Logo          | True   | False          |
    | RP             | Recover Parked        | True   | False          |
    | SA             | Sale                  | True   | True           |
    | SC             | Sale Correct          | False  | True           |
    | XR             | X-Read                | True   | False          |
    | ZR             | Z-Read                | True   | False          |
    And Till TOS Option records contain next data:
    | Sequence number | Sale Type Code | Sale Type Description | Document Number is Asked | Special Print is On | Open Cash Drawer On | Special Account Tender is Allowed | Supervisor Required |
    | 01              | SA             | Sale                  | False           | False               | True                | True                              | False               |
    | 02              | SC             | Sale Correction       | False           | False               | True                | True                              | False               |
    | 03              | RF             | Refund                | False           | False               | False               | True                              | False               |
    | 04              | RC             | Refund Correction     | False           | False               | False               | True                              | False               |
    | 05              | OD             | Open Drawer           | False           | False               | True                | False                             | True                |
    | 06              | M+             | Misc. Income          | True            | False               | True                | False                             | True                |
    | 07              | M-             | Paid Out              | True            | False               | True                | False                             | True                |
    | 08              | C+             | Misc Inc Correct      | True            | False               | True                | False                             | True                |
    | 09              | C-             | Paid Out Correct      | True            | False               | True                | False                             | True                |
    | 11              | RP             | Recall Transaction    | False           | False               | True                | False                             | False               |
    | 12              | RL             | Reprint Logo          | False           | False               | False               | False                             | False               |
    | 13              | EC             | Barcode Check         | False           | False               | False               | False                             | False               |
    | 14              | XR             | X-READ                | False           | False               | False               | False                             | True                |
    | 15              | ZR             | Z-READ                | False           | False               | False               | False                             | True                |
    | 98              | CO             | Sign On               | False           | False               | False               | False                             | False               |
    | 99              | CC             | Sign Off              | False           | False               | False               | False                             | False               |
    And Till TOS Tenders Allowed file contains next data
    | Sequence number | Tender Type |
    | 01              | 01          |
    | 01              | 02          |
    | 01              | 03          |
    | 01              | 04          |
    | 01              | 05          |
    | 01              | 06          |
    | 01              | 07          |
    | 01              | 10          |
    | 01              | 13          |
    | 01              | 41          |
    | 01              | 50          |
    | 01              | 60          |
    | 01              | 65          |
    | 01              | 66          |
    | 01              | 70          |
    | 01              | 77          |
    | 01              | 81          |
    | 01              | 84          |
    | 01              | 90          |
    | 02              | 01          |
    | 02              | 06          |
    | 02              | 07          |
    | 02              | 66          |
    | 02              | 70          |
    | 02              | 81          |
    | 03              | 01          |
    | 03              | 02          |
    | 03              | 06          |
    | 03              | 08          |
    | 03              | 10          |
    | 03              | 13          |
    | 03              | 66          |
    | 03              | 8           |
    | 03              | 81          |
    | 03              | 84          |
    | 04              | 01          |
    | 04              | 70          |
    | 04              | 81          |
    | 05              | 01          |
    | 05              | 05          |
    | 05              | 10          |
    | 05              | 70          |
    | 05              | 81          |
    | 06              | 01          |
    | 06              | 03          |
    | 06              | 05          |
    | 06              | 06          |
    | 06              | 20          |
    | 06              | 66          |
    | 07              | 01          |
    | 07              | 02          |
    | 07              | 03          |
    | 07              | 04          |
    | 07              | 06          |
    | 07              | 20          |
    | 07              | 66          |
    | 07              | 90          |
    | 08              | 01          |
    | 08              | 03          |
    | 08              | 06          |
    | 08              | 66          |
    | 09              | 01          |
    | 09              | 02          |
    | 09              | 03          |
    | 09              | 05          |
    | 09              | 06          |
    | 11              | 01          |
    | 30              | 70          |
