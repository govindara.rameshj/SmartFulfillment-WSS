﻿using Cts.Oasys.Core.Tests;
using NUnit.Framework;
using System;
using System.Linq;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.Pos
{
    [Binding]
    public class PosSteps : BaseStepDefinitions
    {
        private readonly PosRepository posRepository;

        public PosSteps(TestSystemEnvironment systemEnvironment, IDataLayerFactory dataLayerFactory)
        {
            posRepository = dataLayerFactory.Create<PosRepository>();
        }

        [When(@"A Sale was made")]
        public void WhenASaleWasMade()
        {
        }

        [Then(@"Tender Options file contains next data:")]
        public void ThenTenderOptionsFileContainsNextData(Table table)
        {
            var tenopts = posRepository.SelectTenderOptions();

            Assert.AreEqual(table.Rows.Count, tenopts.Count);

            foreach (var row in table.Rows)
            {
                var tenopt = tenopts.FirstOrDefault(x => x.TenderNumber == decimal.Parse(row["Tender Number"]));

                Assert.IsNotNull(tenopt);

                Assert.AreEqual(decimal.Parse(row["Tender Number"]), tenopt.TenderNumber);
                Assert.AreEqual(decimal.Parse(row["Tender Type Identifier"]), tenopt.TenderTypeIdentifier);
                Assert.AreEqual(row["Tender Type Description"], tenopt.TenderTypeDescription.Trim());
                Assert.AreEqual(decimal.Parse(row["TTDS"]), tenopt.TenderTypeDS);
                Assert.AreEqual(bool.Parse(row["Over Tender Allowed"]), tenopt.IsOverTenderAllowed);
                Assert.AreEqual(bool.Parse(row["Capture Credit Card Details"]), tenopt.CaptureCreditCardDetailsOn);
                Assert.AreEqual(bool.Parse(row["Use EFTPoS"]), tenopt.EFTPoSIsUsed);
                Assert.AreEqual(bool.Parse(row["Open Cash Drawer"]), tenopt.IsOpenCashDrawerNeeded);
                Assert.AreEqual(bool.Parse(row["Default remaining amount"]), tenopt.HasDefaultRemainingAmount);
                Assert.AreEqual(bool.Parse(row["Allow to Print Front of Cheque"]), tenopt.PrintChequeFrontChequeIsAllowed);
                Assert.AreEqual(bool.Parse(row["Force Print of Back of Cheque"]), tenopt.PrintChequeBackIsForced);
                Assert.AreEqual(row["Minimum Value"] == "NULL" ? 0.00m : decimal.Parse(row["Minimum Value"]), tenopt.MinimumValue);
                Assert.AreEqual(row["Max Value"] == "NULL" ? 0.00m : decimal.Parse(row["Max Value"]), tenopt.MaximumValue);
                Assert.AreEqual(row["FLIM"] == "NULL" ? 0 : decimal.Parse(row["FLIM"]), tenopt.FLimit);
                Assert.AreEqual(bool.Parse(row["In Use"]), tenopt.InUse);
            }
        }

        [Then(@"Tender Control file contains next data")]
        public void ThenTenderControlFileContainsNextData(Table table)
        {
            var tenctls = posRepository.SelectTenderControls();

            Assert.AreEqual(table.Rows.Count, tenctls.Count);

            foreach (var row in table.Rows)
            {
                var tenctl = tenctls.FirstOrDefault(x => x.TenderTypeIdentifier == decimal.Parse(row["Tender Type Identifier"]));

                Assert.IsNotNull(tenctl);
                Assert.AreEqual(decimal.Parse(row["Tender Type Identifier"]), tenctl.TenderTypeIdentifier);
                Assert.AreEqual(row["Tender Type Description"], tenctl.TenderTypeDescription.Trim());
                Assert.AreEqual(bool.Parse(row["In Use"]), tenctl.InUse);
            }
        }

        [Then(@"Till TOS Contol file contains next data:")]
        public void ThenTillTOSContolFileContainsNextData(Table table)
        {
            var tosctls = posRepository.SelectTillTosControls();

            Assert.AreEqual(table.Rows.Count, tosctls.Count);

            foreach (var row in table.Rows)
            {
                var tosctl = tosctls.FirstOrDefault(x => x.SaleTypeCode == row["Sale Type Code"]);

                Assert.IsNotNull(tosctl);
                Assert.AreEqual(row["Sale Type Code"], tosctl.SaleTypeCode);
                Assert.AreEqual(row["Sale Type Description"], tosctl.SaleTypeDescription.Trim());
                Assert.AreEqual(bool.Parse(row["In Use"]), tosctl.InUse);
                Assert.AreEqual(bool.Parse(row["Z-Reads Update"]), tosctl.UpdateZReads);
            }
        }

        [Then(@"Till TOS Option records contain next data:")]
        public void ThenTillTOSOptionRecordsContainNextData(Table table)
        {
            var tosopts = posRepository.SelectTillTosOptions();

            Assert.AreEqual(table.Rows.Count, tosopts.Count);

            foreach (var row in table.Rows)
            {
                var tosopt = tosopts.FirstOrDefault(x => x.SequenceNumber == row["Sequence number"]);

                Assert.IsNotNull(tosopt);

                Assert.AreEqual(row["Sequence number"], tosopt.SequenceNumber);
                Assert.AreEqual(row["Sale Type Code"], tosopt.SaleTypeCode);
                Assert.AreEqual(row["Sale Type Description"], tosopt.SaleTypeDescription.Trim());
                Assert.AreEqual(bool.Parse(row["Document Number is Asked"]), tosopt.DocumentNumberIsAsked);
                Assert.AreEqual(bool.Parse(row["Special Print is On"]), tosopt.SpecialPrintIsOn);
                Assert.AreEqual(bool.Parse(row["Open Cash Drawer On"]), tosopt.OpenCashDrawerOn);
                Assert.AreEqual(bool.Parse(row["Special Account Tender is Allowed"]), tosopt.SpecialAccountTenderIsAllowed);
                Assert.AreEqual(bool.Parse(row["Supervisor Required"]), tosopt.SupervisorRequired);
            }
        }

        [Then(@"Till TOS Tenders Allowed file contains next data")]
        public void ThenTillTOSTendersAllowedFileContainsNextData(Table table)
        {
            var tostens = posRepository.SelectTendersAllowed();

            Assert.AreEqual(table.Rows.Count, tostens.Count);

            foreach (var row in table.Rows)
            {
                var tosten = tostens.FirstOrDefault(x => x.SequenceNumber == row["Sequence number"] && x.TenderType.Trim() == row["Tender Type"]);

                Assert.IsNotNull(tosten);

                Assert.AreEqual(row["Sequence number"], tosten.SequenceNumber);
                Assert.AreEqual(row["Tender Type"], tosten.TenderType.Trim());
            }
        }
    }
}
