using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.FileParsers;
using WSS.AAT.Common.Utility.Executables;
using WSS.AAT.Common.Qod;
using WSS.AAT.Common.Qod.CtsOrderManager;
using WSS.AAT.Common.Utility.Bindings;
using WSS.AAT.Common.Utility.Configuration;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;
using System.Configuration;
using Cts.Oasys.Core.Tests;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Entities;
using WSS.AAT.Common.Banking;

namespace WSS.AAT.Scenarios.ClickAndCollect
{
    [Binding]
    public class QodSteps : BaseStepDefinitions
    {
        private readonly DateTime yesterday = DateTime.Today.AddDays(-1);
        private readonly TestSystemEnvironment systemEnvironment;
        private readonly IDataLayerFactory dataLayerFactory;
        private readonly TestMiscRepository miscRepo;
        private readonly TestNitmasRepository nitmasRepo;
        private readonly TestAlertRepository alertRepo;
        private readonly TestStockRepository stockRepo;
        private readonly TestCustomerOrderRepository orderRepo;

        private readonly string dsnName;
        private readonly string executablesDir;
        private readonly OrderReports orderReport;

        private static int ordersCountToSyncToOm;
        private static string orderNumber;
        private static string sourceOrderNumber;
        private readonly DateTime startDateTime;
        private static int storeId;

        private readonly CreationRoutine creationRoutine;
        private readonly FulfillmentRoutine fulfillmentRoutine;
        private readonly StatusNotificationRoutine statusNotificationRoutine;
        private readonly EnableRefundCreationRoutine enableRefundCreationRoutine;
        private readonly EnableStatusNotificationRoutine enableStatusNotificationRoutine;

        private readonly RtiRoutine rtiRoutine;

        public string StoreId
        {
            get { return dataLayerFactory.Create<IDictionariesRepository>().GetAbsoluteStoreId().ToString(); }
        }

        public QodSteps(TestSystemEnvironment systemEnvironment, TestConfiguration testConfiguration, IDataLayerFactory dataLayerFactory)
        {
            dsnName = ConfigurationManager.AppSettings["dsnName"];
            executablesDir = testConfiguration.ResolvePathFromAppSettings("executablesDir");
            this.systemEnvironment = systemEnvironment;
            this.dataLayerFactory = dataLayerFactory;

            miscRepo = this.dataLayerFactory.Create<TestMiscRepository>();
            nitmasRepo = this.dataLayerFactory.Create<TestNitmasRepository>();
            alertRepo = this.dataLayerFactory.Create<TestAlertRepository>();
            stockRepo = this.dataLayerFactory.Create<TestStockRepository>();
            orderRepo = this.dataLayerFactory.Create<TestCustomerOrderRepository>();

            creationRoutine = new CreationRoutine(this.dataLayerFactory, systemEnvironment);
            fulfillmentRoutine = new FulfillmentRoutine(this.dataLayerFactory, systemEnvironment);
            statusNotificationRoutine = new StatusNotificationRoutine(this.dataLayerFactory, systemEnvironment);
            enableRefundCreationRoutine = new EnableRefundCreationRoutine(this.dataLayerFactory, systemEnvironment);
            enableStatusNotificationRoutine = new EnableStatusNotificationRoutine(this.dataLayerFactory, systemEnvironment);

            orderReport = new OrderReports();

            rtiRoutine = new RtiRoutine(dsnName, executablesDir, ConfigurationManager.ConnectionStrings["Oasys"].ConnectionString, dataLayerFactory); 
            
            nitmasRepo.ClearNitlog();

            ordersCountToSyncToOm = 0;
            orderNumber = null;
            sourceOrderNumber = null;
            startDateTime = DateTime.Now;
        }

        #region Given

        [Given(@"all previous banking days are closed")]
        public void GivenAllPreviousBankingDaysAreClosed()
        {
            miscRepo.ClearOrderTables();
            miscRepo.ClearCashBalTables();
            miscRepo.UpdateSysdat(yesterday);
        }

        [Given(@"click and collect order is placed on a web site")]
        [Given(@"click and collect order with basket promotion is placed on a website")]
        public void GivenClickAndCollectOrderIsPlacedOnAWebSite()
        {
            ordersCountToSyncToOm = new AllQodStorage().GetOrdersCountToSyncToOM();
            creationRoutine.PrepareRequestForStandardClickAndCollectOrder();
        }

        [Given(@"order date is yesterday")]
        public void GivenOrderDateIsYesterday()
        {
            creationRoutine.SetSaleDate(yesterday);
        }

        [Given(@"click and collect order is placed on a website and fulfilled in the store")]
        public void GivenClickAndCollectOrderIsPlacedOnAWebsiteAndFulfilledInTheStore()
        {
            GivenClickAndCollectOrderIsPlacedOnAWebSite();
            WhenStoreReceivesIt();
        }

        [Given(@"click and collect order with the same source order number is placed on a web site")]
        public void GivenClickAndCollectOrderWithTheSameSourceOrderNumberIsPlacedOnAWebSite()
        {
            GivenClickAndCollectOrderIsPlacedOnAWebSite();
            creationRoutine.SetSourceOrderNumber(sourceOrderNumber);
        }

        [Given(@"click and collect order with the same source order number but different source is placed on a web site")]
        public void GivenClickAndCollectOrderWithTheSameSourceOrderNumberButDifferentSourceIsPlacedOnAWebSite()
        {
            GivenClickAndCollectOrderWithTheSameSourceOrderNumberIsPlacedOnAWebSite();
            creationRoutine.SetSource("T1");
        }

        [Given(@"click and collect order with yesterday date is fulfilled in the store")]
        public void GivenClickAndCollectOrderWithYesterdayDateIsFulfilledInTheStore()
        {
            creationRoutine.PrepareRequestForStandardClickAndCollectOrder();
            creationRoutine.SetSaleDate(yesterday);
            creationRoutine.SetReceivedDateAndTime(yesterday.AddHours(13).AddMinutes(45));
            creationRoutine.ForLineNumberSetPrice(1, 11);
            creationRoutine.ForLineNumberSetPrice(2, 21);
            creationRoutine.SetTotalPrice(45);
            WhenStoreReceivesIt();
        }

        [Given(@"one more click and collect order with yesterday date is fulfilled in the store")]
        public void GivenOneMoreClickAndCollectOrderWithYesterdayDateIsFulfilledInTheStore()
        {
            creationRoutine.PrepareRequestForStandardClickAndCollectOrder();
            creationRoutine.SetSaleDate(yesterday);
            creationRoutine.SetReceivedDateAndTime(yesterday.AddHours(23).AddMinutes(45));
            creationRoutine.ForLineNumberSetPrice(1, 12);
            creationRoutine.ForLineNumberSetPrice(2, 22);
            creationRoutine.SetTotalPrice(46);
            WhenStoreReceivesIt();
        }

        [Given(@"click and collect order with today date is fulfilled in the store")]
        public void GivenClickAndCollectOrderWithTodayDateIsFulfilledInTheStore()
        {
            creationRoutine.PrepareRequestForStandardClickAndCollectOrder();
            creationRoutine.SetSaleDate(DateTime.Today);
            creationRoutine.SetReceivedDateAndTime(DateTime.Now);
            creationRoutine.ForLineNumberSetPrice(1, 13);
            creationRoutine.ForLineNumberSetPrice(2, 23);
            creationRoutine.SetTotalPrice(47);
            WhenStoreReceivesIt();
        }

        [Given(@"click and collect order with (.*) days before today date is placed on a website")]
        public void GivenClickAndCollectOrderWithDaysBeforeTodayDateIsPlacedOnAWebsite(int num)
        {
            GivenClickAndCollectOrderIsPlacedOnAWebSite();
            creationRoutine.SetSaleDate(DateTime.Today.AddDays(-num));
        }

        [Given(@"order is placed in selling store and fulfilled in the store")]
        public void GivenOrderIsPlacedInSellingStoreAndFulfilledInTheStore()
        {
            GivenOrderIsPlacedInSellingStore();
            WhenFulfillingStoreReceivesTheOrder();
        }

        [Given(@"full cancellation with yesterday date is requested")]
        public void GivenFullCancellationWithYesterdayDateIsRequested()
        {
            enableRefundCreationRoutine.PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);
            enableRefundCreationRoutine.SetRefundDate(yesterday);
        }

        [Given(@"partial cancellation is requested")]
        public void GivenPartialCancellationIsRequested()
        {
            enableRefundCreationRoutine.PreparePartialRequestForStandardClickAndCollectOrder(sourceOrderNumber);
        }
        
        [Given(@"partial cancellation with yesterday date is requested")]
        public void GivenPartialCancellationWithYesterdayDateIsRequested()
        {
            GivenPartialCancellationIsRequested();
            enableRefundCreationRoutine.SetRefundDate(yesterday);
        }

        [Given(@"product1 amount is a")]
        public void GivenProductAmountIsA()
        {
            creationRoutine.ForLineNumberSetPrice(1, 11);
        }

        [Given(@"product2 amount is b")]
        public void GivenProductAmountIsB()
        {
            creationRoutine.ForLineNumberSetPrice(2, 21);
        }

        [Given(@"total order amount is a\+b-x")]
        public void GivenTotalOrderAmountIsAB_X()
        {
            creationRoutine.SetTotalPrice(49);
        }

        [Given(@"total amount is (.*)")]
        public void GivenTotalAmountIs(Decimal amount)
        {
            creationRoutine.SetTotalPrice(amount);
        }

        [Given(@"total refund amount is (.*)")]
        public void GivenTotalRefundAmountIs(Decimal amount)
        {
            enableRefundCreationRoutine.SetRefundTotal(amount);
        }

        [Given(@"order is placed in selling store")]
        public void GivenOrderIsPlacedInSellingStore()
        {
            fulfillmentRoutine.PrepareRequestForStandardVendaOrder();
        }

        [Given(@"order fulfilled in the store")]
        public void GivenOrderFulfilledInTheStore()
        {
            WhenStoreReceivesIt();
        }

        [Given(@"click and collect order is placed on a website and store order without delivery is placed on a Till")]
        public void GivenClickAndCollectOrderIsPlacedOnAWebsiteAndStoreOrderWithoutDeliveryIsPlacedOnATill()
        {
            var banking = new CnCBanking(dataLayerFactory, systemEnvironment);
            banking.CreateTrainingTillSaleOfSkuPriceSoldOn(1, "100463", 29.99m, DateTime.Today.AddDays(-2));
            banking.CreateVoidTillSaleOfSkuPriceSoldOn(1, "500300", 33.97m, DateTime.Today.AddDays(-2));
            banking.CreateTillSaleOfSku1Price1AndReversedSku2Price2SoldOn(1, "500300", 33.97m, "100463", 29.99m, DateTime.Today.AddDays(-2));
            banking.CreateTillSaleOfSkuPriceSoldOn(5, "100459", 4.99m, DateTime.Today.AddDays(-2));
            banking.CreateTillSaleOfSkuPriceSoldOn(1, "424995", 113.99m, yesterday);
            banking.CreateTillSaleOfSkuPriceSoldOn(13, "200017", 219.50m, yesterday);
            banking.CreateCncOrderOfSku1Price1AndSku2Price2SoldOnReceivedOnAt("607401", 3.51m, "607402", 3.51m, DateTime.Today.AddDays(-5), DateTime.Today.AddDays(-2).AddHours(15).AddMinutes(30));
            banking.CreateCncOrderOfSku1Price1AndSku2Price2SoldOnReceivedOnAt("100463", 29.99m, "780296", 50.00m, DateTime.Today.AddDays(-5), DateTime.Today.AddDays(-2).AddHours(15).AddMinutes(30));
            banking.CreateCncOrderOfSkuPriceSoldOnReceivedOnAt("305082", 238.00m, DateTime.Today.AddDays(-5), yesterday.AddHours(15).AddMinutes(30));
        }

        [Given(@"source is non Hybris")]
        public void GivenSourceIsNonHybris()
        {
            creationRoutine.SetSource("WO");
        }

        [Given(@"nightly routine procedure is started")]
        public void GivenNightlyRoutineProcedureIsStarted()
        {
            nitmasRepo.AddTaskToRepo(NitmasTaskId.NitmasStart);
        }

        [Given(@"nightly routine procedure is finished")]
        public void GivenNightlyRoutineProcedureIsFinished()
        {
            GivenNightlyRoutineProcedureIsStarted();
            nitmasRepo.AddTaskToRepo(NitmasTaskId.NitmasEnd);
        }

        [Given(@"nightly routine procedure is finished for yesterday")]
        public void GivenNightlyRoutineProcedureIsFinishedForYesterday()
        {
            nitmasRepo.AddTaskToRepoForDate(NitmasTaskId.NitmasStart, yesterday);
            nitmasRepo.AddTaskToRepoForDate(NitmasTaskId.Undefined405, yesterday);

            var banking = new CoreBanking(systemEnvironment);

            var users = banking.GetActiveUserIds();
            banking.setLoggedOnUserAndAuthoriser(users[0], users[1]);

            banking.setBankingDate(DateTime.Today);
            banking.createInitialSafeWithValueAndDate(10000m, DateTime.Today);

            var pt = new ProcessTransmissions(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.RunNightlyRetailUpdates();
            pt.ProcessFileCode("OT");
        }

        [Given(@"there was no nightly routine for previous days")]
        public void GivenThereWasNoNightlyRoutineForPreviousDays()
        {
            miscRepo.ClearStockLog();
            nitmasRepo.ClearNitlog();
        }

        [Given(@"nightly routine for previous days was passed in the evening")]
        public void GivenNightlyRoutineForPreviousDaysWasPassedInTheEvening()
        {
            GivenThereWasNoNightlyRoutineForPreviousDays();
            nitmasRepo.AddTaskToRepoForDatePassedAtTime(NitmasTaskId.ProcessTransHostg, DateTime.Today.AddDays(-3), DateTime.Today.AddDays(-3).AddHours(22).AddMinutes(53));
            nitmasRepo.AddTaskToRepoForDatePassedAtTime(NitmasTaskId.ProcessTransHostg, DateTime.Today.AddDays(-2), DateTime.Today.AddDays(-2).AddHours(23).AddMinutes(01));
        }

        [Given(@"nightly routine for previous days was passed in the morning")]
        public void GivenNightlyRoutineForPreviousDaysWasPassedInTheMorning()
        {
            GivenThereWasNoNightlyRoutineForPreviousDays();
            nitmasRepo.AddTaskToRepoForDatePassedAtTime(NitmasTaskId.ProcessTransHostg, DateTime.Today.AddDays(-3), DateTime.Today.AddDays(-2).AddHours(12).AddMinutes(01));
            nitmasRepo.AddTaskToRepoForDatePassedAtTime(NitmasTaskId.ProcessTransHostg, DateTime.Today.AddDays(-2), DateTime.Today.AddDays(-1).AddHours(2).AddMinutes(15));
        }

        [Given(@"regular sales are made")]
        public void GivenRegularSalesAreMade()
        {
            stockRepo.InsertIntoStockLog(0, "01", DateTime.Today.AddDays(-2).AddHours(13).AddMinutes(15), "800", "100500", 150, 0, -3);
            stockRepo.InsertIntoStockLog(0, "02", DateTime.Today.AddDays(-2).AddHours(13).AddMinutes(17), "800", "100500", 50, -3, -2);
        }

        [Given(@"click and collect orders are fulfilled")]
        public void GivenClickAndCollectOrdersAreFulfilled()
        {
            stockRepo.InsertIntoStockLog(0, "06", DateTime.Today.AddDays(-3).AddHours(22).AddMinutes(45), "499", "100500", 50, 4, 3);
            stockRepo.InsertIntoStockLog(0, "06", DateTime.Today.AddDays(-3).AddHours(23).AddMinutes(10), "499", "100500", 150, 3, 0);
            stockRepo.InsertIntoStockLog(0, "06", DateTime.Today.AddDays(-2).AddHours(10).AddMinutes(15), "499", "100500", 50, -2, -12);
            stockRepo.InsertIntoStockLog(0, "07", DateTime.Today.AddDays(-2).AddHours(14).AddMinutes(17), "499", "100500", 100, -12, -10);
            stockRepo.InsertIntoStockLog(1, "07", DateTime.Today.AddDays(-2).AddHours(1).AddMinutes(3), "499", "100500", 150, -10, -7);
            stockRepo.InsertIntoStockLog(0, "06", DateTime.Today.AddDays(-1).AddHours(10).AddMinutes(11), "499", "100500", 50, -7, -8);
        }

        [Given(@"Click and Collect order was fulfilled in the store")]
        public void GivenClickAndCollectOrderWasFulfilledInTheStore()
        {
            creationRoutine.PrepareRequestForStandardClickAndCollectOrder();
        }

        #endregion

        [When(@"click and collect order is placed on a website and fulfilled in the store")]
        public void WhenClickAndCollectOrderIsPlacedOnAWebsiteAndFulfilledInTheStore()
        {
            GivenClickAndCollectOrderIsPlacedOnAWebsiteAndFulfilledInTheStore();
        }

        [When(@"order is partially cancelled")]
        public void WhenOrderIsPartiallyCancelled()
        {
            GivenPartialCancellationIsRequested();
            WhenCancellationRequestIsProcessed();
        }

        [When(@"order is fully cancelled")]
        public void WhenOrderIsFullyCancelled()
        {
            enableRefundCreationRoutine.PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);
            WhenCancellationRequestIsProcessed();
        }

        [When(@"order is completely cancelled")]
        public void WhenOrderIsCompletelyCancelled()
        {
            enableRefundCreationRoutine.PrepareCompleteRequestWithoutPartialForStandardClickAndCollectOrder(sourceOrderNumber);
            WhenCancellationRequestIsProcessed();
        }

        [When(@"cancellation request is processed")]
        public void WhenCancellationRequestIsProcessed()
        {
            enableRefundCreationRoutine.ProcessPreparedRequest();
            CheckThatRequestWasSuccessfull(enableRefundCreationRoutine);
        }

        [When(@"fulfilling store receives the order")]
        public void WhenFulfillingStoreReceivesTheOrder()
        {
            fulfillmentRoutine.ProcessPreparedRequest();
            CheckThatRequestWasSuccessfull(fulfillmentRoutine);

            orderNumber = fulfillmentRoutine.CurrentResponse.FulfillingSiteOrderNumber.Value;
            sourceOrderNumber = fulfillmentRoutine.CurrentResponse.OrderHeader.SourceOrderNumber.Value;
        }

        [Then(@"order is self fulfilled in the store")]
        public void ThenOrderIsSelfFulfilledInTheStore()
        {
            var order = QodOrder.Get(orderNumber);

            Assert.That(order.Header.IsForDelivery, Is.False);
            Assert.That(order.Header.TranTill, Is.Not.Null);
            Assert.That(order.Header.TranNumber, Is.Not.Null);

            Assert.That(order.Header.DeliveryStatus, Is.EqualTo(300));
            Assert.That(order.Header.DateOrder, Is.EqualTo(yesterday));
            Assert.That(order.QodSale.SaleTenders.First().TenderType, Is.EqualTo(12));
            Assert.That(order.Header.Lines[0].DeliveryStatus, Is.EqualTo(300));
            Assert.That(order.Header.Lines[0].DeliverySource, Is.EqualTo(StoreId));
        }

        [Then(@"order is fulfilled in the store")]
        public void ThenItIsFulfilledInTheStore()
        {
            var order = QodOrder.Get(orderNumber);

            Assert.That(order.Header.DeliveryStatus, Is.EqualTo(300));
            Assert.That(order.Header.Lines[0].DeliveryStatus, Is.EqualTo(300));
            Assert.That(order.Header.Lines[0].DeliverySource, Is.EqualTo(StoreId));
        }

        [Then(@"till and cashier fields are filled properly for new order")]
        public void ThenTillAndCashierFieldsAreFilledProperlyForNewOrder()
        {
            var order = QodOrder.Get(orderNumber);

            Assert.That(order.Header.TranTill, Is.EqualTo("90"));
            Assert.That(order.QodSale.TillNumber, Is.EqualTo("90"));
            Assert.That(order.QodSale.CashierID, Is.EqualTo("499"));
            Assert.That(order.QodSale.SaleTenders[0].TillNumber, Is.EqualTo("90"));
            foreach (var line in order.QodSale.SaleLines)
            {
                Assert.That(line.TillNumber, Is.EqualTo("90"));
            }
        }

        [Then(@"till and cashier fields are filled properly for order cancellation")]
        public void ThenTillAndCashierFieldsAreFilledProperlyForOrderCancellation()
        {
            var order = QodOrder.Get(orderNumber);

            Assert.That(order.Header.RefundTill, Is.EqualTo("90"));
            Assert.That(order.Header.Refunds[0].RefundTill, Is.EqualTo("90"));
            Assert.That(order.RefundSale.TillNumber, Is.EqualTo("90"));
            Assert.That(order.RefundSale.CashierID, Is.EqualTo("499"));
            Assert.That(order.RefundSale.SaleTenders[0].TillNumber, Is.EqualTo("90"));
            Assert.That(order.QodSale.SaleLines[0].TillNumber, Is.EqualTo("90"));
        }
        
        [Then(@"order is fully cancelled")]
        public void ThenOrderIsFullyCancelled()
        {
            var order = QodOrder.Get(orderNumber);

            Assert.That(order.Header.DeliveryStatus, Is.EqualTo(899));
            Assert.That(order.Header.RefundStatus, Is.EqualTo(199));
            Assert.That(order.Header.Status, Is.EqualTo("1"));
            Assert.That(order.Header.QtyRefunded, Is.EqualTo(-3));
            Assert.That(order.Header.RefundTranDate, Is.EqualTo(yesterday));

            foreach (var line in order.Header.Lines)
                Assert.That(line.QtyRefunded, Is.EqualTo(-line.QtyOrdered));

            foreach (var item in order.Header.Refunds)
            {
                Assert.That(item.QtyCancelled, Is.EqualTo(order.Header.Lines.Find(item.Number).QtyOrdered));
                Assert.That(item.RefundDate, Is.EqualTo(yesterday));
            }

            var totalAmount = enableRefundCreationRoutine.CurrentResponse.RefundHeader.RefundTotal.Value;
            Assert.That(order.RefundSale.TotalSaleAmount, Is.EqualTo(-totalAmount));
            Assert.That(order.RefundSale.SaleTenders.First().TenderAmount, Is.EqualTo(totalAmount));
            Assert.That(order.RefundSale.SaleTenders.First().TenderType, Is.EqualTo(12M));
        }

        [Then(@"order is partially cancelled")]
        public void ThenOrderIsPartiallyCancelled()
        {
            var order = QodOrder.Get(orderNumber);

            Assert.That(order.Header.DeliveryStatus, Is.EqualTo(300));
            Assert.That(order.Header.RefundStatus, Is.EqualTo(199));
            Assert.That(order.Header.Status, Is.EqualTo("2"));
            Assert.That(order.Header.QtyRefunded, Is.EqualTo(-1));
            Assert.That(order.Header.RefundTranDate, Is.EqualTo(yesterday));

            Assert.That(order.Header.Lines[0].QtyRefunded, Is.EqualTo(0));
            Assert.That(order.Header.Lines[1].QtyRefunded, Is.EqualTo(-1));

            Assert.That(order.Header.Refunds[0].QtyCancelled, Is.EqualTo(1));
            Assert.That(order.Header.Refunds[0].RefundDate, Is.EqualTo(yesterday));

            var totalAmount = enableRefundCreationRoutine.CurrentResponse.RefundHeader.RefundTotal.Value;
            Assert.That(order.RefundSale.TotalSaleAmount, Is.EqualTo(-totalAmount));
            Assert.That(order.RefundSale.SaleTenders.First().TenderAmount, Is.EqualTo(totalAmount));
            Assert.That(order.RefundSale.SaleTenders.First().TenderType, Is.EqualTo(12M));
        }

        [When(@"store receives order status update")]
        public void WhenStoreReceivesOrderStatusUpdate()
        {
            enableStatusNotificationRoutine.CreateStandardRequest(sourceOrderNumber, "002");
            enableStatusNotificationRoutine.ProcessPreparedRequest();
            CheckThatRequestWasSuccessfull(enableStatusNotificationRoutine);
        }

        [Then(@"order status is updated")]
        public void ThenOrderStatusIsUpdated()
        {
            var order = QodOrder.Get(orderNumber);

            Assert.That(order.Header.DeliveryStatus, Is.EqualTo(499));
            Assert.That(order.Header.Lines[0].DeliveryStatus, Is.EqualTo(499));
        }

        [When(@"Store receives the order")]
        [When(@"store receives the order")]
        public void WhenStoreReceivesIt()
        {
            creationRoutine.ProcessPreparedRequest();
            CheckThatRequestWasSuccessfull(creationRoutine);

            orderNumber = creationRoutine.CurrentResponse.OrderHeader.StoreOrderNumber.Value;
            sourceOrderNumber = creationRoutine.CurrentResponse.OrderHeader.SourceOrderNumber.Value;
        }

        [When(@"Store Id was saved")]
        public void WhenStoreIdWasSaved()
        {
            var singleQodStorage = new SingleQodStorage(orderNumber);
            storeId = singleQodStorage.GetStoreId();
        }

        [When(@"Customer Order Header table value fields were loaded")]
        public void WhenCustomerOrderHeaderTableValueFieldsWereLoaded()
        {
            rtiRoutine.LoadCORHDRValueFields(orderNumber);
            rtiRoutine.RunRTIBulkSend();
        }

        /// <summary>
        /// Used for requests that are going to fail.
        /// </summary>
        [When(@"fulfillment is requested")]
        public void WhenFulfillmentIsRequested()
        {
            creationRoutine.ProcessPreparedRequest();
        }

        [When(@"store receives normal order status update")]
        public void WhenStoreReceivesNormalOrderStatusUpdate()
        {
            var order = QodOrder.Get(orderNumber);

            statusNotificationRoutine.PrepareStandardRequest(order.Header.OmOrderNumber.ToString(), "499", orderNumber);
            statusNotificationRoutine.ProcessPreparedRequest();
            CheckThatRequestWasSuccessfull(statusNotificationRoutine);
        }

        [When(@"cancellation with non existing product is requested")]
        public void WhenCancellationWithNonExistingProductIsRequested()
        {
            enableRefundCreationRoutine.PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);
            enableRefundCreationRoutine.SetRefundDate(yesterday);
            enableRefundCreationRoutine.SetRefundProductCode("999999");
            enableRefundCreationRoutine.ProcessPreparedRequest();
        }

        [When(@"stock movements and inventory updates are calculated during nightly routine")]
        public void WhenStockMovementsAndInventoryUpdatesAreCalculatedDuringNightlyRoutine()
        {
            var pt = new ProcessTransmissions(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.ProcessFileCode("RF", yesterday);
        }

        [When(@"flash totals are calculated during nightly routine")]
        public void WhenFlashTotalsAreCalculatedDuringNightlyRoutine()
        {
            var pt = new ProcessTransmissions(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.ProcessFileCodeFromDateToDate("OF", DateTime.Today.AddDays(-2), yesterday);
        }

        [Then(@"stock and sale order details are included into STHPA file")]
        public void ThenStockAndSaleOrderDetailsAreIncludedIntoSthpaFile()
        {
            var parser = new SthpaFileParser(systemEnvironment);

            Assert.That(parser.IsLineForSkuExists("100012"), Is.True);
            Assert.That(parser.GetCountForSku("100012"), Is.EqualTo(1));
            Assert.That(parser.GetAmountForSku("100012"), Is.EqualTo(4.05));
            Assert.That(parser.IsLineForSkuExists("100013"), Is.True);
            Assert.That(parser.GetCountForSku("100013"), Is.EqualTo(2));
            Assert.That(parser.GetAmountForSku("100013"), Is.EqualTo(19.02));
            Assert.That(parser.IsLineForSkuExists("100014"), Is.False);
        }

        [Then(@"results are included into flash totals file")]
        public void ThenResultsAreIncludedIntoFlashTotalsFile()
        {
            var parser = new SthofFileParser(systemEnvironment);

            DateTime dt2 = DateTime.Today.AddDays(-2);
            DateTime dt1 = yesterday;
            Assert.That(parser.GetSalesInF3(dt2), Is.EqualTo(25.07));
            Assert.That(parser.GetRefundsInF3(dt2), Is.EqualTo(0.00));
            Assert.That(parser.GetSalesInF3(dt1), Is.EqualTo(0.00));
            Assert.That(parser.GetRefundsInF3(dt1), Is.EqualTo(-9.51));
            Assert.That(parser.GetAmountInF4ForTender(dt2, 11), Is.EqualTo(0.00));
            Assert.That(parser.GetAmountInF4ForTender(dt1, 11), Is.EqualTo(0.00));
            Assert.That(parser.GetCountInF5ForTender(dt2, 12), Is.EqualTo(0));
            Assert.That(parser.GetAmountInF4ForTender(dt1, 12), Is.EqualTo(0));

            Assert.That(parser.GetSalesAmountInF7(dt2), Is.EqualTo(0.00));
            Assert.That(parser.GetSalesCountInF7(dt2), Is.EqualTo(0));

            Assert.That(parser.GetRefundsAmountInF7(dt2), Is.EqualTo(0.00));
            Assert.That(parser.GetRefundsCountInF7(dt2), Is.EqualTo(0));

            Assert.That(parser.GetSalesAmountInF7(dt1), Is.EqualTo(0.00));
            Assert.That(parser.GetSalesCountInF7(dt1), Is.EqualTo(0));

            Assert.That(parser.GetRefundsAmountInF7(dt1), Is.EqualTo(0.00));
            Assert.That(parser.GetRefundsCountInF7(dt1), Is.EqualTo(0));
        }

        [Then(@"click and collect virtual cashier balance (increases|decreases)")]
        public void ThenClickAndCollectVirtualCashierBalanceIncreases(string incOrDec)
        {
            var order = QodOrder.Get(orderNumber);
            
            var cb = new CashBalance(dataLayerFactory, order.QodSale.CashierID, order.QodSale.TillNumber, DateTime.Today, DateTime.Today);
            Assert.True(cb.CashierExists());

            if (incOrDec == "increases")
            {
                Assert.That(cb.CashierGrossSalesAmount(), Is.EqualTo(14.00));
                Assert.That(cb.CashierSalesCount(), Is.EqualTo(1));
                Assert.That(cb.CashierSalesAmount(), Is.EqualTo(14.00));
                Assert.That(cb.CashierRefundCount(), Is.EqualTo(0.00));
                Assert.That(cb.CashierRefundAmount(), Is.EqualTo(0.00));
                Assert.That(cb.CashierNumTransactions(), Is.EqualTo(1));
                Assert.That(cb.CashierTenExists(), Is.True);
                Assert.That(cb.CashierTenAmount(), Is.EqualTo(14.00));
                Assert.That(cb.CashierTenQuantity(), Is.EqualTo(1));
                Assert.That(cb.CashierTenVarExists(), Is.True);
                Assert.That(cb.CashierTenVarTradingPeriodCorrect(), Is.True);
                Assert.That(cb.CashierTenVarAmount(), Is.EqualTo(14.00));
                Assert.That(cb.CashierTenVarQuantity(), Is.EqualTo(1));
                Assert.That(cb.CashierTenVarRelatedExists(), Is.False);
            }
            else
            {
                Assert.That(cb.CashierGrossSalesAmount(), Is.EqualTo(221.00));
                Assert.That(cb.CashierSalesCount(), Is.EqualTo(1));
                Assert.That(cb.CashierSalesAmount(), Is.EqualTo(321.00));
                Assert.That(cb.CashierRefundCount(), Is.EqualTo(1));
                Assert.That(cb.CashierRefundAmount(), Is.EqualTo(-100.00));
                Assert.That(cb.CashierNumTransactions(), Is.EqualTo(2));
                Assert.That(cb.CashierTenAmount(), Is.EqualTo(221.00));
                Assert.That(cb.CashierTenQuantity(), Is.EqualTo(2));
                Assert.That(cb.CashierTenVarAmount(), Is.EqualTo(-100.00));
                Assert.That(cb.CashierTenVarQuantity(), Is.EqualTo(1));
                Assert.That(cb.CashierTenVarRelatedAmount(), Is.EqualTo(321.00));
                Assert.That(cb.CashierTenVarRelatedQuantity(), Is.EqualTo(1));
            }
        }

        [Then(@"order cancellation is not processed")]
        public void ThenOrderCancellationIsNotProcessed()
        {
            CheckThatRequestWasNotSuccessfull(enableRefundCreationRoutine);
        }

        [Then(@"status update is not processed")]
        public void ThenStatusUpdateIsNotProcessed()
        {
            CheckThatRequestWasNotSuccessfull(enableStatusNotificationRoutine);
        }

        [Then(@"amounts are reflected in financial reporting")]
        public void AmountsAreReflectedInFinancialReporting()
        {
            // Query totals
            var dash = dataLayerFactory.Create<TestMiscRepository>().GetDashSalesReport(DateTime.Today).ToList();
            
            var cncSalesRow = dash.First(row => row.Description == "C&C Sales");
            Assert.AreEqual(1, cncSalesRow.Qty);
            Assert.AreEqual(1, cncSalesRow.QtyWtd);
            Assert.AreEqual(49.00m, cncSalesRow.Value);
            Assert.AreEqual(49.00m, cncSalesRow.ValueWtd);

            var allSalesRow = dash.First(row => row.Description == "Core Sales + K&B Sales + C&C Sales");
            Assert.AreEqual(1, allSalesRow.Qty);
            Assert.AreEqual(1, allSalesRow.QtyWtd);
            Assert.AreEqual(49.00m, allSalesRow.Value);
            Assert.AreEqual(49.00m, allSalesRow.ValueWtd);

            // Check ledger output
            {
                var banking = new CoreBanking(systemEnvironment);

                var users = banking.GetActiveUserIds();
                banking.setLoggedOnUserAndAuthoriser(users[0], users[1]);

                banking.setBankingDate(DateTime.Today);
                banking.createInitialSafeWithValue(10000);

                var pt = new ProcessTransmissions(systemEnvironment, executablesDir);
                pt.ClearTransmissionFiles();
                pt.RunNightlyRetailUpdates();

                pt.ProcessFileCode("PO SOCLOS=STHPO", DateTime.Today);
                pt.ProcessFileCode("SA", DateTime.Today);
                pt.ProcessFileCode("DB", DateTime.Today);
                pt.ProcessFileCode("OT", DateTime.Today);

                var parser = new SthoaFileParser(systemEnvironment);
                Assert.AreEqual(49.00m, parser.GetA8ValueByAccountOn("8498", DateTime.Today));
                Assert.AreEqual(1, parser.GetA5DailyUnitsForSKU("100012"));
                Assert.AreEqual(11.00m, parser.GetA5DailyTotalForSKU("100012"));
                Assert.AreEqual(2, parser.GetA5DailyUnitsForSKU("100013"));
                Assert.AreEqual(42.00m, parser.GetA5DailyTotalForSKU("100013"));
            }
        }

        [When(@"the first day is closed")]
        public void WhenTheFirstDayIsClosed()
        {
            var banking = new CoreBanking(systemEnvironment);

            var users = banking.GetActiveUserIds();
            banking.setLoggedOnUserAndAuthoriser(users[0], users[1]);

            banking.setBankingDate(DateTime.Today.AddDays(-2));
            banking.createInitialSafeWithValueAndDate(10000m, DateTime.Today.AddDays(-2));
            banking.configurePickupWithDenominations("\u00a333.97 (\u00a320:1,\u00a310:1,\u00a31:3,10p:9,1p:7)");
            banking.createPickupWithSealForCashier("33333331", 800);
            banking.createPickupForCncCashier(499);
            banking.performEndOfDayBankingWithThisSealThisSlipThisCashValueThisCashiers("555-55555555-5", "123", "\u00a330 (\u00a320:1,\u00a310:1)", "499,800");

            var pt = new ProcessTransmissions(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.ProcessFileCode("DB", DateTime.Today.AddDays(-2));
        }

        [When(@"the second day is closed")]
        public void WhenTheSecondDayIsClosed()
        {
            var banking = new CoreBanking(systemEnvironment);

            var users = banking.GetActiveUserIds();
            banking.setLoggedOnUserAndAuthoriser(users[0], users[1]);

            banking.setBankingDate(yesterday);
            banking.createInitialSafeWithValueAndDate(10000m, yesterday);
            banking.configurePickupWithDenominations("\u00a3113.99 (\u00a350:2,\u00a31:13,10p:9,1p:9)");
            banking.createPickupWithSealForCashier("33333332", 800);
            banking.createPickupForCncCashier(499);
            banking.performEndOfDayBankingWithThisSealThisSlipThisCashValueThisCashiers("777-77777777-7", "125", "\u00a3100 (\u00a350:2)", "499,800");

            var pt = new ProcessTransmissions(systemEnvironment, executablesDir);
            pt.ProcessFileCode("DB", yesterday);
        }

        [When(@"process transmissions period for first day is run")]
        public void WhenProcessTransmissionsPeriodForFirstDayIsRun()
        {
            var pt = new ProcessTransmissionPeriod(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.ProcessFileCodeForDate("DB", DateTime.Today.AddDays(-2));
        }

        [When(@"process transmissions period for both days is run")]
        public void WhenProcessTransmissionsPeriodForBothDaysIsRun()
        {
            var pt = new ProcessTransmissionPeriod(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.ProcessFileCodeForDate("DB", DateTime.Today.AddDays(-2));
            pt.ProcessFileCodeForDate("DB", yesterday);
        }

        [Then(@"both orders are included into financial reporting")]
        public void ThenBothOrdersAreIncludedIntoFinancialReporting()
        {
            {
                var fp = new SthoaFileQuery(systemEnvironment, "A8", DateTime.Today.AddDays(-2));

                var goodValues = new Dictionary<object, object>() {
                    {8499, 0.00m},
                    {8440, 0.00m},
                    {9001, 10003.97m},
                    {8498, 87.01m},
                    {8409, 0.00m},
                    {8413, 0.00m},
                    {8411, 0.00m},
                    {8410, 0.00m},
                    {8406, 4.99m},
                    {8404, 0.00m},
                    {8402, 0.00m},
                    {8401, 0.00m},
                    {8415, 33.97m},
                    {3009, 0.00m},
                    {8435, 0.97m},
                    {8434, 3.00m},
                    {8433, 0.00m},
                    {8432, 10.00m},
                    {8431, 20.00m},
                    {8430, 0.00m},
                    {6509, 0.00m},
                    {8443, 0.00m},
                    {8442, 87.01m},
                    {8438, 0.00m},
                    {8436, 0.00m},
                    {8429, 0.00m},
                    {8428, 0.00m},
                    {8427, 0.00m},
                    {8426, 0.00m},
                    {8425, 0.00m},
                    {8424, 4.99m},
                    {8423, 0.00m},
                    {8422, 0.00m},
                    {8419, 0.00m},
                    {8421, 0.00m},
                    {8418, 0.00m},
                    {8417, 0.00m},
                    {8416, 0.00m},
                    {1210, -34.98m},
                    {1250, -33.97m},
                    {1260, -7.02m},
                    {1278, -50.00m},
                    {8213, 10.00m},
                    {8212, 20.00m},
                    {8199, 0.00m},
                    {8198, 87.01m},
                    {8324, 0.00m},
                    {8323, 0.00m},
                    {8232, 0.00m},
                    {8231, 0.00m},
                    {8132, 0.00m},
                    {8131, 0.00m},
                    {8441, 0.00m},
                    {8251, 0.00m},
                    {8144, 4.99m},
                    {8143, 0.00m},
                    {8142, 0.00m},
                    {8121, 0.00m},
                    {8141, 0.00m},
                    {8112, 0.00m},
                    {8111, 0.00m},
                    {8221, 0.00m},
                    {8230, 0.00m},
                    {8130, 0.00m},
                    {8140, 0.00m},
                    {8120, 0.00m},
                    {8110, 0.00m},
                    {9002, 0.00m},
                    {9003, 0.00m},
                    {9004, 0.00m},
                    {9005, 0.00m},
                    {9006, 0.00m},
                    {9007, 0.00m},
                    {9008, 0.00m},
                    {9009, 0.00m},
                    {9010, 0.00m},
                    {9011, 0.00m},
                    {9012, 0.00m},
                    {9013, 0.00m},
                    {9014, 0.00m},
                    {9015, 0.00m},
                    {9016, 0.00m},
                    {9017, 0.00m},
                    {9018, 0.00m},
                    {9019, 0.00m},
                    {9020, 0.00m},
                };
                CheckSthoaFileQueryResult(goodValues, fp);
            }

            {
                var fp = new SthoaFileQuery(systemEnvironment, "AH", DateTime.Today.AddDays(-2));

                var goodValues = new Dictionary<object, object> {
                    {"000009", -4.99m},
                    {"000016", -33.97m},
                    {"000020", -50.00m},
                    {"098054", -7.02m},
                    {"098455", -29.99m},
                };
                CheckSthoaFileQueryResult(goodValues, fp);
            }
        }

        private void CheckSthoaFileQueryResult(Dictionary<object, object> expected, SthoaFileQuery fp)
        {
            var actual = fp.query();

            foreach (var kv in expected)
            {
                object foundValue = actual
                    .Where(x => x[0][1].ToString().Equals(kv.Key.ToString()))
                    .Select(x => x[1][1])
                    .FirstOrDefault();

                Assert.NotNull(foundValue);
                Assert.AreEqual(kv.Value, foundValue);
            }
        }

        [Then(@"both orders are included into financial reporting with yesterday date")]
        public void ThenBothOrdersAreIncludedIntoFinancialReportingWithYesterdayDate()
        {
            ThenBothOrdersAreIncludedIntoFinancialReporting();

            {
                var fp = new SthoaFileQuery(systemEnvironment, "A8", yesterday);

                var goodValues = new Dictionary<object, object>() {
                    {8499, 219.50m},
                    {8440, 0.00m},
                    {9001, 10013.99m},
                    {8498, 238.00m},
                    {8409, 0.00m},
                    {8413, 0.00m},
                    {8411, 0.00m},
                    {8410, 0.00m},
                    {8406, 0.00m},
                    {8404, 0.00m},
                    {8402, 0.00m},
                    {8401, 0.00m},
                    {8415, 113.99m},
                    {3009, 0.00m},
                    {8435, 0.99m},
                    {8434, 13.00m},
                    {8433, 0.00m},
                    {8432, 0.00m},
                    {8431, 0.00m},
                    {8430, 100.00m},
                    {6509, 0.00m},
                    {8443, 219.50m},
                    {8442, 238.00m},
                    {8438, 0.00m},
                    {8436, 0.00m},
                    {8429, 0.00m},
                    {8428, 0.00m},
                    {8427, 0.00m},
                    {8426, 0.00m},
                    {8425, 0.00m},
                    {8424, 0.00m},
                    {8423, 0.00m},
                    {8422, 0.00m},
                    {8419, 0.00m},
                    {8421, 0.00m},
                    {8418, 0.00m},
                    {8417, 0.00m},
                    {8416, 0.00m},
                    {1230, -238.00m},
                    {1242, -113.99m},
                    {1220, -219.50m},
                    {8211, 100.00m},
                    {8199, 219.50m},
                    {8198, 238.00m},
                    {8324, 0.00m},
                    {8323, 0.00m},
                    {8232, 0.00m},
                    {8231, 0.00m},
                    {8132, 0.00m},
                    {8131, 0.00m},
                    {8441, 0.00m},
                    {8251, 0.00m},
                    {8144, 0.00m},
                    {8143, 0.00m},
                    {8142, 0.00m},
                    {8121, 0.00m},
                    {8141, 0.00m},
                    {8112, 0.00m},
                    {8111, 0.00m},
                    {8221, 0.00m},
                    {8230, 0.00m},
                    {8130, 0.00m},
                    {8140, 0.00m},
                    {8120, 0.00m},
                    {8110, 0.00m},
                    {9002, 0.00m},
                    {9003, 0.00m},
                    {9004, 0.00m},
                    {9005, 0.00m},
                    {9006, 0.00m},
                    {9007, 0.00m},
                    {9008, 0.00m},
                    {9009, 0.00m},
                    {9010, 0.00m},
                    {9011, 0.00m},
                    {9012, 0.00m},
                    {9013, 0.00m},
                    {9014, 0.00m},
                    {9015, 0.00m},
                    {9016, 0.00m},
                    {9017, 0.00m},
                    {9018, 0.00m},
                    {9019, 0.00m},
                    {9020, 0.00m},
                };
                CheckSthoaFileQueryResult(goodValues, fp);
            }

            {
                var fp = new SthoaFileQuery(systemEnvironment, "AH", yesterday);

                var goodValues = new Dictionary<object, object> {
                    {"000010", -113.99m},
                    {"000020", -238.00m},
                    {"000017", -219.50m},
                };
                CheckSthoaFileQueryResult(goodValues, fp);
            }
        }

        [Then(@"order is processed")]
        public void ThenOrderIsProcessed()
        {
            CheckThatRequestWasSuccessfull(creationRoutine);
        }
        
        [Then(@"order is not processed")]
        public void ThenOrderIsNotProcessed()
        {
            CheckThatRequestWasNotSuccessfull(creationRoutine);
        }

        [Then(@"source field has validation error")]
        public void ThenSourceFieldHasValidationError()
        {
            Assert.That(!String.IsNullOrEmpty(creationRoutine.CurrentResponse.OrderHeader.Source.ValidationStatus), Is.True);
        }

        [Then(@"cancellation source field has validation error")]
        public void ThenCancellationSourceFieldHasValidationError()
        {
            Assert.That(enableRefundCreationRoutine.IsSourceFieldHasValidationError(), Is.True);
        }

        [Then(@"status update source field has validation error")]
        public void ThenStatusUpdateSourceFieldHasValidationError()
        {
            Assert.That(!String.IsNullOrEmpty(enableStatusNotificationRoutine.CurrentResponse.Source.ValidationStatus), Is.True);
        }

        [Then(@"status update order status field has validation error")]
        public void ThenStatusUpdateOrderStatusFieldHasValidationError()
        {
            Assert.That(!String.IsNullOrEmpty(enableStatusNotificationRoutine.CurrentResponse.OrderStatus.ValidationStatus), Is.True);
        }

        [Then(@"lineno field has validation error")]
        public void ThenLinenoFieldHasValidationError()
        {
            Assert.That(enableRefundCreationRoutine.IsSourceLineNoHasValidationError(), Is.True);
        }

        [Then(@"refund total field has validation error")]
        public void ThenRefundTotalFieldHasValidationError()
        {
            Assert.That(!String.IsNullOrEmpty(enableRefundCreationRoutine.CurrentResponse.RefundHeader.RefundTotal.ValidationStatus), Is.True);
        }

        [Then(@"click and collect sale is reflected in weakly movement report")]
        public void ThenClickAndCollectSaleIsReflectedInWeaklyMovementReport()
        {
            var order = QodOrder.Get(orderNumber);
            foreach (var line in order.Header.Lines)
            {
                CheckStockLogReports(line.SkuNumber, false);
            }
        }

        private void CheckStockLogReports(string sku, bool isRefund)
        {
            var stklog = stockRepo.GetLastStklogForSku(sku);
            Assert.That(stklog.TypeOfMovement, Is.EqualTo(isRefund ? "07" : "06"));

            var stkmove = new StockItemMovements(sku);
            Assert.That(stkmove.DescriptionExists(isRefund ? "C&C Refund" : "C&C Sale"), Is.True);

            var stkrep = new StockReports(sku);
            Assert.That(stkrep.CheckDecodeComment(stklog.Tkey), Is.True);
            if (!isRefund)
            {
                Assert.That(stkrep.IsSalesMovementDone(), Is.True);
            }
        }

        [Then(@"click and collect cancellation is reflected in stock item movements report")]
        public void ThenClickAndCollectCancellationIsReflectedInStockItemMovementsReport()
        {
            var order = QodOrder.Get(orderNumber);
            foreach (var line in order.Header.Lines)
            {
                CheckStockLogReports(line.SkuNumber, true);
            }
        }

        [Then(@"number of orders to be sync to OM is not changed")]
        public void ThenNumberOfOrdersToBeSyncToOmIsNotChanged()
        {
            Assert.AreEqual(ordersCountToSyncToOm, new AllQodStorage().GetOrdersCountToSyncToOM());
        }

        [When(@"cancellation with non matching line sku pair is requested")]
        public void WhenCancellationWithNonMatchingLineSkuPairIsRequested()
        {
            enableRefundCreationRoutine.PreparePartialRequestForStandardClickAndCollectOrder(sourceOrderNumber);
            enableRefundCreationRoutine.SetFirstLineNumber("1001");
            enableRefundCreationRoutine.ProcessPreparedRequest();
        }

        [When(@"cancellation with the same order number but different source is requested")]
        public void WhenCancellationWithTheSameOrderNumberButDifferentSourceIsRequested()
        {
            enableRefundCreationRoutine.PreparePartialRequestForStandardClickAndCollectOrder(sourceOrderNumber);
            enableRefundCreationRoutine.SetSource("WO");
            enableRefundCreationRoutine.ProcessPreparedRequest();
        }

        [When(@"cancellation without lines requested")]
        public void WhenCancellationWithoutLinesRequested()
        {
            enableRefundCreationRoutine.PrepareRequestWithNoLines(sourceOrderNumber);
            enableRefundCreationRoutine.ProcessPreparedRequest();
        }

        [When(@"cancellation without lines and delivery charge refund requested")]
        public void WhenCancellationWithoutLinesAndDeliveryChargeRefundRequested()
        {
            WhenCancellationWithoutLinesRequested();
        }

        [When(@"click and collect status update with non Hybris source is requested")]
        public void WhenClickAndCollectStatusUpdateWithNonHybrisSourceIsRequested()
        {
            enableStatusNotificationRoutine.CreateStandardRequest(sourceOrderNumber, "002");
            enableStatusNotificationRoutine.SetSource("WO");
            enableStatusNotificationRoutine.ProcessPreparedRequest();
        }

        [When(@"store receives order status update with non existing state")]
        public void WhenStoreReceivesOrderStatusUpdateWithNonExistingState()
        {
            enableStatusNotificationRoutine.CreateStandardRequest(sourceOrderNumber, "100");
            enableStatusNotificationRoutine.ProcessPreparedRequest();
        }

        [When(@"nightly routine procedure is finished for today")]
        public void WhenNightlyRoutineProcedureIsFinishedForToday()
        {
            var pt = new ProcessTransmissions(systemEnvironment, executablesDir);
            pt.ClearTransmissionFiles();
            pt.RunRollDates();
            pt.RunNightlyRetailUpdates();
            pt.ProcessFileCode("PO SOCLOS=STHPO", DateTime.Today);
            pt.ProcessFileCode("SA", DateTime.Today);
            pt.ProcessFileCode("DB", DateTime.Today);
            pt.ProcessFileCode("OT");
        }

        [Then(@"both last orders are included into financial reporting")]
        public void ThenBothLastOrdersAreIncludedIntoFinancialReporting()
        {
            var oaParser = new SthoaFileParser(systemEnvironment);

            Assert.That(oaParser.GetA8ValueByAccountOn("8498", DateTime.Today), Is.EqualTo(47));
            Assert.That(oaParser.GetA5DailyUnitsForSKU("100012"), Is.EqualTo(3));
            Assert.That(oaParser.GetA5DailyTotalForSKU("100012"), Is.EqualTo(36));
            Assert.That(oaParser.GetA5DailyUnitsForSKU("100013"), Is.EqualTo(6));
            Assert.That(oaParser.GetA5DailyTotalForSKU("100013"), Is.EqualTo(132));

            var otParser = new SthotFileParser(systemEnvironment);

            Assert.That(otParser.GetOrdersCountForDate(yesterday), Is.EqualTo(1));
            Assert.That(otParser.GetOrderValueForDateOrderNumber(yesterday, 1), Is.EqualTo(46));
            Assert.That(otParser.GetOrdersCountForDate(DateTime.Today), Is.EqualTo(1));
            Assert.That(otParser.GetOrderValueForDateOrderNumber(DateTime.Today, 1), Is.EqualTo(47));
        }

        [Then(@"order is reflected in cashier performance report")]
        public void ThenOrderIsReflectedInCashierPerformanceReport()
        {
            var rep = new BankingReports();
            var ds = rep.LoadCashierPerformance();

            Assert.That(rep.IsLineCreated(ds), Is.True);
            Assert.That(rep.GetTransactionOrderCount(ds), Is.EqualTo(1));
        }

        [Then(@"order cancellation is reflected in cashier performance report")]
        public void ThenOrderCancellationIsReflectedInCashierPerformanceReport()
        {
            var rep = new BankingReports();
            var dt = rep.LoadCashierPerformance();

            Assert.That(rep.IsLineCreated(dt), Is.True);
            Assert.That(rep.GetTransactionOrderCount(dt), Is.EqualTo(2));
        }

        [Then(@"order is reflected in audit roll enquiry report")]
        public void ThenOrderIsReflectedInAuditRollEnquiryReport()
        {
            var rep = new BankingReports();
            var dt = rep.LoadAuditRollEnqury();

            Assert.That(rep.IsClickAndCollectOrderPresentInAuditRollEnquryReport(dt, sourceOrderNumber), Is.True);
            Assert.That(rep.GetCashierNumberFromSale(dt, sourceOrderNumber), Is.EqualTo("499"));
            Assert.That(rep.GetTillNumberFromSale(dt, sourceOrderNumber), Is.EqualTo("90"));
        }

        [Then(@"order cancellation is reflected in audit roll enquiry report")]
        public void ThenOrderCancellationIsReflectedInAuditRollEnquiryReport()
        {
            var rep = new BankingReports();
            var dt = rep.LoadAuditRollEnqury();

            Assert.That(rep.IsClickAndCollectRefundPresentInAuditRollEnquryReport(dt, sourceOrderNumber), Is.True);
            Assert.That(rep.GetCashierNumberFromRefund(dt, sourceOrderNumber), Is.EqualTo("499"));
            Assert.That(rep.GetTillNumberFromRefund(dt, sourceOrderNumber), Is.EqualTo("90"));
        }

        [Then(@"order is reflected in retail sales report")]
        public void ThenOrderIsReflectedInRetailSalesReport()
        {
            OrderAndCancellationsAreReflectedInRetailSalesReport(0);
        }

        [Then(@"partial order cancellation is reflected in retail sales report")]
        public void ThenPartialOrderCancellationIsReflectedInRetailSalesReport()
        {
            OrderAndCancellationsAreReflectedInRetailSalesReport(1);
        }

        [Then(@"both cancellations are reflected in retail sales report")]
        public void ThenBothCancellationsAreReflectedInRetailSalesReport()
        {
            OrderAndCancellationsAreReflectedInRetailSalesReport(2);
        }

        private void OrderAndCancellationsAreReflectedInRetailSalesReport(int cancCount)
        {
            var rep = new RetailSalesReport();

            rep.GetRetailSalesReportByCashierIDSummary("499");
            CheckRetailSalesReportEntries(rep, cancCount);

            rep.GetRetailSalesReportByTillIDSummary("90");
            CheckRetailSalesReportEntries(rep, cancCount);

            rep.GetRetailSalesReportByCashierIDDetails("499");
            CheckRetailSalesReportEntries(rep, cancCount);

            rep.GetRetailSalesReportByTillIDDetails("90");
            CheckRetailSalesReportEntries(rep, cancCount);
        }

        private void CheckRetailSalesReportEntries(RetailSalesReport rep, int cancCount)
        {
            Assert.That(rep.GetRetailSalesCount(), Is.EqualTo(1));
            Assert.That(rep.GetRetailSalesRefundsCount(), Is.EqualTo(cancCount));
        }

        [Then(@"order is reflected in customer enquiry report")]
        public void ThenOrderIsReflectedInCustomerEnquiryReport()
        {
            CheckCustomerOrderEnquiry(false);
        }

        [Then(@"partially cancelled order is reflected in customer enquiry report")]
        public void ThenPartiallyCancelledOrderIsReflectedInCustomerEnquiryReport()
        {
            CheckCustomerOrderEnquiry(true);
        }

        private void CheckCustomerOrderEnquiry(bool wasPartiallyRefunded)
        {
            var dt = miscRepo.GetCustomerOrderEnquiryReport(StoreId);
            var rep = new BankingReports();

            Assert.That(rep.CheckOrderExistsInCustomerOrderEnquiry(dt, orderNumber), Is.True);
            Assert.That(rep.GetOrderQuantityFromCustomerOrderEnquiry(dt, orderNumber, "100012"), Is.EqualTo(1));
            Assert.That(rep.GetOrderQuantityFromCustomerOrderEnquiry(dt, orderNumber, "100013"), Is.EqualTo(2));
            Assert.That(rep.GetRefundQuantityFromCustomerOrderEnquiry(dt, orderNumber, "100012"), Is.EqualTo(0));
            Assert.That(rep.GetRefundQuantityFromCustomerOrderEnquiry(dt, orderNumber, "100013"), Is.EqualTo(wasPartiallyRefunded ? - 1 : 0));
        }

        [Then(@"order is no longer reflected in customer enquiry report")]
        public void ThenOrderIsNoLongerReflectedInCustomerEnquiryReport()
        {
            var dt = miscRepo.GetCustomerOrderEnquiryReport(StoreId);
            var rep = new BankingReports();

            Assert.That(rep.CheckRefundOrderExistsInCustomerOrderEnquiry(dt, orderNumber), Is.False);
        }

        [Then(@"order is reflected in store sale report")]
        public void ThenOrderIsReflectedInStoreSaleReport()
        {
            OrderAndCancellationsAreReflectedInStoreSaleReport(0);
        }

        [Then(@"partial order cancellation is reflected in store sale report")]
        public void ThenPartialOrderCancellationIsReflectedInStoreSaleReport()
        {
            OrderAndCancellationsAreReflectedInStoreSaleReport(1);
        }

        [Then(@"both cancellations are reflected in store sale report")]
        public void ThenBothCancellationsAreReflectedInStoreSaleReport()
        {
            OrderAndCancellationsAreReflectedInStoreSaleReport(2);
        }

        private void OrderAndCancellationsAreReflectedInStoreSaleReport(int cancCount)
        {
            var rep = new BankingReports();

            var dt = rep.LoalDataSetStoreSales();
            Assert.That(rep.GetTodayClickAndCollectSalesQuantity(dt), Is.EqualTo(1));
            Assert.That(rep.GetTodayClickAndCollectRefundsQuantity(dt), Is.EqualTo(cancCount));
        }

        [When(@"sku is checked with hht terminal")]
        public void WhenSkuIsCheckedWithHhtTerminal()
        {
            stockRepo.RecalculateDayN();
        }

        [Then(@"sold per day quantity is correspond to regular sales")]
        public void ThenSoldPerDayQuantityIsCorrespondToRegularSales()
        {
            Assert.That(stockRepo.GetSoldPerDay("100500", DateTime.Today.AddDays(-2)), Is.EqualTo(2));
        }

        [Then(@"sold per day quantity is correspond to regular and click and collect sales")]
        public void ThenSoldPerDayQuantityIsCorrespondToRegularAndClickAndCollectSales()
        {
            Assert.That(stockRepo.GetSoldPerDay("100500", DateTime.Today.AddDays(-2)), Is.EqualTo(10));
        }

        [Then(@"sold per day quantities are correspond to evening nightly routine time")]
        public void ThenSoldPerDayQuantitiesAreCorrespondToEveningNightlyRoutineTime()
        {
            Assert.That(stockRepo.GetSoldPerDay("100500", DateTime.Today.AddDays(-3)), Is.EqualTo(1));
            Assert.That(stockRepo.GetSoldPerDay("100500", DateTime.Today.AddDays(-2)), Is.EqualTo(13));
            Assert.That(stockRepo.GetSoldPerDay("100500", DateTime.Today.AddDays(-1)), Is.EqualTo(-2));
        }

        [Then(@"sold per day quantities are correspond to morning nightly routine time")]
        public void ThenSoldPerDayQuantitiesAreCorrespondToMorningNightlyRoutineTime()
        {
            Assert.That(stockRepo.GetSoldPerDay("100500", DateTime.Today.AddDays(-3)), Is.EqualTo(14));
            Assert.That(stockRepo.GetSoldPerDay("100500", DateTime.Today.AddDays(-2)), Is.EqualTo(-3));
            Assert.That(stockRepo.GetSoldPerDay("100500", DateTime.Today.AddDays(-1)), Is.EqualTo(1));
        }

        [Given(@"regular web order is placed on a web site")]
        public void GivenRegularWebOrderIsPlacedOnAWebSite()
        {
            InsertWebDeliveryOrder("016939", false);
            InsertWebDeliveryOrder("016941", true);
        }

        [When(@"click and collect order is placed on a website")]
        public void WhenClickAndCollectOrderIsPlacedOnAWebsite()
        {
            InsertCncDeliveryOrder("016938");
        }

        [When(@"regular web order and click and collect order are placed on a website")]
        public void WhenRegularWebOrderAndClickAndCollectOrderArePlacedOnAWebsite()
        {
            GivenRegularWebOrderIsPlacedOnAWebSite();
            WhenClickAndCollectOrderIsPlacedOnAWebsite();
        }

        [Then(@"web order is reflected in customer orders")]
        public void ThenWebOrderIsReflectedInCustomerOrders()
        {
            Assert.That(orderReport.CheckOrderExistsInCustomerOrders("016941", dsnName), Is.True);
        }

        [Then(@"click and collect order is not reflected in customer orders")]
        public void ThenClickAndCollectOrderIsNotReflectedInCustomerOrders()
        {
            Assert.That(orderReport.CheckOrderExistsInCustomerOrders("016938", dsnName), Is.False);
        }

        [Then(@"web order is reflected in delivery income detail")]
        public void ThenWebOrderIsReflectedInDeliveryIncomeDetail()
        {
            Assert.That(orderReport.CheckOrderExistsInDeliveryIncomeDetail("016941"), Is.True);
        }

        [Then(@"click and collect order is not reflected in delivery income detail")]
        public void ThenClickAndCollectOrderIsNotReflectedInDeliveryIncomeDetail()
        {
            Assert.That(orderReport.CheckOrderExistsInDeliveryIncomeDetail("016938"), Is.False);
        }

        [Then(@"web order is reflected in despatched orders")]
        public void ThenWebOrderIsReflectedInDespatchedOrders()
        {
            Assert.That(orderReport.CheckOrderExistsInDespatchedOrders("016941"), Is.True);
        }

        [Then(@"click and collect order is not reflected in despatched orders")]
        public void ThenClickAndCollectOrderIsNotReflectedInDespatchedOrders()
        {
            Assert.That(orderReport.CheckOrderExistsInDespatchedOrders("016938"), Is.False);
        }

        [Then(@"web order is reflected in order delivery listing")]
        public void ThenWebOrderIsReflectedInOrderDeliveryListing()
        {
            Assert.That(orderReport.CheckOrderExistsInOrderDeliveryListing("016941"), Is.True);
        }

        [Then(@"click and collect order is not reflected in order delivery listing")]
        public void ThenClickAndCollectOrderIsNotReflectedInOrderDeliveryListing()
        {
            Assert.That(orderReport.CheckOrderExistsInOrderDeliveryListing("016938"), Is.False);
        }

        [Then(@"web order is reflected in qod get order")]
        public void ThenWebOrderIsReflectedInQodGetOrder()
        {
            Assert.That(orderReport.CheckOrderExistsInTable(orderRepo.QodGetOrder("016941")), Is.True);
        }

        [Then(@"click and collect order is not reflected in qod get order")]
        public void ThenClickAndCollectOrderIsNotReflectedInQodGetOrder()
        {
            Assert.That(orderReport.CheckOrderExistsInTable(orderRepo.QodGetOrder("016938")), Is.False);
        }

        [Then(@"web order is reflected in orders awaiting despatch")]
        public void ThenWebOrderIsReflectedInOrdersAwaitingDespatch()
        {
            Assert.That(orderReport.CheckOrderExistsInOrdersAwaitingDespatch("016939"), Is.True);
        }

        [Then(@"click and collect order is not reflected in orders awaiting despatch")]
        public void ThenClickAndCollectOrderIsNotReflectedInOrdersAwaitingDespatch()
        {
            Assert.That(orderReport.CheckOrderExistsInOrdersAwaitingDespatch("016938"), Is.False);
        }

        [Then(@"web order is reflected in qod get orders for delivery source")]
        public void ThenWebOrderIsReflectedInQodGetOrdersForDeliverySource()
        {
            Assert.That(orderReport.CheckOrderExistsInTable("016939", orderRepo.QodGetOrdersForDeliverySource("8852")), Is.True);
        }

        [Then(@"click and collect order is not reflected in qod get orders for delivery source")]
        public void ThenClickAndCollectOrderIsNotReflectedInQodGetOrdersForDeliverySource()
        {
            Assert.That(orderReport.CheckOrderExistsInTable("016938", orderRepo.QodGetOrdersForDeliverySource("8852")), Is.False);
        }

        [Given(@"web order is reflected in todays pending orders")]
        [Then(@"orders count in todays pending orders is not changed")]
        public void GivenWebOrderIsReflectedInTodaysPendingOrders()
        {
            Assert.That(orderReport.GetTodayQuantityFromPendingOrders(), Is.EqualTo(2));
        }

        [Given(@"delivery status is changed")]
        [When(@"delivery status is changed")]
        public void GivenDeliveryStatusIsChanged()
        {
            orderRepo.ChangeDeliveryStatus(700);
        }

        [Given(@"web order is reflected in delivery status log")]
        [Then(@"orders count in delivery status log is not changed")]
        public void GivenWebOrderIsReflectedInDeliveryStatusLog()
        {
            Assert.That(orderReport.GetTotalFromTable(orderRepo.GetDeliveryStatusChangeLog(DateTime.Today)), Is.EqualTo(2));
        }

        private void CheckThatRequestWasNotSuccessfull(IOrderRoutine routine)
        {
            var response = routine.GetResponse();
            Assert.That(response != null && response.IsSuccessful(), Is.False);
        }

        private void CheckThatRequestWasSuccessfull(IOrderRoutine routine)
        {
            var response = routine.GetResponse();
            Assert.That(response, Is.Not.Null, "Response is null, check logs.");
            Assert.That(response.IsSuccessful(), Is.True, "Response is not succesful, check logs.");
        }

        [Given(@"wickes hourly service order is placed on a web site")]
        public void GivenWickesHourlyServiceOrderIsPlacedOnAWebSite()
        {
            creationRoutine.CreateRequestOrderHeader();
            creationRoutine.CreateOrderLines(1);
            creationRoutine.SetupOrderLine(0, "100005", 1, 5.05M);
            creationRoutine.CurrentRequest.OrderHeader.ToBeDelivered.Value = true;
        }

        [Given(@"delivery charge is (.*)")]
        public void GivenDeliveryChargeIs(Decimal deliveryCharge)
        {
            creationRoutine.CurrentRequest.OrderHeader.DeliveryCharge.Value = deliveryCharge;
        }

        protected QodOrder LoadOrder()
        {
            return QodOrder.Get(orderNumber);
        }

        [Then(@"delivery charge item is not created")]
        public void ThenDeliveryChargeItemIsNotCreated()
        {
            var qod = LoadOrder();
            Assert.That(qod.Header.Lines.Any(l => l.SkuNumber == "805111"), Is.False);
            
        }

        [Given(@"wickes hourly service order with delivery charge is placed on a web site and fulfilled in the store")]
        public void GivenWickesHourlyServiceOrderWithDeliveryChargeIsPlacedOnAWebSiteAndFulfilledInTheStore()
        {
            creationRoutine.PrepareRequestForStandardClickAndCollectOrder();
            creationRoutine.CurrentRequest.OrderHeader.ToBeDelivered.Value = true;
            creationRoutine.CurrentRequest.OrderHeader.DeliveryCharge.Value = 10M;
            WhenStoreReceivesIt();
        }

        [Then(@"delivery charge item is created")]
        public void ThenDeliveryChargeItemIsCreated()
        {
            var qod = LoadOrder();
            Assert.That(qod.Header.Lines.Any(l => l.SkuNumber == "805111"), Is.True);
        }

        [Then(@"delivery charge value is (.*)")]
        public void ThenDeliveryChargeValueIs(Decimal deliveryCharge)
        {
            var qod = LoadOrder();
            Assert.That(qod.Header.DeliveryCharge, Is.EqualTo(deliveryCharge));
            Assert.That(qod.Header.Lines.First(l => l.SkuNumber == "805111").Price, Is.EqualTo(deliveryCharge));
            Assert.That(qod.QodSale.SaleLines.First(l => l.SKUNumber == "805111").ExtendedValue, Is.EqualTo(deliveryCharge));
        }

        [Given(@"full cancellation is requested")]
        public void GivenFullCancellationIsRequested()
        {
            enableRefundCreationRoutine.PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);
        }

        [Given(@"delivery charge only refund is requested")]
        public void GivenDeliveryChargeOnlyRefundIsRequested()
        {
            enableRefundCreationRoutine.PrepareRequestWithNoLines(sourceOrderNumber);
            enableRefundCreationRoutine.CurrentRequest.RefundHeader.DeliveryChargeRefundValue = 1M;
        }

        [Given(@"delivery charge refund is (.*)")]
        public void GivenDeliveryChargeRefundIs(Decimal refundDeliveryCharge)
        {
            enableRefundCreationRoutine.CurrentRequest.RefundHeader.DeliveryChargeRefundValue = refundDeliveryCharge;
        }

        [Then(@"delivery charge item is refunded")]
        public void ThenDeliveryChargeItemIsRefunded()
        {
            var qod = LoadOrder();
            Assert.That(qod.Header.Lines.First(l => l.SkuNumber == "805111").QtyRefunded, Is.EqualTo(-1));
        }

        [Then(@"delivery charge refund value is (.*)")]
        public void ThenDeliveryChargeRefundValueIs(Decimal refundDeliveryCharge)
        {
            var qod = LoadOrder();
            Assert.That(qod.RefundSale.SaleLines.First(l => l.SKUNumber == "805111").ExtendedValue, Is.EqualTo(-refundDeliveryCharge));
        }

        private byte GetAlertType(String alertType)
        {
            byte result = 0; 
            
            switch (alertType)
            {
                case "CNC_new": 
                    result = AlertType.CNC_new;
                    break;
                case "CNC_cancelled":
                    result = AlertType.CNC_cancelled;
                    break;
                case "WHS_new":
                    result = AlertType.WHS_new;
                    break;
                case "WHS_cancelled":
                    result = AlertType.WHS_cancelled;
                    break;
            }
            return result;
        }

        [Then(@"(.*) alert is created")]
        public void ThenAlertIsCreated(String alertType)
        {
            Assert.That(alertRepo.AlertExists(orderNumber, GetAlertType(alertType), startDateTime), Is.True);  
        }

        [Then(@"(.*) alert is not created")]
        public void ThenAlertIsNotCreated(String alertType)
        {
            Assert.That(alertRepo.AlertExists(orderNumber, GetAlertType(alertType), startDateTime), Is.False);
        }

        [Then(@"Parsed XML result for '(.*)' is equal to Customer Order Header data")]
        public void ThenParsedXMLResultForIsEqualToCustomerOrderHeaderData(string tableName)
        {
            Assert.IsTrue(rtiRoutine.ParsXMLResult(tableName, storeId.ToString(), rtiRoutine.GetCORHDRField("SDAT"), rtiRoutine.GetCORHDRField("STIL"), rtiRoutine.GetCORHDRField("STRN")));
        }

        [Then(@"XML files were deleted")]
        public void ThenXMLFilesWereDeleted()
        {
            rtiRoutine.DeleteXMLFiles();
        }

        [Given(@"wickes hourly service order is placed on a website and fulfilled in the store")]
        public void GivenWickesHourlyServiceOrderIsPlacedOnAWebsiteAndFulfilledInTheStore()
        {
            creationRoutine.PrepareRequestForStandardClickAndCollectOrder();
            creationRoutine.CurrentRequest.OrderHeader.ToBeDelivered.Value = true;
            WhenStoreReceivesIt();
        }

        #region Order helper

        private void InsertCncDeliveryOrder(string number)
        {
            var deliveryStatus = GetDeliveryStatus(false);
            orderRepo.InsertIntoCorhdr(PrepareOrderHeader(number, false, "90", "0001"));
            orderRepo.InsertIntoCorhdr4(PrepareOrderHeader4(number, deliveryStatus, 8852, 26943, 700000));
            orderRepo.InsertIntoCorhdr5(PrepareOrderHeader5(number, true, "700000"));
            orderRepo.InsertIntoCorlin(PrepareOrderLine(number, deliveryStatus, 8852, 26943));
            orderRepo.InsertIntoCorlin2(PrepareOrderLine2(number));
            orderRepo.InsertIntoQuoHdr(PrepareQuoteHeader(number, "90", "0001"));
        }

        private void InsertWebDeliveryOrder(string number, bool despatched)
        {
            var deliveryStatus = GetDeliveryStatus(despatched);
            orderRepo.InsertIntoCorhdr(PrepareOrderHeader(number, despatched, "01", "0123"));
            orderRepo.InsertIntoCorhdr4(PrepareOrderHeader4(number, deliveryStatus, 8120, 800009, 800000));
            orderRepo.InsertIntoCorhdr5(PrepareOrderHeader5(number, false, "800000"));
            orderRepo.InsertIntoCorlin(PrepareOrderLine(number, deliveryStatus, 8120, 800009));
            orderRepo.InsertIntoCorlin2(PrepareOrderLine2(number));
            orderRepo.InsertIntoQuoHdr(PrepareQuoteHeader(number, "01", "0123"));
        }

        private int GetDeliveryStatus(bool despatched)
        {
            return despatched ? 699 : 300;
        }

        private CustomerOrderHeader PrepareOrderHeader(string number, bool despatched, string tillNumber, string transactionNumber)
        {
            return new CustomerOrderHeader()
            {
                OrderNumber = number,
                CreateDate = DateTime.Today.AddDays(-1),
                DeliveryOrCollectionDate = DateTime.Today,
                CancellationState = "0",
                IsForDelivery = true,
                IsDeliveryDispatched = despatched,
                AddressLine1 = "Buck House The Mall",
                AddressLine2 = "Admiralty",
                Town = "Northampton",
                PhoneNumber = "0207 555 5555",
                MerchandiseValueStore = 2.2M,
                TotalOrderUnitsNumber = 1,
                SaleDate = DateTime.Today.AddDays(-1),
                TillNumber = tillNumber,
                TransactionNumber = transactionNumber,
                MobilePhoneNumber = "07555 555555",
                PostCode = "NN16 6AA",
                CustomerName = "Sir Bob Harris"
            };
        }

        private CustomerOrderHeader4 PrepareOrderHeader4(string number, int deliverystatus, int sellingStoreId, int sellingStoreOrderId, int omOrderNumber)
        {
            return new CustomerOrderHeader4()
            {
                OrderNumber = number,
                CreateDate = DateTime.Today.AddDays(-1),
                DeliveryOrCollectionDate = DateTime.Today,
                Name = "Sir Bob Harris",
                CashierNumber = "001",
                DeliveryStatus = deliverystatus,
                SellingStoreId = sellingStoreId,
                SellingStoreOrderId = sellingStoreOrderId,
                OMOrderNumber = omOrderNumber,
                CustomerAddress1 = "The Hobbit Hole Middle Earth",
                AddressLine2 = "Off Tolkein Avenue",
                Town = "London",
                PostCode = "SE1 1AA",
                WorkPhone = "0207 666 6666"
            };
        }

        private CustomerOrderHeader5 PrepareOrderHeader5(string number, bool clickAndCollect, string sourceOrderNumber)
        {
            return new CustomerOrderHeader5()
            {
                OrderNumber = number,
                Source = clickAndCollect ? "HY" : "WO",
                SourceOrderNumber = sourceOrderNumber,
                DeliveryContactName = "Wales",
                DeliveryContactPhone = "07007 007007",
                IsClickAndCollect = clickAndCollect
            };
        }

        private CustomerOrderLine PrepareOrderLine(string number, int deliveryStatus, int sellingStoreId, int sellingStoreOrderId)
        {
            return new CustomerOrderLine()
            {
                OrderNumber = number,
                LineNumber = "0001",
                SkuNumber = "100012",
                Quantity = 1,
                DeliveryStatus = deliveryStatus,
                SellingStoreId = sellingStoreId,
                SellingStoreOrderId = sellingStoreOrderId,
                QtyToBeDelivered = 1,
                DeliverySource = "8852",
                Price = 2.2M
            };
        }

        private CustomerOrderLine2 PrepareOrderLine2(string number)
        {
            return new CustomerOrderLine2()
            {
                OrderNumber = number,
                LineNumber = "0001",
                SourceOrderLineNo = 1
            };
        }

        private QuoteHeader PrepareQuoteHeader(string number, string tillNumber, string transactionNumber)
        {
            return new QuoteHeader()
            {
                Number = number,
                DateEntered = DateTime.Today.AddDays(-1),
                ExpiryDate = DateTime.Today,
                IsSold = true,
                IsForDelivery = true,
                MerchandiseValueStore = 2.2M,
                CreateDateTime = DateTime.Today.AddDays(-1),
                TillNumber = tillNumber,
                TransactionNumber = transactionNumber
            };
        }
 
        #endregion

    }
}
