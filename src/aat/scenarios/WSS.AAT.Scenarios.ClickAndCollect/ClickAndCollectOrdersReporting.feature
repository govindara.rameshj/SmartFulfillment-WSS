@Qod
Feature: Cnc orders reporting
As a store colleague
I want click and collect orders to be included into back office reports
So that sale and stock information for click and collect orders is available

Scenario: Click and collect stock log reports for new order
#CheckStockLogTypesFotClickAndCollect
Given click and collect order is placed on a web site
When store receives the order
Then click and collect sale is reflected in weakly movement report

Scenario: Click and collect stock log reports for order cancellation
#CheckStockLogTypesFotClickAndCollect
Given click and collect order with 2 days before today date is placed on a website
And order fulfilled in the store
And full cancellation with yesterday date is requested
When cancellation request is processed
Then click and collect cancellation is reflected in stock item movements report

Scenario: Click and collect order is reflected in Cashier Performance Report
#CashierPerformanceReport
Given all previous banking days are closed
When click and collect order is placed on a website and fulfilled in the store
Then order is reflected in cashier performance report

Scenario:  Click and collect order partial cancellation is reflected in cashier performance report
#CashierPerformanceReport
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is partially cancelled
Then order cancellation is reflected in cashier performance report

Scenario:  Click and collect order full cancellation is reflected in cashier performance report
#CashierPerformanceReport
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is fully cancelled
Then order cancellation is reflected in cashier performance report

Scenario: Click and collect order is reflected in audit roll enquiry report
#CheckAuditRollEnquryReporting
Given all previous banking days are closed
When click and collect order is placed on a website and fulfilled in the store
Then order is reflected in audit roll enquiry report

Scenario: Click and collect order full cancellation is reflected in audit roll enquiry report
#CheckAuditRollEnquryReporting
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is fully cancelled
Then order cancellation is reflected in audit roll enquiry report

Scenario: Click and collect order partial cancellation is reflected in audit roll enquiry report
#CheckAuditRollEnquryReporting
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is partially cancelled 
Then order cancellation is reflected in audit roll enquiry report

Scenario: Click and collect order is reflected in retail sales report
#RetailSalesReport
Given all previous banking days are closed
When click and collect order is placed on a website and fulfilled in the store
Then order is reflected in retail sales report

Scenario: Click and collect order partial cancellation is reflected in retail sales report
#RetailSalesReport
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is partially cancelled 
Then partial order cancellation is reflected in retail sales report

Scenario: Click and collect order full cancellation is reflected in retail sales report
#RetailSalesReport
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is partially cancelled 
And order is completely cancelled 
Then both cancellations are reflected in retail sales report

Scenario: Click and collect order is reflected in customer enquiry report
#SystemEnquiriesReporting
Given all previous banking days are closed
When click and collect order is placed on a website and fulfilled in the store
Then order is reflected in customer enquiry report

Scenario: Partially cancelled click and collect order is reflected in customer enquiry report
#SystemEnquiriesReporting
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is partially cancelled 
Then partially cancelled order is reflected in customer enquiry report

Scenario: Fully cancelled click and collect order is not reflected in customer enquiry report
#SystemEnquiriesReporting
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is fully cancelled
Then order is no longer reflected in customer enquiry report

Scenario: Click and collect order is reflected in store sale report
#StoreSaleReporting
Given all previous banking days are closed
When click and collect order is placed on a website and fulfilled in the store
Then order is reflected in store sale report

Scenario: Click and collect order partial cancellation is reflected in store sale report
#StoreSaleReporting
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is partially cancelled 
Then partial order cancellation is reflected in store sale report

Scenario: Click and collect order full cancellation is reflected in store sale report
#StoreSaleReporting
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
When order is partially cancelled 
And order is completely cancelled 
Then both cancellations are reflected in store sale report

Scenario: Click and collect order is not reflected in customer orders report
#CheckOrderReportForClickCollect
Given all previous banking days are closed
When regular web order and click and collect order are placed on a website
Then web order is reflected in customer orders
And click and collect order is not reflected in customer orders

Scenario: Click and collect order is not reflected in delivery income detail report
#CheckOrderReportForClickCollect
Given all previous banking days are closed
When regular web order and click and collect order are placed on a website
Then web order is reflected in delivery income detail
And click and collect order is not reflected in delivery income detail

Scenario: Click and collect order is not reflected in despatched orders report
#CheckOrderReportForClickCollect
Given all previous banking days are closed
When regular web order and click and collect order are placed on a website
Then web order is reflected in despatched orders
And click and collect order is not reflected in despatched orders

Scenario: Click and collect order is not reflected in order delivery listing report
#CheckOrderReportForClickCollect
Given all previous banking days are closed
When regular web order and click and collect order are placed on a website
Then web order is reflected in order delivery listing
And click and collect order is not reflected in order delivery listing

Scenario: Click and collect order is not reflected in qod get order report
#CheckOrderReportForClickCollect
Given all previous banking days are closed
When regular web order and click and collect order are placed on a website
Then web order is reflected in qod get order
And click and collect order is not reflected in qod get order

Scenario: Click and collect order is not reflected in orders awaiting despatch report
#CheckOrderReportForClickCollect
Given all previous banking days are closed
When regular web order and click and collect order are placed on a website
Then web order is reflected in orders awaiting despatch
And click and collect order is not reflected in orders awaiting despatch

Scenario: Click and collect order is not reflected in qod get orders for delivery source report
#CheckOrderReportForClickCollect
Given all previous banking days are closed
When regular web order and click and collect order are placed on a website
Then web order is reflected in qod get orders for delivery source
And click and collect order is not reflected in qod get orders for delivery source

Scenario: Click and collect order is not reflected in pending orders report
#CheckOrderReportForClickCollect
Given all previous banking days are closed
And regular web order is placed on a web site
And web order is reflected in todays pending orders
When click and collect order is placed on a website
Then orders count in todays pending orders is not changed

Scenario: Click and collect order is not reflected in delivery status log
#CheckOrderReportForClickCollect
Given all previous banking days are closed
And regular web order is placed on a web site
And delivery status is changed
And web order is reflected in delivery status log
When click and collect order is placed on a website
And delivery status is changed
Then orders count in delivery status log is not changed


