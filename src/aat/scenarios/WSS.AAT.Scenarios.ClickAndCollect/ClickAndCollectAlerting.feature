﻿@Qod
Feature: Cnc orders alerting
As a store colleague
I want to be notified about new click and collect and wickes hourly service orders and their cancellations
So that I can process them in proper time

Scenario: Click and collect order creation
Given click and collect order is placed on a web site
When store receives the order
Then CNC_new alert is created

Scenario: Click and collect order partial cancellation
Given click and collect order is placed on a website and fulfilled in the store
And partial cancellation is requested
When cancellation request is processed
Then CNC_cancelled alert is created

Scenario: Click and collect order full cancellation
Given click and collect order is placed on a website and fulfilled in the store
And full cancellation is requested
When cancellation request is processed
Then CNC_cancelled alert is created

Scenario: Wickes hourly service order creation
Given wickes hourly service order is placed on a web site
When store receives the order
Then WHS_new alert is created

Scenario: Wickes hourly service order partial cancellation
Given wickes hourly service order is placed on a website and fulfilled in the store
And partial cancellation is requested
When cancellation request is processed
Then WHS_cancelled alert is created

Scenario: Wickes hourly service order full cancellation
Given wickes hourly service order is placed on a website and fulfilled in the store
And full cancellation is requested
When cancellation request is processed
Then WHS_cancelled alert is created

Scenario: Wickes hourly service order delivery charge refund
Given wickes hourly service order with delivery charge is placed on a web site and fulfilled in the store
And delivery charge only refund is requested
When cancellation request is processed
Then WHS_cancelled alert is not created