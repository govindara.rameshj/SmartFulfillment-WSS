@Qod
Feature: Cnc positive scenarios
As a store colleague
I want to have click and collect orders available in the store
So that I can process them

Scenario: Click and collect order creation
#RemoteQodDeliveryTillTranCheckForClickAndCollect
#RemoteQodCreationAndStatusNotificationForClickAndCollect
Given click and collect order is placed on a web site
And order date is yesterday
When store receives the order
Then order is self fulfilled in the store
And number of orders to be sync to OM is not changed

Scenario: Click and collect till and cashier for new order
#CheckTillTranCashierFields
Given click and collect order is placed on a web site
And order date is yesterday
When store receives the order
Then till and cashier fields are filled properly for new order

Scenario: Click and collect order full cancellation
#RemoteQodRefundForClickAndCollect
Given click and collect order with 2 days before today date is placed on a website
And order fulfilled in the store
And full cancellation with yesterday date is requested
When cancellation request is processed
Then order is fully cancelled

Scenario: Click and collect order partial cancellation
#RemoteQodRefundForClickAndCollectPartial
Given click and collect order with 2 days before today date is placed on a website
And order fulfilled in the store
And partial cancellation with yesterday date is requested
When cancellation request is processed
Then order is partially cancelled

Scenario: Click and collect till and cashier for order cancellation
#CheckTillTranCashierFields
Given click and collect order with 2 days before today date is placed on a website
And order fulfilled in the store
And partial cancellation with yesterday date is requested
When cancellation request is processed
Then till and cashier fields are filled properly for order cancellation

Scenario: Click and collect order with basket promotion
#CheckOrderConsignmentsValues
Given all previous banking days are closed
And click and collect order with basket promotion is placed on a website
And product1 amount is a
And product2 amount is b
And total order amount is a+b-x
When store receives the order
Then amounts are reflected in financial reporting

Scenario: Click and collect order status update
#RemoteQodCreationAndStatusNotificationForClickAndCollect
Given click and collect order is placed on a website and fulfilled in the store
When store receives order status update 
Then order status is updated
And number of orders to be sync to OM is not changed

Scenario: Remote Qod store order fulfillment
#RemoteQodFulfillmentForNormalOrder
Given order is placed in selling store
When fulfilling store receives the order
Then order is fulfilled in the store

Scenario: Remote Qod store order status update
#RemoteQodFulfillmentForNormalOrder
Given order is placed in selling store and fulfilled in the store
When store receives normal order status update 
Then order status is updated

Scenario: Sold per day for regular sales
#SoldTodayForHht
Given there was no nightly routine for previous days
And regular sales are made
When sku is checked with hht terminal
Then sold per day quantity is correspond to regular sales

Scenario: Sold per day for click and collect sales
#SoldTodayForHht
Given there was no nightly routine for previous days
And regular sales are made
And click and collect orders are fulfilled
When sku is checked with hht terminal
Then sold per day quantity is correspond to regular and click and collect sales

Scenario: Sold per day for evening nightly routine
#SoldTodayForHht
Given nightly routine for previous days was passed in the evening
And regular sales are made
And click and collect orders are fulfilled
When sku is checked with hht terminal
Then sold per day quantities are correspond to evening nightly routine time

Scenario: Sold per day for morning nightly routine
#SoldTodayForHht
Given nightly routine for previous days was passed in the morning
And regular sales are made
And click and collect orders are fulfilled
When sku is checked with hht terminal
Then sold per day quantities are correspond to morning nightly routine time

Scenario: Wickes hourly service order without delivery charge creation
Given wickes hourly service order is placed on a web site
And delivery charge is 0.00
When store receives the order
Then delivery charge item is not created

Scenario: Wickes hourly service order with delivery charge creation
Given wickes hourly service order is placed on a web site
And delivery charge is 10.00
When store receives the order
Then delivery charge item is created
And delivery charge value is 10.00 

Scenario: Wickes hourly service order with delivery charge cancellation
Given wickes hourly service order with delivery charge is placed on a web site and fulfilled in the store
And full cancellation is requested
And delivery charge refund is 5.00 
When cancellation request is processed
Then delivery charge item is refunded 
And delivery charge refund value is 5.00 

Scenario: Wickes hourly service order cancellation without lines is processed
Given wickes hourly service order with delivery charge is placed on a web site and fulfilled in the store
And delivery charge only refund is requested
When cancellation request is processed
Then delivery charge item is refunded
