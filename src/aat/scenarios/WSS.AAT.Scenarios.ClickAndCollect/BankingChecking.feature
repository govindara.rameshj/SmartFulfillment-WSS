@Qod
Feature: Cnc orders banking
As a store colleague
I want to have click and collect orders included into financial reporting
So that transactions and stock are sent to head office

Scenario: Cash balance is updated for new Click and collect order
#CheckCashBalUpdateDataOk
Given all previous banking days are closed
And click and collect order is placed on a web site
And total amount is 14.00
When store receives the order
Then click and collect virtual cashier balance increases

Scenario: Cash balance is updated for Click and collect order cancellation
#CheckCashBalUpdateDataOk
Given all previous banking days are closed
And click and collect order with 1 days before today date is placed on a website
And total amount is 321.00
And order fulfilled in the store
And partial cancellation is requested
And total refund amount is 100.00
When cancellation request is processed
Then click and collect virtual cashier balance decreases

Scenario: Click and collect stock and sale related info is reported by fulfilling store
#CheckSthpa
Given all previous banking days are closed
And click and collect order is placed on a website and fulfilled in the store
And full cancellation with yesterday date is requested
When cancellation request is processed
And stock movements and inventory updates are calculated during nightly routine
Then stock and sale order details are included into STHPA file

Scenario: Flash totals include click and collect orders
#CheckSthof
Given all previous banking days are closed
And click and collect order with 2 days before today date is placed on a website
And order fulfilled in the store
And partial cancellation with yesterday date is requested
When cancellation request is processed
And flash totals are calculated during nightly routine
Then results are included into flash totals file

Scenario: Banking check for 1 day
#CncBankingSuite
Given all previous banking days are closed
And click and collect order is placed on a website and store order without delivery is placed on a Till
When the first day is closed
Then both orders are included into financial reporting

Scenario: Banking check for 2 days
#CncBankingSuite
Given all previous banking days are closed
And click and collect order is placed on a website and store order without delivery is placed on a Till
When the first day is closed
And the second day is closed
Then both orders are included into financial reporting with yesterday date

Scenario: Process transmissions period check for 1 day
#CncBankingSuite
Given all previous banking days are closed
And click and collect order is placed on a website and store order without delivery is placed on a Till
When the first day is closed
And process transmissions period for first day is run
Then both orders are included into financial reporting

Scenario: Process transmissions period check for 2 days
#CncBankingSuite
Given all previous banking days are closed
And click and collect order is placed on a website and store order without delivery is placed on a Till
When the first day is closed
And the second day is closed
And process transmissions period for both days is run
Then both orders are included into financial reporting with yesterday date
