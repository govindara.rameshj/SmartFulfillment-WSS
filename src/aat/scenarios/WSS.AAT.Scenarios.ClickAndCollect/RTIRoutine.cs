﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.ClickAndCollect
{
    public class RtiRoutine
    {
        private DateTime deleteDate;
        private string sourceDir = @"C:\RTI Files\BulkOut\";
        private readonly string _systemUnderTestLocation;
        private readonly string _dsnName;
        private readonly string _connectionString;
        private string sDAT;
        private string sTIL;
        private string sTRN;

        RtiRepository _rtiRepository;

        public RtiRoutine(string dsnName, string systemUnderTestLocation, string connectionString, IDataLayerFactory dataLayerFactory)
        {
            _dsnName = dsnName;
            _systemUnderTestLocation = systemUnderTestLocation;
            _connectionString = connectionString;

            _rtiRepository = dataLayerFactory.Create<RtiRepository>();
        }

        public void StartDebug()
        {
            Debugger.Launch();
        }

        public void RunRTIBulkSend()
        {
            if (!Directory.Exists(sourceDir))
            {
                Directory.CreateDirectory(sourceDir);
            }

            deleteDate = DateTime.Now;
            Process proc = new Process();
            proc.StartInfo.FileName = _systemUnderTestLocation + "RTIBulkSend.exe";
            proc.StartInfo.Arguments = "dsn=" + _dsnName;
            proc.Start();
            proc.WaitForExit();
        }

        public bool ParsXMLResult(string tableName, string vStoreID, string vDate1, string vTill, string vTran)
        {
            bool res = false;
            XmlDocument xd = new XmlDocument();
            string[] xmlList = Directory.GetFiles(sourceDir, "*.xml");
            foreach (string f in xmlList)
            {
                if (File.GetCreationTime(f) >= deleteDate)
                {
                    if (f.StartsWith(sourceDir + tableName))
                    {
                        FileStream fs = new FileStream(f, FileMode.Open);
                        xd.Load(fs);
                        XmlNodeList list = xd.GetElementsByTagName(tableName);

                        foreach (XmlNode node in list)
                        {
                            bool storeId = GetResultCompare(node, "StoreID", vStoreID.Remove(1, 1));
                            bool date1 = GetResultCompare(node, "DATE1", vDate1);
                            bool till = GetResultCompare(node, "TILL", vTill);
                            bool tran = GetResultCompare(node, "TRAN", vTran);

                            if (storeId && date1 && till && tran)
                            {
                                res = true;
                                break;
                            }
                        }

                       fs.Close();
                    }
                }
            }

            return res;
        }

        public bool GetResultCompare(XmlNode node, string tagName, string value)
        {
            bool res = false;

            XmlNode xn = node.SelectSingleNode(tagName);

            if (tagName == "DATE1")
            {
                if (DateTime.Parse(xn.InnerText) == DateTime.Parse(value))
                {
                    res = true;
                }
            }
            else
            {
                if (xn.InnerText == value)
                {
                    res = true;
                }
            }
            return res;
        }

        public void DeleteXMLFiles()
        {
            string[] xmlList = Directory.GetFiles(sourceDir, "*.xml");

            foreach (string f in xmlList)
            {
                if (File.GetCreationTime(f) >= deleteDate)
                {
                    File.Delete(f);
                }
            }
        }

        public void LoadCORHDRValueFields(string numbOrder)
        {
            var corhdr = _rtiRepository.LoadCustomerOrderHeaderValueFields(numbOrder);

            sDAT = corhdr.SaleDate.ToString();
            sTIL = corhdr.TillNumber;
            sTRN = corhdr.TransactionNumber;
        }

        public string GetCORHDRField(string fieldName)
        {
            switch (fieldName)
            {
                case "SDAT":
                    return sDAT;
                case "STIL":
                    return sTIL;
                case "STRN":
                    return sTRN;
                default:
                    throw new Exception("Unknown field name");
            }
        }
    }
}
