﻿Feature: RtiSuite
@RtiSuite

Scenario: Check Click And Collect Order In Rti Feed
    Given Click and Collect order was fulfilled in the store
    When Store receives the order
    And Store Id was saved
    And Customer Order Header table value fields were loaded
    Then Parsed XML result for 'DLLINE' is equal to Customer Order Header data
    And Parsed XML result for 'DLPAID' is equal to Customer Order Header data
    And Parsed XML result for 'DLTOTS' is equal to Customer Order Header data
    And Parsed XML result for 'DLRCUS' is equal to Customer Order Header data
    And XML files were deleted