@Qod
Feature: Cnc negative scenarios
As a store colleague
I want to be sure that all negative click and collect scenarios are not processed

Scenario: Click and collect order cancellation with non existing product is not processed
#CheckProcessRefundWithDoesnotExistCode
Given click and collect order is placed on a website and fulfilled in the store
When cancellation with non existing product is requested
Then order cancellation is not processed

Scenario: Click and collect order with non Hybris source is not processed
#CheckSourceCodeValidation
Given click and collect order is placed on a web site
And source is non Hybris
When fulfillment is requested
Then order is not processed
And source field has validation error

Scenario: Click and collect orders are not processed during nightly routine
#NightlyRoutineShouldBlockNewOrders
Given nightly routine procedure is started
And click and collect order is placed on a web site
When fulfillment is requested
Then order is not processed

Scenario: Click and collect orders are processed after nightly routine is finished
#NightlyRoutineShouldBlockNewOrders
Given nightly routine procedure is finished
And click and collect order is placed on a web site
When fulfillment is requested
Then order is processed

Scenario: Click and collect orders are included into financial reporting not depending on date
#OrdersForDifferentDates
Given all previous banking days are closed
And click and collect order with yesterday date is fulfilled in the store
And nightly routine procedure is finished for yesterday
And one more click and collect order with yesterday date is fulfilled in the store
And click and collect order with today date is fulfilled in the store
When nightly routine procedure is finished for today
Then both last orders are included into financial reporting

Scenario: Orders with the same order numbers but different sources are processed
#RemoteQodOrdersWithSameSourceOrderNumber
Given order is placed in selling store and fulfilled in the store
And click and collect order with the same source order number is placed on a web site
When fulfillment is requested
Then order is processed

Scenario: Click and collect orders with the same order numbers and sources are not processed
#RemoteQodCreationSourceOrderNumberValidation
Given click and collect order is placed on a website and fulfilled in the store
And click and collect order with the same source order number is placed on a web site
When fulfillment is requested
Then order is not processed

Scenario: Click and collect orders with the same order numbers but different sources are processed
#RemoteQodCreationSourceOrderNumberValidation
Given click and collect order is placed on a website and fulfilled in the store
And click and collect order with the same source order number but different source is placed on a web site
When fulfillment is requested
Then order is processed

Scenario: Click and collect order cancellation with non matching LineSku pair is not processed
#RemoteQodRefundForClickAndCollectValidation
Given click and collect order is placed on a website and fulfilled in the store
When cancellation with non matching line sku pair is requested
Then order cancellation is not processed
And lineno field has validation error

Scenario: Click and collect order cancellation with incorrect source is not processed
#RemoteQodRefundForClickAndCollectValidation
Given click and collect order is placed on a website and fulfilled in the store
When cancellation with the same order number but different source is requested
Then order cancellation is not processed
And cancellation source field has validation error

Scenario: Click and collect order cancellation without lines is not processed
#RemoteQodRefundForClickAndCollectValidation
Given click and collect order is placed on a website and fulfilled in the store
When cancellation without lines requested
Then order cancellation is not processed
And refund total field has validation error

Scenario: Click and collect status update for store order is not processed
#RemoteQodEnableStatusNotificationValidation
Given order is placed in selling store and fulfilled in the store
When click and collect status update with non Hybris source is requested
Then status update is not processed
And status update source field has validation error

Scenario: Click and collect order status update with non existing state is not processed
#RemoteQodEnableStatusNotificationValidation
Given click and collect order is placed on a website and fulfilled in the store
When store receives order status update with non existing state
Then status update is not processed
And status update order status field has validation error

Scenario: Wickes hourly service order cancellation without lines and delivery charge refund is not processed
Given wickes hourly service order with delivery charge is placed on a web site and fulfilled in the store
When cancellation without lines and delivery charge refund requested
Then order cancellation is not processed
And refund total field has validation error
