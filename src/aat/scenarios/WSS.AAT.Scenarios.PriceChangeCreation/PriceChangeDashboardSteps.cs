extern alias bltoolkit;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Bindings;
using WSS.AAT.Common.Utility.Utility;
using WSS.BO.DataLayer.Model.Entities;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;
using Cts.Oasys.Core.Tests;
using WSS.AAT.Common.Banking;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using System;
using System.Data;

namespace WSS.AAT.Scenarios.PriceChangesCreation
{
    [Binding]
    public class PriceChangeDashboardSteps : BaseStepDefinitions
    {
        private readonly TablesRepository repo;
        private readonly TestStockRepository stockRepo;
        private readonly ManagersDashboardReportsRepository managerDashboardRepo;
        private readonly CoreBanking coreBanking;

        public PriceChangeDashboardSteps(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
        {
            coreBanking = new CoreBanking(systemEnvironment);
            repo = dataLayerFactory.Create<TablesRepository>();
            stockRepo = dataLayerFactory.Create<TestStockRepository>();
            managerDashboardRepo = dataLayerFactory.Create<ManagersDashboardReportsRepository>();
        }

        [Given(@"Stock Master contained next data for SKUs:")]
        public void GivenStockMasterContainedNextDataForSKUs(Table table)
        {
            foreach (var row in table.Rows)
            {
                StockUpdateForPriceChanges stockUpdate = new StockUpdateForPriceChanges()
                {
                    SkuNumber = row["SKU Number"],
                    OnHandQuantity = decimal.Parse(row["On Hand Stock Quantity"]),
                    MarkdownStockQuanty = decimal.Parse(row["Mark-down Stock Quantity"]),
                    IsObsolete = bool.Parse(row["Is Non-Stock Item"]),
                    IsAutoApplyPriceChanges = bool.Parse(row["Is Auto Apply Price Changes"]),
                    IsNonStock = bool.Parse(row["Is Non-Stock Item"])
                };

                stockRepo.UpdateStockMaster(stockUpdate);
            }
        }

        [Given(@"Recent Price for SKU '(.*)' was applied today manually with Price (.*)")]
        public void GivenRecentPriceForSKUWasAppliedTodayManuallyWithPrice(string skuNumber, decimal newPrice)
        {
            stockRepo.UpdateStockMasterDateAndPriceOfPriceChange(skuNumber, newPrice, DateTime.Today);
        }

        [Given(@"Recent Price for SKU '(.*)' was auto applied during yesterday Nightly Routine with Price (.*)")]
        public void GivenRecentPriceForSKUWasAutoAppliedDuringYesterdayNightlyRoutineWithPrice(string skuNumber, decimal newPrice)
        {
            stockRepo.UpdateStockMasterDateAndPriceOfPriceChange(skuNumber, newPrice, DateTime.Today.AddDays(-1));
        }

        [Given(@"Recent Price for SKU '(.*)' was auto applied a week ago with Price (.*)")]
        public void GivenRecentPriceForSKUWasAutoAppliedAWeekAgoWithPrice(string skuNumber, decimal newPrice)
        {
            stockRepo.UpdateStockMasterDateAndPriceOfPriceChange(skuNumber, newPrice, DateTime.Today.AddDays(-7));
        }

        [Given(@"Price Change table is empty")]
        public void GivenPriceChangeTableIsEmpty()
        {
            repo.DeleteFromPriceChangeTable();
        }

        [Given(@"the following price change events exist in system without exact dates:")]
        [When(@"the following price change events exist in system without exact dates:")]
        public void GivenTheFollowingPriceChangeEventsExistInSystemWithoutExactDates(Table table)
        {
            Utility.ProcessPriceChangeTableDates(table);
            var wrappedTable = new SpecflowTableWrapper<PriceChange>(table);
            var priceChange = wrappedTable.GetEntities(Utility.CreateEmptyPriceChange);
            repo.InsertPriceChangeTable(priceChange);
        }

        [Then(@"Number of Price Changes including Stock not held by Store that are still pending is (.*)")]
        public void ThenNumberOfPriceChangesIncludingStockNotHeldByStoreThatAreStillPendingIs(int numberOfChanges)
        {
            var priceChangeDashboard = managerDashboardRepo.GetPriceChangesReportOnDashboard(DateTime.Today);
            CheckManagersDasboardPriceChangesLine(priceChangeDashboard, "Price Labels Action Required", numberOfChanges);
        }

        [Then(@"Number of Price Changes including Stock not held by Store for Today is (.*)")]
        public void ThenNumberOfPriceChangesIncludingStockNotHeldByStoreForTodayIs(int numberOfChanges)
        {
            var priceChangeDashboard = managerDashboardRepo.GetPriceChangesReportOnDashboard(DateTime.Today);
            CheckManagersDasboardPriceChangesLine(priceChangeDashboard, "Outstanding Price Label Report", numberOfChanges);
        }

        [Then(@"Number of Price Changes including Stock not held by Store that are Overdue is (.*)")]
        public void ThenNumberOfPriceChangesIncludingStockNotHeldByStoreThatAreOverdueIs(int numberOfChanges)
        {
            var priceChangeDashboard = managerDashboardRepo.GetPriceChangesReportOnDashboard(DateTime.Today);
            CheckManagersDasboardPriceChangesLine(priceChangeDashboard, "Overdue Labels", numberOfChanges);
        }

        [Then(@"Count of Price Change Audit Report is equal to (.*)")]
        public void ThenCountOfPriceChangeAuditReportIsEqualTo(int priceChageAuditCount)
        {
            var priceChangeDashboard = managerDashboardRepo.GetPriceChangesReportOnDashboard(DateTime.Today);
            CheckManagersDasboardPriceChangesLine(priceChangeDashboard, "Price Change Audit Report", priceChageAuditCount);
        }

        private void CheckManagersDasboardPriceChangesLine(DataTable table, string description, int quantity)
        {
            int descriptionColumnIndex = 1;
            int quantityColumnIndex = 2;

            foreach (DataRow row in table.Rows)
            {
                if (row[descriptionColumnIndex].ToString() == description)
                {
                    Assert.AreEqual(quantity, int.Parse(row[quantityColumnIndex].ToString()));
                }
            }
        }
    }
}