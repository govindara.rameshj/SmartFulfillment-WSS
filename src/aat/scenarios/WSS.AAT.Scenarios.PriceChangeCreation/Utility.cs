using System;
using TechTalk.SpecFlow;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.AAT.Scenarios.PriceChangesCreation
{
    static class Utility
    {
        public static PriceChange CreateEmptyPriceChange()
        {
            return new PriceChange
            {
                LabelLarge = "0",
                LabelMedium = "0",
                LabelSmall = "0",
                MarkupComment = "0",
                ShelfEdge = "0"
            };
        }

        public static void ProcessPriceChangeEventTableDates(Table table)
        {
            foreach (var row in table.Rows)
            {
                row["Start date"] = GetDateFromStringDescription(row["Start date"]);
                row["Ending date"] = GetDateFromStringDescription(row["Ending date"]);
            }
        }

        public static void ProcessPriceChangeTableDates(Table table)
        {
            foreach (var row in table.Rows)
            {
                row["Date"] = GetDateFromStringDescription(row["Date"]);
                row["Apply Date"] = GetDateFromStringDescription(row["Apply Date"]);
                if (row.ContainsKey("Apply Price Date"))
                {
                    row["Apply Price Date"] = GetDateFromStringDescription(row["Apply Price Date"]); ;
                }
            }
        }

        private static string GetDateFromStringDescription(string dateDescription)
        {
            switch (dateDescription)
            {
                case "today":
                    return DateTime.Now.Date.ToString();
                case "tomorrow":
                    return DateTime.Now.AddDays(1).Date.ToString();
                case "yesterday":
                    return DateTime.Now.AddDays(-1).Date.ToString();
                case "2 days after today":
                    return DateTime.Now.AddDays(2).Date.ToString();
                case "3 days after today":
                    return DateTime.Now.AddDays(3).Date.ToString();
                case "4 days after today":
                    return DateTime.Now.AddDays(4).Date.ToString();
                case "5 days after today":
                    return DateTime.Now.AddDays(5).Date.ToString();
                case "6 days after today":
                    return DateTime.Now.AddDays(6).Date.ToString();
                case "7 days after today":
                    return DateTime.Now.AddDays(7).Date.ToString();
                case "8 days after today":
                    return DateTime.Now.AddDays(8).Date.ToString();
                case "2 days before today":
                    return DateTime.Now.AddDays(-2).Date.ToString();
                case "3 days before today":
                    return DateTime.Now.AddDays(-3).Date.ToString();
                case "4 days before today":
                    return DateTime.Now.AddDays(-4).Date.ToString();
                case "5 days before today":
                    return DateTime.Now.AddDays(-5).Date.ToString();
                case "6 days before today":
                    return DateTime.Now.AddDays(-6).Date.ToString();
                case "7 days before today":
                    return DateTime.Now.AddDays(-7).Date.ToString();
                case "20 days before today":
                    return DateTime.Now.AddDays(-20).Date.ToString();
                case "2 weeks before today":
                    return DateTime.Now.AddDays(-14).Date.ToString();
                case "3 weeks before today":
                    return DateTime.Now.AddDays(-21).Date.ToString();
                case "4 weeks before today":
                    return DateTime.Now.AddDays(-28).Date.ToString();
                case "1 week after today":
                    return DateTime.Now.AddDays(7).Date.ToString();
                case "3 weeks after today":
                    return DateTime.Now.AddDays(21).Date.ToString();
                case "not set":
                    return "not set";
            }

            Console.WriteLine(dateDescription);
            return string.Empty;
        }
    }
}
