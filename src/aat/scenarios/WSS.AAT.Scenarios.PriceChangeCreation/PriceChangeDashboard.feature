﻿@PriceChange
Feature: Price Change Dashboard

Scenario: Price Changes Dashboard shows number of only latest Overdue Price Changes for SKU
Given Price Change table is empty
And Stock Master contained next data for SKUs:
      | SKU Number | Is Non-Stock Item | Is Obsolete | Is Auto Apply Price Changes | On Hand Stock Quantity | Mark-down Stock Quantity |
      | 100004     | True              | False       | False                       | 8                      | 2                        |
      | 100005     | False             | True        | False                       | 0                      | 1                        |
When the following price change events exist in system without exact dates:
      | SkuNumber | Date                 | Priority | Event number | Price | Status | Apply Date | Shelf Edge | Label small | Label medium | Label large |
      | 100004    | 5 days before today  | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100004    | 4 days before today  | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100004    | 2 days before today  | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100005    | 20 days before today | 10       | 000004       | 25.00 | S      | not set    | False      | False       | True         | False       |
      | 100005    | 5 days before today  | 10       | 000004       | 20.00 | U      | not set    | True       | True        | True         | False       |
      | 100005    | 3 weeks before today | 10       | 000004       | 24.00 | S      | not set    | False      | False       | True         | False       |
Then Number of Price Changes including Stock not held by Store that are still pending is 2
And Number of Price Changes including Stock not held by Store that are Overdue is 2
And Number of Price Changes including Stock not held by Store for Today is 4

Scenario: Price Changes Dashboard shows number of all Price Changes for Today for SKU
Given Price Change table is empty
And Stock Master contained next data for SKUs:
      | SKU Number | Is Non-Stock Item | Is Obsolete | Is Auto Apply Price Changes | On Hand Stock Quantity | Mark-down Stock Quantity |
      | 100004     | True              | False       | False                       | 8                      | 2                        |
      | 100005     | False             | True        | False                       | 0                      | 1                        |
When the following price change events exist in system without exact dates:
      | SkuNumber | Date                | Priority | Event number | Price | Status | Apply Date | Shelf Edge | Label small | Label medium | Label large |
      | 100004    | 5 days after today  | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100004    | 4 days after today  | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100004    | tomorrow            | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100005    | 1 week after today  | 10       | 000004       | 25.00 | U      | not set    | False      | False       | True         | False       |
      | 100005    | 3 weeks after today | 10       | 000004       | 20.00 | U      | not set    | True       | True        | True         | False       |
      | 100005    | tomorrow            | 10       | 000004       | 24.00 | U      | not set    | False      | False       | True         | False       |
Then Number of Price Changes including Stock not held by Store that are still pending is 5
And Number of Price Changes including Stock not held by Store that are Overdue is 0
And Number of Price Changes including Stock not held by Store for Today is 5

Scenario: Price Changes Dashboard shows only Unapplied Price Changes
Given Price Change table is empty
And Stock Master contained next data for SKUs:
      | SKU Number | Is Non-Stock Item | Is Obsolete | Is Auto Apply Price Changes | On Hand Stock Quantity | Mark-down Stock Quantity |
      | 100004     | True              | False       | False                       | 8                      | 2                        |
      | 100005     | False             | True        | False                       | 0                      | 1                        |
When the following price change events exist in system without exact dates:
      | SkuNumber | Date                | Priority | Event number | Price | Status | Apply Date | Shelf Edge | Label small | Label medium | Label large |
      | 100004    | 5 days after today  | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100004    | 4 days after today  | 10       | 000002       | 6     | S      | not set    | False      | True        | False        | False       |
      | 100004    | tomorrow            | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100005    | 1 week after today  | 10       | 000004       | 25.00 | U      | not set    | False      | False       | True         | False       |
      | 100005    | 3 weeks after today | 10       | 000004       | 20.00 | S      | not set    | True       | True        | True         | False       |
      | 100005    | tomorrow            | 10       | 000004       | 24.00 | U      | not set    | False      | False       | True         | False       |
Then Number of Price Changes including Stock not held by Store that are still pending is 4
And Number of Price Changes including Stock not held by Store that are Overdue is 0
And Number of Price Changes including Stock not held by Store for Today is 4

Scenario: Price Change Dashboard does not show Price Changes with all Label flags in False
Given Price Change table is empty
And Stock Master contained next data for SKUs:
      | SKU Number | Is Non-Stock Item | Is Obsolete | Is Auto Apply Price Changes | On Hand Stock Quantity | Mark-down Stock Quantity |
      | 100004     | True              | False       | False                       | 8                      | 2                        |
      | 100005     | False             | True        | False                       | 0                      | 1                        |
When the following price change events exist in system without exact dates:
      | SkuNumber | Date                | Priority | Event number | Price | Status | Apply Date | Shelf Edge | Label small | Label medium | Label large |
      | 100004    | 4 days before today | 10       | 000002       | 6     | U      | not set    | False      | False       | False        | False       |
      | 100004    | 6 days after today  | 10       | 000002       | 6     | U      | not set    | False      | True        | False        | False       |
      | 100005    | 5 days before today | 10       | 000004       | 20.00 | U      | not set    | True       | True        | True         | False       |
      | 100005    | 6 days after today  | 10       | 000004       | 21.00 | U      | not set    | True       | True        | True         | False       |
Then Number of Price Changes including Stock not held by Store that are still pending is 3
And Number of Price Changes including Stock not held by Store for Today is 4
And Number of Price Changes including Stock not held by Store that are Overdue is 1

Scenario: Price Change Dashboard does not show Non-Standart Products with zero Stock On Hand Quantity
Given Price Change table is empty
And Stock Master contained next data for SKUs:
      | SKU Number | Is Non-Stock Item | Is Obsolete | Is Auto Apply Price Changes | On Hand Stock Quantity | Mark-down Stock Quantity |
      | 100005     | False             | True        | False                       | 0                      | 1                        |
      | 100009     | False             | False       | True                        | -6                     | 6                        |
When the following price change events exist in system without exact dates:
      | SkuNumber | Date                | Priority | Event number | Price | Status | Apply Date | Shelf Edge | Label small | Label medium | Label large |
      | 100005    | 5 days before today | 10       | 000004       | 20.00 | U      | not set    | True       | True        | True         | False       |
      | 100005    | 6 days after today  | 10       | 000004       | 21.00 | U      | not set    | True       | True        | True         | False       |
      | 100005    | 7 days after today  | 10       | 000004       | 22.00 | U      | not set    | True       | False       | False        | False       |
      | 100009    | 6 days after today  | 10       | 000012       | 12    | U      | not set    | True       | False       | True         | False       |
Then Number of Price Changes including Stock not held by Store that are still pending is 3
And Number of Price Changes including Stock not held by Store for Today is 4
And Number of Price Changes including Stock not held by Store that are Overdue is 1

Scenario: Price Change Dashboard shows correct Price Change Audit Report Quantity
Given Price Change table is empty
And Current system TOTDT date is yesterday
And Stock Master contained next data for SKUs:
      | SKU Number | Is Non-Stock Item | Is Obsolete | Is Auto Apply Price Changes | On Hand Stock Quantity | Mark-down Stock Quantity |
      | 100004     | True              | False       | False                       | 0                      | 0                        |
      | 100005     | True              | True        | False                       | 3                      | 0                        |
      | 100009     | True              | False       | False                       | 0                      | -3                       |
      | 100006     | False             | False       | False                       | 0                      | 1                        |
And Recent Price for SKU '100004' was applied today manually with Price 22.00
And Recent Price for SKU '100005' was auto applied during yesterday Nightly Routine with Price 22.00
And Recent Price for SKU '100009' was auto applied during yesterday Nightly Routine with Price 12.00
And Recent Price for SKU '100006' was auto applied a week ago with Price 33.00
When the following price change events exist in system without exact dates:
      | SkuNumber | Date                | Apply Price Date    | Priority | Event number | Price | Status | Apply Date | Shelf Edge | Label small | Label medium | Label large |
      | 100004    | today               | yesterday           | 10       | 000004       | 20.00 | S      | not set    | True       | True        | True         | False       |
      | 100004    | tomorrow            | today               | 10       | 000004       | 22.00 | A      | not set    | True       | True        | True         | False       |
      | 100005    | 5 days before today | 5 days before today | 10       | 000004       | 20.00 | A      | not set    | True       | True        | True         | False       |
      | 100005    | 6 days after today  | 6 days after today  | 10       | 000004       | 21.00 | L      | not set    | True       | True        | True         | False       |
      | 100005    | today               | yesterday           | 10       | 000004       | 22.00 | S      | not set    | True       | False       | False        | False       |
      | 100009    | today               | yesterday           | 10       | 000012       | 12.00 | S      | not set    | True       | False       | True         | False       |
      | 100009    | tomorrow            | tomorrow            | 10       | 000012       | 12.00 | U      | not set    | True       | False       | True         | False       |
Then Count of Price Change Audit Report is equal to 2