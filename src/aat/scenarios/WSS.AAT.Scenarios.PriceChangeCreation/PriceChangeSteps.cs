extern alias bltoolkit;
using System;
using System.Linq;
using System.Text;
using Cts.Oasys.WinForm;
using NUnit.Framework;
using TechTalk.SpecFlow;
using WSS.AAT.Common.Utility.Bindings;
using WSS.AAT.Common.Utility.Utility;
using WSS.BO.DataLayer.Model.Entities;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Scenarios.PriceChangesCreation
{
    [Binding]
    public class PriceChangeCreationFromEventsSteps : BaseStepDefinitions
    {
        private readonly TablesRepository repo;

        public PriceChangeCreationFromEventsSteps(IDataLayerFactory dataLayerFactory)
        {
            repo = dataLayerFactory.Create<TablesRepository>();
        }

        [Given(@"that current system date is '(.*)'")]
        public void GivenThatCurrentSystemDateIs(DateTime sysDate)
        {
            repo.UpdateSystemDateTable(sysDate);
        }

        [Given(@"Current system TOTDT date is yesterday")]
        public void GivenCurrentSystemTOTDTDateIsYesterday()
        {
            repo.UpdateSystemDateTable(DateTime.Today.AddDays(-1));
        }

        [Given(@"date of master opening is '(.*)'")]
        public void GivenDateOfMasterOpeningIs(DateTime sysDate)
        {
            repo.UpdateNetworkIDSystemTable(sysDate);
        }

        [Given(@"the following SKUs are saved with parameters:")]
        public void GivenTheFollowingSkUsAreSavedWithParameters(Table table)
        {
            var wrappedTable = new SpecflowTableWrapper<StockMaster>(table);
            var stockMaster = wrappedTable.GetEntities(CreateEmptyStockMaster);
            repo.DeleteFromPriceChangeTable();
            repo.UpdateStockMasterTable(stockMaster);
        }

        [Given(@"there is no parameters for SKU '(.*)'")]
        public void GivenThereIsNoParametersForSku(string sku)
        {
            repo.DeleteFromStockMasterTable(sku);
        }
        
        [Given(@"the following price events were uploaded:")]
        public void GivenTheFollowingPriceEventsWereUploaded(Table table)
        {
            var wrappedTable = new SpecflowTableWrapper<EventChange>(table);
            var eventChange = wrappedTable.GetEntities();
            repo.DeleteFromEventChangeTable();
            repo.InsertEventChangeTable(eventChange);
        }

        [Given(@"the following price change events exist in system:")]
        public void GivenTheFollowingPriceChangeEventsExistInSystem(Table table)
        {
            var wrappedTable = new SpecflowTableWrapper<PriceChange>(table);
            var priceChange = wrappedTable.GetEntities(Utility.CreateEmptyPriceChange);
            repo.InsertPriceChangeTable(priceChange);
        }

        [When(@"I click ""(.*)"" from Back Office menu")]
        public void WhenIClickFromBackOfficeMenu(string p0)
        {
            UpdatePriceChanges();
        }

        [Then(@"there's no new Price Change event created")]
        public void ThenThereSNoNewPriceChangeEventCreated()
        {
            bool priceChangesCreated = repo.IsPriceChangeEventCreated();
            Assert.False(priceChangesCreated, "There are some unexpected price changes.");
        }

        [Then(@"the following Price Change events should be created:")]
        public void ThenTheFollowingPriceChangeEventsShouldBeCreated(Table table)
        {
            var wrappedTablePriceChange = new SpecflowTableWrapper<PriceChange>(table);
            var expected = wrappedTablePriceChange
                .GetEntities()
                .OrderBy(pc => pc.SkuNumber).ThenBy(pc => pc.Date);
            var actual = repo.GetTablePriceChange()
                .OrderBy(pc => pc.SkuNumber).ThenBy(pc => pc.Date);
            if (!expected.SequenceEqual(actual, wrappedTablePriceChange.GetComparer()))
            {
                StringBuilder sb = new StringBuilder("Actual price changes differ from expected:\n");
                wrappedTablePriceChange.FormatTable(actual, sb);
                Assert.Fail(sb.ToString());
            }
        }

        private void UpdatePriceChanges()
        {
            var form = new UpdPriceChgFrmEvts.UpdPriceChgFrmEvts(44, 1, 9, " (/P=',CFC')");
            HostForm host = new HostForm(form, false);
            host.ShowDialog();
        }

        private static StockMaster CreateEmptyStockMaster()
        {
            return new StockMaster
            {
                Obsolete = "0",
                NonStocked = "0",
                StockOnHand = 0,
                AutoApply = "0",
                LabelSmall = 0,
                LabelLarge = 1,
                LabelMedium = 1,
                MarkdownStock = 0,
                HierarchyCategory = "0",
                HierarchyGroup = "0",
                HierarchySubGroup = "0",
                HierarchyStyle = "0"
            };
        }
    }
}