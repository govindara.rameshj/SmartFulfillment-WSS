﻿@PriceChange
Feature: Price Change Creation from Events
    In order to have correct price change process
    As a store colleague
    I want correct price change generation

Scenario:001 Price change sent which is already active in the system
    Given that current system date is '2010-05-26' 
    And the following SKUs are saved with parameters:
    | SkuNumber | Price | Event number | Priority |
    | 100007    | 5.00  | 000100       | 10       |
    And the following price events were uploaded:
    | SkuNumber | Start date | Priority | Event number | Ending date | Price |
    | 100007    | 04/01/2010 | 10       | 000100       | not set     | 5.00  |
    When I click "Update from Price Events" from Back Office menu
    Then there's no new Price Change event created

Scenario:002 Promotional Price Change sent to store effective next day with an existing Base price record.
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007    | 2010-05-27 | 20       | 000105       | 2010-07-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-27 | 7.50  | 000105       | 20       |

Scenario:003 Promotional Price Change sent to store effective next day and ending within 7 days with an existing Base price record
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007    | 2010-05-27 | 20       | 000105       | 2010-05-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-27 | 7.50  | 000105       | 20       |
      | 100007    | 2010-05-30 | 5.00  | 000100       | 10       |
      
Scenario:004 Regression from Promotional Price to Base Price
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 7.50  | 000105       | 20       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007    | 2010-05-24 | 20       | 000105       | 2010-05-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-30 | 5.00  | 000100       | 10       |

Scenario:005 Promotional Price sent to store and not actioned and regressing to base price within 7 days
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007    | 2010-05-24 | 20       | 000105       | 2010-05-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-27 | 7.50  | 000105       | 20       |
      | 100007    | 2010-05-30 | 5.00  | 000100       | 10       |

Scenario:006 Promotional Price change is active and end date of Type 20 is greater than 7 days
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 7.50  | 000105       | 20       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007    | 2010-05-24 | 20       | 000105       | 2010-07-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then there's no new Price Change event created

Scenario:007 Promotional Price change effective the same day and ending within 7 days
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007    | 2010-05-26 | 20       | 000105       | 2010-05-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-27 | 7.50  | 000105       | 20       |
      | 100007    | 2010-05-30 | 5.00  | 000100       | 10       |

Scenario:008 STKMAS Event Information Missing
     Given that current system date is '2010-05-25'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 5.00  | 000000       | 00       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007    | 2010-05-26 | 20       | 000105       | 2010-05-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-26 | 7.50  | 000105       | 20       |
      | 100007    | 2010-05-30 | 5.00  | 000100       | 10       |

Scenario:009 SKU on promotion and reverting back to the base price
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 7.50  | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |

Scenario:010 SKU on promotion and reverting back to a new base price (no deletion flag on first Type 10)
     Given that current system date is '2010-05-29'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 17.50 | 000105       | 20       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007    | 2010-05-26 | 20       | 000105       | 2010-05-29  | 17.50 |
      | 100007    | 2010-05-28 | 10       | 000110       | not set     | 20.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-30 | 20.00 | 000110       | 10       |

Scenario:011 SKU on promotion and reverting back to a new base price (with deletion flag on first Type 10)
     Given that current system date is '2010-05-29'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 17.50 | 000105       | 20       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007    | 2010-05-26 | 20       | 000105       | 2010-05-29  | 17.50 |
      | 100007    | 2010-05-28 | 10       | 000110       | not set     | 20.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-30 | 20.00 | 000110       | 10       |

Scenario:012 SKU on promotion and going to a new promotional price
     Given that current system date is '2010-05-27'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 20.00 | 000105       | 20       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007    | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100007    | 2010-05-30 | 20       | 000110       | 2010-06-10  | 15.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-05-30 | 15.00 | 000110       | 20       |

Scenario:013 SKU on promotion and reverting from new promotional price to original base price
     Given that current system date is '2010-06-10'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 15.00 | 000110       | 20       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007    | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100007    | 2010-05-30 | 20       | 000110       | 2010-06-10  | 15.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Price | Event number | Priority |
      | 100007    | 2010-06-11 | 25.00 | 000100       | 10       |

Scenario:014 SKU on multiple promotions
     Given that current system date is '2010-05-25'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 25.00 | 000100       | 10       | 
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100007 | 2010-05-30 | 20       | 000110       | 2010-06-10  | 15.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-26 | 20.00 | 000105       | 20       |
      | 100007 | 2010-05-30 | 15.00 | 000110       | 20       |

Scenario:015 SKU on multiple priority promotions - regressing
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 20.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100007 | 2010-05-30 | 20       | 000110       | 2010-06-01  | 15.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 15.00 | 000110       | 20       |
      | 100007 | 2010-06-02 | 25.00 | 000100       | 10       |
      
Scenario:016 SKU on continuous booklet promotions
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 25.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100007 | 2010-05-30 | 20       | 000110       | 2010-06-01  | 15.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-29 | 20.00 | 000105       | 20       |
      | 100007 | 2010-05-30 | 15.00 | 000110       | 20       |
      | 100007 | 2010-06-02 | 25.00 | 000100       | 10       |

Scenario:017 SKU on promotion and 2 new base prices sent
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 20.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100007 | 2010-05-30 | 10       | 000120       | not set     | 45.00 |
      | 100007 | 2010-05-31 | 10       | 000125       | not set     | 25.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 45.00 | 000120       | 10       |
      | 100007 | 2010-05-31 | 25.00 | 000125       | 10       |

Scenario:018 SKU on promotion and new promotion sent with same end date
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 20.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-06-02  | 20.00 |
      | 100007 | 2010-05-28 | 20       | 000110       | 2010-06-02  | 15.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-29 | 15.00 | 000110       | 20       |
      | 100007 | 2010-06-03 | 25.00 | 000100       | 10       |

Scenario:019 SKU on promotion and new promotion sent with same end date (deletion flag set on existing promotion)
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 20.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-06-02  | 20.00 |
      | 100007 | 2010-05-28 | 20       | 000110       | 2010-06-02  | 15.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-29 | 15.00 | 000110       | 20       |
      | 100007 | 2010-06-03 | 25.00 | 000100       | 10       |

Scenario:020 SKU at base price and new promotion sent and new base price sent
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 25.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-30 | 20       | 000105       | 2010-06-01  | 20.00 |
      | 100007 | 2010-06-02 | 10       | 000110       | not set     | 22.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 20.00 | 000105       | 20       |
      | 100007 | 2010-06-02 | 22.00 | 000110       | 10       |

Scenario:021 SKU at base price and new base price sent and new promotion sent
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 25.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-30 | 10       | 000105       | not set     | 22.00 |
      | 100007 | 2010-05-31 | 20       | 000110       | 2010-06-02  | 20.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 22.00 | 000105       | 10       |
      | 100007 | 2010-05-31 | 20.00 | 000110       | 20       |
      | 100007 | 2010-06-03 | 22.00 | 000105       | 10       |

Scenario:022 SKU at base price and new base price sent and new promotion sent (deletion flag sent on existing base event)
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 25.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-28 | 10       | 000105       | not set     | 22.00 |
      | 100007 | 2010-05-31 | 20       | 000110       | 2010-06-02  | 20.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-29 | 22.00 | 000105       | 10       |
      | 100007 | 2010-05-31 | 20.00 | 000110       | 20       |
      | 100007 | 2010-06-03 | 22.00 | 000105       | 10       |

Scenario:023 SKU at base price and new promotion sent and new base price sent (deletion flag sent on existing base price)
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 25.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 25.00 |
      | 100007 | 2010-05-28 | 20       | 000105       | 2010-06-01  | 20.00 |
      | 100007 | 2010-06-02 | 10       | 000110       | not set     | 22.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-29 | 20.00 | 000105       | 20       |
      | 100007 | 2010-06-02 | 22.00 | 000110       | 10       |

Scenario:024 Price Change sent which is already active in the system (Multiple SKU's Test)
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
      | 100005 | 7.00  | 000100       | 10       |
      | 100147 | 8.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 7.00  |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 8.00  |
     When I click "Update from Price Events" from Back Office menu
     Then there's no new Price Change event created

Scenario:025 Promotional Price Change sent to store effective next day with an existing Base price record
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
      | 100005 | 5.00  | 000100       | 10       |
      | 100147 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-27 | 20       | 000105       | 2010-07-29  | 7.50  |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100005 | 2010-05-27 | 20       | 000105       | 2010-07-29  | 7.50  |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100147 | 2010-05-27 | 20       | 000105       | 2010-07-29  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-27 | 7.50  | 000105       | 20       |
      | 100005 | 2010-05-27 | 7.50  | 000105       | 20       |
      | 100147 | 2010-05-27 | 7.50  | 000105       | 20       |

Scenario:026 Promotional Price Change sent to store effective next day and ending within 7 days with an existing Base price record 2
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
      | 100005 | 10.00 | 000100       | 10       |
      | 100147 | 15.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-27 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-27 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 15.00 |
      | 100147 | 2010-05-27 | 20       | 000105       | 2010-05-29  | 30.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-27 | 10.00 | 000105       | 20       |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |
      | 100005 | 2010-05-27 | 20.00 | 000105       | 20       |
      | 100005 | 2010-05-30 | 10.00 | 000100       | 10       |
      | 100147 | 2010-05-27 | 30.00 | 000105       | 20       |
      | 100147 | 2010-05-30 | 15.00 | 000100       | 10       |

Scenario:027 Regression from Promotional Price to Base Price 2
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000105       | 20       |
      | 100005 | 20.00 | 000105       | 20       |
      | 100147 | 30.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-24 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-24 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 15.00 |
      | 100147 | 2010-05-24 | 20       | 000105       | 2010-05-29  | 30.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |
      | 100005 | 2010-05-30 | 10.00 | 000100       | 10       |
      | 100147 | 2010-05-30 | 15.00 | 000100       | 10       |

Scenario:028 Promotional Price sent to store and not actioned and regressing to base price within 7 days 2
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
      | 100005 | 10.00 | 000100       | 10       |
      | 100147 | 15.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-24 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-24 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 15.00 |
      | 100147 | 2010-05-24 | 20       | 000105       | 2010-05-29  | 30.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-27 | 10.00 | 000105       | 20       |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |
      | 100005 | 2010-05-27 | 20.00 | 000105       | 20       |
      | 100005 | 2010-05-30 | 10.00 | 000100       | 10       |
      | 100147 | 2010-05-27 | 30.00 | 000105       | 20       |
      | 100147 | 2010-05-30 | 15.00 | 000100       | 10       |

Scenario:029 Promotional Price change is active and end date of Type 20 is greater than 7 days 2
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000105       | 20       |
      | 100005 | 20.00 | 000105       | 20       |
      | 100147 | 30.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-24 | 20       | 000105       | 2010-07-29  | 10.00 |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-24 | 20       | 000105       | 2010-07-29  | 20.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 15.00 |
      | 100147 | 2010-05-24 | 20       | 000105       | 2010-07-29  | 30.00 |
     When I click "Update from Price Events" from Back Office menu
     Then there's no new Price Change event created

Scenario:030 Promotional Price change effective the same day and ending within 7 days 2
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
      | 100005 | 10.00 | 000100       | 10       |
      | 100147 | 15.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 15.00 |
      | 100147 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 30.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-27 | 10.00 | 000105       | 20       |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |
      | 100005 | 2010-05-27 | 20.00 | 000105       | 20       |
      | 100005 | 2010-05-30 | 10.00 | 000100       | 10       |
      | 100147 | 2010-05-27 | 30.00 | 000105       | 20       |
      | 100147 | 2010-05-30 | 15.00 | 000100       | 10       |

Scenario:031 STKMAS Event Information Missing 2
     Given that current system date is '2010-05-25'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000000       | 00       |
      | 100007 | 5.00  | 000000       | 00       |
      | 100007 | 5.00  | 000000       | 00       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 15.00 |
      | 100147 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 30.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-26 | 10.00 | 000105       | 20       |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |
      | 100005 | 2010-05-26 | 20.00 | 000105       | 20       |
      | 100005 | 2010-05-30 | 10.00 | 000100       | 10       |
      | 100147 | 2010-05-26 | 30.00 | 000105       | 20       |
      | 100147 | 2010-05-30 | 15.00 | 000100       | 10       |

Scenario:032 SKU on promotion and reverting back to the base price 2
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000105       | 20       |
      | 100005 | 20.00 | 000105       | 20       |
      | 100147 | 30.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 15.00 |
      | 100147 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 30.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |
      | 100005 | 2010-05-30 | 10.00 | 000100       | 10       |
      | 100147 | 2010-05-30 | 15.00 | 000100       | 10       |

Scenario:033 Combination Test 1
     Given that current system date is '2010-05-29'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000105       | 20       |
      | 100005 | 20.00 | 000105       | 20       |
      | 100147 | 30.00 | 000100       | 10       |
      | 100149 | 50.00 | 000100       | 10       |
      | 100004 | 60.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100007 | 2010-05-28 | 10       | 000110       | not set     | 15.00 |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100005 | 2010-05-28 | 10       | 000110       | not set     | 25.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 15.00 |
      | 100147 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 30.00 |
      | 100147 | 2010-05-30 | 20       | 000110       | 2010-06-10  | 35.00 |
      | 100149 | 2010-04-01 | 10       | 000100       | not set     | 50.00 |
      | 100149 | 2010-05-30 | 20       | 000105       | 2010-06-01  | 55.00 |
      | 100149 | 2010-06-02 | 20       | 000110       | not set     | 60.00 |
      | 100004 | 2010-04-01 | 10       | 000100       | not set     | 60.00 |
      | 100004 | 2010-05-28 | 10       | 000105       | not set     | 65.00 |
      | 100004 | 2010-05-31 | 20       | 000110       | 2010-06-02  | 70.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 15.00 | 000110       | 10       |
      | 100005 | 2010-05-30 | 25.00 | 000110       | 10       |
      | 100147 | 2010-05-30 | 35.00 | 000110       | 20       |
      | 100149 | 2010-05-30 | 55.00 | 000105       | 20       |
      | 100149 | 2010-06-02 | 60.00 | 000110       | 20       |
      | 100004 | 2010-05-30 | 65.00 | 000105       | 10       |
      | 100004 | 2010-05-31 | 70.00 | 000110       | 20       |
      | 100004 | 2010-06-03 | 65.00 | 000105       | 10       |

 Scenario:034 Regression Test with no base 10 until later date
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 20       | 000100       | 2010-04-01  | 5.00  |
      | 100007 | 2010-05-24 | 10       | 000105       | not set     | 10.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-27 | 10.00 | 000105       | 10       |

Scenario:035 Lower Event Number Test
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 15.00 | 000110       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 20       | 000100       | 2010-04-01  | 5.00  |
      | 100007 | 2007-01-01 | 10       | 000105       | not set     | 10.00 |
      | 100007 | 2009-01-01 | 10       | 000110       | not set     | 15.00 |
     When I click "Update from Price Events" from Back Office menu
     Then there's no new Price Change event created

Scenario:036 Combination Test 2
     Given that current system date is '2010-05-29'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000105       | 20       |
      | 100005 | 20.00 | 000105       | 20       |
      | 100147 | 30.00 | 000105       | 20       |
      | 100149 | 40.00 | 000105       | 20       |
      | 100004 | 50.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 1.00  |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100007 | 2010-05-28 | 10       | 000110       | not set     | 6.00  |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 2.00  |
      | 100005 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100005 | 2010-05-28 | 10       | 000110       | not set     | 7.00  |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 3.00  |
      | 100147 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 30.00 |
      | 100147 | 2010-05-28 | 10       | 000110       | not set     | 8.00  |
      | 100149 | 2010-04-01 | 10       | 000100       | not set     | 4.00  |
      | 100149 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 40.00 |
      | 100149 | 2010-05-28 | 10       | 000110       | not set     | 9.00  |
      | 100004 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100004 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 50.00 |
      | 100004 | 2010-05-28 | 10       | 000110       | not set     | 10.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 6.00  | 000110       | 10       |
      | 100005 | 2010-05-30 | 7.00  | 000110       | 10       |
      | 100147 | 2010-05-30 | 8.00  | 000110       | 10       |
      | 100149 | 2010-05-30 | 9.00  | 000110       | 10       |
      | 100004 | 2010-05-30 | 10.00 | 000110       | 10       |

Scenario:037 Combination Test 3
     Given that current system date is '2010-05-29'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000105       | 20       |
      | 100005 | 20.00 | 000105       | 20       |
      | 100147 | 30.00 | 000105       | 20       |
      | 100149 | 40.00 | 000105       | 20       |
      | 100004 | 50.00 | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 1.00  |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100007 | 2010-05-28 | 10       | 000110       | not set     | 6.00  |
      | 100005 | 2010-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100005 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 20.00 |
      | 100005 | 2010-05-28 | 10       | 000110       | not set     | 25.00 |
      | 100147 | 2010-04-01 | 10       | 000100       | not set     | 3.00  |
      | 100147 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 30.00 |
      | 100147 | 2010-05-28 | 10       | 000110       | not set     | 8.00  |
      | 100149 | 2010-04-01 | 10       | 000100       | not set     | 4.00  |
      | 100149 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 40.00 |
      | 100149 | 2010-05-28 | 10       | 000110       | not set     | 9.00  |
      | 100004 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100004 | 2010-05-28 | 20       | 000105       | 2010-05-29  | 50.00 |
      | 100004 | 2010-05-28 | 20       | 000110       | 2010-06-01  | 10.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 6.00  | 000110       | 10       |
      | 100005 | 2010-05-30 | 25.00 | 000110       | 10       |
      | 100147 | 2010-05-30 | 8.00  | 000110       | 10       |
      | 100149 | 2010-05-30 | 9.00  | 000110       | 10       |
      | 100004 | 2010-05-30 | 10.00 | 000110       | 20       |
      | 100004 | 2010-06-02 | 5.00  | 000100       | 10       |

Scenario:038 Combination Test 4
     Given that current system date is '2010-05-29'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000105       | 20       |
      | 100004 | 60.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-26 | 20       | 000105       | 2010-05-29  | 10.00 |
      | 100007 | 2010-05-28 | 10       | 000110       | not set     | 15.00 |
      | 100004 | 2010-04-01 | 10       | 000100       | not set     | 60.00 |
      | 100004 | 2010-05-28 | 10       | 000105       | not set     | 65.00 |
      | 100004 | 2010-05-31 | 20       | 000110       | 2010-06-02  | 70.00 |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 15.00 | 000110       | 10       |
      | 100004 | 2010-05-30 | 65.00 | 000105       | 10       |
      | 100004 | 2010-05-31 | 70.00 | 000110       | 20       |
      | 100004 | 2010-06-03 | 65.00 | 000105       | 10       |

Scenario:039 Promotional Type 30 sent to store
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-27 | 30       | 000105       | 2010-08-02  | 2.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-27 | 2.00  | 000105       | 30       |

Scenario:040 Promotional Type 30 Price Change sent to store effective next day and ending within 7 days with an existing Base price record
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-27 | 30       | 000105       | 2010-05-29  | 2.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-27 | 2.00  | 000105       | 30       |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |

Scenario:041 Errored STKMAS with no Event Information - Two Type 10 Records Test
     Given that current system date is '2010-05-26'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000000       | 00       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-01 | 10       | 000105       | not set     | 2.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-27 | 2.00  | 000105       | 10       |

Scenario:042 STKMAS Event Information Missing - Three Type 10 Records
     Given that current system date is '2010-06-10'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000000       | 00       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-01 | 10       | 000105       | not set     | 2.00  |
      | 100007 | 2010-06-01 | 10       | 000110       | not set     | 1.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-06-11 | 1.00  | 000110       | 10       |

Scenario:043 STKMAS Event Information Missing - Four Type 10 Records
     Given that current system date is '2010-07-10'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000000       | 00       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-01 | 10       | 000105       | not set     | 2.00  |
      | 100007 | 2010-06-01 | 10       | 000110       | not set     | 1.00  |
      | 100007 | 2010-07-01 | 10       | 000115       | not set     | 0.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-11 | 0.50  | 000115       | 10       |

Scenario:044 Regression Type 30 Price Change to Base price record
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 2.00  | 000105       | 30       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-27 | 30       | 000105       | 2010-05-29  | 2.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 5.00  | 000100       | 10       |

Scenario:045 Regression Type 30 Price Change to Type 20 and Base Record
     Given that current system date is '2010-05-28'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 2.00  | 000105       | 30       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-27 | 30       | 000105       | 2010-05-29  | 2.00  |
      | 100007 | 2010-05-29 | 20       | 000110       | 2010-06-02  | 1.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-30 | 1.00  | 000110       | 20       |
      | 100007 | 2010-06-03 | 5.00  | 000100       | 10       |

Scenario:046 STKMAS Event Information Missing - Two Type 10 and One Type 20
     Given that current system date is '2010-06-15'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 2.00  | 000000       | 00       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-27 | 10       | 000105       | not set     | 2.00  |
      | 100007 | 2010-05-29 | 20       | 000110       | 2010-08-02  | 1.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-06-16 | 1.00  | 000110       | 20       |

Scenario:047 STKMAS Event Information Missing - Five Events to sort and Prioritise
     Given that current system date is '2010-06-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 2.00  | 000000       | 00       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-12-01 | 10       | 000100       | not set     | 20.00 |
      | 100007 | 2010-01-01 | 10       | 000105       | not set     | 10.00 |
      | 100007 | 2010-02-01 | 20       | 000110       | 2010-05-02  | 6.00  |
      | 100007 | 2010-05-02 | 10       | 000115       | not set     | 4.00  |
      | 100007 | 2010-05-29 | 20       | 000120       | 2010-08-02  | 2.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-06-02 | 2.00  | 000120       | 20       |

Scenario:048 STKMAS Event Information Missing - Five Events to sort and Prioritise Test 2
     Given that current system date is '2010-06-15'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 2.00  | 000000       | 00       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-12-01 | 10       | 000100       | not set     | 20.00 |
      | 100007 | 2010-01-01 | 10       | 000105       | not set     | 10.00 |
      | 100007 | 2010-02-01 | 10       | 000110       | not set     | 6.00  |
      | 100007 | 2010-05-02 | 20       | 000115       | 2010-05-29  | 4.00  |
      | 100007 | 2010-05-29 | 20       | 000120       | 2010-08-02  | 2.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-06-16 | 2.00  | 000120       | 20       |

Scenario:049 STKMAS Event Information Missing - Five Events to sort and Prioritise Test 3
     Given that current system date is '2010-06-15'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 0.00  | 000000       | 00       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2008-12-01 | 10       | 000100       | not set     | 20.00 |
      | 100007 | 2009-01-01 | 20       | 000105       | 2009-12-31  | 10.00 |
      | 100007 | 2010-01-01 | 10       | 000110       | not set     | 6.00  |
      | 100007 | 2010-05-02 | 20       | 000115       | 2010-05-29  | 4.00  |
      | 100007 | 2010-05-30 | 20       | 000120       | 2010-08-02  | 2.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-06-16 | 2.00  | 000120       | 20       |

Scenario:050 SKU Promotional Priorities Increasing
     Given that current system date is '2010-05-24'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2010-05-25 | 30       | 000105       | 2010-05-26  | 2.00  |
      | 100007 | 2010-05-27 | 30       | 000110       | 2010-07-01  | 1.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-05-25 | 2.00  | 000105       | 30       |
      | 100007 | 2010-05-27 | 1.00  | 000110       | 30       |

Scenario:051 Create price change for first sku, STKMAS doesn't exist for second sku
     Given that current system date is '2010-07-19'
     And date of master opening is '2010-07-19'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100004 | 1.00  | 000000       | 00       |
      | 100005 | 1.00  | 000000       | 00       |
     And there is no parameters for SKU '100147'
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100004 | 2009-09-01 | 10       | 052420       | not set     | 0.01  |
      | 100147 | 2009-09-01 | 10       | 052420       | not set     | 0.01  |
      | 100005 | 2009-09-01 | 10       | 052420       | not set     | 0.01  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100004 | 2010-07-20 | 0.01  | 052420       | 10       |
      | 100005 | 2010-07-20 | 0.01  | 052420       | 10       |

Scenario:052 SKU AAPC Flag Test with On Hand Stock
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-10-01  | 5.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 5.00  | 000105       | 20       |

Scenario:053 SKU AAPC Flag Test with NO On Hand Stock
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-10-01  | 5.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 5.00  | 000105       | 20       |


Scenario:054 SKU IOBS Flag Test with On Hand Stock
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-10-01  | 5.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 5.00  | 000105       | 20       |

Scenario:055 SKU IOBS Flag Test with NO On Hand Stock
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-10-01  | 5.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 5.00  | 000105       | 20       |

Scenario:056 Existing PRCCHG Record Test
     Given that current system date is '2010-07-19'
     And date of master opening is '2010-07-19'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-10-01  | 5.00  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge |
      | 100007 | 2009-09-01 | 10       | 000100       | S      | 10.00 | 1          |
      | 100007 | 2010-07-01 | 10       | 000105       | U      | 10.00 | 0          |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority | Status |
      | 100007 | 2009-09-01 | 10.00 | 000100       | 10       | S      |
      | 100007 | 2010-07-20 | 5.00  | 000105       | 20       | U      |

Scenario:057 Applied PRCCHG Record Test
     Given that current system date is '2010-07-19'
     And date of master opening is '2010-07-19'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-10-01  | 5.00  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge |
      | 100007 | 2009-09-01 | 10       | 000100       | S      | 10.00 | 1          |
      | 100007 | 2010-07-01 | 20       | 000105       | S      | 5.00  | 1          |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2009-09-01 | 10.00 | 000100       | 10       |
      | 100007 | 2010-07-01 | 5.00  | 000105       | 20       |

Scenario:058 Dual Event Test (Progression)
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-10-01  | 5.00  |
      | 100007 | 2010-07-01 | 20       | 000110       | 2010-10-10  | 6.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 6.00  | 000110       | 20       |

Scenario:059 Dual Event Test (Regression 20 to 10)
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 20       | 000100       | 2010-06-20  | 10.00 |
      | 100007 | 2010-07-01 | 10       | 000105       | not set     | 5.00  |
      | 100007 | 2010-07-01 | 10       | 000110       | not set     | 6.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 6.00  | 000110       | 10       |

Scenario:060 Dual Event Test (Regression 30 to 20)
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 30       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-09-01 | 30       | 000100       | 2010-06-20  | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-07-20  | 5.00  |
      | 100007 | 2010-07-01 | 20       | 000110       | 2010-07-21  | 6.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 6.00  | 000110       | 20       |

Scenario:061 Combination Test 5
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-01-01 | 10       | 000005       | not set     | 50.00 |
      | 100007 | 2009-09-01 | 20       | 000100       | 2010-06-20  | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000105       | 2010-07-20  | 5.00  |
      | 100007 | 2010-07-01 | 20       | 000110       | 2010-07-20  | 6.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 6.00  | 000110       | 20       |

Scenario:062 Dual Event Priority Test
     Given that current system date is '2010-07-01'
     And date of master opening is '2010-07-01'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 50.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2009-01-01 | 10       | 000100       | not set     | 50.00 |
      | 100007 | 2010-07-01 | 10       | 000105       | not set     | 10.00 |
      | 100007 | 2010-07-01 | 20       | 000110       | 2010-07-20  | 5.00  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2010-07-02 | 5.00  | 000110       | 20       |

Scenario:063 Type 20 event has ended but no Regression Type 10 exists - Test that application does not crash
     Given that current system date is '2010-07-21'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000110       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2010-07-01 | 20       | 000110       | 2010-07-20  | 5.00  |
     When I click "Update from Price Events" from Back Office menu
     Then there's no new Price Change event created

Scenario:064 Type 20 event has start date before today and end date greater than 7 days after today
     Given that current system date is '2011-09-10'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03| 10       | 000100       | not set        | 5.00  |
      | 100007 | 2011-07-09| 10       | 000105       | not set        | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2011-09-11 | 7.50  | 000105       | 10       |

Scenario:065 Delete existing Unapplied Price Change record and create correct Price Change record when same Start Date
     Given that current system date is '2011-09-10'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03| 10       | 000100       | not set      | 5.00  |
      | 100007 | 2011-09-07| 10       | 000102       | not set      | 6.25  |
      | 100007 | 2011-09-07| 20       | 000105       | 2012-01-03   | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price |
      | 100007 | 2011-09-07 | 10       | 000102       | U      | 6.25  | 
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2011-09-11 | 7.50  | 000105       | 20       |

Scenario:066 Do not create regression event if Type 20 end date is exactly 7 days ahead (DCPA +7) and next event is due next day (T20)
     Given that current system date is '2011-10-13'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-10-15 | 20       | 000105       | 2011-10-20  | 3.50  |
      | 100007 | 2011-10-21 | 20       | 000110       | 2011-10-28  | 7.49  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2011-10-15 | 3.50  | 000105       | 20       |

Scenario:067 Do not create regression event if Type 20 end date is exactly 7 days ahead (DCPA +7) and next event is due next day (T10)
     Given that current system date is '2011-10-13'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-10-15 | 20       | 000105       | 2011-10-20  | 3.50  |
      | 100007 | 2011-10-21 | 10       | 000110       | not set     | 7.49  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2011-10-15 | 3.50  | 000105       | 20       |

Scenario:068 Do not create regression event if Type 20 end date is exactly 7 days ahead (DCPA +7). (Will get created on following day)
     Given that current system date is '2011-10-13'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 5.00  | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-10-15 | 20       | 000105       | 2011-10-20  | 3.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2011-10-15 | 3.50  | 000105       | 20       |

Scenario:069 Do not set PRCCHG label print flags if Obsolete and no stock
     Given that current system date is '2011-10-13'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority | Stock On hand | Obsolete |
      | 100007 | 5.00  | 000100       | 10       | 0             | 1        |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-10-15 | 20       | 000105       | 2011-10-20  | 3.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority | Shelf Edge | Label Small | Label Medium | Label Large |
      | 100007 | 2011-10-15 | 3.50  | 000105       | 20       | True       | False       | False        | False       |

Scenario:070 Set PRCCHG label print flags if Obsolete and with stock
     Given that current system date is '2011-10-13'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority | Obsolete | Stock On Hand |
      | 100007 | 5.00  | 000100       | 10       | 1        | 1             |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-10-15 | 20       | 000105       | 2011-10-20  | 3.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority | Shelf Edge | Label Small | Label Medium | Label Large |
      | 100007 | 2011-10-15 | 3.50  | 000105       | 20       | False      | False       | True         | True        |

Scenario:071 Do not set PRCCHG label print flags if Non-Stock and no stock
     Given that current system date is '2011-10-13'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority | Obsolete | Stock on hand | Non Stocked |
      | 100007 | 5.00  | 000100       | 10       | 0        | 0             | 1           |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-10-15 | 20       | 000105       | 2011-10-20  | 3.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority | Shelf Edge | Label Small | Label Medium | Label Large |
      | 100007 | 2011-10-15 | 3.50  | 000105       | 20       | True       | False       | False        | False       |

Scenario:072 Set PRCCHG label print flags if Non-Stock and with stock
     Given that current system date is '2011-10-13'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority | Obsolete | Stock on hand | Non Stocked |
      | 100007 | 5.00  | 000100       | 10       | 0        | 1             | 1           |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-01-03 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-10-15 | 20       | 000105       | 2011-10-20  | 3.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority | Shelf Edge | Label Small | Label Medium | Label Large |
      | 100007 | 2011-10-15 | 3.50  | 000105       | 20       | False      | False       | True         | True        |

Scenario:073 New Base Price sent when existing Promo price still active and end date > 7 days ahead
     Given that current system date is '2011-11-03'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 7.50  | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-05-24 | 20       | 000105       | 2012-01-03  | 7.50  |
      | 100007 | 2011-11-07 | 10       | 000110       | not set     | 6.25  |
     When I click "Update from Price Events" from Back Office menu
     Then there's no new Price Change event created

Scenario:074 New Base Price sent when existing Promo price still active and end date < 7 days ahead
     Given that current system date is '2011-11-03'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 7.50  | 000105       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-04-01 | 10       | 000100       | not set     | 5.00  |
      | 100007 | 2011-05-24 | 20       | 000105       | 2011-11-08  | 7.50  |
      | 100007 | 2011-11-07 | 10       | 000110       | not set     | 6.25  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2011-11-09 | 6.25  | 000110       | 10       |

Scenario:075 Current Type 20 finishes and regresses back to previous still valid Type 20
     Given that current system date is '2011-11-10'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 7.50  | 000110       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2011-04-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2011-05-24 | 20       | 000105       | 2012-01-03  | 9.00  |
      | 100007 | 2011-10-01 | 20       | 000110       | 2011-11-11  | 7.50  |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Price | Event number | Priority |
      | 100007 | 2011-11-12 | 9.00  | 000105       | 20       |

Scenario:076 PRCCHG already exists and labels have been printed but price change not yet applied. PRCCHG (Type 20) is for today's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-02 | 20       | 000105       | 2012-05-30  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-02 | 20       | 000105       | U      | 7.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | True       | False       | False        | False       |

Scenario:077 PRCCHG already exists and labels have been printed but price change not yet applied. PRCCHG (Type 20)  is for yesterday's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-01 | 20       | 000105       | 2012-05-30  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-01 | 20       | 000105       | U      | 7.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | True       | False       | False        | False       |

Scenario:078 PRCCHG already exists and labels have been printed but price change not yet applied. PRCCHG (Type 20)  is for tomorrow's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-30  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | True       | False       | False        | False       |

Scenario:079 PRCCHG already exists and labels have been printed but price change not yet applied. PRCCHG (Type 10) is for today's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-02 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-02 | 10       | 000105       | U      | 7.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | True       | False       | False        | False       |

Scenario:080 PRCCHG already exists and labels have been printed but price change not yet applied. PRCCHG (Type 10)  is for yesterday's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-01 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-01 | 10       | 000105       | U      | 7.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | True       | False       | False        | False       |

Scenario:081 PRCCHG already exists and labels have been printed but price change not yet applied. PRCCHG (Type 10)  is for tomorrow's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | True       | False       | False        | False       |

Scenario:082 PRCCHG already exists and labels have NOT been printed and price change not yet applied. PRCCHG (Type 20) is for today's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-02 | 20       | 000105       | 2012-05-30  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-02 | 20       | 000105       | U      | 7.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:083 PRCCHG already exists and labels have NOT been printed and price change not yet applied. PRCCHG (Type 20)  is for yesterday's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-01 | 20       | 000105       | 2012-05-30  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-01 | 20       | 000105       | U      | 7.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:084 PRCCHG already exists and labels have NOT been printed and price change not yet applied. PRCCHG (Type 20)  is for tomorrow's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-30  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:085 PRCCHG already exists and labels have NOT been printed and price change not yet applied. PRCCHG (Type 10) is for today's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-02 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-02 | 10       | 000105       | U      | 7.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:086 PRCCHG already exists and labels have NOT been printed and price change not yet applied. PRCCHG (Type 10)  is for yesterday's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-01 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-01 | 10       | 000105       | U      | 7.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:087 PRCCHG already exists and labels have been printed but price change not yet applied. PRCCHG (Type 10)  is for tomorrow's date
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:088 Existing Applied PRCCHG but new EVTCHG with same effective date and new price (Both Type 10, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | A      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:089 Existing Applied PRCCHG but new EVTCHG with same effective date and same price (Both Type 10, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | A      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:090 Existing Unapplied PRCCHG but new EVTCHG with same effective date and new price (Both Type 10, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | U      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:091 Existing Unapplied PRCCHG but new EVTCHG with same effective date and same price (Both Type 10, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | U      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:092 Existing Applied PRCCHG but new EVTCHG with same effective date and new price (Both Type 10, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | A      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:093 Existing Applied PRCCHG but new EVTCHG with same effective date and same price (Both Type 10, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | A      | 9.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 9.50  | False      | False       | True         | True        |

Scenario:094 Existing Unapplied PRCCHG but new EVTCHG with same effective date and new price (Both Type 10, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | U      | 9.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:095 Existing Unapplied PRCCHG but new EVTCHG with same effective date and same price (Both Type 10, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | U      | 9.50  | False      | False       | True         | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000105       | U      | 9.50  | False      | False       | True         | True        |

Scenario:096 Existing Applied PRCCHG but new EVTCHG with same effective date and new price (Existing Type 10, New Type 20, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | A      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:097 Existing Applied PRCCHG but new EVTCHG with same effective date and same price (Existing Type 10,New Type 20, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | A      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:098 Existing Unapplied PRCCHG but new EVTCHG with same effective date and new price (Existing Type 10, New Type 20, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | U      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:099 Existing Unapplied PRCCHG but new EVTCHG with same effective date and same price (Existing Type 10, New Type 20, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | U      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:100 Existing Applied PRCCHG but new EVTCHG with same effective date and new price (Existing Type 10, New Type 20, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | A      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:101 Existing Applied PRCCHG but new EVTCHG with same effective date and same price (Existing Type 10,New Type 20,  labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | A      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 9.50  | False      | False       | True         | True        |

Scenario:102 Existing Unapplied PRCCHG but new EVTCHG with same effective date and new price (Existing Type 10, New Type 20, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | U      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:103 Existing Unapplied PRCCHG but new EVTCHG with same effective date and same price (Existing Type 10,New Type 20,  labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 10       | 000102       | not set     | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 10       | 000102       | U      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 9.50  | False      | False       | True         | True        |

Scenario:104 Existing Applied PRCCHG but new EVTCHG with same effective date and new price (Both Type 20, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 20       | 000102       | 2012-05-31  | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000102       | A      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:105 Existing Applied PRCCHG but new EVTCHG with same effective date and same price (Both Type 20, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 20       | 000102       | 2012-05-31  | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000102       | A      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:106 Existing Unapplied PRCCHG but new EVTCHG with same effective date and new price (Both Type 20, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 20       | 000102       | 2012-05-31  | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000102       | U      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:107 Existing Unapplied PRCCHG but new EVTCHG with same effective date and same price (Both Type 20, labels printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 20       | 000102       | 2012-05-31  | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000102       | U      | 9.50  | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:108 Existing Applied PRCCHG but new EVTCHG with same effective date and new price (Both Type 20, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 20       | 000102       | 2012-05-31  | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 7.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000102       | A      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:109 Existing Applied PRCCHG but new EVTCHG with same effective date and same price (Both Type 20, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 9.50  | 000102       | 20       |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-05-03 | 20       | 000102       | 2012-05-31  | 9.50  |
      | 100007 | 2012-05-03 | 20       | 000105       | 2012-05-31  | 9.50  |
     And the following price change events exist in system:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000102       | A      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      |SkuNumber| Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007 | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007 | 2012-05-03 | 20       | 000105       | U      | 9.50  | False      | False       | True         | True        |

Scenario:110 Existing Unapplied PRCCHG but new EVTCHG with same effective date and new price (Both Type 20, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority |
      | 100007    | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-05-03 | 20       | 000102       | 2012-05-31  | 9.50  |
      | 100007    | 2012-05-03 | 20       | 000105       | 2012-05-31  | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-05-03 | 20       | 000102       | U      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-05-03 | 20       | 000105       | U      | 9.50  | False      | False       | True         | True        |

Scenario:111 Existing Unapplied PRCCHG but new EVTCHG with same effective date and same price (Both Type 20, labels not printed)
     Given that current system date is '2012-05-02'
     And the following SKUs are saved with parameters:
      |SkuNumber| Price | Event number | Priority |
      | 100007 | 10.00 | 000100       | 10       |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-05-03 | 20       | 000102       | 2012-05-31  | 9.50  |
      | 100007    | 2012-05-03 | 20       | 000105       | 2012-05-31  | 7.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-05-03 | 20       | 000102       | U      | 9.50  | False      | False       | False        | True        |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-05-03 | 20       | 000105       | U      | 7.50  | False      | False       | True         | True        |

Scenario:112 Scenario: Set Label print flags as require printing when STKMAS label quantity greater than 1, normal sku, new evtchg Type 10
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 1             | 0              | False    | False       | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 10       | 000105       | U      | 9.50  | False      | True        | True         | True        |

Scenario:113 Scenario: Set Label print flags as require printing when STKMAS label quantity greater than 1, normal sku, new evtchg Type 20
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 1             | 0              | False    | False       | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-08-24 | 20       | 000105       | 2012-08-26  | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 20       | 000105       | U      | 9.50  | False      | True        | True         | True        |
      | 100007    | 2012-08-27 | 10       | 000100       | U      | 10.00 | False      | True        | True         | True        |

Scenario:114 Scenario: Set Label print flags as require printing when STKMAS label quantity greater than 1, auto-apply sku with stock on-hand, new evtchg Type 10
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 1             | 0              | False    | False       | True       | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 10       | 000105       | U      | 9.50  | False      | True        | True         | True        |

Scenario:115 Scenario: Set Label print flags as require printing when STKMAS label quantity greater than 1, auto-apply sku with stock on-hand, new evtchg Type 20
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 1             | 0              | False    | False       | True       | 2           | 2            | 2           |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-08-24 | 20       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 20       | 000105       | U      | 9.50  | False      | True        | True         | True        |

Scenario:116 Scenario: Set Label print flags as require printing when STKMAS label quantity greater than 1, auto-apply sku with zero stock, new evtchg Type 10
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 0             | 0              | False    | False       | True       | 2           | 2            | 2           |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-08-24 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 10       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:117 Scenario: Set Label print flags as require printing when STKMAS label quantity greater than 1, auto-apply sku with zero stock, new evtchg Type 20
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 0             | 0              | False    | False       | True       | 2           | 2            | 2           |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-08-24 | 20       | 000105       | 2012-08-26  | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | 0           | 0            | 0           |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 20       | 000105       | U      | 9.50  | True       | False       | False        | False       |
      | 100007    | 2012-08-27 | 10       | 000100       | U      | 10.00 | True       | False       | False        | False       |

Scenario:118 Set Label print flags as require printing when STKMAS label quantity greater than 1, obsolete sku with stock on-hand, new evtchg Type 10
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 1             | 0              | True     | False       | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      |SkuNumber| Start date | Priority | Event number | Ending date | Price |
      | 100007 | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007 | 2012-08-24 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 10       | 000105       | U      | 9.50  | False      | True        | True         | True        |

Scenario:119 Set Label print flags as require printing when STKMAS label quantity greater than 1, obsolete sku with stock on-hand, new evtchg Type 20
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 1             | 0              | True     | False       | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 20       | 000105       | 2012-08-26  | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 20       | 000105       | U      | 9.50  | False      | True        | True         | True        |
      | 100007    | 2012-08-27 | 10       | 000100       | U      | 10.00 | False      | True        | True         | True        |

Scenario:120 Set Label print flags as require printing when STKMAS label quantity greater than 1, non-stocked sku with stock on-hand, new evtchg Type 10
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 1             | 0              | False    | True        | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 10       | 000105       | U      | 9.50  | False      | True        | True         | True        |

Scenario:121 Set Label print flags as require printing when STKMAS label quantity greater than 1, non-stocked sku with stock on-hand, new evtchg Type 20
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 1             | 0              | False    | True        | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 20       | 000105       | 2012-08-26  | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 20       | 000105       | U      | 9.50  | False      | True        | True         | True        |
      | 100007    | 2012-08-27 | 10       | 000100       | U      | 10.00 | False      | True        | True         | True        |

Scenario:122 Set Label print flags as already printed when STKMAS label quantity greater than 1, obsolete sku with zero stock, new evtchg Type 10
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 0             | 0              | True     | False       | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 10       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:123 Set Label print flags as require printing when STKMAS label quantity greater than 1, obsolete sku with zero stock, new evtchg Type 20
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 0             | 0              | True     | False       | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 20       | 000105       | 2012-08-26  | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 20       | 000105       | U      | 9.50  | True       | False       | False        | False       |
      | 100007    | 2012-08-27 | 10       | 000100       | U      | 10.00 | True       | False       | False        | False       |

Scenario:124 Set Label print flags as require printing when STKMAS label quantity greater than 1, non-stocked sku with zero stock, new evtchg Type 10
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 0             | 0              | False    | True        | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 10       | 000105       | not set     | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 10       | 000105       | U      | 9.50  | True       | False       | False        | False       |

Scenario:125 Set Label print flags as require printing when STKMAS label quantity greater than 1, non-stocked sku with zero stock, new evtchg Type 20
     Given that current system date is '2012-08-22'
     And the following SKUs are saved with parameters:
      | SkuNumber | Price | Event number | Priority | Stock on hand | Markdown Stock | Obsolete | Non Stocked | Auto Apply | Label small | Label medium | Label large |
      | 100007    | 10.00 | 000100       | 10       | 0             | 0              | False    | True        | False      | 2           | 2            | 2           |
     And the following price events were uploaded:
      | SkuNumber | Start date | Priority | Event number | Ending date | Price |
      | 100007    | 2012-01-01 | 10       | 000100       | not set     | 10.00 |
      | 100007    | 2012-08-24 | 20       | 000105       | 2012-08-26  | 9.50  |
     And the following price change events exist in system:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
     When I click "Update from Price Events" from Back Office menu
     Then the following Price Change events should be created:
      | SkuNumber | Date       | Priority | Event number | Status | Price | Shelf Edge | Label small | Label medium | Label large |
      | 100007    | 2012-01-01 | 10       | 000100       | S      | 10.00 | True       | False       | False        | False       |
      | 100007    | 2012-08-24 | 20       | 000105       | U      | 9.50  | True       | False       | False        | False       |
      | 100007    | 2012-08-27 | 10       | 000100       | U      | 10.00 | True       | False       | False        | False       |
