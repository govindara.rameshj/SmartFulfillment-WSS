using System;
using TechTalk.SpecFlow;
using System.Data;
using System.Linq;
using WSS.AAT.Common.DataLayer.Entities;
using System.Collections.Generic;
using Cts.Oasys.Core.Tests;
using NUnit.Framework;
using WSS.BO.DataLayer.Model;
using WSS.AAT.Common.Utility.Bindings;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Qod.CtsOrderManager;
using Cts.Oasys.Hubs.Core.Order.WebService;

namespace WSS.AAT.Scenarios.ManagersDashboardReports
{
    [Binding]
    public class ManagersDashboardReportsSteps : BaseStepDefinitions
    {
        private readonly IDataLayerFactory dataLayerFactory;
        private readonly TestSystemEnvironment systemEnvironment;
        DateTime dashboardOpenDay;
        DataTable reportTable;
        private static Dictionary<string, Func<DateTime, DataTable>> reportNamesToMethodsDictionary = new Dictionary<string,Func<DateTime,DataTable>>();
        private ManagersDashboardReportsRepository repo;
        private string orderNumber;

        public ManagersDashboardReportsSteps(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
        {
            this.dataLayerFactory = dataLayerFactory;
            this.systemEnvironment = systemEnvironment;

            repo = dataLayerFactory.Create<ManagersDashboardReportsRepository>();
            if (!reportNamesToMethodsDictionary.Any())
            {
                CreateReportNamesToMethodsDictionary();
            }
        }

        [Given(@"Dashboard was opened for today")]
        public void GivenDashboardWasOpenedForToday()
        {
            dashboardOpenDay = DateTime.Now.Date;
        }

        [Given(@"Dashboard was opened with a year ago date")]
        public void GivenDashboardWasOpenedWithAYearAgoDate()
        {
            dashboardOpenDay = repo.GetThisDayLastYear(DateTime.Now.Date);
        }

        [Given(@"Sale Ledger contains next transactions")]
        public void GivenSaleLedgerContainsNextTransactions(Table table)
        {
            var dailyRepo = dataLayerFactory.Create<TestDailyTillRepository>();

            dailyRepo.CleanDltots();

            foreach (TableRow row in table.Rows)
            {
                string transactionCode = GetTransactionCodeFromFullDefinition(row["Type"]);
                dailyRepo.InsertIntoDltots(transactionCode, decimal.Parse(row["Total Amount"]), row["Cashier Number"], row["Till Number"], short.Parse(row["Misc. Reason Code"]), repo.GetThisDayLastYear(DateTime.Now.Date));
            }
        }

        [Given(@"There was a payment from Vision with total Amount (.*)")]
        public void GivenThereWasAPaymentFromVisionWithTotalAmount(decimal amount)
        {
            var visRepo = dataLayerFactory.Create<TestVisionPaymentRepository>();

            visRepo.CleanVisionPayment();
            visRepo.InsertIntoVisionPayment(amount, repo.GetThisDayLastYear(DateTime.Now.Date));
        }

        [Given(@"Following Stock Adjustment were made today")]
        public void GivenFollowingStockAdjustmentWereMadeToday(Table table)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            stockRepo.CleanStockAdjustment();

            foreach (TableRow row in table.Rows)
            {
                stockRepo.InsertIntoStockAdjustment(row["SKU Number"], decimal.Parse(row["Quantity"]), decimal.Parse(row["Price"]), row["Write Off's Authorized Cashier"], dashboardOpenDay, "W");
            }
        }

        [Given(@"There were next products")]
        public void GivenThereWereNextProducts(Table table)
        {
            var stockRepo = dataLayerFactory.Create<TestStockRepository>();

            var skuList = table.Rows.AsEnumerable().ToList().Select(x => x["SKU Number"]).ToList();

            stockRepo.DeleteAllExcept(skuList);

            foreach (TableRow row in table.Rows)
            {
                stockRepo.UpdateStockMaster(row["SKU Number"], Int32.Parse(row["On Hand Stock Quantity"]), Int32.Parse(row["Mark-down Stock Quantity"]));
            }
        }

        [Given(@"Click and collect order happened")]
        public void GivenClickAndCollectOrderHappened()
        {
            CreationRoutine innerRoutine = new CreationRoutine(dataLayerFactory, systemEnvironment);

            innerRoutine.PrepareRequestForStandardClickAndCollectOrder();
            innerRoutine.SetSaleDate(DateTime.Now.Date);
            innerRoutine.ProcessPreparedRequest();
            CTSQODCreateResponse response = innerRoutine.CurrentResponse;
            Assert.That(response, Is.Not.Null, "Response is null, check logs.");
            Assert.That(response.IsSuccessful(), Is.True, "Response is not succesful, check logs.");

            orderNumber = response.OrderHeader.SourceOrderNumber.Value;
        }

        [Given(@"The order was refunded")]
        public void GivenTheOrderWasRefunded()
        {
            EnableRefundCreationRoutine innerRoutine = new EnableRefundCreationRoutine(dataLayerFactory, systemEnvironment);

            innerRoutine.PrepareRequestForStandardClickAndCollectOrder(orderNumber);
            innerRoutine.SetRefundDate(DateTime.Now.Date);
            innerRoutine.ProcessPreparedRequest();
            CTSEnableRefundCreateResponse response = innerRoutine.CurrentResponse;
            Assert.That(response, Is.Not.Null, "Response is null, check logs.");
            Assert.That(response.IsSuccessful(), Is.True, "Response is not succesful, check logs.");
        }


        [When(@"'(.*)' Report was generated")]
        public void WhenReportWasGenerated(string reportName)
        {
            Func<DateTime, DataTable> reportMethod = reportNamesToMethodsDictionary[reportName];

            reportTable = reportMethod(dashboardOpenDay);
        }
        
        [Then(@"The report contains only next rows")]
        public void ThenTheReportContainsOnlyNextRows(Table table)
        {
            Assert.AreEqual(table.Rows.Count, reportTable.Rows.Count);
            CheckReports(table);
        }

        [Then(@"The report contains next rows")]
        public void ThenTheReportContainsNextRows(Table table)
        {
            CheckReports(table);
        }

        private void CheckReports(Table table)
        {
            if (table.ContainsColumn("QtyWtd") || table.ContainsColumn("ValueWtd"))
            {
                CheckIfStoreSalesReportHasCorrectNumbers(table);
            }
            else
            {
                if (table.ContainsColumn("Today Qty") || table.ContainsColumn("Today Value"))
                {
                    CheckIfPendingShrinkageReportHasCorrectNumbers(table);
                }
                else
                {
                    if (table.ContainsColumn("Value"))
                    {
                        CheckIfStockReportsReportHasCorrectNumbers(table);
                    }
                    else
                    {
                        CheckIfTablesHaveEqualDescriptions(table);
                    }
                }
            }
        }

        private void CheckIfTablesHaveEqualDescriptions(Table expectedTable)
        {
            foreach (var row in expectedTable.Rows)
            {
                var reportRow = GetReportRowByDescription(row["Description"].ToString());
                Assert.IsNotNull(reportRow);
            }
        }

        private void CheckIfStoreSalesReportHasCorrectNumbers(Table expectedTable)
        {
            foreach (var row in expectedTable.Rows)
            {
                var reportRow = GetReportRowByDescription(row["Description"].ToString());

                Assert.IsNotNull(reportRow);

                Assert.AreEqual(Int32.Parse(row["Qty"]), Int32.Parse(reportRow["Qty"].ToString()));
                Assert.AreEqual(Int32.Parse(row["QtyWtd"]), Int32.Parse(reportRow["QtyWtd"].ToString()));
                Assert.AreEqual(decimal.Parse(row["Value"]), decimal.Parse(reportRow["Value"].ToString()));
                Assert.AreEqual(decimal.Parse(row["ValueWtd"]), decimal.Parse(reportRow["ValueWtd"].ToString()));
            }
        }

        private void CheckIfPendingShrinkageReportHasCorrectNumbers(Table expectedTable)
        {
            foreach (var row in expectedTable.Rows)
            {
                var reportRow = GetReportRowByDescription(row["Description"].ToString());

                Assert.IsNotNull(reportRow);
                Assert.AreEqual(Int32.Parse(row["Today Qty"]), Int32.Parse(reportRow["Today Qty"].ToString()));
                Assert.AreEqual(decimal.Parse(row["Today Value"]), decimal.Parse(reportRow["Today Value"].ToString()));
            }
        }

        private void CheckIfStockReportsReportHasCorrectNumbers(Table expectedTable)
        {
            foreach (var row in expectedTable.Rows)
            {
                var reportRow = GetReportRowByDescription(row["Description"].ToString());

                Assert.IsNotNull(reportRow);
                Assert.AreEqual(decimal.Parse(row["Value"]), decimal.Parse(reportRow["Value"].ToString()));
            }
        }

        private void CreateReportNamesToMethodsDictionary()
        {
            reportNamesToMethodsDictionary.Add("Banking", repo.GetBankingReportOnDashboard);
            reportNamesToMethodsDictionary.Add("Banking Reports", repo.GetBankingReportsReportOnDashboard);
            reportNamesToMethodsDictionary.Add("DRL Analysis", repo.GetDrlAnalysisReportOnDashboard);
            reportNamesToMethodsDictionary.Add("Pending Shrinkage", repo.GetPendingShrinkageReportOnDashboard);
            reportNamesToMethodsDictionary.Add("Price Changes", repo.GetPriceChangesReportOnDashboard);
            reportNamesToMethodsDictionary.Add("Shrinkage", repo.GetShrinkageReportOnDashboard);
            reportNamesToMethodsDictionary.Add("Store Sales", repo.GetStoreSalesReportOnDashboard);
            reportNamesToMethodsDictionary.Add("Pending Orders", repo.GetPendingOrdersReportOnDashboard);
            reportNamesToMethodsDictionary.Add("Stock Reports", repo.GetStockReportsReportOnDashboard);
        }

        private string GetTransactionCodeFromFullDefinition(string fulldefinition)
        {
            switch (fulldefinition)
            {
                case "Sale":
                    return "SA";
                case "Refund":
                    return "RF";
                case "Misc. Income":
                    return "M+";
                default:
                    return string.Empty;
            }
        }

        private DataRow GetReportRowByDescription(string rowDescription)
        {
            var reportTableRows = reportTable.Rows.OfType<DataRow>().ToList();
            return reportTableRows.FirstOrDefault(x => x["Description"].ToString() == rowDescription);
        }
    }
}
