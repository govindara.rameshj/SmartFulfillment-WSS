﻿@ManagersDashboardReports
Feature: ManagersDashboardReports

Scenario: "Banking" Report on Manager Dashboard
Given Dashboard was opened for today
When 'Banking' Report was generated
Then The report contains only next rows
| Description                   |
| Last Daily Banking sent to HQ |
| Safe Check                    |

Scenario: "Banking Reports" Report on Manager Dashboard
Given Dashboard was opened for today
When 'Banking Reports' Report was generated
Then The report contains only next rows
| Description             |
| Colleague Discount      |
| Duress Code Used        |
| Gift Token Allocations  |
| Gift Token Redemptions  |
| Miscellaneous Incomes   |
| Miscellaneous Outgoings |

Scenario: "DRL Analysis" Report on Manager Dashboard
Given Dashboard was opened for today
When 'DRL Analysis' Report was generated
Then The report contains only next rows
| Description                       |
| DRL Value                         |
| Code 4 Receipt Adjustments        |
| Outstanding Direct Deliveries     |
| Direct Deliveries Overdue         |
| Orders Consigned but not Received |

Scenario: "Pending Shrinkage" Report on Manager Dashboard
Given Dashboard was opened for today
When 'Pending Shrinkage' Report was generated
Then The report contains only next rows
| Description        |
| Primary Count      |
| Pending Write Offs |

Scenario: "Price Changes" Report on Manager Dashboard
Given Dashboard was opened for today
When 'Price Changes' Report was generated
Then The report contains only next rows
| Description                    |
| Price Labels Action Required   |
| Overdue Labels                 |
| Labels Required                |
| Outstanding Price Label Report |
| Price Change Audit Report      |
| Labels Request                 |

Scenario: "Shrinkage" Report on Manager Dashboard
Given Dashboard was opened for today
When 'Shrinkage' Report was generated
Then The report contains only next rows
| Description           |
| Stock Loss            |
| Known Theft           |
| Write Off Adjustments |
| Markdowns             |
| Price Violations      |
| Incorrect Day Price   |
| Temporary Deal Group  |

Scenario: "Store Sales" Report on Manager Dashboard
Given Dashboard was opened for today
When 'Store Sales' Report was generated
Then The report contains only next rows
| Description                             |
| Core Sales                              |
| Core Refunds                            |
| Net Core Sales                          |
| C&C Sales                               |
| C&C Cancels/Returns                     |
| K&B Sales                               |
| Core Sales + K&B Sales + C&C Sales      |
| Core Sales Last Year                    |
| K&B Sales Last Year                     |
| Percentage Line Scanned (Exc C&C)       |
| Percentage Refunds to Sales             |
| Percentage C&C Cancels/Returns to Sales |

Scenario: "Store Sales" Report on Manager Dashboard Values for a year ago date
Given Dashboard was opened with a year ago date
And Sale Ledger contains next transactions
| Type         | Total Amount | Cashier Number | Till Number | Misc. Reason Code |
| Sale         | 10.52        | 123            | 1           | 0                 |
| Refund       | 30.43        | 123            | 2           | 0                 |
| Misc. Income | 2.34         | 123            | 3           | 20                |
And There was a payment from Vision with total Amount 19.21
When 'Store Sales' Report was generated
Then The report contains next rows
| Description  | Qty | QtyWtd | Value  | ValueWtd |
| Core Sales   | 1   | 1      | 10.52  | 10.52    |
| Core Refunds | 1   | 1      | 30.43  | 30.43    |
| K&B Sales    | 2   | 2      | -16.87 | -16.87   |

Scenario: "Store Sales" Report on Manager Dashboard Values for today
Given Dashboard was opened for today
And Sale Ledger contains next transactions
| Type         | Total Amount | Cashier Number | Till Number | Misc. Reason Code |
| Sale         | 10.52        | 123            | 1           | 0                 |
| Refund       | 30.43        | 123            | 2           | 0                 |
| Misc. Income | 2.34         | 123            | 3           | 20                |
And There was a payment from Vision with total Amount 19.21
And Click and collect order happened
And The order was refunded
When 'Store Sales' Report was generated
Then The report contains next rows
| Description          | Qty | QtyWtd | Value  | ValueWtd |
| C&C Sales            | 1   | 1      | 25.07  | 25.07    |
| C&C Cancels/Returns  | 1   | 1      | -23.07 | -23.07   |
| Core Sales Last Year | 2   | 2      | 40.95  | 40.95    |
| K&B Sales Last Year  | 2   | 2      | -16.87 | -16.87   |

Scenario: Pending Write Offs values of "Pending Shrinkage" on Manager Dashboard
Given Dashboard was opened for today
And Following Stock Adjustment were made today
| SKU Number | Quantity | Price | Write Off's Authorized Cashier |
| 100004     | -1       | 7.99  | 0                              |
| 100005     | -2       | 5.99  | 002                            |
| 100006     | -3       | 4.99  | 0                              |
| 100007     | -1       | 3.99  | 002                            |
| 100008     | -2       | 2.99  | 002                            |
When 'Pending Shrinkage' Report was generated
Then The report contains next rows
| Description        | Today Qty | Today Value |
| Pending Write Offs | 4         | 22.96       |

Scenario: "Stock Reports" Report on Manager Dashboard
Given Dashboard was opened for today
And There were next products
| SKU Number | Price   | On Hand Stock Quantity | Mark-down Stock Quantity |
| 100004     | 7.99    | -1                     | 0                        |
| 100006     | 296.00  | -1                     | 2                        |
| 100012     | 4669.00 | -1                     | 1                        |
| 100013     | 0.01    | 2                      | 3                        |
When 'Stock Reports' Report was generated
Then The report contains next rows
| Description                        |
| GAP WALK                           |
| Number Items Out of Stock          |
| Markdown Stockholding              |
| Deleted Stockholding               |
| Non-Stock Stockholding             |
| Open Returns Stockholding          |
| Percentage Returns to Weekly Sales |
And The report contains next rows
| Description  | Value  |
| Stockholding | 296.05 |

Scenario: "Pending Orders" Report on Manager Dashboard
Given Dashboard was opened for today
When 'Pending Orders' Report was generated
Then The report contains only next rows
| Description             |
| Unconfirmed Orders      |
| Confirmed Orders        |
| Orders in Picking       |
| Confirmed Picked Orders |
| Orders in Despatch      |
| Undelivered             |
| Delivered               |
| Completed               |
| Suspended               |