using System;
using System.Collections.Generic;
using Cts.Oasys.Core.SystemEnvironment;
using Cts.Oasys.Hubs.Data;
using Cts.Oasys.Hubs.Core.Order.Qod;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Common.Qod.CtsTill
{
    public class OrderCreationRoutine
    {
        class OrderLineData
        {
            public string Sku;
            public int QtyTotal;
            public int QtyTaken;
            public decimal Price;
        }

        class OrderHeaderData
        {
            public DateTime OrderDate;
            public bool IsForDelivery;
        }

        private readonly OrderHeaderData header = new OrderHeaderData();
        private readonly List<OrderLineData> lines = new List<OrderLineData>();
        private readonly SaleCreationRoutine saleRoutine;

        public OrderCreationRoutine(IDataLayerFactory dalFactory, ISystemEnvironment systemEnvironment)
        {
            saleRoutine = new SaleCreationRoutine(dalFactory, systemEnvironment);
        }

        public void InitHeader(DateTime orderDate, bool isForDelivery)
        {
            header.OrderDate = orderDate;
            header.IsForDelivery = isForDelivery;

            saleRoutine.InitHeader(1, orderDate, orderDate.AddMinutes(10), false, false);
        }

        public void InitLine(string sku, int qtyTotal, int qtyTaken, decimal price)
        {
            lines.Add(new OrderLineData
            {
                Sku = sku,
                QtyTotal = qtyTotal,
                QtyTaken = qtyTaken,
                Price = price
            });

            saleRoutine.InitLine(sku, qtyTotal, price, false);
        }

        public QodHeader NewOrder(int storeId)
        {
            var saleHeader = saleRoutine.NewSale();

            var orderHeader = CreateOrderObject(storeId, saleHeader);
            SaveOrderObject(orderHeader);

            // Update sale with order number
            saleHeader.OrderNumber = orderHeader.Number;
            saleRoutine.UpdateOrderNumber(saleHeader);

            return orderHeader;
        }

        private QodHeader CreateOrderObject(int store, SaleHeader saleHeader)
        {
            var qodHeader = new QodHeader(true)
            {
                DateOrder = header.OrderDate,
                DateDelivery = header.OrderDate.AddDays(2),
                IsForDelivery = header.IsForDelivery,
                Status = "0", // CANC in DB

                DeliveryAddress1 = "Address1",
                DeliveryAddress2 = "Address2",
                DeliveryAddress3 = "Address3",
                DeliveryAddress4 = "Address4",
                DeliveryPostCode = "QQ11AA",
                PhoneNumber = "1111111111111",
                CustomerName = "Customer",

                MerchandiseValue = saleHeader.TotalSaleAmount,
                DeliveryCharge = 0M,
                QtyOrdered = 0M,
                QtyTaken = 0M,
                QtyRefunded = 0M,
            
                DeliveryStatus = 100,
                SellingStoreId = store,

                TranDate = saleHeader.TransactionDate,
                TranTill = saleHeader.TillNumber,
                TranNumber = saleHeader.TransactionNumber
            };

            qodHeader.CustomerAddress1 = qodHeader.DeliveryAddress1;
            qodHeader.CustomerAddress2 = qodHeader.DeliveryAddress2;
            qodHeader.CustomerAddress3 = qodHeader.DeliveryAddress3;
            qodHeader.CustomerAddress4 = qodHeader.DeliveryAddress4;
            qodHeader.CustomerPostcode = qodHeader.DeliveryPostCode;

            if (qodHeader.IsForDelivery)
            {
                qodHeader.RequiredDeliverySlotID = "1";
                qodHeader.RequiredDeliverySlotDescription = "Slot";
                qodHeader.RequiredDeliverySlotStartTime = new TimeSpan(10, 0, 0);
                qodHeader.RequiredDeliverySlotEndTime = new TimeSpan(19, 0, 0);
            }
            
            int lineNumber = 1;
            foreach (var line in lines)
            {
                qodHeader.Lines.Add(new QodLine()
                {
                    OrderNumber = qodHeader.Number,
                    SellingStoreId = qodHeader.SellingStoreId,
                    SellingStoreOrderId = qodHeader.SellingStoreOrderId,
                    Number = lineNumber.ToString().PadLeft(4, '0'),
                    SkuNumber = line.Sku,
                    QtyOrdered = line.QtyTotal,
                    QtyTaken = line.QtyTaken,
                    QtyRefunded = 0,
                    QtyToDeliver = line.QtyTotal - line.QtyTaken,
                    Price = line.Price,
                    IsDeliveryChargeItem = false,
                    DeliveryStatus = (line.QtyTotal - line.QtyTaken > 0) ? 100 : 999
                });

                qodHeader.QtyOrdered += line.QtyTotal;
                qodHeader.QtyTaken += line.QtyTaken;
            }

            return qodHeader;
        }

        private void SaveOrderObject(QodHeader order)
        {
            using (var conn = new Connection())
            {
                try
                {
                    conn.StartTransaction();
                    order.PersistTree(conn);
                    conn.CommitTransaction();
                }
                catch (Exception)
                {
                    conn.RollbackTransaction();
                    throw;
                }
            }
        }

    }
}
