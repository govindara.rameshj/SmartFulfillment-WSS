using System;
using System.Collections.Generic;
using Cts.Oasys.Core.SystemEnvironment;
using Cts.Oasys.Hubs.Core.Order.Qod;
using Cts.Oasys.Hubs.Core.Stock;
using Cts.Oasys.Hubs.Data;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.AAT.Common.Qod.CtsTill
{
    public class SaleCreationRoutine
    {
        class SaleLineData
        {
            public string Sku;
            public int Qty;
            public decimal Price;
            public bool Reversed;
        }

        class SaleHeaderData
        {
            public decimal TenderType;
            public DateTime SaleDate;
            public DateTime ReceivedDateTime;
            public bool Voided;
            public bool Training;
        }

        private readonly SaleHeaderData header = new SaleHeaderData();
        private readonly List<SaleLineData> lines = new List<SaleLineData>();
        private readonly IDataLayerFactory dalFactory;
        private readonly ISystemEnvironment systemEnvironment;

        public SaleCreationRoutine(IDataLayerFactory dalFactory, ISystemEnvironment systemEnvironment)
        {
            this.dalFactory = dalFactory;
            this.systemEnvironment = systemEnvironment;
        }

        public void InitHeader(decimal tender, DateTime saleDate, DateTime receivedDateTime, bool voided, bool training)
        {
            header.TenderType = tender;
            header.SaleDate = saleDate;
            header.ReceivedDateTime = receivedDateTime;
            header.Voided = voided;
            header.Training = training;
        }

        public void InitLine(string sku, int qty, decimal price, bool reversed)
        {
            lines.Add(new SaleLineData
            {
                Sku = sku,
                Qty = qty,
                Price = price,
                Reversed = reversed
            });
        }

        public SaleHeader NewSale()
        {
            string strTillTranNumber = dalFactory.Create<IDailyTillRepository>().GetNextTillTranNumber();
            var saleHeader = CreateSaleObject(strTillTranNumber.Substring(0, 2), strTillTranNumber.Substring(2, 4));
            SaveSaleObject(saleHeader);
            return saleHeader;
        }

        /// <summary>
        /// We use this method because SaleHeader can not save existing transaction object
        /// 1) Missing parameter @TotalAmount in SaleUpdate SP wrapper
        /// 2) SalePaid can only insert records, no matter they are new or existing
        /// 3) Maybe something else...
        /// </summary>
        /// <param name="saleHeader"></param>
        public void UpdateOrderNumber(SaleHeader saleHeader)
        {
            var key = new DailyTillTranKey
            {
                CreateDate = saleHeader.TransactionDate,
                TillNumber = saleHeader.TillNumber,
                TransactionNumber = saleHeader.TransactionNumber,
            };
            dalFactory.Create<IDailyTillRepository>().SetOrderNumber(key, saleHeader.OrderNumber);
        }

        private SaleHeader CreateSaleObject(string till, string tran)
        {
            var saleHeader = new SaleHeader();
            var total = 0M;
            var totalVat = 0M;

            int lineNumber = 1;
            foreach (var line in lines)
            {
                var checkStock = Stock.GetStock(line.Sku);

                var saleLine = new SaleLine
                {
                    TransactionDate = header.SaleDate,
                    TillNumber = till,
                    TransactionNumber = tran,
                    LineNumber = lineNumber,
                    SKUNumber = line.Sku,
                    Quantity = line.Qty,
                    SystemLookupPrice = checkStock.Price,
                    ItemPrice = line.Price,
                    ItemExcVatPrice = 0,
                    LineReversed = line.Reversed
                };
                ++lineNumber;
                saleLine.ExtendedValue = saleLine.Quantity * saleLine.ItemPrice;

                saleLine.VatSymbol = "a";
                saleLine.RelatedItemsSingle = checkStock.IsItemSingle;
                if (saleLine.SystemLookupPrice - saleLine.ItemPrice != 0)
                    saleLine.PriceOverrideCode = 10;
                saleLine.TaggedItem = checkStock.IsTaggedItem;
                saleLine.PriceOverrideDiff = saleLine.SystemLookupPrice - saleLine.ItemPrice;
                if (saleLine.SystemLookupPrice - saleLine.ItemPrice != 0)
                    saleLine.POMarginErosionCode = "000010";
                saleLine.HierCategory = checkStock.HieCategory;
                saleLine.HierGroup = checkStock.HieGroup;
                saleLine.HierSubGroup = checkStock.HieSubgroup;
                saleLine.HierStyle = checkStock.HieStyle;
                saleLine.SaleTypeAtt = checkStock.SalesType;
                saleLine.VatCode = checkStock.VatCode;
                saleLine.VatValue = saleLine.ExtendedValue * 0.1M;

                saleHeader.SaleLines.Add(saleLine);

                if (!line.Reversed)
                {
                    total += saleLine.ExtendedValue;
                    totalVat += saleLine.VatValue;
                }
                else
                {
                    saleHeader.SaleLines.Add(CreateReverseLine(saleLine, lineNumber));
                    ++lineNumber;
                }
            }

            var saleCustomer = new SaleCustomer
            {
                TransactionDate = header.SaleDate,
                TillNumber = till,
                TransactionNumber = tran,
                CustomerName = String.Empty,
                PostCode = String.Empty,
                StoreNumber = "000",
                SaleDate = null,
                SaleTill = "00",
                SaleTransaction = "0000",
                HouseNameNo = String.Empty,
                Address1 = String.Empty,
                Address2 = String.Empty,
                Address3 = String.Empty,
                PhoneNumber = String.Empty,
                LineNumber = 0,
                PhoneNumberMobile = String.Empty,
                EmailAddress = String.Empty,
                PhoneNumberWork = String.Empty
            };
            saleHeader.SaleCustomers.Add(saleCustomer);

            var salePaid = new SalePaid()
            {
                TransactionDate = header.SaleDate,
                TillNumber = till,
                TransactionNumber = tran,
                SequenceNumber = 1,
                TenderType = header.TenderType,
                TenderAmount = total * -1,
                CCKeyed = false
            };

            saleHeader.SaleTenders.Add(salePaid);

            saleHeader.TransactionDate = header.SaleDate;
            saleHeader.TillNumber = till;
            saleHeader.TransactionNumber = tran;
            saleHeader.CashierID = "800";
            saleHeader.TransactionTime = DateTime.Now.ToString("HHmmss");
            saleHeader.MerchandiseAmount = total;
            saleHeader.TaxAmount = totalVat;
            if (DateTime.Today != header.SaleDate)
                saleHeader.CBBUpdate = DateTime.Today.ToString("dd/MM/yy");
            saleHeader.DiscountCardNo = "";
            saleHeader.Void = header.Voided;
            saleHeader.TrainingMode = header.Training;

            return saleHeader;
        }

        private static SaleLine CreateReverseLine(SaleLine saleLine, int lineNumber)
        {
            var saleLine1 = new SaleLine
            {
                TransactionDate = saleLine.TransactionDate,
                TillNumber = saleLine.TillNumber,
                TransactionNumber = saleLine.TransactionNumber,
                LineNumber = lineNumber,
                SKUNumber = saleLine.SKUNumber,
                Quantity = -saleLine.Quantity,
                SystemLookupPrice = saleLine.SystemLookupPrice,
                ItemPrice = saleLine.ItemPrice,
                ItemExcVatPrice = saleLine.ItemExcVatPrice,
                LineReversed = true,
                ExtendedValue = -saleLine.ExtendedValue,
                VatSymbol = saleLine.VatSymbol,
                RelatedItemsSingle = saleLine.RelatedItemsSingle,
                PriceOverrideCode = saleLine.PriceOverrideCode,
                TaggedItem = saleLine.TaggedItem,
                PriceOverrideDiff = saleLine.PriceOverrideDiff,
                POMarginErosionCode = saleLine.POMarginErosionCode,
                HierCategory = saleLine.HierCategory,
                HierGroup = saleLine.HierGroup,
                HierSubGroup = saleLine.HierSubGroup,
                HierStyle = saleLine.HierStyle,
                SaleTypeAtt = saleLine.SaleTypeAtt,
                VatCode = saleLine.VatCode,
                VatValue = saleLine.VatValue,
            };
            return saleLine1;
        }

        private void SaveSaleObject(SaleHeader sale)
        {
            using (var conn = new Connection())
            {
                try
                {
                    conn.StartTransaction();
                    sale.PersistTree(conn, systemEnvironment);
                    if (!(header.Voided || header.Training))
                    {
                        sale.UpdateCashBalancingTables(conn);
                    }
                    conn.CommitTransaction();
                }
                catch (Exception)
                {
                    conn.RollbackTransaction();
                    throw;
                }
            }
        }
    }
}
