using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using Cts.Oasys.Hubs.Webservices;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public class StatusNotificationRoutine : BaseRoutine<CTSStatusNotificationRequest, CTSStatusNotificationResponse>
    {
        public StatusNotificationRoutine(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
        }

        protected override BaseRequestHandler CreateRequestHandler()
        {
            return new CtsStatusNotificationHandler(SystemEnvironment, DataLayerFactory);
        }

        public void CreateRequestOrderHeader(string omOrderNumber, string orderStatus, string localOrderNumber)
        {
            CurrentRequest = new CTSStatusNotificationRequest
            {
                DateTimeStamp = new CTSStatusNotificationRequestDateTimeStamp { Value = DateTime.Now },
                OMOrderNumber = new CTSStatusNotificationRequestOMOrderNumber { Value = omOrderNumber },
                OrderStatus = new CTSStatusNotificationRequestOrderStatus { Value = orderStatus },
                OrderHeader = new CTSStatusNotificationRequestOrderHeader
                {
                    SellingStoreCode = new CTSStatusNotificationRequestOrderHeaderSellingStoreCode { Value = StoreId.ToString() },
                    SellingStoreOrderNumber = new CTSStatusNotificationRequestOrderHeaderSellingStoreOrderNumber { Value = localOrderNumber },
                },
                FulfilmentSites = new CTSStatusNotificationRequestFulfilmentSite[0]
            };
        }

        public void AddFullFillmentSite(string localOrderNumber, string orderStatus, string[] skus, string storeId = null)
        {
            var sites = new List<CTSStatusNotificationRequestFulfilmentSite>(CurrentRequest.FulfilmentSites);
            var lineNo = 1 + sites.Sum(site => site.OrderLines.Length);

            var newSite = new CTSStatusNotificationRequestFulfilmentSite
            {
                FulfilmentSite = new CTSStatusNotificationRequestFulfilmentSiteFulfilmentSite { Value = storeId ?? StoreId.ToString() },
                FulfilmentSiteOrderNumber = new CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteOrderNumber { Value = localOrderNumber },
                FulfilmentSiteIBTOutNumber = new CTSStatusNotificationRequestFulfilmentSiteFulfilmentSiteIBTOutNumber { Value = "0" }, // it is not really important for C&C, so leave 0 here
                OrderLines = new CTSStatusNotificationRequestFulfilmentSiteOrderLine[0]
            };

            var lines = skus.Select(sku =>
                new CTSStatusNotificationRequestFulfilmentSiteOrderLine
                    {
                        OMOrderLineNo = new CTSStatusNotificationRequestFulfilmentSiteOrderLineOMOrderLineNo { Value = lineNo.ToString() },
                        SellingStoreLineNo = new CTSStatusNotificationRequestFulfilmentSiteOrderLineSellingStoreLineNo { Value = lineNo.ToString() },
                        LineStatus = new CTSStatusNotificationRequestFulfilmentSiteOrderLineLineStatus { Value = orderStatus },
                        ProductCode = new CTSStatusNotificationRequestFulfilmentSiteOrderLineProductCode { Value = sku }
                    }
                );

            newSite.OrderLines = lines.ToArray();
            sites.Add(newSite);
            CurrentRequest.FulfilmentSites = sites.ToArray<CTSStatusNotificationRequestFulfilmentSite>();
        }

        public void PrepareStandardRequest(string omOrderNumber, string orderStatus, string localOrderNumber)
        {
            CreateRequestOrderHeader(omOrderNumber, orderStatus, localOrderNumber);
            AddFullFillmentSite(localOrderNumber, orderStatus, new[] { "100012" });
        }
    }
}
