﻿using System.Threading;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public static class NumbersGenerator
    {
        private static int currentHybrisOrderNumber = 90000000;

        public static string GenerateNextHybrisOrderNumber()
        {
            Interlocked.Increment(ref currentHybrisOrderNumber);
            return currentHybrisOrderNumber.ToString();
        }

        private static int currentVendaOrderNumber = 900000000;

        public static string GenerateNextVendaOrderNumber()
        {
            Interlocked.Increment(ref currentVendaOrderNumber);
            return currentVendaOrderNumber.ToString();
        }

        private static int currentOmOrderNumber = 2000000;

        public static string GenerateNextOmOrderNumber()
        {
            Interlocked.Increment(ref currentOmOrderNumber);
            return currentOmOrderNumber.ToString();
        }
    }
}
