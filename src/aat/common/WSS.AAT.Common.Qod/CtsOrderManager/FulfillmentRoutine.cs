using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using Cts.Oasys.Hubs.Webservices;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public class FulfillmentRoutine : BaseRoutine<CTSFulfilmentRequest, CTSFulfilmentResponse>
    {
        public FulfillmentRoutine(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
        }

        protected override BaseRequestHandler CreateRequestHandler()
        {
            return new CtsFulfilmentRequestHandler(SystemEnvironment, DataLayerFactory);
        }

        public void CreateRequestOrderHeader()
        {
            CurrentRequest = new CTSFulfilmentRequest
            {
                DateTimeStamp = new CTSFulfilmentRequestDateTimeStamp { Value = DateTime.Now },
                TargetFulfilmentSite = new CTSFulfilmentRequestTargetFulfilmentSite { Value = StoreId.ToString() },
                OrderHeader = new CTSFulfilmentRequestOrderHeader
                {
                    Source = "WO",
                    SourceOrderNumber = NumbersGenerator.GenerateNextVendaOrderNumber(),
                    OMOrderNumber = new CTSFulfilmentRequestOrderHeaderOMOrderNumber { Value = NumbersGenerator.GenerateNextOmOrderNumber() },
                    OrderStatus = new CTSFulfilmentRequestOrderHeaderOrderStatus { Value = "200" },
                    RequiredDeliveryDate = new CTSFulfilmentRequestOrderHeaderRequiredDeliveryDate { Value = Tomorrow() },
                    DeliveryCharge = new CTSFulfilmentRequestOrderHeaderDeliveryCharge { Value = 25M },
                    TotalOrderValue = new CTSFulfilmentRequestOrderHeaderTotalOrderValue { Value = 0M },
                    ToBeDelivered = new CTSFulfilmentRequestOrderHeaderToBeDelivered { Value = true },
                    CustomerAccountNo = new CTSFulfilmentRequestOrderHeaderCustomerAccountNo { Value = "A1" },
                    CustomerName = new CTSFulfilmentRequestOrderHeaderCustomerName { Value = "Sir Bob Harris" },
                    CustomerAddressLine1 = new CTSFulfilmentRequestOrderHeaderCustomerAddressLine1 { Value = "\nThe Hobbit Hole Middle Earth\nAvenue" },
                    CustomerAddressLine2 = new CTSFulfilmentRequestOrderHeaderCustomerAddressLine2 { Value = "Off Tolkein Avenue" },
                    CustomerAddressTown = new CTSFulfilmentRequestOrderHeaderCustomerAddressTown { Value = "London" },
                    CustomerAddressLine4 = new CTSFulfilmentRequestOrderHeaderCustomerAddressLine4 { Value = "Central London" },
                    CustomerPostcode = new CTSFulfilmentRequestOrderHeaderCustomerPostcode { Value = "SE1 1AA" },
                    DeliveryAddressLine1 = new CTSFulfilmentRequestOrderHeaderDeliveryAddressLine1 { Value = "\nBuck House The\nMall" },
                    DeliveryAddressLine2 = new CTSFulfilmentRequestOrderHeaderDeliveryAddressLine2 { Value = "Admiralty" },
                    DeliveryAddressTown = new CTSFulfilmentRequestOrderHeaderDeliveryAddressTown { Value = "Northampton" },
                    DeliveryAddressLine4 = new CTSFulfilmentRequestOrderHeaderDeliveryAddressLine4 { Value = "" },
                    DeliveryPostcode = new CTSFulfilmentRequestOrderHeaderDeliveryPostcode { Value = "NN16 6AA" },
                    DeliveryContactName = "Wales",
                    DeliveryContactPhone = "07007 007007",

                    DeliveryInstructions = new CTSFulfilmentRequestOrderHeaderDeliveryInstructions
                    {
                        InstructionLine = new []
                        {
                            new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine
                            {
                                LineNo = new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo { Value = "1" },
                                LineText = new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText { Value = "collect 9951" },
                            },
                            new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine
                            {
                                LineNo = new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo { Value = "2" },
                                LineText = new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText { Value = "Customer requires a Morning delivery" },
                            },
                            new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine
                            {
                                LineNo = new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo { Value = "3" },
                                LineText = new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText { Value = "Customer requires a Saturday delivery" },
                            },
                            new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLine
                            {
                                LineNo = new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineNo { Value = "4" },
                                LineText = new CTSFulfilmentRequestOrderHeaderDeliveryInstructionsInstructionLineLineText { Value = "Lorem ipsum dolor sit amet." },
                            },
                        }
                    },

                    ExtendedLeadTime = false,
                    SaleDate = new CTSFulfilmentRequestOrderHeaderSaleDate { Value = Today() },
                    SellingStoreCode = new CTSFulfilmentRequestOrderHeaderSellingStoreCode { Value = "8120" },
                    SellingStoreOrderNumber = new CTSFulfilmentRequestOrderHeaderSellingStoreOrderNumber { Value = "800009" },
                    SellingStoreTill = "121",
                    SellingStoreTransaction = "123",
                    ContactPhoneHome = new CTSFulfilmentRequestOrderHeaderContactPhoneHome { Value = "0207 555 5555" },
                    ContactPhoneMobile = new CTSFulfilmentRequestOrderHeaderContactPhoneMobile { Value = "07555 555555" },
                    ContactPhoneWork = new CTSFulfilmentRequestOrderHeaderContactPhoneWork { Value = "0207 666 6666" },
                    ContactEmail = new CTSFulfilmentRequestOrderHeaderContactEmail { Value = "empty" },
                },
                OrderLines = new CTSFulfilmentRequestOrderLine[0]
            };
        }

        public void AddOrderLine(string sku, string description, int quantity, decimal price, string storeId = null)
        {
            var lines = new List<CTSFulfilmentRequestOrderLine>(CurrentRequest.OrderLines);
            var lineNo = (lines.Count + 1).ToString();

            lines.Add(new CTSFulfilmentRequestOrderLine
            {
                OMOrderLineNo = new CTSFulfilmentRequestOrderLineOMOrderLineNo { Value = lineNo },
                SellingStoreLineNo = new CTSFulfilmentRequestOrderLineSellingStoreLineNo { Value = lineNo },
                LineStatus = new CTSFulfilmentRequestOrderLineLineStatus { Value = "200" },
                LineValue = new CTSFulfilmentRequestOrderLineLineValue { Value = quantity * price },
                ProductCode = new CTSFulfilmentRequestOrderLineProductCode { Value = sku },
                ProductDescription = new CTSFulfilmentRequestOrderLineProductDescription { Value = description },
                TotalOrderQuantity = new CTSFulfilmentRequestOrderLineTotalOrderQuantity { Value = quantity },
                QuantityTaken = new CTSFulfilmentRequestOrderLineQuantityTaken { Value = 0 },
                DeliveryChargeItem = new CTSFulfilmentRequestOrderLineDeliveryChargeItem { Value = false },
                SellingPrice = new CTSFulfilmentRequestOrderLineSellingPrice { Value = price },
                FulfilmentSite = new CTSFulfilmentRequestOrderLineFulfilmentSite { Value = storeId ?? StoreId.ToString() },
                UOM = new CTSFulfilmentRequestOrderLineUOM { Value = "" },

            });

            CurrentRequest.OrderHeader.TotalOrderValue.Value += quantity * price;
            CurrentRequest.OrderLines = lines.ToArray<CTSFulfilmentRequestOrderLine>();
        }

        public void PrepareRequestForStandardVendaOrder()
        {
            CreateRequestOrderHeader();
            AddOrderLine("100012", "Heavy Duty Staple Gun (FOB)", 1, 2.2M);
        }

        public void SetCustomerPostcode(string postcode)
        {
            CurrentRequest.OrderHeader.CustomerPostcode.Value = postcode;
        }

        public void SetCustomerName(string name)
        {
            CurrentRequest.OrderHeader.CustomerName.Value = name;
        }

        public void SetDeliveryContactName(string name)
        {
            CurrentRequest.OrderHeader.DeliveryContactName = name;
        }

        public void SetDeliveryPostcode(string postcode)
        {
            CurrentRequest.OrderHeader.DeliveryPostcode.Value = postcode;
        }

        public void SetHomePhoneNumber(string homeNumber)
        {
            CurrentRequest.OrderHeader.ContactPhoneHome.Value = homeNumber;
        }

        public void SeMobilePhoneNumber(string mobileNumber)
        {
            CurrentRequest.OrderHeader.ContactPhoneMobile.Value = mobileNumber;
        }

        public void SetWorkPhoneNumber(string workNumber)
        {
            CurrentRequest.OrderHeader.ContactPhoneWork.Value = workNumber;
        }

        public void SetDeliveryContactPhoneNumber(string deliveryContactNumber)
        {
            CurrentRequest.OrderHeader.DeliveryContactPhone = deliveryContactNumber;
        }
    }
}
