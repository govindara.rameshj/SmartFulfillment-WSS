using System;
using System.Linq;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using Cts.Oasys.Hubs.Webservices;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public class EnableRefundCreationRoutine: BaseRoutine<CTSEnableRefundCreateRequest, CTSEnableRefundCreateResponse>
    {
        public EnableRefundCreationRoutine(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
        }

        public void PrepareRequestForStandardClickAndCollectOrder(string sourceOrderNumber)
        {
            CurrentRequest = new CTSEnableRefundCreateRequest
            {
                DateTimeStamp = new CTSEnableRefundCreateRequestDateTimeStamp { Value = DateTime.Now },
                RefundHeader = new CTSEnableRefundCreateRequestRefundHeader
                {
                    Source = "HY",
                    SourceOrderNumber = sourceOrderNumber,
                    RefundDate = new CTSEnableRefundCreateRequestRefundHeaderRefundDate { Value = Today() },
                    RefundSeq = "1001001",
                    LoyaltyCardNumber = "LoyaltyCardNumberValue",
                    DeliveryChargeRefundValue = 0,
                    RefundTotal = 23.07M,
                },
                RefundLines = new []
                {
                    new CTSEnableRefundCreateRequestRefundLine
                    {
                        SourceLineNo = "1001",
                        ProductCode = "100012",
                        QuantityCancelled = "1",
                        RefundLineValue = 4.05M,
                    },
                    new CTSEnableRefundCreateRequestRefundLine
                    {
                        SourceLineNo = "1002",
                        ProductCode = "100013",
                        QuantityCancelled = "2",
                        RefundLineValue = 19.02M,
                    },
                }
            };
        }

        public void PreparePartialRequestForStandardClickAndCollectOrder(string sourceOrderNumber)
        {
            PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);

            var refundLinetList = CurrentRequest.RefundLines.ToList();
            refundLinetList.Remove(refundLinetList.First(a => a.SourceLineNo == "1001"));
            var secondRefundLine = refundLinetList.First(a => a.SourceLineNo == "1002");
            secondRefundLine.QuantityCancelled = "1";
            secondRefundLine.RefundLineValue = 9.51M;
            CurrentRequest.RefundLines = refundLinetList.ToArray();
            CurrentRequest.RefundHeader.RefundTotal = 9.51M;
        }

        public void PrepareCompleteRequestWithoutPartialForStandardClickAndCollectOrder(string sourceOrderNumber)
        {
            PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);
            var refundLinetList = CurrentRequest.RefundLines.ToList();
            var secondRefundLine = refundLinetList.First(a => a.SourceLineNo == "1002");
            secondRefundLine.QuantityCancelled = "1";
            secondRefundLine.RefundLineValue = 9.51M;
            CurrentRequest.RefundLines = refundLinetList.ToArray();
            CurrentRequest.RefundHeader.RefundTotal = 13.56M;
        }

        public void PrepareRequestWithNoLines(string sourceOrderNumber)
        {
            PrepareRequestForStandardClickAndCollectOrder(sourceOrderNumber);
            CurrentRequest.RefundLines = new CTSEnableRefundCreateRequestRefundLine[0];
        }

        public void SetRefundDate(DateTime dt)
        {
            CurrentRequest.RefundHeader.RefundDate.Value = dt;
        }

        public void SetRefundTotal(decimal total)
        {
            CurrentRequest.RefundHeader.RefundTotal = total;
        }

        public void SetRefundProductCode(string productCode)
        {
            CurrentRequest.RefundLines[0].ProductCode = productCode;
        }

        public void SetSource(string source)
        {
            CurrentRequest.RefundHeader.Source = source;
        }

        public void SetFirstLineNumber(string number)
        {
            if (CurrentRequest.RefundLines.Length == 0)
                return;

            CurrentRequest.RefundLines[0].SourceLineNo = number;
        }

        public bool IsSourceLineNoHasValidationError()
        {
            return CurrentResponse.RefundLines.Any(line => !string.IsNullOrEmpty(line.SourceLineNo.ValidationStatus));
        }

        public bool IsSourceFieldHasValidationError()
        {
            return !string.IsNullOrEmpty(CurrentResponse.RefundHeader.Source.ValidationStatus);
        }

        protected override BaseRequestHandler CreateRequestHandler()
        {
            return new CtsEnableRefundCreateHandler(SystemEnvironment, DataLayerFactory);
        }
    }
}
