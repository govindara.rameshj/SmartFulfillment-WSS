using System;
using Cts.Oasys.Hubs.Core.Order.Qod;
using Cts.Oasys.Hubs.WinForm.Order.SaleOrders;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public class UpdateOrderRoutine
    {
        private readonly IDataLayerFactory dlFactory;
        private QodHeader oldQodHeader;
        private QodHeader qodHeader;

        public UpdateOrderRoutine(IDataLayerFactory dlFactory)
        {
            this.dlFactory = dlFactory;
        }

        public void InitializeOrderForm(QodHeader header)
        {
            oldQodHeader = header;
            qodHeader = header.Clone();
        }

        public void SetDeliveryDate(DateTime date)
        {
            qodHeader.DateDelivery = date;
        }

        public void SetDeliverySlot(DeliverySlot slot)
        {
            qodHeader.RequiredDeliverySlotID = slot.Id;
            qodHeader.RequiredDeliverySlotDescription = slot.Description;
            qodHeader.RequiredDeliverySlotStartTime = slot.StartTime;
            qodHeader.RequiredDeliverySlotEndTime = slot.EndTime;
        }

        public void AcceptOrder()
        {
            MainForm.HandleChanges(oldQodHeader, qodHeader, dlFactory);
            qodHeader.SelfPersistTree(); 
        }

        public void SetPhoneHomeNumber(string number)
        {
            qodHeader.PhoneNumberHome = number;
        }

        public void SetAddress(string address)
        {
            qodHeader.DeliveryAddress1 = address;
        }
    }
}
