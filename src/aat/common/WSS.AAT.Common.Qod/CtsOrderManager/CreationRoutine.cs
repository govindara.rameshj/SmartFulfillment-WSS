using System;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using Cts.Oasys.Hubs.Webservices;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public class CreationRoutine: BaseRoutine<CTSQODCreateRequest, CTSQODCreateResponse>
    {
        public CreationRoutine(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
        }

        public void PrepareRequestForStandardClickAndCollectOrder()
        {
            CreateRequestOrderHeader();
            CreateOrderLines(2);
            SetupOrderLine(0, "100012", 1, 5.05M);
            SetupOrderLine(1, "100013", 2, 10.01M);
        }

        public void CreateRequestOrderHeader()
        {
            CurrentRequest = new CTSQODCreateRequest
            {
                DateTimeStamp = new CTSQODCreateRequestDateTimeStamp { Value = DateTime.Now },
                OrderHeader = new CTSQODCreateRequestOrderHeader
                {
                    Source = new CTSQODCreateRequestOrderHeaderSource { Value = "HY" },
                    SourceOrderNumber = new CTSQODCreateRequestOrderHeaderSourceOrderNumber { Value = NumbersGenerator.GenerateNextHybrisOrderNumber() },
                    RequiredDeliveryDate = new CTSQODCreateRequestOrderHeaderRequiredDeliveryDate { Value = Tomorrow() },
                    DeliveryCharge = new CTSQODCreateRequestOrderHeaderDeliveryCharge { Value = 0 },
                    TotalOrderValue = new CTSQODCreateRequestOrderHeaderTotalOrderValue { Value = 24.05M },
                    SaleDate = new CTSQODCreateRequestOrderHeaderSaleDate { Value = Today() },
                    CustomerAccountNo = new CTSQODCreateRequestOrderHeaderCustomerAccountNo { Value = "A1" },
                    CustomerName = new CTSQODCreateRequestOrderHeaderCustomerName { Value = "Sir Bob Harris" },
                    CustomerAddressLine1 = new CTSQODCreateRequestOrderHeaderCustomerAddressLine1 { Value = "The Hobbit Hole Middle Earth\nAvenue" },
                    CustomerAddressLine2 = new CTSQODCreateRequestOrderHeaderCustomerAddressLine2 { Value = "Off Tolkein Avenue" },
                    CustomerAddressTown = new CTSQODCreateRequestOrderHeaderCustomerAddressTown { Value = "London" },

                    CustomerAddressLine4 = new CTSQODCreateRequestOrderHeaderCustomerAddressLine4 { Value = "Central London" },
                    CustomerPostcode = new CTSQODCreateRequestOrderHeaderCustomerPostcode { Value = "SE1 1AA" },
                    DeliveryAddressLine1 = new CTSQODCreateRequestOrderHeaderDeliveryAddressLine1 { Value = "Buck House The\nMall" },
                    DeliveryAddressLine2 = new CTSQODCreateRequestOrderHeaderDeliveryAddressLine2 { Value = "Admiralty" },
                    DeliveryAddressTown = new CTSQODCreateRequestOrderHeaderDeliveryAddressTown { Value = "Northampton" },
                    DeliveryAddressLine4 = new CTSQODCreateRequestOrderHeaderDeliveryAddressLine4 { Value = "" },
                    DeliveryPostcode = new CTSQODCreateRequestOrderHeaderDeliveryPostcode { Value = "NN16 6AA" },
                    ContactPhoneHome = new CTSQODCreateRequestOrderHeaderContactPhoneHome { Value = "0207 555 5555" },
                    ContactPhoneMobile = new CTSQODCreateRequestOrderHeaderContactPhoneMobile { Value = "07555 555555" },
                    ContactPhoneWork = new CTSQODCreateRequestOrderHeaderContactPhoneWork { Value = "0207 666 6666" },
                    ContactEmail = new CTSQODCreateRequestOrderHeaderContactEmail { Value = "empty" },
                    DeliveryInstructions = new CTSQODCreateRequestOrderHeaderDeliveryInstructions(),

                    ToBeDelivered = new CTSQODCreateRequestOrderHeaderToBeDelivered { Value = false },
                    CreateDeliveryChargeItem = new CTSQODCreateRequestOrderHeaderCreateDeliveryChargeItem { Value = false },
                    LoyaltyCardNumber = new CTSQODCreateRequestOrderHeaderLoyaltyCardNumber { Value = "collect 9951" },
                    ExtendedLeadTime = new CTSQODCreateRequestOrderHeaderExtendedLeadTime { Value = false },
                    DeliveryContactName = new CTSQODCreateRequestOrderHeaderDeliveryContactName { Value = "Wales" },
                    DeliveryContactPhone = new CTSQODCreateRequestOrderHeaderDeliveryContactPhone { Value = "07007 007007" },
                    RecordSaleOnly = new CTSQODCreateRequestOrderHeaderRecordSaleOnly { Value = false },
                }
            };
        }

        public void CreateOrderLines(int count)
        {
            CurrentRequest.OrderLines = new CTSQODCreateRequestOrderLine[count];
            CurrentRequest.OrderHeader.TotalOrderValue.Value = 0M;
        }

        public void SetupOrderLine(int lineNumber, string sku, int quantity, decimal price)
        {
            CurrentRequest.OrderLines[lineNumber] =
                new CTSQODCreateRequestOrderLine
                    {
                        SourceOrderLineNo = new CTSQODCreateRequestOrderLineSourceOrderLineNo {Value = (1000 + 1 + lineNumber).ToString()},
                        ProductCode = new CTSQODCreateRequestOrderLineProductCode {Value = sku},
                        ProductDescription = new CTSQODCreateRequestOrderLineProductDescription {Value = "Sku test1"},
                        TotalOrderQuantity = new CTSQODCreateRequestOrderLineTotalOrderQuantity {Value = quantity},
                        LineValue = new CTSQODCreateRequestOrderLineLineValue {Value = quantity * price},
                        DeliveryChargeItem = new CTSQODCreateRequestOrderLineDeliveryChargeItem {Value = false},
                        SellingPrice = new CTSQODCreateRequestOrderLineSellingPrice {Value = price},
                        RecordSaleOnly = new CTSQODCreateRequestOrderLineRecordSaleOnly {Value = false},
                    };

            CurrentRequest.OrderHeader.TotalOrderValue.Value += quantity * price;
        }

        public void SetSaleDate(DateTime date)
        {
            CurrentRequest.OrderHeader.SaleDate.Value = date;
        }

        public void SetTotalPrice(decimal price)
        {
            CurrentRequest.OrderHeader.TotalOrderValue.Value = price;
        }

        public void ForLineNumberSetPrice(int lineNumber, decimal price)
        {
            CurrentRequest.OrderLines[lineNumber - 1].SellingPrice.Value = price;
            CurrentRequest.OrderLines[lineNumber - 1].LineValue.Value = price * CurrentRequest.OrderLines[lineNumber - 1].TotalOrderQuantity.Value;
        }

        public void SetSource(string source)
        {
            CurrentRequest.OrderHeader.Source.Value = source;
        }

        public void SetSourceOrderNumber(string sourceOrderNumber)
        {
            CurrentRequest.OrderHeader.SourceOrderNumber.Value = sourceOrderNumber;
        }

        protected override BaseRequestHandler CreateRequestHandler()
        {
            return new CtsQodCreateHandler(SystemEnvironment, DataLayerFactory);
        }
    }
}
