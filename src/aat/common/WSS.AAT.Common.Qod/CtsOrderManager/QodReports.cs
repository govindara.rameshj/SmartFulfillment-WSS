using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Cts.Oasys.Hubs.Core.Order.Qod.State;
using Cts.Oasys.Hubs.WinForm.Order.SaleOrders;
using WSS.AAT.Common.Utility.Utility;

namespace QodFixtures
{
    public class QodReports
    {
        readonly string connectionString;
        private readonly DataTable lastCallResult;       

        public QodReports()
        {
            lastCallResult = new DataTable();
        }

        public QodReports(string connectionString = null)
            : this()
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Provides ability to launch debugger from within Wiki
        /// </summary>
        public void StartDebug()
        {
            Debugger.Launch();
        }

        public bool CheckOrderExistsInReportResultsSearchCriteriaOpenOrders(string orderNumber)
        {
            var customerSearch = new CustomerSearch();
            customerSearch.SearchCriteriaParameters(0, string.Empty, string.Empty, string.Empty, string.Empty);
            var qods = customerSearch.GetOrders((int)Delivery.DeliveryRequest, (int)Delivery.DeliveredStatusFailedData);
            return qods.Any(qod => qod.Number == orderNumber);
        }

        public bool CheckOrderExistsInReportResultsSearchCriteriaAllOrders(string orderNumber, DateTime dateStart, DateTime dateEnd)
        {
            var customerSearch = new CustomerSearch();
            customerSearch.SearchCriteriaParameters(0, string.Empty, string.Empty, string.Empty, string.Empty);
            var qods = customerSearch.GetOrders(dateStart, dateEnd);
            return qods.Any(qod => qod.Number == orderNumber);
        }

        public bool CheckOrderExistsInCustomerOrderEnquiry(string orderId)
        {
            foreach (DataRow row in lastCallResult.Rows)
            {
                string numb = (string)row["NUMB"];
                if (numb.Equals(orderId))
                    return true;
            }
            return false;
        }

        public bool CheckRefundOrderExistsInCustomerOrderEnquiry(string orderId)
        {
            foreach (DataRow row in lastCallResult.Rows)
            {
                string numb = (string)row["NUMB"];
                var qtyr = row["QTYR"];
                if (numb.Equals(orderId) && !qtyr.Equals(0))
                    return true;
            }
            return false;
        }

        public static SqlCommand GetCustomerOrderEnquiryReport(int storeId, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = @"
                    SELECT a.NUMB, a.DATE1 As OrderDate, a.DELD, a.DELI, b.SKUN, b.QTYO, b.QTYT, b.QTYR, c.DESCR, c.INON, c.IDEL, c.ONHA, c.IOBS, c.ICAT, c.IRIS, c.IMDN, c.NOOR, c.FODT, d.SOLD
                    FROM CORHDR a
                        INNER JOIN CORHDR4 c4 ON a.NUMB = c4.NUMB
                        INNER JOIN CORLIN b ON a.NUMB = b.NUMB
                        LEFT JOIN STKMAS c ON b.SKUN = c.SKUN
                        LEFT JOIN QUOHDR d ON a.NUMB = d.NUMB
                    WHERE a.CANC <> '1' 
                        AND c4.DELIVERYSTATUS < '900' 
                        AND (ISNULL(a.STIL,0) < '13' OR (ISNULL(a.STIL,0) >= '90' AND ISNULL(a.STIL,0) <= '99'))
                        AND b.DELIVERYSOURCE = '" + storeId.ToString() + "'";
            return cmd;
        }

        public void GetCustomerOrderEnquiryReport()
        {
            var storeId = new Cts.Oasys.Hubs.Core.System.Store.Store(0).Id4; //Passing 0 gets the local store!

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = GetCustomerOrderEnquiryReport(storeId, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(lastCallResult);
                conn.Close();
                da.Dispose();
            }
        }

        public string GetRefundQuantity(string orderId, string sku)
        {
            foreach (DataRow row in lastCallResult.Rows)
            {
                string numb = (string)row["NUMB"];
                string skun = (string)row["SKUN"];
                if (numb.Equals(orderId) && skun.Equals(sku))
                    return row["QTYR"].ToString();
            }

            return null;
        }

        public string GetOrderQuantity(string orderId, string sku)
        {
            foreach (DataRow row in lastCallResult.Rows)
            {
                string numb = (string)row["NUMB"];
                string skun = (string)row["SKUN"];
                if (numb.Equals(orderId) && skun.Equals(sku))
                    return row["QTYO"].ToString();
            }

            return null;
        }       
    }
}
