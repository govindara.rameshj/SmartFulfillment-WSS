﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cts.Oasys.Hubs.WinForm.Order.SaleOrders;
using System.Windows.Forms;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public class SaleOrderMaintain
    {
        MainForm form;
        public void LoadMainForm()
        {
            form = new MainForm(0, 20, 9, "", null);
            //form.uxRetrieveButton.Click += new EventHandler(uxRetrieveButton_Click);
        }

        public string StartDateValue()
        {
            if (form.uxStartDate.EditValue != null)
            {
                return ((DateTime) form.uxStartDate.EditValue).ToShortDateString();
            }
            return "empty";
        }

        public string EndDateValue()
        {
            if (form.uxEndDate.EditValue != null)
            {
                return ((DateTime) form.uxEndDate.EditValue).ToShortDateString();
            }
            return "empty";
        }

        public void SetStartDate(string date)
        {
            if (date == "empty")
            {
                form.uxStartDate.EditValue = null;
                return;
            }
            else
            {
                form.uxStartDate.EditValue = DateTime.Parse(date);
            }
        }

        public void SetEndDate(string date)
        {
            if (date == "empty")
            {
                form.uxEndDate.EditValue = null;
                return;
            }
            else
            {
                form.uxEndDate.EditValue = DateTime.Parse(date);
            }
        }

        public void SwitchToAllOrdersTab()
        {
            form.uxOrderTabControl.SelectedTabPage = form.uxAllOrderTab;
        }

        public void SetNameOnAllOrdersTab(string name)
        {
            form.txtNameAllOrders.Text = name;
        }

        public void RetrieveOrders()
        {
            form.uxRetrieveButton.PerformClick();
        }

        //bool retrieveButtonClicked;
        public string RetrieveOrdersUsingEnterKey()
        {
            //retrieveButtonClicked = false;

            form.temp_uxRetrieveClicked = false;

            form.Show();
            form.txtNameAllOrders.Focus();
            System.Windows.Forms.SendKeys.SendWait("{ENTER}");
            form.Hide();

            //return retrieveButtonClicked ? "done" : "no effect";
            return form.temp_uxRetrieveClicked ? "done" : "no effect";
        }

        /*void uxRetrieveButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Our click!");
            retrieveButtonClicked = true;
        }*/

        public string RetrieveButtonState()
        {
            if (form.uxRetrieveButton.Enabled)
                return "enabled";
            else
                return "disabled";
        }

        public bool IsSearchAllowed()
        {
            return form.IsSearchAllowed();
        }
    }
}
