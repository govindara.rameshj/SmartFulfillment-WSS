using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using Cts.Oasys.Hubs.Webservices;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public class UpdateRefundRoutine : BaseRoutine<CTSUpdateRefundRequest, CTSUpdateRefundResponse>
    {
        public UpdateRefundRoutine(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
        }

        protected override BaseRequestHandler CreateRequestHandler()
        {
            return new CtsUpdateRefundHandler(SystemEnvironment, DataLayerFactory);
        }

        public void CreateRequestOrderHeader(string omOrderNumber)
        {
            CurrentRequest = new CTSUpdateRefundRequest
            {
                DateTimeStamp = DateTime.Now,
                OMOrderNumber = omOrderNumber,
                OrderRefunds = new CTSUpdateRefundRequestOrderRefund[0]
            };
        }

        public void AddOrderRefund(int store, string lineNo, string sku, int qty, decimal value)
        {
            var refunds = new List<CTSUpdateRefundRequestOrderRefund>(CurrentRequest.OrderRefunds);

            var refund = new CTSUpdateRefundRequestOrderRefund
            {
                RefundDate = DateTime.Today.AddDays(-1),
                RefundStoreCode = "8120",
                RefundTransaction = "1258",
                RefundTill = "05"
            };

            if (store > 0)
            {
                refund.FulfilmentSites = new CTSUpdateRefundRequestOrderRefundFulfilmentSite[]
                {
                    new CTSUpdateRefundRequestOrderRefundFulfilmentSite
                    {
                        FulfilmentSiteCode = store.ToString(),
                        SellingStoreIBTOutNumber = "0"
                    }
                };
            }

            refund.RefundLines = new CTSUpdateRefundRequestOrderRefundRefundLine[]
            {
                new CTSUpdateRefundRequestOrderRefundRefundLine
                {
                    SellingStoreLineNo = lineNo,
                    OMOrderLineNo = lineNo,
                    ProductCode = sku,
                    FulfilmentSiteCode = store.ToString(),
                    QuantityReturned = "0",
                    QuantityCancelled = qty.ToString(),
                    RefundLineValue = value,
                    RefundLineStatus = "150"
                }
            };

            refunds.Add(refund);
            CurrentRequest.OrderRefunds = refunds.ToArray<CTSUpdateRefundRequestOrderRefund>();
        }

    }
}
