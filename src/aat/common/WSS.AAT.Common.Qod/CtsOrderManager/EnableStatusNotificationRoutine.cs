using System;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using Cts.Oasys.Hubs.Webservices;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public class EnableStatusNotificationRoutine : BaseRoutine<CTSEnableStatusNotificationRequest, CTSEnableStatusNotificationResponse>
    {
        public EnableStatusNotificationRoutine(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
            : base(dataLayerFactory, systemEnvironment)
        {
        }

        protected override BaseRequestHandler CreateRequestHandler()
        {
            return new CtsEnableStatusNotificationHandler(SystemEnvironment, DataLayerFactory);
        }

        public void CreateStandardRequest(string sourceOrderNumber, string orderStatus)
        {
            CurrentRequest = new CTSEnableStatusNotificationRequest
            {
                DateTimeStamp = new CTSEnableStatusNotificationRequestDateTimeStamp { Value = DateTime.Now },
                Source = new CTSEnableStatusNotificationRequestSource { Value = "HY" },
                SourceOrderNumber = new CTSEnableStatusNotificationRequestSourceOrderNumber { Value = sourceOrderNumber },
                OrderStatus = new CTSEnableStatusNotificationRequestOrderStatus { Value = orderStatus },
            };
        }

        public void SetSource(string source)
        {
            CurrentRequest.Source.Value = source;
        }

    }
}
