using System;
using System.Diagnostics;
using Cts.Oasys.Core;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Hubs.Core.Order.WebService;
using Cts.Oasys.Hubs.Webservices;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;

namespace WSS.AAT.Common.Qod.CtsOrderManager
{
    public interface IOrderRoutine
    {
        BaseResponse GetResponse();
        void ProcessPreparedRequest();
    }

    public abstract class BaseRoutine<TRequest, TResponse> : IOrderRoutine
        where TRequest : class
        where TResponse : BaseResponse
    {
        private DateTime? receivedDate;

        protected TestSystemEnvironment SystemEnvironment { get; private set; }
        protected IDataLayerFactory DataLayerFactory { get; private set; }

        public TRequest CurrentRequest { get; protected set; }
        public TResponse CurrentResponse { get; protected set; }

        public BaseResponse GetResponse()
        {
            return CurrentResponse;
        }

        public int StoreId { get; protected set; }

        protected BaseRoutine(IDataLayerFactory dataLayerFactory, TestSystemEnvironment systemEnvironment)
        {
            DataLayerFactory = dataLayerFactory;
            SystemEnvironment = systemEnvironment;
            StoreId = dataLayerFactory.Create<IDictionariesRepository>().GetAbsoluteStoreId();
        }
        
        /// <summary>
        /// Provides ability to launch debugger from within Wiki
        /// </summary>
        public void StartDebug()
        {
            Debugger.Launch();
        }

        public void ProcessPreparedRequest()
        {
            var requestXml = XmlSerializationHelper.Serialize(CurrentRequest);

            var resultXml = GetResponse(requestXml);

            try
            {
                CurrentResponse = XmlSerializationHelper.Deserialize<TResponse>(resultXml);
            }
            catch
            {
                CurrentResponse = null;
            }
        }

        protected string GetResponse(string requestXml)
        {
            var handler = CreateAndInitializeRequestHandler();
            return handler.Handle(requestXml);
        }

        protected DateTime Today()
        {
            return DateTime.Now;
        }

        protected DateTime Tomorrow()
        {
            return DateTime.Now.AddDays(1);
        }

        public void SetReceivedDateAndTime(DateTime datetime)
        {
            receivedDate = datetime;
        }

        private BaseRequestHandler CreateAndInitializeRequestHandler()
        {
            Func<DateTime> getReceivedDate;
            if ((receivedDate != null))
            {
                getReceivedDate = () => receivedDate.Value;
            }
            else
            {
                getReceivedDate = () => DateTime.Now;
            }

            SystemEnvironment.GetDateTimeNowMethod = getReceivedDate;

            return CreateRequestHandler();
        }

        protected abstract BaseRequestHandler CreateRequestHandler();
    }
}
