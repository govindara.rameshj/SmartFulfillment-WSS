using System;
using System.Diagnostics;
using System.Linq;
using Cts.Oasys.Hubs.Core.Order.Qod;
using WSS.AAT.Common.Utility.Utility;

namespace WSS.AAT.Common.Qod
{
    public class SingleQodStorage
    {
        private readonly QodHeader qodHeader;

        public QodHeader QodHeader { get { return qodHeader; } }

        public SingleQodStorage(string orderNumber)
        {
            var qodHeaders = new QodHeaderCollection();
            qodHeaders.LoadExistingOrder(orderNumber);
            qodHeader = qodHeaders.First();

            qodSale = new Lazy<SaleHeader>(LoadQodSale);

            refundSale = new Lazy<SaleHeader>(LoadRefundSale);
        }

        /// <summary>
        /// Provides ability to launch debugger from within Wiki
        /// </summary>
        public void StartDebug()
        {
            Debugger.Launch();
        }

        public int GetOmOrderNumber()
        {
            return qodHeader.OmOrderNumber;
        }

        public string GetSourceOrderNumber()
        {
            return qodHeader.SourceOrderNumber;
        }

        public int GetDeliveryStatus()
        {
            return qodHeader.DeliveryStatus;
        }

        public string GetDeliveryDate()
        {
            return DateTimeParser.EncodeDateTime(qodHeader.DateDelivery.Value, DateTime.Now);
        }

        public int GetLineDeliveryStatus(string lineNumber)
        { 
            return qodHeader.Lines.First(l => l.Number == lineNumber).DeliveryStatus;
        }

        public string GetLineDeliverySource(string lineNumber)
        {
            return qodHeader.Lines.First(l => l.Number == lineNumber).DeliverySource;
        }

        public string GetLineIbtOut(string lineNumber)
        {
            return qodHeader.Lines.First(l => l.Number == lineNumber).DeliverySourceIbtOut;
        }

        public int GetStoreId()
        {
            var store = new Cts.Oasys.Hubs.Core.System.Store.Store(0); //Passing 0 gets the local store!
            return store.Id4;
        }
        
        public bool GetToBeDelivered()
        {
            return qodHeader.IsForDelivery;
        }

        public bool QodTillTranNumbersAreNotNull()
        {
            return qodHeader.TranTill != null && qodHeader.TranNumber != null;
        }

        public string GetSaleDate()
        {
            return DateTimeParser.EncodeDateTime(qodHeader.DateOrder, DateTime.Now);
        }

        public string GetOrderSaleTill()
        {
            return qodHeader.TranTill;
        }

        public string GetOrderSaleTran()
        {
            return qodHeader.TranNumber;
        }

        public string GetOrderRefundTill()
        {
            return qodHeader.RefundTill;
        }

        public string GetOrderRefundTran()
        {
            return qodHeader.RefundTranNumber;
        }

        #region Sale

        private readonly Lazy<SaleHeader> qodSale;

        private SaleHeader LoadQodSale()
        {
            var saleHeaders = new SaleHeaderCollection();
            saleHeaders.LoadExistingSale(qodHeader.DateOrder, qodHeader.TranTill, qodHeader.TranNumber);

            var saleHeader = saleHeaders.First();
            saleHeader.LoadSalePaid();

            return saleHeader;
        }

        public decimal? GetOrderTenderType()
        {
            return qodSale.Value.SaleTenders.Count > 0 ? qodSale.Value.SaleTenders.First().TenderType : 0;
        }

        public string GetSaleCashier()
        {
            return qodSale.Value.CashierID;
        }

        public string GetSaleTill()
        {
            return qodSale.Value.TillNumber;
        }

        public string GetSaleTran()
        {
            return qodSale.Value.TransactionNumber;
        }

        public string GetSaleLineTill(int lineNumber)
        {
            return qodSale.Value.SaleLines.First(l => l.LineNumber == lineNumber).TillNumber;
        }

        public string GetSaleLineTran(int lineNumber)
        {
            return qodSale.Value.SaleLines.First(l => l.LineNumber == lineNumber).TransactionNumber;
        }

        public string GetSalePaidTill()
        {
            return qodSale.Value.SaleTenders.First().TillNumber;
        }

        public string GetSalePaidTran()
        {
            return qodSale.Value.SaleTenders.First().TransactionNumber;
        }

        #endregion

        #region Refund

        public string GetRefundFull()
        {
            return qodHeader.Status;
        }

        public int GetRefundStatus()
        {
            return qodHeader.RefundStatus;
        }

        public string GetRefundDate()
        {
            return qodHeader.RefundTranDate.HasValue ?
                DateTimeParser.EncodeDateTime(qodHeader.RefundTranDate.Value, DateTime.Now)
                : "N/A";
        }

        public string GetStatus()
        {
            return qodHeader.Status;
        }

        public decimal GetQuantityRefunded()
        {
            return Math.Abs(qodHeader.QtyRefunded);
        }

        public int GetLineQuantityRefunded(string lineNumber)
        {
            return Math.Abs(qodHeader.Lines.First(l => l.Number == lineNumber).QtyRefunded);
        }

        public string GetRefundItemRefundDate(string lineNumber)
        {
            var dateValue = qodHeader.Refunds.First(l => l.Number == lineNumber).RefundDate;
            return DateTimeParser.EncodeDateTime(dateValue, DateTime.Now);
        }

        public int GetRefundItemQuantityCancelled(string lineNumber)
        {
            return qodHeader.Refunds.First(l => l.Number == lineNumber).QtyCancelled;
        }

        public string GetRefundItemTill(string lineNumber)
        {
            return qodHeader.Refunds.First(l => l.Number == lineNumber).RefundTill;
        }

        public string GetRefundItemTran(string lineNumber)
        {
            return qodHeader.Refunds.First(l => l.Number == lineNumber).RefundTransaction;
        }

        public string GetRefundItemIbtIn(string lineNumber)
        {
            return qodHeader.Refunds.First(l => l.Number == lineNumber).FulfillingStoreIbtIn;
        }

        #endregion

        #region Refund Sale

        private readonly Lazy<SaleHeader> refundSale;

        private SaleHeader LoadRefundSale()
        {
            var saleHeaders = new SaleHeaderCollection();
            saleHeaders.LoadExistingSale(qodHeader.RefundTranDate.Value, qodHeader.RefundTill, qodHeader.RefundTranNumber);

            var saleHeader = saleHeaders.First();
            saleHeader.LoadSalePaid();

            return saleHeader;
        }

        public string GetRefundSaleDate()
        {
            return DateTimeParser.EncodeDateTime(refundSale.Value.TransactionDate, DateTime.Now);
        }

        public decimal GetRefundSaleTotalAmount()
        {
            return Math.Abs(refundSale.Value.TotalSaleAmount);
        }

        public decimal GetRefundSaleLinePrice(int lineNumber)
        {
            return refundSale.Value.SaleLines.First(l => l.LineNumber == lineNumber).ItemPrice;
        }

        public decimal GetRefundSaleTenderAmount()
        {
            return refundSale.Value.SaleTenders.First().TenderAmount;
        }

        public decimal? GetRefundSaleTenderType()
        {
            return refundSale.Value.SaleTenders.First().TenderType;
        }

        public string GetRefundSaleCashier()
        {
            return refundSale.Value.CashierID;
        }

        public string GetRefundSaleTill()
        {
            return refundSale.Value.TillNumber;
        }

        public string GetRefundSaleTran()
        {
            return refundSale.Value.TransactionNumber;
        }

        public string GetRefundSaleLineTill(int lineNumber)
        {
            return refundSale.Value.SaleLines.First(l => l.LineNumber == lineNumber).TillNumber;
        }

        public string GetRefundSaleLineTran(int lineNumber)
        {
            return refundSale.Value.SaleLines.First(l => l.LineNumber == lineNumber).TransactionNumber;
        }

        public string GetRefundSalePaidTill()
        {
            return refundSale.Value.SaleTenders.First().TillNumber;
        }

        public string GetRefundSalePaidTran()
        {
            return refundSale.Value.SaleTenders.First().TransactionNumber;
        }

        #endregion
    }
}
