using System;
using System.Collections.Generic;
using Cts.Oasys.Hubs.Core.Order.Qod;

namespace WSS.AAT.Common.Qod
{
    public class AllQodStorage
    {
        public int GetOrdersCountToSyncToOM()
        {
            return SharedFunctions.GetAllNewOrders().Count;
        }
    }
}
