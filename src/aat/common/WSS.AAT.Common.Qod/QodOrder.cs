using System;
using System.Linq;
using Cts.Oasys.Hubs.Core.Order.Qod;

namespace WSS.AAT.Common.Qod
{
    public class QodOrder
    {
        private readonly QodHeader qodHeader;
        public QodHeader Header
        {
            get
            {
                return qodHeader;
            }
        }
        private readonly Lazy<SaleHeader> qodSale;
        public SaleHeader QodSale
        {
            get
            {
                return qodSale.Value;
            }
        }

        private readonly Lazy<SaleHeader> refundSale;
        public SaleHeader RefundSale
        {
            get
            {
                return refundSale.Value;
            }
        }

        public QodOrder(QodHeader qodHeader)
        {
            this.qodHeader = qodHeader;
            qodSale = new Lazy<SaleHeader>(LoadQodSale);
            refundSale = new Lazy<SaleHeader>(LoadRefundSale);
        }

        private SaleHeader LoadQodSale()
        {
            var saleHeaders = new SaleHeaderCollection();
            saleHeaders.LoadExistingSale(qodHeader.DateOrder, qodHeader.TranTill, qodHeader.TranNumber);

            var saleHeader = saleHeaders.First();
            saleHeader.LoadSalePaid();

            return saleHeader;
        }

        private SaleHeader LoadRefundSale()
        {
            var saleHeaders = new SaleHeaderCollection();
            saleHeaders.LoadExistingSale(qodHeader.RefundTranDate.Value, qodHeader.RefundTill, qodHeader.RefundTranNumber);

            var saleHeader = saleHeaders.First();
            saleHeader.LoadSalePaid();

            return saleHeader;
        }

        public static QodOrder Get(string orderNumber)
        {
            var qodHeaders = new QodHeaderCollection();
            qodHeaders.LoadExistingOrder(orderNumber);
            return new QodOrder(qodHeaders.First());
        }
    }
}
