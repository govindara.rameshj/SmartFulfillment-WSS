using System;
using System.Data;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class ManagersDashboardReportsRepository: BaseRepository
    {
        public DataTable GetBankingReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetBankingReportOnDashBoard(date);
            }
        }

        public DataTable GetBankingReportsReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetBankingReportsReportOnDashBoard(date);
            }
        }

        public DataTable GetDrlAnalysisReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetDrlAnalysisReportOnDashBoard(date);
            }
        }

        public DataTable GetPendingShrinkageReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetPendingShrinkageReportOnDashBoard();
            }
        }

        public DataTable GetPriceChangesReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetPriceChangesReportOnDashBoard(date);
            }
        }

        public DataTable GetShrinkageReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetShrinkageReportOnDashBoard(date);
            }
        }

        public DataTable GetStoreSalesReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetStoreSalesReportOnDashboard(date);
            }
        }

        public DataTable GetPendingOrdersReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetPendingOrdersReportOnDashboard(date);
            }
        }

        public DataTable GetStockReportsReportOnDashboard(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetStockReportsReportOnDashboard(date);
            }
        }

        public DateTime GetThisDayLastYear(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetThisDayLastYear(date);
            }
        }
    }
}
