﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class PosRepository : BaseRepository
    {
        public IList<TenderOption> SelectTenderOptions()
        {
            using (var db = GetDb())
            {
                return db.GetTable<TenderOption>().ToList();
            }
        }

        public IList<TenderControl> SelectTenderControls()
        {
            using (var db = GetDb())
            {
                return db.GetTable<TenderControl>().ToList();
            }
        }

        public IList<TillTosControl> SelectTillTosControls()
        {
            using (var db = GetDb())
            {
                return db.GetTable<TillTosControl>().ToList();
            }
        }

        public IList<TillTosOption> SelectTillTosOptions()
        {
            using (var db = GetDb())
            {
                return db.GetTable<TillTosOption>().ToList();
            }
        }

        public IList<TillTosTenderAllowed> SelectTendersAllowed()
        {
            using (var db = GetDb())
            {
                return db.GetTable<TillTosTenderAllowed>().ToList();
            }
        }
    }
}
