extern alias bltoolkit;
using bltoolkit::BLToolkit.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Database;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.DataLayer.Model.Entities;
using Cts.Oasys.Core.Helpers;

namespace WSS.AAT.Common.DataLayer
{
    public class TablesRepository : BaseRepository
    {
        public IEnumerable<Report> GetTableReport()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportTable
                       .ToList();
                return actual;
            }
        }

        public List<ReportColumn> GetTableReportColumn()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportColumnTable
                       .ToList();
                return actual;
            }
        }

        public List<ReportColumns> GetTableReportColumns()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportColumnsTable
                       .ToList();
                return actual;
            }
        }

        public List<ReportHyperlink> GetTableReportHyperlink()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportHyperlinkTable
                       .ToList();
                return actual;
            }
        }

        public List<ReportParameters> GetTableReportParameters()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportParametersTable
                       .ToList();
                return actual;
            }
        }

        public List<ReportTable> GetTableReportTable()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportTableTable
                       .ToList();
                return actual;
            }
        }

        public List<ReportGrouping> GetTableReportGrouping()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportGroupingTable
                       .ToList();
                return actual;
            }
        }

        public List<ReportRelation> GetTableReportRelation()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportRelationTable
                       .ToList();
                return actual;
            }
        }

        public List<ReportSummary> GetTableReportSummary()
        {
            using (var db = GetDb())
            {
                var actual = db.ReportSummaryTable
                       .ToList();
                return actual;
            }
        }

        public List<MenuConfig> GetTableMenuConfig()
        {
            using (var db = GetDb())
            {
                var actual = db.MenuConfigTable
                       .ToList();
                return actual;
            }
        }

        public List<ProfilemenuAccess> GetTableProfilemenuAccess()
        {
            using (var db = GetDb())
            {
                var actual = db.ProfilemenuAccessTable
                       .ToList();
                return actual;
            }
        }

        public List<Parameter> GetTableParameters()
        {
            using (var db = GetDb())
            {
                var actual = db.ParametersTable
                       .Where(p => (p.Id <= 0 || p.Id >= 980000) && p.Id != 999999)
                       .ToList();
                return actual;
            }
        }

        public List<PriceChange> GetTablePriceChange()
        {
            using (var db = GetDb())
            {
                var actual = db.PriceChangeTable
                       .ToList();
                return actual;
            }
        }

        public void UpdateSystemDateTable(DateTime sysDate)
        {
            using (var db = GetDb())
            {
                db.SystemDateTable
                    .Update(a => new SystemDate
                    {
                        SysDate = sysDate
                    }
                );
            }
        }

        public void UpdateNetworkIDSystemTable(DateTime sysDate)
        {
            using (var db = GetDb())
            {
                db.NetworkIdSystemTable
                    .Update(a => new NetworkIdSystem
                    {
                        SystemDate = sysDate
                    }
                );
            }
        }

        public bool IsPriceChangeEventCreated()
        {
            bool priceChangesCreated;
            using (var db = GetDb())
            {
                priceChangesCreated = db.PriceChangeTable.Any();
            }
            return priceChangesCreated;
        }

        public void DeleteFromStockMasterTable(string sku)
        {
            using (var db = GetDb())
            {
                db.StockMasterTable
                    .Delete(sm => sm.SkuNumber == sku);
            }
        }

        public void DeleteFromPriceChangeTable()
        {
            using (var db = GetDb())
            {
                db.PriceChangeTable.Delete();
            }
        }

        public void DeleteFromEventChangeTable()
        {
            using (var db = GetDb())
            {
                db.EventChangeTable.Delete();
            }
        }

        public void UpdateStockMasterTable(IEnumerable<StockMaster> stockMaster)
        {
            using (var db = GetDb())
            {
                db.Update<StockMaster>(stockMaster);
            }
        }

        public void InsertEventChangeTable(IEnumerable<EventChange> eventChange)
        {
            using (var db = GetDb())
            {
                foreach (var ec in eventChange)
                {
                    db.Insert(ec);
                }
            }
        }

        public void InsertPriceChangeTable(IEnumerable<PriceChange> priceChange)
        {
            using (var db = GetDb())
            {
                foreach (var pc in priceChange)
                {
                    db.Insert(pc);
                }
            }
        }

        public void UpdateBoolParameterValue(bool value, int parameterId)
        {
            using (var db = GetDb())
            {
                db.GetTable<Parameter>().Where(x => x.Id == parameterId).Set(x => x.BooleanValue, value).Update();
            }
        }

        public void UpdateStringParameterValue(string value, int parameterId)
        {
            using (var db = GetDb())
            {
                db.GetTable<Parameter>().Where(x => x.Id == parameterId).Set(x => x.StringValue, value).Update();
            }
        }
    }
}
