using System;
using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class TestExternalRequestRepository : ExternalRequestRepository
    {
        // ReSharper disable once EmptyConstructor
        public TestExternalRequestRepository()
        {
        }

        public ExternalRequest FindExternalRequest(string type, string omOrderNumber, DateTime createdAfter)
        {
            using (var db = GetDb())
            {
                var query =
                    from e in db.GetTable<ExternalRequest>()
                    where e.CreateTime >= createdAfter &
                    e.Source.Contains(omOrderNumber) &
                    e.Type == type
                    select e;

                return query.FirstOrDefault();
            }
        }
    }
}