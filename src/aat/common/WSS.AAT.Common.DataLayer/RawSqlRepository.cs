using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data.SqlClient;
using Cts.Oasys.Core.Helpers;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using OasysDBBO.Oasys3.DB;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class RawSqlRepository : BaseRepository
    {
        public void ExecuteScript(string content)
        {
            using (var db = GetDb())
            {
                SqlConnection conn = (SqlConnection) db.Connection;
                ExecuteSmoScript(content, conn);
            }
        }

        public void RollbackDatabaseToSnapshot(string snapshotName)
        {
            int killedCount;
            using (var db = GetDb())
            {
                SqlConnection conn = (SqlConnection) db.Connection;
                var dbName = conn.Database;

                killedCount = KillAllClientProcesses(conn, dbName);
                RestoreDatabaseSnapshot(conn, dbName, snapshotName);
            }

            // reset connection pool as the SQL Server processes were killed
            SqlConnection.ClearAllPools();
            RestorePooledOdbcConnections(killedCount + ConfigurationHelper.GetAppSettingValue("additionalOdbcConnectionsToRestoreAfterSnapshotRollback", 0));
        }

        private static void RestoreDatabaseSnapshot(SqlConnection conn, string dbName, string snapshotName)
        {
            const string sqlFormat =
@"USE [master]
RESTORE DATABASE [{0}] FROM DATABASE_SNAPSHOT = '{1}'";

            string sql = String.Format(sqlFormat, dbName, snapshotName);
            ExecuteSmoScript(sql, conn);
        }

        private static int KillAllClientProcesses(SqlConnection conn, string dbName)
        {
            const string sqlFormat =
@"USE [master]
DECLARE @count int = 0;
DECLARE @kill varchar(8000) = '';
SELECT @kill = @kill + 'kill ' + CONVERT(varchar(5), spid) + ';'
    ,@count = @count + 1
    FROM master..sysprocesses
    WHERE dbid = db_id('{0}') AND spid > 50;
EXEC(@kill);
select @count;";
            return ExecuteSmoScriptWithResult<int>(String.Format(sqlFormat, dbName), conn);
        }

        private static void RestorePooledOdbcConnections(int killedCount)
        {
            var openedConnections = new List<OdbcConnection>();

            for (int i = 0; i < killedCount; ++i)
            {
                var db = new clsOasys3DB("Default", 0);
                var connection = db.Connection;
                connection.Open();
                try
                {
                    ExecuteDummySqlCommand(connection);
                    // looks like the connection was created after snapshot restoration, dont close rgiht now to avoid getting it in the next loop iteration
                    openedConnections.Add(connection);
                }
                catch (InvalidOperationException)
                {
                    // looks like the connection was disabled, although it should be in good state after the call, dont close rgiht now to avoid getting it in the next loop iteration
                    openedConnections.Add(connection);
                }
                catch (Exception)
                {
                    // some unexpected exception, close all connections and throw it to caller
                    connection.Close();
                    foreach (var openedConnection in openedConnections)
                    {
                        openedConnection.Dispose();
                    }

                    throw;
                }
            }

            foreach (var connection in openedConnections)
            {
                // just dispose, it was restored after the InvalidOperationException
                connection.Dispose();
            }
        }

        private static void ExecuteDummySqlCommand(OdbcConnection connection)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = "select db_name()";
            cmd.ExecuteScalar();
        }

        private static void ExecuteSmoScript(string content, SqlConnection conn)
        {
            Server server = new Server(new ServerConnection(conn));
            server.ConnectionContext.ExecuteNonQuery(content);
        }

        private static T ExecuteSmoScriptWithResult<T>(string content, SqlConnection conn)
        {
            Server server = new Server(new ServerConnection(conn));
            return (T)server.ConnectionContext.ExecuteScalar(content);
        }
    }
}
