extern alias bltoolkit;

using System.Linq;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;
using System;
using System.Collections.Generic;
using WSS.AAT.Common.DataLayer.Entities;
using WSS.BO.DataLayer.Repositories;
using System.Data;

namespace WSS.AAT.Common.DataLayer
{
    public class TestMiscRepository : BaseRepository
    {
        public string CalculateAuthCodeForGiftCard()
        {
            string result;
            using (var db = GetDb())
            {
                var maxAuthCode = db.GetTable<DailyGiftCard>().Select(x => x.AuthCode).Max();
                result = String.IsNullOrEmpty(maxAuthCode) ? "000000001" : (Convert.ToInt32(maxAuthCode) + 1).ToString("d9");
            }
            return result;
        }

        public int GetPeriodId(DateTime creationDateTime)
        {
            using (var db = GetDb())
            {
                return db.GetTable<SystemPeriods>().Where(x => x.StartDate.Date == creationDateTime.Date).Select(x => x.PeriodId).FirstOrDefault();
            }
        }

        public void SetSettingValue(int parameterId, bool value)
        {
            using (var db = GetDb())
            {
                db.GetTable<Parameter>().Update(
                    p => p.Id == parameterId,
                    p => new Parameter
                    {
                        BooleanValue = value
                    });
            }
        }

        #region Clear tables

        public void ClearOrderTables()
        {
            ClearTables("dbo.[CORHDR]", "dbo.[CORHDR4]", "dbo.[CORHDR5]", "dbo.[CORLIN]", "dbo.[CORLIN2]", "dbo.[QUOHDR]", "dbo.[QUOLIN]", "dbo.[DLTOTS]", "dbo.[DLLINE]",
                "dbo.[SafeBagScanned]", "dbo.[SafeBags]", "dbo.[SafeBagsDenoms]", "dbo.[SafeDenoms]", "dbo.[SafeComment]", "dbo.[Safe]", "dbo.[NonTradingDay]", "dbo.[CORHDR4Log]");
        }

        public void ClearCashBalTables()
        {
            ClearTables("dbo.[CashBalCashier]", "dbo.[CashBalCashierTen]", "dbo.[CashBalCashierTenVar]", "dbo.[CashBalTill]", "dbo.[CashBalTillTen]", "dbo.[CashBalTillTenVar]");
        }

        public void ClearStockLog()
        {
            ClearTables("dbo.[STKLOG]");
        }
        
        private void ClearTables(params string[] tables)
        {
            using (var db = GetDb())
            {
                foreach (var table in tables)
                {
                    db.SetCommand("DELETE FROM " + table + ";").ExecuteNonQuery();
                }
            }
        }

        #endregion

        public IEnumerable<DashSalesReportRow> GetDashSalesReport(DateTime endDate)
        {
            using (var db = GetDb())
            {
                return db.SetSpCommand("DashSales2", db.Parameter("@DateEnd", endDate)).ExecuteList<DashSalesReportRow>();
            }
        }

        public void UpdateSysdat(DateTime today)
        {
            using (var db = GetDb())
            {
                db.SetCommand("update dbo.Sysdat set TODT = @todt, TODW = @todw, TMDT = @tmdt, TMDW = @tmdw",
                    db.Parameter("@todt", today),
                    db.Parameter("@todw", GetDayOfWeek(today)),
                    db.Parameter("@tmdt", today.AddDays(1)),
                    db.Parameter("@tmdw", GetDayOfWeek(today.AddDays(1))))
                    .ExecuteScalar();
            }
        }

        public DataTable GetCustomerOrderEnquiryReport(string storeId)
        {
            using (var db = GetDb())
            {
                return db.SetCommand(@"
                    SELECT a.NUMB, a.DATE1 As OrderDate, a.DELD, a.DELI, b.SKUN, b.QTYO, b.QTYT, b.QTYR, c.DESCR, c.INON, c.IDEL, c.ONHA, c.IOBS, c.ICAT, c.IRIS, c.IMDN, c.NOOR, c.FODT, d.SOLD
                    FROM CORHDR a
                        INNER JOIN CORHDR4 c4 ON a.NUMB = c4.NUMB
                        INNER JOIN CORLIN b ON a.NUMB = b.NUMB
                        LEFT JOIN STKMAS c ON b.SKUN = c.SKUN
                        LEFT JOIN QUOHDR d ON a.NUMB = d.NUMB
                    WHERE a.CANC <> '1' 
                        AND c4.DELIVERYSTATUS < '900' 
                        AND (ISNULL(a.STIL,0) < '13' OR (ISNULL(a.STIL,0) >= '90' AND ISNULL(a.STIL,0) <= '99'))
                        AND b.DELIVERYSOURCE = @storeId",
                            db.Parameter("@storeId", storeId))
                            .ExecuteDataTable();
            }
        }

        private int GetDayOfWeek(DateTime date)
        {
            return (date.DayOfWeek == DayOfWeek.Sunday) ? 7 : (int)date.DayOfWeek;
        }
    }
}
