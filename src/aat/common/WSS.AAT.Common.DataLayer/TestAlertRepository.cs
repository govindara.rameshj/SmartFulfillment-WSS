﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.AAT.Common.DataLayer
{
    public class TestAlertRepository : AlertRepository
    {
        public bool AlertExists(String orderNumber, byte alertType, DateTime dateTime)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Alert>()
                    .Where(a => a.OrderNumb == orderNumber && a.AlertType == alertType && a.ReceivedDate >= dateTime)
                    .Any();
            }
        }

    }
}
