extern alias bltoolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class BankingSuiteRepository : BaseRepository
    {
        public SystemUser GetSystemUserByName(string userName)
        {
            using (var db = GetDb())
            {
                return db.GetTable<SystemUser>().FirstOrDefault(x => x.EmployeeName == userName);
            }
        }

        public void ResetDataBeforetestRun()
        {
            using (var db = GetDb())
            {
                db.GetTable<SafeBagScanned>().Delete();
                db.GetTable<Safe>().Delete();
                db.GetTable<SafeDenomination>().Delete();
                db.GetTable<CashBalCashier>().Delete();
                db.GetTable<CashBalCashierTen>().Delete();
                db.GetTable<CashBalCashierTenVar>().Delete();
                db.GetTable<NonTradingDay>().Delete();
                db.GetTable<SafeBagsDenoms>().Delete();
                db.GetTable<SafeBags>().Delete();
            }
        }

        #region Safe

        public void ClearSafeTablesForTest()
        {
            int firstPeriodId = GetSafeFirstPeriodId();

            using (var db = GetDb())
            {
                db.GetTable<Safe>().Delete(safe => safe.PeriodId != firstPeriodId);
                db.GetTable<SafeDenomination>().Delete(safe => safe.PeriodId != firstPeriodId);
            }
        }

        public int GetSafeFirstPeriodId()
        {
            using (var db = GetDb())
            {
                return db.GetTable<Safe>().Min(safe => safe.PeriodId);
            }
        }

        public DateTime GetSafeLastPeriodDate()
        {
            using (var db = GetDb())
            {
                return db.GetTable<Safe>().Max(safe => safe.PeriodDate);
            }
        }

        public int GetSafeDenomsLastPeriodId()
        {
            using (var db = GetDb())
            {
                return db.GetTable<SafeDenomination>().Max(safe => safe.PeriodId);
            }
        }

        public List<int> GetSafePeriodIdList()
        {
            using (var db = GetDb())
            {
                return db.GetTable<Safe>().Select(safe => safe.PeriodId).ToList();
            }
        }

        public List<int> GetSafeDenomsPeriodIdList()
        {
            using (var db = GetDb())
            {
                return db.GetTable<SafeDenomination>().Select(safe => safe.PeriodId).Distinct().ToList();
            }
        }

        #endregion
    }
}
