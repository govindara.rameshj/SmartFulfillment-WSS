extern alias bltoolkit;

using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using System;
using Cts.Oasys.Core.Helpers;

namespace WSS.AAT.Common.DataLayer
{
    public class TestVisionPaymentRepository: BaseRepository
    {
        public void InsertIntoVisionPayment(decimal valueTender, DateTime date)
        {
            VisionPayment visPayment = new VisionPayment();

            visPayment.ValueTender = valueTender;
            visPayment.TranDate = date;

            using (var db = GetDb())
            {
                db.Insert(visPayment);
            }
        }

        public void CleanVisionPayment()
        {
            using (var db = GetDb())
            {
                db.VisionPaymentTable.Delete();
            }
        }
    }
}
