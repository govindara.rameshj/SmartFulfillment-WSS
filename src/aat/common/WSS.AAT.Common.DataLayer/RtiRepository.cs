﻿extern alias bltoolkit;
using System.Linq;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.DataLayer.Model.Entities;

namespace WSS.AAT.Common.DataLayer
{
    public class RtiRepository : BaseRepository
    {
        public CustomerOrderHeader LoadCustomerOrderHeaderValueFields(string numbOrder)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CustomerOrderHeader>().Where(x => x.OrderNumber == numbOrder).FirstOrDefault();
            }
        }
    }
}
