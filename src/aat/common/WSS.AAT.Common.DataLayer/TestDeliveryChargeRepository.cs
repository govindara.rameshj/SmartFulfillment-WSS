﻿extern alias bltoolkit;

using System.Linq;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;
using Cts.Oasys.Core.Helpers;


namespace WSS.AAT.Common.DataLayer
{
    public class TestDeliveryChargeRepository: DeliveryChargeRepository
    {
        public void UpdateDeliveryChargeGroupValue(string group, decimal oldValue, decimal newValue)
        {
            using (var db = GetDb())
            {
                db.GetTable<DeliveryChargeGroupValue>().Where(x => x.Group == group.ToString() && x.Value == oldValue).Set(x => x.Value, newValue).Update();
            }
        }

        public void InsertDeliveryChargeGroup(string group, decimal value, string skuNumber)
        {
            using (var db = GetDb())
            {
                if (!CheckIfDeliveryChargeGroupExists(group, skuNumber))
                {
                    var deliveryChargeGroup = new DeliveryChargeGroup() {Group = group, SkuNumber = skuNumber, IsDefault = false};
                    db.Insert(deliveryChargeGroup);

                    int id = db.GetTable<DeliveryChargeGroup>().First(x => x.Group == group).Id;

                    var deliveryChargeGroupValue = new DeliveryChargeGroupValue() {Id = id, Group = group, Value = value};
                    db.Insert(deliveryChargeGroupValue);
                }
                else
                {
                    if (!CheckIfDeliveryChargeGroupValueExists(group, value))
                    {
                        int id = db.GetTable<DeliveryChargeGroup>().First(x => x.Group == group).Id;

                        var deliveryChargeGroupValue = new DeliveryChargeGroupValue() { Id = id, Group = group, Value = value };
                        db.Insert(deliveryChargeGroupValue);
                    }
                }
            }
        }

        public void DeleteDeliveryChargeGroupValue(decimal value)
        {
            using (var db = GetDb())
            {
                db.GetTable<DeliveryChargeGroupValue>().Delete(x => x.Value == value);
            }
        }

        private bool CheckIfDeliveryChargeGroupExists(string group, string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DeliveryChargeGroup>().Any(x => x.Group == group && x.SkuNumber == skuNumber);
            }
        }

        private bool CheckIfDeliveryChargeGroupValueExists(string group, decimal value)
        {
            using (var db = GetDb())
            {
                return db.GetTable<DeliveryChargeGroupValue>().Any(x => x.Group == group && x.Value == value);
            }
        }
    }
}
