extern alias bltoolkit;
using bltoolkit::BLToolkit.Data;
using System;
using System.Linq;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class ChangesTrackingRepository : BaseRepository
    {
        public int GetTableRowsCount(string tableName)
        {
            int count = 0;
            using (var db = GetDb())
            {
                switch (tableName.ToUpper())
                {
                    case "EVTENQ":
                        count = db.EvtenqTable.Count();
                        break;
                    case "HHTHDR":
                        count = db.PicCountHeaderFileTable.Count();
                        break;
                    case "HHTDET":
                        count = db.PicCountDetailFileTable.Count();
                        break;
                    case "SYSDAT":
                        count = db.SystemDateTable.Count();
                        break;
                    case "RETOPT":
                        count = db.RetailOptionsTable.Count();
                        break;
                    case "DRLSUM":
                        count = db.DrlSummaryTable.Count();
                        break;
                    case "DRLDET":
                        count = db.DailyReformattedTillTotalsTable.Count();
                        break;
                    case "STKADJ":
                        count = db.StockAdjustmentFileTable.Count();
                        break;
                    case "STKMAS":
                        count = db.StockMasterTable.Count();
                        break;
                    case "SKU":
                        count = db.SkuTable.Count();
                        break;
                    case "SKUWORD":
                        count = db.SkuWordTable.Count();
                        break;
                    case "HIERARCHYWORD":
                        count = db.HierarchyWordTable.Count();
                        break;
                    case "PRCCHG":
                        count = db.PriceChangeTable.Count();
                        break;
                }
            }
            return count;
        }

        public int GetNumberOfChanges(string tableName)
        {
            string query = String.Format("SELECT count(1) as sum FROM CHANGETABLE (CHANGES {0}, 1) as c", tableName);
            using (var db = GetDb())
            {
                return db.SetCommand(query)
                    .ExecuteScalar<int>(ScalarSourceType.DataReader);
            }
        }
    }
}
