﻿extern alias bltoolkit;

using System;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class TestPicCountRepository : BaseRepository
    {
        public void InsertStubPicCountHeader()
        {
            using (var db = GetDb())
            {
                var newRecord = new PicCountHeaderFile()
                {
                    Date = DateTime.Today
                };
                Insert(db, newRecord);
            }
        }

        public void InsertStubPicCountDetail(string skuNumber)
        {
            using (var db = GetDb())
            {
                var newRecord = new PicCountDetailFile()
                {
                    Date = DateTime.Today,
                    NormalOnHandStockStartDay = 285,
                    SkuNumber = skuNumber,
                    TotalPresoldStock = 70,
                    TotalShopFloorCounts = 350,
                    TotalWarehouseCounts = 0
                };
                Insert(db, newRecord);
            }
        }

        public void CleanPicCountTables()
        {
            using (var db = GetDb())
            {
                db.GetTable<PicCountDetailFile>().Delete();
                db.GetTable<PicCountHeaderFile>().Delete();
            }
        }
    }
}
