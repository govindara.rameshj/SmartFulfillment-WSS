extern alias bltoolkit;

using System;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.DataLayer.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;

namespace WSS.AAT.Common.DataLayer
{
    public class TestStockRepository: StockRepository
    {
        public void CleanStockTables()
        {
            CleanStockAdjustment();
            CleanStockLog();
        }

        public void CleanStockAdjustment()
        {
            using (var db = GetDb())
            {
                db.GetTable<StockAdjustmentFile>().Delete();
            }
        }

        public void CleanStockLog()
        {
            using (var db = GetDb())
            {
                db.GetTable<Stklog>().Delete();
            }
        }

        public void InsertIntoStockAdjustment(string skuNumber, decimal quantity, decimal price, string writeOffCashier, DateTime date, string movedOrWriteOff)
        {
            var stkadj = new StockAdjustmentFile();
            stkadj.SkuNumber = skuNumber;
            stkadj.Quantity = quantity;
            stkadj.Price = price;
            stkadj.WriteOffCashier = writeOffCashier;
            stkadj.Date = date;
            stkadj.MovedOrWriteOff = movedOrWriteOff;

            using (var db = GetDb())
            {
                db.Insert(stkadj);
            }
        }

        public void CleanStockMaster()
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>().Delete();
            }
        }

        public void UpdateStockMaster(string skuNumber, int stockOnHand, int markdownStock)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>()
                .Where(sm => sm.SkuNumber == skuNumber)
                .Set(sm => sm.StockOnHand, stockOnHand)
                .Set(sm => sm.MarkdownStock, markdownStock).Update();
            }
        }

        public void UpdateStockMaster(string skuNumber, decimal price)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>()
                .Where(sm => sm.SkuNumber == skuNumber)
                .Set(sm => sm.Price, price).Update();
            }
        }

        public void UpdateStockMasterDateAndPriceOfPriceChange(string skuNumber, decimal newPrice, DateTime dateOfPriceChange)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>()
                .Where(sm => sm.SkuNumber == skuNumber)
                .Set(sm => sm.DateOfLastPriceChange, dateOfPriceChange)
                .Set(sm => sm.Price, newPrice).Update();
            }
        }

        public void UpdateStockMaster(StockUpdateForPriceChanges stockUpdate)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>()
                    .Where(sm => sm.SkuNumber == stockUpdate.SkuNumber)
                    .Set(sm => sm.StockOnHand, stockUpdate.OnHandQuantity)
                    .Set(sm => sm.MarkdownStock, stockUpdate.MarkdownStockQuanty)
                    .Set(sm => sm.Obsolete, stockUpdate.IsObsolete? "1" : "0")
                    .Set(sm => sm.NonStocked, stockUpdate.IsNonStock ? "1" : "0")
                    .Set(sm => sm.AutoApply, stockUpdate.IsAutoApplyPriceChanges ? "1" : "0")
                    .Update();
            }
        }

        public void DeleteAllExcept(List<string> skuList)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>().Delete(x => !skuList.Contains(x.SkuNumber));
            }
        }

        public decimal GetStockOnHand(string sku)
        {
            return Select<StockMaster>()
                            .Where(sm => sm.SkuNumber == sku)
                            .Select(sm => sm.StockOnHand)
                            .FirstOrDefault();
        }

        public decimal GetMarkdownStockOnHand(string sku)
        {
            return Select<StockMaster>()
                            .Where(sm => sm.SkuNumber == sku)
                            .Select(sm => sm.MarkdownStock)
                            .FirstOrDefault();
        }

        public void SetStockAmount(string skuNumber, decimal stockAmount)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>().Where(x => x.SkuNumber == skuNumber).Set(x => x.StockOnHand, stockAmount).Update();
            }
        }

        public void SetMarkdownStockAmount(string skuNumber, decimal markdownStockAmount)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>().Where(x => x.SkuNumber == skuNumber).Set(x => x.MarkdownStock, markdownStockAmount).Update();
            }
        }

        public void SetStockUnitsValues(string skuNumber, int value)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>().Where(x => x.SkuNumber == skuNumber)
                    .Set(x => x.SalesUnits1, value)
                    .Set(x => x.SalesUnits2, value)
                    .Set(x => x.SalesUnits4, value)
                    .Set(x => x.SalesUnits6, value)
                    .Update();
            }
        }

        public void SetStockValues(string skuNumber, decimal value)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>().Where(x => x.SkuNumber == skuNumber)
                    .Set(x => x.SalesValue1, value)
                    .Set(x => x.SalesValue2, value)
                    .Set(x => x.SalesValue4, value)
                    .Set(x => x.SalesValue6, value)
                    .Update();
            }
        }

        public void SetAdjustmentValue(string skuNumber, int value)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>().Where(x => x.SkuNumber == skuNumber)
                    .Set(x => x.AdjustmentsValue1, value)
                    .Update();
            }
        }

        public int GetStklogLineCount()
        {
            using (var db = GetDb())
            {
                return db.GetTable<Stklog>().Count();
            }
        }

        public Stklog GetLastStklogForSku(string skuNumber)
        {
            using (var db = GetDb())
            {
                var maxValue = db.GetTable<Stklog>().Where(x => x.ItemNumber == skuNumber).Max(x => x.Tkey);
                return db.GetTable<Stklog>().First(x => x.Tkey == maxValue);
            }
        }

        public Stklog GetLastStklogForSkuAndType(string skuNumber, string type)
        {
            using (var db = GetDb())
            {
                var maxValue = db.GetTable<Stklog>().Where(x => x.ItemNumber == skuNumber && x.TypeOfMovement == type).Max(x => x.Tkey);
                return db.GetTable<Stklog>().First(x => x.Tkey == maxValue);
            }
        }

        public Stklog GetStklogForSku(string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Stklog>().FirstOrDefault(x => x.ItemNumber == skuNumber);
            }
        }

        public Stklog GetLastStklog()
        {
            using (var db = GetDb())
            {
                var maxValue = db.GetTable<Stklog>().Max(x => x.Tkey);
                return db.GetTable<Stklog>().First(x => x.Tkey == maxValue);
            }
        }

        public void InsertIntoStockLog(int dayNumber, string type, DateTime datetime, string cashier, string sku, decimal price, int startStock, int endStock)
        {
            using (var db = GetDb())
            {
                db.Insert(new Stklog()
                {
                    ItemNumber = sku,
                    DayNumber = dayNumber,
                    TypeOfMovement = type,
                    DateOfMovement = datetime.Date,
                    TimeOfMovement = datetime.ToString("HHmmss"),
                    MovementKeys = "149000011",
                    CashierNumber = cashier,
                    StartingStockOnHand = startStock,
                    StartingPrice = price,
                    EndingStockOnHand = endStock,
                    EndingPrice = price,
                    RtiStatus = "S"
                }
                );
            }
        }

        public void RecalculateDayN()
        {
            using (var db = GetDb())
            {
                db.SetCommand("update dbo.STKLOG set DAYN = DATEDIFF(dd, '1900-01-01', DATE1) + 1 + DAYN where DAYN < 10")
                    .ExecuteNonQuery();
            }
        }

        public decimal GetSoldPerDay(string skun, DateTime date)
        {
            using (var db = GetDb())
            {
                return db.SetSpCommand("GetStkLogSoldPerDay",
                    db.Parameter("@skun", skun),
                    db.Parameter("@dayn", date.Subtract(new DateTime(1900, 1, 1)).Days + 1))
                .ExecuteScalar<decimal>();
            }
        }

        public StockAdjustmentFile SelectStockAdjustmentFile(string skuNumber, string adjustmentCode, bool isStaAuditUpload, string sequenceNumber, DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetTable<StockAdjustmentFile>().FirstOrDefault(x => x.SkuNumber == skuNumber && x.Code == adjustmentCode && x.IsStaAuditUpload == isStaAuditUpload && x.Date == date && x.SequenceNumber == sequenceNumber);
            }
        }

        public StockAdjustmentFile SelectStockAdjustmentFile(string skuNumber)
        {
            using (var db = GetDb())
            {
                return db.GetTable<StockAdjustmentFile>().FirstOrDefault(x => x.SkuNumber == skuNumber);
            }
        }

        public IList<StockAdjustmentFile> SelectStockAdjustmentFiles(string skuNumber, string adjustmentCode)
        {
            using (var db = GetDb())
            {
                return db.GetTable<StockAdjustmentFile>().Where(x => x.SkuNumber == skuNumber && x.Code == adjustmentCode).ToList();
            }
        }

        public Stklog SelectStockLog(string skuNumber, string movementType, DateTime date)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Stklog>().FirstOrDefault(x => x.ItemNumber == skuNumber && x.TypeOfMovement == movementType && x.DateOfMovement == date);
            }
        }
    }
}
