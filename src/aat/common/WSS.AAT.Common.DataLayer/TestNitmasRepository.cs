extern alias bltoolkit;
using bltoolkit::BLToolkit.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class TestNitmasRepository : NitmasRepository
    {
        public void ClearNitlog()
        {
            using (var db = GetDb())
            {
                db.GetTable<Nitlog>().Delete();
            }
        }

        public void AddTaskToRepo(string taskId)
        {
            AddTaskToRepoForDate(taskId, DateTime.Today);
        }

        public void AddTaskToRepoForDate(string taskId, DateTime date)
        {
            AddTaskToRepoForDatePassedAtTime(taskId, date, date.Add(DateTime.Now.TimeOfDay));
        }

        public void AddTaskToRepoForDatePassedAtTime(string taskId, DateTime date, DateTime endTime)
        {
            var startTme = endTime.AddMinutes(-2);

            using (var db = GetDb())
            {
                db.GetTable<Nitlog>().Insert(() => new Nitlog
                {
                    Date1 = date,
                    Nset = "1",
                    TaskNumber = taskId,
                    TaskStartedDate = startTme.Date,
                    TaskStartedTime = startTme.ToString("HHmmss"),
                    TaskEndDate = endTime.Date,
                    TaskEndTime = endTime.ToString("HHmmss")
                });
            }
        }

        public void CloseBankingDay(int periodId)
        {
            using (var db = GetDb())
            {
                db.GetTable<Safe>().Where(x => x.PeriodId == periodId).Set(x => x.IsClosed, true).Update();
            }
        }

        public void OpenBankingDay(int periodId)
        {
            using (var db = GetDb())
            {
                db.GetTable<Safe>().Where(x => x.PeriodId == periodId).Set(x => x.IsClosed, false).Update();
            }
        }

        public void ClearOrderTables()
        {
            using (var db = GetDb())
            {
                //db.GetTable<SystemDate>().Delete();
                db.GetTable<CustomerOrderHeader>().Delete();
                db.GetTable<CustomerOrderHeader4>().Delete();
                db.GetTable<CustomerOrderHeader5>().Delete();
                db.GetTable<CustomerOrderLine>().Delete();
                db.GetTable<CustomerOrderLine2>().Delete();
                db.GetTable<QuoteHeader>().Delete();
                db.GetTable<Dltots>().Delete();
                db.GetTable<DailyTillLine>().Delete();
                db.GetTable<Dlpaid>().Delete();
                //db.GetTable<SafeBagScanned>().Delete();
                db.GetTable<SafeBags>().Delete();
                //db.GetTable<SafeBagsDenoms>().Delete();
                //db.GetTable<SafeDenoms>().Delete();
                //db.GetTable<SafeComment>().Delete();
                //db.GetTable<NonTradingDay>().Delete();
                //db.GetTable<Safe>().Delete();
            }
        }

        public void ClearCashBalTables()
        {
            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Delete();
                db.GetTable<CashBalCashierTen>().Delete();
                db.GetTable<CashBalCashierTenVar>().Delete();
            }
        }

        public void CleanPickupTables()
        {
            using (var db = GetDb())
            {
                db.GetTable<SafeBags>().Delete();
                db.GetTable<Locks>().Delete();
            }
        }

        public void CleanLockTable()
        {
            using (var db = GetDb())
            {
                db.GetTable<Locks>().Delete();
            }
        }

        public void StartPickupProcessForCashier(string cashierId, DateTime lockTime)
        {
            using (var db = GetDb())
            {
                db.GetTable<Locks>().Insert(() => new Locks()
                {
                    EntityId = "NewBanking.Form.PickupEntry",
                    LockTime = lockTime,
                    OwnerId = cashierId
                });
            }
        }

        public void CompletePickupProcessForCashier(string cashierId, int periodId, DateTime pickupDate)
        {
            using (var db = GetDb()) 
            {
                db.GetTable<SafeBags>().Insert(() => new SafeBags()
                {
                    SealNumber = "12313212",
                    Type = "P",
                    State = "S",
                    Value = 0,
                    InPeriodID = periodId,
                    InDate = pickupDate,
                    InUserID1 = Int32.Parse(cashierId),
                    InUserID2 = 0,
                    OutPeriodID = 0,
                    OutUserID1 = 0,
                    OutUserID2 = 0,
                    AccountabilityType = "C",
                    AccountabilityID = Int32.Parse(cashierId),
                    FloatValue = 0,
                    PickupPeriodID = periodId,
                    RelatedBagId = 0,
                    Comments = String.Empty,
                });
            }
        }

        public IList<NightlyProcedure> GetAllNightlyProcedures()
        {
            using (var db = GetDb())
            {
                return db.NightlyProcedureRecordTable.ToList();
            }
        }

        public void InsertStubGapWalk(DateTime gapWalkDate, string skuNumber)
        {
            var gapWalk = new GapWalk()
            {
                DateCreated = gapWalkDate,
                Id = 1,
                IsPrinted = false,
                SkuNumber = skuNumber,
                Location = String.Empty,
                QuantityRequired = 0,
                Sequence = 1
            };

            InsertGapWalk(gapWalk);
        }

        public void SetStkmasLastSoldDate(string skuNumber, DateTime? lastSoldDate)
        {
            using (var db = GetDb())
            {
                db.GetTable<StockMaster>()
                    .Where(x => x.SkuNumber == skuNumber)
                    .Set(x => x.DateLastSold, lastSoldDate)
                    .Update();
            }
        }

        public void CleanDatabaseBeforeTests()
        {
            using (var db = GetDb())
            {
                db.GetTable<GapWalk>().Delete();
                db.GetTable<DailyTillLine>().Delete();
                db.GetTable<Dltots>().Delete();
            }
        }

        public void CreateBankingLock(DateTime dateForBanking)
        {
            using (var db = GetDb())
            {
                db.GetTable<Locks>().Insert(() => new Locks()
                {
                    EntityId = "NewBanking.Form.BankingPickupCheck",
                    LockTime = dateForBanking,
                    OwnerId = "001"
                });
            }
        }

        public void RemoveBankingLock()
        {
            using (var db = GetDb())
            {
                db.GetTable<Locks>().Where(x => x.EntityId == "NewBanking.Form.BankingPickupCheck").Delete();
            }
        }
    }
}
