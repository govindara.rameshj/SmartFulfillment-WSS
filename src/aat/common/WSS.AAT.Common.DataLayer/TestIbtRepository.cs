﻿extern alias bltoolkit;
using System;
using System.Linq;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class TestIbtRepository : BaseRepository
    {
        public void CleanDrl()
        {
            using (var db = GetDb())
            {
                db.GetTable<DrlSummary>().Delete();
                db.GetTable<DrlDetails>().Delete();
            }
        }

        public void CleanPurAndSupplierTables()
        {
            using (var db = GetDb())
            {
                db.GetTable<PurchaseOrderHeader>().Delete();
                db.GetTable<PurchaseOrderLine>().Delete();
                db.GetTable<SupplierMaster>().Delete();
                db.GetTable<DrlSummary>().Delete();
                db.GetTable<DailyReformattedTillTotals>().Delete();
            }
        }

        public void InsertStubPurhdr()
        {
            using (var db = GetDb())
            {
                var newRecord = new PurchaseOrderHeader()
                {
                    AssignedPricingDocNumber = 0,
                    AssignedReceiptNumber = 0,
                    BbcSupplierCode = String.Empty,
                    ConfirmationIndicator = "C",
                    CreationDate = DateTime.Today,
                    DeletedByMaintenance = false,
                    EmployeeId = null,
                    EntrySource = "H",
                    ExpectedDeliveryDate = DateTime.Today,
                    FreeFormMessage1 = String.Empty,
                    FreeFormMessage2 = String.Empty,
                    HeadOfficePurchaseOrderNumber = "309171",
                    IssueFileValidated = false,
                    LastUpdated = DateTime.Today,
                    Nocr = 5,
                    OrderValue = 279.50M,
                    PreparedForCommsToBbc = true,
                    PrintFlag = "N",
                    RaiserInitials = "BBC",
                    ReasonCodes = 0,
                    ReceivedComplete = false,
                    ReceivedPartial = false,
                    ReleaseNumber = 0,
                    ReprintsNumber = 0,
                    ResponseType = 309171,
                    SoqNumber = 0,
                    SupplierNumber = "01031",
                    TotalQuantity = 50,
                    TraderNetSupplier = false
                };
                Insert(db, newRecord);
            }
        }

        public void InsertStubPurlin()
        {
            using (var db = GetDb())
            {
                var key = db.GetTable<PurchaseOrderHeader>().Select(x => x.DigCompilerKey).FirstOrDefault();
                var newRecord = new PurchaseOrderLine()
                {
                    SkuNumber = "215613",
                    Cost = 0.0M,
                    DeleteLine = false,
                    FreeFormMessage = null,
                    HeaderKey = key,
                    KeyedDescription = String.Empty,
                    LastOrderDate = DateTime.Today,
                    LastReceiptDate = null,
                    OrderConfirmedIndicator = "A",
                    Price = 5.59M,
                    QuantityBackordered = 0,
                    QuantityOfReceipt = 0,
                    QuantityOrdered = 50,
                    QuantityReceived = 0,
                    ReasonCode = 0,
                    ReceiptCost = null,
                    ReceiptPrice = null,
                    ShipperNumber = String.Empty,
                    SupplierProductCode = "778064"
                };
                Insert(db, newRecord);
            }
        }

        public void InsertStubSupmas()
        {
            using (var db = GetDb())
            {
                var newRecord = new SupplierMaster()
                {
                    SupplierNumber = "01031",
                    AlphaKey = "COMMERCIAL CEIL",
                    SupplierName = "Commercial Ceiling Factors(CCF",
                    DeletedByHeadOffice = false,
                    HelpLineNumber = "0117 9822613",
                    PrimaryMerchant = String.Empty,
                    SupplierTypeCode = "01",
                    OrderDepotNumber = 1,
                    BbcSiteNumber = "000",
                    SoqControlNumber = 0,
                    ProcessSoq = false,
                    ReturnsDepotNumber = 1,
                    LastPurchaseOrderDate = DateTime.Today,
                    LastReceiptDate = DateTime.Today,
                    CurrentOutstandingPurchaseOrdersNumber = 2,
                    CurrentOutstandingPurchaseOrdersValue = 2511.02M,
                    QuantitiesOrderedSum1 = 17919,
                    QuantitiesOrderedSum2 = 92,
                    QuantitiesOrderedSum3 = 96,
                    QuantitiesOrderedSum4 = 104,
                    QuantitiesOrderedSum5 = 1063,
                    QuantitiesReceivedSum1 = 104,
                    QuantitiesReceivedSum2 = 92,
                    QuantitiesReceivedSum3 = 96,
                    QuantitiesReceivedSum4 = 104,
                    QuantitiesReceivedSum5 = 1063,
                    LinesPoReceivedSum1 = 5,
                    LinesPoReceivedSum2 = 6,
                    LinesPoReceivedSum3 = 6,
                    LinesPoReceivedSum4 = 5,
                    LinesPoReceivedSum5 = 61,
                    LinesPoNotReceivedSum1 = 0,
                    LinesPoNotReceivedSum2 = 0,
                    LinesPoNotReceivedSum3 = 0,
                    LinesPoNotReceivedSum4 = 0,
                    LinesPoNotReceivedSum5 = 0,
                    LinesReceivedSumOverQtyOrdered1 = 0,
                    LinesReceivedSumOverQtyOrdered2 = 0,
                    LinesReceivedSumOverQtyOrdered3 = 0,
                    LinesReceivedSumOverQtyOrdered4 = 0,
                    LinesReceivedSumOverQtyOrdered5 = 0,
                    LinesReceivedSumUnderQtyOrdered1 = 0,
                    LinesReceivedSumUnderQtyOrdered2 = 0,
                    LinesReceivedSumUnderQtyOrdered3 = 0,
                    LinesReceivedSumUnderQtyOrdered4 = 0,
                    LinesReceivedSumUnderQtyOrdered5 = 0,
                    NumberPoReceived1 = 3,
                    NumberPoReceived2 = 4,
                    NumberPoReceived3 = 6,
                    NumberPoReceived4 = 3,
                    NumberPoReceived5 = 44,
                    ValuePoReceived1 = 2357.84M,
                    ValuePoReceived2 = 1962.36M,
                    ValuePoReceived3 = 2230.08M,
                    ValuePoReceived4 = 2357.84M,
                    ValuePoReceived5 = 21427.99M,
                    DaysSumReceivedOrders1 = 7,
                    DaysSumReceivedOrders2 = 15,
                    DaysSumReceivedOrders3 = 19,
                    DaysSumReceivedOrders4 = 7,
                    DaysSumReceivedOrders5 = 158,
                    ShortestNumberOfDays1 = 2,
                    ShortestNumberOfDays2 = 0,
                    ShortestNumberOfDays3 = 0,
                    ShortestNumberOfDays4 = 2,
                    ShortestNumberOfDays5 = 0,
                    LongestNumberOfDays1 = 5,
                    LongestNumberOfDays2 = 0,
                    LongestNumberOfDays3 = 0,
                    LongestNumberOfDays4 = 5,
                    LongestNumberOfDays5 = 0,
                    VendorPrimaryCountCode = "00",
                    SomeNotUsedDate = null,
                    SoqDate = DateTime.Today,
                    PalletCheckNeeded = false
                };
                Insert(db, newRecord);
            }
        }

        public void SetNextNumberForId(int nextNumber, int id)
        {
            using (var db = GetDb())
            {
                db.GetTable<SystemNumbers>().Where(x => x.Id == id).Set(x=> x.NextNumber, nextNumber).Update();
            }
        }

        public string GetCurrentIbtCounterValue()
        {
            using (var db = GetDb())
            {
                return db.GetTable<SystemNumbers>().Where(x => x.Id == 4).Select(x => x.NextNumber).First().ToString().PadLeft(6, '0');
            }
        }

        public void InsertDrlSummary(DrlSummary drlSummary)
        {
            Insert(drlSummary);
        }

        public DrlSummary GetDrlSummary(string drlNumber)
        {
            return Select<DrlSummary>().FirstOrDefault(x => x.DrlNumber == drlNumber);
        }
    }
}
