﻿
namespace WSS.AAT.Common.DataLayer.Entities
{
    public class DashSalesReportRow
    {
        public int RowId { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int QtyWtd { get; set; }
        public decimal Value { get; set; }
        public decimal ValueWtd { get; set; }
    }
}
