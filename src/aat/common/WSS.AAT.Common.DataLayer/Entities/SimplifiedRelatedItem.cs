﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.BO.DataLayer.Model.Entities.NonPersistent
{
    public class SimplifiedRelatedItem
    {
        public string SingleSkun { get; set; }
        public decimal SinglePrice { get; set; }
        public int SingleOnHand { get; set; }
        public string PackSkun { get; set; }
        public decimal PackPrice { get; set; }
        public int PackOnHand { get; set; }
        public decimal ItemPackSize { get; set; }
    }
}
