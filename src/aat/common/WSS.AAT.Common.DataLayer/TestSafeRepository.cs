﻿extern alias bltoolkit;
using bltoolkit::BLToolkit.Data.Linq;
using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class TestSafeRepository : SafeRepository
    {
        public void SetSafeStatusByPeriod(int periodId, bool status)
        {
            using (var db = GetDb())
            {
                db.GetTable<Safe>().Where(x => x.PeriodId == periodId).Set(x => x.IsClosed, status).Update();
            }
        }
    }
}
