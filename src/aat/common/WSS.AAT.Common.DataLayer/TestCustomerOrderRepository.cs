extern alias bltoolkit;
using bltoolkit::BLToolkit.Data.Linq;
using System;
using System.Linq;
using WSS.BO.DataLayer.Repositories;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model.Constants;
using System.Data;

namespace WSS.AAT.Common.DataLayer
{
    public class TestCustomerOrderRepository : CustomerOrderRepository
    {
        public void RemoveOrdersFromTables(string orderNumber)
        {
            using (var db = GetDb())
            {
                db.GetTable<Dltots>().Delete(x => x.OrderNumber == orderNumber);
                RemoveOrdersFromTables();
            }
        }

        public void RemoveOrdersFromTables()
        {
            using (var db = GetDb())
            {
                db.GetTable<CustomerOrderHeader>().Delete();
                db.GetTable<CustomerOrderLine>().Delete();
                db.GetTable<CustomerOrderHeader4>().Delete();
                db.GetTable<CustomerOrderHeader5>().Delete();
                db.GetTable<CustomerOrderLine2>().Delete();
                db.GetTable<CustomerOrderRefund>().Delete();
                db.GetTable<CustomerOrderText>().Delete();
            }
        }

        public string GetCurrentOrderNumber()
        {
            using (var db = GetDb())
            {
                return db.GetTable<Sysnum>().Select(x => x.Next15).First();
            }
        }

        public void UpdateNextOrderNumber(string orderNumber)
        {
            using (var db = GetDb())
            {
                db.GetTable<Sysnum>().Set(x => x.Next15, orderNumber).Update();
            }
        }

        public string GetPreviousOrderNumber()
        {
            var currentOrderNumber = GetCurrentOrderNumber();
            return (Convert.ToInt32(currentOrderNumber) - 1).ToString("D6");
        }

        public string GetOrderNumberForOrderCancelledInOrder()
        {
            var currentOrderNumber = GetCurrentOrderNumber();
            return (Convert.ToInt32(currentOrderNumber) - 2).ToString("D6");
        }

        public void SetDeliveryStatusToDeliveredForOrder(string orderNumber)
        {
            using (var db = GetDb())
            {
                db.GetTable<CustomerOrderLine>()
                    .Where(x => x.OrderNumber == orderNumber)
                    .Set(x => x.DeliveryStatus, OrderConsignmentStatus.DeliveredStatusOk)
                    .Update();

                db.GetTable<CustomerOrderHeader4>()
                    .Where(x => x.OrderNumber == orderNumber)
                    .Set(x => x.DeliveryStatus, OrderConsignmentStatus.DeliveredStatusOk)
                    .Update();
            }
        }

        public void InsertIntoQuoHdr(QuoteHeader quoteHeader)
        {
            Insert(quoteHeader);
        }

        public DataTable QodGetOrder(string orderNumber)
        {
            using (var db = GetDb())
            {
                return db.SetSpCommand("QodGetOrder",
                    db.Parameter("@orderNumber", orderNumber)).ExecuteDataTable();
            }
        }

        public DataTable QodGetOrdersForDeliverySource(string storeId)
        {
            using (var db = GetDb())
            {
                return db.SetSpCommand("QodGetOrdersForDeliverySource",
                    db.Parameter("@deliveryStoreID", storeId),
                    db.Parameter("@selectDelivery", true)).ExecuteDataTable();
            }
        }

        public void ChangeDeliveryStatus(int newStatus)
        {
            using (var db = GetDb())
            {
                db.SetCommand("UPDATE dbo.CORHDR4 SET [DeliveryStatus] = @newStatus",
                    db.Parameter("@newStatus", newStatus)).ExecuteNonQuery();
            }
        }

        public DataTable GetDeliveryStatusChangeLog(DateTime date)
        {
            using (var db = GetDb())
            {
                return db.SetSpCommand("usp_GetDeliveryStatusLogInfo",
                    db.Parameter("@DateInterestedIn", date)).ExecuteDataTable();
            }
        }

        public void SetOrderLineNumber(string orderNumber, string oldOrderLineNumber, string newOrderLineNumber)
        {
            using (var db = GetDb())
            {
                db.GetTable<CustomerOrderLine>()
                    .Where(x => x.OrderNumber == orderNumber && x.LineNumber == oldOrderLineNumber)
                    .Set(x => x.LineNumber, newOrderLineNumber)
                    .Update();
            }
        }

        public void SetCustomerOrderQuantityTaken(string orderNumber, decimal quantityTaken)
        {
            using (var db = GetDb())
            {
                db.GetTable<CustomerOrderHeader>()
                    .Where(x => x.OrderNumber == orderNumber)
                    .Set(x => x.TotalUnitsTakenNumber, quantityTaken)
                    .Update();
            }
        }
    }
}
