extern alias bltoolkit;

using System.Linq;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;
using System;
using WSS.BO.DataLayer.Repositories;
using System.Collections.Generic;

namespace WSS.AAT.Common.DataLayer
{
    public class TestCashierBalanceRepository : CashierBalanceRepository
    {
        public void RemoveCashierBalances()
        {
            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Delete();
                db.GetTable<CashBalCashierTen>().Delete();
                db.GetTable<CashBalCashierTenVar>().Delete();
            }
        }

        #region CashierBalance (aka CashBalCashier)

        public CashBalCashier GetCashierBalance(int periodId, int cashierId, string currencyId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashier>()
                    .FirstOrDefault(cash => cash.PeriodId == periodId && cash.CashierId == cashierId && cash.CurrencyId == currencyId);
            }
        }

        private void CheckCashierBalance(int periodId, int cashierId, string currencyId)
        {
            if (GetCashierBalance(periodId, cashierId, currencyId) == null)
            {
                InsertEmptyCashierBalance(periodId, cashierId, currencyId);
            }
        }

        private void InsertEmptyCashierBalance(int periodId, int cashierId, string currencyId)
        {
            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Insert(() => new CashBalCashier { PeriodId = periodId, CashierId = cashierId, CurrencyId = currencyId });
            }
        }

        public void UpdateCashBalCashierForTest(int periodId, string currencyId, int cashierId, decimal amount, int numVoid)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Where(cash => cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                   .Set(x => x.NumVoids, numVoid)
                   .Set(x => x.GrossSalesAmount, amount)
                   .Set(x => x.SalesAmount, amount)
                   .Update();
            }
        }

        public void UpdateCashBalCashierRefundCountForTest(int periodId, string currencyId, int cashierId, decimal refundCount)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Where(cash => cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                   .Set(x => x.RefundCount, refundCount)
                   .Update();
            }
        }

        public void UpdateCashBalCashierForGiftCardTest(int periodId, string currencyId, int cashierId, decimal balance, int count)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Where(cash => cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                   .Set(x => x.MiscIncomeCount12, count)
                   .Set(x => x.MiscIncomeValue12, balance)
                   .Update();
            }
        }

        public void UpdateCashBalCashierTotalAmount(int periodId, string currencyId, int cashierId, decimal amount)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Where(cash => cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                   .Set(x => x.GrossSalesAmount, amount)
                   .Update();
            }
        }

        public void UpdateCashBalCashierSalesAmount(int periodId, string currencyId, int cashierId, decimal amount)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Where(cash => cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                   .Set(x => x.SalesAmount, amount)
                   .Update();
            }
        }

        public void UpdateCashBalCashierDiscountAmount(int periodId, string currencyId, int cashierId, decimal amount)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Where(cash => cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                   .Set(x => x.DiscountAmount, amount)
                   .Update();
            }
        }

        public void UpdateCashBalCashierRefundAmount(int periodId, string currencyId, int cashierId, decimal amount)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>().Where(cash => cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                   .Set(x => x.RefundAmount, amount)
                   .Update();
            }
        }

        public void SetReversedLinesCountInCashBalCashier(int periodId, string currencyId, int cashierId, int reversedLines)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>()
                    .Where(
                        cash =>
                            cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                    .Set(x => x.NumLinesReversed, reversedLines)
                    .Update();
            }
        }

        public void SetOpenDrawerLinesAmountInCashBalCashier(int periodId, string currencyId, int cashierId, int openDrawerLines)
        {
            CheckCashierBalance(periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashier>()
                    .Where(
                        cash =>
                            cash.PeriodId == periodId && cash.CurrencyId == currencyId && cash.CashierId == cashierId)
                    .Set(x => x.NumOpenDrawer, openDrawerLines)
                    .Update();
            }
        }
        #endregion

        #region CashierBalanceTender (aka CashBalCashierTen)


        public CashBalCashierTen GetCashierBalanceTender(int tenderTypeId, int periodId, int cashierId, string currencyId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashierTen>().FirstOrDefault(cash => cash.PeriodId == periodId && cash.CashierId == cashierId && cash.CurrencyId == currencyId && cash.TenderTypeId == tenderTypeId);
            }
        }

        public IList<CashBalCashierTen> GetCashierBalanceTenders(int periodId, int cashierId, string currencyId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashierTen>().Where(cash => cash.PeriodId == periodId && cash.CashierId == cashierId && cash.CurrencyId == currencyId).ToList();
            }
        }

        public void CheckCashierBalanceTender(int cardType, int periodId, int cashierId, string currencyId)
        {
            if (GetCashierBalanceTender(cardType, periodId, cashierId, currencyId) == null)
            {
                InsertEmptyCashierBalanceTender(cardType, periodId, cashierId, currencyId);
            }
        }

        private void InsertEmptyCashierBalanceTender(int tenderTypeId, int periodId, int cashierId, string currencyId)
        {
            using (var db = GetDb())
            {
                db.GetTable<CashBalCashierTen>().Insert(
                    () => new CashBalCashierTen
                    {
                        PeriodId = periodId,
                        CashierId = cashierId,
                        CurrencyId = currencyId,
                        TenderTypeId = Convert.ToInt32(tenderTypeId),
                        PickUp = 0,
                        Quantity = 0,
                        Amount = 0
                    });
            }
        }

        public void UpdateCashierBalanceTenderForTest(int periodId, int cashierId, string currencyId, int tenderTypeId, decimal amount)
        {
            CheckCashierBalanceTender(tenderTypeId, periodId, cashierId, currencyId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashierTen>().Where(x => x.PeriodId == periodId && x.CurrencyId == currencyId && x.CashierId == cashierId && x.TenderTypeId == tenderTypeId)
                   .Set(x => x.Quantity, 0)
                   .Set(x => x.Amount, x => amount)
                   .Update();
            }
        }

        public decimal GetCashierBalanceTenderAmount(int periodId, int cashierId, string currencyId, int tenderTypeId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashierTen>()
                    .Where(x => x.PeriodId == periodId && x.CurrencyId == currencyId && x.CashierId == cashierId && x.TenderTypeId == tenderTypeId)
                    .Select(x => x.Amount)
                    .FirstOrDefault();
            }
        }

        #endregion

        #region CashierBalanceTenderVariance (aka CashBalCashierTenVar)

        public CashBalCashierTenVar GetCashierBalanceTenderVariance(int tenderTypeId, int periodId, int cashierId, string currencyId, int tradingPeriodId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashierTenVar>()
                    .FirstOrDefault(cash => cash.PeriodId == periodId
                        && cash.CashierId == cashierId
                        && cash.CurrencyId == currencyId
                        && cash.TenderTypeId == tenderTypeId
                        && cash.TradingPeriodId == tradingPeriodId);
            }
        }

        public IList<CashBalCashierTenVar> GetCashierBalanceTenderVariances(int periodId, int cashierId, string currencyId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashierTenVar>()
                    .Where(cash => (cash.PeriodId == periodId || cash.TradingPeriodId == periodId)
                        && cash.CashierId == cashierId
                        && cash.CurrencyId == currencyId).ToList();
            }
        }

        private void CheckCashierBalanceTenderVariance(int tenderTypeId, int periodId, int tillId, string currencyId, int tradingPeriodId)
        {
            if (GetCashierBalanceTenderVariance(tenderTypeId, periodId, tillId, currencyId, tradingPeriodId) == null)
            {
                InsertEmptyCashierBalanceTenderVariance(tenderTypeId, periodId, tillId, currencyId, tradingPeriodId);
            }
        }

        private void InsertEmptyCashierBalanceTenderVariance(int tenderTypeId, int periodId, int cashierId, string currencyId, int tradingPeriodId)
        {
            using (var db = GetDb())
            {
                db.GetTable<CashBalCashierTenVar>().Insert(() => new CashBalCashierTenVar
                {
                    PeriodId = periodId,
                    CashierId = cashierId,
                    CurrencyId = currencyId,
                    TenderTypeId = tenderTypeId,
                    TradingPeriodId = tradingPeriodId
                });
            }
        }

        public void UpdateCashBalCashierTenVarForTest(int periodId, int cashierId, string currencyId, int tenderTypeId, int tradingPeriodId, decimal amount)
        {
            CheckCashierBalanceTenderVariance(tenderTypeId, periodId, cashierId, currencyId, tradingPeriodId);

            using (var db = GetDb())
            {
                db.GetTable<CashBalCashierTenVar>()
                    .Where(x => x.PeriodId == periodId && x.CurrencyId == currencyId && x.CashierId == cashierId && x.TenderTypeId == tenderTypeId && x.TradingPeriodId == periodId)
                    .Set(x => x.Quantity, 0)
                    .Set(x => x.Amount, amount)
                    .Update();
            }
        }

        public decimal GetCashBalCashierTenVarAmount(int periodId, int cashierId, string currencyId, int tenderTypeId, int tradingPeriodId)
        {
            using (var db = GetDb())
            {
                return db.GetTable<CashBalCashierTenVar>()
                    .Where(x => x.PeriodId == periodId && x.CurrencyId == currencyId && x.CashierId == cashierId && x.TenderTypeId == tenderTypeId && x.TradingPeriodId == periodId)
                    .Select(x => x.Amount)
                    .FirstOrDefault();
            }
        }

        #endregion
    }
}
