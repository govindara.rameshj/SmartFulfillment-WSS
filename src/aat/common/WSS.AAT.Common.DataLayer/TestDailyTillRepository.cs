extern alias bltoolkit;
using System;
using System.Linq;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Database;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Repositories;

namespace WSS.AAT.Common.DataLayer
{
    public class TestDailyTillRepository : DailyTillRepository
    {
        public Dltots SelectDailyTotal(String sourceId, String source)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Dltots>().FirstOrDefault(t => t.SourceTranId == sourceId && t.Source == source);
            }
        }

        public void CleanDltots()
        {
            using (var db = GetDb())
            {
                db.GetTable<Dltots>().Delete();
            }
        }

        public void RemoveDltots(string sourceTransactionId, string source)
        {
            using (var db = GetDb())
            {
                db.GetTable<Dltots>().Delete(x => x.SourceTranId == sourceTransactionId);
            }
        }

        private static void DeleteByTillNumbers<T>(OasysDb db, string tillNumber)
            where T : class, IDailyTillTranKey
        {
            db.GetTable<T>().Delete(x => x.TillNumber == tillNumber);
        }

        public void RemoveDailies(string tillNumber)
        {
            using (var db = GetDb())
            {
                DeleteByTillNumbers<Dltots>(db, tillNumber);
                DeleteByTillNumbers<DailyTillLine>(db, tillNumber);
                DeleteByTillNumbers<Dlpaid>(db, tillNumber);
                DeleteByTillNumbers<DailyCustomer>(db, tillNumber);
                DeleteByTillNumbers<Dlcomm>(db, tillNumber);
            }
        }

        public void RemoveDailies()
        {
            using (var db = GetDb())
            {
                db.GetTable<Dltots>().Delete();
                db.GetTable<DailyTillLine>().Delete();
                db.GetTable<Dlpaid>().Delete();
                db.GetTable<DailyCustomer>().Delete();
                db.GetTable<Dlcomm>().Delete();
            }
        }

        public string GetNumberOfLastTransaction(string tillNumber)
        {
            var systemNumbersLine = GetSystemNumbersLine(tillNumber);

            if (systemNumbersLine.NextNumber == systemNumbersLine.MinNumber)
            {
                return systemNumbersLine.MaxNumber.ToString();
            }
            return (systemNumbersLine.NextNumber - 1).ToString();
        }

        public string GetNumberForNextTransaction(string tillNumber)
        {
            return GetSystemNumbersLine(tillNumber).NextNumber.ToString();
        }

        public void SetNextTransactionNumber(string tillNumber, int transactionNumber)
        {
            // to create record in SystmNumbers table
            GenerateNextTransactionNumber(tillNumber);

            var id = GetSystemNumberId(tillNumber);

            using (var db = GetDb())
            {
                db.GetTable<SystemNumbers>().Where(x => x.Id == id).Set(x => x.NextNumber, transactionNumber).Update();
            }
        }

        private int GetSystemNumberId(string tillNumber)
        {
            return Int32.Parse(tillNumber) + 9000;
        }

        private SystemNumbers GetSystemNumbersLine(string tillNumber)
        {
            var id = GetSystemNumberId(tillNumber);

            using (var db = GetDb())
            {
                return db.GetTable<SystemNumbers>().FirstOrDefault(x => x.Id == id);
            }
        }

        public ActivityLog GetLastActivityLogRecord()
        {
            using (var db = GetDb())
            {
                var maxIndex = db.GetTable<ActivityLog>().Max(x => x.ID);
                return db.GetTable<ActivityLog>().First(x => x.ID == maxIndex);
            }
        }

        public void InsertIntoDltots(string transactionCode, decimal totalAmount, string cashierNumber, string tillNumber, short misc, DateTime createDate)
        {
            var dltots = new Dltots
            {
                TransactionCode = transactionCode,
                TotalSaleAmount = totalAmount,
                CashierNumber = cashierNumber,
                TillNumber = tillNumber,
                Misc = misc
            };
            Console.WriteLine(createDate);
            dltots.CreateDate = createDate;
            dltots.ReceivedDate = createDate;

            InsertIntoDltots(dltots);
        }

        public void UpdateVatRate1ForDltots(IDailyTillTranKey searchValues, decimal vatRate)
        {
            SelectEntities<Dltots>(searchValues).Set(x => x.VatRate1, vatRate).Update();
        }

        public void ClearSourceTranId(IDailyTillTranKey dailyTillTranKey)
        {
            using (var db = GetDb())
            {
                db.GetTable<Dltots>()
                    .Where(x => x.CreateDate == dailyTillTranKey.CreateDate &&
                                x.TillNumber == dailyTillTranKey.TillNumber &&
                                x.TransactionNumber == dailyTillTranKey.TransactionNumber)
                    .Set(x => x.SourceTranId, (string)null)
                    .Set(x => x.Source, (string)null)
                    .Update();
                db.GetTable<DailyTillLine>()
                    .Where(x => x.CreateDate == dailyTillTranKey.CreateDate &&
                                x.TillNumber == dailyTillTranKey.TillNumber &&
                                x.TransactionNumber == dailyTillTranKey.TransactionNumber)
                    .Set(x => x.SourceItemId, (string) null)
                    .Update();
            }
        }

        public void ClearDailyCustomer(IDailyTillTranKey dailyTillTranKey)
        {
            using (var db = GetDb())
            {
                db.GetTable<DailyCustomer>()
                    .Where(x => x.CreateDate == dailyTillTranKey.CreateDate &&
                                x.TillNumber == dailyTillTranKey.TillNumber &&
                                x.TransactionNumber == dailyTillTranKey.TransactionNumber)
                    .Set(x => x.OriginalLineNumber, (int?)null)
                    .Update();
            }
        }

        public void CreateEventMaster(string eventNumber, string eventType, string couponNumber)
        {
            var eventMaster = new EventMaster()
            {
                BuyCoupon = couponNumber,
                BuyQuantity = 0,
                EndDate = DateTime.Today,
                EventKey1 = "000000",
                EventKey2 = "00000000",
                EventNumber = eventNumber,
                EventType = eventType,
                Price = 10.00M,
                GetCoupon = "0000000",
                GetQuantity = 0,
                IsDayOfWeekEvent = false,
                IsDeleted = false,
                PercentageDiscount = 0,
                Priority = "30",
                StartDate = DateTime.Today,
                ValueDiscount = 0
            };
            InsertIntoEventMaster(eventMaster);
        }

        public void InsertStubDlline(DateTime date, string tillId, string tranId, string skuNumber, int quantity, bool isReversed = false, bool isMarkdown = false)
        {
            var dlline = new DailyTillLine()
            {
                CreateDate = date,
                TillNumber = tillId,
                TransactionNumber = tranId,
                SkuNumber = skuNumber,
                Quantity = quantity,
                IsReversed = isReversed,
                IsFromMarkDown = isMarkdown
            };

            InsertIntoDlline(dlline);
        }

        public void InsertStubDltots(DateTime date, string tillId, string tranId)
        {
            var dltots = new Dltots()
            {
                CreateDate = date,
                TillNumber = tillId,
                TransactionNumber = tranId,
                ReceivedDate = date,
                CashierNumber = "01"
            };

            InsertIntoDltots(dltots);
        }

        public void UpdateDailyLine(IDailyTillTranKey searchValues, string skuNumber, string priceOverrideReasonCode)
        {
            using (var db = GetDb())
            {
                db.GetTable<DailyTillLine>()
                    .Where(x => x.CreateDate == searchValues.CreateDate && x.TillNumber == searchValues.TillNumber && x.TransactionNumber == searchValues.TransactionNumber && x.SkuNumber == skuNumber)
                    .Set(x => x.PriceOverrideCode, Convert.ToInt16(priceOverrideReasonCode)).Update();
            }
        }

        public void UpdateDailyCustomer(IDailyTillTranKey searchValues, string refundReasonCode)
        {
            using (var db = GetDb())
            {
                db.GetTable<DailyCustomer>()
                    .Where(x => x.CreateDate == searchValues.CreateDate && x.TillNumber == searchValues.TillNumber && x.TransactionNumber == searchValues.TransactionNumber)
                    .Set(x => x.RefundReasonCode, refundReasonCode).Update();
            }
        }

        public void CleanDailyGiftCard()
        {
            using (var db = GetDb())
            {
                db.GetTable<DailyGiftCard>().Delete();
            }
        }

        public DailyGiftCard SelectDailyGiftCard()
        {
            using (var db = GetDb())
            {
                return db.GetTable<DailyGiftCard>().Single();
            }
        }

        /// <summary>
        /// Gets transaction number for the next transaction for given date and till number
        /// </summary>
        /// <param name="date">Transaction date</param>
        /// <param name="till">Transaction till number</param>
        /// <returns>Number of next transaction</returns>
        public string GetNextSourceTransactionId(DateTime date, string till)
        {
            string result;
            using (var db = GetDb())
            {
                var maxTran =
                    db.GetTable<Dltots>().Where(
                        dltots => dltots.CreateDate == date.Date && dltots.TillNumber == till)
                        .Select(dltots => dltots.SourceTranNumber)
                        .Max();

                result = String.IsNullOrEmpty(maxTran) ? "0001" : (Convert.ToInt32(maxTran) + 1).ToString("d4");
            }
            return result;
        }
    }
}