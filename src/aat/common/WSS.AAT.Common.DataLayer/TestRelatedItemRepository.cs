﻿extern alias bltoolkit;
using System.Collections.Generic;
using WSS.BO.DataLayer.Model.Entities.NonPersistent;
using WSS.BO.DataLayer.Repositories;
using bltoolkit::BLToolkit.Data.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Utility;
using System.Linq;
using System;
using bltoolkit::BLToolkit.Data;

namespace WSS.AAT.Common.DataLayer
{
    public class TestRelatedItemRepository : RelatedItemRepository
    {
        private const int WickesCOMMSPathParameterId = 914;

        public IEnumerable<SimplifiedRelatedItem> GetRelatedItemsRequiringAdjustments()
        {
            using (var db = GetDb())
            {
                return db.SetSpCommand("usp_GetRelatedItemsRequiringAdjustments").ExecuteList<SimplifiedRelatedItem>();
            }
        }

        public int StockMovementForSku(string sku)
        {
            using (var db = GetDb())
            {
                return db.GetTable<Stklog>().Where(x => x.ItemNumber == sku).Select(x => (int)(x.EndingStockOnHand - x.StartingStockOnHand)).FirstOrDefault();
            }
        }

        public DateTime SystemDate()
        {
            using (var db = GetDb())
            {
                return db.GetTable<SystemDate>().Select(x => x.SysDate).FirstOrDefault();
            }
        }

        public string LoadSthotFile()
        {
            using (var db = GetDb())
            {
                return db.GetTable<Parameter>().Where(x => x.Id == WickesCOMMSPathParameterId).Select(x => x.StringValue).FirstOrDefault();
            }
        }

        public void AdjustStockForPackSplitForSkuWithQuantityOfValue(int sku, int quantity, decimal value)
        {
            using (var db = GetDb())
            {
                var parameters = new object[]
                {
                    db.Parameter("@Skun", sku),
                    db.Parameter("@Quantity", quantity),
                    db.Parameter("@Value", value)
                };

                db.SetSpCommand("usp_AdjustStockForPackSplit", parameters).ExecuteNonQuery();
            }
        }

        public void AdjustUnitsSoldInCurrentWeekForSkuWithQuantity(int sku, int quantity)
        {
            using (var db = GetDb())
            {
                var parameters = new object[]
                {
                    db.Parameter("@Skun", sku),
                    db.Parameter("@Quantity", quantity)
                };

                db.SetSpCommand("usp_AdjustUnitsSoldInCurrentWeek", parameters).ExecuteNonQuery();
            }
        }

        public void AdjustRelatedItemsMarkupForSkuWithValue(int sku, decimal value)
        {
            using (var db = GetDb())
            {
                var parameters = new object[]
                {
                    db.Parameter("@Skun", sku),
                    db.Parameter("@Value", value)
                };

                db.SetSpCommand("usp_AdjustRelatedItemsMarkup", parameters).ExecuteNonQuery();
            }
        }

        public void AdjustSinglesSoldNotUpdatedYetForSku(int sku)
        {
            using (var db = GetDb())
            {
                var parameters = new object[]
                {
                    db.Parameter("@Skun", sku)
                };

                db.SetSpCommand("usp_AdjustSinglesSoldNotUpdatedYet", parameters).ExecuteNonQuery();
            }
        }

        #region STKMAS selects

        public int BulkToSingleTransferQuantityForSku(string sku)
        {
            string query = String.Format("SELECT [MBSQ1] FROM [STKMAS] WHERE [SKUN]='{0}'", sku);

            using (var db = GetDb())
            {
                return db.SetCommand(query).ExecuteScalar<int>();
            }
        }

        public decimal BulkToSingleTransferValueForSku(string sku)
        {
            string query = String.Format("SELECT [MBSV1] FROM [STKMAS] WHERE [SKUN]='{0}'", sku);

            using (var db = GetDb())
            {
                return db.SetCommand(query).ExecuteScalar<decimal>();
            }
        }

        public int UnitsSoldInCurrentWeekForSku(string sku)
        {
            string query = String.Format("SELECT [US001] FROM [STKMAS] WHERE [SKUN]='{0}'", sku);

            using (var db = GetDb())
            {
                return db.SetCommand(query).ExecuteScalar<int>();
            }
        }

        public int UnitsOnHandForSku(string sku)
        {
            using (var db = GetDb())
            {
                return db.GetTable<StockMaster>().Where(x => x.SkuNumber == sku).Select(x => (int) x.StockOnHand).FirstOrDefault();
            }
        }

        #endregion

        #region Selects from RELITM

        public int ThisWeekUnitsTransfered(string sku)
        {
            using (var db = GetDb())
            {
                return db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == sku).Select(x => (int)x.ThisWeekUnitsTransfered).FirstOrDefault();
            }
        }

        public decimal WeekToDateMarkup(string sku)
        {
            using (var db = GetDb())
            {
                return db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == sku).Select(x => x.ThisWeekMarkup).FirstOrDefault();
            }
        }

        public decimal PeriodToDateMarkup(string sku)
        {
            using (var db = GetDb())
            {
                return db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == sku).Select(x => x.PeriodToDateMarkup).FirstOrDefault();
            }
        }

        public decimal YearToDateMarkup(string sku)
        {
            using (var db = GetDb())
            {
                return db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == sku).Select(x => x.YearToDateMarkup).FirstOrDefault();
            }
        }

        public int NumberOfSinglesPerPack(string singleSku, string packSku)
        {
            using (var db = GetDb())
            {
                return db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == singleSku && x.PackageItemNumber == packSku).Select(x => (int) x.NumberOfSinglesPerPack).FirstOrDefault();
            }
        }

        public int SinglesSoldNotUpdatedYetValue(string sku)
        {
            using (var db = GetDb())
            {
                return db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == sku).Select(x => (int) x.NumberOfSinglesSoldNotUpdated).FirstOrDefault();
            }
        }

        #endregion

        #region RELITM updates
        
        public void ReverseAdjustmentOfSinglesSoldNotUpdatedYetForSku(int sku)
        {
            using (var db = GetDb())
            {
                var numberOfSinglesPerPack = db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == sku.ToString()).Select(x => x.NumberOfSinglesPerPack).FirstOrDefault();

                if (numberOfSinglesPerPack != 0)
                {
                    db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == sku.ToString())
                        .Set(x => x.ThisWeekUnitsTransfered, -numberOfSinglesPerPack)
                        .Update();
                }
            }
        }

        public void ReverseAdjustmentOfRelatedItemsMarkupForSkuWithValue(int sku, decimal value)
        {
            using (var db = GetDb())
            {
                db.GetTable<RelatedItem>().Where(x => x.SingleItemNumber == sku.ToString())
                    .SetAdd(x => x.ThisWeekUnitsTransfered, -value)
                    .SetAdd(x => x.ThisWeekMarkup, -value)
                    .SetAdd(x => x.PeriodToDateMarkup, -value)
                    .SetAdd(x => x.YearToDateMarkup, -value)
                    .Update();
            }
        }

        #endregion
    }
}
