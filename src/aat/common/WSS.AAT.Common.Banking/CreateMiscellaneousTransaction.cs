using System;
using System.Globalization;
using System.Text;
using System.Data.SqlClient;
using Cts.Oasys.Core.SystemEnvironment;

namespace WSS.AAT.Common.Banking
{
    // TODO refactor to use IDataLayerFacade
    public class CreateMiscellaneousTransaction
    {
        private const string Currencyid = "GBP";
        private ISystemEnvironment systemEnvironment;

        public enum TenderTypes
        {
            TenderTypeCash = 1,
            TenderTypeCheque = 2
        }

        public CreateMiscellaneousTransaction(ISystemEnvironment systemEnvironment)
        {
            this.systemEnvironment = systemEnvironment;
        }

        internal void Save(int bankingPeriodId, string forCashier, decimal value, string invoiceNumber,
                           string description, string salesTranType, string salesTranactionNumber, string accountType,
                           string accountSlotNumber)
        {
            string dateToday = DateTime.Today.ToString("yyyy-MM-dd");
            string cashierId = getEmployeeNumber(forCashier).ToString(CultureInfo.InvariantCulture);

            insertSalesHeader(dateToday, "11", salesTranactionNumber, cashierId, value, invoiceNumber, description,
                              salesTranType);
            insertSalesPayment(dateToday, "11", salesTranactionNumber, -value);
            updateCashBalCashierRecord(bankingPeriodId, Int32.Parse(cashierId), value, accountType, accountSlotNumber);
        }

        private int getEmployeeNumber(string cashierName)
        {
            int number;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                var employeeNumber = dbConnection.CreateCommand();
                employeeNumber.CommandText = "select [EmployeeCode] from [SystemUsers] where [Name]='" + cashierName +
                                             "'";

                number = int.Parse(employeeNumber.ExecuteScalar().ToString());
            }

            return number;
        }

        private void insertSalesHeader(string dateToday, string tillId, string transactionNumber, string cashierId,
                                       decimal value, string invoiceNumber, string description, string salesTranType)
        {
            string sql = string.Format(
                "INSERT INTO [dbo].[DLTOTS] ([DATE1], [TILL], [TRAN], [CASH], [TIME], [SUPV], [TCOD], [OPEN], [MISC], [DESCR], [ORDN], [ACCT], [VOID], [VSUP], [TMOD], " +
                "[PROC], [DOCN], [SUSE], [STOR], [MERC], [NMER], [TAXA], [DISC], [DSUP], [TOTL], [ACCN], [CARD], [AUPD], [BACK], [ICOM], [IEMP], [RCAS], [RSUP], [VATR1], " +
                "[VATR2], [VATR3], [VATR4], [VATR5], [VATR6], [VATR7], [VATR8], [VATR9], [VSYM1], [VSYM2], [VSYM3], [VSYM4], [VSYM5], [VSYM6], [VSYM7], [VSYM8], [VSYM9], " +
                "[XVAT1], [XVAT2], [XVAT3], [XVAT4], [XVAT5], [XVAT6], [XVAT7], [XVAT8], [XVAT9], [VATV1], [VATV2], [VATV3], [VATV4], [VATV5], [VATV6], [VATV7], [VATV8], [VATV9], " +
                "[PARK], [RMAN], [TOCD], [PKRC], [REMO], [GTPN], [CCRD], [SSTA], [SSEQ], [CBBU], [CARD_NO], [RTI]) VALUES " +
                "('{0}', '{1}', '{2}', '{3}', '153333', '044', '{4}', 0, 1, '{5}', '000000', 0, 0, '000', 0, " +
                "0, '{6}', 1, '000', 0.00, {7}, 0.00, 0.00, '000', {8}, '000000', '000000', 0, 0, 1, 0, '000', '000', 20.000, " +
                "0.000, 5.000, 0.000, 0.000, 0.000, 17.500, 15.000, 17.500, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', " +
                "0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, " +
                "0, '000', '00', 0, 1, 0, '000000000', '99', '    ', NULL, '                   ', 'S')", dateToday,
                tillId, transactionNumber, cashierId, salesTranType, description, invoiceNumber, value, value);
            executeSql(sql);
        }

        private void insertSalesPayment(string dateToday, string tillId, string transactionNumber, decimal value)
        {
            string sql = string.Format(
                "INSERT INTO [dbo].[DLPAID] ([DATE1], [TILL], [TRAN], [NUMB], [TYPE], [AMNT], [CARD], [EXDT], [COPN], [CLAS], [AUTH], " +
                "[CKEY], [SUPV], [POST], [CKAC], [CKSC], [CKNO], [DBRF], [SEQN], [ISSU], [ATYP], [MERC], [CBAM], [DIGC], [ECOM], [CTYP], " +
                "[EFID], [EFTC], [STDT], [AUTHDESC], [SECCODE], [TENC], [MPOW], [MRAT], [TENV], [RTI], [SPARE]) VALUES " +
                "('{0}', '{1}', '{2}', 1, 1, {3}, '0000000000000000000', '0000', '000000', '00', '         ', " +
                "0, '000', '        ', '0000000000', '000000', '000000', 0, '0000', '  ', ' ', '               ', 0.00, 0, 1, " +
                "'', '      ', ' ', '0000', ' ', '   ', '   ', 0, 0.00000, 0.00, 'S',  ' ')", dateToday, tillId,
                transactionNumber, value);
            executeSql(sql);
        }

        private void updateCashBalCashierRecord(int periodId, int employeeNumber, decimal value, string accountType,
                           string accountSlotNumber)
        {

            var sqlUpdateStatement = new StringBuilder("UPDATE [CashBalCashier] SET ");

            switch (accountType)
            {
                case "MiscOut":
                    sqlUpdateStatement.Append("MisOutValue" + accountSlotNumber + " = " + "MisOutValue" + accountSlotNumber + "+" +
                               value);
                    sqlUpdateStatement.Append(", MisOutCount" + accountSlotNumber + " = " + "MisOutCount" + accountSlotNumber +
                               (value > 0 ? " - 1" : " + 1"));
                    break;
                case "MiscInc":
                    sqlUpdateStatement.Append("MiscIncomeValue" + accountSlotNumber + " = " + "MiscIncomeValue" + accountSlotNumber + "+" +
                               value);
                    sqlUpdateStatement.Append(", MiscIncomeCount" + accountSlotNumber + " = " + "MiscIncomeCount" + accountSlotNumber +
                               (value > 0 ? " + 1" : " - 1"));
                    break;
            }
            sqlUpdateStatement.Append(string.Format(" WHERE PeriodID={0} AND CurrencyID='{1}' AND CashierID={2}", periodId, Currencyid, employeeNumber));

            executeSql(sqlUpdateStatement.ToString());
        }

        private void executeSql(string sql)
        {
            using (var con = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                con.Open();
                var cmd = con.CreateCommand();
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
        }
    }
}
