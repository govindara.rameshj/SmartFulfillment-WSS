using System;
using System.Data;
using System.Linq;
using WSS.BO.DataLayer.Model.Entities;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Repositories;
using WSS.AAT.Common.DataLayer;

namespace WSS.AAT.Common.Banking
{
    public class CashBalance
    {
        #region Private fields

        private CashBalCashier _cbCashier;
        private CashBalCashierTen _cbCashierTen;
        private CashBalCashierTenVar _cbCashierTenVar;
        private CashBalCashierTenVar _cbCashierTenVarRelated;

        private int _periodId;
        private int _tradingPeriodId;

        #endregion

        public CashBalance(IDataLayerFactory dalFactory, string cashier, string till, DateTime receivedDate, DateTime saleDate)
        {
            var dictRepo = dalFactory.Create<DictionariesRepository>();
            var currency = dictRepo.GetDefaultSystemCurrency();
            
            var miscRepo = dalFactory.Create<TestMiscRepository>(); 
            _periodId = miscRepo.GetPeriodId(receivedDate);
            _tradingPeriodId = miscRepo.GetPeriodId(saleDate);

            var cashierRepo = dalFactory.Create<TestCashierBalanceRepository>();
            var cashierId = Convert.ToInt32(cashier);
            _cbCashier = cashierRepo.GetCashierBalance(_periodId, cashierId, currency);
            _cbCashierTen = cashierRepo.GetCashierBalanceTenders(_periodId, cashierId, currency).FirstOrDefault();
            var cbCashierTenVars = cashierRepo.GetCashierBalanceTenderVariances(_periodId, cashierId, currency);
            _cbCashierTenVar = cbCashierTenVars.FirstOrDefault(variance => variance.PeriodId == variance.TradingPeriodId);
            _cbCashierTenVarRelated = cbCashierTenVars.FirstOrDefault(variance => variance.PeriodId != variance.TradingPeriodId);
        }

        #region Cashier

        public bool CashierExists()
        {
            return _cbCashier != null;
        }

        public decimal CashierGrossSalesAmount()
        {
            return (CashierExists() ? _cbCashier.GrossSalesAmount : 0M);
        }

        public decimal CashierSalesCount()
        {
            return (CashierExists() ? _cbCashier.SalesCount : 0M);
        }

        public decimal CashierSalesAmount()
        {
            return (CashierExists() ? _cbCashier.SalesAmount : 0M);
        }

        public decimal CashierRefundCount()
        {
            return (CashierExists() ? _cbCashier.RefundCount : 0M);
        }

        public decimal CashierRefundAmount()
        {
            return (CashierExists() ? _cbCashier.RefundAmount : 0M);
        }

        public int CashierNumTransactions()
        {
            return (CashierExists() ? _cbCashier.NumTransactions : 0);
        }

        #endregion

        #region CashierTen

        public bool CashierTenExists()
        {
            return _cbCashierTen != null;
        }

        public decimal CashierTenQuantity()
        {
            return (CashierTenExists() ? _cbCashierTen.Quantity : 0M);
        }
        public decimal CashierTenAmount()
        {
            return (CashierTenExists() ? _cbCashierTen.Amount : 0M);
        }

        #endregion

        #region CashierTenVar

        public bool CashierTenVarExists()
        {
            return _cbCashierTenVar != null;
        }

        public bool CashierTenVarTradingPeriodCorrect()
        {
            return _cbCashierTenVar != null && _cbCashierTenVar.TradingPeriodId == _periodId;
        }

        public decimal CashierTenVarQuantity()
        {
            return (CashierTenVarExists() ? _cbCashierTenVar.Quantity : 0M);
        }
        public decimal CashierTenVarAmount()
        {
            return (CashierTenVarExists() ? _cbCashierTenVar.Amount : 0M);
        }

        #endregion

        #region CashierTenVarRelated

        public bool CashierTenVarRelatedExists()
        {
            return _cbCashierTenVarRelated != null;
        }

        public bool CashierTenVarRelatedPeriodCorrect()
        {
            return _cbCashierTenVarRelated != null && _cbCashierTenVarRelated.PeriodId == _tradingPeriodId;
        }

        public decimal CashierTenVarRelatedQuantity()
        {
            return (CashierTenVarRelatedExists() ? _cbCashierTenVarRelated.Quantity : 0M);
        }
        public decimal CashierTenVarRelatedAmount()
        {
            return (CashierTenVarRelatedExists() ? _cbCashierTenVarRelated.Amount : 0M);
        }

        #endregion
    }
}
