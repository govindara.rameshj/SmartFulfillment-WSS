using System;
using System.Linq;
using System.Data.SqlClient;
using Cts.Oasys.Core.SystemEnvironment;

namespace WSS.AAT.Common.Banking
{
    // TODO: Refactor to use IDataLayerFacade
    public class CreateSale
    {
        private const string Currencyid = "GBP";
        private ISystemEnvironment systemEnvironment;

        public enum TenderTypes
        {
            TenderTypeCash = 1,
            TenderTypeCheque = 2,
            TenderTypeGiftCard = 13
        }

        public CreateSale(ISystemEnvironment systemEnvironment)
        {
            this.systemEnvironment = systemEnvironment;
        }

        internal void Save(int bankingPeriodId, string forCashier, string withBasketValues, int tenderType)
        {
            var basketValues = withBasketValues.Split(',');
            var salesCount = basketValues.Length;

            var totalBasketValue = basketValues.Sum(basketValue => Int32.Parse(basketValue));
            var cashierId = GetEmployeeNumber(forCashier);

            UpdateCashBalCashierRecord(bankingPeriodId, cashierId, totalBasketValue, salesCount);
            InsertCashBalCashierTenRecord(bankingPeriodId, cashierId, totalBasketValue, salesCount, tenderType);
            InsertCashBalCashierTenVarRecord(bankingPeriodId, cashierId, totalBasketValue, salesCount, tenderType);

            if (tenderType == (Int32)TenderTypes.TenderTypeGiftCard)
            {
                InsertDltotsRecord(cashierId, "01", "0001");
                InsertDllineRecord("01", "0001");
                InsertDlpaidRecord("01", "0001");
            }
        }

        private void InsertDltotsRecord(int cashierId, string tillId, string transactionNumber)
        {
            string sql = string.Format(
                "INSERT INTO [dbo].[DLTOTS] ([DATE1], [TILL], [TRAN], [CASH], [TIME], [SUPV], [TCOD], [OPEN], [MISC], [DESCR], [ORDN], [ACCT], [VOID], [VSUP], [TMOD], " +
                "[PROC], [DOCN], [SUSE], [STOR], [MERC], [NMER], [TAXA], [DISC], [DSUP], [TOTL], [ACCN], [CARD], [AUPD], [BACK], [ICOM], [IEMP], [RCAS], [RSUP], [VATR1], " +
                "[VATR2], [VATR3], [VATR4], [VATR5], [VATR6], [VATR7], [VATR8], [VATR9], [VSYM1], [VSYM2], [VSYM3], [VSYM4], [VSYM5], [VSYM6], [VSYM7], [VSYM8], [VSYM9], " +
                "[XVAT1], [XVAT2], [XVAT3], [XVAT4], [XVAT5], [XVAT6], [XVAT7], [XVAT8], [XVAT9], [VATV1], [VATV2], [VATV3], [VATV4], [VATV5], [VATV6], [VATV7], [VATV8], [VATV9], " +
                "[PARK], [RMAN], [TOCD], [PKRC], [REMO], [GTPN], [CCRD], [SSTA], [SSEQ], [CBBU], [CARD_NO], [RTI]) VALUES " +
                "('{0}', '{1}', '{2}', '{3}', '153333', '044', 'SA', 0, 1, '', '000000', 0, 0, '000', 0, " +
                "0, '', 1, '000', 0.00, 0.00, 0.00, 0.00, '000', 34.99, '000000', '000000', 0, 0, 1, 0, '000', '000', 20.000, " +
                "0.000, 5.000, 0.000, 0.000, 0.000, 17.500, 15.000, 17.500, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', " +
                "0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, " +
                "0, '000', '00', 0, 1, 0, '000000000', '99', '    ', NULL, '                   ', 'S')", DateTime.Today.ToString("yyyy-MM-dd"), tillId, transactionNumber, cashierId.ToString("000"));
            ExecuteSql(sql);
        }

        private void InsertDllineRecord(string tillId, string transactionNumber)
        {
            var sql =
                string.Format("INSERT INTO [DLLINE] ([DATE1], [TILL], [TRAN], [NUMB], [SKUN], [DEPT], [IBAR], [QUAN], [PRIC], [EXTP] " +
                              ", [EXTC], [SPRI], [VSYM], [RITM], [CATA], [LREV], [PORC], [SUPV], [PRVE], [POME], [ESEQ], [CTGY], [GRUP] " +
                              ", [SGRP], [STYL], [QSUP], [ESEV], [IMDN], [SALT], [VATN], [POPD], [ITAG], [TPPD], [TPME], [QBPD], [QBME] " +
                              ", [DGPD], [DGME], [MBPD], [MBME], [HSPD], [HSME], [ESPD], [ESME], [VATV], [BDCO], [BDCOInd], [RCOD], [WEERATE], [WEESEQN], [WEECOST]) VALUES" +
                              "('{0}', '{1}', '{2}', 1,'500300','00', 0, 1, 34.99, 34.99, 0, 34.99, 'a', 0, 0, 0, 0, '000', " +
                              "0, '000000', '000000', '000016', '000707', '000732', '000742', '000', 0, 0, 'A', 1, 0, 0, 0, '000000' " +
                              ", 0, '000000', 0, '000000', 0, '000000', 0, '000000', 0, '000000', 5.83, 'N', 0, '', '00', '000', 0)", DateTime.Today.ToString("yyyy-MM-dd"), tillId, transactionNumber);
            ExecuteSql(sql);
        }

        private void InsertDlpaidRecord(string tillId, string transactionNumber)
        {
            string sql = string.Format(
                "INSERT INTO [dbo].[DLPAID] ([DATE1], [TILL], [TRAN], [NUMB], [TYPE], [AMNT], [CARD], [EXDT], [COPN], [CLAS], [AUTH], " +
                "[CKEY], [SUPV], [POST], [CKAC], [CKSC], [CKNO], [DBRF], [SEQN], [ISSU], [ATYP], [MERC], [CBAM], [DIGC], [ECOM], [CTYP], " +
                "[EFID], [EFTC], [STDT], [AUTHDESC], [SECCODE], [TENC], [MPOW], [MRAT], [TENV], [RTI], [SPARE]) VALUES " +
                "('{0}', '{1}', '{2}', 1, '{3}', 34.99, '0000000000000000000', '0000', '000000', '00', '         ', " +
                "0, '000', '        ', '0000000000', '000000', '000000', 0, '0000', '  ', ' ', '               ', 0.00, 0, 1, " +
                "'', '      ', ' ', '0000', ' ', '   ', '   ', 0, 0.00000, 0.00, 'S',  ' ')", DateTime.Today.ToString("yyyy-MM-dd"), tillId, transactionNumber, (Int32)TenderTypes.TenderTypeGiftCard);
            ExecuteSql(sql);
        }

        private int GetEmployeeNumber(string cashierName)
        {
            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                var employeeNumber = dbConnection.CreateCommand();
                employeeNumber.CommandText = "select [EmployeeCode] from [SystemUsers] where [Name]='" + cashierName + "'";

                return  int.Parse(employeeNumber.ExecuteScalar().ToString());
            }
        }

        private void InsertCashBalCashierTenVarRecord(int periodId, int cashierId, decimal withBasketValue, int salesCount, int tenderType)
        {
            var sql =
                string.Format("INSERT INTO [CashBalCashierTenVar] (PeriodID,TradingPeriodID,CashierID,CurrencyID,ID,Quantity,Amount,PickUp)" +
                              "Values ({0}, {0}, {1}, '{2}', {3}, {4}, {5}, 0)", periodId, cashierId, Currencyid, tenderType, salesCount, withBasketValue);
            ExecuteSql(sql);
        }

        private void InsertCashBalCashierTenRecord(int periodId, int cashierId, decimal withBasketValue, int salesCount, int tenderType)
        {
            var sql =
                string.Format("INSERT INTO [CashBalCashierTen] (PeriodID,CashierID,CurrencyID,ID,Quantity,Amount,PickUp)" +
                              "Values ({0}, {1}, '{2}', {3}, {4}, {5}, 0)", periodId, cashierId, Currencyid, tenderType, salesCount, withBasketValue);
            ExecuteSql(sql);
        }

        private void UpdateCashBalCashierRecord(int periodId, int employeeNumber, decimal withBasketValue, int salesCount)
        {
            var updateSql = string.Format("UPDATE [CashBalCashier] SET [GrossSalesAmount] = {0}, [SalesCount] ={1}, [SalesAmount] = {0}, " +
                                          "[NumTransactions] ={1}, [NumLinesSold] = {1} " +
                                          "WHERE [PeriodID] = {2} AND [CurrencyID] = '{3}' AND [CashierID] = {4}",
                                          withBasketValue, salesCount, periodId, Currencyid, employeeNumber);

            ExecuteSql(updateSql);
        }

        private void ExecuteSql(string sql)
        {
            using (var con = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                con.Open();
                var cmd = con.CreateCommand();
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
        }
    }
}
