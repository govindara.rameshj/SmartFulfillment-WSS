﻿using FarPoint.Win.Spread;
using NewBanking.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.AAT.Common.Banking
{
    public class BankingBagCollectionReport
    {
        NewBanking.Core.BankingBagCollection data = new NewBanking.Core.BankingBagCollection();
        FpSpread spread;
        string collectionSlip;
        string comment;

        public void addItemForPeriodAndDateWithBagSealNumberAndBagValueWithComment(
            int forPeriod, DateTime andDate, string withBagSealNumber, decimal andBagValue, string withComment)
        {
            var bag = new NewBanking.Core.BankingBag()
            {
                BagPeriodID = forPeriod,
                BagPeriodDate = andDate,
                BagSealNumber = withBagSealNumber,
                BagValue = andBagValue,
                BagComment = withComment
            };

            data.Add(bag);
        }

        public void setCollectionSlip(string collectionSlip)
        {
            this.collectionSlip = collectionSlip;
        }

        public void setComment(string comment)
        {
            this.comment = comment;
        }

        public void prepareReport()
        {
            spread = BankingBagCollection.CreateReportForPrint(data, n => (n % 2) == 0, // All even rows are checked
                collectionSlip, comment, "123", "Test Store");
        }

        public int getNumberOfRows()
        {
            return spread.Sheets[0].Rows.Count;
        }

        public bool getCheckBoxValueForRow(int forRow)
        {
            return (bool) spread.Sheets[0].Cells[forRow, 0].Value;
        }

        public string getValueForRowNumberColumnNumber(int rowNumber, int columnNumber)
        {
            return spread.Sheets[0].Cells[rowNumber, columnNumber].Value.ToString();
        }

        public string getComment()
        {
            return getValueForRowNumberColumnNumber(5, 1);
        }

        public string getCollectionSlip()
        {
            return getValueForRowNumberColumnNumber(6, 1);
        }
    }
}
