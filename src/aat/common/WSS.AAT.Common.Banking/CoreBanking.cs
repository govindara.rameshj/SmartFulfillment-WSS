using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Banking.Core;
using NewBanking.Form;
using WSS.AAT.Common.Utility.Executables;
using WSS.AAT.Common.Utility.Utility;
using Cts.Oasys.Core.SystemEnvironment;
using NewBanking.Core;
using Pickup = Banking.Core.Pickup;
using BagStates = Banking.Core.BagStates;
using FarPoint.Win.Spread;
using System.Reflection;
using System.Diagnostics;
using Cts.Oasys.Core.Tests;
using RefundsList;

namespace WSS.AAT.Common.Banking
{
    // TODO: Refactor to use IDataLayerFacade
    public class CoreBanking
    {
        public enum TenderTypes
        {
            TenderTypeCash = 1,
            TenderTypeCheque = 2,
            TenderTypeGiftCard = 13
        }

        public CoreBanking(ISystemEnvironment systemEnvironment)
        {
            this.systemEnvironment = systemEnvironment;
        }

        //public CoreBanking(ISystemEvironment )

        #region "Internal Variables"

        private const string CurrencyId = "GBP";
        private const int StartingSalesTranactionNumber = 5000;

        private int _bankingPeriodId;
        private int _loggedOnUser;
        private int _authoriser;

        ISystemEnvironment systemEnvironment;

        #endregion

        #region "Setters"

        public void setLoggedOnUserAndAuthoriser(int loggedOnUser, int andAuthoriser)
        {
            _loggedOnUser = loggedOnUser;
            _authoriser = andAuthoriser;
        }

        public void setLoggedOnUserAndAuthoriser(string loggedOnUser, string andAuthoriser)
        {
            _loggedOnUser = getUserIdForName(loggedOnUser);
            _authoriser = getUserIdForName(andAuthoriser);
        }

        public void setBankingDate(DateTime date)
        {
            _bankingPeriodId = getPeriodIdForDate(date);
        }

        #endregion

        public void createNonTradingDay()
        {
            var createInitialSafeOnDateCommand = new StringBuilder();

            int periodIdForDate = getPeriodIdForDate(DateTime.Today);

            createInitialSafeOnDateCommand.Append("Insert Into NonTradingDay (SystemPeriodID) Values(");
            createInitialSafeOnDateCommand.Append(periodIdForDate.ToString(CultureInfo.InvariantCulture));
            createInitialSafeOnDateCommand.Append(")");

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand createInitialSafe = dbConnection.CreateCommand();
                createInitialSafe.CommandText = createInitialSafeOnDateCommand.ToString();

                createInitialSafe.ExecuteNonQuery();
            }
        }

        public void advanceOneDay()
        {
            var advanceOneDayCommand = new StringBuilder();

            advanceOneDayCommand.Append("DECLARE @table nvarchar(max)\n");

            advanceOneDayCommand.Append("DECLARE table_cursor CURSOR FOR \n");
            advanceOneDayCommand.Append(
                "SELECT 'UPDATE ['+C.TABLE_NAME+'] SET ['+C.COLUMN_NAME+']=DATEADD(d,-1,['+C.COLUMN_NAME+']) WHERE ['+C.COLUMN_NAME+'] IS NOT NULL;'\n");
            advanceOneDayCommand.Append("FROM INFORMATION_SCHEMA.COLUMNS C\n");
            advanceOneDayCommand.Append(
                "JOIN INFORMATION_SCHEMA.TABLES T ON T.TABLE_SCHEMA = C.TABLE_SCHEMA AND T.TABLE_NAME = C.TABLE_NAME AND T.TABLE_CATALOG = C.TABLE_CATALOG\n");
            advanceOneDayCommand.Append("WHERE C.DATA_TYPE IN ('date','datetime')\n");
            advanceOneDayCommand.Append("AND C.TABLE_NAME NOT IN ('SystemUsers','SecurityProfile')\n");
            // These tables have triggers that cause issues
            advanceOneDayCommand.Append("AND T.TABLE_TYPE = 'BASE TABLE'\n");

            advanceOneDayCommand.Append("OPEN table_cursor\n");
            advanceOneDayCommand.Append("FETCH NEXT FROM table_cursor INTO @table\n");

            advanceOneDayCommand.Append("WHILE @@FETCH_STATUS = 0\n");
            advanceOneDayCommand.Append("BEGIN\n");
            advanceOneDayCommand.Append("    EXECUTE sp_executesql @table\n");
            advanceOneDayCommand.Append("    FETCH NEXT FROM table_cursor INTO @table\n");
            advanceOneDayCommand.Append("END\n");

            advanceOneDayCommand.Append("CLOSE table_cursor\n");
            advanceOneDayCommand.Append("DEALLOCATE table_cursor");

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand advanceOneDay = dbConnection.CreateCommand();
                advanceOneDay.CommandText = advanceOneDayCommand.ToString();
                advanceOneDay.ExecuteNonQuery();
            }
        }

        public void createInitialSafeWithValue(decimal withValue)
        {
            createInitialSafe();
            setSafeValue(withValue);
        }

        public void createInitialSafeWithValueAndDate(decimal withValue, DateTime date)
        {
            createInitialSafeForDate(date);
            setSafeValue(withValue, date);
        }

        public void createInitialSafeWithThisValueStartingOnPeriod(decimal withThisValue, int startingOnPeriod)
        {
            createInitialSafeWithThisPeriodId(startingOnPeriod);
            setSafeValue(withThisValue);
        }

        public void configureSafeBagsWithInitialIdentitySeed(int withInitialIdentitySeed)
        {
            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();
                command.CommandText = "dbcc checkident (SafeBags, reseed, " + withInitialIdentitySeed.ToString(CultureInfo.InvariantCulture) + ")";
                command.ExecuteNonQuery();
            }
        }

        public void createSaleForCashierWithBasketValue(string forCashier, string withBasketValue)
        {
            // date will always be today
            var sale = new CreateSale(systemEnvironment);
            sale.Save(_bankingPeriodId, forCashier, withBasketValue, (Int32)TenderTypes.TenderTypeCash);
        }

        public void createSaleForCashierPayedByGiftCard(string forCashier, int tenderType)
        {
            // date will always be today
            var sale = new CreateSale(systemEnvironment);
            sale.Save(_bankingPeriodId, forCashier, "0", (Int32)TenderTypes.TenderTypeGiftCard);
        }

        public List<int> GetActiveUserIds()
        {
            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();
                command.CommandText = "select ID from SystemUsers where IsDeleted = 0";

                var list = new List<int>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add((int)reader["ID"]);
                    }
                }
                return list;
            }
        }

        #region "Miscellaneous Income And Outcome Transactions"

        public void createPaidOutForCustomerConcernForCashierWithValueWithInvoiceNumber(string forCashier, decimal withValue, string withInvoiceNumber)
        {
            var paidOutCorrection = new CreateMiscellaneousTransaction(systemEnvironment);
            paidOutCorrection.Save(_bankingPeriodId, forCashier, -withValue, withInvoiceNumber, "Customer Concern", "M-", StartingSalesTranactionNumber.ToString(CultureInfo.InvariantCulture), "MiscOut", "01");
        }

        public void createPaidOutForStationeryForCashierWithValueWithInvoiceNumber(string forCashier, decimal withValue, string withInvoiceNumber)
        {
            var paidOutCorrection = new CreateMiscellaneousTransaction(systemEnvironment);
            paidOutCorrection.Save(_bankingPeriodId, forCashier, -withValue, withInvoiceNumber, "Stationery", "M-", (StartingSalesTranactionNumber + 1).ToString(CultureInfo.InvariantCulture), "MiscOut", "02");
        }

        public void createPaidOutCorrectionForBreakfastVoucherForCashierWithValueWithInvoiceNumber(string forCashier, decimal withValue, string withInvoiceNumber)
        {
            var paidOutCorrection = new CreateMiscellaneousTransaction(systemEnvironment);
            paidOutCorrection.Save(_bankingPeriodId, forCashier, withValue, withInvoiceNumber, "Breakfast Voucher", "C-", (StartingSalesTranactionNumber + 2).ToString(CultureInfo.InvariantCulture), "MiscOut", "03");
        }

        public void createPaidOutCorrectionForSocialFundForCashierWithValueWithInvoiceNumber(string forCashier, decimal withValue, string withInvoiceNumber)
        {
            var paidOutCorrection = new CreateMiscellaneousTransaction(systemEnvironment);
            paidOutCorrection.Save(_bankingPeriodId, forCashier, withValue, withInvoiceNumber, "Social Fund", "C-", (StartingSalesTranactionNumber + 3).ToString(CultureInfo.InvariantCulture), "MiscOut", "04");
        }

        public void createPaidOutForTravelForCashierWithValueWithInvoiceNumber(string forCashier, decimal withValue, string withInvoiceNumber)
        {
            var paidOutCorrection = new CreateMiscellaneousTransaction(systemEnvironment);
            paidOutCorrection.Save(_bankingPeriodId, forCashier, -withValue, withInvoiceNumber, "Travel", "M-", (StartingSalesTranactionNumber + 4).ToString(CultureInfo.InvariantCulture), "MiscOut", "05");
        }

        public void createPaidOutCorrectionForTravelForCashierWithValueWithInvoiceNumber(string forCashier, decimal withValue, string withInvoiceNumber)
        {
            var paidOutCorrection = new CreateMiscellaneousTransaction(systemEnvironment);
            paidOutCorrection.Save(_bankingPeriodId, forCashier, withValue, withInvoiceNumber, "Travel", "C-", (StartingSalesTranactionNumber + 5).ToString(CultureInfo.InvariantCulture), "MiscOut", "05");
        }

        public void createMiscellaneousIncomeForBadChequeForCashierWithValueWithInvoiceNumber(string forCashier, decimal withValue, string withInvoiceNumber)
        {
            var paidOutCorrection = new CreateMiscellaneousTransaction(systemEnvironment);
            paidOutCorrection.Save(_bankingPeriodId, forCashier, withValue, withInvoiceNumber, "Bad Cheque", "M+", (StartingSalesTranactionNumber + 6).ToString(CultureInfo.InvariantCulture), "MiscInc", "01");
        }

        public void createMiscellaneousIncomeCorrectionForSocialFundForCashierWithValueWithInvoiceNumber(string forCashier, decimal withValue, string withInvoiceNumber)
        {
            var paidOutCorrection = new CreateMiscellaneousTransaction(systemEnvironment);
            paidOutCorrection.Save(_bankingPeriodId, forCashier, -withValue, withInvoiceNumber, "Social Fund", "C+", (StartingSalesTranactionNumber + 7).ToString(CultureInfo.InvariantCulture), "MiscInc", "02");
        }

        #endregion

        #region "SUT: Get Today"

        private int GetTodayAsDefinedBySystemUnderTest()
        {
            var periods = new PeriodCollection();

            periods.LoadDates(DateTime.Today);

            return periods.TodaysPeriod().PeriodID;
        }

        #endregion

        #region "SUT: Create Safe"

        public void safeCreatedToday()
        {
            int periodId = GetTodayAsDefinedBySystemUnderTest();

            var bp = new BankingPeriod();
            bp.LoadSafe(periodId, _loggedOnUser, DateTime.Now);
        }

        #endregion

        #region "SUT: Safe maintenance"

        private int _safeDatePeriodId;

        public void setSafeMaintenanceOnCurrentSafe(DateTime date)
        {
            _safeDatePeriodId = getPeriodIdForDate(date);
        }

        public void performSafeMaintenanceWithThisMainSafeAndThisChangeSafe(string withThisMainSafe,
                                                                            string andThisChangeSafe)
        {
            var safeMaintenanceBankingPeriod = new BankingPeriod();

            safeMaintenanceBankingPeriod.LoadSafe(_safeDatePeriodId);
            var mainSafeDenoms = new DenominationTotal(withThisMainSafe);
            var changeSafeDenoms = new DenominationTotal(andThisChangeSafe);


            safeMaintenanceBankingPeriod.SetSafeUser1(_loggedOnUser);
            safeMaintenanceBankingPeriod.SetSafeUser2(_authoriser);

            decimal denomTotal;
            decimal denomKey;
            decimal denomMainSafeValue;
            decimal denomChangeSafeValue;

            foreach (KeyValuePair<string, int> denom in DenominationTotal.DenominationValues)
            {
                denomTotal = 0;
                denomMainSafeValue = 0;
                denomChangeSafeValue = 0;
                denomKey = denom.Value / (decimal)100;

                if (mainSafeDenoms.Denominations.ContainsKey(denomKey))
                {
                    denomMainSafeValue = mainSafeDenoms.Denominations[denomKey];
                }
                denomTotal += denomMainSafeValue;
                safeMaintenanceBankingPeriod.set_SafeMain(CurrencyId, denomKey, (int)TenderTypes.TenderTypeCash,
                                                          denomMainSafeValue);

                if (changeSafeDenoms.Denominations.ContainsKey(denomKey))
                {
                    denomChangeSafeValue = changeSafeDenoms.Denominations[denomKey];
                }
                denomTotal += denomChangeSafeValue;
                safeMaintenanceBankingPeriod.set_SafeChange(CurrencyId, denomKey, (int)TenderTypes.TenderTypeCash,
                                                            denomChangeSafeValue);

                safeMaintenanceBankingPeriod.set_SafeSystem(CurrencyId, denomKey, (int)TenderTypes.TenderTypeCash,
                                                            denomTotal);
            }

            safeMaintenanceBankingPeriod.NewBankingSafeMaintenanceTransactionSafe(_bankingPeriodId);
        }

        #endregion

        #region "SUT: Create float from safe"

        public void createFloatFromSafeWithThisSealAndDenominations(string withThisSeal, string andDenominations)
        {
            int periodId = GetTodayAsDefinedBySystemUnderTest();

            var newFloat = new FloatBanking(_bankingPeriodId, periodId,
                                            _loggedOnUser, _authoriser,
                                            FloatBanking.Types.Float,
                                            NewBanking.Core.Library.AccountabilityModel());

            newFloat.SetSealNumber(withThisSeal);
            newFloat.Comments = string.Empty;

            var denomTotal = new DenominationTotal(andDenominations);

            foreach (KeyValuePair<decimal, decimal> denom in denomTotal.Denominations)
            {
                if (denom.Value > 0)
                {
                    newFloat.SetDenomination(CurrencyId, denom.Key, (int)TenderTypes.TenderTypeCash, denom.Value,
                                             string.Empty);
                }

            }
            newFloat.Save();
        }

        #endregion

        #region "SUT: Assign float to cashier"

        public void assignFloatUsingSealToCashier(string usingSeal, string toCashier)
        {
            int periodId = GetTodayAsDefinedBySystemUnderTest();

            var assignFloat = new FloatBanking(_bankingPeriodId, periodId,
                                               _loggedOnUser, _authoriser,
                                               FloatBanking.Types.Float,
                                               NewBanking.Core.Library.AccountabilityModel());

            var floatId = getBagIdForSeal(usingSeal);


            assignFloat.SetBagState(BagStates.Released);
            assignFloat.SetAccountId(getUserIdForName(toCashier));
            assignFloat.LoadOriginalBag(floatId);

            var denoms = getDenominationBreakdownForBag(floatId);


            foreach (DataRow denom in denoms.Rows)
            {
                if (Convert.ToDecimal(denom["Value"]) > 0)
                {
                    assignFloat.SetDenomination(CurrencyId, Convert.ToDecimal(denom["ID"]),
                                                (int)TenderTypes.TenderTypeCash, Convert.ToDecimal(denom["Value"]),
                                                string.Empty);
                }
            }

            var assignFloatSaveImplementation = (new FloatAssignSaveFactory()).GetImplementation();

            assignFloatSaveImplementation.SaveFloatAssign(ref assignFloat, _bankingPeriodId,
                                                          periodId);
        }

        /// <summary>
        /// Check that all floats actually assigned by authorizer
        /// </summary>
        /// <param name="authorizer">authorizer name</param>
        /// <returns>OK if all correct, names of assigned by persons - if not</returns>
        public List<string> allFloatsAssignedByAuthorizer(string authorizer)
        {
            DataSet ds = new DataSet();
            int periodId = GetTodayAsDefinedBySystemUnderTest();

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "NewBankingFloatedCashierDisplay";
                command.Parameters.Add(new SqlParameter("@PeriodID", SqlDbType.Int) { Value = periodId });

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(ds);
            }

            var table = ds.Tables[0].AsEnumerable();

            var userID = getUserIdForName(authorizer);

            var assignedFloatList = table.Where(row => row.Field<Int32>("StartFloatAssignedByUserID") != userID)
                .Select(row => row.Field<string>("StartFloatAssignedByUserName")).ToList();

            if (!assignedFloatList.Any())
            {
                assignedFloatList.Add("OK");
            }

            return assignedFloatList;
        }

        #endregion

        #region "SUT: Create pickup"

        private DenominationTotal _floatDenoms;
        private DenominationTotal _pickupDenoms;
        private string _floatSeal = string.Empty;
        private string _pickupSeal = string.Empty;

        public void configureFloatCreatedFromPickupWithThisSealAndDenominations(string withThisSeal,
                                                                                string andDenominations)
        {
            _floatSeal = withThisSeal;
            _floatDenoms = new DenominationTotal(andDenominations);
        }

        public void configurePickupWithThisSealAndDenominations(string withThisSeal, string andDenominations)
        {
            _pickupSeal = withThisSeal;
            _pickupDenoms = new DenominationTotal(andDenominations);
        }

        public void configurePickupWithDenominations(string andDenominations)
        {
            _pickupDenoms = new DenominationTotal(andDenominations);
        }

        public void setNewCommentOnBagForSealIdWithComment(string forSealId, string withComment)
        {
            int pickupId = getBagIdForSeal(forSealId);

            var pickup = new Pickup(_loggedOnUser, _authoriser, NewBanking.Core.Library.AccountabilityModel());
            pickup.LoadOriginalBag(pickupId);
            pickup.Save();
        }

        public void createPickupAndFloatForThisCashier(string forThisCashier)
        {
            createPickupAndFloatForThisCashierWithComment(forThisCashier, string.Empty);
        }

        public void createPickupAndFloatForThisCashierWithComment(string forThisCashier, string withComment)
        {
            var cashierId = getUserIdForName(forThisCashier);
            createPickupAndFloatForThisCashierWithComment(cashierId, withComment);
        }

        public void createPickupAndFloatForThisCashierWithComment(int cashierId, string withComment)
        {
            var periodId = GetTodayAsDefinedBySystemUnderTest();

            var newFloat = new FloatBanking(_bankingPeriodId, periodId, _loggedOnUser,
                                            _authoriser, FloatBanking.Types.Float,
                                            NewBanking.Core.Library.AccountabilityModel());

            newFloat.SetSealNumber(_floatSeal);

            foreach (KeyValuePair<decimal, decimal> denom in _floatDenoms.Denominations)
            {
                if (denom.Value > 0)
                {
                    newFloat.SetDenomination(CurrencyId, denom.Key, (int)TenderTypes.TenderTypeCash, denom.Value,
                                             string.Empty);
                }
            }

            var newPickup = new Pickup(_loggedOnUser, _authoriser, NewBanking.Core.Library.AccountabilityModel());

            newPickup.State = Pickup.States.PickupFloat;
            newPickup.LoadSystemFigures(_bankingPeriodId, cashierId);
            newPickup.SetBagState(BagStates.Sealed);
            newPickup.SetSealNumber(_pickupSeal);
            newPickup.SetFloatReturned(_floatDenoms.TotalConvertedToPounds());
            newPickup.SetComments(withComment);

            foreach (KeyValuePair<decimal, decimal> denom in _pickupDenoms.Denominations)
            {
                if (denom.Value > 0)
                {
                    newPickup.SetDenomination(CurrencyId, denom.Key, (int)TenderTypes.TenderTypeCash, denom.Value);
                }
            }

            newFloat.SetAccountId(cashierId);
            newFloat.SaveFromPickup();

            var cashierTills = newPickup.CashierTills;
            newFloat.AddPickupsToCashierTills(ref cashierTills);
            newPickup.SetRelatedBagId(newFloat.NewId);

            newPickup.Save(_floatDenoms.TotalConvertedToPounds());
        }

        public void createPickupWithSealForCashier(string pickupSeal, int cashierId)
        {
            var newPickup = new Pickup(_loggedOnUser, _authoriser, NewBanking.Core.Library.AccountabilityModel());

            newPickup.State = Pickup.States.Pickup;
            newPickup.LoadSystemFigures(_bankingPeriodId, cashierId);
            newPickup.SetBagState(BagStates.Sealed);
            newPickup.SetSealNumber(pickupSeal);
            newPickup.SetComments(string.Empty);

            foreach (var cashTillTender in newPickup.CashierTills.First().Tenders)
            {
                if (cashTillTender.TenderId == (int)TenderTypes.TenderTypeCash)
                {
                    foreach (KeyValuePair<decimal, decimal> denom in _pickupDenoms.Denominations)
                    {
                        if (denom.Value > 0)
                        {
                            newPickup.SetDenomination(CurrencyId, denom.Key, (int)TenderTypes.TenderTypeCash, denom.Value);
                        }
                    }
                }
                else
                {
                    newPickup.SetDenomination(CurrencyId, 0.01M, cashTillTender.TenderId, cashTillTender.Amount);
                }
            }

            newPickup.Save();
        }

        public void createPickupForCncCashier(int cashierId)
        {
            var autoPickupForCnC = new AutoPickupForClickAndCollect();
            autoPickupForCnC.CompletePickupForClickAndCollect(_bankingPeriodId, cashierId);
        }

        #endregion

        #region SUT: Check bags

        public void recheckPickupSealIsCommentIs(string sealIs, string commentIs)
        {
            int pickupId = getBagIdForSeal(sealIs);

            var pickup = new Pickup(_loggedOnUser, _authoriser, NewBanking.Core.Library.AccountabilityModel());
            pickup.LoadOriginalBag(pickupId);
            pickup.SetComments(commentIs);
            pickup.Save();
        }

        public void recheckFloatSealIsCommentIs(string sealIs, string commentIs)
        {
            int floatId = getBagIdForSeal(sealIs);

            var floatBanking = new FloatBanking(_loggedOnUser, _authoriser, FloatBanking.Types.Float,
                                                         NewBanking.Core.Library.AccountabilityModel());
            floatBanking.LoadOriginalBag(floatId);
            floatBanking.Comments = commentIs;
            floatBanking.Save();
        }

        public void addCashDropForCashierSealIsValueIsCommentIs(string forCashier, string sealIs, string valueIs,
                                                                string commentIs)
        {
            var cashDrop = new Pickup(_loggedOnUser, _authoriser, NewBanking.Core.Library.AccountabilityModel());

            cashDrop.State = Pickup.States.Pickup;
            cashDrop.LoadSystemFigures(GetTodayAsDefinedBySystemUnderTest(), getUserIdForName(forCashier));
            cashDrop.SetBagState(BagStates.Sealed);
            cashDrop.SetComments(commentIs);
            cashDrop.SetSealNumber(sealIs);

            var bankingDenoms = new DenominationTotal(valueIs);

            foreach (KeyValuePair<decimal, decimal> denom in bankingDenoms.Denominations)
            {
                if (denom.Value != 0)
                {
                    cashDrop.SetDenomination(CurrencyId, denom.Key, (int)TenderTypes.TenderTypeCash,
                                             denom.Value);
                }
            }

            cashDrop.NewBankingSaveCashDrop();
        }

        public void recheckSafeSealIsCommentIs(string sealIs, string commentIs)
        {
            /*int pickupId = GetBagIdForSeal(sealIs);

            BankingBag safeBag = new BankingBag();
            safeBag.
            */
        }


        #endregion

        #region "SUT: End of day banking"

        public void performEndOfDayBankingWithThisSealThisSlipThisCashValueThisCashiers(string thisSeal, string thisSlip, string thisCashValue, string thisCashiers)
        {
            string storeId = string.Empty;
            string storeName = string.Empty;

            NewBanking.Core.Library.GetStoreID(ref storeId, ref storeName);

            returnPickupsToSafe();

            int bankingBagId = createBankingBag(thisSeal, storeId + thisSlip, string.Empty, thisCashValue);
            managerConfirmsBanking(bankingBagId, thisSeal, storeId + thisSlip, string.Empty);

            bankingBagId = createNonCashBankingBag(storeId + thisSlip, thisCashiers);
            managerConfirmsBanking(bankingBagId, thisSeal, storeId + thisSlip, string.Empty);

            closeSafeToBanking();
        }

        public void performEndOfDayBankingCashBagOnlyWithThisSealThisSlipThisCommentThisValue(string thisSeal,
                                                                                              string thisSlip,
                                                                                              string thisComment,
                                                                                              string thisValue)
        {
            string storeId = string.Empty;
            string storeName = string.Empty;

            NewBanking.Core.Library.GetStoreID(ref storeId, ref storeName);

            returnPickupsToSafe();
            int bankingBagId = createBankingBag(thisSeal, storeId + thisSlip, thisComment, thisValue);
            managerConfirmsBanking(bankingBagId, thisSeal, storeId + thisSlip, thisComment);
            closeSafeToBanking();
        }

        private void returnPickupsToSafe()
        {
            DataTable pickups = getSealedPickupBagsForSelectedPeriodId(_bankingPeriodId);

            foreach (DataRow pickup in pickups.Rows)
            {
                var pickUpId = Convert.ToInt32(pickup["ID"]);

                var pickupDenomination = getDenominationBreakdownForBag(pickUpId);

                var returnPickupToSafe = new PickupUsingLatestSafe(_loggedOnUser, _authoriser,
                                                                   NewBanking.Core.Library.AccountabilityModel());

                returnPickupToSafe.State = Pickup.States.Pickup;
                returnPickupToSafe.State = Pickup.States.Check;
                returnPickupToSafe.LoadOriginalBag(pickUpId);
                returnPickupToSafe.LoadSystemFigures(_bankingPeriodId, (int)pickup["AccountabilityID"]);
                returnPickupToSafe.SetBagState(BagStates.Sealed);
                returnPickupToSafe.SetBagState(BagStates.BackToSafe);

                returnPickupToSafe.SetFloatReturned(0);

                foreach (DataRow denom in pickupDenomination.Rows)
                {
                    if (Convert.ToDecimal(denom["Value"]) != 0)
                    {
                        returnPickupToSafe.SetDenomination(CurrencyId,
                                                           Convert.ToDecimal(denom["ID"]),
                                                           (int)denom["TenderID"],
                                                           Convert.ToDecimal(denom["Value"]));
                    }
                }

                returnPickupToSafe.Save();
            }
        }

        private int createNonCashBankingBag(string slipNumber, string cashiers)
        {
            int periodId = GetTodayAsDefinedBySystemUnderTest();

            var createBankingBag = new FloatBankingUsingLatestSafe(_bankingPeriodId,
                                                                   periodId,
                                                                   _loggedOnUser,
                                                                   _authoriser,
                                                                   FloatBanking.Types.Banking,
                                                                   NewBanking.Core.Library.AccountabilityModel());
            createBankingBag.SetBankingPeriodId(_bankingPeriodId);
            createBankingBag.SetSealNumber("00000000");

            var pickup = new Pickup(_loggedOnUser, _authoriser, NewBanking.Core.Library.AccountabilityModel());
            foreach (var cashier in cashiers.Split(new[] { ',' }))
            {
                pickup.LoadSystemFigures(_bankingPeriodId, Convert.ToInt32(cashier));

                foreach (var cashTillTender in pickup.CashierTills.First().Tenders)
                {
                    if (cashTillTender.TenderId != (int)TenderTypes.TenderTypeCash)
                    {
                        createBankingBag.SetDenomination(CurrencyId, 0.01M, cashTillTender.TenderId, cashTillTender.Amount, slipNumber);
                    }
                }
            }

            createBankingBag.Save();

            return createBankingBag.NewId;
        }

        private int createBankingBag(string sealNumber, string slipNumber, string comment, string value)
        {
            int periodId = GetTodayAsDefinedBySystemUnderTest();

            var bankingDenoms = new DenominationTotal(value);

            var createBankingBag = new FloatBankingUsingLatestSafe(_bankingPeriodId,
                                                                   periodId,
                                                                   _loggedOnUser,
                                                                   _authoriser,
                                                                   FloatBanking.Types.Banking,
                                                                   NewBanking.Core.Library.AccountabilityModel());
            createBankingBag.SetBankingPeriodId(_bankingPeriodId);
            createBankingBag.SetSealNumber(sealNumber);
            createBankingBag.Comments = comment;

            foreach (KeyValuePair<decimal, decimal> denom in bankingDenoms.Denominations)
            {
                if (denom.Value != 0)
                {
                    createBankingBag.SetDenomination(CurrencyId, denom.Key, (int)TenderTypes.TenderTypeCash,
                                                     denom.Value,
                                                     slipNumber);
                }
            }
            createBankingBag.Save();

            return createBankingBag.NewId;
        }

        private void managerConfirmsBanking(int bankingBagId, string sealNumber, string slipNumber, string comment)
        {
            int periodId = GetTodayAsDefinedBySystemUnderTest();

            var managerCheckBankingBag = new FloatBankingUsingLatestSafe(_bankingPeriodId,
                                                                         periodId,
                                                                         _loggedOnUser,
                                                                         _authoriser,
                                                                         FloatBanking.Types.Banking,
                                                                         NewBanking.Core.Library.AccountabilityModel());

            managerCheckBankingBag.LoadOriginalBag(bankingBagId);
            managerCheckBankingBag.SetBagState(BagStates.ManagerChecked);
            managerCheckBankingBag.SetBankingPeriodId(_bankingPeriodId);
            managerCheckBankingBag.Comments = comment;
            managerCheckBankingBag.SetSealNumber(sealNumber);

            var bankingDenomination = getDenominationBreakdownForBag(bankingBagId);

            foreach (DataRow denom in bankingDenomination.Rows)
            {
                if (Convert.ToDecimal(denom["Value"]) != 0)
                {
                    managerCheckBankingBag.SetDenomination(CurrencyId,
                                                           Convert.ToDecimal(denom["ID"]),
                                                           (int)denom["TenderID"],
                                                           Convert.ToDecimal(denom["Value"]),
                                                           slipNumber);
                }
            }
            managerCheckBankingBag.Save();
        }

        private void closeSafeToBanking()
        {
            var actualSafe = new BankingPeriod();

            actualSafe.LoadSafe(_bankingPeriodId);
            actualSafe.Safe.ClosePeriod();
        }

        #endregion

        #region "SUT: End of day banking STHOA output"

        public void initialiseEndOfDayBanking()
        {
            string filePath = outputFileLocation() + "STHOA";
            if (File.Exists(filePath))
                File.Delete(filePath);
        }

        public void createBankingFileForHeadOfficeWithCostCode(ProcessTransmissions processTransmissions)
        {
            int? existingState = configureChangeRecord0094(1);
            CreateBankingOutput(processTransmissions);
            configureChangeRecord0094(existingState);
        }

        public void createBankingFileForHeadOfficeWithoutCostCode(ProcessTransmissions processTransmissions)
        {
            int? existingState = configureChangeRecord0094(0);
            CreateBankingOutput(processTransmissions);
            configureChangeRecord0094(existingState);
        }

        public void createBankingFileForHeadOfficeWithoutCostCode()
        {
            int? existingState = configureChangeRecord0094(0);
            CreateBankingOutput();
            configureChangeRecord0094(existingState);
        }

        #region "Private methods"

        private void CreateBankingOutput(ProcessTransmissions processTransmissions)
        {
            processTransmissions.ProcessFileCode("DB", getDateFromPeriodId(_bankingPeriodId));
        }

        private void CreateBankingOutput()
        {
            var proc = new Process
            {
                StartInfo =
                {
                    FileName = @"..\..\..\..\out\build\Oasys3\" + "ProcessTransmissions.exe",
                    Arguments = "DB PDATE=" + getDateFromPeriodId(_bankingPeriodId).ToString("dd/MM/yy"),
                    UseShellExecute = false
                }
            };

            TestEnvironmentSetup.SetupSpawnedProcess(proc.StartInfo);

            proc.Start();
            proc.WaitForExit();
        }

        private int? configureChangeRecord0094(int? active)
        {
            if (active == null)
            {
                deleteChangeRecord0094();
                return null;
            }

            int? existingState = existingChangeRecord0094();

            if (existingState == null)
            {
                createChangeRecord0094((int)active);
            }
            else
            {
                updateChangeRecord0094((int)active);
            }
            return existingState;
        }

        private int? existingChangeRecord0094()
        {
            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandText = "select BooleanValue from Parameters where ParameterID = -94";

                return (int?)command.ExecuteScalar();
            }
        }

        private void deleteChangeRecord0094()
        {
            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandText = "delete Parameters where ParameterID = -94";
                command.ExecuteNonQuery();
            }
        }

        private void createChangeRecord0094(int value)
        {
            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandText = "insert Parameters (ParameterID, Description, StringValue, LongValue, BooleanValue, DecimalValue, ValueType) " +
                                      "            values(-94, null, null, null, " + value.ToString(CultureInfo.InvariantCulture) + ", null, 3)";

                command.ExecuteNonQuery();
            }
        }

        private void updateChangeRecord0094(int value)
        {
            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandText = "update Parameters set BooleanValue = " + value.ToString(CultureInfo.InvariantCulture) + " where ParameterID = -94";

                command.ExecuteNonQuery();
            }
        }

        #endregion

        #endregion

        #region "SUT: End Of Day Check"

        private IBagScanningEngine _scanEngine;

        public void startEndOfDayCheckProcessForToday()
        {
            setBankingDate(DateTime.Today);

            _scanEngine = (new BagScanningEngineFactory()).GetImplementation();
            _scanEngine.Initialise(_bankingPeriodId, IBagScanningEngine.BusinessProcess.EndOfDayCheckProcess);
        }

        public void scanValidPickupBag(string withThisSeal)
        {
            _scanEngine.CreatePickupBag(withThisSeal);
        }

        public void scanValidBankingBag(string withThisSeal)
        {
            _scanEngine.CreateBankingBag(withThisSeal);
        }

        public void safeLockedFromDateAndTimeToDateAndTimeByManagerAndWitnessedBy(DateTime fromDate, string fromTime,
                                                                                  DateTime toDate, string toTime,
                                                                                  string byManager, string andWitnessedBy)
        {
            _scanEngine.EndOfDayCheckAdditionalInformation(true,
                                                           safeLockedFrom(fromDate, fromTime),
                                                           safeLockedTo(toDate, toTime),
                                                           getUserIdForName(byManager), getUserIdForName(andWitnessedBy));
        }

        public void completeEndOfDayCheckProcess()
        {
            _scanEngine.EndOfDayCheckPersist();
        }

        private static DateTime safeLockedFrom(DateTime lockedFrom, string fromTime)
        {
            lockedFrom = lockedFrom.AddHours(Convert.ToDouble(fromTime.Substring(0, 2)));
            lockedFrom = lockedFrom.AddMinutes(Convert.ToDouble(fromTime.Substring(3, 2)));

            return lockedFrom;
        }

        private static DateTime safeLockedTo(DateTime lockedTo, string toTime)
        {
            lockedTo = lockedTo.AddHours(Convert.ToDouble(toTime.Substring(0, 2)));
            lockedTo = lockedTo.AddMinutes(Convert.ToDouble(toTime.Substring(3, 2)));

            return lockedTo;
        }

        #endregion

        #region "TEST: Safe Correct"

        public string safeHasCorrectTotal()
        {
            Dictionary<decimal, decimal> safeValues = getSafeValues();

            return DenominationTotal.ParseSafeToString(safeValues);
        }

        public Boolean badSafeDoesNotExist()
        {
            int periodCount;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand sqlCommand = dbConnection.CreateCommand();

                sqlCommand.CommandText = @"select count(*) from dbo.SafeDenoms where [PeriodID] = 0";
                var toParse = sqlCommand.ExecuteScalar();
                periodCount = int.Parse(toParse.ToString());
            }

            return periodCount == 0;
        }

        public string bankingBagWithThisSealWasCreatedToday(string withThisSeal)
        {
            return bankingBagWithThisSeal(withThisSeal, "InPeriodID", "C");
        }

        public string bankingBagWithThisSealAfterBeingManagerCheckedWasDestoryedToday(string withThisSeal)
        {
            return bankingBagWithThisSeal(withThisSeal, "OutPeriodID", "C");
        }

        public string bankingBagWithThisSealThenManagerCheckedToday(string withThisSeal)
        {
            return bankingBagWithThisSeal(withThisSeal, "InPeriodID", "M");
        }

        private Dictionary<decimal, decimal> getSafeValues()
        {
            var safeValues = new Dictionary<decimal, decimal>();

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandText =
                    @"select ID, SystemValue from dbo.SafeDenoms where [PeriodID] = (select max(PeriodID) from [Safe]) and SystemValue <> 0";

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    safeValues.Add((decimal)reader["ID"], (decimal)reader["SystemValue"]);
                }
            }

            return safeValues;
        }

        private string bankingBagWithThisSeal(string withThisSeal, string fieldRequired, string bagState)
        {
            int whenBagCreatedPeriodId;
            DateTime whenBagCreatedDate;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandText = @"select " + fieldRequired + " from SafeBags where SealNumber = '" + withThisSeal +
                                      "' and [Type] ='B' and [State] = '" + bagState + "'";

                whenBagCreatedPeriodId = (int)command.ExecuteScalar();
            }

            whenBagCreatedDate = getDateFromPeriodId(whenBagCreatedPeriodId);

            if (whenBagCreatedDate.Date.CompareTo(DateTime.Today) == 0)
            {
                return "Today";
            }

            return whenBagCreatedDate.Date.ToString(CultureInfo.InvariantCulture);
        }

        #endregion

        #region TEST: Comments on Bags

        public string pickupBagInDatabaseWithThisSealHasThisCommentValue(string withThisSeal)
        {
            string comment;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand sqlCommand = dbConnection.CreateCommand();

                sqlCommand.CommandText = @"SELECT [Comments] FROM [Oasys].[dbo].[SafeBags] where [SealNumber]='" + withThisSeal + "' and [TYPE]='P'";
                comment = sqlCommand.ExecuteScalar().ToString();
            }

            return comment.Trim();
        }

        public string inCashierReportPickupBagWithThisSealDisplaysThisCommentValue(string withThisSeal)
        {
            string comment;
            int periodId = GetTodayAsDefinedBySystemUnderTest();

            // CashierReport_Load
            var report = new CashierReport(periodId, DateTime.Today, null, "store", "StoreName");
            var t = typeof(CashierReport);
            var spd = new FpSpread();

            var cashierReportVmField = t.GetField("CashierReportVM", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            var cashierReportVmWithComments = (CashierReportVMWithComments)cashierReportVmField.GetValue(report);

            cashierReportVmWithComments.SetCashierReportDisplayGrid(ref spd);

            // using reflection to access private method and friend field
            MethodInfo m = t.GetMethod("DisplayGrid", BindingFlags.NonPublic | BindingFlags.Instance);
            m.Invoke(report, new Object[] { spd, periodId });

            // Get _CommentsRowIndex
            var v = typeof(CashierReportVMWithComments);
            var commentsRowIndexField = v.GetField("_CommentsRowIndex", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            var commentsRowIndex = (int)commentsRowIndexField.GetValue(cashierReportVmWithComments);

            comment = spd.ActiveSheet.Cells.Get(commentsRowIndex, 1).Value.ToString();

            return comment;
        }

        public string floatBagInDatabaseWithThisSealHasThisCommentValue(string withThisSeal)
        {
            string comment;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand sqlCommand = dbConnection.CreateCommand();

                sqlCommand.CommandText = @"SELECT [Comments] FROM [Oasys].[dbo].[SafeBags] where [SealNumber]='" + withThisSeal + "' and [TYPE]='F'";
                comment = sqlCommand.ExecuteScalar().ToString();
            }

            return comment.Trim();
        }

        public string[] bagCommentsOnDatabaseForCashierHasThisCommentValue(string forCashier)
        {
            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand sqlCommand = dbConnection.CreateCommand();

                sqlCommand.CommandText = @"SELECT [Comments] FROM [Oasys].[dbo].[SafeBags] where [Comments] != '' and [AccountabilityID]=" + getUserIdForName(forCashier);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                var dataTable = new DataTable();
                dataTable.Load(reader);

                var comments = new List<string>();

                foreach (DataRow row in dataTable.Rows)
                {
                    comments.Add(row["Comments"].ToString().Trim());
                }

                return comments.ToArray();
            }
        }

        public string[] bagCommentsOnReportForCashierHasThisCommentValue(string forCashier)
        {
            //todo: confirm this should be testing manually
            return new[] { "" };
        }

        #endregion

        #region TEST: Refund report

        public string refundReportHasColumnNameForIndex(int forIndex)
        {
            var refundsList = new RefundsList.RefundsList(_loggedOnUser, 0, 0, "");

            var t = typeof(RefundsList.RefundsList);

            var refundListField = t.GetField("refundList", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            var refundList = (IRefundList)refundListField.GetValue(refundsList);

            var sheeeetView = new SheetView();
            refundList.SetReportColumnNames(ref sheeeetView);

            return sheeeetView.Columns[forIndex].Label;
        }

        #endregion

        #region TEST: Reports

        public string salesOnDatabaseForCashierWithStartDateAndEndDateHasValue(string forCashier, DateTime withStartDate, DateTime andEndDate)
        {
            var reportData = new DataTable();

            decimal value = 0M;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "BankingCashierPerformanceGet";
                command.Parameters.Add(new SqlParameter("@DateStart", SqlDbType.DateTime) { Value = withStartDate });
                command.Parameters.Add(new SqlParameter("@DateEnd", SqlDbType.DateTime) { Value = andEndDate });
                reportData.Load(command.ExecuteReader());

                foreach (DataRow row in reportData.Rows)
                {
                    if (getUserIdForName(forCashier) == Convert.ToInt32(row["CashierId"]))
                    {
                        value += Convert.ToDecimal(row["ValueSales"]);
                    }
                }

                return value.ToString("0.00");
            }
        }

        #endregion

        #region TEST: Search Orders

        public string orderNumberForPhoneNumberIs(string phoneNumber)
        {
            ISearchType searchType = new PhoneNumberSearch();
            return getOrderNumberGivenSearchCriteria(phoneNumber, searchType);
        }
        public string orderNumberForMobileNumberIs(string phoneNumber)
        {
            ISearchType searchType = new PhoneNumberSearch();
            return getOrderNumberGivenSearchCriteria(phoneNumber, searchType);
        }
        public string orderNumberForWorkNumberIs(string phoneNumber)
        {
            ISearchType searchType = new PhoneNumberSearch();
            return getOrderNumberGivenSearchCriteria(phoneNumber, searchType);
        }
        public string orderNumberForDeliveryContactNumberIs(string phoneNumber)
        {
            ISearchType searchType = new PhoneNumberSearch();
            return getOrderNumberGivenSearchCriteria(phoneNumber, searchType);
        }

        public int searchForPhoneNumberReturnsNumberOfMatches(string phoneNumber)
        {
            ISearchType searchType = new PhoneNumberSearch();
            return getMatchingCountGivenSearchCriteria(phoneNumber, searchType);
        }
        public int searchForMobileNumberReturnsNumberOfMatches(string phoneNumber)
        {
            ISearchType searchType = new PhoneNumberSearch();
            return getMatchingCountGivenSearchCriteria(phoneNumber, searchType);
        }
        public int searchForWorkNumberReturnsNumberOfMatches(string phoneNumber)
        {
            ISearchType searchType = new PhoneNumberSearch();
            return getMatchingCountGivenSearchCriteria(phoneNumber, searchType);
        }
        public int searchForDeliveryContactNumberReturnsNumberOfMatches(string phoneNumber)
        {
            ISearchType searchType = new PhoneNumberSearch();
            return getMatchingCountGivenSearchCriteria(phoneNumber, searchType);
        }

        public string orderNumberForCustomerNameIs(string customerName)
        {
            ISearchType searchType = new CustomerNameSearch();
            return getOrderNumberGivenSearchCriteria(customerName, searchType);
        }

        public string orderNumberForDeliveryCustomerNameIs(string customerName)
        {
            ISearchType searchType = new CustomerNameSearch();
            return getOrderNumberGivenSearchCriteria(customerName, searchType);
        }

        public string orderNumberForDeliveryContactNameIs(string customerName)
        {
            ISearchType searchType = new CustomerNameSearch();
            return getOrderNumberGivenSearchCriteria(customerName, searchType);
        }

        public int searchForCustomerNameReturnsNumberOfMatches(string customerName)
        {
            ISearchType searchType = new CustomerNameSearch();
            return getMatchingCountGivenSearchCriteria(customerName, searchType);
        }

        public int searchForDeliveryCustomerNameReturnsNumberOfMatches(string customerName)
        {
            ISearchType searchType = new CustomerNameSearch();
            return getMatchingCountGivenSearchCriteria(customerName, searchType);
        }

        public int searchForDeliveryContactNameReturnsNumberOfMatches(string customerName)
        {
            ISearchType searchType = new CustomerNameSearch();
            return getMatchingCountGivenSearchCriteria(customerName, searchType);
        }

        public string orderNumberForCustomerPostcodeIs(string postCode)
        {
            ISearchType searchType = new PostCodeSearch();
            return getOrderNumberGivenSearchCriteria(postCode, searchType);
        }

        public string orderNumberForDeliveryPostcodeIs(string postCode)
        {
            ISearchType searchType = new PostCodeSearch();
            return getOrderNumberGivenSearchCriteria(postCode, searchType);
        }

        public int searchForCustomerPostcodeReturnsNumberOfMatches(string postCode)
        {
            ISearchType searchType = new PostCodeSearch();
            return getMatchingCountGivenSearchCriteria(postCode, searchType);
        }

        public int searchForDeliveryPostcodeReturnsNumberOfMatches(string postCode)
        {
            ISearchType searchType = new PostCodeSearch();
            return getMatchingCountGivenSearchCriteria(postCode, searchType);
        }

        private int getMatchingCountGivenSearchCriteria(string searchValue, ISearchType searchType)
        {
            var reportData = new DataTable();

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                var command = prepareSqlCommand(dbConnection);
                command = searchType.addParameter(command, searchValue);

                reportData.Load(command.ExecuteReader());

                return reportData.Rows.Count;

            }
        }

        private string getOrderNumberGivenSearchCriteria(string searchValue, ISearchType searchType)
        {
            var reportData = new DataTable();

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                var command = prepareSqlCommand(dbConnection);
                command = searchType.addParameter(command, searchValue);

                reportData.Load(command.ExecuteReader());

                if (reportData.Rows.Count > 0)
                    return Convert.ToString(reportData.Rows[0]["Number"]);

                return "Not found";
            }
        }

        private static SqlCommand prepareSqlCommand(SqlConnection dbConnection)
        {
            dbConnection.Open();

            SqlCommand command = dbConnection.CreateCommand();

            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "SaleOrderGet";
            return command;
        }

        private interface ISearchType
        {
            SqlCommand addParameter(SqlCommand command, string searchValue);
        }

        private class CustomerNameSearch : ISearchType
        {
            #region ISearchType Members

            public SqlCommand addParameter(SqlCommand command, string searchValue)
            {
                command.Parameters.Add(new SqlParameter("@CustomerName", SqlDbType.VarChar) { Value = searchValue });
                return command;
            }

            #endregion
        }

        private class PostCodeSearch : ISearchType
        {
            #region ISearchType Members

            public SqlCommand addParameter(SqlCommand command, string searchValue)
            {
                command.Parameters.Add(new SqlParameter("@PostCode", SqlDbType.VarChar) { Value = searchValue });
                return command;
            }

            #endregion
        }

        private class PhoneNumberSearch : ISearchType
        {
            #region ISearchType Members

            public SqlCommand addParameter(SqlCommand command, string searchValue)
            {
                command.Parameters.Add(new SqlParameter("@TelephoneNumber", SqlDbType.VarChar) { Value = searchValue });
                return command;
            }

            #endregion
        }

        #endregion

        #region "TEST: End Of Day Check Showing Unscanned Bags"

        private DateTime _endOfDayCheckReportSelectedDate;

        public void endOfDayCheckReportSelectedDateIs(DateTime selectedDateIs)
        {
            _endOfDayCheckReportSelectedDate = selectedDateIs;
        }

        public int numberOfUnScannedBagsAre()
        {
            DataTable reportData = getEndOfDayCheckReport(_endOfDayCheckReportSelectedDate);

            return unscannedBagCount(ref reportData);

        }

        public string unscannedBagsAre()
        {
            DataTable reportData = getEndOfDayCheckReport(_endOfDayCheckReportSelectedDate);

            return unscannedBagSealsAre(ref reportData);
        }

        private DataTable getEndOfDayCheckReport(DateTime selectedDate)
        {
            var reportData = new DataTable();

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();
                var parameterDate = new SqlParameter("@BankingDate", SqlDbType.DateTime) { Value = selectedDate };

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "NewBankingEndOfDayCheckReport";
                command.Parameters.Add(parameterDate);
                reportData.Load(command.ExecuteReader());
            }

            return reportData;
        }

        private static int unscannedBagCount(ref DataTable dataTable)
        {
            int rowCount = 0;

            foreach (DataRow row in dataTable.Rows)
            {

                if ((int)row["RowId"] == 31 || (int)row["RowId"] == 51)
                {
                    rowCount += 1;
                }

            }
            return rowCount;
        }

        private static string unscannedBagSealsAre(ref DataTable dataTable)
        {
            string seals = string.Empty;

            foreach (DataRow row in dataTable.Rows)
            {

                if ((int)row["RowId"] == 31 || (int)row["RowId"] == 51)
                {
                    if (seals.Length != 0)
                    {
                        seals += " ";
                    }
                    seals += row["SealNumber"].ToString();
                }
            }
            return seals;
        }

        #endregion

        #region TEST: Cash drop dropdown

        public string[] cashDropDropdownHasCashiersForToday()
        {
            var cashierCollection = new CashierCollection();
            cashierCollection.LoadDataCashDrop(GetTodayAsDefinedBySystemUnderTest());

            var list = new string[cashierCollection.Count];

            foreach (Cashier cashier in cashierCollection)
            {
                list[cashierCollection.IndexOf(cashier)] = cashier.Employee;
            }

            return list;
        }

        #endregion

        #region "TEST: Banking File"

        public string[] bankingFileForAccountNumberHasValues(string outputFileName, string stringaccountNumber)
        {
            string[] outputBankingContent = File.ReadAllLines(outputFileLocation() + outputFileName);
            var endOfDaySafeValues = new List<string>();

            foreach (string line in outputBankingContent)
            {
                if (line.Substring(0, 2) == "A8" && line.Substring(22, 4) == stringaccountNumber)
                {
                    endOfDaySafeValues.Add(line.Substring(10, 12).Trim());
                }
            }
            return endOfDaySafeValues.ToArray();
        }

        public string bankingFileForAccountNumberDoesNotExist(string outputFileName, string stringaccountNumber)
        {
            return bankingFileForAccountNumberHasValues(outputFileName, stringaccountNumber).Length == 0 ? "true" : "false";
        }

        #endregion

        #region "Internals"

        private void createInitialSafe()
        {
            int periodIdForDate = getPeriodIdForDate(DateTime.Today);
            initialSafeCreated(periodIdForDate);
        }

        private void createInitialSafeForDate(DateTime date)
        {
            int periodIdForDate = getPeriodIdForDate(date);
            initialSafeCreated(periodIdForDate, date);
        }

        private void createInitialSafeWithThisPeriodId(int periodId)
        {
            initialSafeCreated(periodId);
        }

        private void initialSafeCreated(int periodIdForDate)
        {
            initialSafeCreated(periodIdForDate, DateTime.Today);
        }

        private void initialSafeCreated(int periodIdForDate, DateTime date)
        {
            var createInitialSafeOnDateCommand = new StringBuilder();
            const string dateFormat = "yyyy-MM-dd";

            createInitialSafeOnDateCommand.Append(
                "Insert Into Safe (PeriodID, PeriodDate, UserID1, UserID2, LastAmended, IsClosed, SafeChecked, EndOfDayCheckDone) Values(");
            createInitialSafeOnDateCommand.Append(periodIdForDate.ToString(CultureInfo.InvariantCulture));
            createInitialSafeOnDateCommand.Append(", '");
            createInitialSafeOnDateCommand.Append(date.ToString(dateFormat));
            createInitialSafeOnDateCommand.Append("', 0, 0, '");
            createInitialSafeOnDateCommand.Append(date.ToString(dateFormat));
            createInitialSafeOnDateCommand.Append("', 1, 1, 0)");

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand createInitialSafe = dbConnection.CreateCommand();
                createInitialSafe.CommandText = createInitialSafeOnDateCommand.ToString();

                createInitialSafe.ExecuteNonQuery();
            }
        }

        private void setSafeValue(decimal withValue)
        {
            setSafeValue(withValue, DateTime.Today);
        }

        private void setSafeValue(decimal withValue, DateTime date)
        {
            var setSafeValueCommand = new StringBuilder();

            int periodIdForDate = getPeriodIdForDate(date);
            const int cashTenderId = (int)TenderTypes.TenderTypeCash;

            setSafeValueCommand.Append(
                "Insert Into SafeDenoms (PeriodID, CurrencyID, TenderID, ID, SafeValue, ChangeValue, SystemValue, SuggestedValue) Values(");
            setSafeValueCommand.Append(periodIdForDate.ToString(CultureInfo.InvariantCulture));
            setSafeValueCommand.Append(", '");
            setSafeValueCommand.Append(CurrencyId);
            setSafeValueCommand.Append("', ");
            setSafeValueCommand.Append(cashTenderId.ToString(CultureInfo.InvariantCulture));
            setSafeValueCommand.Append(", .01, ");
            setSafeValueCommand.Append(withValue.ToString(CultureInfo.InvariantCulture));
            setSafeValueCommand.Append(", 0, ");
            setSafeValueCommand.Append(withValue.ToString(CultureInfo.InvariantCulture));
            setSafeValueCommand.Append(", 0)");

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand createInitialSafe = dbConnection.CreateCommand();
                createInitialSafe.CommandText = setSafeValueCommand.ToString();

                createInitialSafe.ExecuteNonQuery();
            }
        }

        #endregion

        #region "Data helpers"

        private int getPeriodIdForDate(DateTime onDate)
        {
            int periodId;

            var periodIdForDateCommand = new StringBuilder();
            const string dateFormat = "yyyy-MM-dd";

            periodIdForDateCommand.Append("Select Id From SystemPeriods Where StartDate >= '");
            periodIdForDateCommand.Append(onDate.ToString(dateFormat));
            periodIdForDateCommand.Append("' And EndDate < '");
            periodIdForDateCommand.Append(onDate.AddDays(1).ToString(dateFormat));
            periodIdForDateCommand.Append("'");

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand periodIdForDate = dbConnection.CreateCommand();
                periodIdForDate.CommandText = periodIdForDateCommand.ToString();

                var toParse = periodIdForDate.ExecuteScalar();
                periodId = int.Parse(toParse.ToString());
            }

            return periodId;
        }

        private DateTime getDateFromPeriodId(int periodId)
        {
            DateTime date;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand command = dbConnection.CreateCommand();

                command.CommandText = @"select StartDate from SystemPeriods where ID = " + periodId;

                date = (DateTime)command.ExecuteScalar();
            }

            return date;

        }

        private int getUserIdForName(string userName)
        {
            int userId;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                SqlCommand userIdCmd = dbConnection.CreateCommand();
                userIdCmd.CommandText = @"select [ID] from dbo.SystemUsers where [Name] = '" + userName + "'";

                var toParse = userIdCmd.ExecuteScalar();
                userId = int.Parse(toParse.ToString());
            }

            return userId;
        }

        private int getBagIdForSeal(string sealNumber)
        {
            int userId;

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                var userIdCmd = dbConnection.CreateCommand();
                userIdCmd.CommandText = @"select [ID] from dbo.SafeBags where [SealNumber] = '" + sealNumber + "'";

                var toParse = userIdCmd.ExecuteScalar();
                userId = int.Parse(toParse.ToString());
            }

            return userId;
        }

        private DataTable getDenominationBreakdownForBag(int bagId)
        {
            var data = new DataTable();

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                var denomCmd = dbConnection.CreateCommand();
                denomCmd.CommandText = @"select * from dbo.SafeBagsDenoms where [BagID] = " + bagId;

                data.Load(denomCmd.ExecuteReader());
            }

            return data;
        }

        private DataTable getSealedPickupBagsForSelectedPeriodId(int periodId)
        {
            var data = new DataTable();

            using (var dbConnection = new SqlConnection(systemEnvironment.GetOasysSqlServerConnectionString()))
            {
                dbConnection.Open();

                var pickupCmd = dbConnection.CreateCommand();
                pickupCmd.CommandText =
                    @"select [ID], [AccountabilityID] from dbo.SafeBags where [Type] = 'P' and [State] = 'S' and [PickupPeriodID] = " +
                    periodId;

                data.Load(pickupCmd.ExecuteReader());
            }

            return data;
        }

        private string outputFileLocation()
        {
            return systemEnvironment.GetCommsDirectoryPath();
        }

        #endregion
    }
}
