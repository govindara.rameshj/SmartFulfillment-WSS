using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Cts.Oasys.Core.Reporting;

namespace WSS.AAT.Common.Banking
{
    public class BankingReports
    {
        #region Cashier performance report

        public DataTable LoadCashierPerformance()
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("DateStart", DateTime.Today.AddDays(-1));
            dic.Add("DateEnd", DateTime.Today);

            var rep = Report.GetReport(611);

            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }

            rep.LoadData();
            return rep.Dataset.Tables[0];
        }

        public bool IsLineCreated(DataTable table)
        {
            return table.AsEnumerable().Any(row => row.Field<int>("CashierID") == 499);
        }

        public int GetTransactionOrderCount(DataTable table)
        {
            foreach (DataRow row in table.AsEnumerable())
                if (row.Field<int>("CashierID") == 499)
                    return row.Field<int>("NumTransactions");
            return 0;
        }

        #endregion

        #region Audit roll enquiry report

        public DataTable LoadAuditRollEnqury()
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("Date", DateTime.Today);

            var rep = Report.GetReport(310);
            rep.Parameters[0].Value = DateTime.Today;

            rep.LoadData();

            return rep.Dataset.Tables[0];
        }

        public bool IsClickAndCollectOrderPresentInAuditRollEnquryReport(DataTable table, string documentNumber)
        {
            return table.AsEnumerable().Any(row => (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Sale")));
        }

        public bool IsClickAndCollectRefundPresentInAuditRollEnquryReport(DataTable table, string documentNumber)
        {
            return table.AsEnumerable().Any(row => (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Refund")));
        }

        public string GetCashierNumberFromSale(DataTable table, string documentNumber)
        {
            foreach (DataRow row in table.AsEnumerable())
                if (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Sale"))
                    return row.Field<string>("CashierId");
            
            return string.Empty;
        }

        public string GetTillNumberFromSale(DataTable table, string documentNumber)
        {
            foreach (DataRow row in table.AsEnumerable())
                if (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Sale"))
                    return row.Field<string>("TillId");

            return string.Empty;
        }

        public string GetCashierNumberFromRefund(DataTable table, string documentNumber)
        {
            foreach (DataRow row in table.AsEnumerable())
                if (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Refund"))
                    return row.Field<string>("CashierId");

            return string.Empty;
        }

        public string GetTillNumberFromRefund(DataTable table, string documentNumber)
        {
            foreach (DataRow row in table.AsEnumerable())
                if (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Refund"))
                    return row.Field<string>("TillId");

            return string.Empty;
        }

        #endregion

        #region Customer order enquiry report

        public bool CheckOrderExistsInCustomerOrderEnquiry(DataTable dt, string orderId)
        {
            foreach (DataRow row in dt.Rows)
            {
                string numb = (string)row["NUMB"];
                if (numb.Equals(orderId))
                    return true;
            }
            return false;
        }

        public bool CheckRefundOrderExistsInCustomerOrderEnquiry(DataTable dt, string orderId)
        {
            foreach (DataRow row in dt.Rows)
            {
                string numb = (string)row["NUMB"];
                var qtyr = row["QTYR"];
                if (numb.Equals(orderId) && !qtyr.Equals(0))
                    return true;
            }
            return false;
        }

        public decimal GetOrderQuantityFromCustomerOrderEnquiry(DataTable dt, string orderId, string sku)
        {
            foreach (DataRow row in dt.Rows)
            {
                string numb = (string)row["NUMB"];
                string skun = (string)row["SKUN"];
                if (numb.Equals(orderId) && skun.Equals(sku))
                    return (decimal)row["QTYO"];
            }
            return 0;
        }

        public decimal GetRefundQuantityFromCustomerOrderEnquiry(DataTable dt, string orderId, string sku)
        {
            foreach (DataRow row in dt.Rows)
            {
                string numb = (string)row["NUMB"];
                string skun = (string)row["SKUN"];
                if (numb.Equals(orderId) && skun.Equals(sku))
                    return (decimal)row["QTYR"];
            }
            return 0;
        }

        #endregion

        #region Store sales report

        public DataTable LoalDataSetStoreSales()
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("DateEnd", DateTime.Today);

            var rep = Report.GetReport(1);

            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }

            rep.LoadData();
            return rep.Dataset.Tables[0];
        }

        public int GetTodayClickAndCollectSalesQuantity(DataTable table)
        {
            foreach (DataRow row in table.AsEnumerable())
                if (row.Field<string>("Description").Trim().Equals("C&C Sales"))
                    return int.Parse(row.Field<string>("Qty"));
            return 0;
        }

        public int GetTodayClickAndCollectRefundsQuantity(DataTable table)
        {
            foreach (DataRow row in table.AsEnumerable())
                if (row.Field<string>("Description").Trim().Equals("C&C Cancels/Returns"))
                    return int.Parse(row.Field<string>("Qty"));
            return 0;
        }

        #endregion
    }
}
