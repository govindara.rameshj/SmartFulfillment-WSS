﻿using Cts.Oasys.Core.Reporting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using WSS.AAT.Common.Utility.Utility;

namespace WSS.AAT.Common.Banking
{
    public class BankingReportRoutine
    {
        private DataSet LastCallResult;

        public void StartDebug()
        {
            Debugger.Launch();
        }

        public void LoadDataSetAuditRollEnqury()
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("Date", DateTime.Today);

            var rep = Report.GetReport(310);
            rep.Parameters[0].Value = DateTime.Today;

            rep.LoadData();

            LastCallResult = rep.Dataset;
        }

        public bool ISClickAndCollectOrderPresentInAuditRollEnquryReport(string documentNumber)
        {
            var table = LastCallResult.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");

            return table.Any(row => (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Sale")));
        }

        public bool ISClickAndCollectRefundPresentInAuditRollEnquryReport(string documentNumber)
        {
            var table = LastCallResult.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");

            return table.Any(row => (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Refund")));
        }

        public int GetCashierRefundNumber(string documentNumber)
        {
            var table = LastCallResult.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");
            foreach (DataRow row in table)
                if (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Refund"))
                    return int.Parse(row.Field<string>("CashierId"));
            return -1;
        }

        public int GetTillRefundNumber(string documentNumber)
        {
            var table = LastCallResult.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");
            foreach (DataRow row in table)
                if (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Refund"))
                    return int.Parse(row.Field<string>("TillId"));
            return -1;
        }

        public int GetCashierSaleNumber(string documentNumber)
        {
            var table = LastCallResult.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");
            foreach (DataRow row in table)
                if (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Sale"))
                    return int.Parse(row.Field<string>("CashierId"));
            return -1;
        }

        public int GetTillSaleNumber(string documentNumber)
        {
            var table = LastCallResult.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");
            foreach (DataRow row in table)
                if (row.Field<string>("DocumentNumber").Equals(documentNumber)
                && row.Field<string>("Type").Equals("Sale"))
                    return int.Parse(row.Field<string>("TillId"));
            return -1;
        }

        public void LoadDataSetCashierPerformance()
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("DateStart", DateTime.Today.AddDays(-1));
            dic.Add("DateEnd", DateTime.Today);

            var rep = Report.GetReport(611);

            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }

            rep.LoadData();
            LastCallResult = rep.Dataset;
        }

        public bool IsLineCreated()
        {
            var table = LastCallResult.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");
            return table.Any(row => row.Field<int>("CashierID") == 499);
        }

        public int GetTransactionOrderCount()
        {
            var table = LastCallResult.Tables[0].AsEnumerable();
            foreach (DataRow row in table)
                if (row.Field<int>("CashierID") == 499)
                    return row.Field<int>("NumTransactions");
            return 0;
        }

        public decimal GetTenderTypePayGiftCard(DateTime date, string tillID, string tranNum)
        {
            var ldate = date;

            var table = LastCallResult.Tables[2].AsEnumerable();

            foreach (DataRow row in table)
                if (row.Field<DateTime>("Date").Equals(ldate)
                    && row.Field<string>("TillId").Equals(tillID)
                    && row.Field<string>("TranNumber").Equals(tranNum))
                    return row.Field<decimal>("Type");

            return -1;
        }

        public string GetDescriptionTenderTypePayGiftCard(DateTime date, string tillID, string tranNum)
        {
            var ldate = date;

            var table = LastCallResult.Tables[2].AsEnumerable();

            foreach (DataRow row in table)
                if (row.Field<DateTime>("Date").Equals(ldate)
                    && row.Field<string>("TillId").Equals(tillID)
                    && row.Field<string>("TranNumber").Equals(tranNum))
                    return row.Field<string>("Description").Trim();

            return "Nothing";
        }
    }
}
