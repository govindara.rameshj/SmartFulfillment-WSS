using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Cts.Oasys.Core.Reporting;

namespace WSS.AAT.Common.Banking
{
    public class StockReports
    {
        private readonly string skuNumber;

        public StockReports(String skuNumber)
        {
            this.skuNumber = skuNumber;
        }

        public bool CheckDecodeComment(int tkey)
        {
            var dic = new Dictionary<String, String>
            {
                {"TKEY", tkey.ToString()}
            };

            var rep = Report.GetReport(84);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();
            var ds = rep.Dataset;
            var table = ds.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");
            Regex regex = new Regex(@"Date:\s*\d{2}/\d{2}/\d{2,44}\s*Till:\d{2}\s*Tran:\d{4}\s*Line Number:\d{1,6}\s*");
            return table.Any(row => regex.Match(row.Field<string>("KEYS")).Success);
        }

        public EnumerableRowCollection<DataRow> GetStockLogWeeklySummaryByStock()
        {
            var dic = new Dictionary<String, String>
            {
                {"SkuNumber", skuNumber}
            };

            var rep = Report.GetReport(73);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();
            var ds = rep.Dataset;
            var table = ds.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");

            return table;
        }

        public bool IsSalesMovementDone()
        {
            var table = GetStockLogWeeklySummaryByStock();

            return table.Any(row => row.Field<int>("QtySales") != 0);
        }

        public EnumerableRowCollection<DataRow> PicStockCount()
        {
            var dic = new Dictionary<String, String>
            {
                {"SkuNumber", skuNumber}
            };

            var rep = Report.GetReport(71);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();
            var ds = rep.Dataset;
            var table = ds.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");

            return table;
        }
    }
}
