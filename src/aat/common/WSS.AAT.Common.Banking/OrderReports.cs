using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BOViewer;
using BOSalesOrders;
using Cts.Oasys.Core.Reporting;

namespace WSS.AAT.Common.Banking
{
    public class OrderReports
    {
        public bool CheckOrderExistsInCustomerOrders(string orderNumber, string dsnName)
        {
            var viewConfig = new cViewerConfig(string.Format("DSN={0}", dsnName));
            viewConfig.Load(1);
            
            var flag = true;
            viewConfig.LoadRecords(ref flag);

            return viewConfig.BO.BORecords.Cast<cOrderHeader>().Any(x => x.OrderNo.Value == orderNumber);
        }

        public bool CheckOrderExistsInDeliveryIncomeDetail(string orderNumber)
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("DateStart", DateTime.Today.AddDays(-1));
            dic.Add("DateEnd", DateTime.Today);

            var rep = Report.GetReport(401);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();

            var table = rep.Dataset.Tables[0].AsEnumerable();
            return table.Any(row => row.Field<string>("OrderNumber") == orderNumber);
        }

        public bool CheckOrderExistsInDespatchedOrders(string orderNumber)
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("DateStart", DateTime.Today.AddDays(-1));
            dic.Add("DateEnd", DateTime.Today);

            var rep = Report.GetReport(403);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();

            var table = rep.Dataset.Tables[0].AsEnumerable();
            return table.Any(row => row.Field<string>("OrderNumber") == orderNumber);
        }

        public bool CheckOrderExistsInOrderDeliveryListing(string orderNumber)
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("DateStart", DateTime.Today.AddDays(-1));
            dic.Add("DateEnd", DateTime.Today);

            var rep = Report.GetReport(400);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();

            var table = rep.Dataset.Tables[0].AsEnumerable();
            return table.Any(row => row.Field<string>("OrderNumber") == orderNumber);
        }

        public bool CheckOrderExistsInOrdersAwaitingDespatch(string orderNumber)
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("DateStart", DateTime.Today.AddDays(-1));
            dic.Add("DateEnd", DateTime.Today);

            var rep = Report.GetReport(404);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();

            var table = rep.Dataset.Tables[0].AsEnumerable();
            return table.Any(row => row.Field<string>("OrderNumber") == orderNumber);
        }

        public bool CheckOrderExistsInTable(DataTable table)
        {
            return table.AsEnumerable().Any();
        }

        public bool CheckOrderExistsInTable(string orderNumber, DataTable table)
        {
            return table.AsEnumerable().Any(row => row.Field<string>("NUMB") == orderNumber);
        }

        public int GetTodayQuantityFromPendingOrders()
        {
            var dic = (new Dictionary<String, DateTime>());
            dic.Add("DateEnd", DateTime.Today);

            var rep = Report.GetReport(10);
            foreach (var prm in rep.Parameters)
            {
                prm.Value = dic[prm.Name];
            }
            rep.LoadData();

            var table = rep.Dataset.Tables[0].AsEnumerable();
            return table.Sum(r => (int)r["Today Qty"]);
        }

        public int GetTotalFromTable(DataTable table)
        {
            return Convert.ToInt32(table.Rows[0]["Total"]);
        }
    }
}
