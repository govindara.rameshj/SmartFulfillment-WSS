using System;
using Cts.Oasys.Core.Tests;
using WSS.AAT.Common.Qod.CtsOrderManager;
using WSS.AAT.Common.Qod.CtsTill;
using WSS.BO.DataLayer.Model;

namespace WSS.AAT.Common.Banking
{
    public class CnCBanking
    {
        private readonly IDataLayerFactory dalFactory;
        private readonly TestSystemEnvironment systemEnvironment;

        public CnCBanking(IDataLayerFactory dalFactory, TestSystemEnvironment systemEnvironment)
        {
            this.dalFactory = dalFactory;
            this.systemEnvironment = systemEnvironment;
        }

        public bool CreateCncOrderOfSku1Price1AndSku2Price2SoldOnReceivedOnAt(
            string sku1,
            decimal sku1Price,
            string sku2,
            decimal sku2Price,
            DateTime saleDate,
            DateTime receivedDateTime)
        {
            return CreateCncOrderOfSkusPricesSoldOnReceivedOnAt(
                new[] { sku1, sku2 },
                new[] { sku1Price, sku2Price },
                saleDate,
                receivedDateTime);
        }

        public bool CreateCncOrderOfSkuPriceSoldOnReceivedOnAt(
            string sku1,
            decimal sku1Price,
            DateTime saleDate,
            DateTime receivedDateTime)
        {
            return CreateCncOrderOfSkusPricesSoldOnReceivedOnAt(
                new[] { sku1 },
                new[] { sku1Price },
                saleDate,
                receivedDateTime);
        }

        private bool CreateCncOrderOfSkusPricesSoldOnReceivedOnAt(
            string[] skus,
            decimal[] prices,
            DateTime saleDate,
            DateTime receivedDateTime)
        {
            var routine = new CreationRoutine(dalFactory, systemEnvironment);
            routine.PrepareRequestForStandardClickAndCollectOrder();
            routine.CreateOrderLines(skus.Length);
            routine.SetSaleDate(saleDate);
            InitCncLines(routine, skus, prices);
            routine.SetReceivedDateAndTime(receivedDateTime);
            routine.ProcessPreparedRequest();

            var response = routine.GetResponse();
            bool isCreatedSuccesfully = response != null && response.IsSuccessful();
            return isCreatedSuccesfully;
        }

        private void InitCncLines(CreationRoutine routine, string[] skus, decimal[] prices)
        {
            if (skus.Length != prices.Length)
                throw new ArgumentException("Array lengths mismatch.");

            for (var i = 0; i < skus.Length; i++)
                routine.SetupOrderLine(i, skus[i], 1, prices[i]);
        }

        public void CreateTillSaleOfSku1Price1AndReversedSku2Price2SoldOn(
            decimal tender,
            string sku1,
            decimal sku1Price,
            string sku2,
            decimal sku2Price,
            DateTime saleDate)
        {
            var saleCreationRoutine = CreateSaleCreationRoutine();
            saleCreationRoutine.InitHeader(tender, saleDate, saleDate.AddHours(15), false, false);
            saleCreationRoutine.InitLine(sku1, 1, sku1Price, false);
            saleCreationRoutine.InitLine(sku2, 1, sku2Price, true);
            saleCreationRoutine.NewSale();
        }

        public void CreateVoidTillSaleOfSkuPriceSoldOn(
            decimal tender,
            string sku1,
            decimal sku1Price,
            DateTime saleDate)
        {
            var routine = CreateSaleCreationRoutine();
            routine.InitHeader(tender, saleDate, saleDate.AddHours(15), true, false);
            routine.InitLine(sku1, 1, sku1Price, false);
            routine.NewSale();
        }

        public void CreateTrainingTillSaleOfSkuPriceSoldOn(
            decimal tender,
            string sku1,
            decimal sku1Price,
            DateTime saleDate)
        {
            var routine = CreateSaleCreationRoutine();
            routine.InitHeader(tender, saleDate, saleDate.AddHours(15), false, true);
            routine.InitLine(sku1, 1, sku1Price, false);
            routine.NewSale();
        }

        public void CreateTillSaleOfSku1Price1AndSku2Price2SoldOn(
            decimal tender,
            string sku1,
            decimal sku1Price,
            string sku2,
            decimal sku2Price,
            DateTime saleDate)
        {
            CreateTillSaleOfSkusPricesSoldOn(
                tender,
                new[] { sku1, sku2 },
                new[] { sku1Price, sku2Price },
                saleDate);
        }

        public void CreateTillSaleOfSkuPriceSoldOn(
            decimal tender,
            string sku1,
            decimal sku1Price,
            DateTime saleDate)
        {
            CreateTillSaleOfSkusPricesSoldOn(
                tender,
                new[] { sku1 },
                new[] { sku1Price },
                saleDate);
        }

        private void CreateTillSaleOfSkusPricesSoldOn(
            decimal tender,
            string[] skus,
            decimal[] prices,
            DateTime saleDate)
        {
            var routine = CreateSaleCreationRoutine();
            routine.InitHeader(tender, saleDate, saleDate.AddHours(15), false, false);
            InitSaleLines(routine, skus, prices);
            routine.NewSale();
        }

        private void InitSaleLines(SaleCreationRoutine routine, string[] skus, decimal[] prices)
        {
            if (skus.Length != prices.Length)
                throw new ArgumentException("Array lengths mismatch.");

            for (var i = 0; i < skus.Length; i++)
            {
                routine.InitLine(skus[i], 1, prices[i], false);
            }
        }

        private SaleCreationRoutine CreateSaleCreationRoutine()
        {
            return new SaleCreationRoutine(dalFactory, systemEnvironment);
        }
    }
}
