using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Cts.Oasys.Core.Reporting;

namespace WSS.AAT.Common.Banking
{
    public class StockItemMovements
    {
        private readonly Report _report;

        public StockItemMovements(String sku)
            : this(sku, "1,2,3,4,5")
        {
        }

        public StockItemMovements(String sku, String groups)
        {
            _report = Report.GetReport(72);
            var paremeters = new Dictionary<String, String>
            {
                {"SkuNumber", sku},
                {"GroupIds", groups}
            };
            foreach (var parameter in _report.Parameters)
            {
                parameter.Value = paremeters[parameter.Name];
            }
            _report.LoadData();
        }

        public bool DescriptionExists(String description)
        {
            var table = _report.Dataset.Tables[0].AsEnumerable();
            if (!table.Any())
                throw new ApplicationException("Report didn't return data.");
            return table.Any(row => row.Field<string>("Description") == description.Trim());
        }
    }
}
