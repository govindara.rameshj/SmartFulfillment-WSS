using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace WSS.AAT.Common.Banking
{
    public class RetailSalesReport
    {
        private int salesCount = 0;
        private int refundCount = 0;

        public int GetRetailSalesCount()
        {
            return salesCount;
        }

        public int GetRetailSalesRefundsCount()
        {
            return refundCount;
        }

        public void GetRetailSalesReportByCashierIDSummary(string cashierId)
        {
            salesCount = 0;
            refundCount = 0;
            RetailSalesReports.DAL.DataAccessor acessor = new RetailSalesReports.DAL.DataAccessor("SpecFlow Test", "1.0");
            var reportRes = acessor.GetCashierSummary(DateTime.Today);


            var cashier = reportRes.Cashiers.Find(x => (x.Id == cashierId));
            
            if (cashier != null)
            {
                salesCount = cashier.SalesCount;
                refundCount = cashier.RefundCount;
            }           
         }

        public void GetRetailSalesReportByTillIDSummary(string tillId)
        {
            salesCount = 0;
            refundCount = 0;
            RetailSalesReports.DAL.DataAccessor acessor = new RetailSalesReports.DAL.DataAccessor("SpecFlow Test", "1.0");
            var reportRes = acessor.GetTillSummary(DateTime.Today);

            var till = reportRes.Tills.Find(x => (x.Id == tillId));

            if (till != null)
            {
                salesCount = till.SalesCount;
                refundCount = till.RefundCount;
            }        
        }       
        
        public void GetRetailSalesReportByCashierIDDetails(string cashierId)
        {
            salesCount = 0;
            refundCount = 0;
            int cashierID = Convert.ToInt32(cashierId);
            RetailSalesReports.DAL.DataAccessor acessor = new RetailSalesReports.DAL.DataAccessor("SpecFlow Test", "1.0");
            var reportRes = acessor.GetCashierDetails(DateTime.Today, cashierID);

            var operations = reportRes.DLTOTS;

            foreach (DataRow reportRow in operations.Rows)
            {
                switch (reportRow.Field<string>("TCOD"))
                {
                    case "SA":
                        salesCount++;
                        break;
                    case "RF": 
                        refundCount++;
                        break;
                }
            }                    
        }        

        public void GetRetailSalesReportByTillIDDetails(string reportTill)
        {
            salesCount = 0;
            refundCount = 0;
            int tillID = Convert.ToInt32(reportTill);
            RetailSalesReports.DAL.DataAccessor acessor = new RetailSalesReports.DAL.DataAccessor("SpecFlow Test", "1.0");
            var reportRes = acessor.GetTillDetails(DateTime.Today, tillID);

            var operations = reportRes.DLTOTS;

            foreach (DataRow reportRow in operations.Rows)
            {
                switch (reportRow.Field<string>("TCOD"))
                {
                    case "SA":
                        salesCount++;
                        break;
                    case "RF":
                        refundCount++;
                        break;
                }
            }        
        }        
    }
}
