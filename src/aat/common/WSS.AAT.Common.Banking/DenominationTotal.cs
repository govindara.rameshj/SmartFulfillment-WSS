using System;
using System.Collections.Generic;
using System.Linq;

namespace WSS.AAT.Common.Banking
{
    public class DenominationTotal
    {
        public static Dictionary<string, int> DenominationValues;

        public Dictionary<string, int> DenominationBreakDown;

        public Dictionary<decimal, decimal> Denominations;

        static DenominationTotal()
        {
            DenominationValues = new Dictionary<string, int>
            {
                {"\u00a3100", 10000},
                {"\u00a350", 5000},
                {"\u00a320", 2000},
                {"\u00a310", 1000},
                {"\u00a35", 500},
                {"\u00a31", 100},
                {"\u00a32", 200},
                {"50p", 50},
                {"20p", 20},
                {"10p", 10},
                {"5p", 5},
                {"2p", 2},
                {"1p", 1}
            };
        }

        public DenominationTotal(string denominationInput)
        {
            DenominationBreakDown = new Dictionary<string, int>();
            Denominations = new Dictionary<decimal, decimal>();
            Parse(denominationInput);
        }

        public decimal Total { get; set; }

        private void Parse(string denominationInput)
        {
            decimal runningTotal = 0M;

            int openIndex = denominationInput.IndexOf('(');

            // Get and parse total value
            Total = Decimal.Parse(denominationInput.Substring(0, openIndex - 1), System.Globalization.NumberStyles.Currency, new System.Globalization.CultureInfo("en-GB")) * 100;

            // Process each denomination in turn
            foreach (string amount in denominationInput.Substring(openIndex).Replace("(", "").Replace(")", "").Split(','))
            {
                string[] pair = amount.Split(':');

                if (DenominationValues.ContainsKey(pair[0]))
                {
                    // Get value and apply multiplier
                    DenominationBreakDown.Add(pair[0], Int32.Parse(pair[1]) * DenominationValues[pair[0]]);
                    decimal denomKey = (decimal)(DenominationValues[pair[0]]) / 100;
                    decimal denomValue = (decimal)(Int32.Parse(pair[1]) * DenominationValues[pair[0]]) / 100;
                    Denominations.Add(denomKey, denomValue);
                    runningTotal += DenominationBreakDown[pair[0]];
                }
            }

            if (runningTotal != Total)
            {
                throw new Exception("Denomination total mismatch");
            }
        }

        private static string ParseDenominationToString(decimal denomination)
        {
            if (denomination >= 1)
            {
                return "\u00a3" + ((int)denomination).ToString();
            }
            return ((int)(denomination * 100)).ToString() + "p";
        }

        private static string ParseValueToString(decimal denomination, decimal value)
        {
            return (value / denomination).ToString();
        }

        public decimal TotalConvertedToPounds()
        {
            return Total / 100;
        }

        #region "Parse Safe to String"

        public static string ParseSafeToString(Dictionary<decimal, decimal> safeValues)
        {

            string output = string.Empty;

            foreach (KeyValuePair<decimal, decimal> denom in safeValues)
            {
                if (output.Length != 0)
                {
                    output += ",";
                }
                output += ParseDenominationToString(denom.Key) + ":" + ParseValueToString(denom.Key, denom.Value);
            }

            return ParseSafeValeToString(safeValues) + " (" + output + ")";
        }

        private static string ParseSafeValeToString(Dictionary<decimal, decimal> safeValues)
        {
            Decimal total = safeValues.Sum(denomValue => denomValue.Value);

            string safeVale = "\u00a3" + total;
            return safeVale.Replace(".00", "");
        }

        #endregion

    }
}
