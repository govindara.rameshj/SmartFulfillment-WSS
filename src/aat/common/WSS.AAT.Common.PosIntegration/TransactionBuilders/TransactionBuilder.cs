﻿using System;
using System.Diagnostics;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.Model.Entity.Transactions;
using PriceOverrideReasonCode = WSS.BO.Model.Enums.PriceOverrideReasonCode;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public abstract class TransactionBuilder
    {
        protected const string DefaultTillNumber = "1";
        protected const string DefaultCashierNumber = "18";
        protected const string DefaultDocumentNumber = "00000000";
        protected const decimal DefaultLineAmount = 295;
        protected const decimal DefaultInitialPrice = 33.97M;
        protected const PriceOverrideReasonCode DefaultPriceOverrideCode = PriceOverrideReasonCode.DAMAGED_GOODS;
        protected const int DefaultStoreNumber = 8852;
        protected const decimal DefaultPaymentAmount = 169.85M;

        protected readonly IDataLayerFactory DlFactory;
        public readonly DateTime CreationDateTime; //YES, ITS READONLY. DONT CHANGE IT!
        protected Transaction Transaction { get { return _transaction.Value; }}

        private readonly Lazy<Transaction> _transaction;

        protected TransactionBuilder(IDataLayerFactory dlFactory, DateTime saleDate)
        {
            DlFactory = dlFactory;
            CreationDateTime = saleDate;
            _transaction = new Lazy<Transaction>(CreateStubTransaction); 
        }

        protected virtual Transaction CreateStubTransaction()
        {
            var transaction = CreateEmptyTransaction();
            transaction.BrandCode = Global.BrandCode;
            transaction.CreateDateTime = CreationDateTime;
            transaction.TillNumber = DefaultTillNumber;
            transaction.TransactionNumber =
                DlFactory.Create<TestDailyTillRepository>().GetNextSourceTransactionId(CreationDateTime, DefaultTillNumber.PadLeft(2,'0'));
            transaction.ReceiptBarcode = "111222333444555";
            transaction.CashierNumber = DefaultCashierNumber;
            transaction.SourceTransactionId = Guid.NewGuid().ToString();
            transaction.Source = "OVC";
            transaction.StoreNumber = DefaultStoreNumber.ToString();
            transaction.IsTraining = false;
            return transaction;
        }

        protected abstract Transaction CreateEmptyTransaction();

        public Transaction GetFullyPreparedTransaction()
        {
            PrepareFinalTransaction();
            return Transaction;
        }

        protected virtual void PrepareFinalTransaction()
        {
            
        }

        public void SetTransactionNumber(string transactionNumber)
        {
            Transaction.TransactionNumber = transactionNumber;
        }

        public void SetSource(string source)
        {
            Transaction.Source = source;
        }

        public void SetSourceTransactionId(string sourceTransactionId)
        {
            Transaction.SourceTransactionId = sourceTransactionId;
        }

        public void SetReceiptBarcode(string receiptBarcode)
        {
            Transaction.ReceiptBarcode = receiptBarcode;
        }

        public void SetTillNumber(string tillNumber)
        {
            Transaction.TillNumber = tillNumber;
        }

        public void SetCashierNumber(string cashierNumber)
        {
            Transaction.CashierNumber = cashierNumber;
        }

        public void SetTransactionIsTraining()
        {
            Transaction.IsTraining = true;
        }

        public string GetNextSourceTransactionId(DateTime creationDateTime)
        {
            return DlFactory.Create<TestDailyTillRepository>().GetNextSourceTransactionId(creationDateTime, DefaultTillNumber.PadLeft(2, '0'));
        }

        private string giftCardSkuNumber;

        public string GetGiftCardSkuNumber()
        {
            // ReSharper disable once ConvertIfStatementToNullCoalescingExpression
            if (giftCardSkuNumber == null)
            {
                giftCardSkuNumber = DlFactory.Create<IDictionariesRepository>().GetSettingStringValue(ParameterId.GiftCardSku);
            }
            return giftCardSkuNumber;
        }

        public string GetTillNumber()
        {
            return Transaction.TillNumber;
        }

        [DebuggerStepThrough]
        protected T GetTypedTransaction<T>()
            where T : Transaction
        {
            return (T)Transaction;
        }
    }
}
