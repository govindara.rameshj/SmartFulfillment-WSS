﻿using System;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public class MiscInTransactionBuilder : MiscellaneousTransactionBuilder
    {
        public MiscInTransactionBuilder(IDataLayerFactory dlFactory, DateTime saleDate)
            : base(dlFactory, saleDate)
        {
        }

        protected override Transaction CreateStubTransaction()
        {
            var tran = (MiscInTransaction)base.CreateStubTransaction();

            tran.ReasonCode = 0;

            return tran;
        }

        protected override Transaction CreateEmptyTransaction()
        {
            return new MiscInTransaction();
        }

        public void SetReasonCode(MiscInReasonCode reasonCode)
        {
            ((MiscInTransaction)Transaction).ReasonCode = reasonCode;
        }
    }
}
