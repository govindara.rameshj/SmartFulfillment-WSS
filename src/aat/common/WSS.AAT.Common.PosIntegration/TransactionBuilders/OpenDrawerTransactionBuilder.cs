﻿using System;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public class OpenDrawerTransactionBuilder : VoidableTransactionBuilder
    {
        public OpenDrawerTransactionBuilder(IDataLayerFactory dlFactory, DateTime saleDate)
            : base(dlFactory, saleDate)
        {
        }

        protected override Transaction CreateStubTransaction()
        {
            var tran = (OpenDrawerTransaction)base.CreateStubTransaction();
            return tran;
        }

        protected override Transaction CreateEmptyTransaction()
        {
            return new OpenDrawerTransaction();
        }
    }
}
