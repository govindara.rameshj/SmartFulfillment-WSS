﻿using System;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Entity.Transactions;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public abstract class MiscellaneousTransactionBuilder : PayableTransactionBuilder
    {
        protected MiscellaneousTransactionBuilder(IDataLayerFactory dlFactory, DateTime saleDate)
            : base(dlFactory, saleDate)
        {
        }

        protected override Transaction CreateStubTransaction()
        {
            var tran = (MiscellaneousTransaction)base.CreateStubTransaction();

            tran.InvoiceNumber = DefaultDocumentNumber;

            return tran;
        }

        public void SetInvoiceNumber(string number)
        {
            ((MiscellaneousTransaction) Transaction).InvoiceNumber = number;
        }
    }
}
