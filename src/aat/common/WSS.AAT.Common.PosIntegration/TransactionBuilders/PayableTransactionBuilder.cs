using System;
using System.Collections.Generic;
using System.Linq;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Constants;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Payments;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public abstract class PayableTransactionBuilder : VoidableTransactionBuilder
    {
        protected PayableTransactionBuilder(IDataLayerFactory dlFactory, DateTime saleDate)
            : base(dlFactory, saleDate)
        {
        }

        protected override Transaction CreateStubTransaction()
        {
            var tran = (PayableTransaction)base.CreateStubTransaction();

            tran.Payments = new List<Payment>();
            tran.TotalAmount = 169.85M;

            return tran;
        }

        public void AddGiftCardPaymentToTransaction(string cardNumber, decimal amount, Direction direction)
        {
            GiftCardPayment payment = CreateStubGiftCardPayment();

            payment.GiftCardOperation.CardNumber = cardNumber;

            payment.AbsAmount = amount;
            payment.Direction = direction;

            var tran = GetTypedTransaction<PayableTransaction>();
            tran.Payments.Add(payment);
        }

        public void AddPaymentToTransaction(decimal amount, int tenderType, Direction direction)
        {
            Payment payment;
            switch (tenderType)
            {
                case TenderType.Cash:
                    {
                        payment = CreateStubCashTransactionPayment();
                        break;
                    }

                case TenderType.Cheque:
                    {
                        payment = CreateStubChequePayment();
                        break;
                    }

                case TenderType.GiftCard:
                    {
                        payment = CreateStubGiftCardPayment();
                        break;
                    }

                case TenderType.AmericanExpress:
                    {
                        payment = CreateStubCardPayment();
                        ((EftPayment)payment).PaymentCard.CardType = CardType.AMERICAN_EXPRESS;
                        break;
                    }

                case TenderType.CreditCard:
                    {
                        payment = CreateStubCardPayment();
                        ((EftPayment)payment).PaymentCard.CardType = CardType.VISA;
                        break;
                    }

                case TenderType.Change:
                    {
                        payment = CreateStubCashChangePayment();
                        break;
                    }

                default:
                    {
                        throw new ArgumentException("Unknown tender type");
                    }
            }

            payment.AbsAmount = amount;
            payment.Direction = direction;

            var tran = GetTypedTransaction<PayableTransaction>();
            tran.Payments.Add(payment);
        }

        public void SetIssueNumberSwitchForCardPayment(string issueNumberSwitch = null)
        {
            var tran = GetTypedTransaction<PayableTransaction>();
            var cardPayment = tran.Payments.OfType<EftPayment>().First();
            cardPayment.PaymentCard.IssueNumberSwitch = issueNumberSwitch;
        }

        #region Payment creation

        private CashPayment CreateStubCashTransactionPayment()
        {
            var payment = new CashPayment()
            {
                AbsAmount = DefaultPaymentAmount,
                Direction = Direction.FROM_CUSTOMER,
            };

            return payment;
        }

        private ChequePayment CreateStubChequePayment()
        {
            var payment = new ChequePayment()
            {
                AbsAmount = DefaultPaymentAmount
            };

            return payment;
        }

        private GiftCardPayment CreateStubGiftCardPayment()
        {
            var payment = new GiftCardPayment()
            {
                AbsAmount = DefaultPaymentAmount,
                CardDescription = "Barclaycard gift",
                CardAcceptType = CardAcceptType.SWIPED,
                GiftCardOperation = new GiftCardOperation()
                {
                    AuthCode = DlFactory.Create<TestMiscRepository>().CalculateAuthCodeForGiftCard(),
                    CardNumber = "000000000######0000"
                }
            };

            return payment;
        }

        private ChangePayment CreateStubCashChangePayment()
        {
            var payment = new ChangePayment()
            {
                AbsAmount = DefaultPaymentAmount
            };
            return payment;
        }

        private EftPayment CreateStubCardPayment()
        {
            var payment = new EftPayment()
            {
                AbsAmount = DefaultPaymentAmount,
                AuthCode = "005430",
                AuthDescription = "ICC",
                CardAcceptType = CardAcceptType.KEYED,
                PaymentCard = new PaymentCard()
                {
                    CardDescription = "Visa Debit",
                    CardExpireDate = new DateTime(2012, 12, 4),
                    CardType = CardType.VISA,
                    IssueNumberSwitch = "0",
                    MaskedCardNumber = "000******######1853",
                    CardStartDate = new DateTime(2009, 12, 4),
                    PanHash = "piXNODQoDKGFqBAMvs44CuFJAIM="
                },
                MerchantNumber = "2262434",
                TransactionUid = "2780",
                TokenId = "554329902"
            };

            return payment;
        }

        #endregion

        #region Final preparations

        protected override void PrepareFinalTransaction()
        {
            base.PrepareFinalTransaction();
            CheckTransactionForPayment();
            SetupTransactionTotalAmount();
        }

        /// <summary>
        /// Adds stub payment in transaction if it has no payments and transaction is not parked.
        /// </summary>
        public void CheckTransactionForPayment()
        {
            var tran = GetTypedTransaction<PayableTransaction>();
            if (tran.TransactionStatus == TransactionStatus.COMPLETED)
            {
                if (!tran.Payments.Any())
                {
                    var payment = CreateStubCashTransactionPayment();
                    payment.Direction = tran.TotalAmount < 0 ? Direction.TO_CUSTOMER : Direction.FROM_CUSTOMER;
                    tran.Payments.Add(payment);
                }
            }
        }

        private void SetupTransactionTotalAmount()
        {
            var totalTranAmount = CalculateValidTotalTranAmount();

            var tran = GetTypedTransaction<PayableTransaction>();
            if (tran.TransactionStatus == TransactionStatus.COMPLETED)
            {
                var totalPaymentsAmount = tran.Payments.Sum(x => x.Direction == Direction.FROM_CUSTOMER ? x.AbsAmount : -x.AbsAmount);

                var delta = totalTranAmount - totalPaymentsAmount;
                if (delta != 0)
                {
                    var payment = tran.Payments.First();
                    var amount = payment.GetSignedAmount();
                    var correctedAmount = amount + delta;
                    payment.AbsAmount = Math.Abs(correctedAmount);
                    payment.Direction = correctedAmount < 0 ? Direction.TO_CUSTOMER : Direction.FROM_CUSTOMER;
                }
            }

            tran.TotalAmount = totalTranAmount;
        }

        protected virtual decimal CalculateValidTotalTranAmount()
        {
            // by default the correct amount is payments amount
            return GetTypedTransaction<PayableTransaction>().CalculatePaymentsAmount();
        }

        #endregion
    }
}
