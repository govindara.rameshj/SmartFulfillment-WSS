﻿using System;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public abstract class VoidableTransactionBuilder : TransactionBuilder
    {
        protected VoidableTransactionBuilder(IDataLayerFactory dlFactory, DateTime saleDate) : base (dlFactory, saleDate)
        {
        }

        protected override Transaction CreateStubTransaction()
        {
            var tran = (VoidableTransaction)base.CreateStubTransaction();

            tran.AuthorizerNumber = null;
            tran.TransactionStatus = TransactionStatus.COMPLETED;
            tran.VoidSupervisor = null;

            return tran;
        }

        public void SetTransactionStatus(TransactionStatus status)
        {
            ((VoidableTransaction)Transaction).TransactionStatus = status;
        }
    }
}
