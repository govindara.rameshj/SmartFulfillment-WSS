using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Cts.Oasys.Core.Helpers;
using WSS.AAT.Common.DataLayer;
using WSS.BO.DataLayer.Model;
using WSS.BO.DataLayer.Model.Repositories;
using WSS.BO.Model.Entity;
using WSS.BO.Model.Entity.Fulfillments;
using WSS.BO.Model.Entity.Products;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;
using RefundReasonCode = WSS.BO.Model.Enums.RefundReasonCode;
using WSS.BO.Model.Entity.Discounts;
using PriceOverrideReasonCode = WSS.BO.Model.Enums.PriceOverrideReasonCode;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public class RetailTransactionBuilder : PayableTransactionBuilder
    {
        private readonly bool isRefund;

        public RetailTransactionBuilder(IDataLayerFactory dlFactory, bool isRefund, DateTime saleDate)
            : base(dlFactory, saleDate)
        {
            this.isRefund = isRefund;
        }

        #region Interface

        public RetailTransactionItem AddTransactionItem(int quantity, string skuNumber, Direction direction, decimal vatRate = 20)
        {
            var transactionItem = CreateStubTransactionItem();
            FillTransactionItem(transactionItem, quantity, skuNumber, direction, vatRate);
            InnerAddTransactionItem(transactionItem);
            return transactionItem;
        }

        public void AddTransactionItem(int quantity, string skuNumber, int sourceLineNumber, string sourceItemId, Direction direction, decimal vatRate = 20)
        {
            var transactionItem = CreateStubTransactionItem();
            FillTransactionItem(transactionItem, quantity, skuNumber, direction, vatRate);
            transactionItem.SourceItemId = sourceItemId;
            InnerAddTransactionItem(transactionItem, sourceLineNumber);
        }

        public void AddTransactionItem(int quantity, string skuNumber, string sourceItemId, Direction direction, decimal vatRate = 20)
        {
            var transactionItem = CreateStubTransactionItem();
            FillTransactionItem(transactionItem, quantity, skuNumber, direction, vatRate);
            transactionItem.SourceItemId = sourceItemId;
            InnerAddTransactionItem(transactionItem);
        }

        public void AddTransactionItem(int quantity, string skuNumber, int sourceLineNumber, Direction direction, decimal vatRate = 20)
        {
            var transactionItem = CreateStubTransactionItem();
            FillTransactionItem(transactionItem, quantity, skuNumber, direction, vatRate);
            InnerAddTransactionItem(transactionItem, sourceLineNumber);
        }

        public void AddGiftCardTransactionItem(decimal amount)
        {
            var transactionItem = CreateStubGiftCardTransactionItem(amount);
            InnerAddTransactionItem(transactionItem);
        }

        public void AddDeliveryChargeTransactionItem(decimal amount)
        {
            var transactionItem = CreateStubDeliveryChargeTransactionItem(amount);
            InnerAddTransactionItem(transactionItem);
        }

        public void AddRefundItemWithReasonCode(int quantity, string skuNumber, string refundedTranId, string reason, string refundedItemId = null)
        {
            var item = AddTransactionItem(quantity, skuNumber, Direction.FROM_CUSTOMER);
            item.ItemRefund = new ItemRefund
            {
                RefundReasonCode = EnumParser.Parse<RefundReasonCode>(reason, "_"),
                RefundedTransactionId = refundedTranId,
                RefundedTransactionItemId = refundedItemId
            };
        }

        public void AddCustomer(string customerName)
        {
            var contact = CreateStubCustomer();
            contact.Name = customerName;

            Transaction.Contacts = new List<ContactInfo>
            {
                contact
            };

            Transaction.Customer = new Customer { ContactInfoIndex = 0 };
        }

        public void AddContactForOrder()
        {
            var contact = CreateSecondStubCustomer();

            Transaction.Contacts.Add(contact);
            ((OrderFulfillment) Transaction.Fulfillments.First(x => x is OrderFulfillment)).ContactInfoIndex = 1;
        }

        public void SetEmployeeDiscount(string skuNumber, decimal discountAmount, string supervisorNumber = null)
        {
            var skuProduct = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>().FirstOrDefault(x => x.BrandProductId == skuNumber);

            if (skuProduct.Discounts == null)
            {
                skuProduct.Discounts = new List<Discount>();
            }

            skuProduct.Discounts.Add(
                new EmployeeDiscount()
                {
                    CardNumber = "9611059830311217",
                    SavingAmount = discountAmount,
                    SupervisorNumber = supervisorNumber
                });
        }

        public void SetItemPrice(string skuNumber, decimal price)
        {
            var skuProduct = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>().FirstOrDefault(x => x.BrandProductId == skuNumber);

            if (skuProduct != null)
            {
                skuProduct.InitialPrice = price;
            }
        }

        public void SetItemRefundedTransactionId(string skuNumber, string refundedTransactionId)
        {
            var item = Transaction.Items.Where(x => x.Product is SkuProduct && ((SkuProduct)x.Product).BrandProductId == skuNumber).FirstOrDefault();

            if (item != null)
            {
                item.ItemRefund.RefundedTransactionId = refundedTransactionId;
            }
        }

        public void SetSurveyPostcode(string surveyPostcode)
        {
            Transaction.SurveyPostcode = surveyPostcode;
        }

        public void ReverseLine(string skuNumber)
        {
            var item = FindItem<SkuProduct>(x => x.BrandProductId == skuNumber);
            item.IsReversed = true;
        }

        public void ReverseGiftCardLine()
        {
            var item = FindItem<GiftCardProduct>();
            item.IsReversed = true;
            item.GetTypedProduct<GiftCardProduct>().GiftCardOperation = null;
        }

        public void AddRefundLineWithSourceInformation(int quantity, string skuNumber, string refundedTransactionItemId, string refundedTranId)
        {
            var transactionItem = CreateStubTransactionItem();
            transactionItem.ItemRefund = new ItemRefund
            {
                RefundedTransactionItemId = refundedTransactionItemId,
                RefundedTransactionId = refundedTranId,
            };

            FillTransactionItem(transactionItem, quantity, skuNumber, Direction.FROM_CUSTOMER);
            InnerAddTransactionItem(transactionItem);
        }

        public void AddRefundLineWithSourceInformationAndCancelledQuantity(int quantity, string skuNumber,
            int cancelledQuantity, string refundedSourceTranId, string refundedTransactionItemId = null)
        {
            var transactionItem = CreateStubTransactionItem();
            transactionItem.ItemRefund = new ItemRefund
            {
                RefundedTransactionItemId = refundedTransactionItemId,
                RefundedTransactionId = refundedSourceTranId,
            };
            FillTransactionItem(transactionItem, quantity, skuNumber, Direction.FROM_CUSTOMER);
            InnerAddTransactionItem(transactionItem);

            AddCancellationItem(cancelledQuantity, transactionItem.ItemIndex);
        }

        public FulfillmentItem AddFulfillmentItem<T>(int quantity, int lineNumber) where T : Fulfillment
        {
            var newFulfillmentItem = new FulfillmentItem()
            {
                AbsQuantity = quantity,
                TransactionItemIndex = lineNumber
            };

            var fulfillment = Transaction.Fulfillments.FirstOrDefault(x => x is T);

            if (fulfillment == null)
            {
                fulfillment = CreateFulfillment<T>();
                Transaction.Fulfillments.Add(fulfillment);
            }
            fulfillment.Items.Add(newFulfillmentItem);

            return newFulfillmentItem;
        }

        public void AddSlotToDeliveryFulFillment(string slotId, string slotDescription, TimeSpan slotStartTime, TimeSpan slotEndTime)
        {
            var deliveryFulfillment = Transaction.Fulfillments.OfType<DeliveryFulfillment>().First();

            deliveryFulfillment.DeliverySlot = new DeliverySlot()
            {
                Id = slotId,
                Description = slotDescription,
                StartTime = slotStartTime,
                EndTime = slotEndTime
            };
        }

        public void AddRequiredFulfiller(int fulfiller)
        {
            var fulfillment = Transaction.Fulfillments.First(x => x is DeliveryFulfillment);
            foreach (var item in fulfillment.Items)
            {
                item.FulfillerNumber = fulfiller.ToString();
            }
        }

        public void AddDeliveryInstruction(string instruction)
        {
            var deliveryFulfillment = Transaction.Fulfillments.OfType<DeliveryFulfillment>().First();

            if (deliveryFulfillment.DeliveryInstructions == null)
            {
                deliveryFulfillment.DeliveryInstructions = new List<string>();
            }

            deliveryFulfillment.DeliveryInstructions.Add(instruction);
        }

        public void AddRestrictedItem(RestrictedItemAcceptType restrictedItemAcceptedType, string skuNumber, RestrictionType restrictionType, string authorizerNumber = null)
        {
            if (Transaction.RestrictedItems == null)
            {
                Transaction.RestrictedItems = new List<RestrictedItem>();
            }

            Transaction.RestrictedItems.Add(new RestrictedItem() { BrandProductId = skuNumber, AcceptType = restrictedItemAcceptedType, AuthorizerNumber = authorizerNumber, RestrictionType = restrictionType});
        }

        public void AddConfirmationTextToRestrictedItem(string skuNumber, string confirmationText)
        {
            var restrictedItem = Transaction.RestrictedItems.FirstOrDefault(x => x.BrandProductId == skuNumber);
            restrictedItem.Confirmation = confirmationText;
        }

        public void SetRecalledTransactionId(string tranId)
        {
            Transaction.RecalledTransactionId = tranId;
        }

        public void MoveDeliveryDateToNextDow(DayOfWeek dow)
        {
            var deliveryFulfillment = Transaction.Fulfillments.OfType<DeliveryFulfillment>().First();
            deliveryFulfillment.DeliveryDate = GetWeekDayAfterDate(dow, deliveryFulfillment.DeliveryDate);
        }

        public void ResetDeliverySlot()
        {
            var deliveryFulfillment = Transaction.Fulfillments.OfType<DeliveryFulfillment>().First();
            deliveryFulfillment.DeliverySlot = null;
        }

        public void AddPriceOverrideDiscountByNewPrice(string skuNumber, decimal newPrice, PriceOverrideReasonCode priceOverrideCode, string supervisorNumber = null)
        {
            var skuProducts = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>();

            var skuProduct = skuProducts.First(x => x.BrandProductId == skuNumber);

            if (skuProduct.Discounts == null)
            {
                skuProduct.Discounts = new List<Discount>();
            }

            skuProduct.Discounts.Add(
                new PriceOverrideDiscount()
                {
                    SavingAmount = (skuProduct.InitialPrice - newPrice) * skuProduct.AbsQuantity,
                    PriceOverrideCode = priceOverrideCode,
                    SupervisorNumber = supervisorNumber
                }
            );
        }

        public void AddPriceOverrideDiscountBySavingAmount(string skuNumber, decimal savingAmount, PriceOverrideReasonCode priceOverrideCode, string supervisorNumber = null)
        {
            var skuProducts = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>();

            var skuProduct = skuProducts.First(x => x.BrandProductId == skuNumber);

            if (skuProduct.Discounts == null)
            {
                skuProduct.Discounts = new List<Discount>();
            }

            skuProduct.Discounts.Add(
                new PriceOverrideDiscount()
                {
                    SavingAmount = savingAmount,
                    PriceOverrideCode = priceOverrideCode,
                    SupervisorNumber = supervisorNumber
                }
            );
        }

        public void AddPriceMatchDiscount(string skuNumber, decimal newPrice, string competitorName, string supervisorNumber = null)
        {
            var skuProducts = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>();

            var skuProduct = skuProducts.First(x => x.BrandProductId == skuNumber);

            if (skuProduct.Discounts == null)
            {
                skuProduct.Discounts = new List<Discount>();
            }

            skuProduct.Discounts.Add(
                new PriceMatchDiscount()
                {
                    SavingAmount = (skuProduct.InitialPrice - newPrice)*skuProduct.AbsQuantity,
                    CompetitorName = competitorName,
                    NewPrice = newPrice,
                    IsVATIncluded = true,
                    SupervisorNumber = supervisorNumber
                }
           );
        }

        public void AddEventDiscountToSku(EventType eventType, decimal eventAmount, string code, string skuNumber = null)
        {
            SkuProduct skuProduct;

            if (string.IsNullOrEmpty(skuNumber))
            {
                skuProduct = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>().FirstOrDefault();
            }
            else
            {
                skuProduct = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>().First(x => x.BrandProductId == skuNumber);
            }

            AddEventDiscountToSkuProduct(skuProduct, eventType, eventAmount, code);
        }

        public void AddEventDiscountToSku(EventType eventType, decimal eventAmount, string code, int lineNumber)
        {
            AddEventDiscountToSkuProduct(Transaction.Items.Where(x => x.ItemIndex == lineNumber).Select(x => x.Product).OfType<SkuProduct>().FirstOrDefault(), eventType, eventAmount, code);
        }

        private void AddEventDiscountToSkuProduct(SkuProduct skuProduct, EventType eventType, decimal eventAmount, string code)
        {
            if (skuProduct.Discounts == null)
            {
                skuProduct.Discounts = new List<Discount>();
            }

            if (skuProduct.Discounts.OfType<EventDiscount>().Any(x => x.EventDescriptor.EventCode == code))
            {
                var discountEvent = skuProduct.Discounts.OfType<EventDiscount>().FirstOrDefault(x => x.EventDescriptor.EventCode == code);
                discountEvent.HitAmounts.Add(eventAmount);
            }
            else
            {
                skuProduct.Discounts.Add(
                    new EventDiscount()
                    {
                        EventDescriptor = new EventDescriptor() { EventCode = code, EventType = eventType },
                        HitAmounts = new List<decimal>() { eventAmount }
                    });
            }
        }

        public void AddCoupon(string eventCode, string barcode)
        {
            var skuProductWithEvent = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>()
                .FirstOrDefault(x => x.Discounts != null && x.Discounts.OfType <EventDiscount>().Any(y => y.EventDescriptor.EventCode == eventCode));

            if (skuProductWithEvent != null)
            {
                var eventDescriptor = skuProductWithEvent.Discounts.OfType<EventDiscount>().First(y => y.EventDescriptor.EventCode == eventCode).EventDescriptor;

                if (Transaction.Coupons == null)
                {
                    Transaction.Coupons = new List<Coupon>();
                }

                Transaction.Coupons.Add(new Coupon() { EventDescriptor = eventDescriptor, CouponBarcode = barcode });
            }
        }

        public void SetManagerId(string managerId)
        {
            Transaction.Manager = managerId;
        }

        public void SetStockType(string skuNumber, bool isMarkdown)
        {
            var skuProduct = Transaction.Items.Select(x => x.Product).OfType<SkuProduct>().FirstOrDefault(x => x.BrandProductId == skuNumber);

            if (skuProduct != null)
            {
                skuProduct.StockType = isMarkdown ? StockType.MARKDOWN : StockType.NORMAL;
            }
        }

        #endregion

        #region Final preparations

        protected override void PrepareFinalTransaction()
        {
            CheckTransactionForItem(); //need to be first before base to calcualte tital amount correctly

            base.PrepareFinalTransaction();
            CheckTransactionForContacts();
            RecalculateAbsVatItemAmounts();
            SetupOtherTotalsForTransaction();
            CheckTransactionForFulfillments();
        }

        protected override decimal CalculateValidTotalTranAmount()
        {
            decimal total = 0;

            foreach (var item in Transaction.Items.Where(x => !x.IsReversed))
            {
                if (item.Product is SkuProduct)
                {
                    var skuProduct = item.Product as SkuProduct;
                    total += (skuProduct.AbsQuantity*skuProduct.InitialPrice - 
                        (skuProduct.Discounts != null ? skuProduct.Discounts.OfType<AmountDiscount>().Sum(x => x.SavingAmount) : 0) - skuProduct.CalculateEventsDiscount())
                        * (item.Direction == Direction.TO_CUSTOMER ? 1 : -1);
                }
                else if (item.Product is GiftCardProduct)
                {
                    var giftCardProduct = item.Product as GiftCardProduct;
                    total += giftCardProduct.AbsAmount;
                }
                else if (item.Product is DeliveryProduct)
                {
                    var deliveryProduct = item.Product as DeliveryProduct;
                    total += deliveryProduct.AbsAmount;
                }
            }

            return total;
        }

        /// <summary> Adds stub item in transaction if it has no items. </summary>
        private void CheckTransactionForItem()
        {
            if (!Transaction.Items.Any())
            {
                var item = AddTransactionItem(5, "500300", isRefund ? Direction.FROM_CUSTOMER : Direction.TO_CUSTOMER);
                if (isRefund)
                {
                    item.ItemRefund = new ItemRefund();
                }
            }
        }

        private void CheckTransactionForContacts()
        {
            if (Transaction.Contacts == null) //contacts not set
            {
                if (Transaction.Items.Any(x => x.Direction == Direction.FROM_CUSTOMER) //any refund item
                    || Transaction.Fulfillments.Any(x => x is OrderFulfillment)) // or new order
                {
                    Transaction.Contacts = new List<ContactInfo>
                    {
                        CreateStubCustomer()
                    };
                    Transaction.Customer = new Customer { ContactInfoIndex = 0 };
                }
            }
        }

        private void SetupOtherTotalsForTransaction()
        {
            var items = Transaction.Items.Where(x => !x.IsReversed).ToList();
            var toCustomer = items.Where(x => x.Direction == Direction.TO_CUSTOMER).ToList();
            var fromCustomer = items.Where(x => x.Direction == Direction.FROM_CUSTOMER).ToList();

            var taxAmount = toCustomer.Sum(x => x.AbsVatAmount) - fromCustomer.Sum(x => x.AbsVatAmount);

            Transaction.TaxAmount = taxAmount;
        }

        private void CheckTransactionForFulfillments()
        {
            // add missing fulfillments as instant

            var fulfilledTranItemsQuantity = Transaction.Fulfillments.SelectMany(f => f.Items)
                .GroupBy(fi => fi.TransactionItemIndex)
                .ToDictionary(g => (int)g.Key, g => g.Sum(fi => fi.AbsQuantity));

            var missedItems = new List<Tuple<int, int>>();

            foreach (int itemIndex in Transaction.Items.EnumerateIndexes())
            {
                var item = Transaction.Items[itemIndex];
                if (item.Direction == Direction.TO_CUSTOMER && item.Product is SkuProduct)
                {
                    var skuProduct = item.GetTypedProduct<SkuProduct>();

                    int missedItemsCount;
                    var transactionItemIndex = item.ItemIndex;

                    if (!fulfilledTranItemsQuantity.ContainsKey(transactionItemIndex))
                    {
                        missedItemsCount = skuProduct.AbsQuantity;
                    }
                    else
                    {
                        missedItemsCount = fulfilledTranItemsQuantity[transactionItemIndex] - skuProduct.AbsQuantity;
                    }

                    if (missedItemsCount != 0)
                    {
                        missedItems.Add(new Tuple<int, int>(transactionItemIndex, Math.Abs(missedItemsCount)));
                    }
                }
            }

            if (missedItems.Any())  //So, this code will add instant fulfillments for every item with missing quantity, or update old one if quantity was not enough
            {
                var instantFulfillment = Transaction.Fulfillments.FirstOrDefault(x => x is InstantFulfillment);
                if (instantFulfillment == null)
                {
                    instantFulfillment = new InstantFulfillment { Items = new List<FulfillmentItem>() };
                    Transaction.Fulfillments.Add(instantFulfillment);
                }

                foreach (var missedItem in missedItems)
                {
                    var itemIndex = missedItem.Item1;
                    var quanity = missedItem.Item2;
                    var fulfillmentItem =
                        instantFulfillment.Items.FirstOrDefault(x => x.TransactionItemIndex == itemIndex);

                    if (fulfillmentItem == null)
                    {
                        instantFulfillment.Items.Add(new FulfillmentItem() { TransactionItemIndex = itemIndex, AbsQuantity = quanity });
                    }
                    else
                    {
                        fulfillmentItem.AbsQuantity += quanity;
                    }
                }
            }
        }

        #endregion

        #region Default data

        protected override Transaction CreateStubTransaction()
        {
            var tran = (RetailTransaction)base.CreateStubTransaction();

            tran.Items = new List<RetailTransactionItem>();
            tran.TaxAmount = 0;
            tran.Cancellation = null;
            tran.RecalledTransactionId = String.Empty;
            tran.Manager = null;
            tran.RefundCashier = null;
            tran.Fulfillments = new List<Fulfillment>();

            return tran;
        }

        protected override Transaction CreateEmptyTransaction()
        {
            return new RetailTransaction();
        }

        private RetailTransactionItem CreateStubTransactionItem()
        {
            var transactionItem = new RetailTransactionItem()
            {
                IsReversed = false,
                ItemIndex = 1,
                SourceItemId = "1",
                Product = new SkuProduct
                {
                    InitialPrice = DefaultInitialPrice,
                    ProductInputType = ProductInputType.SCANNED,
                    BrandProductId = "500300",
                    AbsQuantity = 1,
                    StockType = StockType.NORMAL
                },
                VatRate = 20,
                AbsVatAmount = 6.79m
            };

            return transactionItem;
        }

        private RetailTransactionItem CreateStubGiftCardTransactionItem(decimal amount)
        {
            var transactionItem = new RetailTransactionItem()
            {
                IsReversed = false,
                ItemIndex = 1,
                Direction = Direction.TO_CUSTOMER,

                Product = new GiftCardProduct
                {
                    AbsAmount = amount,
                    GiftCardOperation = new GiftCardOperation
                    {
                        AuthCode = DlFactory.Create<TestMiscRepository>().CalculateAuthCodeForGiftCard(),
                        CardNumber = "000000000######0000"
                    },
                },
            };

            return transactionItem;
        }

        private RetailTransactionItem CreateStubDeliveryChargeTransactionItem(decimal amount)
        {
            var transactionItem = new RetailTransactionItem()
            {
                IsReversed = false,
                ItemIndex = 1,
                Direction = Direction.TO_CUSTOMER,

                Product = new DeliveryProduct
                {
                    AbsAmount = amount
                },
            };

            return transactionItem;
        }

        private ContactInfo CreateStubCustomer()
        {
            var contactInfo = new ContactInfo()
            {
                ContactAddress = new Address()
                {
                    AddressLine1 = "NORTH ROAD",
                    AddressLine2 = "COMPTON DANDO",
                    PostCode = "CF645TD",
                    Town = "Ryazan"
                },
                Email = "Vavava@gmail.com",
                WorkPhone = String.Empty,
                MobilePhone = "07483 889116",
                Name = "ANTON",
                Phone = "07483 889116"
            };
            return contactInfo;
        }

        private ContactInfo CreateSecondStubCustomer()
        {
            var contactInfo = new ContactInfo()
            {
                ContactAddress = new Address()
                {
                    AddressLine1 = "WALES",
                    AddressLine2 = "DEFINETLY NOT",
                    PostCode = "XX111XX",
                    Town = "LUXIM"
                },
                Email = "Valentin@gmail.com",
                WorkPhone = String.Empty,
                MobilePhone = "12345 678912",
                Name = "JustPigeon",
                Phone = "98765 321446"
            };
            return contactInfo;
        }

        private T CreateFulfillment<T>() where T : Fulfillment
        {
            var fulfillment = new Fulfillment();
            if (typeof(T) == typeof(InstantFulfillment))
            {
                fulfillment = new InstantFulfillment
                {
                    Items = new List<FulfillmentItem>()
                };
            }
            if (typeof(T) == typeof(DeliveryFulfillment))
            {
                fulfillment = CreateStubDeliveryFulfillment();
            }
            if (typeof(T) == typeof(CollectionFulfillment))
            {
                fulfillment = CreateStubCollectionFulfillment();
            }
            return (T)fulfillment;
        }

        private OrderFulfillment CreateStubCollectionFulfillment()
        {
            return new CollectionFulfillment()
            {
                Items = new List<FulfillmentItem>(),
                Number = "000000",
                ContactInfoIndex = 0,
                CollectionDate = CreationDateTime.AddDays(1).Date
            };
        }

        private OrderFulfillment CreateStubDeliveryFulfillment()
        {
            return new DeliveryFulfillment()
            {
                Items = new List<FulfillmentItem>(),
                Number = "000000",
                DeliveryDate = CreationDateTime.AddDays(1).Date,
                ContactInfoIndex = 0,
                DeliveryType = DeliveryType.NORMAL,
                FreeDeliveryAuthorizerNumber = null // Dont know what value to use
            };
        }

        #endregion

        #region Helpers

        private new RetailTransaction Transaction
        {
            [DebuggerStepThrough]
            get { return GetTypedTransaction<RetailTransaction>(); }
        }

        private void FillTransactionItem(RetailTransactionItem transactionItem, int quantity, string skuNumber, Direction direction, 
            decimal vatRate = 20, decimal? initialPrice = null)
        {
            var skuProduct = transactionItem.Product as SkuProduct ?? new SkuProduct();
            skuProduct.AbsQuantity = quantity;
            skuProduct.BrandProductId = skuNumber;
            if (initialPrice != null)
            {
                skuProduct.InitialPrice = initialPrice.Value;
            }
            else
            {
                skuProduct.InitialPrice = DlFactory.Create<IStockRepository>().GetStockMasterPrice(skuNumber);
            }

            transactionItem.Direction = direction;
            transactionItem.Product = skuProduct;
            transactionItem.VatRate = vatRate;
        }

        private void RecalculateAbsVatItemAmounts()
        {
            foreach (var item in Transaction.Items.Where(x => x.Product is SkuProduct))
            {
                var skuProduct = item.Product as SkuProduct;
                decimal extpAmount = Transaction.CalculateExtpAmount(skuProduct);
                item.AbsVatAmount = (extpAmount - skuProduct.CalculateEventsDiscount()) - (Math.Round((extpAmount - skuProduct.CalculateEventsDiscount())/(1 + Math.Round(item.VatRate/100, 2)), 2));
            }
        }

        private void InnerAddTransactionItem(RetailTransactionItem transactionItem, int? itemIndex = null)
        {
            transactionItem.ItemIndex = itemIndex ?? (Transaction.Items.Count + 1);
            Transaction.Items.Add(transactionItem);
        }

        private RetailTransactionItem FindItem<TProduct>()
            where TProduct : Product
        {
            return Transaction.Items.First(x => x.Product is TProduct);
        }

        private RetailTransactionItem FindItem<TProduct>(Predicate<TProduct> predicate)
            where TProduct : Product
        {
            return Transaction.Items.First(x => x.Product is TProduct && predicate((TProduct)x.Product));
        }

        private void AddCancellationItem(int quantity, int transactionItemIndex)
        {
            var cancellationItem = new FulfillmentItem()
            {
                AbsQuantity = quantity,
                TransactionItemIndex = transactionItemIndex
            };

            if (Transaction.Cancellation == null)
            {
                Transaction.Cancellation = new Fulfillment { Items = new List<FulfillmentItem>() };
            }

            Transaction.Cancellation.Items.Add(cancellationItem);
        }

        private DateTime GetWeekDayAfterDate(DayOfWeek day, DateTime date)
        {
            var result = date;
            while (result.DayOfWeek != day)
            {
                result = result.AddDays(1);
            }
            return result;
        }

        #endregion
    }
}
