﻿using System;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public class PaidOutTransactionBuilder : MiscellaneousTransactionBuilder
    {
        public PaidOutTransactionBuilder(IDataLayerFactory dlFactory, DateTime saleDate)
            : base(dlFactory, saleDate)
        {
        }

        protected override Transaction CreateStubTransaction()
        {
            var tran = (PaidOutTransaction)base.CreateStubTransaction();

            tran.ReasonCode = 0;

            return tran;
        }

        protected override Transaction CreateEmptyTransaction()
        {
            return new PaidOutTransaction();
        }

        public void SetReasonCode(PaidOutReasonCode reasonCode)
        {
            ((PaidOutTransaction) Transaction).ReasonCode = reasonCode;
        }
    }
}
