﻿using System;
using WSS.BO.DataLayer.Model;
using WSS.BO.Model.Entity.Transactions;
using WSS.BO.Model.Enums;

namespace WSS.AAT.Common.PosIntegration.TransactionBuilders
{
    public class AuditTransactionBuilder : TransactionBuilder
    {
        public AuditTransactionBuilder(IDataLayerFactory dlFactory, DateTime saleDate)
            : base(dlFactory, saleDate)
        {
        }

        protected override Transaction CreateStubTransaction()
        {
            var tran = (AuditTransaction)base.CreateStubTransaction();

            tran.AuditType = AuditType.SIGN_ON;

            return tran;
        }

        protected override Transaction CreateEmptyTransaction()
        {
            return new AuditTransaction();
        }

        public void SetAuditType(AuditType type)
        {
            ((AuditTransaction)Transaction).AuditType = type;
        }
    }
}
