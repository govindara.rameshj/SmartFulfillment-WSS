﻿using System;
using WSS.BO.BOReceiverService.BL.Configuration;

namespace WSS.AAT.Common.PosIntegration.Configuration
{
    public class MockServiceConfiguration : IConfiguration
    {
        public MockServiceConfiguration()
        {
            TruncateTooLongStrings = true;
        }
        
        public string ServiceName
        {
            get { return "AAT service mock"; }
        }

        public Version ServiceVersion
        {
            get { return new Version("1.2.3.4"); }
        }

        public string DeadlockPriorityForSavingTransaction
        {
            get { return "Low"; }
        }

        public bool ValidateTotalAmounts
        {
            get { return true; }
        }

        public bool TruncateTooLongStrings{ get; set; }

        public bool GatherStatistics { get { return false; } }

        public int CacheAbsoluteExpirationMinutes { get { return 0; } }
    }
}
