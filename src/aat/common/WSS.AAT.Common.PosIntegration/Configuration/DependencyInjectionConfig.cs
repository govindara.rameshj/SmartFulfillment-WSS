using System;
using System.Configuration;
using Cts.Oasys.Core;
using Cts.Oasys.Core.SystemEnvironment;
using Ninject;
using Ninject.Extensions.Factory;
using WSS.BO.BOReceiverService.BL.Configuration;
using WSS.BO.BOReceiverService.BL.Facade;
using WSS.BO.BOReceiverService.BL.ServiceInfoProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing;
using WSS.BO.BOReceiverService.BL.TransactionsProcessing.DataLayer;
using WSS.BO.Common.Hosting;
using WSS.BO.DataLayer.Database;

namespace WSS.AAT.Common.PosIntegration.Configuration
{
    public class DependencyInjectionConfig : BaseDependencyInjectionConfig
    {
        protected override void InitOasysDbConfiguration(IKernel kernel)
        {
            kernel.Bind<IOasysDbConfiguration>().To<AppConfigBasedOasysDbConfiguration>();
        }

        public override IKernel GetKernel()
        {
            var kernel = base.GetKernel();

            kernel.Bind<ISystemEnvironment>().To<LiveSystemEnvironment>();

            kernel.Bind<IConfiguration>().To<MockServiceConfiguration>().InSingletonScope();

            kernel.Bind<IReadOnlyDataRetriever>().To<DataRetriever>();
            kernel.Bind<IRuntimeDataRetriever>().To<DataRetriever>();

            kernel.Bind<IServiceInfoProcessor>().To<ServiceInfoProcessor>();
            kernel.Bind<ITransactionProcessor>().To<TransactionProcessor>();
            kernel.Bind<IProcessorsFactory>().ToFactory();

            kernel.Bind<ITransactionProcessingSteps>().To<TransactionProcessingSteps>();
            kernel.Bind<IBusinessLogicFacade>().To<BusinessLogicFacade>();

            return kernel;
        }
    }
}