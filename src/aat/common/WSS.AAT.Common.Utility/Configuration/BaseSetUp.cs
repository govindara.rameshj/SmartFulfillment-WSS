using BoDi;
using Cts.Oasys.Core.SystemEnvironment;
using Cts.Oasys.Core.Tests;
using Ninject;
using WSS.AAT.Common.Utility.Bindings;
using WSS.BO.Common.Hosting;
using WSS.BO.DataLayer.Model;
using WSS.AAT.Common.DataLayer;
using WSS.AAT.Common.Utility.Utility;
using System.Configuration;
using Cts.Oasys.Core;

namespace WSS.AAT.Common.Utility.Configuration
{
    public class BaseSetUp
    {
        private IKernel kernel;
        protected IDataLayerFactory DataLayerFactory { get; private set; }
        protected ISystemEnvironment SystemEnvironment { get; private set; }
        protected TestConfiguration TestConfiguration { get; private set; }

        public virtual void OnBeforeTestRun()
        {
            TestConfiguration = new TestConfiguration(GetType());

            // Init specflow reporting
            SpecResultsReportingBinding.Instance.Initialize(TestConfiguration.ResolvePathFromAppSettings("testReportFilePath"));

            // Init external dependencies
            SystemEnvironment = GlobalVars.SystemEnvironment ?? TestEnvironmentSetup.Setup(InitializeTestEnvironmentSettings);
            
            kernel = GetDependencyInjectionConfig().GetKernel();
            DataLayerFactory = kernel.Get<IDataLayerFactory>();

            if (ConfigurationManager.AppSettings["disableSnapshotRollback"] != "true")
            {
                SnapshotHelper snapshotHelper = new SnapshotHelper(DataLayerFactory);
                snapshotHelper.RollbackSnapshot();
            }
        }

        public void OnBeforeScenario()
        {
            var container = GetObjectContainer();
            InitializeObjectContainer(container);
        }

        protected virtual IDependencyInjectionConfig GetDependencyInjectionConfig()
        {
            return new BaseAatDependencyInjectionConfig();
        }

        protected virtual void InitializeTestEnvironmentSettings(TestEnvironmentSettings settings)
        {
            settings.ConnectToAatDb();
        }

        protected virtual void InitializeObjectContainer(IObjectContainer container)
        {
            container.RegisterInstanceAs(TestConfiguration);
            BindObjectContainerTypeFromKernel<IDataLayerFactory>(container);
            container.RegisterInstanceAs(SystemEnvironment);
        }

        protected IObjectContainer GetObjectContainer()
        {
            var container =
                (IObjectContainer) TechTalk.SpecFlow.ScenarioContext.Current.GetBindingInstance(typeof (IObjectContainer));
            return container;
        }

        protected void BindObjectContainerTypeFromKernel<T>(IObjectContainer container) where T : class
        {
            container.RegisterInstanceAs(kernel.Get<T>());
        }
    }
}
