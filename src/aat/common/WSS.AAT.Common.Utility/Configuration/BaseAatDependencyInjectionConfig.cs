﻿using Ninject;
using System;
using System.Configuration;
using WSS.BO.Common.Hosting;
using WSS.BO.DataLayer.Database;

namespace WSS.AAT.Common.Utility.Configuration
{
    public class BaseAatDependencyInjectionConfig : BaseDependencyInjectionConfig
    {
        protected override void InitOasysDbConfiguration(IKernel kernel)
        {
            kernel.Bind<IOasysDbConfiguration>().To<AppConfigBasedOasysDbConfiguration>();
        }
    }
}