using System;

namespace WSS.AAT.Common.Utility.Configuration
{
    public class BaseSetUpBinding<T>
        where T : BaseSetUp, new()
    {
        protected static T Instance;

        protected static void DoBeforeTestRun()
        {
            Instance = new T();
            Instance.OnBeforeTestRun();
        }

        protected static void DoBeforeScenario()
        {
            Instance.OnBeforeScenario();
        }
    }
}
