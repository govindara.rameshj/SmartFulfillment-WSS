﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace WSS.AAT.Common.Utility.Configuration
{
    public class TestConfiguration
    {
        private readonly Assembly baseAssembly;

        public TestConfiguration(Type aTypeFromBaseAssembly)
        {
            baseAssembly = Assembly.GetAssembly(aTypeFromBaseAssembly);
        }

        public string ResolvePathFromAppSettings(string appSettingName)
        {
            string result = ConfigurationManager.AppSettings[appSettingName];
            if (!Path.IsPathRooted(result))
            {
                // relative path is resolved against test dll location. But dont use Assembly.Location, because nUnit makes a copy of the dll.
                Uri assemblyUri = new Uri(baseAssembly.CodeBase);
                result = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(assemblyUri.LocalPath), result));
            }

            return result;
        }

    }
}
