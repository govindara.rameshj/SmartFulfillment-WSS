﻿using System;
using System.Diagnostics;
using System.IO;
using SpecResults;
using SpecResults.Json;

namespace WSS.AAT.Common.Utility.Bindings
{
    /// <summary>
    /// Binding registers SpecResults listener and report generator.
    /// </summary>
#if ENABLE_SPECFLOW_REPORTING
    [TechTalk.SpecFlow.Binding]
#endif
    public sealed class SpecResultsReportingBinding
    {
        private static readonly Lazy<SpecResultsReportingBinding> InnerInstance = new Lazy<SpecResultsReportingBinding>();

        public static SpecResultsReportingBinding Instance
        {
            get { return InnerInstance.Value; }
        }

        private string reportFilePath;

        public void Initialize(string reportFilePath)
        {
            // ReSharper disable once UnusedVariable
            var dummySerializer = new Newtonsoft.Json.JsonSerializer(); // the line is useless, but is necessary to copy Newtonsoft.Json.dll to output of Specflow.Common clients

            this.reportFilePath = reportFilePath;
        }

#if ENABLE_SPECFLOW_REPORTING
        [TechTalk.SpecFlow.BeforeTestRun]
        public static void InitializeReporting()
        {
            var webApp = new JsonReporter();
            Reporters.Add(webApp);
            Reporters.FinishedReport += (sender, args) => Instance.OnReportFinished(args.Reporter);
        }
#endif

        // ReSharper disable once UnusedMember.Local
        private void OnReportFinished(Reporter reporter)
        {
            // if the reporter is not json one, just ignore it
            var jsonReporter = reporter as JsonReporter;
            if (jsonReporter != null)
            {
                SaveJsonReportToFile(jsonReporter);
            }
        }

        private void SaveJsonReportToFile(JsonReporter reporter)
        {
            if (reportFilePath == null)
            {
                throw new ApplicationException("Json report file path is not initialized. You should call SpecResultsReportingBinding.Initialize");
            }

            if (File.Exists(reportFilePath))
            {
                File.Delete(reportFilePath);
            }
            else
            {
                var directoryName = Path.GetDirectoryName(reportFilePath);
                Debug.Assert(directoryName != null, "directoryName != null");
                Directory.CreateDirectory(directoryName);
            }

            File.WriteAllText(reportFilePath, reporter.WriteToString());
        }
    }
}