using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using TechTalk.SpecFlow;

namespace WSS.AAT.Common.Utility.Utility
{
    public class SpecflowTableWrapper<T>
            where T : new()
    {
        private readonly Table table;
        private readonly IList<PropertyInfo> givenProperties;

        public SpecflowTableWrapper(Table table) 
        {
            this.table = table;
            givenProperties = SelectProperties(table);
        }

        public IEnumerable<T> GetEntities()
        {
            return GetEntities(() => new T());
        }

        public IEnumerable<T> GetEntities(Func<T> creator)
        {
            return table.Rows.Select(row => CreateEntity(row, creator));
        }

        public void FormatTable(IEnumerable<T> entities, StringBuilder sb)
        {
            sb.Append(" | ");
            foreach (var property in givenProperties)
            {
                sb.AppendFormat(" {0} |", property.Name);
            }
            sb.AppendLine();

            foreach (var entity in entities)
            {
                sb.Append(" | ");
                foreach (var property in givenProperties)
                {
                    sb.AppendFormat(" {0} |", property.GetValue(entity, null));
                }
                sb.AppendLine();
            }
        }

        public void FormatRow(T entity, StringBuilder sb)
        {
            sb.Append(" | ");
            foreach (var property in givenProperties)
            {
                sb.AppendFormat(" {0} |", property.Name);
            }
            sb.AppendLine();

            sb.Append(" | ");
            foreach (var property in givenProperties)
            {
                sb.AppendFormat(" {0} |", property.GetValue(entity, null));
            }
            sb.AppendLine();
        }

        public IEqualityComparer<T> GetComparer()
        {
            return new PropertiesEqualityComparer<T>(givenProperties);
        }

        private T CreateEntity(TableRow row, Func<T> creator)
        {
            T obj = creator();

            foreach (PropertyInfo property in givenProperties)
            {
                if (table.ContainsColumn(property.Name))
                {
                    object val = row[property.Name];
                    var propertyPrimitiveType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                    property.SetValue(obj, "not set".Equals(val) ? null : Convert.ChangeType(val, propertyPrimitiveType), null);
                }
            }
            return obj;
        }

        private static IList<PropertyInfo> SelectProperties(Table table)
        {
            var type = typeof(T);
            var properties = type.GetProperties();

            var result = new List<PropertyInfo>();

            foreach (var columnName in table.Header)
            {
                string normalizedColumnName = columnName.Replace(" ", "").Replace("_", "");
                var property = properties.FirstOrDefault(p => String.Equals(normalizedColumnName, p.Name, StringComparison.CurrentCultureIgnoreCase));
                if (property == null)
                {
                    throw new ApplicationException(String.Format("Table column name [{0}] is invalid. It does not match to any of class [{1}] properties.", columnName, type.Name));
                }

                result.Add(property);
                table.RenameColumn(columnName, property.Name);
            }

            return result;
        }

        public void ReplaceDateTimeWithActualValues(string columnName)
        {
            foreach (var row in table.Rows)
            {
                row[columnName] = DateTimeParser.ParseAsDate(row[columnName]).ToString();
            }
        }
    }
}
