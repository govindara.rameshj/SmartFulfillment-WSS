﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WSS.AAT.Common.Utility.Utility
{
    public static class DateTimeHelper
    {
        public static DateTime Parse(string val)
        {
            DateTime res;

            Regex reg = new Regex(@"(.+)([-+])(\d+)");
            Match m = reg.Match(val);
            if (!m.Success)
                throw new ArgumentException("Wrong input datetime format", "val");
            var basePart = m.Groups[1].Value;
            if (basePart == "today")
                res = DateTime.Today;
            else
                throw new ArgumentException("Wrong input datetime format", "val");

            var sign = m.Groups[2].Value;
            var add = int.Parse(m.Groups[3].Value);
            if (sign == "-")
                res = res.AddDays(-add);
            else
                res = res.AddDays(add);

            return res;
        }
    }
}
