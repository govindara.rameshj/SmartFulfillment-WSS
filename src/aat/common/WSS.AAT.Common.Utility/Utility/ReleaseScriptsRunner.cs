using System;
using System.Diagnostics;
using System.IO;
using Cts.Oasys.Core.SystemEnvironment;

namespace WSS.AAT.Common.Utility.Utility
{
    public class ReleaseScriptsRunner
    {
        ISystemEnvironment systemEnvironment;
        public ReleaseScriptsRunner(ISystemEnvironment systemEnvironment)
        {
            this.systemEnvironment = systemEnvironment;
        }

        public bool innerRunReleaseScriptsInLocation(string inLocation, bool ignoreErrors)
        {
            bool success = false;

            string dataSource = string.Empty;
            string connStr = systemEnvironment.GetOasysSqlServerConnectionString();
            string initialCatalog = string.Empty;

            string[] parameters = connStr.Split(';');

            foreach (string parameter in parameters)
            {
                string[] pair = parameter.Split('=');

                if (pair[0].ToLower() == "data source")
                {
                    dataSource = pair[1];
                }
                else if (pair[0].ToLower() == "initial catalog")
                {
                    initialCatalog = pair[1];
                }
            }

            if (Directory.Exists(inLocation))
            {
                foreach (string file in Directory.GetFiles(inLocation, "*.sql"))
                {
                    var proc = new Process
                    {
                        EnableRaisingEvents = false,
                        StartInfo =
                        {
                            WindowStyle = ProcessWindowStyle.Hidden,
                            FileName = String.Format("sqlcmd"),
                            Arguments = String.Format("-S {0} -d {1} {3} -i \"{2}\"", dataSource, initialCatalog,
                                                        file, ignoreErrors ? "" : " -b")

                        }
                    };
                    proc.Start();
                    proc.WaitForExit();
                    success = proc.ExitCode == 0;
                }
            }
            else
            {
                throw new Exception("script directory not found: " + inLocation);
            }

            return success;
        }

        public bool runReleaseScriptsInLocation(string inLocation, string databaseScriptsVersion, string scriptType)
        {
            bool result = false;
            string versionDirectory = Path.Combine(inLocation, databaseScriptsVersion);
            if (Directory.Exists(versionDirectory))
            {
                result = innerRunReleaseScriptsInLocation(Path.Combine(versionDirectory, scriptType), false);
            }
            return result;
        }
    }
}
