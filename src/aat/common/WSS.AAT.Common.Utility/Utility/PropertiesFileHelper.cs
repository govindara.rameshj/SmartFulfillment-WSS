using System;

namespace WSS.AAT.Common.Utility.Utility
{
    public class PropertiesFileHelper
    {
        public Version GetDbVersion()
        {
            PropertiesFileParser propertiesFile = new PropertiesFileParser("..\\..\\..\\..\\config\\env_vars.properties");
            string databaseVersion = propertiesFile.Get("VERSION");
            return new Version(databaseVersion);
        }
    }
}
