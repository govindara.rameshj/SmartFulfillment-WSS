﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using WSS.AAT.Common.DataLayer;

namespace WSS.AAT.Common.Utility.Utility
{
    public class TableComparator
    {
        private readonly TablesRepository _tableRepo;

        public TableComparator(TablesRepository tableRepo)
        {
            _tableRepo = tableRepo;
        }

        public string CompareActualAndExpected<T, TKey>(Table tableExpected, Func<T, TKey> keySelector, Func<TablesRepository, IEnumerable<T>> getTable) where T : new()
        {
            var wrappedTableReport = getSpecflowWrapperTable<T>(tableExpected);
            var expected = wrappedTableReport.GetEntities();
            var actual = getTable(_tableRepo);

            StringBuilder sb = new StringBuilder();

            DisplayTablesKeysDifferences<T, TKey>(expected, actual, wrappedTableReport, sb, keySelector);

            DisplayTablesNotKeyDifferences<T, TKey>(expected, actual, wrappedTableReport, sb, keySelector);

            return sb.ToString();
        }

        private void DisplayTablesKeysDifferences<T, TKey>(IEnumerable<T> expected, IEnumerable<T> actual, SpecflowTableWrapper<T> wrappedTableReport,
            StringBuilder sb, Func<T, TKey> keySelector) where T : new()
        {
            var keySelectorComparer = new KeySelectorComparer<T, TKey>(keySelector);

            DisplayTableKeysDifferenceMessage<T>(expected.Except(actual, keySelectorComparer).ToList(), "Rows which are expected, but do not exist in the db table:", wrappedTableReport, sb);

            DisplayTableKeysDifferenceMessage<T>(actual.Except(expected, keySelectorComparer).ToList(), "Rows which exist in the db table, but are not expected:", wrappedTableReport, sb);
        }

        private void DisplayTableKeysDifferenceMessage<T>(List<T> keyDifference, string headerMessage, SpecflowTableWrapper<T> wrappedTableReport, StringBuilder sb) where T : new()
        {
            if (keyDifference.Count != 0)
            {
                sb.AppendLine();

                sb.AppendLine(headerMessage);
                wrappedTableReport.FormatTable(keyDifference, sb);
            }
        }

        private void DisplayTablesNotKeyDifferences<T, TKey>(IEnumerable<T> expected, IEnumerable<T> actual, SpecflowTableWrapper<T> wrappedTableReport,
            StringBuilder sb, Func<T, TKey> keySelector) where T : new()
        {
            var keySelectorComparer = new KeySelectorComparer<T, TKey>(keySelector);

            var expectedWithoutKeyDifference = expected.Intersect(actual, keySelectorComparer).ToList();
            var actualWithoutKeyDifference = actual.Intersect(expected, keySelectorComparer).ToList();

            var expectedLinesDifference = expectedWithoutKeyDifference.Except(actualWithoutKeyDifference, wrappedTableReport.GetComparer()).ToList();
            var actualLinesDifference = actualWithoutKeyDifference.Except(expectedWithoutKeyDifference, wrappedTableReport.GetComparer()).ToList();

            if (expectedLinesDifference.Count != 0 && actualLinesDifference.Count != 0)
            {
                DisplayTablesNotKeyDifferencesMessage<T, TKey>(expectedLinesDifference, actualLinesDifference, wrappedTableReport, sb, keySelector);
            }
        }

        private static void DisplayTablesNotKeyDifferencesMessage<T, TKey>(List<T> expectedLinesDifference, List<T> actualLinesDifference, SpecflowTableWrapper<T> wrappedTableReport,
            StringBuilder sb, Func<T, TKey> keySelector) where T : new()
        {
            var keySelectorComparer = new KeySelectorComparer<T, TKey>(keySelector);
            var propertiesComparer = wrappedTableReport.GetComparer() as PropertiesEqualityComparer<T>;

            sb.AppendLine();
            sb.AppendLine("There are differences in not primary key columns:");
            sb.AppendLine();

            foreach (var expectedLine in expectedLinesDifference)
            {
                sb.AppendLine("Expected Line:");
                wrappedTableReport.FormatRow(expectedLine, sb);

                var actualLine = actualLinesDifference.Single(x => keySelectorComparer.Equals(expectedLine, x));
                sb.AppendLine("Actual Line:");
                wrappedTableReport.FormatRow(actualLine, sb);

                sb.AppendLine("Expected and actual lines differ in columns:");
                sb.AppendLine(propertiesComparer.GetDifferentProperties(expectedLine, actualLine, sb).ToString());
            }
        }

        private SpecflowTableWrapper<T> getSpecflowWrapperTable<T>(Table table) where T : new()
        {
            var wrappedTable = new SpecflowTableWrapper<T>(table);
            return wrappedTable;
        }
    }
}
