using System;
using WSS.BO.DataLayer.Model;
using WSS.AAT.Common.DataLayer;

namespace WSS.AAT.Common.Utility.Utility
{
    class SnapshotHelper
    {
        private readonly IDataLayerFactory dataLayerFactory;

        public SnapshotHelper(IDataLayerFactory dataLayerFactory)
        {
            this.dataLayerFactory = dataLayerFactory;
        }

        public void RollbackSnapshot()
        {
            PropertiesFileHelper propertiesFile = new PropertiesFileHelper();
            string snapshotVersion = propertiesFile.GetDbVersion().ToString(2); // Major.Minor
            string snapshotName = String.Format("Oasys_AAT_snapshot_{0}_{1:yyyy-MM-dd}", snapshotVersion, DateTime.Now);

            var repo = dataLayerFactory.Create<RawSqlRepository>();
            repo.RollbackDatabaseToSnapshot(snapshotName);
        }
    }
}
