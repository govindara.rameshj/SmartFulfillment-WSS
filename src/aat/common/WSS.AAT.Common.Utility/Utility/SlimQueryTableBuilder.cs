﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.AAT.Common.Utility.Utility
{
    public class SlimQueryTableBuilder
    {
        List<string> columnNames = new List<string>();
        List<List<object>> rows = new List<List<object>>();

        public SlimQueryTableBuilder(params string[] columnNames)
        {
            this.columnNames.AddRange(columnNames);
        }

        public void AddRow(List<object> values)
        {
            rows.Add(values);
        }

        public void AddRow(params object[] values)
        {
            AddRow(new List<object>(values));
        }

        public void AddRows<T>(IEnumerable<T> source, Func<T, List<object>> selector)
        {
            foreach (T item in source)
            {
                AddRow(selector(item));
            }
        }

        private List<List<object>> GetResultListForRow(List<object> row)
        {
            return row.Zip(columnNames, (v, cn) => new List<object> { cn, v }).ToList();
        }

        public List<List<List<object>>> GetResult()
        {
            return rows.Select(row => GetResultListForRow(row)).ToList();
        }
    }
}
