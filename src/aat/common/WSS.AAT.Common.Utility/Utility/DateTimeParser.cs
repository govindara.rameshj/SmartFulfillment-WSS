﻿namespace WSS.AAT.Common.Utility.Utility
{
    using System;
    using System.Collections.Generic;

    public class DateTimeParser
    {
        /// <summary>
        /// Output dates in a format suitable for SQL Server
        /// </summary>
        private static string _sqlFormat = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// List of core day types we understand
        /// </summary>
        private static List<UnitSpecification> _coreDays;

        /// <summary>
        /// List of units we understand
        /// </summary>
        private static List<UnitSpecification> _modifierUnits;

        /// <summary>
        /// List of directions to apply modifiers to we understand
        /// </summary>
        private static List<UnitSpecification> _modifierDirections;

        /// <summary>
        /// Initializes static members of the DateTimeParser class
        /// </summary>
        static DateTimeParser()
        {
            _coreDays = new List<UnitSpecification>();
            _coreDays.Add(new UnitSpecification("TODAY", 0));
            _coreDays.Add(new UnitSpecification("YESTERDAY", -1));
            _coreDays.Add(new UnitSpecification("TOMORROW", 1));

            _modifierUnits = new List<UnitSpecification>();
            _modifierUnits.Add(new UnitSpecification("DAYS", 1));
            _modifierUnits.Add(new UnitSpecification("DAY", 1));
            _modifierUnits.Add(new UnitSpecification("WEEKS", 7));
            _modifierUnits.Add(new UnitSpecification("WEEK", 7));
            _modifierUnits.Add(new UnitSpecification("MONTHS", 28));
            _modifierUnits.Add(new UnitSpecification("MONTH", 28));

            _modifierDirections = new List<UnitSpecification>();
            _modifierDirections.Add(new UnitSpecification("BEFORE", -1));
            _modifierDirections.Add(new UnitSpecification("AFTER", 1));
        }

        /// <summary>
        /// Gets a default threshold date
        /// </summary>
        /// <returns></returns>
        public static DateTime GetThresholdDate()
        {
            return DateTime.Now.Date;
        }

        /// <summary>
        /// Generate relative wording for datetime value
        /// </summary>
        /// <param name="suppliedDate">Date to process</param>
        /// <returns></returns>
        public static string EncodeDateTime(DateTime suppliedDate)
        {
            return EncodeDateTime(suppliedDate, GetThresholdDate());
        }

        /// <summary>
        /// Generate relative wording for datetime value
        /// </summary>
        /// <param name="suppliedDate">Date to process</param>
        /// <param name="thresholdDate">Date time value to use as threshold</param>
        /// <returns></returns>
        public static string EncodeDateTime(DateTime suppliedDate, DateTime thresholdDate)
        {
            string output = string.Empty;

            // Calculate number of days between supplied date and threshold date
            int difference = (suppliedDate.Date - thresholdDate.Date).Days;

            // Special cases
            switch (difference)
            {
                case 0:
                    output = "today";
                    break;
                case 1:
                    output = "tomorrow";
                    break;
                case -1:
                    output = "yesterday";
                    break;

                default:
                    string units = "day";

                    // Is this an exact number of weeks?
                    if (difference % 7 == 0)
                    {
                        difference /= 7;
                        units = "week";
                    }

                    if (Math.Abs(difference) == 1)
                    {
                        output += String.Format("1 {0} ", units);
                    }
                    else
                    {
                        output += String.Format("{0} {1}s ", Math.Abs(difference).ToString(), units);
                    }

                    if (difference < 0)
                    {
                        output += "before today";
                    }
                    else
                    {
                        output += "after today";
                    }
                    break;
            }

            if (suppliedDate.TimeOfDay != (new TimeSpan(0, 0, 0)))
            {
                output += String.Format(" @ {0}:{1}:{2}.{3}", suppliedDate.Hour,
                                        suppliedDate.Minute, suppliedDate.Second,
                                        suppliedDate.Millisecond.ToString("000"));
            }

            return output;
        }

        /// <summary>
        /// This will parse a 'datetime' value and adjust it for the current date
        /// datetime values must always have a token in them and an exception will
        /// be thrown if a real datetime is found
        /// </summary>
        /// <param name="supplied">Date to parse</param>
        /// <returns>Formatted date time value</returns>
        public static string Parse(string supplied)
        {
            // Return date nicely formatted
            return ParseAsDate(supplied).ToString(_sqlFormat);
        }

        /// <summary>
        /// This will parse a 'datetime' value and adjust it for the current date
        /// datetime values must always have a token in them and an exception will
        /// be thrown if a real datetime is found
        /// </summary>
        /// <param name="supplied">Date to parse</param>
        /// <returns>Date time value</returns>
        public static DateTime ParseAsDate(string supplied)
        {
            DateTime parsedDate;
            bool monthFlag=false;

            // Valid dates are not allowed
            if (DateTime.TryParse(supplied, out parsedDate))
            {
                throw new Exception(String.Format("DateTime values must be passed as tokens and not hardcoded. Supplied value was [{0}]", supplied));
            }

            supplied = supplied.ToUpper();

            // Strip out time
            var suppliedTime = StripSuppliedTime(ref supplied);

            // Get core starting day
            parsedDate = DateTime.Today.AddDays(ParseUnitSpecification(_coreDays, ref supplied));

            // Now check for modifiers to core date
            int modifier = ParseUnitSpecification(_modifierUnits, ref supplied);

            //special month processing
            if (modifier == 28)
            {
                monthFlag = true;
                modifier = 1;
            }

            // Check for modifier direction
            modifier *= ParseUnitSpecification(_modifierDirections, ref supplied);

            // Should just be left with a count for the modifier
            int modifierCount = 1;

            supplied = supplied.Trim();

            // The last thing left should be a number if present
            if (supplied.Length > 0 && int.TryParse(supplied, out modifierCount) == false)
            {
                throw new Exception("Invalid date specification provided");
            }

            modifier *= modifierCount;

            // Adjust date according to modifier

            if (monthFlag)
            {
                parsedDate = parsedDate.AddMonths(modifier);                               
            }
            else
            {
                parsedDate = parsedDate.AddDays(modifier);                
            }


            // Return date nicely formatted with time added back in
            return DateTime.Parse(parsedDate.ToString("yyyy-MM-dd") + " " + suppliedTime);
        }

        public static DateTime ParseAsDateAndTime(string dateValue, string timeValue)
        {
            return DateTimeParser.ParseAsDate(dateValue).Add(DateTime.Parse(timeValue).TimeOfDay);
        }

        /// <summary>
        /// Strip out time from string if supplied
        /// </summary>
        /// <param name="supplied">Supplied date time</param>
        /// <returns>Time string</returns>
        private static string StripSuppliedTime(ref string supplied)
        {
            string suppliedTime = string.Empty;

            int timeIndex = supplied.IndexOf('@');

            if (timeIndex > 0)
            {
                suppliedTime = supplied.Substring(timeIndex + 1).Trim();
                supplied = supplied.Substring(0, timeIndex);
            }

            return suppliedTime;
        }

        /// <summary>
        /// Parse part of the supplied string
        /// </summary>
        /// <param name="units">List of units to parse</param>
        /// <param name="stringToParse">String to parse</param>
        /// <returns>Value based on unit found</returns>
        private static int ParseUnitSpecification(List<UnitSpecification> units, ref string stringToParse)
        {
            int unitOffset = 0;

            foreach (UnitSpecification unit in units)
            {
                int unitIndex = stringToParse.IndexOf(unit.Name);

                if (unitIndex >= 0)
                {
                    stringToParse = stringToParse.Remove(unitIndex, unit.Name.Length);
                    return unit.Value;
                }
            }

            return unitOffset;
        }

        /// <summary>
        /// Holds unit name and value
        /// </summary>
        private class UnitSpecification
        {
            /// <summary>
            /// Initializes a new instance of the UnitSpecification class
            /// </summary>
            /// <param name="name">Unit name</param>
            /// <param name="value">Unit value</param>
            public UnitSpecification(string name, int value)
            {
                Name = name;
                Value = value;
            }      
            
            /// <summary>
            /// Gets or sets the unit name
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the unit value
            /// </summary>
            public int Value { get; set; }
        }
    }
}
