using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.AAT.Common.Utility.Utility
{
    public class PropertiesFileParser
    {
        private Dictionary<string, string> list;
        private string filename;

        public PropertiesFileParser(string file)
        {
            Load(file);
        }

        public void Load(string filename)
        {
            this.filename = filename;
            list = new Dictionary<string, string>();
            LoadFromFile(filename);
        }

        public string Get(string field)
        {
            return (list.ContainsKey(field)) ? (list[field]) : (null);
        }

        private void LoadFromFile(string file)
        {
            foreach (string line in System.IO.File.ReadAllLines(file))
            {
                int index = line.IndexOf('=') != -1 ? line.IndexOf('=') : 0;
                string key = line.Substring(0, index).Trim();
                string value = line.Substring(index + 1).Trim();
                try
                {
                    list.Add(key, value);
                }
                catch { }
            }
        }
    }
}
