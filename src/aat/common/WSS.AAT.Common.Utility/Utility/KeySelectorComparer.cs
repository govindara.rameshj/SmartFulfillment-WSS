﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace WSS.AAT.Common.Utility.Utility
{
    public class KeySelectorComparer<T, TKey> : IComparer<T>, IEqualityComparer<T>
    {
        private readonly Func<T, TKey> keySelector;

        public KeySelectorComparer(Func<T, TKey> keySelector)
        {
            this.keySelector = keySelector;
        }

        public int Compare(T x, T y)
        {
            return Comparer<TKey>.Default.Compare(keySelector(x), keySelector(y));
        }

        public bool Equals(T x, T y)
        {
            return EqualityComparer<TKey>.Default.Equals(keySelector(x), keySelector(y));
        }

        public int GetHashCode(T x)
        {
            return EqualityComparer<TKey>.Default.GetHashCode(keySelector(x));
        }
    }
}
