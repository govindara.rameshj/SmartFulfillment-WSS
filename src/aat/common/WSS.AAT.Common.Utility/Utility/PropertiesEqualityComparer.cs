using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace WSS.AAT.Common.Utility.Utility
{
    public class PropertiesEqualityComparer<T> : IEqualityComparer<T>
    {
        IList<PropertyInfo> propertiesToCompare;

        public PropertiesEqualityComparer(IList<PropertyInfo> propertiesToCompare)
        {
            this.propertiesToCompare = propertiesToCompare;
        }
        
        public bool Equals(T x, T y)
        {
            foreach (var property in propertiesToCompare)
            {
                object xValue = property.GetValue(x, null);
                object yValue = property.GetValue(y, null);
                if (!EqualityComparer<Object>.Default.Equals(xValue, yValue))
                {
                    return false;
                }
            }
            return true;
        }

        public int GetHashCode(T x)
        {
            int result = 0;
            foreach (var property in propertiesToCompare)
            {
                object value = property.GetValue(x, null);
                if (value != null)
                {
                    result ^= value.GetHashCode();
                }
            }
            return result;
        }

        public StringBuilder GetDifferentProperties(T x, T y, StringBuilder sb)
        {
            StringBuilder propertyNames = new StringBuilder("           |");
            StringBuilder expectedValues = new StringBuilder("| expected |");
            StringBuilder actualValues = new StringBuilder("| actual   |");
            foreach (var property in propertiesToCompare)
            {
                object xValue = property.GetValue(x, null);
                object yValue = property.GetValue(y, null);
                if (!EqualityComparer<Object>.Default.Equals(xValue, yValue))
                {
                    propertyNames.Append(property.Name.PadRight(25)).Append('|');
                    expectedValues.Append(xValue.ToString().PadRight(25)).Append('|');
                    actualValues.Append(yValue.ToString().PadRight(25)).Append('|');
                }
            }

            propertyNames.AppendLine();
            propertyNames.AppendLine(expectedValues.ToString());
            propertyNames.AppendLine(actualValues.ToString());

            return propertyNames;
        }
    }

}
