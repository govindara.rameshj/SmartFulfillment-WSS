using System;
using System.Diagnostics;
using System.IO;
using Cts.Oasys.Core.Tests;
using Cts.Oasys.Core.SystemEnvironment;
using System.Collections.Generic;

namespace WSS.AAT.Common.Utility.Executables
{
    public class ProcessTransmissions
    {
        protected virtual string MainProcessName
        {
            get { return "ProcessTransmissions.exe"; }
        }

        private readonly ISystemEnvironment systemEnvironment;
        private readonly string executablesDir;

        public ProcessTransmissions(ISystemEnvironment systemEnvironment, string executablesDir)
        {
            this.systemEnvironment = systemEnvironment;
            this.executablesDir = executablesDir;
        }

        public void ClearTransmissionFiles()
        {
            var outputDir = systemEnvironment.GetCommsDirectoryPath();

            if (Directory.Exists(outputDir))
            {
                foreach (var file in Directory.GetFiles(outputDir, "*", SearchOption.AllDirectories))
                {
                    File.Delete(file);
                }
            }
        }

        public void ProcessFileCode(string fileCode, DateTime? dateTime = null)
        {
            var args = new List<string>
            {
                fileCode
            };

            if (dateTime.HasValue)
                args.Add("PDATE=" + dateTime.Value.ToString("dd/MM/yy"));

            StartMainProcess(args.ToArray());
        }

        public void ProcessFileCodeFromDateToDate(string fileCode, DateTime startDate, DateTime endDate)
        {
            var args = new[] {
                fileCode,
                "SDATE=" + startDate.ToString("dd/MM/yy"),
                "EDATE=" + endDate.ToString("dd/MM/yy")
            };

            StartMainProcess(args);
        }

        private void StartMainProcess(string[] args)
        {
            StartProcess(MainProcessName, args);
        }

        public void RunRollDates()
        {
            StartProcess("RollDates.EXE", "/P=',CFC'", "/Y");
        }

        public void RunNightlyRetailUpdates()
        {
            StartProcess("NightlyRetailUpdates.exe", "Y");
        }

        private void StartProcess(string executableFileName, params string[] args)
        {
            var proc = new Process
            {
                StartInfo =
                {                    
                    FileName = executablesDir + executableFileName,
                    Arguments = String.Join(" ", args),
                    UseShellExecute = false
                }
            };

            TestEnvironmentSetup.SetupSpawnedProcess(proc.StartInfo);

            proc.Start();
            proc.WaitForExit();
        }
 	}
}
