using System;
using Cts.Oasys.Core.SystemEnvironment;

namespace WSS.AAT.Common.Utility.Executables
{
    public class ProcessTransmissionPeriod : ProcessTransmissions
    {
        protected override string MainProcessName
        {
            get { return "ProcessTransmissionsPeriod.exe"; }
        }

        public ProcessTransmissionPeriod(ISystemEnvironment systemEnvironment, string executablesDir)
            : base(systemEnvironment, executablesDir)
        {
        }

        public void ProcessFileCodeForDate(string fileCode, DateTime forDate)
        {
            if (fileCode == "DB")
            {
                fileCode += "," + forDate.ToString("dd/MM/yy");
            }
            ProcessFileCode(fileCode, forDate);
        }
  	}
}
