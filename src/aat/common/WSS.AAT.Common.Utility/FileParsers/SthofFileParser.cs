using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Cts.Oasys.Core.SystemEnvironment;

namespace WSS.AAT.Common.Utility.FileParsers
{
    public class SthofFileParser
    {
        private readonly Dictionary<DateTime, Dictionary<String, String>> _fileContent = new Dictionary<DateTime, Dictionary<string, string>>();

        public SthofFileParser(ISystemEnvironment systemEnvironment)
        {
            string commsDir = systemEnvironment.GetCommsDirectoryPath();
            var fileName = Path.Combine(commsDir, "STHOF");
            using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                using (TextReader tr = new StreamReader(fs, Encoding.GetEncoding(1251)))
                {
                    string str;
                    while (!string.IsNullOrEmpty(str = tr.ReadLine()))
                    {
                        var code = str.Substring(0, 2);
                        var date = DateTime.Parse(str.Substring(2, 8), CultureInfo.CreateSpecificCulture("en-GB"));

                        Dictionary<String, String> d1;
                        if (_fileContent.ContainsKey(date))
                        {
                            d1 = _fileContent[date];
                            if (d1.ContainsKey(code))
                                continue;

                            d1.Add(code, str);
                        }
                        else
                        {
                            d1 = new Dictionary<string, string> {{code, str}};
                            _fileContent.Add(date, d1);
                        }
                    }
                }
            }
        }

        private string GetValue(DateTime date, string code, int position, int length)
        {
            if (!_fileContent.ContainsKey(date))
                return "";

            if (!_fileContent[date].ContainsKey(code))
                return "";

            var str = _fileContent[date][code];
            if (str.Length < position + length)
                return "";

            return str.Substring(position, length);
        }

        #region F3

        public decimal GetSalesInF3(DateTime date)
        {
            return GetAmountInF3ForItem(date, 1);
        }

        public decimal GetRefundsInF3(DateTime date)
        {
            return GetAmountInF3ForItem(date, 3);
        }

        private decimal GetAmountInF3ForItem(DateTime date, int itemType)
        {
            var pos = 14 + 10 * itemType;
            var str = GetValue(date, "F3", pos, 10);
            if (str.Length == 0)
                return 0;

            var positiveSign = String.IsNullOrWhiteSpace(str.Substring(9, 1));
            var result = decimal.Parse(str.Substring(0, 9), CultureInfo.CreateSpecificCulture("en-GB"));

            return positiveSign ? result : -result;
        }

        #endregion

        #region F4

        public decimal GetAmountInF4ForTender(DateTime date, int tenderType)
        {
            var pos = 14 + 10 * tenderType;
            var str = GetValue(date, "F4", pos, 10);
            if (str.Length == 0)
                return 0;

            var positiveSign = String.IsNullOrWhiteSpace(str.Substring(9, 1));
            var result = decimal.Parse(str.Substring(0, 9), CultureInfo.CreateSpecificCulture("en-GB"));

            return positiveSign ? result : -result;
        }

        #endregion

        #region F5

        public int GetCountInF5ForTender(DateTime date, int tenderType)
        {
            var pos = 19 + 5 * tenderType;
            var str = GetValue(date, "F5", pos, 5);
            if (str.Length == 0)
                return 0;

            var positiveSign = String.IsNullOrWhiteSpace(str.Substring(4, 1));
            var result = int.Parse(str.Substring(0, 4));

            return positiveSign ? result : -result;
        }

        #endregion

        #region F7

        public decimal GetSalesAmountInF7(DateTime date)
        {
            return GetAmountInF7ForItem(date, 1);
        }

        public decimal GetSalesCountInF7(DateTime date)
        {
            return GetCountInF7ForItem(date, 1);
        }

        public decimal GetRefundsAmountInF7(DateTime date)
        {
            return GetAmountInF7ForItem(date, 2);
        }

        public decimal GetRefundsCountInF7(DateTime date)
        {
            return GetCountInF7ForItem(date, 2);
        }

        private decimal GetAmountInF7ForItem(DateTime date, int itemType)
        {
            var pos = 11 + 17 * itemType;
            var str = GetValue(date, "F7", pos, 10);
            if (str.Length == 0)
                return 0;

            var positiveSign = String.IsNullOrWhiteSpace(str.Substring(9, 1));
            var result = decimal.Parse(str.Substring(0, 9), CultureInfo.CreateSpecificCulture("en-GB"));

            return positiveSign ? result : -result;
        }

        private int GetCountInF7ForItem(DateTime date, int itemType)
        {
            var pos = 21 + 17 * itemType;
            var str = GetValue(date, "F7", pos, 7);
            if (str.Length == 0)
                return 0;

            var positiveSign = String.IsNullOrWhiteSpace(str.Substring(6, 1));
            var result = int.Parse(str.Substring(0, 6));

            return positiveSign ? result : -result;
        }

        #endregion
    }
}
