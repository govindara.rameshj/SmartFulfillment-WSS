﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSS.AAT.Common.Utility.FileParsers
{
    static class ParseUtils
    {
        public static int ParseAsInt(this string s)
        {
            return (int)ParseAsDecimal(s);
        }

        public static string[] SplitAt(this string str, int[] pointsArg)
        {
            var points = (new[] { 0 })
                .Concat(pointsArg.Where(p => p < str.Length))
                .Concat(new[] { str.Length });

            var result = points.Zip(points.Skip(1),
                (left, right) => str.Substring(left, right - left).Trim()
            ).ToArray();

            return result.Concat(Enumerable.Repeat("", pointsArg.Length - result.Length + 1)).ToArray();
        }

        public static decimal ParseAsDecimal(this string s)
        {
            if (s.EndsWith("-"))
            {
                return -(decimal.Parse(s.Substring(0, s.Length - 1)));
            }
            else
            {
                return decimal.Parse(s);
            }
        }

        public static int[] GetSplitPositionsByExample(string s)
        {
            List<int> result = new List<int>();

            var pos = 0;
            for (var n = 0; n < s.Length; n++)
            {
                if (s[n] == '|')
                {
                    result.Add(pos);
                }
                else
                {
                    pos++;
                }
            }
            return result.ToArray();
        }

    }
}
