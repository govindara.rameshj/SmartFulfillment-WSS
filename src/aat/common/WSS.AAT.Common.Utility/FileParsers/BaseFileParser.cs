using System;
using System.IO;
using Cts.Oasys.Core.SystemEnvironment;

namespace WSS.AAT.Common.Utility.FileParsers
{
    public abstract class BaseFileParser
    {
        protected abstract void ParseLine(string line);

        protected BaseFileParser(ISystemEnvironment systemEnvironment, string fileName)
        {
            var filePath = GetFilePath(systemEnvironment, fileName);
            ParseFile(filePath);
        }

        private void ParseFile(string filePath)
        {
            if (!File.Exists(filePath))
                throw new Exception("File not found: " + filePath);
            using (StreamReader file = new StreamReader(filePath))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    ParseLine(line);
                }
            }
        }

        private static string GetFilePath(ISystemEnvironment systemEnvironment, string fileName)
        {
            string commDirectory = systemEnvironment.GetCommsDirectoryPath();
            string filePath = Path.GetFullPath(Path.Combine(commDirectory, fileName));
            return filePath;
        }

        protected static bool FileExists(ISystemEnvironment systemEnvironment, string fileName)
        {
            return File.Exists(GetFilePath(systemEnvironment, fileName));
        }
    }
}
