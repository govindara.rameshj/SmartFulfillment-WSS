using System;
using System.Linq;
using System.Collections.Generic;
using Cts.Oasys.Core.SystemEnvironment;
using WSS.AAT.Common.Utility.Utility;

namespace WSS.AAT.Common.Utility.FileParsers
{
    public class SthoaFileQuery
    {
        enum RecordType
        {
            A8,
            AH
        }

        SthoaFileParser parser;
        private string _checkDateString = "";
        private RecordType recordType;
        
        public SthoaFileQuery(ISystemEnvironment systemEnvironment, string recordType, DateTime date)
        {
            parser = new SthoaFileParser(systemEnvironment);
            _checkDateString = date.ToString("dd/MM/yy");
            this.recordType = (RecordType)Enum.Parse(typeof(RecordType), recordType);
        }

        public void table(List<List<String>> table)
        {
            //optional function
        }

        public List<List<List<object>>> query()
        {
            switch (recordType)
            {
                case RecordType.A8:
                    {
                        var records = parser.SelectA8Lines().Where(rec => rec.Date == _checkDateString);
                        var builder = new SlimQueryTableBuilder("Account", "Value");
                        builder.AddRows(records, l => new List<object> { l.Account, l.Value2 });
                        return builder.GetResult();
                    }
                case RecordType.AH:
                    {
                        var records = parser.SelectAHLines().Where(rec => rec.Date == _checkDateString);
                        var builder = new SlimQueryTableBuilder("Category", "Value");
                        builder.AddRows(records, l => new List<object> { l.Category, l.Value2 });
                        return builder.GetResult();
                    }
                default:
                    throw new ApplicationException(String.Format("Unknown record type {0}", recordType)); 
            }
        }
    }
}
