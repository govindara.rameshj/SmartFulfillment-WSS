using System;
using System.Linq;
using System.Collections.Generic;
using Cts.Oasys.Core.SystemEnvironment;

namespace WSS.AAT.Common.Utility.FileParsers
{
    public class SthoaFileParser : BaseFileParser
    {
        private const string FileName = "STHOA";

        public SthoaFileParser(ISystemEnvironment systemEnvironment)
            : base(systemEnvironment, FileName)
        {
        }

        public class A8Line
        {
            public string Date;
            public decimal Value1;
            public string Account;
            public decimal Value2;
            public string PickUp;
        }

        public class A5Line
        {
            public string Date;
            public string Hash;
            public string SKU;
            public int DailyUnits;
            public decimal DailyTotal;
            public int ClosingStockQuantity;
        }

        public class AHLine
        {
            public string Date;
            public decimal Value1;
            public string Category;
            public decimal Value2;
        }

        readonly List<A8Line> A8Records = new List<A8Line>();
        readonly List<AHLine> AHRecords = new List<AHLine>();

        readonly Dictionary<string, A5Line> A5Records = new Dictionary<string, A5Line>();

        private string _checkDateString = "";
        
        public void SetCheckDate(DateTime date)
        {
            _checkDateString = date.ToString("dd/MM/yy");
        }

        public decimal GetA8ValueByAccountOn(string targetAccount, DateTime date)
        {
            SetCheckDate(date);
            return GetA8ValueByAccount(targetAccount);
        }

        public decimal GetA8ValueByAccount(string targetAccount)
        {
            var a8Record = A8Records.Where(rec =>
                            rec.Date == _checkDateString
                            && rec.Account == targetAccount)
                            .SingleOrDefault();

            if (a8Record != null)
            {
                return a8Record.Value2;
            }
            else
            {
                throw new Exception(String.Format(
                    "There is no line with record type={0}, date={1} and account={2}"
                    , "A8", _checkDateString, targetAccount));
            }
        }

        public decimal GetAHValueByCategory(string targetCategory)
        {
            var ahRecord = AHRecords.Where(rec =>
                            rec.Date == _checkDateString
                            && rec.Category == targetCategory)
                            .SingleOrDefault();

            if (ahRecord != null)
            {
                return ahRecord.Value2;
            }
            else
            {
                throw new Exception(String.Format(
                    "There is no line with record type={0}, date={1} and category={2}"
                    , "AH", _checkDateString, targetCategory));
            }
        }

        public int GetA5DailyUnitsForSKU(string sku)
        {
            return A5Records[sku].DailyUnits;
        }

        public decimal GetA5DailyTotalForSKU(string sku)
        {
            return A5Records[sku].DailyTotal;
        }

        public int GetA5ClosingStockQuantityForSKU(string sku)
        {
            return A5Records[sku].ClosingStockQuantity;
        }

        protected override void ParseLine(string line)
        {
            switch (line.Substring(0, 2))
            {
                case "A8": A8Records.Add(ParseA8(line)); break;
                case "AH": AHRecords.Add(ParseAH(line)); break;
                case "A5":
                    var rec = ParseA5(line);
                    A5Records[rec.SKU] = rec;
                    break;
            }
        }

        static int[] A8SplitPositions = ParseUtils.GetSplitPositionsByExample(
            "A8|20/01/14|      47.00 |8498|      47.00 |       0.00 ");

        static A8Line ParseA8(string line)
        {
            var parts = line.SplitAt(A8SplitPositions);

            return new A8Line 
            {
                Date = parts[1],
                Value1 = parts[2].ParseAsDecimal(),
                Account = parts[3],
                Value2 = parts[4].ParseAsDecimal(),
                PickUp = parts[5]
            };
        }

        static int[] A5SplitPositions = ParseUtils.GetSplitPositionsByExample(
                "A5|20/01/14|      36.00 |100012|      3 |      36.00 |      3-|   14007.00-|14020 |     0 |        0.00 |      0 |       0.00 |      0 |       0.00 "
            );

        static A5Line ParseA5(string line)
        {
            var parts = line.SplitAt(A5SplitPositions);

            return new A5Line
            {
                Date = parts[1],
                Hash = parts[2],
                SKU = parts[3],
                DailyUnits = parts[4].ParseAsInt(),
                DailyTotal = parts[5].ParseAsDecimal(),
                ClosingStockQuantity = parts[6].ParseAsInt()
            };
        }

        static int[] AHSplitPositions = ParseUtils.GetSplitPositionsByExample(
            "AH|05/02/14|     125.97-|000017|     125.97-");

        static AHLine ParseAH(string line)
        {
            var parts = line.SplitAt(AHSplitPositions);

            return new AHLine
            {
                Date = parts[1],
                Value1 = parts[2].ParseAsDecimal(),
                Category = parts[3],
                Value2 = parts[4].ParseAsDecimal()
            };
        }

        public IEnumerable<A8Line> SelectA8Lines()
        {
            return A8Records; 
        }

        public IEnumerable<AHLine> SelectAHLines()
        {
            return AHRecords;
        }

        public static bool FileExists(ISystemEnvironment systemEnvironment)
        {
            return FileExists(systemEnvironment, FileName);
        }
    }
}
