using System;
using System.Collections.Generic;
using System.Linq;
using Cts.Oasys.Core.SystemEnvironment;
using WSS.AAT.Common.Utility.Utility;

namespace WSS.AAT.Common.Utility.FileParsers
{
    public class SthotFileParser : BaseFileParser
    {
        public SthotFileParser(ISystemEnvironment systemEnvironment)
            : base(systemEnvironment, "sthot")
        {
        }

        class DTLine
        {
            public string OrderNumber;
            public string Date;
            public decimal Total;
        }

        class BSLine
        {
            public string RecordType;
            public DateTime Date;
            public int Hash;
            public string SingleSku;
            public int SingleSkuAdjQty;
            public decimal SingleSkuPrice;
            public string PackSku;
            public int PackSkuAdjQty;
            public decimal PackSkuPrice;
        }

        List<DTLine> DTLines = new List<DTLine>();
        Dictionary<string, BSLine> BSLines = new Dictionary<string,BSLine>();

        public decimal GetOrdersCountForDate(DateTime date)
        {
            var parsedDate = date.ToString("dd/MM/yy");
            return DTLines.Where(l => l.Date == parsedDate).Count();
        }

        public decimal GetOrderValueForDateOrderNumber(DateTime date, int orderNumber)
        {
            var parsedDate = date.ToString("dd/MM/yy");
            return DTLines.Where(l => l.Date == parsedDate).ToArray()[orderNumber - 1].Total;
        }

        public string GetBSRecordType(string singleSku)
        {
            return BSLines[singleSku].RecordType;
        }

        public string GetBSTransactionDate(string singleSku)
        {
            return DateTimeParser.EncodeDateTime(BSLines[singleSku].Date, DateTime.Now);
        }

        public int GetBSHashValue(string singleSku)
        {
            return BSLines[singleSku].Hash;
        }

        public string GetBSSingleSkuCode(string singleSku)
        {
            return BSLines[singleSku].SingleSku;
        }

        public int GetBSSingleSkuAdjustmentQuantity(string singleSku)
        {
            return BSLines[singleSku].SingleSkuAdjQty;
        }

        public string GetBSPackSkuCode(string singleSku)
        {
            return BSLines[singleSku].PackSku;
        }

        public int GetBSPackSkuAdjustmentQuantity(string singleSku)
        {
            return BSLines[singleSku].PackSkuAdjQty;
        }

        public decimal GetBSPackSkuPrice(string singleSku)
        {
            return BSLines[singleSku].PackSkuPrice;
        }

        public decimal GetBSSingleSkuPrice(string singleSku)
        {
            return BSLines[singleSku].SingleSkuPrice;
        }

        public bool IsExistsBSRecordForSku(string sku)
        {
            return BSLines.ContainsKey(sku);
        }

        protected override void ParseLine(string line)
        {
            switch (line.Substring(0, 2))
            {
                case "DT": DTLines.Add(ParseDTLine(line)); break;
                case "BS":
                    var rec = ParseBSLine(line);
                    BSLines[rec.SingleSku] = rec;
                    break;
            }
        }

        int[] DTLineSplitPositions = ParseUtils.GetSplitPositionsByExample(
"DT|19/01/14|       2.00 |900002|499152501000SA|                      026944NN000NY|90000002|N|       46.00 |     0.00      9.33      0.00 000    46.00 00000000NYN00000020.000 a    46.67      9.33  0.000 b     0.00      0.00  5.000 c     0.00      0.00  0.000 d     0.00      0.00  0.000 e     0.00      0.00  0.000 f     0.00      0.00 17.500 g     0.00      0.00 15.000 h     0.00      0.00 17.500 i     0.00      0.00 Nollect 9900000NY 0 99    20/01/14collect 9951       *");

        DTLine ParseDTLine(string line)
        {
            var parts = line.SplitAt(DTLineSplitPositions);

            return new DTLine
            {
                Date = parts[1],
                OrderNumber = parts[6],
                Total = parts[8].ParseAsDecimal()
            };
        }

        int[] BSLineSplitPositions = ParseUtils.GetSplitPositionsByExample(
            "BS|18/02/14|       6.00 |100147|     6 |158058|     1-|    28.98 |     4.83 ");

        BSLine ParseBSLine(string line)
        {
            var parts = line.SplitAt(BSLineSplitPositions);

            return new BSLine
            {
                RecordType = parts[0],
                Date = DateTime.ParseExact(parts[1], "dd/MM/yy", null),
                Hash = parts[2].ParseAsInt(),
                SingleSku = parts[3],
                SingleSkuAdjQty = parts[4].ParseAsInt(),
                PackSku = parts[5],
                PackSkuAdjQty = parts[6].ParseAsInt(),
                PackSkuPrice = parts[7].ParseAsDecimal(),
                SingleSkuPrice = parts[8].ParseAsDecimal()
            };
        }
    }
}
