using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Cts.Oasys.Core.SystemEnvironment;

namespace WSS.AAT.Common.Utility.FileParsers
{
    public class SthpaFileParser
    {
        private readonly List<String> _fileContent = new List<String>();

        public SthpaFileParser(ISystemEnvironment systemEnvironment)
        {
            string commsDir = systemEnvironment.GetCommsDirectoryPath();

            var fileName = Path.Combine(commsDir, "STHPA");
            using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                using (TextReader tr = new StreamReader(fs, Encoding.GetEncoding(1251)))
                {
                    string str;
                    while (!string.IsNullOrEmpty(str = tr.ReadLine()))
                    {
                        if (str.StartsWith("AR"))
                            _fileContent.Add(str);
                    }
                }
            }
        }

        private string FindLineForSku(string sku)
        {
            return _fileContent.FirstOrDefault(s => s.Length >= 53 && s.Substring(47, 6).Equals(sku));
        }

        public bool IsLineForSkuExists(string sku)
        {
            return !string.IsNullOrEmpty(FindLineForSku(sku));
        }

        public int GetCountForSku(string sku)
        {
            var str = FindLineForSku(sku);
            if (string.IsNullOrEmpty(str) || str.Length < 60)
                return 0;

            var positiveSign = String.IsNullOrWhiteSpace(str.Substring(59, 1));
            var result = int.Parse(str.Substring(53, 6));

            return positiveSign ? result : -result;
        }

        public decimal GetAmountForSku(string sku)
        {
            var str = FindLineForSku(sku);
            if (string.IsNullOrEmpty(str) || str.Length < 72)
                return 0;

            var positiveSign = String.IsNullOrWhiteSpace(str.Substring(71, 1));
            var result = decimal.Parse(str.Substring(60, 11), CultureInfo.CreateSpecificCulture("en-GB"));

            return positiveSign ? result : -result;
        }

    }
}
