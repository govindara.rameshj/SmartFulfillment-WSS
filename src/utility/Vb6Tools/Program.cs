using System;
using System.IO;
using System.Configuration;
using System.Diagnostics;

namespace Vb6Tools
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var parts = args[0].Split('|');
                string rootProjectsDirectoryPath = parts[0];
                string vbpFilePath = Path.GetFullPath(Path.Combine(parts[0], parts[1]));

                var projectFile = new Vb6ProjectFile(vbpFilePath);

                Console.WriteLine("File [{0}]", vbpFilePath);

                Console.WriteLine("Making backup of vbp file");
                projectFile.BackUp();

                string binaryFilePath = projectFile.GetOutputBinaryFilePath();
                string compatibilityFilePath = projectFile.GetCompatibilityFilePath();

                var oldTypeLibInfo = new ComTypeLibraryInfo(compatibilityFilePath);
                Console.WriteLine("Old type lib info: {0}", oldTypeLibInfo);

                Console.WriteLine("Set binary compatibility off");
                projectFile.SetBinaryCompatibilityOff();
                projectFile.IncRevision();
                projectFile.Save();

                MakeProject(vbpFilePath);

                var newTypeLibInfo = new ComTypeLibraryInfo(binaryFilePath);
                if (newTypeLibInfo.Guid == oldTypeLibInfo.Guid)
                {
                    throw new ApplicationException("Type lib GUID has not been changed after setting BinaryCompatibility=Off.");
                }

                Console.WriteLine("New type lib info: {0}", newTypeLibInfo);

                File.Copy(binaryFilePath, compatibilityFilePath, true);
                Console.WriteLine("Copy project binary to compatibility");

                Console.WriteLine("Set binary compatibility on");
                projectFile.SetBinaryCompatibilityOn();
                projectFile.Save();

                MakeProject(vbpFilePath);

                var newTypeLibInfo2 = new ComTypeLibraryInfo(binaryFilePath);
                if (newTypeLibInfo.Guid != newTypeLibInfo2.Guid)
                {
                    throw new ApplicationException("Type lib GUID has been changed after setting BinaryCompatibility=Off.");
                }

                Console.WriteLine("New type lib info(2): {0}", newTypeLibInfo2);

                var vbpFiles = Directory.EnumerateFiles(rootProjectsDirectoryPath, "*.vbp", SearchOption.AllDirectories);
                bool anyReplaced = false;
                foreach (var vbpFile in vbpFiles)
                {
                    Console.Write("Processing [{0}] ...", vbpFile);
                    Vb6ProjectFile file = new Vb6ProjectFile(vbpFile);
                    bool replaced = file.ReplaceReference(oldTypeLibInfo, newTypeLibInfo);
                    if (replaced)
                    {
                        file.IncRevision();
                        file.Save();
                        anyReplaced = true;
                    }
                    Console.WriteLine(replaced ? "Replaced" : "Skipped");
                }

                if (!anyReplaced)
                {
                    throw new ApplicationException("No reference found. Something is wrong. Check if there is actual compatibiltiy binary.");
                }
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine("ERROR:" + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex); 
            }
        }

        private static void MakeProject(string vbpFilePath)
        {
            Console.Write("Making project binary ... ");
            var process = Process.Start(ConfigurationManager.AppSettings["vb6ExeFilePath"], String.Format("/make \"{0}\"", vbpFilePath));
            Debug.Assert(process != null, "process != null");
            process.WaitForExit();
            if (process.ExitCode != 0)
            {
                throw new ApplicationException("VB6 executable exited with exit code = " + process.ExitCode);
            }
            Console.WriteLine("Done");
        }
    }
}
