using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Vb6Tools
{
    class Vb6ProjectFile
    {
        private readonly string filePath;
        private string fileContents;
        private readonly string directoryName;

        public Vb6ProjectFile(string filePath)
        {
            this.filePath = filePath;
            fileContents = File.ReadAllText(filePath);
            directoryName = Path.GetDirectoryName(this.filePath);
        }

        public void Save()
        {
            File.WriteAllText(filePath, fileContents);
        }

        public void SetBinaryCompatibilityOff()
        {
            fileContents = fileContents.Replace("CompatibleMode=\"2\"", "CompatibleMode=\"0\"");
        }

        public void SetBinaryCompatibilityOn()
        {
            fileContents = fileContents.Replace("CompatibleMode=\"0\"", "CompatibleMode=\"2\"");
        }

        private string GetParameterValue(string parameterName, bool quotes)
        {
            string regex = quotes ? "{0}=\"(.+)\"" : "{0}=(.+)";
            var match = Regex.Match(fileContents, String.Format(regex, parameterName));
            if (match == Match.Empty)
            {
                throw new ApplicationException(String.Format("{0} not found in VBP file.", parameterName));
            }

            return match.Groups[1].Value;
        }

        public string GetOutputBinaryFilePath()
        {
            string result = Path.GetFullPath(
                Path.Combine(
                    directoryName,
                    GetParameterValue("Path32", true),
                    GetParameterValue("ExeName32", true)));
            return result;
        }

        public string GetCompatibilityFilePath()
        {
            string result = Path.GetFullPath(
                Path.Combine(
                    directoryName,
                    GetParameterValue("CompatibleEXE32", true)));
            return result;
        }

        public bool ReplaceReference(ComTypeLibraryInfo oldTypeLibraryInfo, ComTypeLibraryInfo newTypeLibraryInfo)
        {
            string newReference = BuildTypeLibraryReferenceKey(newTypeLibraryInfo);

            // Process *.frm files
            string frmRegex = BuildRegexForFrmReferenceAnyVersion(oldTypeLibraryInfo);

            DirectoryInfo dir = new DirectoryInfo(directoryName);
            foreach (var frmFileInfo in dir.EnumerateFiles("*.frm"))
            {
                var oldFrmContent = File.ReadAllText(frmFileInfo.FullName);
                string replacedFrmContents = Regex.Replace(oldFrmContent, frmRegex, newReference, RegexOptions.IgnoreCase);
                if (oldFrmContent != replacedFrmContents)
                {
                    File.WriteAllText(frmFileInfo.FullName, replacedFrmContents);  
                }
            }

            string regex = BuildRegexForTypeLibraryReferenceAnyVersion(oldTypeLibraryInfo);
            string replacedContents = Regex.Replace(fileContents, regex, newReference, RegexOptions.IgnoreCase);

            bool replaced = false;
            if (replacedContents != fileContents)
            {
                replaced = true;
                fileContents = replacedContents;
            }

            return replaced;
        }

        public string BuildRegexForFrmReferenceAnyVersion(ComTypeLibraryInfo info)
        {
            return String.Format(@"(?<=Object = ""){{{0}}}#[\d|a-f]*\.[\d|a-f]*#", info.Guid.ToString().ToUpper());
        }

        public string BuildRegexForTypeLibraryReferenceAnyVersion(ComTypeLibraryInfo info)
        {
            return String.Format(@"(?<=(Reference=\*\\G)|(Object=)){{{0}}}#[\d|a-f]*\.[\d|a-f]*#", info.Guid.ToString().ToUpper());
        }

        public string BuildTypeLibraryReferenceKey(ComTypeLibraryInfo info)
        {
            return String.Format(@"{{{0}}}#{1}.{2}#", info.Guid.ToString().ToUpper(), info.Version.Major, info.Version.Minor);
        }

        public void BackUp()
        {
            File.Copy(filePath, String.Format("{0}.{1:yyyyMMdd_HHmmss}.bak", filePath, DateTime.Now)); 
        }

        public void IncRevision()
        {
            int currentRevision = Convert.ToInt32(GetParameterValue("RevisionVer", false));
            fileContents = fileContents.Replace(String.Format("RevisionVer={0}", currentRevision), String.Format("RevisionVer={0}", currentRevision + 1));
        }
    }
}
