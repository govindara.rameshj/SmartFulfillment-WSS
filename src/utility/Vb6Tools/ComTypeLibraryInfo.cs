using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;

namespace Vb6Tools
{
    class ComTypeLibraryInfo
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        internal enum REGKIND
        {
            REGKIND_DEFAULT = 0,
            REGKIND_REGISTER = 1,
            REGKIND_NONE = 2,
        }

        [DllImport("oleaut32.dll", CharSet = CharSet.Unicode, PreserveSig = false)]
        static extern ITypeLib LoadTypeLibEx(string szFile, REGKIND regkind);

        public ComTypeLibraryInfo(string filePath)
        {
            var typeLib = LoadTypeLibEx(filePath, REGKIND.REGKIND_NONE);
            try
            {
                IntPtr ptr;
                typeLib.GetLibAttr(out ptr);
                var t = (System.Runtime.InteropServices.ComTypes.TYPELIBATTR)Marshal.PtrToStructure(ptr, typeof(System.Runtime.InteropServices.ComTypes.TYPELIBATTR));

                Guid = t.guid;
                Version = new Version(t.wMajorVerNum, t.wMinorVerNum); 

                typeLib.ReleaseTLibAttr(ptr);
            }
            finally
            {
                Marshal.ReleaseComObject(typeLib);
            }
        }

        public Guid Guid {get; private set;}
        public Version Version { get; private set; }

        public override string ToString()
        {
            return String.Format("{0}.{1}#{2}", Version.Major, Version.Minor, Guid);
        }
    }
}
