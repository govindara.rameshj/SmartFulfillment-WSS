﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using OciusEmulator.Core;
using System.Diagnostics;

namespace OciusEmulatorFrontend
{
    public partial class FormGiftCardDetails : Form
    {
        public string CardNumber;
        public GiftCard NewCard;

        public FormGiftCardDetails(CardInputMethod? method)
        {
            InitializeComponent();
            var settings = ConfigurationManager.AppSettings;
            if (bool.Parse(settings["AlwaysOnTop"]))
                this.TopMost = true;

            if (method.HasValue)
            {
                buttonOK.Text = "Swipe";
            }
            else
            {
                createNew.Checked = true;
                createNew.Enabled = false;
            }

            CardNumber = null;
            NewCard = null;
        }

        public FormGiftCardDetails() :
            this(null)
        {
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            CardNumber = textBoxCardNumber.Text;
            if (createNew.Checked)
            {
                decimal balance;
                if (!decimal.TryParse(textBoxBalance.Text, out balance))
                {
                    MessageBox.Show("Please enter balance using correct format", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                NewCard = new GiftCard(CardNumber, balance);
                NewCard.LostOrStolen = checkBoxLostOrStolen.Checked;
                NewCard.Expired = checkBoxExpired.Checked;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

		private void CreateNew_CheckedChanged(object sender, EventArgs e)
		{
			panelCardInfo.Enabled = createNew.Checked;
		}

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
