﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OciusEmulator.Core.Interaction;
using System.Windows.Forms;

namespace OciusEmulatorFrontend.Interaction
{
    internal class BaseInteractionControl : UserControl
    {
        internal delegate void OnInterationDelegate(BaseInteractionResponse response);
        internal event OnInterationDelegate OnInteration = null;

        protected void TriggerOnInteraction(BaseInteractionResponse response)
        {
            foreach(Control ctl in this.Controls)
                ctl.Enabled = false;

            if (OnInteration != null)
                OnInteration(response);
        }
    }
}
