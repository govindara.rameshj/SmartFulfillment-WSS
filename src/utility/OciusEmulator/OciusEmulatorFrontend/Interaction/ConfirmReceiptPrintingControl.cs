﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OciusEmulator.Core.Interaction;
using OciusEmulator.Core;

namespace OciusEmulatorFrontend.Interaction
{
    internal partial class ConfirmReceiptPrintingControl : BaseInteractionControl
    {
        public ConfirmReceiptPrintingControl(ReceiptType receiptType)
        {
            InitializeComponent();

            labelText.Text = "Please tear " + ((receiptType == ReceiptType.Merchant) ? "merchant" : "customer") + " receipt";
        }

        private void buttonContinue_Click(object sender, EventArgs e)
        {
            var resp = new ConfirmReceiptPrintingResponse(true);
            TriggerOnInteraction(resp);
        }

        private void buttonReprint_Click(object sender, EventArgs e)
        {
            var resp = new ConfirmReceiptPrintingResponse(false);
            TriggerOnInteraction(resp);
        }
    }
}
