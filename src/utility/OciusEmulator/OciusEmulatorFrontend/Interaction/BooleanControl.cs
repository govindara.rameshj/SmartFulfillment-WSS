﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OciusEmulator.Core.Interaction;
using OciusEmulator.Core;

namespace OciusEmulatorFrontend.Interaction
{
    internal partial class BooleanControl : BaseInteractionControl
    {
        public BooleanControl(string text)
        {
            InitializeComponent();

            labelText.Text = text;
        }

        private void buttonYes_Click(object sender, EventArgs e)
        {
            var resp = new BooleanResponse(true);
            TriggerOnInteraction(resp);
        }

        private void buttonNo_Click(object sender, EventArgs e)
        {
            var resp = new BooleanResponse(false);
            TriggerOnInteraction(resp);
        }
    }
}
