﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using OciusEmulator.Core;
using System.Diagnostics;

namespace OciusEmulatorFrontend
{
    public partial class FormCardDetails : Form
    {
        public string CardNumber;
        public Card NewCard;

        public FormCardDetails(CardInputMethod? method)
        {
            InitializeComponent();
            var settings = ConfigurationManager.AppSettings;
            if (bool.Parse(settings["AlwaysOnTop"]))
                this.TopMost = true;

            if (method.HasValue)
            {
                if (method.Value == CardInputMethod.Swiped)
                    buttonOK.Text = "Swipe";
                else if (method.Value == CardInputMethod.ICC)
                    buttonOK.Text = "Insert";
            }
            else
            {
                createNew.Checked = true;
                createNew.Enabled = false;
            }

            CardNumber = null;
            NewCard = null;
        }

        public FormCardDetails() :
            this(null)
        {
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            CardNumber = textBoxCardNumber.Text;
            if (createNew.Checked)
            {
                NewCard = new Card(CardNumber, textBoxPIN.Text);
                if (!String.IsNullOrEmpty(textBoxBalance.Text))
                    NewCard.Balance = Convert.ToDecimal(textBoxBalance.Text);
                else
                    NewCard.Balance = 0;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

		private void CreateNew_CheckedChanged(object sender, EventArgs e)
		{
			panelCardInfo.Enabled = createNew.Checked;
		}

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
