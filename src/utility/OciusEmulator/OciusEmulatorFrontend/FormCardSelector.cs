﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace OciusEmulatorFrontend
{
    public partial class FormCardSelector : Form
    {
        public string CardType;

        public FormCardSelector()
        {
            InitializeComponent();

            var settings = ConfigurationManager.AppSettings;
            if (bool.Parse(settings["AlwaysOnTop"]))
                this.TopMost = true;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (radioButtonCard.Checked)
                CardType = "Card";
            else if (radioButtonGiftCard.Checked)
                CardType = "GiftCard";
            else
                return;
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
