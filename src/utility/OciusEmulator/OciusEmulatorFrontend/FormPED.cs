﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using OciusEmulator;
using OciusEmulator.Core;

namespace OciusEmulatorFrontend
{
    public partial class FormPED : Form
    {
        private Point mouseOffset;
        private bool isMouseDown = false;

        private Device device;
        private Timer timer;

        public FormPED(Device device)
        {
            InitializeComponent();

            this.device = device;
            var settings = ConfigurationManager.AppSettings;

            Size keyPadSize = panelKeypad.Size;
            if (!device.Mode820)
                BackgroundImage = Properties.Resources.VX810;
            Size = BackgroundImage.Size;

            if (!device.Mode820)
            {
                BackgroundImage = Properties.Resources.VX810;

                panelKeypad.Location = new Point(58, 297);
                panelKeypad.Size = keyPadSize;
                textBoxDisplay.Location = new Point(47, 51);
                textBoxDisplay.Size = new Size(179, 182);

                buttonSwipe.Left -= 18;
                buttonChipInsert.Top += 8;
            }

            device.OnDisplayRefresh += new Device.OnDisplayRefreshDelegate(device_OnDisplayRefresh);

            if (bool.Parse(settings["AlwaysOnTop"]))
            {
                timer = new Timer();
                timer.Tick += new EventHandler(timer_Tick);
                timer.Interval = 1000;
                timer.Start();
            }

            var currentScreen = Screen.FromControl(this);
            Location = new Point(currentScreen.WorkingArea.Width - Width - 20, currentScreen.WorkingArea.Height - Height - 20);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            this.WindowState = FormWindowState.Normal;
            this.TopMost = true;
        }

        void device_OnDisplayRefresh(string content)
        {
            if (InvokeRequired)
            {
                Action action = () => device_OnDisplayRefresh(content);
                Invoke(action);
                return;
            }

            textBoxDisplay.Text = content;
        }

        private void FormPED_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.ExitThread();
        }

        private void FormPED_MouseDown(object sender, MouseEventArgs e)
        {
            int xOffset;
            int yOffset;

            if (e.Button == MouseButtons.Left)
            {
                xOffset = -e.X; // -SystemInformation.FrameBorderSize.Width;
                yOffset = -e.Y; // -SystemInformation.CaptionHeight - SystemInformation.FrameBorderSize.Height;
                mouseOffset = new Point(xOffset, yOffset);
                isMouseDown = true;
            } 
        }

        private void FormPED_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos;
            }
        }

        private void FormPED_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                isMouseDown = false;
        }

        private void buttonMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void buttonPad_Click(object sender, EventArgs e)
        {
            try
            {
                device.PressButton(((Button)sender).Tag.ToString());
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string getCard(CardInputMethod method)
        {
            var formSelector = new FormCardSelector();
            if (formSelector.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                if (formSelector.CardType == "Card")
                {
                    var form = new FormCardDetails(method);
                    if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    {
                        if (form.NewCard != null)
                            CardsRepository.AddCard(form.NewCard);
                        return form.CardNumber;
                    }
                }
                else if (formSelector.CardType == "GiftCard")
                {
                    var form = new FormGiftCardDetails(method);
                    if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    {
                        if (form.NewCard != null)
                            CardsRepository.AddCard(form.NewCard);
                        return form.CardNumber;
                    }
                }
            }
            return null;
        }

        private void inputCard(CardInputMethod method, Button button)
        {
            try
            {
                if (method == CardInputMethod.ICC)
                {
                    if ((string)button.Tag == "INSERTED")
                    {
                        device.RemoveCard();
                        button.BackColor = Color.FromKnownColor(KnownColor.Control);
                        button.Tag = null;
                        button.Text = "INSERT CARD";
                    }
                    else
                    {
                        string cardNumber = getCard(method);
                        if (cardNumber != null)
                        {
                            if (device.InsertCard(cardNumber))
                            {
                                button.BackColor = Color.FromKnownColor(KnownColor.Lime);
                                button.Tag = "INSERTED";
                                button.Text = "REMOVE CARD";
                            }
                        }
                    }
                }
                else if (method == CardInputMethod.Swiped)
                {
                    string cardNumber = getCard(method);
                    if (cardNumber != null)
                        device.SwipeCard(cardNumber);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonSwipe_Click(object sender, EventArgs e)
        {
            inputCard(CardInputMethod.Swiped, sender as Button);
        }

        private void buttonChipInsert_Click(object sender, EventArgs e)
        {
            inputCard(CardInputMethod.ICC, sender as Button);
        }
    }
}
