﻿namespace OciusEmulatorFrontend
{
    partial class FormCardDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.textBoxCardNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPIN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.createNew = new System.Windows.Forms.CheckBox();
            this.panelCardInfo = new System.Windows.Forms.Panel();
            this.Balance = new System.Windows.Forms.Label();
            this.textBoxBalance = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panelCardInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(129, 146);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "Add";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // textBoxCardNumber
            // 
            this.textBoxCardNumber.Location = new System.Drawing.Point(85, 16);
            this.textBoxCardNumber.Name = "textBoxCardNumber";
            this.textBoxCardNumber.Size = new System.Drawing.Size(177, 20);
            this.textBoxCardNumber.TabIndex = 1;
            this.textBoxCardNumber.Text = "4012888888881881";
            this.textBoxCardNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Card number";
            // 
            // textBoxPIN
            // 
            this.textBoxPIN.Location = new System.Drawing.Point(84, 9);
            this.textBoxPIN.Name = "textBoxPIN";
            this.textBoxPIN.Size = new System.Drawing.Size(177, 20);
            this.textBoxPIN.TabIndex = 1;
            this.textBoxPIN.Text = "0000";
            this.textBoxPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Correct PIN";
            // 
            // createNew
            // 
            this.createNew.AutoSize = true;
            this.createNew.Checked = true;
            this.createNew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.createNew.Location = new System.Drawing.Point(15, 42);
            this.createNew.Name = "createNew";
            this.createNew.Size = new System.Drawing.Size(82, 17);
            this.createNew.TabIndex = 2;
            this.createNew.Tag = "CreateNew";
            this.createNew.Text = "Create New";
            this.createNew.UseVisualStyleBackColor = true;
            this.createNew.CheckedChanged += new System.EventHandler(this.CreateNew_CheckedChanged);
            // 
            // panelCardInfo
            // 
            this.panelCardInfo.Controls.Add(this.Balance);
            this.panelCardInfo.Controls.Add(this.textBoxBalance);
            this.panelCardInfo.Controls.Add(this.label2);
            this.panelCardInfo.Controls.Add(this.textBoxPIN);
            this.panelCardInfo.Location = new System.Drawing.Point(15, 65);
            this.panelCardInfo.Name = "panelCardInfo";
            this.panelCardInfo.Size = new System.Drawing.Size(270, 70);
            this.panelCardInfo.TabIndex = 3;
            // 
            // Balance
            // 
            this.Balance.AutoSize = true;
            this.Balance.Location = new System.Drawing.Point(4, 48);
            this.Balance.Name = "Balance";
            this.Balance.Size = new System.Drawing.Size(46, 13);
            this.Balance.TabIndex = 2;
            this.Balance.Text = "Balance";
            // 
            // textBoxBalance
            // 
            this.textBoxBalance.Location = new System.Drawing.Point(84, 41);
            this.textBoxBalance.Name = "textBoxBalance";
            this.textBoxBalance.Size = new System.Drawing.Size(177, 20);
            this.textBoxBalance.TabIndex = 3;
            this.textBoxBalance.Text = "0.00";
            this.textBoxBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(210, 146);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // FormCardDetails
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(292, 181);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.panelCardInfo);
            this.Controls.Add(this.createNew);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxCardNumber);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCardDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Specify card details";
            this.panelCardInfo.ResumeLayout(false);
            this.panelCardInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label Balance;
        protected System.Windows.Forms.TextBox textBoxCardNumber;
        protected System.Windows.Forms.TextBox textBoxPIN;
        protected System.Windows.Forms.CheckBox createNew;
        protected System.Windows.Forms.Panel panelCardInfo;
        protected System.Windows.Forms.TextBox textBoxBalance;
        private System.Windows.Forms.Button buttonCancel;
    }
}