﻿namespace OciusEmulatorFrontend
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addgiftCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showBinaryDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoscrollDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.troublesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commsdownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.declineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voiceReferralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reversedInOfflineModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ociusOfflineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nonidleScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.failedToRetrieveMessageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unknownProgressMessageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerClose = new System.Windows.Forms.Timer(this.components);
            this.panelApp = new System.Windows.Forms.Panel();
            this.panelAppDisplay = new System.Windows.Forms.Panel();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem_Tray = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.panelApp.SuspendLayout();
            this.contextMenuStripTray.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.BackColor = System.Drawing.Color.Black;
            this.richTextBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxLog.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxLog.ForeColor = System.Drawing.Color.Lime;
            this.richTextBoxLog.Location = new System.Drawing.Point(0, 153);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.ReadOnly = true;
            this.richTextBoxLog.Size = new System.Drawing.Size(468, 237);
            this.richTextBoxLog.TabIndex = 0;
            this.richTextBoxLog.Text = "";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.logToolStripMenuItem,
            this.troublesToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(492, 24);
            this.menuStrip.TabIndex = 3;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.addgiftCardToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.fileToolStripMenuItem.Text = "&Card";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.addToolStripMenuItem.Text = "&Add...";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // addgiftCardToolStripMenuItem
            // 
            this.addgiftCardToolStripMenuItem.Name = "addgiftCardToolStripMenuItem";
            this.addgiftCardToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.addgiftCardToolStripMenuItem.Text = "Add &gift card...";
            this.addgiftCardToolStripMenuItem.Click += new System.EventHandler(this.addgiftCardToolStripMenuItem_Click);
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showBinaryDataToolStripMenuItem,
            this.autoscrollDownToolStripMenuItem,
            this.clearLogToolStripMenuItem});
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(36, 20);
            this.logToolStripMenuItem.Text = "Log";
            // 
            // showBinaryDataToolStripMenuItem
            // 
            this.showBinaryDataToolStripMenuItem.CheckOnClick = true;
            this.showBinaryDataToolStripMenuItem.Name = "showBinaryDataToolStripMenuItem";
            this.showBinaryDataToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.showBinaryDataToolStripMenuItem.Text = "Show binary data";
            // 
            // autoscrollDownToolStripMenuItem
            // 
            this.autoscrollDownToolStripMenuItem.Checked = true;
            this.autoscrollDownToolStripMenuItem.CheckOnClick = true;
            this.autoscrollDownToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoscrollDownToolStripMenuItem.Name = "autoscrollDownToolStripMenuItem";
            this.autoscrollDownToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.autoscrollDownToolStripMenuItem.Text = "Auto &scroll down";
            // 
            // clearLogToolStripMenuItem
            // 
            this.clearLogToolStripMenuItem.Name = "clearLogToolStripMenuItem";
            this.clearLogToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.clearLogToolStripMenuItem.Text = "&Clear log";
            this.clearLogToolStripMenuItem.Click += new System.EventHandler(this.clearLogToolStripMenuItem_Click);
            // 
            // troublesToolStripMenuItem
            // 
            this.troublesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commsdownToolStripMenuItem,
            this.declineToolStripMenuItem,
            this.voiceReferralToolStripMenuItem,
            this.reversedInOfflineModeToolStripMenuItem,
            this.ociusOfflineToolStripMenuItem,
            this.nonidleScreenToolStripMenuItem,
            this.failedToRetrieveMessageToolStripMenuItem,
            this.unknownProgressMessageToolStripMenuItem});
            this.troublesToolStripMenuItem.Name = "troublesToolStripMenuItem";
            this.troublesToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.troublesToolStripMenuItem.Text = "&Troubles";
            // 
            // commsdownToolStripMenuItem
            // 
            this.commsdownToolStripMenuItem.Name = "commsdownToolStripMenuItem";
            this.commsdownToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.commsdownToolStripMenuItem.Tag = "CommsDown";
            this.commsdownToolStripMenuItem.Text = "&Comms down";
            this.commsdownToolStripMenuItem.Click += new System.EventHandler(this.troubleToolStripMenuItem_Click);
            // 
            // declineToolStripMenuItem
            // 
            this.declineToolStripMenuItem.Name = "declineToolStripMenuItem";
            this.declineToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.declineToolStripMenuItem.Tag = "Decline";
            this.declineToolStripMenuItem.Text = "&Decline";
            this.declineToolStripMenuItem.Click += new System.EventHandler(this.troubleToolStripMenuItem_Click);
            // 
            // voiceReferralToolStripMenuItem
            // 
            this.voiceReferralToolStripMenuItem.Name = "voiceReferralToolStripMenuItem";
            this.voiceReferralToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.voiceReferralToolStripMenuItem.Tag = "VoiceReferral";
            this.voiceReferralToolStripMenuItem.Text = "&Voice referral";
            this.voiceReferralToolStripMenuItem.Click += new System.EventHandler(this.troubleToolStripMenuItem_Click);
            // 
            // reversedInOfflineModeToolStripMenuItem
            // 
            this.reversedInOfflineModeToolStripMenuItem.Name = "reversedInOfflineModeToolStripMenuItem";
            this.reversedInOfflineModeToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.reversedInOfflineModeToolStripMenuItem.Tag = "ReversedInOfflineMode";
            this.reversedInOfflineModeToolStripMenuItem.Text = "Reversed in Offline Mode";
            this.reversedInOfflineModeToolStripMenuItem.Click += new System.EventHandler(this.troubleToolStripMenuItem_Click);
            // 
            // ociusOfflineToolStripMenuItem
            // 
            this.ociusOfflineToolStripMenuItem.Name = "ociusOfflineToolStripMenuItem";
            this.ociusOfflineToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.ociusOfflineToolStripMenuItem.Tag = "Offline";
            this.ociusOfflineToolStripMenuItem.Text = "Ocius Offline";
            this.ociusOfflineToolStripMenuItem.Click += new System.EventHandler(this.troubleToolStripMenuItem_Click);
            // 
            // nonidleScreenToolStripMenuItem
            // 
            this.nonidleScreenToolStripMenuItem.Name = "nonidleScreenToolStripMenuItem";
            this.nonidleScreenToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.nonidleScreenToolStripMenuItem.Tag = "NonIdle";
            this.nonidleScreenToolStripMenuItem.Text = "Non-idle screen";
            this.nonidleScreenToolStripMenuItem.Click += new System.EventHandler(this.troubleToolStripMenuItem_Click);
            // 
            // failedToRetrieveMessageToolStripMenuItem
            // 
            this.failedToRetrieveMessageToolStripMenuItem.Name = "failedToRetrieveMessageToolStripMenuItem";
            this.failedToRetrieveMessageToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.failedToRetrieveMessageToolStripMenuItem.Tag = "FailedToRetrieveMessage";
            this.failedToRetrieveMessageToolStripMenuItem.Text = "Failed to retrieve message";
            this.failedToRetrieveMessageToolStripMenuItem.Click += new System.EventHandler(this.troubleToolStripMenuItem_Click);
            // 
            // unknownProgressMessageToolStripMenuItem
            // 
            this.unknownProgressMessageToolStripMenuItem.Name = "unknownProgressMessageToolStripMenuItem";
            this.unknownProgressMessageToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.unknownProgressMessageToolStripMenuItem.Tag = "UnknownProgressMessage";
            this.unknownProgressMessageToolStripMenuItem.Text = "Unknown progress message";
            this.unknownProgressMessageToolStripMenuItem.Click += new System.EventHandler(this.troubleToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // timerClose
            // 
            this.timerClose.Interval = 10000;
            this.timerClose.Tick += new System.EventHandler(this.timerClose_Tick);
            // 
            // panelApp
            // 
            this.panelApp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelApp.Controls.Add(this.richTextBoxLog);
            this.panelApp.Controls.Add(this.panelAppDisplay);
            this.panelApp.Location = new System.Drawing.Point(12, 27);
            this.panelApp.Name = "panelApp";
            this.panelApp.Size = new System.Drawing.Size(468, 390);
            this.panelApp.TabIndex = 5;
            // 
            // panelAppDisplay
            // 
            this.panelAppDisplay.BackColor = System.Drawing.SystemColors.Window;
            this.panelAppDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAppDisplay.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAppDisplay.Location = new System.Drawing.Point(0, 0);
            this.panelAppDisplay.Name = "panelAppDisplay";
            this.panelAppDisplay.Size = new System.Drawing.Size(468, 153);
            this.panelAppDisplay.TabIndex = 1;
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStripTray;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Ocius Emulator";
            this.notifyIcon.Visible = true;
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // contextMenuStripTray
            // 
            this.contextMenuStripTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem_Tray});
            this.contextMenuStripTray.Name = "contextMenuStripTray";
            this.contextMenuStripTray.Size = new System.Drawing.Size(112, 54);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(108, 6);
            // 
            // exitToolStripMenuItem_Tray
            // 
            this.exitToolStripMenuItem_Tray.Name = "exitToolStripMenuItem_Tray";
            this.exitToolStripMenuItem_Tray.Size = new System.Drawing.Size(111, 22);
            this.exitToolStripMenuItem_Tray.Text = "Exit";
            this.exitToolStripMenuItem_Tray.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 423);
            this.Controls.Add(this.panelApp);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(500, 450);
            this.Name = "FormMain";
            this.Text = "Ocius";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.panelApp.ResumeLayout(false);
            this.contextMenuStripTray.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showBinaryDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoscrollDownToolStripMenuItem;
        private System.Windows.Forms.Timer timerClose;
        private System.Windows.Forms.ToolStripMenuItem clearLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem troublesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commsdownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem declineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voiceReferralToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addgiftCardToolStripMenuItem;
        private System.Windows.Forms.Panel panelApp;
        private System.Windows.Forms.Panel panelAppDisplay;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTray;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem_Tray;
        private System.Windows.Forms.ToolStripMenuItem reversedInOfflineModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ociusOfflineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nonidleScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem failedToRetrieveMessageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unknownProgressMessageToolStripMenuItem;
    }
}

