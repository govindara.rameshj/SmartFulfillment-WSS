﻿namespace OciusEmulatorFrontend
{
    partial class FormPED
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button buttonMinimize;
            this.buttonChipInsert = new System.Windows.Forms.Button();
            this.buttonSwipe = new System.Windows.Forms.Button();
            this.panelKeypad = new System.Windows.Forms.Panel();
            this.button15 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxDisplay = new System.Windows.Forms.TextBox();
            buttonMinimize = new System.Windows.Forms.Button();
            this.panelKeypad.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonMinimize
            // 
            buttonMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            buttonMinimize.BackColor = System.Drawing.Color.White;
            buttonMinimize.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            buttonMinimize.Location = new System.Drawing.Point(239, 3);
            buttonMinimize.Name = "buttonMinimize";
            buttonMinimize.Size = new System.Drawing.Size(24, 23);
            buttonMinimize.TabIndex = 0;
            buttonMinimize.Text = "-";
            buttonMinimize.UseVisualStyleBackColor = false;
            buttonMinimize.Click += new System.EventHandler(this.buttonMinimize_Click);
            // 
            // buttonChipInsert
            // 
            this.buttonChipInsert.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonChipInsert.BackColor = System.Drawing.SystemColors.Control;
            this.buttonChipInsert.Location = new System.Drawing.Point(54, 494);
            this.buttonChipInsert.Name = "buttonChipInsert";
            this.buttonChipInsert.Size = new System.Drawing.Size(141, 23);
            this.buttonChipInsert.TabIndex = 18;
            this.buttonChipInsert.Text = "INSERT CARD";
            this.buttonChipInsert.UseVisualStyleBackColor = false;
            this.buttonChipInsert.Click += new System.EventHandler(this.buttonChipInsert_Click);
            // 
            // buttonSwipe
            // 
            this.buttonSwipe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSwipe.BackColor = System.Drawing.SystemColors.Control;
            this.buttonSwipe.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSwipe.Location = new System.Drawing.Point(237, 242);
            this.buttonSwipe.Name = "buttonSwipe";
            this.buttonSwipe.Size = new System.Drawing.Size(20, 108);
            this.buttonSwipe.TabIndex = 19;
            this.buttonSwipe.Text = "SWIPE";
            this.buttonSwipe.UseVisualStyleBackColor = false;
            this.buttonSwipe.Click += new System.EventHandler(this.buttonSwipe_Click);
            // 
            // panelKeypad
            // 
            this.panelKeypad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelKeypad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelKeypad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelKeypad.Controls.Add(this.button15);
            this.panelKeypad.Controls.Add(this.button9);
            this.panelKeypad.Controls.Add(this.button14);
            this.panelKeypad.Controls.Add(this.button8);
            this.panelKeypad.Controls.Add(this.button13);
            this.panelKeypad.Controls.Add(this.button12);
            this.panelKeypad.Controls.Add(this.button7);
            this.panelKeypad.Controls.Add(this.button11);
            this.panelKeypad.Controls.Add(this.button6);
            this.panelKeypad.Controls.Add(this.button10);
            this.panelKeypad.Controls.Add(this.button5);
            this.panelKeypad.Controls.Add(this.button4);
            this.panelKeypad.Controls.Add(this.button3);
            this.panelKeypad.Controls.Add(this.button2);
            this.panelKeypad.Controls.Add(this.button1);
            this.panelKeypad.Location = new System.Drawing.Point(40, 289);
            this.panelKeypad.Name = "panelKeypad";
            this.panelKeypad.Size = new System.Drawing.Size(171, 199);
            this.panelKeypad.TabIndex = 17;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Green;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(115, 159);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(45, 33);
            this.button15.TabIndex = 14;
            this.button15.Tag = "ENTER";
            this.button15.Text = "ENT";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(115, 81);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(45, 33);
            this.button9.TabIndex = 8;
            this.button9.Tag = "NUM9";
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Yellow;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(64, 159);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(45, 33);
            this.button14.TabIndex = 13;
            this.button14.Tag = "BACK";
            this.button14.Text = "<-";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(64, 81);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(45, 33);
            this.button8.TabIndex = 7;
            this.button8.Tag = "NUM8";
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Red;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(13, 159);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(45, 33);
            this.button13.TabIndex = 12;
            this.button13.Tag = "CANCEL";
            this.button13.Text = "X";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(115, 120);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(45, 33);
            this.button12.TabIndex = 11;
            this.button12.Tag = "HASH";
            this.button12.Text = "#";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(13, 81);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(45, 33);
            this.button7.TabIndex = 6;
            this.button7.Tag = "NUM7";
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(64, 120);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(45, 33);
            this.button11.TabIndex = 10;
            this.button11.Tag = "NUM0";
            this.button11.Text = "0";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(115, 42);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(45, 33);
            this.button6.TabIndex = 5;
            this.button6.Tag = "NUM6";
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(13, 120);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(45, 33);
            this.button10.TabIndex = 9;
            this.button10.Tag = "ASTERISK";
            this.button10.Text = "*";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(64, 42);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(45, 33);
            this.button5.TabIndex = 4;
            this.button5.Tag = "NUM5";
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(13, 42);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(45, 33);
            this.button4.TabIndex = 3;
            this.button4.Tag = "NUM4";
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(115, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(45, 33);
            this.button3.TabIndex = 2;
            this.button3.Tag = "NUM3";
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(64, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(45, 33);
            this.button2.TabIndex = 1;
            this.button2.Tag = "NUM2";
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 33);
            this.button1.TabIndex = 0;
            this.button1.Tag = "NUM1";
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonPad_Click);
            // 
            // textBoxDisplay
            // 
            this.textBoxDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDisplay.BackColor = System.Drawing.Color.White;
            this.textBoxDisplay.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxDisplay.Location = new System.Drawing.Point(38, 46);
            this.textBoxDisplay.Multiline = true;
            this.textBoxDisplay.Name = "textBoxDisplay";
            this.textBoxDisplay.ReadOnly = true;
            this.textBoxDisplay.Size = new System.Drawing.Size(173, 210);
            this.textBoxDisplay.TabIndex = 20;
            // 
            // FormPED
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.BackgroundImage = global::OciusEmulatorFrontend.Properties.Resources.VX820;
            this.ClientSize = new System.Drawing.Size(266, 532);
            this.Controls.Add(this.textBoxDisplay);
            this.Controls.Add(this.buttonChipInsert);
            this.Controls.Add(this.buttonSwipe);
            this.Controls.Add(this.panelKeypad);
            this.Controls.Add(buttonMinimize);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormPED";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "PED";
            this.TransparencyKey = System.Drawing.Color.Blue;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPED_FormClosed);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormPED_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormPED_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormPED_MouseUp);
            this.panelKeypad.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonChipInsert;
        private System.Windows.Forms.Button buttonSwipe;
        private System.Windows.Forms.Panel panelKeypad;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxDisplay;


    }
}