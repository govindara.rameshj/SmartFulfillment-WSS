﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Configuration;
using OciusEmulator;
using OciusEmulator.Core;
using OciusEmulator.Core.Interaction;

namespace OciusEmulatorFrontend
{
    public partial class FormMain : Form
    {
        private Device device;
        private bool hide = true;

        public FormMain()
        {
            InitializeComponent();

            panelAppDisplay.Visible = false;

            var settings = ConfigurationManager.AppSettings;
            var args = Environment.GetCommandLineArgs();
            if (bool.Parse(settings["DummyMode"]) || args.Contains("--dummy"))
            {
                Text += " [Dummy mode]";
                log("Application running in dummy mode (no open ports).");
                if (!args.Contains("--dummy"))
                {
                    log("Will be automatically closed in " + timerClose.Interval + "ms");
                    timerClose.Enabled = true;
                }
                else
                {
                    close = true;
                    hide = false;
                }
            }
            else
            {
                int integrationPort = int.Parse(settings["IntegrationPort"]);
                int progressPort = int.Parse(settings["ProgressPort"]);
                bool mode820 = bool.Parse(settings["Mode820"]);

                device = new OciusEmulator.Core.Device(mode820);
                device.VisionCompatibility = bool.Parse(settings["VisionCompatibility"]);
                device.LoginTimespan = int.Parse(settings["LoginTimespan"]);

                device.OnConnect += new Device.OnConnectDelegate(l_OnConnect);
                device.OnDisconnect += new Device.OnDisconnectDelegate(l_OnDisconnect);
                device.OnDataArrival += new Device.OnDataArrivalDelegate(l_OnDataArrival);
                device.OnDataSent += new Device.OnDataSentDelegate(l_OnDataSent);
                device.OnError += new Device.OnErrorDelegate(l_OnError);
                device.OnShutdown += new Device.OnShutdownDelegate(device_OnShutdown);
                device.OnInteraction += Interaction;
                device.OnNotification += Notification;

                FormPED formPED = new FormPED(device);
                formPED.Show();

                device.Listen(integrationPort, progressPort);
                log(string.Format("Listening for inbound connections. Integration port: {0}, progress port: {1}", integrationPort, progressPort));

                if (bool.Parse(settings["AlwaysOnTop"]))
                    this.TopMost = true;
            }
        }

        private void log(string message)
        {
            if (InvokeRequired)
            {
                Action action = () => log(message);
                Invoke(action);
                return;
            }

            richTextBoxLog.AppendText("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff") + "] " + message + "\n");
            if (autoscrollDownToolStripMenuItem.Checked)
            {
                richTextBoxLog.Select(richTextBoxLog.TextLength, 0);
                richTextBoxLog.ScrollToCaret();
            }
        }

        private static string ByteArrayToHexString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            bool first = true;
            foreach (byte b in ba)
            {
                if (!first)
                    hex.Append(":");
                hex.AppendFormat("{0:X2}", b);
                first = false;
            }
            return hex.ToString();
        }

        private static string ByteArrayToString(byte[] ba)
        {
            UTF8Encoding enc = new UTF8Encoding();
            return enc.GetString(ba);
        }

        void l_OnConnect(EndPoint local, EndPoint remote)
        {
            log(string.Format("Client {0} connected to {1}", remote, local));
        }

        void l_OnDisconnect(EndPoint local, EndPoint remote)
        {
            log(string.Format("Client {0} disconnected from {1}", remote, local));
        }

        void l_OnDataArrival(EndPoint local, EndPoint remote, byte[] data)
        {
            if (showBinaryDataToolStripMenuItem.Checked)
                log(string.Format("Client {0} sent to {1} some data:\n{2}\n{3}", remote, local, ByteArrayToHexString(data), ByteArrayToString(data)));
            else
                log(string.Format("Client {0} sent to {1} some data:\n{2}", remote, local, ByteArrayToString(data)));
        }

        void l_OnDataSent(EndPoint local, EndPoint remote, byte[] data)
        {
            if (showBinaryDataToolStripMenuItem.Checked)
                log(string.Format("Port {1} sent to client {0} some data:\n{2}\n{3}", remote, local, ByteArrayToHexString(data), ByteArrayToString(data)));
            else
                log(string.Format("Port {1} sent to client {0} some data:\n{2}", remote, local, ByteArrayToString(data)));
        }

        void l_OnError(Exception exc)
        {
            log("Error occurred: " + exc.ToString());
        }

        void device_OnShutdown()
        {
            if (InvokeRequired)
            {
                Action action = () => device_OnShutdown();
                Invoke(action);
                return;
            }

            if (bool.Parse(ConfigurationManager.AppSettings["StayAliveAfterShutdown"]))
            {
                log("Now device wants to close application, but StayAliveAfterShutdown is set to true value, so this app will stay alive");
            }
            else
            {
                log("Closing application as Device Emulator wants");
                close = true;
                Close();
            }
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormCardDetails form = new FormCardDetails();
            if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                CardsRepository.AddCard(form.NewCard);
        }

        private void addgiftCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormGiftCardDetails form = new FormGiftCardDetails();
            if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                CardsRepository.AddCard(form.NewCard);
        }

        private void timerClose_Tick(object sender, EventArgs e)
        {
            close = true;
            Close();
        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxLog.Clear();
        }

        private void troubleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = (ToolStripMenuItem)sender;
            if (mi.Checked)
            {
                mi.Checked = false;
                device.ActiveTrouble = null;
            }
            else
            {
                commsdownToolStripMenuItem.Checked = false;
                declineToolStripMenuItem.Checked = false;
                voiceReferralToolStripMenuItem.Checked = false;
                reversedInOfflineModeToolStripMenuItem.Checked = false;
                ociusOfflineToolStripMenuItem.Checked = false;
                failedToRetrieveMessageToolStripMenuItem.Checked = false;
                unknownProgressMessageToolStripMenuItem.Checked = false;

                mi.Checked = true;
                device.ActiveTrouble = (TroubleEnum)Enum.Parse(typeof(TroubleEnum), (string)mi.Tag);
            }
        }

        private void Interaction(BaseInteraction interaction)
        {
            if (InvokeRequired)
            {
                Action action = () => Interaction(interaction);
                Invoke(action);
                return;
            }

            if (interaction == null)
            {
                panelAppDisplay.Visible = false;
            }
            else if (interaction is WinState)
            {
                Show();
                WindowState = FormWindowState.Normal;
                BringToFront();
            }
            else
            {
                panelAppDisplay.Controls.Clear();
                OciusEmulatorFrontend.Interaction.BaseInteractionControl ctl = null;
                if (interaction is ConfirmReceiptPrinting)
                {
                    ctl = new Interaction.ConfirmReceiptPrintingControl(((ConfirmReceiptPrinting)interaction).ReceiptType);
                }
                else if (interaction is ConfirmPartialPurchase)
                {
                    var inter = (ConfirmPartialPurchase)interaction;
                    ctl = new Interaction.BooleanControl(string.Format("Card balance is {0}. Do you want to allow partial purchase?", inter.RemainingBalance));
                }

                if (ctl != null)
                {
                    ctl.OnInteration += new OciusEmulatorFrontend.Interaction.BaseInteractionControl.OnInterationDelegate(ctl_OnInteration);
                    ctl.Dock = DockStyle.Fill;
                    panelAppDisplay.Controls.Add(ctl);

                    panelAppDisplay.Visible = true;
                }
            }
        }

        void ctl_OnInteration(BaseInteractionResponse response)
        {
            try
            {
                device.Interact(response);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Notification(string message)
        {
            log(message);
        }

        private bool close = false;
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            close = true;
            Close();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!close)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            Show();
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            if (hide)
            {
                Hide();
                hide = false;
            }
        }
    }
}
