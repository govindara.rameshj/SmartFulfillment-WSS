﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OciusEmulator.Core;

namespace OciusEmulator
{
    public static class CardsRepository
    {
        private static Dictionary<string, Card> cards = new Dictionary<string, Card>();
        private static Dictionary<string, GiftCard> giftCards = new Dictionary<string, GiftCard>();

        public static void AddCard(Card card)
        {
            if (cards.ContainsKey(card.Number))
                cards.Remove(card.Number);
            cards.Add(card.Number, card);
        }

        public static void AddCard(GiftCard card)
        {
            if (giftCards.ContainsKey(card.Number))
                giftCards.Remove(card.Number);
            giftCards.Add(card.Number, card);
        }

        public static void Clear()
        {
            cards.Clear();
        }

        public static Card GetCard(string cardNumber)
        {
            if (IsCardExisted(cardNumber))
                return cards[cardNumber];
            else
                return null;
        }

        public static GiftCard GetGiftCard(string cardNumber)
        {
            if (giftCards.ContainsKey(cardNumber))
                return giftCards[cardNumber];
            else
                return null;
        }

        public static bool IsCardExisted(string cardNumber)
        {
            return cards.ContainsKey(cardNumber);
        }
    }
}
