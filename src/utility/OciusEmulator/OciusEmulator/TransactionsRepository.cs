﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OciusEmulator.Core;

namespace OciusEmulator
{
    public static class TransactionsRepository
    {
        private static Dictionary<string, CreditOrDebitCardTransaction> transactions = new Dictionary<string, CreditOrDebitCardTransaction>();

        public static void Add(CreditOrDebitCardTransaction transaction)
        {
            if (string.IsNullOrEmpty(transaction.TokenId))
                throw new ArgumentException("TokenId is mandatory for storing transaction");
            transactions.Add(transaction.TokenId, transaction);
        }

        public static CreditOrDebitCardTransaction Get(string tokenId)
        {
            if (!string.IsNullOrEmpty(tokenId) && transactions.ContainsKey(tokenId))
                return transactions[tokenId];
            else
                return null;
        }

        public static IEnumerable<CreditOrDebitCardTransaction> GetAll()
        {
            return transactions.Values;
        }

        public static void AddRange(IEnumerable<CreditOrDebitCardTransaction> transactions)
        {
            foreach (var transaction in transactions)
                if (!TransactionsRepository.transactions.Keys.Contains(transaction.TokenId))
                    Add(transaction);
        }
    }
}
