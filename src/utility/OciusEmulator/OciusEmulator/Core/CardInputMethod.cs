﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    public enum CardInputMethod
    {
        /// <summary>
        /// Chip
        /// </summary>
        ICC,
        /// <summary>
        /// Magnetic stripe swiped
        /// </summary>
        Swiped,
        /// <summary>
        /// Entered by hand
        /// </summary>
        KeyedIn,
    }
}
