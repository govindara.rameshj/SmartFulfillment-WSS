﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    // TODO: http://en.wikipedia.org/wiki/Bank_card_number
    public class Card: ICloneable
    {
        private string number;
        public string Number
        {
            get { return number; }
        }

        private string pin;
        public string Pin
        {
            get { return pin; }
        }

        private string scheme;
        public string Scheme
        {
            get { return scheme; }
        }

        /// <summary>
        /// Some card issuers refer to the card number as the primary account number or PAN
        /// </summary>
        public string MaskedPAN
        {
            get
            {
                string res = "";
                for (int i = 0; i < number.Length; i++)
                {
                    if (i < number.Length - 4)
                        res += "*";
                    else
                        res += number[i];
                }
                return res;
            }
        }

        private DateTime expiryDate;
        public DateTime ExpiryDate
        {
            get { return expiryDate; }
        }

        private DateTime startDate;
        public DateTime StartDate
        {
            get { return startDate; }
        }

        private int merchantNumber;
        /// <summary>
        /// The Merchant Number for the given card scheme and account.
        /// </summary>
        public int MerchantNumber
        {
            get { return merchantNumber; }
        }

		public decimal Balance;

        public string Hash
        {
            get
            {
                var hash = System.Security.Cryptography.SHA256.Create();
                var output = hash.ComputeHash(Encoding.UTF8.GetBytes(this.Number + this.MerchantNumber));
                return Convert.ToBase64String(output);
            }
        }

        public Card(string number, string pin)
        {
            this.number = number;
            this.pin = pin;

            // determine scheme usign card number
            var iin = Convert.ToByte(number.Substring(0, 2));
            if (iin == 34 || iin == 37)
                this.scheme = "American Express";
            else if (iin >= 51 && iin <= 55)
                this.scheme = "MasterCard";
            else if (iin >= 50 && iin <= 69)
                this.scheme = "Maestro";
            else
                this.scheme = "Visa";

            // auto-generate other attributes
            this.expiryDate = DateTime.Now.AddYears(3);
            this.startDate = DateTime.Now.AddMonths(-1);
            this.merchantNumber = new Random().Next();
			Balance = 0;
        }

        public object Clone()
        {
            return new Card(this.number, this.pin);
        }
    }
}
