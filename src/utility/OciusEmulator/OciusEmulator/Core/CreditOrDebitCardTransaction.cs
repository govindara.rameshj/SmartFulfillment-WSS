﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    public class CreditOrDebitCardTransaction : BaseTransaction
    {
        public enum TransactionTypeEnum
        {
            Purchase,
            Refund,
            CashAdvance
        }

        public enum TransactionModifierEnum
        {
            CardholderPresent,
            /// <summary>
            /// CNP (Customer not present) case
            /// </summary>
            TelephoneOrder,
            /// <summary>
            /// CNP (Customer not present) case
            /// </summary>
            AccountOnFile,
        }

        public TransactionTypeEnum Type;
        public bool Authorized;
        public bool Declined;
        public bool VoiceReferral;
        public bool Completed;
        public bool Rejected;
        public bool InvalidTokenId;

        public int? TerminalId;
        public int? EFTSequenceNumber;
        public string AuthorisationCode;
        public string TokenId;

        public TransactionModifierEnum Modifier;

        public Card UsedCard;
        public CardInputMethod? CardInputMethod;

        public CreditOrDebitCardTransaction() :
            base()
        {
            this.Type = TransactionTypeEnum.Purchase;
            this.Authorized = false;
            this.Declined = false;
            this.VoiceReferral = false;
            this.Completed = false;
            this.Rejected = false;
            this.InvalidTokenId = false;

            this.TerminalId = null;
            this.EFTSequenceNumber = null;
            this.AuthorisationCode = null;
            this.TokenId = null;

            this.Modifier = TransactionModifierEnum.CardholderPresent;

            this.UsedCard = null;
            this.CardInputMethod = null;
        }
    }
}
