﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    internal class GiftCardTransaction : BaseTransaction
    {
        public enum TransactionTypeEnum
        {
            BalanceEnquiry = 1,
            NewCard_TopUp = 2,
            Sale = 3,
            Refund = 4,
            Cash = 5,
            Void = 6,
        }

        public TransactionTypeEnum Type;

        public GiftCard UsedCard;
        public CardInputMethod? CardInputMethod;
        public int? AuthCode;
        public string Reference;
        public decimal? TransactionId;

        public bool Success;
        public bool CardNotFound;
        public bool ZeroBalance;
        public bool PartialPurchase;
        public bool BadCard;

        public GiftCardTransaction() :
            base()
        {
            Type = TransactionTypeEnum.BalanceEnquiry;

            UsedCard = null;
            CardInputMethod = null;
            AuthCode = null;
            TransactionId = null;

            Success = false;
            CardNotFound = false;
            ZeroBalance = false;
            PartialPurchase = false;
            BadCard = false;
        }
    }
}
