﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Configuration;
using System.Threading;

namespace OciusEmulator.Core
{
    public class Device
    {
        public const int PINEntryTimeout = 30;
        /// <summary>
        /// Delay right after customer inserts a card
        /// </summary>
        public const int CardProcessingDelay = 2;

        public delegate void OnConnectDelegate(EndPoint local, EndPoint remote);
        public delegate void OnDisconnectDelegate(EndPoint local, EndPoint remote);
        public delegate void OnDataArrivalDelegate(EndPoint local, EndPoint remote, byte[] data);
        public delegate void OnDataSentDelegate(EndPoint local, EndPoint remote, byte[] data);
        public delegate void OnErrorDelegate(Exception exc);
        public delegate void OnDisplayRefreshDelegate(string content);
        public delegate void OnShutdownDelegate();
        public delegate void OnInteractionDelegate(Interaction.BaseInteraction interaction);
        public delegate void OnNotificationDelegate(string message);

        public event OnConnectDelegate OnConnect;
        public event OnDisconnectDelegate OnDisconnect;
        public event OnDataArrivalDelegate OnDataArrival;
        public event OnDataSentDelegate OnDataSent;
        public event OnDisplayRefreshDelegate OnDisplayRefresh;
        public event OnShutdownDelegate OnShutdown;
        public event OnErrorDelegate OnError;
        public event OnInteractionDelegate OnInteraction;
        public event OnNotificationDelegate OnNotification;

        private bool tcpActive = false;

        private TcpListener tcpListenerIntegration;
        private TcpListener tcpListenerProgress;
        private TcpClient tcpClientIntegration = null;
        private TcpClient tcpClientProgress = null;

        private State state;

        public bool Mode820 = false;
        public bool VisionCompatibility = false;
        public TroubleEnum? ActiveTrouble = null;
        public int LoginTimespan = 5000;
        private Timer loginTimer = null;

        private object criticalLock = new object();
        private Random rnd = new Random();

        private Interaction.BaseInteraction currentInteraction = null;
        private bool ProcessingMessage = false;

        public Device(bool mode820)
        {
            this.Mode820 = mode820;

            state = new State();
        }

        ~Device()
        {
            OnShutdown = null;
            Shutdown();
        }

        public StatusEnum CurrentStatus
        {
            get
            {
                return state.Status;
            }
        }

        public void Listen(int integrationPort, int progressPort)
        {
            tcpActive = true;

            IPAddress ip = IPAddress.Any;

            tcpListenerIntegration = new TcpListener(ip, integrationPort);
            tcpListenerIntegration.Start();
            tcpListenerIntegration.BeginAcceptSocket(OnConnectIntegration, null);

            tcpListenerProgress = new TcpListener(ip, progressPort);
            tcpListenerProgress.Start();
            tcpListenerProgress.BeginAcceptSocket(OnConnectProgress, null);

            DisplayRefresh();
        }

        private void CloseIntegrationClient()
        {
            if (tcpClientIntegration != null)
            {
                if (tcpClientIntegration.Connected)
                {
                    if (OnDisconnect != null)
                        OnDisconnect(tcpClientIntegration.Client.LocalEndPoint, tcpClientIntegration.Client.RemoteEndPoint);
                    tcpClientIntegration.Close();
                }
                tcpClientIntegration = null;
            }
        }

        private void CloseProgressClient()
        {
            if (tcpClientProgress != null)
            {
                if (tcpClientProgress.Connected)
                {
                    if (OnDisconnect != null)
                        OnDisconnect(tcpClientProgress.Client.LocalEndPoint, tcpClientProgress.Client.RemoteEndPoint);
                    tcpClientProgress.Close();
                }
                tcpClientProgress = null;
            }
        }

        public void Shutdown()
        {
            tcpActive = false;

            CloseIntegrationClient();
            CloseProgressClient();
            if (tcpListenerIntegration != null)
                tcpListenerIntegration.Stop();
            if (tcpListenerProgress != null)
                tcpListenerProgress.Stop();

            if (OnShutdown != null)
                OnShutdown();
        }

        private void OnConnectIntegration(IAsyncResult res)
        {
            try
            {
                tcpClientIntegration = tcpListenerIntegration.EndAcceptTcpClient(res);
                if (OnConnect != null)
                    OnConnect(tcpClientIntegration.Client.LocalEndPoint, tcpClientIntegration.Client.RemoteEndPoint);

                NetworkStream ns = tcpClientIntegration.GetStream();
                byte[] buffer = new byte[tcpClientIntegration.ReceiveBufferSize];
                List<byte> currentIncomeData = new List<byte>();

                while (true)
                {
                    int b;
                    try
                    {
                        b = ns.Read(buffer, 0, buffer.Length);
                    }
                    catch (ObjectDisposedException)
                    {
                        // most probably it's due to shutdown
                        break;
                    }
                    if (b == 0)
                        break;

                    currentIncomeData.AddRange(buffer.Take(b));
                    if (currentIncomeData.Last() != '\n')
                        continue;

                    if (OnDataArrival != null)
                        OnDataArrival(tcpClientIntegration.Client.LocalEndPoint, tcpClientIntegration.Client.RemoteEndPoint, currentIncomeData.ToArray());

                    ProcessIntegrationMessage(currentIncomeData.ToArray());
                    currentIncomeData.Clear();
                }
            }
            catch (Exception exc)
            {
                if (OnError != null)
                    OnError(exc);
            }

            if (tcpActive)
            {
                CloseIntegrationClient();
                tcpListenerIntegration.BeginAcceptTcpClient(OnConnectIntegration, null);
            }
        }

        private void OnConnectProgress(IAsyncResult res)
        {
            try
            {
                tcpClientProgress = tcpListenerProgress.EndAcceptTcpClient(res);
            }
            catch (ObjectDisposedException)
            {
                return;
            }

            if (OnConnect != null)
                OnConnect(tcpClientProgress.Client.LocalEndPoint, tcpClientProgress.Client.RemoteEndPoint);
            SendCurrentStatus();

            try
            {
                while (tcpClientProgress.GetStream().ReadByte() != -1)
                {
                }
            }
            catch (Exception exc)
            {
                if (OnError != null)
                    OnError(exc);
            }

            if (tcpActive)
            {
                CloseProgressClient();
                tcpListenerProgress.BeginAcceptTcpClient(OnConnectProgress, null);
            }
        }

        private void SendPacket(Packet.Packet packet, TcpClient client)
        {
            if (client == null)
            {
                if (OnError != null)
                    OnError(new ApplicationException("Warning! There's a try to send integration packet while client is not connected"));
                return;
            }

            byte[] data = packet.GetData();
            try
            {
                client.GetStream().Write(data, 0, data.Length);

                if (OnDataSent != null)
                    OnDataSent(client.Client.LocalEndPoint, client.Client.RemoteEndPoint, data);
            }
            catch (ObjectDisposedException)
            {
            }
        }

        private void SendCurrentStatus()
        {
            DisplayRefresh();
            if (tcpClientProgress == null)
                return;

            Packet.ProgressPacket packet = null;
            switch (state.Status)
            {
                case StatusEnum.LoginRequired:
                    packet = new Packet.ProgressPacket(100, 45);
                    break;
                case StatusEnum.Ready:
                    packet = new Packet.ProgressPacket(100, 46);
                    break;
                case StatusEnum.StartingTransaction:
                    packet = new Packet.ProgressPacket(100, 32);
                    break;
                case StatusEnum.AwaitingCard:
                case StatusEnum.KeyInCardDetails:
                case StatusEnum.SwipeCard:
                    {
                        int statusId;
                        switch (state.Status)
                        {
                            case StatusEnum.AwaitingCard:
                                statusId = 3;
                                break;
                            case StatusEnum.KeyInCardDetails:
                                statusId = 10;
                                break;
                            case StatusEnum.SwipeCard:
                                statusId = 4;
                                break;
                            default:
                                throw new Exception("Unknown status");
                        }
                        var parameters = new Dictionary<string, string>();
                        if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
                        {
                            parameters.Add("TXN VALUE", state.Transaction.TxnValue.ToString());
                            parameters.Add("CASHBACK VALUE", "0.00");
                            parameters.Add("GRATUITY VALUE", "0.00");
                            parameters.Add("TOTAL AMOUNT", state.Transaction.TxnValue.ToString());
                        }
                        packet = new Packet.ProgressPacket(100, statusId, parameters);
                    }
                    break;
                case StatusEnum.ExpiryDateRequired:
                    packet = new Packet.ProgressPacket(100, 36);
                    break;
                case StatusEnum.CSCRequired:
                    packet = new Packet.ProgressPacket(100, 41);
                    break;
                case StatusEnum.AVSHouseNumberRequired:
                    packet = new Packet.ProgressPacket(100, 39);
                    break;
                case StatusEnum.AVSPostCodeRequired:
                    packet = new Packet.ProgressPacket(100, 40);
                    break;
                case StatusEnum.CardInserted:
                    packet = new Packet.ProgressPacket(100, 5);
                    break;
                case StatusEnum.CardProcessing:
                    packet = new Packet.ProgressPacket(100, 7);
                    break;
                case StatusEnum.PinEntry:
                    packet = new Packet.ProgressPacket(100, 12);
                    break;
                case StatusEnum.PinEntered:
                    packet = new Packet.ProgressPacket(100, 191);
                    break;
                case StatusEnum.RiskManagementComplete:
                    packet = new Packet.ProgressPacket(100, 13);
                    break;
                case StatusEnum.AuthorisingTxn:
                    packet = new Packet.ProgressPacket(100, 14);
                    break;
                case StatusEnum.WaitingForResult:
                    packet = new Packet.ProgressPacket(100, 15);
                    break;
                case StatusEnum.AuthResultReceived:
                    packet = new Packet.ProgressPacket(100, 16);
                    break;
                case StatusEnum.RemoveCard:
                    packet = new Packet.ProgressPacket(100, 25);
                    break;
                case StatusEnum.CardRemoved:
                    packet = new Packet.ProgressPacket(100, 6);
                    break;
                case StatusEnum.ConfirmingTxn:
                    packet = new Packet.ProgressPacket(100, 21);
                    break;
                case StatusEnum.FinalResultReceived:
                    packet = new Packet.ProgressPacket(100, 23);
                    break;
                case StatusEnum.PrintingCustomerReceipt:
                    packet = new Packet.ProgressPacket(100, 53);
                    break;
                case StatusEnum.PrintingMerchantReceipt:
                    packet = new Packet.ProgressPacket(100, 17);
                    break;
                case StatusEnum.ContinueRequired:
                    packet = new Packet.ProgressPacket(100, 19);
                    break;
                case StatusEnum.CancellingTransaction:
                    packet = new Packet.ProgressPacket(100, 194);
                    break;
                case StatusEnum.TransactionCancelled:
                    packet = new Packet.ProgressPacket(100, 49);
                    break;
                case StatusEnum.ProcessingTxn:
                    packet = new Packet.ProgressPacket(100, 0);
                    break;
                case StatusEnum.InitialisingPED:
                    packet = new Packet.ProgressPacket(100, 54);
                    break;
                case StatusEnum.PinTryLimitExceeded:
                    packet = new Packet.ProgressPacket(100, 174);
                    break;
                case StatusEnum.SignatureConfirmationRequired:
                    packet = new Packet.ProgressPacket(100, 18);
                    break;
                case StatusEnum.CardSwiped:
                    packet = new Packet.ProgressPacket(100, 192);
                    break;
                case StatusEnum.MustInsertCard:
                    packet = new Packet.ProgressPacket(100, 190);
                    break;
                case StatusEnum.CardDataRetrieval:
                    packet = new Packet.ProgressPacket(100, 31);
                    break;
                case StatusEnum.RejectingTxn:
                    packet = new Packet.ProgressPacket(100, 22);
                    break;
                case StatusEnum.ConfirmAuthCode:
                    {
                        var parameters = new Dictionary<string, string>();
                        if (state.RefundTransaction != null)
                            parameters.Add("AUTH CODE", (state.RefundTransaction as CreditOrDebitCardTransaction).AuthorisationCode);

                        packet = new Packet.ProgressPacket(100, 20, parameters);
                    }
                    break;
                case StatusEnum.VoiceReferral:
                    {
                        CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

                        var parameters = new Dictionary<string, string>();
                        parameters.Add("VR TEL NO", "0845 760 0510");
                        parameters.Add("CARD", trans.UsedCard.MaskedPAN);
                        parameters.Add("EXPIRY", trans.UsedCard.ExpiryDate.ToString("MMyy"));
                        parameters.Add("MID", trans.UsedCard.MerchantNumber.ToString());
                        parameters.Add("TID", trans.TerminalId.ToString());

                        packet = new Packet.ProgressPacket(100, 24, parameters);
                    }
                    break;
                case StatusEnum.UnknownProgressMessage:
                    packet = new Packet.ProgressPacket(100, 211);
                    break;
                case StatusEnum.FailedToReadCard:
                    packet = new Packet.ProgressPacket(100, 212);
                    break;

            }
            if (packet != null)
                SendPacket(packet, tcpClientProgress);
        }

        private void DisplayRefresh()
        {
            if (OnDisplayRefresh != null)
            {
                string content = "";
                switch (state.Status)
                {
                    case StatusEnum.PinEntry:
                        {
                            if (state.PINFailedTriesCount > 0)
                                content += string.Format("Invalid PIN entered: {0} times\r\n", state.PINFailedTriesCount);
                            content += "PIN: ";
                            for (int i = 0; i < state.CurrentPIN.Length; i++)
                                content += "*";
                        }
                        break;
                    case StatusEnum.KeyInCardDetails:
                        {
                            content = "Enter card details:\r\n\r\nCard number:\r\n";
                            content += state.ManuallyEnteredData.Number;
                        }
                        break;
                    case StatusEnum.ExpiryDateRequired:
                        {
                            content = "Enter card details:\r\n\r\nEnter expiry date (MMYY):\r\n";
                            content += state.ManuallyEnteredData.ExpiryDate;
                        }
                        break;
                    case StatusEnum.CSCRequired:
                        {
                            content = "Enter card details:\r\n\r\nEnter CSC:\r\n";
                            content += state.ManuallyEnteredData.CSC;
                        }
                        break;
                    case StatusEnum.AVSHouseNumberRequired:
                        {
                            content = "Enter card details:\r\n\r\nEnter house number:\r\n";
                            content += state.ManuallyEnteredData.AVSHouseNumber;
                        }
                        break;
                    case StatusEnum.AVSPostCodeRequired:
                        {
                            content = "Enter card details:\r\n\r\nEnter post code:\r\n";
                            content += state.ManuallyEnteredData.AVSPostCode;
                        }
                        break;
                    case StatusEnum.LoginRequired:
                        {
                            if (ProcessingMessage)
                                content = "Please wait...";
                            else
                                content = state.Status.ToString();
                        }
                        break;
                    default:
                        content = state.Status.ToString();
                        break;
                }
                OnDisplayRefresh(content);
            }
        }

        private void ContinueIntegrationLoop()
        {
            lock (criticalLock)
            {
                if (state.LoopActions.Count > 0)
                {
                    var currentAction = state.LoopActions.First();
                    state.LastLoopAction = currentAction;

                    switch (currentAction)
                    {
                        case State.TransactionAction.PrintMerchantReceipt:
                            {
                                if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
                                {
                                    CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

                                    ReceiptPrinter.PrintReceipt(state, ReceiptType.Merchant);
                                    state.PrintReceiptTry++;
                                    if (!(!Mode820 && state.PrintReceiptTry >= 2 && trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Refund))
                                    {
                                        state.Status = StatusEnum.PrintingMerchantReceipt;
                                        SendCurrentStatus();
                                        Thread.Sleep(2000); // Something bad happen in Till after sending PrintingMerchantReceipt + SignatureConfirmationRequired sequence
                                    }

                                    if (!Mode820 && trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.CardholderPresent && trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase && state.LoopActions.Count > 1 && state.LoopActions[1] == State.TransactionAction.ConfirmSignature)
                                    {
                                        // move on to confirm signature
                                        state.LoopActions.RemoveAt(0);
                                        ContinueIntegrationLoop();
                                        return;
                                    }
                                }
                                else if (state.OperationType == State.OperationTypeEnum.GiftCard)
                                {
                                    RunGiftCardReceiptsPrintingBehaviour(ReceiptType.Merchant);
                                }
                            }
                            break;

                        case State.TransactionAction.PrintCustomerReceipt:
                            {
                                if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
                                {
                                    ReceiptPrinter.PrintReceipt(state, ReceiptType.Customer);
                                    state.Status = StatusEnum.PrintingCustomerReceipt;
                                    SendCurrentStatus();
                                }
                                else if (state.OperationType == State.OperationTypeEnum.GiftCard)
                                {
                                    RunGiftCardReceiptsPrintingBehaviour(ReceiptType.Customer);
                                }
                            }
                            break;

                        case State.TransactionAction.SendResponse:
                            state.LoopActions.RemoveAt(0);
                            {
                                Packet.Packet packet = null;
                                if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
                                {
                                    CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;
                                    var specificPacket = new Packet.TransactionResponse()
                                    {
                                        TerminateLoop = 0,
                                        TotalTransactionValueProcessed = trans.TxnValue,
                                        TransactionDateTime = trans.StartTime,

                                        TerminalId = trans.TerminalId,
                                        AuthorisationCode = trans.AuthorisationCode,
                                        EFTSequenceNumber = trans.EFTSequenceNumber.HasValue?trans.EFTSequenceNumber.Value.ToString("D4"):null,
                                        TokenId = trans.TokenId,
                                    };

                                    // add OL prefix if we're offline
                                    if (ActiveTrouble.HasValue && ActiveTrouble.Value == TroubleEnum.Offline && !string.IsNullOrEmpty(specificPacket.EFTSequenceNumber))
                                        specificPacket.EFTSequenceNumber = "OL" + specificPacket.EFTSequenceNumber;

                                    if (!string.IsNullOrEmpty(trans.TokenId))
                                        specificPacket.AccountOnFileRegistrationResult = Packet.TransactionResponse.AccountOnFileRegistrationResultEnum.Success;

                                    if (trans.UsedCard != null)
                                    {
                                        var card = trans.UsedCard;

                                        specificPacket.PAN = card.MaskedPAN;
                                        specificPacket.SchemeName = card.Scheme;
                                        specificPacket.MerchantNumber = card.MerchantNumber;
                                        specificPacket.ExpiryDate = card.ExpiryDate;
                                        specificPacket.Start = card.StartDate;
                                        specificPacket.CardInputMethod = trans.CardInputMethod.Value;
                                        specificPacket.CardNumberHash = card.Hash.Substring(0, 28);
                                    }

                                    if (state.ManuallyEnteredData != null)
                                    {
                                        specificPacket.AVSPostCodeResult = Packet.TransactionResponse.DataCheckResultEnum.Matched;
                                        specificPacket.AVSHouseNumberResult = Packet.TransactionResponse.DataCheckResultEnum.Matched;
                                        specificPacket.CSCResult = Packet.TransactionResponse.DataCheckResultEnum.Matched;
                                    }

                                    if (trans.InvalidTokenId)
                                    {
                                        specificPacket.Result = Packet.TransactionResponse.ResultEnum.InvalidTokenId;
                                        specificPacket.Message = "Invalid TokenID";
                                        specificPacket.AccountOnFileRegistrationResult = Packet.TransactionResponse.AccountOnFileRegistrationResultEnum.NotPerformed;
                                    }
                                    else if (trans.Rejected)
                                    {
                                        specificPacket.Result = Packet.TransactionResponse.ResultEnum.Reversed;
                                        specificPacket.Message = "REJECTED";
                                    }
                                    else if (trans.Cancelled)
                                    {
                                        specificPacket.Result = Packet.TransactionResponse.ResultEnum.TransactionBillCancelled;
                                        specificPacket.Message = "TRANSACTION CANCELLED";
                                        specificPacket.AccountOnFileRegistrationResult = Packet.TransactionResponse.AccountOnFileRegistrationResultEnum.NotPerformed;
                                    }
                                    else if (trans.Declined)
                                    {
                                        specificPacket.Result = Packet.TransactionResponse.ResultEnum.Reversed;
                                        specificPacket.Message = "DECLINED";
                                        specificPacket.AccountOnFileRegistrationResult = Packet.TransactionResponse.AccountOnFileRegistrationResultEnum.Failed;
                                    }
                                    else if (trans.Completed)
                                    {
                                        if (trans.CardInputMethod == CardInputMethod.ICC)
                                            specificPacket.Message = "PIN VERIFIED";
                                        else
                                            specificPacket.Message = "SIGNATURE VERIFIED";
                                        specificPacket.Result = Packet.TransactionResponse.ResultEnum.Completed;
                                    }
                                    else if (trans.Authorized)
                                    {
                                        specificPacket.Result = Packet.TransactionResponse.ResultEnum.Authorized;
                                        if (trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase)
                                        {
                                            specificPacket.Message = "Auth Code: " + specificPacket.AuthorisationCode;
                                        }
                                        else if (trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Refund)
                                        {
                                            specificPacket.Message = "CONFIRMED";
                                            // to be clarified!
                                            if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.AccountOnFile)
                                                specificPacket.AccountOnFileRegistrationResult = Packet.TransactionResponse.AccountOnFileRegistrationResultEnum.NotPerformed;
                                        }
                                        else
                                            throw new Exception("Result is undefined for transaction response");
                                    }
                                    else if (trans.VoiceReferral)
                                    {
                                        specificPacket.Result = Packet.TransactionResponse.ResultEnum.Referred;
                                        specificPacket.VRTelNo = "0845 760 0510";
                                        specificPacket.Message = "VOICE REFERRAL";
                                        specificPacket.AccountOnFileRegistrationResult = Packet.TransactionResponse.AccountOnFileRegistrationResultEnum.Success;
                                    }
                                    else
                                        throw new Exception("Result is undefined for transaction response");
                                    packet = specificPacket;
                                }
                                else if (state.OperationType.Value == State.OperationTypeEnum.GiftCard)
                                {
                                    GiftCardTransaction trans = (GiftCardTransaction)state.Transaction;
                                    var specificPacket = new Packet.BarclaycardGiftResponse();
                                    specificPacket.TransactionID = trans.TransactionId;
                                    specificPacket.TransactionDateTime = trans.StartTime;
                                    specificPacket.Amount = trans.TxnValue;
                                    if (trans.AuthCode.HasValue)
                                        specificPacket.TransactionAuthCode = trans.AuthCode.Value.ToString("000000000").Substring(0, 9);

                                    if (trans.Cancelled)
                                    {
                                        if (trans.PartialPurchase)
                                        {
                                            specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Reversed;
                                            specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.PartialPurchaseTransaction;
                                            specificPacket.Message = "Transaction Reversed";
                                        }
                                        else
                                        {
                                            specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Cancelled;
                                            specificPacket.Amount = 0;
                                            specificPacket.Message = "Transaction / Bill Cancelled";
                                        }
                                    }
                                    else if (trans.BadCard)
                                    {
                                        specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Rejected;
                                        specificPacket.RemainingCardBalance = 0;

                                        if (trans.UsedCard.LostOrStolen)
                                        {
                                            specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.CallCallCentre;
                                            if (Mode820)
                                                specificPacket.Message = "Call Helpdesk (MID:5461001)";
                                            else
                                                specificPacket.Message = "Card Has Expired (<ExpiryMessage>)";
                                        }
                                        else if (trans.UsedCard.Expired)
                                        {
                                            specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.CardExpired;
                                            specificPacket.Message = string.Format("Card Has Expired ({0:dd/MM/yyyy})", trans.UsedCard.ExpiryDate);
                                        }
                                        else
                                            throw new Exception("Result is undefined for transaction response");
                                    }
                                    else
                                    {
                                        specificPacket.MessageNumber = state.BarclaysGiftTransactionNumber;
                                        switch (trans.Type)
                                        {
                                            case GiftCardTransaction.TransactionTypeEnum.BalanceEnquiry:
                                                {
                                                    specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Success;
                                                    specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.Ok;
                                                    if (trans.Success)
                                                    {
                                                        specificPacket.RemainingCardBalance = trans.UsedCard.Balance;
                                                        specificPacket.Message = "Expiry:" + trans.UsedCard.ExpiryDate.ToString("dd/MM/yy");
                                                    }
                                                    else
                                                    {
                                                    }
                                                }
                                                break;
                                            case GiftCardTransaction.TransactionTypeEnum.NewCard_TopUp:
                                                {
                                                    if (trans.Success)
                                                    {
                                                        specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Success;
                                                        specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.Ok;
                                                        specificPacket.RemainingCardBalance = trans.UsedCard.Balance;
                                                    }
                                                    else
                                                        throw new Exception("Result is undefined for transaction response");
                                                }
                                                break;
                                            case GiftCardTransaction.TransactionTypeEnum.Sale:
                                                {
                                                    if (trans.CardNotFound)
                                                    {
                                                        specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Rejected;
                                                        specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.NoCard;
                                                        specificPacket.Message = "Card Not Valid";
                                                    }
                                                    else if (trans.ZeroBalance)
                                                    {
                                                        specificPacket.Amount = 0;
                                                        specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Reversed;
                                                        specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.PartialPurchaseTransaction;
                                                        specificPacket.Message = "Transaction Reversed";
                                                    }
                                                    else if (trans.Success)
                                                    {
                                                        specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Success;
                                                        specificPacket.RemainingCardBalance = trans.UsedCard.Balance;
                                                        if (trans.PartialPurchase)
                                                        {
                                                            specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.PartialPurchaseTransaction;
                                                        }
                                                        else
                                                        {
                                                            specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.Ok;
                                                        }
                                                    }
                                                    else
                                                        throw new Exception("Result is undefined for transaction response");
                                                }
                                                break;
                                            case GiftCardTransaction.TransactionTypeEnum.Refund:
                                                {
                                                    if (trans.CardNotFound)
                                                    {
                                                        specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Rejected;
                                                        specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.NoCard;
                                                        specificPacket.Message = "Card Not Valid";
                                                    }
                                                    else if (trans.Success)
                                                    {
                                                        specificPacket.ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.Success;
                                                        specificPacket.RemainingCardBalance = trans.UsedCard.Balance;
                                                        specificPacket.ResponseCode = Packet.BarclaycardGiftResponse.ResponseCodeEnum.Ok;
                                                    }
                                                    else
                                                        throw new Exception("Result is undefined for transaction response");
                                                }
                                                break;
                                            default:
                                                throw new Exception("Result is undefined for transaction response");
                                        }
                                    }

                                    packet = specificPacket;
                                }

                                if (state.CardInserted && !state.LoopActions.Contains(State.TransactionAction.RemoveCard) && state.CardProcessed)
                                    state.LoopActions.Insert(0, State.TransactionAction.RemoveCard);

                                if (state.LoopActions.Count == 0 && (packet is Packet.TransactionResponse))
                                    (packet as Packet.TransactionResponse).TerminateLoop = 1;

                                if (VisionCompatibility && (packet is Packet.TransactionResponse) && (packet as Packet.TransactionResponse).TerminateLoop == 0)
                                {
                                    ContinueIntegrationLoop();
                                    return;
                                }

                                packet.RebuildPayload();
                                SendPacket(packet, tcpClientIntegration);
                            }
                            break;
                        case State.TransactionAction.RemoveCard:
                            if (state.CardInserted)
                            {
                                state.WaitingForCardRemoval = true;
                                state.Status = StatusEnum.RemoveCard;
                                SendCurrentStatus();
                            }
                            else
                            {
                                state.LoopActions.RemoveAt(0);
                                ContinueIntegrationLoop();
                                return;
                            }
                            break;
                        case State.TransactionAction.ConfirmAuthCode:
                            state.Status = StatusEnum.ConfirmAuthCode;
                            SendCurrentStatus();
                            break;
                        case State.TransactionAction.ConfirmSignature:
                            state.Status = StatusEnum.SignatureConfirmationRequired;
                            SendCurrentStatus();
                            break;
                        case State.TransactionAction.AuthorizeVoiceReferral:
                            state.Status = StatusEnum.VoiceReferral;
                            SendCurrentStatus();
                            break;
                        case State.TransactionAction.ConfirmTransaction:
                            state.LoopActions.RemoveAt(0);
                            ConfirmTransaction();
                            ContinueIntegrationLoop();
                            return;
                    }
                }

                if (state.LoopActions.Count > 0)
                {
                    if (state.LastLoopAction.HasValue)
                    {
                        // actions that do need response from integrator's software
                        // remove that block and use return instead of break, above
                        if (state.LastLoopAction.Value == State.TransactionAction.RemoveCard
                            || state.LastLoopAction.Value == State.TransactionAction.ConfirmSignature
                            || state.LastLoopAction.Value == State.TransactionAction.ConfirmAuthCode
                            || state.LastLoopAction.Value == State.TransactionAction.AuthorizeVoiceReferral
                            )
                            return;

                        // actions that do not need continue required command
                        if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
                        {
                            if (state.LastLoopAction.Value == State.TransactionAction.SendResponse)
                            {
                                if (state.LoopActions[0] == State.TransactionAction.PrintMerchantReceipt
                                    || state.LoopActions[0] == State.TransactionAction.ConfirmAuthCode // redundant. to be removed at next refactoring
                                    || state.LoopActions[0] == State.TransactionAction.AuthorizeVoiceReferral
                                   )
                                {
                                    ContinueIntegrationLoop();
                                    return;
                                }
                            }
                        }
                    }

                    state.Status = StatusEnum.ContinueRequired;
                    SendCurrentStatus();
                }
                else
                {
                    FireInteractionEvent(null);

                    state.Reset();
                    SendCurrentStatus();
                    if (VisionCompatibility)
                    {
                        CloseIntegrationClient();
                        CloseProgressClient();
                    }
                }
            }
        }

        private void RunGiftCardReceiptsPrintingBehaviour(ReceiptType receiptType)
        {
            ReceiptPrinter.PrintReceipt(state, receiptType);

            if (!Mode820)
                FireInteractionEvent(new Interaction.WinState());
            FireInteractionEvent(new Interaction.ConfirmReceiptPrinting(receiptType));
        }

        private void ProcessIntegrationMessage(byte[] msg)
        {
            lock (criticalLock)
            {

                Packet.Packet packet = null;
                try
                {
                    packet = Packet.Packet.ParseRequest(msg);
                }
                catch (Packet.PacketParseException)
                {
                }
                catch (ArgumentException exc)
                {
                    if (OnError != null)
                        OnError(exc);
                }

                // send ACK to client first
                SendPacket(new Packet.Acknowledge(), tcpClientIntegration);

                if (packet == null)
                {
                    SendPacket(new Packet.Packet("-83,1,,,,,,,,,,,,,,,,UNKNOWN MESSAGE"), tcpClientIntegration);
                    return;
                }

                if (packet is Packet.RequestInfoRequest)
                {
                    // Request info
                    // ******************************************************************************************************************************
                    var specificPacket = (Packet.RequestInfoRequest)packet;

                    switch (specificPacket.ReqInfoType)
                    {
                        case Packet.RequestInfoRequest.ReqInfoTypeEnum.Version:
                            SendPacket(new Packet.RequestInfoResponse(this.Mode820 ? "3.6.4.43" : "3.5.0.4"/*3.4.05.05*/), tcpClientIntegration);
                            break;
                        case Packet.RequestInfoRequest.ReqInfoTypeEnum.PTID:
                            SendPacket(new Packet.RequestInfoResponse("CT202797"), tcpClientIntegration);
                            break;
                        default:
                            throw new Exception("Unknown ReqInfoType in request info record");
                    }
                }
                else if (packet is Packet.LoginRequest)
                {
                    // Login
                    // ******************************************************************************************************************************
                    if (state.Status == StatusEnum.LoginRequired)
                    {
                        if (ProcessingMessage)
                        {
                            // Already Processing Message From Another Client
                            SendPacket(new Packet.ServiceNotAllowed(), tcpClientIntegration);
                        }
                        else
                        {
                            ProcessingMessage = true;
                            DisplayRefresh();
                            if (OnNotification != null)
                                OnNotification("Starting Login timer for " + LoginTimespan + " ms");
                            loginTimer = new Timer((object state1) =>
                            {
                                if (OnNotification != null)
                                    OnNotification("Login timer expired. Logging in and sending response to a client");

                                state.LoggedIn = true;
                                state.Status = StatusEnum.Ready;
                                SendPacket(new Packet.LoginResponse(Packet.LoginResponse.Kind.Success), tcpClientIntegration);
                                ProcessingMessage = false;
                                SendCurrentStatus();

                                loginTimer = null;
                            }, new object(), LoginTimespan, Timeout.Infinite);
                        }
                    }
                    else
                    {
                        SendPacket(new Packet.LoginResponse(Packet.LoginResponse.Kind.Already), tcpClientIntegration);
                    }
                }
                else if (packet is Packet.LogoutRequest)
                {
                    // Logout
                    // ******************************************************************************************************************************
                    var specificPacket = (Packet.LogoutRequest)packet;

                    switch (specificPacket.Function)
                    {
                        case Packet.LogoutRequest.FunctionEnum.ExitAndLogout:
                            {
                                state.LoggedIn = false;
                                state.Status = StatusEnum.LoginRequired;
                                SendCurrentStatus();

                                SendPacket(new Packet.LogoutResponse(), tcpClientIntegration);
                                Shutdown();
                            }
                            break;
                        default:
                            throw new Exception("Unknown Function in LogoutRequest: " + specificPacket.Function);
                    }
                }
                else if (packet is Packet.TransactionRequest)
                {
                    // Start transaction
                    // ******************************************************************************************************************************
                    var specificPacket = (Packet.TransactionRequest)packet;
                    if (specificPacket.RegisterForAccountOnFile != Packet.TransactionRequest.RegisterForAccountOnFileEnum.Register)
                        throw new Exception("Unexpected RegisterForAccountOnFile value in Transaction Request record");

                    state.OperationType = State.OperationTypeEnum.CreditOrDebitCard;

                    switch (specificPacket.TransactionType)
                    {
                        case CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase:
                            {
                                state.Status = StatusEnum.StartingTransaction;
                                SendCurrentStatus();

                                CreditOrDebitCardTransaction trans = new CreditOrDebitCardTransaction();
                                state.Transaction = trans;

                                trans.Type = specificPacket.TransactionType;
                                trans.TxnValue = specificPacket.TxnValue;
                                trans.Modifier = specificPacket.Modifier;

                                if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.TelephoneOrder)
                                {
                                    state.ManuallyEnteredData = new ManualData();
                                    state.Status = StatusEnum.KeyInCardDetails;
                                    SendCurrentStatus();
                                }
                                else
                                {
                                    state.Status = StatusEnum.AwaitingCard;
                                    SendCurrentStatus();
                                }
                            }
                            break;
                        case CreditOrDebitCardTransaction.TransactionTypeEnum.Refund:
                            {
                                state.Status = StatusEnum.StartingTransaction;
                                SendCurrentStatus();

                                CreditOrDebitCardTransaction trans = new CreditOrDebitCardTransaction();
                                state.Transaction = trans;

                                trans.Type = specificPacket.TransactionType;
                                trans.TxnValue = specificPacket.TxnValue;
                                trans.TokenId = specificPacket.TokenId;
                                trans.Modifier = specificPacket.Modifier;

                                if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.CardholderPresent)
                                {
                                    if (state.CardInserted)
                                        startCardProcessing(null);
                                    else
                                    {
                                        state.Status = StatusEnum.AwaitingCard;
                                        SendCurrentStatus();
                                    }
                                }
                                else if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.TelephoneOrder)
                                {
                                    state.ManuallyEnteredData = new ManualData();
                                    state.Status = StatusEnum.KeyInCardDetails;
                                    SendCurrentStatus();
                                }
                                else if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.AccountOnFile)
                                {
                                    state.Status = StatusEnum.CardProcessing;
                                    SendCurrentStatus();

                                    state.Status = StatusEnum.AuthorisingTxn;
                                    SendCurrentStatus();
                                    // req-resp

                                    state.Status = StatusEnum.WaitingForResult;
                                    SendCurrentStatus();
                                    // req-resp

                                    CreditOrDebitCardTransaction refundTrans = TransactionsRepository.Get(trans.TokenId);
                                    // accept unly regular purchase transaction while making a refund
                                    if (refundTrans != null && refundTrans.Type != CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase)
                                        refundTrans = null;

                                    if (refundTrans == null)
                                    {
                                        trans.InvalidTokenId = true;
                                        state.LoopActions.Add(State.TransactionAction.SendResponse);
                                        state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                                        state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                                        state.LoopActions.Add(State.TransactionAction.SendResponse);
                                    }
                                    else
                                    {
                                        trans.TerminalId = 4380001;
                                        trans.EFTSequenceNumber = 1036;
                                        trans.AuthorisationCode = "789DE";
                                        trans.TokenId = rnd.Next().ToString();
                                        trans.UsedCard = refundTrans.UsedCard;
                                        trans.CardInputMethod = CardInputMethod.KeyedIn;

                                        trans.Authorized = true;
                                        state.Status = StatusEnum.AuthResultReceived;
                                        SendCurrentStatus();

                                        state.LoopActions.Add(State.TransactionAction.SendResponse);
                                        state.LoopActions.Add(State.TransactionAction.ConfirmAuthCode);
                                    }
                                    state.RefundTransaction = refundTrans;
                                    ContinueIntegrationLoop();
                                }
                            }
                            break;
                        default:
                            throw new Exception("Unknown transaction type in transaction request record");
                    }
                }
                else if (packet is Packet.ContinueTransactionRequest)
                {
                    // Continue transaction
                    // ******************************************************************************************************************************
                    var specificPacket = (Packet.ContinueTransactionRequest)packet;

                    switch (specificPacket.Action)
                    {
                        case Packet.ContinueTransactionRequest.ActionEnum.ContinueTransaction:
                            if (state.Status == StatusEnum.ContinueRequired)
                            {
                                if (state.LoopActions.Count > 0)
                                {
                                    var action = state.LoopActions.First();
                                    if (action == State.TransactionAction.PrintCustomerReceipt || action == State.TransactionAction.PrintMerchantReceipt)
                                    {
                                        // if BGIFT and 810 - do nothing!
                                        if (state.OperationType.Value == State.OperationTypeEnum.GiftCard && !Mode820)
                                        {
                                            if (OnNotification != null)
                                                OnNotification("WARNING: ContinueTransaction has been ignored in VX810 BGIFT emulation mode");
                                            return;
                                        }

                                        state.LoopActions.RemoveAt(0);
                                        state.PrintReceiptTry = 0;
                                    }
                                }
                                ContinueIntegrationLoop();
                            }
                            else
                            {
                                if (OnNotification != null)
                                    OnNotification("WARNING: ContinueTransaction has been ignored in non-ContinueTransaction state");
                            }
                            break;
                        case Packet.ContinueTransactionRequest.ActionEnum.ConfirmSignature:
                            if (state.Status == StatusEnum.SignatureConfirmationRequired)
                            {
                                if (state.LoopActions.First() == State.TransactionAction.ConfirmSignature)
                                    state.LoopActions.RemoveAt(0);
                                ConfirmTransaction();
                                state.LoopActions.Add(State.TransactionAction.SendResponse);
                                ContinueIntegrationLoop();
                            }
                            else
                                throw new Exception("Invalid status for ConfirmSignature action in ContinueTransaction request");
                            break;
                        case Packet.ContinueTransactionRequest.ActionEnum.RejectSignature:
                            if (state.Status == StatusEnum.SignatureConfirmationRequired)
                            {
                                if (state.LoopActions.First() == State.TransactionAction.ConfirmSignature)
                                    state.LoopActions.RemoveAt(0);

                                state.Status = StatusEnum.RejectingTxn;
                                SendCurrentStatus();

                                // req-resp here
                                state.Status = StatusEnum.FinalResultReceived;
                                SendCurrentStatus();

                                (state.Transaction as CreditOrDebitCardTransaction).Rejected = true;
                                state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                                state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                                state.LoopActions.Add(State.TransactionAction.SendResponse);
                                ContinueIntegrationLoop();
                            }
                            break;
                        case Packet.ContinueTransactionRequest.ActionEnum.ConfirmAuthCode:
                            if (state.Status == StatusEnum.ConfirmAuthCode)
                            {
                                CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

                                if (state.LoopActions.First() == State.TransactionAction.ConfirmAuthCode)
                                    state.LoopActions.RemoveAt(0);

                                if (trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Refund)
                                {
                                    if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.TelephoneOrder)
                                    {
                                        state.LoopActions.Add(State.TransactionAction.ConfirmTransaction);
                                    }
                                    else if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.AccountOnFile)
                                    {
                                        state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                                        state.LoopActions.Add(State.TransactionAction.ConfirmTransaction);
                                    }
                                }
                                else if (trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase)
                                {
                                    ConfirmTransaction();
                                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                                }

                                ContinueIntegrationLoop();
                            }
                            break;

                        case Packet.ContinueTransactionRequest.ActionEnum.RejectAuthCode:
                            CancelTransaction();
                            break;

                        case Packet.ContinueTransactionRequest.ActionEnum.VoiceReferralAuthorised:
                            if (state.Status == StatusEnum.VoiceReferral)
                            {
                                if (state.LoopActions.First() == State.TransactionAction.AuthorizeVoiceReferral)
                                    state.LoopActions.RemoveAt(0);

                                CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;
                                trans.AuthorisationCode = specificPacket.Parameters["AUTHCODE"];
                                if (trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase)
                                {
                                    ConfirmTransaction();
                                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                                }

                                ContinueIntegrationLoop();
                            }
                            break;

                        case Packet.ContinueTransactionRequest.ActionEnum.VoiceReferralRejected:
                            {
                                state.Status = StatusEnum.RejectingTxn;
                                SendCurrentStatus();

                                state.LoopActions.Clear();

                                // req-resp here
                                state.Status = StatusEnum.FinalResultReceived;
                                SendCurrentStatus();

                                (state.Transaction as CreditOrDebitCardTransaction).Rejected = true;
                                state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                                state.LoopActions.Add(State.TransactionAction.SendResponse);
                                ContinueIntegrationLoop();
                            }
                            break;

                        case Packet.ContinueTransactionRequest.ActionEnum.ReprintReceipt:
                            if (!Mode820 && state.LoopActions.Count > 0 && state.LoopActions[0] == State.TransactionAction.ConfirmSignature)
                            {
                                ReceiptPrinter.PrintReceipt(state, ReceiptType.Merchant);
                            }
                            else
                                ContinueIntegrationLoop();
                            break;

                        default:
                            throw new Exception("Unknown action in ContinueTransactionRequest: " + specificPacket.Action);
                    }
                }
                else if (packet is Packet.OfflineSubmissionRequest)
                {
                    // Offline
                    // ******************************************************************************************************************************
                    var resp = new Packet.OfflineSubmissionResponse();
                    SendPacket(resp, tcpClientIntegration);
                }
                else if (packet is Packet.BarclaycardGiftRequest)
                {
                    // Gift card
                    // ******************************************************************************************************************************
                    var specificPacket = (Packet.BarclaycardGiftRequest)packet;

                    if (state.Status == StatusEnum.Ready)
                    {
                        if (ActiveTrouble.HasValue && ActiveTrouble.Value == TroubleEnum.NonIdle)
                        {
                            if (OnNotification != null)
                                OnNotification("Barclays Gift Transaction Cannot Be Accepted Unless On Idle Screen");

                            var resp = new Packet.BarclaycardGiftResponse()
                            {
                                ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.ServiceNotAllowed,
                                Message = "SERVICE NOT ALLOWED",
                                TransactionDateTime = new DateTime(1, 1, 1)
                            };
                            resp.RebuildPayload();
                            SendPacket(resp, tcpClientIntegration);
                        }
                        else
                        {
                            switch (specificPacket.TransactionType)
                            {
                                case GiftCardTransaction.TransactionTypeEnum.BalanceEnquiry:
                                case GiftCardTransaction.TransactionTypeEnum.NewCard_TopUp:
                                case GiftCardTransaction.TransactionTypeEnum.Sale:
                                case GiftCardTransaction.TransactionTypeEnum.Refund:
                                    // validate first
                                    if (!specificPacket.AccountID.HasValue || !(specificPacket.AccountID.Value > 0))
                                    {
                                        var resp = new Packet.BarclaycardGiftResponse()
                                        {
                                            ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.InvalidBarclaysGiftConfiguration,
                                            Message = "Invalid Barclays Gift Configuration",
                                            TransactionDateTime = DateTime.Now,
                                        };
                                        resp.RebuildPayload();
                                        SendPacket(resp, tcpClientIntegration);
                                        break;
                                    }

                                    state.BarclaysGiftTransactionNumber++;

                                    state.OperationType = State.OperationTypeEnum.GiftCard;
                                    GiftCardTransaction trans = new GiftCardTransaction();
                                    state.Transaction = trans;
                                    //state.Status = State.StatusEnum.StartingTransaction;
                                    //SendCurrentStatus();
                                    trans.Type = specificPacket.TransactionType;
                                    trans.Reference = specificPacket.Reference;
                                    if (specificPacket.TransactionType != GiftCardTransaction.TransactionTypeEnum.BalanceEnquiry)
                                        trans.TxnValue = specificPacket.Amount.Value;

                                    state.Status = StatusEnum.SwipeCard;
                                    SendCurrentStatus();

                                    if (Mode820)
                                    {
                                        Thread.Sleep(500);
                                        state.Status = StatusEnum.CardDataRetrieval;
                                        SendCurrentStatus();
                                        Thread.Sleep(500);
                                        state.Status = StatusEnum.SwipeCard;
                                        SendCurrentStatus();
                                    }

                                    break;
                                default:
                                    throw new Exception("Unknown TransactionType in BarclaycardGiftRequest: " + specificPacket.TransactionType);
                            }
                        }
                    }
                    else
                    {
                        var resp = new Packet.BarclaycardGiftResponse()
                        {
                            ResultCode = Packet.BarclaycardGiftResponse.ResultCodeEnum.ServiceNotAllowed,
                            Message = "SERVICE NOT ALLOWED",
                            TransactionDateTime = new DateTime(1, 1, 1)
                        };
                        resp.RebuildPayload();
                        SendPacket(resp, tcpClientIntegration);
                    }
                }
                else if (packet is Packet.GetCardDetailsRequest)
                {
                    // Get card details
                    // ******************************************************************************************************************************
                    var specificPacket = (Packet.GetCardDetailsRequest)packet;

                    state.OperationType = State.OperationTypeEnum.GetCardDetails;

                    state.Status = StatusEnum.AwaitingCard;
                    SendCurrentStatus();
                }
                else if (packet is Packet.WindowStateRequest)
                {
                    FireInteractionEvent(new Interaction.WinState());

                    var resp = new Packet.WindowStateResponse();
                    SendPacket(resp, tcpClientIntegration);
                }
                else
                    throw new Exception("Cannot process integration packet. Do not know what to do with request type " + packet.GetType().FullName);
            }
        }

        private void FireInteractionEvent(Interaction.BaseInteraction interaction)
        {
            if (interaction == null)
            {
                currentInteraction = null;
                if (OnInteraction != null)
                    OnInteraction(interaction);
                return;
            }

            bool skip = interaction is Interaction.WinState;
            if (OnInteraction != null)
            {
                Timer timer = new Timer((object state) =>
                {
                    if (!skip)
                        currentInteraction = interaction;
                    OnInteraction(interaction);
                });
                timer.Change(1500, Timeout.Infinite);
            }
            else if (!skip)
                currentInteraction = interaction;
        }

        public void Interact(Interaction.BaseInteractionResponse response)
        {
            if (currentInteraction == null)
                throw new Exception("Can't do action when currentInteraction is null");

            if (currentInteraction is Interaction.ConfirmReceiptPrinting)
            {
                if (((Interaction.ConfirmReceiptPrintingResponse)response).ContinueTransaction)
                {
                    FireInteractionEvent(null);

                    if (state.LoopActions.Count > 0 && (state.LoopActions[0] == State.TransactionAction.PrintCustomerReceipt || state.LoopActions[0] == State.TransactionAction.PrintMerchantReceipt))
                        state.LoopActions.RemoveAt(0);
                    ContinueIntegrationLoop();
                }
                else
                    ContinueIntegrationLoop();
            }
            else if (currentInteraction is Interaction.ConfirmPartialPurchase)
            {
                var resp = (Interaction.BooleanResponse)response;
                if (resp.Result)
                {
                    FireInteractionEvent(null);

                    var trans = (GiftCardTransaction)state.Transaction;
                    decimal amount = trans.UsedCard.Balance;
                    trans.UsedCard.Balance = 0;

                    trans.Success = true;
                    //trans.PartialPurchase = true;
                    trans.TxnValue = amount;
                    trans.AuthCode = rnd.Next();
                    trans.TransactionId = rnd.Next();
                    state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                    state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                    ContinueIntegrationLoop();
                }
                else
                    CancelTransaction();
            }
            else
                throw new Exception("Unknown interaction type");
        }

        public void PressButton(string buttonCode)
        {
            switch (buttonCode)
            {
                case "CANCEL":
                    CancelTransaction();
                    break;
                case "BACK":
                    if (state.Status == StatusEnum.PinEntry)
                    {
                        if (state.CurrentPIN.Length > 0)
                        {
                            state.CurrentPIN = state.CurrentPIN.Substring(0, state.CurrentPIN.Length - 1);
                            DisplayRefresh();
                        }
                    }
                    else if (state.Status == StatusEnum.KeyInCardDetails)
                    {
                        if (state.ManuallyEnteredData.Number.Length > 0)
                        {
                            state.ManuallyEnteredData.Number = state.ManuallyEnteredData.Number.Substring(0, state.ManuallyEnteredData.Number.Length - 1);
                            DisplayRefresh();
                        }
                    }
                    else if (state.Status == StatusEnum.ExpiryDateRequired)
                    {
                        if (state.ManuallyEnteredData.ExpiryDate.Length > 0)
                        {
                            state.ManuallyEnteredData.ExpiryDate = state.ManuallyEnteredData.ExpiryDate.Substring(0, state.ManuallyEnteredData.ExpiryDate.Length - 1);
                            DisplayRefresh();
                        }
                    }
                    else if (state.Status == StatusEnum.CSCRequired)
                    {
                        if (state.ManuallyEnteredData.CSC.Length > 0)
                        {
                            state.ManuallyEnteredData.CSC = state.ManuallyEnteredData.CSC.Substring(0, state.ManuallyEnteredData.CSC.Length - 1);
                            DisplayRefresh();
                        }
                    }
                    else if (state.Status == StatusEnum.AVSHouseNumberRequired)
                    {
                        if (state.ManuallyEnteredData.AVSHouseNumber.Length > 0)
                        {
                            state.ManuallyEnteredData.AVSHouseNumber = state.ManuallyEnteredData.AVSHouseNumber.Substring(0, state.ManuallyEnteredData.AVSHouseNumber.Length - 1);
                            DisplayRefresh();
                        }
                    }
                    else if (state.Status == StatusEnum.AVSPostCodeRequired)
                    {
                        if (state.ManuallyEnteredData.AVSPostCode.Length > 0)
                        {
                            state.ManuallyEnteredData.AVSPostCode = state.ManuallyEnteredData.AVSPostCode.Substring(0, state.ManuallyEnteredData.AVSPostCode.Length - 1);
                            DisplayRefresh();
                        }
                    }
                    break;
                case "ENTER":
                    if (state.Status == StatusEnum.PinEntry)
                    {
                        if (state.OperationType == State.OperationTypeEnum.CreditOrDebitCard)
                        {
                            CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

                            if (!state.CardInserted || !state.CardProcessed)
                            {
                                if (!state.CardProcessed)
                                {
                                    // FOR VX810. Check for VX820 later
                                    trans.Cancelled = true;
                                    state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                                    //state.LoopActions.Add(State.TransactionAction.RemoveCard);
                                }
                                else
                                {
                                    trans.Declined = true;
                                    state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                                }
                                state.LoopActions.Add(State.TransactionAction.SendResponse);
                                ContinueIntegrationLoop();
                                break;
                            }

                            if (state.CurrentPIN == trans.UsedCard.Pin)
                            {
                                if (Mode820)
                                {
                                    state.Status = StatusEnum.PinEntered;
                                    SendCurrentStatus();
                                }

                                state.Status = StatusEnum.RiskManagementComplete;
                                SendCurrentStatus();

                                AuthorizeTransaction();
                            }
                            else
                            {
                                state.CurrentPIN = "";
                                state.PINFailedTriesCount++;
                                if (state.PINFailedTriesCount == 3)
                                {
                                    if (Mode820)
                                    {
                                        state.Status = StatusEnum.PinTryLimitExceeded;
                                        SendCurrentStatus();
                                    }

                                    state.Status = StatusEnum.RiskManagementComplete;
                                    SendCurrentStatus();
                                    state.Status = StatusEnum.AuthorisingTxn;
                                    SendCurrentStatus();
                                    // send request here

                                    state.Status = StatusEnum.WaitingForResult;
                                    SendCurrentStatus();
                                    // send request here

                                    trans.EFTSequenceNumber = 1036;
                                    trans.TerminalId = 4380001;
                                    trans.Declined = true;
                                    state.Status = StatusEnum.AuthResultReceived;
                                    SendCurrentStatus();
                                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                                    ContinueIntegrationLoop();
                                }
                                else
                                {
                                    state.Status = StatusEnum.PinEntry;
                                    SendCurrentStatus();
                                    RunPINTimeout();
                                }
                            }
                        }
                    }
                    else if (state.Status == StatusEnum.KeyInCardDetails)
                    {
                        if (state.OperationType.Value == State.OperationTypeEnum.GetCardDetails)
                        {
                            ProcessManuallyEnteredData();
                        }
                        else if (state.OperationType.Value == State.OperationTypeEnum.GiftCard && !Mode820)
                        {
                            ProcessManuallyEnteredData();
                        }
                        else
                        {
                            state.Status = StatusEnum.ExpiryDateRequired;
                            SendCurrentStatus();
                        }
                    }
                    else if (state.Status == StatusEnum.ExpiryDateRequired)
                    {
                        CreditOrDebitCardTransaction trans = state.Transaction as CreditOrDebitCardTransaction;
                        if (trans != null && trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase && trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.TelephoneOrder)
                        {
                            state.Status = StatusEnum.CSCRequired;
                            SendCurrentStatus();
                        }
                        else
                            ProcessManuallyEnteredData();
                    }
                    else if (state.Status == StatusEnum.CSCRequired)
                    {
                        state.Status = StatusEnum.AVSHouseNumberRequired;
                        SendCurrentStatus();
                    }
                    else if (state.Status == StatusEnum.AVSHouseNumberRequired)
                    {
                        state.Status = StatusEnum.AVSPostCodeRequired;
                        SendCurrentStatus();
                    }
                    else if (state.Status == StatusEnum.AVSPostCodeRequired)
                    {
                        ProcessManuallyEnteredData();
                    }
                    break;
            }

            if (buttonCode.StartsWith("NUM"))
            {
                int num = int.Parse(buttonCode.Substring(3));

                if (state.Status == StatusEnum.PinEntry)
                {
                    if (state.CurrentPIN.Length < 4)
                    {
                        state.CurrentPIN += buttonCode.Substring(3);
                        DisplayRefresh();
                    }
                }
                else if (state.Status == StatusEnum.AwaitingCard
                    || state.Status == StatusEnum.SwipeCard)
                {
                    state.ManuallyEnteredData = new ManualData();
                    state.ManuallyEnteredData.Number = num.ToString();
                    state.Status = StatusEnum.KeyInCardDetails;
                    SendCurrentStatus();
                }
                else if (state.Status == StatusEnum.KeyInCardDetails)
                {
                    state.ManuallyEnteredData.Number += num.ToString();
                    DisplayRefresh();
                }
                else if (state.Status == StatusEnum.ExpiryDateRequired)
                {
                    state.ManuallyEnteredData.ExpiryDate += num.ToString();
                    DisplayRefresh();
                }
                else if (state.Status == StatusEnum.CSCRequired)
                {
                    state.ManuallyEnteredData.CSC += num.ToString();
                    DisplayRefresh();
                }
                else if (state.Status == StatusEnum.AVSHouseNumberRequired)
                {
                    state.ManuallyEnteredData.AVSHouseNumber += num.ToString();
                    DisplayRefresh();
                }
                else if (state.Status == StatusEnum.AVSPostCodeRequired)
                {
                    state.ManuallyEnteredData.AVSPostCode += num.ToString();
                    DisplayRefresh();
                }
            }
        }

        private void RetrieveCardDetails(string cardNumber)
        {
            var resp = new Packet.GetCardDetailsResponse();
            resp.CardData = cardNumber;

            resp.RebuildPayload();
            SendPacket(resp, tcpClientIntegration);

            state.Reset();
            SendCurrentStatus();
        }

        private void ProcessManuallyEnteredData()
        {
            if (state.OperationType == State.OperationTypeEnum.GetCardDetails)
            {
                RetrieveCardDetails(state.ManuallyEnteredData.Number);
            }
            else if (state.OperationType == State.OperationTypeEnum.GiftCard)
            {
                ProcessGiftCard(state.ManuallyEnteredData.Number, CardInputMethod.KeyedIn);
            }
            else
            {
                if (!CardsRepository.IsCardExisted(state.ManuallyEnteredData.Number))
                {
                    CancelTransaction();
                }
                else
                {
                    CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

                    // or maybe skip this status when card number is in wrong format?
                    state.Status = StatusEnum.CardProcessing;
                    SendCurrentStatus();
                    trans.UsedCard = CardsRepository.GetCard(state.ManuallyEnteredData.Number);
                    trans.CardInputMethod = CardInputMethod.KeyedIn;

                    AuthorizeTransaction();
                }
            }
        }

        private void pinEntryTimeout(object s)
        {
            CancelTransaction();
        }

        private void startCardProcessing(object s)
        {
            if (!state.OperationType.HasValue || state.OperationType.Value != State.OperationTypeEnum.CreditOrDebitCard)
                return;

            // TODO: maybe copy InsertedCard to Transaction.UserCard here?
            state.Status = StatusEnum.CardProcessing;
            SendCurrentStatus();
            // req-resp
            state.CardProcessed = true;

            var trans = (CreditOrDebitCardTransaction)state.Transaction;

            if (trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase)
            {
                state.Status = StatusEnum.PinEntry;
                state.CurrentPIN = "";
                SendCurrentStatus();

                RunPINTimeout();
            }
            else if (trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Refund)
            {
                state.Status = StatusEnum.AuthorisingTxn;
                SendCurrentStatus();
                // req-resp

                state.Status = StatusEnum.WaitingForResult;
                SendCurrentStatus();
                // req-resp

                trans.CardInputMethod = CardInputMethod.Swiped; // Double check it
                if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.CardholderPresent)
                {
                    trans.TerminalId = 4380001;
                    trans.EFTSequenceNumber = 1036;
                    trans.AuthorisationCode = "789DE";
                    trans.TokenId = rnd.Next().ToString();
                    trans.Authorized = true;
                    state.Status = StatusEnum.AuthResultReceived;
                    SendCurrentStatus();

                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                    state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                    state.LoopActions.Add(State.TransactionAction.RemoveCard);
                    state.LoopActions.Add(State.TransactionAction.ConfirmSignature);
                }
                else if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.AccountOnFile)
                {
                    throw new NotImplementedException();
                }
                ContinueIntegrationLoop();
            }
        }

        private void RunPINTimeout()
        {
            state.PinEntryTimer = new System.Threading.Timer(new System.Threading.TimerCallback(pinEntryTimeout), null, PINEntryTimeout * 1000, System.Threading.Timeout.Infinite);
        }

        public bool InsertCard(string cardNumber)
        {
            lock (criticalLock)
            {
                if (!state.OperationType.HasValue)
                    return false;

                if (ActiveTrouble.HasValue && (
                    ActiveTrouble.Value == TroubleEnum.FailedToRetrieveMessage ||
                    ActiveTrouble.Value == TroubleEnum.UnknownProgressMessage))
                {
                    if (ActiveTrouble.Value == TroubleEnum.FailedToRetrieveMessage)
                    {
                        state.Status = StatusEnum.FailedToReadCard;
                    }
                    else if (ActiveTrouble.Value == TroubleEnum.UnknownProgressMessage)
                    {
                        state.Status = StatusEnum.UnknownProgressMessage;
                    }
                    SendCurrentStatus();
                    Thread.Sleep(5000);

                    state.Status = StatusEnum.AwaitingCard;
                    SendCurrentStatus();

                    return false;
                }

                if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
                {
                    // TODO: decline transaction or something like that
                    if (!CardsRepository.IsCardExisted(cardNumber))
                        throw new Exception("Card does not exist in repository");

                    CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

                    state.CardInserted = true;
                    state.InsertedCard = CardsRepository.GetCard(cardNumber);
                    trans.UsedCard = state.InsertedCard;
                    trans.CardInputMethod = CardInputMethod.ICC;
                    state.CardProcessed = false;
                    if (state.Status == StatusEnum.AwaitingCard)
                    {
                        state.Status = StatusEnum.CardInserted;
                        SendCurrentStatus();
                        state.StartCardProcessingTimer = new System.Threading.Timer(new System.Threading.TimerCallback(startCardProcessing), null, CardProcessingDelay * 1000, System.Threading.Timeout.Infinite);
                    }
                }
            }

            return true;
        }

        public void RemoveCard()
        {
            lock (criticalLock)
            {
                state.CardInserted = false;
                if (state.Status == StatusEnum.CardInserted)
                {
                    state.CardBadInsertions++;
                    if (state.CardBadInsertions == 3)
                    {
                        state.Status = StatusEnum.SwipeCard;
                        SendCurrentStatus();
                    }
                    else
                    {
                        state.Status = StatusEnum.AwaitingCard;
                        SendCurrentStatus();
                    }
                }
                else if (state.WaitingForCardRemoval)
                {
                    CreditOrDebitCardTransaction trans = null;
                    if (state.Transaction != null)
                        trans = (CreditOrDebitCardTransaction)state.Transaction;

                    state.Status = StatusEnum.CardRemoved;
                    SendCurrentStatus();

                    if (state.LoopActions.Count > 0 && state.LoopActions.First() == State.TransactionAction.RemoveCard)
                    {
                        state.LoopActions.RemoveAt(0);
                    }

                    if (trans != null && trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase && trans.Authorized)
                    {
                        ConfirmTransaction();
                    }
                    else if (trans != null && trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase && trans.Cancelled && state.PINFailedTriesCount >= 3)
                    {
                        state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                    }

                    // revise later
                    if (trans != null && trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase)
                    {
                        if (!state.LoopActions.Contains(State.TransactionAction.SendResponse))
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                    }

                    if (state.Transaction != null)
                        ContinueIntegrationLoop();
                }
            }
        }

        public void SwipeCard(string cardNumber)
        {
            lock (criticalLock)
            {
                if (!state.OperationType.HasValue)
                    return;

                if (state.OperationType.Value == State.OperationTypeEnum.GetCardDetails)
                {
                    RetrieveCardDetails(cardNumber);
                }
                else
                {
                    if (state.Status == StatusEnum.AwaitingCard)
                    {
                        if (Mode820)
                        {
                            state.Status = StatusEnum.CardSwiped;
                            SendCurrentStatus();

                            state.Status = StatusEnum.MustInsertCard;
                            SendCurrentStatus();
                        }
                        state.Status = StatusEnum.AwaitingCard;
                        SendCurrentStatus();
                    }
                    else if (state.Status == StatusEnum.SwipeCard)
                    {
                        if (state.OperationType == State.OperationTypeEnum.GiftCard &&
                            ActiveTrouble.HasValue && (
                            ActiveTrouble.Value == TroubleEnum.FailedToRetrieveMessage ||
                            ActiveTrouble.Value == TroubleEnum.UnknownProgressMessage))
                        {
                            state.Status = StatusEnum.CardDataRetrieval;
                            SendCurrentStatus();
                            SendCurrentStatus();
                            SendCurrentStatus();
                            SendCurrentStatus();

                            if (ActiveTrouble.Value == TroubleEnum.FailedToRetrieveMessage)
                            {
                                state.Status = StatusEnum.FailedToReadCard;
                            }
                            else if (ActiveTrouble.Value == TroubleEnum.UnknownProgressMessage)
                            {
                                state.Status = StatusEnum.UnknownProgressMessage;
                            }
                            SendCurrentStatus();
                            Thread.Sleep(5000);

                            state.Status = StatusEnum.SwipeCard;
                            SendCurrentStatus();

                            return;
                        }

                        if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
                        {
                            state.Status = StatusEnum.CardDataRetrieval;
                            SendCurrentStatus();
                        }

                        if (Mode820)
                        {
                            state.Status = StatusEnum.CardSwiped;
                            SendCurrentStatus();
                        }

                        // VX810 for GC
                        if (state.OperationType.Value != State.OperationTypeEnum.GiftCard)
                        {
                            state.Status = StatusEnum.CardProcessing;
                            SendCurrentStatus();
                        }

                        if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
                        {
                            CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

                            // TODO: decline transaction or something like that
                            if (!CardsRepository.IsCardExisted(cardNumber))
                                throw new Exception("Card does not exist in repository");

                            trans.UsedCard = CardsRepository.GetCard(cardNumber);
                            trans.CardInputMethod = CardInputMethod.Swiped;

                            AuthorizeTransaction();
                        }
                        else if (state.OperationType == State.OperationTypeEnum.GiftCard)
                        {
                            ProcessGiftCard(cardNumber, CardInputMethod.Swiped);
                        }
                    }
                }
            }
       }

        private void ProcessGiftCard(string cardNumber, CardInputMethod cardInputMethod)
        {
            var trans = (GiftCardTransaction)state.Transaction;

            switch (trans.Type)
            {
                case GiftCardTransaction.TransactionTypeEnum.BalanceEnquiry:
                    {
                        var card = CardsRepository.GetGiftCard(cardNumber);
                        if (card == null)
                        {
                            trans.AuthCode = 0;
                            trans.TransactionId = rnd.Next();
                            state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                            ContinueIntegrationLoop();
                        }
                        else
                        {
                            trans.UsedCard = card;
                            trans.CardInputMethod = cardInputMethod;

                            trans.Success = true;
                            trans.AuthCode = rnd.Next();
                            trans.TransactionId = rnd.Next();
                            state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                            ContinueIntegrationLoop();
                        }
                    }
                    break;
                case GiftCardTransaction.TransactionTypeEnum.NewCard_TopUp:
                    {
                        var card = CardsRepository.GetGiftCard(cardNumber);
                        if (card == null)
                        {
                            card = new GiftCard(cardNumber, 0);
                            CardsRepository.AddCard(card);
                        }

                        trans.UsedCard = card;
                        trans.CardInputMethod = cardInputMethod;

                        if (card.LostOrStolen || card.Expired)
                        {
                            trans.BadCard = true;
                            trans.AuthCode = 0;
                            trans.TransactionId = rnd.Next();
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                            ContinueIntegrationLoop();
                        }
                        else
                        {
                            card.Balance += trans.TxnValue;

                            trans.Success = true;
                            trans.AuthCode = rnd.Next();
                            trans.TransactionId = rnd.Next();
                            state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                            state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                            ContinueIntegrationLoop();
                        }
                    }
                    break;
                case GiftCardTransaction.TransactionTypeEnum.Sale:
                    {
                        var card = CardsRepository.GetGiftCard(cardNumber);
                        if (card == null)
                        {
                            trans.CardNotFound = true;
                            trans.AuthCode = rnd.Next();
                            trans.TransactionId = rnd.Next();
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                            ContinueIntegrationLoop();
                        }
                        else if (card.Balance == 0)
                        {
                            trans.ZeroBalance = true;
                            trans.AuthCode = rnd.Next();
                            trans.TransactionId = rnd.Next();
                            state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                            state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                            ContinueIntegrationLoop();
                        }
                        else
                        {
                            trans.UsedCard = card;
                            trans.CardInputMethod = cardInputMethod;

                            if (card.LostOrStolen || card.Expired)
                            {
                                trans.BadCard = true;
                                trans.AuthCode = 0;
                                trans.TransactionId = rnd.Next();
                                state.LoopActions.Add(State.TransactionAction.SendResponse);
                                ContinueIntegrationLoop();
                            }
                            else if (trans.UsedCard.Balance >= trans.TxnValue)
                            {
                                trans.Success = true;
                                trans.UsedCard.Balance -= trans.TxnValue;
                                trans.AuthCode = rnd.Next();
                                trans.TransactionId = rnd.Next();
                                state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                                state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                                state.LoopActions.Add(State.TransactionAction.SendResponse);
                                ContinueIntegrationLoop();
                            }
                            else
                            {
                                // Partial purchase!
                                trans.PartialPurchase = true;

                                FireInteractionEvent(new Interaction.ConfirmPartialPurchase(card.Balance));
                            }
                        }
                    }
                    break;
                case GiftCardTransaction.TransactionTypeEnum.Refund:
                    {
                        var card = CardsRepository.GetGiftCard(cardNumber);
                        if (card == null)
                        {
                            trans.CardNotFound = true;
                            trans.AuthCode = rnd.Next();
                            trans.TransactionId = rnd.Next();
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                            ContinueIntegrationLoop();
                        }
                        else
                        {
                            trans.UsedCard = card;
                            trans.CardInputMethod = cardInputMethod;

                            if (card.LostOrStolen || card.Expired)
                            {
                                trans.BadCard = true;
                                trans.AuthCode = 0;
                                trans.TransactionId = rnd.Next();
                            }
                            else
                            {
                                trans.UsedCard.Balance += trans.TxnValue;
                                trans.Success = true;
                                trans.AuthCode = rnd.Next();
                                trans.TransactionId = rnd.Next();
                                state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                                state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                            }
                            state.LoopActions.Add(State.TransactionAction.SendResponse);
                            ContinueIntegrationLoop();
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private void AuthorizeTransaction()
        {
            CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

            if (ActiveTrouble.HasValue)
            {
                if (ActiveTrouble.Value == TroubleEnum.ReversedInOfflineMode)
                {
                    trans.Declined = true;

                    state.LoopActions.Add(State.TransactionAction.RemoveCard);
                    state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                    state.LoopActions.Add(State.TransactionAction.SendResponse);
					
					ContinueIntegrationLoop();
					return;
                }
            }

            state.Status = StatusEnum.AuthorisingTxn;
            SendCurrentStatus();
            #region req-resp
            // send request to server, like:
            /*
            <?xml version="1.0"?>
            <Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
              <ClientHeader>
                <PTID>CT202730</PTID>
                <BufferSize>1050</BufferSize>
                <mkTerminalID>1000004484</mkTerminalID>
                <mkSystemID>10002542</mkSystemID>
                <mkTerminalUserID>100000271</mkTerminalUserID>
                <mkTerminalUserGroupID>1000001510</mkTerminalUserGroupID>
                <EncryptionKey />
                <ProcessingDB />
              </ClientHeader>
              <MsgType>TOKENREGISTRATIONREQUEST</MsgType>
              <MsgData>&lt;![CDATA[&lt;?xml version="1.0"?&gt;
            &lt;tokenregistrationrequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="TOKEN"&gt;
              &lt;pan&gt;## Sensitive Data Removed ##&lt;/pan&gt;
              &lt;expirydate&gt;0803&lt;/expirydate&gt;
              &lt;capturemethod&gt;6&lt;/capturemethod&gt;
              &lt;tokenexpirationdate&gt;08062014&lt;/tokenexpirationdate&gt;
              &lt;starredpan&gt;************0002&lt;/starredpan&gt;
              &lt;mkcardschemeid&gt;2&lt;/mkcardschemeid&gt;
              &lt;cardschemename&gt;Visa&lt;/cardschemename&gt;
              &lt;authcodelength&gt;5&lt;/authcodelength&gt;
              &lt;tokentransactiondetails&gt;
                &lt;transactiondatetime&gt;2014-02-28 14:28:06&lt;/transactiondatetime&gt;
                &lt;transactiontype&gt;01&lt;/transactiontype&gt;
                &lt;transactionvalue&gt;34.99&lt;/transactionvalue&gt;
                &lt;currencyexponent&gt;2&lt;/currencyexponent&gt;
                &lt;mid&gt;140011409&lt;/mid&gt;
                &lt;processingindicator&gt;1&lt;/processingindicator&gt;
                &lt;mkaccountid&gt;140011409&lt;/mkaccountid&gt;
                &lt;mksystemid&gt;10002542&lt;/mksystemid&gt;
              &lt;/tokentransactiondetails&gt;
              &lt;masterkeyid&gt;SEb14L&lt;/masterkeyid&gt;
              &lt;termserial&gt;koGma+udOSg=&lt;/termserial&gt;
              &lt;eftvasalt&gt;UDqagHQh8nHt2UMiD5pRY5kUvIIJ371n9mt0F0APeSE=&lt;/eftvasalt&gt;
              &lt;ptid&gt;29259621&lt;/ptid&gt;
            &lt;/tokenregistrationrequest&gt;]]&gt;</MsgData>
            </Message>

            RESPONSE:
            <?xml version="1.0"?>
            <Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
              <MsgType>TOKENREGISTRATIONINSERTRESULT</MsgType>
              <MsgData>&lt;![CDATA[&lt;tokenregistrationinsertresult xmlns="TOKEN"&gt;&lt;id&gt;1061692600&lt;/id&gt;&lt;authdb&gt;AuthDB\CST-DB-AUTH-3&lt;/authdb&gt;&lt;/tokenregistrationinsertresult&gt;]]&gt;</MsgData>
            </Message>
            */
            #endregion

            // GET TXN status. In cycle
            state.Status = StatusEnum.WaitingForResult;
            SendCurrentStatus();
            #region req-resp
            /*
            <?xml version="1.0"?>
            <Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                <ClientHeader>
                <PTID>CT202730</PTID>
                <BufferSize>1050</BufferSize>
                <mkTerminalID>1000004484</mkTerminalID>
                <mkSystemID>10002542</mkSystemID>
                <mkTerminalUserID>100000271</mkTerminalUserID>
                <mkTerminalUserGroupID>1000001510</mkTerminalUserGroupID>
                <EncryptionKey />
                <ProcessingDB>AuthDB\CST-DB-AUTH-3</ProcessingDB>
                </ClientHeader>
                <MsgType>TOKENREGISTRATIONSTATUS</MsgType>
                <MsgData>&lt;![CDATA[&lt;?xml version="1.0"?&gt;
            &lt;tokenregistrationstatus xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="TOKEN"&gt;
                &lt;id&gt;1061692600&lt;/id&gt;
            &lt;/tokenregistrationstatus&gt;]]&gt;</MsgData>
            </Message>

            Response:
            <?xml version="1.0"?>
            <Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                <MsgType>TOKENREGISTRATIONRESULT</MsgType>
                <MsgData>&lt;![CDATA[&lt;tokenregistrationresult xmlns="TOKEN"&gt;&lt;id&gt;1036107900&lt;/id&gt;&lt;status&gt;COMPLETED&lt;/status&gt;&lt;/tokenregistrationresult&gt;]]&gt;</MsgData>
            </Message>
                */
            #endregion

            state.Status = StatusEnum.AuthorisingTxn;
            SendCurrentStatus();
            #region req-resp
            /*
            <?xml version="1.0"?>
            <Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                <ClientHeader>
                <PTID>CT202730</PTID>
                <BufferSize>1050</BufferSize>
                <mkTerminalID>1000004484</mkTerminalID>
                <mkSystemID>10002542</mkSystemID>
                <mkTerminalUserID>100000271</mkTerminalUserID>
                <mkTerminalUserGroupID>1000001510</mkTerminalUserGroupID>
                <EncryptionKey />
                <ProcessingDB />
                </ClientHeader>
                <MsgType>TXN</MsgType>
                <MsgData>&lt;![CDATA[&lt;?xml version="1.0"?&gt;
            &lt;T&gt;
                &lt;MID&gt;140011409&lt;/MID&gt;
                &lt;CM&gt;6&lt;/CM&gt;
                &lt;TTy&gt;01&lt;/TTy&gt;
                &lt;CD&gt;## Sensitive Data Removed ##&lt;/CD&gt;
                &lt;PAN&gt;## Sensitive Data Removed ##&lt;/PAN&gt;
                &lt;ARQC&gt;AC7A914A00370ECC&lt;/ARQC&gt;
                &lt;AIP&gt;5C00&lt;/AIP&gt;
                &lt;ATC&gt;08DC&lt;/ATC&gt;
                &lt;TVR&gt;40C0008000&lt;/TVR&gt;
                &lt;IAD&gt;06FE0A03A4A900&lt;/IAD&gt;
                &lt;AID&gt;A0000000031010&lt;/AID&gt;
                &lt;P&gt;1&lt;/P&gt;
                &lt;IDT&gt;2014-02-28 14:28:06&lt;/IDT&gt;
                &lt;SA&gt;1&lt;/SA&gt;
                &lt;ACC&gt;140011409&lt;/ACC&gt;
                &lt;Val&gt;34.99&lt;/Val&gt;
                &lt;CB&gt;0&lt;/CB&gt;
                &lt;Gr&gt;0&lt;/Gr&gt;
                &lt;ACId&gt;16&lt;/ACId&gt;
                &lt;TC&gt;F296&lt;/TC&gt;
                &lt;TxCu&gt;826&lt;/TxCu&gt;
                &lt;Ref&gt;S0052T01X0075D28022014N0001&lt;/Ref&gt;
                &lt;CSId&gt;2&lt;/CSId&gt;
                &lt;ED&gt;0308&lt;/ED&gt;
                &lt;SD&gt;0402&lt;/SD&gt;
                &lt;IN /&gt;
                &lt;CardSchemeName&gt;Visa&lt;/CardSchemeName&gt;
                &lt;currencyexponent&gt;2&lt;/currencyexponent&gt;
                &lt;ETT&gt;22&lt;/ETT&gt;
                &lt;TCC&gt;826&lt;/TCC&gt;
                &lt;ROC&gt;09&lt;/ROC&gt;
                &lt;PSEQ&gt;00&lt;/PSEQ&gt;
                &lt;UN&gt;86C056F6&lt;/UN&gt;
                &lt;CTT&gt;00&lt;/CTT&gt;
                &lt;CID&gt;80&lt;/CID&gt;
                &lt;CVMR&gt;410302&lt;/CVMR&gt;
                &lt;CAVN&gt;0084&lt;/CAVN&gt;
                &lt;TAVN&gt;0096&lt;/TAVN&gt;
                &lt;ETC&gt;E0B8C8&lt;/ETC&gt;
                &lt;CNu&gt;************0002&lt;/CNu&gt;
                &lt;EODS&gt;A&lt;/EODS&gt;
                &lt;IACDEN&gt;1010A00060&lt;/IACDEN&gt;
                &lt;IACONL&gt;40E84C9800&lt;/IACONL&gt;
                &lt;IACDEF&gt;4040488000&lt;/IACDEF&gt;
                &lt;masterkeyid&gt;SEb14L&lt;/masterkeyid&gt;
                &lt;termserial&gt;koGma+udOSg=&lt;/termserial&gt;
                &lt;ptid&gt;29259621&lt;/ptid&gt;
                &lt;CTLSAREC&gt;
                &lt;DRT&gt;06&lt;/DRT&gt;
                &lt;DRST&gt;01&lt;/DRST&gt;
                &lt;TA&gt;300000&lt;/TA&gt;
                &lt;TAU&gt;000000&lt;/TAU&gt;
                &lt;ETCU /&gt;
                &lt;/CTLSAREC&gt;
                &lt;vgistransaction&gt;false&lt;/vgistransaction&gt;
                &lt;charitabledonationvalue&gt;0&lt;/charitabledonationvalue&gt;
                &lt;originalcharitabledonationvalue&gt;0&lt;/originalcharitabledonationvalue&gt;
            &lt;/T&gt;]]&gt;</MsgData>
            </Message>

            <?xml version="1.0"?>
            <Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                <MsgType>TIR</MsgType>
                <MsgData>&lt;![CDATA[&lt;TIR&gt;&lt;ID&gt;1005151232&lt;/ID&gt;&lt;AuthDB&gt;AuthDB\CST-DB-AUTH-3&lt;/AuthDB&gt;&lt;/TIR&gt;]]&gt;</MsgData>
            </Message>
            */
            #endregion

            state.Status = StatusEnum.WaitingForResult;
            SendCurrentStatus();
            #region req-resp
            /*
            <?xml version="1.0"?>
            <Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                <ClientHeader>
                <PTID>CT202730</PTID>
                <BufferSize>1050</BufferSize>
                <mkTerminalID>1000004484</mkTerminalID>
                <mkSystemID>10002542</mkSystemID>
                <mkTerminalUserID>100000271</mkTerminalUserID>
                <mkTerminalUserGroupID>1000001510</mkTerminalUserGroupID>
                <EncryptionKey />
                <ProcessingDB>AuthDB\CST-DB-AUTH-3</ProcessingDB>
                </ClientHeader>
                <MsgType>GTR</MsgType>
                <MsgData>&lt;![CDATA[&lt;?xml version="1.0"?&gt;
            &lt;GTR&gt;
                &lt;ID&gt;1005151232&lt;/ID&gt;
            &lt;/GTR&gt;]]&gt;</MsgData>
            </Message>

            <?xml version="1.0"?>
            <Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                <MsgType>TRM</MsgType>
                <MsgData>&lt;![CDATA[&lt;TRM&gt;&lt;ID&gt;1005151232&lt;/ID&gt;&lt;TxRt&gt;AUTHORISED&lt;/TxRt&gt;&lt;TID&gt;04380001&lt;/TID&gt;&lt;MNo&gt;1036&lt;/MNo&gt;&lt;ARCode&gt;00&lt;/ARCode&gt;&lt;AC&gt;789DE&lt;/AC&gt;&lt;AM&gt;AUTH CODE:789DE&lt;/AM&gt;&lt;VRT&gt;&lt;/VRT&gt;&lt;IARC&gt;&lt;/IARC&gt;&lt;IOAD&gt;&lt;/IOAD&gt;&lt;ISD&gt;&lt;/ISD&gt;&lt;CSCR&gt;0&lt;/CSCR&gt;&lt;AVHR&gt;0&lt;/AVHR&gt;&lt;AVPR&gt;0&lt;/AVPR&gt;&lt;AE&gt;0&lt;/AE&gt;&lt;ADT&gt;2014-02-28T14:29:19.407&lt;/ADT&gt;&lt;VC&gt;ldId+XBCRBg=&lt;/VC&gt;&lt;cardnumberhash&gt;s7EMHSC/B/EtkHVCf15LQYnRzuw=&lt;/cardnumberhash&gt;&lt;/TRM&gt;]]&gt;</MsgData>
            </Message>
            */
            #endregion

            if (ActiveTrouble.HasValue && (
                ActiveTrouble.Value == TroubleEnum.CommsDown ||
                ActiveTrouble.Value == TroubleEnum.Decline ||
                ActiveTrouble.Value == TroubleEnum.VoiceReferral))
            {
                if (ActiveTrouble.Value == TroubleEnum.CommsDown)
                {
                    trans.Declined = true;
                    if (!Mode820)
                        state.LoopActions.Add(State.TransactionAction.SendResponse);
                    state.LoopActions.Add(State.TransactionAction.RemoveCard);
                    state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                }
                else if (ActiveTrouble.Value == TroubleEnum.Decline)
                {
                    state.Status = StatusEnum.AuthResultReceived;
                    SendCurrentStatus();

                    trans.Declined = true;
                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                    state.LoopActions.Add(State.TransactionAction.RemoveCard);
                    state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                }
                else if (ActiveTrouble.Value == TroubleEnum.VoiceReferral)
                {
                    trans.VoiceReferral = true;
                    trans.TerminalId = 4380001;
                    trans.EFTSequenceNumber = 1036;

                    state.LoopActions.Add(State.TransactionAction.RemoveCard);
                    state.LoopActions.Add(State.TransactionAction.SendResponse);
                    state.LoopActions.Add(State.TransactionAction.AuthorizeVoiceReferral);
                }

                ContinueIntegrationLoop();
                return;
            }

            trans.TerminalId = 4380001;
            trans.EFTSequenceNumber = 1036;
            trans.AuthorisationCode = "789DE";
            trans.TokenId = new Random().Next().ToString();
            trans.Authorized = true;
            TransactionsRepository.Add(trans);
            state.Status = StatusEnum.AuthResultReceived;
            SendCurrentStatus();

            state.LoopActions.Add(State.TransactionAction.SendResponse);
            if (trans.CardInputMethod == CardInputMethod.KeyedIn || trans.CardInputMethod == CardInputMethod.Swiped)
            {
                // TODO: check that stuff
                if (trans.Modifier != CreditOrDebitCardTransaction.TransactionModifierEnum.TelephoneOrder && trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Refund)
                    state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                else if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.CardholderPresent && trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase)
                    state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);

                if (trans.Modifier == CreditOrDebitCardTransaction.TransactionModifierEnum.TelephoneOrder)
                    state.LoopActions.Add(State.TransactionAction.ConfirmAuthCode);
                else
                    state.LoopActions.Add(State.TransactionAction.ConfirmSignature);
            }
            ContinueIntegrationLoop();
        }

        private void ConfirmTransaction()
        {
            CreditOrDebitCardTransaction trans = (CreditOrDebitCardTransaction)state.Transaction;

            // Complete transaction
            state.Status = StatusEnum.ConfirmingTxn;
            SendCurrentStatus();
            #region req-resp
            /*
<?xml version="1.0"?>
<Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <ClientHeader>
    <PTID>CT202730</PTID>
    <BufferSize>1050</BufferSize>
    <mkTerminalID>1000004484</mkTerminalID>
    <mkSystemID>10002542</mkSystemID>
    <mkTerminalUserID>100000271</mkTerminalUserID>
    <mkTerminalUserGroupID>1000001510</mkTerminalUserGroupID>
    <EncryptionKey />
    <ProcessingDB>AuthDB\CST-DB-AUTH-3</ProcessingDB>
    </ClientHeader>
    <MsgType>CNF</MsgType>
    <MsgData>&lt;![CDATA[&lt;?xml version="1.0"?&gt;
&lt;CNF&gt;
    &lt;ID&gt;1005151232&lt;/ID&gt;
    &lt;CGy&gt;0&lt;/CGy&gt;
    &lt;CARC&gt;00&lt;/CARC&gt;
    &lt;CITC&gt;10DEF826C84C7BCB&lt;/CITC&gt;
    &lt;CAUC&gt;FD00&lt;/CAUC&gt;
    &lt;CTVR&gt;40C0008000&lt;/CTVR&gt;
    &lt;CIAD&gt;06FE0A0364AD00&lt;/CIAD&gt;
    &lt;CCID&gt;40&lt;/CCID&gt;
    &lt;CTSI&gt;E800&lt;/CTSI&gt;
&lt;/CNF&gt;]]&gt;</MsgData>
</Message>

Response:
<?xml version="1.0"?>
<Message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <MsgType>FTR</MsgType>
    <MsgData>&lt;![CDATA[&lt;FTR&gt;&lt;ID&gt;1005151232&lt;/ID&gt;&lt;TxRt&gt;CHARGED&lt;/TxRt&gt;&lt;PRT&gt;&lt;/PRT&gt;&lt;/FTR&gt;]]&gt;</MsgData>
</Message>
                */
            #endregion

            if (trans.VoiceReferral)
                trans.Authorized = true;

            trans.Completed = true;
            state.Status = StatusEnum.FinalResultReceived;
            SendCurrentStatus();

            if (!VisionCompatibility)
                state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);

            if (trans.Type == CreditOrDebitCardTransaction.TransactionTypeEnum.Refund)
            {
                state.LoopActions.Add(State.TransactionAction.SendResponse);
            }
        }

        private void CancelTransaction()
        {
            state.LoopActions.Clear();
            FireInteractionEvent(null);

            if (!state.OperationType.HasValue)
                return;

            if (state.OperationType.Value == State.OperationTypeEnum.CreditOrDebitCard)
            {
                if (state.Transaction == null)
                    return;

                state.Transaction.Cancelled = true;
                if (state.Status == StatusEnum.PinEntry)
                {
                    if (Mode820)
                    {
                        state.Status = StatusEnum.CancellingTransaction;
                        SendCurrentStatus();
                    }
                }
                else if (state.Status == StatusEnum.AwaitingCard)
                {
                    state.Status = StatusEnum.ProcessingTxn;
                    SendCurrentStatus();
                    state.Status = StatusEnum.TransactionCancelled;
                    SendCurrentStatus();
                }

                state.LoopActions.Add(State.TransactionAction.RemoveCard);
                state.LoopActions.Add(State.TransactionAction.SendResponse);
                ContinueIntegrationLoop();
            }
            else if (state.OperationType == State.OperationTypeEnum.GiftCard)
            {
                if (state.Transaction == null)
                    return;

                state.Transaction.Cancelled = true;
                if (((GiftCardTransaction)state.Transaction).PartialPurchase)
                {
                    state.LoopActions.Add(State.TransactionAction.PrintMerchantReceipt);
                    state.LoopActions.Add(State.TransactionAction.PrintCustomerReceipt);
                }
                state.LoopActions.Add(State.TransactionAction.SendResponse);
                ContinueIntegrationLoop();
            }
            else if (state.OperationType == State.OperationTypeEnum.GetCardDetails)
            {
                var resp = new Packet.GetCardDetailsResponse();
                resp.Result = Packet.GetCardDetailsResponse.ResultEnum.RetrievalCancelled;
                resp.RebuildPayload();
                SendPacket(resp, tcpClientIntegration);
                state.Reset();
                SendCurrentStatus();
            }
        }
    }
}
