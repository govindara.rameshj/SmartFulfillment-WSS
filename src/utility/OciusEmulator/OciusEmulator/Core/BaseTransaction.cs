﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    public class BaseTransaction
    {
        public DateTime StartTime;
        public decimal TxnValue;
        public bool Cancelled;

        public BaseTransaction()
        {
            this.StartTime = DateTime.Now;
            this.TxnValue = 0;
            this.Cancelled = false;
        }
    }
}
