﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    class ManualData
    {
        public string Number;
        public string ExpiryDate;
        public string CSC;
        public string AVSHouseNumber;
        public string AVSPostCode;
    }
}
