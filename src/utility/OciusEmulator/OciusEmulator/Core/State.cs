﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    class State
    {
        #region Specific state structures
        public enum OperationTypeEnum
        {
            CreditOrDebitCard,
            GiftCard,
            GetCardDetails,
        }

        public enum ReceiptTypeEnum
        {
            Customer,
            Merchant,
        }

        public enum TransactionAction
        {
            PrintMerchantReceipt,
            PrintCustomerReceipt,
            SendResponse,
            RemoveCard,
            ConfirmSignature,
            AuthorizeVoiceReferral,
            ConfirmAuthCode,
            ConfirmTransaction,
        }
        #endregion

        public System.Threading.Timer PinEntryTimer;
        public System.Threading.Timer StartCardProcessingTimer;

        #region State fields
        public OperationTypeEnum? OperationType;
        private StatusEnum status;
        public StatusEnum Status
        {
            get { return status; }
            set
            {
                if (StartCardProcessingTimer != null)
                {
                    StartCardProcessingTimer.Dispose();
                    StartCardProcessingTimer = null;
                }

                if (status == StatusEnum.PinEntry)
                {
                    if (PinEntryTimer != null)
                    {
                        PinEntryTimer.Dispose();
                        PinEntryTimer = null;
                    }
                }
                else if (value == StatusEnum.PinEntry)
                {
                    PINFailedTriesCount = 0;
                }
                status = value;
            }
        }

        public bool LoggedIn;
        public BaseTransaction Transaction;
        public BaseTransaction RefundTransaction;
        public Card InsertedCard;
        public bool CardInserted;
        public bool CardProcessed;
        public bool WaitingForCardRemoval;
        public int CardBadInsertions;

        // temp data for manual CNP mode
        public ManualData ManuallyEnteredData;

        public string CurrentPIN;
        public int PINFailedTriesCount;
        public List<TransactionAction> LoopActions;
        public TransactionAction? LastLoopAction;
        public int PrintReceiptTry;

        public int BarclaysGiftTransactionNumber;
        #endregion

        public State()
        {
            this.LoopActions = new List<TransactionAction>();

            this.LoggedIn = false;
            this.CardInserted = false;

            BarclaysGiftTransactionNumber = 1000;
            Reset();
        }

        public void Reset()
        {
            if (LoggedIn)
                Status = StatusEnum.Ready;
            else
                Status = StatusEnum.LoginRequired;

            OperationType = null;
            Transaction = null;
            RefundTransaction = null;
            InsertedCard = null;
            CardProcessed = false;
            ManuallyEnteredData = null;
            WaitingForCardRemoval = false;

            CurrentPIN = "";
            PINFailedTriesCount = 0;
            CardBadInsertions = 0;
            LoopActions.Clear();
            LastLoopAction = null;

            PrintReceiptTry = 0;
        }
    }
}
