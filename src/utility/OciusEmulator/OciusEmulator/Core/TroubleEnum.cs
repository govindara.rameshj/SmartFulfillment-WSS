﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    public enum TroubleEnum
    {
        CommsDown,
        Decline,
        VoiceReferral,
        ReversedInOfflineMode,
        Offline,
        NonIdle,
        FailedToRetrieveMessage,
        UnknownProgressMessage
    }
}
