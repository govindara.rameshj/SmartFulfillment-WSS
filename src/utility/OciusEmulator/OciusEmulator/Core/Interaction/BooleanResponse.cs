﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Interaction
{
    public class BooleanResponse: BaseInteractionResponse
    {
        private bool result;
        public bool Result
        {
            get
            {
                return result;
            }
        }

        public BooleanResponse(bool result)
        {
            this.result = result;
        }
    }
}
