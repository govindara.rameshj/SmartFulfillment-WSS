﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Interaction
{
    public class ConfirmReceiptPrintingResponse: BaseInteractionResponse
    {
        private bool continueTransaction;
        public bool ContinueTransaction
        {
            get
            {
                return continueTransaction;
            }
        }

        public ConfirmReceiptPrintingResponse(bool continueTransaction)
        {
            this.continueTransaction = continueTransaction;
        }
    }
}
