﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Interaction
{
    public class ConfirmReceiptPrinting: BaseInteraction
    {
        private ReceiptType receiptType;
        public ReceiptType ReceiptType
        {
            get
            {
                return receiptType;
            }
        }

        public ConfirmReceiptPrinting(ReceiptType receiptType)
        {
            this.receiptType = receiptType;
        }
    }
}
