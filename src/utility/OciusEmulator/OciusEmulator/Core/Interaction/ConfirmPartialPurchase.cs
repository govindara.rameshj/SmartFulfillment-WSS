﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Interaction
{
    public class ConfirmPartialPurchase : BaseInteraction
    {
        private decimal remainingBalance;
        public decimal RemainingBalance
        {
            get
            {
                return remainingBalance;
            }
        }

        public ConfirmPartialPurchase(decimal remainingBalance)
        {
            this.remainingBalance = remainingBalance;
        }
    }
}
