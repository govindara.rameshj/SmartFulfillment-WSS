﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace OciusEmulator.Core
{
    static class ReceiptPrinter
    {
        public static string CustomerReceiptFile = @"C:\Commidea\CustomerReceipt.txt";
        public static string MerchantReceiptFile = @"C:\Commidea\MerchantReceipt.txt";

        public static void PrintReceipt(State state, ReceiptType receiptType)
        {
            switch (state.OperationType.Value)
            {
                case State.OperationTypeEnum.GiftCard:
                    PrintGiftCardReceipt((GiftCardTransaction)state.Transaction, receiptType);
                    break;

                case State.OperationTypeEnum.CreditOrDebitCard:
                    PrintCreditCardReceipt((CreditOrDebitCardTransaction)state.Transaction, receiptType);
                    break;
                default:
                    throw new NotImplementedException();
            }

            /*{
                string filename = null;
                switch (receiptType)
                {
                    case ReceiptType.Customer:
                        filename = CustomerReceiptFile;
                        break;
                    case ReceiptType.Merchant:
                        filename = MerchantReceiptFile;
                        break;
                }
                FileInfo fi = new FileInfo(filename);
                fi.CreationTime = fi.CreationTime.AddMinutes(1);
                fi.LastWriteTime = fi.CreationTime;
                fi.LastAccessTime = fi.CreationTime;
            }*/
        }

        private static void PrintCreditCardReceipt(CreditOrDebitCardTransaction tran, ReceiptType receiptType)
        {
            switch (receiptType)
            {
                case ReceiptType.Customer:
                    PrintCreditCardCustomerReceipt(tran);
                    break;
                case ReceiptType.Merchant:
                    PrintCreditCardMerchantReceipt(tran);
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        private static void PrintGiftCardReceipt(GiftCardTransaction tran, ReceiptType receiptType)
        {
            switch (receiptType)
            {
                case ReceiptType.Customer:
                    PrintGiftCardCustomerReceipt(tran);
                    break;
                case ReceiptType.Merchant:
                    PrintGiftCardMerchantReceipt(tran);
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        private static void PrintCreditCardCustomerReceipt(CreditOrDebitCardTransaction tran)
        {
            using (StreamWriter writer = new StreamWriter(CustomerReceiptFile, false, new ASCIIEncoding()))
            {
                /*writer.WriteLine(@"###################DefaultCustomerHeader###################
P:PW001106 T:****0001
M:**19049
24/11/2011 11:54:39
-------------------------------
Visa
************0006
KEYED CP SALE
Please debit my account
AMOUNT pound10.00
TOTAL pound10.00
SIGNATURE VERIFIED
-------------------------------
Please Keep This Receipt
For your Records
Auth Code: 789DE
Ref:
###################DefaultCustomerFooter###################");*/

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(@"|2C|bC **CARDHOLDER COPY**



Wickes Test ,Store 8808



P:CT202802           T:****5678

M:**61001

     05/05/2014 12:11:24

----------------------------------------

|2C|bC    CREDITO DE VISA   

************0119



ICC   CP SALE

|2C|bC      PIN ENTERED     



   Please debit my account

|2C|bCAMOUNT        \u00a3{0:0.00} 
|2C|bCTOTAL         \u00a3{0:0.00} 


|2C|bC     PIN VERIFIED     

----------------------------------------

   Please Keep This Receipt

       For your Records

Auth Code: {1}

Ref:       S0853T01X1929D05052014N0001

AID: A0000000031010

App Seq: 01



----------------------------------------

 Account on File Registration

           Details

Result: Registration Successful

Token ID: 1034423800

----------------------------------------
",
                tran.TxnValue,
                tran.AuthorisationCode);

                writer.WriteLine(sb.ToString());
                writer.Flush();
                writer.Close();
            }
        }

        private static void PrintCreditCardMerchantReceipt(CreditOrDebitCardTransaction tran)
        {
            using (StreamWriter writer = new StreamWriter(MerchantReceiptFile, false, new ASCIIEncoding()))
            {
                /*writer.WriteLine(@"###################DefaultMerchantHeader###################
P:PW001106 T:****0001
M:**19049
24/11/2011 11:54:39
-------------------------------
Visa
************0006
KEYED CP SALE
Please debit my account
AMOUNT pound10.00
TOTAL pound10.00
SIGNATURE VERIFIED
-------------------------------
Please Keep This Receipt
For your Records
Auth Code: 789DE
Ref:
###################DefaultMerchantFooter###################");*/

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(@"|2C|bC*** MERCHANT COPY *** 



Wickes Test ,Store 8808



P:CT202802           T:12345678

M:5461001

     05/05/2014 12:13:10

----------------------------------------

|2C|bC         Visa         

************0119

Expiry: 12/15



KEYED   CNP Acc On File

|2C|bC    ##############    

|2C|bC    ##  REFUND  ##    

|2C|bC    ##############    



   Please credit my account

|2C|bCAMOUNT        \u00a3{0:0.00} 
|2C|bCTOTAL         \u00a3{0:0.00} 


          CONFIRMED

----------------------------------------

   Please Keep This Receipt

       For your Records

EFTSN:     1051

Auth Code: {1}

Ref:       S0853T01X1930D05052014N0001

Token ID: 1034423800
",
                tran.TxnValue,
                tran.AuthorisationCode);

                writer.WriteLine(sb.ToString());
                writer.Flush();
                writer.Close();
            }
        }

        private static void PrintGiftCardCustomerReceipt(GiftCardTransaction tran)
        {
            using (StreamWriter writer = new StreamWriter(CustomerReceiptFile, false, new ASCIIEncoding()))
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(
@"Wickes Test ,Store 8808

P:CT203037           T:12345678
M:5461001
     {0:dd/MM/yyyy HH:mm:ss}
-------------------------------
Gift Card Number: 
{1} ({8})

PURCHASE VALUE         GBP {2:0.00}

       Sale Successful

BALANCE              GBP {3:0.00}

Message No:         {4}
Txn ID:             {5}
Ref:                {6}
Auth Code: {7}
",
                    tran.StartTime,
                    (tran.UsedCard != null) ? tran.UsedCard.GetMaskedCardNumber() : "-",
                    tran.TxnValue,
                    (tran.UsedCard != null) ? (object)tran.UsedCard.Balance : "-",
                    1032,
                    tran.TransactionId,
                    tran.Reference,
                    tran.AuthCode.HasValue ? tran.AuthCode.Value.ToString("000000000") : "-", // TODO: check - sign in case auth code is empty
                    tran.CardInputMethod != null ? tran.CardInputMethod.ToString().ToUpper() : "UNKNOWN");

                writer.WriteLine(sb.ToString());
                writer.Flush();
                writer.Close();
            }
        }

        private static void PrintGiftCardMerchantReceipt(GiftCardTransaction tran)
        {
            using (StreamWriter writer = new StreamWriter(MerchantReceiptFile, false, new ASCIIEncoding()))
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(
@"Wickes Test ,Store 8808

P:CT203037           T:12345678
M:5461001
     {0:dd/MM/yyyy HH:mm:ss}
-------------------------------
Gift Card Number: 
{1} ({8})

PURCHASE VALUE         GBP {2:0.00}


BALANCE              GBP {3:0.00}

Message No:         {4}
Txn ID:             {5}
Ref:                {6}
Auth Code: {7}
",
                    tran.StartTime,
                    (tran.UsedCard != null) ? tran.UsedCard.GetMaskedCardNumber() : "-",
                    tran.TxnValue,
                    (tran.UsedCard != null) ? (object)tran.UsedCard.Balance : "-",
                    1032,
                    tran.TransactionId,
                    tran.Reference,
                    tran.AuthCode.HasValue ? tran.AuthCode.Value.ToString("000000000") : "-", // TODO: check - sign in case auth code is empty
                    tran.CardInputMethod != null ? tran.CardInputMethod.ToString().ToUpper() : "UNKNOWN");

                writer.WriteLine(sb.ToString());
                writer.Flush();
                writer.Close();
            }
        }
    }
}
