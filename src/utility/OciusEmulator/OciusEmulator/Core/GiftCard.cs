﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    public class GiftCard
    {
        private string number;
        public string Number
        {
            get { return number; }
        }

        public decimal Balance;

        private DateTime expiryDate;
        public DateTime ExpiryDate
        {
            get { return expiryDate; }
        }

        public bool LostOrStolen;
        private bool expired;
        public bool Expired
        {
            get
            {
                return expired;
            }
            set
            {
                if (expired == value)
                    return;

                expired = value;
                if (expired)
                    this.expiryDate = DateTime.Now.AddMonths(2);
                else
                    this.expiryDate = DateTime.Now.AddYears(2);
            }
        }

        public GiftCard(string number, decimal balance)
        {
            this.number = number;
            this.Balance = balance;

            LostOrStolen = false;
            expired = false;

            // auto-generate other attributes
            this.expiryDate = DateTime.Now.AddYears(2);
        }

        public string GetMaskedCardNumber()
        {
            var chars = number.Select((c, index) => index < number.Length - 4 ? '*' : c).ToArray();
            return String.Join("", chars);
        }
    }
}
