﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class WindowStateResponse: Packet
    {
        public WindowStateResponse() :
            base()
        {
            this.Payload = "0,1,,,,,,,,,,,,,,,,Window State Changed";
        }
    }
}
