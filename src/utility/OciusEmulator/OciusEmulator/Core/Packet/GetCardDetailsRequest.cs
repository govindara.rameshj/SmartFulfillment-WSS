﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class GetCardDetailsRequest : Packet
    {
        public enum OperationModeEnum
        {
            NonEFT = 0,
            EFT = 1,
        }

        public enum RemoveCardSettingEnum
        {
            DoNotRemove = 0,
            Remove = 1
        }

        public enum POSVersionEnum
        {
            Standard = 0,
            Version1 = 1
        }

        public OperationModeEnum OperationMode;
        public RemoveCardSettingEnum RemoveCardSetting;
        public POSVersionEnum? POSVersion;

        public override void Parse(string[] parts)
        {
            base.Parse(parts);

            this.OperationMode = (OperationModeEnum)Enum.Parse(typeof(OperationModeEnum), parts[1]);
            this.RemoveCardSetting = (RemoveCardSettingEnum)Enum.Parse(typeof(RemoveCardSettingEnum), parts[2]);
            if (parts.Length >= 3)
                this.POSVersion = (POSVersionEnum)Enum.Parse(typeof(POSVersionEnum), parts[3]);
            else
                this.POSVersion = null;
        }
    }
}
