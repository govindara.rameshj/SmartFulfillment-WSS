﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class Packet
    {
        private static UTF8Encoding encoding = new UTF8Encoding();

        public string Payload;

        protected Packet()
        {
        }

        public Packet(string payload)
        {
            this.Payload = payload;
        }

        public virtual byte[] GetData()
        {
            string msg = Payload + "\r\n";
            return encoding.GetBytes(msg);
        }

        public static Packet ParseRequest(byte[] data)
        {
            Packet res = null;

            // TODO request may contain some commands, one in each line. In such case the code below fails.
            string str = encoding.GetString(data);
            str = str.TrimEnd(new char[] { '\r', '\n' });

            string[] parts = str.Split(new char[] { ',' });
            switch (parts[0])
            {
                case "L2":
                    res = new LoginRequest();
                    break;
                case "T":
                    res = new TransactionRequest();
                    break;
                case "CONTTXN":
                    res = new ContinueTransactionRequest();
                    break;
                case "BGIFT":
                    res = new BarclaycardGiftRequest();
                    break;
                case "OLS":
                    res = new OfflineSubmissionRequest();
                    break;
                case "O":
                    res = new LogoutRequest();
                    break;
                case "REQINFO":
                    res = new RequestInfoRequest();
                    break;
                case "GDET":
                    res = new GetCardDetailsRequest();
                    break;
                case "WINSTATE":
                    res = new WindowStateRequest();
                    break;
                default:
                    throw new PacketParseException("Unknown type " + parts[0]);
            }
            res.Parse(parts);

            return res;
        }

        public virtual void Parse(string[] parts)
        {
            this.Payload = string.Join(",", parts);
        }

        public virtual void RebuildPayload()
        {
        }

        protected string BuildParameters(Dictionary<string, string> parameters)
        {
            StringBuilder res = new StringBuilder();
            if (parameters != null)
            {
                foreach (var kv in parameters)
                {
                    if (res.Length > 0)
                        res.Append(';');
                    res.Append(kv.Key + "=" + kv.Value);
                }
            }
            return res.ToString();
        }

        protected Dictionary<string, string> ParseParameters(string parameters)
        {
            var res = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(parameters))
            {
                string[] parts = parameters.Split(new char[] { ';' });
                foreach (var par in parts)
                {
                    var kv = par.Split(new char[] { '=' });
                    res.Add(kv[0], kv[1]);
                }
            }
            return res;
        }
    }
}
