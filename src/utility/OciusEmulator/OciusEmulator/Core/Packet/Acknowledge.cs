﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class Acknowledge: Packet
    {
        public override byte[] GetData()
        {
            return new byte[] { 0x06 };
        }
    }
}
