﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class PacketParseException : Exception
    {
        public PacketParseException(string message) :
            base(message)
        {
        }
    }
}
