﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class BarclaycardGiftRequest: Packet
    {
        public decimal? AccountID;
        public GiftCardTransaction.TransactionTypeEnum TransactionType;
        public decimal? Amount;
        public string Reference;
        /// <summary>
        /// For void operations only
        /// </summary>
        public decimal? OriginalTransactionId;

        public override void Parse(string[] parts)
        {
            base.Parse(parts);

            if (!string.IsNullOrEmpty(parts[1]))
                this.AccountID = decimal.Parse(parts[1]);
            else
                this.AccountID = null;

            this.TransactionType = (GiftCardTransaction.TransactionTypeEnum)Enum.Parse(typeof(GiftCardTransaction.TransactionTypeEnum), parts[2]);

            if (!string.IsNullOrEmpty(parts[3]))
                this.Amount = decimal.Parse(parts[3]);
            else
                this.Amount = null;

            this.Reference = parts[4];

            if (!string.IsNullOrEmpty(parts[5]))
                this.OriginalTransactionId = decimal.Parse(parts[5]);
            else
                this.OriginalTransactionId = null;
        }
    }
}
