﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class BarclaycardGiftResponse: Packet
    {
        public enum ResultCodeEnum
        {
            Success = 0,
            Reversed = 1,
            Rejected = 2,
            /// <summary>
            /// TODO: must be specific code
            /// </summary>
            Error = -1,
            UnspecifiedError = -30,
            ServiceNotAllowed = -31,
            Cancelled = -99,
            InvalidBarclaysGiftConfiguration = -130,
        }

        public enum ResponseCodeEnum
        {
            Ok = 0,
            NoCard = 10,
            CardValidButNotForThisMerchant = 20,
            PartialPurchaseTransaction = 30,
            ExchangeRateNotPresent = 40,
            MaximumLoadExceeded = 50,
            MinimumLoadNotMet = 51,
            MaximumValueOnCardExceeded = 52,
            CardExpired = 60,
            MessageTypeInvalid = 71,
            VoidPeriodElapsed = 72,
            MessageDetailsIncorrect = 73,
            InsufficientFunds = 74,
            CallCallCentre = 99,
            ConfigurationProblem = -1,
        }

        public ResultCodeEnum ResultCode;
        public ResponseCodeEnum? ResponseCode;
        public string TransactionAuthCode;
        public decimal? Amount;
        public decimal? RemainingCardBalance;
        public string Message;
        public int? MessageNumber;
        public DateTime TransactionDateTime;
        public decimal? TransactionID;

        public BarclaycardGiftResponse()
        {
            ResultCode = ResultCodeEnum.UnspecifiedError;
            ResponseCode = null;
            Amount = null;
            RemainingCardBalance = null;
            Message = null;
            MessageNumber = null;
            TransactionDateTime = new DateTime(1, 1, 1);
            TransactionID = null;
        }

        public override void RebuildPayload()
        {
            string amount = null;
            string remainingCardBalance = null;

            if (Amount.HasValue)
                amount = Amount.Value.ToString(".00000");

            if (RemainingCardBalance.HasValue)
                remainingCardBalance = RemainingCardBalance.Value.ToString(".00000");

            this.Payload = string.Format("{0},{1},{2},{3},{4},{5},{6},{7:dd MMM yyyy HH:mm:ss},{8}",
                (int)ResultCode, (int?)ResponseCode, TransactionAuthCode, amount, remainingCardBalance, Message,
                MessageNumber, TransactionDateTime, TransactionID);
        }
    }
}
