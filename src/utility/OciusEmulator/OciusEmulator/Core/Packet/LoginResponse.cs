﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class LoginResponse : Packet
    {
        public enum Kind
        {
            Success,
            Already,
        }

        public LoginResponse(Kind responseKind) :
            base()
        {
            if (responseKind == Kind.Success)
                this.Payload = "0,1,,,,,,,,,,,,,,,,Login Successful";
            else if (responseKind == Kind.Already)
                this.Payload = "-84,1,,,,,,,,,,,,,,,,USER ALREADY LOGGED IN";
        }
    }
}
