﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class TransactionRequest : Packet
    {
        public enum RegisterForAccountOnFileEnum
        {
            NotSet = 0,
            DoNotRegister,
            Register,
            RegisterOnly,
            RegisterDecline
        }

        public CreditOrDebitCardTransaction.TransactionTypeEnum TransactionType;
        public CreditOrDebitCardTransaction.TransactionModifierEnum Modifier;
        public decimal TxnValue;
        public string Reference;
        public RegisterForAccountOnFileEnum RegisterForAccountOnFile;
        public string TokenId;

        public override void Parse(string[] parts)
        {
            base.Parse(parts);

            switch (parts[2])
            {
                case "01":
                    this.TransactionType = CreditOrDebitCardTransaction.TransactionTypeEnum.Purchase;
                    break;
                case "02":
                    this.TransactionType = CreditOrDebitCardTransaction.TransactionTypeEnum.Refund;
                    break;
                case "04":
                    this.TransactionType = CreditOrDebitCardTransaction.TransactionTypeEnum.CashAdvance;
                    break;
                default:
                    throw new ArgumentException("Transaction type unknown: " + parts[2], "parts[2]");
            }

            switch (parts[3])
            {
                case "0000":
                    this.Modifier = CreditOrDebitCardTransaction.TransactionModifierEnum.CardholderPresent;
                    break;
                case "1000":
                    this.Modifier = CreditOrDebitCardTransaction.TransactionModifierEnum.TelephoneOrder;
                    break;
                case "2000":
                    this.Modifier = CreditOrDebitCardTransaction.TransactionModifierEnum.AccountOnFile;
                    break;
                default:
                    throw new ArgumentException("Modifier unknown: " + parts[3], "parts[3]");
            }

            this.TxnValue = decimal.Parse(parts[10]);
            this.Reference = parts[22];
            this.RegisterForAccountOnFile = (RegisterForAccountOnFileEnum)Enum.Parse(typeof(RegisterForAccountOnFileEnum), parts[26]);
            if (parts.Length >= 28)
                this.TokenId = parts[27];
            else
                this.TokenId = null;
        }
    }
}
