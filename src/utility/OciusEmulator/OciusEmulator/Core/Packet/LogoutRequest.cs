﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class LogoutRequest: Packet
    {
        public enum FunctionEnum
        {
            Logout = 0,
            PrintQReportAndLogout = 1,
            ExitAndLogout = 2,
            ExitAndLogoutWithQReport = 3,
            SubmitOfflineTransactionsLogoutAndShutDown = 4,
        }

        public FunctionEnum Function;

        public override void Parse(string[] parts)
        {
            base.Parse(parts);

            this.Function = (FunctionEnum)Enum.Parse(typeof(FunctionEnum), parts[1]);
        }
    }
}
