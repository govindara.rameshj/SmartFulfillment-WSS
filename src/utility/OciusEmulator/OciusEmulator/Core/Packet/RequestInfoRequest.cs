﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class RequestInfoRequest : Packet
    {
        public enum ReqInfoTypeEnum
        {
            PTID = 1,
            Version = 2,
            LoginStatusReport = 3,
            TestTransactionServer = 4,
            LastPEDStatus = 5,
            PEDDetails = 6
        }

        public ReqInfoTypeEnum ReqInfoType;

        public override void Parse(string[] parts)
        {
            base.Parse(parts);

            this.ReqInfoType = (ReqInfoTypeEnum)Enum.Parse(typeof(ReqInfoTypeEnum), parts[1]);
        }
    }
}
