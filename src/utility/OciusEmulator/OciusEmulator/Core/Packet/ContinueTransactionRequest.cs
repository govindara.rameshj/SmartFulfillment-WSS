﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class ContinueTransactionRequest: Packet
    {
        public enum ActionEnum
        {
            NotSpecified = 0,
            BypassPIN = 1,
            ContinueTransaction = 2,
            ConfirmSignature = 3,
            RejectSignature = 4,
            ReprintReceipt = 5,
            KeyedEntryRequired = 6,
            VoiceReferralAuthorised = 7,
            VoiceReferralRejected = 8,
            GratuityRequired = 9,
            GratuityNotRequired = 10,
            GratuityOnPED = 11,
            CancelTransaction = 12,
            AlternatePayment = 13,
            CustomerPresentRequired = 14,
            CustomerNotPresentRequired = 15,
            ECommerceRequired = 16,
            MailOrderRequired = 17,
            TelephoneOrderRequired = 18,
            ChangeCard = 19,
            ConfirmAuthCode = 20,
            RejectAuthCode = 21,
            ChargeAuthCode = 22,
            ReverseUATICCTxn = 23,
            RetryDevice = 24,
            ContinueWithoutDevice = 25,
            AbortDeviceConnectivity = 26,
            RetryDownload = 27,
            CancelDownload = 28,
            CashbackRequired = 29,
            CashbackNotRequired = 30,
            Restart = 31,
            AcceptUnsafeDownload = 32,
            RejectUnsafeDownload = 33,
            ReplaceAccount = 34,
            CancelReplaceAccount = 35,
            ConfirmGratuity = 36,
            ChangeGratuity = 37,
            AccountOnFileRegistrationRequired = 38,
            AccountOnFileRegistrationNotRequired = 39,
            ReconnectToServer = 40,
            AbortReconnectToServer = 41,
            SelectPayPointAccount = 42,
            SelectPayPointOption = 43,
            RetryPayPointConfirmation = 44,
            CancelPayPointConfirmation = 45,
            AcceptLicenceKey = 46,
            RejectLicenceKey = 47,
            CancelLicenceKeyVerification = 48,
            ContinueLicenceKeyVerification = 49,
            ConfirmParkRetailGiftTransaction = 52,
            ReverseParkRetailGiftTransaction = 53,
            CancelGetCardDetails = 54,
            NoTransactionValueUpdateRequired = 55,
            TransactionValueUpdateRequired = 56,
            MerchantCurrencyRequired = 57,
            CardholderCurrencyRequired = 58,
            MerchantReferenceEntered = 59,
            SupplyIVRDetails = 60,
            RejectIVR = 61,
            PrintShopCopyReceipt = 62,
            CancelShopCopyReceipt = 63,
            SupplySTAN = 64,
            AcceptPartPayment = 65,
            RejectPartPayment = 66,
            FurtherPaymentRequired = 67,
            NoFurtherPaymentRequired = 68,
        }

        public ActionEnum Action;
        public Dictionary<string, string> Parameters;

        public override void Parse(string[] parts)
        {
            base.Parse(parts);

            // CONTTXN,2,
            this.Action = (ActionEnum)Enum.Parse(typeof(ActionEnum), parts[1]);
            if (parts.Length >= 2)
                this.Parameters = ParseParameters(parts[2]);
            else
                this.Parameters = new Dictionary<string, string>();
        }
    }
}
