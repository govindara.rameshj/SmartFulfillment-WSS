﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class LogoutResponse : Packet
    {
        public LogoutResponse() :
            base()
        {
            this.Payload = "0,1,,,,,,,,,,,,,,,,Ocius Shutting Down";
        }
    }
}
