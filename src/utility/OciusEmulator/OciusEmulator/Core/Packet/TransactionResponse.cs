﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class TransactionResponse : Packet
    {
        public enum ResultEnum
        {
            Completed = 0,
            Referred = 2,
            Declined = 5,
            Authorized = 6,
            Reversed = 7,
            CommsDown = 8,
            TransactionBillCancelled = -99,
            /// <summary>
            /// -30 can have other meanings
            /// </summary>
            InvalidTokenId = -30,
        }

        public enum AccountOnFileRegistrationResultEnum
        {
            NotSet = 0,
            NotPerformed,
            Success,
            Failed
        }

        public enum DataCheckResultEnum
        {
            Unknown = 0,
            NotChecked = 1,
            Matched = 2,
            NotMatched = 4,
            Reserved = 8
        }

        public ResultEnum Result;
        public int TerminateLoop;
        public decimal TotalTransactionValueProcessed;

        /// <summary>
        /// Primary account number (or Card number in other words)
        /// </summary>
        public string PAN;

        /// <summary>
        /// Card Expiry month and year
        /// </summary>
        public DateTime? ExpiryDate;

        /// <summary>
        /// Card start month and year
        /// </summary>
        public DateTime? Start;
        public DateTime TransactionDateTime;

        public int? MerchantNumber;
        public int? TerminalId;
        public string SchemeName;
        public string EFTSequenceNumber;
        public string AuthorisationCode;
        public string VRTelNo;
        public string Message;
        public CardInputMethod? CardInputMethod;
        public AccountOnFileRegistrationResultEnum AccountOnFileRegistrationResult;
        public string TokenId;
        /// <summary>
        /// This is a SHA-256 hash of the PAN with a merchant specific salt
        /// </summary>
        public string CardNumberHash;

        public DataCheckResultEnum AVSPostCodeResult;
        public DataCheckResultEnum AVSHouseNumberResult;
        public DataCheckResultEnum CSCResult;

        public TransactionResponse() :
            base()
        {
            TerminateLoop = 1;
            TotalTransactionValueProcessed = 0;

            PAN = null;
            ExpiryDate = null;
            Start = null;

            MerchantNumber = null;
            TerminalId = null;
            SchemeName = null;
            EFTSequenceNumber = null;
            AuthorisationCode = null;
            VRTelNo = null;
            Message = null;

            CardInputMethod = null;
            AccountOnFileRegistrationResult = AccountOnFileRegistrationResultEnum.NotSet;
            TokenId = null;
            CardNumberHash = null;

            AVSPostCodeResult = DataCheckResultEnum.Unknown;
            AVSHouseNumberResult = DataCheckResultEnum.Unknown;
            CSCResult = DataCheckResultEnum.Unknown;
        }

        public override void RebuildPayload()
        {
            string cardMethod = "";
            if (CardInputMethod.HasValue)
            {
                switch (CardInputMethod.Value)
                {
                    case Core.CardInputMethod.ICC:
                        cardMethod = "ICC";
                        break;
                    case Core.CardInputMethod.KeyedIn:
                        cardMethod = "Keyed";
                        break;
                    case Core.CardInputMethod.Swiped:
                        cardMethod = "Swipe";
                        break;
                    default:
                        throw new Exception("Unknown card input method for transaction response");
                }
            }
            this.Payload = string.Format("{0},{1},{2:0.00},{3:0.00},{4:0.00},{5},{6:MMyy},{7},{8:MMyy},{9},{10},{11:D8},{12},{13},{14},{15},{16},{17},{18},826,,,,,,,{19},{20},{21},{22},{23},{24}",
                (int)Result, TerminateLoop, TotalTransactionValueProcessed, 0/*cashback*/, 0/*gratuity*/, PAN,
                ExpiryDate, ""/*issue number*/, Start, TransactionDateTime.ToString("yyyyMMddHHmmss")/*CCYYMMDDHHMMSS in documentation*/, MerchantNumber,
                TerminalId, SchemeName, ""/*floor limit*/, EFTSequenceNumber, AuthorisationCode,
                VRTelNo, Message, cardMethod, (int)AccountOnFileRegistrationResult, TokenId,
                (int)AVSPostCodeResult, (int)AVSHouseNumberResult, (int)CSCResult, CardNumberHash);
        }
    }
}
