﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class OfflineSubmissionResponse: Packet
    {
        public OfflineSubmissionResponse() :
            base()
        {
            this.Payload = "0,1,,,,,,,,,,,,,,,,No Offline Txns";
        }
    }
}
