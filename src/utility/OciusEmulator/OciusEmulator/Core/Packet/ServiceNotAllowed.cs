﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class ServiceNotAllowed: Packet
    {
        public ServiceNotAllowed() :
            base()
        {
            this.Payload = "-31,1,,,,,,,,,,,,,,,,SERVICE NOT ALLOWED";
        }
    }
}
