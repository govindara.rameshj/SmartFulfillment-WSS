﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class GetCardDetailsResponse : Packet
    {
        public enum ResultEnum
        {
            Success = 0,
            CardNotRecognised = -4,
            EFTMismatch = -5,
            InvalidRecord = -12,
            ProcessingFailed = -30,
            OperationModeNotSupported = -70,
            UserNotLoggedIn = -85,
            ServiceNotAllowed = -90,
            RetrievalCancelled = -99,
            TimeoutWaitingForCard = -135,
        }

        public ResultEnum Result;
        public string CardData;
        public string SchemeName;
        public string Hash;

        public GetCardDetailsResponse() :
            base()
        {
            Result = ResultEnum.Success;
            CardData = null;
            SchemeName = null;
            Hash = null;
        }

        public override void RebuildPayload()
        {
            this.Payload = string.Format("{0},{1},{2}", (int)Result, CardData, SchemeName);
            if (Hash != null)
                this.Payload += "," + Hash;
        }
    }
}
