﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class LoginRequest: Packet
    {
        public string UserID;
        public string PIN;

        public override void Parse(string[] parts)
        {
            base.Parse(parts);

            UserID = parts[1];
            PIN = parts[2];
        }
    }
}
