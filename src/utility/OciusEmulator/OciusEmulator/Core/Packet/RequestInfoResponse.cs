﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class RequestInfoResponse : Packet
    {
        public RequestInfoResponse(string version) :
            base()
        {
            this.Payload = string.Format("0,1,,,,,,,,,,,,,,,,{0}", version);
        }
    }
}
