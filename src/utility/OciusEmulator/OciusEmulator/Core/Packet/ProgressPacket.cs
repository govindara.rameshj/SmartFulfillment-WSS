﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core.Packet
{
    class ProgressPacket: Packet
    {
        private static Dictionary<int,string> statusMappings;
        static ProgressPacket()
        {
            statusMappings = new Dictionary<int, string>();
            statusMappings.Add(0, "Processing Transaction");
            statusMappings.Add(1, "Waiting For Gratuity");
            statusMappings.Add(2, "Gratuity Being Entered");
            statusMappings.Add(3, "Awaiting Card");
            statusMappings.Add(4, "Swipe Card");
            statusMappings.Add(5, "Card Inserted");
            statusMappings.Add(6, "Card Removed");
            statusMappings.Add(7, "Card Processing");
            statusMappings.Add(8, "Change Card");
            statusMappings.Add(9, "Contact Transaction Required");
            statusMappings.Add(10, "Key In Card Details");
            statusMappings.Add(11, "Waiting For Cashback");
            statusMappings.Add(12, "Pin Entry");
            statusMappings.Add(13, "Risk Management Complete");
            statusMappings.Add(14, "Authorising Transaction");
            statusMappings.Add(15, "Waiting For Result");
            statusMappings.Add(16, "Auth Result Received");
            statusMappings.Add(17, "Printing Merchant Receipt");
            statusMappings.Add(18, "Signature Confirmation Required");
            statusMappings.Add(19, "Continue Required");
            statusMappings.Add(20, "Confirm Auth Code");
            statusMappings.Add(21, "Confirming Transaction");
            statusMappings.Add(22, "Rejecting Transaction");
            statusMappings.Add(23, "Final Result Received");
            statusMappings.Add(24, "Voice Referral");
            statusMappings.Add(25, "Remove Card");
            statusMappings.Add(26, "Auth Result Error");
            statusMappings.Add(27, "Fallback To Swipe Disabled");
            statusMappings.Add(28, "Downloading File");
            statusMappings.Add(29, "Updating PED");
            statusMappings.Add(30, "Invalid PED Configuration");
            statusMappings.Add(31, "Card Data Retrieval");
            statusMappings.Add(32, "Starting Transaction");
            statusMappings.Add(33, "Performing Download");
            statusMappings.Add(34, "Requesting Report");
            statusMappings.Add(35, "Gratuity Selection Required");
            statusMappings.Add(36, "Expiry Date Required");
            statusMappings.Add(37, "Start Date Required");
            statusMappings.Add(38, "Issue Number Required");
            statusMappings.Add(39, "AVS House Number Required");
            statusMappings.Add(40, "AVS Post Code Required");
            statusMappings.Add(41, "CSC Required");
            statusMappings.Add(42, "Customer Present / Not Present Selection Required");
            statusMappings.Add(43, "Customer / Not Present Option Selection Required");
            statusMappings.Add(44, "Enter Charge Auth Code");
            statusMappings.Add(45, "LoginRequired");
            statusMappings.Add(46, "Ready");
            statusMappings.Add(47, "Card Not Accepted");
            statusMappings.Add(48, "Card Blocked");
            statusMappings.Add(49, "Transaction Cancelled");
            statusMappings.Add(50, "Invalid Expiry");
            statusMappings.Add(51, "Gratuity Invalid");
            statusMappings.Add(52, "Invalid Card");
            statusMappings.Add(53, "Printing Customer Receipt");
            statusMappings.Add(54, "Initialising PED");
            statusMappings.Add(55, "PED Unavailable");
            statusMappings.Add(56, "Card Application Selection");
            statusMappings.Add(57, "Retry Download");
            statusMappings.Add(58, "Restart After Software Update");
            statusMappings.Add(59, "Requesting DCC");
            statusMappings.Add(60, "DCC Currency Choice");
            statusMappings.Add(61, "Cardholder DCC Currency Choice");
            statusMappings.Add(62, "Unsafe Download");
            statusMappings.Add(63, "Unexpected Login");
            statusMappings.Add(64, "Start Barclays Bonus Transaction");
            statusMappings.Add(65, "Update Barclays Bonus Transaction");
            statusMappings.Add(66, "Cancel Barclays Bonus Transaction");
            statusMappings.Add(67, "Confirm Gratuity");
            statusMappings.Add(68, "Register For Account On File Decision");
            statusMappings.Add(69, "Awaiting Token ID");
            statusMappings.Add(70, "Barclays Bonus Discount Summary");
            statusMappings.Add(71, "Barclays Bonus Use Bonus");
            statusMappings.Add(72, "Barclays Bonus Enter Redemption");
            statusMappings.Add(73, "Barclays Bonus Not Available");
            statusMappings.Add(74, "Download Complete");
            statusMappings.Add(75, "Download Still Being Prepared");
            statusMappings.Add(76, "Server Connection Failed");
            statusMappings.Add(77, "Resume Download");
            statusMappings.Add(78, "PayPoint Account Extraction Failed");
            statusMappings.Add(79, "PayPoint Amount Outside Allowed Range");
            statusMappings.Add(80, "PayPoint Card Expired");
            statusMappings.Add(81, "PayPoint Initialised");
            statusMappings.Add(82, "PayPoint Initialisation Failed");
            statusMappings.Add(83, "PayPoint Initialising");
            statusMappings.Add(84, "PayPoint Invalid Account");
            statusMappings.Add(85, "PayPoint Invalid Amount");
            statusMappings.Add(86, "PayPoint Invalid Capture Method");
            statusMappings.Add(87, "PayPoint Invalid Card Number");
            statusMappings.Add(88, "PayPoint Invalid Configuration");
            statusMappings.Add(89, "PayPoint Invalid Denomination");
            statusMappings.Add(90, "PayPoint Invalid Expiry Date");
            statusMappings.Add(91, "PayPoint Invalid Scheme");
            statusMappings.Add(92, "PayPoint Invalid Scheme Option");
            statusMappings.Add(93, "PayPoint Invalid Top-up Type");
            statusMappings.Add(94, "PayPoint Invalid Service Provider");
            statusMappings.Add(95, "PayPoint Invalid Track2 Format");
            statusMappings.Add(96, "PayPoint Invalid Transaction Type");
            statusMappings.Add(97, "PayPoint Keyed Entry Not Allowed");
            statusMappings.Add(98, "PayPoint Merchant Reference Required");
            statusMappings.Add(99, "PayPoint No Accounts");
            statusMappings.Add(100, "PayPoint Processing Transaction");
            statusMappings.Add(101, "PayPoint Retry Confirmation Decision");
            statusMappings.Add(102, "PayPoint Scheme Not Recognised");
            statusMappings.Add(103, "PayPoint Transaction Cancelled");
            statusMappings.Add(104, "PayPoint Transaction Type Not Allowed");
            statusMappings.Add(105, "PayPoint Select Scheme Option");
            statusMappings.Add(106, "PayPoint Download Required");
            statusMappings.Add(107, "PayPoint Select Account");
            statusMappings.Add(108, "Printing PayPoint Receipt");
            statusMappings.Add(109, "Licence Detail Confirmation");
            statusMappings.Add(110, "Licence File Required");
            statusMappings.Add(111, "Pay Point Service Unavailable");
            statusMappings.Add(112, "Park Retail Gift Account Extraction Failed");
            statusMappings.Add(113, "Park Retail Gift Amount Outside Allowed Range");
            statusMappings.Add(114, "Park Retail Gift Card Expired");
            statusMappings.Add(115, "Park Retail Gift Initialisation Failed");
            statusMappings.Add(116, "Park Retail Gift Initialising");
            statusMappings.Add(117, "Park Retail Gift Invalid Account");
            statusMappings.Add(118, "Park Retail Gift Invalid Amount");
            statusMappings.Add(119, "Park Retail Gift Invalid Capture Method");
            statusMappings.Add(120, "Park Retail Gift Invalid Card Number");
            statusMappings.Add(121, "Park Retail Gift Invalid Configuration");
            statusMappings.Add(122, "Park Retail Gift Invalid Expiry Date");
            statusMappings.Add(123, "Park Retail Gift Invalid Track2 Format");
            statusMappings.Add(124, "Park Retail Gift Invalid Transaction Type");
            statusMappings.Add(125, "Park Retail Gift Keyed Entry Not Allowed");
            statusMappings.Add(126, "Park Retail Gift Merchant Reference Required");
            statusMappings.Add(127, "Park Retail Gift No Accounts");
            statusMappings.Add(128, "Park Retail Gift Service Unavailable");
            statusMappings.Add(129, "Park Retail Gift Processing Transaction");
            statusMappings.Add(131, "Park Retail Gift Scheme Not Recognised");
            statusMappings.Add(132, "Park Retail Gift Select Account");
            statusMappings.Add(133, "Park Retail Gift Transaction Cancelled");
            statusMappings.Add(134, "Park Retail Gift Transaction Type Not Allowed");
            statusMappings.Add(135, "Printing Park Retail Gift Receipt");
            statusMappings.Add(136, "PED In ESD Recovery Mode2");
            statusMappings.Add(137, "Update Transaction Value Decision");
            statusMappings.Add(138, "Update Barclaycard Freedom Config");
            statusMappings.Add(139, "Processing Key Exchange");
            statusMappings.Add(140, "Barclays Gift Initialising");
            statusMappings.Add(141, "Global Blue Tax Free Shopping Initialising");
            statusMappings.Add(142, "GiveX Initialising");
            statusMappings.Add(143, "M-Voucher Initialising");
            statusMappings.Add(144, "Performing Post Confirm Reversal");
            statusMappings.Add(145, "Invalid Amount");
            statusMappings.Add(146, "Merchant Reference Required");
            statusMappings.Add(147, "Merchant Reference Length Invalid");
            statusMappings.Add(148, "Initialising");
            statusMappings.Add(149, "Account Extraction Failed");
            statusMappings.Add(150, "Amount Outside Allowed Range");
            statusMappings.Add(151, "Enter Amount");
            statusMappings.Add(152, "Confirm Authorisation");
            statusMappings.Add(153, "Card Expired");
            statusMappings.Add(154, "Transax SV Initialisation Failed");
            statusMappings.Add(155, "Transax SV Initialising");
            statusMappings.Add(156, "Invalid Account");
            statusMappings.Add(157, "Invalid Capture Method");
            statusMappings.Add(158, "Invalid Card Number");
            statusMappings.Add(159, "Invalid Configuration");
            statusMappings.Add(160, "Invalid Expiry Date");
            statusMappings.Add(161, "Invalid Track2 Format");
            statusMappings.Add(162, "Invalid Transaction Type");
            statusMappings.Add(163, "Keyed Entry Not Allowed");
            statusMappings.Add(164, "No Accounts");
            statusMappings.Add(165, "Card Scheme Not Recognised");
            statusMappings.Add(166, "Transaction Type Not Allowed");
            statusMappings.Add(167, "Transax SV Service Unavailable");
            statusMappings.Add(168, "Invalid Issue Number");
            statusMappings.Add(169, "Invalid Card Security Code");
            statusMappings.Add(170, "Confirmation Failed");
            statusMappings.Add(171, "Printing Receipt");
            statusMappings.Add(172, "Waiting For Donation");
            statusMappings.Add(173, "Pin Blocked");
            statusMappings.Add(174, "Pin Try Limit Exceeded");
            statusMappings.Add(175, "Further Payment Required");
            statusMappings.Add(176, "SVS Initialising");
            statusMappings.Add(177, "SVS Initialisation Failed");
            statusMappings.Add(178, "SVS Invalid Configuration");
            statusMappings.Add(179, "SVS Service Unavailable");
            statusMappings.Add(180, "Cannot Continue Transaction");
            statusMappings.Add(181, "Part Payment");
            statusMappings.Add(182, "Obtain STAN");
            statusMappings.Add(183, "Invalid STAN");
            statusMappings.Add(184, "IVR Required");
            statusMappings.Add(185, "Prompt For Authorisation Code");
            statusMappings.Add(186, "Invalid Auth Code");
            statusMappings.Add(187, "Offline Txn Failed");
            statusMappings.Add(188, "Enter Reference");
            statusMappings.Add(189, "Print Shop Copy");
            statusMappings.Add(190, "Must Insert Card");
            statusMappings.Add(191, "PIN Entered");
            statusMappings.Add(192, "Card Swiped");
            statusMappings.Add(193, "PIN Bypassed");
            statusMappings.Add(194, "Cancelling Transaction");
            statusMappings.Add(195, "Card Presented");
            statusMappings.Add(196, "Server Error");
            statusMappings.Add(197, "On Hold Transaction");
            statusMappings.Add(211, "Unknown Progress Message");
            statusMappings.Add(212, "Failed To Read Card");


/*
statusMappings.Add(0, "Processing Transaction");
statusMappings.Add(1, "Waiting For Gratuity");
statusMappings.Add(2, "Gratuity Being Entered");
statusMappings.Add(3, "Awaiting Card");
statusMappings.Add(4, "Swipe Card");
statusMappings.Add(5, "Card Inserted");
statusMappings.Add(6, "Card Removed");
statusMappings.Add(7, "Card Processing");
statusMappings.Add(8, "Change Card");
statusMappings.Add(9, "Contact Transaction Required");
statusMappings.Add(10, "Key In Card Details");
statusMappings.Add(11, "Waiting For Cashback");
statusMappings.Add(12, "Pin Entry");
statusMappings.Add(13, "Risk Management Complete");
statusMappings.Add(14, "Authorising Transaction");
statusMappings.Add(15, "Waiting For Result");
statusMappings.Add(16, "Auth Result Received");
statusMappings.Add(17, "Printing Merchant Receipt");
statusMappings.Add(18, "Signature Confirmation Required");
statusMappings.Add(19, "Continue Required");
statusMappings.Add(20, "Confirm Auth Code");
statusMappings.Add(21, "Confirming Transaction");
statusMappings.Add(22, "Rejecting Transaction");
statusMappings.Add(23, "Final Result Received");
statusMappings.Add(24, "Voice Referral");
statusMappings.Add(25, "Remove Card");
statusMappings.Add(26, "Auth Result Error");
statusMappings.Add(27, "Fallback To Swipe Disabled");
statusMappings.Add(28, "Downloading File");
statusMappings.Add(29, "Updating PED");
statusMappings.Add(30, "Invalid PED Configuration");
statusMappings.Add(31, "Card Data Retrieval");
statusMappings.Add(32, "Starting Transaction");
statusMappings.Add(33, "Performing Download");
statusMappings.Add(34, "Requesting Report");
statusMappings.Add(35, "Gratuity Selection Required");
statusMappings.Add(36, "Expiry Date Required");
statusMappings.Add(37, "Start Date Required");
statusMappings.Add(38, "Issue Number Required");
statusMappings.Add(39, "AVS House Number Required");
statusMappings.Add(40, "AVS Post Code Required");
statusMappings.Add(41, "CSC Required");
statusMappings.Add(42, "Customer Present / Not Present Selection Required");
statusMappings.Add(43, "Customer / Not Present Option Selection Required");
statusMappings.Add(44, "Enter Charge Auth Code");
statusMappings.Add(45, "Login Required");
statusMappings.Add(46, "Ready");
statusMappings.Add(47, "Card Not Accepted");
statusMappings.Add(48, "Card Blocked");
statusMappings.Add(49, "Transaction Cancelled");
statusMappings.Add(50, "Invalid Expiry");
statusMappings.Add(51, "Gratuity Invalid");
statusMappings.Add(52, "Invalid Card");
statusMappings.Add(53, "Printing Customer Receipt");
statusMappings.Add(54, "Initialising PED");
statusMappings.Add(55, "PED Unavailable");
statusMappings.Add(56, "Card Application Selection");
statusMappings.Add(57, "Retry Download");
statusMappings.Add(58, "Restart After Software Update");
statusMappings.Add(59, "Requesting DCC");
statusMappings.Add(60, "DCC Currency Choice");
statusMappings.Add(61, "Cardholder DCC Currency Choice");
statusMappings.Add(62, "Unsafe Download");
statusMappings.Add(63, "Unexpected Login");
statusMappings.Add(64, "Start Barclays Bonus Transaction");
statusMappings.Add(65, "Update Barclays Bonus Transaction");
statusMappings.Add(66, "Cancel Barclays Bonus Transaction");
statusMappings.Add(67, "Confirm Gratuity");
statusMappings.Add(68, "Register For Account On File Decision");
statusMappings.Add(69, "Awaiting Token ID");
statusMappings.Add(70, "Barclays Bonus Discount Summary");
statusMappings.Add(71, "Barclays Bonus Use Bonus");
statusMappings.Add(72, "Barclays Bonus Enter Redemption");
statusMappings.Add(73, "Barclays Bonus Not Available");
statusMappings.Add(74, "Download Complete");
statusMappings.Add(75, "Download Still Being Prepared");
statusMappings.Add(76, "Server Connection Failed");
statusMappings.Add(77, "Resume Download");
statusMappings.Add(78, "PayPoint Account Extraction Failed");
statusMappings.Add(79, "PayPoint Amount Outside Allowed Range");
statusMappings.Add(80, "PayPoint Card Expired");
statusMappings.Add(81, "PayPoint Initialised");
statusMappings.Add(82, "PayPoint Initialisation Failed");
statusMappings.Add(83, "PayPoint Initialising");
statusMappings.Add(84, "PayPoint Invalid Account");
statusMappings.Add(85, "PayPoint Invalid Amount");
statusMappings.Add(86, "PayPoint Invalid Capture Method");
statusMappings.Add(87, "PayPoint Invalid Card Number");
statusMappings.Add(88, "PayPoint Invalid Configuration");
statusMappings.Add(89, "PayPoint Invalid Denomination");
statusMappings.Add(90, "PayPoint Invalid Expiry Date");
statusMappings.Add(91, "PayPoint Invalid Scheme");
statusMappings.Add(92, "PayPoint Invalid Scheme Option");
statusMappings.Add(93, "PayPoint Invalid Top-up Type");
statusMappings.Add(94, "PayPoint Invalid Service Provider");
statusMappings.Add(95, "PayPoint Invalid Track2 Format");
statusMappings.Add(96, "PayPoint Invalid Transaction Type");
statusMappings.Add(97, "PayPoint Keyed Entry Not Allowed");
statusMappings.Add(98, "PayPoint Merchant Reference Required");
statusMappings.Add(99, "PayPoint No Accounts");
statusMappings.Add(100, "PayPoint Processing Transaction");
statusMappings.Add(101, "PayPoint Retry Confirmation Decision");
statusMappings.Add(102, "PayPoint Scheme Not Recognised");
statusMappings.Add(103, "PayPoint Transaction Cancelled");
statusMappings.Add(104, "PayPoint Transaction Type Not Allowed");
statusMappings.Add(105, "PayPoint Select Scheme Option");
statusMappings.Add(106, "PayPoint Download Required");
statusMappings.Add(107, "PayPoint Select Account");
statusMappings.Add(108, "Printing PayPoint Receipt");
statusMappings.Add(109, "Licence Detail Confirmation");
statusMappings.Add(110, "Licence File Required");
statusMappings.Add(111, "Pay Point Service Unavailable");
statusMappings.Add(112, "Park Retail Gift Account Extraction Failed");
statusMappings.Add(113, "Park Retail Gift Amount Outside Allowed Range");
statusMappings.Add(114, "Park Retail Gift Card Expired");
statusMappings.Add(115, "Park Retail Gift Initialisation Failed");
statusMappings.Add(116, "Park Retail Gift Initialising");
statusMappings.Add(117, "Park Retail Gift Invalid Account");
statusMappings.Add(118, "Park Retail Gift Invalid Amount");
statusMappings.Add(119, "Park Retail Gift Invalid Capture Method");
statusMappings.Add(120, "Park Retail Gift Invalid Card Number");
statusMappings.Add(121, "Park Retail Gift Invalid Configuration");
statusMappings.Add(122, "Park Retail Gift Invalid Expiry Date");
statusMappings.Add(123, "Park Retail Gift Invalid Track2 Format");
statusMappings.Add(124, "Park Retail Gift Invalid Transaction Type");
statusMappings.Add(125, "Park Retail Gift Keyed Entry Not Allowed");
statusMappings.Add(126, "Park Retail Gift Merchant Reference Required");
statusMappings.Add(127, "Park Retail Gift No Accounts");
statusMappings.Add(128, "Park Retail Gift Service Unavailable");
statusMappings.Add(129, "Park Retail Gift Processing Transaction");
statusMappings.Add(131, "Park Retail Gift Scheme Not Recognised");
statusMappings.Add(132, "Park Retail Gift Select Account");
statusMappings.Add(133, "Park Retail Gift Transaction Cancelled");
statusMappings.Add(134, "Park Retail Gift Transaction Type Not Allowed");
statusMappings.Add(135, "Printing Park Retail Gift Receipt");
statusMappings.Add(136, "PED In ESD Recovery Mode2");
statusMappings.Add(137, "Update Transaction Value Decision");
statusMappings.Add(138, "Update Barclaycard Freedom Config");
statusMappings.Add(139, "Processing Key Exchange");
statusMappings.Add(140, "Barclays Gift Initialising");
statusMappings.Add(141, "Global Blue Tax Free Shopping Initialising");
statusMappings.Add(142, "GiveX Initialising");
statusMappings.Add(143, "M-Voucher Initialising");
statusMappings.Add(144, "Performing Post Confirm Reversal");
statusMappings.Add(145, "Invalid Amount");
statusMappings.Add(146, "Merchant Reference Required");
statusMappings.Add(147, "Merchant Reference Length Invalid");
statusMappings.Add(148, "Initialising");
statusMappings.Add(149, "Account Extraction Failed");
statusMappings.Add(150, "Amount Outside Allowed Range");
statusMappings.Add(151, "Enter Amount");
statusMappings.Add(152, "Confirm Authorisation");
statusMappings.Add(153, "Card Expired");
statusMappings.Add(154, "Transax SV Initialisation Failed");
statusMappings.Add(155, "Transax SV Initialising");
statusMappings.Add(156, "Invalid Account");
statusMappings.Add(157, "Invalid Capture Method");
statusMappings.Add(158, "Invalid Card Number");
statusMappings.Add(159, "Invalid Configuration");
statusMappings.Add(160, "Invalid Expiry Date");
statusMappings.Add(161, "Invalid Track2 Format");
statusMappings.Add(162, "Invalid Transaction Type");
statusMappings.Add(163, "Keyed Entry Not Allowed");
statusMappings.Add(164, "No Accounts");
statusMappings.Add(165, "Card Scheme Not Recognised");
statusMappings.Add(166, "Transaction Type Not Allowed");
statusMappings.Add(167, "Transax SV Service Unavailable");
statusMappings.Add(168, "Invalid Issue Number");
statusMappings.Add(169, "Invalid Card Security Code");
statusMappings.Add(170, "Confirmation Failed");
statusMappings.Add(171, "Printing Receipt");
statusMappings.Add(172, "Waiting For Donation");
statusMappings.Add(173, "Pin Blocked");
statusMappings.Add(174, "Pin Try Limit Exceeded");
statusMappings.Add(175, "Further Payment Required");
statusMappings.Add(176, "SVS Initialising");
statusMappings.Add(177, "SVS Initialisation Failed");
statusMappings.Add(178, "SVS Invalid Configuration");
statusMappings.Add(179, "SVS Service Unavailable");
statusMappings.Add(180, "Cannot Continue Transaction");
statusMappings.Add(181, "Part Payment");
statusMappings.Add(182, "Obtain STAN");
statusMappings.Add(183, "Invalid STAN");
statusMappings.Add(184, "IVR Required");
statusMappings.Add(185, "Prompt For Authorisation Code");
statusMappings.Add(186, "Invalid Auth Code");
statusMappings.Add(187, "Offline Txn Failed");
statusMappings.Add(188, "Enter Reference");
statusMappings.Add(189, "Print Shop Copy");
statusMappings.Add(190, "Must Insert Card");
statusMappings.Add(191, "PIN Entered");
statusMappings.Add(192, "Card Swiped");
statusMappings.Add(193, "PIN Bypassed");
statusMappings.Add(194, "Cancelling Transaction");
statusMappings.Add(195, "Card Presented");
statusMappings.Add(196, "Server Error");
statusMappings.Add(197, "On Hold Transaction");
*/
        }

        public ProgressPacket(int result, int statusId, Dictionary<string, string> parameters)
        {
            this.Payload = string.Format("{0},0,{1},{2},{3}", result, statusId, statusMappings[statusId].Replace(" ", ""), BuildParameters(parameters));
        }

        public ProgressPacket(int result, int statusId):
            this(result, statusId, null)
        {
        }
    }
}
