﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OciusEmulator.Core
{
    public enum StatusEnum
    {
        LoginRequired,
        Ready,
        StartingTransaction,
        AwaitingCard,
        CardInserted,
        CardProcessing,
        PinEntry,
        PinEntered,
        RiskManagementComplete,
        AuthorisingTxn,
        WaitingForResult,
        AuthResultReceived,
        RemoveCard,
        CardRemoved,
        ConfirmingTxn,
        FinalResultReceived,
        PrintingCustomerReceipt,
        PrintingMerchantReceipt,
        ContinueRequired,
        CancellingTransaction,
        TransactionCancelled,
        ProcessingTxn,
        InitialisingPED,
        PinTryLimitExceeded,

        KeyInCardDetails,
        ExpiryDateRequired,
        CSCRequired,
        AVSHouseNumberRequired,
        AVSPostCodeRequired,

        SignatureConfirmationRequired,
        CardSwiped,
        MustInsertCard,
        SwipeCard,
        CardDataRetrieval,
        RejectingTxn,
        ConfirmAuthCode,
        VoiceReferral,

        UnknownProgressMessage,
        FailedToReadCard
    }
}
