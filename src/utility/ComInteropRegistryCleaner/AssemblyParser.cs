﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ComInteropRegistryCleaner
{
    /// <summary>
    /// Gets all nessesary information from assemblies.
    /// </summary>
    public class AssemblyParser
    {
        /// <summary>
        /// Extract ProgIds from assembly for visible types.
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public IList<string> GetProgIds(Assembly assembly)
        {
            var result = new List<string>();

            bool defaultVisibility = GetComVisibility(assembly);

            foreach (Type type in assembly.GetTypes())
            {
                bool isComVisible = GetComVisibility(type, defaultVisibility);
                if (isComVisible)
                {
                    var progIdAttributes = type.GetCustomAttributes(typeof(ProgIdAttribute), true).Cast<ProgIdAttribute>();
                    if (progIdAttributes.Any())
                    {
                        result.Add(progIdAttributes.First().Value);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Extract Guids from assembly for visible interfaces.
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public IList<string> GetGuids(Assembly assembly)
        {
            var result = new List<string>();

            bool defaultVisibility = GetComVisibility(assembly);

            foreach (Type type in assembly.GetTypes())
            {
                bool isComVisible = GetComVisibility(type, defaultVisibility);
                if (isComVisible)
                {
                    if (type.IsInterface)
                    {
                        var guidAttributes = type.GetCustomAttributes(typeof(GuidAttribute), true).Cast<GuidAttribute>();
                        if (guidAttributes.Any())
                        {
                            result.Add(guidAttributes.First().Value); 
                        }
                    }
                }
            }
            return result;
        }

        public string GetTypeLibraryId(Assembly assembly)
        {
            return assembly.GetCustomAttributes(typeof(GuidAttribute), false)
                .Cast<GuidAttribute>().Select(x => x.Value).FirstOrDefault();
        }

        /// <summary>
        /// Get ComVisibility attribute.
        /// </summary>
        /// <param name="attributeProvider"></param>
        /// <returns></returns>
        private bool GetComVisibility(ICustomAttributeProvider attributeProvider, bool defaultValue = true)
        {
            bool visibility;
            var attributes = attributeProvider.GetCustomAttributes(typeof(ComVisibleAttribute), true).Cast<ComVisibleAttribute>();
            if (!attributes.Any())
            {
                visibility = defaultValue;
            }
            else
            {
                visibility = attributes.First().Value;
            }
            return visibility;
        }
    }
}
