﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace ComInteropRegistryCleaner
{
    /// <summary>
    /// Gets keys from registry and deletes them.
    /// </summary>
    public class ClassesRootRegistryHelper
    {
        /// <summary>
        /// Deletes progId and clsId keys from registry.
        /// </summary>
        /// <param name="progId"></param>
        public void CleanProgIdAndClsIdFromRegistry(string progId)
        {
            if (!String.IsNullOrEmpty(progId))
            {
                var clsId = GetRegistryValueByKey(progId + "\\CLSID"); //First of all, lets get clsId for our progid.
                DeleteRegistryKey(progId); //Next, lets delete ProgId dir.
                if (!String.IsNullOrEmpty(clsId))
                {
                    DeleteRegistryKey("CLSID\\" + clsId); //Delete ClsID dir.
                    DeleteRegistryKey("APPID\\" + clsId); //Also we need to clear up AppId directories with clsId value. 
                }
                else
                {
                    Console.WriteLine("Unable to find CLSID for PROGID = {0}", progId);
                }
            }
        }

        /// <summary>
        /// Deletes interface and typeLib keys from registry.
        /// </summary>
        /// <param name="guid"></param>
        public void CleanInterfaceAndTypeLibFromRegistry(string guid)
        {
            if (!String.IsNullOrEmpty(guid))
            {
                var typeLib = GetRegistryValueByKey("Interface\\{" + guid + "}\\TypeLib"); //Here we get typeLib id from interface directory.

                DeleteRegistryKey("Interface\\{" + guid + "}");    //Delete interface directory.
                if (!String.IsNullOrEmpty(typeLib))
                {
                    CleanTypeLibKeyFromRegistry(typeLib, true);
                }
                else
                {
                    Console.WriteLine("Unable to find TYPELIB for GUID = {0}", guid);
                }
            }
        }

        /// <summary>
        /// Deletes typeLib key from registry.
        /// </summary>
        /// <param name="guid"></param>
        public void CleanTypeLibKeyFromRegistry(string guid, bool isGuidWrapped = false)
        {
            if (!String.IsNullOrEmpty(guid))
            {
                string key = isGuidWrapped ? "TypeLib\\" + guid : String.Format("TypeLib\\{{{0}}}", guid);
                DeleteRegistryKey(key);
            }
        }

        /// <summary>
        /// Read value from Registry by key.
        /// </summary>
        /// <param name="registryPath">Path to the folder in registry.</param>
        /// <returns>Value from registry.</returns>
        public string GetRegistryValueByKey(string registryPath)
        {
            string value = null;

            using (var register = Registry.ClassesRoot.OpenSubKey(registryPath))
            {
                if (register != null)
                {
                    value = register.GetValue("").ToString();
                }
            }
            return value;
        }

        /// <summary>
        /// Deletes directory from registry with given path.
        /// </summary>
        /// <param name="registerPath"></param>
        /// <returns></returns>
        public void DeleteRegistryKey(string registerPath)
        {
            var exists = false;
            using (var register = Registry.ClassesRoot.OpenSubKey(registerPath))
            {
                if (register != null)
                {
                    exists = true;
                }
            }
            if (exists)
            {
                Registry.ClassesRoot.DeleteSubKeyTree(registerPath);
                Console.WriteLine("Key {0} has been successfully deleted from registry", registerPath);
            }
            else
            {
                Console.WriteLine("Key {0} doesn't exist. Maybe it was deleted before?", registerPath);
            }
        }
    }
}
