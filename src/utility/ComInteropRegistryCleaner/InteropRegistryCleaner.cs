﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ComInteropRegistryCleaner
{
    /// <summary>
    /// Gets information from assembly and clean registry.
    /// </summary>
    public class InteropRegistryCleaner
    {
        /// <summary>
        /// Main method in project with most logic in it.
        /// </summary>
        /// <param name="dllPath"></param>
        public void CleanRegistry(string dllPath)
        {
            var assembly = Assembly.LoadFile(dllPath);
            var assemblyParser = new AssemblyParser();
            var progIds = assemblyParser.GetProgIds(assembly);
            var interfaceGuids = assemblyParser.GetGuids(assembly);
            var registryHelper = new ClassesRootRegistryHelper();

            if (progIds.Any())
            {
                foreach (var progId in progIds)
                {
                    registryHelper.CleanProgIdAndClsIdFromRegistry(progId);
                }
            }
            else
            {
                Console.WriteLine("Looks like dll you've provided doesn't contain any VB classes that need any registry cleanup.");
            }
            if (interfaceGuids.Any())
            {
                foreach (var guid in interfaceGuids)
                {
                    registryHelper.CleanInterfaceAndTypeLibFromRegistry(guid);
                }
            }
            else
            {
                Console.WriteLine("Looks like dll you've provided doesn't contain any VB interfaces that need any registry cleanup.");
            }

            string typeLibId = assemblyParser.GetTypeLibraryId(assembly);

            if (!String.IsNullOrEmpty(typeLibId))
            {
                registryHelper.CleanTypeLibKeyFromRegistry(typeLibId);
            }
            else
            {
                Console.WriteLine("Looks like dll you've provided does not have TypeLib guid.");
            }
        }
    }
}
