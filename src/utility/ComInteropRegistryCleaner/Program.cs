﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace ComInteropRegistryCleaner
{
    // TODO I can see 4 classes - Program, Entire Routine itself, parsing of Interop assembly separate methods for ProgIDs and interface IDs) and work with the registry, actually with ClassesRoot only, so it should be in the class name.
    public class Program
    {
        /// <summary>
        /// Entry point to the cleaning tool.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                if (args.Length != 1)
                {
                    Console.WriteLine("Provide path to dll you want unregister!");
                    return;
                }

                Console.WriteLine("\nStart processing dll: {0}", args[0]);
                var dllPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), args[0]);
                Console.WriteLine("Absolute path to dll: {0}", dllPath);

                if (File.Exists(dllPath))
                {
                    var cleaner = new InteropRegistryCleaner();
                    cleaner.CleanRegistry(dllPath);
                }
                else 
                {
                    Console.WriteLine("Where is no dll to handle at path: {0}", dllPath);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Application has thrown an exception: {0}", ex);
                return;
            }
        }
    }
}
