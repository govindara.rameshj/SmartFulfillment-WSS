﻿using System;
using System.IO;

namespace Base64Encoder
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Usage: Base64Encoder.exe <path_to_certificate>. You can also drag .p12 file on Base64Encoder.exe");
                return;
            }

            string originalFile = args[0];
            string destFile = originalFile + ".txt";

            string encodedText = Convert.ToBase64String(File.ReadAllBytes(originalFile));
            File.WriteAllText(destFile, encodedText);
            Console.WriteLine("Done: " + destFile);
            Console.ReadKey();
        }
    }
}
