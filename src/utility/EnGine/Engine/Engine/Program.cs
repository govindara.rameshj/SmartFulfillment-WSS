﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace Engine
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread thread = new Thread(Stub);
            thread.Start();
            InsertIntoLogFile();
        }

        private static void InsertIntoLogFile()
        {
            string path = @"C:\Program Files\Image Computer Systems\EnGine";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            File.Create(path + "\\Engine.LOG");
        }

        private static void Stub()
        {
            Thread.Sleep(3500);
        }
    }
}
