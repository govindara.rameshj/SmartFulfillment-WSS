# JDA Files Comparator

## Usage

Edit the following lines in `jda_files_comparator.rb`:

    folder1 = JDAFilesParser.new.parse('c:/Wix/COMMS/TORTI')
    folder2 = JDAFilesParser.new.parse('c:/Wix/COMMS/TORTI1')

Insert paths where your output folders are situated.

Run comparator with command:

    ruby jda_files_comparator.rb

Output will show which files are processed and in which lines you have mismatches (ig they are). To investigate the mismatches open appropriate files with WinMerge.