class JDAFilesParser
  def row_type(row)
    row[0...2]
  end

  def extract_field_model(str)
    data_regexp = /^DATA\+(\d+)\s+A(\d+)\s*:?([a-z]*)?\s+(.*)$/
    str
      .match(data_regexp)
      .captures
      .map { |e| e.strip unless e == nil }
  end

  def load_field_models(file)
    File
      .read(file)
      .split("\n")
      .map { |e| extract_field_model(e) }
  end

  def extract_field(str, field_model)
    str[field_model[0].to_i...field_model[0].to_i + field_model[1].to_i]
  end

  def extract_row(row, model_file)
    model = load_field_models(model_file)
    values = []
    model.each do |m|
      values << extract_field(row, m) unless m[2] == "false"
    end
    values
  end

  def file_type(filename)
    filename
      .match(/(ST[A-Z]+)\d+/)
      .captures
      .first
  end

  def file_list(dir)
    Dir.glob("#{dir}/ST*")
  end

  def parse(dir)
    files_content = {}
    file_list(dir).each do |f|
      if File.directory?("./models/#{file_type(f)}")
        puts "\nParsing #{f}"
        rows = File.read(f).split("\n")
        content = []
        rows.each do |r|
          model_file = "./models/#{file_type(f)}/#{row_type(r)}"
          if File.exists?(model_file)
            content << extract_row(r, model_file)        
          else
            puts "WARNING: Row type #{row_type(r)} for #{file_type(f)} wasn't described in models!!!"
          end
        end
        files_content[file_type(f)] = content
      else
        puts "\nWARNING: File #{file_type(f)} wasn't described in models!!!"
      end
    end
    files_content
  end
end
