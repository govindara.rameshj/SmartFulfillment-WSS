require_relative './jda_files_parser'

folder1 = JDAFilesParser.new.parse('c:/Wix/COMMS/TORTI')
folder2 = JDAFilesParser.new.parse('c:/Wix/COMMS/TORTI1')

folder1.each do |filename, file1|
  begin
    file2 = folder2.fetch(filename)
    puts "\nComparing #{filename} files"
    mismatch_counter = 0
    file1.each_with_index do |row, row_index|
      row.each_with_index do |col, col_index|
        unless col == file2[row_index][col_index]
          puts "ERROR: Mismatch in row #{row_index + 1} of #{filename}"
          puts "  >> #{col} != #{file2[row_index][col_index]}"
          mismatch_counter += 1
        end
      end
    end
    puts "Comparation finished, #{mismatch_counter} mismatch(es) found."
  rescue KeyError
    puts "WARNING: Cannot find pair for #{filename} !!!"
    next
  end
end
