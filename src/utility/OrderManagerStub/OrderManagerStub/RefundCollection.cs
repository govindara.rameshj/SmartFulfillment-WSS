﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderManagerStub
{
    public class RefundCollection
    {
        private static Dictionary<string, Refund> refunds = new Dictionary<string, Refund>();

        public static void AddRefund(Refund refund, bool concat = false)
        {
            if (concat)
            {
                if (refunds.ContainsKey(string.Concat(refund.OMOrderNumber.Value, refund.TargetFulfilmentSite)))
                    RemoveRefund(refund, true);
                refunds.Add(string.Concat(refund.OMOrderNumber.Value, refund.TargetFulfilmentSite), refund);
            }
            else
            {

                if (refunds.ContainsKey(refund.OMOrderNumber.Value))
                    refunds[refund.OMOrderNumber.Value] = refund;
                else
                    refunds.Add(refund.OMOrderNumber.Value, refund);
            }
        }

        public static void RemoveRefund(Refund refund, bool concat = false)
        {
            if (concat)
            {
                if (refunds.ContainsKey(string.Concat(refund.OMOrderNumber.Value, refund.TargetFulfilmentSite)))
                    refunds.Remove(string.Concat(refund.OMOrderNumber.Value, refund.TargetFulfilmentSite));
            }
            else
            {
                string key = refunds.FirstOrDefault(x => x.Key.Contains(refund.OMOrderNumber.Value)).Key;
                if (key != null)
                    refunds.Remove(key);
            }
        }

        public static Refund GetRefund(string refundNumber, string storeNumber = "")
        {
            string key = refunds.FirstOrDefault(x => x.Key.Equals(refundNumber + storeNumber)).Key;
            if (key != null)
                return refunds[key];
            else
                return null;
        }
    }
}