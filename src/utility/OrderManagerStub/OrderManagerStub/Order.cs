﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub
{
    public class Order : ICloneable
    {
        public Order()
        {
        }
        
        public string OMOrderNumber
        {
            get
            {
                if (OrderHeader.Elements("OMOrderNumber").Any())
                    return OrderHeader.Element("OMOrderNumber").Value;
                return null;
            }
            set
            {
                if (OrderHeader.Elements("OMOrderNumber").Any())
                    OrderHeader.Element("OMOrderNumber").Value = value;
                else
                    OrderHeader.Add(new XElement("OMOrderNumber", value));
            }
        }

        public string OrderStatus
        {
            get
            {
                if (OrderHeader.Elements("OrderStatus").Any())
                    return OrderHeader.Element("OrderStatus").Value;
                return null;
            }
            set
            {
                if (OrderHeader.Elements("OrderStatus").Any())
                    OrderHeader.Element("OrderStatus").Value = value;
                else
                    OrderHeader.Add(new XElement("OrderStatus", value));
            }
        }

        private string lineStatus;
        private XElement orderHeader;
        public XElement OrderHeader
        {
            get { return orderHeader; }
            set { orderHeader = value; }
        }

        private XElement orderLines;
        public XElement OrderLines
        {
            get { return orderLines; }
            set { orderLines = value; }
        }

        private XElement dateTimeStamp;
        public XElement DateTimeStamp
        {
            get { return dateTimeStamp; }
            set { dateTimeStamp = value; }
        }

        private XElement targetFulfilmentSite;
        public XElement TargetFulfilmentSite
        {
            get { return targetFulfilmentSite; }
            set { targetFulfilmentSite = value; }
        }

        private XElement sellingStoreOrderNumber;
        public XElement SellingStoreOrderNumber
        {
            get { return sellingStoreOrderNumber; }
            set { sellingStoreOrderNumber = value; }
        }

        public XElement FulfillingSiteOrderNumber { get; set; }

        public string LineStatus
        {
            get { return lineStatus; }
            set { lineStatus = value; }
        }

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        public bool OnlyNonSellingStoreSKU { get; set; }

        public string SellStore { get; set; }
    }
}