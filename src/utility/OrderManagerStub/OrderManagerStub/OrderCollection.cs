﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderManagerStub
{
    public static class OrderCollection
    {
        private static Dictionary<string, Order> orders = new Dictionary<string, Order>();

        public static void AddOrder(Order order, bool concat = false)
        {
            if (concat)
            {
                if (!orders.ContainsKey(String.Concat(order.OMOrderNumber, order.TargetFulfilmentSite.Value)))
                    orders.Add(String.Concat(order.OMOrderNumber,order.TargetFulfilmentSite.Value), order);
            }
            else
            {

                if (orders.ContainsKey(order.OMOrderNumber))
                    orders[order.OMOrderNumber] = order;
                else
                    orders.Add(order.OMOrderNumber, order);
            }
        }

        public static void RemoveOrder(Order order, bool concat = false)
        {
            if (concat)
            {
                if (orders.ContainsKey(String.Concat(order.OMOrderNumber, order.TargetFulfilmentSite.Value)))
                    orders.Remove(order.OMOrderNumber);
            }
            else
            {
                string key = orders.FirstOrDefault(x => x.Key.Equals(order.OMOrderNumber)).Key;
                orders.Remove(key);
            }
        }

        public static Order GetOrder(string orderNumber, string targetFulfilmentSite = "")
        {
            string key = orders.FirstOrDefault(x => x.Key.Contains(orderNumber + targetFulfilmentSite)).Key;
            if (key != null)
                return orders[key];
            else
                return null;
        }
    }
}