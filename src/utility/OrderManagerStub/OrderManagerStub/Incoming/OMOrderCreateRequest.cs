﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Incoming
{
    public class OMOrderCreateRequest : BaseIncomingRequest
    {
        public Order Order;
        public OMOrderCreateRequest(XElement requestElement)
        {
            var inner = ParseInner(requestElement, "OM_Order_CreateXMLInput");

            Order = new Order();
            Order.OrderLines = inner.Root.Element("OrderLines");
            Order.OrderHeader = inner.Root.Element("OrderHeader");
            Order.DateTimeStamp = inner.Root.Element("DateTimeStamp");
            Order.OMOrderNumber = DateTime.Now.ToString("HHmmssfff").PadLeft(10, '0');
        }
    }
}