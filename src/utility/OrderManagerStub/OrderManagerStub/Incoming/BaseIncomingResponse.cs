﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Incoming
{
    public class BaseIncomingResponse
    {
        protected XDocument doc;
        protected string respName;
        protected string xmlOutputName;
        protected BaseIncomingResponse() { }

        public BaseIncomingResponse(string respName, string xmlOutputName)
        {
            this.respName = respName;
            this.xmlOutputName = xmlOutputName;
            XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace ns1 = "http://orderManagerWebServices.tpplc.co.uk";
            doc = new XDocument(
                new XDeclaration("1.0", "utf-8", null),
                new XElement(soapenv + "Envelope", new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                                                    new XAttribute(XNamespace.Xmlns + "soapenc", "http://schemas.xmlsoap.org/soap/encoding/"),
                                                    new XAttribute(XNamespace.Xmlns + "xsd", "http://www.w3.org/2001/XMLSchema"),
                                                    new XAttribute(XNamespace.Xmlns + "soapenv", "http://schemas.xmlsoap.org/soap/envelope/"),
                    new XElement(soapenv + "Header"),
                    new XElement(soapenv + "Body", new XAttribute(XNamespace.Xmlns + "ns1", "http://orderManagerWebServices.tpplc.co.uk"),
                        new XElement(ns1 + this.respName, new XAttribute("xmlns", "http://orderManagerWebServices.tpplc.co.uk"),
                            new XElement(ns1 + this.xmlOutputName)
                                        )))
                );
        }

        protected XDocument CreateResponse(string responseString)
        {
            XNamespace prefixSoapenv = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace ns = "http://orderManagerWebServices.tpplc.co.uk";

            doc.Root.Element(prefixSoapenv + "Body").Elements().First().Elements().First().Value = responseString;
            return doc;
        }
    }
}