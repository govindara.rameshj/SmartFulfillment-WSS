﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Incoming
{
    public class CTSEnableRefundCreateResponse : BaseIncomingResponse
    {
         XDocument response;
         public CTSEnableRefundCreateResponse(Refund refund)
             : base("CTS_Enable_Refund_CreateResponse", "CTS_Enable_Refund_CreateXMLOutput")
        {
            SetLineStatus(refund);
           
           response = new XDocument(
                              new XDeclaration("1.0", "utf-8", "no"),
                              new XElement("OMOrderRefundResponse",
                                  new XElement(refund.DateTimeStamp),
                                  new XElement("OMOrderNumber", refund.OMOrderNumber),
                                  new XElement("OrderRefunds", 
                                       new XElement("OrderRefund",
                                           new XElement("SuccessFlag", true),
                                           new XElement(refund.RefundDate),
                                           new XElement(refund.RefundStoreCode),
                                           new XElement(refund.RefundTransaction),
                                           new XElement(refund.RefundTill),
                                           new XElement(refund.FulfilmentSites),
                                           new XElement(refund.RefundLines))
                                  ))
                  );

            response = CreateResponse(response.ToString());
        }

         private void SetLineStatus(Refund refund)
         {
             foreach (XElement line in refund.RefundLines.Elements("RefundLine"))
             {
                 line.Element("RefundLineStatus").Value = Convert.ToString(150);
             } 
         }

       public XDocument GetCTSEnableRefundCreate()
       {
           return response;
       }
    }
}