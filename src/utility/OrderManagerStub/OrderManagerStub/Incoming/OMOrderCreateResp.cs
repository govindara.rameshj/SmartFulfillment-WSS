﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using OrderManagerStub.Outgoing;

namespace OrderManagerStub.Incoming
{
    public class OMOrderCreateResp: BaseIncomingResponse
    {
        XDocument response;
        public OMOrderCreateResp(Order order)
            : base("OM_Order_CreateResponse", "OM_Order_CreateXMLOutput")
        {
            AddOMOrderLineNo(order);
            SetDeliveryChargeItem(order);
            SetSellingStoreLineNo(order);
           
           response = new XDocument(
                              new XDeclaration("1.0", "utf-8", null),
                              new XElement("OMOrderCreateResponse",
                                  new XElement(order.DateTimeStamp),
                                  new XElement("SuccessFlag", true),
                                  new XElement("OMOrderNumber", order.OMOrderNumber),
                                  new XElement(order.OrderHeader),
                                  new XElement(order.OrderLines)
                                  )
                  );

            response = CreateResponse(response.ToString());
        }

       public XDocument GetOMOrderCreate()
       {
           return response;
       }

       private void SetDeliveryChargeItem(Order order)
       {
           int linesCount = order.OrderLines.Elements("OrderLine").ToList().Count;
           foreach (XElement line in order.OrderLines.Elements("OrderLine"))
           {
               line.Element("DeliveryChargeItem").Value = "0";
           }   
       }

       private void SetSellingStoreLineNo(Order order)
       {
           foreach (XElement line in order.OrderLines.Elements("OrderLine"))
           {
               line.Element("SellingStoreLineNo").Value = Convert.ToInt64(line.Element("SellingStoreLineNo").Value).ToString();
           }     
       }

       private void AddOMOrderLineNo(Order order)
       {
           int linesCount = order.OrderLines.Elements("OrderLine").ToList().Count;
           int i = 1;
           foreach (XElement line in order.OrderLines.Elements("OrderLine"))
           {
               line.Add(new XElement("OMOrderLineNo", i));
               i++;
           }           
       }

    }
}