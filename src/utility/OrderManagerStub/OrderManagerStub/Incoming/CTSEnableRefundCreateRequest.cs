﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Incoming
{
    public class CTSEnableRefundCreateRequest: BaseIncomingRequest
    {
        public Refund Refund;
        public CTSEnableRefundCreateRequest(XElement requestElement)
        {
            var inner = ParseInner(requestElement, "CTS_Enable_Refund_CreateXMLInput");

            Refund = new Refund();
            Refund.DateTimeStamp = inner.Root.Element("DateTimeStamp");
            Refund.OMOrderNumber = inner.Root.Element("OMOrderNumber");

            XElement orderRefund = inner.Root.Element("OrderRefunds").Element("OrderRefund");

            Refund.RefundDate = orderRefund.Element("RefundDate");
            Refund.RefundStoreCode = orderRefund.Element("RefundStoreCode");
            Refund.RefundTransaction = orderRefund.Element("RefundTransaction");
            Refund.RefundTill = orderRefund.Element("RefundTill");
            Refund.FulfilmentSites = orderRefund.Element("FulfilmentSites");
            Refund.RefundLines = orderRefund.Element("RefundLines");
        }
    }
}