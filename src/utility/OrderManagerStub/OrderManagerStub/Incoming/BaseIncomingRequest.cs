﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Incoming
{
    public class BaseIncomingRequest
    {
        public static BaseIncomingRequest ParseRequest(string postBody)
        {
            var doc = XDocument.Parse(postBody, LoadOptions.None);
            XNamespace prefixSoap = "http://schemas.xmlsoap.org/soap/envelope/";

            var body = doc.Root.Element(prefixSoap + "Body");
            var element = (XElement)body.FirstNode;
            var requestName = element.Name.LocalName;
            switch (requestName)
            {
                case "OM_Check_Fulfilment":
                    return new CheckFulfilment(element);
                case "OM_Order_Create":
                    return new OMOrderCreateRequest(element);
                case "OM_Order_Refund":
                    return new OMRefundCreateRequest(element);
                default:
                    return null;
            }
        }

        protected XDocument ParseInner(XElement rootElement, string requestName)
        {
            XNamespace ns = "http://orderManagerWebServices.tpplc.co.uk";
            var inner = rootElement.Element(ns + requestName).Value;
            XDocument res = XDocument.Parse(inner);
            return res;
        }
    }
}