﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Incoming
{
    public class CheckFulfilment : BaseIncomingRequest
    {
        public string SellingStoreCode;
        public string SellingStoreOrderNumber;
        public DateTime RequiredDeliveryDate;

        public CheckFulfilment(XElement requestElement)
        {
            var inner = ParseInner(requestElement, "OM_Check_FulfilmentXMLInput");

            SellingStoreCode = inner.Root.Element("OrderHeader").Element("SellingStoreCode").Value;
        }
    }
}