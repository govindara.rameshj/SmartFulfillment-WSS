﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Xml.Linq;
using OrderManagerStub.Settings;
using log4net;

namespace OrderManagerStub
{
    public static class OutgoingProcessor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WebApiApplication));
        private static object lockObject = new object();
        private static MockSettingsSection settings;

        public enum ActionEnum
        {
            Fulfil,
            Notify,
            Update
        }

        public class OrderItem
        {
            public ActionEnum Action;
            public Order Order;
            public Refund Refund;

            public OrderItem(ActionEnum action, Order order)
            {
                this.Action = action;
                this.Order = order;
            }
        }

        public class RefundItem
        {
            public ActionEnum Action;
            public Refund Refund;

            public RefundItem(ActionEnum action, Refund refund)
            {
                this.Action = action;
                this.Refund = refund;
            }
        }

        private static List<OrderItem> OrderItemsToProcess;
        public static void AddOrderItem(OrderItem item)
        {
            lock (lockObject)
            {
                OrderItemsToProcess.Add(item);
            }
        }

        private static List<RefundItem> RefundItemsToProcess;
        public static void AddRefundItem(RefundItem item)
        {
            lock (lockObject)
            {
                RefundItemsToProcess.Add(item);
            }
        }


        private static System.Threading.Timer timer;
        static OutgoingProcessor()
        {
            settings = MockSettingsSection.Current;
            OrderItemsToProcess = new List<OrderItem>();
            RefundItemsToProcess = new List<RefundItem>();
            timer = new System.Threading.Timer(timerFired, new object(), 0, 10000);
        }

        private static List<String> GetOrderFulfilmentSites(Order order)
        {
            List<String> FulfilmentSites = new List<string>();
            order.SellStore = order.OrderHeader.Element("SellingStoreCode").Value;
            OrderManagerStub.Settings.MockSettingsSection.MappingItem mapItemFF = settings.GetByPostCode(order.OrderHeader.Element("DeliveryPostcode").Value);
            string fulfilmentSite = mapItemFF.StoreNumber;
            if (!order.SellStore.Equals(fulfilmentSite))
                FulfilmentSites.Add(fulfilmentSite);
            foreach (XElement line in order.OrderLines.Elements("OrderLine"))
            {
                if (!mapItemFF.SKU.Contains(line.Element("ProductCode").Value))
                    fulfilmentSite = settings.GetByPostCode("").StoreNumber;
                if (!FulfilmentSites.Contains(fulfilmentSite))
                    FulfilmentSites.Add(fulfilmentSite);
            }

            if (!FulfilmentSites.Contains(order.SellStore))
                order.OnlyNonSellingStoreSKU = true;

            return FulfilmentSites;
        }

        private static List<String> GetRefundFulfilmentSites(Refund refund)
        {

            List<String> FulfilmentSites = new List<string>();
            FulfilmentSites.Add(refund.RefundStoreCode.Value);
            Order order = OrderCollection.GetOrder(refund.OMOrderNumber.Value);
            order.SellStore = order.OrderHeader.Element("SellingStoreCode").Value;
            foreach (XElement line in order.OrderLines.Elements("OrderLine"))
            {
                if (!FulfilmentSites.Contains(line.Element("FulfilmentSite").Value))
                    FulfilmentSites.Add(line.Element("FulfilmentSite").Value);
            }
            return FulfilmentSites;
        }

        static void timerFired(object state)
        {
            lock (lockObject)
            {
                if (Controllers.OrderManagerController.noIncomingProcessing.WaitOne(0) == false)
                {
                    log.Warn("OutgoingProcessor: Incoming request is processing. Waiting for the end of the processing");
                    Controllers.OrderManagerController.noIncomingProcessing.WaitOne();
                    log.Warn("Done");
                }
                OrderProcessing();
                RefundProcessing();
            }
        }

        private static void OrderProcessing()
        {
            if (OrderItemsToProcess.Count == 0)
                return;

            var item = OrderItemsToProcess[0];
            int maxTries = 3;
            int tryNo = 1;
            while (tryNo <= maxTries)
            {
                try
                {
                    switch (item.Action)
                    {
                        case ActionEnum.Fulfil:
                            {
                                List<String> FulfilmentSites = GetOrderFulfilmentSites(item.Order);
                                Outgoing.CTSFulfilmentRequest req;
                                foreach (String ffsite in FulfilmentSites)
                                {
                                    req = new Outgoing.CTSFulfilmentRequest(item.Order, ffsite);
                                    req.Order.TargetFulfilmentSite.Value = ffsite;
                                    MakeRequest(req, ffsite);
                                }
                            }
                            break;
                        case ActionEnum.Notify:
                            {
                                Outgoing.CTSStatusNotificationRequest req = new Outgoing.CTSStatusNotificationRequest(item.Order);
                                MakeRequest(req, item.Order.TargetFulfilmentSite.Value);
                                if (item.Order.OnlyNonSellingStoreSKU)
                                {
                                    item.Order.TargetFulfilmentSite.Value = item.Order.SellStore;
                                    OrderCollection.AddOrder(item.Order, true);
                                    MakeRequest(req, item.Order.TargetFulfilmentSite.Value);
                                }
                            }
                            break;
                    }
                    OrderItemsToProcess.RemoveAt(0);
                    if (tryNo > 1)
                        log.InfoFormat("Item has been processed with try number {0}", tryNo);
                    break;
                }
                catch (Exception exc)
                {
                    log.Error("Failed to process item", exc);
                }

                System.Threading.Thread.Sleep(2000); // wait for a while before the next try
                tryNo++;
            }
        }

        private static void RefundProcessing()
        {
            if (RefundItemsToProcess.Count == 0)
                return;

            var item = RefundItemsToProcess[0];
            try
            {
                switch (item.Action)
                {
                    case ActionEnum.Update:
                        {
                            List<String> FulfilmentSites = GetRefundFulfilmentSites(item.Refund);
                            Outgoing.CTSUpdateRefundRequest req;
                            foreach (String ffsite in FulfilmentSites)
                            {
                                req = new Outgoing.CTSUpdateRefundRequest(item.Refund);
                                MakeRequest(req, ffsite);
                            }
                        }
                        break;
                    case ActionEnum.Notify:
                        {
                            Outgoing.CTSRefundStatusNotificationRequest req = new Outgoing.CTSRefundStatusNotificationRequest(item.Refund);
                            MakeRequest(req, item.Refund.TargetFulfilmentSite);
                        }
                        break;
                }
            }
            catch (Exception exc)
            {
                log.Error("Failed to process refund item", exc);
            }
            RefundItemsToProcess.RemoveAt(0);
        }

        private static void MakeRequest(Outgoing.BaseRequest req, String storeNumber)
        {
            var uri = new Uri(settings.GetByStoreNumber(storeNumber).CtsOrderManagerUrl);
            HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(uri);
            var contentToSend = req.XmlContent.ToString();
            log.InfoFormat("Sending request to {0}:\r\n{1}", httpRequest.RequestUri.ToString(), contentToSend);
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml; charset=utf-8";
            httpRequest.Host = uri.Host;
            httpRequest.Headers.Add("SOAPAction: " + req.SoapAction);
            var reqStream = new StreamWriter(httpRequest.GetRequestStream());
            reqStream.Write(contentToSend);
            reqStream.Close();

            log.Info("Getting response");
            var resp = httpRequest.GetResponse();
            var respReader = new StreamReader(resp.GetResponseStream());
            var contents = respReader.ReadToEnd().ToString();
            log.Info("Response retrieved:\r\n" + contents);
            var xmlResponse = Outgoing.BaseResponse.ParseResponse(contents);
            var inner = xmlResponse.InnerXml;
            if (req is Outgoing.CTSFulfilmentRequest)
            {
                if (!(xmlResponse is Outgoing.CTSFulfilmentResponse))
                    throw new Exception("Unexpected response type: " + xmlResponse.GetType().ToString());
                //response parsing                       
                Order order = ParseCTSFulfilmentResponse(xmlResponse);
                AddOrderItem(new OutgoingProcessor.OrderItem(OutgoingProcessor.ActionEnum.Notify, order));
            }
            else if (req is Outgoing.CTSStatusNotificationRequest)
            {
                if (!(xmlResponse is Outgoing.CTSStatusNotificationResponse))
                    throw new Exception("Unexpected response type: " + xmlResponse.GetType().ToString());
                //response parsing                    
                ParseCTSStatusNotificationResponse(xmlResponse, storeNumber);
            }
            else if (req is Outgoing.CTSUpdateRefundRequest)
            {
                if (!(xmlResponse is Outgoing.CTSUpdateRefundResponse))
                    throw new Exception("Unexpected response type: " + xmlResponse.GetType().ToString());
                Refund refund = ParseCTSUpdateRefundResponse(xmlResponse, storeNumber);
                AddRefundItem(new OutgoingProcessor.RefundItem(OutgoingProcessor.ActionEnum.Notify, refund));
            }
            else if (req is Outgoing.CTSRefundStatusNotificationRequest)
            {
                if (!(xmlResponse is Outgoing.CTSStatusNotificationResponse))
                    throw new Exception("Unexpected response type: " + xmlResponse.GetType().ToString());
                ParseCTSRefundStatusNotificationResponse(xmlResponse, storeNumber);
            }
        }

        private static Refund ParseCTSUpdateRefundResponse(Outgoing.BaseResponse xmlResponse, String storeNumber)
        {
            log.InfoFormat("Parsing {0} Response Start from Store {1}", xmlResponse.GetType().ToString(), storeNumber);
            string expectedStatus = "150";
            var inner = xmlResponse.InnerXml;
            XElement OrderRefund = inner.Root.Element("OrderRefunds").Element("OrderRefund");
            bool successFlag = Convert.ToBoolean(OrderRefund.Element("SuccessFlag").Value);              
            bool notRefundableFlag = Convert.ToBoolean(OrderRefund.Element("NotRefundableFlag").Value);
            string omOrderNumber = inner.Root.Element("OMOrderNumber").Value.Trim();
            Refund refund = RefundCollection.GetRefund(omOrderNumber).Clone() as Refund;
            if (refund == null)
                throw new Exception("Refund with coming OMOrderNumber was not found. OMOrderNumber: " + omOrderNumber);
            if (!successFlag || notRefundableFlag)
            {
                throw new Exception("Refund with coming OMOrderNumber was not fulfiled. OMOrderNumber: " + omOrderNumber);
            }

            foreach (XElement line in OrderRefund.Element("RefundLines").Elements("RefundLine"))
            {
                if (!line.Element("RefundLineStatus").Value.Equals(expectedStatus))
                    throw new Exception("Order or Lines status does not expected: " + expectedStatus);
            }
            log.InfoFormat("Parsing {0} Response Finish", xmlResponse.GetType().ToString());
            refund.TargetFulfilmentSite = storeNumber;
            refund.FulfilmentSites = OrderRefund.Element("FulfilmentSites");
            RefundCollection.AddRefund(refund, true);
            return refund;
        }

        private static Order ParseCTSFulfilmentResponse(Outgoing.BaseResponse xmlResponse)
        {
            log.InfoFormat("Parsing {0} Response Start", xmlResponse.GetType().ToString());
            var inner = xmlResponse.InnerXml;
            string expectedStatus = "300";
            if (inner.Root.Element("OrderHeader").Element("OMOrderNumber").Value.Trim().Equals("false"))
                expectedStatus = "499";
            string omOrderNumber = inner.Root.Element("OrderHeader").Element("OMOrderNumber").Value.Trim();
            Order order = OrderCollection.GetOrder(omOrderNumber).Clone() as Order;
            if (order == null)
                throw new Exception("Order with coming OMOrderNumber was not found. OMOrderNumber: " + omOrderNumber);
                        
            bool successFlag = Convert.ToBoolean(inner.Root.Element("SuccessFlag").Value.ToString());
            string targetFulfilmentSite = inner.Root.Element("TargetFulfilmentSite").Value.Trim();
            order.TargetFulfilmentSite = new XElement("TargetFulfilmentSite", targetFulfilmentSite);
            if (successFlag)
            {
                bool isOrderStatusValid = inner.Root.Element("OrderHeader").Element("OrderStatus").Value.Trim().Equals(expectedStatus);
                XElement orderLines = inner.Root.Element("OrderLines");
                bool isLinesStatusValid = xmlResponse.checkLinesStatus(orderLines, expectedStatus);

                if (!isLinesStatusValid && isOrderStatusValid)
                    throw new Exception("Order or Lines status does not expected: " + expectedStatus);
                int fulfilmentSiteOrderNumber = Convert.ToInt32(inner.Root.Element("FulfillingSiteOrderNumber").Value);
                order.SellingStoreOrderNumber = new XElement("SellingStoreOrderNumber", fulfilmentSiteOrderNumber);

                order.OrderLines = inner.Root.Element("OrderLines");
                order.OrderHeader = inner.Root.Element("OrderHeader");
                order.DateTimeStamp = inner.Root.Element("DateTimeStamp");
                order.FulfillingSiteOrderNumber = new XElement(inner.Root.Element("FulfillingSiteOrderNumber"));
                OrderCollection.AddOrder(order, true);
            }
            else
                throw new Exception("Order was not get for Fulfilling in store or unknown store was got in response " + targetFulfilmentSite);

            log.InfoFormat("Parsing {0} Response Finish", xmlResponse.GetType().ToString());
            return order;
        }

        private static void ParseCTSStatusNotificationResponse(Outgoing.BaseResponse xmlResponse, String storeNumber)
        {
            log.InfoFormat("Parsing {0} Response Start from Store {1}", xmlResponse.GetType().ToString(), storeNumber);
            string expectedStatus = "399";

            var inner = xmlResponse.InnerXml;
            string omOrderNumber = inner.Root.Element("OMOrderNumber").Value.Trim();
            Order order = OrderCollection.GetOrder(omOrderNumber, storeNumber).Clone() as Order;

            if (order == null)
                throw new Exception("Order with coming OMOrderNumber was not found. OMOrderNumber: " + omOrderNumber);

            bool successFlag = Convert.ToBoolean(inner.Root.Element("SuccessFlag").Value.ToString());

            bool isOrderStatusValid = inner.Root.Element("OrderStatus").Value.Trim().Equals(expectedStatus);

            if (successFlag && isOrderStatusValid)
            {
                String sellingStoreCode = inner.Root.Element("OrderHeader").Element("SellingStoreCode").Value;
                String sellingStoreOrderNumber = inner.Root.Element("OrderHeader").Element("SellingStoreOrderNumber").Value;
                bool isSellingStoreCodeValid = sellingStoreCode.Equals(order.OrderHeader.Element("SellingStoreCode").Value);
                bool isSellingStoreOrderNumberValid = sellingStoreOrderNumber.Equals(order.SellingStoreOrderNumber.Value);

                if (!isSellingStoreCodeValid && isSellingStoreOrderNumberValid)
                    throw new Exception("SellingStoreCode or SellingStoreOrderNumber does not expected");

                bool isLinesStatusValid = xmlResponse.checkLinesStatus(inner.Root.Element("FulfilmentSites").Element("FulfilmentSite").Element("OrderLines"), expectedStatus);

                if (!isLinesStatusValid)
                    throw new Exception("Order Lines status does not expected: " + expectedStatus);
            }
            else
                throw new Exception("Order was not get for Fulfilling in store or unknown store was got in response or unexpected OrderStatus");
            log.InfoFormat("Parsing {0} Response Finish", xmlResponse.GetType().ToString());
        }

        private static void ParseCTSRefundStatusNotificationResponse(Outgoing.BaseResponse xmlResponse, String storeNumber)
        {
            log.InfoFormat("Parsing {0} Response Start from Store {1}", xmlResponse.GetType().ToString(), storeNumber);
            var inner = xmlResponse.InnerXml;
            string omOrderNumber = inner.Root.Element("OMOrderNumber").Value.Trim();
            Refund refund = RefundCollection.GetRefund(omOrderNumber, storeNumber).Clone() as Refund;
            if (refund == null)
                throw new Exception("Refund with coming OMOrderNumber was not found. OMOrderNumber: " + omOrderNumber);

            string expOrderStatus = refund.ExpectedOrderStatus;
            if (expOrderStatus.Equals("999"))
                RefundCollection.RemoveRefund(refund, true);
            string expLineStatus = "999";
            string expectedStatusSS = "399";

            bool successFlag = Convert.ToBoolean(inner.Root.Element("SuccessFlag").Value.ToString());

            bool isOrderStatusValid = inner.Root.Element("OrderStatus").Value.Trim().Equals(expOrderStatus)
                || inner.Root.Element("OrderStatus").Value.Trim().Equals(expectedStatusSS); ;

            if (successFlag && isOrderStatusValid)
            {
                String sellingStoreCode = inner.Root.Element("OrderHeader").Element("SellingStoreCode").Value;
                String sellingStoreOrderNumber = inner.Root.Element("OrderHeader").Element("SellingStoreOrderNumber").Value;
                bool isSellingStoreCodeValid = sellingStoreCode.Equals(refund.SellingStoreCode);
                bool isSellingStoreOrderNumberValid = sellingStoreOrderNumber.Equals(refund.SellingStoreOrderNumber);

                if (!isSellingStoreCodeValid && isSellingStoreOrderNumberValid)
                    throw new Exception("SellingStoreCode or SellingStoreOrderNumber does not expected");

                bool isLinesStatusValid = xmlResponse.checkLinesStatus(inner.Root.Element("FulfilmentSites").Element("FulfilmentSite").Element("OrderLines"), expLineStatus);

                if (!isLinesStatusValid)
                    throw new Exception("Order Lines status does not expected: " + expLineStatus);
            }
            else
                throw new Exception("Order was not get for Fulfilling in store or unknown store was got in response or unexpected OrderStatus");
            log.InfoFormat("Parsing {0} Response Finish", xmlResponse.GetType().ToString());
        }
    }
}