﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Xml;
using OrderManagerStub;
using System.Net.Http.Headers;
using log4net;
using System.Threading;

namespace OrderManagerStub.Controllers
{
    public class OrderManagerController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WebApiApplication));
        public static ManualResetEvent noIncomingProcessing = new ManualResetEvent(true);

        [HttpPost]
        public HttpResponseMessage CreateQOD()
        {
            noIncomingProcessing.Reset();
            try
            {
                var res = new HttpResponseMessage(HttpStatusCode.OK);

                var postBody = Request.Content.ReadAsStringAsync().Result;

                log.Info("Parsing incoming request:\r\n" + postBody);
                var request = Incoming.BaseIncomingRequest.ParseRequest(postBody);
                log.Info("Parsing was successful. Request type is " + request.GetType());

                string stringResp = null;
                if (request is Incoming.CheckFulfilment)
                {
                    stringResp = "Fulfillment is Okay";
                    res.Content = new StringContent(stringResp);
                }
                else if (request is Incoming.OMOrderCreateRequest)
                {
                    var specificRequest = (Incoming.OMOrderCreateRequest)request;

                    // add order to our internal collection
                    OrderCollection.AddOrder(specificRequest.Order);

                    Incoming.OMOrderCreateResp omOrderCreateResp = new Incoming.OMOrderCreateResp(specificRequest.Order);
                    stringResp = omOrderCreateResp.GetOMOrderCreate().ToString();
                    res.Content = new StringContent(stringResp);
                    OutgoingProcessor.AddOrderItem(new OutgoingProcessor.OrderItem(OutgoingProcessor.ActionEnum.Fulfil, specificRequest.Order));
                }
                else if (request is Incoming.OMRefundCreateRequest)
                {

                    var specificRequest = (Incoming.OMRefundCreateRequest)request;
                    RefundCollection.AddRefund(specificRequest.Refund);
                    Incoming.OMRefundCreateResponse ctsEnableRefundCreateResp = new Incoming.OMRefundCreateResponse(specificRequest.Refund);

                    stringResp = ctsEnableRefundCreateResp.GetCTSRefundCreate().ToString();
                    res.Content = new StringContent(stringResp);
                    OutgoingProcessor.AddRefundItem(new OutgoingProcessor.RefundItem(OutgoingProcessor.ActionEnum.Update, specificRequest.Refund));
                }

                if (stringResp != null)
                    log.Info("Sending response:\r\n" + stringResp);
                res.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
                return res;
            }
            catch (Exception exc)
            {
                log.Error("Exception occurred while processing incoming request", exc);
                throw;
            }
            finally
            {
                noIncomingProcessing.Set();
            }
        }
    }
}
