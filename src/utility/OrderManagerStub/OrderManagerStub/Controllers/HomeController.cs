﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderManagerStub.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // DEBUG
            //var settings = OrderManagerStub.Settings.MockSettingsSection.Current;
            return View();
        }
    }
}
