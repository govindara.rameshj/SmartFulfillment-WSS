﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub
{
    public class Refund : ICloneable
    {
        public Refund() { }
        
        public XElement RefundDate { get; set; }

        public XElement RefundStoreCode { get; set; }

        public XElement RefundTransaction { get; set; }

        public XElement RefundTill { get; set; }

        public XElement FulfilmentSites { get; set; }

        public XElement RefundLines { get; set; }

        public XElement DateTimeStamp { get; set; }

        public XElement OMOrderNumber { get; set; }

        public String TargetFulfilmentSite { get; set; }

        public String ExpectedOrderStatus { get; set; }

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        public string SellingStoreCode { get; set; }

        public string SellingStoreOrderNumber { get; set; }
    }
}