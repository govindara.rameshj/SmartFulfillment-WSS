﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
namespace OrderManagerStub.Outgoing
{
    public abstract class BaseRequest
    {
        protected XDocument doc;
        protected string respName;
        protected string xmlOutputName;
        protected XDocument innerXml;

        public abstract string SoapAction
        {
            get;
        }

        public BaseRequest(string respName, string xmlOutputName)
        {
            this.respName = respName;
            this.xmlOutputName = xmlOutputName;
            XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace ns1 = "http://orderManagerWebServices.tpplc.co.uk";
            doc = new XDocument(
                new XDeclaration("1.0", "utf-8", null),
                new XElement(soapenv + "Envelope", new XAttribute(XNamespace.Xmlns + "soapenv", "http://schemas.xmlsoap.org/soap/envelope/"),
                                                    new XAttribute(XNamespace.Xmlns + "ord", "http://orderManagerWebServices.tpplc.co.uk"),
                                                    
                    new XElement(soapenv + "Header"),
                    new XElement(soapenv + "Body",
                        new XElement(ns1 + this.respName, 
                            new XElement(ns1 + this.xmlOutputName)
                                        )))
                );
        }

        public XDocument XmlContent
        {
            get
            {
                XNamespace prefixSoapenv = "http://schemas.xmlsoap.org/soap/envelope/";
                doc.Root.Element(prefixSoapenv + "Body").Elements().First().Elements().First().Value = innerXml.Declaration + innerXml.ToString();
                return doc;
            }
        }
    }
}