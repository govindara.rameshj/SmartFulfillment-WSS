﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Outgoing
{
    public class CTSFulfilmentResponse: BaseResponse
    {
        public CTSFulfilmentResponse(XElement requestElement)
        {
            xmlOutputName = "CTS_Fulfilment_RequestXMLOutput";
            innerElementName = "CTSFulfilmentResponse";
            rootElement = requestElement;            
        }                 
    }
}