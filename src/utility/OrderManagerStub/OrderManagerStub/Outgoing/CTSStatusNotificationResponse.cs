﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Outgoing
{
    public class CTSStatusNotificationResponse: BaseResponse
    {
        public CTSStatusNotificationResponse(XElement requestElement)
        {
            xmlOutputName = "CTS_Status_NotificationXMLOutput";
            innerElementName = "CTSStatusNotification";
            rootElement = requestElement;
        }
    }
}