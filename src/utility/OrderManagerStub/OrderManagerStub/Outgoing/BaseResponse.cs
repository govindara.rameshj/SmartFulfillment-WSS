﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Outgoing
{
    public class BaseResponse
    {
        protected XDocument doc;
        protected string respName;
        protected string xmlOutputName;
        protected XElement rootElement;
        protected static String innerElementName;
        XDocument innerXml;
                
        public static BaseResponse ParseResponse(string postBody)
        {
            var doc = XDocument.Parse(postBody, LoadOptions.None);
            XNamespace prefixSoap = "http://schemas.xmlsoap.org/soap/envelope/";

            var body = doc.Root.Element(prefixSoap + "Body");
            var element = (XElement)body.FirstNode;
            switch (element.Name.LocalName)
            {
                case "CTS_Fulfilment_RequestResponse":
                    return new CTSFulfilmentResponse(element);
                case "CTS_Status_NotificationResponse":
                    return new CTSStatusNotificationResponse(element);
                case "CTS_Update_RefundResponse":
                    return new CTSUpdateRefundResponse(element);
                default:
                    throw new Exception("Unknown request name: " + element.Name.LocalName);
            }
        }

        public XDocument InnerXml
        {
            get
            {
                XNamespace ns = "http://orderManagerWebServices.tpplc.co.uk";
                var inner = rootElement.Element(ns + xmlOutputName).Value;
                innerXml = XDocument.Parse(inner);
                return innerXml;
            }
        }

        public bool checkLinesStatus(XElement lines, String expectedStatus)
        {
            foreach (XElement line in lines.Elements("OrderLine"))
            {
                if (!line.Element("LineStatus").Value.Trim().Equals(expectedStatus) && !line.Element("LineStatus").Value.Trim().Equals("200"))
                    return false;
                else
                    line.Element("LineStatus").Value = expectedStatus;
            }
            return true;
        }  
    }
}