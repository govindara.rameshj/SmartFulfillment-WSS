﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using OrderManagerStub.Settings;
using log4net;

namespace OrderManagerStub.Outgoing
{
    public class CTSFulfilmentRequest : BaseRequest
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WebApiApplication));
        public Order Order;
        private MockSettingsSection settings;
        public CTSFulfilmentRequest(Order order, String targetFulfilmentSite)
            : base("CTS_Fulfilment_Request", "CTS_Fulfilment_RequestXMLInput")
        {
            settings = MockSettingsSection.Current;
            Order = order.Clone() as Order ;
            Order.OrderStatus = "200";

            string postCode = Order.OrderHeader.Element("DeliveryPostcode").Value;
            Order.TargetFulfilmentSite = new XElement("TargetFulfilmentSite", targetFulfilmentSite);
            if(!Order.OrderHeader.Elements("ExtendedLeadTime").Any())
                Order.OrderHeader.Add(new XElement("ExtendedLeadTime", 0));
            if (Order.OrderHeader.Element("ToBeDelivered").Value.Equals("true")
                || Order.OrderHeader.Element("ToBeDelivered").Value.Equals("1"))
            {
                log.InfoFormat("Order {0} will be Delivered", Order.OMOrderNumber);
                Order.OrderHeader.Element("ToBeDelivered").Value = "1";
            }
            else
            {
                Order.OrderHeader.Element("ToBeDelivered").Value = "0";
                if (Order.OrderHeader.Elements("DeliveryContactPhone").Any())
                    Order.OrderHeader.Element("DeliveryContactPhone").Remove();
                log.InfoFormat("Order {0} will not be Delivered", Order.OMOrderNumber);
            }
            SetFulfilmentSites();
            SetLinesStatus("200");
            String sellingStoreCode = Order.OrderHeader.Element("SellingStoreCode").Value;
            if (targetFulfilmentSite.Equals(MockSettingsSection.Current.MCFCStoreNumber))
            {
                Order.OrderHeader.Element("DeliveryContactPhone").Remove();
                log.Info("Request will be sent to MCFC");
            }

            innerXml = new XDocument(
                            new XDeclaration("1.0", "utf-16", null),
                            new XElement("CTSFulfilmentRequest",
                                new XElement(Order.DateTimeStamp),
                                new XElement(Order.TargetFulfilmentSite),
                                new XElement(Order.OrderHeader),
                                new XElement(Order.OrderLines)
                                )
                );
        }

        public override string SoapAction
        {
            get
            {
                return "http://orderManagerWebServices.tpplc.co.uk/CTS_Fulfilment_Request";
            }
        }

        private void SetFulfilmentSites()
        {
            foreach (XElement line in Order.OrderLines.Elements("OrderLine"))
            {
                string fulfilmentSite = settings.GetBySKU(line.Element("ProductCode").Value).StoreNumber;
                if (line.Elements("SourceOrderLineNo").Any())
                    line.Element("SourceOrderLineNo").Remove();
                if (line.Elements("FulfilmentSite").Any())
                    line.Element("FulfilmentSite").Value = fulfilmentSite;
                else
                    line.Add(new XElement("FulfilmentSite", fulfilmentSite));                
            }
        }

        private void SetLinesStatus(string status)
        {
            foreach (XElement line in Order.OrderLines.Elements("OrderLine"))
            {
                if (line.Elements("LineStatus").Any())
                    line.Element("LineStatus").Value = status;
                else
                    line.Add(new XElement("LineStatus", status));
            }
        }
    }
}