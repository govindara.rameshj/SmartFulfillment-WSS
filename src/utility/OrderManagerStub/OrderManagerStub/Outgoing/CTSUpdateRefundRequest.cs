﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrderManagerStub.Settings;
using System.Xml.Linq;

namespace OrderManagerStub.Outgoing
{
    public class CTSUpdateRefundRequest : BaseRequest
    {
        public Refund Refund;

        public CTSUpdateRefundRequest(Refund refund)
            : base("CTS_Update_Refund", "CTS_Update_RefundXMLInput")
        {
            Refund = refund.Clone() as Refund;
            
            SetLinesStatus("150");
            SetSellingStoreLineNo();
            SetOMOrderLineNo();

            innerXml = new XDocument(
                              new XDeclaration("1.0", "utf-8", "no"),
                              new XElement("CTSUpdateRefundRequest",
                                  new XElement(refund.DateTimeStamp),
                                  new XElement(refund.OMOrderNumber),
                                  new XElement("OrderRefunds",
                                       new XElement("OrderRefund",
                                           new XElement(refund.RefundDate),
                                           new XElement(refund.RefundStoreCode),
                                           new XElement(refund.RefundTransaction),
                                           new XElement(refund.RefundTill),
                                           new XElement(refund.FulfilmentSites),
                                           new XElement(refund.RefundLines))
                                  ))
                  );
        }

        private void SetLinesStatus(string status)
        {
            foreach (XElement line in Refund.RefundLines.Elements("RefundLine"))
            {
                line.Element("RefundLineStatus").Value = status;
            } 
        }

        private void SetSellingStoreLineNo()
        {
            foreach (XElement line in Refund.RefundLines.Elements("OrderLine"))
            {
                line.Element("SellingStoreLineNo").Value = Convert.ToInt64(line.Element("SellingStoreLineNo").Value).ToString();
            }
        }

        private void SetOMOrderLineNo()
        {
            foreach (XElement line in Refund.RefundLines.Elements("OrderLine"))
            {
                line.Element("OMOrderLineNo").Value = Convert.ToInt64(line.Element("OMOrderLineNo").Value).ToString();
            }
        }


        public override string SoapAction
        {
            get { return "http://orderManagerWebServices.tpplc.co.uk/CTS_Update_Refund"; }
        }
    }
}