﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Outgoing
{
    public class CTSUpdateRefundResponse: BaseResponse
    {
        public CTSUpdateRefundResponse(XElement requestElement)
        {
            xmlOutputName = "CTS_Update_RefundXMLOutput";
            innerElementName = "CTSUpdateRefundResponse";
            rootElement = requestElement;            
        }                 
    }
}