﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrderManagerStub.Outgoing
{
    public class CTSRefundStatusNotificationRequest : BaseRequest
    {
        private Refund refund;
        private Order order;
        
        public CTSRefundStatusNotificationRequest(Refund refund)
            : base("CTS_Status_Notification", "CTS_Status_NotificationXMLInput")
        {
            this.refund = refund.Clone() as Refund;

            order = OrderCollection.GetOrder(refund.OMOrderNumber.Value, refund.TargetFulfilmentSite).Clone() as Order;
            
            XElement orderLines;
            orderLines = GenerateLines(order);
            string orderStatus = GetOrderStatus();

            XElement refundHeader = new XElement("OrderHeader");
            refundHeader.Add(order.OrderHeader.Element("SellingStoreCode"));
            refundHeader.Add(order.SellingStoreOrderNumber);

            this.refund.SellingStoreCode = order.OrderHeader.Element("SellingStoreCode").Value;
            this.refund.SellingStoreOrderNumber = order.OrderHeader.Element("SellingStoreOrderNumber").Value;

            if (orderStatus.Equals("950"))
                refund.ExpectedOrderStatus = "999";
            else
                refund.ExpectedOrderStatus = "399";
            
            innerXml = new XDocument(
                              new XDeclaration("1.0", "utf-8", "no"),
                              new XElement("CTSStatusNotificationRequest",
                                  new XElement(refund.DateTimeStamp),
                                  new XElement(refund.OMOrderNumber),
                                  new XElement("OrderStatus", orderStatus),
                                  new XElement(refundHeader),
                                  new XElement(orderLines)                          
                  ));
            if (refund.ExpectedOrderStatus.Equals("999"))
                OrderCollection.RemoveOrder(order, true);
        }

        private string GetOrderStatus()
        {
            if (this.refund.RefundLines.Elements("RefundLine").Elements("FulfilmentSiteCode")
                .FirstOrDefault(x => x.Value.Equals(refund.TargetFulfilmentSite)) == null)
                return "399";
            IEnumerable<XElement> orderLines = order.OrderLines.Elements("OrderLine");
            IEnumerable<XElement> refundLines = this.refund.RefundLines.Elements("RefundLine");
            foreach (XElement refLine in refundLines)
            {
                string productCode = refLine.Element("ProductCode").Value;
                XElement line = orderLines.First(x => x.Element("ProductCode").Value.Equals(productCode));
                int totalOrderQuantity = Convert.ToInt32(line.Element("TotalOrderQuantity").Value);
                int quantityReturned = Convert.ToInt32(refLine.Element("QuantityReturned").Value);
                int quantityCancelled = Convert.ToInt32(refLine.Element("QuantityCancelled").Value);
                if (totalOrderQuantity > (quantityReturned + quantityCancelled))
                    return "350";
                else
                    return "950";
            }
            return "399";
        }

        private XElement GenerateLines(Order order)
        {
            XElement lines = new XElement("FulfilmentSites");
            String lineStatus = "";
            foreach (XElement line in order.OrderLines.Elements("OrderLine"))
            {
                lineStatus = GetLineStatus(line.Element("ProductCode").Value);
                lines.Add(new XElement("FulfilmentSite",
                        new XElement("FulfilmentSite", line.Element("FulfilmentSite").Value),
                        new XElement("FulfilmentSiteOrderNumber", order.FulfillingSiteOrderNumber.Value),
                        new XElement("FulfilmentSiteIBTOutNumber", "0"),
                        new XElement("OrderLines",
                            new XElement("OrderLine",
                            new XElement("SellingStoreLineNo", line.Element("SellingStoreLineNo").Value),
                            new XElement("OMOrderLineNo", line.Element("OMOrderLineNo").Value),
                            new XElement("ProductCode", line.Element("ProductCode").Value),
                            new XElement("LineStatus", lineStatus)
                        ))));
            }
            return lines;
        }

        private string GetLineStatus(string productCode)
        {
            XElement refundLine = this.refund.RefundLines.Elements("RefundLine").FirstOrDefault(x => x.Element("ProductCode").Value.Equals(productCode));
            if (this.refund.RefundLines.Elements("RefundLine").Elements("FulfilmentSiteCode")
                .FirstOrDefault(x => x.Value.Equals(refund.TargetFulfilmentSite)) == null)
                return "399";
            IEnumerable<XElement> orderLines = order.OrderLines.Elements("OrderLine");
            if (refundLine == null)
                return "399";
            else
                return "950";              
        }

        public override string SoapAction
        {
            get { return "http://orderManagerWebServices.tpplc.co.uk/CTS_Status_Notification"; }
        }
    }
}