﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using OrderManagerStub.Settings;

namespace OrderManagerStub.Outgoing
{
    public class CTSStatusNotificationRequest : BaseRequest
    {
        Order Order;
        public CTSStatusNotificationRequest(Order order)
            : base("CTS_Status_Notification", "CTS_Status_NotificationXMLInput")
        {
            Order = order;
            XElement orderStatus = new XElement("OrderStatus", "300");
            String storeNumber = Order.TargetFulfilmentSite.Value;
            XElement orderHeader = new XElement("OrderHeader");
            orderHeader.Add(order.OrderHeader.Element("SellingStoreCode"));
            orderHeader.Add(order.SellingStoreOrderNumber);
            XElement orderLines;
            orderLines = GenerateLines(order);
            innerXml = new XDocument(
                            new XDeclaration("1.0", "utf-8", null),
                            new XElement("CTSStatusNotificationRequest",
                                new XElement(order.DateTimeStamp),
                                new XElement("OMOrderNumber", order.OMOrderNumber),
                                new XElement(orderStatus),
                                new XElement(orderHeader),
                                new XElement(orderLines)
                                )
                );            
        }

        public override string SoapAction
        {
            get
            {
                return "http://orderManagerWebServices.tpplc.co.uk/CTS_Status_Notification";
            }
        }

        private XElement GenerateLines(Order order)
        {
            XElement lines = new XElement("FulfilmentSites");
            String lineStatus = "300";
            foreach (XElement line in order.OrderLines.Elements("OrderLine"))
            {
                lines.Add(new XElement ("FulfilmentSite",
                        new XElement ("FulfilmentSite",line.Element("FulfilmentSite").Value),
                        new XElement("FulfilmentSiteOrderNumber", order.FulfillingSiteOrderNumber.Value),
                        new XElement("FulfilmentSiteIBTOutNumber", "0"),                      
                        new XElement("OrderLines",
                            new XElement("OrderLine",
                            new XElement("SellingStoreLineNo", line.Element("SellingStoreLineNo").Value),
                            new XElement("OMOrderLineNo", line.Element("OMOrderLineNo").Value),
                            new XElement("ProductCode", line.Element("ProductCode").Value),
                            new XElement("LineStatus", lineStatus)
                        ))));
            }
            return lines;
        }
    }
}