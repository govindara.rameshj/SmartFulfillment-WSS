﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace OrderManagerStub.Settings
{
    public class MockSettingsSection : ConfigurationSection
    {
        public class MappingItem
        {
            public string StoreNumber;
            public string CtsOrderManagerUrl;
            public List<string> SKU = new List<string>();
            public List<string> PostCodes = new List<string>();
        }

        public string MCFCStoreNumber;
        public string SleepTimeOut;

        public List<MappingItem> StoreMappings = new List<MappingItem>();

        public MappingItem GetBySKU(string sku)
        {
            return StoreMappings.Find((MappingItem storeItem) => storeItem.SKU.Contains(sku));
        }

        public MappingItem GetByPostCode(string pc)
        {
            return StoreMappings.Find((MappingItem storeItem) => storeItem.PostCodes.Contains(pc));
        }

        public MappingItem GetByStoreNumber(string storeNumber)
        {
            return StoreMappings.Find((MappingItem storeItem) => storeItem.StoreNumber.Equals(storeNumber));
        }

        protected override void DeserializeSection(XmlReader reader)
        {
            reader.Read();
            var MockSettings = reader.ReadOuterXml();
            XDocument content = XDocument.Parse(MockSettings);
            MCFCStoreNumber = content.Element("MockSettings").Element("MCFCStoreNumber").Value;
            SleepTimeOut = content.Element("MockSettings").Element("SleepTimeOut").Value;
            XElement StoreMapping = content.Element("MockSettings").Element("StoreMapping");
            foreach (XElement store in StoreMapping.Elements("Store"))
            {
                MappingItem item = new MappingItem();
                item.StoreNumber = store.Attribute("id").Value.ToString();
                item.CtsOrderManagerUrl = store.Attribute("CtsOrderManagerUrl").Value.ToString();
                foreach (XElement sku in store.Elements("SKU"))
                    item.SKU.Add(sku.Value);
                if (store.Elements("PostCode").Any())
                    foreach (XElement pc in store.Elements("PostCode"))
                        item.PostCodes.Add(pc.Value);
                StoreMappings.Add(item);
            }
        }

        private static MockSettingsSection current = null;
        public static MockSettingsSection Current
        {
            get
            {
                if (current == null)
                    current = (MockSettingsSection)ConfigurationManager.GetSection("MockSettings");
                return current;
            }
        }
    }
}