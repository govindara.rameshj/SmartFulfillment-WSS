﻿Option Strict On
Option Explicit On

Public Class HandHeldTerminalDetail
    Inherits Cts.Oasys.Core.Base

    Implements IHandHeldTerminalDetail

    Private m_AdjustmentApplied As Boolean
    Private m_HHTDate As Date
    Private m_LabelDetailCorrect As Boolean
    'Private m_LocationCount As Integer
    Private m_MarkDownStock As Integer
    Private m_NormalOnHandStock As Integer
    Private m_ProductCode As String
    Private m_StockExist As Boolean
    Private m_StockSold As Integer
    Private m_TotalCount As Integer
    Private m_TotalMarkDownCount As Integer
    Private m_TotalPreSoldStock As Integer

    Private m_NormalSellingPrice As Decimal
    Private m_ProductDescription As String
    Private m_SupplierNumber As String
    Private m_SupplierPartCode As String
    Private m_Original As String

#Region "Properties"

    'HHTDET

    <ColumnMapping("IADJ")> Public Property AdjustmentApplied() As Boolean Implements IHandHeldTerminalDetail.AdjustmentApplied
        Get
            Return m_AdjustmentApplied
        End Get
        Set(ByVal value As Boolean)
            m_AdjustmentApplied = value
        End Set
    End Property

    <ColumnMapping("DATE1")> Public Property HHTDate() As Date Implements IHandHeldTerminalDetail.HHTDate
        Get
            Return m_HHTDate
        End Get
        Set(ByVal value As Date)
            m_HHTDate = value
        End Set
    End Property

    <ColumnMapping("LBOK")> Public Property LabelDetailCorrect() As Boolean Implements IHandHeldTerminalDetail.LabelDetailCorrect
        Get
            Return m_LabelDetailCorrect
        End Get
        Set(ByVal value As Boolean)
            m_LabelDetailCorrect = value
        End Set
    End Property

    '<ColumnMapping("LocationCount")> Public Property LocationCount() As Integer Implements IHandHeldTerminalDetail.LocationCount
    '    Get
    '        Return m_LocationCount
    '    End Get
    '    Set(ByVal value As Integer)
    '        m_LocationCount = value
    '    End Set
    'End Property

    <ColumnMapping("MDNQ")> Public Property MarkDownStock() As Integer Implements IHandHeldTerminalDetail.MarkDownStock
        Get
            Return m_MarkDownStock
        End Get
        Set(ByVal value As Integer)
            m_MarkDownStock = value
        End Set
    End Property

    <ColumnMapping("ONHA")> Public Property NormalOnHandStock() As Integer Implements IHandHeldTerminalDetail.NormalOnHandStock
        Get
            Return m_NormalOnHandStock
        End Get
        Set(ByVal value As Integer)
            m_NormalOnHandStock = value
        End Set
    End Property

    <ColumnMapping("ORIG")> Public Property Original() As String Implements IHandHeldTerminalDetail.Original
        Get
            Return m_Original
        End Get
        Set(ByVal value As String)
            m_Original = value
        End Set
    End Property

    <ColumnMapping("SKUN")> Public Property ProductCode() As String Implements IHandHeldTerminalDetail.ProductCode
        Get
            Return m_ProductCode
        End Get
        Set(ByVal value As String)
            m_ProductCode = value
        End Set
    End Property

    <ColumnMapping("StockExist")> Public Property StockExist() As Boolean Implements IHandHeldTerminalDetail.StockExist
        Get
            Return m_StockExist
        End Get
        Set(ByVal value As Boolean)
            m_StockExist = value
        End Set
    End Property

    <ColumnMapping("StockSold")> Public Property StockSold() As Integer Implements IHandHeldTerminalDetail.StockSold
        Get
            Return m_StockSold
        End Get
        Set(ByVal value As Integer)
            m_StockSold = value
        End Set
    End Property

    <ColumnMapping("TCNT")> Public Property TotalCount() As Integer Implements IHandHeldTerminalDetail.TotalCount
        Get
            Return m_TotalCount
        End Get
        Set(ByVal value As Integer)
            m_TotalCount = value
        End Set
    End Property

    <ColumnMapping("TMDC")> Public Property TotalMarkDownCount() As Integer Implements IHandHeldTerminalDetail.TotalMarkDownCount
        Get
            Return m_TotalMarkDownCount
        End Get
        Set(ByVal value As Integer)
            m_TotalMarkDownCount = value
        End Set
    End Property

    <ColumnMapping("TPRE")> Public Property TotalPreSoldStock() As Integer Implements IHandHeldTerminalDetail.TotalPreSoldStock
        Get
            Return m_TotalPreSoldStock
        End Get
        Set(ByVal value As Integer)
            m_TotalPreSoldStock = value
        End Set
    End Property

    'STKMAS

    <ColumnMapping("PRIC")> Public Property NormalSellingPrice() As Decimal Implements IHandHeldTerminalDetail.NormalSellingPrice
        Get
            Return m_NormalSellingPrice
        End Get
        Set(ByVal value As Decimal)
            m_NormalSellingPrice = value
        End Set
    End Property

    <ColumnMapping("DESCR")> Public Property ProductDescription() As String Implements IHandHeldTerminalDetail.ProductDescription
        Get
            Return m_ProductDescription
        End Get
        Set(ByVal value As String)
            m_ProductDescription = value
        End Set
    End Property

    <ColumnMapping("SUPP")> Public Property SupplierNumber() As String Implements IHandHeldTerminalDetail.SupplierNumber
        Get
            Return m_SupplierNumber
        End Get
        Set(ByVal value As String)
            m_SupplierNumber = value
        End Set
    End Property

    <ColumnMapping("PROD")> Public Property SupplierPartCode() As String Implements IHandHeldTerminalDetail.SupplierPartCode
        Get
            Return m_SupplierPartCode
        End Get
        Set(ByVal value As String)
            m_SupplierPartCode = value
        End Set
    End Property

#End Region

#Region "Functions & Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub Update(ByVal SelectedDate As Date, ByVal ProductCode As String, ByVal AdjustmentApplied As Boolean) Implements IHandHeldTerminalDetail.Update

        Dim Repository As IHandHeldTerminalDetailRepository

        Repository = HandHeldTerminalDetailRepositoryFactory.FactoryGet
        Repository.Update(SelectedDate, ProductCode, AdjustmentApplied)

    End Sub

#End Region

End Class

Public Class HandHeldTerminalDetailCollection
    Inherits BaseCollection(Of HandHeldTerminalDetail)

    Implements IHandHeldTerminalDetailCollection

    Public Sub ReadDetail(ByVal SelectedDate As Date) Implements IHandHeldTerminalDetailCollection.ReadDetail
        Dim Repository As IHandHeldTerminalDetailRepository
        Dim DT As DataTable

        Repository = HandHeldTerminalDetailRepositoryFactory.FactoryGet
        DT = Repository.Read(SelectedDate)
        Me.Load(DT)
    End Sub

End Class