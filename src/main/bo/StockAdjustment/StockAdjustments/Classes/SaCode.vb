﻿Imports Cts.Oasys.Core
Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Xml.Serialization
Imports System.Reflection
Imports Cts.Oasys.Data

Namespace SACode
    Public Class SaCode

        Inherits Cts.Oasys.Core.Base

        Implements ISaCode

        'Private m_SaCode As iSaCode

        Private m_AllowCodes As String = String.Empty

        Private m_CodeNumber As String = String.Empty

        Private m_Description As String = String.Empty

        Private m_CodeSign As String = String.Empty

        Private m_CodeType As String = String.Empty

        Private m_IsAuthRequired As Boolean = False

        Private m_IsCommentable As Boolean = False

        Private m_IsIssueQuery As Boolean = False

        Private m_IsKnownTheft As Boolean = False

        Private m_IsMarkdown As Boolean = False

        Private m_IsPassRequired As Boolean = False

        Private m_IsReserved As Boolean = False

        Private m_IsStockLoss As Boolean = False

        Private m_IsWriteOff As Boolean = False

        Private m_SecurityLevel As String = String.Empty

        Private m_MarkdownWriteoff As String = String.Empty

        Private m_DataDisplay As String = String.Empty

        Private m_SignDisplay As String = String.Empty

        Private m_TypeDisplay As String = String.Empty


#Region "Properties"
        <ColumnMapping("AllowCodes")> Public Property AllowCodes() As String Implements ISaCode.AllowCodes
            Get
                Return m_AllowCodes
            End Get
            Set(ByVal value As String)
                m_AllowCodes = value
            End Set
        End Property

        <ColumnMapping("Description")> Public Property Description() As String Implements ISaCode.Description
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <ColumnMapping("CodeNumber")> Public Property CodeNumber() As String Implements ISaCode.CodeNumber
            Get
                Return m_CodeNumber
            End Get
            Set(ByVal value As String)
                m_CodeNumber = value
            End Set
        End Property

        <ColumnMapping("CodeSign")> Public Property CodeSign() As String Implements ISaCode.CodeSign
            Get
                Return m_CodeSign
            End Get
            Set(ByVal value As String)
                m_CodeSign = value
            End Set
        End Property

        <ColumnMapping("CodeType")> Public Property CodeType() As String Implements ISaCode.CodeType
            Get
                Return m_CodeType
            End Get
            Set(ByVal value As String)
                m_CodeType = value
            End Set
        End Property

        <ColumnMapping("IsAuthRequired")> Public Property IsAuthRequired() As Boolean Implements ISaCode.IsAuthRequired
            Get
                Return m_IsAuthRequired
            End Get
            Set(ByVal value As Boolean)
                m_IsAuthRequired = value
            End Set
        End Property

        <ColumnMapping("IsCommentable")> Public Property IsCommentable() As Boolean Implements ISaCode.IsCommentable
            Get
                Return m_IsCommentable
            End Get
            Set(ByVal value As Boolean)
                m_IsCommentable = value
            End Set
        End Property

        <ColumnMapping("IsIssueQuery")> Public Property IsIssueQuery() As Boolean Implements ISaCode.IsIssueQuery
            Get
                Return m_IsIssueQuery
            End Get
            Set(ByVal value As Boolean)
                m_IsIssueQuery = value
            End Set
        End Property

        <ColumnMapping("IsKnownTheft")> Public Property IsKnownTheft() As Boolean Implements ISaCode.IsKnownTheft
            Get
                Return m_IsKnownTheft
            End Get
            Set(ByVal value As Boolean)
                m_IsKnownTheft = value
            End Set
        End Property

        <ColumnMapping("IsMarkdown")> Public Property IsMarkdown() As Boolean Implements ISaCode.IsMarkdown
            Get
                Return m_IsMarkdown
            End Get
            Set(ByVal value As Boolean)
                m_IsMarkdown = value
            End Set
        End Property

        <ColumnMapping("IsPassRequired")> Public Property IsPassRequired() As Boolean Implements ISaCode.IsPassRequired
            Get
                Return m_IsPassRequired
            End Get
            Set(ByVal value As Boolean)
                m_IsPassRequired = value
            End Set
        End Property

        <ColumnMapping("IsReserved")> Public Property IsReserved() As Boolean Implements ISaCode.IsReserved
            Get
                Return m_IsReserved
            End Get
            Set(ByVal value As Boolean)
                m_IsReserved = value
            End Set
        End Property

        <ColumnMapping("IsStockLoss")> Public Property IsStockLoss() As Boolean Implements ISaCode.IsStockLoss
            Get
                Return m_IsStockLoss
            End Get
            Set(ByVal value As Boolean)
                m_IsStockLoss = value
            End Set
        End Property

        <ColumnMapping("IsWriteOff")> Public Property IsWriteOff() As Boolean Implements ISaCode.IsWriteOff
            Get
                Return m_IsWriteOff
            End Get
            Set(ByVal value As Boolean)
                m_IsWriteOff = value
            End Set
        End Property

        <ColumnMapping("SecurityLevel")> Public Property SecurityLevel() As String Implements ISaCode.SecurityLevel
            Get
                Return m_SecurityLevel
            End Get
            Set(ByVal value As String)
                m_SecurityLevel = value
            End Set
        End Property

        Public Property MarkdownWriteoff() As String Implements ISaCode.MarkdownWriteoff
            Get
                Return m_MarkdownWriteoff
            End Get
            Set(ByVal value As String)
                m_MarkdownWriteoff = value
            End Set
        End Property

        <ColumnMapping("DataDisplay")> Public ReadOnly Property DataDisplay() As String
            Get
                Return m_CodeNumber & " " & m_Description
            End Get
        End Property

        <ColumnMapping("SignDisplay")> Public Property SignDisplay() As String Implements ISaCode.DisplaySign
            Get
                Return GetSignString(m_CodeSign)
            End Get
            Set(ByVal value As String)
                m_SignDisplay = value
            End Set
        End Property

        <ColumnMapping("TypeDisplay")> Public Property TypeDisplay() As String Implements ISaCode.DisplayType
            Get
                Return GetTypeString(m_CodeType)
            End Get
            Set(ByVal value As String)
                m_TypeDisplay = value
            End Set
        End Property

#End Region

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overloads Function AddNew() As SaCode
            Dim Line As SaCode = New SaCode
            Return Line
        End Function

        Public Overloads Function Load(ByVal SecurityLevel As Integer, ByVal CodeNumber As String) As Boolean Implements ISaCode.Load
            Dim DataAccess As ISACodesRepository = SACodesRepositoryFactory.FactoryGet
            Dim dr As DataRow = DataAccess.GetSACode(SecurityLevel, CodeNumber)
            If dr IsNot Nothing Then
                Me.Load(dr)
                Return True
            Else
                Return False
            End If

        End Function

        Private Function GetTypeString(ByVal CodeType As String) As String
            Select Case CodeType
                Case "N"
                    Return "Normal"
                Case "T"
                    Return "Transfer"
                Case Else
                    Return String.Empty
            End Select
        End Function

        Private Function GetSignString(ByVal CodeSign As String) As String
            Select Case CodeSign
                Case "+"
                    Return "Add"
                Case "-"
                    Return "Subtract"
                Case "S"
                    Return "Add or Subtract"
                Case "C"
                    Return "Calculate"
                Case "R"
                    Return "Replace"
                Case Else
                    Return String.Empty
            End Select
        End Function
    End Class

End Namespace
