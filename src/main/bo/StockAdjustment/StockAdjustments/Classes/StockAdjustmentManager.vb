﻿Imports Cts.Oasys.Core

Public Class StockAdjustmentManager
    Implements IStockAdjustmentManager

    Private m_StockAdjustmentManager As IStockAdjustmentManager
    Private m_Stock As IStock = StockFactory.FactoryGet
    Private m_StockAdjustment As IAdjustment = AdjustmentFactory.FactoryGet
    Private m_DRL As IDRLValidator
    Private m_TypeOfAdjustment As String = String.Empty

#Region "Properties"

    Public Property DRL() As IDRLValidator Implements IStockAdjustmentManager.DRL
        Get
            Return m_DRL
        End Get
        Set(ByVal value As IDRLValidator)
            m_DRL = value
        End Set
    End Property

    Public Property Stock() As IStock Implements IStockAdjustmentManager.Stock
        Get
            Return m_Stock
        End Get
        Set(ByVal value As IStock)
            m_Stock = value
        End Set
    End Property

    Public Property StockAdjustment() As IAdjustment Implements IStockAdjustmentManager.StockAdjustment
        Get
            Return m_StockAdjustment
        End Get
        Set(ByVal value As IAdjustment)
            m_StockAdjustment = value
        End Set
    End Property

#End Region

#Region "Interface"

    Public Overridable Function NormalAdjustment(ByVal SACode As ISaCode, ByRef Adjustment As IAdjustment, ByVal SkuNumber As String, _
                                                 ByVal AdjDate As Date, ByVal DRLNumber As String, ByRef AdjQty As Integer, _
                                                 ByVal Comment As String, ByVal EmployeId As Integer) As Boolean Implements IStockAdjustmentManager.NormalAdjustment

        Dim Connect As IConnecxion = ConnexionFactory.FactoryGet
        Dim Con As Connection = Connect.Connecxion()

        Trace.WriteLine("Saving Adjustment.", Application.ProductName & ".ProcessSaleOrders")

        Try
            Dim Stocklog As IStockLog = StockLogFactory.FactoryGet

            Stocklog.ReadStockLog(SkuNumber)

            Try
                Con.StartTransaction()


                'reload stock; til may have amended STKMAS:ONHA and/or STKMAS:MDNQ
                m_Stock.ReadSKU(Con, SkuNumber)
                StockUpdate(SACode, Adjustment, AdjDate, AdjQty)


                Adjustment.SaveAdjustment(Con, AdjQty, AdjDate, SkuNumber, DRLNumber, Comment, SACode, m_Stock, Stocklog, EmployeId)
                Stocklog.SaveStockLog(Con, AdjQty, SACode, m_Stock, Adjustment, EmployeId)
                m_Stock.Update(Con)


                Con.CommitTransaction()

            Catch ex As Exception
                Con.RollbackTransaction()

                If (New StockAdjustmentManagerRollbackTransactionReturnStateFactory).GetImplementation = False Then Return False

            End Try

            Trace.WriteLine("Saved order.", Application.ProductName & ".Normal Update")
            Return True

        Catch ex As Exception
            Return False

        Finally
            Con.Dispose()

        End Try

    End Function

    Public Overridable Function NormalAdjustment(ByRef Con As Cts.Oasys.Data.Connection, _
                                                 ByVal SACode As ISaCode, _
                                                 ByRef Adjustment As IAdjustment, _
                                                 ByVal SkuNumber As String, _
                                                 ByVal AdjDate As Date, _
                                                 ByVal DRLNumber As String, _
                                                 ByRef AdjQty As Integer, _
                                                 ByVal Comment As String, _
                                                 ByVal EmployeeID As Integer) As Boolean Implements IStockAdjustmentManager.NormalAdjustment

        Trace.WriteLine("Saving Adjustment.", Application.ProductName & ".ProcessSaleOrders")

        Try
            Dim StockLog As IStockLog = StockLogFactory.FactoryGet
            Dim StockLogReadDecide As IStockLogOverloadDecide

            Dim NextSequenceNumber As IAdjustmentNextSequenceNumberDecide

            StockLogReadDecide = (New StockLogOverloadDecideFactory).GetImplementation
            StockLogReadDecide.ReadStockLog(Con, SkuNumber, StockLog)

            'reload stock; til may have amended STKMAS:ONHA and/or STKMAS:MDNQ
            m_Stock.ReadSKU(Con, SkuNumber)
            StockUpdate(SACode, Adjustment, AdjDate, AdjQty)


            NextSequenceNumber = (New AdjustmentNextSequenceNumberDecideFactory).GetImplementation
            NextSequenceNumber.SaveAdjustment(Adjustment, Con, AdjQty, AdjDate, SkuNumber, DRLNumber, Comment, SACode, m_Stock, StockLog, EmployeeID, , , , , True)


            StockLog.SaveStockLog(Con, AdjQty, SACode, m_Stock, Adjustment, EmployeeID)
            m_Stock.Update(Con)

        Catch ex As Exception
            Return False

        End Try

        Trace.WriteLine("Saved order.", Application.ProductName & ".Normal Update")

        Return True

    End Function

    Public Overridable Function NormalAdjustment(ByVal AdjustmentCode As ISaCode, _
                                                 ByVal NewStockAdjustment As IAdjustment, _
                                                 ByVal ExistingStockAdjustment As IAdjustment, _
                                                 ByVal SkuNumber As String, _
                                                 ByVal AdjustmentDate As Date, _
                                                 ByVal DRLNumber As String, _
                                                 ByVal AdjustmentQuantity As Integer, _
                                                 ByVal Comment As String, _
                                                 ByVal EmployeeID As Integer) As Boolean Implements IStockAdjustmentManager.NormalAdjustment


        Dim Connect As Connection
        Dim Stocklog As IStockLog = StockLogFactory.FactoryGet


        Connect = CType(ConnectionFactory.FactoryGet(False), Connection)
        Stocklog = StockLogFactory.FactoryGet

        Stocklog.ReadStockLog(SkuNumber)

        Trace.WriteLine("Saving revsere adjustment(s) - ", Application.ProductName)

        Try
            Connect.StartTransaction()

            'existing adjustment
            ExistingStockAdjustment.UpdateAdjustment(Connect)

            'new adjustment
            m_Stock.ReadSKU(Connect, SkuNumber)
            StockUpdate(AdjustmentCode, NewStockAdjustment, AdjustmentDate, AdjustmentQuantity)

            NewStockAdjustment.SaveAdjustment(Connect, AdjustmentQuantity, AdjustmentDate, SkuNumber, DRLNumber, Comment, AdjustmentCode, m_Stock, Stocklog, EmployeeID)
            Stocklog.SaveStockLog(Connect, AdjustmentQuantity, AdjustmentCode, m_Stock, NewStockAdjustment, EmployeeID)
            m_Stock.Update(Connect)

            Connect.CommitTransaction()

        Catch ex As Exception
            Connect.RollbackTransaction()
            Return False

        Finally
            Connect.Dispose()

        End Try

        Trace.WriteLine("Saved revsere adjustment(s) - ", Application.ProductName)
        Return True

    End Function

    Public Sub AdjustmentMakeEqual(ByRef Source As IAdjustment, ByRef Target As IAdjustment) Implements IStockAdjustmentManager.AdjustmentMakeEqual

        Target.MakeEqual(Source)

    End Sub

    Public Function TransferAdjustment(ByVal SACode As ISaCode, ByRef Adjustment As IAdjustment, ByVal SkuNumber As String, ByVal TransferSkuNumber As String, ByVal AdjQty As Integer, ByVal AdjDate As Date, ByVal DRLNumber As String, ByVal Comment As String, ByVal EmployeId As Integer) As Boolean Implements IStockAdjustmentManager.TranserAdjustment

        Dim Connect As IConnecxion = ConnexionFactory.FactoryGet
        Dim Con As Connection = Connect.Connecxion()

        Try

            Dim AdjVal As Decimal = AdjQty * m_Stock.Price_PRIC
            Dim AdjCost As Decimal = AdjQty * m_Stock.Cost
            Dim TransStock As IStock = StockFactory.FactoryGet


            TransStock.ReadSKU(Con, TransferSkuNumber)
            Dim TranAdjVal As Decimal = AdjQty * TransStock.Price_PRIC
            Dim TranAdjCost As Decimal = AdjQty * TransStock.Cost

            'Subtract the Reduce Pallette Item
            TransStock.OnHand_ONHA -= AdjQty
            TransStock.AdjustmentQty_MADQ1 -= AdjQty
            TransStock.AdjustmentValue_MADV1 -= TranAdjVal
            TransStock.TodayActivity_TACT = True

            'Increase the Good Pallette Item
            m_Stock.OnHand_ONHA += AdjQty
            m_Stock.AdjustmentQty_MADQ1 += AdjQty
            m_Stock.AdjustmentValue_MADV1 += AdjVal
            m_Stock.TodayActivity_TACT = True

            Trace.WriteLine("Saving Adjustment.", Application.ProductName & ".ProcessSaleOrders")

            'Create Stock Log Records
            Dim Stocklog As IStockLog = StockLogFactory.FactoryGet
            Stocklog.ReadStockLog(SkuNumber)

            Try

                Con.StartTransaction()

                'Setup and save Adjustment values
                Adjustment.SaveAdjustment(Con, AdjQty, AdjDate, SkuNumber, DRLNumber, Comment, SACode, m_Stock, Stocklog, EmployeId, TransStock.SKUN, TranAdjVal, TransStock.Price_PRIC)

                Stocklog.SaveStockLog(Con, CInt(AdjQty * -1), SACode, m_Stock, Adjustment, EmployeId, m_Stock.SKUN)
                Stocklog.SaveStockLog(Con, AdjQty, SACode, TransStock, Adjustment, EmployeId, m_Stock.SKUN)

                'Save STKMAS knob Changes
                m_Stock.Update(Con)
                TransStock.Update(Con)
                Con.CommitTransaction()

            Catch ex As Exception
                Con.RollbackTransaction()

            End Try

            Trace.WriteLine("Saved order.", Application.ProductName & ".Normal Update")
            Trace.WriteLine("Committed transaction at " & Now.ToLocalTime & ".", Application.ProductName & ".Normal Update")
            Return True
        Catch ex As Exception
            Return False
        Finally
            Con.Dispose()
        End Try

    End Function

    Public Function GetSku(ByVal SACode As ISaCode, ByVal SkuNumber As String, ByVal AdjDate As Date) As Boolean Implements IStockAdjustmentManager.GetSku

        Dim Connect As IConnecxion = ConnexionFactory.FactoryGet
        Dim Con As Connection = Connect.Connecxion()

        Try
            If SkuNumber = "000000" Then
                MessageBox.Show("Invalid Sku number.", "Invalid Sku", MessageBoxButtons.OK)
                Return False
            End If

            'Get the STKMAS Record needed for the Sku entered
            m_Stock.ReadSKU(Con, SkuNumber)
            If m_Stock.SKUN = String.Empty Then
                MessageBox.Show("Sku number not found in database.", "Invalid Sku", MessageBoxButtons.OK)
                Return False
            End If

            If SACode.CodeType = "T" And m_Stock.SaleTypeAttribute_SALT <> "G" Then
                MessageBox.Show("Not a valid Transfer Sku number.", "Invalid Sku", MessageBoxButtons.OK)
                Return False
            End If

            If m_Stock.DummyItem_ICAT Then
                MessageBox.Show("Dummy Item Invalid.", "Invalid Sku", MessageBoxButtons.OK)
                Return False
            End If

            If m_Stock.HODeleted_IDEL And m_Stock.OnHand_ONHA = 0 Then
                MessageBox.Show("Item is deleted by Head Office and has zero stock.", "Invalid Sku", MessageBoxButtons.OK)
                Return False
            End If

            Dim AllowedAdjustmentFactory As New AllowedAdjustmentFactory
            Dim AllowAdjustment As IAllowedAdjustment = (New AllowedAdjustmentFactory).GetImplementation
            If AllowAdjustment.IsAdjustmentAllowed(SACode, m_Stock, m_StockAdjustment, Con, SkuNumber, AdjDate) = False Then
                MessageBox.Show("This Adjustment type is not allowed for this sku.", "Invalid Sku", MessageBoxButtons.OK)
                Return False
            End If

            If m_StockAdjustment.AdjustmentDate_DATE1 = AdjDate AndAlso SACode.IsMarkdown = False AndAlso SACode.IsWriteOff = False Then
                MessageBox.Show("Stock Adjustment dated today already exists for sku entered.", "Adjustment Exists", MessageBoxButtons.OK)
                Return False
            End If

            Return True
        Catch
            'Con.RollbackTransaction()
            Return False
        Finally
            Con.Dispose()
        End Try
    End Function

    Public Function GetDRLAdjustment(ByVal SACode As ISaCode, _
                                     ByVal DRLNumber As String, _
                                     ByVal SkuNumber As String) As Boolean Implements IStockAdjustmentManager.GetDRLAdjustment

        If DRLNumber = "000000" Then
            MessageBox.Show("Invalid DRL Number.", "Invalid DRLNumber", MessageBoxButtons.OK)
            Return False
        End If

        m_DRL = DRLValidatorFactory.FactoryGet
        m_DRL.DRLNumber = DRLNumber





        Dim StockAdjustmentFixes As IStockAdjustmentFixes

        StockAdjustmentFixes = (New StockAdjustmentFixesFactory).GetImplementation()

        If StockAdjustmentFixes.ReadDRL(SACode, m_DRL, DRLNumber, SkuNumber) = False Then Return False


        If m_DRL.DRLType <> "0" Then
            MessageBox.Show("Invalid - DRL is not for a P/O.", "Invalid DRLNumber", MessageBoxButtons.OK)
            Return False
        End If

        If m_DRL.purBBCC = "A" Or m_DRL.supBBCC = "A" Then
            MessageBox.Show("Created from an alternative supplied order.", "Invalid DRLNumber", MessageBoxButtons.OK)
            Return False
        End If

        If m_DRL.purBBCC = "W" Or m_DRL.supBBCC = "W" Then
            MessageBox.Show("Created from a Warehouse supplied order.", "Invalid DRLNumber", MessageBoxButtons.OK)
            Return False
        End If

        If m_DRL.IsSentToHO = False And (m_DRL.purBBCC = "" Or m_DRL.supBBCC = "") Then
            MessageBox.Show("Direct delivery and not yet sent to H/O.", "Invalid DRLNumber", MessageBoxButtons.OK)
            Return False
        End If

        If StockAdjustment.ReversalCode_RCOD = "A" Then
            MessageBox.Show("Unable to maintain a reversed Adjustment.", "Invalid Adjustment", MessageBoxButtons.OK)
            Return False
        End If

        Return True
    End Function

    Public Function GetAdjustment(ByVal SACode As ISaCode, ByRef AdjQty As Integer, ByVal SkuNumber As String, ByVal StockAdjust As IAdjustment) As Boolean Implements IStockAdjustmentManager.GetAdjustment

        If SACode.IsWriteOff Then
            m_TypeOfAdjustment = "W"
        End If

        If SACode.IsMarkdown Then
            m_TypeOfAdjustment = "M"
        End If

        If AdjQty = 0 And StockAdjust.ExistsInDatabase = False Then  'We delete an adjustment with a zero qty in maintenance routine
            MessageBox.Show("Adjustment Quantity cannot be zero.", "Invalid Adjustment", MessageBoxButtons.OK)
            Return False
        End If

        If StockAdjust.Quantity_QUAN = AdjQty And StockAdjust.IsReversed = False Then
            MessageBox.Show("Adjustment Quantity cannot be the same quantity as before.", "Invalid Adjustment", MessageBoxButtons.OK)
            Return False
        End If

        If StockAdjust.Quantity_QUAN <> AdjQty And StockAdjust.IsReversed = True Then
            MessageBox.Show("Adjustment Quantity must be the same quantity as before, but reversed.", "Invalid Adjustment", MessageBoxButtons.OK)
            Return False
        End If

        If SACode.CodeSign = "-" Then
            AdjQty *= -1
        End If

        If SACode.CodeSign = "+" And AdjQty < 0 Then
            MessageBox.Show("Adjustment must be positive.", "Invalid Adjustment", MessageBoxButtons.OK)
            Return False
        End If

        If SACode.CodeSign = "-" And AdjQty > 0 Then
            MessageBox.Show("Negative adjustment - Do not enter sign.", "Invalid Adjustment", MessageBoxButtons.OK)
            Return False
        End If

        If m_TypeOfAdjustment = "W" And (m_Stock.WriteOffStock_WTFQ - AdjQty) < 0 Then
            If MessageBox.Show("Adjustment will make write off quantity negative. Do you wish to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                Return False
            End If
        End If

        If m_TypeOfAdjustment = "M" And (m_Stock.MarkdownQty_MDNQ - AdjQty) < 0 Then
            If MessageBox.Show("Adjustment will make Markdown quantity negative. Do you wish to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                Return False
            End If
        End If

        If SACode.CodeNumber = "04" Then

            If m_DRL.IsSKuInDRL(CInt(m_DRL.DRLNumber), SkuNumber) = False Then
                If MessageBox.Show("Sku Number entered is not on the DRL. Do you wish to continue?", "Warning", MessageBoxButtons.YesNo) = DialogResult.No Then
                    Return False
                End If
            End If







            'NOT REQUIRED
            '"Cannot negatively adjust...." message for F8 reversal not an issue; txtSkuAdjust control validating event not called, now readonly
            '"Cannot negatively adjust...." message for F7 maintenance not an issue since adjust existing adjustment

            'Dim StockAdjustmentFixes As IStockAdjustmentFixes

            'StockAdjustmentFixes = (New StockAdjustmentFixesFactory).GetImplementation()

            'If StockAdjustmentFixes.ValidAdjustmentQuantity(AdjQty, m_DRL.GetReceiptQty(m_DRL.DRLNumber, SkuNumber), StockAdjust.Quantity_QUAN) = False Then
            '    MessageBox.Show("Cannot negatively adjust greater than received quantity.", "Invalid Adjustment", MessageBoxButtons.OK)
            '    Return False
            'End If


            'ORIGINAL CODE
            'For DRL Adjustment - disallow negative adjustments if > than received 
            Dim ModQty As Integer = (AdjQty * -1)
            If ModQty > m_DRL.GetReceiptQty(CInt(m_DRL.DRLNumber), SkuNumber) Then
                MessageBox.Show("Cannot negatively adjust greater than received quantity.", "Invalid Adjustment", MessageBoxButtons.OK)
                Return False
            End If





        End If

        If (SACode.CodeSign = "+" Or SACode.CodeSign = "S") And AdjQty > 100 Then
            If MessageBox.Show("Adjustment quantity is more than 100, do you wish to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.No Then
                Return False
            End If
        End If

        Return True
    End Function

    Public Function GetStoreNumber() As String Implements IStockAdjustmentManager.GetStoreNumber
        Dim StoreNumber As String = m_Stock.GetStoreNumber()
        If CInt(StoreNumber) < 8000 Then
            StoreNumber = "8" & StoreNumber
        End If
        Return StoreNumber
    End Function

    Public Sub SetDescriptionForSkuNumber(ByVal container As ISkuNumberAndDescriptionContainer) Implements IStockAdjustmentManager.SetDescriptionForSkuNumber
        Dim skuNumberText As String = container.GetSkuNumberText()

        Dim descriptionText As String
        Dim skuDescriptionStyle As SkuDescriptionStyle

        Dim skuNumberLength As Integer = Len(skuNumberText)

        If skuNumberLength = GlobalConstants.SkuNumberTextLength Then
            Dim stock As Cts.Oasys.Core.Stock.Stock = Cts.Oasys.Core.Stock.Stock.GetStock(skuNumberText)
            If (stock IsNot Nothing) Then
                descriptionText = stock.Description
                skuDescriptionStyle = StockAdjustments.SkuDescriptionStyle.SkuNumberIsCorrect
            Else
                descriptionText = My.Resources.Messages.IncorectSKUNumberIsEntered
                skuDescriptionStyle = StockAdjustments.SkuDescriptionStyle.SkuNumberIsIncorrect
            End If
        Else
            descriptionText = CStr(IIf(skuNumberLength = 0, String.Empty, String.Format(My.Resources.Messages.PleaseEnterSKUNumber, GlobalConstants.SkuNumberTextLength)))
            skuDescriptionStyle = StockAdjustments.SkuDescriptionStyle.SkuNumberIsNotEntered
        End If

        container.SetSkuDescriptionText(descriptionText, skuDescriptionStyle)
    End Sub

#End Region

    Public Function GetCalculate(ByVal StartBalance As Integer, ByVal NewBalance As Integer, ByRef AdjQty As Integer) As Boolean

        AdjQty = NewBalance - StartBalance
        If AdjQty = 0 Then
            MessageBox.Show("Starting stock is the same as ending stock.", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If

        If AdjQty > 100 Or AdjQty < -100 Then
            If MessageBox.Show("Adjustment quantity is more than 100 or less than -100, do you want to reject ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Return False
            End If

        End If
    End Function

#Region "Private Procedures & Functions"

    Private Sub StockUpdate(ByVal SACode As ISaCode, ByVal Adjustment As IAdjustment, ByVal AdjDate As Date, ByVal AdjQty As Integer)

        Dim AdjVal As Decimal = AdjQty * m_Stock.Price_PRIC
        Dim AdjCost As Decimal = AdjQty * m_Stock.Cost

        If Adjustment.ExistsInDatabase Then 'Its an update so wee need to adjust the stock
            m_Stock.OnHand_ONHA -= CInt(Adjustment.Quantity_QUAN)
        End If

        m_Stock.OnHand_ONHA += AdjQty

        If SACode.MarkdownWriteoff = String.Empty Then
            m_Stock.AdjustmentQty_MADQ1 += AdjQty
            m_Stock.AdjustmentValue_MADV1 += AdjVal
        Else
            If SACode.MarkdownWriteoff.Substring(0, 1) = "W" Then
                If Adjustment.ExistsInDatabase Then
                    m_Stock.WriteOffStock_WTFQ += CInt(Adjustment.Quantity_QUAN)
                End If
                m_Stock.WriteOffStock_WTFQ -= AdjQty
            End If
            If SACode.MarkdownWriteoff.Substring(0, 1) = "M" Then
                If Adjustment.ExistsInDatabase Then
                    m_Stock.MarkdownQty_MDNQ += CInt(Adjustment.Quantity_QUAN)
                End If
                m_Stock.MarkdownQty_MDNQ -= AdjQty
            End If
        End If

        If SACode.CodeNumber = "02" Then
            m_Stock.CyclicalCount_MCCV1 -= AdjVal
            m_Stock.CycleDate_ADAT1 = AdjDate
            m_Stock.AdjustmentQty_AQ021 += AdjQty
            m_Stock.AdjustmentValue_AV021 += AdjVal
        End If

        m_Stock.TodayActivity_TACT = True

    End Sub

#End Region

End Class

Public Class AllowedAdjustmentNew

    Implements IAllowedAdjustment

    Public Function IsAdjustmentAllowed(ByVal SACode As ISaCode, ByVal m_Stock As IStock, ByVal m_StockAdjustment As IAdjustment, ByVal Con As Cts.Oasys.Data.Connection, ByVal SkuNumber As String, ByVal AdjDate As Date) As Boolean Implements IAllowedAdjustment.IsAdjustmentAllowed
        Dim result As Boolean = False

        If SACode.AllowCodes.Length = 0 Then Return True

        Dim AdjustCodesAllowed() As String = SACode.AllowCodes.Split(","c)

        For index As Integer = 0 To AdjustCodesAllowed.Count - 1
            If AdjustCodesAllowed(index) = m_Stock.AllowAdjustments_AADJ Then
                result = True
                Exit For
            End If
        Next index

        Return result
    End Function

End Class