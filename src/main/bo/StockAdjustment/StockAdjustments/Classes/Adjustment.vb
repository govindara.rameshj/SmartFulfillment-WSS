﻿Public Class Adjustment
    Inherits Base
    Implements IAdjustment

#Region "Private Variables"

    Private m_AdjustmentDate As Date = Nothing
    Private m_Code As String = String.Empty
    Private m_SKUN As String = String.Empty
    Private m_SEQN As String = String.Empty
    Private m_AmendId As Integer = 0
    Private m_EmployeeInitials As String = String.Empty
    Private m_Department As String = String.Empty
    Private m_StartingStock As Decimal = 0@
    Private m_Quantity As Decimal = 0@
    Private m_Price As Decimal = 0@
    Private m_Cost As Decimal = 0@
    Private m_IsSentToHO As Boolean = False
    Private m_AdjustType As String = String.Empty
    Private m_Comment As String = String.Empty
    Private m_DeliveryNumber As String = "000000"
    Private m_ReversalCode As String = String.Empty
    Private m_MarkDownOrWriteOff As String = String.Empty
    Private m_WriteOffAuthority As String = "0"
    Private m_WriteOffAuthorityDate As Date = Nothing
    Private m_PeriodId As Integer = 0
    Private m_TransferSku As String = "000000"
    Private m_TransferValue As Decimal = 0@
    Private m_TransferStart As Decimal = 0@
    Private m_TransferPrice As Decimal = 0@
    Private m_IsReversed As Boolean = False
    Private m_ExistsInDatabase As Boolean = False
    Private m_RTI As String = "N"
    Private m_EndingValue As Decimal = 0

#End Region

#Region "Properties"

    <ColumnMapping("AdjustmentDate")> Public Property AdjustmentDate() As Date Implements IAdjustment.AdjustmentDate_DATE1
        Get
            Return m_AdjustmentDate
        End Get
        Set(ByVal value As Date)
            m_AdjustmentDate = value
        End Set
    End Property

    <ColumnMapping("AdjustmentType")> Public Property AdjustType() As String Implements IAdjustment.AdjustType
        Get
            Return m_AdjustType
        End Get
        Set(ByVal value As String)
            m_AdjustType = value
        End Set
    End Property

    <ColumnMapping("AmendId")> Public Property AmendId() As Integer Implements IAdjustment.AmendId
        Get
            Return m_AmendId
        End Get
        Set(ByVal value As Integer)
            m_AmendId = value
        End Set
    End Property

    <ColumnMapping("AdjustmentCode")> Public Property Code() As String Implements IAdjustment.Code
        Get
            Return m_Code
        End Get
        Set(ByVal value As String)
            m_Code = value
        End Set
    End Property

    <ColumnMapping("Comment")> Public Property Comment() As String Implements IAdjustment.Comment_INFO
        Get
            Return m_Comment
        End Get
        Set(ByVal value As String)
            m_Comment = value
        End Set
    End Property

    <ColumnMapping("AdjustmentCost")> Public Property Cost() As Decimal Implements IAdjustment.Cost
        Get
            Return m_Cost
        End Get
        Set(ByVal value As Decimal)
            m_Cost = value
        End Set
    End Property

    <ColumnMapping("DRLNumber")> Public Property DeliveryNumber() As String Implements IAdjustment.DeliveryNumber_DRLN
        Get
            Return m_DeliveryNumber
        End Get
        Set(ByVal value As String)
            m_DeliveryNumber = value
        End Set
    End Property

    <ColumnMapping("Department")> Public Property Department() As String Implements IAdjustment.Department
        Get
            Return m_Department
        End Get
        Set(ByVal value As String)
            m_Department = value
        End Set
    End Property

    <ColumnMapping("Initials")> Public Property EmployeeInitials() As String Implements IAdjustment.EmployeeInitials_INIT
        Get
            Return m_EmployeeInitials
        End Get
        Set(ByVal value As String)
            m_EmployeeInitials = value
        End Set
    End Property

    <ColumnMapping("ReversalCode")> Public Property ReversalCode() As String Implements IAdjustment.ReversalCode_RCOD
        Get
            Return m_ReversalCode
        End Get
        Set(ByVal value As String)
            m_ReversalCode = value
        End Set
    End Property

    <ColumnMapping("IsReversed")> Public Property IsReversed() As Boolean Implements IAdjustment.IsReversed
        Get
            Return m_IsReversed
        End Get
        Set(ByVal value As Boolean)
            m_IsReversed = value
        End Set
    End Property

    <ColumnMapping("IsSentToHO")> Public Property IsSentToHO() As Boolean Implements IAdjustment.IsSentToHO_COMM
        Get
            Return m_IsSentToHO
        End Get
        Set(ByVal value As Boolean)
            m_IsSentToHO = value
        End Set
    End Property

    <ColumnMapping("MarkDownWriteOff")> Public Property MarkDownOrWriteOff() As String Implements IAdjustment.MarkDownOrWriteOff_MOWT
        Get
            Return m_MarkDownOrWriteOff
        End Get
        Set(ByVal value As String)
            m_MarkDownOrWriteOff = value
        End Set
    End Property

    <ColumnMapping("PeriodId")> Public Property PeriodId() As Integer Implements IAdjustment.PeriodId
        Get
            Return m_PeriodId
        End Get
        Set(ByVal value As Integer)
            m_PeriodId = value
        End Set
    End Property

    <ColumnMapping("AdjustmentPrice")> Public Property Price() As Decimal Implements IAdjustment.Price
        Get
            Return m_Price
        End Get
        Set(ByVal value As Decimal)
            m_Price = value
        End Set
    End Property

    <ColumnMapping("AdjustmentQty")> Public Property Quantity() As Decimal Implements IAdjustment.Quantity_QUAN
        Get
            Return m_Quantity
        End Get
        Set(ByVal value As Decimal)
            m_Quantity = value
        End Set
    End Property

    <ColumnMapping("RTI")> Public Property RTI() As String Implements IAdjustment.RTI
        Get
            Return m_RTI
        End Get
        Set(ByVal value As String)
            m_RTI = value
        End Set
    End Property

    <ColumnMapping("SEQN")> Public Property SEQN() As String Implements IAdjustment.SEQN
        Get
            Return m_SEQN
        End Get
        Set(ByVal value As String)
            m_SEQN = value
        End Set
    End Property

    <ColumnMapping("SKUN")> Public Property SKUN() As String Implements IAdjustment.SKUN
        Get
            Return m_SKUN
        End Get
        Set(ByVal value As String)
            m_SKUN = value
        End Set
    End Property

    <ColumnMapping("StartingStock")> Public Property StartingStock() As Decimal Implements IAdjustment.StartingStock_SSTK
        Get
            Return m_StartingStock
        End Get
        Set(ByVal value As Decimal)
            m_StartingStock = value
        End Set
    End Property

    <ColumnMapping("TransferSku")> Public Property TransferSku_TSKU() As String Implements IAdjustment.TransferSku_TSKU
        Get
            Return m_TransferSku
        End Get
        Set(ByVal value As String)
            m_TransferSku = value
        End Set
    End Property

    <ColumnMapping("TransferValue")> Public Property TransferValue() As Decimal Implements IAdjustment.TransferValue
        Get
            Return m_TransferValue
        End Get
        Set(ByVal value As Decimal)
            m_TransferValue = value
        End Set
    End Property

    <ColumnMapping("TransferPrice")> Public Property TransferPrice() As Decimal Implements IAdjustment.TransferPrice
        Get
            Return m_TransferPrice
        End Get
        Set(ByVal value As Decimal)
            m_TransferPrice = value
        End Set
    End Property

    <ColumnMapping("TransferStart")> Public Property TransferStart() As Decimal Implements IAdjustment.TransferStart
        Get
            Return m_TransferStart
        End Get
        Set(ByVal value As Decimal)
            m_TransferStart = value
        End Set
    End Property

    <ColumnMapping("WriteOffAuthorised")> Public Property WriteOffAuthority() As String Implements IAdjustment.WriteOffAuthority_WAUT
        Get
            Return m_WriteOffAuthority
        End Get
        Set(ByVal value As String)
            m_WriteOffAuthority = value
        End Set
    End Property

    <ColumnMapping("DateAuthorised")> Public Property WriteOffAuthorityDate() As Date Implements IAdjustment.WriteOffAuthorityDate_DAUT
        Get
            Return m_WriteOffAuthorityDate
        End Get
        Set(ByVal value As Date)
            m_WriteOffAuthorityDate = value
        End Set
    End Property

    Public Property ExistsInDatabase() As Boolean Implements IAdjustment.ExistsInDatabase
        Get
            Return m_ExistsInDatabase
        End Get
        Set(ByVal value As Boolean)
            m_ExistsInDatabase = value
        End Set
    End Property

    <ColumnMapping("EndingStock")> Public Property EndingStock() As Decimal Implements IAdjustment.EndingStock

        Get
            Return m_StartingStock + m_Quantity
        End Get
        Set(ByVal value As Decimal)
            m_EndingValue = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region "Interface"

    Public Overloads Sub LoadFromRow(ByVal Dr As DataRow) Implements IAdjustment.LoadFromRow
        Me.Load(Dr)
    End Sub

    Public Function CalculateAdjustment() As Boolean Implements IAdjustment.CalculateAdjustment

    End Function

    Public Overridable Function SaveAdjustment(ByRef Con As Connection, ByVal AdjQty As Integer, ByVal AdjDate As Date, ByVal SkuNumber As String, ByVal DRLNumber As String, ByVal Comment As String, ByVal SACode As ISaCode, ByVal Stock As IStock, ByVal StockLog As IStockLog, ByVal EmployeeId As Integer, Optional ByVal TransferSku As String = "", Optional ByVal TransferValue As Decimal = 0@, Optional ByVal TransferPrice As Decimal = 0@, Optional ByVal TransferStart As Decimal = 0, Optional ByVal PicCountAcceptance As Boolean = False, Optional ByVal PicCountAcceptanceCode53 As Boolean = False) As Boolean Implements IAdjustment.SaveAdjustment

        Dim AdjustmentRep As IAdjustmentRepository = AdjustmentRepositoryFactory.FactoryGet
        Dim ReadAdjustmentImplementation As IAdjustmentRepository
        Dim Adjustment As IAdjustmentRepository

        m_Code = SACode.CodeNumber
        m_AdjustmentDate = AdjDate.Date
        m_SKUN = SkuNumber
        m_Quantity = AdjQty
        m_EmployeeInitials = GetEmployeeInitials(EmployeeId)
        m_Department = Stock.Department

        If StockLog.SKUN <> String.Empty Then 'Make sure there was a stocklog record
            m_StartingStock = StockLog.EndingStock_ESTK
        End If

        If PicCountAcceptance Then
            ReadAdjustmentImplementation = (New ReadAdjustmentPicCountFactory).GetImplementation()
            Adjustment = ReadAdjustmentImplementation


            Dim RepositoryOverload As IAdjustmentRepositoryOverloadDecide

            RepositoryOverload = (New AdjustmentRepositoryOverloadDecideFactory).GetImplementation
            m_SEQN = RepositoryOverload.GetNextSequenceNumberForSkunCodeDate(Adjustment, Con, SKUN, m_Code, AdjDate)

            If m_SEQN = String.Empty Then
                If m_ExistsInDatabase = False Then
                    If m_SEQN = String.Empty Then
                        m_SEQN = "00"
                    Else
                        Dim seqn As Integer = CInt(m_SEQN) + 1
                        m_SEQN = seqn.ToString.PadLeft(2, "0"c)
                    End If
                End If
            End If
        Else
            If m_ExistsInDatabase = False Then
                If m_SEQN = String.Empty Then
                    m_SEQN = "00"
                Else
                    Dim seqn As Integer = CInt(m_SEQN) + 1
                    m_SEQN = seqn.ToString.PadLeft(2, "0"c)
                End If
            End If
        End If

        m_Price = Stock.Price_PRIC
        m_Cost = Stock.Cost
        m_IsSentToHO = False
        m_AdjustType = SACode.CodeType

        If Comment <> String.Empty Then
            m_Comment = Comment
        End If

        If DRLNumber <> String.Empty Then
            m_DeliveryNumber = DRLNumber
        End If

        'm_ReversalCode = String.Empty
        If SACode.MarkdownWriteoff.Length > 0 Then
            m_MarkDownOrWriteOff = SACode.MarkdownWriteoff.Substring(0, 1)
            If PicCountAcceptanceCode53 Then
                Dim stockAdjPrice As IStockAdjustmentPrice = (New StockAdjustmentPriceFactory).GetImplementation
                m_Price = stockAdjPrice.ResetPrice(m_Price)
            End If
        Else
            m_MarkDownOrWriteOff = String.Empty
        End If

        m_RTI = "N"

        If TransferSku <> String.Empty Then
            m_TransferSku = TransferSku
            m_TransferPrice = TransferPrice
            m_TransferValue = ((m_Price * AdjQty) - (TransferPrice * AdjQty))
        End If

        Return AdjustmentRep.SaveAdjustment(Me, Con)
    End Function

    Public Function UpdateAdjustment(ByRef Con As Connection) As Boolean Implements IAdjustment.UpdateAdjustment

        Dim Repository As IAdjustmentRepository = AdjustmentRepositoryFactory.FactoryGet

        Repository = AdjustmentRepositoryFactory.FactoryGet

        Return Repository.SaveAdjustment(Me, Con)

    End Function

    Public Function ReadAdjustment(ByRef Con As Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As Boolean Implements IAdjustment.ReadAdjustment
        Dim ReadAdjustmentImplementation As IAdjustmentRepository = (New ReadAdjustmentFactory).GetImplementation()

        Dim Adjustment As IAdjustmentRepository = ReadAdjustmentImplementation
        Dim dr As DataRow = Adjustment.ReadAdjustmentExcludingMarkdownSales(Con, Skun, AdjDate, CodeNumber)
        If dr IsNot Nothing Then
            Me.Load(dr)
            m_ExistsInDatabase = True
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Validate(ByVal stock As IStock) As Boolean Implements IAdjustment.Validate

    End Function

    Public Function ReadAdjustmentDate() As Date Implements IAdjustment.ReadAdjustmentDate
        Dim Adjustment As IAdjustmentRepository = AdjustmentRepositoryFactory.FactoryGet
        Dim dr As DataRow = Adjustment.ReadAdjustmentDate()
        If dr IsNot Nothing Then
            Return CDate(dr.Item("TommorowsDate"))
        End If
    End Function

    Public Function GetEmployeeInitials(ByVal Id As Integer) As String Implements IAdjustment.GetEmployeeInitials
        Dim Adjustment As IAdjustmentRepository = AdjustmentRepositoryFactory.FactoryGet
        Return Adjustment.GetEmployeeInitials(Id)
    End Function

    Public Sub MakeEqual(ByVal Value As IAdjustment) Implements IAdjustment.MakeEqual

        With Me

            .AdjustmentDate = Value.AdjustmentDate_DATE1
            .Code = Value.Code
            .SKUN = Value.SKUN
            .SEQN = Value.SEQN
            .AmendId = Value.AmendId
            .EmployeeInitials = Value.EmployeeInitials_INIT
            .Department = Value.Department
            .StartingStock = Value.StartingStock_SSTK
            .Quantity = Value.Quantity_QUAN
            .Price = Value.Price
            .Cost = Value.Cost
            .IsSentToHO = Value.IsSentToHO_COMM
            .AdjustType = Value.AdjustType
            .Comment = Value.Comment_INFO
            .DeliveryNumber = Value.DeliveryNumber_DRLN
            .ReversalCode = Value.ReversalCode_RCOD
            .MarkDownOrWriteOff = Value.MarkDownOrWriteOff_MOWT
            .WriteOffAuthority = Value.WriteOffAuthority_WAUT
            .WriteOffAuthorityDate = Value.WriteOffAuthorityDate_DAUT
            .PeriodId = Value.PeriodId
            .TransferSku_TSKU = Value.TransferSku_TSKU
            .TransferValue = Value.TransferValue
            .TransferStart = Value.TransferStart
            .TransferPrice = Value.TransferPrice
            .IsReversed = Value.IsReversed
            .RTI = Value.RTI
            .ExistsInDatabase = Value.ExistsInDatabase
            .EndingStock = Value.EndingStock

        End With

    End Sub

#End Region

End Class