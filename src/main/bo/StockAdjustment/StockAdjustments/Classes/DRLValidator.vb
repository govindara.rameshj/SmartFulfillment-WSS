﻿Option Strict On

Public Class DRLValidator

    Inherits Cts.Oasys.Core.Base

    Implements IDRLValidator

    Private m_DRLType As String = String.Empty
    Private m_DRLNumber As String = String.Empty
    Private m_purBBCC As String = String.Empty
    Private m_supBBCC As String = String.Empty
    Private m_IsReversible As Boolean = Nothing
    Private m_IsSentToHO As Boolean = False

#Region "Properties"


    <ColumnMapping("IsSentToHO")> Public Property IsSentToHO() As Boolean Implements IDRLValidator.IsSentToHO
        Get
            Return m_IsSentToHO
        End Get
        Set(ByVal value As Boolean)
            m_IsSentToHO = value
        End Set
    End Property

    <ColumnMapping("DRLNumber")> Public Property DRLNumber() As String Implements IDRLValidator.DRLNumber
        Get
            Return m_DRLNumber
        End Get
        Set(ByVal value As String)
            m_DRLNumber = value
        End Set
    End Property

    <ColumnMapping("DRLType")> Public Property DRLType() As String Implements IDRLValidator.DRLType
        Get
            Return m_DRLType
        End Get
        Set(ByVal value As String)
            m_DRLType = value
        End Set
    End Property

    <ColumnMapping("purBBCC")> Public Property purBBCC() As String Implements IDRLValidator.purBBCC
        Get
            Return m_purBBCC
        End Get
        Set(ByVal value As String)
            m_purBBCC = value
        End Set
    End Property

    <ColumnMapping("supBBCC")> Public Property supBBCC() As String Implements IDRLValidator.supBBCC
        Get
            Return m_supBBCC
        End Get
        Set(ByVal value As String)
            m_supBBCC = value
        End Set
    End Property

    <ColumnMapping("IsReversible")> Public Property IsReversible() As Boolean Implements IDRLValidator.IsReversible
        Get
            Return m_IsReversible
        End Get
        Set(ByVal value As Boolean)
            m_IsReversible = value
        End Set
    End Property

#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Public Function ReadDRL(ByVal DRLNumber As String) As Boolean Implements IDRLValidator.ReadDRL

        Dim DataAccess As IDRLValidatorRepository = DRLValidatorRepositoryFactory.FactoryGet
        Dim dr As DataRow = DataAccess.ReadDRL(DRLNumber)
        If dr IsNot Nothing Then
            Me.Load(dr)
            Return True
        Else
            Return False
        End If

    End Function

    Public Function IsSKuInDRL(ByVal DRLNumber As Integer, ByVal SkuNumber As String) As Boolean Implements IDRLValidator.IsSKuInDRL

        Dim DataAccess As IDRLValidatorRepository = DRLValidatorRepositoryFactory.FactoryGet
        Return DataAccess.IsSkuInDRL(DRLNumber.ToString, SkuNumber)

    End Function

    Public Function GetReceiptQty(ByVal DRLNumber As Integer, ByVal SkuNumber As String) As Integer Implements IDRLValidator.GetReceiptQty

        Dim DataAccess As IDRLValidatorRepository = DRLValidatorRepositoryFactory.FactoryGet
        Return DataAccess.GetReceiptQty(DRLNumber.ToString, SkuNumber)

    End Function

End Class