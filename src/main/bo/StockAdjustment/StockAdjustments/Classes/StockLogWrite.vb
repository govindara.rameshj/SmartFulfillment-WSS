﻿Option Strict On
Option Explicit On

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Dhanesh Ramachandran
' Date     : 22/08/2011
' Referral : RF0865
' Notes    : Added for Stock Movements Detail Report to display correct adjustment code
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Class StockLogWrite
    Implements IStockLogWrite

    Public Function StockLogWrite() As Boolean Implements IStockLogWrite.StockLogWrite
        'do nothing
        Return False
    End Function


End Class
Public Class StockLogWriteNew
    Implements IStockLogWrite

    Public Function StockLogWrite() As Boolean Implements IStockLogWrite.StockLogWrite
        Return True
    End Function


End Class