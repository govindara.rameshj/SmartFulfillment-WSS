﻿Option Strict On

Public Class StockAdjustmentFixesExisting
    Implements IStockAdjustmentFixes

#Region "Create Form Fixes"

    Private _NextSequenceNo As String

#Region "Properties"

    Public WriteOnly Property AllCodeFourStockAdjustments() As IAdjustments Implements IStockAdjustmentFixes.AllCodeFourStockAdjustments

        Set(ByVal value As IAdjustments)

            'do nothing

        End Set

    End Property

#End Region

#Region "General"

    Public Function GetAdjustment(ByVal StockAdjustmentCode As ISaCode, _
                                  ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                  ByRef AdjustmentQuantity As Integer, _
                                  ByVal SkuNumber As String, _
                                  ByVal StockAdjustment As IAdjustment) As Boolean Implements IStockAdjustmentFixes.GetAdjustment

        Return StockAdjustmentManager.GetAdjustment(StockAdjustmentCode, AdjustmentQuantity, SkuNumber, StockAdjustment)

    End Function

    Public Function GetDrlAdjustment(ByVal StockAdjustmentCode As ISaCode, _
                                     ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                     ByRef DailyReceiverListingTextControl As DevExpress.XtraEditors.TextEdit, _
                                     ByVal DrlNumber As String, _
                                     ByVal SkuNumber As String) As Boolean Implements IStockAdjustmentFixes.GetDrlAdjustment

        Return StockAdjustmentManager.GetDRLAdjustment(StockAdjustmentCode, _
                                                       DailyReceiverListingTextControl.EditValue.ToString, _
                                                       SkuNumber)

        'existing code
        'If _StockAdjustManager.GetDRLAdjustment(_SaCode, txtDrlNumber.EditValue) = False Then

    End Function

    Public Sub CreateOrMaintainOrReverseAdjustmentOptions(ByVal StockAdjustmentCode As ISaCode, _
                                                          ByVal SkuPanelVisibility As Boolean, _
                                                          ByVal DevExpressGridGroupRow As Boolean, _
                                                          ByVal StockAdjustmentAmendID As Integer, _
                                                          ByVal StockAdjustment As IAdjustment, _
                                                          ByRef CreateAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef MaintainAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton) Implements IStockAdjustmentFixes.CreateOrMaintainOrReverseAdjustmentOptions

        If (Not SkuPanelVisibility) AndAlso (Not DevExpressGridGroupRow) AndAlso (StockAdjustmentAmendID = 0) Then
            MaintainAdjustmentButton.Enabled = True
            ReverseAdjustmentButton.Enabled = True
        Else
            MaintainAdjustmentButton.Enabled = False
            ReverseAdjustmentButton.Enabled = False
        End If

        'existing code
        'If (Not pnlSku.Visible) AndAlso (Not xgvAdjusts.IsGroupRow(e.FocusedRowHandle)) AndAlso (xgvAdjusts.GetFocusedRowCellValue("AmendId") = 0) Then
        '    btnMaintain.Enabled = True
        '    btnReverse.Enabled = True
        'Else
        '    btnMaintain.Enabled = False
        '    btnReverse.Enabled = False
        'End If

    End Sub

    Public Function DailyReceiverListingNumber(ByVal StockAdjustmentCode As ISaCode, _
                                               ByVal DrlNumber As String) As String Implements IStockAdjustmentFixes.DailyReceiverListingNumber

        Return DrlNumber

        'existing code
        'txtDrlNumber.EditValue = _SpecificStockAdjustment.DeliveryNumber_DRLN

    End Function

    Public Sub HighestSequenceNo(ByVal StockAdjustmentCode As ISaCode, _
                                 ByVal StockAdjustmentCollection As IAdjustments) Implements IStockAdjustmentFixes.HighestSequenceNo

        'do nothing

    End Sub

    Public Sub DisableReverseAdjustmentButton(ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton) Implements IStockAdjustmentFixes.DisableReverseAdjustmentButton

        'do nothing

    End Sub

#End Region

#Region "Create Adjustment"

    Public Sub CreateAdjustmentCodeFourOnly(ByVal StockAdjustmentCode As ISaCode, _
                                            ByVal StockAdjustmentManager As IStockAdjustmentManager, _
                                            ByRef StockAdjustment As IAdjustment) Implements IStockAdjustmentFixes.CreateAdjustmentCodeFourOnly

        'do nothing 

    End Sub

#End Region

#Region "Reverse Adjustment"

    Public Sub CodeFourReverseAdjustCancel(ByVal StockAdjustmentCode As ISaCode, _
                                           ByRef StockAdjustment As IAdjustment) Implements IStockAdjustmentFixes.CodeFourReverseAdjustCancel

        'do nothing

    End Sub

    Public Sub CodeFourReverseAdjustCompletedMessage(ByVal StockAdjustmentCode As ISaCode, _
                                                     ByVal StockAdjustment As IAdjustment, _
                                                     ByVal EventArguement As System.Windows.Forms.KeyPressEventArgs, _
                                                     ByRef GridView As DevExpress.XtraGrid.Views.Grid.GridView) Implements IStockAdjustmentFixes.CodeFourReverseAdjustCompletedMessage

        'set week number and redraw to set back colour of cell
        If EventArguement.KeyChar = ChrW(Keys.Enter) Then
            GridView.Invalidate()
        End If

    End Sub

    Public Function CodeFourReverseAdjustEndStock(ByVal StockAdjustmentCode As ISaCode, _
                                                  ByVal StockAdjustment As IAdjustment, _
                                                  ByRef StartingStockTextControl As DevExpress.XtraEditors.TextEdit, _
                                                  ByRef AdjustmentValueTextControl As DevExpress.XtraEditors.TextEdit) As Decimal Implements IStockAdjustmentFixes.CodeFourReverseAdjustEndStock

        Return CType(StartingStockTextControl.EditValue, Decimal) - CType(AdjustmentValueTextControl.EditValue, Decimal)

        'existing code
        'txtSkuEnd.EditValue = txtSkuStart.EditValue - txtSkuAdjust.EditValue

    End Function

#End Region

#Region "Grid Event(s)"

    Public Sub GridControlSelectionChangedEvent(ByVal StockAdjustmentCode As ISaCode, _
                                                ByRef SelectionChangedEvent As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler, _
                                                ByRef GridControl As DevExpress.XtraGrid.GridControl, _
                                                ByRef GridView As DevExpress.XtraGrid.Views.Grid.GridView) Implements IStockAdjustmentFixes.GridControlSelectionChangedEvent

        'do nothing

    End Sub

    Public Sub GridControlRefreshDataSourceEvent(ByVal StockAdjustmentCode As ISaCode, _
                                                 ByRef StockAdjustmentCollection As IAdjustments, _
                                                 ByRef GridControl As DevExpress.XtraGrid.GridControl) Implements IStockAdjustmentFixes.GridControlRefreshDataSourceEvent

        'do nothing

    End Sub

#End Region

#Region "Daily Receiver Listing Control Event"

    Public Sub DailyReceiverListingControlValidatingEvent(ByVal StockAdjustmentCode As ISaCode, _
                                                          ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                                          ByRef SpecificStockAdjustment As IAdjustment, _
                                                          ByRef StockAdjustmentCollection As IAdjustments, _
                                                          ByVal SkuNumber As String, _
                                                          ByVal CancelSelected As Boolean, _
                                                          ByRef CreateAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef MaintainAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef DailyReceiverListingTextControl As DevExpress.XtraEditors.TextEdit, _
                                                          ByRef DailyReceiverListingEventArguement As System.ComponentModel.CancelEventArgs) Implements IStockAdjustmentFixes.DailyReceiverListingControlValidatingEvent

        If CancelSelected = True Then Exit Sub

        If StockAdjustmentManager.GetDRLAdjustment(StockAdjustmentCode, _
                                                   DailyReceiverListingTextControl.Text, SkuNumber) = False Then

            ' drl does not exist
            SpecificStockAdjustment = Nothing
            DailyReceiverListingTextControl.SelectAll()
            DailyReceiverListingEventArguement.Cancel = True

        Else

            ' drl exist
            CreateAdjustmentButton.Visible = True
            CreateAdjustmentButton.Enabled = True
            CreateAdjustmentButton.Focus()

        End If

        'existing code
        'If _CancelSelected = True Then Exit Sub

        'If _StockAdjustmentManager.GetDRLAdjustment(StockAdjustmentCode, txtDrlNumber.Text) = False Then
        '    ' DRL not found
        '    _SpecificStockAdjustment = Nothing
        '    txtDrlNumber.SelectAll()
        '    e.Cancel = True
        'Else
        '    ' DRL found
        '    btnCreate.Visible = True
        '    btnCreate.Enabled = True
        '    btnCreate.Focus()
        'End If

    End Sub

#End Region

#End Region

#Region "Stock Adjustment Manager Fixes"

    Public Function ReadDRL(ByVal StockAdjustmentCode As ISaCode, _
                            ByRef Validator As IDRLValidator, _
                            ByVal DrlNumber As String, _
                            ByVal SkuNumber As String) As Boolean Implements IStockAdjustmentFixes.ReadDRL

        ReadDRL = True

        If Validator.ReadDRL(DrlNumber) = False Then

            MessageBox.Show("DRL Number does not exist.", "Invalid DRLNumber", MessageBoxButtons.OK)
            ReadDRL = False

        End If

        'existing code
        'If StockAdjustmentFixes.ReadDRL(SACode, m_DRL, DrlNumber, SkuNumber) = False Then
        '    MessageBox.Show("DRL Number does not exist.", "Invalid DRLNumber", MessageBoxButtons.OK)
        '    Return False
        'End If

    End Function

    Public Overridable Function PersistNormalAdjustments(ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                                         ByVal StockAdjustmentCode As ISaCode, _
                                                         ByRef ActualStockAdjustment As IAdjustment, _
                                                         ByRef ExistingStockAdjustment As IAdjustment, _
                                                         ByVal SkuNumber As String, _
                                                         ByVal AdjustmentDate As Date, _
                                                         ByVal DRLNumber As String, _
                                                         ByRef AdjustmentQuantity As Integer, _
                                                         ByVal Comment As String, _
                                                         ByVal EmployeeID As Integer) As Boolean Implements IStockAdjustmentFixes.PersistNormalAdjustments

        Return StockAdjustmentManager.NormalAdjustment(StockAdjustmentCode, _
                                                       ActualStockAdjustment, _
                                                       SkuNumber, _
                                                       AdjustmentDate, _
                                                       DRLNumber, _
                                                       AdjustmentQuantity, _
                                                       Comment, _
                                                       EmployeeID)

    End Function

    Public Sub ConfigureExistingAdjustment(ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                           ByVal StockAdjustmentCode As ISaCode, _
                                           ByRef Source As IAdjustment, _
                                           ByRef Target As IAdjustment) Implements IStockAdjustmentFixes.ConfigureExistingAdjustment

        'do nothing

    End Sub

#End Region

    Public Sub ConfigureNewAdjustment(ByRef Value As IAdjustment) Implements IStockAdjustmentFixes.ConfigureNewAdjustment

        Value.ExistsInDatabase = False
        Value.Quantity_QUAN *= -1 'Reverse this adjustment
        Value.IsReversed = True
        Value.ReversalCode_RCOD = "A"

    End Sub

    'NOT REQUIRED
    '"Cannot negatively adjust...." message for F8 reversal not an issue; txtSkuAdjust control validating event not called, now readonly
    '"Cannot negatively adjust...." message for F7 maintenance not an issue since adjust existing adjustment

    'Public Function ValidAdjustmentQuantity(ByVal AdjustmentQuantity As Integer, _
    '                                        ByVal ReceiptQuantity As Integer, _
    '                                        ByVal ExistingAdjustmentQuantity As Integer) As Boolean Implements IStockAdjustmentFixes.ValidAdjustmentQuantity

    '    Dim ModQty As Integer = (AdjustmentQuantity * -1)

    '    If ModQty > ReceiptQuantity Then
    '        MessageBox.Show("Cannot negatively adjust greater than received quantity.", "Invalid Adjustment", MessageBoxButtons.OK)
    '        Return False
    '    End If

    '    Return True

    '    'existing code

    '    'For DRL Adjustment - disallow negative adjustments if > than received 
    '    'Dim ModQty As Integer = (AdjQty * -1)
    '    'If ModQty > m_DRL.GetReceiptQty(m_DRL.DRLNumber, SkuNumber) Then
    '    '    MessageBox.Show("Cannot negatively adjust greater than received quantity.", "Invalid Adjustment", MessageBoxButtons.OK)
    '    '    Return False
    '    'End If

    'End Function

End Class