﻿Option Strict On

Public Class StockAdjustmentFixes
    Implements IStockAdjustmentFixes

#Region "Create Form Fixes"

    Private _NextSequenceNo As String
    Private _AllCodeFourStockAdjustments As IAdjustments

#Region "Properties"

    Public WriteOnly Property AllCodeFourStockAdjustments() As IAdjustments Implements IStockAdjustmentFixes.AllCodeFourStockAdjustments

        Set(ByVal value As IAdjustments)

            _AllCodeFourStockAdjustments = AdjustmentsFactory.FactoryGet
            _AllCodeFourStockAdjustments = value

        End Set

    End Property

#End Region

#Region "General"

    Public Function GetAdjustment(ByVal StockAdjustmentCode As ISaCode, _
                                  ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                  ByRef AdjustmentQuantity As Integer, _
                                  ByVal SkuNumber As String, _
                                  ByVal StockAdjustment As IAdjustment) As Boolean Implements IStockAdjustmentFixes.GetAdjustment

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"
                If StockAdjustment.DeliveryNumber_DRLN = "000000" Then
                    'new adjustment
                    Return StockAdjustmentManager.GetAdjustment(StockAdjustmentCode, AdjustmentQuantity, SkuNumber, StockAdjustment)

                Else
                    'existing adjustment
                    Return StockAdjustmentManager.GetDRLAdjustment(StockAdjustmentCode, _
                                                                   StockAdjustment.DeliveryNumber_DRLN, SkuNumber) _
                           AndAlso _
                           StockAdjustmentManager.GetAdjustment(StockAdjustmentCode, AdjustmentQuantity, SkuNumber, StockAdjustment)
                End If

            Case Else
                Return StockAdjustmentManager.GetAdjustment(StockAdjustmentCode, AdjustmentQuantity, SkuNumber, StockAdjustment)

        End Select

    End Function

    Public Function GetDrlAdjustment(ByVal StockAdjustmentCode As ISaCode, _
                                     ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                     ByRef DailyReceiverListingTextControl As DevExpress.XtraEditors.TextEdit, _
                                     ByVal DrlNumber As String, _
                                     ByVal SkuNumber As String) As Boolean Implements IStockAdjustmentFixes.GetDrlAdjustment

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"
                Return StockAdjustmentManager.GetDRLAdjustment(StockAdjustmentCode, DrlNumber, SkuNumber)

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Return Existing.GetDrlAdjustment(StockAdjustmentCode, _
                                                 StockAdjustmentManager, _
                                                 DailyReceiverListingTextControl, _
                                                 DrlNumber, _
                                                 SkuNumber)

        End Select

    End Function

    Public Sub CreateOrMaintainOrReverseAdjustmentOptions(ByVal StockAdjustmentCode As ISaCode, _
                                                          ByVal SkuPanelVisibility As Boolean, _
                                                          ByVal DevExpressGridGroupRow As Boolean, _
                                                          ByVal StockAdjustmentAmendID As Integer, _
                                                          ByVal StockAdjustment As IAdjustment, _
                                                          ByRef CreateAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef MaintainAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton) Implements IStockAdjustmentFixes.CreateOrMaintainOrReverseAdjustmentOptions

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"

                DisableButtonControls(CreateAdjustmentButton, MaintainAdjustmentButton, ReverseAdjustmentButton)

                EnableMaintainAndReverseAdjustmentButtons(SkuPanelVisibility, StockAdjustment, MaintainAdjustmentButton, ReverseAdjustmentButton)

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Existing.CreateOrMaintainOrReverseAdjustmentOptions(StockAdjustmentCode, _
                                                                    SkuPanelVisibility, _
                                                                    DevExpressGridGroupRow, _
                                                                    StockAdjustmentAmendID, _
                                                                    StockAdjustment, _
                                                                    CreateAdjustmentButton, _
                                                                    MaintainAdjustmentButton, _
                                                                    ReverseAdjustmentButton)

        End Select

    End Sub

    Public Function DailyReceiverListingNumber(ByVal StockAdjustmentCode As ISaCode, _
                                               ByVal DrlNumber As String) As String Implements IStockAdjustmentFixes.DailyReceiverListingNumber

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"
                Return String.Empty

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Return Existing.DailyReceiverListingNumber(StockAdjustmentCode, DrlNumber)

        End Select

    End Function

    Public Sub HighestSequenceNo(ByVal StockAdjustmentCode As ISaCode, _
                                 ByVal StockAdjustmentCollection As IAdjustments) Implements IStockAdjustmentFixes.HighestSequenceNo

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"

                _NextSequenceNo = StockAdjustmentCollection.HighestSequenceNo.ToString.PadLeft(2, CType("0", Char))

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Existing.HighestSequenceNo(StockAdjustmentCode, StockAdjustmentCollection)

        End Select

    End Sub

    Public Sub DisableReverseAdjustmentButton(ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton) Implements IStockAdjustmentFixes.DisableReverseAdjustmentButton

        ReverseAdjustmentButton.Visible = False
        ReverseAdjustmentButton.Enabled = False

    End Sub

#End Region

#Region "Create Adjustment"

    Public Sub CreateAdjustmentCodeFourOnly(ByVal StockAdjustmentCode As ISaCode, _
                                            ByVal StockAdjustmentManager As IStockAdjustmentManager, _
                                            ByRef StockAdjustment As IAdjustment) Implements IStockAdjustmentFixes.CreateAdjustmentCodeFourOnly

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"
                StockAdjustment = StockAdjustments.AdjustmentFactory.FactoryGet
                StockAdjustment.Price = StockAdjustmentManager.Stock.Price_PRIC
                StockAdjustment.StartingStock_SSTK = StockAdjustmentManager.Stock.OnHand_ONHA
                StockAdjustment.SEQN = _NextSequenceNo

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Existing.CreateAdjustmentCodeFourOnly(StockAdjustmentCode, _
                                                      StockAdjustmentManager, _
                                                      StockAdjustment)

        End Select

    End Sub

#End Region

#Region "Reverse Adjustment"

    Public Sub CodeFourReverseAdjustCancel(ByVal StockAdjustmentCode As ISaCode, _
                                           ByRef StockAdjustment As IAdjustment) Implements IStockAdjustmentFixes.CodeFourReverseAdjustCancel

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"
                With StockAdjustment

                    .IsReversed = False
                    .ReversalCode_RCOD = String.Empty

                End With

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Existing.CodeFourReverseAdjustCancel(StockAdjustmentCode, StockAdjustment)

        End Select

    End Sub

    Public Sub CodeFourReverseAdjustCompletedMessage(ByVal StockAdjustmentCode As ISaCode, _
                                                     ByVal StockAdjustment As IAdjustment, _
                                                     ByVal EventArguement As System.Windows.Forms.KeyPressEventArgs, _
                                                     ByRef GridView As DevExpress.XtraGrid.Views.Grid.GridView) Implements IStockAdjustmentFixes.CodeFourReverseAdjustCompletedMessage

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"

                If AlreadyReversed(EventArguement, StockAdjustment.IsReversed) Then

                    MessageBox.Show("Unable to maintain a reversed adjustment." & vbCrLf & _
                                    "Please enter the DRL number and press F6 to create new adjustment", _
                                    String.Empty, MessageBoxButtons.OK)

                End If

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Existing.CodeFourReverseAdjustCompletedMessage(StockAdjustmentCode, StockAdjustment, EventArguement, GridView)

        End Select

    End Sub

    Public Function CodeFourReverseAdjustEndStock(ByVal StockAdjustmentCode As ISaCode, _
                                                  ByVal StockAdjustment As IAdjustment, _
                                                  ByRef StartingStockTextControl As DevExpress.XtraEditors.TextEdit, _
                                                  ByRef AdjustmentValueTextControl As DevExpress.XtraEditors.TextEdit) As Decimal Implements IStockAdjustmentFixes.CodeFourReverseAdjustEndStock

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"
                Return StockAdjustment.StartingStock_SSTK + StockAdjustment.Quantity_QUAN

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Return Existing.CodeFourReverseAdjustEndStock(StockAdjustmentCode, StockAdjustment, StartingStockTextControl, AdjustmentValueTextControl)

        End Select

    End Function

#End Region

#Region "Grid Event(s)"

    Public Sub GridControlSelectionChangedEvent(ByVal StockAdjustmentCode As ISaCode, _
                                                ByRef SelectionChangedEvent As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler, _
                                                ByRef GridControl As DevExpress.XtraGrid.GridControl, _
                                                ByRef GridView As DevExpress.XtraGrid.Views.Grid.GridView) Implements IStockAdjustmentFixes.GridControlSelectionChangedEvent

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"
                Call SelectionChangedEvent(GridControl, New DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(GridView.FocusedRowHandle, 0))

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Existing.GridControlSelectionChangedEvent(StockAdjustmentCode, SelectionChangedEvent, GridControl, GridView)

        End Select

    End Sub

    Public Sub GridControlRefreshDataSourceEvent(ByVal StockAdjustmentCode As ISaCode, _
                                                 ByRef StockAdjustmentCollection As IAdjustments, _
                                                 ByRef GridControl As DevExpress.XtraGrid.GridControl) Implements IStockAdjustmentFixes.GridControlRefreshDataSourceEvent

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"

                GridControl.DataSource = StockAdjustmentCollection
                GridControl.RefreshDataSource()

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Existing.GridControlRefreshDataSourceEvent(StockAdjustmentCode, StockAdjustmentCollection, GridControl)

        End Select

    End Sub

#End Region

#Region "Daily Receiver Listing Control Event"

    Public Sub DailyReceiverListingControlValidatingEvent(ByVal StockAdjustmentCode As ISaCode, _
                                                          ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                                          ByRef SpecificStockAdjustment As IAdjustment, _
                                                          ByRef StockAdjustmentCollection As IAdjustments, _
                                                          ByVal SkuNumber As String, _
                                                          ByVal CancelSelected As Boolean, _
                                                          ByRef CreateAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef MaintainAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                          ByRef DailyReceiverListingTextControl As DevExpress.XtraEditors.TextEdit, _
                                                          ByRef DailyReceiverListingEventArguement As System.ComponentModel.CancelEventArgs) Implements IStockAdjustmentFixes.DailyReceiverListingControlValidatingEvent

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"
                Dim DrlNumber As String

                DrlNumber = DailyReceiverListingTextControl.Text

                If CancelSelected = True Then Exit Sub

                If DrlNumber = String.Empty Then

                    MessageBox.Show("Blank daily receiver listing number", String.Empty, MessageBoxButtons.OK)
                    If StockAdjustmentCollection Is Nothing Then DisableButtonControls(CreateAdjustmentButton, MaintainAdjustmentButton, ReverseAdjustmentButton)
                    Exit Sub

                End If

                If StockAdjustmentManager.GetDRLAdjustment(StockAdjustmentCode, DrlNumber, SkuNumber) = False Then

                    'bad drl
                    DailyReceiverListingTextControl.EditValue = String.Empty
                    If StockAdjustmentCollection Is Nothing Then DisableButtonControls(CreateAdjustmentButton, MaintainAdjustmentButton, ReverseAdjustmentButton)
                    Exit Sub

                End If

                If _AllCodeFourStockAdjustments IsNot Nothing AndAlso _AllCodeFourStockAdjustments.MaintainableDailyReceiverListingAdjustment(DrlNumber) = True Then

                    MessageBox.Show("Daily receiver listing has existing adjustment that can be maintained", String.Empty, MessageBoxButtons.OK)
                    DailyReceiverListingTextControl.EditValue = String.Empty
                    If StockAdjustmentCollection Is Nothing Then DisableButtonControls(CreateAdjustmentButton, MaintainAdjustmentButton, ReverseAdjustmentButton)
                    Exit Sub

                End If

                'completely reversed or non-existant adjustment; allow creation of new adjustment
                StockAdjustmentCollection = Nothing
                DisableButtonControls(CreateAdjustmentButton, MaintainAdjustmentButton, ReverseAdjustmentButton)
                CreateAdjustmentButton.Enabled = True

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Existing.DailyReceiverListingControlValidatingEvent(StockAdjustmentCode, _
                                                                    StockAdjustmentManager, _
                                                                    SpecificStockAdjustment, _
                                                                    StockAdjustmentCollection, _
                                                                    SkuNumber, _
                                                                    CancelSelected, _
                                                                    CreateAdjustmentButton, _
                                                                    MaintainAdjustmentButton, _
                                                                    ReverseAdjustmentButton, _
                                                                    DailyReceiverListingTextControl, _
                                                                    DailyReceiverListingEventArguement)

        End Select

    End Sub

#End Region

#Region "Private Procedures And Functions"

    Private Sub DisableButtonControls(ByRef CreateAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                      ByRef MaintainAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                      ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton)

        CreateAdjustmentButton.Visible = True
        MaintainAdjustmentButton.Visible = True
        ReverseAdjustmentButton.Visible = True

        CreateAdjustmentButton.Enabled = False
        MaintainAdjustmentButton.Enabled = False
        ReverseAdjustmentButton.Enabled = False

    End Sub

    Friend Overridable Function EnableMaintainAdjustmentButton(ByVal HeadOfficeCommed As Boolean, ByVal IsReversed As Boolean) As Boolean

        If HeadOfficeCommed = False And IsReversed = False Then Return True

    End Function

    Friend Overridable Function EnableReverseAdjustmentButton(ByVal HeadOfficeCommed As Boolean, ByVal IsReversed As Boolean) As Boolean

        If HeadOfficeCommed = True And IsReversed = False Then Return True

    End Function

    Friend Sub EnableMaintainAndReverseAdjustmentButtons(ByVal SkuPanelVisibility As Boolean, ByVal StockAdjustment As IAdjustment, ByRef MaintainAdjustmentButton As DevExpress.XtraEditors.SimpleButton, ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton)

        If SkuPanelVisibility = True Then Exit Sub

        If StockAdjustment Is Nothing Then Exit Sub

        If EnableMaintainAdjustmentButton(StockAdjustment.IsSentToHO_COMM, StockAdjustment.IsReversed) = True Then MaintainAdjustmentButton.Enabled = True
        If EnableReverseAdjustmentButton(StockAdjustment.IsSentToHO_COMM, StockAdjustment.IsReversed) = True Then ReverseAdjustmentButton.Enabled = True

    End Sub

    Friend Function AlreadyReversed(ByVal EventArguement As KeyPressEventArgs, ByVal IsReversed As Boolean) As Boolean

        Return EventArguement.KeyChar = ChrW(Keys.Enter) AndAlso IsReversed = True

    End Function

#End Region

#End Region

#Region "Stock Adjustment Manager Fixes"

    Public Function ReadDRL(ByVal StockAdjustmentCode As ISaCode, _
                            ByRef Validator As IDRLValidator, _
                            ByVal DrlNumber As String, _
                            ByVal SkuNumber As String) As Boolean Implements IStockAdjustmentFixes.ReadDRL


        Select Case StockAdjustmentCode.CodeNumber
            Case "04"

                ReadDRL = True

                If Validator.ReadDRL(DrlNumber) = False Then

                    MessageBox.Show("DRL Number does not exist.", "Invalid DRLNumber", MessageBoxButtons.OK)
                    ReadDRL = False

                Else

                    'Require ability to adjust sku(s) where not part of the original DRL; following validation suplus to requirement

                    'If Validator.IsSKuInDRL(DrlNumber, SkuNumber) = False Then

                    '    MessageBox.Show("DRL Number exist but not for this sku", "Invalid DRLNumber", MessageBoxButtons.OK)
                    '    ReadDRL = False

                    'End If

                End If

            Case Else
                Dim Existing As New StockAdjustmentFixesExisting

                Return Existing.ReadDRL(StockAdjustmentCode, _
                                        Validator, _
                                        DrlNumber, _
                                        SkuNumber)

        End Select

    End Function

    Friend Function ProcessReverseAdjustment(ByVal StockAdjustmentCode As ISaCode, ByRef ExistingStockAdjustment As IAdjustment) As Boolean

        Return ExistingStockAdjustment IsNot Nothing And StockAdjustmentCode.CodeNumber = "04"

    End Function

    Public Overridable Function PersistNormalAdjustments(ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                                  ByVal StockAdjustmentCode As ISaCode, _
                                                  ByRef ActualStockAdjustment As IAdjustment, _
                                                  ByRef ExistingStockAdjustment As IAdjustment, _
                                                  ByVal SkuNumber As String, _
                                                  ByVal AdjustmentDate As Date, _
                                                  ByVal DRLNumber As String, _
                                                  ByRef AdjustmentQuantity As Integer, _
                                                  ByVal Comment As String, _
                                                  ByVal EmployeeID As Integer) As Boolean Implements IStockAdjustmentFixes.PersistNormalAdjustments


        If ProcessReverseAdjustment(StockAdjustmentCode, ExistingStockAdjustment) Then

            Return StockAdjustmentManager.NormalAdjustment(StockAdjustmentCode, _
                                                           ActualStockAdjustment, _
                                                           ExistingStockAdjustment, _
                                                           SkuNumber, _
                                                           AdjustmentDate, _
                                                           DRLNumber, _
                                                           AdjustmentQuantity, _
                                                           Comment, _
                                                           EmployeeID)


            ExistingStockAdjustment = Nothing

        Else

            Return StockAdjustmentManager.NormalAdjustment(StockAdjustmentCode, _
                                                           ActualStockAdjustment, _
                                                           SkuNumber, _
                                                           AdjustmentDate, _
                                                           DRLNumber, _
                                                           AdjustmentQuantity, _
                                                           Comment, _
                                                           EmployeeID)

        End If

    End Function

#End Region

    Public Sub ConfigureExistingAdjustment(ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                           ByVal StockAdjustmentCode As ISaCode, _
                                           ByRef Source As IAdjustment, _
                                           ByRef Target As IAdjustment) Implements IStockAdjustmentFixes.ConfigureExistingAdjustment

        Select Case StockAdjustmentCode.CodeNumber
            Case "04"

                Target = AdjustmentFactory.FactoryGet

                StockAdjustmentManager.AdjustmentMakeEqual(Source, Target)

                Target.IsReversed = True
                Target.ExistsInDatabase = True

            Case Else

                'do nothing

        End Select

    End Sub

    Public Sub ConfigureNewAdjustment(ByRef Value As IAdjustment) Implements IStockAdjustmentFixes.ConfigureNewAdjustment

        Value.ExistsInDatabase = False
        Value.IsReversed = True
        Value.ReversalCode_RCOD = "A"

    End Sub

    'NOT REQUIRED
    '"Cannot negatively adjust...." message for F8 reversal not an issue; txtSkuAdjust control validating event not called, now readonly
    '"Cannot negatively adjust...." message for F7 maintenance not an issue since adjust existing adjustment

    'Public Function ValidAdjustmentQuantity(ByVal AdjustmentQuantity As Integer, _
    '                                        ByVal ReceiptQuantity As Integer, _
    '                                        ByVal ExistingAdjustmentQuantity As Integer) As Boolean Implements IStockAdjustmentFixes.ValidAdjustmentQuantity

    '    Dim ModQty As Integer = (AdjustmentQuantity * -1)

    '    If ModQty > ReceiptQuantity Then
    '        MessageBox.Show("Cannot negatively adjust greater than received quantity.", "Invalid Adjustment", MessageBoxButtons.OK)
    '        Return False
    '    End If

    '    Return True

    '    'existing code
    '    'For DRL Adjustment - disallow negative adjustments if > than received 
    '    'Dim ModQty As Integer = (AdjQty * -1)
    '    'If ModQty > m_DRL.GetReceiptQty(m_DRL.DRLNumber, SkuNumber) Then
    '    '    MessageBox.Show("Cannot negatively adjust greater than received quantity.", "Invalid Adjustment", MessageBoxButtons.OK)
    '    '    Return False
    '    'End If

    'End Function

End Class