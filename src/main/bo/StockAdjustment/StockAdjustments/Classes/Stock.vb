﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Data

Public Class Stock

    Inherits Cts.Oasys.Core.Base

    Implements IStock

    Private m_AdjustmentQty_AQ021 As Decimal = 0@

    Private m_AdjustmentQty_MADQ1 As Integer = 0

    Private m_AdjustmentValue_AV021 As Decimal = 0@

    Private m_AdjustmentValue_MADV1 As Decimal = 0@

    Private m_AllowAdjustments_AADJ As String = "0.0"

    Private m_Cost As Decimal = 0@

    Private m_CycleDate_ADAT1 As Date = Nothing

    Private m_CyclicalCount_MCCV1 As Decimal = 0@

    Private m_DummyItem_ICAT As Boolean = False

    Private m_HODeleted_IDEL As Boolean = False

    Private m_MarkdownQty_MDNQ As Integer = 0

    Private m_OnHand_ONHA As Integer = 0

    Private m_OpenReturns_RETQ As Integer

    Private m_Price_PRIC As Decimal = 0@

    Private m_Department As String = String.Empty

    Private m_SaleTypeAttribute_SALT As String = String.Empty

    Private m_SKUN As String = String.Empty

    Private m_TodayActivity_TACT As Boolean = False

    Private m_WriteOffStock_WTFQ As Integer = 0

#Region "Properties"

    <ColumnMapping("ActivityAdjQty")> Public Property AdjustmentQty_AQ021() As Decimal Implements IStock.AdjustmentQty_AQ021
        Get
            Return m_AdjustmentQty_AQ021
        End Get
        Set(ByVal value As Decimal)
            m_AdjustmentQty_AQ021 = value
        End Set
    End Property

    <ColumnMapping("AdjustmentQty1")> Public Property AdjustmentQty_MADQ1() As Integer Implements IStock.AdjustmentQty_MADQ1
        Get
            Return m_AdjustmentQty_MADQ1
        End Get
        Set(ByVal value As Integer)
            m_AdjustmentQty_MADQ1 = value
        End Set
    End Property

    <ColumnMapping("ActivityAdjValue")> Public Property AdjustmentValue_AV021() As Decimal Implements IStock.AdjustmentValue_AV021
        Get
            Return m_AdjustmentValue_AV021
        End Get
        Set(ByVal value As Decimal)
            m_AdjustmentValue_AV021 = value
        End Set
    End Property

    <ColumnMapping("AdjustmentValue1")> Public Property AdjustmentValue_MADV1() As Decimal Implements IStock.AdjustmentValue_MADV1
        Get
            Return m_AdjustmentValue_MADV1
        End Get
        Set(ByVal value As Decimal)
            m_AdjustmentValue_MADV1 = value
        End Set
    End Property

    <ColumnMapping("AllowAdjustment")> Public Property AllowAdjustments_AADJ() As String Implements IStock.AllowAdjustments_AADJ
        Get
            Return m_AllowAdjustments_AADJ
        End Get
        Set(ByVal value As String)
            m_AllowAdjustments_AADJ = value
        End Set
    End Property

    <ColumnMapping("Cost")> Public Property Cost() As Decimal Implements IStock.Cost
        Get
            Return m_Cost
        End Get
        Set(ByVal value As Decimal)
            m_Cost = value
        End Set
    End Property

    <ColumnMapping("ActivityDate")> Public Property CycleDate_ADAT1() As Date Implements IStock.CycleDate_ADAT1
        Get
            Return m_CycleDate_ADAT1
        End Get
        Set(ByVal value As Date)
            m_CycleDate_ADAT1 = value
        End Set
    End Property

    <ColumnMapping("CyclicalCount")> Public Property CyclicalCount_MCCV1() As Decimal Implements IStock.CyclicalCount_MCCV1
        Get
            Return m_CyclicalCount_MCCV1
        End Get
        Set(ByVal value As Decimal)
            m_CyclicalCount_MCCV1 = value
        End Set
    End Property

    <ColumnMapping("DummyItem")> Public Property DummyItem_ICAT() As Boolean Implements IStock.DummyItem_ICAT
        Get
            Return m_DummyItem_ICAT
        End Get
        Set(ByVal value As Boolean)
            m_DummyItem_ICAT = value
        End Set
    End Property

    <ColumnMapping("HODeleted")> Public Property HODeleted_IDEL() As Boolean Implements IStock.HODeleted_IDEL
        Get
            Return m_HODeleted_IDEL
        End Get
        Set(ByVal value As Boolean)
            m_HODeleted_IDEL = value
        End Set
    End Property

    <ColumnMapping("MarkDownQty")> Public Property MarkdownQty_MDNQ() As Integer Implements IStock.MarkdownQty_MDNQ
        Get
            Return m_MarkdownQty_MDNQ
        End Get
        Set(ByVal value As Integer)
            m_MarkdownQty_MDNQ = value
        End Set
    End Property

    <ColumnMapping("OnHand")> Public Property OnHand_ONHA() As Integer Implements IStock.OnHand_ONHA
        Get
            Return m_OnHand_ONHA
        End Get
        Set(ByVal value As Integer)
            m_OnHand_ONHA = value
        End Set
    End Property

    <ColumnMapping("Price")> Public Property Price_PRIC() As Decimal Implements IStock.Price_PRIC
        Get
            Return m_Price_PRIC
        End Get
        Set(ByVal value As Decimal)
            m_Price_PRIC = value
        End Set
    End Property

    <ColumnMapping("SaleTypeAttribute")> Public Property SaleTypeAttribute_SALT() As String Implements IStock.SaleTypeAttribute_SALT
        Get
            Return m_SaleTypeAttribute_SALT
        End Get
        Set(ByVal value As String)
            m_SaleTypeAttribute_SALT = value
        End Set
    End Property

    <ColumnMapping("SKUN")> Public Property SKUN() As String Implements IStock.SKUN
        Get
            Return m_SKUN
        End Get
        Set(ByVal value As String)
            m_SKUN = value
        End Set
    End Property

    <ColumnMapping("TodayActivityFlag")> Public Property TodayActivity_TACT() As Boolean Implements IStock.TodayActivity_TACT
        Get
            Return m_TodayActivity_TACT
        End Get
        Set(ByVal value As Boolean)
            m_TodayActivity_TACT = value
        End Set
    End Property

    <ColumnMapping("OpenReturnQty")> Public Property OpenReturnQty_RETQ() As Integer Implements IStock.OpenReturnQty_RETQ
        Get
            Return m_OpenReturns_RETQ
        End Get
        Set(ByVal value As Integer)
            m_OpenReturns_RETQ = value
        End Set
    End Property

    <ColumnMapping("WriteOffStock")> Public Property WriteOffStock_WTFQ() As Integer Implements IStock.WriteOffStock_WTFQ
        Get
            Return m_WriteOffStock_WTFQ
        End Get
        Set(ByVal value As Integer)
            m_WriteOffStock_WTFQ = value
        End Set
    End Property

    <ColumnMapping("Department")> Public Property Department() As String Implements IStock.Department
        Get
            Return m_Department
        End Get
        Set(ByVal value As String)
            m_Department = value
        End Set
    End Property

#End Region

    Public Sub New()
        MyBase.New()
    End Sub

    Public Function ReadSKU(ByRef Con As Connection, ByVal ProductCode As String) As Boolean Implements IStock.ReadSKU
        Dim DataAccess As IStockRepository = StockRepositoryFactory.FactoryGet
        Dim dr As DataRow = DataAccess.ReadSKU(Con, ProductCode)
        If dr IsNot Nothing Then
            Me.Load(dr)
            Return True
        Else

            Return False
        End If
    End Function

    Public Sub Update(ByRef Con As Connection) Implements IStock.Update
        Dim DataAccess As IStockRepository = StockRepositoryFactory.FactoryGet
        DataAccess.Update(Me, Con)
    End Sub

    Public Function GetReducedStocksDataTable() As DataTable Implements IStock.GetReducedStocksDataTable
        Dim DataAccess As IStockRepository = StockRepositoryFactory.FactoryGet
        Dim dt As DataTable = DataAccess.GetReducedStocksDataTable
        Return dt
    End Function

    Public Function GetStoreNumber() As String Implements IStock.GetStoreNumber
        Dim DataAccess As IStockRepository = StockRepositoryFactory.FactoryGet
        Dim StoreNumber As String = DataAccess.GetStoreNumber
        Return StoreNumber
    End Function

    Public Function DoesSkuExist(ByVal ProductCode As String) As Boolean Implements IStock.DoesSkuExist
        Dim DataAccess As IStockRepository = StockRepositoryFactory.FactoryGet
        Return DataAccess.DoesSkuExist(ProductCode)
    End Function

End Class
