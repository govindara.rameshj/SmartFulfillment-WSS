﻿Option Strict On

Public Class AdjustmentRepositoryOverloadDecideUseConnection
    Implements IAdjustmentRepositoryOverloadDecide

    Public Function GetNextSequenceNumberForSkunCodeDate(ByRef AdjustmentRepository As IAdjustmentRepository, _
                                                         ByRef Con As Connection, _
                                                         ByVal SkuNumber As String, _
                                                         ByVal AdjustmentCode As String, _
                                                         ByVal AdjustmentDate As Date) As String Implements IAdjustmentRepositoryOverloadDecide.GetNextSequenceNumberForSkunCodeDate

        Return AdjustmentRepository.GetNextSequenceNumberForSkunCodeDate(Con, SkuNumber, AdjustmentCode, AdjustmentDate)

    End Function

End Class