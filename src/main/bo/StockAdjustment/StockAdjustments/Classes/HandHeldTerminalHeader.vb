﻿Option Strict On
Option Explicit On
Imports Cts.Oasys.Core
Imports Cts.Oasys.Core.Net4Replacements

Public Class HandHeldTerminalHeader
    Inherits Cts.Oasys.Core.Base

    Implements IHandHeldTerminalHeader

    Private m_HeaderExist As Boolean
    Private m_AdjustmentsApplied As Boolean
    Private m_AuthorisationID As String
    Private m_HHTDate As Date
    Private m_IsLocked As Boolean
    Private m_ItemsCounted As Integer
    Private m_ItemsToCount As Integer
    Private mcol_Details As New HandHeldTerminalDetailCollection

#Region "Properties"

    Public Property HeaderExist() As Boolean Implements IHandHeldTerminalHeader.HeaderExist
        Get
            Return m_HeaderExist
        End Get
        Set(ByVal value As Boolean)
            m_HeaderExist = value
        End Set
    End Property

    <ColumnMapping("IADJ")> Public Property AdjustmentApplied() As Boolean Implements IHandHeldTerminalHeader.AdjustmentApplied
        Get
            Return m_AdjustmentsApplied
        End Get
        Set(ByVal value As Boolean)
            m_AdjustmentsApplied = value
        End Set
    End Property

    <ColumnMapping("AUTH")> Public Property AuthorisationID() As String Implements IHandHeldTerminalHeader.AuthorisationID
        Get
            Return m_AuthorisationID
        End Get
        Set(ByVal value As String)
            m_AuthorisationID = value
        End Set
    End Property

    <ColumnMapping("DATE1")> Public Property HHTDate() As Date Implements IHandHeldTerminalHeader.HHTDate
        Get
            Return m_HHTDate
        End Get
        Set(ByVal value As Date)
            m_HHTDate = value
        End Set
    End Property

    <ColumnMapping("IsLocked")> Public Property IsLocked() As Boolean Implements IHandHeldTerminalHeader.IsLocked
        Get
            Return m_IsLocked
        End Get
        Set(ByVal value As Boolean)
            m_IsLocked = value
        End Set
    End Property

    <ColumnMapping("DONE")> Public Property ItemsCounted() As Integer Implements IHandHeldTerminalHeader.ItemsCounted
        Get
            Return m_ItemsCounted
        End Get
        Set(ByVal value As Integer)
            m_ItemsCounted = value
        End Set
    End Property

    <ColumnMapping("NUMB")> Public Property ItemsToCount() As Integer Implements IHandHeldTerminalHeader.ItemsToCount
        Get
            Return m_ItemsToCount
        End Get
        Set(ByVal value As Integer)
            m_ItemsToCount = value
        End Set
    End Property

    Public Property Details() As HandHeldTerminalDetailCollection Implements IHandHeldTerminalHeader.Details
        Get
            Return mcol_Details
        End Get
        Set(ByVal value As HandHeldTerminalDetailCollection)
            mcol_Details = value
        End Set
    End Property

#End Region

#Region "Events"

    Public Event ClearProgress() Implements IHandHeldTerminalHeader.ClearProgress

    Public Event UpdateProgess(ByVal percent As Integer, ByVal message As String) Implements IHandHeldTerminalHeader.UpdateProgess

#End Region

#Region "Functions & Methods"

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub ReadLatestHHT() Implements IHandHeldTerminalHeader.ReadLatestHHT
        Dim Repository As IHandHeldTerminalHeaderRepository
        Dim DR As DataRow

        Repository = HandHeldTerminalHeaderRepositoryFactory.FactoryGet
        DR = Repository.ReadLatestHHT
        If DR IsNot Nothing Then
            Me.Load(DR)
            Me.HeaderExist = True
        End If

        If Me.HeaderExist = False Then Exit Sub 'no header record

        If Me.AdjustmentApplied = True Then Exit Sub 'adjustments applied, end process now

        If Me.ItemsCounted <> Me.ItemsToCount Then Exit Sub 'count complete, end process now

        'load details
        Dim colDetails As New HandHeldTerminalDetailCollection

        colDetails.ReadDetail(Me.HHTDate)
        Me.Details = colDetails
    End Sub

    Public Sub ReadHHT(ByVal AdjDate As Date) Implements IHandHeldTerminalHeader.ReadHHT
        Dim DR As DataRow

        Dim Repository = HandHeldTerminalHeaderRepositoryFactory.FactoryGet
        DR = Repository.ReadHHT(AdjDate)
        If DR IsNot Nothing Then
            Me.Load(DR)
            Me.HeaderExist = True
        Else
            DR = Repository.ReadLatestHHT
            If DR IsNot Nothing Then
                Me.Load(DR)
                Me.HeaderExist = True
            End If
        End If

        If Me.HeaderExist = False Then Exit Sub 'no header record

        If Me.AdjustmentApplied = True Then Exit Sub 'adjustments applied, end process now

        If Me.ItemsCounted <> Me.ItemsToCount Then Exit Sub 'count complete, end process now

        'load details
        Dim colDetails As New HandHeldTerminalDetailCollection

        colDetails.ReadDetail(Me.HHTDate)
        Me.Details = colDetails

    End Sub

    Public Sub Update(ByVal SelectedDate As Date, ByVal ItemsToCount As Integer?, ByVal ItemsCounted As Integer?, ByVal AdjustmentApplied As Boolean?, _
                      ByVal AuthorisationID As String, ByVal IsLocked As Boolean?) Implements IHandHeldTerminalHeader.Update
        Dim Repository As IHandHeldTerminalHeaderRepository

        Repository = HandHeldTerminalHeaderRepositoryFactory.FactoryGet
        Repository.Update(SelectedDate, ItemsToCount, ItemsCounted, AdjustmentApplied, AuthorisationID, IsLocked)

    End Sub

    Public Function CheckAdjustmentsAuthorizingNeeded() As Tuple(Of Boolean, String) Implements IHandHeldTerminalHeader.CheckAdjustmentsAuthorizingNeeded
        Dim isAdjustmentNeeded As Boolean
        Dim errorMessage As String = String.Empty
        Select Case True
            Case Not HeaderExist
                errorMessage = "No Record Exist"
                isAdjustmentNeeded = False
            Case AdjustmentApplied
                errorMessage = "Adjustments Already Applied"
                isAdjustmentNeeded = False
            Case ItemsCounted <> ItemsToCount
                errorMessage = "Items have not all been counted."
                isAdjustmentNeeded = False
            Case Details.Count = 0
                errorMessage = "No Details Exist"
                isAdjustmentNeeded = False
            Case Else : isAdjustmentNeeded = True
        End Select

        Return New Tuple(Of Boolean, String)(isAdjustmentNeeded, errorMessage)
    End Function

    'Public Sub CalculateHeaderTotals() Implements IHandHeldTerminalHeader.CalculateHeaderTotals
    '    Dim intItemsCounted As Integer
    '    Dim blnAllAdjustmentsApplied As Boolean

    '    intItemsCounted = 0
    '    blnAllAdjustmentsApplied = True

    '    If Me.Details IsNot Nothing Then
    '        For Each objDetail As HandHeldTerminalDetail In Me.Details
    '            If objDetail.AdjustmentApplied = False Then blnAllAdjustmentsApplied = False
    '        Next
    '    End If

    '    Me.AdjustmentApplied = blnAllAdjustmentsApplied
    '    Me.ItemsCounted = intItemsCounted
    '    If Me.Details IsNot Nothing Then Me.ItemsToCount = Me.Details.Count
    'End Sub

#End Region

End Class