﻿Public Class AdjustmentQuantity

    Implements IAdjustmentQuantity

    Private m_AdjustmentQuantity As IAdjustmentQuantity

    Public Sub Validate(ByVal Type As String, ByVal Value As Integer) Implements IAdjustmentQuantity.Validate

    End Sub

    Public Property Value() As Integer Implements IAdjustmentQuantity.Value
        Get
            Return m_AdjustmentQuantity.Value
        End Get
        Set(ByVal value As Integer)
            m_AdjustmentQuantity.Value = value
        End Set
    End Property
End Class
