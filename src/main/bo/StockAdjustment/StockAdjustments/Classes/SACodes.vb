﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Data
Imports System.Text

Namespace SACode

    Public Class SACodes

        Inherits BaseCollection(Of SaCode)

        Implements ISACodes

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overloads Function AddNew() As SaCode
            Dim line As SaCode = MyBase.AddNew
            Return line
        End Function

        Public Sub GetSACode(ByVal SecurityLevel As Integer, ByVal CodeNumber As String) Implements ISACodes.GetSACode
            Dim DataAccess As ISACodesRepository = SACodesRepositoryFactory.FactoryGet
            Dim dr As DataRow = DataAccess.GetSACode(SecurityLevel, CodeNumber)
            Dim dt As DataTable = Nothing
            dt.Rows.Add(dr)
            Me.Load(dt)
        End Sub

        Public Sub GetSACodes(ByVal SecurityLevel As Integer) Implements ISACodes.GetSACodes

            Dim DataAccess As ISACodesRepository = SACodesRepositoryFactory.FactoryGet
            Dim dt As DataTable = DataAccess.GetSACodes(SecurityLevel)
            Me.Load(dt)
        End Sub

    End Class

End Namespace
