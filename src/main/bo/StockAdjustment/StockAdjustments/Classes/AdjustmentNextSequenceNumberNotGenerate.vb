﻿Option Strict On

Public Class AdjustmentNextSequenceNumberNotGenerate
    Implements IAdjustmentNextSequenceNumberDecide

    Public Function SaveAdjustment(ByRef Adjustment As IAdjustment, _
                                   ByRef Con As Connection, _
                                   ByVal AdjQty As Integer, _
                                   ByVal AdjDate As Date, _
                                   ByVal SkuNumber As String, _
                                   ByVal DRLNumber As String, _
                                   ByVal Comment As String, _
                                   ByVal SACode As ISaCode, _
                                   ByVal Stock As IStock, _
                                   ByVal StockLog As IStockLog, _
                                   ByVal EmployeeId As Integer, _
                                   Optional ByVal TransferSku As String = "", _
                                   Optional ByVal TransferValue As Decimal = 0, _
                                   Optional ByVal TransferPrice As Decimal = 0, _
                                   Optional ByVal TransferStart As Decimal = 0, _
                                   Optional ByVal PicCountAcceptance As Boolean = False, _
                                   Optional ByVal PicCountAcceptanceCode53 As Boolean = False) As Boolean Implements IAdjustmentNextSequenceNumberDecide.SaveAdjustment

        Adjustment.SaveAdjustment(Con, AdjQty, AdjDate, SkuNumber, DRLNumber, Comment, SACode, Stock, StockLog, EmployeeId)

    End Function

End Class