﻿Option Strict On

Public Class StockLogOverloadDecideIgnoreConnection
    Implements IStockLogOverloadDecide

    Public Sub OverloadedReadDecide(ByRef Con As Connection, _
                                    ByVal SkuNumber As String, _
                                    ByRef StockLog As IStockLog) Implements IStockLogOverloadDecide.ReadStockLog

        StockLog.ReadStockLog(SkuNumber)

    End Sub

End Class