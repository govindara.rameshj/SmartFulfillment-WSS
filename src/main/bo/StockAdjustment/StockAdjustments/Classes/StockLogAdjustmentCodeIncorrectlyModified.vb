﻿Option Strict On

Public Class StockLogAdjustmentCodeIncorrectlyModified
    Implements IStockLogModifyAdjustmentCode

    Public Sub ModifyAdjustmentCode(ByVal SACode As ISaCode) Implements IStockLogModifyAdjustmentCode.ModifyAdjustmentCode

        If SACode.MarkdownWriteoff = String.Empty Then SACode.MarkdownWriteoff = " "

    End Sub

End Class