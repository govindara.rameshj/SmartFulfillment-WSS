﻿Option Strict On

Public Class Adjustments
    Inherits BaseCollection(Of Adjustment)

    Implements IAdjustments

    Public Sub New()
        MyBase.New()
    End Sub

    Public Overloads Function AddNew() As Adjustment
        Dim line As Adjustment = MyBase.AddNew
        Return line
    End Function

    Public Function ReadAdjustments(ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As Boolean Implements IAdjustments.ReadAdjustments

        Dim ReadAdjustmentsImplementation As IAdjustmentsRepository = (New ReadAdjustmentsFactory).GetImplementation()
        Dim Adjustments As IAdjustmentsRepository = ReadAdjustmentsImplementation
        Dim dt As DataTable = Adjustments.ReadAdjustmentsExcludingMarkdownSku(Skun, AdjDate, CodeNumber)
        Me.Load(dt)

    End Function

    Public Function HighestSequenceNo() As Integer Implements IAdjustments.HighestSequenceNo

        HighestSequenceNo = 0

        If Me.Items Is Nothing Then Exit Function

        Try

            For Each Adj As IAdjustment In Me.Items

                If CInt(Adj.SEQN) > HighestSequenceNo Then HighestSequenceNo = CInt(Adj.SEQN)

            Next

        Catch ex As Exception

            Throw ex

        End Try

    End Function

    Public Function MaintainableDailyReceiverListingAdjustment(ByVal DrlNumber As String) As Boolean Implements IAdjustments.MaintainableDailyReceiverListingAdjustment

        MaintainableDailyReceiverListingAdjustment = False

        If Me.Items Is Nothing Then Exit Function

        For Each Adj As IAdjustment In Me.Items

            If Adj.DeliveryNumber_DRLN <> DrlNumber Then Continue For

            If Adj.IsReversed = False Then MaintainableDailyReceiverListingAdjustment = True

        Next

    End Function

    Public Function skuCount() As Integer Implements IAdjustments.skuCount
        Return Me.Items.Count
    End Function

End Class