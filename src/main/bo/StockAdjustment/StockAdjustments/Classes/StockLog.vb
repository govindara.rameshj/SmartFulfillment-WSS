﻿<Assembly: InternalsVisibleTo("StockAdjustment_Tests, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                               "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                               "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                               "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                               "54e0a4a4")> 
Public Class StockLog
    Inherits Base

    Implements IStockLog

#Region "ReadStockLog Variables"

    Protected _Connection As Connection
    Protected _SkuNumber As String
    Protected _Repository As IStockLogRepository
    Protected _DR As DataRow

#End Region

#Region "Properties"

    Private m_StockLogId_TKEY As Integer = 0
    Private m_DayNumber_DAYN As Decimal = 0@
    Private m_StockLogType_TYPE As String = String.Empty
    Private m_StockLogDate_DATE1 As Date = Nothing
    Private m_StockLogTime_TIME As String = String.Empty
    Private m_Keys As String = String.Empty
    Private m_EmployeeId_EEID As String = String.Empty
    Private m_IsSentToHO_ICOM As Boolean = False
    Private m_StartingStock_SSTK As Decimal = 0@
    Private m_StartingOpenReturnsQuantity_SRET As Decimal = 0@
    Private m_StartingPrice_PRIC As Decimal = 0@
    Private m_StartingMarkDown_SMDN As Decimal = 0@
    Private m_StartingWriteOff_SWTF As Decimal = 0@
    Private m_EndingStock_ESTK As Decimal = 0@
    Private m_EndingOpenReturnsQuantity_ERET As Decimal = 0@
    Private m_EndingPrice_EPRI As Decimal = 0@
    Private m_EndingMarkDown_EMDN As Decimal = 0@
    Private m_EndingWriteOff_EWTF As Decimal = 0@
    Private m_RTI As String = "S"
    Private m_SKUN As String = String.Empty

    <ColumnMapping("DayNumber")> Public Property DayNumber_DAYN() As Decimal Implements IStockLog.DayNumber_DAYN
        Get
            Return m_DayNumber_DAYN
        End Get
        Set(ByVal value As Decimal)
            m_DayNumber_DAYN = value
        End Set
    End Property

    <ColumnMapping("EmployeeId")> Public Property EmployeeId_EEID() As String Implements IStockLog.EmployeeId_EEID
        Get
            Return m_EmployeeId_EEID
        End Get
        Set(ByVal value As String)
            m_EmployeeId_EEID = value
        End Set
    End Property

    <ColumnMapping("EndingMarkdown")> Public Property EndingMarkDown_EMDN() As Decimal Implements IStockLog.EndingMarkDown_EMDN
        Get
            Return m_EndingMarkDown_EMDN
        End Get
        Set(ByVal value As Decimal)
            m_EndingMarkDown_EMDN = value
        End Set
    End Property

    <ColumnMapping("EndingOpenReturns")> Public Property EndingOpenReturnsQuantity_ERET() As Decimal Implements IStockLog.EndingOpenReturnsQuantity_ERET
        Get
            Return m_EndingOpenReturnsQuantity_ERET
        End Get
        Set(ByVal value As Decimal)
            m_EndingOpenReturnsQuantity_ERET = value
        End Set
    End Property

    <ColumnMapping("EndingPrice")> Public Property EndingPrice_EPRI() As Decimal Implements IStockLog.EndingPrice_EPRI
        Get
            Return m_EndingPrice_EPRI
        End Get
        Set(ByVal value As Decimal)
            m_EndingPrice_EPRI = value
        End Set
    End Property

    <ColumnMapping("EndingStock")> Public Property EndingStock_ESTK() As Decimal Implements IStockLog.EndingStock_ESTK
        Get
            Return m_EndingStock_ESTK
        End Get
        Set(ByVal value As Decimal)
            m_EndingStock_ESTK = value
        End Set
    End Property

    <ColumnMapping("EndingWriteOff")> Public Property EndingWriteOff_EWTF() As Decimal Implements IStockLog.EndingWriteOff_EWTF
        Get
            Return m_EndingWriteOff_EWTF
        End Get
        Set(ByVal value As Decimal)
            m_EndingWriteOff_EWTF = value
        End Set
    End Property

    <ColumnMapping("IsSentToHO")> Public Property IsSentToHO_ICOM() As Boolean Implements IStockLog.IsSentToHO_ICOM
        Get
            Return m_IsSentToHO_ICOM
        End Get
        Set(ByVal value As Boolean)
            m_IsSentToHO_ICOM = value
        End Set
    End Property

    <ColumnMapping("Keys")> Public Property Keys() As String Implements IStockLog.Keys
        Get
            Return m_Keys
        End Get
        Set(ByVal value As String)
            m_Keys = value
        End Set
    End Property

    <ColumnMapping("RTI")> Public Property RTI() As String Implements IStockLog.RTI
        Get
            Return m_RTI
        End Get
        Set(ByVal value As String)
            m_RTI = value
        End Set
    End Property

    <ColumnMapping("StartingMarkDown")> Public Property StartingMarkDown_SMDN() As Decimal Implements IStockLog.StartingMarkDown_SMDN
        Get
            Return m_StartingMarkDown_SMDN
        End Get
        Set(ByVal value As Decimal)
            m_StartingMarkDown_SMDN = value
        End Set
    End Property

    <ColumnMapping("StartingOpenReturns")> Public Property StartingOpenReturnsQuantity_SRET() As Decimal Implements IStockLog.StartingOpenReturnsQuantity_SRET
        Get
            Return m_StartingOpenReturnsQuantity_SRET
        End Get
        Set(ByVal value As Decimal)
            m_StartingOpenReturnsQuantity_SRET = value
        End Set
    End Property

    <ColumnMapping("StartingPrice")> Public Property StartingPrice_PRIC() As Decimal Implements IStockLog.StartingPrice_PRIC
        Get
            Return m_StartingPrice_PRIC
        End Get
        Set(ByVal value As Decimal)
            m_StartingPrice_PRIC = value
        End Set
    End Property

    <ColumnMapping("StartingStock")> Public Property StartingStock_SSTK() As Decimal Implements IStockLog.StartingStock_SSTK
        Get
            Return m_StartingStock_SSTK
        End Get
        Set(ByVal value As Decimal)
            m_StartingStock_SSTK = value
        End Set
    End Property

    <ColumnMapping("StartingWriteOff")> Public Property StartingWriteOff_SWTF() As Decimal Implements IStockLog.StartingWriteOff_SWTF
        Get
            Return m_StartingWriteOff_SWTF
        End Get
        Set(ByVal value As Decimal)
            m_StartingWriteOff_SWTF = value
        End Set
    End Property

    <ColumnMapping("LogDate")> Public Property StockLogDate_DATE1() As Date Implements IStockLog.StockLogDate_DATE1
        Get
            Return m_StockLogDate_DATE1
        End Get
        Set(ByVal value As Date)
            m_StockLogDate_DATE1 = value
        End Set
    End Property

    <ColumnMapping("Id")> Public Property StockLogId_TKEY() As Integer Implements IStockLog.StockLogId_TKEY
        Get
            Return m_StockLogId_TKEY
        End Get
        Set(ByVal value As Integer)
            m_StockLogId_TKEY = value
        End Set
    End Property

    <ColumnMapping("LogTime")> Public Property StockLogTime_TIME() As String Implements IStockLog.StockLogTime_TIME
        Get
            Return m_StockLogTime_TIME
        End Get
        Set(ByVal value As String)
            m_StockLogTime_TIME = value
        End Set
    End Property

    <ColumnMapping("TYPE")> Public Property StockLogType_TYPE() As String Implements IStockLog.StockLogType_TYPE
        Get
            Return m_StockLogType_TYPE
        End Get
        Set(ByVal value As String)
            m_StockLogType_TYPE = value
        End Set
    End Property

    <ColumnMapping("SKUN")> Public Property SKUN() As String Implements IStockLog.SKUN
        Get
            Return m_SKUN
        End Get
        Set(ByVal value As String)
            m_SKUN = value
        End Set
    End Property

#End Region

#Region "Constructors"

    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region "Interface"

    Public Function SaveStockLog(ByRef Con As Cts.Oasys.Data.Connection, _
                                 ByVal AdjustmentQty As Integer, _
                                 ByVal SACode As ISaCode, _
                                 ByVal Stock As IStock, _
                                 ByVal StockAdjust As IAdjustment, _
                                 ByVal EmployeeId As Integer, _
                                 Optional ByVal TransferSku As String = "") As Boolean Implements IStockLog.SaveStockLog

        Dim StockLogConnection As IWhichConnection

        StockLogConnection = (New WhichConnectionFactory).GetImplementation()
        StockLogConnection.ConnectionObject(CType(Con, IConnection))

        Return ActualSaveStockLog(Con, AdjustmentQty, SACode, Stock, StockAdjust, EmployeeId, TransferSku)

    End Function

    Public Overridable Function ReadStockLog(ByVal ProductCode As String) As Boolean Implements IStockLog.ReadStockLog

        Dim DataAccess As IStockLogRepository = StockLogRepositoryFactory.FactoryGet
        Dim dr As DataRow = DataAccess.GetLastStockLogForSkun(ProductCode)

        If dr IsNot Nothing Then
            Me.Load(dr)
            Return True
        Else
            Return False
        End If

    End Function

    Public Overridable Function ReadStockLog(ByRef Con As Connection, ByVal SkuNumber As String) As Boolean Implements IStockLog.ReadStockLog

        Reset()

        SetConnection(Con)

        SetSkuNumber(SkuNumber)

        CreateStockLogRepository()

        ExecuteGetStockLog()

        If DataRowIsNothing() = True Then Return False

        LoadMe()

        Return True

    End Function

    Public Function GetEmployeeCode(ByVal EmployeeId As Integer) As String Implements IStockLog.GetEmployeeCode

        Dim DataAccess As IStockLogRepository = StockLogRepositoryFactory.FactoryGet

        Return DataAccess.GetEmployeeCode(EmployeeId)

    End Function

#End Region

#Region "Private Procedures & Functions"

#Region "ReadStockLog"

    Friend Overridable Sub Reset()

        _Connection = Nothing
        _SkuNumber = Nothing
        _Repository = Nothing
        _DR = Nothing

    End Sub

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        _Connection = Con

    End Sub

    Friend Overridable Sub SetSkuNumber(ByVal SkuNumber As String)

        _SkuNumber = SkuNumber

    End Sub

    Friend Overridable Sub CreateStockLogRepository()

        _Repository = StockLogRepositoryFactory.FactoryGet

    End Sub

    Friend Overridable Sub ExecuteGetStockLog()

        _DR = _Repository.GetLastStockLogForSkun(_Connection, _SkuNumber)

    End Sub

    Friend Overridable Sub LoadMe()

        Me.Load(_DR)

    End Sub

    Friend Function DataRowIsNothing() As Boolean

        If _DR Is Nothing Then Return True

    End Function

#End Region

#Region "Rest"

    Private Function ActualSaveStockLog(ByRef Con As Connection, ByVal AdjustmentQty As Integer, ByVal SACode As ISaCode, ByVal Stock As IStock, ByVal StockAdjust As IAdjustment, ByVal EmployeeId As Integer, Optional ByVal TransferSku As String = "") As Boolean

        Dim DataAccess As IStockLogRepository = StockLogRepositoryFactory.FactoryGet
        Dim DataAccessStock As IStockRepository = StockRepositoryFactory.FactoryGet
        Dim oEnv As ISystemEnvironment = EnvironmentFactory.FactoryGet
        Dim strDate As String

        SetType(AdjustmentQty, SACode, Stock, StockAdjust.IsReversed)

        Dim CurrentStock As IStock = StockAdjustments.StockFactory.FactoryGet
        CurrentStock.ReadSKU(Con, Stock.SKUN)

        Dim StockLogType As String = Me.m_StockLogType_TYPE

        'Set the opening values

        m_StartingStock_SSTK = CurrentStock.OnHand_ONHA
        m_StartingOpenReturnsQuantity_SRET = CurrentStock.OpenReturnQty_RETQ
        m_StartingPrice_PRIC = CurrentStock.Price_PRIC
        m_StartingMarkDown_SMDN = CurrentStock.MarkdownQty_MDNQ
        m_StartingWriteOff_SWTF = CurrentStock.WriteOffStock_WTFQ

        If m_EndingStock_ESTK <> CurrentStock.OnHand_ONHA Or _
           m_EndingOpenReturnsQuantity_ERET <> CurrentStock.OpenReturnQty_RETQ Or _
           m_EndingMarkDown_EMDN <> CurrentStock.MarkdownQty_MDNQ Or _
           m_EndingWriteOff_EWTF <> CurrentStock.WriteOffStock_WTFQ Then

            m_StartingStock_SSTK = m_EndingStock_ESTK
            m_StartingOpenReturnsQuantity_SRET = m_EndingOpenReturnsQuantity_ERET
            m_StartingPrice_PRIC = m_EndingPrice_EPRI
            m_StartingMarkDown_SMDN = m_EndingPrice_EPRI
            m_StartingWriteOff_SWTF = m_EndingWriteOff_EWTF

            m_EndingStock_ESTK = CurrentStock.OnHand_ONHA
            m_EndingOpenReturnsQuantity_ERET = Stock.OpenReturnQty_RETQ
            m_EndingPrice_EPRI = Stock.Price_PRIC
            m_EndingMarkDown_EMDN = Stock.MarkdownQty_MDNQ
            m_EndingWriteOff_EWTF = Stock.WriteOffStock_WTFQ

            m_SKUN = Stock.SKUN
            m_StockLogType_TYPE = "91"
            m_StockLogDate_DATE1 = oEnv.Now.Date
            m_StockLogTime_TIME = oEnv.Now.ToString("HHmmss")
            m_DayNumber_DAYN = DateDiff(DateInterval.Day, CDate("01/01/1900"), oEnv.Now.Date) + 1
            m_EmployeeId_EEID = GetEmployeeCode(EmployeeId)
            m_Keys = "System Adjustment 91"
            m_RTI = "S"
            DataAccess.Save(Me, Con)

            m_StartingStock_SSTK = CurrentStock.OnHand_ONHA
            m_StartingOpenReturnsQuantity_SRET = CurrentStock.OpenReturnQty_RETQ
            m_StartingPrice_PRIC = CurrentStock.Price_PRIC
            m_StartingMarkDown_SMDN = CurrentStock.MarkdownQty_MDNQ
            m_StartingWriteOff_SWTF = CurrentStock.WriteOffStock_WTFQ

        End If

        m_EndingStock_ESTK = Stock.OnHand_ONHA
        m_EndingOpenReturnsQuantity_ERET = Stock.OpenReturnQty_RETQ
        m_EndingPrice_EPRI = Stock.Price_PRIC
        m_EndingMarkDown_EMDN = Stock.MarkdownQty_MDNQ
        m_EndingWriteOff_EWTF = Stock.WriteOffStock_WTFQ

        m_SKUN = Stock.SKUN
        m_StockLogType_TYPE = StockLogType

        m_StockLogDate_DATE1 = oEnv.Now.Date
        m_StockLogTime_TIME = oEnv.Now.ToString("HHmmss")
        m_DayNumber_DAYN = DateDiff(DateInterval.Day, CDate("01/01/1900"), oEnv.Now.Date) + 1
        m_EmployeeId_EEID = GetEmployeeCode(EmployeeId)

        strDate = m_StockLogDate_DATE1.ToString("dd-MM-yyyy")
        strDate = strDate.Replace("-", "/")
        If SACode.CodeType = "T" Then
            m_Keys = strDate & Space(2) & SACode.CodeNumber & Space(2) & TransferSku & Space(2) & StockAdjust.SEQN
        Else
            m_Keys = strDate & Space(2) & SACode.CodeNumber & Space(2) & m_SKUN & Space(2) & StockAdjust.SEQN
        End If

        m_RTI = "S"
        DataAccess.Save(Me, Con)
        Return True

    End Function

    Friend Function SetTypeASDave(ByVal AdjustmentQty As Integer, ByRef SACode As ISaCode, ByRef Stock As IStock) As Boolean

        m_StockLogType_TYPE = "35"

        If SACode.CodeSign = "+" Or SACode.CodeType = "T" Or _
         ((SACode.CodeSign = "S" Or SACode.CodeSign = "C") AndAlso AdjustmentQty >= 0) Then
            m_StockLogType_TYPE = "36"
        End If

        If SACode.IsMarkdown Then
            If AdjustmentQty > 0 Then
                m_StockLogType_TYPE = "67"
            Else
                m_StockLogType_TYPE = "66"
            End If
        End If

        If SACode.IsWriteOff Then
            If AdjustmentQty > 0 Then
                m_StockLogType_TYPE = "69"
            Else
                m_StockLogType_TYPE = "68"
            End If
        End If

        Return True

    End Function

    Friend Function SetType(ByVal AdjustmentQty As Integer, ByVal SACode As ISaCode, ByRef Stock As IStock, ByVal IsReversed As Boolean) As Boolean

        Dim Modify As IStockLogModifyAdjustmentCode

        Modify = (New StockLogModifyAdjustmentCodeFactory).GetImplementation
        Modify.ModifyAdjustmentCode(SACode)

        SetTypeWriteOff(SACode, IsReversed)

        SetTypeMarkDown(SACode, AdjustmentQty)

        SetTypeRest(SACode, AdjustmentQty)

    End Function

    Friend Sub SetTypeMarkDown(ByVal AdjustmentCode As ISaCode, ByVal AdjustmentQuantity As Integer)

        If AdjustmentCode.MarkdownWriteoff = String.Empty Then Exit Sub

        If AdjustmentCode.MarkdownWriteoff = "W" Then Exit Sub

        m_StockLogType_TYPE = "66"

        If AdjustmentQuantity > 0 Then m_StockLogType_TYPE = "67"

    End Sub

    Friend Sub SetTypeWriteOff(ByVal AdjustmentCode As ISaCode, ByVal IsReversed As Boolean)

        If AdjustmentCode.MarkdownWriteoff = "" Then Exit Sub

        If AdjustmentCode.MarkdownWriteoff = "M" Then Exit Sub

        m_StockLogType_TYPE = "68"

        If IsReversed = True Then m_StockLogType_TYPE = "69"

    End Sub

    Friend Sub SetTypeRest(ByVal AdjustmentCode As ISaCode, ByVal AdjustmentQuantity As Integer)
        If AdjustmentCode.MarkdownWriteoff = "W" Then Exit Sub

        If AdjustmentCode.MarkdownWriteoff = "M" Then Exit Sub

        m_StockLogType_TYPE = "31"
        If AdjustmentQuantity > 0 Then m_StockLogType_TYPE = "32"

        If AdjustmentCode.CodeNumber = "04" Then
            m_StockLogType_TYPE = "35"
            If AdjustmentQuantity > 0 Then
                m_StockLogType_TYPE = "36"
            End If
        End If

        If AdjustmentCode.IsIssueQuery = True Then m_StockLogType_TYPE = "35"

        If AdjustmentQuantity > 0 And AdjustmentCode.IsIssueQuery = True Then m_StockLogType_TYPE = "36"
    End Sub

#End Region

#End Region

End Class