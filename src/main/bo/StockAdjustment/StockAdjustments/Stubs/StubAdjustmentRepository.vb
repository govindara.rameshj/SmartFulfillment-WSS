﻿Option Strict On

Public Class StubAdjustmentRepository

    Implements IAdjustmentRepository

    Public Function GetEmployeeInitials(ByVal Id As Integer) As String Implements IAdjustmentRepository.GetEmployeeInitials
        Return Nothing
    End Function

    Public Function ReadAdjustment(ByRef Con As Cts.Oasys.Data.Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustment
        Return Nothing
    End Function

    Public Function ReadAdjustmentDate() As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustmentDate
        Return Nothing
    End Function

    Public Function SaveAdjustment(ByVal Adjustment As IAdjustment, ByRef Con As Cts.Oasys.Data.Connection) As Boolean Implements IAdjustmentRepository.SaveAdjustment
        Return True
    End Function

    Public Function ReadAdjustmentExcludingMarkdownSales(ByRef Con As Cts.Oasys.Data.Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustmentExcludingMarkdownSales
        Return Nothing
    End Function

    Public Function GetNextSequenceNumberForSkunCodeDate(ByVal Skun As String, ByVal Code As String, ByVal AdjDate As Date) As String Implements IAdjustmentRepository.GetNextSequenceNumberForSkunCodeDate
        Return String.Empty
    End Function

    Public Function GetNextSequenceNumberForSkunCodeDate(ByRef Con As Cts.Oasys.Data.Connection, ByVal SkuNumber As String, ByVal AdjustmentCode As String, ByVal AdjDate As Date) As String Implements IAdjustmentRepository.GetNextSequenceNumberForSkunCodeDate

        Return String.Empty

    End Function

End Class