﻿Public Class StubAdjustment
    Implements IAdjustment

    Private m_AdjustmentDate As Date = Nothing
    Private m_Code As String = String.Empty
    Private m_SKUN As String = String.Empty
    Private m_SEQN As String = String.Empty
    Private m_AmendId As Integer = 0
    Private m_EmployeeInitials As String = String.Empty
    Private m_Department As String = String.Empty
    Private m_StartingStock As Decimal = 0@
    Private m_Quantity As Decimal = 0@
    Private m_Price As Decimal = 0@
    Private m_Cost As Decimal = 0@
    Private m_IsSentToHO As Boolean = False
    Private m_AdjustType As String = String.Empty
    Private m_Comment As String = String.Empty
    Private m_DeliveryNumber As String = "000000"
    Private m_ReversalCode As String = String.Empty
    Private m_MarkDownOrWriteOff As String = String.Empty
    Private m_WriteOffAuthority As String = "0"
    Private m_WriteOffAuthorityDate As Date = Nothing
    Private m_PeriodId As Integer = 0
    Private m_TransferSku As String = "000000"
    Private m_TransferValue As Decimal = 0@
    Private m_TransferStart As Decimal = 0@
    Private m_TransferPrice As Decimal = 0@
    Private m_IsReversed As Boolean = False
    Private m_ExistsInDatabase As Boolean = False
    Private m_RTI As String = "N"
    Private m_EndingValue As Decimal = 0

#Region "Properties"

    Public Property AdjustmentDate() As Date Implements IAdjustment.AdjustmentDate_DATE1
        Get
            Return m_AdjustmentDate
        End Get
        Set(ByVal value As Date)
            m_AdjustmentDate = value
        End Set
    End Property

    Public Property AdjustType() As String Implements IAdjustment.AdjustType
        Get
            Return m_AdjustType
        End Get
        Set(ByVal value As String)
            m_AdjustType = value
        End Set
    End Property

    Public Property AmendId() As Integer Implements IAdjustment.AmendId
        Get
            Return m_AmendId
        End Get
        Set(ByVal value As Integer)
            m_AmendId = value
        End Set
    End Property


    Public Property Code() As String Implements IAdjustment.Code
        Get
            Return m_Code
        End Get
        Set(ByVal value As String)
            m_Code = value
        End Set
    End Property

    Public Property Comment() As String Implements IAdjustment.Comment_INFO
        Get
            Return m_Comment
        End Get
        Set(ByVal value As String)
            m_Comment = value
        End Set
    End Property

    Public Property Cost() As Decimal Implements IAdjustment.Cost
        Get
            Return m_Cost
        End Get
        Set(ByVal value As Decimal)
            m_Cost = value
        End Set
    End Property

    Public Property DeliveryNumber() As String Implements IAdjustment.DeliveryNumber_DRLN
        Get
            Return m_DeliveryNumber
        End Get
        Set(ByVal value As String)
            m_DeliveryNumber = value
        End Set
    End Property

    Public Property Department() As String Implements IAdjustment.Department
        Get
            Return m_Department
        End Get
        Set(ByVal value As String)
            m_Department = value
        End Set
    End Property

    Public Property EmployeeInitials() As String Implements IAdjustment.EmployeeInitials_INIT
        Get
            Return m_EmployeeInitials
        End Get
        Set(ByVal value As String)
            m_EmployeeInitials = value
        End Set
    End Property

    Public Property ReversalCode() As String Implements IAdjustment.ReversalCode_RCOD
        Get
            Return m_ReversalCode
        End Get
        Set(ByVal value As String)
            m_ReversalCode = value
        End Set
    End Property

    Public Property IsReversed() As Boolean Implements IAdjustment.IsReversed
        Get
            Return m_IsReversed
        End Get
        Set(ByVal value As Boolean)
            m_IsReversed = value
        End Set
    End Property

    Public Property IsSentToHO() As Boolean Implements IAdjustment.IsSentToHO_COMM
        Get
            Return m_IsSentToHO
        End Get
        Set(ByVal value As Boolean)
            m_IsSentToHO = value
        End Set
    End Property

    Public Property MarkDownOrWriteOff() As String Implements IAdjustment.MarkDownOrWriteOff_MOWT
        Get
            Return m_MarkDownOrWriteOff
        End Get
        Set(ByVal value As String)
            m_MarkDownOrWriteOff = value
        End Set
    End Property

    Public Property PeriodId() As Integer Implements IAdjustment.PeriodId
        Get
            Return m_PeriodId
        End Get
        Set(ByVal value As Integer)
            m_PeriodId = value
        End Set
    End Property

    Public Property Price() As Decimal Implements IAdjustment.Price
        Get
            Return m_Price
        End Get
        Set(ByVal value As Decimal)
            m_Price = value
        End Set
    End Property

    Public Property Quantity() As Decimal Implements IAdjustment.Quantity_QUAN
        Get
            Return m_Quantity
        End Get
        Set(ByVal value As Decimal)
            m_Quantity = value
        End Set
    End Property

    Public Property RTI() As String Implements IAdjustment.RTI
        Get
            Return m_RTI
        End Get
        Set(ByVal value As String)
            m_RTI = value
        End Set
    End Property

    Public Property SEQN() As String Implements IAdjustment.SEQN
        Get
            Return m_SEQN
        End Get
        Set(ByVal value As String)
            m_SEQN = value
        End Set
    End Property

    Public Property SKUN() As String Implements IAdjustment.SKUN
        Get
            Return m_SKUN
        End Get
        Set(ByVal value As String)
            m_SKUN = value
        End Set
    End Property

    Public Property StartingStock() As Decimal Implements IAdjustment.StartingStock_SSTK
        Get
            Return m_StartingStock
        End Get
        Set(ByVal value As Decimal)
            m_StartingStock = value
        End Set
    End Property

    Public Property TransferSku_TSKU() As String Implements IAdjustment.TransferSku_TSKU
        Get
            Return m_TransferSku
        End Get
        Set(ByVal value As String)
            m_TransferSku = value
        End Set
    End Property

    Public Property TransferValue() As Decimal Implements IAdjustment.TransferValue
        Get
            Return m_TransferValue
        End Get
        Set(ByVal value As Decimal)
            m_TransferValue = value
        End Set
    End Property

    Public Property TransferPrice() As Decimal Implements IAdjustment.TransferPrice
        Get
            Return m_TransferPrice
        End Get
        Set(ByVal value As Decimal)
            m_TransferPrice = value
        End Set
    End Property

    Public Property TransferStart() As Decimal Implements IAdjustment.TransferStart
        Get
            Return m_TransferStart
        End Get
        Set(ByVal value As Decimal)
            m_TransferStart = value
        End Set
    End Property

    Public Property WriteOffAuthority() As String Implements IAdjustment.WriteOffAuthority_WAUT
        Get
            Return m_WriteOffAuthority
        End Get
        Set(ByVal value As String)
            m_WriteOffAuthority = value
        End Set
    End Property

    Public Property WriteOffAuthorityDate() As Date Implements IAdjustment.WriteOffAuthorityDate_DAUT
        Get
            Return m_WriteOffAuthorityDate
        End Get
        Set(ByVal value As Date)
            m_WriteOffAuthorityDate = value
        End Set
    End Property

    Public Property ExistsInDatabase() As Boolean Implements IAdjustment.ExistsInDatabase
        Get
            Return m_ExistsInDatabase
        End Get
        Set(ByVal value As Boolean)
            m_ExistsInDatabase = value
        End Set
    End Property

    Public Property EndingStock() As Decimal Implements IAdjustment.EndingStock

        Get
            Return m_StartingStock + m_Quantity
        End Get
        Set(ByVal value As Decimal)
            m_EndingValue = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function CalculateAdjustment() As Boolean Implements IAdjustment.CalculateAdjustment
        Return True
    End Function

    Public Function GetEmployeeInitials(ByVal Id As Integer) As String Implements IAdjustment.GetEmployeeInitials
        Return "MO'C"
    End Function

    Public Sub LoadFromRow(ByVal dr As System.Data.DataRow) Implements IAdjustment.LoadFromRow

    End Sub

    Public Function ReadAdjustment(ByRef Con As Cts.Oasys.Data.Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As Boolean Implements IAdjustment.ReadAdjustment
        Return True
    End Function

    Public Function ReadAdjustmentDate() As Date Implements IAdjustment.ReadAdjustmentDate
        Return Now
    End Function

    Public Function Validate(ByVal stock As IStock) As Boolean Implements IAdjustment.Validate
        Return True
    End Function

    Public Function SaveAdjustment(ByRef Con As Cts.Oasys.Data.Connection, ByVal AdjQty As Integer, ByVal AdjDate As Date, ByVal SkuNumber As String, ByVal DRLNumber As String, ByVal Comment As String, ByVal SACode As ISaCode, ByVal Stock As IStock, ByVal StockLog As IStockLog, ByVal EmployeeId As Integer, Optional ByVal TransferSku As String = "", Optional ByVal TransferValue As Decimal = 0, Optional ByVal TransferPrice As Decimal = 0, Optional ByVal TransferStart As Decimal = 0, Optional ByVal PicCountAcceptance As Boolean = False, Optional ByVal PicCountAcceptanceCode53 As Boolean = False) As Boolean Implements IAdjustment.SaveAdjustment

        Dim AdjFactory As New StubAdjustmentRepository

        AdjustmentRepositoryFactory.FactorySet(AdjFactory)
        Dim AdjustmentRep As IAdjustmentRepository = AdjustmentRepositoryFactory.FactoryGet

        m_Code = SACode.CodeNumber
        m_AdjustmentDate = AdjDate.Date
        m_SKUN = SkuNumber
        m_Quantity = AdjQty
        m_EmployeeInitials = GetEmployeeInitials(EmployeeId)
        m_Department = Stock.Department

        If StockLog.SKUN <> String.Empty Then 'Make sure there was a stocklog record
            m_StartingStock = StockLog.EndingStock_ESTK
        End If

        If m_ExistsInDatabase = False Then
            If m_SEQN = String.Empty Then
                m_SEQN = "00"
            Else
                Dim seqn As Integer = CInt(m_SEQN) + 1
                m_SEQN = seqn.ToString.PadLeft(2, "0"c)
            End If
        End If

        m_Price = Stock.Price_PRIC
        m_Cost = Stock.Cost
        m_IsSentToHO = False
        m_AdjustType = SACode.CodeType

        If Comment <> String.Empty Then
            m_Comment = Comment
        End If

        If DRLNumber <> String.Empty Then
            m_DeliveryNumber = DRLNumber
        End If

        If SACode.MarkdownWriteoff.Length > 0 Then
            m_MarkDownOrWriteOff = SACode.MarkdownWriteoff.Substring(0, 1)
        End If

        m_RTI = "N"

        If TransferSku <> String.Empty Then
            m_TransferSku = TransferSku
            m_TransferPrice = TransferPrice
            m_TransferValue = ((m_Price * AdjQty) - (TransferPrice * AdjQty))
        End If

        Return AdjustmentRep.SaveAdjustment(Me, Con)
        Return True

    End Function

    Public Sub MakeEqual(ByVal Value As IAdjustment) Implements IAdjustment.MakeEqual


    End Sub

    Public Function UpdateAdjustment(ByRef Con As Connection) As Boolean Implements IAdjustment.UpdateAdjustment

    End Function

#End Region

End Class