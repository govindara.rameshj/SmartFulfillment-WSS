﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Data

Public Class StubRepository
    Implements IStockRepository

    Private m_stock As IStock

    Public Sub Update(ByVal stock As IStock, ByRef Con As Connection) Implements IStockRepository.Update
        ' no implementation
    End Sub

    Public Function ReadSKU(ByRef con As Connection, ByVal ProductCode As String) As System.Data.DataRow Implements IStockRepository.ReadSKU
        Dim dr As DataRow = Nothing
        Return dr
    End Function

    Public Function GetReducedStocksDataTable() As System.Data.DataTable Implements IStockRepository.GetReducedStocksDataTable
        Return Nothing
    End Function

    Public Function GetStoreNumber() As String Implements IStockRepository.GetStoreNumber
        Return Nothing
    End Function

    Public Function DoesSkuExist(ByVal ProductCode As String) As Boolean Implements IStockRepository.DoesSkuExist
        Return Nothing
    End Function


End Class
