﻿Public Class SACodesFactory
    Private Shared m_FactoryMember As ISACodes = Nothing

    Public Shared Function FactoryGet() As ISACodes
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New SACode.SACodes
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As ISACodes)
        m_FactoryMember = obj
    End Sub

End Class
