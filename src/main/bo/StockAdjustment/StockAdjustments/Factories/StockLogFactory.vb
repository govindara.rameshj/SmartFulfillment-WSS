﻿Public Class StockLogFactory

    Private Shared m_FactoryMember As IStockLog = Nothing

    Public Shared Function FactoryGet() As IStockLog
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New StockLog
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IStockLog)
        m_FactoryMember = obj
    End Sub

End Class

