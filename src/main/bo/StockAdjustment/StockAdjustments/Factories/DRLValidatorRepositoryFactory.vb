﻿Public Class DRLValidatorRepositoryFactory
    Private Shared m_FactoryMember As IDRLValidatorRepository = Nothing

    Public Shared Function FactoryGet() As IDRLValidatorRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New DRLValidatorRepository
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IDRLValidatorRepository)
        m_FactoryMember = obj
    End Sub

End Class

