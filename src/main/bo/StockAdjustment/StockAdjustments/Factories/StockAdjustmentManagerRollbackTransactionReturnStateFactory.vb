﻿Option Strict On

Public Class StockAdjustmentManagerRollbackTransactionReturnStateFactory
    Inherits RequirementSwitchFactory(Of Boolean)

    Private Const _RequirementSwitchPO15 As Integer = -15

    Public Overrides Function ImplementationA() As Boolean

        Return False

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchPO15)

    End Function

    Public Overrides Function ImplementationB() As Boolean

        Return True

    End Function

End Class