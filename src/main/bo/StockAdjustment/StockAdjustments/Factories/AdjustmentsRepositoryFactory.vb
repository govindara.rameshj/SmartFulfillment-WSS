﻿Public Class AdjustmentsRepositoryFactory

    Private Shared m_FactoryMember As IAdjustmentsRepository = Nothing

    Public Shared Function FactoryGet() As IAdjustmentsRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New AdjustmentsRepositoryOld
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IAdjustmentsRepository)
        m_FactoryMember = obj
    End Sub

End Class
