﻿Option Strict On
Option Explicit On

Public Class HandHeldTerminalHeaderRepositoryFactory

    Private Shared m_FactoryMember As IHandHeldTerminalHeaderRepository = Nothing

    Public Shared Function FactoryGet() As IHandHeldTerminalHeaderRepository
        If m_FactoryMember Is Nothing Then
            Return New HandHeldTerminalHeaderRepository    'live implementation
        Else
            Return m_FactoryMember               'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IHandHeldTerminalHeaderRepository)
        m_FactoryMember = obj
    End Sub

End Class
