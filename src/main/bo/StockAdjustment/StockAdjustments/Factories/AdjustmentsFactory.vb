﻿Public Class AdjustmentsFactory
    Private Shared m_FactoryMember As IAdjustments = Nothing

    Public Shared Function FactoryGet() As IAdjustments
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New Adjustments
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IAdjustments)
        m_FactoryMember = obj
    End Sub

End Class

