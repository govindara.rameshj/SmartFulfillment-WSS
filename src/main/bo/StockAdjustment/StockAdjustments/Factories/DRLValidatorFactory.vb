﻿Public Class DRLValidatorFactory

    Private Shared m_FactoryMember As IDRLValidator = Nothing

    Public Shared Function FactoryGet() As IDRLValidator
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New DRLValidator
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IDRLValidator)
        m_FactoryMember = obj
    End Sub

End Class


