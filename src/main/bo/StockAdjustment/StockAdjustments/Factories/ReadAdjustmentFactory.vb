﻿Public Class ReadAdjustmentFactory
    Inherits RequirementSwitchFactory(Of IAdjustmentRepository)

    Public Overrides Function ImplementationA() As IAdjustmentRepository

        Return New AdjustmentRepository
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As TpWickes.Library.IRequirementRepository = TpWickes.Library.RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-909)
    End Function

    Public Overrides Function ImplementationB() As IAdjustmentRepository

        Return New AdjustmentRepositoryOld
    End Function


End Class
