﻿Public Class AdjustmentFactory

    Private Shared m_FactoryMember As IAdjustment = Nothing

    Public Shared Function FactoryGet() As IAdjustment
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New Adjustment
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IAdjustment)
        m_FactoryMember = obj
    End Sub

End Class
