﻿Public Class StockAdjustmentPriceFactory
    Inherits BaseFactory(Of IStockAdjustmentPrice)

    Public Overrides Function Implementation() As IStockAdjustmentPrice

        Return New StockAdjustmentPrice

    End Function

End Class