﻿Public Class SACodeFactory
    Private Shared m_FactoryMember As ISaCode = Nothing

    Public Shared Function FactoryGet() As ISaCode
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New SACode.SaCode
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As ISaCode)
        m_FactoryMember = obj
    End Sub

End Class
