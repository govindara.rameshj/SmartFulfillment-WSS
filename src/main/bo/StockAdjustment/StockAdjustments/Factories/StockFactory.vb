﻿Public Class StockFactory
    Private Shared m_FactoryMember As IStock = Nothing

    Public Shared Function FactoryGet() As IStock
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New Stock
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IStock)
        m_FactoryMember = obj
    End Sub

End Class

