﻿Public Class SACodesRepositoryFactory
    Private Shared m_FactoryMember As ISACodesRepository = Nothing

    Public Shared Function FactoryGet() As ISACodesRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New SACode.SACodesRepository
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As ISACodesRepository)
        m_FactoryMember = obj
    End Sub

End Class

