﻿' TODO this calss can be replaced by ISystemEnvironment implementation from Core2 dll. If you are working around it and able to test it, please ,do it.
Public Class EnvironmentFactory

    Private Shared m_EnvironmentFactory As ISystemEnvironment = Nothing

    Public Shared Function FactoryGet() As ISystemEnvironment

        If m_EnvironmentFactory Is Nothing Then                 'using real environment

            Return New LiveEnvironment

        Else

            Return m_EnvironmentFactory                         'using test stub database

        End If

    End Function

    Public Shared Sub FactorySet(ByVal env As ISystemEnvironment)

        m_EnvironmentFactory = env

    End Sub

End Class
