﻿Public Class ConnexionFactory
    Private Shared m_FactoryMember As IConnecxion = Nothing

    Public Shared Function FactoryGet() As IConnecxion
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New Connecxion
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IConnecxion)
        m_FactoryMember = obj
    End Sub

End Class


