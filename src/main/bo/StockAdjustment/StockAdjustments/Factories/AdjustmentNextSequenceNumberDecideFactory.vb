﻿Option Strict On

Public Class AdjustmentNextSequenceNumberDecideFactory
    Inherits RequirementSwitchFactory(Of IAdjustmentNextSequenceNumberDecide)

    Private Const _RequirementSwitchPO15 As Integer = -15

    Public Overrides Function ImplementationA() As IAdjustmentNextSequenceNumberDecide

        Return New AdjustmentNextSequenceNumberGenerate

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchPO15)

    End Function

    Public Overrides Function ImplementationB() As IAdjustmentNextSequenceNumberDecide

        Return New AdjustmentNextSequenceNumberNotGenerate

    End Function

End Class
