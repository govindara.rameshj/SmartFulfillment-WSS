﻿Option Strict On
Option Explicit On


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Dhanesh Ramachandran
' Date     : 22/08/2011
' Referral : RF0865
' Notes    : Added for Stock Movements Detail Report to display correct adjustment code
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Class StockLogWriteFactory
    Inherits BaseFactory(Of IStockLogWrite)

    Public Overrides Function Implementation() As IStockLogWrite

        Return New StockLogWriteNew
    End Function
End Class
