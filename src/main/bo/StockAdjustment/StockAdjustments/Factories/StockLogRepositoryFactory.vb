﻿Public Class StockLogRepositoryFactory

    Private Shared m_FactoryMember As IStockLogRepository = Nothing

    Public Shared Function FactoryGet() As IStockLogRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New StockLogRepository
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IStockLogRepository)
        m_FactoryMember = obj
    End Sub

End Class

