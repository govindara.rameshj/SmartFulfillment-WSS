﻿Option Strict On
Option Explicit On

Public Class HandHeldTerminalDetailRepositoryFactory

    Private Shared m_FactoryMember As IHandHeldTerminalDetailRepository = Nothing

    Public Shared Function FactoryGet() As IHandHeldTerminalDetailRepository
        If m_FactoryMember Is Nothing Then
            Return New HandHeldTerminalDetailRepository     'live implementation
        Else
            Return m_FactoryMember                          'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IHandHeldTerminalDetailRepository)
        m_FactoryMember = obj
    End Sub

End Class
