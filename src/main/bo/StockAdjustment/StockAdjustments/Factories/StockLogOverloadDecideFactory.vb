﻿Option Strict On

Public Class StockLogOverloadDecideFactory
    Inherits RequirementSwitchFactory(Of IStockLogOverloadDecide)

    Private Const _RequirementSwitchPO15 As Integer = -15

    Public Overrides Function ImplementationA() As IStockLogOverloadDecide

        Return New StockLogOverloadDecideUseConnection

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchPO15)

    End Function

    Public Overrides Function ImplementationB() As IStockLogOverloadDecide

        Return New StockLogOverloadDecideIgnoreConnection

    End Function

End Class