﻿Option Strict On

Public Class StockAdjustmentFixesFactory
    Inherits RequirementSwitchFactory(Of IStockAdjustmentFixes)

    Private Const _RequirementSwitch901 As Integer = -901

    Public Overrides Function ImplementationA() As IStockAdjustmentFixes

        Return New StockAdjustmentFixes
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As TpWickes.Library.IRequirementRepository = TpWickes.Library.RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitch901)
    End Function

    Public Overrides Function ImplementationB() As IStockAdjustmentFixes

        Return New StockAdjustmentFixesExisting
    End Function
End Class