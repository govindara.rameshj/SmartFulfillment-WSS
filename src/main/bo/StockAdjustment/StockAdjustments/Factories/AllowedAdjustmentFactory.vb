﻿Public Class AllowedAdjustmentFactory
    Inherits BaseFactory(Of IAllowedAdjustment)

    Public Overrides Function Implementation() As IAllowedAdjustment

        Return New AllowedAdjustmentNew

    End Function

End Class