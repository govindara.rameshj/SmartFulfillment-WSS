﻿Public Class StockRepositoryFactory
    Private Shared m_FactoryMember As IStockRepository = Nothing

    Public Shared Function FactoryGet() As IStockRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New StockRepository
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IStockRepository)
        m_FactoryMember = obj
    End Sub

End Class
