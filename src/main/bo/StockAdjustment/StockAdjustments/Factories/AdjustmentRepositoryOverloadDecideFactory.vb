﻿Option Strict On

Public Class AdjustmentRepositoryOverloadDecideFactory
    Inherits RequirementSwitchFactory(Of IAdjustmentRepositoryOverloadDecide)

    Private Const _RequirementSwitchPO15 As Integer = -15

    Public Overrides Function ImplementationA() As IAdjustmentRepositoryOverloadDecide

        Return New AdjustmentRepositoryOverloadDecideUseConnection

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchPO15)

    End Function

    Public Overrides Function ImplementationB() As IAdjustmentRepositoryOverloadDecide

        Return New AdjustmentRepositoryOverloadDecideIgnoreConnection

    End Function

End Class