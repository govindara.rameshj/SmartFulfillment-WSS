﻿Option Strict On
Option Explicit On

Public Class HandHeldTerminalDetailFactory

    Private Shared m_FactoryMember As IHandHeldTerminalDetail = Nothing

    Public Shared Function FactoryGet() As IHandHeldTerminalDetail
        If m_FactoryMember Is Nothing Then
            Return New HandHeldTerminalDetail     'live implementation
        Else
            Return m_FactoryMember                'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IHandHeldTerminalDetail)
        m_FactoryMember = obj
    End Sub

End Class

Public Class HandHeldTerminalDetailCollectionFactory

    Private Shared m_FactoryMember As IHandHeldTerminalDetailCollection = Nothing

    Public Shared Function FactoryGet() As IHandHeldTerminalDetailCollection
        If m_FactoryMember Is Nothing Then
            Return New HandHeldTerminalDetailCollection      'live implementation
        Else
            Return m_FactoryMember                           'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IHandHeldTerminalDetailCollection)
        m_FactoryMember = obj
    End Sub

End Class
