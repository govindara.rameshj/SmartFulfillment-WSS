﻿Option Strict On
Option Explicit On

Public Class HandHeldTerminalHeaderFactory

    Private Shared m_FactoryMember As IHandHeldTerminalHeader = Nothing

    Public Shared Function FactoryGet() As IHandHeldTerminalHeader
        If m_FactoryMember Is Nothing Then
            Return New HandHeldTerminalHeader    'live implementation
        Else
            Return m_FactoryMember               'stub implementation
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IHandHeldTerminalHeader)
        m_FactoryMember = obj
    End Sub

End Class