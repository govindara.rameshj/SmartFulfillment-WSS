﻿Public Class AdjustmentRepositoryFactory

    Private Shared m_FactoryMember As IAdjustmentRepository = Nothing

    Public Shared Function FactoryGet() As IAdjustmentRepository
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New AdjustmentRepositoryOld
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IAdjustmentRepository)
        m_FactoryMember = obj
    End Sub

End Class

