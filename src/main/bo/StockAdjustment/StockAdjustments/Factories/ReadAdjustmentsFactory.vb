﻿Public Class ReadAdjustmentsFactory
    Inherits RequirementSwitchFactory(Of IAdjustmentsRepository)

    Public Overrides Function ImplementationA() As IAdjustmentsRepository

        Return New AdjustmentsRepository
    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean
        Dim SwitchRepository As TpWickes.Library.IRequirementRepository = TpWickes.Library.RequirementRepositoryFactory.FactoryGet()

        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(-909)
    End Function

    Public Overrides Function ImplementationB() As IAdjustmentsRepository

        Return New AdjustmentsRepositoryOld
    End Function


End Class
