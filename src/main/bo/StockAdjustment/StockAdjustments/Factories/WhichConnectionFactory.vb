﻿Option Strict On

Public Class WhichConnectionFactory
    Inherits RequirementSwitchFactory(Of IWhichConnection)

    Private Const _RequirementSwitchPO15 As Integer = -15

    Public Overrides Function ImplementationA() As IWhichConnection

        Return New UseExistingConnection

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchPO15)

    End Function

    Public Overrides Function ImplementationB() As IWhichConnection

        Return New UseNewConnection

    End Function

End Class