﻿Option Strict On

Public Class StockLogModifyAdjustmentCodeFactory
    Inherits RequirementSwitchFactory(Of IStockLogModifyAdjustmentCode)

    Private Const _RequirementSwitchPO15 As Integer = -15

    Public Overrides Function ImplementationA() As IStockLogModifyAdjustmentCode

        Return New StockLogAdjustmentCodeUnaltered

    End Function

    Public Overrides Function ImplementationA_IsActive() As Boolean

        Dim SwitchRepository As IRequirementRepository

        SwitchRepository = RequirementRepositoryFactory.FactoryGet()
        ImplementationA_IsActive = SwitchRepository.IsSwitchPresentAndEnabled(_RequirementSwitchPO15)

    End Function

    Public Overrides Function ImplementationB() As IStockLogModifyAdjustmentCode

        Return New StockLogAdjustmentCodeIncorrectlyModified

    End Function

End Class