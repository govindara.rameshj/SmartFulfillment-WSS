﻿Public Class StockAdjustmentManagerFactory
    Private Shared m_FactoryMember As IStockAdjustmentManager = Nothing

    Public Shared Function FactoryGet() As IStockAdjustmentManager
        If m_FactoryMember Is Nothing Then
            'using live implementation
            Return New StockAdjustmentManager
        Else
            'using stub implementation
            Return m_FactoryMember
        End If
    End Function

    Public Shared Sub FactorySet(ByVal obj As IStockAdjustmentManager)
        m_FactoryMember = obj
    End Sub

End Class

