﻿Option Strict On
Option Explicit On

<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Pic.Build.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                            "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                            "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                            "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                            "54e0a4a4"), _
 Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("StockAdjustments.UnitTest, PublicKey=00240000048000009400000006020000002400005253413100040000010001003f4cbff41a165c" & _
                                                                                            "9d6cb216ad505a05b2f0d07ea65599f03a8258b9a19af66f8a203ab99d5c0d0b70a87aa640c308" & _
                                                                                            "3d5e01a739b5b21b4a9e6f665d641056801605e94680cd45cd2f70785e15d043f9fd8af036b588" & _
                                                                                            "97125de587bc610d2fa293e403ad3394f0060b1ff188a43f6d191445612049ab032cafadc6da38" & _
                                                                                            "54e0a4a4")> 

Public Class HandHeldTerminalDetailRepositoryStub
    Inherits Cts.Oasys.Core.Base

    Implements IHandHeldTerminalDetailRepository

#Region "Stub Code"

    Private _ReadDT As System.Data.DataTable
    Private _ReadRefundedItemsDT As System.Data.DataTable

    Friend Sub ConfigureRead(ByVal DT As DataTable)

        _ReadDT = DT

    End Sub

    Friend Sub ConfigureReadRefundedItems(ByVal DT As DataTable)

        _ReadRefundedItemsDT = DT

    End Sub

#End Region

#Region "Interface"

    Public Function Read(ByVal SelectedDate As Date) As System.Data.DataTable Implements IHandHeldTerminalDetailRepository.Read

        Return _ReadDT

    End Function

    Public Sub Update(ByVal SelectedDate As Date, ByVal ProductCode As String, ByVal AdjustmentApplied As Boolean) Implements IHandHeldTerminalDetailRepository.Update

    End Sub

#End Region

End Class