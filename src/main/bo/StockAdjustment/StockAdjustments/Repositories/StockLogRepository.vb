﻿Option Strict On

Public Class StockLogRepository
    Inherits Base

    Implements IStockLogRepository

    Protected _Connection As IConnection
    Protected _Command As ICommand
    Protected _DT As DataTable

    Protected _WhichConnection As IWhichConnection

    Protected _StockLog As IStockLog
    Protected _SkuNumber As String

#Region "Interface"

    Public Function Save(ByVal StockLog As IStockLog, ByRef Con As Connection) As Boolean Implements IStockLogRepository.Save

        Reset()

        SetConnection(Con)

        SetStockLog(StockLog)

        CreateWhichConnection()
        DecideWhichConnectionToUse()

        ExecuteSave()

    End Function

    Public Function GetLastStockLogForSkun(ByVal PartCode As String) As System.Data.DataRow Implements IStockLogRepository.GetLastStockLogForSkun

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetLastStockLogForSku
                com.AddParameter(My.Resources.Parameters.ProductCode, PartCode)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0)
                End If
            End Using
        End Using
        Return Nothing

    End Function

    Public Function GetLastStockLogForSkun(ByRef Con As Connection, ByVal SkuNumber As String) As DataRow Implements IStockLogRepository.GetLastStockLogForSkun

        Reset()

        SetConnection(Con)

        SetSkuNumber(SkuNumber)

        Return ExecuteGetStockLog()

    End Function

    Public Function GetEmployeeCode(ByVal EmployeeId As Integer) As String Implements IStockLogRepository.GetEmployeeCode

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetEmployeeCode
                com.AddParameter(My.Resources.Parameters.Id, EmployeeId)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return CType(dt.Rows(0).Item(0), String)
                End If
            End Using
        End Using
        Return String.Empty

    End Function

#End Region

#Region "Private Procedures & Functions"

#Region "Rest"

    Friend Overridable Sub Reset()

        _Connection = Nothing
        _Command = Nothing
        _DT = Nothing
        _WhichConnection = Nothing
        _StockLog = Nothing
        _SkuNumber = Nothing

    End Sub

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        _Connection = Con

    End Sub

    Friend Overridable Sub SetStockLog(ByVal StockLog As IStockLog)

        _StockLog = StockLog

    End Sub

    Friend Overridable Sub SetSkuNumber(ByVal SkuNumber As String)

        _SkuNumber = SkuNumber

    End Sub

    Friend Overridable Sub CreateWhichConnection()

        _WhichConnection = (New WhichConnectionFactory).GetImplementation()

    End Sub

    Friend Overridable Sub DecideWhichConnectionToUse()

        _WhichConnection.ConnectionObject(CType(_Connection, IConnection))

    End Sub

#End Region

#Region "Method: Save"

    Friend Overridable Sub ExecuteSave()

        ConfigureSaveCommand()
        ExecuteSaveCommand()

    End Sub

    Friend Overridable Sub ConfigureSaveCommand()

        If ConnectionIsNothing() = True Then Exit Sub
        If StockLogIsNothing() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))

        _Command.StoredProcedureName = My.Resources.Procedure.InsertStockLogForSku
        _Command.AddParameter(My.Resources.Parameters.SkuNumber, _StockLog.SKUN)
        _Command.AddParameter(My.Resources.Parameters.DayNumber, _StockLog.DayNumber_DAYN)
        _Command.AddParameter(My.Resources.Parameters.Type, _StockLog.StockLogType_TYPE)
        _Command.AddParameter(My.Resources.Parameters.AdjustmentDate, _StockLog.StockLogDate_DATE1)
        _Command.AddParameter(My.Resources.Parameters.AdjustmentTime, _StockLog.StockLogTime_TIME)
        _Command.AddParameter(My.Resources.Parameters.Keys, _StockLog.Keys)
        _Command.AddParameter(My.Resources.Parameters.UserId, _StockLog.EmployeeId_EEID)
        _Command.AddParameter(My.Resources.Parameters.StartingStock, _StockLog.StartingStock_SSTK)
        _Command.AddParameter(My.Resources.Parameters.EndingStock, _StockLog.EndingStock_ESTK)
        _Command.AddParameter(My.Resources.Parameters.StartingReturns, _StockLog.StartingOpenReturnsQuantity_SRET)
        _Command.AddParameter(My.Resources.Parameters.EndingReturns, _StockLog.EndingOpenReturnsQuantity_ERET)
        _Command.AddParameter(My.Resources.Parameters.StartingMarkdown, _StockLog.StartingMarkDown_SMDN)
        _Command.AddParameter(My.Resources.Parameters.EndingMarkdown, _StockLog.EndingMarkDown_EMDN)
        _Command.AddParameter(My.Resources.Parameters.StartingWriteOff, _StockLog.StartingWriteOff_SWTF)
        _Command.AddParameter(My.Resources.Parameters.EndingWriteOff, _StockLog.EndingWriteOff_EWTF)
        _Command.AddParameter(My.Resources.Parameters.StartingPrice, _StockLog.StartingPrice_PRIC)
        _Command.AddParameter(My.Resources.Parameters.EndingPrice, _StockLog.EndingPrice_EPRI)

    End Sub

    Friend Overridable Sub ExecuteSaveCommand()

        If CommandIsNothing() = True Then Exit Sub

        ExecuteNonQuery()

    End Sub

    Friend Overridable Sub ExecuteNonQuery()

        _Command.ExecuteNonQuery()

    End Sub

#End Region

#Region "Method: GetLastStockLogForSkun"

    Friend Overridable Function ExecuteGetStockLog() As DataRow

        ConfigureGetStockLogCommand()
        Return ExecuteGetStockLogCommand()

    End Function

    Friend Overridable Sub ConfigureGetStockLogCommand()

        If ConnectionIsNothing() = True Then Exit Sub
        If SkuNumberIsEmpty() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))

        _Command.StoredProcedureName = My.Resources.Procedure.GetLastStockLogForSku
        _Command.AddParameter(My.Resources.Parameters.ProductCode, _SkuNumber)

    End Sub

    Friend Overridable Function ExecuteGetStockLogCommand() As DataRow

        ExecuteGetStockLogCommand = Nothing

        If CommandIsNothing() = True Then Exit Function

        ExecuteDataTable()

        If DataTableIsNothing() = True Then Exit Function

        If DataTableZeroRecordCount() = True Then Exit Function

        Return _DT.Rows(0)

    End Function

    Friend Overridable Sub ExecuteDataTable()

        _DT = _Command.ExecuteDataTable()

    End Sub

#End Region

#Region "Test Conditions"

    Friend Function ConnectionIsNothing() As Boolean

        If _Connection Is Nothing Then Return True

    End Function

    Friend Function CommandIsNothing() As Boolean

        If _Command Is Nothing Then Return True

    End Function

    Friend Function DataTableIsNothing() As Boolean

        If _DT Is Nothing Then Return True

    End Function

    Friend Function DataTableZeroRecordCount() As Boolean

        If _DT.Rows.Count = 0 Then Return True

    End Function

    Friend Function StockLogIsNothing() As Boolean

        If _StockLog Is Nothing Then Return True

    End Function

    Friend Function SkuNumberIsEmpty() As Boolean

        If _SkuNumber = String.Empty Then Return True

    End Function

#End Region

#End Region

End Class