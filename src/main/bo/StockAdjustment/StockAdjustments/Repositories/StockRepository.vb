﻿Imports Cts.Oasys.Data
Imports Cts.Oasys.Core

Public Class StockRepository

    Inherits Cts.Oasys.Core.Base

    Implements IStockRepository

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub Update(ByVal stock As IStock, ByRef Con As Connection) Implements IStockRepository.Update
        'Using conn As New Connection
        Using com As New Command(CType(Con, IConnection))
            com.StoredProcedureName = My.Resources.Procedure.UpdateStockAndCYHMASValuesForSku
            com.AddParameter(My.Resources.Parameters.ProductCode, stock.SKUN)
            com.AddParameter(My.Resources.Parameters.StockOnHand, stock.OnHand_ONHA)
            com.AddParameter(My.Resources.Parameters.SaleTypeAttribute, stock.SaleTypeAttribute_SALT)
            com.AddParameter(My.Resources.Parameters.DummyItem, stock.DummyItem_ICAT)
            com.AddParameter(My.Resources.Parameters.HODeleted, stock.HODeleted_IDEL)
            com.AddParameter(My.Resources.Parameters.AllowAdjustments, stock.AllowAdjustments_AADJ)
            com.AddParameter(My.Resources.Parameters.WriteOffStock, stock.WriteOffStock_WTFQ)
            com.AddParameter(My.Resources.Parameters.Price, stock.Price_PRIC)
            com.AddParameter(My.Resources.Parameters.Cost, stock.Cost)
            com.AddParameter(My.Resources.Parameters.AdjustmentQty1, stock.AdjustmentQty_MADQ1)
            com.AddParameter(My.Resources.Parameters.AdjustmentValue1, stock.AdjustmentValue_MADV1)
            com.AddParameter(My.Resources.Parameters.MarkDownQty, stock.MarkdownQty_MDNQ)
            com.AddParameter(My.Resources.Parameters.OpenReturnQty, stock.OpenReturnQty_RETQ)
            com.AddParameter(My.Resources.Parameters.CyclicalCount, stock.CyclicalCount_MCCV1)
            com.AddParameter(My.Resources.Parameters.ToadyActivityFlag, stock.TodayActivity_TACT)
            Dim myDate As Date = CDate("01-01-2000")
            If stock.CycleDate_ADAT1 > myDate Then
                com.AddParameter(My.Resources.Parameters.ActivityDate, stock.CycleDate_ADAT1)
            End If
            com.AddParameter(My.Resources.Parameters.ActivityQty, stock.AdjustmentQty_AQ021)
            com.AddParameter(My.Resources.Parameters.ActivityValue, stock.AdjustmentValue_AV021)
            com.ExecuteNonQuery()
        End Using
        'End Using
    End Sub

    Public Function ReadSKU(ByRef Con As Connection, ByVal ProductCode As String) As System.Data.DataRow Implements IStockRepository.ReadSKU
        'Using con As New Connection
        Using com As New Command(CType(Con, IConnection))
            com.StoredProcedureName = My.Resources.Procedure.GetStockAndCYHMASValuesForSku
            com.AddParameter(My.Resources.Parameters.ProductCode, ProductCode)
            Dim dt As DataTable = com.ExecuteDataTable
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)
            End If
        End Using
        'End Using
        Return Nothing
    End Function

    Public Function GetReducedStocksDataTable() As DataTable Implements IStockRepository.GetReducedStocksDataTable
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetReducedStockSkuns
                com.AddParameter(My.Resources.Parameters.SaltCode, "R")
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return dt
                End If
            End Using
        End Using
        Return Nothing
    End Function

    Public Function GetStoreNumber() As String Implements IStockRepository.GetStoreNumber
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetStoreNumber
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return CStr(dt.Rows(0).Item(0))
                End If
            End Using
        End Using
        Return String.Empty
    End Function

    Public Function DoesSkuExist(ByVal ProductCode As String) As Boolean Implements IStockRepository.DoesSkuExist
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.StockGetStock
                com.AddParameter(My.Resources.Parameters.SkuNumber, ProductCode)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return True
                End If
            End Using
        End Using
        Return False

    End Function


End Class
