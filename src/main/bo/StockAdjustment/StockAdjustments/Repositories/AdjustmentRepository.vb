﻿Public Class AdjustmentRepository
    Inherits Base
    Implements IAdjustmentRepository

    Protected _Connection As IConnection
    Protected _Command As ICommand
    Protected _DT As DataTable

    Protected _SkuNumber As String
    Protected _AdjustmentCode As String
    Protected _AdjustmentDate As System.Nullable(Of Date)

#Region "Constructor"

    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region "Interface"

    Public Function ReadAdjustment(ByRef Con As Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustment
        'Using con As New Connection
        Using com As New Command(CType(Con, IConnection))
            com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentForSkuToday
            com.AddParameter(My.Resources.Parameters.ProductCode, Skun)
            If CodeNumber <> "04" Then
                com.AddParameter(My.Resources.Parameters.AdjustDate, AdjDate.ToString("yyyy-MM-dd"))
            End If
            com.AddParameter(My.Resources.Parameters.CodeNumber, CodeNumber)
            Dim dt As DataTable = com.ExecuteDataTable
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)
            End If
        End Using
        'End Using
        Return Nothing
    End Function

    Public Function SaveAdjustment(ByVal Adjustment As IAdjustment, ByRef Con As Connection) As Boolean Implements IAdjustmentRepository.SaveAdjustment

        Try
            'Using conn As New Connection
            Using com As New Command(CType(Con, IConnection))
                'Make decision on wether to create or update stock adjustment using the ExistsInDB flag
                If Adjustment.ExistsInDatabase And Adjustment.Quantity_QUAN = 0 Then
                    com.StoredProcedureName = My.Resources.Procedure.DeleteStockAdjustment
                    com.AddParameter(My.Resources.Parameters.AdjustmentDate, Adjustment.AdjustmentDate_DATE1.ToString("yyyy-MM-dd"))
                    com.AddParameter(My.Resources.Parameters.AdjustmentCode, Adjustment.Code)
                    com.AddParameter(My.Resources.Parameters.Skun, Adjustment.SKUN)
                    com.AddParameter(My.Resources.Parameters.Seqn, Adjustment.SEQN)
                    Return CBool(com.ExecuteNonQuery())
                End If
                If Adjustment.ExistsInDatabase Then 'Update required
                    com.StoredProcedureName = My.Resources.Procedure.UpdateStockAdjustment
                Else
                    com.StoredProcedureName = My.Resources.Procedure.InsertStockAdjustment
                End If
                com.AddParameter(My.Resources.Parameters.AdjustmentDate, Adjustment.AdjustmentDate_DATE1.ToString("yyyy-MM-dd"))
                com.AddParameter(My.Resources.Parameters.AdjustmentCode, Adjustment.Code)
                com.AddParameter(My.Resources.Parameters.Skun, Adjustment.SKUN)
                com.AddParameter(My.Resources.Parameters.Seqn, Adjustment.SEQN)
                com.AddParameter(My.Resources.Parameters.AmendId, Adjustment.AmendId)
                com.AddParameter(My.Resources.Parameters.Initials, Adjustment.EmployeeInitials_INIT)
                com.AddParameter(My.Resources.Parameters.Department, Adjustment.Department)
                com.AddParameter(My.Resources.Parameters.StartingStock, Adjustment.StartingStock_SSTK)
                com.AddParameter(My.Resources.Parameters.AdjustmentQty, Adjustment.Quantity_QUAN)
                com.AddParameter(My.Resources.Parameters.AdjustmentPrice, Adjustment.Price)
                com.AddParameter(My.Resources.Parameters.AdjustmentCost, Adjustment.Cost)
                com.AddParameter(My.Resources.Parameters.IsSentToHO, IIf(Adjustment.IsSentToHO_COMM, 1, 0))
                com.AddParameter(My.Resources.Parameters.AdjustmentType, Adjustment.AdjustType)
                com.AddParameter(My.Resources.Parameters.Comment, Adjustment.Comment_INFO)
                com.AddParameter(My.Resources.Parameters.DRLNumber, Adjustment.DeliveryNumber_DRLN)
                com.AddParameter(My.Resources.Parameters.ReversalCode, Adjustment.ReversalCode_RCOD)
                com.AddParameter(My.Resources.Parameters.MarkDownWriteOff, Adjustment.MarkDownOrWriteOff_MOWT)
                com.AddParameter(My.Resources.Parameters.WriteOffAuthorised, Adjustment.WriteOffAuthority_WAUT)
                'com.AddParameter(My.Resources.Parameters.DateAuthorised, Adjustment.WriteOffAuthorityDate_DAUT)
                com.AddParameter(My.Resources.Parameters.RTI, Adjustment.RTI)
                com.AddParameter(My.Resources.Parameters.PeriodId, Adjustment.PeriodId)
                com.AddParameter(My.Resources.Parameters.TransferSku, Adjustment.TransferSku_TSKU)
                com.AddParameter(My.Resources.Parameters.TransferValue, Adjustment.TransferValue)
                com.AddParameter(My.Resources.Parameters.TransferStart, Adjustment.TransferStart)
                com.AddParameter(My.Resources.Parameters.TransferPrice, Adjustment.TransferPrice)
                com.AddParameter(My.Resources.Parameters.IsReversed, IIf(Adjustment.IsReversed, 1, 0))
                Return CBool(com.ExecuteNonQuery())
            End Using

        Catch ex As Exception
            Throw (ex)
            Return False
        End Try

    End Function

    Public Function ReadAdjustmentDate() As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustmentDate
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentDate
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0)
                End If
            End Using
        End Using
        Return Nothing

    End Function

    Public Function GetEmployeeInitials(ByVal Id As Integer) As String Implements IAdjustmentRepository.GetEmployeeInitials
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetEmployeeInitials
                com.AddParameter(My.Resources.Parameters.Id, Id)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return CStr(dt.Rows(0).Item(0))
                End If
            End Using
        End Using
        Return String.Empty
    End Function

    Public Function ReadAdjustmentExcludingMarkdownSales(ByRef Con As Cts.Oasys.Data.Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustmentExcludingMarkdownSales
        Using com As New Command(CType(Con, IConnection))
            com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentForSkuToday
            com.AddParameter(My.Resources.Parameters.ProductCode, Skun)
            If CodeNumber <> "04" Then
                com.AddParameter(My.Resources.Parameters.AdjustDate, AdjDate.ToString("yyyy-MM-dd"))
            End If
            com.AddParameter(My.Resources.Parameters.CodeNumber, CodeNumber)
            com.AddParameter(My.Resources.Parameters.ExcludeTillMarkDownSales, 1)
            Dim dt As DataTable = com.ExecuteDataTable
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)
            End If
        End Using
        Return Nothing
    End Function

    Public Overridable Function GetNextSequenceNumberForSkunCodeDate(ByVal Skun As String, ByVal Code As String, ByVal AdjDate As Date) As String Implements IAdjustmentRepository.GetNextSequenceNumberForSkunCodeDate

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentForSkuToday
                com.AddParameter(My.Resources.Parameters.ProductCode, Skun)
                If Code <> "04" Then
                    com.AddParameter(My.Resources.Parameters.AdjustDate, AdjDate.ToString("yyyy-MM-dd"))
                End If
                com.AddParameter(My.Resources.Parameters.CodeNumber, Code)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Dim SequnceNumber As Integer = CInt(dt.Rows(0).Item("SEQN")) + 1
                    Return SequnceNumber.ToString.PadLeft(2, "0"c)
                End If
            End Using
        End Using
        Return String.Empty

    End Function

    Public Overridable Function GetNextSequenceNumberForSkunCodeDate(ByRef Con As Connection, _
                                                                     ByVal SkuNumber As String, _
                                                                     ByVal AdjustmentCode As String, _
                                                                     ByVal AdjustmentDate As Date) As String Implements IAdjustmentRepository.GetNextSequenceNumberForSkunCodeDate

        Reset()

        SetConnection(Con)

        SetSkuNumber(SkuNumber)

        SetAdjustmentCode(AdjustmentCode)

        SetAdjustmentDate(AdjustmentDate)

        Return ExecuteGetNextSequenceNo()

    End Function

#End Region

#Region "Private Procedures & Functions"

#Region "Next Sequence No"

    Friend Overridable Sub Reset()

        _Connection = Nothing
        _Command = Nothing
        _DT = Nothing
        _SkuNumber = Nothing
        _AdjustmentCode = Nothing
        _AdjustmentDate = Nothing

    End Sub

    Friend Overridable Sub SetConnection(ByRef Con As Connection)

        _Connection = Con

    End Sub

    Friend Overridable Sub SetSkuNumber(ByVal SkuNumber As String)

        _SkuNumber = SkuNumber

    End Sub

    Friend Overridable Sub SetAdjustmentCode(ByVal AdjustmentCode As String)

        _AdjustmentCode = AdjustmentCode

    End Sub

    Friend Overridable Sub SetAdjustmentDate(ByVal AdjustmentDate As Date)

        If MinimumDateValue(AdjustmentDate) = False Then _AdjustmentDate = AdjustmentDate

    End Sub

    Friend Overridable Function ExecuteGetNextSequenceNo() As String

        ConfigureGetNextSequenceNoCommand()
        Return ExecuteGetNextSequenceNoCommand()

    End Function

    Friend Overridable Sub ConfigureGetNextSequenceNoCommand()

        If ConnectionIsNothing() = True Then Exit Sub
        If SkuNumberIsEmpty() = True Then Exit Sub
        If AdjustmentCodeIsEmpty() = True Then Exit Sub
        If AdjustmentDateIsEmpty() = True Then Exit Sub

        _Command = CommandFactory.FactoryGet(CType(_Connection, IConnection))

        _Command.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentForSkuToday

        _Command.AddParameter(My.Resources.Parameters.ProductCode, _SkuNumber)

        If CodeFour() = False Then _Command.AddParameter(My.Resources.Parameters.AdjustDate, _AdjustmentDate.Value.ToString("yyyy-MM-dd"))

        _Command.AddParameter(My.Resources.Parameters.CodeNumber, _AdjustmentCode)

    End Sub

    Friend Overridable Function ExecuteGetNextSequenceNoCommand() As String

        ExecuteGetNextSequenceNoCommand = Nothing

        If CommandIsNothing() = True Then Exit Function

        ExecuteDataTable()

        If DataTableIsNothing() = True Then Exit Function

        If DataTableZeroRecordCount() = True Then Exit Function

        Return IntegerConvertToStringWithPadding(NextSequenceNo(CInt(_DT.Rows(0).Item("SEQN"))))

    End Function

    Friend Overridable Sub ExecuteDataTable()

        _DT = _Command.ExecuteDataTable()

    End Sub

#Region "Test Conditions"

    Friend Function ConnectionIsNothing() As Boolean

        If _Connection Is Nothing Then Return True

    End Function

    Friend Function CommandIsNothing() As Boolean

        If _Command Is Nothing Then Return True

    End Function

    Friend Function DataTableIsNothing() As Boolean

        If _DT Is Nothing Then Return True

    End Function

    Friend Function DataTableZeroRecordCount() As Boolean

        If _DT.Rows.Count = 0 Then Return True

    End Function

    Friend Function SkuNumberIsEmpty() As Boolean

        If _SkuNumber = String.Empty Then Return True

    End Function

    Friend Function AdjustmentCodeIsEmpty() As Boolean

        If _AdjustmentCode = String.Empty Then Return True

    End Function

    Friend Function AdjustmentDateIsEmpty() As Boolean

        If _AdjustmentDate.HasValue = False Then Return True

    End Function

    Friend Function MinimumDateValue(ByVal DateValue As Date) As Boolean

        If DateValue = Date.MinValue Then Return True

    End Function

    Friend Function CodeFour() As Boolean

        If _AdjustmentCode = "04" Then Return True

    End Function

    Friend Function NextSequenceNo(ByVal Value As Integer) As Integer

        Return Value + 1

    End Function

    Friend Function IntegerConvertToStringWithPadding(ByVal Value As Integer) As String

        Return Value.ToString.PadLeft(2, "0"c)

    End Function

#End Region

#End Region

#End Region

End Class