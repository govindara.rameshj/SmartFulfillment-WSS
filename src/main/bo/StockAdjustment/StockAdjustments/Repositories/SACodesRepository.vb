﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Data

Namespace SACode

    Public Class SACodesRepository

        Inherits Cts.Oasys.Core.Base

        Implements ISACodesRepository


        Public Function GetSACode(ByVal SecurityLevel As Integer, ByVal CodeNumber As String) As System.Data.DataRow Implements ISACodesRepository.GetSACode
            Using con As New Connection
                Using com As New Command(con)
                    com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentCodes
                    com.AddParameter(My.Resources.Parameters.SecurityLevel, SecurityLevel.ToString)
                    com.AddParameter(My.Resources.Parameters.CodeNumber, CodeNumber)
                    Dim dt As DataTable = com.ExecuteDataTable
                    If dt.Rows.Count > 0 Then
                        Return dt.Rows(0)
                    End If
                End Using
            End Using
            Return Nothing

        End Function

        Public Function GetSACodes(ByVal SecurityLevel As Integer) As System.Data.DataTable Implements ISACodesRepository.GetSACodes
            Using con As New Connection
                Using com As New Command(con)
                    com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentCodes
                    com.AddParameter(My.Resources.Parameters.SecurityLevel, SecurityLevel.ToString)
                    Return com.ExecuteDataTable
                End Using
            End Using
            Return Nothing
        End Function

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
