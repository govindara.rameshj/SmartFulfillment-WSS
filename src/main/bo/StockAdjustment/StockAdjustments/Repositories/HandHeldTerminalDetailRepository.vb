﻿Option Strict On
Option Explicit On

Public Class HandHeldTerminalDetailRepository
    Inherits Cts.Oasys.Core.Base

    Implements IHandHeldTerminalDetailRepository

    Public Function Read(ByVal SelectedDate As Date) As System.Data.DataTable Implements IHandHeldTerminalDetailRepository.Read
        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_HandHeldTerminalDetail"
                com.AddParameter("@SelectedDate", SelectedDate, SqlDbType.Date)
                DT = com.ExecuteDataTable

                If DT.Rows.Count > 0 Then Return DT
            End Using
        End Using

        Return Nothing
    End Function

    Public Sub Update(ByVal SelectedDate As Date, ByVal ProductCode As String, ByVal AdjustmentApplied As Boolean) Implements IHandHeldTerminalDetailRepository.Update

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_HandHeldTerminalDetailUpdate"

                com.AddParameter("@SelectedDate", SelectedDate, SqlDbType.Date)
                com.AddParameter("@ProductCode", ProductCode, SqlDbType.Char, 6)
                com.AddParameter("@AdjustmentApplied", AdjustmentApplied, SqlDbType.Bit)

                com.ExecuteNonQuery()
            End Using
        End Using

    End Sub

End Class