﻿Public Class AdjustmentsRepository

    Implements IAdjustmentsRepository

    Public Function ReadAdjustments(ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As System.Data.DataTable Implements IAdjustmentsRepository.ReadAdjustments
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentForSkuToday
                com.AddParameter(My.Resources.Parameters.ProductCode, Skun)
                If CodeNumber <> "04" Then
                    com.AddParameter(My.Resources.Parameters.AdjustDate, AdjDate.ToString("yyyy-MM-dd"))
                End If
                com.AddParameter(My.Resources.Parameters.CodeNumber, CodeNumber)
                Return com.ExecuteDataTable
            End Using
        End Using
        Return Nothing
    End Function

    Public Function ReadAdjustmentsExcludingMarkdownSku(ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As System.Data.DataTable Implements IAdjustmentsRepository.ReadAdjustmentsExcludingMarkdownSku
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentForSkuToday
                com.AddParameter(My.Resources.Parameters.ProductCode, Skun)
                If CodeNumber <> "04" Then
                    com.AddParameter(My.Resources.Parameters.AdjustDate, AdjDate.ToString("yyyy-MM-dd"))
                End If
                com.AddParameter(My.Resources.Parameters.CodeNumber, CodeNumber)
                com.AddParameter(My.Resources.Parameters.ExcludeTillMarkDownSales, 1)
                Return com.ExecuteDataTable
            End Using
        End Using
        Return Nothing

    End Function
End Class
