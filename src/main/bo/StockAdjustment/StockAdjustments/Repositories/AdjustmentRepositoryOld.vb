﻿Public Class AdjustmentRepositoryOld
    Inherits Base

    Implements IAdjustmentRepository

    Public Sub New()
        MyBase.New()
    End Sub

    Public Function ReadAdjustment(ByRef Con As Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustment
        'Using con As New Connection
        Using com As New Command(CType(Con, IConnection))
            com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentForSkuToday
            com.AddParameter(My.Resources.Parameters.ProductCode, Skun)
            If CodeNumber <> "04" Then
                com.AddParameter(My.Resources.Parameters.AdjustDate, AdjDate.ToString("yyyy-MM-dd"))
            End If
            com.AddParameter(My.Resources.Parameters.CodeNumber, CodeNumber)
            Dim dt As DataTable = com.ExecuteDataTable
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)
            End If
        End Using
        'End Using
        Return Nothing
    End Function

    Public Function SaveAdjustment(ByVal Adjustment As IAdjustment, ByRef Con As Connection) As Boolean Implements IAdjustmentRepository.SaveAdjustment
        Try

            'Using conn As New Connection
            Using com As New Command(CType(Con, IConnection))
                'Make decision on wether to create or update stock adjustment using the ExistsInDB flag
                If Adjustment.ExistsInDatabase And Adjustment.Quantity_QUAN = 0 Then
                    com.StoredProcedureName = My.Resources.Procedure.DeleteStockAdjustment
                    com.AddParameter(My.Resources.Parameters.AdjustmentDate, Adjustment.AdjustmentDate_DATE1.ToString("yyyy-MM-dd"))
                    com.AddParameter(My.Resources.Parameters.AdjustmentCode, Adjustment.Code)
                    com.AddParameter(My.Resources.Parameters.Skun, Adjustment.SKUN)
                    com.AddParameter(My.Resources.Parameters.Seqn, Adjustment.SEQN)
                    Return CBool(com.ExecuteNonQuery())
                End If
                If Adjustment.ExistsInDatabase Then 'Update required
                    com.StoredProcedureName = My.Resources.Procedure.UpdateStockAdjustment
                Else
                    com.StoredProcedureName = My.Resources.Procedure.InsertStockAdjustment
                End If
                com.AddParameter(My.Resources.Parameters.AdjustmentDate, Adjustment.AdjustmentDate_DATE1.ToString("yyyy-MM-dd"))
                com.AddParameter(My.Resources.Parameters.AdjustmentCode, Adjustment.Code)
                com.AddParameter(My.Resources.Parameters.Skun, Adjustment.SKUN)
                com.AddParameter(My.Resources.Parameters.Seqn, Adjustment.SEQN)
                com.AddParameter(My.Resources.Parameters.AmendId, Adjustment.AmendId)
                com.AddParameter(My.Resources.Parameters.Initials, Adjustment.EmployeeInitials_INIT)
                com.AddParameter(My.Resources.Parameters.Department, Adjustment.Department)
                com.AddParameter(My.Resources.Parameters.StartingStock, Adjustment.StartingStock_SSTK)
                com.AddParameter(My.Resources.Parameters.AdjustmentQty, Adjustment.Quantity_QUAN)
                com.AddParameter(My.Resources.Parameters.AdjustmentPrice, Adjustment.Price)
                com.AddParameter(My.Resources.Parameters.AdjustmentCost, Adjustment.Cost)
                com.AddParameter(My.Resources.Parameters.IsSentToHO, IIf(Adjustment.IsSentToHO_COMM, 1, 0))
                com.AddParameter(My.Resources.Parameters.AdjustmentType, Adjustment.AdjustType)
                com.AddParameter(My.Resources.Parameters.Comment, Adjustment.Comment_INFO)
                com.AddParameter(My.Resources.Parameters.DRLNumber, Adjustment.DeliveryNumber_DRLN)
                com.AddParameter(My.Resources.Parameters.ReversalCode, Adjustment.ReversalCode_RCOD)
                com.AddParameter(My.Resources.Parameters.MarkDownWriteOff, Adjustment.MarkDownOrWriteOff_MOWT)
                com.AddParameter(My.Resources.Parameters.WriteOffAuthorised, Adjustment.WriteOffAuthority_WAUT)
                'com.AddParameter(My.Resources.Parameters.DateAuthorised, Adjustment.WriteOffAuthorityDate_DAUT)
                com.AddParameter(My.Resources.Parameters.RTI, Adjustment.RTI)
                com.AddParameter(My.Resources.Parameters.PeriodId, Adjustment.PeriodId)
                com.AddParameter(My.Resources.Parameters.TransferSku, Adjustment.TransferSku_TSKU)
                com.AddParameter(My.Resources.Parameters.TransferValue, Adjustment.TransferValue)
                com.AddParameter(My.Resources.Parameters.TransferStart, Adjustment.TransferStart)
                com.AddParameter(My.Resources.Parameters.TransferPrice, Adjustment.TransferPrice)
                com.AddParameter(My.Resources.Parameters.IsReversed, IIf(Adjustment.IsReversed, 1, 0))
                Return CBool(com.ExecuteNonQuery())
            End Using
            'End Using

        Catch ex As Exception
            Throw (ex)
            Return False
        End Try

    End Function

    Public Function ReadAdjustmentDate() As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustmentDate
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetStockAdjustmentDate
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0)
                End If
            End Using
        End Using
        Return Nothing

    End Function

    Public Function GetEmployeeInitials(ByVal Id As Integer) As String Implements IAdjustmentRepository.GetEmployeeInitials
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetEmployeeInitials
                com.AddParameter(My.Resources.Parameters.Id, Id)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return CStr(dt.Rows(0).Item(0))
                End If
            End Using
        End Using
        Return String.Empty
    End Function

    Public Function ReadAdjustmentExcludingMarkdownSales(ByRef Con As Cts.Oasys.Data.Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As System.Data.DataRow Implements IAdjustmentRepository.ReadAdjustmentExcludingMarkdownSales
        Return ReadAdjustment(Con, Skun, AdjDate, CodeNumber)
    End Function

    Public Function GetNextSequenceNumberForSkunCodeDate(ByVal Skun As String, ByVal Code As String, ByVal AdjDate As Date) As String Implements IAdjustmentRepository.GetNextSequenceNumberForSkunCodeDate
        Return String.Empty
    End Function

    Public Function GetNextSequenceNumberForSkunCodeDate(ByRef Con As Cts.Oasys.Data.Connection, ByVal SkuNumber As String, ByVal AdjustmentCode As String, ByVal AdjDate As Date) As String Implements IAdjustmentRepository.GetNextSequenceNumberForSkunCodeDate

        Return String.Empty

    End Function

End Class