﻿Imports Cts.Oasys.Data
Imports Cts.Oasys.Core

Public Class DRLValidatorRepository

    Implements IDRLValidatorRepository



    Private _IsDRLTypeZero As Boolean
    Private _IsPURHDRBBCC_A As Boolean
    Private _IsPURHDRBBCC_W As Boolean
    Private _IsSUPDETBBCC_C As Boolean
    Private _IsSUPDETBBCC_W As Boolean
    Private _IsReversible As Boolean
    Private _IsValidDRL As Boolean

    'Public Function IsDRLTypeZero(ByVal DRLNumber As Integer) As Boolean Implements IDRLValidatorRepository.IsDRLTypeZero
    '    Return _IsDRLTypeZero
    'End Function

    'Public Function IsPURHDRBBCC_A(ByVal DRLNumber As Integer) As Boolean Implements IDRLValidatorRepository.IsPURHDRBBC_A
    '    Return _IsPURHDRBBCC_A
    'End Function

    'Public Function IsPURHDRBBCC_W(ByVal DRLNumber As Integer) As Boolean Implements IDRLValidatorRepository.IsPURHDRBBC_W
    '    Return _IsPURHDRBBCC_W
    'End Function

    'Public Function IsSUPDETBBCC_C(ByVal DRLNumber As Integer) As Boolean Implements IDRLValidatorRepository.IsSUPDETBBC_C
    '    Return _IsSUPDETBBCC_C
    'End Function

    'Public Function IsSUPDETBBCC_W(ByVal DRLNumber As Integer) As Boolean Implements IDRLValidatorRepository.IsSUPDETBBC_W
    '    Return _IsSUPDETBBCC_W
    'End Function

    'Public Function IsValidDRL(ByVal DRLNumber As Integer) As Boolean Implements IDRLValidatorRepository.IsValidDRL
    '    Return _IsValidDRL
    'End Function

    Public Function ReadDRL(ByVal DRLNumber As String) As System.Data.DataRow Implements IDRLValidatorRepository.ReadDRL
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetDRLNumber
                com.AddParameter(My.Resources.Parameters.DRLNumber, DRLNumber)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    'With dt.Rows.Item(0)
                    '    _IsDRLTypeZero = .Item("DRLType").ToString = "0"
                    '    _IsPURHDRBBCC_A = .Item("purBBCC").ToString = "A"
                    '    _IsPURHDRBBCC_W = .Item("purBBCC").ToString = "W"
                    '    _IsSUPDETBBCC_C = .Item("supBBCC").ToString = "C"
                    '    _IsSUPDETBBCC_W = .Item("supBBCC").ToString = "W"
                    'End With
                    Return dt.Rows(0)
                End If
            End Using
        End Using
        Return Nothing
    End Function

    Public Function IsSkuInDRL(ByVal DRLNumber As String, ByVal SkuNumber As String) As Boolean Implements IDRLValidatorRepository.IsSkuInDRL
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.IsSkuInDRL
                com.AddParameter(My.Resources.Parameters.DRLNumber, DRLNumber)
                com.AddParameter(My.Resources.Parameters.SkuNumber, SkuNumber)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return True
                End If
            End Using
        End Using
        Return False

    End Function

    Public Function GetReceiptQty(ByVal DRLNumber As String, ByVal SkuNumber As String) As Integer Implements IDRLValidatorRepository.GetReceiptQty
        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = My.Resources.Procedure.GetReceiptQty
                com.AddParameter(My.Resources.Parameters.DRLNumber, DRLNumber)
                com.AddParameter(My.Resources.Parameters.SkuNumber, SkuNumber)
                Dim dt As DataTable = com.ExecuteDataTable
                If dt.Rows.Count > 0 Then
                    Return CInt(dt.Rows(0).Item("ReceiptQty"))
                End If
            End Using
        End Using
        Return Nothing
    End Function


End Class
