﻿Option Strict On
Option Explicit On

Imports Cts.Oasys.Core
Imports Cts.Oasys.Data

Public Class HandHeldTerminalHeaderRepository
    Inherits Cts.Oasys.Core.Base

    Implements IHandHeldTerminalHeaderRepository

    Public Function ReadLatestHHT() As System.Data.DataRow Implements IHandHeldTerminalHeaderRepository.ReadLatestHHT
        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_HandHeldTerminalHeaderLatest"
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT.Rows(0)
            End Using
        End Using

        Return Nothing
    End Function

    Public Sub Update(ByVal SelectedDate As Date, ByVal ItemsToCount As Integer?, ByVal ItemsCounted As Integer?, ByVal AdjustmentApplied As Boolean?, _
                      ByVal AuthorisationID As String, ByVal IsLocked As Boolean?) Implements IHandHeldTerminalHeaderRepository.Update

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_HandHeldTerminalHeaderUpdate"

                com.AddParameter("@SelectedDate", SelectedDate, SqlDbType.Date)

                If ItemsToCount.HasValue = True Then com.AddParameter("@ItemsToCount", ItemsToCount.Value, SqlDbType.Int)
                If ItemsCounted.HasValue = True Then com.AddParameter("@ItemsCounted", ItemsCounted.Value, SqlDbType.Int)
                If AdjustmentApplied.HasValue = True Then com.AddParameter("@AdjustmentApplied", AdjustmentApplied.Value, SqlDbType.Bit)

                If AuthorisationID.Length > 0 Then com.AddParameter("@AuthorisationID", AuthorisationID, SqlDbType.Char, 3)

                If IsLocked.HasValue = True Then com.AddParameter("@IsLocked", IsLocked.Value, SqlDbType.Bit)

                com.ExecuteNonQuery()
            End Using
        End Using

    End Sub

    Public Function ReadHHT(ByVal AdjDate As Date) As System.Data.DataRow Implements IHandHeldTerminalHeaderRepository.ReadHHT
        Dim DT As DataTable

        Using con As New Connection
            Using com As New Command(con)
                com.StoredProcedureName = "usp_HandHeldTerminalHeaderByDate"
                com.AddParameter("@SelectedDate", AdjDate, SqlDbType.Date)
                DT = com.ExecuteDataTable
                If DT.Rows.Count > 0 Then Return DT.Rows(0)
            End Using
        End Using

        Return Nothing

    End Function
End Class