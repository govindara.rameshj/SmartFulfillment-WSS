﻿Option Strict On
Option Explicit On

Public Interface IHandHeldTerminalDetailRepository

    Function Read(ByVal SelectedDate As Date) As DataTable
    Sub Update(ByVal SelectedDate As Date, ByVal ProductCode As String, ByVal AdjustmentApplied As Boolean)

End Interface