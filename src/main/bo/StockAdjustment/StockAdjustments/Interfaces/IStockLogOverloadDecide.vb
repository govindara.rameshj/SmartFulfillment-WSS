﻿Option Strict On

Public Interface IStockLogOverloadDecide

    Sub ReadStockLog(ByRef Con As Connection, ByVal SkuNumber As String, ByRef StockLog As IStockLog)

End Interface