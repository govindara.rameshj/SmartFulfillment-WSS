﻿Option Strict On

Public Interface IStockLog

    Property StockLogId_TKEY() As Integer
    Property DayNumber_DAYN() As Decimal
    Property StockLogType_TYPE() As String
    Property StockLogDate_DATE1() As Date
    Property StockLogTime_TIME() As String
    Property Keys() As String
    Property EmployeeId_EEID() As String
    Property IsSentToHO_ICOM() As Boolean
    Property StartingStock_SSTK() As Decimal
    Property StartingOpenReturnsQuantity_SRET() As Decimal
    Property StartingPrice_PRIC() As Decimal
    Property StartingMarkDown_SMDN() As Decimal
    Property StartingWriteOff_SWTF() As Decimal
    Property EndingStock_ESTK() As Decimal
    Property EndingOpenReturnsQuantity_ERET() As Decimal
    Property EndingPrice_EPRI() As Decimal
    Property EndingMarkDown_EMDN() As Decimal
    Property EndingWriteOff_EWTF() As Decimal
    Property RTI() As String
    Property SKUN() As String

    Function SaveStockLog(ByRef Con As Connection, ByVal AdjustmentQty As Integer, ByVal SACode As ISaCode, ByVal Stock As IStock, ByVal StockAdjust As IAdjustment, ByVal EmployeeId As Integer, Optional ByVal TransferSku As String = "") As Boolean

    Function ReadStockLog(ByVal ProductCode As String) As Boolean
    Function ReadStockLog(ByRef Con As Connection, ByVal SkuNumber As String) As Boolean

    Function GetEmployeeCode(ByVal EmployeeId As Integer) As String

End Interface