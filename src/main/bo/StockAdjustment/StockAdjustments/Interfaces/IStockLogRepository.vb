﻿Option Strict On

Public Interface IStockLogRepository

    Function GetLastStockLogForSkun(ByVal PartCode As String) As DataRow
    Function GetLastStockLogForSkun(ByRef Con As Connection, ByVal SkuNumber As String) As DataRow

    Function Save(ByVal StockLog As IStockLog, ByRef Con As Connection) As Boolean

    Function GetEmployeeCode(ByVal EmployeeId As Integer) As String

End Interface