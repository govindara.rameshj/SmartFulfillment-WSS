﻿Option Strict On

Public Interface IAdjustmentRepositoryOverloadDecide

    Function GetNextSequenceNumberForSkunCodeDate(ByRef AdjustmentRepository As IAdjustmentRepository, _
                                                  ByRef Con As Connection, _
                                                  ByVal SkuNumber As String, _
                                                  ByVal AdjustmentCode As String, _
                                                  ByVal AdjustmentDate As Date) As String

End Interface
