﻿''' <summary>
''' Style of the SKU description to show.
''' </summary>
Public Enum SkuDescriptionStyle

    ''' <summary>
    ''' Description for the correct SKU number
    ''' </summary>
    SkuNumberIsCorrect = 1

    ''' <summary>
    ''' Description for the incorrect SKU number
    ''' </summary>
    SkuNumberIsIncorrect = 2

    ''' <summary>
    ''' Description for the case, then the input of the SKU number is not completed
    ''' </summary>
    SkuNumberIsNotEntered = 3
End Enum
