﻿Public Interface IDRLValidator

    Enum DRLTypes
        DRLReceipt
        DRLIBTIn
        DRLIBTOut
        DRLReturn
    End Enum

    Property DRLType() As String
    Property DRLNumber() As String
    Property purBBCC() As String
    Property supBBCC() As String
    Property IsSentToHO() As Boolean
    Property IsReversible() As Boolean

    Function ReadDRL(ByVal DRLNumber As String) As Boolean
    Function IsSKuInDRL(ByVal DRLNumber As Integer, ByVal SkuNumber As String) As Boolean
    Function GetReceiptQty(ByVal DRLNumber As Integer, ByVal SkuNumber As String) As Integer

End Interface