﻿Option Strict On
Option Explicit On

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Author   : Dhanesh Ramachandran
' Date     : 22/08/2011
' Referral : RF0865
' Notes    : Added for Stock Movements Detail Report to display correct adjustment code
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Interface IStockLogWrite

    Function StockLogWrite() As Boolean


End Interface
