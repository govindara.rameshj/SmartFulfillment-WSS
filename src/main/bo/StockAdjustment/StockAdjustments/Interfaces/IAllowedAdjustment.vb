﻿Public Interface IAllowedAdjustment
    Function IsAdjustmentAllowed(ByVal SACode As ISaCode, ByVal m_Stock As IStock, ByVal m_StockAdjustment As IAdjustment, ByVal Con As Connection, ByVal SkuNumber As String, ByVal AdjDate As Date) As Boolean
End Interface
