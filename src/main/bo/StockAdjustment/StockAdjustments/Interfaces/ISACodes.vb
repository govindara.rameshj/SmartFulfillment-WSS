﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.Reflection
Imports Cts.Oasys.Data
Imports Cts.Oasys.Core

Public Interface ISACodes
    Sub GetSACodes(ByVal SecurityLevel As Integer)
    Sub GetSACode(ByVal SecurityLevel As Integer, ByVal CodeNumber As String)
End Interface
