﻿Public Interface IDRLValidatorRepository
    Function ReadDRL(ByVal DRLNumber As String) As DataRow
    Function IsSkuInDRL(ByVal DRLNumber As String, ByVal SkuNumber As String) As Boolean
    Function GetReceiptQty(ByVal DRLNumber As String, ByVal SkuNumber As String) As Integer
End Interface
