﻿Option Strict On

Public Interface IAdjustmentRepository

    Function SaveAdjustment(ByVal Adjustment As IAdjustment, ByRef Con As Connection) As Boolean
    Function ReadAdjustment(ByRef Con As Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As DataRow
    Function ReadAdjustmentDate() As DataRow
    Function GetEmployeeInitials(ByVal Id As Integer) As String
    Function ReadAdjustmentExcludingMarkdownSales(ByRef Con As Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As DataRow

    Function GetNextSequenceNumberForSkunCodeDate(ByVal Skun As String, ByVal Code As String, ByVal AdjDate As Date) As String
    Function GetNextSequenceNumberForSkunCodeDate(ByRef Con As Connection, ByVal SkuNumber As String, ByVal AdjustmentCode As String, ByVal AdjustmentDate As Date) As String

End Interface