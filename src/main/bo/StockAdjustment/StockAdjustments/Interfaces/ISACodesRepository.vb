﻿Public Interface ISACodesRepository
    Function GetSACode(ByVal SecurityLevel As Integer, ByVal CodeNumber As String) As DataRow
    Function GetSACodes(ByVal SecurityLevel As Integer) As DataTable
End Interface
