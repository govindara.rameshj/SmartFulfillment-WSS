﻿Public Interface IDRLValidastorRepository
    Function IsValidDRL(ByVal DRLNumber As Integer) As Boolean
    Function IsDRLTypeZero(ByVal DRLNumber As Integer) As Boolean
    Function IsPURHDRBBC_A(ByVal DRLNumber As Integer) As Boolean
    Function IsPURHDRBBC_W(ByVal DRLNumber As Integer) As Boolean
    Function IsSUPDETBBC_C(ByVal DRLNumber As Integer) As Boolean
    Function IsSUPDETBBC_W(ByVal DRLNumber As Integer) As Boolean
End Interface
