﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Data

Public Interface IStock
    Property SKUN() As String
    Property OnHand_ONHA() As Integer
    Property SaleTypeAttribute_SALT() As String
    Property DummyItem_ICAT() As Boolean
    Property HODeleted_IDEL() As Boolean
    Property AllowAdjustments_AADJ() As String
    Property WriteOffStock_WTFQ() As Integer
    Property Price_PRIC() As Decimal
    Property Cost() As Decimal
    Property AdjustmentQty_MADQ1() As Integer
    Property AdjustmentValue_MADV1() As Decimal
    Property MarkdownQty_MDNQ() As Integer
    Property CyclicalCount_MCCV1() As Decimal
    Property TodayActivity_TACT() As Boolean
    Property OpenReturnQty_RETQ() As Integer
    Property CycleDate_ADAT1() As Date                  'This is from CYHMAS
    Property AdjustmentQty_AQ021() As Decimal           'This is from CYHMAS
    Property AdjustmentValue_AV021() As Decimal         'This is from CYHMAS
    Property Department() As String
    Sub Update(ByRef Con As Connection)
    Function ReadSKU(ByRef Con As Connection, ByVal ProductCode As String) As Boolean
    Function GetReducedStocksDataTable() As DataTable
    Function GetStoreNumber() As String
    Function DoesSkuExist(ByVal ProductCode As String) As Boolean
End Interface
