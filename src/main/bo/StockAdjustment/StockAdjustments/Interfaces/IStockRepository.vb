﻿Imports Cts.Oasys.Core
Imports Cts.Oasys.Data

Public Interface IStockRepository
    Sub Update(ByVal stock As IStock, ByRef Con As Connection)
    Function ReadSKU(ByRef Con As Connection, ByVal ProductCode As String) As DataRow
    Function GetReducedStocksDataTable() As DataTable
    Function GetStoreNumber() As String
    Function DoesSkuExist(ByVal ProductCode As String) As Boolean
End Interface
