﻿Imports Cts.Oasys.Data
Imports Cts.Oasys.Core

Public Interface ISaCode
    Enum Codes
        NormalAdjustment = 2
        Pallets = 3
        DRLCollection = 4
        StoreUse = 6
        StoreUse1 = 7
        ParkedTransactionAdj = 8
        IQNotAgreed = 10
        AdgreedIQ = 20
        Delete = 51
        Damages = 53
        CustomerComplaint = 54
        DamagedUnload = 55
        NoReturnsPolicy = 58
        HDCMarkDowns = 59
        CAWTestReversal = 77
        CAWTest = 78
    End Enum

    Enum Sign
        Calculate = 0
        Replace
        Add
        Subtract
        AddOrSubtract
    End Enum

    Enum Type
        Normal = 0
        Transfer
    End Enum

    Property CodeNumber() As String
    Property Description() As String
    Property CodeType() As String
    Property CodeSign() As String
    Property MarkdownWriteoff() As String
    Property SecurityLevel() As String
    Property IsMarkdown() As Boolean
    Property IsWriteOff() As Boolean
    Property IsAuthRequired() As Boolean
    Property IsPassRequired() As Boolean
    Property IsReserved() As Boolean
    Property IsCommentable() As Boolean
    Property IsStockLoss() As Boolean
    Property IsKnownTheft() As Boolean
    Property IsIssueQuery() As Boolean
    Property AllowCodes() As String
    Property DisplayType() As String
    Property DisplaySign() As String
    Function Load(ByVal SecurityLevel As Integer, ByVal CodeNumber As String) As Boolean
End Interface
