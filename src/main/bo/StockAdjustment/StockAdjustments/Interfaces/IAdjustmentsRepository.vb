﻿Public Interface IAdjustmentsRepository
    Function ReadAdjustments(ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As DataTable
    Function ReadAdjustmentsExcludingMarkdownSku(ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As DataTable
End Interface
