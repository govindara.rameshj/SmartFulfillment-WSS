﻿''' <summary>
''' Interface of some form, wich contains controls for entering SKU number and for showing its description on the fly.
''' </summary>
Public Interface ISkuNumberAndDescriptionContainer

    ''' <summary>
    ''' Get entered SKU number text.
    ''' </summary>
    Function GetSkuNumberText() As String

    ''' <summary>
    ''' Set the description text and style.
    ''' </summary>
    ''' <param name="text">Text to set</param>
    ''' <param name="skuDescriptionStatus">Mode of the description text</param>
    Sub SetSkuDescriptionText(ByVal text As String, ByVal skuDescriptionStyle As SkuDescriptionStyle)

End Interface
