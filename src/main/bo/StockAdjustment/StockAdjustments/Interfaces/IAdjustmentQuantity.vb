﻿Public Interface IAdjustmentQuantity
    Sub Validate(ByVal Type As String, ByVal Value As Integer)
    Property Value() As Integer
End Interface
