﻿Option Strict On

Public Interface IAdjustments
    Function ReadAdjustments(ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As Boolean
    Function HighestSequenceNo() As Integer
    Function MaintainableDailyReceiverListingAdjustment(ByVal DrlNumber As String) As Boolean
    Function skuCount() As Integer
End Interface