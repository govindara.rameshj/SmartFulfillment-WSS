﻿Option Strict On

Public Interface IStockAdjustmentManager

    Property Stock() As IStock
    Property StockAdjustment() As IAdjustment
    Property DRL() As IDRLValidator

    Function NormalAdjustment(ByVal SACode As ISaCode, ByRef Adjustment As IAdjustment, ByVal SkuNumber As String, ByVal AdjDate As Date, ByVal DRLNumber As String, ByRef AdjQty As Integer, ByVal Comment As String, ByVal EmployeeID As Integer) As Boolean
    Function NormalAdjustment(ByRef Con As Connection, ByVal SACode As ISaCode, ByRef Adjustment As IAdjustment, ByVal SkuNumber As String, ByVal AdjDate As Date, ByVal DRLNumber As String, ByRef AdjQty As Integer, ByVal Comment As String, ByVal EmployeeID As Integer) As Boolean
    Function NormalAdjustment(ByVal AdjustmentCode As ISaCode, _
                              ByVal NewStockAdjustment As IAdjustment, _
                              ByVal ExistingStockAdjustment As IAdjustment, _
                              ByVal SkuNumber As String, _
                              ByVal AdjustmentDate As Date, _
                              ByVal DRLNumber As String, _
                              ByVal AdjustmentQuantity As Integer, _
                              ByVal Comment As String, _
                              ByVal EmployeeID As Integer) As Boolean

    Function TranserAdjustment(ByVal SACode As ISaCode, ByRef Adjustment As IAdjustment, ByVal SkuNumber As String, ByVal TransferSkuNumber As String, ByVal AdjQty As Integer, ByVal AdjDate As Date, ByVal DRLNumber As String, ByVal Comment As String, ByVal EmployeeId As Integer) As Boolean
    Function GetSku(ByVal SACode As ISaCode, ByVal SkuNumber As String, ByVal AdjDate As Date) As Boolean

    Function GetDRLAdjustment(ByVal SACode As ISaCode, ByVal DRLNumber As String, ByVal SkuNumber As String) As Boolean

    Function GetAdjustment(ByVal SACode As ISaCode, ByRef AdjQty As Integer, ByVal SkuNumber As String, ByVal StockAdjust As IAdjustment) As Boolean
    Function GetStoreNumber() As String

    Sub AdjustmentMakeEqual(ByRef Source As IAdjustment, ByRef Target As IAdjustment)

    ''' <summary>
    ''' Set the description for the entered SKU number
    ''' </summary>
    ''' <param name="container">Container of SKU number and description controls</param>
    Sub SetDescriptionForSkuNumber(ByVal container As ISkuNumberAndDescriptionContainer)

End Interface