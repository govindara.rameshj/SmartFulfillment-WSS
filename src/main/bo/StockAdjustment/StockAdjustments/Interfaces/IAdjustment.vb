﻿Option Strict On

Public Interface IAdjustment

    Enum ValidationResult
        Valid
        InvalidSku
        InvalidSkuNotFound
        InvalidTransferSku
        IvalidDummyItem
        InvalidDeletedZeroStock
        InvalidNotAllowed
        InvalidAlreadyDone
    End Enum

    Enum AdjustmentType
        Normal
        Transfer
    End Enum

    Property AdjustmentDate_DATE1() As Date
    Property Code() As String
    Property SKUN() As String
    Property SEQN() As String
    Property AmendId() As Integer
    Property EmployeeInitials_INIT() As String
    Property Department() As String
    Property StartingStock_SSTK() As Decimal
    Property Quantity_QUAN() As Decimal
    Property Price() As Decimal
    Property Cost() As Decimal
    Property IsSentToHO_COMM() As Boolean
    Property AdjustType() As String
    Property Comment_INFO() As String
    Property DeliveryNumber_DRLN() As String
    Property ReversalCode_RCOD() As String
    Property MarkDownOrWriteOff_MOWT() As String
    Property WriteOffAuthority_WAUT() As String
    Property WriteOffAuthorityDate_DAUT() As Date
    Property PeriodId() As Integer
    Property TransferSku_TSKU() As String
    Property TransferValue() As Decimal
    Property TransferStart() As Decimal
    Property TransferPrice() As Decimal
    Property IsReversed() As Boolean
    Property RTI() As String
    Property ExistsInDatabase() As Boolean
    Property EndingStock() As Decimal

    Function CalculateAdjustment() As Boolean

    Function SaveAdjustment(ByRef Con As Connection, ByVal AdjQty As Integer, ByVal AdjDate As Date, ByVal SkuNumber As String, ByVal DRLNumber As String, ByVal Comment As String, ByVal SACode As ISaCode, ByVal Stock As IStock, ByVal StockLog As IStockLog, ByVal EmployeeId As Integer, Optional ByVal TransferSku As String = "", Optional ByVal TransferValue As Decimal = 0, Optional ByVal TransferPrice As Decimal = 0, Optional ByVal TransferStart As Decimal = 0, Optional ByVal PicCountAcceptance As Boolean = False, Optional ByVal PicCountAcceptanceCode53 As Boolean = False) As Boolean
    Function UpdateAdjustment(ByRef Con As Connection) As Boolean

    Function ReadAdjustment(ByRef Con As Connection, ByVal Skun As String, ByVal AdjDate As Date, ByVal CodeNumber As String) As Boolean
    Function Validate(ByVal stock As IStock) As Boolean
    Function ReadAdjustmentDate() As Date
    Function GetEmployeeInitials(ByVal Id As Integer) As String

    Sub LoadFromRow(ByVal dr As DataRow)

    Sub MakeEqual(ByVal Value As IAdjustment)

End Interface
