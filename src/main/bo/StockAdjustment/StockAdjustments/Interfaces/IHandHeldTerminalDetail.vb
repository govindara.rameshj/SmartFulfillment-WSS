﻿Option Strict On
Option Explicit On

Public Interface IHandHeldTerminalDetail

    Property AdjustmentApplied() As Boolean     'HHTDET:IADJ
    Property HHTDate() As Date                  'HHTDET:DATE1
    Property LabelDetailCorrect() As Boolean    'HHTDET:LBOK
    'Property LocationCount() As Integer
    Property MarkDownStock() As Integer         'HHTDET:MDNQ
    Property NormalOnHandStock() As Integer     'HHTDET:ONHA
    Property ProductCode() As String            'HHTDET:SKUN
    Property StockExist() As Boolean
    Property StockSold() As Integer             'HHTDET:StockSold
    Property TotalCount() As Integer            'HHTDET:TCNT
    Property TotalMarkDownCount() As Integer    'HHTDET:TMDC
    Property TotalPreSoldStock() As Integer     'HHTDET:TPRE

    Property NormalSellingPrice() As Decimal    'STKMAS:PRIC
    Property ProductDescription() As String     'STKMAS:DESCR
    Property SupplierNumber() As String         'STKMAS:SUPP
    Property SupplierPartCode() As String       'STKMAS:PROD
    Property Original() As String               'HHTDET:ORIG

    Sub Update(ByVal SelectedDate As Date, ByVal ProductCode As String, ByVal AdjustmentApplied As Boolean)

End Interface

Public Interface IHandHeldTerminalDetailCollection

    Sub ReadDetail(ByVal SelectedDate As Date)


End Interface