﻿Option Strict On
Option Explicit On

Imports Cts.Oasys.Core.Net4Replacements

Public Interface IHandHeldTerminalHeader

    Property HeaderExist() As Boolean                            'flag to indicate that a header record exist in repository
    Property HHTDate() As Date                                   'HHTHDR:DATE1
    Property ItemsToCount() As Integer                           'HHTHDR:NUMB
    Property ItemsCounted() As Integer                           'HHTHDR:DONE
    Property AdjustmentApplied() As Boolean                      'HHTHDR:IADJ
    Property AuthorisationID() As String                         'HHTHDR:AUTH
    Property IsLocked() As Boolean                               'HHTHDR:IsLocked
    Property Details() As HandHeldTerminalDetailCollection

    Sub ReadLatestHHT()
    Sub ReadHHT(ByVal AdjDate As Date)
    Sub Update(ByVal SelectedDate As Date, ByVal ItemsToCount As System.Nullable(Of Integer), ByVal ItemsCounted As System.Nullable(Of Integer), _
               ByVal AdjustmentApplied As System.Nullable(Of Boolean), ByVal AuthorisationID As String, ByVal IsLocked As System.Nullable(Of Boolean))
    Function CheckAdjustmentsAuthorizingNeeded() As Tuple(Of Boolean, String)
    'Sub CalculateHeaderTotals()

    Event ClearProgress()
    Event UpdateProgess(ByVal percent As Integer, ByVal message As String)

End Interface
