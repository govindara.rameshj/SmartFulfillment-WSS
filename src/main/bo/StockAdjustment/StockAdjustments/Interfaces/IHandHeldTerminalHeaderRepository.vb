﻿Option Strict On
Option Explicit On

Public Interface IHandHeldTerminalHeaderRepository

    Function ReadLatestHHT() As DataRow
    Function ReadHHT(ByVal AdjDate As Date) As DataRow
    Sub Update(ByVal SelectedDate As Date, ByVal ItemsToCount As System.Nullable(Of Integer), ByVal ItemsCounted As System.Nullable(Of Integer), _
               ByVal AdjustmentApplied As System.Nullable(Of Boolean), ByVal AuthorisationID As String, ByVal IsLocked As System.Nullable(Of Boolean))

End Interface
