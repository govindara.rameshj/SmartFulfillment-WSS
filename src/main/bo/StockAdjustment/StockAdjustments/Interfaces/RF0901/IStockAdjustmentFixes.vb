﻿Option Strict On

Public Interface IStockAdjustmentFixes

#Region "Create Form Fixes"

#Region "Properties"

    WriteOnly Property AllCodeFourStockAdjustments() As IAdjustments

#End Region

#Region "General"

    Function GetAdjustment(ByVal StockAdjustmentCode As ISaCode, _
                           ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                           ByRef AdjustmentQuantity As Integer, _
                           ByVal SkuNumber As String, _
                           ByVal StockAdjustment As IAdjustment) As Boolean

    Function GetDrlAdjustment(ByVal StockAdjustmentCode As ISaCode, _
                              ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                              ByRef DailyReceiverListingTextControl As DevExpress.XtraEditors.TextEdit, _
                              ByVal DrlNumber As String, _
                              ByVal SkuNumber As String) As Boolean

    Sub CreateOrMaintainOrReverseAdjustmentOptions(ByVal StockAdjustmentCode As ISaCode, _
                                                   ByVal SkuPanelVisibility As Boolean, _
                                                   ByVal DevExpressGridGroupRow As Boolean, _
                                                   ByVal StockAdjustmentAmendID As Integer, _
                                                   ByVal StockAdjustment As IAdjustment, _
                                                   ByRef CreateAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                   ByRef MaintainAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                   ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton)

    Function DailyReceiverListingNumber(ByVal StockAdjustmentCode As ISaCode, ByVal DrlNumber As String) As String

    Sub HighestSequenceNo(ByVal StockAdjustmentCode As ISaCode, ByVal StockAdjustmentCollection As IAdjustments)

    Sub DisableReverseAdjustmentButton(ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton)

#End Region

#Region "Create Adjustment"

    Sub CreateAdjustmentCodeFourOnly(ByVal StockAdjustmentCode As ISaCode, _
                                     ByVal StockAdjustmentManager As IStockAdjustmentManager, _
                                     ByRef StockAdjustment As IAdjustment)

#End Region

#Region "Reverse Adjustment"

    Sub CodeFourReverseAdjustCancel(ByVal StockAdjustmentCode As ISaCode, ByRef StockAdjustment As IAdjustment)

    Sub CodeFourReverseAdjustCompletedMessage(ByVal StockAdjustmentCode As ISaCode, _
                                              ByVal StockAdjustment As IAdjustment, _
                                              ByVal EventArguement As System.Windows.Forms.KeyPressEventArgs, _
                                              ByRef GridView As DevExpress.XtraGrid.Views.Grid.GridView)

    Function CodeFourReverseAdjustEndStock(ByVal StockAdjustmentCode As ISaCode, _
                                           ByVal StockAdjustment As IAdjustment, _
                                           ByRef StartingStockTextControl As DevExpress.XtraEditors.TextEdit, _
                                           ByRef AdjustmentValueTextControl As DevExpress.XtraEditors.TextEdit) As Decimal

#End Region

#Region "Grid Control Event(s)"

    Sub GridControlSelectionChangedEvent(ByVal StockAdjustmentCode As ISaCode, _
                                         ByRef SelectionChangedEvent As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler, _
                                         ByRef GridControl As DevExpress.XtraGrid.GridControl, _
                                         ByRef GridView As DevExpress.XtraGrid.Views.Grid.GridView)

    Sub GridControlRefreshDataSourceEvent(ByVal StockAdjustmentCode As ISaCode, _
                                          ByRef StockAdjustmentCollection As IAdjustments, _
                                          ByRef GridControl As DevExpress.XtraGrid.GridControl)

#End Region

#Region "Daily Receiver Listing Control Event"

    Sub DailyReceiverListingControlValidatingEvent(ByVal StockAdjustmentCode As ISaCode, _
                                                   ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                                   ByRef SpecificStockAdjustment As IAdjustment, _
                                                   ByRef StockAdjustmentCollection As IAdjustments, _
                                                   ByVal SkuNumber As String, _
                                                   ByVal CancelSelected As Boolean, _
                                                   ByRef CreateAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                   ByRef MaintainAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                   ByRef ReverseAdjustmentButton As DevExpress.XtraEditors.SimpleButton, _
                                                   ByRef DailyReceiverListingTextControl As DevExpress.XtraEditors.TextEdit, _
                                                   ByRef DailyReceiverListingEventArguement As System.ComponentModel.CancelEventArgs)

#End Region

#End Region

#Region "Stock Adjustment Manager Fixes"

    Function ReadDRL(ByVal StockAdjustmentCode As ISaCode, _
                     ByRef Validator As IDRLValidator, _
                     ByVal DrlNumber As String, _
                     ByVal SkuNumber As String) As Boolean

    Function PersistNormalAdjustments(ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                      ByVal StockAdjustmentCode As ISaCode, _
                                      ByRef ActualStockAdjustment As IAdjustment, _
                                      ByRef ExistingStockAdjustment As IAdjustment, _
                                      ByVal SkuNumber As String, _
                                      ByVal AdjustmentDate As Date, _
                                      ByVal DRLNumber As String, _
                                      ByRef AdjustmentQuantity As Integer, _
                                      ByVal Comment As String, _
                                      ByVal EmployeeID As Integer) As Boolean

    Sub ConfigureExistingAdjustment(ByRef StockAdjustmentManager As IStockAdjustmentManager, _
                                    ByVal StockAdjustmentCode As ISaCode, _
                                    ByRef Source As IAdjustment, _
                                    ByRef Target As IAdjustment)

    Sub ConfigureNewAdjustment(ByRef Value As IAdjustment)

    'NOT REQUIRED
    '"Cannot negatively adjust...." message for F8 reversal not an issue; txtSkuAdjust control validating event not called, now readonly
    '"Cannot negatively adjust...." message for F7 maintenance not an issue since adjust existing adjustment

    'Function ValidAdjustmentQuantity(ByVal AdjustmentQuantity As Integer, _
    '                                 ByVal ReceiptQuantity As Integer, _
    '                                 ByVal ExistingAdjustmentQuantity As Integer) As Boolean

#End Region

End Interface