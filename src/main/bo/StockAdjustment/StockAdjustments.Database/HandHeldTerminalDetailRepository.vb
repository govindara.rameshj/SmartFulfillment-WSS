﻿<TestClass()> Public Class HandHeldTerminalDetailRepository

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

#Region "Stored Procedure Unit Test: LoadRefundedItems"

    <TestMethod()> Public Sub LoadRefundedItems_SP_Against_DS_SingleDate_SingleDetailRecordPerDate_RecordCountCheck_ExactMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable
        Dim SelectedDate As Date

        'arrange & act: add data
        SelectedDate = Now.AddDays(-1)
        InsertHHTHDR(SelectedDate, False, False)
        InsertHHTDET(SelectedDate, "111111", False, False, False, "R")

        'arrange & act: get data
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(Now.Date, -1)
        StoredProcedureDT = LoadRefundedItemsNewStoredProcedure(Now.Date, -1)
        'arrange & act: delete data
        DeleteHHTDET(SelectedDate, "111111")
        DeleteHHTHDR(SelectedDate)

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsTrue(DataTableRecordCountMatch(DynamicSequelDT, StoredProcedureDT))

    End Sub

    <TestMethod()> Public Sub LoadRefundedItems_SP_Against_DS_MultipleDates_SingleDetailRecordPerDate_RecordCountCheck_MisMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable
        Dim SelectedDateOne As Date
        Dim SelectedDateTwo As Date

        'arrange & act: add data
        SelectedDateOne = Now.AddDays(-1)
        SelectedDateTwo = Now.AddDays(-2)

        InsertHHTHDR(SelectedDateOne, False, False)
        InsertHHTDET(SelectedDateOne, "111111", False, False, False, "R")

        InsertHHTHDR(SelectedDateTwo, False, False)
        InsertHHTDET(SelectedDateTwo, "222222", False, False, False, "R")

        'arrange & act
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(Now.Date, -2)
        StoredProcedureDT = LoadRefundedItemsNewStoredProcedure(Now.Date, -2)
        'arrange & act: delete data
        DeleteHHTDET(SelectedDateTwo, "222222")
        DeleteHHTHDR(SelectedDateTwo)

        DeleteHHTDET(SelectedDateOne, "111111")
        DeleteHHTHDR(SelectedDateOne)

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsFalse(DataTableRecordCountMatch(DynamicSequelDT, StoredProcedureDT))

    End Sub

    <TestMethod()> Public Sub LoadRefundedItems_SP_Against_DS_MinusDaysZero_FieldNameCheck_ExactMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        'arrange & act
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(Now.Date, 0)
        StoredProcedureDT = LoadRefundedItemsNewStoredProcedure(Now.Date, 0)

        'column zero - SKUN
        Assert.IsTrue(DataTableFieldMatch(DynamicSequelDT, StoredProcedureDT))

    End Sub

    <TestMethod()> Public Sub LoadRefundedItems_SP_Against_DS_SingleDate_SingleDetailRecordPerDate_DataCheck_ExactMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable
        Dim SelectedDate As Date

        'arrange & act: add data
        SelectedDate = Now.AddDays(-1)
        InsertHHTHDR(SelectedDate, False, False)
        InsertHHTDET(SelectedDate, "111111", False, False, False, "R")

        'arrange & act: get data
        DynamicSequelDT = LoadRefundedItemsExistingDynamicSequel(Now.Date, -1)
        StoredProcedureDT = LoadRefundedItemsNewStoredProcedure(Now.Date, -1)
        'arrange & act: delete data
        DeleteHHTDET(SelectedDate, "111111")
        DeleteHHTHDR(SelectedDate)

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsTrue(DataTableDataMatch(DynamicSequelDT, StoredProcedureDT, "SKUN asc"))

    End Sub

#End Region

#Region "Stored Procedure Unit Test: RefundRandomSku"

    <TestMethod()> Public Sub RefundRandomSku_SP_Against_DS_MinusDays1_MinimumPrice25_RandomPercentage10_RecordCountCheck_ExactMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        'arrange & act: add data
        SaleHeaderAdd(Now.Date.AddDays(-1), "99", "9999", "RF", False, False, False, False, False, False, False, False, False, False, False, False, CType("", Char))
        SaleLineAdd(Now.Date.AddDays(-1), "99", "9999", 1, "999999", False, -9999, -9999, False, False, False, False, False, False)
        SaleLineAdd(Now.Date.AddDays(-1), "99", "9999", 2, "999998", False, -9999, 0, False, False, False, False, False, False)
        SalePaidAdd(Now.Date.AddDays(-1), "99", "9999", 1, False, False, False)

        'arrange & act: get data
        DynamicSequelDT = RefundRandomSkuExistingDynamicSequel(Now.Date, -1, 25, 10)
        StoredProcedureDT = RefundRandomSkuNewStoredProcedure(Now.Date, -1, 25, 10)

        'arrange & act: delete data
        DeleteSale(Now.Date.AddDays(-1), "99", "9999")

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsTrue(DataTableRecordCountMatch(DynamicSequelDT, StoredProcedureDT))

    End Sub

    <TestMethod()> Public Sub RefundRandomSku_SP_Against_DS_FieldNameCheck_ExactMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        DynamicSequelDT = RefundRandomSkuExistingDynamicSequel(Now.Date, 0, 0, 0)
        StoredProcedureDT = RefundRandomSkuNewStoredProcedure(Now.Date, 0, 0, 0)

        Assert.IsTrue(DataTableFieldMatch(DynamicSequelDT, StoredProcedureDT))

    End Sub

    <TestMethod()> Public Sub RefundRandomSku_SP_Against_DS_MinusDays1_MinimumPrice25_RandomPercentage10_DataCheck_ExactMatch()

        'returns an random dataset; not testable
        Assert.IsTrue(True)

    End Sub

#End Region

#Region "Stored Procedure Unit Test: DetailRefund"

    <TestMethod()> Public Sub DetailRefund_SP_Against_DS_MinusDays1_MinimumPrice25_RecordCountCheck_ExactMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        'arrange & act: add data
        SaleHeaderAdd(Now.Date.AddDays(-1), "99", "9999", "RF", False, False, False, False, False, False, False, False, False, False, False, False, CType("", Char))
        SaleLineAdd(Now.Date.AddDays(-1), "99", "9999", 1, "999999", False, -9999, -9999, False, False, False, False, False, False)
        SalePaidAdd(Now.Date.AddDays(-1), "99", "9999", 1, False, False, False)

        'arrange & act: get data
        DynamicSequelDT = DetailRefundExistingDynamicSequel(Now.Date, -1, 25)
        StoredProcedureDT = DetailRefundNewStoredProcedure(Now.Date, -1, 25)

        'arrange & act: delete data
        DeleteSale(Now.Date.AddDays(-1), "99", "9999")

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsTrue(DataTableRecordCountMatch(DynamicSequelDT, StoredProcedureDT))

    End Sub

    <TestMethod()> Public Sub DetailRefund_SP_Against_DS_ValiateFieldNames_ExactMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        DynamicSequelDT = DetailRefundExistingDynamicSequel(Now.Date, 0, 0)
        StoredProcedureDT = DetailRefundNewStoredProcedure(Now.Date, 0, 0)

        Assert.IsTrue(DataTableFieldMatch(DynamicSequelDT, StoredProcedureDT))

    End Sub

    <TestMethod()> Public Sub DetailRefund_SP_Against_DS_MinusDays1_MinimumPrice25_DataCheck_ExactMatch()

        Dim DynamicSequelDT As DataTable
        Dim StoredProcedureDT As DataTable

        'arrange & act: add data
        SaleHeaderAdd(Now.Date.AddDays(-1), "99", "9999", "RF", False, False, False, False, False, False, False, False, False, False, False, False, CType("", Char))
        SaleLineAdd(Now.Date.AddDays(-1), "99", "9999", 1, "999999", False, -9999, -9999, False, False, False, False, False, False)
        SalePaidAdd(Now.Date.AddDays(-1), "99", "9999", 1, False, False, False)

        'arrange & act: get data
        DynamicSequelDT = DetailRefundExistingDynamicSequel(Now.Date, -1, 25)
        StoredProcedureDT = DetailRefundNewStoredProcedure(Now.Date, -1, 25)

        'arrange & act: delete data
        DeleteSale(Now.Date.AddDays(-1), "99", "9999")

        If DynamicSequelDT.Rows.Count = 0 Then Assert.IsTrue(False, "No data returned")

        Assert.IsTrue(DataTableDataMatch(DynamicSequelDT, StoredProcedureDT, "SKUN asc"))

    End Sub

#End Region

#Region "Private Test Procedures And Functions"

#Region "DataTable"

    Private Function DataTableRecordCountMatch(ByRef DT1 As DataTable, ByRef DT2 As DataTable) As Boolean

        If DT1.Rows.Count = DT2.Rows.Count Then

            Return True

        Else

            Return False

        End If

    End Function

    Private Function DataTableFieldMatch(ByRef DT1 As DataTable, ByRef DT2 As DataTable) As Boolean

        Try
            'compare DT1 against DT2
            For ColumnIndex As Integer = 0 To DT1.Columns.Count - 1 Step 1

                If DT1.Columns.Item(ColumnIndex).ColumnName.ToUpper <> DT2.Columns.Item(ColumnIndex).ColumnName.ToUpper Then Return False

            Next
            'compare DT2 against DT1
            For ColumnIndex As Integer = 0 To DT2.Columns.Count - 1 Step 1

                If DT2.Columns.Item(ColumnIndex).ColumnName.ToUpper <> DT1.Columns.Item(ColumnIndex).ColumnName.ToUpper Then Return False

            Next

        Catch ex As Exception
            'most likely error is missing column
            Return False

        End Try

        Return True

    End Function

    Private Function DataTableDataMatch(ByRef DT1 As DataTable, ByRef DT2 As DataTable, Optional ByVal OrderBy As String = "") As Boolean

        Dim RowCount As Integer
        Dim ColumnCount As Integer

        Try
            'compare DT1 against DT2
            RowCount = DT1.Rows.Count - 1
            ColumnCount = DT1.Columns.Count - 1

            If OrderBy.Length > 0 Then

                DT1.DefaultView.Sort = OrderBy
                DT2.DefaultView.Sort = OrderBy

            End If

            For RowIndex As Integer = 0 To RowCount Step 1
                For ColumnIndex As Integer = 0 To ColumnCount Step 1

                    Select Case DT1.Columns.Item(ColumnIndex).DataType.FullName
                        Case "System.Int32"
                            If CType(DT1.Rows.Item(RowIndex).Item(ColumnIndex), Integer) <> CType(DT2.Rows.Item(RowIndex).Item(ColumnIndex), Integer) Then Return False

                        Case "System.String"
                            If CType(DT1.Rows.Item(RowIndex).Item(ColumnIndex), String) <> CType(DT2.Rows.Item(RowIndex).Item(ColumnIndex), String) Then Return False

                    End Select

                Next
            Next
            'compare DT2 against DT1
            RowCount = DT2.Rows.Count - 1
            ColumnCount = DT2.Columns.Count - 1

            For RowIndex As Integer = 0 To RowCount Step 1
                For ColumnIndex As Integer = 0 To ColumnCount Step 1

                    Select Case DT2.Columns.Item(ColumnIndex).DataType.FullName
                        Case "System.Int32"
                            If CType(DT2.Rows.Item(RowIndex).Item(ColumnIndex), Integer) <> CType(DT1.Rows.Item(RowIndex).Item(ColumnIndex), Integer) Then Return False

                        Case "System.String"
                            If CType(DT2.Rows.Item(RowIndex).Item(ColumnIndex), String) <> CType(DT1.Rows.Item(RowIndex).Item(ColumnIndex), String) Then Return False

                    End Select

                Next
            Next

        Catch ex As Exception

            'most likely error is missing column, or mismatched data types
            Return False

        End Try

        Return True

    End Function

#End Region

#Region "Sale: Create and Remove"

    Private Sub SaleHeaderAdd(ByVal DATE1 As Date, ByVal TILL As String, ByVal TRAN As String, ByVal TCOD As String, ByVal ACCT As Boolean, _
                              ByVal VOID As Boolean, ByVal TMOD As Boolean, ByVal PROC As Boolean, ByVal SUSE As Boolean, _
                              ByVal AUPD As Boolean, ByVal BACK As Boolean, ByVal ICOM As Boolean, ByVal IEMP As Boolean, _
                              ByVal PARK As Boolean, ByVal PKRC As Boolean, ByVal REMO As Boolean, ByVal RTI As Char)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                NotExist.Append("if not exists(select * from DLTOTS where DATE1  = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                NotExist.Append("                                   and   Till   = '" & TILL & "' ")
                NotExist.Append("                                   and   [TRAN] = '" & TRAN & "' ")
                NotExist.Append("             )")

                SB.Append(NotExist.ToString)
                SB.Append("begin")
                SB.Append("   insert DLTOTS (DATE1, TILL, [TRAN], TCOD, ACCT, VOID, TMOD, [PROC], SUSE, AUPD, BACK, ICOM, IEMP, PARK, PKRC, REMO, RTI) ")
                SB.Append("          values ('" & DATE1.ToString("yyyy-MM-dd") & "', ")
                SB.Append("                  '" & TILL & "', ")
                SB.Append("                  '" & TRAN & "', ")
                SB.Append("                  '" & TCOD & "', ")
                SB.Append("                  '" & ACCT.ToString & "',  ")
                SB.Append("                  '" & VOID.ToString & "', ")
                SB.Append("                  '" & TMOD.ToString & "', ")
                SB.Append("                  '" & PROC.ToString & "', ")
                SB.Append("                  '" & SUSE.ToString & "', ")
                SB.Append("                  '" & AUPD.ToString & "', ")
                SB.Append("                  '" & BACK.ToString & "', ")
                SB.Append("                  '" & ICOM.ToString & "', ")
                SB.Append("                  '" & IEMP.ToString & "', ")
                SB.Append("                  '" & PARK.ToString & "', ")
                SB.Append("                  '" & PKRC.ToString & "', ")
                SB.Append("                  '" & REMO.ToString & "', ")
                SB.Append("                  '" & RTI & "' ")
                SB.Append("                 )")
                SB.Append("end")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub SaleLineAdd(ByVal DATE1 As Date, ByVal TILL As String, ByVal TRAN As String, ByVal NUMB As Integer, _
                            ByVal SKUN As String, ByVal IBAR As Boolean, ByVal QUAN As Decimal, ByVal EXTP As Decimal, ByVal RITM As Boolean, _
                            ByVal ITAG As Boolean, ByVal CATA As Boolean, ByVal LREV As Boolean, ByVal IMDN As Boolean, ByVal BDCOInd As Boolean)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                NotExist.Append("if not exists(select * from DLLINE where DATE1  = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                NotExist.Append("                                   and   Till   = '" & TILL & "' ")
                NotExist.Append("                                   and   [TRAN] = '" & TRAN & "' ")
                NotExist.Append("                                   and   NUMB   =  " & NUMB.ToString & " ")
                NotExist.Append("             )")

                SB.Append(NotExist.ToString)
                SB.Append("begin")
                SB.Append("   insert DLLINE (DATE1, TILL, [TRAN], NUMB, SKUN, IBAR, QUAN, EXTP, RITM, ITAG, CATA, LREV, IMDN, BDCOInd) ")
                SB.Append("          values ('" & DATE1.ToString("yyyy-MM-dd") & "', ")
                SB.Append("                  '" & TILL & "', ")
                SB.Append("                  '" & TRAN & "', ")
                SB.Append("                   " & NUMB.ToString & ",  ")
                SB.Append("                  '" & SKUN & "', ")
                SB.Append("                  '" & IBAR.ToString & "', ")
                SB.Append("                   " & QUAN.ToString & ",  ")
                SB.Append("                   " & EXTP.ToString & ",  ")
                SB.Append("                  '" & RITM.ToString & "', ")
                SB.Append("                  '" & ITAG.ToString & "', ")
                SB.Append("                  '" & CATA.ToString & "', ")
                SB.Append("                  '" & LREV.ToString & "', ")
                SB.Append("                  '" & IMDN.ToString & "', ")
                SB.Append("                  '" & BDCOInd.ToString & "' ")
                SB.Append("                 )")
                SB.Append("end")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub SalePaidAdd(ByVal DATE1 As Date, ByVal TILL As String, ByVal TRAN As String, ByVal NUMB As Integer, ByVal CKEY As Boolean, _
                            ByVal DBRF As Boolean, ByVal ECOM As Boolean)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                NotExist.Append("if not exists(select * from DLPAID where DATE1  = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                NotExist.Append("                                   and   Till   = '" & TILL & "' ")
                NotExist.Append("                                   and   [TRAN] = '" & TRAN & "' ")
                NotExist.Append("             )")

                SB.Append(NotExist.ToString)
                SB.Append("begin")
                SB.Append("   insert DLPAID (DATE1, TILL, [TRAN], NUMB, CKEY, DBRF, ECOM) ")
                SB.Append("          values ('" & DATE1.ToString("yyyy-MM-dd") & "', ")
                SB.Append("                  '" & TILL & "', ")
                SB.Append("                  '" & TRAN & "', ")
                SB.Append("                   " & NUMB.ToString & ",  ")
                SB.Append("                  '" & CKEY.ToString & "', ")
                SB.Append("                  '" & DBRF.ToString & "', ")
                SB.Append("                  '" & ECOM.ToString & "' ")
                SB.Append("                 )")
                SB.Append("end")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub DeleteSale(ByVal DATE1 As Date, ByVal TILL As String, ByVal TRAN As String)

        Dim SB As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)
                'DLPAID
                SB.Append("delete DLPAID ")
                SB.Append("where DATE1  = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                SB.Append("and   TILL   = '" & TILL & "' ")
                SB.Append("and   [TRAN] = '" & TRAN & "' ")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()
                'DLLINE
                SB.Remove(0, SB.Length)
                SB.Append("delete DLLINE ")
                SB.Append("where DATE1  = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                SB.Append("and   TILL   = '" & TILL & "' ")
                SB.Append("and   [TRAN] = '" & TRAN & "' ")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()
                'DLTOTS
                SB.Remove(0, SB.Length)
                SB.Append("delete DLTOTS ")
                SB.Append("where DATE1  = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                SB.Append("and   TILL   = '" & TILL & "' ")
                SB.Append("and   [TRAN] = '" & TRAN & "' ")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

#End Region

#Region "HHTDET: Create and Remove"

    Private Sub InsertHHTHDR(ByVal DATE1 As Date, ByVal IADJ As Boolean, ByVal IsLocked As Boolean)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                NotExist.Append("if not exists(select * from HHTHDR where DATE1 = '" & DATE1.ToString("yyyy-MM-dd") & "')")

                SB.Append(NotExist.ToString)
                SB.Append("begin")
                SB.Append("   insert HHTHDR (DATE1, IADJ, IsLocked) ")
                SB.Append("          values ('" & DATE1.ToString("yyyy-MM-dd") & "', ")
                SB.Append("                  '" & IADJ.ToString & "', ")
                SB.Append("                  '" & IsLocked.ToString & "' ")
                SB.Append("                 )")
                SB.Append("end")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub InsertHHTDET(ByVal DATE1 As Date, ByVal SKUN As String, ByVal ICNT As Boolean, ByVal IADJ As Boolean, _
                             ByVal LBOK As Boolean, ByVal ORIG As String)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                NotExist.Append("if not exists(select * from HHTDET where DATE1 = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                NotExist.Append("                                   and   ORIG  = 'R' ")
                NotExist.Append("                                   and   ICNT  = '0' ")
                NotExist.Append("             )")

                SB.Append(NotExist.ToString)
                SB.Append("begin")
                SB.Append("   insert HHTDET (DATE1, SKUN, ICNT, IADJ, LBOK, ORIG) ")
                SB.Append("          values ('" & DATE1.ToString("yyyy-MM-dd") & "', ")
                SB.Append("                  '" & SKUN & "', ")
                SB.Append("                  '" & ICNT.ToString & "', ")
                SB.Append("                  '" & IADJ.ToString & "', ")
                SB.Append("                  '" & LBOK.ToString & "', ")
                SB.Append("                  '" & ORIG & "' ")
                SB.Append("                 )")
                SB.Append("end")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub DeleteHHTHDR(ByVal DATE1 As Date)

        Dim SB As New StringBuilder
        Dim NotExist As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                SB.Append("delete HHTHDR where DATE1 = '" & DATE1.ToString("yyyy-MM-dd") & "' ")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

    Private Sub DeleteHHTDET(ByVal DATE1 As Date, ByVal SKUN As String)

        Dim SB As New StringBuilder

        Using con As New Connection
            Using com As New Command(con)

                SB.Append("delete HHTDET ")
                SB.Append("where DATE1  = '" & DATE1.ToString("yyyy-MM-dd") & "' ")
                SB.Append("and   SKUN   = '" & SKUN & "' ")

                com.CommandText = SB.ToString
                com.ExecuteNonQuery()

            End Using
        End Using

    End Sub

#End Region

#Region "LoadRefundedItems"

    Private Function LoadRefundedItemsExistingDynamicSequel(ByVal DateCreated As Date, ByVal Offset As Integer) As DataTable

        Dim SB As New StringBuilder
        Dim DT As DataTable

        SB.Append("Select Distinct SKUN from HHTDET where ORIG = 'R' and ICNT = '0' and DATE1 >= ")
        SB.Append("'" & Format(DateCreated.AddDays(Offset), "yyyy-MM-dd") & "'")

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using
        Return DT

    End Function

    Private Function LoadRefundedItemsNewStoredProcedure(ByVal DateCreated As Date, ByVal Offset As Integer) As DataTable

        Dim DT As DataTable

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = "usp_HandHeldTerminalDetailRefundItemsDistinctSKUs"
                com.AddParameter("@SelectedDate", DateCreated.AddDays(Offset), SqlDbType.Date)
                DT = com.ExecuteDataTable

            End Using
        End Using
        Return DT

    End Function

#End Region

#Region "RefundRandomSku"

    Private Function RefundRandomSkuExistingDynamicSequel(ByVal SelectedDate As Date, ByVal Offset As Integer, _
                                                          ByVal MinimumPrice As Decimal, ByVal PercentageReturned As Integer) As DataTable

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB
            'existing
            '.Append(" SELECT TOP " & PercentageReturned.ToString)
            '.Append(" PERCENT SKUN  From DLLINE as dl")
            '.Append(" inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
            '.Append(" dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN]  ")
            '.Append(" inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and  ")
            '.Append(" dp.TILL = dp.TILL and dp.[TRAN] = dl.[TRAN]  ")
            '.Append(" where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
            '.Append(" and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' and dl.DATE1 = ")
            '.Append("'" & SelectedDate.AddDays(Offset).ToString("yyyy-MM-dd") & "'")
            '.Append(" and dl.SKUN not in (Select SKUN  From DLLINE as dl ")
            '.Append(" inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
            '.Append(" dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN] ")
            '.Append(" inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and ")
            '.Append(" dp.TILL = dp.TILL and dp.[TRAN] = dl.[TRAN] ")
            '.Append(" where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
            '.Append(" and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' ")
            '.Append(" and dl.DATE1 = ")
            '.Append("'" & SelectedDate.AddDays(Offset).ToString("yyyy-MM-dd") & "'")
            '.Append(" group by dl.skun, dl.[tran] having sum(dl.extp) <= -")
            '.Append(MinimumPrice.ToString)
            '.Append(" )group by dl.SKUN, dl.[TRAN]  ORDER BY NEWID() ")

            .Append("select top " & PercentageReturned.ToString & " percent SKUN ")
            .Append("from DLLINE as dl                                           ")
            .Append("inner join DLTOTS as dt                                     ")
            .Append("      on  dt.DATE1  = dl.DATE1                              ")
            .Append("      and dt.TILL   = dl.TILL                               ")
            .Append("      and dt.[TRAN] = dl.[TRAN]                             ")
            .Append("inner join DLPAID as dp                                     ")
            .Append("      on  dp.DATE1  = dl.DATE1                              ")
            '.Append("     and dp.TILL   = dp.TILL                               ")                'incorrect
            .Append("      and dp.TILL   = dl.TILL                               ")
            .Append("      and dp.[TRAN] = dl.[TRAN]                             ")
            .Append("where dl.QUAN  < 0                                          ")
            .Append("and   dl.LREV  = 0                                          ")
            .Append("and   dl.DATE1 = '" & SelectedDate.AddDays(Offset).ToString("yyyy-MM-dd") & "' ")
            .Append("and   dl.SKUN not in (select SKUN                           ")
            .Append("                      from DLLINE as dl                     ")
            .Append("                      inner join DLTOTS as dt               ")
            .Append("                            on  dt.DATE1  = dl.DATE1        ")
            .Append("                            and dt.TILL   = dl.TILL         ")
            .Append("                            and dt.[TRAN] = dl.[TRAN]       ")
            .Append("                      inner join DLPAID as dp               ")
            .Append("                            on  dp.DATE1  = dl.DATE1        ")
            '.Append("                           and dp.TILL   = dp.TILL         ")                'incorrect
            .Append("                            and dp.TILL   = dl.TILL         ")
            .Append("                            and dp.[TRAN] = dl.[TRAN]       ")
            .Append("                      where dl.QUAN  < 0                    ")
            .Append("                      and   dl.LREV  = 0                    ")
            .Append("                      and   dl.DATE1 = '" & SelectedDate.AddDays(Offset).ToString("yyyy-MM-dd") & "' ")
            .Append("                      and   dt.TCOD  = 'RF'                 ")
            .Append("                      and   dt.VOID  = 0                    ")
            .Append("                      and   dt.TMOD  = 0                    ")
            .Append("                      group by dl.skun, dl.[tran]           ")
            .Append("                      having sum(dl.extp) <= -" & MinimumPrice.ToString & ") ")
            .Append("and   dt.TCOD  = 'RF'                                       ")
            .Append("and   dt.VOID  = 0                                          ")
            .Append("and   dt.TMOD  = 0                                          ")
            .Append("group by dl.SKUN, dl.[TRAN]                                 ")
            .Append("order by NEWID()                                            ")
        End With

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using
        Return DT

    End Function

    Private Function RefundRandomSkuNewStoredProcedure(ByVal SelectedDate As Date, ByVal Offset As Integer, _
                                                       ByVal MinimumPrice As Decimal, ByVal PercentageReturned As Integer) As DataTable

        Dim DT As DataTable

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = "usp_HandHeldTerminalDetailRefundRandomSKU"
                com.AddParameter("@SelectedDate", SelectedDate.AddDays(Offset), SqlDbType.Date)
                com.AddParameter("@MinPrice", MinimumPrice, SqlDbType.Decimal)
                com.AddParameter("@RandPercent", PercentageReturned, SqlDbType.Int)

                DT = com.ExecuteDataTable

            End Using
        End Using
        Return DT

    End Function

#End Region

#Region "DetailRefund"

    Private Function DetailRefundExistingDynamicSequel(ByVal SelectedDate As Date, ByVal Offset As Integer, ByVal MinimumPrice As Decimal) As DataTable

        Dim SB As New StringBuilder
        Dim DT As DataTable

        With SB
            'existing
            '.Append("Select dl.SKUN From DLLINE as dl ")
            '.Append("inner join DLTOTS as dt on dt.DATE1 = dl.DATE1 and ")
            '.Append("dt.TILL = dl.TILL and dt.[TRAN] = dl.[TRAN] ")
            '.Append("inner join DLPAID as dp on dp.DATE1 = dl.DATE1 and ")
            '.Append("dp.TILL = dp.TILL and dp.[TRAN] = dl.[TRAN] ")
            '.Append("where dl.QUAN < 0 and dt.TCOD = 'RF'and dt.VOID = '0' ")
            '.Append("and dt.VOID = '0' and dt.TMOD = '0' and dl.LREV = '0' ")
            '.Append("and dl.DATE1 = ")
            '.Append("'" & SelectedDate.AddDays(Offset).ToString("yyyy-MM-dd") & "'")
            '.Append(" group by dl.skun, dl.[tran] having sum(dl.extp) <= -")
            '.Append(MinimumPrice.ToString)

            .Append("select dl.SKUN                  ")
            .Append("from DLLINE as dl               ")
            .Append("inner join DLTOTS as dt         ")
            .Append("      on  dt.DATE1  = dl.DATE1  ")
            .Append("      and dt.TILL   = dl.TILL   ")
            .Append("      and dt.[TRAN] = dl.[TRAN] ")
            .Append("inner join DLPAID as dp         ")
            .Append("      on  dp.DATE1  = dl.DATE1  ")
            '.Append("     and dp.TILL   = dp.TILL   ")                'incorrect
            .Append("      and dp.TILL   = dl.TILL   ")
            .Append("      and dp.[TRAN] = dl.[TRAN] ")
            .Append("where dl.QUAN  < 0              ")
            .Append("and   dl.LREV  = 0              ")
            .Append("and   dl.DATE1 = '" & SelectedDate.AddDays(Offset).ToString("yyyy-MM-dd") & "' ")
            .Append("and   dt.TCOD  = 'RF'           ")
            .Append("and   dt.VOID  = 0              ")
            .Append("and   dt.TMOD  = 0              ")
            .Append("group by dl.SKUN, dl.[TRAN]     ")
            .Append("having sum(dl.extp) <= -" & MinimumPrice.ToString)
        End With

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.CommandText = SB.ToString
                DT = com.ExecuteDataTable

            End Using
        End Using
        Return DT

    End Function

    Private Function DetailRefundNewStoredProcedure(ByVal SelectedDate As Date, ByVal Offset As Integer, ByVal MinimumPrice As Decimal) As DataTable

        Dim DT As DataTable

        DT = Nothing
        Using con As New Connection
            Using com As New Command(con)

                com.StoredProcedureName = "usp_HandHeldTerminalDetailRefund"
                com.AddParameter("@SelectedDate", SelectedDate.AddDays(Offset), SqlDbType.Date)
                com.AddParameter("@MinPrice", MinimumPrice, SqlDbType.Decimal)

                DT = com.ExecuteDataTable

            End Using
        End Using
        Return DT

    End Function

#End Region

#End Region

End Class
